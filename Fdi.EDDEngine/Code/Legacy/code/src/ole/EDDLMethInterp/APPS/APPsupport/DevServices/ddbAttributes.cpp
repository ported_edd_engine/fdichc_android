/*************************************************************************************************
 *
 * $Workfile: ddbAttributes.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2005, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The attribute''s upport code.  From ddbVar.cpp, and others to put all attribute code
 *	together for easier maintainence.  The new_Copy() method was added here.
 *
 */

#include "stdafx.h"
#include "ddbCollAndArr.h"


hCattrVarIndex::hCattrVarIndex(DevInfcHandle_t h, ITEM_ID ItemID ) : hCattrBase( h, 0 )
{
	m_pItemArray = new hCitemArrayValues( h, ItemID );
}

RETURNCODE hCattrVarIndex::getArrayPointer(hCitemBase*& pItemArray)
{
    pItemArray = m_pItemArray;
    return SUCCESS;
}

hCattrVarIndex::~hCattrVarIndex()
{
	if( m_pItemArray )
	{
		delete m_pItemArray;
		m_pItemArray = NULL;
	}
}
RETURNCODE hCattrVarDefault::getDfltVal(CValueVarient& returnedValue)
{	
	returnedValue=DefaultValue;
	return SUCCESS;
}
