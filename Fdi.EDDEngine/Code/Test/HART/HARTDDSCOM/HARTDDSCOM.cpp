// HARTDDSCOM.cpp : Implementation of DLL Exports.

#include "stdafx.h"
#include "resource.h"

// The module attribute causes DllMain, DllRegisterServer and DllUnregisterServer to be automatically implemented for you
[ module(dll, uuid = "{FFB5E80C-7D66-4949-810C-0751A189FAD1}", 
		 name = "HARTDDSCOM", 
		 helpstring = "HARTDDSCOM 1.0 Type Library",
		 resource_name = "IDR_HARTDDSCOM") ];
