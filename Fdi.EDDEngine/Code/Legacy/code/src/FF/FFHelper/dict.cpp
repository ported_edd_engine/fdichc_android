/**
 *	Copyright 1993 Rosemount Inc.
 * 	All rights reserved.
 *
 *		@(#) $Id: dict.cpp,v 1.7 1996/08/15 16:18:18 kimwolk Exp $
 */


#include "stdinc.h"

//#include <afxmt.h>
#include "panic.h"
#include "DDI_LIB.H"
#include "dict.h" // Contains implementation of classes so must be here
#include "../../../../../Inc/FF/FFEDDEngine/DDSSupport.h"
#include <Inf/NtcSpecies/BssProgLog.h>
#include <mutex>
#include <cstring>
#include <string>

#include <fstream>

StdDictTable g_StdDictTable;

#define OutputWarning(x)	PS_OutputDebugString(x)	// MHD Redirect error log for now

#define SB_CODE_PAGE	1252	// Code Page for singlebyte narrow/wide conversion

#ifdef _WIN32
#define STDFIELDBUSDCT	_T("EDDE_FF_StdFieldbusMap.dct")
#else
using namespace std;
#define STDFIELDBUSDCT	"EDDE_FF_StdFieldbusMap.dct"
#endif    

/*
 *	Formats for TOKEN_ERROR messages
 *  This never changes so it is a thread safe static
 */
static char *errfmt[] = {
	"string for language %03d not found in\n\t%s",
	"duplicate entries found for section %d, offset %d",
	"non contiguous entries, no entries between offsets %d and %d, sect %d",
	"invalid start of section %d",
	"high TOKEN_TEXT marker not highest offset in section %d",
	"high TOKEN_TEXT marker is missing for section %d",
};

/*
 *	The special TOKEN_TEXT the must be the highest offset for each section
 *  This never changes so it is a thread safe static
 */
static wchar_t *high_TOKEN_TEXT = L"|en|@XX@Highest Offset";


/*
 * Token values (lex() return values).
 */
#define TOKEN_ERROR 0
#define TOKEN_DONE 1
#define TOKEN_NUMBER 2
#define TOKEN_NAME 3
#define TOKEN_TEXT 4
#define TOKEN_COMMA 5



/*************************************************************
 * Class StdDictTable
 *************************************************************/
/*************************************************************
 * Constructor: Initialize variables that had been initialized
 *              global variables.
 *************************************************************/



StdDictTable::StdDictTable()
{
    m_bLoaded = false;
	
	dict_program = NULL;
    /*
     * Info kept in globals for efficiency.
     */
    dicterrs = 0;
    dictfp = NULL;
    dictline = 0;

    /*
     * The token buffer.
     */
    tokbase = NULL;
    tokptr = NULL;
    toklim = NULL;

    
    hFieldbusMap		= NULL;
    lpFieldbusView	    = NULL;
    pRuntimeMap	        = NULL;		// class members point into the lpFielbusView map file

    // former static from lex() function
    lex_nextchar = -1;

    // former static from dict_table_install() function
    install_dwCurrentSection = MAXDWORD;	// Initialize to something bad
	install_iSectionIndex = 0;
	install_iNextDictIndex = 0;
	install_iStringOffset = 0;

    // Array of DICT_EXT_ENTRYs for Fieldbus
    pDictExtArrayFieldbus = NULL;
}

/*************************************************************
 * Destructor: Close the std dict table, because this is now global.
 *************************************************************/
StdDictTable::~StdDictTable()
{
	CloseFFStdDictTable();
}


/*********************************************************************
 *
 *	Name: dicterr
 *	ShortDesc: print TOKEN_ERROR message
 *
 *	Description:
 *		Print an TOKEN_ERROR message on standard TOKEN_ERROR and returns.
 *		If the global variable "dict_program" is set, it is printed
 *		before the TOKEN_TEXT of the TOKEN_ERROR message.
 *
 *		We use this function instead of the other TOKEN_ERROR printing
 *		functions we have (such as TOKEN_ERROR(), panic(), etc), so that
 *		this file remains as self contained as possible. 
 *
 *	Inputs:
 *		msg - TOKEN_ERROR message to print
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Nothing
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

void
StdDictTable::dicterr(wchar_t *msg)
{
	OutputWarning(msg);
	dicterrs++;
}


/*********************************************************************
 *
 *	Name: growBuffer
 *
 *	ShortDesc: Grow the token buffer as needed
 *
 *	Description:
 *		The token buffer is always kept
 *		null terminated, and is grown as needed.
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/
void
StdDictTable::growBuffer()
{
	int off, size;

	//
	// Make sure token buffer exists, and can
	// accommodate another character. We keep
	// the token buffer null terminated, hence
	// the strange limit check.
	//
	off = tokptr - tokbase;
	size = toklim - tokbase;
	if (size == 0) 
	{
		size = 256;
		tokbase = (char *) malloc((unsigned)size);
	} 
	else 
	{
		size *= 2;
		char * oldtokbase = tokbase;
		tokbase = (char *) realloc(tokbase, (unsigned)size);
		if (tokbase == NULL)
		{
			free (oldtokbase);// if realloc fails, we need to free previous memory.
			ASSERT_ALL(0);
		}
	}
	if (tokbase == NULL) 
	{
		dicterr(L"memory exhausted");

		// :EXIT: 99/06/03 ksm - exit() should never be used. Replaced 
		// exit() with ASSERT_ALL() to catch execution of this branch.
		ASSERT_ALL(0);
	}
	
	tokptr = tokbase + off;
	toklim = tokbase + size;
}


/*********************************************************************
 *
 *	Name: inschar
 *
 *	ShortDesc: Insert character into the token buffer
 *
 *	Description:
 *		Insert character into the token buffer and null terminate.
 *		The token buffer is always kept null terminated, and is grown 
 *		as needed.
 *
 *	Inputs:
 *		c - character to be inserted
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/
inline void StdDictTable::inschar(int c)
{
	if (tokbase == NULL || tokptr + 1 == toklim) 
	{
		//
		// Grow the buffer and insert
		//
		growBuffer();
	}
	
	//
	// Insert the character, keeping the buffer null terminated.
	//
	*tokptr++ = (char) c;
	*tokptr = '\0';
}



/*********************************************************************
 *
 *	Name: comment
 *	ShortDesc: reads a C style comment
 *
 *	Description:
 *		Read a C style comment from the current dictionary
 *		file (dictfp). If an EOF is seen prior to the closing
 * 		of the comment, an TOKEN_ERROR is returned.
 *
 *	Inputs:
 *		None		
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		TOKEN_DONE - if successful
 *		TOKEN_ERROR - otherwise
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

int
StdDictTable::comment(void)
{
	int c;

	/*
	 * Verify we're at the start of a comment. If we don't
	 * find a valid start of comment, return an TOKEN_ERROR.
	 */
	c = getc(dictfp);
	if (c != '*') {
		return TOKEN_ERROR;
	}

	/*
	 * Consume the comment.
	 */
	c = getc(dictfp);
	for (;;) {
		if (c == EOF) {
			return TOKEN_ERROR;
		}

		/*
		 * If it's a *, check for end of comment.
		 */
		if (c == '\n') {
			++dictline;
		} else if (c == '*') {
			c = getc(dictfp);
			if (c == EOF) {
				return TOKEN_ERROR;
			}
			if (c == '\n') {
				++dictline;
			} else if (c == '/') {
				break;
			} else {
				continue;
			}
		}
		/*
		 * Next please.
		 */
		c = getc(dictfp);
	}

	/*
	 * Indicates valid comment consumed.
	 */
	return TOKEN_DONE;
}

/*********************************************************************
 *
 *	Name:  lex
 *	ShortDesc: standard dictionary lexical analyser
 *
 *	Description:
 *		Reads characters from the current dictionary file
 *		(dictfp) and translates them into tokens, which
 *		are parsed by parse() (see below).
 *
 *	Inputs:
 *		None
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		TOKEN_ERROR  - TOKEN_ERROR in the input
 *		TOKEN_DONE   - end of file
 *		TOKEN_NUMBER - standard dictionary TOKEN_NUMBER
 *		name   - standard dictionary name
 *		TOKEN_TEXT   - TOKEN_TEXT of a standard dictionary string
 *		TOKEN_COMMA  - the TOKEN_COMMA character
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

int
StdDictTable::lex(void)
{
	int c;

	/*
	 * Get next character.
	 */
	if (lex_nextchar < 0) {
		c = getc(dictfp);
	} else {
		c = lex_nextchar;
		lex_nextchar = -1;
	}

	for (;;) {
		switch (c) {
		case EOF:
			return TOKEN_DONE;

		/*
		 * Eat whitespace.
		 */
		case '\n':
			++dictline;
			/* FALL THROUGH */

		case ' ':
		case '\b':
		case '\f':
		case '\r':
		case '\t':
		case '\v':
			c = getc(dictfp);
			break;

		/*
		 * Eat a comment.
		 */
		case '/':
			if (comment() != TOKEN_DONE) 
			{
				return TOKEN_ERROR;
			}
			c = getc(dictfp);
			break;

		case ',':
			return TOKEN_COMMA;

		/*
		 * Read a TOKEN_NUMBER.
		 */
		case '[':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * Read a digit sequence.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isdigit(c));

			/*
			 * Read a TOKEN_COMMA.
			 */
			if (c != ',') 
			{
				return TOKEN_ERROR;
			}

			/*
			 * Read another digit sequence.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isdigit(c));

			/*
			 * Verify it ends correctly.
			 */
			if (c != ']') 
			{
				return TOKEN_ERROR;
			}
			inschar(c);

			/*
			 * Return it.
			 */
			return TOKEN_NUMBER;

		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
		case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
		case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
		case 'Y': case 'Z': case '_': case 'a': case 'b': case 'c':
		case 'd': case 'e': case 'f': case 'g': case 'h': case 'i':
		case 'j': case 'k': case 'l': case 'm': case 'n': case 'o':
		case 'p': case 'q': case 'r': case 's': case 't': case 'u':
		case 'v': case 'w': case 'x': case 'y': case 'z':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * Read a sequence of alphanumerics and underscores.
			 */
			do {
				inschar(c);
				c = getc(dictfp);
			} while (isalnum(c) || c == '_');

			/*
			 * Save the lookahead character.
			 */
			lex_nextchar = c;

			/*
			 * Return it.
			 */
			return TOKEN_NAME;

		case '\"':
			/*
			 * Clear the token buffer.
			 */
			tokptr = tokbase;

			/*
			 * If string does not start with a country code,
			 * add the default country code (English).
			 */
			c = getc(dictfp);
			if (c != '|') {
				inschar('|');
				inschar('e');
				inschar('n');
				inschar('|');
			}

			/*
			 * Read the characters of the string.
			 */
			for (;; c = getc(dictfp)) {
				switch (c) {
				/*
				 * Unterminated string.
				 */
				case EOF:
				case '\n':
					return TOKEN_ERROR;

				/*
				 * Anything can be escaped with a backslash,
				 * this will usually be \".
				 */
				case '\\':
					inschar(c);
					c = getc(dictfp);
					if (c == EOF) 
					{
						return TOKEN_ERROR;
					}
					inschar(c);
					break;

				/*
				 * We've come to the end of the string. Read ahead
				 * to see if there is string immediately following
				 * this one, and if there is read that one also.
				 */
				case '\"':
					c = getc(dictfp);
					for (;;) {
						switch (c) {
						/*
						 * Eat whitespace.
						 */
						case '\n':
							dictline++;
							/* FALL THROUGH */

						case ',':
						case ' ':
						case '\b':
						case '\f':
						case '\r':
						case '\t':
							c = getc(dictfp);
							continue;

						/*
						 * Eat a comment.
						 */
						case '/':
							if (comment() != TOKEN_DONE) 
							{
								return TOKEN_ERROR;
							}
							c = getc(dictfp);
							continue;

						/*
						 * We've seen something other than
						 * a comment or whitespace.
						 */
						default:
							break;
						}
						break;
					}

					/*
					 * If what we've seen is the start
					 * of another string, read it also.
					 */
					if (c == '\"')
						break;

					/*
					 * We've seen something other than
					 * the start of a string. Save the
					 * lookahead character.
					 */
					lex_nextchar = c;
					return TOKEN_TEXT;

				/*
				 * Ordinary character, add it to the token buffer.
				 */
				default:
					inschar(c);
					break;
				}
			}
			/*NOTREACHED*/

		/*
		 * Invalid input.
		 */
		default:
			return TOKEN_ERROR;
		}
	}
}

/*********************************************************************
 *
 *	Name: parse
 *	ShortDesc: standard dictionary parser
 *
 *	Description:
 *		Parses the current dictionary file (dictfp). Calls lex()
 *		to get tokens. When an TOKEN_ERROR is detected, it resyncronizes
 *		(by looking for the beginning of a valid dictionary entry)
 *		and continues parsing from that point. This allows multiple
 *		TOKEN_ERRORs in a dictionary file to be detected at once.
 *
 *	Inputs:
 *		None
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		Nothing
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

const int DICT_ARRAY_INCREASE = 1024;	// Grow dict_array by this amount when to small

void
StdDictTable::parse(DICT_ENT** out_DictArray, int* out_iDictCount, int* out_iNumSections, int* out_iCumStringLen, bool isUTF8 )
{
	unsigned long ulLastSection = MAXDWORD;

	/*
	 *	Create the DICT_ENT pointer array.
	 */

	int dict_limit = (15 * 1024);		// Initialize to something reasonable; can grow later
	int dict_count = 0;
	DICT_ENT* dict_array = (DICT_ENT *)malloc ((unsigned)(dict_limit * sizeof (*dict_array)));
	if (!dict_array) 
	{
		dicterr(L"memory exhausted");
		return;
	}

	int iNumSections = 0;	// Total number of sections found
	int iCumStringLen = 0;	// Cumulative dictionary string lengths including the '\0'

	/*
	 * Initialize.
	 */
	int tok = lex();

	DICT_ENT *curr_dict = dict_array + dict_count;
	DICT_ENT *end_dict = dict_array + dict_limit;

	while (tok != TOKEN_DONE) 
	{
		/*
		 * Verify the token is a TOKEN_NUMBER.
		 */
		if (tok != TOKEN_NUMBER) 
		{
			dicterr(L"syntax TOKEN_ERROR");
			do { 
				tok = lex();
			} 
			while (tok != TOKEN_DONE && tok != TOKEN_NUMBER);
			continue;
		}

		/*
		 * Extract the section TOKEN_NUMBER and offset from
		 * the token.
		 */
		char *last = NULL;
		unsigned long section = strtoul(tokbase + 1, &last, 10);
		unsigned long offset = strtoul(last + 1, (char **) 0, 10);

		if (section != ulLastSection)
		{
			iNumSections++;			// Count total number of sections
			ulLastSection = section;
		}

		/*
		 * Get next token and verify it's a string name.
		 */
		tok = lex();
		if (tok != TOKEN_NAME) 
		{
			dicterr(L"syntax TOKEN_ERROR");
			do 
			{
				tok = lex();
			} 
			while (tok != TOKEN_DONE && tok != TOKEN_NUMBER);
			continue;
		}

		/*
		 * Skip over the name. We don't need it here.
		 */

		/*
		 * Get the next token and verify it's a string.
		 */
		tok = lex();
		if (tok != TOKEN_TEXT) 
		{
			dicterr(L"syntax TOKEN_ERROR");
			do {
				tok = lex();
			} 
			while (tok != TOKEN_DONE && tok != TOKEN_NUMBER);
			continue;
		}


		/*
		 * Strip away unwanted languages.
		 */
		int nCovertionCode = SB_CODE_PAGE;
		if( isUTF8 )
		{
			nCovertionCode=CP_UTF8;
		}

		// Strip away unwanted languages.
		// We have to convert the multibyte input string to wide characters
		// to get the right language string, then convert the language string 
		// back to multibyte to write to the output file.
		int lenW = PS_MultiByteToWideChar(nCovertionCode, 0, tokbase, -1, NULL, 0);

		if (lenW == 0)
		{
			// This error shouldn't happen, but display a message indicating
			// where it occurred and quit.
			wchar_t szError[_MAX_PATH + 1] = { 0 };
            PS_WsPrintf(szError, _T("UTF-8 to UNICODE conversion failed at [%d,%d]"), section, offset);
            dicterr(szError);
			break;
		}

		wchar_t* strbuf = (wchar_t*)malloc(lenW * sizeof(wchar_t));

		if (strbuf == NULL)
		{
			dicterr(TEXT("memory exhausted"));
			// :EXIT: 99/06/03 ksm - exit() should never be used. Replaced 
			// exit() with ASSERT_ALL() to catch execution of this branch.
			ASSERT_ALL(0);
		}

		PS_MultiByteToWideChar(nCovertionCode, 0, tokbase, -1, strbuf, lenW);

		/*
		 *	Save this entry into dict_array, expanding it if necessary.
		 */

		if (curr_dict == end_dict) 
		{
			dict_count = dict_limit;
			dict_limit += DICT_ARRAY_INCREASE;
			dict_array = (DICT_ENT *)realloc ((char *)dict_array,
							(unsigned)(dict_limit * sizeof (*dict_array)));
			if (!dict_array) {
				dicterr(L"memory exhausted");

				// :EXIT: 99/06/03 ksm - exit() should never be used. Replaced 
				// exit() with ASSERT_ALL() to catch execution of this branch.
				ASSERT_ALL(0);
			}
			curr_dict = dict_array + dict_count;
			end_dict = dict_array + dict_limit;
		}

		curr_dict->wSection = (WORD)section;
		curr_dict->wOffset = (WORD)offset;
		curr_dict->dwRef = (section << 16) | offset;
		curr_dict->usLen = (unsigned short)wcslen(strbuf);
		curr_dict->dict_text = strbuf;
		curr_dict++;

		iCumStringLen += 2*(wcslen(strbuf)+1);		// Accumulate the total space required for the strings
		/*
		 *	Get the next token
		 */

		tok = lex();
	}

	dict_count = curr_dict - dict_array;

	*out_DictArray = dict_array;
	*out_iDictCount = dict_count;
	*out_iNumSections = iNumSections;
	*out_iCumStringLen = iCumStringLen;
}

/*********************************************************************
 *
 *	Name: compdict
 *	ShortDesc: Compare function for qsort of DICT_ENTs
 *
 *	Description
 *		Compares two DICT_ENT entries, and returns less than zero,
 *		equal zero, or greater than zero depending on whether the
 *		first entry is smaller, equal, or greater than the second.
 *
 *	Inputs:
 *		dict_ent1, dict_ent2 - pointers to the two DICT_ENT entries
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		< 0 - *dict_ent1 < *dict_ent2
 *		  0 - *dict_ent1 == *dict_ent2
 *		> 0 - *dict_ent1 > *dict_ent2
 *
 *	Author:
 *		Bruce Davis
 *********************************************************************/

int
StdDictTable::compdict (DICT_ENT *dict_ent1, DICT_ENT *dict_ent2)
{
	return (int)(dict_ent1->dwRef - dict_ent2->dwRef);
}

/*********************************************************************
 *
 *	Name: makedict
 *	ShortDesc: build a dictionary table
 *
 *	Description:
 *		Reads a dictionary file and creates a user defined
 *		dictionary table. The caller must supply an install
 *		function which is called every time a valid dictionary
 *		entry is read. This allows the caller to create as
 *		large or small a table as needed, and allows the 
 *		complete control over the structure of the table.
 *
 *		The install function takes three arguments:
 *			unsigned long value - string TOKEN_NUMBER
 *			char *TOKEN_TEXT			- TOKEN_TEXT of string
 *
 *		The TOKEN_TEXT arguments are malloc'd, and
 *	 	if the caller does not store them, they should be
 *		free'd. If a TOKEN_TEXT string does not contain the
 *		requested language (global variable "language"),
 * 		the TOKEN_TEXT argument may be 0. This situation is
 *		flagged as an TOKEN_ERROR, so exit'ing if the dictionary
 *		is not correctly parsed avoids this situation.
 *
 *	Inputs:
 *		file - file name of dictionary file
 *		func - install function
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		TOKEN_NUMBER of TOKEN_ERROR detected.
 *
 *	Author:
 *		Jon Westbrock
 *
 *********************************************************************/

int
StdDictTable::makedict(const CStdString& strFullPath)
{
	char *errbuf = NULL;

	/*
	 * Initialize the globals.
	 */
	dicterrs = 0;
	dictline = 1;

	/*
	 * Open the file.
	 */
    dictfp = PS_wfopen((LPCTSTR)strFullPath, _T("r"));
	if (!dictfp) {
		if (dict_program != NULL) {
			fprintf(stderr, "%s: ", dict_program);
		}
//		panic ("Unable to open standard dictionary file.");
		return (-1);//?????????
	}

	/*
	 * Read it.
	 */

	///////////////////////////////////////////////////////
	//
	// Allocate token buffer
	//
	inschar('\0');
	bool isUTF8 = false;
	int a = getc(dictfp);
	int b = getc(dictfp);
	int c = getc(dictfp);

	if( (a == 0xEF) && (b == 0xBB) && (c == 0xBF))
	{//this is a utf8 file header.  Read data from here.
		isUTF8 = true;
	}
	else//No it was not a header, we are standard text. Seek back to the start.
	{
		fseek( dictfp, 0L, SEEK_SET);
	}

	// Allocate output parameters for parse()
	DICT_ENT* dict_array = NULL;
	int dict_count = 0;
	int iNumSections = 0;	// Total number of sections found
	int iCumStringLen = 0;	// Cumulative dictionary string lengths including the '\0'

	try 
	{
		parse(&dict_array, &dict_count, &iNumSections, &iCumStringLen,isUTF8);
	} 
	catch(...)
	{
		dicterr(L"unhandled exception");

		// :EXIT: 99/06/03 ksm - exit() should never be used. Replaced 
		// exit() with ASSERT_ALL() to catch execution of this branch.
		ASSERT_ALL(0);
	}

	/*
	 * Close the file.
	 */
	fclose(dictfp);
	dictfp = NULL;

	if (tokbase) {
		free (tokbase);
		tokbase = NULL;
		tokptr = NULL;
		toklim = NULL;
	}


	///////////////////////////////////////////////////////

	/*
	 *	Sort the entries from the files
	 */

	(void)qsort ((char *)dict_array, dict_count, 
				sizeof (*dict_array), (CMP_FN_PTR)compdict);


	// Create the mapping
	DWORD rs = dict_FF_table_create(iNumSections, dict_count, iCumStringLen);
	if (rs != 0)
	{
		OutputWarning(_T("Unable to create standard dictionary mapped file."));
		return (-1);
	}

	/*
	 *	Step through the sorted entries, verifying them and passing
	 *	them to the use supplied function.  Verification includes
	 *	that the offsets are continguous within a section, that each
	 *	section starts with offset zero, and that each section ends
	 *	with a special entry with TOKEN_TEXT matching high_TOKEN_TEXT.
	 */

	DICT_ENT* curr_dict = dict_array;
	DICT_ENT* end_dict = dict_array + dict_count - 1;
	for (; curr_dict <= end_dict; curr_dict++) {
		
		/*
		 *	If this is the last entry, it must be a high TOKEN_TEXT entry.
		 */

		if (curr_dict == end_dict) 
		{
#if (defined WIN32 || defined _WIN32)
			if (wcscmp(curr_dict->dict_text, high_TOKEN_TEXT))
#else
			if (!(wcsstr(curr_dict->dict_text, high_TOKEN_TEXT)))
#endif      
			{
				errbuf = (char*)malloc ((unsigned)(strlen(errfmt[5]) + 20));
				if (errbuf == NULL) 
				{
					dicterr(L"memory exhausted");
					return (-1);
				}
				(void)sprintf(errbuf,errfmt[5],curr_dict->wSection);
				wchar_t szWideError[_MAX_PATH+1]={0};
				mbstowcs( szWideError, errbuf, _MAX_PATH );
				dicterr (szWideError);
				free (errbuf);
			}

			/*
			 *	This is a "hidden" entry, so don't pass it to the
			 *	user function.
			 */
			continue;
		}

		/*
		 *	Each entry must be unique.
		 */

		DICT_ENT* next_dict = curr_dict + 1;
		if (curr_dict->dwRef == next_dict->dwRef) 
		{
			errbuf = (char*)malloc ((unsigned)(strlen(errfmt[1]) + 20));
			if (errbuf == NULL) 
			{
				dicterr(L"memory exhausted");
				return (-1);
			}
			(void)sprintf(errbuf,errfmt[1],curr_dict->wSection, 
						curr_dict->wOffset);
			wchar_t szWideError[_MAX_PATH+1]={0};
			mbstowcs( szWideError, errbuf, _MAX_PATH );
			dicterr (szWideError);
			free (errbuf);
			curr_dict++;
			continue;
		}

		/*
		 *	If this entry is the special high TOKEN_TEXT string ...
		 */

#if (defined WIN32 || defined _WIN32)
		if (curr_dict->dict_text && wcscmp(curr_dict->dict_text, high_TOKEN_TEXT) == 0)
#else                                                                                                                                                                                                       
		if (curr_dict->dict_text && (wcsstr(curr_dict->dict_text, high_TOKEN_TEXT) != 0))
#endif            
		{
			//
			//	... then the next section must be different than
			//	this section, and ...
			//
			if (curr_dict->wSection == next_dict->wSection) 
			{
				errbuf = (char*)malloc ((unsigned)(strlen(errfmt[4]) + 20));
				if (errbuf == NULL) 
				{
					dicterr(L"memory exhausted");
					return (-1);
				}
				(void)sprintf(errbuf,errfmt[4],curr_dict->wSection);
				wchar_t szWideError[_MAX_PATH+1]={0};
				mbstowcs( szWideError, errbuf, _MAX_PATH );
				dicterr (szWideError);
				free (errbuf);
			}

			/*
			 *	... the first offset in the next section must
			 *	be zero.
			 */

			else if (next_dict->wOffset != 0) 
			{
				errbuf = (char*)malloc ((unsigned)(strlen(errfmt[3]) + 20));
				if (errbuf == NULL) 
				{
					dicterr(L"memory exhausted");
					return (-1);
				}
				(void)sprintf(errbuf,errfmt[3],next_dict->wSection);
				wchar_t szWideError[_MAX_PATH+1]={0};
				mbstowcs( szWideError, errbuf, _MAX_PATH );
				dicterr (szWideError);
				free (errbuf);
			}

			/*
			 *	This is a "hidden" entry, so don't pass it to the
			 *	user function.
			 */

			continue;
		}
		else {

			/*
			 *	This is a "regular" entry.  It must not be the
			 *	last entry in a section.
			 */

			if (curr_dict->wSection != next_dict->wSection) 
			{
				errbuf = (char*)malloc ((unsigned)(strlen(errfmt[5]) + 20));
				if (errbuf == NULL) 
				{
					dicterr(L"memory exhausted");
					return (-1);
				}
				(void)sprintf(errbuf,errfmt[5],curr_dict->wSection);
				wchar_t szWideError[_MAX_PATH+1]={0};
				mbstowcs( szWideError, errbuf, _MAX_PATH );
				dicterr (szWideError);
				free (errbuf);
			}

			/*
			 *	The offset of the next entry must be one larger
			 *	than this one.
			 */

			else if (curr_dict->wOffset != next_dict->wOffset - 1) 
			{
				errbuf = (char*)malloc ((unsigned)(strlen(errfmt[2]) + 40));
				if (errbuf == NULL) 
				{
					dicterr(L"memory exhausted");
					return (-1);
				}
				(void)sprintf(errbuf,errfmt[2], curr_dict->wOffset, 
							next_dict->wOffset, curr_dict->wSection);
				wchar_t szWideError[_MAX_PATH+1]={0};
				mbstowcs( szWideError, errbuf, _MAX_PATH );
				dicterr (szWideError);
				free (errbuf);
			}

			/*
			 *	Call the install function.
			 */

			dict_table_install(curr_dict->dwRef, curr_dict->dict_text, curr_dict->usLen);
		}
	}
		
	/*
	 * Tidy up.
	 */
	curr_dict = dict_array;
	end_dict = dict_array + dict_count;
	for (; curr_dict < end_dict; curr_dict++)
	{
		free(curr_dict->dict_text);
	}

	free (dict_array);
	dict_array = NULL;

	// Close the writeable view
	rs = dict_FF_table_unmap_view();
	if (rs != 0)
	{
		OutputWarning(_T("Unable to unmap standard dictionary mapped file."));
		return (-1);
	}

	/*
	 * Return TOKEN_NUMBER of TOKEN_ERRORs seen.
	 */
	return dicterrs;
}


/*----------------------------------------------------------------------------*/


/*********************************************************************
 *
 *  Name: dict_compare
 *
 *  ShortDesc:  Compare the reference numbers in the DICT_TABLE_ENTRY
 *				struct passed.  This routine is used by the bsearch
 *				library function called in app_func_get_dict_string upcall.
 *
 *  Inputs:
 *		ptr_a - pointer to void.
 *		ptr_b - pointer to void.
 *
 *  Returns:
 *		< 0, > 0, or = 0 -- comparison result;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
int
dict_compare(SECTIONS* sect_a, SECTIONS* sect_b)
{
	return (sect_a->wSection - sect_b->wSection);
}


/*********************************************************************
*
*  Name: GetMappedFilename()
*
*  ShortDesc:  Get the name of the Memory Mapped File that we use to store the FF Standard dictionary contents.
*
*               The name is generated by appending the Process Id to a known string.
*
*  Returns:
*		a const pointer to the generated Mapped filename
*
*  Author: Mike Dieter
*
**********************************************************************/
CStdString
StdDictTable::GetMappedFilename()
{
    DWORD dwProcessId = PS_GetCurrentProcessId();

    CStdString s_MappedFilename;

    s_MappedFilename.Format(L"%s_%ld", STDFIELDBUSDCT, dwProcessId);

    return s_MappedFilename;
}


/***********************************************************************
 *
 * Name: dict_FF_table_create()
 *
 * ShortDesc: Creates memory-mapped files for standard dictionary.
 *
 * Description:
 *		This routine creates a memory mapped file to hold the standard
 *		dictionary strings.
 *
 *		It's format is as follows:
 *		+-------------------------------------------------------------+
 *		|  MAP_HEADER - contains info about the sizes of each section |
 *		+-------------------------------------------------------------+
 *		|  SECTIONS table - stores an entry for each section in the   |
 *		|                   dictionary.                               |
 *		|                   size is iSectionSize,                     |
 *		|                   actual number used is iSectionCount.      |
 *		+-------------------------------------------------------------+
 *		|  DICT_TABLE_ENTRY table - stores an entry for each string in|
 *		|                   the dictionary.                           |
 *		|                   size is iDictSize                         |
 *		+-------------------------------------------------------------+
 *		|  String Area - this is where the actual string text is      |
 *		|                   stored. All are null terminated. The size |
 *		|                   of this area is iCumStringLen             |
 *		+-------------------------------------------------------------+
 *
 *		The CRuntimeMap object has pointers which are set to point
 *		at these tables in the mapped file. This way each table can
 *		be accessed using standard array notation.
 *
 *		NOTE: Since the memory mapped file is opened by different processes
 *		at different starting addresses, you cannot store pointers in the
 *		file itself; they will be wrong for the other process. Instead
 *		you must use offsets relative to each table.
 *
 * Inputs:
 *		iSectionSize = max number of SECTIONS this will hold
 *		iDictSize = max number of DICT_TABLE_ENTRYs this will hold
 *		iCumStringLen = max size of string area
 *
 * Returns:
 *		0 - success; !0 - failure -> Microsoft defined.
 *
 * Author:
 *		Mike Dieter
 *
 **********************************************************************/
DWORD
StdDictTable::dict_FF_table_create(int iSectionSize, int iDictSize, int iCumStringLen)
{
	// Figure out the size of the big data structure.
	DWORD dwMapSize =	  sizeof(MAP_HEADER)
						+ (sizeof(SECTIONS) * iSectionSize)
						+ (sizeof(DICT_TABLE_ENTRY) * iDictSize) 
						+ iCumStringLen;

	// Create the map file
	ASSERT(hFieldbusMap == NULL);
    hFieldbusMap = (HANDLE)PS_CreateFileMapping(NULL, FALSE, PI_PAGE_READ_WRITE, 0L,
		dwMapSize, GetMappedFilename());

	if (hFieldbusMap == NULL)
	{
        return (PS_GetLastError());
	}

	// Get a writable view of the mapfile
	ASSERT(lpFieldbusView == NULL);
    lpFieldbusView = (MAP_HEADER*)PS_MapViewOfFile ((void *)hFieldbusMap, PI_FILE_MAP_READ_WRITE, 0, 0, 0);

	if (lpFieldbusView == NULL)
	{
        return (PS_GetLastError());
	}

	// Initialize the mapfile with passed in parameters
	lpFieldbusView->dwMapSize = dwMapSize;
	lpFieldbusView->iSectionSize = iSectionSize;
	lpFieldbusView->iSectionCount = 0;
	lpFieldbusView->iDictSize = iDictSize;
	lpFieldbusView->iStringSize = iCumStringLen;


	// Create the RuntimeMap class that points into the mapfile
	ASSERT(pRuntimeMap == NULL);
	pRuntimeMap = new CRuntimeMap(lpFieldbusView);	// Setup the RuntimeMap struct

	pRuntimeMap->Dict[0].dwRef = MAXDWORD;	// Initialize the first one to something bad
												// because the FF standard.dc8 didn't define [0,0]

	return (0);
}


/***********************************************************************
 *
 * Name: dict_FF_table_open()
 *
 * ShortDesc: Opens memory-mapped files for standard dictionaries.
 *
 * Description:
 *		This routine is called by processes other than the server who want
 *		access to the standard dictionary memory-mapped files for Fieldbus.
 *
 * Inputs:
 *
 * Returns:
 *		0 - success; !0 - failure -> Microsoft defined.
 *
 * Author:
 *		Kimberly Wolk
 *
 **********************************************************************/
DWORD
StdDictTable::dict_FF_table_open()
{
	DWORD rs = 0;

//	ASSERT(hFieldbusMap == NULL);
	if (hFieldbusMap) {
		return rs;
	}

    hFieldbusMap = (HANDLE)PS_OpenFileMapping (PI_PAGE_READ_WRITE, FALSE, GetMappedFilename());

	if (hFieldbusMap == NULL)
	{
		rs = PS_GetLastError();
	}

	return rs;
}


/***********************************************************************
 *
 * Name: dict_FF_table_map_read_view
 *
 * ShortDesc: Maps read-only view to the standard dictionary 
 *            memory-mapped files.
 *
 * Description:
 *		Maps read-only view to the standard dictionary 
 *		memory-mapped files for Fieldbus.  
 *		If it fails, GetLastError is returned.
 *
 * Inputs:
 *
 * Returns:
 *		0 - success; !0 - failure -> Microsoft defined.
 *
 * Author:
 *		Kimberly Wolk
 *
 **********************************************************************/
DWORD
StdDictTable::dict_FF_table_map_read_view()
{
//	ASSERT(lpFieldbusView == NULL);
	if (lpFieldbusView) {
		return 0;
	}
    lpFieldbusView = (MAP_HEADER*)PS_MapViewOfFile((void *)hFieldbusMap, PI_FILE_MAP_READ_ONLY, 0, 0, 0);

	if (lpFieldbusView == NULL)
	{
		return (PS_GetLastError());
	}

	ASSERT(pRuntimeMap == NULL);
	pRuntimeMap = new CRuntimeMap(lpFieldbusView);	// Setup the RuntimeMap struct

	return (0);
}


/***********************************************************************
 *
 * Name: dict_FF_table_unmap_view
 *
 * ShortDesc: Unmaps view to the standard dictionary memory-mapped files.
 *
 * Description:
 *		Unmaps views to the standard dictionary memory-mapped files for 
 *		Fieldbus.  If it fails, GetLastError is returned.
 *
 * Inputs:
 *
 * Returns:
 *		0 - success; !0 - failure -> Microsoft defined.
 *
 * Author:
 *		Kimberly Wolk
 *
 **********************************************************************/
DWORD
StdDictTable::dict_FF_table_unmap_view()
{
	DWORD rs = 0;

    if (lpFieldbusView && !PS_UnmapViewOfFile((void *)lpFieldbusView))
	{
        rs = PS_GetLastError();
	}

	lpFieldbusView = NULL;

	delete pRuntimeMap;
	pRuntimeMap = NULL;

	return rs;
}


/***********************************************************************
 *
 * Name: dict_FF_table_close
 *
 * ShortDesc: Closes the standard dictionary memory-mapped files.
 *
 * Description:
 *		Closes the memory-mapped files. The file mapping object will not
 *		close until all open handles to it are closed.
 *
 * Inputs:
 *
 * Returns:
 *		0 - success; !0 - failure -> Microsoft defined.
 *
 * Author:
 *		Kimberly Wolk
 *
 **********************************************************************/
DWORD
StdDictTable::dict_FF_table_close()
{
	unsigned long rs = 0;

    if (PS_CloseHandle((void*)hFieldbusMap))
	{
        rs = PS_GetLastError();
	}

	hFieldbusMap = NULL;

	return (rs);
}



/***********************************************************************
 *
 * Name: dict_table_install()
 *
 * ShortDesc: Put new entry in a standard dictionary table.
 *
 * Description:
 *		This routine is called from makedict(),
 *		and puts new entry into the standard dictionary table.
 *		There are two map files.  One contains the strings from the
 *		standard dictionary.  The other is broken down into four 
 *		areas. The first LONG contains the number of dictionary entries
 *		stored in the map view.  The second LONG contains the number of 
 *		sections found in the standard dictionary file.  The third area 
 *		contains an array of SECTIONS entries which point to the first 
 *		dictionary entry for each section.  The last area contains the 
 *		DICT_TABLE_ENTRYs.
 *
 * Inputs:
 *		ref - standard dictionary reference number.
 *		str - char pointer to the standard dictionary string.
 *
 * Returns:
 *		void
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
void
StdDictTable::dict_table_install(DWORD dwRef, wchar_t *str, unsigned short usLength)
{
	//
	// Check space still available in lpFieldbusView map file
	//
	if (	(lpFieldbusView->iSectionCount	>  lpFieldbusView->iSectionSize)
		||	(install_iNextDictIndex					>= lpFieldbusView->iDictSize)
		||	((install_iStringOffset + usLength + 1)	>= lpFieldbusView->iStringSize)
		)
	{
		OutputWarning(_T("Out of space for standard dictionary strings."));
		return;
	}

	// Break down reference
	WORD wSection = HIWORD(dwRef);
	WORD wOffset  = LOWORD(dwRef);

	// Store SECTIONS info
	if (install_dwCurrentSection != wSection)		// If we are at the start of a new section...
	{
		install_iSectionIndex = (lpFieldbusView->iSectionCount)++;	// Get next section index

		// Set this new section's properties
		pRuntimeMap->Sections[install_iSectionIndex].iDictOffset = install_iNextDictIndex;
		pRuntimeMap->Sections[install_iSectionIndex].wSection = wSection;

		install_dwCurrentSection = wSection;
	}

	// Keep track of the largest offset for this section
	pRuntimeMap->Sections[install_iSectionIndex].wMaxOffset = wOffset;	// This will always be the largest seen so far


	// Store DICT_TABLE_ENTRY info
	// Calculate offset the same way that we will do it when we do a lookup
    int iDictIndex = pRuntimeMap->Sections[install_iSectionIndex].iDictOffset + wOffset;

	pRuntimeMap->Dict[iDictIndex].dwRef = dwRef;
	pRuntimeMap->Dict[iDictIndex].usLen = usLength;
	pRuntimeMap->Dict[iDictIndex].iStringOffset = install_iStringOffset;

	// Set next offset to be one past this one
	install_iNextDictIndex = iDictIndex+1;

	//
	// Store dictionary entries and dictionary strings
	//
	(void)wcscpy (&(pRuntimeMap->StringTable[install_iStringOffset]), str);

	install_iStringOffset += (usLength + 1);
}


///////////////////////////////////////////////////////////////////////
//
//		Directory Extension (DICT_EXT) support
//
//////////////////////////////////////////////////////////////////////




/*********************************************************************
 *
 *	Name: dictExtCompare
 *
 *	ShortDesc: Compare function for qsort and bsearch of standard
 *				dictionary extenstion table.
 *
 *	Description
 *		Compares two DICT_EXT_ENTRY entries, and returns less than zero,
 *		equal zero, or greater than zero depending on whether the
 *		first entry is smaller, equal, or greater than the second.
 *
 *	Inputs:
 *		dict_ent1, dict_ent2 - pointers to the two DICT_EXT_ENTRY entries
 *
 *	Returns:
 *		< 0 - *entry1 < *entry2
 *		  0 - *entry1 == *entry2
 *		> 0 - *entry1 > *entry2
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
int
DICT_EXT_ENTRY::dictExtCompare (DICT_EXT_ENTRY* entry1, DICT_EXT_ENTRY* entry2)
{
	return (int)(entry1->m_dictIndex - entry2->m_dictIndex);
}


/***********************************************************************
 *
 * Name: dictExtFileLoad
 *
 * ShortDesc: Loads the standard dictionary extension file.
 *
 * Description:
 *		Load a file that is passed in and parse it into a table.
 *		This file has extension and / or overrides of standard
 *		dictionary strings.
 *
 * Inputs:
 *		filePath - path to the file to open.
 *
 * Returns:
 *		0 - success; !0 - failure
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
DWORD
StdDictTable::dictExtFileLoad(const CStdString& strDictExtFile)
{
	char*			tmpLine = NULL;

	//
	// Open the file and associate a stream with it
	//
    FILE* pExtFile = PS_wfopen(strDictExtFile, _T("r"));
	if (pExtFile == NULL)
	{
		//
		// File doesn't exist and that's ok,
		// the standard dictionary will be used
		// for all lookups.
		//
		return SUCCESS;
	}

	//
	// Get the number of lines in the file
	//
	const int MAX_LINE_SIZE = 4096;			// Something big enough to hold a single line
	char curLine[MAX_LINE_SIZE] = {0};
	int listSize = 0;						// Count of non-empty lines in file
	int nRet = 0;

	while (feof(pExtFile) == 0 && ferror(pExtFile) == 0)
	{
		// This if statement guards against counting the empty line
		// at the end of the standard.ext file.
		if (fgets(curLine, sizeof(curLine), pExtFile) != NULL)
		{
			listSize++;
		}
		else
		{
			ASSERT(feof(pExtFile) != 0);	// We've reached EOF
			ASSERT(ferror(pExtFile) == 0);	// There are no errors
		}
	}

	rewind(pExtFile);

	//
	// Create a list big enough to store all the values
	//
	if (pDictExtArrayFieldbus == NULL)
	{
		pDictExtArrayFieldbus = new DICT_EXT_TBL;
		pDictExtArrayFieldbus->createList(listSize);
	}

	//
	// Store the values in the table
	//
	char* lastChar = NULL;
	char* str = NULL;
	for (int i = 0; i < listSize; i++)
	{
		//
		// Get the next line in the file
		//
		tmpLine = fgets(curLine, sizeof(curLine), pExtFile);
		ASSERT(tmpLine != NULL);
		
		//
		// Strip off the line in between the
		// first and the last double quotes
		//
		str = strchr(tmpLine, '\"');				// This gets you to the first quote
		str++;										// This gets you to the first char after the quote
		lastChar = strrchr(tmpLine, '\"');			// This gets you to the second quote
		*lastChar = '\0';							// This turns the second quote into the end of the string
		
		//
		// Convert the string
		//
		int nStringLength = strlen(str)+1;
		wchar_t szWideString[MAX_LINE_SIZE+1]={0};		

		PS_MultiByteToWideChar(CP_UTF8, 0, str, -1, szWideString, nStringLength);
		//
		// Store the string
		//
		pDictExtArrayFieldbus->getListEntry(i)->setDictStr(szWideString);

	    //
		// Store the index
		//
		char* strIndex = strtok(tmpLine, " \t\n");
		pDictExtArrayFieldbus->getListEntry(i)->setDictIndex(strtoul(strIndex, NULL, 0));
	}

	nRet = fclose(pExtFile);
	ASSERT(nRet == 0);
	pExtFile = NULL;

	//
	// Sort the table
	//
	(void)qsort((char *)(pDictExtArrayFieldbus->getListEntry(0)), 
				pDictExtArrayFieldbus->getCount(), 
				sizeof (DICT_EXT_ENTRY), 
				(CMP_FN_PTR) DICT_EXT_ENTRY::dictExtCompare);

	//pDictExtArrayFieldbus->dump();

	return SUCCESS;
}
	
	
/***********************************************************************
 *
 * Name: dictExtFFTableCreate
 *
 * ShortDesc: Creates standard dictionary extension table.
 *
 * Description:
 *		Create a table for the standard dictionary extension file.  This
 *		file has definitions of standard dictionary strings that are not
 *		in the standard dictionary itself or are overrides there of.
 *
 * Inputs:
 *		devicePath - path to release directory.
 *
 * Returns:
 *		0 - success; !0 - failure
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
DWORD
StdDictTable::dictExtFFTableCreate(const CStdString& devicePath)
{
	CStdString strDictExtFile;

#if (defined WIN32 || defined _WIN32)
	strDictExtFile = devicePath + _T("\\stdFieldbus.ext");
#else
	strDictExtFile = devicePath + _T("/stdFieldbus.ext");
#endif

	DWORD rc = dictExtFileLoad(strDictExtFile);
	
	return (rc);
}


/***********************************************************************
 *
 * Name: OpenFFStdDictTable
 *
 * ShortDesc: Opens standard dictionary table.
 *
 * Description:
 *		Do all the necessary memory mapped file manipulations to open or create
 *		a FF standard dictionary file.  This code is protected by a semaphore
 *		between three potential processes that will call this function:
 *		FF Server, and Method Interpreter.
 *
 * Inputs:
 *		devicePath - path to release directory.
 *
 * Returns:
 *		0 - success; !0 - failure, WIN32 error code will be returned
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
DWORD
StdDictTable::OpenFFStdDictTable(LPCWSTR pDevicePath)
{
	CStdString strDevicePath(pDevicePath);

	Semaphore* pDictSemaphore = new Semaphore();
	//
	// Create or open the semaphore
	//
	BOOL bResult = pDictSemaphore->InitSemaphore(1, 1, L"AMSFFFhxGenSemaphore");
	
	if(!bResult)
	{
		OutputWarning(_T("Failed to create semaphore for standard dictionary table create."));
		return 1;
	}

	//
	// Treat the following as a critical section
	//
	if (!pDictSemaphore->WaitSemaphore())
	{
		OutputWarning(_T("Failed to lock the making of standard dictionary table."));
		return 1;
	}


	//Only load once to avoid file handle problems.  Class is now global.
	if (m_bLoaded)
	{
		//
		// Leave the critical section
		//
		pDictSemaphore->ReleaseSemaphore(1, NULL);	// Make sure we unlock before deleting the Semaphore below.

		delete pDictSemaphore;
		pDictSemaphore = NULL;

		return 0;
	}

	//
	// Create the tables for the standard dictionary 
	// extension files - standard.ext
	//
	DWORD drs = dictExtFFTableCreate(strDevicePath);
	if (drs != 0)
	{
		return drs;
	}

	//
	// Open the memory mapped file
	// for the standard dictionary
	//
	drs = dict_FF_table_open();

	// If it couldn't be opened, create it
	if (drs != 0)
	{
		CStdString strStdDictPathDar;
		CStdString strStdDictPath;

		// Create the Dictionary Archive filename from strDevicePath
#if (defined WIN32 || defined _WIN32_)
		strStdDictPath = strDevicePath + _T("\\standard.dc8");
		strStdDictPathDar = strDevicePath + _T("\\standard.dar"); // Append .dar
#else
		strStdDictPath = strDevicePath + _T("/standard.dc8");
		strStdDictPathDar = strDevicePath + _T("/standard.dar");
#endif     

		bool bShouldUseArchive = ChooseDictionaryFile(strStdDictPath, strStdDictPathDar);

		if (bShouldUseArchive)	// We want to load the Dictionary Archive file
		{
			// Fill in the standard dictionary table for Fieldbus
			// Begin by attempting to read the binary file
			drs = LoadBinaryDictionary(strStdDictPathDar);

			if (drs == 0)
			{

				m_bLoaded = true;
				//
				// Map a READ-ONLY view to the memory mapped files
				//
				drs = dict_FF_table_map_read_view();
			}
		}


		if ((drs != SUCCESS) || (bShouldUseArchive == false))
		{

			drs = makedict(strStdDictPath);

			if (drs != 0)
			{
				//OutputWarning(_T("Failed to make standard dictionary table.\n"));
				return (1);
			}
			else
			{
				//
				// Map a READ-ONLY view to the memory mapped files
				//
				drs = dict_FF_table_map_read_view();

				// if this fails we just continue.  No errors are needed because the DD will work just fine.
				WriteBinaryDictionary(strStdDictPathDar);

				m_bLoaded = true;
			}
		}
	}

	//
	// Leave the critical section
	//
	pDictSemaphore->ReleaseSemaphore(1, NULL);	// Make sure we unlock before deleting the Semaphore below.

	delete pDictSemaphore;
	pDictSemaphore = NULL;

	return drs;
}


/***********************************************************************
 *
 * Name: CloseFFStdDictTable
 *
 * ShortDesc: Close standard dictionary table.
 *
 * Description:
 *		Do all the necessary memory mapped file manipulations to close
 *		a standard dictionary file.
 *
 * Inputs:
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
void
StdDictTable::CloseFFStdDictTable()
{
	(void)dict_FF_table_unmap_view();
	(void)dict_FF_table_close();

	delete pDictExtArrayFieldbus;
	pDictExtArrayFieldbus = NULL;
}




/*---------------------------------------------------------------------
 ------------------------------ UPCALLS -------------------------------
 ---------------------------------------------------------------------*/

/***********************************************************************
 *
 * Name: dictExtTableSearch
 *
 * ShortDesc: Searches standard dictionary extension table for the key.
 *
 * Inputs:
 *		index - Standard dictionary index to search the table for.
 *		str - String to stuff if we find the index.
 *
 * Returns:
 *		0 - success; !0 - failure
 *
 * Author:
 *		Dave Raskin
 *
 **********************************************************************/
BOOL
StdDictTable::dictExtTableSearch(DDL_UINT index, STRING* str, ENV_INFO *env_info)
{
	BOOL bRet = FALSE;

    // this was static but I had to change it to be instantiated every
    // time to keep it thread safe.
	DICT_EXT_ENTRY key;
	
	// Verify that the standard dictionary extension table exists
	if (pDictExtArrayFieldbus != NULL)
	{
		//
		// Set up the key to search for
		//
		key.setDictIndex(index);

		//
		// Search the table for the passed in index
		//
		DICT_EXT_ENTRY* foundPtr = (DICT_EXT_ENTRY*) bsearch((char *) &key,
											(char *) (pDictExtArrayFieldbus->getListEntry(0)),
											pDictExtArrayFieldbus->getCount(),
											sizeof(DICT_EXT_ENTRY),
											(CMP_FN_PTR) DICT_EXT_ENTRY::dictExtCompare);
		if (foundPtr)
		{
            // Translate to the language specified in the env_info->lang_code
            int buf_len = wcslen(foundPtr->getDictStr());
			wchar_t *buf = new wchar_t[buf_len+1];
            int rc = ddi_get_string_translation( foundPtr->getDictStr(), (wchar_t *)env_info->lang_code, buf, buf_len );

            if (rc == DDS_SUCCESS)
            {
			    str->len = (unsigned short)wcslen(buf);
			    str->str = (wchar_t *) malloc((str->len+1) * 2);
			    wcscpy(str->str, buf);
			    str->str[str->len]=0;
			    str->flags = FREE_STRING; 
			    bRet = TRUE;
            }
            else
            {
                bRet = FALSE;
            }
            delete [] (buf);
    	}
	}

	return bRet;
}


 /*********************************************************************
 *
 *  Name: app_func_get_dict_string
 *
 *  ShortDesc:  Upcall to retreive a standard text dictionary string.
 *
 *  Description:  Returns a standard text dictionary string which has been 
 *				  specified by a standard text dictionary index.
 *
 *  Inputs:
 *      env_info - pointer to the env_info structure
 *		index - reference into the application's standard text dictionary,
 *
 *  Outputs:
 *		str - pointer to a STRING structure to be filled by the application.
 *
 *  Returns:
 *		DDL_DICT_STRING_NOT_FOUND;
 *		DDL_SUCCESS;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
/* ARGSUSED */
int
app_func_get_dict_string(ENV_INFO* env_info, DDL_UINT index, STRING *str)
{
	int rc = DDL_SUCCESS;

   
	//
	// First consult the standard dictionary 
	// extension table
	//
	BOOL found = g_StdDictTable.dictExtTableSearch(index, str, env_info);
	if (found)
	{
		return DDL_SUCCESS;
	}

	WORD wSection = HIWORD(index);
	WORD wOffset  = LOWORD(index);

	SECTIONS key = {0};
	key.wSection = wSection;

	// Use a bsearch to find the right section
	SECTIONS* pSection = (SECTIONS*) bsearch( (char *) &key,
										(char *) (g_StdDictTable.pRuntimeMap->Sections),
										g_StdDictTable.lpFieldbusView->iSectionCount,
										sizeof(SECTIONS),
										(CMP_FN_PTR)dict_compare);

	if (pSection)								// If we found the section
	{
		if (wOffset <= pSection->wMaxOffset)	// and the offset is within bounds
		{										// index into the DICT_TABLE_ENTRY
			DICT_TABLE_ENTRY *pEntry   = &(g_StdDictTable.pRuntimeMap->Dict[pSection->iDictOffset + wOffset]);

			if (pEntry->dwRef == index)			// and if the indexes match
			{
                // Translate to the language specified in the env_info->lang_code
                int buf_len = wcslen(&g_StdDictTable.pRuntimeMap->StringTable[pEntry->iStringOffset]);
			    wchar_t *buf = new wchar_t[buf_len+1];
                rc = ddi_get_string_translation( (wchar_t*)&g_StdDictTable.pRuntimeMap->StringTable[pEntry->iStringOffset], (wchar_t*)env_info->lang_code, buf, buf_len );

                if (rc == DDS_SUCCESS)
                {
				    // Save the string info
				    str->len = (unsigned short)wcslen(buf);
				    str->str = (wchar_t *) malloc((str->len+1) * 2);
				    wcscpy(str->str, buf);
				    str->str[str->len]=0;
				    str->flags = FREE_STRING; 
                }
                delete [] (buf);

			}
			else
			{
				OutputWarning(L"Standard Dictionary lookup: Index didn't match\n");
				rc = DDL_DICT_STRING_NOT_FOUND;
			}
		}
		else
		{
			OutputWarning(L"Standard Dictionary lookup: Offset out of bounds\n");
			rc = DDL_DICT_STRING_NOT_FOUND;
		}
	}
	else
	{
		OutputWarning(L"Standard Dictionary lookup: Section not found\n");
		rc = DDL_DICT_STRING_NOT_FOUND;
	}

	return rc;
}


/******************************************************************************
Name: LoadDict

ShortDesc: Load the fieldbus standard dictionary table.
	
Description:
	Call the function that reads the standard dictionary file and maps it into
	shared memory.  
Inputs:
	None.				
Outputs:
	None.
	
Returns:
	TRUE if successful, FALSE otherwise.

Author:
	Bill Knauff
******************************************************************************/

BOOL StdDictTable::LoadDict(const wchar_t* pReleasePath)
{ 
	DWORD dw = OpenFFStdDictTable( pReleasePath );
	
	if (dw != 0)
	{
		OutputWarning(L"Failed to make standard dictionary table.\n");
	}

	return (dw == 0);
}



/*********************************************************************
 *
 *	Name: LoadBinaryDictionary
 *	ShortDesc: Load a binary version of the dictionary.
 *
 *	Description:
 *		Load a binary version of the dictionary.
 *		This eliminates parsing the text, and speeding up the load time.
 *
 *	Inputs:
 *		binaryDictionaryFile - file name of the input binary dictionary file
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		0 on success
 *		non-0 on failure.
 *
 *	Author:
 *		Mark Sandmann
 *
 *********************************************************************/
int StdDictTable::LoadBinaryDictionary(LPCTSTR binaryDictionaryFile)
{
	int drs = 1;
	CStdString sError;

	// Open the input file.  If it is not found we just return non-zero but it isn't a fatal
	// error.  It just means the file needs to be generated.  We can go to the standard dictionary
	// and take the longer route.

	std::ifstream fileIn;
    char cFile[_MAX_PATH]{0};
    const int size = PS_WideCharToMultiByte(CP_UTF8, 0, binaryDictionaryFile, (int)wcslen(binaryDictionaryFile), cFile, 0, NULL, NULL);

    fileIn.open(cFile, std::ios::in | std::ios::binary);

	if (fileIn.is_open() != true)
	{
		CStdString sErrorInfo;
		sError.Format(_T("Unable to open binary dictionary file for reading: %s moving on"), binaryDictionaryFile, sErrorInfo);
		OutputWarning(sError.c_str());
		return (-1);
	}
	else
	{
		const int BUFFER_INCREMENT = 64 * sizeof(LPTSTR);	// 64 characters = 128 bytes
		unsigned int iBufSize = BUFFER_INCREMENT;			// Start buffer at increment size
		LPTSTR pBuffer = (LPTSTR)malloc(BUFFER_INCREMENT);	// Create initial buffer

		DWORD			 numSections;
		DWORD			 numEntries;
		DWORD			 cumStringLen;

		// First Read in the table parameters.
		fileIn.read((char *)&numSections, sizeof(numSections));
		if(fileIn.gcount() != sizeof(numSections))
		{
			fileIn.close();
			return (-1);
		}
		fileIn.read((char *)&numEntries, sizeof(numEntries));
		if (fileIn.gcount() != sizeof(numEntries))
		{
			fileIn.close();
			return (-1);
		}
		fileIn.read((char *)&cumStringLen, sizeof(cumStringLen));
		if (fileIn.gcount() != sizeof(cumStringLen))
		{
			fileIn.close();
			return (-1);
		}

		// Generate table.
		// Create the mapping
		drs = dict_FF_table_create((int)numSections, (int)numEntries, (int)cumStringLen);
		if (drs != 0)
		{
			OutputWarning(_T("Unable to create standard dictionary mapped file."));
			fileIn.close();
			return (-1);
		}

		if (drs == 0)
		{
			DWORD index = 0;		// Index (section << 32 + offset)
			DWORD iLen = 0;			// Length in characters (2 bytes each)
			DWORD iByteLen = 0;		// Length in bytes.

			for (;;)
			{
				// Read the index
				fileIn.read((char *)&index, sizeof(index));
				if (fileIn.gcount() != sizeof(index))
				{
					break;
				}

				// Read the length of the string
				fileIn.read((char *)&iLen, sizeof(iLen));
				if (fileIn.gcount() != sizeof(iLen))
				{
					break;
				}

                iByteLen = (iLen * sizeof(wchar_t));

				// Adjust buffer if necessary
				if (iByteLen >= iBufSize)
				{	// Allocate the greater of, the amount we currently need or BUFFER_INCREMENT
                    const int lBufsize = (const int)(iByteLen - iBufSize) + sizeof(wchar_t);
                    iBufSize += max (lBufsize, BUFFER_INCREMENT);

					pBuffer = (LPTSTR)realloc(pBuffer, iBufSize);
				}

				// read the string
				fileIn.read((char *)pBuffer, iByteLen);
				if (fileIn.gcount() != iByteLen)
				{
					break;
				}

				pBuffer[iLen] = '\0';	// Null terminate

				// Put the string in the dictionary table
				dict_table_install((DWORD)index, pBuffer, (unsigned short)iLen);
			}
			// We unmap the view from Write so later code can map it read only.
			drs = dict_FF_table_unmap_view();
			if (drs != 0)
			{
				OutputWarning(_T("Unable to create standard dictionary mapped file."));
			}
		}

		// Clean up and set appropriate flags.
		free (pBuffer);
		m_bLoaded = true;
		drs = 0;
		fileIn.close();
	}
	return drs;
}


/*********************************************************************
 *
 *	Name: WriteBinaryDictionary
 *	ShortDesc: Write a binary version of the dictionary for later use.
 *
 *	Description:
 *		Write a binary version of the dictionary.
 *		This is called if we had to read the standard dictionary because there
 *	    was not already a binary version.  We write one out for the next time
 *		the protocol is used.
 *
 *		Failures can be simply be ignored.
 *
 *	Inputs:
 *		binaryDictionaryFile - file name of the output binary dictionary file
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		0 on success
 *		non-0 on failure.
 *
 *	Author:
 *		Mark Sandmann
 *
 *********************************************************************/
int StdDictTable::WriteBinaryDictionary(LPCTSTR binaryDictionaryFile)
{
	int drs = 1;	// default error.
	CStdString sError;


	// Open the input file.  If it is not found we just return non-zero but it isn't a fatal
	// error.  It just takes longer for the app to come up by about 12 seconds.
	std::ofstream OutFile;
    char cFile[_MAX_PATH]{0};
    const int size = PS_WideCharToMultiByte(CP_UTF8, 0, binaryDictionaryFile, (int)wcslen(binaryDictionaryFile), cFile, 0, NULL, NULL);

    OutFile.open(cFile, std::ios::out | std::ios::binary);
	if (OutFile.is_open() != true)
	{
		CStdString sErrorInfo;
		sError.Format(_T("Unable to open binary dictionary file: %s moving on"), binaryDictionaryFile, sErrorInfo);
		OutputWarning(sError.c_str());
	}
	else
	{
		DWORD	 numSections = (long)lpFieldbusView->iSectionCount;
		// iDictSize is not valid except as a record of the size the buffer was originally created as.
		// install_iNextDictIndex remains as the best indicator of number of entries in the dictionary.
		//DWORD	 numEntries = (long)lpFieldbusView->iDictSize;
		DWORD	 numEntries = (long)install_iNextDictIndex;
		DWORD	 cumStringLen = (long)lpFieldbusView->iStringSize;
		DICT_TABLE_ENTRY *dictTable = g_StdDictTable.pRuntimeMap->Dict;
		LPTSTR			 stringTable = g_StdDictTable.pRuntimeMap->StringTable;

		OutFile.write((const char*)&numSections, sizeof(numSections));
		OutFile.write((const char*)&numEntries, sizeof(numEntries));
		OutFile.write((const char*)&cumStringLen, sizeof(cumStringLen));

		DWORD currentIndex = 0;

		// loop through all the dictionary table entries and write them to the file.
		// Sorting and culling has already done so just write what is there.
		// for some reason index 1 does not exist.
		for (currentIndex = 1 ; currentIndex < numEntries; currentIndex++)
		{
			DWORD	value = dictTable[currentIndex].dwRef;
			LPTSTR	text = stringTable + dictTable[currentIndex].iStringOffset;
			DWORD	iLen = (DWORD) dictTable[currentIndex].usLen;
            DWORD	byteLen = iLen * sizeof (wchar_t);
			/*
			 *	Write the value and string to the output file.
			 */
			OutFile.write((const char*)&value, sizeof(value));
			OutFile.write((const char*)&iLen, sizeof(iLen));
			OutFile.write((const char*)text, byteLen);
		}
		/*
		 * Tidy up.
		 */
		OutFile.close();
		drs = 0;

	}
	return drs;
}

/*********************************************************************
 *
 *	Name: ChooseDictionaryFile
 *	ShortDesc: Choose whether to use the Standard dictionary or the archived one.
 *
 *	Description:
 *		Chooses which dictionary to use and calculates the Table Size needed for the memory map
 *
 *	Inputs:
 *		sStdDictName - file name of the dictionary file
 *		sDarDictName - file name of the archived dictionary file
 *
 *
 *	Returns:
 *		true = use the archived file
 *		false = use the standard file
 *
 *	Author:
 *		Mark Sandmann
 *
 *********************************************************************/
bool StdDictTable::ChooseDictionaryFile(const wchar_t* sStdDictName, const wchar_t* sDarDictName)
{
	FileFind finder;

	// Find info for the Std file
	bool bStdExists = false;
	time_t ctStdTime = 0;


	BOOL isFound = finder.FindFile(sStdDictName);
	if (isFound)
	{
		finder.FindFileNext();	// Move the finder forward (required)

		bStdExists = true;
		finder.GetLastWriteTime(ctStdTime);
	}
	finder.CloseFile();

	// Find info for the Dar file
	bool bDarExists = false;
	time_t ctDarTime = 0;
	

	isFound = finder.FindFile(sDarDictName);
	if (isFound)
	{
		finder.FindFileNext();	// Mofe the finder forward (required)

		bDarExists = true;
		finder.GetLastWriteTime(ctDarTime);
	}
	finder.CloseFile();


	// Figure out which file to use
	//  - If the Dar file exists and is newer than the Std, use it.
	//  - Otherwise, use the Std to create the Dar
	// NOTE: if Std doesn't exist, the calling function will error out.

	bool bShouldUseDar = false;

	if (bDarExists)
	{
		if (ctDarTime > ctStdTime)	// Note that ctStdTime is zero if Std does not exist
		{
			bShouldUseDar = true;
		}
		else
		{
			bShouldUseDar = false;	// Recreate the Dar file, since the Std is newer
		}
	}
	else
	{
		bShouldUseDar = false;		// Dar doesn't exist so it must be created.
	}

	return bShouldUseDar;
}
