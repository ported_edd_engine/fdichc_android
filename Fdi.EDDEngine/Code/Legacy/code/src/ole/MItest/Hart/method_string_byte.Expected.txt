﻿<Method>
{
 DD_STRING  local_dd_str;
 char  local_c_str[20];
 int  iLen;
 int  iLoop;

 method_string = literal_string(xxx);
 DISPLAY(literal_string(xxx));
 local_dd_str = method_string;
 DISPLAY(literal_string(xxx));
 local_c_str = local_dd_str;
 DISPLAY(literal_string(xxx));
 local_c_str = method_string;
 DISPLAY(literal_string(xxx));
 iLen = strlen(local_c_str);
 for (iLoop = 0; iLoop < iLen; iLoop++)
 {
  DISPLAY(literal_string(xxx));
 }

}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Initial String
End of Function literal_string
IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 16487, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: ASCII,  Value: Initial String
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The DD string value is %{method_string}.
End of Function literal_string
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 16487, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: ASCII,  Value: Initial String
IBuiltinSupport AcknowledgementRequest() is being called with "The DD string value is Initial String."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The DD string value is %{method_string}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 16487, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: ASCII,  Value: Initial String
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local DD_STRING variable value assigned by globle DD string is %{local_dd_str}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local DD_STRING variable value assigned by globle DD string is Initial String."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local DD_STRING variable value assigned by globle DD string is %{local_dd_str}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable value assigned by local DD_STRING variable is %{local_c_str}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable value assigned by local DD_STRING variable is Initial String."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable value assigned by local DD_STRING variable is %{local_c_str}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 16487, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: ASCII,  Value: Initial String
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable value assigned by globle DD string is %{local_c_str}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable value assigned by globle DD string is Initial String."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable value assigned by globle DD string is %{local_c_str}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: strlen
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SAFEARRAY
    Param 1 Array: Initial String
  Returned Param Type: RUL_INT
  Returned Param Value: 14
End of Function strlen
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 0 is I."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 1 is n."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 2 is i."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 3 is t."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 4 is i."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 5 is a."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 6 is l."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 7 is  ."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 8 is S."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 9 is t."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 10 is r."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 11 is i."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 12 is n."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The local char[] variable byte in index 13 is g."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The local char[] variable byte in index %{iLoop} is %{local_c_str[iLoop]}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function DISPLAY
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><local_dd_str>Initial String</local_dd_str><local_c_str>[0]I,[1]n,[2]i,[3]t,[4]i,[5]a,[6]l,[7] ,[8]S,[9]t,[10]r,[11]i,[12]n,[13]g,[14][15][16][17][18][19]</local_c_str><iLen>14,</iLen><iLoop>14,</iLoop></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
