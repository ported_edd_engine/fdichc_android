////////////////////////////////////////////////////////////////////////
////	Device Type Load/Unload Functions.
////
////	@(#) $Id: dev_type.cpp,v 1.11 1996/06/11 13:16:15 joefish Exp $
////////////////////////////////////////////////////////////////////////

#include "stdinc.h"

#include "dev_type.h"
#include "cm_lib.h"
#include "DDI_LIB.H"
#include "DeviceTypeMgr.h"

#ifdef _DEBUG
#ifdef _MFC_VER
#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////
////
////    Load Symbol Information for Device Type 
////
////////////////////////////////////////////////////////////////////////

int load_sym_info (ENV_INFO *env_info, DEVICE_TYPE_HANDLE dth) 
{
	DD_DEVICE_ID	dd_device_id ;
	int				rc ;
	SYMINFO			*syminfo ;
	DDI_DEVICE_DIR_REQUEST ddi_dir_req;
	FLAT_DEVICE_DIR	*flat_device_dir;

	// get the DD Device ID for loading the symbol file

	rc = g_DeviceTypeMgr.get_adtt_dd_device_id (dth, &dd_device_id);
	if (rc != SUCCESS) {
		return(rc) ;
	}

 	rc = g_DeviceTypeMgr.get_adtt_sym_info(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;
	
	if (syminfo != NULL)
	{
		return SUCCESS ;
	}

	syminfo = new SYMINFO ;

	if (syminfo == NULL) {
		return (CM_NO_MEMORY) ;
	}
	
	//read sym file

	rc = read_sym_file(dd_device_id.dd_path, /*symfile_name*/ /*&dd_device_id,*/ syminfo);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	/*
	 * Save the symbol information
	 */

	rc = g_DeviceTypeMgr.set_adtt_sym_info(dth, syminfo) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}

	rc = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(dth, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}

	memset((char*)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
	ddi_dir_req.type = DD_DT_HANDLE;

	ROD_HANDLE rod_handle = 0;
	rc = g_DeviceTypeMgr.get_adtt_dd_handle (dth, &rod_handle);
	if (rc != CM_SUCCESS)
	{
		return(rc) ;
	}

	ddi_dir_req.spec.ref.rod_handle = rod_handle;

	unsigned long rod_version = g_DeviceTypeMgr.rod_get_version(ddi_dir_req.spec.ref.rod_handle);

	
	if (rod_version >= 0x0500)
	{
		ddi_dir_req.mask = STRING_TBL_MASK | BINARY_TBL_MASK;
	}
	else
	{
		ddi_dir_req.mask = STRING_TBL_MASK;
	}
	

	ddi_dir_req.spec.device_type_handle =  dth;
	rc = ddi_device_dir_request(env_info, &ddi_dir_req, flat_device_dir);
	if (rc != SUCCESS) {
		return (rc);
	}

	return(SUCCESS) ;
}	

////////////////////////////////////////////////////////////////////////
////
////    Remove Symbol Information for Device Type 
////
////////////////////////////////////////////////////////////////////////

int remove_sym_info (ENV_INFO * /* env_info */, DEVICE_TYPE_HANDLE dth) 
{
	SYMINFO	*syminfo ;
	int		rc ;

	rc = g_DeviceTypeMgr.get_adtt_sym_info(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	if (syminfo) {
		free_symtbl(syminfo);
		delete(syminfo);
	}

	rc = g_DeviceTypeMgr.set_adtt_sym_info(dth, (SYMINFO *)NULL) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	return(SUCCESS) ;
}	
	
