/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)int_meth.h	30.3  30  14 Nov 1996
 *
 *	int_meth.h - method interpreter internal builtin interface
 *				internal interface to the method interpreter
 *
 */

#ifndef INT_METH_H
#define INT_METH_H

#include <setjmp.h>
#include "sysparam.h"
#include "rmalloc.h"
#include <AMSServer/bi_codes.h>
#include "env_info.h"

#ifdef __cplusplus
    extern "C" {
#endif

#if 1 
#define INTERP_LINE 
#endif
/**
 *	INTERP_LINE (above), should be enabled when you wish to have line
 *	numbers reported for error or warning statements.  The current
 *	version of the 275 tokenizer pulls out all new-lines, so the line
 *	number is always zero anyway.  This makes this information not
 *	very useful.  So, line numbers have been removed to help compress
 *	the expression trees, symbol tables, and statement lists.
 */

/*
 * Conditionally compile to have actual error line
 * number or zero out the line number when display.
 */ 
#define	GET_SYM_LINE(s)	 s->line

/*
 * Tydefs for recursive references.
 */
typedef struct built_in BUILT_IN;
typedef struct statement STATEMENT;

/*
 * The data type.
 * Use in structure TYPE_SPEC for specifying type of data.
 */
#define	METH_VOID_TYPE			0
#define	METH_CHAR_TYPE			1
#define	METH_SHORT_TYPE			2
#define	METH_LONG_TYPE			3
#define	METH_U_CHAR_TYPE		4
#define METH_U_SHORT_TYPE		5
#define	METH_U_LONG_TYPE		6
#define	METH_FLOAT_TYPE			7
#define	METH_DOUBLE_TYPE		8
#define	METH_ARRAY_TYPE			9
#define	METH_PTR_TYPE			10
#define	METH_BUILT_IN_TYPE		11

typedef struct type_spec TYPE_SPEC;
struct type_spec {
	int 		typ;				/* For specifying data type */
	long 		size;				/* size, if an array */
	TYPE_SPEC 	*madeof;			/* Element type, if an array or */
									/* Built in return value type */
};

typedef union value_p {
	char 			*c_p;			/* Ptr to a character storage */
	unsigned char 	*uc_p;			/* Ptr to an unsigned character storage */
	short 			*s_p;			/* Ptr to a short storage */
	unsigned short 	*us_p;			/* Ptr to a unsigned short storage */
	long 			*l_p;			/* Ptr to a long storage */
	unsigned long 	*ul_p;			/* Ptr to an unsigned long storage */
	float 			*f_p;			/* Ptr to a float storage */
	double 			*d_p;			/* Ptr to a double storage */
	BUILT_IN		*bltin_p;		/* Ptr to a built-in function */
} VALUE_P;

typedef union {
	char 			c;				/* Character value storage */
	short 			s;				/* Ptr to a short storage */
	long 			l;				/* Long value storage */
	unsigned char 	uc;				/* Unsigned Character value storage */
	unsigned short 	us;				/* Ptr to a unsigned short storage */
	unsigned long 	ul;				/* Unsigned Long value storage */
	float 			f;				/* Float value storage */
	double 			d;				/* Double value storage */
	VALUE_P			p;				/* Ptr to one of above storage */
									/* or a built-in function */
} VALUE;

typedef struct sym 	SYM;
struct sym {

	unsigned int	line;
	char 			*name;			/* Identifier ASCII name */
	TYPE_SPEC 		*type;			/* The type of this symbol */
	VALUE_P			v;				/* Ptrs to a symbol value */
	SYM 			*next_sym;		/* Link to the next symbol */
};


struct built_in {
	SYM 	*params;				/* parameter for built-in */
	VALUE 	(*bi) (SYM *, struct mi_environ *);/*			 built-in function */
};


typedef struct express EXPRESS;
struct express {

	unsigned int	line;
	TYPE_SPEC 		*type;			
	int 			op;				/* Operator applies to operands */
	EXPRESS 		*op1;			/* 1st operand of expression */
									/* Func call passing args */
	EXPRESS 		*op2;			/* 2nd operand of expression */
	union {
		EXPRESS	*op3;				/* 3rd operand for ?: operator */
		char 	*name;				/* for SYMREF, FUNCREF */
		VALUE_P	con_v;				/* for CONSTREF */
	} misc;
	EXPRESS 	*next_exp;			/* to link built-in args, and */
									/* array subscripts */
};

/*
 * The statement type.
 * Use in structure STATEMENT for specifying what kind of statement.
 */
#define NULL_STMT	0
#define COMPOUND	1
#define EXPRESSION	2
#define RETURN		3
#define IF			4
#define DO			5
#define WHILE		6
#define FOR			7
#define BREAK		8
#define CONTINUE	9
#define SWITCH		10
#define CASE		11
#define DEFAULT		12

struct statement {

	unsigned int	line;
	int				stmt_type;		/* The statement type or kind */
	STATEMENT 		*stmt;			/* First statement of compound */
	EXPRESS 		*exp1;			/* Expression, include built-in call */
	EXPRESS 		*exp2;
	union {
		EXPRESS 	*exp3;			/* For the FOR statement */
		STATEMENT	*_else;			/* For The IF-ELSE */
		SYM 		*new_vars;		/* Local vars for COMPOUND */
	} misc;
	STATEMENT 		*next_stmt;		/* Link to the next statement */
};

typedef struct {
	STATEMENT	*head;		/* pointer to the head of a statement list */
	STATEMENT	*tail;		/* pointer to the tail of a statement list */
} STMT_PTRS;

/*
 * The scope type.
 * Use in structure SCOPE for specifying scope.
 */
#define SC_OTHER	0
#define SC_FUNC		1

typedef struct scope SCOPE;
struct scope {
	int 	type;					/* The scope type or kind */
	SYM 	*symbols;				/* Symbol list for this scope */
	SCOPE 	*next_scope;			/* Link to the next scope */
};

typedef struct switch_scope SWITCH_SCOPE;
struct switch_scope {
	char 			flag;			/* parse: default seen;  */
									/* exec: currently executing */
	long 			*case_exp;		/* parse: case constants seen; */
									/* exec: switch value  */
	long 			size;			/* size of above array */
	SWITCH_SCOPE	*next_switch;
};

typedef struct mi_environ 	ENVIRON;
struct mi_environ {
	SCOPE 			*global_scope;	/* For built-in function symbols */
	SCOPE 			*cur_scope;		/* For method local var symbols */ 
	SWITCH_SCOPE	*cur_switch;
	VALUE 			return_value;	/* return value and type */
	TYPE_SPEC		*return_type;
	int				in_loop;		/* If non-zero, a loop is being parsed */
	int				in_bltin_ref;	/* If non-zero, built-in params are being parsed */
	RHEAP 			*heap_ptr;		/* Used with rmalloc built-ins */
	ENV_INFO		*env_info;
};

/*
 *	Abort list growth value.  This is how much the abort list
 *	will grow when it is full.
 */

#define METH_ABORT_GROWTH_CT	5

/*
 *	List structure for handling the abort/retry/fail lists,
 *	and the list growth factor.
 */

#define ERRLIST_GROWTH_CT		5
typedef struct {
	int		errlist_count;
	int		errlist_limit;
	UINT32	*errlist_list;
} ERRLIST;

/*
 *	Defines for default action (abort/retry/fail) for
 *	comm errors and response codes.
 */

#define DEFAULT_IS_RETRY		1
#define DEFAULT_IS_ABORT		2
#define DEFAULT_IS_FAIL			3

/*
 *	Structure that holds the per-method information.
 */

typedef struct {
	int			meth_inuse;
	jmp_buf		meth_setjmp;
	uchar		meth_processing_abort;
	uchar		meth_close_option;
	uchar		meth_default_comm_err;
	uchar		meth_default_response_code;
	ERRLIST		meth_abort_comm_errs;
	ERRLIST		meth_retry_comm_errs;
	ERRLIST		meth_fail_comm_errs;
	ERRLIST		meth_abort_response_codes;
	ERRLIST		meth_retry_response_codes;
	ERRLIST		meth_fail_response_codes;
	int			meth_resolve_nest_count;
	int			meth_resolve_rc;
	int			meth_collision_retry;
	int			meth_comm_retry;
	int			meth_abort_list_limit;
	int			meth_abort_list_count;
	ITEM_ID		*meth_abort_list;
	ENVIRON		meth_environ;

} METH_INFO;

/*
 *	Information about all of the methods.
 */

#define VALID_METHOD_TAG(tag) ((tag) > 0 && \
				(tag) <= MAX_METHODS && \
				meth_info[(tag)-1].meth_inuse != 0)

extern METH_INFO meth_info[MAX_METHODS];

/*
 *  Method interface routines.
 */

/* int_stab.c */
extern SYM *lookup P((char *, ENVIRON *));

/* dds_meth.c */
extern long meth_start (ENV_INFO *env_info, ITEM_ID meth_id,
                          int do_init, int close_option);
extern int valid_method_tag (unsigned short method_tag);

/* extern int valid_method_tag (unsigned short method_tag); */

/* domethod.c */
extern long do_method P((ENV_INFO *env_info, ITEM_ID meth_id));

#ifdef __cplusplus
    }
#endif

#endif /* INT_METH_H */
