
#include "stdafx.h"
#include "ProtectedDeviceList.h"
#include<list>

//Forward declaration
class CDeviceList;


// Template declaration required for linkage
std::mutex CProtectedDeviceList::m_mutex;			//protecting m_pInstance
std::unique_ptr<CProtectedDeviceList>  CProtectedDeviceList::m_pInstance;

CProtectedDeviceList :: CProtectedDeviceList()
{
	m_list = new std::list<CDeviceList*>();
}


CProtectedDeviceList::~CProtectedDeviceList()
{
	if(m_list)
	{
		delete m_list;
	}
	
}

CProtectedDeviceList* CProtectedDeviceList::Instance()
{
	m_mutex.lock();
	if (!m_pInstance.get())
	{
		m_pInstance.reset(new CProtectedDeviceList());;
	}
	m_mutex.unlock();
	return m_pInstance.get();

}

std::list<CDeviceList *>* CProtectedDeviceList::LockList()
{
	m_cs.lock();
	return m_list;
}

void CProtectedDeviceList::ReleaseList()
{
	m_cs.unlock();
}
