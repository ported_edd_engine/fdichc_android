#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestRequestPlugin
        {
        public:
            static FLAT_PLUGIN* test_uiplugin_1;

            static void SetupBase(String^ testCaseName)
            {
                EddEngineController^ engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_uiplugin_1_specifier;
                engine->FillParamSpecifier("test_plugin", &test_uiplugin_1_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_uiplugin_1_specifier.id, nsEDDEngine::ITYPE_PLUGIN);
                test_uiplugin_1 = dynamic_cast<FLAT_PLUGIN*>(genericItem->item);            
            }

            static void CleanupBase()
            {
                delete test_uiplugin_1;
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_the_label_string_uip1label_for_uiplugin_1()
            {
                Assert::AreEqual("Test Plugin Label", gcnew String(test_uiplugin_1->label.c_str()));
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_the_help_string_uip1help_for_uiplugin_1()
            {
                Assert::AreEqual("Test Plugin Help", gcnew String(test_uiplugin_1->help.c_str()));
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_for_uiplugin_1()
            {
                Assert::AreEqual(0x1EE6C3CEu, test_uiplugin_1->uuid.Data1);
            }
            
            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_two_for_uiplugin_1()
            {

                Assert::AreEqual(0xF9C1u,(UINT32) test_uiplugin_1->uuid.Data2);
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_three_for_uiplugin_1()
            {
                Assert::AreEqual(0x4EC5u,(UINT32) test_uiplugin_1->uuid.Data3);
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_four_for_uiplugin_1()
            {
                Assert::AreEqual((byte)0x8C, test_uiplugin_1->uuid.Data4[0]);
            }
        };

        // WORKAROUND: use TestInitalize instead ClassInitalize
        // Impediment
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with items 7, but every test use only 1)

        [TestClass]
        public ref class When_request_a_plugin_Element_with_FMA : BaseTestRequestPlugin
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0011.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
