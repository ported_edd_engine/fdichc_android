
// Common.h

#pragma once
#ifdef _WIN32
#include <intsafe.h>

#if defined (EXPORT_INTERFACE)
    #define _INTERFACE __declspec(dllexport)
#else
    #define _INTERFACE __declspec(dllimport)
#endif
#else
    #define _INTERFACE __attribute__((interface))
    #include "PlatformCommon.h"
#endif
#include "nsEDDEngine/Ddldefs.h"
namespace nsEDDEngine
{
    enum EDDEngineFactoryError //EDD_Engine_Errors
	{
		EDDE_SUCCESS				= 0,
		CONFIG_FILE_ERROR			= 1,
		DICTIONARY_FILE_ERROR		= 2,
		LOAD_DD_AND_BLOCK_OPEN_ERROR= 3,
		INVALID_BINARY_FORMAT_ERROR	= 4,
		BAD_FILE_OPEN_ERROR			= 5,
		BAD_HEADER_FILE_ERROR		= 6,
		CREATE_BLOCK_ERROR			= 7
	};

    typedef int BLOCK_INSTANCE;

    const BLOCK_INSTANCE DeviceLevel_BI = 0;	// The BLOCK_INSTANCE number to use for a
												// HART or PROFIBUS device or for an FF
												// device when a device level item is desired

	//
	// These are the possible ways to specify a particular EDDL Item
	//

	enum ItemSpecifierType {
		FDI_ITEM_ID,				// fill-in item.id, leave subindex zero
		FDI_ITEM_ID_SI,				// fill-in item.id and subindex
		FDI_ITEM_PARAM,				// fill-in item.param, leave subindex zero
		FDI_ITEM_PARAM_SI,			// fill-in item.param and subindex
		FDI_ITEM_BLOCK,				// leave subindex and item zero
		FDI_ITEM_CHARACTERISTICS	// fill-in subindex, leave item zero
	};

	typedef struct tag_FDI_ITEM_SPECIFIER {
		ItemSpecifierType	eType;

		union tag_item {
			ITEM_ID	id;
			int		param;
		} item;

		SUBINDEX subindex;

	} FDI_ITEM_SPECIFIER;



	//
	// These are the possible type of the param specifier
	//

	enum ParamSpecifierType {
									// items not filled in should be 0
		FDI_PS_ITEM_ID,				// fill-in id, leave subindex zero
		FDI_PS_ITEM_ID_SI,			// fill-in id and subindex
		FDI_PS_ITEM_ID_COMPLEX,		// fill-in RefList
		FDI_PS_PARAM_OFFSET,		// fill-in param, leave subindex zero
		FDI_PS_PARAM_OFFSET_SI		// fill-in param and subindex
	};

	class _INTERFACE OP_REF_INFO
	{
	public:
		ITEM_ID		id;
		ITEM_ID		member;
		ITEM_TYPE	type;

		OP_REF_INFO();
		
		~OP_REF_INFO();
		
        //Overloaded Assignment Operator 
		OP_REF_INFO& operator= (const OP_REF_INFO &op_ref_info)
        {
            id      = op_ref_info.id;
            member  = op_ref_info.member;
            type    = op_ref_info.type;
            return * this;
        }

	};

	_INTERFACE
	bool operator<(const OP_REF_INFO &_Left, const OP_REF_INFO &_Right);

	_INTERFACE
	bool operator==(const OP_REF_INFO &_Left, const OP_REF_INFO &_Right);

	_INTERFACE
	bool operator!=(const OP_REF_INFO &_Left, const OP_REF_INFO &_Right);


	class _INTERFACE OP_REF_INFO_LIST
	{
	public:
		unsigned short  count;
		OP_REF_INFO     *list;

		OP_REF_INFO_LIST();
		
		//Copy Constructor 
		OP_REF_INFO_LIST(const OP_REF_INFO_LIST &op_ref_info_list);

		//Overloaded Assignment Operator 
		OP_REF_INFO_LIST& operator= (const OP_REF_INFO_LIST &op_ref_info_list);

		~OP_REF_INFO_LIST();

    private:

        void AssignOpRefInfoList( const OP_REF_INFO_LIST &op_ref_info_list );
	};

	class _INTERFACE FDI_PARAM_SPECIFIER 
	{
	public:
		ParamSpecifierType eType;
		ITEM_ID		id;
		int			param;
		SUBINDEX	subindex;

		OP_REF_INFO_LIST	RefList;

		// Constructors
		FDI_PARAM_SPECIFIER();			// Default Constructor
		FDI_PARAM_SPECIFIER(const FDI_PARAM_SPECIFIER &rhs);	// Copy Constructor

		// Destructor
		~FDI_PARAM_SPECIFIER();
	};

	_INTERFACE
	bool operator<(const FDI_PARAM_SPECIFIER &_Left, const FDI_PARAM_SPECIFIER &_Right);


	//
	// Types used for GetCommandList()
	//

	enum CommandType {
		FDI_READ_COMMAND,
		FDI_WRITE_COMMAND
	};

	typedef struct tag_COMMAND_INDEX {
		ITEM_ID		id;
		ulong		value;

	} COMMAND_INDEX;


	class _INTERFACE COMMAND_ELEM {
	public:
		ITEM_ID				command_id;
		ulong				command_number;
		ulong				transaction;
		ushort				weight;
		int					count;
		COMMAND_INDEX		*index_list;

		COMMAND_ELEM()
		{
			command_id = 0;
			command_number = 0;
			transaction = 0;
			weight = 0;
			count = 0;
			index_list = nullptr;
		}

		~COMMAND_ELEM();
	};

	class _INTERFACE FDI_COMMAND_LIST {
	public:
		int				count;
		COMMAND_ELEM	*list;

		FDI_COMMAND_LIST()
		{
			count = 0;
			list = nullptr;
		}

		~FDI_COMMAND_LIST();
		void Dispose();
	};


	//
	// Types used for EDDFileHeader
	//

	typedef struct tag_Device_Description_ID {
			UINT32  manufacturer;
			UINT16	device_type;
			UINT8	device_revision;
			UINT8	dd_revision;
	} Device_Description_ID;

	enum EDD_Profile
	{
		PROFILE_NONE		= 0,
		PROFILE_HART		= 1,
		PROFILE_FF			= 2,
		PROFILE_PB			= 3,
		PROFILE_PN			= 4,
		PROFILE_ISA100		= 5,
		PROFILE_GPE			= 6,
		PROFILE_COMMSERVER	= 7

	};

	enum LayoutType
	{
		COLUMNWIDTH_NONE		= 0,
		COLUMNWIDTH_EQUAL		= 1,
		COLUMNWIDTH_OPTIMIZED	= 2
	};

	typedef struct tag_EDDFileHeader {
		UINT32					magic_number;
		UINT32					header_size;
		UINT32					metadata_size;
		UINT32					item_objects_size;
		Device_Description_ID	device_id;
		UINT8					major_rev;
		UINT8					minor_rev;
		EDD_Profile				edd_profile;
		UINT8					tool_release;
		UINT32					signature;
		UINT32					reserved3;
		UINT32					reserved4;
        wchar_t					filepath[512];
		LayoutType				layout;
        wchar_t					manufacturer_ext[512];
        wchar_t					device_type_ext[512];
	} EDDFileHeader;

}
