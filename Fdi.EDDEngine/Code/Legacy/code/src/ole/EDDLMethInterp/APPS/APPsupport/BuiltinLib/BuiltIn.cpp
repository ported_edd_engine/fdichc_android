#pragma warning (disable : 4786)
#include "stdafx.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "MEE.h"
#include "HartBuiltIn.h"
#include "ProfiBuiltIn.h"
#include "FFBuiltIn.h"
#include "messageUI.h"
#include <ServerProjects/CommonToolkitStrings.h>
#include "NAN_DEF.h"
#include "varientTag.h"


///////////////////////////////////////////////////////////////////////////
//ADDED for AMS Device Manager
CBuiltIn* CBuiltIn::ConstructChild(nsEDDEngine::ProtocolType type)
{
	//TODO: stubbed, finish off
	switch (type)
	{
	case nsEDDEngine::HART:
		return new CHartBuiltIn();
		break;
	case nsEDDEngine::PROFIBUS:
		return new CProfiBuiltIn();
		break;
	case nsEDDEngine::FF:
		return new CFFBuiltIn();
		break;
	default:
		return 0;
		break;
	}
	return 0;
}//End of AMS Device Manager Change

///////////////////////////////////////////////////////////////////////////
const wchar_t* CBuiltIn::DecodeVariantType(VARIANT_TYPE type)
{
	static const wchar_t* szNULL = _T("RUL_NULL");
	static const wchar_t* szCHAR = _T("RUL_CHAR");
	static const wchar_t* szUCHAR = _T("RUL_UNSIGNED_CHAR");
	static const wchar_t* szSHORT = _T("RUL_SHORT");
	static const wchar_t* szUSHORT = _T("RUL_UNSIGNED_SHORT");
	static const wchar_t* szINT  = _T("RUL_INT");
	static const wchar_t* szUINT = _T("RUL_UNSIGNED_INT");
	static const wchar_t* szBOOL = _T("RUL_BOOL");
	static const wchar_t* szFLOAT = _T("RUL_FLOAT");
	static const wchar_t* szDOUBLE = _T("RUL_DOUBLE");
	static const wchar_t* szCHARPTR = _T("RUL_CHARPTR");
	static const wchar_t* szSAFEARRAY = _T("RUL_SAFEARRAY");
	static const wchar_t* szDDSTRING = _T("RUL_DD_STRING");
	static const wchar_t* szLONGLONG = _T("RUL_LONGLONG");
	static const wchar_t* szULONGLONG = _T("RUL_ULONGLONG");
	switch (type)
	{
		case RUL_NULL:
			return szNULL;
			break;
		case RUL_CHAR:
			return szCHAR;
			break;
		case RUL_UNSIGNED_CHAR:
			return szUCHAR;
			break;
		case RUL_SHORT:
			return szSHORT;
			break;
		case RUL_USHORT:
			return szUSHORT;
			break;
		case RUL_INT:
			return szINT;
			break;
		case RUL_UINT:
			return szUINT;
			break;
		case RUL_BOOL:
			return szBOOL;
			break;
		case RUL_FLOAT:
			return szFLOAT;
			break;
		case RUL_DOUBLE:
			return szDOUBLE;
			break;
		case RUL_CHARPTR:
			return szCHARPTR;
			break;
		case RUL_SAFEARRAY:
			return szSAFEARRAY;
			break;
		case RUL_DD_STRING:
			return szDDSTRING;
			break;
		case RUL_LONGLONG:
			return szLONGLONG;
			break;
		case RUL_ULONGLONG:
			return szULONGLONG;
			break;
		default:
			return _T("");
			break;
	}
	return 0;
}

#if 0
void CBuiltIn::FormatDouble(double dValue, char * strReturn)
{
	unsigned long long ullValue = static_cast<long long>(dValue);
	unsigned long long ullExp = (ullValue & EXP_MASK);
	unsigned long long ullFrac = (ullValue & FRAC_MASK);

	if ((dValue >=0) && (ullExp == EXP_MASK) && (ullFrac !=0))
	{
		if (ullValue & QUIET_MASK)
		{
			strcpy(strReturn,"1.#QNAN0");
		}
		else
		{
			strcpy(strReturn,"1.#SNAN0");	
		}
	}
	else
	{
		sprintf(strReturn,"%f", dValue);
	}
}
#endif

///////////////////////////////////////////////////////////////////////////
void CBuiltIn::DebugLogParams(
				char *pchFunctionName
				, int iNumberOfParameters
				, INTER_VARIANT *pVarParameters
				, INTER_VARIANT *pVarReturnValue
				, int	*pBuiltinReturnCode
				)
{
	
	wchar_t *wFuncName = c2w(pchFunctionName);
	
	//Write in new log file
	wchar_t* wStrMessage = new wchar_t[DOUBLE_MAX_DD_STRING];
	PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"Function Name: %s\n", wFuncName);
	m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	
	PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Number Of Input Parameters: %i\n", iNumberOfParameters);
	m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");

	VARIANT_TYPE valType;
	__VAL value;
	wchar_t *wStr = NULL;
	for (int i=0; i<iNumberOfParameters; ++i)
	{
		valType = pVarParameters[i].GetVarType();
		value = pVarParameters[i].GetValue();
				
		PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Type: %s\n", i + 1, DecodeVariantType(valType));
		m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		
		switch(valType)
		{
			case RUL_NULL:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value doesn't exist\n", i + 1);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			  /************numeric types********/
			case RUL_BOOL:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %hd\n", i + 1, value.bValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_CHAR:
				if (strcmp(pchFunctionName, "literal_string") == 0)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %s\n", i + 1, L"xxx");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{	
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %hd\n", i + 1, value.cValue);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_UNSIGNED_CHAR:
				if (strcmp(pchFunctionName, "literal_string") == 0)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %s\n", i + 1, L"xxx");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{					
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %hhu\n", i + 1, value.ucValue);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_INT:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %d\n", i + 1, value.nValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_UINT:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %u\n", i + 1, value.unValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_SHORT:
				if (strcmp(pchFunctionName, "literal_string") == 0)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %s\n", i + 1, L"xxx");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %hd\n", i + 1, value.sValue);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_USHORT: /* 16 bit char */
				if (strcmp(pchFunctionName, "literal_string") == 0)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %s\n", i + 1, L"xxx");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %hu\n", i + 1, value.usValue);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_LONGLONG:
				if (strcmp(pchFunctionName, "literal_string") == 0)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %s\n", i + 1, L"xxx");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %lld\n", i + 1, value.lValue);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_ULONGLONG:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %llu\n", i + 1, value.ulValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_FLOAT:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %f\n", i + 1, value.fValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_DOUBLE:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %f\n", i + 1, value.dValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			  /**************** string types ****/
			case RUL_CHARPTR:
				{
					if (value.pzcVal == NULL)
					{
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: (null)\n", i + 1);
						m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					}
					else
					{
						wStr = c2w(value.pzcVal);
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"    Param %i Value: %.1900s\n", i + 1, wStr);
						m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						if (wStr != NULL)
						{
							delete [] wStr;
							wStr = NULL;
						}
					}
				}
				break;
			case RUL_WIDECHARPTR:/* aka _USHORTPTR */	
				PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"    Param %i Value: %.1900s\n", i + 1, value.pszValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_BYTE_STRING:
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: %.2hx", i + 1, value.bString.bs[0]);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					wstring wsMessage = wstring(L"");
					for (int j = 0; j < (int)value.bString.bsLen; j++)
					{
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%.2hx", value.bString.bs[j]);
						wsMessage.append(wStrMessage);
						
					}
					//PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"\n");
					m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				break;
			case RUL_DD_STRING:				
				PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"    Param %i Value: %.1900s\n", i + 1, value.pszValue);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			case RUL_SAFEARRAY:
				{
					_INT32 sarrayType = (value.prgsa)->Type();
					switch(sarrayType)
					{
					case RUL_CHAR:					
					case RUL_UNSIGNED_CHAR:
						{
							wchar_t spchString[MAX_DD_STRING]={0};
							int pSize = MAX_DD_STRING;

							GetWCharArray(pVarParameters[i],spchString,pSize);

							if (sarrayType == RUL_CHAR)
							{
								pSize = wcslen(spchString);
								if (pSize == 0)
								{
									pSize = 3;	//because 3 is commonly used for command builtins
								}
							}


							wstring wsMessage = wstring(L"");
							//print byte by byte
							for (int j=0; j < pSize; j++)
							{
								if (sarrayType == RUL_CHAR)
								{
									if (j == 0)    // If this is the first time, output the param label first
									{
										PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Array: ", i + 1);

										wsMessage.append(wStrMessage);
									}

									if (spchString[j] == '\0')
									{
										PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%c", '\x7F');
									}
									else
									{
										PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%c", spchString[j]);
									}
									wsMessage.append(wStrMessage);
								}
								else
								{
									if (j == 0)
									{
										PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Array: %.2hx", i + 1, spchString[0]);
										wsMessage.append(wStrMessage);
									}
									else
									{
										PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%.2hx", spchString[j]);
										wsMessage.append(wStrMessage);
									}
								}
							}
							m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
							m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
							break;
						}
					case RUL_USHORT:
					{
						//This maybe a local method variable in DD_STRING type
						wchar_t spchString[MAX_DD_STRING] = { 0 };
						int pSize = MAX_DD_STRING;

						GetWCharArray(pVarParameters[i], spchString, pSize);

						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"    Param %i Array: %.1900s\n", i + 1, spchString);
						m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					}
					case RUL_INT:
						{
							long pLongItemIds[100]={0};
							int iNumberOfItemIds = 0;
							int j = 0;
							wstring wsMessage = wstring(L"");
							GetLongArray(pVarParameters[i],pLongItemIds,iNumberOfItemIds);
							for (j=0; j < iNumberOfItemIds; j++)
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Array[%i]: %d\n", i + 1, j, pLongItemIds[j]);
								wsMessage.append(wStrMessage);
							}
							m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
							break;
						}
					case RUL_UINT:
						{
							unsigned long pULongItemIds[100]={0};
							int iNumberOfItemIds = 0;
							int j = 0;
							wstring wsMessage = wstring(L"");
							GetULongArray(pVarParameters[i],pULongItemIds,iNumberOfItemIds);
							for (j=0; j < iNumberOfItemIds; j++)
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Array[%i]: %u\n", i + 1, j, pULongItemIds[j]);
								wsMessage.append(wStrMessage);
							}
							m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
						}
					default:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Array: ?\n", i + 1);
						m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					}
				}
				break;
			default:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"    Param %i Value: unknown\n", i + 1);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
		}
	}

	valType = pVarReturnValue->GetVarType();
	value = pVarReturnValue->GetValue();
	PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Type: %s\n", DecodeVariantType(valType));
	m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	switch(valType)
	{
		case RUL_NULL:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value doesn't exist\n");
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
			/************numeric types********/
		case RUL_BOOL:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %hd\n", value.bValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_CHAR:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %c\n", value.cValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_UNSIGNED_CHAR:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %hhu\n", value.ucValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_INT:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %d\n", value.nValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_UINT:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %u\n", value.unValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_SHORT:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %hd\n", value.sValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_USHORT: /* 16 bit char */
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %hu\n", value.usValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_LONGLONG:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %lld\n", value.lValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_ULONGLONG:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %llu\n", value.ulValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_FLOAT:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %f\n", value.fValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_DOUBLE:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %f\n", value.dValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
			/**************** string types ****/
		case RUL_CHARPTR:
			{
				if (value.pzcVal == NULL)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: (null)\n");
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
				else
				{
					wStr = c2w(value.pzcVal);
					PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"  Returned Param Value: %.1900s\n", wStr);
					m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					if (wStr != NULL)
					{
						delete [] wStr;
						wStr = NULL;
					}
				}
			}
			break;
		case RUL_WIDECHARPTR:/* aka _USHORTPTR */
			PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"  Returned Param Value: %.1900s\n", value.pszValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_BYTE_STRING:
			{
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: %c", value.bString.bs[0]);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				wstring wsMessage = wstring(L"");
				for (int i = 0; i < (int)value.bString.bsLen; i++)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%c", value.bString.bs[i]);
					wsMessage.append(wStrMessage);
				}
				m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				//PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"\n");
				m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			}
			break;
		case RUL_DD_STRING: 
			PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"  Returned Param Value: %.1900s\n", value.pszValue);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
		case RUL_SAFEARRAY:
			switch((value.prgsa)->Type())
			{
			case RUL_CHAR:			
			case RUL_UNSIGNED_CHAR:
				{
					wchar_t pchString[MAX_DD_STRING]={0};
					int pSize = MAX_DD_STRING;
					_INT32 sarrayType = (value.prgsa)->Type();

					GetWCharArray(*pVarReturnValue, pchString, pSize);					

					//if the parameter is a sting, don't print the NULL character
					if ( (sarrayType == RUL_CHAR) && (pchString[pSize - 1] == _T('\0')) )
					{
						pSize--;
					}
					wstring wsMessage = wstring(L"");
					//print byte by byte
					for (int j=0; j < pSize; j++)
					{
						if (sarrayType == RUL_CHAR)
						{
							if (j == 0)
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Array: %c", pchString[0]);
								//m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
								wsMessage.append(wStrMessage);
							}
							else
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%c", pchString[j]);
								//m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
								wsMessage.append(wStrMessage);
							}
						}
						else
						{
							if (j == 0)
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Array: %.2hx", pchString[0]);
								//m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
								wsMessage.append(wStrMessage);
							}
							else
							{
								PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"%.2hx", pchString[j]);
								//m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
								wsMessage.append(wStrMessage);
							}
						}
					}
					//PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"\n");
					m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");

					break;
				}
			case RUL_USHORT:
			{
				//This maybe a local method variable in DD_STRING type
				wchar_t spchString[DOUBLE_MAX_DD_STRING] = { 0 };
				int pSize = DOUBLE_MAX_DD_STRING;

				GetWCharArray(*pVarReturnValue, spchString, pSize);

				PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"  Returned Param Array: %.1900s\n", spchString);
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			}
			case RUL_INT:
				{
				long pLongItemIds[100]={0};
				int iNumberOfItemIds = 0;
				int j = 0;

				GetLongArray(*pVarReturnValue,pLongItemIds,iNumberOfItemIds);
				wstring wsMessage = wstring(L"");
				for (j=0; j < iNumberOfItemIds; j++)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Array[%i]: %d\n", j, pLongItemIds[j]);
					wsMessage.append(wStrMessage);
				}
				m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
				}
			case RUL_UINT:
				{
				unsigned long pLongItemIds[100]={0};
				int iNumberOfItemIds = 0;
				int j = 0;

				GetULongArray(*pVarReturnValue,pLongItemIds,iNumberOfItemIds);
				wstring wsMessage = wstring(L"");
				for (j=0; j < iNumberOfItemIds; j++)
				{
					PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Array[%i]: %u\n", j, pLongItemIds[j]);
					wsMessage.append(wStrMessage);
				}
				m_pDevice->MILog((wchar_t*)wsMessage.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
				}
			default:
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Array: ?\n");
				m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				break;
			}
			break;
		default:
			PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"  Returned Param Value: unknown\n");
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			break;
	}
	PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"End of Function %s\n", wFuncName);
	m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	if (wFuncName != NULL)
	{
		delete [] wFuncName;
		wFuncName = NULL;
	}

	if (wStrMessage != NULL)
	{
		delete[] wStrMessage;
		wStrMessage = NULL;
	}

}

void CBuiltIn::PreDebugLogParams(
			char *pchFunctionName
			, int iNumberOfParameters
			, INTER_VARIANT *pVarParameters
			, INTER_VARIANT *pVarReturnValue
			, int	*pBuiltinReturnCode
			)
{
	LOGIT(COUT_LOG,"Called Builtin: %s()\n", pchFunctionName );
	for( int nIndex = 0; nIndex<iNumberOfParameters; nIndex++ )
	{
		char szTempString[MAX_DD_STRING]={0};
		switch( pVarParameters[nIndex].GetVarType() )
		{
			case RUL_NULL:
				break;
			case RUL_CHAR :
				sprintf( szTempString, "Arg%d: %c\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_BOOL:
			case RUL_UNSIGNED_CHAR:
				sprintf( szTempString, "Arg%d: %hhu\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_SHORT:
				sprintf( szTempString, "Arg%d: %hd\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_USHORT:
				sprintf( szTempString, "Arg%d: %hu\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_INT:
				sprintf( szTempString, "Arg%d: %d\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_UINT:
				sprintf( szTempString, "Arg%d: %u\n", nIndex, (int)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_LONGLONG:
				sprintf( szTempString, "Arg%d: %lld\n", nIndex, (long long)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_ULONGLONG:
				sprintf( szTempString, "Arg%d: %llu\n", nIndex, (unsigned long long)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_FLOAT:
			case RUL_DOUBLE:
				sprintf( szTempString, "Arg%d: %f\n", nIndex, (float)pVarParameters[nIndex] );
				LOGIT(COUT_LOG,szTempString );
				break;
			default:
				wchar_t *szLocalString = NULL;	//using wchar_t* instead of char_t* to avoid calling TStr2AStr()
				pVarParameters[nIndex].GetStringValue(&szLocalString);
				size_t size_src = wcslen(szLocalString)+15;

				char *szLogStr = new char[size_src];
				if (szLocalString != NULL)
				{
                    snprintf( szLogStr, size_src, "Arg%d: %ls\n", nIndex, szLocalString );

					delete szLocalString;
					szLocalString = NULL;
				}
				else
				{
					sprintf( szLogStr, "Arg%d: unknown due to its array or output type.\n", nIndex );
				}
				LOGIT(COUT_LOG,szLogStr );
				delete szLogStr;
				break;
		}
	}
}

void CBuiltIn::PostDebugLogParams(
			char *pchFunctionName
			, int iNumberOfParameters
			, INTER_VARIANT *pVarParameters
			, INTER_VARIANT *pVarReturnValue
			, int	*pBuiltinReturnCode
			)
{
	LOGIT(COUT_LOG,"Builtin Returned: %d\n", *pBuiltinReturnCode );
	if( *pBuiltinReturnCode )
	{
		char szTempString[MAX_DD_STRING]={0};
		switch( pVarReturnValue->GetVarType() )
		{
			case RUL_NULL:
				break;
			case RUL_CHAR :
				sprintf( szTempString, "ReturnValue: %c\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_BOOL:
			case RUL_UNSIGNED_CHAR:
				sprintf( szTempString, "ReturnValue: %hhu\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_SHORT:
				sprintf( szTempString, "ReturnValue: %hd\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_USHORT:
				sprintf( szTempString, "ReturnValue: %hu\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_INT:
				sprintf( szTempString, "ReturnValue: %d\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_UINT:
				sprintf( szTempString, "ReturnValue: %u\n", (int)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_LONGLONG:
				sprintf( szTempString, "ReturnValue: %lld\n", (long long)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_ULONGLONG:
				sprintf( szTempString, "ReturnValue: %llu\n", (unsigned long long)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			case RUL_FLOAT:
			case RUL_DOUBLE:
				sprintf( szTempString, "ReturnValue: %f\n", (float)*pVarReturnValue );
				LOGIT(COUT_LOG,szTempString );
				break;
			default:
				wchar_t *szLocalString = NULL;	//using wchar_t* instead of char_t* to avoid calling TStr2AStr()
				pVarReturnValue->GetStringValue(&szLocalString);
				if (szLocalString != NULL)
				{
					size_t size_src = wcslen(szLocalString)+15;
					size_t size_dst = sizeof(szTempString);
                    snprintf( szTempString, size_dst, "ReturnValue: %ls\n", szLocalString );

					delete szLocalString;
					szLocalString = NULL;
				}
				else
				{
					sprintf( szTempString, "ReturnValue: unknown due to its array or output type.\n");
				}
				LOGIT(COUT_LOG,szTempString );
				break;
		}
	}
}

/*************** <START> Methods UI Built-ins	**************************************/

int CBuiltIn::_fvar_value
		(
			float& fretVal,
			long lItemId
		)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(0, 0, lItemId, 0, &vtValue);
	if (iRetVal == BLTIN_SUCCESS)
	{
		fretVal = static_cast<float>(CV_R4(&vtValue));
	}

	return iRetVal;
}

int CBuiltIn::_ivar_value
		(
			INT64& iretVal,
			long lItemId
		)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(0, 0, lItemId, 0, &vtValue);
	if (iRetVal == BLTIN_SUCCESS)
	{
		iretVal = CV_I8(&vtValue);
	}
		
	return(iRetVal);
}
	
int CBuiltIn::_get_status_code_string
		(
			long lItemId
			, int iStatusCode
			, tchar *pchStatusString
			, int iStatusStringLength  // truncates at this length
		)
{
	int iRet = BLTIN_VAR_NOT_FOUND;
	wstring status_str;
	hCitemBase* pIB = NULL;

	if (m_pDevice->getItemBySymNumber(lItemId, &pIB) == BLTIN_SUCCESS && pIB != NULL)
	{
		hCVar *pVar=(hCVar *)pIB;

		switch(pVar->VariableType())
		{
			case vT_Enumerated:
			{
				hCEnum *pEnum=(hCEnum *)pVar;
				iRet = pEnum->procureString(iStatusCode, status_str); // input value, get a string							
				break;
			}
			case vT_BitEnumerated:
			{
				hCBitEnum *pBitEnum=(hCBitEnum *)pVar;
				iRet = pBitEnum->procureString(iStatusCode, status_str); // input value, get a string
				break;					
			}
			default:
				iRet = BLTIN_WRONG_DATA_TYPE;
				break;
		}
		
		if(!status_str.empty())
		{
			_tstrncpy(pchStatusString,status_str.c_str(),(iStatusStringLength-1));
			pchStatusString[iStatusStringLength-1] = '\0';
		}
		else // stevev - 30may07 -- EVERY 'if' must have an 'else' - even if it's just a comment!!!
		{
			pchStatusString[0] = '\0';
		}
	}

	return (iRet);
}


int CBuiltIn::_get_dictionary_string
		(
			long lItemId
			, tchar *pchDictionaryString
			, int iMaxStringLength
		)
{
	wchar_t *str = new wchar_t[iMaxStringLength];
	wchar_t *newstr = new wchar_t[iMaxStringLength];
	int rc = m_pDevice->dictionary->get_dictionary_string(lItemId, newstr, iMaxStringLength);

	int strLen = (int)wcslen(newstr);
	if((strLen > 0) && (iMaxStringLength > 0) && (rc == BLTIN_SUCCESS))
	{
		rc = (m_pDevice->dictionary)->get_string_translation(newstr, str, iMaxStringLength, m_pDevice->device_sLanguageCode);
		wcsncpy(pchDictionaryString, str, (strLen));
		if (strLen <= iMaxStringLength)
		{
			pchDictionaryString[strLen] = _T('\0');
		}
		else
		{
			pchDictionaryString[iMaxStringLength] = _T('\0');
		}
	}
	else
	{
		pchDictionaryString[0] = _T('\0');
	}
	delete[] newstr;
	delete[] str;

	return (rc);
}


// stevev 29jan08
int CBuiltIn::literal_string
		(
			long lItemId
			, tchar **pchLiteralString			
		)
{
	wchar_t str[DOUBLE_MAX_DD_STRING] = _T("");
	int rc = m_pDevice->dictionary->get_lit_string(lItemId, str, DOUBLE_MAX_DD_STRING);
	
	size_t strLen = wcslen(str);
	*pchLiteralString = new tchar[strLen + 1];
	PS_Wcscpy(*pchLiteralString, strLen + 1, str);

	if ( rc != DDL_SUCCESS )
		return (BI_ERROR);
	else
		return (BI_SUCCESS);
}

/*Arun 190505 Start of code*/

int CBuiltIn::get_enum_string
		(
			long lItemId
			, int iStatusCode
			, tchar *pchStatusString
			, int iStatusStringLength
		)
{
	int iRet = BLTIN_VAR_NOT_FOUND;
	wstring status_str;
	hCitemBase* pIB = NULL;

	if (m_pDevice->getItemBySymNumber(lItemId, &pIB) == BLTIN_SUCCESS && pIB != NULL)
	{
		hCVar *pVar=(hCVar *)pIB;

		switch(pVar->VariableType())
		{
			case vT_Enumerated:
			{
				hCEnum *pEnum=(hCEnum *)pVar;
				iRet = pEnum->procureString(iStatusCode, status_str); // input value, get a string							
				break;
			}
			case vT_BitEnumerated:
			{
				hCBitEnum *pBitEnum=(hCBitEnum *)pVar;
				iRet = pBitEnum->procureString(iStatusCode, status_str); // input value, get a string
				break;					
			}
			default:
				iRet = BLTIN_WRONG_DATA_TYPE;
				break;
		}
		
		if(!status_str.empty())
		{
			PS_Wcsncpy(pchStatusString, iStatusStringLength, status_str.c_str(),_TRUNCATE);
		}
		else
		{
			pchStatusString[0] = '\0';
		}
	}
	return iRet;

/*End of code*/
}

int CBuiltIn::_iassign( long item_id, INT64 new_value )
{
	CValueVarient vtValue{}; 
	CV_VT(&vtValue, ValueTagType::CVT_I8);
	CV_I8(&vtValue) = new_value;
	long lRetVal = put_variable_value(0, 0, item_id, 0, &vtValue);

	if ((lRetVal != BI_SUCCESS) && (lRetVal != BI_ABORT))
	{
		lRetVal = BI_ERROR;
	}
	return (lRetVal);	
}

int CBuiltIn::_lassign( long item_id, INT64 new_value )
{
	return (_iassign(item_id, new_value));	
}


int CBuiltIn::_fassign(long item_id,float new_value)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_R4);
	CV_R4(&vtValue) = new_value;

	long lRetVal = put_variable_value(0, 0, item_id, 0, &vtValue);

	if ((lRetVal != BI_SUCCESS) && (lRetVal != BI_ABORT))
	{
		lRetVal = BI_ERROR;
	}
	return (lRetVal);	
}

int CBuiltIn::_dassign(long item_id,double new_value)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_R8); 
	CV_R8(&vtValue) = new_value;
	long lRetVal = put_variable_value(0, 0, item_id, 0, &vtValue);

	if ((lRetVal != BI_SUCCESS) && (lRetVal != BI_ABORT))
	{
		lRetVal = BI_ERROR;
	}
	return (lRetVal);	
}

//This function is used for HART and Profibus
int CBuiltIn::_vassign(long item_id_dest,long item_id_source)
{
	int iRetVal = assign(0, 0, item_id_dest, 0, 0, 0, item_id_source, 0);
	
	switch (iRetVal)
	{
	case BLTIN_SUCCESS:
		iRetVal = BI_SUCCESS;
		break;
	case BLTIN_DDS_ERROR:
	case BI_ABORT:
		iRetVal = BI_ABORT;
		break;
	default:
		iRetVal = BI_ERROR;
		break;
	}

	return (iRetVal);	
}	


int CBuiltIn::rspcode_string
		(
			int iCommandNumber
			, int iResponseCode
			, tchar *pchResponseCodeString
			, int iResponseCodeStringLength
			, nsEDDEngine::ProtocolType protocol
		)
{
	int iRet = BI_COMM_ERR;

	if( m_pDevice )
	{
		iRet = m_pDevice->get_rspcode_string( iCommandNumber,
									   iResponseCode,
			                           pchResponseCodeString,
									   iResponseCodeStringLength,
									   protocol);
		
		if(iRet != BI_SUCCESS)
		{
			iRet = BI_ERROR; 
		}
	}

	return(iRet); 
}

int CBuiltIn::display_response_status
		(
			long lCommandNumber
			, int  iStatusValue
		)
{
	int		retVal = BI_ERROR;


	responseCodeList_t respCodeList;

	hCrespCode	rspCode(m_pDevice->devHndl());

	hCRespCodeList	*tmprespCodeList = (hCRespCodeList *) &respCodeList;	// treat as a vector of <hCrespCode>

	CCmdList *ptrCmndList = (CCmdList *)m_pDevice->getListPtr(iT_Command);
	if (ptrCmndList)
	{
		wchar_t* pRespStatusStr;
	
		hCcommand *pCommand = ptrCmndList->getCmdByNumber(lCommandNumber);
		if (pCommand)
		{
			wstring str_desc;
			bool	bRespFound = false;

			pCommand->getRespCodes(*tmprespCodeList);
			for (hCRespCodeList::iterator iT = respCodeList.begin(); iT != respCodeList.end(); iT++)
			{
				rspCode=(*iT);
				
				if( rspCode.getVal() == (unsigned long)iStatusValue)
				{
					str_desc=rspCode.getDescStr(); 
					
					size_t str_len = str_desc.length(); 
					pRespStatusStr = new tchar[str_len + 1];
					PS_Wcscpy(pRespStatusStr, str_len + 1, str_desc.c_str() );
					bRespFound = true;
					break;
				}
			}

			//check if the response string is found
			if (!bRespFound)
			{
				pRespStatusStr = new tchar[32];
				PS_VsnwPrintf(pRespStatusStr, 32, L"0x%02X Undefined", iStatusValue);
			}
		}
		else//Added by WaltSigermans for AMS Device Manager
		{
			pRespStatusStr = new tchar[MAX_DD_STRING];
			pRespStatusStr[0] = L'\0';
			if (SUCCESS != m_pDevice->get_rspcode_string( lCommandNumber,
									   iStatusValue,
			                           pRespStatusStr,
									   1000, nsEDDEngine::HART))
			{
				PS_VsnwPrintf(pRespStatusStr, MAX_DD_STRING, L"0x%02X Undefined", iStatusValue);	
			}
		}//end of AMS Device Manager Change

		retVal = ACKNOWLEDGE(pRespStatusStr);

		if(pRespStatusStr)
		{
			delete[] pRespStatusStr;
		}
	}

	return (retVal);
}


int CBuiltIn::sassign(long lItemId, char* new_value)
{
	CValueVarient vtValue{};
	CV_VT (&vtValue, ValueTagType::CVT_STR);
	CV_STR(&vtValue) = new_value;
	long lRetVal = put_variable_value(0, 0, lItemId, 0, &vtValue);
	int iRet = ConvertToBICode(lRetVal);
	return (iRet);	
}

/*Vibhor 200905: Start of Code*/


long CBuiltIn::ListInsert2(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId)
{
	hCitemBase *pIB = NULL;
	int iRetCode = BLTIN_WRONG_DATA_TYPE;
	
	if((0 >= ulListId) || (-1 >= iListIndex) || (0 >= ulItemId))
	{
		return BLTIN_NOT_IMPLEMENTED;
	}

	/*Check if List exists & is Valid*/
	if((BLTIN_SUCCESS == m_pDevice->getItemBySymNumber(ulListId, &pIB))
		&& (NULL != pIB)
		&& (pIB->IsValidTest())
		)
	{
		itemType_e itemType = pIB->getIType();

		if (itemType == iT_List)
		{
			iRetCode = m_pDevice->sendListOpMethod(ulListId, iListIndex, ulEmbListId, iIndex, ulItemId, 0);	//non-zero == insertion
		}
	}

	ProtocolType iProtocol = m_pMeth->GetStartedProtocol();

	if ((iProtocol == nsEDDEngine::HART) || (iProtocol == nsEDDEngine::PROFIBUS))
	{
		if(iRetCode != BI_SUCCESS)
		{
			iRetCode = BI_ERROR;
		}
	}
	else	//FF
	{
		iRetCode = ConvertToBLTINCode(iRetCode);
	}
	return (iRetCode);
}/*End _ListInsert()*/


double CBuiltIn::get_double_lelem(ITEM_ID list_id, unsigned long index, ITEM_ID element_id, ITEM_ID subelement_id)
{
	return get_double_lelem2(list_id, index, 0, 0, element_id, subelement_id);
}

float CBuiltIn::get_float_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id)
{
	return get_float_lelem2(list_id, index, 0, 0, element_id, subelement_id);
}

long CBuiltIn::get_string_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id, wchar_t* strval, int* length)
{
	return get_string_lelem2(list_id, index, 0, 0, element_id, subelement_id, strval, length);
}


long CBuiltIn::get_signed_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id)
{
	return get_signed_lelem2(list_id, index, 0, 0, element_id, subelement_id);
}

unsigned long CBuiltIn::get_unsigned_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id)
{
	return get_unsigned_lelem2(list_id, index, 0, 0, element_id, subelement_id);
}


//
// convertUllTo8Char() - Converts a ulongling to an FF "date"-type byte buffer representation (which is used in Builtins).
//		See IEC 61804-3 for date-type memory layout.
//
void CBuiltIn::convertUllTo8Char(unsigned long long const ullSrcVal, unsigned char *pDestChar, long length)
{
	//valid input data length is no longer than 8
	if (length > 8)
	{
		length = 8;
	}

	int maxShift = length - 1;

	for (int i = 0; i < length; i++)	// The most significant byte is the first one...
	{
		pDestChar[i] = (unsigned char) (ullSrcVal >> (8 * (maxShift - i) ));
	}
}

//
// convert8CharToUll() - Converts an FF "date"-type from its byte buffer representation (which is used in Builtins), to an ulonglong.
//		See IEC 61804-3 for date-type memory layout.
//
void CBuiltIn::convert8CharToUll(const unsigned char *pSrcChar, long length, unsigned long long &ullDestVal)
{
	//valid input data length is no longer than 8
	if (length > 8)
	{
		length = 8;
	}

	int maxShift = length - 1;

	ullDestVal = 0;
	for (int i = 0; i < length; i++)	// The most significant byte is the first one...
	{
		ullDestVal |= (((unsigned long long)pSrcChar[i]) << (8 * (maxShift - i) ));
	}
}


long CBuiltIn::get_date_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id, unsigned char* pDate, int* size)
{
	return get_date_lelem2(list_id, index, 0, 0, element_id, subelement_id, pDate, size);
}

int CBuiltIn::get_response_code_string
		(
			int iItemId
			, int iMemberId
			, int iResponseCode
			, tchar *pchResponseCodeString
			, int iResponseCodeStringLength
		)
{
	int rtnCode = BLTIN_NOT_IMPLEMENTED;

	if( m_pDevice )	
	{				
		rtnCode = m_pDevice->getResponseCodeString(iItemId, iMemberId, iResponseCode, pchResponseCodeString, iResponseCodeStringLength);				
		rtnCode = ConvertToBLTINCode(rtnCode);
	}

	return(rtnCode); 
}

int CBuiltIn::get_dds_error(tchar *pchErrorString, int iMaxLength)
{
	int iRetValue;
	if( m_pDevice )	
	{				
		iRetValue = m_pDevice->getDdsError(pchErrorString, iMaxLength);				
	}
	else
	{
		iRetValue = BI_NO_DEVICE; 
	}
	
	return(iRetValue);
}

long CBuiltIn::display_builtin_error(long errorCode)
{
	long lRetCode = BLTIN_VAR_NOT_FOUND;
	static size_t stringSize = sizeof(bltinErrorString)/sizeof(bltinErrorString[0]);

	if (errorCode < 0)
	{
		errorCode *= -1;
	}
	if (errorCode != 0)
	{
		errorCode = errorCode - 1800;	//get index in bltinErrorString[] array
	}

	if (errorCode < (long)stringSize)
	{
		lRetCode = ACKNOWLEDGE((tchar*)bltinErrorString[errorCode]);
	}

	return (lRetCode);
}

long CBuiltIn::display_comm_error(unsigned long errorCode)
{
	long lRetCode = BLTIN_VAR_NOT_FOUND;
	static size_t stringSize = sizeof(commErrorString)/sizeof(commErrorString[0]);

	if (errorCode < 0)
	{
		errorCode *= (-1);
	}

	if (errorCode < (unsigned long)stringSize)
	{
		lRetCode = ACKNOWLEDGE((tchar*)commErrorString[errorCode]);
	}

	return (lRetCode);
}

//This function is used by Fieldbus. The return code must expect BLTIN_*
unsigned long CBuiltIn::get_comm_error()
{
	unsigned long iRetValue = (unsigned long) BLTIN_CANNOT_READ_VARIABLE;

	if( m_pDevice )	
	{				
		iRetValue = m_pDevice->getCommError();				
	}
	
	return(iRetValue);
}

long CBuiltIn::get_comm_error_string(unsigned long ulErrorCode, tchar* pchErrorString, long lMaxStringLength)
{
	long lRetCode = BLTIN_VAR_NOT_FOUND;
	static size_t stringSize = sizeof(commErrorString)/sizeof(commErrorString[0]);

	if (ulErrorCode < 0)
	{
		ulErrorCode *= (-1);
	}

	if (ulErrorCode < (unsigned long)stringSize)
	{
		lRetCode = BLTIN_NOT_IMPLEMENTED;
		if(m_pDevice)
		{
			wchar_t *str = new wchar_t[lMaxStringLength];
		
			wchar_t *newstr  = commErrorString[ulErrorCode];

			lRetCode = (m_pDevice->dictionary)->get_string_translation(newstr, str, lMaxStringLength, m_pDevice->device_sLanguageCode);
			wcsncpy(pchErrorString, str, (lMaxStringLength));
			long strLen = (long)wcslen(str);
			if (strLen <= lMaxStringLength)
			{
				pchErrorString[strLen] = _T('\0');
			}
			else
			{
				pchErrorString[lMaxStringLength] = _T('\0');
			}
			
			delete[] str;

			lRetCode = ConvertToBLTINCode(lRetCode);
		}
	}
	return (lRetCode);
}

long CBuiltIn::send_on_exit()
{
	m_pMeth->send_on_exit(); 
	return BI_SUCCESS;
}

long CBuiltIn::save_on_exit()
{
	m_pMeth->save_on_exit(); 
	return BI_SUCCESS;
}

long CBuiltIn::get_response_code(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId)
{
	long iRetValue = BLTIN_NOT_IMPLEMENTED;
	if( m_pDevice )	
	{				
		iRetValue = m_pDevice->getResponseCode(pulResponseCode, pulErrorItemId, pulErrorMemberId);				
		iRetValue = ConvertToBLTINCode(iRetValue);
	}
	
	return(iRetValue);
}

int CBuiltIn::AccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType)
{
	long iRetValue = BI_COMM_ERR;
	if( m_pDevice )	
	{				
		iRetValue = m_pDevice->devAccessAllDeviceValues(eAccessType);				
	}
	
	return(iRetValue);
}

//This function is used to access FF device variable.
long CBuiltIn::AccessDeviceValue(/* in */ unsigned long ulBlockItemId, /* in */ unsigned long iOccurrence, /* in */ unsigned long ulItemId, /* in */ unsigned long ulMemberId, /* in */ AccessType iAccessType)
{
	int iRetValue = BLTIN_NOT_IMPLEMENTED;

	if( m_pDevice )	
	{
		bool bRetry = false;					/*Main loop controlling var*/
		bool bRetryAsked = false;				/*Flag set from the retry/abort masks for the method execution*/
		const int iFFAutoRetryLimit = 3;		//retry limit for FF is 3.
		UIntList_t::iterator i;
		int iRetryCount = 0;					/*Command Retry Counter*/
		PARAM_REF opRef;
		if (GetPARAM_REF(ulBlockItemId, iOccurrence, ulItemId, ulMemberId, &opRef) != BLTIN_SUCCESS)
		{
			return BLTIN_VAR_NOT_FOUND;
		}

		do
		{
			bRetry = false;
			bRetryAsked = false;

			iRetValue = m_pDevice->sendParamValues(&opRef, iAccessType);	


			if(iRetValue == BLTIN_FAIL_RESPONSE_CODE)
			{
				//check all response mask first
				if (m_pMeth->m_bFFAllRespAbortMask)
				{
					iRetValue = BUILTIN_COMM_FAIL_RESPONSE;
				}
				else
				{
					//check individual response mask code
					unsigned long ulRespCode, ulItem, ulMember;
					long lRetVal = get_response_code(&ulRespCode, &ulItem, &ulMember);

					if (lRetVal == BLTIN_SUCCESS)
					{
						//check individual response abort mask
						for (i = m_pMeth->m_vFFRespAbortMask.begin(); i != m_pMeth->m_vFFRespAbortMask.end(); i++)
						{
							if (*i == ulRespCode)
							{
								iRetValue = BUILTIN_COMM_FAIL_RESPONSE;
								break;
							}
						}
					}
				}
				
				if (iRetValue != BUILTIN_COMM_FAIL_RESPONSE)
				{
					if(m_pMeth->m_bFFAllRespRetryMask)
					{
						bRetryAsked = true;
					}
					else
					{
						//check individual response mask code
						unsigned long ulRespCode, ulItem, ulMember;
						long lRetVal = get_response_code(&ulRespCode, &ulItem, &ulMember);

						if (lRetVal == BLTIN_SUCCESS)
						{
							//check individual response retry mask
							for (i = m_pMeth->m_vFFRespRetryMask.begin(); i != m_pMeth->m_vFFRespRetryMask.end(); i++)
							{
								if (*i == ulRespCode)
								{
									bRetryAsked = true;
									break;
								}
							}
						}
					}
				}
			}

			else if(iRetValue == BLTIN_FAIL_COMM_ERROR)
			{
				//check all communication abort mask first
				if (m_pMeth->m_bFFAllCommAbortMask)
				{
					iRetValue = BUILTIN_COMM_ABORTED;
				}
				else
				{
					//check individual communication mask code
					unsigned long ulCommError = get_comm_error();

					//check individual response abort mask
					for (i = m_pMeth->m_vFFCommAbortMask.begin(); i != m_pMeth->m_vFFCommAbortMask.end(); i++)
					{
						if (*i == ulCommError)
						{
							iRetValue = BUILTIN_COMM_ABORTED;
							break;
						}
					}
				}
				
				//check all communication retry mask first
				if (iRetValue != BUILTIN_COMM_ABORTED)
				{
					if(m_pMeth->m_bFFAllCommRetryMask)
					{
						bRetryAsked = true;
					}
					else
					{
						unsigned long ulCommError = get_comm_error();

						//check individual response retry mask
						for (i = m_pMeth->m_vFFCommRetryMask.begin(); i != m_pMeth->m_vFFCommRetryMask.end(); i++)
						{
							if (*i == ulCommError)
							{
								bRetryAsked = true;
								break;
							}
						}
					}
				}
			}

			else if (iRetValue == BI_NO_DEVICE)
			{
				iRetValue = BUILTIN_COMM_NO_DEVICE;
			}

			else if (iRetValue == BI_ABORT)
			{
				iRetValue = BUILTIN_COMM_ABORTED;
			}
			//else do nothing

			if(bRetryAsked == true)
			{
				if(iRetryCount < iFFAutoRetryLimit)
				{
					iRetryCount++;
					bRetry = true;
				}
				//after three retries the error condition persists, either the BLTIN_FAIL_COMM_ERROR or BLTIN_FAIL_RESPONSE_CODE error codes will be returned. see FF-900.pdf
			}

		} while (bRetry == true);
	}

	switch(iRetValue)
	{
	case BUILTIN_COMM_ABORTED:
	case BUILTIN_COMM_NO_DEVICE:
	case BUILTIN_COMM_FAIL_RESPONSE:
		break;
	case BLTIN_SUCCESS:
	default:
		iRetValue = ConvertToBLTINCode(iRetValue);
		break;
	}
	return iRetValue;
}

long CBuiltIn::set_ff_comm_status(unsigned long ulCommStatus, int iAbortIgnoreRetry)
{
	UIntList_t::iterator i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			//push element into abort vector
			m_pMeth->m_vFFCommAbortMask.push_back(ulCommStatus);
			//erase element from retry vector
			for (i = m_pMeth->m_vFFCommRetryMask.begin(); i != m_pMeth->m_vFFCommRetryMask.end(); i++)
			{
				if (*i == ulCommStatus)
				{
					m_pMeth->m_vFFCommRetryMask.erase(i);
					break;
				}
			}
			break;

		case __RETRY__:
			//erase element from abort vector
			for (i = m_pMeth->m_vFFCommAbortMask.begin(); i != m_pMeth->m_vFFCommAbortMask.end(); i++)
			{
				if (*i == ulCommStatus)
				{
					m_pMeth->m_vFFCommAbortMask.erase(i);
					break;
				}
			}
			//push element into retry vector
			m_pMeth->m_vFFCommRetryMask.push_back(ulCommStatus);
			break;

		default:		/* For fail_* builtins in FF */
			//erase element from abort vector
			for (i = m_pMeth->m_vFFCommAbortMask.begin(); i != m_pMeth->m_vFFCommAbortMask.end(); i++)
			{
				if (*i == ulCommStatus)
				{
					m_pMeth->m_vFFCommAbortMask.erase(i);
					break;
				}
			}
			//erase element from retry vector
			for (i = m_pMeth->m_vFFCommRetryMask.begin(); i != m_pMeth->m_vFFCommRetryMask.end(); i++)
			{
				if (*i == ulCommStatus)
				{
					m_pMeth->m_vFFCommRetryMask.erase(i);
					break;
				}
			}
			break;
	}

	return BLTIN_SUCCESS;
}

long CBuiltIn::set_ff_resp_code(unsigned long ulResponseCode, int iAbortIgnoreRetry)
{
	UIntList_t::iterator i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			//push element into abort vector
			m_pMeth->m_vFFRespAbortMask.push_back(ulResponseCode);
			//erase element from retry vector
			for (i = m_pMeth->m_vFFRespRetryMask.begin(); i != m_pMeth->m_vFFRespRetryMask.end(); i++)
			{
				if (*i == ulResponseCode)
				{
					m_pMeth->m_vFFRespRetryMask.erase(i);
					break;
				}
			}
			break;

		case __RETRY__:
			//erase element from abort vector
			for (i = m_pMeth->m_vFFRespAbortMask.begin(); i != m_pMeth->m_vFFRespAbortMask.end(); i++)
			{
				if (*i == ulResponseCode)
				{
					m_pMeth->m_vFFRespAbortMask.erase(i);
					break;
				}
			}
			//push element into retry vector
			m_pMeth->m_vFFRespRetryMask.push_back(ulResponseCode);
			break;

		default:		/* For fail_* builtins in FF */
			//erase element from abort vector
			for (i = m_pMeth->m_vFFRespAbortMask.begin(); i != m_pMeth->m_vFFRespAbortMask.end(); i++)
			{
				if (*i == ulResponseCode)
				{
					m_pMeth->m_vFFRespAbortMask.erase(i);
					break;
				}
			}
			//erase element from retry vector
			for (i = m_pMeth->m_vFFRespRetryMask.begin(); i != m_pMeth->m_vFFRespRetryMask.end(); i++)
			{
				if (*i == ulResponseCode)
				{
					m_pMeth->m_vFFRespRetryMask.erase(i);
					break;
				}
			}
			break;
	}

 return BLTIN_SUCCESS;

}

int CBuiltIn::set_ff_all_comm_status(int iAbortIgnoreRetry)
{
	UIntList_t::iterator i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			
			m_pMeth->m_bFFAllCommAbortMask = true;

			m_pMeth->m_bFFAllCommRetryMask = false;
			m_pMeth->m_vFFCommRetryMask.clear();

			break;

		case __RETRY__:
			m_pMeth->m_bFFAllCommAbortMask = false;
			m_pMeth->m_vFFCommAbortMask.clear();

			m_pMeth->m_bFFAllCommRetryMask = true;

			break;

		default:		/* For fail_* builtins in FF */
			m_pMeth->m_bFFAllCommAbortMask = false;
			m_pMeth->m_vFFCommAbortMask.clear();

			m_pMeth->m_bFFAllCommRetryMask = false;
			m_pMeth->m_vFFCommRetryMask.clear();

			break;
	}
	
	return BLTIN_SUCCESS;
}

int CBuiltIn::set_ff_all_resp_code(int iAbortIgnoreRetry)
{
	UIntList_t::iterator i;

	switch(iAbortIgnoreRetry)
	{
		case __ABORT__:
			m_pMeth->m_bFFAllRespAbortMask = true;

			m_pMeth->m_bFFAllRespRetryMask = false;
			m_pMeth->m_vFFRespRetryMask.clear();

			break;

		case __RETRY__:
			m_pMeth->m_bFFAllRespAbortMask = false;
			m_pMeth->m_vFFRespAbortMask.clear();

			m_pMeth->m_bFFAllRespRetryMask = true;

			break;

		default:		/* For fail_* builtins in FF */
			m_pMeth->m_bFFAllRespAbortMask = false;
			m_pMeth->m_vFFRespAbortMask.clear();

			m_pMeth->m_bFFAllRespRetryMask = false;
			m_pMeth->m_vFFRespRetryMask.clear();

			break;
	}

	return BLTIN_SUCCESS;
}

//This function is used to assign one variable value from another variable.
long CBuiltIn::assign(unsigned long uldst_BlockItemId, unsigned long uldst_Occurrence, unsigned long uldst_ItemId, unsigned long uldst_MemberId, 
					  unsigned long ulsrc_BlockItemId, unsigned long ulsrc_Occurrence, unsigned long ulsrc_ItemId, unsigned long ulsrc_MemberId)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if(m_pDevice != NULL)
	{
		//get destination operative item ID
		PARAM_REF dst_opRef;
		if (GetPARAM_REF(uldst_BlockItemId, uldst_Occurrence, uldst_ItemId, uldst_MemberId, &dst_opRef) != BLTIN_SUCCESS)
		{
			return BLTIN_VAR_NOT_FOUND;
		}
		//get source operative item ID
		PARAM_REF src_opRef;
		if (GetPARAM_REF(ulsrc_BlockItemId, ulsrc_Occurrence, ulsrc_ItemId, ulsrc_MemberId, &src_opRef) != BLTIN_SUCCESS)
		{
			return BLTIN_VAR_NOT_FOUND;
		}

		iRetVal = m_pDevice->assignValue(&dst_opRef, &src_opRef);
		if (iRetVal == BI_ABORT)
		{
			return (iRetVal);	//return BI_ABORT directly
		}
	}

	iRetVal = ConvertToBLTINCode(iRetVal);
	return (iRetVal);
}

long CBuiltIn::get_date_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, unsigned char *stringval, long *len)
{
	int iRetVal = BLTIN_BUFFER_TOO_SMALL;

	//first of all, check if the input item ID is not member ID that sometimes messed up in legacy FF
	//and get item size
	CValueVarient vtSize{};
	CStdString sProperty;

	sProperty.Format(DCI_PROPERTY_SIZE);
	ulItemId = VerifyItemIdNGetProperty(ulBlkId, ulBlkNum, ulItemId, ulMemberId, sProperty, &vtSize);
	

	if ((ulItemId != 0) && (ValueTagType::CVT_EMPTY != vtSize.vTagType))
	{
		if (CV_INT(&vtSize) < *len)
		{
			*len = CV_INT(&vtSize);
		}

		CValueVarient vtValue{};
		iRetVal = get_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vtValue);
		if (iRetVal == BLTIN_SUCCESS)
		{
			//convert returned data into a simple variable, then into a char buffer
			
			unsigned long long ullDate  = CV_UI8(&vtValue);

			convertUllTo8Char(ullDate, stringval, *len);
		}
	}

	return (iRetVal);
}

long CBuiltIn::get_signed_value(unsigned long ulItemId, unsigned long ulMemberId, long *lValue)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(0, 0, ulItemId, ulMemberId, &vtValue);
	if (iRetVal == BLTIN_SUCCESS)
	{
		
		*lValue = CV_I4(&vtValue);
	}
	return (iRetVal);
}

long CBuiltIn::get_unsigned_value(unsigned long ulItemId, unsigned long ulMemberId, unsigned long *ulValue)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(0, 0, ulItemId, ulMemberId, &vtValue);
	if (iRetVal == BLTIN_SUCCESS)
	{
		
		*ulValue = CV_UI4(&vtValue);
	}
	return (iRetVal);
}

long CBuiltIn::get_string_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, wchar_t *stringval, long *length)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vtValue);
	if (iRetVal == BLTIN_SUCCESS)
	{		
		PS_Wcsncpy(stringval, *length, CV_WSTR(&vtValue).c_str(), _TRUNCATE);

		//retrieve real string length shorter than the maximum size of the string buffer
		long len = (long)wcslen(CV_WSTR(&vtValue).c_str());
		if (*length > len)
		{
			*length = len;
		}
	}

	return (iRetVal);
}

long CBuiltIn::get_bitstring_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, _BYTE_STRING *pBitStr)
{
	CValueVarient vtValue{};
	long iRetVal = get_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vtValue);

	if (iRetVal == BLTIN_SUCCESS)
	{
		if (vtValue.vTagType == ValueTagType::CVT_BITSTR)
		{
			ULONG uSize = CV_BITSTR_LEN(&vtValue);
			
			if (uSize < pBitStr->bsLen)
			{
				pBitStr->bsLen = uSize;
			}

			memcpy(pBitStr->bs, CV_BITSTR_BUFFER(&vtValue), pBitStr->bsLen);
		}
		else
		{
			iRetVal = BLTIN_WRONG_DATA_TYPE;
		}
	}

	return (iRetVal);
}

long CBuiltIn::put_string_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long id, unsigned long member_id, wchar_t* strval)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_WSTR); 
	CV_WSTR(&vtValue) = strval;
	long lRetVal = put_variable_value(ulBlkId, ulBlkNum, id, member_id, &vtValue);

	return (lRetVal);
}

long CBuiltIn::put_bitstring_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, _BYTE_STRING *pBitStr)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_BITSTR);
	CV_BITSTR(&vtValue, pBitStr->bs, pBitStr->bsLen);
	
	long iRetVal = put_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vtValue);

	return (iRetVal);
}

long CBuiltIn::put_signed_value(unsigned long id, unsigned long member_id, long value)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_I4);
	CV_I4(&vtValue) = value;
	long lRetVal = put_variable_value(0, 0, id, member_id, &vtValue);
	return (lRetVal);
}

long CBuiltIn::put_unsigned_value(unsigned long id, unsigned long member_id, unsigned long value)
{
	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_UI4);
	CV_UI4(&vtValue) = value;
	long lRetVal = put_variable_value(0, 0, id, member_id, &vtValue);
	return (lRetVal);
}

ITEM_ID CBuiltIn::resolve_block_ref(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType)
{
	ITEM_ID ulRetValue;
	
	ulResolveStatus = 0; 
	if( m_pDevice )	
	{				
		ulRetValue = m_pDevice->ResolveBlockRef(ulBlockItemId, iOccurrence, ulMemberId, eResolveType);				
		if (ulRetValue == 0)
		{
			ulResolveStatus = BLTIN_BAD_ID; 
		}
	}
	else
	{
		ulRetValue = 0;
		ulResolveStatus = BLTIN_VAR_NOT_FOUND; 
	}
	return(ulRetValue);
}

unsigned long CBuiltIn::resolve_list_ref(unsigned long  ulItemId)
{
	unsigned long ulRetValue;

	ulResolveStatus = 0; 
	if( m_pDevice )	
	{		
		CValueVarient vtType{};
		
		if ( m_pDevice->ReadParameterData2( &vtType, DCI_PROPERTY_ITEM_ID, 0, 0, ulItemId, 1) == BLTIN_SUCCESS )
		{
			
			ulRetValue = CV_UI4(&vtType);
		}
		else
		{
			ulRetValue = 0;
			ulResolveStatus = BLTIN_BAD_ID; 
		}
	}
	else
	{
		ulRetValue = 0;
		ulResolveStatus = BLTIN_VAR_NOT_FOUND; 
	}
	return(ulRetValue);
}

long CBuiltIn::put_date_value(unsigned long ulItemId, unsigned long ulMemberId, unsigned char* pchVal, long lLength)
{
	//convert date value to a integer variable
	unsigned long long ullDate = 0;
	convert8CharToUll (pchVal, lLength, ullDate);

	CValueVarient vtValue{};
	CV_VT(&vtValue, ValueTagType::CVT_UI8);
	CV_UI8(&vtValue) = ullDate;
	long lRetVal = put_variable_value(0, 0, ulItemId, ulMemberId, &vtValue);
	return (lRetVal);
}

unsigned long CBuiltIn::get_resolve_status()
{
	return ulResolveStatus;
}

long CBuiltIn::get_status_string( unsigned long ulItemId, unsigned long ulMemberId, unsigned long ulStatusCode, tchar *pchStatusString, long lMaxLength)
{
	long lRet = BLTIN_NOT_IMPLEMENTED;

	if( m_pDevice )	
	{
		wstring status_str;
		hCitemBase* pIB = NULL;

		lRet = m_pDevice->getElementOrMemberPointer(ulItemId, ulMemberId, &pIB);

		if (lRet == BLTIN_SUCCESS && pIB != NULL)
		{
			hCVar *pVar=(hCVar *)pIB;

			switch(pVar->VariableType())
			{
			case vT_Enumerated:
				{
					hCEnum *pEnum=(hCEnum *)pVar;
					lRet = pEnum->procureString(ulStatusCode, status_str); // input value, get a string							
					break;
				}
			case vT_BitEnumerated:
				{
					hCBitEnum *pBitEnum=(hCBitEnum *)pVar;
					lRet = pBitEnum->procureString(ulStatusCode, status_str); // input value, get a string
					break;					
				}
			default:
				break;
			}

			if(!status_str.empty())
			{
				_tstrncpy(pchStatusString,status_str.c_str(),(lMaxLength-1));
				pchStatusString[lMaxLength-1] = '\0';
			}
			else 
			{
				pchStatusString[0] = '\0';
			}
		}
		else
		{
			lRet = BLTIN_VAR_NOT_FOUND;
		}	
	}

	lRet = ConvertToBLTINCode(lRet);
	return(lRet); 
}

BI_ErrorCode CBuiltIn::ConvertToBICode(int iErrorCode)
{
	BI_ErrorCode biCode = BI_ERROR;

	// Add the BI_* codes in here, like I did below.
	// There is no need to add all the BLTIN_* codes here, since they all map to BI_ERROR.
	switch(iErrorCode)
	{
	case BI_ERROR:
	case BI_ABORT:
	case BI_COMM_ERR:
	case BI_NO_DEVICE:
		biCode = (BI_ErrorCode)iErrorCode;
		break;
	case BLTIN_SUCCESS:
		biCode = BI_SUCCESS;
		break;
	case BLTIN_DDS_ERROR:
		biCode = BI_ABORT;
		break;
	case BLTIN_FAIL_COMM_ERROR:
		biCode = BI_COMM_ERR;
		break;
	default:
		biCode = BI_ERROR;
		break;
	}

	return biCode;
}

BLTIN_ErrorCode CBuiltIn::ConvertToBLTINCode(int iErrorCode)
{
	BLTIN_ErrorCode bltinCode = BLTIN_NOT_IMPLEMENTED;

	switch(iErrorCode)
	{
	case BI_SUCCESS:
		bltinCode = BLTIN_SUCCESS;
		break;
	case BI_ERROR:
		bltinCode = BLTIN_VAR_NOT_FOUND;
		break;
	case BI_ABORT:
		bltinCode = BLTIN_DDS_ERROR;	//method will be aborted
		break;
	case BI_COMM_ERR:
	case BI_NO_DEVICE:
		bltinCode = BLTIN_FAIL_COMM_ERROR;
		break;

	// Leave any BLTIN_* codes as they are
	case BLTIN_NO_MEMORY:
	case BLTIN_VAR_NOT_FOUND:
	case BLTIN_BAD_ID:
	case BLTIN_NO_DATA_TO_SEND:
	case BLTIN_WRONG_DATA_TYPE:
	case BLTIN_NO_RESP_CODES:
	case BLTIN_BAD_METHOD_ID:
	case BLTIN_BUFFER_TOO_SMALL:
	case BLTIN_CANNOT_READ_VARIABLE:
	case BLTIN_INVALID_PROMPT:
	case BLTIN_NO_LANGUAGE_STRING:
	case BLTIN_DDS_ERROR:
	case BLTIN_FAIL_RESPONSE_CODE:
	case BLTIN_FAIL_COMM_ERROR:
	case BLTIN_NOT_IMPLEMENTED:
	case BLTIN_BAD_ITEM_TYPE:
	case BLTIN_VALUE_NOT_SET:
	case BLTIN_BLOCK_NOT_FOUND:
		bltinCode = (BLTIN_ErrorCode)iErrorCode;
		break;
		
	// Anything else, return BLTIN_DDS_ERROR (not really correct, but the best choice)
	default:
		bltinCode = BLTIN_NOT_IMPLEMENTED;
		break;
	}

	return bltinCode;
}

//This function is called by FF builtin only to read or write value
long CBuiltIn::AccessTypeValue2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue, AccessType eAccessType)
{
	long iRetVal = BLTIN_NOT_IMPLEMENTED;

	switch (eAccessType)
	{
	case READ:
		iRetVal = get_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, pvtValue);
		break;
	case WRITE:
		iRetVal = put_variable_value(ulBlkId, ulBlkNum, ulItemId, ulMemberId, pvtValue);
		break;
	}

	return (iRetVal);
}

long CBuiltIn::get_block_instance_count(unsigned long ulBlockId, unsigned long *pulCount)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;
	
	if( m_pDevice != NULL )	
	{				
		iRetVal = m_pDevice->getBlockInstanceCount(ulBlockId, pulCount);				
	}
	
	iRetVal = ConvertToBLTINCode(iRetVal);
	return (iRetVal);
}

int CBuiltIn::get_block_instance_by_object_index(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;
	
	if( m_pDevice != NULL )	
	{				
		iRetVal = m_pDevice->getBlockByObjectIndex(ulObjectIndex, pulRelativeIndex);				
	}
	
	iRetVal = ConvertToBLTINCode(iRetVal);
	return (iRetVal);
}

int CBuiltIn::get_block_instance_by_block_tag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;
	
	if( m_pDevice != NULL )	
	{				
		iRetVal = m_pDevice->getBlockByBlockTag(ulBlockId, pchBlockTag, pulRelativeIndex);				
	}

	iRetVal = ConvertToBLTINCode(iRetVal);
	return (iRetVal);
}

long CBuiltIn::get_date_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, unsigned char *pchData, int* iSize)
{
	int iRetVal = BLTIN_VAR_NOT_FOUND;

	if (0 == ulListId)
	{
		return iRetVal;
	}

 	long dateLen = *iSize;	//date variable maximum size
	*iSize = 0;				//initial value in case of failure

   /*Check is device exists*/
    if (m_pDevice)
    {
		//get item size
		CValueVarient vtSize{};
		iRetVal = m_pDevice->ReadParameterData2( &vtSize, DCI_PROPERTY_SIZE, 0, 0, ulElementId, ulSubElementId );
		

		if ((iRetVal == BLTIN_SUCCESS) && (ValueTagType::CVT_EMPTY != vtSize.vTagType))
		{
			if (CV_INT(&vtSize) < dateLen)
			{
				*iSize = CV_INT(&vtSize);
			}
			else
			{
				*iSize = dateLen;
			}

			CValueVarient data{};
			iRetVal = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
			iRetVal = ConvertToBLTINCode(iRetVal);
			if(iRetVal == BLTIN_SUCCESS)
			{
				//convert returned data into a simple variable, then into a string
				
				unsigned long long ullDate  = CV_UI8(&data);

				convertUllTo8Char(ullDate, pchData, *iSize);
			}
		}
		else
		{
			iRetVal = BLTIN_BUFFER_TOO_SMALL;
		}
    }

	return iRetVal;
}

double CBuiltIn::get_double_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId)
{
	double dRetValue = 0.0;

	if (0 == ulListId)
	{
		return dRetValue;
	}

    /*Check is device exists*/
    if (m_pDevice)
    {
        CValueVarient data{};
        int iRetCode = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
        if(iRetCode == BLTIN_SUCCESS)
        {
			
            dRetValue = CV_R8(&data);
        }
    }

    return dRetValue;

}

float CBuiltIn::get_float_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId)
{
	float fRetVal = 0.0;

	if (0 == ulListId)
	{
		return fRetVal;
	}

    /*Check is device exists*/
    if (m_pDevice)
    {
        CValueVarient data{};
        int iRetCode = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
        if(iRetCode == BLTIN_SUCCESS)
        {
			
            fRetVal = static_cast<float>(CV_R4(&data));
        }
    }

    return fRetVal;
}

long CBuiltIn::get_signed_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId)
{
	long lRetVal = 0;

	if (0 == ulListId)
	{
		return lRetVal;
	}

    /*Check is device exists*/
    if (m_pDevice)
    {
        CValueVarient data{};
        int iRetCode = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
        if(iRetCode == BLTIN_SUCCESS)
        {
 			
            lRetVal = CV_I4(&data);
        }
    }
    return lRetVal;
}

long CBuiltIn::get_string_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, tchar *pchData, int* iSize)
{
	long lRetValue = BLTIN_NOT_IMPLEMENTED;

	if (0 == ulListId)
    {
        return lRetValue;
    }

    /*Check is device exists*/
    if (m_pDevice)
    {
        CValueVarient data{};
        lRetValue = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
		lRetValue = ConvertToBLTINCode(lRetValue);
        if(lRetValue == BLTIN_SUCCESS)
        {
			

			int len = (int)wcslen((CV_WSTR(&data)).c_str());
            //retrieve real string length shorter than the maximum size of the string buffer
            if (*iSize > len)
            {
                *iSize = len;
            }

            wcsncpy(pchData, (CV_WSTR(&data)).c_str(), *iSize);
        }
    }
    return lRetValue;
}

unsigned long CBuiltIn::get_unsigned_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId)
{
	unsigned long ulRetVal = 0;

	if (0 == ulListId)
	{
		return ulRetVal;
	}

    /*Check is device exists*/
    if (m_pDevice)
    {
        CValueVarient data{};
        int iRetCode = m_pDevice->getListElem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, &data);
        if(iRetCode == BLTIN_SUCCESS)
        {
 			
            ulRetVal = CV_UI4(&data);
        }
    }
    return ulRetVal;
}

void CBuiltIn::LOG_MESSAGE(int priorityVal, wchar_t *msg)
{
	if (m_pDevice)
	{
		tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
		bool	bDynVarValsChanged=false;

		int retval= bltin_format_string2 (out_buf, DOUBLE_MAX_DD_STRING, msg, 0, 0, 0, 0, 0, &bDynVarValsChanged);

		if (retval == BLTIN_SUCCESS)
		{
			m_pDevice->deviceLogMessage(priorityVal, out_buf);
		}
	}
}

int CBuiltIn::isOffline()
{
	int iRetVal = 1;	//Offline

	if (m_pDevice)
	{
		iRetVal = m_pDevice->deviceIsOffline();
	}
	return iRetVal;
}

int CBuiltIn::ListDeleteElementAt2(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, int iCount)
{
	hCitemBase *pIB = NULL;
	int iRetCode = BLTIN_WRONG_DATA_TYPE;

	if((0 >= ulListId)  || (-1 >= iListIndex))
	{
		return BLTIN_NOT_IMPLEMENTED;
	}

	/*Check is List exists & is Valid*/
	if((BLTIN_SUCCESS == m_pDevice->getItemBySymNumber(ulListId, &pIB))
		&& (NULL != pIB)
		&& (pIB->IsValidTest())
		)
	{
		itemType_e itemType = pIB->getIType();

		if (itemType == iT_List)
		{
			iRetCode = m_pDevice->sendListOpMethod(ulListId, iListIndex, ulEmbListId, iIndex, 0, iCount);	//zero == deletion
		}
	}

	ProtocolType iProtocol = m_pMeth->GetStartedProtocol();

	if ((iProtocol == nsEDDEngine::HART) || (iProtocol == nsEDDEngine::PROFIBUS))
	{
		if(iRetCode != BI_SUCCESS)
		{
			iRetCode = BI_ERROR;
		}
	}
	else	//FF
	{
		iRetCode = ConvertToBLTINCode(iRetCode);
	}
	return (iRetCode);
}

//The following builtins are used by Profibus
int CBuiltIn::GET_DDL(DDLInfoType ddlType)
{
	int iRetVal = 0;

	if( m_pDevice )
	{
		CValueVarient vtDLL{};
		if ( m_pDevice->ReadMethodData( 0, _T("DDL"), &vtDLL) )
		{
			if( vtDLL.vTagType == ValueTagType::CVT_WSTR )
			{
				wchar_t *sDLLStr = const_cast<wchar_t*>((CV_WSTR(&vtDLL)).c_str());

				long long manufacturer=0;
				long device_type = 0, device_revision = 0, dd_revision = 0;
                swscanf(sDLLStr, _T("%08llx%04x%04x%04x"), &manufacturer, &device_type, &device_revision, &dd_revision);
				switch (ddlType)
				{
				case (DDLInfoType)GET_DD_REVISION:
					iRetVal = dd_revision;
					break;
				case (DDLInfoType)GET_DEVICE_REVISION:
					iRetVal = device_revision;
					break;
				case (DDLInfoType)GET_DEVICE_TYPE:
					iRetVal = device_type;
					break;
				case (DDLInfoType)GET_MANUFACTURER:
					iRetVal = (int)manufacturer;
					break;
				}

				
			}
		}
	}

	return iRetVal;
}

int CBuiltIn::ProfibusSendCommand (unsigned long commandId, long *responseCode)
{
	long iRetValue = (long) BI_COMM_ERR;

	if( m_pDevice )	
	{
		bool bRetry = false;			/*Main loop controlling var*/
		int iRetryCount = 0;			/*Command Retry Counter*/

		do
		{
			bRetry = false;

			iRetValue = m_pDevice->deviceSendCommand(commandId, responseCode);			

			//check no device retry mask
			switch(iRetValue)
			{
			case BLTIN_SUCCESS:
				break;
			case BI_NO_DEVICE:
				if (m_pMeth->m_byReturnNodevRetryMask)
				{
					if(iRetryCount < m_pMeth->m_iAutoRetryLimit)
					{
						iRetryCount++;
						bRetry = true;
					}
					else
					{
						iRetValue = BUILTIN_RETRY_FAILED;
						break;
					}
				}
				else if(m_pMeth->m_byReturnNodevAbortMask)
				{	
					iRetValue = BUILTIN_COMM_NO_DEVICE;
				}
				else
				{
					iRetValue = BI_NO_DEVICE;
				}
				break;
			case BI_ABORT:
				iRetValue = BUILTIN_COMM_ABORTED;
				break;
			case BLTIN_FAIL_RESPONSE_CODE:
			case BLTIN_FAIL_COMM_ERROR:
			default:
				iRetValue = BI_COMM_ERR;
				break;
			}

		} while (bRetry == true);
	}

	return(iRetValue);
}

//This function will get a textual representation of the referenceced variable
int CBuiltIn::get_variable_string(ITEM_ID ulItemId, wchar_t *pValueDisp, unsigned int iValueDispLen)
{
	long iRetValue = (long) BLTIN_NOT_IMPLEMENTED;

	if (m_pDevice)
	{
		//first of all, check if the input item ID is not member ID that sometimes messed up in legacy FF
		CValueVarient vtItemType{};	//not used here
		CStdString sProperty;

		sProperty.Format(DCI_PROPERTY_KIND);
		ulItemId = VerifyItemIdNGetProperty(0, 0, ulItemId, 0, sProperty, &vtItemType);

		pValueDisp[0] = _T('\0');

		//get data class instance
		hCitemBase* pIB = NULL;
		iRetValue = m_pDevice->getItemBySymNumber(ulItemId, &pIB);

		if ((iRetValue == BLTIN_SUCCESS) && (pIB != NULL))
		{
			//get variable value
			CValueVarient vtValue{};
			iRetValue = get_variable_value(0, 0, ulItemId, 0, &vtValue);
			
			//format the value
			if (iRetValue == BLTIN_SUCCESS)
			{
				wchar_t wsFormat[20] = L"";
				INTER_VARIANT	ivValue;
				ivValue = INTER_VARIANT::InitFrom(&vtValue);
				iRetValue = doFormat (pValueDisp, iValueDispLen, wsFormat, 20, &ivValue, (hCVar *)pIB, false);
			}
		}
	}

	return(iRetValue);
}

int CBuiltIn::display_bitenum (unsigned long ulItemId)
{
	//get value
	CValueVarient vtValue{};
	long iRetValue = get_variable_value(0, 0, ulItemId, 0, &vtValue);

	hCitemBase* pIB = NULL;
	if( (iRetValue == BLTIN_SUCCESS) && (m_pDevice != NULL) && (m_pDevice->getItemBySymNumber(ulItemId, &pIB) == BLTIN_SUCCESS) )
	{				
		if( pIB != NULL )
		{
			

			hCVar *pVar = (hCVar*)pIB;
			if (pVar->VariableType() == vT_BitEnumerated)
			{
				hCBitEnum *pBitEnum = (hCBitEnum *)pVar;
				unsigned long ulBitEnumVal = CV_UI4(&vtValue);
				wstring sBitEnumDesc = wstring(L"");
				iRetValue = pBitEnum->procureString(ulBitEnumVal, sBitEnumDesc);
				if (iRetValue == BLTIN_SUCCESS)
				{
					iRetValue = acknowledge((wchar_t*)sBitEnumDesc.c_str(), (long *)&ulItemId, NULL, 0);
				}
			}
		}
	}

	return iRetValue;
}



double CBuiltIn::get_sel_double2(unsigned long ulItemId, int iIndex, unsigned long ulEmbededId, unsigned long ulSelector, unsigned long ulAdditional)
{
	double dOutput = 0;
	const varAttrType_t aAttrArray[12] = {varAttrViewMin, varAttrViewMax, varAttrMinValue, varAttrMaxValue, varAttrConstUnit, varAttrLabel, varAttrHelp, varAttrCapacity, varAttrCount, varAttrScaling, varAttrXAxis, varAttrYAxis};

	if (m_pDevice != NULL)
	{
		//first of all, check if the input item ID is not member ID that sometimes messed up in legacy FF
		CValueVarient vtItemId{};
		CStdString sProperty;

		sProperty.Format(DCI_PROPERTY_ITEM_ID);
		ulItemId = VerifyItemIdNGetProperty(0, 0, ulItemId, iIndex, sProperty, &vtItemId);

		//find out reference item ID if needed
		if (ulEmbededId != 0)
		{
			

			if (vtItemId.vTagType != ValueTagType::CVT_EMPTY)
			{
				//overwrite the item ID and index/member ID for the use below.
				ulItemId = CV_UI4(&vtItemId);
				iIndex = ulEmbededId;
			}
		}

		hCitemBase* pIB = NULL;

		//find out the item data pointer
		int iRetVal =  m_pDevice->getItemBySymNumber(ulItemId, &pIB);
		if ( (iRetVal == BLTIN_SUCCESS) && (pIB != NULL) )
		{
			switch (aAttrArray[ulSelector - 16])
			{
			case varAttrViewMin:
			case varAttrViewMax:
			case varAttrCount:
				{
					CValueVarient paramValue{};
					PARAM_REF opRef;

					opRef.op_info.id = ulItemId;
					opRef.op_info.member = iIndex;
					opRef.op_info.type = pIB->getIType();
					iRetVal = m_pDevice->AccessDynamicAttributeMethod(&opRef, &paramValue, aAttrArray[ulSelector - 16], READ);
					if (paramValue.vType != paramValue.isFloatConst)
					{
						paramValue.vValue.fFloatConst = paramValue.vValue.varSymbolID;
					}
					dOutput = CV_R8(&paramValue); //(double)CV_I2(&paramValue);
				}
				break;
			case varAttrMinValue:
			case varAttrMaxValue:
				{
					hCRangeList RangeList;
					if ( (pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric()) )
					{
						RangeList = ((hCNumeric*)pIB)->getRangeList();
						iRetVal = SUCCESS;
					}
					else if (pIB->getIType() == iT_Axis)
					{
						iRetVal = ((hCaxis*)pIB)->getMinMaxList(RangeList);
					}
					else
					{// only numeric variables or axis have a min - max
						SDCLOG(CERR_LOG,"get_sel_double2(): '%s' must be a numeric or axis to "
										"resolve MIN_VALUE\n",	pIB->getName().c_str() );
						iRetVal = BLTIN_VAR_NOT_FOUND;
					}

					// we are doing a number MIN_VALUEn or MAX_VALUEn
					if (iRetVal == SUCCESS)
					{
						RangeList_t::iterator RangeIterator = RangeList.find(ulAdditional);
						if ( RangeIterator == RangeList.end() )
						{
							SDCLOG(CERR_LOG,"get_sel_double2(): Did not find MIN_VALUE%d in '%s'\n",
								pIB->getName().c_str(), ulAdditional  );
							iRetVal = BLTIN_CANNOT_READ_VARIABLE;
						}// else we found it
						else
						{
							if (aAttrArray[ulSelector - 16] == varAttrMinValue)
							{
								dOutput = (double) (RangeIterator->second).minVal;
							}
							else
							{
								dOutput = (double) (RangeIterator->second).maxVal;
							}
						}
					}
				}
				break;
			case varAttrCapacity:
				if (pIB->getIType() == iT_List)
				{
					hClist* pListPtr = (hClist *)pIB;
					dOutput = pListPtr->getCapacity();
				}
				break;
			case varAttrScaling:
				if (m_pDevice != NULL)
				{
					CValueVarient vtValue{};
					iRetVal = m_pDevice->ReadParameterData2( &vtValue, "Scaling", 0, 0, ulItemId, iIndex );
					if (iRetVal  == BLTIN_SUCCESS)
					{
						
						dOutput = CV_R8(&vtValue);
					}
				}
				break;
			case varAttrXAxis:		//looking for X_Axis item ID?
				{
					CValueVarient vtValue{};
					iRetVal = m_pDevice->ReadParameterData2( &vtValue, _T("XAxisItemID"), 0, 0, ulItemId, iIndex);
					if (iRetVal  == BLTIN_SUCCESS)
					{
						
						dOutput = CV_R8(&vtValue);
					}
				}
			break;
			case varAttrYAxis:		//looking for Y_Axis item ID?
				{
					CValueVarient vtValue{};
					iRetVal = m_pDevice->ReadParameterData2( &vtValue, _T("YAxisItemID"), 0, 0, ulItemId, iIndex);
					if (iRetVal  == BLTIN_SUCCESS)
					{
						
						dOutput = CV_R8(&vtValue);
					}
				}
			break;
			default:
				iRetVal = BLTIN_WRONG_DATA_TYPE;
				break;
			}
		}
	}

	return dOutput;
}

long CBuiltIn::get_sel_string(char* pchString, unsigned long ulItemId, unsigned long ulSelector, unsigned long /*ulAdditional*/, long *length)
{
	long lRetVal = BLTIN_NOT_IMPLEMENTED;
	const varAttrType_t aAttrArray[12] = {varAttrViewMin, varAttrViewMax, varAttrMinValue, varAttrMaxValue, varAttrConstUnit, varAttrLabel, varAttrHelp, varAttrCapacity, varAttrCount, varAttrScaling, varAttrXAxis, varAttrYAxis};


	if (m_pDevice != NULL)
	{
		//first of all, check if the input item ID is not member ID that sometimes messed up in legacy FF
		CValueVarient vtItemType{};	//not used here
		CStdString sProperty;

		sProperty.Format(DCI_PROPERTY_KIND);
		ulItemId = VerifyItemIdNGetProperty(0, 0, ulItemId, 0, sProperty, &vtItemType);

		hCitemBase* pIB = NULL;
		lRetVal =  m_pDevice->getItemBySymNumber(ulItemId, &pIB);
		if ( (lRetVal == BLTIN_SUCCESS) && (pIB != NULL) )
		{
			wstring pStr;
			switch (aAttrArray[ulSelector - 16])
			{
			case varAttrLabel:
				lRetVal = pIB->Label(pStr);
				break;
			case varAttrHelp:
				lRetVal = pIB->Help(pStr);
				break;
			case varAttrConstUnit:
				{
					if (pIB->IsVariable())
					{
						hCVar *pVar = (hCVar *)pIB;
						lRetVal = pVar->getUnitString(pStr);	//pStr is output
					}
					else
					{
						//shall be AXIS item type
						CValueVarient vtValue{};	
						lRetVal = m_pDevice->ReadParameterData2( &vtValue, "Units", 0, 0, ulItemId, 0 );
						if ( (lRetVal  == BLTIN_SUCCESS) && (vtValue.vTagType == ValueTagType::CVT_WSTR) )
						{
							pStr = CV_WSTR(&vtValue);							
						}
					}
				}
				break;
			default:
				lRetVal = BLTIN_NOT_IMPLEMENTED;
				break;
			}

			if (lRetVal  == BLTIN_SUCCESS)
			{
				//convert wchar_t* to char*
				*length = (long)pStr.length();
				size_t convertedChars = 0;
				size_t  sizeInBytes = (size_t)((*length + 1) * 2);
				errno_t err = PS_Wcstombs(&convertedChars, pchString, sizeInBytes, pStr.c_str(), _TRUNCATE);
				if (err != 0)
				{
					lRetVal = BLTIN_CANNOT_READ_VARIABLE;
				}
			}
		}
	}

	return lRetVal;
}

//This function is used to get variable value.
//If the variable is a string and the buffer size is known, the argument size should be set. Otherwise, the argument size can be zero.
long CBuiltIn::get_variable_value(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue)
{
	PARAM_REF opRef;
	if (GetPARAM_REF(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &opRef) != BLTIN_SUCCESS)
	{
		return BLTIN_VAR_NOT_FOUND;
	}

	int iRetVal = BLTIN_BAD_ID;
	
	if (m_pDevice)
	{
		pvtValue->clear();
		iRetVal = m_pDevice->devAccessTypeValue2(&opRef, pvtValue, READ);
		if (iRetVal == BI_ABORT)
		{
			return (iRetVal);	//return BI_ABORT directly
		}
	}

	long lRetVal = ConvertToBLTINCode(iRetVal);
	return (lRetVal);
}

//This function is used to set variable value.
//If the variable is a string and the buffer size is known, the argument size should be set. Otherwise, the argument size can be zero.
long CBuiltIn::put_variable_value(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue)
{
	PARAM_REF opRef;
	if (GetPARAM_REF(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &opRef) != BLTIN_SUCCESS)
	{
		return BLTIN_VAR_NOT_FOUND;
	}

	int iRetVal = BLTIN_BAD_ID;

	if (m_pDevice)
	{
		iRetVal = m_pDevice->devAccessTypeValue2(&opRef, pvtValue, WRITE);
		if (iRetVal == BI_ABORT)
		{
			return (iRetVal);	//return BI_ABORT directly
		}
	}

	long lRetVal = ConvertToBLTINCode(iRetVal);
	return (lRetVal);
}


/****************************************************************************************************************************** 
 * This function VerifyItemIdNGetProperty() returns variable property and returns
 * 1) the given input as item ID if the input is a real item ID;
 * 2) item ID found from the given member ID if the protocol is FF
 * 
 * Inputs:
 *	ulBlockItemId is block item ID used for cross block
 *	ulOccurrence is block occurence number used for cross block
 *	ulItemId is item ID
 *	ulMemberId is member ID
 *	sProperty is variable property string
 *
 * Output pvtProperty is variable property value
 * output new item ID if the function succeeds. Otherwise, the funtion returns 0 as error.
*******************************************************************************************************************************/
ITEM_ID CBuiltIn::VerifyItemIdNGetProperty(const ITEM_ID ulBlockItemId, const unsigned long ulOccurrence, 
										   const ITEM_ID ulItemId, ITEM_ID ulMemberId, CStdString sProperty, /*out*/CValueVarient *pvtProperty)
{
	ITEM_ID ulRetItemId = 0;		//intialized to be failure

	pvtProperty->clear();
	if (m_pDevice)
	{
		//get variable property
		int iReturnValue = m_pDevice->ReadParameterData2( pvtProperty, sProperty, ulBlockItemId, ulOccurrence, ulItemId, ulMemberId );

		if ( (m_pMeth->GetStartedProtocol() == nsEDDEngine::FF) && (iReturnValue == BLTIN_BAD_ID) )
		{
			//This happens in legacy FF device
			//The given item ID is actually a member ID. turn the member id into real item id
			ulRetItemId = m_pDevice->ResolveBlockRef(ulBlockItemId, ulOccurrence, ulItemId, RESOLVE_PARAM_REF);				

			//get variable property again with new item ID
			pvtProperty->clear();
			(void) m_pDevice->ReadParameterData2( pvtProperty, sProperty, ulBlockItemId, ulOccurrence, ulRetItemId, ulMemberId );
		}
		else if (iReturnValue == BLTIN_SUCCESS)
		{
			ulRetItemId = ulItemId;
		}
	}

	return ulRetItemId;
}

/*******************************************************************************************************
 * This function returns operative reference PARAM_REF value.
 *
 * Inputs:
 *	ulBlockItemId is block item ID used for cross block
 *	ulOccurrence is block occurence number used for cross block
 *	ulItemId: a given item ID or reference proxy
 *  pOpRef: a pointer to a PARAM_REF instance associated with the given input
 * Output:
 *	BLTIN_* error code
 ******************************************************************************************************/
int CBuiltIn::GetPARAM_REF(const ITEM_ID ulBlockItemId, const unsigned long ulOccurrence, ITEM_ID ulItemId, ITEM_ID ulMemberId, /*out*/PARAM_REF *pOpRef)
{
	//check if it is device variable reference proxy
	if ((ulItemId & REF_PROXY_MASK) == REF_PROXY_MASK)
	{
		//this is a device variable reference proxy
		PARAM_REF *pRefProxyOpRef = m_pMeth->GetGlobalTokenOperative(ulItemId);
		if (pOpRef)
		{
			//copy content from global symbol table
			*pOpRef = *pRefProxyOpRef;
		}
	}
	else
	{
		CValueVarient vtItemType{};
		CStdString sProperty;

		//get item type
		sProperty.Format(DCI_PROPERTY_KIND);
		ITEM_ID	ulNewItemId = VerifyItemIdNGetProperty(ulBlockItemId, ulOccurrence, ulItemId, ulMemberId, sProperty, &vtItemType);
		if (ulNewItemId != 0)
		{
			
			itemType_e eItemType = m_pDevice->GetItemType(CV_I4(&vtItemType));
			
			switch (eItemType)
			{
			case iT_File:
			case iT_ItemArray:
			case iT_Collection:
			case iT_VariableList:
				{	
					//it's an indirection DD item
					/* resolve indirection item ID with the given item ID and member ID */
					CValueVarient vtIndirItemId{};
					(void)m_pDevice->ReadParameterData2(&vtIndirItemId, _T("ItemID"), ulBlockItemId, ulOccurrence, ulNewItemId, ulMemberId);
					
					ulItemId = CV_UI4(&vtIndirItemId);
					ulMemberId = 0;
				
					// get item type again
					vtItemType.clear();
					(void)m_pDevice->ReadParameterData2(&vtItemType, DCI_PROPERTY_KIND, ulBlockItemId, ulOccurrence, ulItemId, 0);
					
					eItemType = m_pDevice->GetItemType(CV_I4(&vtItemType));

					pOpRef->addOperationalID(ulItemId, ulMemberId, eItemType);
				}
				break;
			case iT_Array:
			case iT_List:
				if ( m_pMeth->GetStartedProtocol() != nsEDDEngine::FF )
				{
					pOpRef->addOperationalID(ulNewItemId, ulMemberId+1, eItemType);
				}
				else
				{
					pOpRef->addOperationalID(ulNewItemId, ulMemberId, eItemType);
				}
				break;
			default:
				pOpRef->addOperationalID(ulNewItemId, ulMemberId, eItemType);
				break;
			}

			pOpRef->addBlockID(ulBlockItemId, ulOccurrence);
		}
	}

	if (pOpRef->isOperationalIDValid())
	{
		return BLTIN_SUCCESS;
	}
	else
	{
		return BLTIN_VAR_NOT_FOUND;
	}
}
void CBuiltIn::MethodDebugMessage(unsigned long ulLineNo)
{
	if (m_pDevice)
	{
		const wchar_t *pModifiedMethodDebugXml = NULL;
		wchar_t *pCurrentDebugInfoXml = NULL;

		m_pInterpreter->GetLocalVariables(&pCurrentDebugInfoXml);

		m_pDevice->deviceMethodDebugMessage(ulLineNo, pCurrentDebugInfoXml, &pModifiedMethodDebugXml);

		if (pCurrentDebugInfoXml)
		{
			delete[] pCurrentDebugInfoXml;
			pCurrentDebugInfoXml = NULL;
		}

		if (pModifiedMethodDebugXml)
		{
			delete[] pModifiedMethodDebugXml;
			pModifiedMethodDebugXml = NULL;
		}

	}
}