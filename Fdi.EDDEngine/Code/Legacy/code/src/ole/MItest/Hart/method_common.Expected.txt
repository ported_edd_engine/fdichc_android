﻿<Method>
{
 int  local_integer1;
 int  local_integer2;
 long  select;

 ACKNOWLEDGE(literal_string(xxx));
 remove_all_abort();
 local_integer1 = _ListInsert(16587,2,16586);
 local_integer1 = _ListInsert(16592,0,16526);
 _MenuDisplay(16533,literal_string(xxx),literal_string(xxx));
 discard_on_exit();
 ACKNOWLEDGE(literal_string(xxx));
}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: All abort methods will be removed
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "All abort methods will be removed"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: All abort methods will be removed
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: remove_all_abort
  Number Of Input Parameters: 0
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function remove_all_abort
IBuiltinSupport ListInsert() is being called
IBuiltinSupport ListInsert with returned value 0 () is returned
Function Name: _ListInsert
  Number Of Input Parameters: 3
    Param 1 Type: RUL_SHORT
    Param 1 Value: 16587
    Param 2 Type: RUL_CHAR
    Param 2 Value: 2
    Param 3 Type: RUL_SHORT
    Param 3 Value: 16586
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _ListInsert
IBuiltinSupport ListInsert() is being called
IBuiltinSupport ListInsert with returned value 0 () is returned
Function Name: _ListInsert
  Number Of Input Parameters: 3
    Param 1 Type: RUL_SHORT
    Param 1 Value: 16592
    Param 2 Type: RUL_CHAR
    Param 2 Value: 0
    Param 3 Type: RUL_SHORT
    Param 3 Value: 16526
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _ListInsert
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: FINISH
End of Function literal_string
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: select 
End of Function literal_string
IBuiltinSupport UIDRequest() is being called
UI Builtin Selection List: "FINISH"
IBuiltinSupport UIDRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: _MenuDisplay
  Number Of Input Parameters: 3
    Param 1 Type: RUL_SHORT
    Param 1 Value: 16533
    Param 2 Type: RUL_DD_STRING
    Param 2 Value: FINISH
    Param 3 Type: RUL_DD_STRING
    Param 3 Value: select 
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _MenuDisplay
Function Name: discard_on_exit
  Number Of Input Parameters: 0
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function discard_on_exit
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: The method_common method is finished, and the value should not be updated on the page upon completion of the method.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "The method_common method is finished, and the value should not be updated on the page upon completion of the method."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: The method_common method is finished, and the value should not be updated on the page upon completion of the method.
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><local_integer1>0,</local_integer1><local_integer2>0,</local_integer2><select>0,</select></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
