/******************************************************************************
 *  @(#) $Id: stdinc.h,v 1.22 1996/08/07 16:04:07 kimwolk Exp $
 *
 *  Copyright 1994 Rosemount, Inc.- All rights reserved
 *
 *  Description : Include file for the method interpreter include files, that
 *                are used frequently, but are changed infrequently  
 *
 ******************************************************************************/

/******************************************************************************
 *
 * Standard C header files
 *
 ******************************************************************************/

#pragma once

#ifndef __STDINC_H_
#define __STDINC_H_

#if 0
#include <amsversion.h>

#include <afx.h>                     /* MFC core and standard      */
#include <afxwin.h>                  /* MFC core and standard      */
#include <afxext.h>                  /* MFC extensions             */
#include <afxole.h>                  /* MFC OLE classes            */
#include <windows.h>
#include <windowsx.h>                /* Windows Memory             */
#include <atlbase.h>
#include <atlCOM.h>
#endif
#include <malloc.h>

#if 0
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#endif

#include <cstdarg>
#include <cstdio>
#include <string>
#include <cctype>
#include <wchar.h>
#include <cstddef>
#include <limits>
#ifdef __ANDROID__
#include <sys/time.h>
#else
#include <sys/timeb.h>
#endif
#include <cerrno>
#include <sys/stat.h>
#include <cmath>
#include <cfloat>
#include <ServerProjects/ServerRes.h>



/******************************************************************************
 *
 * Operating System dependent header files
 *
 ******************************************************************************/

#if 0
#include <winnetwk.h>
#include <winuser.h>
#endif
#include <fstream>

#include "std.h"
#include "od_defs.h"
#include "ddldefs.h"
#include "dds_upcl.h"


#ifdef _DEBUG 
//#define new DEBUG_NEW //TODO fixit Debug_new itself not defined
#ifndef DEBUG_NEW
   #define DEBUG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
   #define new DEBUG_NEW
#endif
#endif 
#include "cm_loc.h"


 /******************************************************************************
 *
 * Operating System dependent header files
 *
 ******************************************************************************/
#include "PlatformCommon.h"
#include "stdstring.h"

#endif // __STDINC_H
/******************************************************************************/

