/**
 *	@(#) $Id: dds_upcl.h,v 1.3 2012/08/22 22:44:40 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	dds_upcl.h - Connection Manager Library Definitions
 *
 *	This file contains all interface definitions for
 *	the Connection Manager Library.
 */

#ifndef DDS_UPCL_H
#define DDS_UPCL_H

#include "std.h"
#include "cm_dds.h"

#include "attrs.h"

typedef int		DEVICE_TYPE_HANDLE;
typedef int		BLOCK_HANDLE;
typedef int		DEVICE_HANDLE;
typedef int		ROD_HANDLE;

typedef UINT8		BOOLEAN;



typedef struct	tag_OD_DESCRIPTION_SPECIFIC {
	OBJECT_INDEX	sod_first_index;
	UINT16			sod_object_count;
} OD_DESCRIPTION_SPECIFIC;

typedef struct tag_DOMAIN_SPECIFIC {
	UINT32			size;
	UINT32			local_address;
} DOMAIN_SPECIFIC;

typedef struct tag_DDEF_DESC_EXT {
	nsEDDEngine::LayoutType layout;
	wchar_t* manufacturer_extended;
	wchar_t* device_extended;
} DDEF_DESC_EXT;

typedef struct tag_DDEF_DESC {
	UINT32 ext_byte_count;
	UINT32 item_count;
	UINT16 block_count;
	UINT32 first_item_index;
	DDEF_DESC_EXT desc_ext;
} DDEF_DESC;

typedef struct tag_OBJECT
{
	UINT32 index;
	UINT8 object_code;
	UINT32 ext_length;
	UINT8 *extension;
} OBJECT;

#define OBJECT_FIXED_SIZE 5

// FDI object codes
#define FDI_OBJECT_CODE_DDEF_DESC 1
#define FDI_OBJECT_CODE_DEVICE_TABLE_SET 2
#define FDI_OBJECT_CODE_BLOCK_TABLE_SET 3
#define FDI_OBJECT_CODE_DD_ITEMS 4

typedef struct tag_BINARY_LANGUAGE_IMAGE {
    STRING		  language_code;
	unsigned long length;
    unsigned char *data;
} BINARY_LANGUAGE_IMAGE;	// MHD Get rid of this

extern int ds_get_header(FILE *file,wchar_t *filename,  DDOD_HEADER *header);

#endif	/* DDS_UPCL_H */


