#include "stdinc.h"

#include "ManualUnitLookup.h"

#include "HART/HARTEDDEngine/DDSSupport.h"


// ManualUnitLookup_Lock constructor
ManualUnitLookup_Lock::ManualUnitLookup_Lock(ManualUnitLookup* pClassToLock)
{
	m_pClassToLock = pClassToLock;

	m_pClassToLock->m_cs.lock();
}

// ManualUnitLookup_Lock destructor
ManualUnitLookup_Lock::~ManualUnitLookup_Lock()
{
	m_pClassToLock->m_cs.unlock();
}



// ManualUnitLookup constructor
ManualUnitLookup::ManualUnitLookup()
{
	

	// Other class members have their own constructors
}


// ManualUnitLookup destructor
ManualUnitLookup::~ManualUnitLookup()
{
	UNITS_VECTOR::iterator Iter;

	// Clean out the vecStaticUnits vector
	for (	Iter  = vecStaticUnits.begin();
			Iter != vecStaticUnits.end();
			Iter  = vecStaticUnits.begin()	)
	{
		DDI_GENERIC_ITEM *gi_unit = *Iter;
		ddi_clean_item(gi_unit);
		free(gi_unit);

		vecStaticUnits.erase(Iter);
	}

	// Clean out the vecConditionalUnits vector
	for (	Iter  = vecConditionalUnits.begin();
			Iter != vecConditionalUnits.end();
			Iter  = vecConditionalUnits.begin()	)
	{
		DDI_GENERIC_ITEM *gi_unit = *Iter;
		ddi_clean_item(gi_unit);
		free(gi_unit);

		vecConditionalUnits.erase(Iter);
	}

	
}


int ManualUnitLookup::Init(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Create the vecStaticUnits and vecConditionalUnits vectors

	// Foreach UNIT in this EDD
	// call ddi_get_item to get the definition
	// store the FLAT_UNITs into one of two vectors
	//   vecStaticUnits - for UNITs that are statically defined
	//   vecConditionalUnits - for UNITs that have conditionals in them

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr* pConnMgr = pDDSSupport->GetConnectionManager();

	BLOCK_HANDLE block_handle = 0;

	int rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	FLAT_DEVICE_DIR	*flat_device_dir = nullptr;

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}

	ITEM_TBL *it = &flat_device_dir->item_tbl;

	for (int i = 0; i < it->count; i++)
	{
		if (it->list[i].item_type == UNIT_ITYPE)	// If this is a UNIT
		{
			DDI_ITEM_SPECIFIER is_unit = {0};
			is_unit.type = DDI_ITEM_ID;
			is_unit.item.id = it->list[i].item_id;

			DDI_GENERIC_ITEM *gi_unit = (DDI_GENERIC_ITEM *)calloc(1, sizeof(DDI_GENERIC_ITEM));

			// Get this UNITs definition
			rc = ddi_get_item(block_spec, &is_unit, env_info, UNIT_ITEMS, gi_unit);
			if (rc != SUCCESS) {
				return(rc) ;
			}

			FLAT_UNIT *pUnit = (FLAT_UNIT *)gi_unit->item;

			if (pUnit->masks.dynamic == 0)	// This UNIT is statically defined
			{
				vecStaticUnits.push_back(gi_unit);
			}
			else
			{								// This UNIT has conditional references in it
				vecConditionalUnits.push_back(gi_unit);
			}
		}
	}

	return rc;
}


int ManualUnitLookup::FindUnitRel(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitFromTbl, DDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	int rc = DDL_SUCCESS;

	if (param_spec->type == DDI_PS_ITEM_ID)
	{
		ITEM_ID paramID = param_spec->item.id;

		// Look up this param in the map to see if we have it cached already.
		UNITS_MAP::const_iterator cIter = mapUnitLookup.find(paramID);

		if (cIter == mapUnitLookup.cend())	// This param is not found, process it and return the result
		{
			rc = FindForFirstTime(env_info, block_spec, unitFromTbl, paramID, pUnitID);
		}
		else	// The units for this param have been cached...
		{
			if (cIter->second.etype == CachedUnit::StaticUnit)	// The cached unit was static
			{
				// This param is found and we have an ITEM_ID already stored.
				*pUnitID = cIter->second.UnitID;
			}
			else if (cIter->second.etype == CachedUnit::ConditionalUnit)	// The cached unit was conditional
			{
				// This param is found, but its UNITs are not static.
				// Look in the vecConditionalUnits vector, using the cached UnitID as the best guess
				rc = FindInConditionalVector(env_info, block_spec, cIter->second.UnitID, paramID, pUnitID);
			}
			else
			{
				// This param is found, but it's UNITs were not found the last time.
				// Look in the vecConditionalUnits vector, using the Table Units as the best guess
				rc = FindInConditionalVector(env_info, block_spec, unitFromTbl, paramID, pUnitID);
			}
		}
	}
	else
	{
		rc = DDI_INVALID_PARAM;	// This param_spec->type cannot be processed
	}

	return rc;
}


int ManualUnitLookup::FindForFirstTime(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitFromTbl, ITEM_ID paramID, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Search for the UNIT relation in this order
	//	- Use the unitFromTbl relation, if it works and is in varStaticUnits
	//	- Search varStaticUnits, use the first one that works
	//	- Use the unitFromTbl relation, if it works and is in varConditionalUnits
	//	- Search varConditionalUnits, use the first one that works

	// If the UNIT relation is from the varStaticUnits vector, cache it and mark as "StaticUnit".
	// If the UNIT relation is from the varConditionalUnits vector, cache it and mark as
	//		"ConditionalUnit". It will need to be verified next time.
	// If a UNIT relation is not found, cache a zero and mark as "NoUnit". We will look in
	//      varConditionalUnits next time. The conditions might be better and we might
	//      find a match.

	UNITS_VECTOR::const_iterator cIter;

	//
	// Look first in the vecStaticUnits for the "unitFromTbl" relation
	//
	for (cIter = vecStaticUnits.cbegin(); cIter != vecStaticUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		FLAT_UNIT *pUnit = (FLAT_UNIT *)gi_unit->item;

		// Looking only for the UNIT mentioned in the Relation table
		   //////////////////////////
		if (unitFromTbl == pUnit->id)
		{ 
			if (IsMyUnit(pUnit, paramID))	// Found it.
			{								// Cache this value for next time
				mapUnitLookup[paramID] = CachedUnit ( CachedUnit::StaticUnit, pUnit->id );

				*pUnitID = pUnit->id;
				return DDL_SUCCESS;
			}
		}
	}

	//
	// If not found, look in the vecStaticUnits vector for any relation that works.
	//
	for (cIter = vecStaticUnits.cbegin(); cIter != vecStaticUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		FLAT_UNIT *pUnit = (FLAT_UNIT *)gi_unit->item;

		if (unitFromTbl == pUnit->id )
		{
			continue;		// We already checked this unit, above
		}

		if (IsMyUnit(pUnit, paramID))	// Found it.
		{								// Cache this value for next time
			mapUnitLookup[paramID] = CachedUnit ( CachedUnit::StaticUnit, pUnit->id );

			*pUnitID = pUnit->id;
			return DDL_SUCCESS;
		}
	}

	//
	// If still not found, look in the varConditionalUnits vector.
	//
	int rc = FindInConditionalVector(env_info, block_spec, unitFromTbl, paramID, pUnitID);

	return rc;
}


int ManualUnitLookup::FindInConditionalVector(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitBestGuess, ITEM_ID paramID, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Foreach FLAT_UNIT in the vecConditionalUnits vector
	// Call ddi_get_item() to get the current definition.
	// Look to see if this paramID is in this FLAT_UNIT
	// If so, return it
	// if not, continue to the next one in the vector

	// If not found, return an error
	int rc = DDL_SUCCESS;

	UNITS_VECTOR::const_iterator cIter;

	//
	// Look first in the vecConditionalUnits for the "unitBestGuess" relation
	//
	for (cIter = vecConditionalUnits.cbegin(); cIter != vecConditionalUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		FLAT_UNIT *pUnit = (FLAT_UNIT *)gi_unit->item;

		// Looking only for the UNIT that is our Best Guess
		    //////////////////////////
		if (unitBestGuess == pUnit->id)
		{
			DDI_ITEM_SPECIFIER is_unit = {0};
			is_unit.type = DDI_ITEM_ID;
			is_unit.item.id = pUnit->id;

			rc = ddi_get_item(block_spec, &is_unit, env_info, UNIT_ITEMS, gi_unit);
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (IsMyUnit(pUnit, paramID))	// Found it.
			{								// Cache this value for next time
				mapUnitLookup[paramID] = CachedUnit ( CachedUnit::ConditionalUnit, pUnit->id );

				*pUnitID = pUnit->id;
				return DDL_SUCCESS;
			}
		}
	}

	//
	// Next look in the vecConditionalUnits vector for any relation that works.
	//
	for (cIter = vecConditionalUnits.cbegin(); cIter != vecConditionalUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		FLAT_UNIT *pUnit = (FLAT_UNIT *)gi_unit->item;

		if (unitBestGuess == pUnit->id )
		{
			continue;		// We already checked this unit, above
		}

		DDI_ITEM_SPECIFIER is_unit = {0};
		is_unit.type = DDI_ITEM_ID;
		is_unit.item.id = pUnit->id;

		rc = ddi_get_item(block_spec, &is_unit, env_info, UNIT_ITEMS, gi_unit);
		if (rc != SUCCESS) {
			return(rc) ;
		}

		if (IsMyUnit(pUnit, paramID))	// Found it.
		{								// Cache this value for next time
			mapUnitLookup[paramID] = CachedUnit ( CachedUnit::ConditionalUnit, pUnit->id );

			*pUnitID = pUnit->id;
			return DDL_SUCCESS;
		}
	}

	// No FLAT_UNIT found, so cache a zero and mark with "NoUnit"
	mapUnitLookup[paramID] = CachedUnit ( CachedUnit::NoUnit, 0 );

	// Not found, so return an error
	return DDI_TAB_NO_UNIT;
}


bool
ManualUnitLookup::IsMyUnit(FLAT_UNIT *pUnit, ITEM_ID paramID)
{
	// Look in this FLAT_UNIT to see if this paramID is on the right side

    for (int i=0; i < pUnit->items.var_units.count; i++)
    {
		if ( pUnit->items.var_units.list[i].op_ref_type == STANDARD_TYPE )
		{
			if ((pUnit->items.var_units.list[i].desc_id == paramID) ||
				(pUnit->items.var_units.list[i].op_info.id == paramID))
			{
				/* We have found the matching unit relation */
				return true;
			}
		}
		else
		{
			for( int j = 0; j < pUnit->items.var_units.list[i].op_info_list.count; j++ )
			{
				if ((pUnit->items.var_units.list[i].desc_id == paramID) ||
					(pUnit->items.var_units.list[i].op_info_list.list[j].id == paramID))
				{
					/* We have found the matching unit relation */
					return true;
				}
			}
		}
    }

	return false;
}
