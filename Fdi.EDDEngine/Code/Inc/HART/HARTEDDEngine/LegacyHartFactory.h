#pragma once

#include "EDDEngineImpl.h"


#if defined (EXPORT_LEGACY_HART_API)
	#define __LEGACY_HART_API __declspec(dllexport)
#elif defined (__GNUC__)
    #define __LEGACY_HART_API __attribute__((dllexport))
#else
	#define __LEGACY_HART_API __declspec(dllimport)
#endif


class __LEGACY_HART_API LegacyHartFactory
{
public: 
	static CEDDEngineImpl* CreateLegacyHartDDS(const wchar_t *sEDDBinaryFilename, 
	const wchar_t* pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode, UINT32 manufacturer, UINT16 device_type, UINT8 device_revision);
private:
	LegacyHartFactory& operator=(const LegacyHartFactory &rhs);
};
