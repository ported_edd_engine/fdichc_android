/*************************************************************************************************
 *
 * $Workfile: logging.h$
 *
 * Description:
 *		logging.h : external interface to the error logging functions
 *
 * #include "logging.h"
 *
 */

//#pragma warning(disable: 4996) //WHS (EmersonProcess) Disable the VS2005 security warning 
#pragma once

#include <mutex>
#include "memory"
#include "typedefs.h"
#ifdef INC_DEBUG
#pragma message("In logging.h") 
#endif


#ifdef INC_DEBUG
#pragma message("    Finished Includes::logging.h") 
#endif

/************************************************************************************************/
/*  Logging channels:   use channel | channel... to log to multiple destinations
/************************************************************************************************/

#define CERR_LOG	1
#define CLOG_LOG	2
#define COUT_LOG	4
#define UI_LOG		8
#define STAT_LOG   16

/************************************************************************************************/

#define LOGIT	(MI_LOG::getMILOGInstance()->logout)
/* Usage:
 * LOGIT(CERR_LOG | CLOG_LOG, "ERROR: Failed to find %d in %s.\n", 42, "The answer");
 */
/* Args requires
 *	'O' , "outputFileName"
 *	'E' , "errorFileName"
 *  'G' , "logFileName"
 *
 * if any are the empty string then that log path will go into the bit bucket.
 */

#if defined(_DEBUG)
#if defined(__GNUC__)
    #define DEBUGLOG
#else
    #define DEBUGLOG	(MI_LOG::getMILOGInstance()->logout)
#endif
#else
#if defined(__GNUC__)
    #define DEBUGLOG
#else
    #define DEBUGLOG	(__noop)
#endif
#endif

#define SDCLOG      (MI_LOG::getMILOGInstance()->logout)

#define LOG_BUF_SIZE	2048	/* gotta be big for methods */
#define LOGFILE_SIZE	266

class MI_LOG
{
public:
	~MI_LOG();
	static MI_LOG* getMILOGInstance();
	int logout(int channel, char* format, ...);
	char * Space (int m);		// Return a static buffer with "m" spaces in it.

private:
	MI_LOG();
	bool IsMiLoggingEnabled();

	std::mutex m_MILOG_cs;
    char g_szLogFileName[LOGFILE_SIZE]{0};
};
