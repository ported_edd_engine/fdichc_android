#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <wtypes.h>


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_request_a_plugin_Element_with_PB
        {
        public: 
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FLAT_PLUGIN* test_uiplugin_1;
            static  FDI_GENERIC_ITEM* genericItem;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0006.DDL.bin");                
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_uiplugin_1_specifier;
                engine->FillParamSpecifier("test_plugin", &test_uiplugin_1_specifier);
                genericItem = engine->GetGenericItem(
                    test_uiplugin_1_specifier.id, 
                    nsEDDEngine::ITYPE_PLUGIN);

                test_uiplugin_1 = dynamic_cast<FLAT_PLUGIN*>(genericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_the_label_string_uip1label_for_uiplugin_1()
            {
                Assert::AreEqual("Test Plugin Label", gcnew String(test_uiplugin_1->label.c_str()));
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_the_help_string_uip1help_for_uiplugin_1()
            {
                Assert::AreEqual("Test Plugin Help", gcnew String(test_uiplugin_1->help.c_str()));
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_for_uiplugin_1()
            {
                Assert::AreEqual(0x1EE6C3CEu, test_uiplugin_1->uuid.Data1);
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_two_for_uiplugin_1()
            {
                Assert::AreEqual(0xF9C1u,(UINT32) test_uiplugin_1->uuid.Data2);
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_three_for_uiplugin_1()
            {
                Assert::AreEqual(0x4EC5u,(UINT32) test_uiplugin_1->uuid.Data3);
            }

            [TestMethod, TestCategory("UiPluginTest")]
            void It_should_return_correct_uuid_part_four_for_uiplugin_1()
            {
                Assert::AreEqual((byte)0x8C, test_uiplugin_1->uuid.Data4[0]);
            }
        };

    }
}
