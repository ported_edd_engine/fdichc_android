#include "stdafx.h"
#include "BuiltIn.h"
#include "ddbdefs.h"
#include <time.h>


// most of the uses of this should be optimized out by the compiler
#define make_time_t( a)    ( (long)   (a) )	/* returns Htime_t, a long */
#define get_time( b)       ( (time_t) (b) )	/* returns time_t, a int or int64 */

/****************************Date Time Builtins (eDDL)*******************/

//int CHart_Builtins::is_leap_year(int year)// WS 25jun07 - no longer required.
//{
//	return (year%4 == 0 && year%100 != 0) || year%400 == 0;
//}


//convert long DATE type to struct tm type
//if there is error in input, this function returns -1. Otherwise, this function returns 0
int CBuiltIn::get_tm(struct tm *pstTM, long date)
{
	pstTM->tm_wday = 0;
	pstTM->tm_yday = 0;
	pstTM->tm_year =  date  &  0x0000FF;
	pstTM->tm_mon =   ((date & 0x00FF00) >> 8) - 1;
	pstTM->tm_mday =  (date & 0xFF0000) >> 16;
	pstTM->tm_hour = 0;
	pstTM->tm_min = 0;
	pstTM->tm_sec = 0;
	pstTM->tm_isdst = -1;	//the C run-time library code compute whether standard time or daylight saving time is in effect.

	//check input limits
	if (pstTM->tm_mon < 0)
	{
		return BI_ERROR;
	}
	else if (pstTM->tm_mon > 11)
	{
		return BI_ERROR;
	}
	if (pstTM->tm_mday < 1)
	{
		return BI_ERROR;
	}
	else if (pstTM->tm_mday > 31)
	{
		return BI_ERROR;
	}

	(void)mktime(pstTM);
	return BI_SUCCESS;
}	

//convert unsinged long long DATE_AND_TIME type to struct tm type
//if there is error in input, this function returns -1. Otherwise, this function returns 0
int CBuiltIn::get_tm_from_date_and_time(struct tm *pstTM, UINT64 dateTime)
{
	pstTM->tm_year =  dateTime  &  0xFF;
	pstTM->tm_mon =   ((dateTime >> 8) & 0x0F) - 1;
	pstTM->tm_mday =  ((dateTime >> 16) & 0x1F);
	int wday = ((dateTime >> 21) & 0x07);
	if (wday == 7)
	{
		wday = 0;
	}
	pstTM->tm_wday = wday;				//0~6 are Sunday~Saturday
	pstTM->tm_yday = 0;
	pstTM->tm_hour = ((dateTime >> 24) & 0x1F);
	pstTM->tm_min = ((dateTime >> 32) & 0x3F);
	pstTM->tm_sec = ((dateTime >> 40) & 0xFFFF) / 1000;
	//1: daylight saving time is in effect. 0: daylight saving time is not in effect.
	pstTM->tm_isdst = ((dateTime >> 31) & 0x1);

	//check input limits
	if (pstTM->tm_mon < 0)
	{
		return BI_ERROR;
	}
	else if (pstTM->tm_mon > 11)
	{
		return BI_ERROR;
	}
	if (pstTM->tm_mday < 1)
	{
		return BI_ERROR;
	}
	else if (pstTM->tm_mday > 31)
	{
		return BI_ERROR;
	}

	(void)mktime(pstTM);
	return BI_SUCCESS;
}

//convert unsinged long long TIME_VALUE  type to struct tm type
//if there is error in input, this function returns -1. Otherwise, this function returns 0
int CBuiltIn::get_tm_from_time_value(struct tm *pstTM, INT64 time_value)
{
	//struct tm StDateNTime;	
	
	pstTM->tm_wday = 0;
	pstTM->tm_yday = 0;
	pstTM->tm_year =  72;	//Jan. 1, 1972
	pstTM->tm_mon =   0;
	pstTM->tm_mday =  1;
	pstTM->tm_hour = 0;
	pstTM->tm_min = 0;
	pstTM->tm_sec = (int)((time_value)/32000);
	pstTM->tm_isdst = -1;	//daylight saving time is unknown
	//update pstTM in stantdard format for display
	(void)mktime(pstTM);
	return BI_SUCCESS;

}

//convert unsigned long long TIME  type to struct tm type
//if there is error in input, this function returns -1. Otherwise, this function returns 0
int CBuiltIn::get_tm_from_time(struct tm *pstTM, unsigned long long time)
{	
	unsigned long ulSec	=	( ((time >> 16) & 0xFFFFFFFF) ) / 1000;
	unsigned long ulDay = (time  &  0xFFFF);

	pstTM->tm_wday =  0;
	pstTM->tm_yday =  0;
	pstTM->tm_year =  84;						//Jan. 1, 1984
	pstTM->tm_mon  =  0;
	pstTM->tm_mday =  ulDay + 1;
	pstTM->tm_hour =  ( ulSec/(60*60) ) % 24;
	pstTM->tm_min  =  (ulSec/60) % 60;
	pstTM->tm_sec  =  (ulSec) - (pstTM->tm_hour * 60 * 60) - (pstTM->tm_min * 60);

	//daylight saving time is unknown
	pstTM->tm_isdst = -1;											

	//update pstTM in stantdard format for display
	(void)mktime(pstTM);
	return BI_SUCCESS;

}

int CBuiltIn::split_duration(UINT64 time_value, _DURATION  &duration)
{
	
	unsigned long long ulMSec	=	((time_value >> 16) & 0xFFFFFFFF);
	
	duration.iMinutes		=	(ulMSec/(1000*60)) % 60;
	duration.iHours			=	(ulMSec/(1000*60*60)) %24;
	duration.iDays			=	(time_value & 0xFFFF);
	duration.dSeconds		=	(ulMSec / 1000.00) - (duration.iHours * 60 * 60) - (duration.iMinutes * 60);

	return BI_SUCCESS;

}

int CBuiltIn::Date_to_Year( long hart_date )
{
	int nYear = hart_date & 0xFF;
	nYear += 1900;
	return nYear;
}

int CBuiltIn::Date_to_Month( long hart_date )
{
	int nMonth = ((hart_date >> 8) & 0xFF);
	return nMonth;
}

int CBuiltIn::Date_to_DayOfMonth( long hart_date )
{
	int nDay = (hart_date >> 16) & 0xFF;
	return nDay;
}

// This function assumes the inputs are DATE types in 4 bytes.
// | days of month ([1,31] in 2 bytes) | month ([1,12] in 1 byte) | year ([0, 255] in 1 byte) |
long CBuiltIn::DATE_to_days( long date1, long date0 )
{
	struct tm StDateNTime;
	if (get_tm(&StDateNTime, date1) == BI_ERROR)
	{
		return BI_ERROR;
	}

	time_t time1 = mktime( &StDateNTime);

	struct tm StDateNTime0;	
	if (get_tm(&StDateNTime0, date0) == BI_ERROR)
	{
		return BI_ERROR;
	}

	time_t time0 = mktime( &StDateNTime0);

	double dDiffTime = difftime(time1, time0);	//in seconds

	long lDiffTime = (long)(dDiffTime/(24*60*60));	//in days
	return(lDiffTime);
}

// This function assumes the second input is DATE types in 4 bytes.
// | days of month ([1,31] in 2 bytes) | month ([1,12] in 1 byte) | year ([0, 255] in 1 byte) |
long CBuiltIn::days_to_DATE( long days, long date0 )
{
	struct tm StDateNTime;	
	if (get_tm(&StDateNTime, date0) == BI_ERROR)
	{
		return BI_ERROR;
	}
	StDateNTime.tm_mday += days;

	//update tm in stantdard format
	(void)mktime(&StDateNTime);

	long futureDate = ((StDateNTime.tm_mday << 16) & 0xFFFF0000) | (((StDateNTime.tm_mon + 1) << 8) & 0xFF00) | (StDateNTime.tm_year & 0xFF);
	return futureDate;
}

//This builtin creates a time_t value from the DATE and TIME_VALUE
long CBuiltIn::From_DATE_AND_TIME_VALUE( long date, unsigned long time_value )
{
	struct tm timeinfo;
	if (get_tm(&timeinfo, date) == BI_ERROR)
	{
		return BI_ERROR;
	}
	// Fill tm structure  
	int sec = (int)((double)time_value / 32000);
	timeinfo.tm_sec += sec;

	// call mktime to create time_t type  
	time_t returnedVal = mktime (&timeinfo);
	return make_time_t( returnedVal );
}

//These two builtins create a time_t from the TIME_VALUE
//Input TIME_VALUE(4) value is a time of day 
long CBuiltIn::From_TIME_VALUE( unsigned long time_value )
{
	time_t returnedVal;

	//get current time
	time( &returnedVal );
	// Convert to local time
	struct tm *newtime = localtime(&returnedVal);
	// add UTC offset
	returnedVal = mktime(newtime) + get_utc_offset_inSec();

	returnedVal += (int)((double)time_value / 32000);

	return make_time_t( returnedVal);
}

//Input TIME_VALUE(8) value is an absolute time in 1/32 ms since January 1, 1972.
long CBuiltIn::From_TIME_VALUE( unsigned long long time_value )
{
	struct tm StDateNTime;

	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;
	StDateNTime.tm_year =  72;	//Jan. 1, 1972
	StDateNTime.tm_mon =   0;
	StDateNTime.tm_mday =  1;
	StDateNTime.tm_hour = 0;
	StDateNTime.tm_min = 0;
	StDateNTime.tm_sec = (int)((double)time_value/32000);
	StDateNTime.tm_isdst = -1;	//daylight saving time is unknown

	//update StDateNTime in stantdard format
	time_t returnedVal = mktime(&StDateNTime);
	return make_time_t( returnedVal );
}

// This builtin returns time_t which is value in the number of seconds since midnight in January 1
long CBuiltIn::GetCurrentDate()
{
	time_t		ltime;		//long long

	time( &ltime );

	struct tm *tm_value = localtime(&ltime);
	
	return (tm_value->tm_yday * 24 * 60 *60);
}

// This builtin returns time_t which is value in the number of seconds since midnight
long CBuiltIn::GetMICurrentTime()
{
	time_t		ltime;		//long long

	time( &ltime );

	struct tm *tm_value = localtime(&ltime);
	
	return ((tm_value->tm_hour * 60 + tm_value->tm_min) * 60 + tm_value->tm_sec);
}

// This builtin returns time_t which is value in the number of seconds since Jan. 1, 1970
long CBuiltIn::GetCurrentDateAndTime()
{
	time_t        ltime;        //long long

	time(&ltime);
	//convert to local time
	struct tm *newtime = localtime(&ltime);
	
	// Get UTC offset with current local time and add
	ltime = mktime(newtime) + get_utc_offset_inSec();

	return make_time_t(ltime);
}

// This function returns UTC offset with local time in seconds.
long CBuiltIn::get_utc_offset_inSec()
{
	// Check if day light saving is in effect for the local time
	//////////////////////////////////////////////////////////////////
	bool bIsDst = false;
	time_t currtime;
	struct tm * timeinfo;
	time(&currtime);
	timeinfo = localtime(&currtime);
	time_t local = mktime(timeinfo);
	// Note that gmtime and localtime use a single buffer per thread for the conversion.
	// If you supply this buffer to mktime, _mktime32 or _mktime64, the previous contents are destroyed. 
	// So lets set the flag for 
	if (timeinfo->tm_isdst)
	{
		// Day light saving is in effect
		bIsDst = true;
	}
	//////////////////////////////////////////////////////////////////

	// get the local time for Jan 2, 1900 00:00 UTC 
	time_t zero = 24 * 60 * 60L;
	struct tm * timeptr;
	long gmtime_hours;
	timeptr = localtime(&zero);
	gmtime_hours = timeptr->tm_hour;

	// if the local time is the "day before" the UTC, subtract 24 hours
	// from the hours to get the UTC offset 
	if (timeptr->tm_mday < 2)
		gmtime_hours -= 24;

	if (bIsDst) // If Daylight saving is in effect, add 1 hour.
	{
		gmtime_hours += 1;
	}
	// convert hours and minutes into seconds.
	long lSeconds = ((gmtime_hours * 60) + timeptr->tm_min) * 60;
	
	return lSeconds;
}

_UINT64 CBuiltIn::To_Date_and_Time(int days, int hour, int minute, int second, int millisecond)
{
	struct tm StDateNTime;

	StDateNTime.tm_sec = second + millisecond / 1000;
	StDateNTime.tm_min = minute;
	StDateNTime.tm_hour = hour;
	if (days > 0)
	{
		StDateNTime.tm_mday = days;							//1-31 day of the month
	}
	else
	{
		StDateNTime.tm_mday = 1;							//1st day of the month
	}
	StDateNTime.tm_mon =   0;
	StDateNTime.tm_year =  70;							//since 1900
	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;							//days since January 1
	StDateNTime.tm_isdst = -1;							//daylight saving time is unknown

	//update StDateNTime in stantdard format.
	//mktime() ignores tm_wday and tm_yday as input. But it udpates tm_wday and tm_yday as output.
	time_t timet_val = mktime(&StDateNTime);
	if (timet_val == -1)
	{
		return 0;
	}
	StDateNTime.tm_year -= 70;

    _UINT64 returnVal = 0;

    returnVal = (_UINT64)StDateNTime.tm_year & 0x00000000000000ff;
    returnVal |= ((((_UINT64)StDateNTime.tm_mon + 1) << 8) & 0x0000000000000fff);
    returnVal |= (((_UINT64)StDateNTime.tm_mday << 16) & 0x00000000001fffff);
	int wday = StDateNTime.tm_wday;
	if (wday == 0)
	{
		wday = 7;
	}
    returnVal |= ((((_UINT64)wday) << 21) & 0x0000000000ffffff);	//1~7 are Monday~Sunday
    returnVal |= (((_UINT64)StDateNTime.tm_hour << 24) & 0x000000001fffffff);
	if (StDateNTime.tm_isdst > 0)
	{
		returnVal |= (1 << 31);		//daylight saving time
	}
	else
	{
		returnVal &= ~(1 << 31);
	}
    returnVal |= (((_UINT64)StDateNTime.tm_min << 32) & 0x0000003fffffffff);
    returnVal |= ((((_UINT64)StDateNTime.tm_sec * 1000) << 40) & 0x00ffffffffffffff);

	return (returnVal);
}


double CBuiltIn::DiffTime(long time_t1,long time_t0)
{// modified to decode 25jun07 - stevev
	time_t _t1 = get_time(time_t1);
	time_t _t0 = get_time(time_t0);
	return difftime( _t1, _t0 );
}
// WS 25jun07-changed 2nd param type- it is cast from numeric in Invoke
long CBuiltIn::AddTime(long time_t1, double dSeconds)
{// modified to decode/encode 25jun07 - stevev
	time_t _t1 = get_time(time_t1);
	return make_time_t(_t1 + dSeconds);
}

// Assuming month = [1, 12], dayOfMonth = [1, 31], hour = [0, 23]
long CBuiltIn::Make_Time( int year, int month, int dayOfMonth, int hour, int minute, int second, int isDST )
{
	struct tm StDateNTime;	
	
	//do some range checking
	if( year >= 1900 )
	{
		year = year - 1900;
	}
	if( year > 255 )
	{
		return BI_ERROR;
	}

	if( month > 0 )//assume that the users pass in Jan == 1 not Jan == 0
	{
		month--;
	}
	if( month > 11 )//No months are greater than Dec.
	{
		return BI_ERROR;
	}

	if( dayOfMonth < 1 )//require that the users pass in dayOfMonth = [1, 31]
	{
		return BI_ERROR;
	}
	else if( dayOfMonth > 31 )//No day of months > 31
	{
		return BI_ERROR;
	}
	
	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;
	StDateNTime.tm_year = year;
	StDateNTime.tm_mon = month;
	StDateNTime.tm_mday = dayOfMonth;
	StDateNTime.tm_hour = hour;
	StDateNTime.tm_min = minute;
	StDateNTime.tm_sec = second;
	StDateNTime.tm_isdst = isDST;
	
	return make_time_t( mktime( &StDateNTime) );// encode stevev 25jun07
	

}

long CBuiltIn::To_Time(long date, int hour, int minute, int second, int isDST)
{
	struct tm StDateNTime;	
	if (get_tm(&StDateNTime, date) == BI_ERROR)
	{
		return BI_ERROR;
	}

	StDateNTime.tm_hour += hour;
	StDateNTime.tm_min += minute;
	StDateNTime.tm_sec += second;
	StDateNTime.tm_isdst = isDST;

	return make_time_t( mktime( &StDateNTime) );// encode stevev 25jun07

}

// This builtin intputs value in DATE type
long CBuiltIn::Date_To_Time (long date)
{
	struct tm StDateNTime;	
	if (get_tm(&StDateNTime, date) == BI_ERROR)
	{
		return BI_ERROR;
	}

	return make_time_t( mktime( &StDateNTime) );// encode stevev 25jun07

}


// This builtin outputs value in DATE type
// Assuming month = [1, 12]
long CBuiltIn::To_Date(int year, int month, int dayOfMonth)
{
	long hart_date = 0;
	//do some range checking, being consistent with builtin Make_Time
	if( year >= 1900 )
	{
		year = year - 1900;
	}
	if( year > 255 )
	{
		return BI_ERROR;
	}

	if( month < 1 )//assume that the users pass in Jan == 1 not Jan = 0
	{
		return BI_ERROR;
	}
	else if( month > 12 )//No months are greater than Dec.
	{
		return BI_ERROR;
	}

	if (dayOfMonth < 1)
	{
		return BI_ERROR;
	}
	else if( dayOfMonth > 31 )//No day of months > 31
	{
		return BI_ERROR;
	}

	hart_date = year;
	hart_date |= (month << 8);
	hart_date |= (dayOfMonth << 16);

	return hart_date;
}

// This builtin inputs value in time_t type and outputs value in DATE type
long CBuiltIn::Time_To_Date(long time_t1)
{// stevev 25jun07 - decode time_t coming in
	time_t tempTime = get_time(time_t1);  // copy our data from a long into a time_t (which is defined as an __int64 in VS2005)
	struct tm *StDateNTime = gmtime( &tempTime );// WS - 9apr07 - 2005 checkin

	long hart_date = StDateNTime->tm_year;
	hart_date |= ((StDateNTime->tm_mon + 1) << 8);
	hart_date |= (StDateNTime->tm_mday << 16);

	return hart_date;
}

unsigned long CBuiltIn::seconds_to_TIME_VALUE(double seconds)
{
	return ((unsigned long)(seconds * 32000));
}
unsigned long long CBuiltIn::seconds_to_TIME_VALUE8(double seconds)
{
	return ((unsigned long long)(seconds * 32000));
}

double CBuiltIn::TIME_VALUE_to_seconds(double time_value)
{
	return (time_value / 32000);
}

int CBuiltIn::TIME_VALUE_to_Hour(double time_value)
{
	int returnVal = (int) (time_value / (60 * 60 * 32000));
	return (returnVal);
}

int CBuiltIn::TIME_VALUE_to_Minute(double time_value)
{
	int returnVal = (int) (time_value / (60 * 32000));
	return (returnVal);
}

int CBuiltIn::TIME_VALUE_to_Second(double time_value)
{
	int returnVal = (int) (time_value / 32000);
	return (returnVal);
}

int CBuiltIn::DATE_AND_TIME_VALUE_to_string(wchar_t* output_str, wchar_t* format, long date, unsigned long time_value)
{
	struct tm StDateNTime;	
	if (get_tm(&StDateNTime, date) == BI_ERROR)
	{
		return BI_ERROR;
	}
	
	StDateNTime.tm_sec = (time_value)/32000;

	//update tm in stantdard format for display
	(void)mktime(&StDateNTime);
	int returnVal = (int)wcsftime(output_str, MAX_DD_STRING, format, &StDateNTime);

	return (returnVal);
}

int CBuiltIn::DATE_to_string(wchar_t* output_str, wchar_t* format, long date)
{
	struct tm StDateNTime;	
	if (get_tm(&StDateNTime, date) == BI_ERROR)
	{
		return BI_ERROR;
	}

	int returnVal = (int)wcsftime(output_str, MAX_DD_STRING, format, &StDateNTime);

	return (returnVal);
}

//These two builtins create a time_t from the TIME_VALUE
//Input TIME_VALUE(4) value is a time of day 
int CBuiltIn::TIME_VALUE_to_string(wchar_t* time_value_str, wchar_t* format, unsigned long time_value)
{
	struct tm StDateNTime;	
	
	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;
	StDateNTime.tm_year = 72;	//Jan. 1, 1972
	StDateNTime.tm_mon  = 0;
	StDateNTime.tm_mday = 1;
	StDateNTime.tm_hour = 0;
	StDateNTime.tm_min  = 0;
	StDateNTime.tm_sec  = (time_value)/32000;
	StDateNTime.tm_isdst= -1;	//daylight saving time is unknown

	//update tm in stantdard format for display
	(void)mktime(&StDateNTime);
	int returnVal = (int)wcsftime(time_value_str, MAX_DD_STRING, format, &StDateNTime);

	return (returnVal);
}

//Input TIME_VALUE(8) value is an absolute time in 1/32 ms since January 1, 1972.
int CBuiltIn::TIME_VALUE_to_string(wchar_t* time_value_str, wchar_t* format, unsigned long long time_value)
{
	struct tm StDateNTime;	
	
	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;
	StDateNTime.tm_year =  72;	//Jan. 1, 1972
	StDateNTime.tm_mon =   0;
	StDateNTime.tm_mday =  1;
	StDateNTime.tm_hour = 0;
	StDateNTime.tm_min = 0;
	StDateNTime.tm_sec = (int)((double)time_value/32000);
	StDateNTime.tm_isdst = -1;	//daylight saving time is unknown

	//update tm in stantdard format for display
	(void)mktime(&StDateNTime);
	int returnVal = (int)wcsftime(time_value_str, MAX_DD_STRING, format, &StDateNTime);

	return (returnVal);
}

int CBuiltIn::timet_to_string(wchar_t* timet_str, wchar_t* format, long timet_value)
{
	time_t time_t_val = get_time(timet_value);

	//get the time in tm struct
	struct tm *StDateNTime = gmtime(&time_t_val);
	int returnVal = (int)wcsftime(timet_str, MAX_DD_STRING, format, StDateNTime);

	return (returnVal);
}

// The Builtin timet_to_TIME_VALUE converts the time of day part of a time_t to a TIME_VALUE(4).
unsigned long CBuiltIn::timet_to_TIME_VALUE(long timet_value)
{
	//convert time_t structure to tm structure in local time zone
	time_t inputTime = get_time(timet_value);
	struct tm *StDateNTime;
	StDateNTime = gmtime(&inputTime);

	//output is number of 1/32 ms since midnight
	unsigned long time_value = 32000 * ((StDateNTime->tm_hour * 60 + StDateNTime->tm_min) * 60 + StDateNTime->tm_sec);

	return (time_value);
}

// The Builtin timet_to_TIME_VALUE8 converts the time of day part of a time_t to a TIME_VALUE(8).
unsigned long long CBuiltIn::timet_to_TIME_VALUE8(long timet_value)
{
	//convert time_t structure to tm structure in local time zone
	time_t inputTime = get_time(timet_value);
	struct tm *StDateNTime;
	StDateNTime = gmtime(&inputTime);

	//output is number of 1/32 ms since midnight
	unsigned long long time_value = To_TIME_VALUE8(StDateNTime->tm_year+1900, StDateNTime->tm_mon, StDateNTime->tm_mday, StDateNTime->tm_hour, StDateNTime->tm_min, StDateNTime->tm_sec);

	return (time_value);
}

unsigned long CBuiltIn::To_TIME_VALUE(int hours, int minutes, int seconds)
{
	unsigned long time_value = (unsigned long)32000 * (60 * (60 * hours + minutes) + seconds);

	return (time_value);
}

unsigned long long CBuiltIn::To_TIME_VALUE8 (int year, int month, int day, int hour, int minute, int second)
{
	struct tm StDateNTime;

	StDateNTime.tm_wday = 0;
	StDateNTime.tm_yday = 0;
	StDateNTime.tm_year =  year - 1900;																	//since 1900
	StDateNTime.tm_mon =   month;
	if (day > 0)
	{
		StDateNTime.tm_mday = day;		//based on 1
	}
	else
	{
		StDateNTime.tm_mday = 1;
	}
	StDateNTime.tm_hour = hour;
	StDateNTime.tm_min = minute;
	StDateNTime.tm_sec = second;
	StDateNTime.tm_isdst = -1;			//daylight saving time is unknown

	//update pstTM in stantdard format
	time_t timet_val = mktime(&StDateNTime);															//since 1970
	
	unsigned long long time_value = ((unsigned long long)timet_val - 2 * 365 * 24 * 60 * 60) * 32000;	//since 1972

	return (time_value);
}

/***************************End of Date Time Builtins (eDDL)*************/
