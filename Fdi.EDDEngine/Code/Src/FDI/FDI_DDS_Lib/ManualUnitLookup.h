#pragma once

#include <vector>
#include <map>
#include "ddi_lib.h"
#include <mutex>


// ManualUnitLookup_Lock manages the CRITICAL_SECTION contained in the ManualUnitLookup class
class ManualUnitLookup_Lock 
{
private:
	friend class ManualUnitLookup;		// Needed to gain access to the CRITICAL_SECTION

	// data
	ManualUnitLookup* m_pClassToLock;	// Pointer to the class instance that it is managing

	// constructor and destructor
	ManualUnitLookup_Lock(ManualUnitLookup* m_pClassToLock);
	~ManualUnitLookup_Lock();

private:
	ManualUnitLookup_Lock();				// Hide default constructor
};


// CachedUnit is a class which is used by ManualUnitLookup to store information about
// a parameters UNIT relationship
class CachedUnit
{
public:
	enum CachedUnitType
		{ NoUnit, StaticUnit, ConditionalUnit };

	CachedUnitType etype;
	ITEM_ID UnitID;

	CachedUnit ()
	{ etype = NoUnit; UnitID = 0; }

	CachedUnit (CachedUnitType type, ITEM_ID unit)
	{ etype = type; UnitID = unit; }
};


typedef std::vector<DDI_GENERIC_ITEM *> UNITS_VECTOR;
typedef std::map<nsEDDEngine::FDI_PARAM_SPECIFIER, CachedUnit> UNITS_MAP;


// ManualUnitLookup is used to lookup the correct UNIT relation for a specific parameter.
// It was needed since some EDDs contain both static UNIT defintions and Conditional UNIT
//    definitions for the same parameter.
class ManualUnitLookup
{
	friend class ManualUnitLookup_Lock;	// Used to manage the CRITICAL_SECTION
	std::recursive_mutex m_cs;

	// std::map<ITEM_ID, CachedUnit>
	UNITS_MAP mapUnitLookup;			// Used to cache Param ID with corresponding UNIT ID

	// std::vector<DDI_GENERIC_ITEM *>
	UNITS_VECTOR vecStaticUnits;		// contains static FLAT_UNIT definitions
	UNITS_VECTOR vecConditionalUnits;	// contains conditional FLAT_UNIT definitions

public:
	ManualUnitLookup();
	~ManualUnitLookup();

	int Init(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec);

	// Finds the best UNIT relation for the param specifier
	int FindUnitRel(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitFromTbl, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID);

	// Checks to see if this FLAT_UNIT has my param on the right side
	static bool IsMyUnit(nsEDDEngine::FLAT_UNIT *pUnit, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec);

private:
	// This is the part of the Init() function which evaluates the UNIT and stores it in the proper vector
    int EvalAndStore(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID item_id);


	// Looks through all the UNIT relations to find the best one, if it is not already cached.
	int FindForFirstTime(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
								ITEM_ID unitFromTbl, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID);

	// Finds the best UNIT relation from the list of Conditional UNIT definitions
	int FindInConditionalVector(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
								ITEM_ID unitFromTbl, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID);

private:
	// Disallow automatically generated functions
	ManualUnitLookup(const ManualUnitLookup& rhs);	// copy constructor
	
	ManualUnitLookup&
	operator=(const ManualUnitLookup& rhs);			// assignment operator

	ManualUnitLookup* operator&();					// address-of operators
	const ManualUnitLookup* operator&() const;

};

