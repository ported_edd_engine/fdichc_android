#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <map>

using namespace nsEDDEngine;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class ItemTableTest
        {
        private:
            TestContext^ testContextInstance;

        public: 
            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            property Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ TestContext
            {
                Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ get()
                {
                    return testContextInstance;
                }
                System::Void set(Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ value)
                {
                    testContextInstance = value;
                }
            };

            /// <summary>
            /// Tests if the EDD Engine reports the correct number of items for FF DDs
            ///</summary>
            [TestMethod, TestCategory("ItemTableTest")]
            void TestVariableCount()
            {
                EddEngineController^ engine = gcnew EddEngineController("FF\\TestCase0007\\TestCase0007.ff6");

                
                engine->Initialize();
                

                FLAT_DEVICE_DIR *flat_device_dir;
                int returnCode = engine->Instance->GetDeviceDir(&flat_device_dir);
                Assert::AreEqual(0, returnCode);

                std::map<ITEM_TYPE,int> itemCount;
                for (int nIndex = 0; nIndex < flat_device_dir->item_tbl.count; nIndex++)
                {
                    ITEM_TBL_ELEM item = flat_device_dir->item_tbl.list[nIndex];

                    itemCount[item.item_type]++;
                }

                Assert::AreEqual(19, itemCount[ITYPE_VARIABLE], "invalid VARIABLE count");
                Assert::AreEqual(0, itemCount[ITYPE_COMMAND], "invalid COMMAND count");
                Assert::AreEqual(0, itemCount[ITYPE_MENU], "invalid MENU count");
                Assert::AreEqual(1, itemCount[ITYPE_BLOCK], "invalid BLOCK count");
                Assert::AreEqual(2, itemCount[ITYPE_RECORD], "invalid RECORD count");
                Assert::AreEqual(1, itemCount[ITYPE_ARRAY], "invalid ARRAY count");
            }            
        };

    } // namespace FF
} // namespace ComponentTests
