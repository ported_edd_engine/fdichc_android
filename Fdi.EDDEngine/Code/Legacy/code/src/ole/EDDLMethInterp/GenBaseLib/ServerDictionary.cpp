

#include "stdafx.h"
#pragma warning (disable : 4786)

#include <ServerProjects/rtn_code.h>
#include "ServerDictionary.h"


CServerDictionary::CServerDictionary()
{	
	m_pDevice = NULL;
}


CServerDictionary::CServerDictionary( CMethodInterpreterDevice *pDevice )
{
	m_pDevice = pDevice;
}


CServerDictionary::~CServerDictionary()
{
}
int CServerDictionary::get_dictionary_string(char* chStringKey , STRING *str)
{
	return DDL_SUCCESS;

}
int CServerDictionary ::get_dictionary_string(unsigned long index , STRING *str)
{
	return DDL_SUCCESS;
}

int CServerDictionary::get_dictionary_string( unsigned long index, wchar_t* str, int iStringLen )
{
	//get dictionary string from DDS
	int rc = m_pDevice->getDictionaryString(index, str, iStringLen);
	return rc;
}

//This function is used by MEE::GetDictionaryString()
int CServerDictionary::get_dictionary_string(wchar_t* chStringKey ,  wchar_t* str, int iStringLen )
{
	int nReturn = BLTIN_VAR_NOT_FOUND;
	if(m_pDevice)
	{
		if(m_pDevice->ReadDictionaryString(chStringKey, str, iStringLen ) == BLTIN_SUCCESS)
		{
			nReturn = BLTIN_SUCCESS;
		}
	}
	return nReturn; 

}

//
//
//
int CServerDictionary::get_lit_string( unsigned long index, wchar_t* str, long lStringLen )
{
	//get literal string from DDS
	int rc = m_pDevice->ReadLiteralString(index, str, lStringLen);
	return rc;
}
