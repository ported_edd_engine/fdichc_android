#-------------------------------------------------
#
# Project created by QtCreator 2017-11-30T11:37:37
#
#-------------------------------------------------

QT       -= gui

TARGET = ddsHart
TEMPLATE = lib
CONFIG += staticlib
#CONFIG +=plugin
DEFINES += DDSHART_LIBRARY
DEFINES += HART
DEFINES += ISP
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#QMAKESPEC=linux-g++-32
#TARGET = $$(TARGET)x32
#QMAKE_CXXFLAGS += -m32
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKE_CXXFLAGS += -std=gnu++11

SOURCES +=  ddi_cmi.cpp \
    ddi_conv.cpp \
    ddi_dir.cpp \
    ddi_item.cpp \
    ddi_lang.cpp \
    ddi_tab.cpp \
    dds_chk.cpp \
    dds_err.cpp \
    dds_tab.cpp \
    ManualUnitLookup.cpp \
    stdinc.cpp \
    evl_base.cpp \
    evl_cln.cpp \
    evl_cond.cpp \
    evl_dir.cpp \
    evl_enum.cpp \
    evl_expr.cpp \
    evl_iary.cpp \
    evl_item.cpp \
    evl_mem.cpp \
    evl_menu.cpp \
    evl_ref.cpp \
    evl_rel.cpp \
    evl_resp.cpp \
    evl_rslv.cpp \
    evl_str.cpp \
    evl_tran.cpp \
    evl_type.cpp \
    fch_rdir.cpp \
    fch_ritm.cpp \
    fch_rodf.cpp \
    fch_slct.cpp \

HEADERS += ../../../inc/ServerProjects/DDS/attrs.h \
    ../../../inc/ServerProjects/DDS/ddi_item.h \
    ../../../inc/ServerProjects/DDS/ddi_lib.h \
    ../../../inc/ServerProjects/DDS/ddi_tab.h \
    ../../../inc/ServerProjects/DDS/ddldefs.h \
    ../../../inc/ServerProjects/DDS/dds_chk.h \
    ../../../inc/ServerProjects/DDS/env_info.h \
    ../../../inc/ServerProjects/DDS/evl_lib.h \
    ../../../inc/ServerProjects/DDS/evl_loc.h \
    ../../../inc/ServerProjects/DDS/evl_ret.h \
    ../server/hrt_cstk/hsym0002.h \
    dds_tab.h \
    ../../../inc/ServerProjects/DDS/dds_upcl.h \
    ../../../inc/ServerProjects/DDS/fch_lib.h \
    ../../../inc/ServerProjects/DDS/flats.h \
    ../../../inc/ServerProjects/DDS/od_defs.h \
    ../../../inc/ServerProjects/DDS/table.h \
    ../../../inc/ServerProjects/DDS/tags_sa.h \
    ManualUnitLookup.h
    stdinc.h

unix {
    target.path = /usr/lib/
    INSTALLS += target
}

DESTDIR = ../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u



INCLUDEPATH += $$PWD/../../../inc/ServerProjects/
INCLUDEPATH += $$PWD/../../../inc/ServerProjects/DDS/
INCLUDEPATH += $$PWD/../../../src/ServerProjects/server/src/
INCLUDEPATH += $$PWD/../../../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../../../Inc/
INCLUDEPATH += $$PWD/../../../inc/
INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Linux
}

unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common

DEPENDPATH += $$DESTDIR/EDD_Engine_Common

unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a

