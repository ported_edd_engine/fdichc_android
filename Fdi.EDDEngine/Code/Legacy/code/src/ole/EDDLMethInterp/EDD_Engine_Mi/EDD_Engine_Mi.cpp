// EDD_Engine_Mi.cpp : Defines the entry point for the DLL application.
//

// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f EDDLMethInterpps.mk in the project directory.

#include "stdafx.h"

#include "resource.h"
#include "BuiltInMapper.h"


class CEDD_Engine_Mi_App : public CWinApp
{
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEDD_Engine_Mi_App)
	public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CEDD_Engine_Mi_App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CEDD_Engine_Mi_App, CWinApp)
	//{{AFX_MSG_MAP(CEDD_Engine_Mi_App)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CEDD_Engine_Mi_App theApp;

BOOL CEDD_Engine_Mi_App::InitInstance()
{
    return CWinApp::InitInstance();
}

int CEDD_Engine_Mi_App::ExitInstance()
{
	CBuiltInMapper::GetInstance()->Shutdown();
	CBuiltInMapper::DestroyInstance();

    return CWinApp::ExitInstance();
}

