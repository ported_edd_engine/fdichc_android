#pragma once

#pragma warning( push, 3 )
#pragma warning(disable: 4996) //MHD (EmersonProcess) Disable the VS2005 security warning - 'function': was declared deprecated

#include "ddbDevice.h"
#include "stdstring.h"
#pragma warning( pop )
#pragma warning(disable: 4996) //MHD (EmersonProcess) Disable the VS2005 security warning - 'function': was declared deprecated

//#include <comutil.h>

class CCmdList;

class CItemMapping
{
	public:
		CItemMapping( long lItemID, itemType_e eItemType, CStdString sName ):m_lItemID(lItemID), m_eItemType(eItemType), m_sName(sName), m_pItemPointer(NULL){}
		long m_lItemID;
		itemType_e m_eItemType;
		CStdString m_sName;
		hCitemBase *m_pItemPointer;
};

class CMethodInterpreterDevice : public hCddbDevice
{
	public:
		CMethodInterpreterDevice( DevInfcHandle_t h );
		virtual ~CMethodInterpreterDevice();
		itemType_e GetItemType( long lKind );
		virtual bool ReadData( CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadMethodData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadValueArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadItemArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadFileData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadCollectionData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
		virtual bool ReadEnumerationData( unsigned long ulItemId, unsigned long ulMemberId, hCenumList& returnList ) = 0;
		virtual bool AddMapBasedOnName( CStdString sName )=0;
		virtual bool AddMapBasedOnItemID( long lItemID )=0;
		virtual int ReadLiteralString( unsigned long ulStringID, wchar_t* sValue, long lStrLen )=0;
		virtual int ReadDictionaryString( wchar_t *sStringName, wchar_t *sValue, int iStringLen ) = 0;
		virtual bool LockMethodUIEvent(unsigned long milliSeconds = 0);
		virtual void UnlockMethodUIEvent();
		virtual void ResetMethodUIEvent();
		
	protected:
		void ClearItemMapping();
		CItemMapping *GetItemMapObject( long lItemID );
		CItemMapping *GetItemMapObject( CStdString sName );
		hCitemBase* GetItemPointer( itemType_e itemType, symbolNumb_t symNum, CStdString sName );
		hCitemBase* GetValueArrayPointer( symbolNumb_t symNum );
		hCitemBase* GetMethodPointer( symbolNumb_t symNum, CStdString sName );
		hCitemBase* GetArrayPointer( symbolNumb_t symNum );
		hCitemBase* GetCollectionPointer( symbolNumb_t symNum );
		hCitemBase* GetCommandPointer( symbolNumb_t symNum, CStdString sName );
		hCitemBase* GetFilePointer( symbolNumb_t symNum );
		hCitemBase* GetRecordPointer( symbolNumb_t symNum, CStdString sName );
		hCitemBase* GetParameterPointer( symbolNumb_t symNum );
		hCitemBase* GetGenericPointer( symbolNumb_t symNum, itemType_e itemType );
		hCitemBase* GetListPointer( ITEM_ID itemID, CStdString sName );
		hCitemBase* GetChartPointer( ITEM_ID itemID );
		hCitemBase* GetGraphPointer( ITEM_ID itemID );
		hCitemBase* GetWaveformPointer( ITEM_ID itemID );
		hCitemBase* GetSourcePointer( ITEM_ID itemID );

	public://overriden methods of hCddbDevice
		virtual RETURNCODE getItemBySymName  ( string& symName,hCitemBase** ppReturnedItempointer);
		virtual int get_rspcode_string( int nCommandNumber, int nResponseCode, tchar* pchResponseString, int nLength, nsEDDEngine::ProtocolType protocol ) = 0;
		virtual int getElementOrMemberPointer(unsigned long ulItemId, unsigned long ulMemberId, hCitemBase **pIB);
		virtual int GetMemberIdByName( CStdString sMemberName, unsigned long *pMemberId ) = 0;
		virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0) = 0;

	public: //overriden methods of hCdeviceSrvc	
		virtual  RETURNCODE getItemBySymNumber(symbolNumb_t synNum, hCitemBase** ppReturnedItempointer);

	public: //overriden methods of hCdeviceSrvc	
		virtual void getStartDate(dmDate_t& dt);
		virtual hCobject* getListPtr(CitemType  it); 
		virtual RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue);

	protected: //member variables
		std::vector<CItemMapping*> m_aItemIdMapping;
		CCmdList *m_pCommandList;

	private:
		mutex m_methodLock;
		static bool m_bmethodProcessed;
		condition_variable m_methodCondVariable;

};
