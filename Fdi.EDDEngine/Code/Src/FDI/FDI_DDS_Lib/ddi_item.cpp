/*
 *	@(#) $Id: ddi_item.cpp,v 1.2 2012/10/04 23:50:57 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	This file contains all functions relating to ddi_get_item
 */

#include "stdafx.h"
#include "fch_lib.h"
#include "evl_lib.h"
#include "ddi_lib.h"
#include "ddi_tab.h"
#include "dds_tab.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"

#include "nsEDDEngine/Flats.h"

extern nsEDDEngine::SYMBOL_TBL_ELEM *get_symbol_tbl_lookup(BLOCK_HANDLE block_handle, ENV_INFO* env_info, int id);

/*********************************************************************
 *
 *	Name: convert_item_spec
 *	ShortDesc: convert the item specifier into a dd_ref
 *
 *	Description:
 *		convert_item_spec takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request function to
 *		get the device description reference
 *
 *	Inputs:
 *		dd_item_spec:	a pointer to the DDI_ITEM_SPECIFIER structure
 *		block_handle:	the current block handle
 *		bt_elem:		a pointer to a Block Table element
 *
 *	Outputs:
 *		dd_ref:			a pointer to the device description reference
 *		item_type:		a pointer to the type of the item
 *
 *	Returns: DDI_INVALID_TYPE and returns from dd_ref table
 *  request functions
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int
convert_item_spec(
ENV_INFO		*env_info,
nsEDDEngine::FDI_ITEM_SPECIFIER *dd_item_spec,
BLOCK_HANDLE    block_handle,
nsEDDEngine::BLK_TBL_ELEM   *bt_elem,
nsEDDEngine::DD_REFERENCE   *dd_ref,
ITEM_TYPE      *item_type)
{

	int             rc;	/* return code */
	nsEDDEngine::ITEM_TBL       *it;
	nsEDDEngine::ITEM_TBL_ELEM  *it_elem;
	nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	it_elem = NULL;

	if(bt_elem==NULL)
	{
		rc = pConnectionMgr->get_adt_dd_dev_tbls(0, (void **) &flat_device_dir);
	}
	else
	{
		rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **) &flat_device_dir);
	}

	if (rc != SUCCESS) {
		return (rc);
	}

	it = &flat_device_dir->item_tbl;

	switch (dd_item_spec->eType) {

	case (nsEDDEngine::ItemSpecifierType::FDI_ITEM_BLOCK):
		rc = tr_block_to_ite(it, bt_elem, &it_elem);
		break;

	case (nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID):
		rc = tr_id_to_ite(env_info, it, dd_item_spec->item.id, &it_elem);
		break;

	case (nsEDDEngine::ItemSpecifierType::FDI_ITEM_PARAM):
		rc = tr_param_to_ite(it, bt_elem, dd_item_spec->item.param, &it_elem);
		break;

	case (nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID_SI):
		rc = tr_id_si_to_ite(it, bt_elem, dd_item_spec->item.id, dd_item_spec->subindex, &it_elem);
		break;

	case (nsEDDEngine::ItemSpecifierType::FDI_ITEM_PARAM_SI):
		rc = tr_param_si_to_ite(it, bt_elem, dd_item_spec->item.param, dd_item_spec->subindex, &it_elem);
		break;

	default:
		rc = DDI_INVALID_TYPE;
	}

	if (it_elem != NULL) {

		/*
		 * copy the dd reference and set item type for our return
		 * values
		 */

		memcpy((char *) dd_ref, (char *) &(ITE_DD_REF(it_elem)), sizeof(*dd_ref));

		*item_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);
	}

	return rc;

}


/*********************************************************************
 *
 *	Name: get_masks_ptr
 *	ShortDesc: get the flat masks pointer
 *
 *	Description:
 *		This function will return the address for the masks of the
 *		specified item
 *
 *	Inputs:
 *		item_type:		type of item requested
 *		item:			the loaded FLAT structure
 *
 *	Outputs:
 *		see Returns:
 *
 *	Returns:
 *		a pointer to the masks in the item, pointer is NULL if the
 *		type is not in the switch statement
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static inline 
::nsEDDEngine::FLAT_MASKS  *
get_masks_ptr(
ITEM_TYPE       /* item_type */,
::nsEDDEngine::ItemBase *item)
{
	::nsEDDEngine::FLAT_MASKS     *masks_ptr=NULL;	/* the pointer to return to the caller */

	if(!item)
		return NULL;

	masks_ptr=&item->masks;
	return masks_ptr;
}


/*********************************************************************
 *
 *	Name: get_item_id()
 *
 *	ShortDesc: get the DDL item's item ID
 *
 *	Description:
 *		This function will return the item ID of the given
 *      DDL item
 *
 *	Inputs:
 *		item_type:		type of item requested
 *		item:			pointer to the DDL item structure
 *
 *	Outputs:
 *		see Returns:
 *
 *	Returns:
 *      the item ID of the given item
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
ITEM_ID
get_item_id(
ITEM_TYPE       /* item_type */,
::nsEDDEngine::ItemBase  *item)
{
	if(!item)
		return 0;

	return item->id;
}



/*********************************************************************
 *
 *	Name: get_depbin_list_ptrs
 *	ShortDesc: returns a list of pointers for all depbin lists pertaining
 *	to the item requested
 *
 *	Description:
 *		get_depbin_list_ptrs loads an array of void pointers with the
 *		addresses of the depbin lists for a given flat structure (up to
 *		3 pointers for a VARIABLE and one pointer for all other items
 *
 *	Inputs:
 *		item_type:	type of item requested
 *		item:		the FLAT item structure
 *
 *	Outputs:
 *		depbins:	the address of a void pointer
 *
 *	Returns: DDI_INVALID_ITEM_TYPE, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static inline
int
get_depbin_list_ptrs(
ITEM_TYPE       /*item_type*/,
::nsEDDEngine::ItemBase           *item,
void          ***depbins)
{
	*depbins=NULL;

	if(!item)
		return E_FAIL;

	return DDS_SUCCESS; // FD_FDI free
}


/*********************************************************************
 *
 *	Name: get_depbin_list_sizes
 *	ShortDesc: returns a list of all the depbin list sizes for the item
 *	requested
 *
 *	Description:
 *		get_depbin_list_sizes loads an array with the sizes
 *		of the depbin lists for a given flat structure (3 lists for a
 *		VARIABLE and one list for all other items)
 *
 *	Inputs:
 *		item_type:	type of item requested
 *
 *	Outputs:
 *		size:		the array of sizes to fill
 *
 *	Returns: DDI_INVALID_ITEM_TYPE, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/


static inline int
get_depbin_list_sizes(
ITEM_TYPE       item_type,
size_t          *size)
{

	switch (item_type) {

	case VARIABLE_ITYPE:
		*size = sizeof(::nsEDDEngine::VAR_DEPBIN);
		break;

	case MENU_ITYPE:
		*size = sizeof(::nsEDDEngine::MENU_DEPBIN);
		break;

	case EDIT_DISP_ITYPE:
		*size = sizeof(::nsEDDEngine::EDIT_DISPLAY_DEPBIN);
		break;

	case METHOD_ITYPE:
		*size = sizeof(::nsEDDEngine::METHOD_DEPBIN);
		break;

	case REFRESH_ITYPE:
		*size = sizeof(::nsEDDEngine::REFRESH_DEPBIN);
		break;

	case UNIT_ITYPE:
		*size = sizeof(::nsEDDEngine::UNIT_DEPBIN);
		break;

	case WAO_ITYPE:
		*size = sizeof(::nsEDDEngine::WAO_DEPBIN);
		break;

	case ITEM_ARRAY_ITYPE:
		*size = sizeof(::nsEDDEngine::ITEM_ARRAY_DEPBIN);
		break;

	case COLLECTION_ITYPE:
		*size = sizeof(::nsEDDEngine::COLLECTION_DEPBIN);
		break;

	case BLOCK_ITYPE:
		*size = sizeof(::nsEDDEngine::BLOCK_DEPBIN);
		break;

	case RECORD_ITYPE:
		*size = sizeof(::nsEDDEngine::RECORD_DEPBIN);
		break;

	case ARRAY_ITYPE:
		*size = sizeof(::nsEDDEngine::ARRAY_DEPBIN);
		break;

	case RESP_CODES_ITYPE:
		*size = sizeof(::nsEDDEngine::RESP_CODE_DEPBIN);
		break;

	case COMMAND_ITYPE:
		*size = sizeof(::nsEDDEngine::COMMAND_DEPBIN);
		break;

	case AXIS_ITYPE:
		*size = sizeof(::nsEDDEngine::AXIS_DEPBIN);
		break;

	case CHART_ITYPE:
		*size = sizeof(::nsEDDEngine::CHART_DEPBIN);
		break;

	case FILE_ITYPE:
		*size = sizeof(::nsEDDEngine::FILE_DEPBIN);
		break;

	case GRAPH_ITYPE:
		*size = sizeof(::nsEDDEngine::GRAPH_DEPBIN);
		break;

	case GRID_ITYPE:
		*size = sizeof(::nsEDDEngine::GRID_DEPBIN);
		break;

	case IMAGE_ITYPE:
		*size = sizeof(::nsEDDEngine::IMAGE_DEPBIN);
		break;

	case LIST_ITYPE:
		*size = sizeof(::nsEDDEngine::LIST_DEPBIN);
		break;

	case SOURCE_ITYPE:
		*size = sizeof(::nsEDDEngine::SOURCE_DEPBIN);
		break;

	case WAVEFORM_ITYPE:
		*size = sizeof(::nsEDDEngine::WAVEFORM_DEPBIN);
		break;

	case BLOCK_B_ITYPE:
		*size = sizeof(::nsEDDEngine::BLOCK_B_DEPBIN);
		break;

	default:
		ASSERT(0);
		return DDI_INVALID_PARAM;
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: make_generic_item()
 *
 *	ShortDesc: create if necessary the DDL item and hook it to the
 *             generic item
 *
 *	Description:
 *      make_generic_item() will malloc and attach the memory necessary
 *      for the DD item which is to be fetched and evaluated by ddi_get_item()
 *
 *	Inputs:
 *
 *		generic_item:		the pointer to the generic item structure
 *
 *      attr_request:       the attr_request mask which indicates if the
 *                          variable substructures need to be allocated
 *                          and attached to the VARIABLE_ITEM struct                    
 *
 *	Outputs:
 *
 *		item:	the generic item with base item allocated
 *
 *	Returns:	DDI_MEMORY_ERROR, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int 
make_generic_item(
DDI_GENERIC_ITEM *generic_item)
{
	void	**tmp_item;
	tmp_item = &generic_item->item;

	/*
     * if this is a call for reevaluation
	 * initialize the errors list, if this is
     * not a call back we assume the error list
     * has been initialized by ddi_clean_item()
     * or by the caller
	 */

	if (*tmp_item) {
		memset((char *) &generic_item->errors, 0, sizeof(generic_item->errors));
	}

	if (*tmp_item) {
		return DDS_SUCCESS;
	}

	::nsEDDEngine::ITEM_TYPE item_type=(::nsEDDEngine::ITEM_TYPE)generic_item->item_type;

	switch (item_type) {

	case ::nsEDDEngine::ITYPE_VARIABLE:
		*tmp_item = new ::nsEDDEngine::FLAT_VAR();
		break;

	case ::nsEDDEngine::ITYPE_COMMAND:
		*tmp_item = new ::nsEDDEngine::FLAT_COMMAND();
		break;

	case ::nsEDDEngine::ITYPE_MENU:
		*tmp_item = new ::nsEDDEngine::FLAT_MENU();
		break;

	case ::nsEDDEngine::ITYPE_EDIT_DISP:
		*tmp_item = new ::nsEDDEngine::FLAT_EDIT_DISPLAY();
		break;

	case ::nsEDDEngine::ITYPE_METHOD:
		*tmp_item = new ::nsEDDEngine::FLAT_METHOD();
		break;

	case ::nsEDDEngine::ITYPE_REFRESH:
		*tmp_item = new ::nsEDDEngine::FLAT_REFRESH();
		break;

	case ::nsEDDEngine::ITYPE_UNIT:
		*tmp_item = new ::nsEDDEngine::FLAT_UNIT();
		break;

	case ::nsEDDEngine::ITYPE_WAO:
		*tmp_item = new ::nsEDDEngine::FLAT_WAO();
		break;

	case ::nsEDDEngine::ITYPE_ITEM_ARRAY:
		*tmp_item = new ::nsEDDEngine::FLAT_ITEM_ARRAY();
		break;

	case ::nsEDDEngine::ITYPE_COLLECTION:
		*tmp_item = new ::nsEDDEngine::FLAT_COLLECTION();
		break;

	case ::nsEDDEngine::ITYPE_BLOCK_B:
		*tmp_item = new ::nsEDDEngine::FLAT_BLOCK_B();
		break;

	case ::nsEDDEngine::ITYPE_BLOCK:
		*tmp_item = new ::nsEDDEngine::FLAT_BLOCK();
		break;

	case ::nsEDDEngine::ITYPE_RECORD:
		*tmp_item = new ::nsEDDEngine::FLAT_RECORD();
		break;

	case ::nsEDDEngine::ITYPE_ARRAY:
		*tmp_item = new ::nsEDDEngine::FLAT_ARRAY();
		break;

	case ::nsEDDEngine::ITYPE_VAR_LIST:
		*tmp_item = new ::nsEDDEngine::FLAT_VAR_LIST();
		break;

	case ::nsEDDEngine::ITYPE_RESP_CODES:
		*tmp_item = new ::nsEDDEngine::FLAT_RESP_CODE();
		break;

	case ::nsEDDEngine::ITYPE_MEMBER:
		*tmp_item = new ::nsEDDEngine::FLAT_MEMBER();
		break;

	case ::nsEDDEngine::ITYPE_FILE:
		*tmp_item = new ::nsEDDEngine::FLAT_FILE();
		break;

	case ::nsEDDEngine::ITYPE_CHART:
		*tmp_item = new ::nsEDDEngine::FLAT_CHART();
		break;

	case ::nsEDDEngine::ITYPE_GRAPH:
		*tmp_item = new ::nsEDDEngine::FLAT_GRAPH();
		break;

	case ::nsEDDEngine::ITYPE_AXIS:
		*tmp_item = new ::nsEDDEngine::FLAT_AXIS();
		break;

	case ::nsEDDEngine::ITYPE_WAVEFORM:
		*tmp_item = new ::nsEDDEngine::FLAT_WAVEFORM();
		break;

	case ::nsEDDEngine::ITYPE_SOURCE:
		*tmp_item = new ::nsEDDEngine::FLAT_SOURCE();
		break;

	case ::nsEDDEngine::ITYPE_LIST:
		*tmp_item = new ::nsEDDEngine::FLAT_LIST();
		break;

	case ::nsEDDEngine::ITYPE_GRID:
		*tmp_item = new ::nsEDDEngine::FLAT_GRID();
		break;

	case ::nsEDDEngine::ITYPE_IMAGE:
		*tmp_item = new ::nsEDDEngine::FLAT_IMAGE();
		break;

	case ::nsEDDEngine::ITYPE_BLOB:
		*tmp_item = new ::nsEDDEngine::FLAT_BLOB();
		break;

	case ::nsEDDEngine::ITYPE_PLUGIN:
		*tmp_item = new ::nsEDDEngine::FLAT_PLUGIN();
		break;

	case ::nsEDDEngine::ITYPE_TEMPLATE:
		*tmp_item = new ::nsEDDEngine::FLAT_TEMPLATE();
		break;

	case ::nsEDDEngine::ITYPE_COMPONENT:
		*tmp_item = new ::nsEDDEngine::FLAT_COMPONENT();
		break;

	case ::nsEDDEngine::ITYPE_COMPONENT_FOLDER:
		*tmp_item = new ::nsEDDEngine::FLAT_COMPONENT_FOLDER();
		break;

	case ::nsEDDEngine::ITYPE_COMPONENT_REFERENCE:
		*tmp_item = new ::nsEDDEngine::FLAT_COMPONENT_REFERENCE();
		break;

	case ::nsEDDEngine::ITYPE_COMPONENT_RELATION:
		*tmp_item = new ::nsEDDEngine::FLAT_COMPONENT_RELATION();
		break;

	default:
		ASSERT(0);
		return DDI_INVALID_PARAM;
	}

	if (*tmp_item == NULL) {

		return DDI_MEMORY_ERROR;
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_item()
 *
 *	ShortDesc: Gets the requested item and returns it to the caller
 *
 *	Description:
 *		ddi_get_item fetches an item from either a ROD or a FLAT, calls
 *		eval if necessary to evaluate the values and copies the dynamic
 *		attributes binary into malloced memory for possible future evaluation
 *		by the calling application
 *
 *		If the application calls ddi_get_item() with a generic_item which
 *		has already been created and filled with DD information by
 *		ddi_get_item(), this is known as calling in for reevaluation.
 *		When the application calls ddi_get_item() for the reevaluation
 *		of an item, ddi_get_item() does not use the DDI_ITEM_SPECIFIER
 *		input structure as the specification for the item, instead
 *		it uses	the item_id and the item_type which are part of the previously
 *		evaluated and DDI_GENERIC_ITEM structure.
 *
 *	Inputs:
 *
 *      block_spec: specifies the block with either a BLOCK_TAG or BLOCK_HANDLE
 *
 *		item_spec:	specifies the item being requested
 *
 *		env_info:	the application's environment information
 *
 *      req_mask: the mask which specifies the item attributes
 *                the caller wants ddi_get_item() to fetch and evaluate
 *                for the requested item
 *
 *	Outputs:
 *
 *		item:		the generic DDI item
 *
 *
 *	Returns: DDI_INVALID_PARAM, DDI_INVALID_FLAT_FORMAT,
 *		DDI_BAD_DD_BLOCK_LOAD, and returns from other DDS functions.
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/


int
ddi_get_item(
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_ITEM_SPECIFIER  *item_spec,
ENV_INFO            *env_info,
nsEDDEngine::AttributeNameSet     req_mask, //DDI_ATTR_REQUEST     req_mask,
DDI_GENERIC_ITEM    *generic_item)
{
	int             rc;	/* return code */
	int             return_code;	/* saved return code from eval_item() call */

	BLOCK_HANDLE    block_handle = 0;	/* handle for the block */
	nsEDDEngine::DD_REFERENCE    dd_ref;	/* device description reference */
	ROD_HANDLE       rod_handle = 0;	/* device description handle */

	nsEDDEngine::FLAT_MASKS     *masks;	/* pointer to the masks of the flat item */
	nsEDDEngine::AttributeNameSet old_dynamic;	// state of the dynamic mask before eval */

	nsEDDEngine::BLK_TBL_ELEM   *bt_elem = nullptr;/* Block Table element pointer */

	ITEM_TYPE       item_type;	/* type of the item being requested */
	::nsEDDEngine::ItemBase *item;	/* pointer to the item inside of generic_item */
	nsEDDEngine::FDI_ITEM_SPECIFIER local_ispec;	/* local copy of the item specifier */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	ASSERT_RET(block_spec && item_spec && env_info && generic_item, DDI_INVALID_PARAM);

	/*
	 * if the generic item has an item hooked up, check to see that type exists
	 */

	if (generic_item->item) { 

		ASSERT_RET(generic_item->item_type, DDI_INVALID_PARAM);
	}

	return_code = DDS_SUCCESS;

	memset((char *) &dd_ref, 0, sizeof(nsEDDEngine::DD_REFERENCE));
	rod_handle = 0;

	/*
	 * convert the block specifier to a block handle
	 */
	if(env_info->handle_type==ENV_INFO::DeviceHandle)
	{
		rc = pConnectionMgr->get_adt_dd_handle(0, &rod_handle);
		if (rc != SUCCESS) {
			return (rc);
		}
	}
	else
	{
		rc = conv_block_spec(env_info, block_spec, &block_handle);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
		else {

			/*
			 * load environment information with block handle
			 */

			env_info->block_handle = block_handle;
		}

		/*
		 * convert block handle to a block table element
		 */

		rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
		if (rc != DDS_SUCCESS) {

			return rc;
		}

		/*
		 * convert block handle to a dd handle
		 */

		rc = pConnectionMgr->get_abt_dd_handle(block_handle, &rod_handle);
		if (rc != SUCCESS) {
			return (rc);
		}
	}

	/*
	 * If this is a call back for the reevaluation of an item, force the item
	 * specifier to be the item ID contained by the ddl Item structure.
     * We're doing this for speed reasons, item_id to dd reference is fast
     * in the tables. This also resolves the problem of a call to ddi_get_item()
     * which includes a previously loaded GENERIC_ITEM structure but a
     * DDI_ITEM_SPECIFIER structure which specifies a different item then the
     * the item loaded in the GENERIC_ITEM. If there's a conflict, we ignore
     * it and simply reevaluate the item loaded in the GENERIC_ITEM
	 */


	if (generic_item->item) {

#ifdef DEBUG
		dds_item_flat_check(generic_item->item, generic_item->item_type);
#endif
		local_ispec.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
		local_ispec.item.id = get_item_id(generic_item->item_type,(::nsEDDEngine::ItemBase *) generic_item->item);
	}
	else {

	/*
	 * make a local copy of item_spec
	 */

		(void) memcpy((char *) &local_ispec, (char *) item_spec, sizeof(nsEDDEngine::FDI_ITEM_SPECIFIER));
	}


	/*
	 * convert item_spec to a dd reference and item type
	 */
	if(env_info->handle_type==ENV_INFO::DeviceHandle)
	{
		rc = convert_item_spec(env_info, &local_ispec, -1, NULL, &dd_ref, &item_type);
	}
	else
	{
		rc = convert_item_spec(env_info, &local_ispec, block_handle, bt_elem, &dd_ref, &item_type);
	}

	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_item_spec fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnectionMgr->ds_dd_block_load(env_info, block_handle);
			if (!rc) {

				rc = convert_item_spec(env_info, &local_ispec, block_handle, bt_elem, &dd_ref, &item_type);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else
			{
				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {
			PS_TRACE(L"\n*** error (ddi_get_item): could not find item in tables type:%d, id:0x%x, subindex:%d\n",local_ispec.eType,local_ispec.item, local_ispec.subindex);
			return rc;
		}
	}

	/*
	 * Scratch pad is set up, we have our Block Handle we have a Device
	 * Description reference for the item, we're ready to allocate the item
	 * and call fetch and eval
	 */


	/*****************************************************************
	 *   				 ROD fetch
	 ****************************************************************/

	/*
     * We call make_generic_item() even if the generic_item.item
     * has been allocated. This is for two reasons. First, make_generic_item()
     * zeros out the return list. Second, make_generic_item() may need to
     * allocate substructuress below the FLAT_VAR structure, i.e. FLAT_VAR_MISC
     * or FLAT_VAR_ACTIONS.
	 */

	generic_item->item_type = item_type;
	rc = make_generic_item(generic_item);
	if (rc != DDS_SUCCESS) {
		if(generic_item->item)
			delete ((::nsEDDEngine::ItemBase *)generic_item->item);
		return rc;
	}

	/*
	 * set the temp item pointer
	 */

	item = (::nsEDDEngine::ItemBase*) generic_item->item;

	/*
	 * get the pre-eval masks to save for compares later
	 */

	masks = &((::nsEDDEngine::ItemBase *)item)->masks;//get_masks_ptr(item_type, item);
	old_dynamic = masks->dynamic;

#if 0	// MHD this is the old way where we hooked the bin to the flat
	/*
	 * sanity check: if there are bin_hooked bits set which don't
	 * match the dynamic mask bits, this depbin list was not
	 * created by DDI
	 */

	if (masks->bin_hooked ^ masks->dynamic) {

		return DDI_INVALID_FLAT_FORMAT;
	}
#endif
	bool bListCountAdded	= false;
	bool bAxisMinValueAdded = false;
	bool bAxisMaxValueAdded = false;

	if(item_type == nsEDDEngine::ITYPE_LIST)
	{
		if(req_mask.isMember(nsEDDEngine::count_ref))
		{
			if(!req_mask.isMember(nsEDDEngine::count))
			{
				req_mask.insert(nsEDDEngine::count);
				bListCountAdded = true;
			}
		}
	}

	if(item_type == nsEDDEngine::ITYPE_AXIS)
	{
		if(req_mask.isMember(nsEDDEngine::min_value_ref))
		{
			if(!req_mask.isMember(nsEDDEngine::min_value))
			{
				req_mask.insert(nsEDDEngine::min_value);
				bAxisMinValueAdded = true;
			}
		}
	}

	if(item_type == nsEDDEngine::ITYPE_AXIS)
	{
		if(req_mask.isMember(nsEDDEngine::max_value_ref))
		{
			if(!req_mask.isMember(nsEDDEngine::max_value))
			{
				req_mask.insert(nsEDDEngine::max_value);
				bAxisMaxValueAdded = true;
			}
		}
	}
	/* call fetch item */

	rc = fch_rod_item(env_info, rod_handle, dd_ref.object_index,
						req_mask, item, item_type);

	if (rc != DDS_SUCCESS)
	{
#ifdef _DEBUG
		::nsEDDEngine::ItemBase *flat=(::nsEDDEngine::ItemBase *)item;

		USES_CONVERSION;

		// TODO: Missing memory allocation release!!!

		//wchar_t* str=A2W(flat->GetTypeName());
		//TODO fixit
		wchar_t* str = nullptr;
		PS_MultiByteToWideChar(0, 0, flat->GetTypeName(), strlen(flat->GetTypeName()), str, 1000);
        wchar_t buf[1000]{0};

		::nsEDDEngine::SYMBOL_TBL_ELEM *sym=get_symbol_tbl_lookup(block_handle, env_info, local_ispec.item.id);
		wchar_t* symbol=_T("<symbol not found>");
		if(sym && sym->name)
			symbol=sym->name;

		if((rc==FETCH_INVALID_ATTRIBUTE) || (rc==FETCH_ATTR_ZERO_LENGTH))
			wsprintf(buf,_T("\n*** Error Fetch : this item contains extra invalid attributes : %-25s - item = %x, type = %s\n"), symbol, local_ispec.item.id, str);
		else
			wsprintf(buf,_T("\n*** Error ROD : could not find in DDEF_OBJ : %-25s - item = %x, type = %s\n"), symbol, local_ispec.item.id, str);

		PS_OutputDebugString(buf);

		if(rc != FETCH_INVALID_ATTRIBUTE)
			return rc;
#else
		return rc;
#endif

	}

	if(bListCountAdded)
	{
		req_mask -= nsEDDEngine::count;
	}
	
	if(bAxisMinValueAdded)
	{
		req_mask -= nsEDDEngine::min_value;
	}
	
	if(bAxisMaxValueAdded)
	{
		req_mask -= nsEDDEngine::max_value;
	}

	/* call eval */
	rc = eval_item(item, req_mask, env_info, &generic_item->errors, item_type);
	if (rc != DDS_SUCCESS) {

		return_code = rc;	/* save the return code */
	}


	if ( masks->dynamic.empty() ) {

		/*
		 * if there are no dynamics
		 */
		/*
		rc = get_depbin_list_ptrs(item_type, item, &depbins);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		if (depbins) {
			*depbins = NULL;
		}
		*/

		//masks->bin_hooked = 0;

	}
	else if (masks->dynamic == old_dynamic) {

		/*
		 * The dynamics have not changed
		 */

		//rc = clean_item(item_type, item, masks);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
	}
	else {

		/*
		 * New binarys need to be added
		 */

		/*
		rc = bin_saver(item_type, item, masks->dynamic, old_dynamic);
		if (rc != DDS_SUCCESS) {

			return rc;
		}

		rc = clean_item(item_type, item, masks);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
		*/
	}

#ifdef DEBUG
	dds_item_flat_check(generic_item->item, generic_item->item_type);
#endif

	return return_code;
}


