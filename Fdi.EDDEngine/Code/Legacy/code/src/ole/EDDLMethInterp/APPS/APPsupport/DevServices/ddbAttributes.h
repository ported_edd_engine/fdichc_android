/*************************************************************************************************
 *
 * $Workfile: ddbAttributes.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		8/9/2	sjv	created from attributes.h
 *		
 *	
 * #include "ddbAttributes.h"
 */


#ifndef _DDBATTRIBUTES_H
#define _DDBATTRIBUTES_H
#ifdef INC_DEBUG
#pragma message("In DDBattributes.h") 
#endif

#include <map>

#include "ddbGeneral.h"

#include "ddbdefs.h"
#include "ddbPrimatives.h"

#include "ddbGlblSrvInfc.h"
#include <3rdParty/panic.h>


#ifdef INC_DEBUG
#pragma message("    Finished Includes::DDBattributes.h") 
#endif


/********************* VARIABLE ATTRIBUTES BASE *******************/
class hCitemBase;

/*******************************************************************
 *  the attribute needs to know the itembase so the itembase's basedevice
 *  is usable by the attribute and its children
 */
class hCattrBase : public hCobject
{
protected:
	hCitemBase*	pItem;
	// due to no unique typing: upper 2 bytes of GenericType is item type, lower 2 are attr type 
	// This is NOT the mask! as earlier comments suggested.
	// This is a varAttrType_t,or a eddispAttrType_t or a etc 
	//			(must be long to handle the different enum types)
	//         filled hardcoded from the instantiation call
	ulong		attr_Type;

public:
	//  copy construction
	hCattrBase(const hCattrBase& aB) : hCobject(aB.devHndl()){}

	// raw construction
	hCattrBase(DevInfcHandle_t h, hCitemBase* pIB, ulong attrType = 0) :hCobject(h){}
	virtual ~hCattrBase(){};
	virtual RETURNCODE  destroy(void)   = 0;//force all to implement//was {return FAILURE;};
	void    setItemPtr(hCitemBase* p){}
	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL){return VIRTUALCALLERR; }
	ulong static makeGenericType(itemType_t it, unsigned short at){ return ((it<<16) | (at&0xffff));}
	ulong getGenericType(void){return 0;}
};

#define GETDESCNOTHERE( a ) LOGIT(CERR_LOG,"ERROR:getDescription NOT IMPLEMENTED in %s\n", a );\
							return FAILURE


/******************** LABEL **************************/
class hCattrLabel : public hCattrBase
{
protected:
	hCddlString Resolved;// temporary holder of resolved value

public:
	// default and up-Pointer constructor
	hCattrLabel(DevInfcHandle_t h, hCitemBase* pIB = NULL) : hCattrBase(h,pIB,varAttrLabel), 
				Resolved(h){}
	// copy constructor
	hCattrLabel(const hCattrLabel& sae) : hCattrBase(sae),Resolved(sae.devHndl())
	{ 
		attr_Type = sae.attr_Type;
	};
	// abstract constructor (from abstract class)
	hCattrLabel(DevInfcHandle_t h, const aCattrString* aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrLabel), Resolved(h)
		//{ condLabel = aCa.condLabel;};
	{
	};
	RETURNCODE destroy(void){ Resolved.destroy();return SUCCESS;};
	virtual ~hCattrLabel()
	{
		Resolved.destroy();
	}

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL)  
		{return SUCCESS;}

	RETURNCODE getString(wstring& retStr,unsigned int iID = 0)
	{ 
		RETURNCODE rc = FAILURE;
		if ( pItem != NULL )
		{
			Resolved.clear();
			{	// note that a conditional without a default||else stmt will exit here
				retStr = L"No String Defined";
				rc = SUCCESS;
			}
		}
		return rc;
	};	
	
	RETURNCODE getDepends(ddbItemList_t& retList) { return SUCCESS;}

};


//const CddlString (unsigned long Tag, unsigned int ddItem, unsigned long enumVal,unsigned int ddKey=0)  dfltUnit;
extern hCddlString dfltUnit;//see attrVars.cpp



/******************** VALIDITY **************************/
class hCattrValid : public hCattrBase
{
public:	
	typedef enum validity_e
	{
		v_INVALID = 0,
		v_VALID   = 0xffffffff
		/*EVERYTHING ELSE IS UNDEFINED*/
	}validity_t;
#define v_UNKNOWN ((validity_t)0x0f0f0f0f)

protected:
	RETURNCODE fetch(void);
	hCddlLong  Resolved;

	validity_t validity;

public:
	// default and up-Pointer constructor
	hCattrValid(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrValidity),Resolved(h),validity(v_UNKNOWN)
		{};
	// copy constructor
	hCattrValid(const hCattrValid& sae)// base handles hCobject
		:hCattrBase(sae) ,Resolved(sae.devHndl()){}
	// abstract constructor (from abstract class)
	hCattrValid(DevInfcHandle_t h, const aCattrCondLong* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrValidity),Resolved(h),validity(v_UNKNOWN){}
	RETURNCODE destroy(void){ return SUCCESS;}
	virtual ~hCattrValid(){}
	void* procure(bool isAtest = false);/*implementation moved to ddbVar.cpp*/
	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL){return FAILURE;}
	RETURNCODE getDescription(int oSet = 0){ GETDESCNOTHERE("CattrValid"); }
	RETURNCODE getDepends(ddbItemList_t& retList){ return SUCCESS; }
	bool isConditional(void) { return false; }
};


//VMKP Modified for Help conditional handling
class hCattrHelp : public hCattrBase	// was::CattrVarHelp, made it generic here
{
protected:
	hCddlString Resolved;
public:	
	// default and up-Pointer constructor
	hCattrHelp(DevInfcHandle_t h, hCitemBase* pIB = NULL):
				hCattrBase(h,pIB,varAttrHelp),Resolved(h) {	}
	// copy constructor
	hCattrHelp(const hCattrHelp& sae):
				hCattrBase(sae),Resolved(sae.devHndl())
		{ attr_Type = sae.attr_Type; }
		
	// abstract constructor (from abstract class)
	hCattrHelp(DevInfcHandle_t h, const aCattrString * aCa,hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrHelp),Resolved(h){}
	RETURNCODE destroy(void){ return Resolved.destroy(); }
	virtual ~hCattrHelp()
	{
		Resolved.destroy();
	}

	// returns ptr2ddlstring
	void* procure(void)
	{	Resolved.clear();
		return NULL;
	}

	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarHelp");};
	

	RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL) {return SUCCESS;}

	RETURNCODE getString(wstring& retStr,unsigned int iID = 0)
	{ 
		RETURNCODE rc = FAILURE;
		if ( pItem != NULL )
		{
			Resolved.clear();
			{	// note that a conditional without a default||else stmt will exit here
				retStr = L"No String Defined";
				rc = SUCCESS;
			}
		}
		return rc;
	}
};








/*======================= MIN-MAX VALUES ========================*/

class hCRangeItem
{
public:
	CValueVarient minVal{};
	CValueVarient maxVal{};

	hCRangeItem(){minVal.clear();maxVal.clear();};// sets 'em to invalid
	hCRangeItem(const hCRangeItem& s){operator=(s);};

	hCRangeItem& operator=(const hCRangeItem& s)
	{minVal=s.minVal;maxVal=s.maxVal;return *this;};
};

// map is used in case there is a sparse array of values
typedef map<int,hCRangeItem> RangeList_t;

class hCRangeList : public RangeList_t
{
public:
	hCRangeList(){RangeList_t::clear();};
	hCRangeList(const hCRangeList& s){ *this = s; };
};


class hCitemArray; // external def

class hCattrVarIndex : public hCattrBase
{	
private:
	hCitemArray *m_pItemArray;
public:
    hCattrVarIndex(DevInfcHandle_t h, ITEM_ID ItemID );
	hCattrVarIndex(DevInfcHandle_t h, const aCattrCondReference* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrIndexItemArray){}
	RETURNCODE destroy(void){ return SUCCESS;}
	virtual ~hCattrVarIndex();
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarIndex");}
    RETURNCODE getArrayPointer(hCitemBase*& pItemArray);
};


class hCattrVarDefault : public hCattrBase
{
protected:
	CValueVarient DefaultValue{};
public:
	hCattrVarDefault(DevInfcHandle_t h, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDefaultValue){}
	hCattrVarDefault(DevInfcHandle_t h, const aCattrCondExpr* aCa, hCitemBase* pIB = NULL)
		:hCattrBase(h,pIB,varAttrDefaultValue){}
	RETURNCODE destroy(void){ return SUCCESS;}
	RETURNCODE getDescription(int oSet = 0){GETDESCNOTHERE("CattrVarDefault");}
	RETURNCODE getDfltVal(CValueVarient& returnedValue);
	RETURNCODE setDfltVal(CValueVarient returnedValue){DefaultValue=returnedValue;return SUCCESS;}
};








/******************** METHOD SUPPORT  **************************/



class hCmethodParam
{
public:
	methodVarType_t		type;
	methodTypeFlag_t	flags;
	string				name;
public:
	hCmethodParam() :type(methodVarVoid),flags(methodType_NonFlaged),name(""){};
	hCmethodParam(const hCmethodParam& mp):type(mp.type),flags(mp.flags),name(mp.name){};
	hCmethodParam(const aCparameter*  aCa)
		:type((methodVarType_t)(aCa->type)),flags((methodTypeFlag_t)(aCa->modifiers)),
		 name(aCa->paramName){/* 3oct05 - arrays are ONLY by ref (as per wap)*///Anil  has changed on behalf of Steve
		if (flags & methodType_Array) 
			flags = (methodTypeFlag_t)(flags |(int)methodType_Reference);
	};

	RETURNCODE destroy(void) {name.erase(); return SUCCESS;};
	methodVarType_t		getType() { return type;};
	bool				isArray() { return ((flags & methodType_Array) != 0); };
	bool				isRef()   { return ((flags & methodType_Reference) != 0); };
	bool				isConst() { return ((flags & methodType_Const) != 0); };
	string&             getName() { return name; };
	/* DD is the only Setter */	
	hCmethodParam& operator=(const hCmethodParam& mP)
	{	type = mP.type;flags=mP.flags;name=mP.name; return *this;};
};

typedef vector<hCmethodParam> ParamList_t;


#endif//_ATTRIBUTES_H
