/**
 *	@(#) $Id: evl_dir.cpp,v 1.4 2012/10/04 23:50:57 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for evaluating a block or device
 *	directory.
 */


#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif
#include "ManualUnitLookup.h"

#define IMAGE_LANGUAGE_CODE_SIZE 6

typedef int (*eval_type)(void*, nsEDDEngine::BININFO*, ROD_HANDLE, ::nsEDDEngine::FLAT_DEVICE_DIR *);

extern void deobfuscate(char *str, int size, int ddtype);

/* FD_FDI externs */
UINT32 decode_UINT32(unsigned char **ptr, unsigned long *size);
void decode_BYTE_STRING(unsigned char **ptr, unsigned long *bin_size, char *str, int size, int ddtype);

/*********************************************************************
 *
 *	Name:	malloc_list
 *
 *	ShortDesc:	malloc a list of a certain size
 *
 *	Description:
 *		malloc a list of a certain size
 *
 *	Inputs:
 *		count -			element count
 *		element_size -	size of each element
 *
 *	Returns:
 *		malloc'd list
 *
 *
 *********************************************************************/

void *malloc_list(int *count, int element_size)
{
	void *v=malloc(	(size_t) ((*count) * element_size ) );

	if (v == NULL) {
		(*count)=0;
		return NULL;
	}

	memset((char *) v, 0,(size_t)(*count) * element_size);
	return v;
}

/*********************************************************************
 *
 *	Name:	ddl_decode_string
 *
 *	ShortDesc:	decodes a string
 *
 *	Description:
 *		decodes a string
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		string -		string
 *		ddtype -		type of dd used for obfuscation
 *
 *	Outputs:
 *		string -		string
 *
 *	Returns:
 *		error
 *
 *
 *********************************************************************/
int
ddl_decode_string(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::STRING       *string,
int ddtype)
{
	int numChars = 0;	// Number of wide characters
	DDL_UINT temp_int=0;

	int rc = DDL_SUCCESS;
	DDL_PARSE_INTEGER(chunkp, size, &temp_int);	// parse number of UTF-8 bytes

	wchar_t *tmp;
	if (temp_int > 0)	// This check allows there to be a NULL string in the table
	{
		if(temp_int>(*size))
		{
			return DDL_ENCODING_ERROR;
		}

		char *temp_chunk = (char *)malloc(temp_int);
		memcpy(temp_chunk,*chunkp,temp_int);
		::deobfuscate(temp_chunk,temp_int,ddtype);

		tmp = new wchar_t[temp_int];

		numChars = PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_chunk, temp_int, tmp/*string->str*/, temp_int);

		free(temp_chunk);

		if (numChars == 0)	// Error
		{
			return DDL_ENCODING_ERROR;
		}

		*string = tmp;
		delete []tmp; 
	}

	(*size)-=temp_int;
	(*chunkp)+=temp_int;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	ddl_decode_COMMAND_INDEX
 *
 *	ShortDesc:	decodes a COMMAND_INDEX
 *
 *	Description:
 *		decodes a COMMAND_INDEX
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *
 *********************************************************************/

int
ddl_decode_COMMAND_INDEX(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::COMMAND_INDEX       *element)
{
	int rc = DDL_SUCCESS;

	DDL_PARSE_INTEGER(chunkp, size, &element->id);	
	DDL_PARSE_INTEGER(chunkp, size, &element->value);	

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	ddl_decode_COMMAND_ELEM
 *
 *	ShortDesc:	decodes a COMMAND_TBL_ELEM
 *
 *	Description:
 *		decodes a COMMAND_TBL_ELEM
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/
int
ddl_decode_COMMAND_ELEM(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::COMMAND_TBL_ELEM       *element)
{
	int rc = DDL_SUCCESS;

	if(!element)
		return DDL_MEMORY_ERROR;

	memset(element,0,sizeof(nsEDDEngine::COMMAND_TBL_ELEM));

	DDL_PARSE_INTEGER(chunkp, size,(ulong *) &element->command_number);	

	DDL_PARSE_INTEGER(chunkp, size,(ulong *) &element->command_item_info_index);	

	DDL_PARSE_INTEGER(chunkp, size,(ulong *) &element->transaction_number);	

	DDL_PARSE_INTEGER(chunkp, size,(ulong *) &element->weight);	

	DDL_PARSE_INTEGER(chunkp, size,(ulong *) &element->count);	
	
	if(element->count)
	{
		element->index_list=(nsEDDEngine::COMMAND_INDEX*)malloc_list(&element->count, sizeof(nsEDDEngine::COMMAND_INDEX));
		if(!element->index_list)
			return DDL_MEMORY_ERROR;

		for(int i=0; i<element->count; i++)
		{
			rc=ddl_decode_COMMAND_INDEX(chunkp,size,&(element->index_list[i]));
			if(rc)
				return rc;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	ddl_decode_RESOLVED_REFERENCE
 *
 *	ShortDesc:	decodes a RESOLVED_REFERENCE
 *
 *	Description:
 *		decodes a RESOLVED_REFERENCE
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/
int
ddl_decode_RESOLVED_REFERENCE(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::RESOLVED_REFERENCE   *element)
{

	int rc = DDL_SUCCESS;
	DDL_UINT temp_int=0;
	DDL_UINT temp_tag;		/* the tag value */

	DDL_UINT *pTempBuffer = new DDL_UINT [ *size ];		// Create temp buffer bigger than we need.
	int iBuffUsed = 0;

	while (*size > 0)
	{
		DDL_PARSE_TAG(chunkp, size, &temp_tag,  &temp_int);

		if(temp_tag==ITEM_ID_REF)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);
			pTempBuffer[iBuffUsed++] = temp_int;
			break;
		}
		else if(temp_tag==VIA_RESOLVED_REF)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			pTempBuffer[iBuffUsed++] = temp_int;
		}
		else
		{
			return DDL_ENCODING_ERROR;
		}
	}

	for (int i=0; i < iBuffUsed/2; i++)			// Reverse the list
	{
		temp_int = pTempBuffer[i];
		pTempBuffer[i] = pTempBuffer[iBuffUsed-i-1];
		pTempBuffer[iBuffUsed-i-1] = temp_int;
	}
	
	element->Set(pTempBuffer, iBuffUsed);

	delete [] pTempBuffer;

	return DDL_SUCCESS;
}
/*********************************************************************
 *
 *	Name:	ddl_decode_ITEM_TO_COMMAND_TBL_ELEM
 *
 *	ShortDesc:	decodes a ITEM_TO_COMMAND_TBL_ELEM
 *
 *	Description:
 *		decodes a ITEM_TO_COMMAND_TBL_ELEM
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/
int
ddl_decode_ITEM_TO_COMMAND_TBL_ELEM(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM       *element)
{
	int rc = DDL_SUCCESS;
	DDL_UINT temp_int=0;

	if(!element)
		return DDL_MEMORY_ERROR;

	memset(element,0,sizeof(nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM));

	rc=ddl_decode_RESOLVED_REFERENCE(chunkp, size, &element->resolved_ref);
	if(rc)
		return rc;

	DDL_PARSE_INTEGER(chunkp, size, (ulong *)&element->number_of_read_commands);	

	if(element->number_of_read_commands)
	{
		element->read_command_list = (nsEDDEngine::COMMAND_TBL_ELEM *) malloc_list(&element->number_of_read_commands,sizeof(nsEDDEngine::COMMAND_TBL_ELEM));

		if (!element->read_command_list) 
			return DDL_MEMORY_ERROR;

		for(int i=0;i<element->number_of_read_commands;i++)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->read_command_list[i].command_number=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->read_command_list[i].command_item_info_index=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->read_command_list[i].transaction_number=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->read_command_list[i].weight=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->read_command_list[i].count=temp_int;

			if(element->read_command_list[i].count>0)
			{
				element->read_command_list[i].index_list=(nsEDDEngine::COMMAND_INDEX *) malloc_list(&element->read_command_list[i].count,sizeof(nsEDDEngine::COMMAND_INDEX));

				for(int j=0;j<element->read_command_list[i].count;j++)
				{
					DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
					element->read_command_list[i].index_list[j].id=temp_int;
					DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
					element->read_command_list[i].index_list[j].value=temp_int;
				}
			}
		}
	}

	DDL_PARSE_INTEGER(chunkp, size, (ulong *)&element->number_of_write_commands);	

	if(element->number_of_write_commands)
	{
		element->write_command_list = (nsEDDEngine::COMMAND_TBL_ELEM *) malloc_list(&element->number_of_write_commands,sizeof(nsEDDEngine::COMMAND_TBL_ELEM));

		if (!element->write_command_list) 
			return DDL_MEMORY_ERROR;

		for(int i=0;i<element->number_of_write_commands;i++)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->write_command_list[i].command_number=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->write_command_list[i].command_item_info_index=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->write_command_list[i].transaction_number=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->write_command_list[i].weight=temp_int;
			DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
			element->write_command_list[i].count=temp_int;

			if(element->write_command_list[i].count>0)
			{
				element->write_command_list[i].index_list=(nsEDDEngine::COMMAND_INDEX *) malloc_list(&element->write_command_list[i].count,sizeof(nsEDDEngine::COMMAND_INDEX));

				for(int j=0;j<element->write_command_list[i].count;j++)
				{
					DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
					element->write_command_list[i].index_list[j].id=temp_int;
					DDL_PARSE_INTEGER(chunkp, size, &temp_int);	
					element->write_command_list[i].index_list[j].value=temp_int;
				}
			}
		}

	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_string_tbl
 *	ShortDesc: evaluate the STRING_TBL
 *
 *	Description:
 *		eval_dir_string_tbl will load the STRING_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		string_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_string_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{
	nsEDDEngine::STRING_TBL* string_tbl = static_cast<nsEDDEngine::STRING_TBL*>(tableP);

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_STRING_TBL);
#endif

	string_tbl->root = NULL;	// Initialize STRING_TBL
	string_tbl->count = 0;
	string_tbl->list = NULL;

	int rc = DDL_SUCCESS;
	DDL_UINT temp_int = 0;		// integer value from binary

	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);	// parse number of strings

	if (temp_int == 0)			// No strings to load
	{
		return DDL_SUCCESS;
	}

	/* create the list */
	string_tbl->list = new nsEDDEngine::STRING[temp_int];
	if (string_tbl->list == NULL)
	{
		return DDL_MEMORY_ERROR;
	}

	string_tbl->count = (int) temp_int;

	/* malloc the string buffer (number of bytes in binary + a null for each string */
	int iNumRootChars = bin->size + temp_int;
	string_tbl->root = (wchar_t *) malloc(iNumRootChars * sizeof(wchar_t));
	if (string_tbl->root == NULL)
	{
		return DDL_MEMORY_ERROR;
	}

	/* load the list */

	wchar_t *root_ptr = string_tbl->root;		// temp pointer
	nsEDDEngine::STRING *string = string_tbl->list;	 	// temp pointer for the loop
	nsEDDEngine::STRING *end_string = string + temp_int;	// end pointer for the loop
	for ( ; string < end_string; string++)
	{
		int numChars = 0;	// Number of wide characters

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);	// parse number of UTF-8 bytes

		if (temp_int > 0)	// This check allows there to be a NULL string in the table
		{
			char *temp_chunk = (char *)malloc(temp_int);
			memcpy(temp_chunk,bin->chunk,temp_int);

			::deobfuscate(temp_chunk,temp_int,flat_device_dir->header.edd_profile);

			numChars = PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_chunk, temp_int,
														root_ptr, iNumRootChars);
			free(temp_chunk);

			if (numChars == 0)	// Error
			{
				return DDL_ENCODING_ERROR;
			}
		}

		root_ptr[numChars] = L'\0';	// Null terminate

		string->assign(root_ptr,false);

		bin->chunk += temp_int;		// Move bin pointer forward past string
		bin->size  -= temp_int;

		root_ptr      += numChars + 1;	// Move root pointer forward (include the NULL)
		iNumRootChars -= numChars + 1;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	ASSERT_DBG(iNumRootChars >= 0);		// Make sure we didn't fall off the end

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_image_tbl
 *	ShortDesc: evaluate the IMAGE_TBL
 *
 *	Description:
 *		eval_dir_imag_tbl will load the IMAGE_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		image_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

static
int eval_dir_image_tbl(			// Make like FF (see eval_dir_binary_tbl)
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE ,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{
	nsEDDEngine::IMAGE_TBL* image_tbl = static_cast<nsEDDEngine::IMAGE_TBL*>(tableP);
	nsEDDEngine::IMAGE_LIST	*element;	  /* temp pointer for the list */
	DDL_UINT    	temp_int;		/* integer value */
	int				rc;				/* return code */

	if(!image_tbl)
		return DDL_MEMORY_ERROR;

	ASSERT_DBG(bin && bin->chunk && bin->size);
	//The image table should only be evaluated once!
	ASSERT_DBG(!image_tbl->count && !image_tbl->list);

	memset(image_tbl,0,sizeof(nsEDDEngine::IMAGE_TBL));

#ifdef DDSTEST
	TEST_FAIL(EVAL_IMAGE_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, (ulong *)&image_tbl->count);

	if(image_tbl->count)
	{
		image_tbl->list=(nsEDDEngine::IMAGE_LIST *)malloc_list(&image_tbl->count,sizeof(nsEDDEngine::IMAGE_LIST));

		for(int i=0;i<image_tbl->count;i++)
		{
			element=&image_tbl->list[i];

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->count=element->number_of_languages = temp_int;

			if(!element->number_of_languages)
				continue;

			element->list=(nsEDDEngine::IMAGE_ITEM *)malloc_list(&element->number_of_languages,sizeof(nsEDDEngine::IMAGE_ITEM));

			if(!element->list)
				return DDL_MEMORY_ERROR;

			for(int j=0;j<element->number_of_languages;j++)
			{
				nsEDDEngine::IMAGE_ITEM *im = &element->list[j];

				::decode_BYTE_STRING(&bin->chunk, &bin->size, im->language_code, 6, flat_device_dir->header.edd_profile);

				temp_int=::decode_UINT32(&bin->chunk, &bin->size);
				im->image_file_offset=temp_int;

				temp_int=::decode_UINT32(&bin->chunk, &bin->size);
				im->image_file_length=temp_int;
			}
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_item_tbl
 *	ShortDesc: evaluate the ITEM_TBL
 *
 *	Description:
 *		eval_dir_item_tbl will load the ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_item_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::ITEM_TBL* item_tbl = static_cast<nsEDDEngine::ITEM_TBL*>(tableP);

	nsEDDEngine::ITEM_TBL_ELEM	*element;	  /* temp pointer for the list */
	nsEDDEngine::ITEM_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT       	temp_int = 0;	  /* integer value */
	int				rc = 0;			  /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ITEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		item_tbl->count = 0;
		item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	item_tbl->count = (int) temp_int;

	/* malloc the list */
	item_tbl->list = (nsEDDEngine::ITEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::ITEM_TBL_ELEM) ) );

	if (item_tbl->list == NULL) {

		item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) item_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		//PS_TRACE(L"id=%x\n", element->item_id);

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->symbol_index = (ITEM_TYPE) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_type = (nsEDDEngine::ITEM_TYPE) temp_int;

		temp_int=::decode_UINT32(&bin->chunk, &bin->size);
		element->item_offset = (ITEM_TYPE) temp_int;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_dict_ref_tbl
 *	ShortDesc: evaluate the DICT_REF_TBL
 *
 *	Description:
 *		eval_dir_dict_ref_tbl will load the DICT_REF_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		dict_ref_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_dict_ref_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{
	nsEDDEngine::DICT_REF_TBL* dict_ref_tbl = static_cast<nsEDDEngine::DICT_REF_TBL*>(tableP);

	nsEDDEngine::DICT_TBL_ELEM		*element;	 			/* temp pointer for the list */
	nsEDDEngine::DICT_TBL_ELEM		*end_element;			/* end pointer for the list */
	DDL_UINT 	temp_int = 0;	 			/* integer value */
	int			rc = 0;						/* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DICT_REF_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		dict_ref_tbl->count = 0;
		dict_ref_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	dict_ref_tbl->count = (int) temp_int;

	/* malloc the list */
	dict_ref_tbl->list = (nsEDDEngine::DICT_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::DICT_TBL_ELEM) ) );

	if (dict_ref_tbl->list == NULL) {

		dict_ref_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) dict_ref_tbl->list, 0,(size_t) temp_int * sizeof(nsEDDEngine::DICT_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = dict_ref_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

 		if(bin->size<=0)
			return DDL_ENCODING_ERROR;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dictionary_id=temp_int;

		rc=ddl_decode_string(&bin->chunk, &bin->size, &element->name,flat_device_dir->header.edd_profile);
		if(rc)
			return rc;

		rc=ddl_decode_string(&bin->chunk, &bin->size, &element->dictionary_entry,flat_device_dir->header.edd_profile);
		if(rc)
			return rc;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_symbol_tbl
 *	ShortDesc: evaluate the SYMBOL_TBL
 *
 *	Description:
 *		eval_dir_symbol_tbl will load the SYMBOL_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		symbol_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Timir Panchal
 *
 **********************************************************************/

static
int eval_dir_symbol_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{
	nsEDDEngine::SYMBOL_TBL* symbol_tbl = static_cast<nsEDDEngine::SYMBOL_TBL*>(tableP);

	nsEDDEngine::SYMBOL_TBL_ELEM		*element;	 			/* temp pointer for the list */
	nsEDDEngine::SYMBOL_TBL_ELEM		*end_element;			/* end pointer for the list */
	DDL_UINT 	temp_int = 0;		 				/* integer value */
	int			rc = 0;								/* return code */
	DDL_UINT	temp_tag = 0;

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DICT_REF_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */
	if(!temp_int) {

		symbol_tbl->count = 0;
		symbol_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	symbol_tbl->count = (int) temp_int;

	/* malloc the list */
	symbol_tbl->list = (nsEDDEngine::SYMBOL_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::SYMBOL_TBL_ELEM) ) );

	if (symbol_tbl->list == NULL) {

		symbol_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) symbol_tbl->list, 0,(size_t) temp_int * sizeof(nsEDDEngine::SYMBOL_TBL_ELEM));

	for (element = symbol_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->symbol_id = temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->type = temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->subtype = temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_info_index = temp_int;

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &temp_tag,  &temp_int);

		if((ASCII_STRING_TAG!=61) || (temp_int<=0))
			return DDL_ENCODING_ERROR;

		//get the path
		if (temp_int > 0)	
		{
			char *temp_chunk = (char *)malloc(temp_int);
			memcpy(temp_chunk,bin->chunk,temp_int);

			::deobfuscate(temp_chunk,temp_int,flat_device_dir->header.edd_profile);

			element->name = (wchar_t *) malloc((temp_int+1) * sizeof(wchar_t));

			int numChars = PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_chunk, temp_int,
											element->name, temp_int+1);

			free(temp_chunk);

			if (numChars == 0)	// Error
			{
				return DDL_ENCODING_ERROR;
			}
			else
			{
				element->name[numChars] = L'\0';
			}
		}

		bin->chunk += temp_int;		// Move bin pointer forward past string
		bin->size  -= temp_int;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_blk_tbl
 *	ShortDesc: evaluate the BLK_TBL
 *
 *	Description:
 *		eval_dir_blk_tbl will load the BLK_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::BLK_TBL* blk_tbl = static_cast<nsEDDEngine::BLK_TBL*>(tableP);

	nsEDDEngine::BLK_TBL_ELEM	*element;	 /* temp pointer for the list */
//	BLK_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int = 0;	 /* integer value */
	int				rc = 0;			 /* return code */

	if(!blk_tbl)
		return DDL_MEMORY_ERROR;

	memset(blk_tbl,0,sizeof(nsEDDEngine::BLK_TBL));

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_BLK_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, (ulong *)&blk_tbl->count);

	if(!blk_tbl->count) 
		return DDL_SUCCESS;

	if(blk_tbl->count)
	{
		blk_tbl->list=(nsEDDEngine::BLK_TBL_ELEM *)malloc_list(&blk_tbl->count, sizeof(nsEDDEngine::BLK_TBL_ELEM));
		if(!blk_tbl->list)
			return DDL_MEMORY_ERROR;

		/*
		ITEM_ID				blk_id;
		int					block_item_info_index;
		int					charRec_item_info_index;
		int					charRec_block_item_name_index;
		int					block_tables_object_index;
		*/
		for(int i=0; i<blk_tbl->count; i++)
		{
			element = &blk_tbl->list[i];

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_id = (ITEM_ID) temp_int;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_tbl_offset = temp_int;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->char_rec_item_tbl_offset =  temp_int;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->char_rec_bint_offset =  temp_int;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_dir_dd_ref.object_index = (OBJECT_INDEX)temp_int;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_rel_tbl
 *	ShortDesc: evaluate the REL_TBL
 *
 *	Description:
 *		eval_dir_rel_tbl will load the REL_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		rel_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_rel_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::REL_TBL* rel_tbl = static_cast<nsEDDEngine::REL_TBL*>(tableP);

	nsEDDEngine::REL_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::REL_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int = 0;	 /* integer value */
	int				rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_REL_TBL);
#endif


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		rel_tbl->count = 0;
		rel_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	rel_tbl->count = (int) temp_int;

	/* malloc the list */
	rel_tbl->list = (nsEDDEngine::REL_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::REL_TBL_ELEM)));

	if (rel_tbl->list == NULL) {

		rel_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) rel_tbl->list, 0,(size_t) temp_int * sizeof(nsEDDEngine::REL_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = rel_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->wao_item_tbl_offset = -1;
		}
		else {
			element->wao_item_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->unit_item_tbl_offset = -1;
		}
		else {
			element->unit_item_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->update_tbl_offset = -1;
		}
		else {
			element->update_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->update_count = (int) temp_int;

	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_update_tbl
 *	ShortDesc: evaluate the UPDATE_TBL
 *
 *	Description:
 *		eval_dir_update_tbl will load the UPDATE_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		update_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_update_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{

//	ASSERT(0);/// not encoded properly

	nsEDDEngine::UPDATE_TBL* update_tbl = static_cast<nsEDDEngine::UPDATE_TBL*>(tableP);

	nsEDDEngine::UPDATE_TBL_ELEM	*element;			/* temp pointer for the list */
	nsEDDEngine::UPDATE_TBL_ELEM	*end_element;		/* end pointer for the list */
	DDL_UINT 		temp_int = 0;	 		/* integer value */
	int				rc = 0;					/* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_UPDATE_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		update_tbl->count = 0;
		update_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	update_tbl->count = (int) temp_int;

	/* malloc the list */
	update_tbl->list = (nsEDDEngine::UPDATE_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::UPDATE_TBL_ELEM) ) );

	if (update_tbl->list == NULL) {

		update_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) update_tbl->list, 0,(size_t) temp_int * sizeof(nsEDDEngine::UPDATE_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = update_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->desc_it_offset = (int) temp_int;

		rc=ddl_decode_RESOLVED_REFERENCE(&bin->chunk, &bin->size, &element->to_be_updated);
		if(rc)
			return rc;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_command_tbl
 *	ShortDesc: evaluate the COMMAND_TBL
 *
 *	Description:
 *		eval_dir_command_tbl will load the COMMAND_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		command_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_item_to_command_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * flat_device_dir)
{
	nsEDDEngine::ITEM_TO_COMMAND_TBL* item_to_command_tbl = static_cast<nsEDDEngine::ITEM_TO_COMMAND_TBL*>(tableP);
	nsEDDEngine::COMPLEX_ITEM_TO_CMD_TBL* complex_item_to_cmd_tbl = &flat_device_dir->complex_item_to_cmd_tbl;

	nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM	*element;			/* temp pointer for the list of commands*/
	nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM	*end_element;		/* end pointer for the list of commands */
	DDL_UINT 			temp_uint = 0; 		    /* unsigned integer value */
	int					rc = 0;				    /* return code */
	int					complexCount = 0; // count of how many complex references in the item to command table
	ASSERT_DBG(bin && bin->chunk && bin->size);


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_uint);
	
	/*
	 * if count is zero
	 */

	if(!temp_uint) {

		item_to_command_tbl->count = 0;
		item_to_command_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	item_to_command_tbl->count = (int) temp_uint;

	/* malloc the list */
	item_to_command_tbl->list = (nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM *) malloc((size_t) (temp_uint * sizeof(nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM) ) );

	if (item_to_command_tbl->list == NULL) {
		item_to_command_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) item_to_command_tbl->list, 0,(size_t) temp_uint * sizeof(nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = item_to_command_tbl->list, end_element = element + temp_uint;
		 element < end_element; element++) {
			
		rc=ddl_decode_ITEM_TO_COMMAND_TBL_ELEM(&bin->chunk, &bin->size,element);
		
		if(rc)
		{
			return rc;
		}

		//update complexCount, encountered a reference that is complex - array/list element
		if(element->resolved_ref.size > 1)
		{
			complexCount++;
		}

	}
	
	// load the complex references in to the list( complex_item_to_command_table )
	if(!complexCount) 
	{
		complex_item_to_cmd_tbl->count = 0;
		complex_item_to_cmd_tbl->list = NULL;
	}
	else
	{
		complex_item_to_cmd_tbl->count = complexCount;

		/* malloc the list */
		complex_item_to_cmd_tbl->list = (nsEDDEngine::COMPLEX_ITEM_TO_CMD_TBL_ELEM *) malloc((size_t) (complexCount * sizeof(nsEDDEngine::COMPLEX_ITEM_TO_CMD_TBL_ELEM) ) );

		if (complex_item_to_cmd_tbl->list == NULL) {
			complex_item_to_cmd_tbl->count = 0;
			return DDL_MEMORY_ERROR;
		}

		/* load list with zeros */
		memset((char *) complex_item_to_cmd_tbl->list, 0,(size_t) complexCount * sizeof(nsEDDEngine::COMPLEX_ITEM_TO_CMD_TBL_ELEM));
		int j = 0;
		int size = 0;
		for( int i = 0; i < item_to_command_tbl->count; i++)
		{
			size = item_to_command_tbl->list[i].resolved_ref.size;
			if( size > 1 )
			{
				complex_item_to_cmd_tbl->list[j].item_to_command_table_offset = i;
				complex_item_to_cmd_tbl->list[j].resolved_ref = item_to_command_tbl->list[i].resolved_ref;
				j++;
			}
		}
	}
	
	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_command_number_tbl
 *	ShortDesc: evaluate the command_number_tbl
 *
 *	Description:
 *		eval_dir_command_tbl will load the COMMAND_NUMBER_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		command_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *
 **********************************************************************/
static
int eval_dir_command_number_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::COMMAND_NUMBER_TBL* cmd_num_tbl = static_cast<nsEDDEngine::COMMAND_NUMBER_TBL*>(tableP);

	nsEDDEngine::COMMAND_NUMBER_TBL_ELEM	*element;	 /* temp pointer for the list */
//	COMMAND_NUMBER_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int = 0;	 /* integer value */
	int				rc = 0;			 /* return code */

	if(!cmd_num_tbl)
		return DDL_MEMORY_ERROR;

	memset(cmd_num_tbl,0,sizeof(nsEDDEngine::COMMAND_NUMBER_TBL));

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_BLK_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
	cmd_num_tbl->count=temp_int;

	if(cmd_num_tbl->count)
	{
		cmd_num_tbl->list=(nsEDDEngine::COMMAND_NUMBER_TBL_ELEM *)malloc_list(&cmd_num_tbl->count, sizeof(nsEDDEngine::COMMAND_NUMBER_TBL_ELEM));
		if(!cmd_num_tbl->list)
			return DDL_MEMORY_ERROR;

		for(int i=0;i<cmd_num_tbl->count;i++)
		{
			element = &cmd_num_tbl->list[i];

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->command_number = (int) temp_int;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_info_index = (int) temp_int;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_crit_param_tbl
 *
 *	ShortDesc: evaluate the CRIT_PARAM_TBL
 *
 *	Description:
 *		eval_dir_crit_param_tbl will load the CRIT_PARAM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		crit_param_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
eval_dir_crit_param_tbl(
void*		 tableP ,
nsEDDEngine::BININFO*	 bin ,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::CRIT_PARAM_TBL* crit_param_tbl = static_cast<nsEDDEngine::CRIT_PARAM_TBL*>(tableP);
	
	nsEDDEngine::RESOLVED_REFERENCE* element;
	nsEDDEngine::RESOLVED_REFERENCE* end_element;
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_BLK_ITEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		crit_param_tbl->count = 0;
		crit_param_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	crit_param_tbl->count = (int) temp_int;

	/* malloc the list */
	crit_param_tbl->list = (nsEDDEngine::RESOLVED_REFERENCE *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::RESOLVED_REFERENCE) ) );

	if (crit_param_tbl->list == NULL) {

		crit_param_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) crit_param_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::RESOLVED_REFERENCE));

	/*
	 * load the list
	 */

	for (element = crit_param_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */
		rc=ddl_decode_RESOLVED_REFERENCE(&bin->chunk, &bin->size, element);
		if(rc)
			return rc;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_tbl
 *	ShortDesc: evaluate the PARAM_TBL
 *
 *	Description:
 *		eval_dir_param_tbl will load the PARAM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_TBL* param_tbl = static_cast<nsEDDEngine::PARAM_TBL*>(tableP);

	nsEDDEngine::PARAM_TBL_ELEM	*element;	 		/* temp pointer for the list */
	DDL_UINT        temp_int = 0;			/* integer value */
	DDL_UINT_LONG   temp_long = 0;			/* long value */
	DDL_UINT		count = 0;				/* element count */
	DDL_UINT		tag = 0;				/* the tag value */
	int				rc = 0;					/* return code */
	unsigned long	inc = 0;			/* element tracker */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);

	/*
	 * if count is zero
	 */

	if(!count) {

		param_tbl->count = 0;
		param_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_tbl->count = (int) count;

	/* malloc the list */
	param_tbl->list = (nsEDDEngine::PARAM_TBL_ELEM *)malloc((size_t) (count * sizeof(nsEDDEngine::PARAM_TBL_ELEM)));

	if (param_tbl->list == NULL) {

		param_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_tbl->list, 0,	(size_t) count * sizeof(nsEDDEngine::PARAM_TBL_ELEM));

	/*
	 * load the list
	 */

	element = param_tbl->list;


	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {


		case PT_BLK_ITEM_NAME_TBL_OFFSET_TAG:

			/*
			 * Increment the element pointer
			 */

			if (inc > 0) {

				if(inc == count) {

					return DDL_ENCODING_ERROR;
				}
				element++;
			}
			++inc;

			/*
			 * Set optional table offsets to -1
		     */
			element->param_mem_tbl_offset = (int) -1;
			element->param_elem_tbl_offset = (int) -1;
			element->array_elem_item_tbl_offset = (int) -1;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_MEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_mem_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_MEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_mem_count = (int) temp_int;
			break;

		case PT_PARAM_ELEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_elem_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_ELEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_elem_count = (int) temp_int;
			break;

		case PT_PARAM_ELEM_MAX_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_count = (int) temp_int;
			break;

		case PT_ARRAY_ELEM__ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_item_tbl_offset = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_TYPE_OR_VAR_TYPE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_type_or_var_type = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_SIZE_OR_VAR_SIZE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_size_or_var_size = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_CLASS_VAR_CLASS_TAG:
			DDL_PARSE_INTEGER_LONG(&bin->chunk, &bin->size, &temp_long);
			element->array_elem_class_or_var_class = temp_long;
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_blk_item_tbl
 *	ShortDesc: evaluate the BLK_ITEM_TBL
 *
 *	Description:
 *		eval_dir_blk_item_tbl will load the BLK_ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::BLK_ITEM_TBL* blk_item_tbl = static_cast<nsEDDEngine::BLK_ITEM_TBL*>(tableP);

	nsEDDEngine::BLK_ITEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::BLK_ITEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_BLK_ITEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		blk_item_tbl->count = 0;
		blk_item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_tbl->count = (int) temp_int;

	/* malloc the list */
	blk_item_tbl->list = (nsEDDEngine::BLK_ITEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::BLK_ITEM_TBL_ELEM) ) );

	if (blk_item_tbl->list == NULL) {

		blk_item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) blk_item_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::BLK_ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = blk_item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_id = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;		
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_param_mem_name_tbl will load the PARAM_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_name_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_MEM_NAME_TBL* param_mem_name_tbl = static_cast<nsEDDEngine::PARAM_MEM_NAME_TBL*>(tableP);

	nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_MEM_NAME_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_name_tbl->count = 0;
		param_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_name_tbl->list = (nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM) ) );

	if (param_mem_name_tbl->list == NULL) {

		param_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_mem_name_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_mem_name_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_name = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_offset = (int) temp_int;		
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_list_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_LIST_TBL* param_elem_tbl = static_cast<nsEDDEngine::PARAM_LIST_TBL*>(tableP);

	nsEDDEngine::PARAM_LIST_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::PARAM_LIST_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::PARAM_LIST_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_LIST_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::PARAM_LIST_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_offset = (int) temp_int;		
		if (temp_int == UNUSED_OFFSET) {
			element->blk_item_name_offset = -1;
		}

		/* parse relation table offset */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_offset = (int) temp_int;
		if (temp_int == UNUSED_OFFSET) {
			element->param_list_mem_offset = -1;
		}

		/* parse relation table offset */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_count = (int) temp_int;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_LIST_MEM_TBL* param_elem_tbl = static_cast<nsEDDEngine::PARAM_LIST_MEM_TBL*>(tableP);

	nsEDDEngine::PARAM_LIST_MEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::PARAM_LIST_MEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::PARAM_LIST_MEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_LIST_MEM_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::PARAM_LIST_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_offset = (int) temp_int;		
		if (temp_int == UNUSED_OFFSET) {
			element->blk_item_name_offset = -1;
		}

	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_name_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_LIST_MEM_NAME_TBL* param_elem_tbl = static_cast<nsEDDEngine::PARAM_LIST_MEM_NAME_TBL*>(tableP);

	nsEDDEngine::PARAM_LIST_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::PARAM_LIST_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::PARAM_LIST_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_LIST_MEM_NAME_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::PARAM_LIST_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_name = (ITEM_ID) temp_int;		

		/* parse relation table offset */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->blk_item_name_offset = -1;
		}
		else {
			element->blk_item_name_offset = (int) temp_int;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_elem_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_elem_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_ELEM_TBL* param_elem_tbl = static_cast<nsEDDEngine::PARAM_ELEM_TBL*>(tableP);

	nsEDDEngine::PARAM_ELEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::PARAM_ELEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::PARAM_ELEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_ELEM_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::PARAM_ELEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_elem_subindex = (SUBINDEX) temp_int;		


		/* parse relation table offset */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_char_mem_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_char_mem_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::CHAR_MEM_TBL* param_elem_tbl = static_cast<nsEDDEngine::CHAR_MEM_TBL*>(tableP);

	nsEDDEngine::CHAR_MEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::CHAR_MEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::CHAR_MEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::CHAR_MEM_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {
		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::CHAR_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;		
		if (temp_int == UNUSED_OFFSET)
			element->item_tbl_offset = -1;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_type = (int) temp_int;		

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_size = (int) temp_int;		

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_class = (ulong) temp_int;		

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->rel_tbl_offset = (int) temp_int;		
		if (temp_int == UNUSED_OFFSET)
			element->rel_tbl_offset = -1;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_char_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_char_mem_name_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::CHAR_MEM_NAME_TBL* param_elem_tbl = static_cast<nsEDDEngine::CHAR_MEM_NAME_TBL*>(tableP);

	nsEDDEngine::CHAR_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	nsEDDEngine::CHAR_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int = 0;	 /* integer value */
	int					rc = 0;			 /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (nsEDDEngine::CHAR_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::CHAR_MEM_NAME_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(nsEDDEngine::CHAR_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_name = (ITEM_ID) temp_int;		

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_offset = (int) temp_int;		
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_blk_item_name_tbl
 *	ShortDesc: evaluate the BLK_ITEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_blk_item_name_tbl will load the BLK_ITEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_name_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::BLK_ITEM_NAME_TBL* blk_item_name_tbl = static_cast<nsEDDEngine::BLK_ITEM_NAME_TBL*>(tableP);

	nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM	*element=NULL;	// temp pointer for the list 
	DDL_UINT				count = 0;		// element count 
	DDL_UINT				tag = 0;		// the tag value 
	DDL_UINT       			temp_int = 0;	// integer value 
	int						rc = 0;			// return code 
	unsigned long			inc = 0;	// element tracker 
//	DDL_UINT				tag = -1;	// temporary tag 
	
	if(!blk_item_name_tbl)
		return DDL_MEMORY_ERROR;
	
	ASSERT_DBG(bin && bin->chunk && bin->size);
	
	memset(blk_item_name_tbl,0,sizeof(nsEDDEngine::BLK_ITEM_NAME_TBL));

#ifdef DDSTEST
	TEST_FAIL(EVAL_BLK_ITEM_NAME_TBL);
#endif

	//	fd_fdi RESURRECT THE CODE BELOW
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, (ulong *)&count);

	if(!count) {
		blk_item_name_tbl->count = 0;
		blk_item_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_name_tbl->count = (int) count;

	// malloc the list
	blk_item_name_tbl->list = (nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM *)malloc((size_t)(count * sizeof(nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM) ) );

	if (blk_item_name_tbl->list == NULL) {

		blk_item_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	// load list with zeros 
	memset((char *) blk_item_name_tbl->list, 0,	(size_t) count * sizeof(nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM));

	// load the list
	element = blk_item_name_tbl->list;

	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {

		case BINT_BLK_ITEM_NAME_TAG:
			
			// Increment the element pointer
			if (inc > 0) {
				if(inc == count) {
					return DDL_ENCODING_ERROR;
				}
				++element;
			}
			++inc;
		
			//* Set optional table offsets to -1
			element->item_tbl_offset = (int) -1;
			element->param_tbl_offset = (int) -1;
			element->rel_tbl_offset = (int) -1;
			element->item_to_command_tbl_offset=(int)-1;
			element->param_list_tbl_offset=(int)-1;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name = (ITEM_ID) temp_int;
			break;

		case BINT_ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_LIST_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_list_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_tbl_offset = (int) temp_int;
			break;

		case BINT_REL_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->rel_tbl_offset = (int) temp_int;
			break;

		case BINT_ITEM_TO_CMD_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_to_command_tbl_offset = (int) temp_int;
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
	
}


/*********************************************************************
 *
 *	Name: eval_dir_param_mem_tbl
 *	ShortDesc: evaluate the PARAM_MEM_TBL
 *
 *	Description:
 *		eval_dir_param_mem_tbl will load the PARAM_MEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_tbl(
void*		tableP,
nsEDDEngine::BININFO*	bin,
ROD_HANDLE,
nsEDDEngine::FLAT_DEVICE_DIR * /* flat_device_dir */)
{
	nsEDDEngine::PARAM_MEM_TBL* param_mem_tbl = static_cast<nsEDDEngine::PARAM_MEM_TBL*>(tableP);

	nsEDDEngine::PARAM_MEM_TBL_ELEM	*element;		/* temp pointer for the list */
	nsEDDEngine::PARAM_MEM_TBL_ELEM	*end_element;	/* end pointer for the list */
	DDL_UINT            temp_int = 0;	 	/* integer value */
	int                 rc = 0;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef DDSTEST
	TEST_FAIL(EVAL_PARAM_MEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_tbl->count = 0;
		param_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_tbl->list = (nsEDDEngine::PARAM_MEM_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(nsEDDEngine::PARAM_MEM_TBL_ELEM) ) );

	if (param_mem_tbl->list == NULL) {

		param_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_mem_tbl->list, 0,	(size_t) temp_int * sizeof(nsEDDEngine::PARAM_MEM_TBL_ELEM));

	/*
	 * load the list
	 */
	for (element = param_mem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == UNUSED_OFFSET) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_type = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_size = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_class = (ulong) temp_int;
	}

	if(bin->size)
		return DDL_ENCODING_ERROR;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: dir_mask_man()
 *	ShortDesc: dir_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		dir_mask_man() handles all mask switch cases for the directory structures
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		bin_exists:		the mask which indicates which binarys are available
 *		bin_hooked:		the mask which indicates which binarys are hooked to the bin structure
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		bin:			the binary to evaluate
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by dir_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
dir_mask_man(  // fd_fdi
unsigned long   attr_mask,
UINT32			bin_exists,
UINT32			bin_hooked,
unsigned long*	attr_avail,
eval_type		eval,
void*			attribute,
nsEDDEngine::BININFO*		bin,
ROD_HANDLE		rod_handle,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{

	int             rc;	/* return code */

#ifdef DDSTEST
	TEST_FAIL(MASK_MAN_DIR);
#endif

	/*
	 * No binary exists
	 */

	if (!(attr_mask & bin_exists)) {

		/*
		 * This is a DDOD error,
         * by definition all directory tables
         * must have binary available
		 */

		rc = DDL_BINARY_REQUIRED;
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & bin_hooked)) {

		/*
		 * If value is already available
		 */

		if (attr_mask & *attr_avail) {

			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else {

			rc = DDL_BINARY_REQUIRED;
		}
	}

	else {

		/*
		 * check masks for evaluating
		 */

		if (!(attr_mask & *attr_avail)) {

			rc = eval(attribute, bin, rod_handle, flat_device_dir);

			if (rc == DDL_SUCCESS) {

				*attr_avail |= attr_mask;
			}
		}

		/*
		 * evaluation is not necessary
		 */

		else {

			rc = DDL_SUCCESS;
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_dir_block_tables
 *	ShortDesc: evaluate (ie. create) the block tables
 *
 *	Description:
 *		eval_dir_block_tables will load the desired block tables into the 
 *		FLAT_BLOCK_DIR structure. The user must specify which block tables
 *		are desired by setting the appropriate bits in the "mask parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying which block tables are requested.
 *
 *	Outputs:
 *		block_dir:	pointer to a FLAT_BLOCK_DIR structure. Will contain
 *					the desired block tables.
 *      block_bin:  pointer to a BIN_BLOCK_DIR structure. Stores the 
 *					binary block information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		returns from other dds functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
eval_dir_block_tables(
nsEDDEngine::FLAT_BLOCK_DIR 		*block_dir,
nsEDDEngine::BIN_BLOCK_DIR		*block_bin,
unsigned long   	mask,
ROD_HANDLE			rod_handle,
nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir)
{
	ASSERT(flat_device_dir);

	int  rc = DDS_SUCCESS;	/* return code */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_BLOCK_DIR);
#endif

	// FD_FDI you cannot ask for something that does not exist
	mask&=block_bin->bin_exists;

#ifdef DEBUG
	dds_blk_dir_flat_chk((nsEDDEngine::BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	if (mask & BLK_ITEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_blk_item_tbl,
								&block_dir->blk_item_tbl,
								&block_bin->blk_item_tbl,
								-1,
								flat_device_dir);

		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & BLK_ITEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_blk_item_name_tbl,
								&block_dir->blk_item_name_tbl,
								&block_bin->blk_item_name_tbl,
								-1,
								flat_device_dir);

		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_mem_tbl,
								&block_dir->param_mem_tbl,
								&block_bin->param_mem_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_tbl,
								&block_dir->param_tbl,
								&block_bin->param_tbl,
								-1,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_mem_name_tbl,
								&block_dir->param_mem_name_tbl,
								&block_bin->param_mem_name_tbl,
								-1,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_ELEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_ELEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_elem_tbl,
								&block_dir->param_elem_tbl,
								&block_bin->param_elem_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_LIST_TBL_MASK) {
		rc = dir_mask_man((unsigned long) PARAM_LIST_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_list_tbl,
								&block_dir->param_list_tbl,
								&block_bin->param_list_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_TBL_MASK) {
		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_list_mem_tbl,
								&block_dir->param_list_mem_tbl,
								&block_bin->param_list_mem_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_NAME_TBL_MASK) {
		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_param_list_mem_name_tbl,
								&block_dir->param_list_mem_name_tbl,
								&block_bin->param_list_mem_name_tbl,
								rod_handle,
								flat_device_dir);

		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & CHAR_MEM_TBL_MASK) {
		rc = dir_mask_man((unsigned long) CHAR_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_char_mem_tbl,
								&block_dir->char_mem_tbl,
								&block_bin->char_mem_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & CHAR_MEM_NAME_TBL_MASK) {
		rc = dir_mask_man((unsigned long) CHAR_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_char_mem_name_tbl,
								&block_dir->char_mem_name_tbl,
								&block_bin->char_mem_name_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & REL_TBL_MASK) {

		rc = dir_mask_man((unsigned long) REL_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_rel_tbl,
								&block_dir->rel_tbl,
								&block_bin->rel_tbl,
								rod_handle,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & UPDATE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) UPDATE_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_update_tbl,
								&block_dir->update_tbl,
								&block_bin->update_tbl,
								-1,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if (mask & CRIT_PARAM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CRIT_PARAM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								eval_dir_crit_param_tbl,
								&block_dir->crit_param_tbl,
								&block_bin->crit_param_tbl,
								-1,
								flat_device_dir);


		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

#ifdef DEBUG
	dds_blk_dir_flat_chk((nsEDDEngine::BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	return rc;
}


/*********************************************************************
 *
 *	Name: eval_clean_block_dir
 *	ShortDesc: Frees the block tables
 *
 *	Description:
 *		eval_clean_block_dir will check the attr_avail flags
 *      in the FLAT_BLOCK_DIR structure to see which block tables
 *		exist and free them. Everything in the structure is then 
 *		set to zero.
 *
 *	Inputs:
 *		block_dir: 	pointer to a FLAT_BLOCK_DIR to be cleaned.
 *
 *	Outputs:
 *		block_dir: 	pointer to the empty FLAT_BLOCK_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_block_dir(
nsEDDEngine::FLAT_BLOCK_DIR     *block_dir)
{
	ulong           temp_attr_avail;


	if (block_dir == NULL) {
		return;
	}

//	ASSERT(0);
	
#ifdef DEBUG
	dds_blk_dir_flat_chk((nsEDDEngine::BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	temp_attr_avail = block_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_ITEM_TBL_MASK) && 
		(block_dir->blk_item_tbl.list)) {
		free((void *) block_dir->blk_item_tbl.list);
	}

	if ((temp_attr_avail & BLK_ITEM_NAME_TBL_MASK) && 
		(block_dir->blk_item_name_tbl.list)) {
		free((void *) block_dir->blk_item_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_TBL_MASK) && 
		(block_dir->param_mem_tbl.list)) {
		free((void *) block_dir->param_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_NAME_TBL_MASK) && 
		(block_dir->param_mem_name_tbl.list)) {
		free((void *) block_dir->param_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_ELEM_TBL_MASK) && 
		(block_dir->param_elem_tbl.list)) {
		free((void *) block_dir->param_elem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_TBL_MASK) && 
		(block_dir->param_tbl.list)) {
		free((void *) block_dir->param_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_TBL_MASK) && 
		(block_dir->param_list_tbl.list)) {
		free((void *) block_dir->param_list_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_TBL_MASK) && 
		(block_dir->param_list_mem_tbl.list)) {
		free((void *) block_dir->param_list_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_NAME_TBL_MASK) && 
		(block_dir->param_list_mem_name_tbl.list)) {
		free((void *) block_dir->param_list_mem_name_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_TBL_MASK) && 
		(block_dir->char_mem_tbl.list)) {
		free((void *) block_dir->char_mem_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_NAME_TBL_MASK) && 
		(block_dir->char_mem_name_tbl.list)) {
		free((void *) block_dir->char_mem_name_tbl.list);
	}

	if ((temp_attr_avail & REL_TBL_MASK) && 
		(block_dir->rel_tbl.list))
	{
		free((void *) block_dir->rel_tbl.list);
	}

	if (block_dir->rel_tbl.UnitLookup)	// UnitLookup could exist independently of the rel_tbl
	{
		delete block_dir->rel_tbl.UnitLookup;
		block_dir->rel_tbl.UnitLookup = nullptr;
	}

	if ((temp_attr_avail & UPDATE_TBL_MASK) && 
		(block_dir->update_tbl.list)) {

		for(int i=0;i<block_dir->update_tbl.count;i++)
		{
			block_dir->update_tbl.list[i].to_be_updated.Dispose();
		}

		free((void *) block_dir->update_tbl.list);
	}

	if ((temp_attr_avail & CRIT_PARAM_TBL_MASK) && 
		(block_dir->crit_param_tbl.list)) 
	{
		for(int i=0;i<block_dir->crit_param_tbl.count;i++)
		{
			block_dir->crit_param_tbl.list[i].Dispose();
		}

		free((void *) block_dir->crit_param_tbl.list);
	}

	if(block_dir->dominant_tbl)
	{
		nsEDDEngine::DOMINANT_TBL::iterator it;

		for (it = block_dir->dominant_tbl->begin(); it != block_dir->dominant_tbl->end(); it++) 
		{
			nsEDDEngine::OP_REF_LIST *opList = NULL;
			opList = it->second;
				
			free((void*)(opList->list)); 
			opList->list = NULL;
				
			free((void*)(opList)); 	
		}

		block_dir->dominant_tbl->clear();
		delete block_dir->dominant_tbl;
			
	}

	memset((char *) block_dir, 0, sizeof(nsEDDEngine::FLAT_BLOCK_DIR));

#ifdef DEBUG
	dds_blk_dir_flat_chk((nsEDDEngine::BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
}

/*********************************************************************
 *
 *	Name: eval_dir_device_tables
 *	ShortDesc: evaluate (ie. create) the device tables
 *
 *	Description:
 *		eval_dir_device_tables will load the desired device tables into the 
 *		FLAT_DEVICE_DIR structure. The user must specify which device tables
 *		are desired by setting the appropriate bits in the "mask" parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying the desired device tables.
 *
 *	Outputs:
 *		device_dir:	pointer to a FLAT_DEVICE_DIR structure. Will contain
 *					the desired device tables.
 *      device_bin: pointer to a BIN_DEVICE_DIR structure. Stores the 
 *					binary device information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		return codes from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_dir_device_tables( // fd_fdi 
nsEDDEngine::FLAT_DEVICE_DIR		*device_dir,
nsEDDEngine::BIN_DEVICE_DIR		*device_bin,
unsigned long   	mask,
ROD_HANDLE			rod_handle)
{

	int  rc = DDS_SUCCESS;	/* return code */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_DEVICE_DIR);
#endif

	// FD_FDI you cannot ask for something that does not exist
	mask&=device_bin->bin_exists;

#ifdef DEBUG
	dds_dev_dir_flat_chk((nsEDDEngine::BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	if (mask & BLK_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_blk_tbl,
								&device_dir->blk_tbl,
								&device_bin->blk_tbl,
								rod_handle,
								device_dir);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & DICT_REF_TBL_MASK) {

		rc = dir_mask_man((unsigned long) DICT_REF_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_dict_ref_tbl,
								&device_dir->dict_ref_tbl,
								&device_bin->dict_ref_tbl,
								rod_handle,
								device_dir);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	// FD_FDI Symbols are decoding properly
	if (mask & ITEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) ITEM_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_item_tbl,
								&device_dir->item_tbl,
								&device_bin->item_tbl,
								rod_handle,
								device_dir);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	// FD_FDI Symbols are decoding properly
	if (mask & STRING_TBL_MASK) {

		rc = dir_mask_man((unsigned long) STRING_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_string_tbl,
								&device_dir->string_tbl,
								&device_bin->string_tbl,
								rod_handle,
								device_dir);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & IMAGE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) IMAGE_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_image_tbl,
								&device_dir->image_tbl,
								&device_bin->image_tbl,
								rod_handle,
								device_dir);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	

	// FD_FDI Symbols are decoding properly
	if (mask & SYMBOL_TBL_MASK) {

		rc = dir_mask_man((unsigned long) SYMBOL_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_symbol_tbl,
								&device_dir->symbol_tbl,
								&device_bin->symbol_table,
								rod_handle,
								device_dir);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & ITEM_TO_COMMAND_TBL_MASK) {

		rc = dir_mask_man((unsigned long) ITEM_TO_COMMAND_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_item_to_command_tbl,
								&device_dir->item_to_command_tbl,
								&device_bin->item_to_command_tbl, //->symbol_table,
								rod_handle,
								device_dir);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & COMMAND_NUMBER_TBL_MASK) {

		rc = dir_mask_man((unsigned long) COMMAND_NUMBER_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								eval_dir_command_number_tbl,
								&device_dir->command_number_tbl,
								&device_bin->command_number_tbl, //->symbol_table,
								rod_handle,
								device_dir);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((nsEDDEngine::BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	return rc;
}


/*********************************************************************
 *
 *	Name: eval_clean_device_dir
 *	ShortDesc: Free the device tablesIR structure
 *
 *	Description:
 *		eval_clean_device_dir will check the attr_avail flags
 *      in the FLAT_DEVICE_DIR structure to see which device tables
 *		exist and free them. Everything in the structure is then
 *		set to zero.
 *
 *	Inputs:
 *		device_dir: pointer to the FLAT_DEVICE_DIR to be cleaned.
 *
 *	Outputs:
 *		device_dir: pointer to the empty FLAT_DEVICE_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_device_dir(
nsEDDEngine::FLAT_DEVICE_DIR     *device_dir)  // FD_FDI make sure to free up device dir
{
	ulong           temp_attr_avail;

	if (device_dir == NULL) {
		return;
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((nsEDDEngine::BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	temp_attr_avail = device_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_TBL_MASK) && 
		(device_dir->blk_tbl.list)) {
		free((void *) device_dir->blk_tbl.list);
	}

	if ((temp_attr_avail & DICT_REF_TBL_MASK) && 
		(device_dir->dict_ref_tbl.list)) {

		for(int i=0;i<device_dir->dict_ref_tbl.count;i++)
		{
			device_dir->dict_ref_tbl.list[i].dictionary_entry.~STRING();
			device_dir->dict_ref_tbl.list[i].name.~STRING();
		}

		free((void *)device_dir->dict_ref_tbl.list);
	}

	if ((temp_attr_avail & ITEM_TBL_MASK) && 
		(device_dir->item_tbl.list)) {
		free((void *) device_dir->item_tbl.list);
	}

	if ((temp_attr_avail & STRING_TBL_MASK) && 
		(device_dir->string_tbl.list)) {

		free((void *) device_dir->string_tbl.root);
		delete []device_dir->string_tbl.list;
	}

	if ((temp_attr_avail & IMAGE_TBL_MASK) && 
		(device_dir->image_tbl.list)) {
			
		for(int i=0; i<device_dir->image_tbl.count; i++)
		{
			nsEDDEngine::IMAGE_LIST	*element = &device_dir->image_tbl.list[i];
			
			free((void *)element->list);
			free((void *)element->data);
			free((void *)element->name);
		}
	
		free((void *) device_dir->image_tbl.list);
		device_dir->image_tbl.list = NULL;
	}

	if ((temp_attr_avail & SYMBOL_TBL_MASK) && 
		(device_dir->symbol_tbl.list)) {

		for(int i=0; i<device_dir->symbol_tbl.count; i++){
			free((void *) device_dir->symbol_tbl.list[i].name);
		}

		free((void *) device_dir->symbol_tbl.list);
		device_dir->symbol_tbl.list = NULL;
	}

	if ((temp_attr_avail & ITEM_TO_COMMAND_TBL_MASK) && 
		(device_dir->item_to_command_tbl.list)) {

		for( int j = 0; j<device_dir->complex_item_to_cmd_tbl.count;j++)
		{
			device_dir->complex_item_to_cmd_tbl.list[j].resolved_ref.Dispose();
		}
		free((void *) device_dir->complex_item_to_cmd_tbl.list);

		for(int i=0;i<device_dir->item_to_command_tbl.count;i++)
		{
			device_dir->item_to_command_tbl.list[i].resolved_ref.Dispose();

			if(device_dir->item_to_command_tbl.list[i].number_of_read_commands>0)
			{
				for(int j=0;j<device_dir->item_to_command_tbl.list[i].number_of_read_commands;j++)
				{
					free((void *)device_dir->item_to_command_tbl.list[i].read_command_list[j].index_list);
				}

				free((void *) device_dir->item_to_command_tbl.list[i].read_command_list);
			}

			if(device_dir->item_to_command_tbl.list[i].number_of_write_commands>0)
			{
				for(int j=0;j<device_dir->item_to_command_tbl.list[i].number_of_write_commands;j++)
				{
					free((void *)device_dir->item_to_command_tbl.list[i].write_command_list[j].index_list);
				}

				free((void *) device_dir->item_to_command_tbl.list[i].write_command_list);
			}
		}


		free((void *) device_dir->item_to_command_tbl.list);

		
		
		device_dir->symbol_tbl.list = NULL;
	}

	if ((temp_attr_avail & COMMAND_NUMBER_TBL_MASK) && 
		(device_dir->command_number_tbl.list)) {

		free((void *) device_dir->command_number_tbl.list);
		device_dir->symbol_tbl.list = NULL;
	}

	memset((char *) device_dir, 0, sizeof(nsEDDEngine::FLAT_DEVICE_DIR));

#ifdef DEBUG
	dds_dev_dir_flat_chk((nsEDDEngine::BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
}






