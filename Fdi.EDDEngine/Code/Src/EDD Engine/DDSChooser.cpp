#include "stdafx.h"
#include "DDSChooser.h"
#include "HART/HARTEDDEngine/LegacyHartFactory.h"
#include "FF/FFEDDEngine/LegacyFFFactory.h"
#include "FDIEDDEngine/FDIEngineFactory.h"
#include "nsEDDEngine/ProtocolType.h"
#include "inc/rtn_code.h"
//#include "PlatformCommon.h"


#define INVALID_FORMAT	0
#define LEGACY_HART 1
#define LEGACY_FF	2
#define FDI_FMT		3

#define HEADER_SIZE 39 // header size for all the header types

CEDDEngineImpl* CDDSChooser::ChooseDDSLib( const wchar_t *sEDDBinaryFilename, 
	 const wchar_t* pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
{
	UINT32 manufacturer = 0;
	UINT16 device_type = 0;
	UINT8  device_revision = 0;	
	
	int iFmt = GetFileFormat(sEDDBinaryFilename, &manufacturer, &device_type, &device_revision);

	CEDDEngineImpl *EDDEngineImpl = nullptr;

	switch (iFmt)
	{
	case LEGACY_HART:
		EDDEngineImpl = LegacyHartFactory::CreateLegacyHartDDS(sEDDBinaryFilename, 
		    pConfigXML, pIEDDEngineLogger, pErrorCode, manufacturer, device_type, device_revision);
		break;

	case LEGACY_FF:
		EDDEngineImpl = LegacyFFFactory::CreateLegacyFFDDS(sEDDBinaryFilename, 
			pConfigXML, pIEDDEngineLogger, pErrorCode);
		break;
	case FDI_FMT:
		EDDEngineImpl = FDIEngineFactory::CreateFDIDDS(sEDDBinaryFilename, 
			pConfigXML, pIEDDEngineLogger, pErrorCode );
		break;

    case INVALID_FORMAT:
        *pErrorCode = nsEDDEngine::INVALID_BINARY_FORMAT_ERROR;
        break;
	default:
		*pErrorCode = nsEDDEngine::BAD_FILE_OPEN_ERROR;
		break;
	}

	return EDDEngineImpl;
}


/***********************************************************************
 *
 *	Name:  GetInteger
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *      Copied and Modified: Mark Sandmann
 *
 **********************************************************************/

UINT32 CDDSChooser::GetInteger( UINT8 *bin_ptr, int bin_size)
{
	UINT32	value = 0;
	int		byte_num;

	// Go through each byte and total up the integer value.
	for (byte_num = 0; byte_num < bin_size; byte_num ++)
    {
		value <<= 8;
		value += (UINT32)bin_ptr[byte_num];
	}

	return(value);
}

/***********************************************************************
 *
 *	Name:  LoadLegacyHartDeviceInformation
 *
 *	ShortDesc:  Returns the manufacturer, device type and device rev from the header.
 *				Offsets are hardcoded, and copied over from tags_sa.h in the legacy hart project.
 *
 *	Inputs:
 *		Header Buffer
 *
 *	Outputs:
 *		Manufacturer, device type and device rev
 *
 *	Returns:
 *		None
 *
 *	Authors:
 *		Ken Johnson
 *
 **********************************************************************/

void CDDSChooser::LoadLegacyHartDeviceInformation(UINT8 * header_buffer,UINT32 * manufacturer, UINT16	* device_type, UINT8 * device_revision)
{
	//size definitions based on tags_sa.h for legacy hart project.
	const int iManufacturerSize = 3;
	const int iDeviceTypeSize = 2;
	const int iDeviceRevSize = 1;
	const int iManufacturerOffset = 16;

	const int iDeviceTypeOffset = iManufacturerOffset + iManufacturerSize;
	const int iDeviceRevOffset = iDeviceTypeOffset + iDeviceTypeSize;

	*manufacturer = GetInteger(
					&header_buffer[iManufacturerOffset],
					iManufacturerSize);

	*device_type = (UINT16) GetInteger(
					&header_buffer[iDeviceTypeOffset],
					iDeviceTypeSize);

	*device_revision = (UINT8) GetInteger(
					&header_buffer[iDeviceRevOffset],
					iDeviceRevSize);
}


/***********************************************************************
 *
 *	Name:  GetFileFormat
 *
 *	ShortDesc:  Get the file format from the header.
 *              Read the header from the file.
 *              Determine if it is FDI or HART Cipher from the unique magic #
 *              FF and HART have the same magic # so we have to look and see
 *                 if it has lit80_major_rev value of non-zero if it does then
 *                 it is HART Legacy otherwise it is FF Legacy
 *
 *	Inputs:
 *		sEDDBinaryFilename - string containing the file name
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of:
 *      >0 = type
 *      0  = unknown type
 *      <0 = error code
 *
 *	Authors:
 *		Mark Sandmann
 *
 **********************************************************************/

int CDDSChooser::GetFileFormat(const wchar_t *sEDDBinaryFilename, UINT32 * manufacturer, UINT16	* device_type, UINT8 * device_revision)
{
    const UINT32 LIT80_MAJOR_REV_OFFSET = 23;
	int ret = INVALID_FORMAT;

    /*
	 *	Open the DDOD file and get the Header information.
	 */

    // file pointer when sEDDBinaryFilename is opened
	FILE *file = PS_wfopen(sEDDBinaryFilename, L"rb");
	if (!file)
    {
		ret = CM_BAD_FILE_OPEN;
	}
    else
    {
        UINT8	header_buffer[HEADER_SIZE];

		// variable for containing results from calls made from here
        size_t r_code = fread((char *)header_buffer, 1, HEADER_SIZE, file);
	    if (r_code != HEADER_SIZE)
        {
		    ret = (CM_FILE_ERROR);
        }
        else
        {
            // decide on which format this is based on the contents of the header
            UINT32  magic_number;
            magic_number = GetInteger(header_buffer, sizeof(UINT32));
            switch (magic_number)
            {
            case 0x8a45f533:    // FDI magic #
                ret = FDI_FMT;
                        break;
            case 0x7f3f5f77:    // both HART And FF use this magic #
                // Only HART has a LIT80_MAJOR_REV
                if(header_buffer[LIT80_MAJOR_REV_OFFSET] != 0)
                {
                    ret = LEGACY_HART;
                }
                else   
				{
					// Check the extension of file.
					wchar_t ext[_MAX_EXT] =  {0};
                   
					PS_GetFileExtension(sEDDBinaryFilename, ext);
                  
					for (int i = 0 ; i < (int)wcslen(ext) ; i ++)
					{
						ext[i] = towlower(ext[i]);
					}

					// if the extension is .fms, It's Legacy HART
					if(!wcscmp(ext, L".fms"))
					{
						ret = LEGACY_HART;
					}
					else
					{
						ret = LEGACY_FF;
					}
				}
                break;
            default:
                ret = INVALID_FORMAT;
                break;
            }// switch	

			if (ret == LEGACY_HART)
			{
				LoadLegacyHartDeviceInformation(header_buffer, manufacturer, device_type, device_revision);
			}				
        }   // else not read error
	}   // else not open error

    // Cleanup file state
    if (file)
    {
        fclose(file);
    }

	return ret;
}
