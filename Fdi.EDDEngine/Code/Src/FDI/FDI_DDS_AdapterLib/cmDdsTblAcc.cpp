/******************************************************************************
 * $Header: cmDdsTblAcc.cpp: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:54 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description: Connection Manager Tables module
 *				This file contains all access functions pertaining 
 *				to the Connection Manager Active Tables.
 *
 * $Workfile: cmDdsTblAcc.cpp$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/


#include "STDINC.H"

#include "cm_loc.h"
#include <malloc.h>
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


int CConnectionMgr::get_abt_op_index (BLOCK_HANDLE bh, OBJECT_INDEX *oi)
{
	DEVICE_HANDLE	device_handle;


	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	device_handle = ABT_ADT_OFFSET(bh);

	ASSERT_RET(valid_device_handle(device_handle),
			CM_BAD_DEVICE_HANDLE);


	ASSERT_RET(oi, CM_BAD_POINTER);

	*oi = active_blk_tbl.list[bh]->op_index;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_param_count (BLOCK_HANDLE bh, unsigned int *pc)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	ASSERT_RET(pc, CM_BAD_POINTER);
	*pc = active_blk_tbl.list[bh]->param_count;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_dd_blk_id (BLOCK_HANDLE bh, ITEM_ID *dbi)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	ASSERT_RET(dbi, CM_BAD_POINTER);
	*dbi = active_blk_tbl.list[bh]->dd_blk_id;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int *dbto)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	ASSERT_RET(dbto, CM_BAD_POINTER);
	*dbto = active_blk_tbl.list[bh]->dd_blk_tbl_offset;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE *dth)
{
	DEVICE_HANDLE	device_handle;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE);
	}
	ASSERT_RET(dth, CM_BAD_POINTER);
	device_handle = ABT_ADT_OFFSET(bh);
	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE);
	}
	*dth = ADT_ADTT_OFFSET(device_handle);
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_adt_offset (BLOCK_HANDLE bh, DEVICE_HANDLE *dh)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	ASSERT_RET(dh, CM_BAD_POINTER);
	*dh = active_blk_tbl.list[bh]->active_dev_tbl_offset;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_dd_handle(BLOCK_HANDLE bh, ROD_HANDLE *ddh)
{
	DEVICE_HANDLE		dh;
	DEVICE_TYPE_HANDLE	dth;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE);
	}
	
	ASSERT_RET(ddh, CM_BAD_POINTER);

	dh = ABT_ADT_OFFSET(bh);
	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE);
	}
	
	dth = ADT_ADTT_OFFSET(dh);
	if (!g_DeviceTypeMgr.valid_device_type_handle(dth)) {
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}

	*ddh = g_DeviceTypeMgr.ADTT_DD_HANDLE(dth);

	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_dd_dev_tbls(BLOCK_HANDLE bh, void **ddt)
{
	DEVICE_HANDLE		device_handle;
	DEVICE_TYPE_HANDLE	device_type_handle;
	
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	device_handle = ABT_ADT_OFFSET(bh);

	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE);
	}

	device_type_handle = ADT_ADTT_OFFSET(device_handle);

	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(CM_BAD_DEVICE_TYPE_HANDLE);
	}

	*ddt = (void*) g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(device_type_handle);	

	if ( *ddt == NULL ) {
		return CM_BAD_POINTER;
	}

	return(CM_SUCCESS);
}


int CConnectionMgr::set_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int dbto)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto;
	return(CM_SUCCESS);
}


int CConnectionMgr::set_abt_param_count (BLOCK_HANDLE bh, unsigned int pc)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	active_blk_tbl.list[bh]->param_count = pc;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_adt_dd_handle(DEVICE_HANDLE dh, ROD_HANDLE *ddh)
{
	DEVICE_TYPE_HANDLE	device_type_handle;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	ASSERT_RET(ddh, CM_BAD_POINTER);
	
	device_type_handle = ADT_ADTT_OFFSET(dh);
	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(CM_BAD_DEVICE_TYPE_HANDLE);
	}

	*ddh = g_DeviceTypeMgr.ADTT_DD_HANDLE(device_type_handle);
	return(CM_SUCCESS);
}


int CConnectionMgr::get_adt_dd_dev_tbls(DEVICE_HANDLE dh, void **ddt)
{
	DEVICE_TYPE_HANDLE	device_type_handle;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE);
	}
	ASSERT_RET(ddt, CM_BAD_POINTER);
	device_type_handle = ADT_ADTT_OFFSET(dh);
	*ddt = (void*) g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(device_type_handle);
	return (CM_SUCCESS);
}


int CDeviceTypeMgr::get_adtt_dd_handle (DEVICE_TYPE_HANDLE dth, ROD_HANDLE *ddh)
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}
	ASSERT_RET(ddh, CM_BAD_POINTER);
	
	*ddh = active_dev_type_tbl.list[dth]->rod_handle;
	return(CM_SUCCESS);
}


int CDeviceTypeMgr::get_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void **ddt)
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}
	ASSERT_RET(ddt, CM_BAD_POINTER);
	*ddt = (void*) active_dev_type_tbl.list[dth]->dd_dev_tbls;
	return(CM_SUCCESS);
}


int CDeviceTypeMgr::set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void *ddt)
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}
	active_dev_type_tbl.list[dth]->dd_dev_tbls = (nsEDDEngine::FLAT_DEVICE_DIR*) ddt;
	return(CM_SUCCESS);
}


int CConnectionMgr::valid_device_handle (DEVICE_HANDLE dh)
{
	return ((dh >= 0) && (dh < active_dev_tbl.count)
			&& (active_dev_tbl.list[dh]));
}


int CDeviceTypeMgr::valid_device_type_handle (DEVICE_TYPE_HANDLE dth)
{
	return ((dth >= 0) && (dth < active_dev_type_tbl.count) 
			&& (active_dev_type_tbl.list[dth]));
}


int CConnectionMgr::valid_dd_handle (ROD_HANDLE ddh)
{
	return (ddh >= 0);
}


////////////////////////////////////////////////////////////////////////
//
//  DDS Specific function, cannot be in a class, DO NOT REMOVE!!!
//
////////////////////////////////////////////////////////////////////////

/***********************************************************************
 *
 *	Name:  ct_block_search 
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				a tag.
 *
 *	Description:
 *		The ct_block_tag_search_for_DDS function takes a block tag and 
 *		searches through the valid (allocated) Active Block Table elements 
 *		in the Active Block Table list, looking for the element that
 *		matches the tag.  If found, the corresponding block handle
 *		will be returned.  Otherwise an error code will be returned.
 *		NOTE:  This function is ONLY used in AmsDDS library and should
 *				NOT be removed.
 *
 *	Inputs:
 *		tag - unique identifier of a block.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		block_handle for success and -1 for failure
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/
BLOCK_HANDLE CConnectionMgr::ct_block_search(char *tag)
{
	BLOCK_HANDLE	block_handle;

	ASSERT_RET(tag, CM_BAD_POINTER) ;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (block_handle = 0; block_handle < active_blk_tbl.count;
			block_handle ++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the tag of the element and its time.
			 */

			if (!strcmp(ABT_TAG(block_handle),tag)) {
				return(block_handle);
			}
		}
	}

	return(-1);
}


