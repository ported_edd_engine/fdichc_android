/*** FDI DeviceModelServices EDD TestCase ***/
#include "Macros.h"
#include "Methods.h"

// Manufacturer 0xFD = "Special"
MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0028, DEVICE_REVISION 1, DD_REVISION 1

/****************************************************************************** */
/*    Common Tables and Univeral Commands (mandatory)                           */
/* **************************************************************************** */
/* */                                                                           
/* */  IMPORT STANDARD _TABLES, DEVICE_REVISION 6, DD_REVISION 1                
/* */  {                                                                        
/* */       
/* */       EVERYTHING;                                                       
/* */  }                                                                        
/* */                                                                           
/* */  IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 5, DD_REVISION 1             
/* */  {                                                                        
/* */      EVERYTHING;
/* */  }                                                                        
/* */

/****************************************************************************** */
/* Dynamic variables (mandatory)                                                */
/* **************************************************************************** */
/* */  
/* */  
/* */  ARRAY OF VARIABLE dynamic_variables
/* */  {
/* */      ELEMENTS
/* */      {
/* */          PRIMARY, primary_value;
/* */      }
/* */  }
/* */




VARIABLE primary_value
{
    LABEL "Primary value as float";
    TYPE FLOAT;
    CLASS DEVICE & DYNAMIC;
}


/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    STYLE MENU;
    ITEMS
    {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */
MENU main_test_menu
{
    LABEL "Main test menu";
    STYLE WINDOW;
    ITEMS
    {
        menu_array_coll_variable,
        menu_array_coll_coll_variable
    }
}
/****************************************************************************** */
/* Menus                                                             */
/****************************************************************************** */
MENU menu_array_coll_variable
{
    LABEL "Array of Collection of Variable";
    ITEMS
    {
        deviceVariables[0].DIGITAL_VALUE (DISPLAY_VALUE), // 2 Levels, text = Press
        deviceVariables[0].DIGITAL_VALUE2 (DISPLAY_VALUE),  
        deviceVariables[0].DIGITAL_UNITS (DISPLAY_VALUE), // 2 Levels, text = Press Unit
        deviceVariables2[0].DIGITAL_VALUE (DISPLAY_VALUE),
        deviceVariables2[0].DIGITAL_VALUE2 (DISPLAY_VALUE),
        deviceVariables2[0].DIGITAL_UNITS (DISPLAY_VALUE),
        deviceVariables[0].DATA_QUALITY (DISPLAY_VALUE),               
        deviceVariables[0].LIMIT_STATUS (DISPLAY_VALUE),              
        deviceVariables[0].DEVICE_FAMILY_STATUS (DISPLAY_VALUE),      
        deviceVariables[0].CLASSIFICATION (DISPLAY_VALUE),                         
        deviceVariables[0].UPPER_SENSOR_LIMIT (DISPLAY_VALUE),         
        deviceVariables[0].LOWER_SENSOR_LIMIT (DISPLAY_VALUE),        
        deviceVariables[0].MINIMUM_SPAN (DISPLAY_VALUE),               
        deviceVariables[0].DAMPING_VALUE (DISPLAY_VALUE),           
        deviceVariables[0].SENSOR_SERIAL_NUMBER (DISPLAY_VALUE)   
    }
}
MENU menu_array_coll_coll_variable
{
    LABEL "Array of Collection of Collection of Variable";
    ITEMS
    {
        dynamic_variables_test[0].DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE),     // 4 Levels, text = PV
        dynamic_variables_test[0].DEVICE_VARIABLE.DIGITAL_VALUE2 (DISPLAY_VALUE),
        dynamic_variables_test[0].DEVICE_VARIABLE.DIGITAL_UNITS (DISPLAY_VALUE)     // 4 Levels, text = PV Unit

        dynamic_variables2[0].DEVICE_VARIABLE.DIGITAL_VALUE (DISPLAY_VALUE),
        dynamic_variables2[0].DEVICE_VARIABLE.DIGITAL_VALUE2 (DISPLAY_VALUE),
        dynamic_variables2[0].DEVICE_VARIABLE.DIGITAL_UNITS (DISPLAY_VALUE)

        dynamic_variables2[0].DEVICE_VARIABLE2.DIGITAL_VALUE (DISPLAY_VALUE),
        dynamic_variables2[0].DEVICE_VARIABLE2.DIGITAL_VALUE2 (DISPLAY_VALUE),
        dynamic_variables2[0].DEVICE_VARIABLE2.DIGITAL_UNITS (DISPLAY_VALUE)

    }
}
/****************************************************************************** */
/* Array and Collection                                                               */
/****************************************************************************** */

ARRAY OF COLLECTION deviceVariables
{
    LABEL "Device Variables";
    ELEMENTS
    {
        0, press,       "Press", "Pressure Device Variable";
        1, temperature, "Temp" , "Temperature Device Variable";
    }
}
COLLECTION OF VARIABLE press
{
    LABEL "Pressure";
    HELP "Pressure - pressure of the process measured with respect to a reference pressure.";
    MEMBERS
    {
        DIGITAL_VALUE,              pressureValue,               "",                    "Digital value changes with time";
        DIGITAL_VALUE2,             pressureValue;
        DIGITAL_UNITS,              pressureUnits,               "Unit",                "Engineering unit displayed with digital value";
        DATA_QUALITY,               pressurePDQ,                [process_data_quality], [process_data_quality_help];
        LIMIT_STATUS,               pressureLS,                 [limit_status],         [limit_status_help];
        DEVICE_FAMILY_STATUS,       pressureStatus,             [family_status],        [family_status_help];

        CLASSIFICATION,             pressureClassification,     [classification],       [classification_help];
        DEVICE_FAMILY,              pressureFamily,             [device_family],        [device_family_help];

        UPPER_SENSOR_LIMIT,         pressureUSL,                [usl],                  [upper_sensor_limit_help];
        LOWER_SENSOR_LIMIT,         pressureLSL,                [lsl],                  [lower_sensor_limit_help];
        MINIMUM_SPAN,               pressureMinimumSpan,        [minimum_span],         [minimum_span_help];
        DAMPING_VALUE,              pressureDampingValue,       [damping_value],        [seconds_damping_value_help];

        // Device Information
        SENSOR_SERIAL_NUMBER,       pressureSerialNumber,       [sensor_serial_number], [sensor_serial_number_help];
    }
}

COLLECTION OF VARIABLE temperature
{
    MEMBERS
    {
        DIGITAL_VALUE,              temperatureValue;
        DIGITAL_UNITS,              temperatureUnits,              "Unit",                 "Engineering unit displayed with digital value";
        DATA_QUALITY,               temperaturePDQ,                [process_data_quality], [process_data_quality_help];
        LIMIT_STATUS,               temperatureLS,                 [limit_status],         [limit_status_help];
        DEVICE_FAMILY_STATUS,       temperatureStatus,             [family_status],        [family_status_help];

        CLASSIFICATION,             temperatureClassification,     [classification],       [classification_help];
    }
}
ARRAY OF COLLECTION deviceVariables2
{
    LABEL "Device Variables";
    ELEMENTS
    {
        0, press_nolabel_nohelp;
    }
}

COLLECTION OF VARIABLE press_nolabel_nohelp
{
    MEMBERS
    {
        DIGITAL_VALUE,  pressureValue,  "",     "Digital value changes with time";
        DIGITAL_VALUE2, pressureValue;
        DIGITAL_UNITS,  pressureUnits,  "Unit", "Engineering unit displayed with digital value";
    }
}

ARRAY OF ARRAY processVariables2
{
    ELEMENTS
    {
        // Top level descriptions (if defined)
        0, dynamic_variables2;
    }
}

ARRAY OF COLLECTION dynamic_variables2
{
    LABEL "Dynamic Variables";
    ELEMENTS
    { 
        // 2nd level descriptions (becomes top level descriptions if above is not defined)
        0, primary_variable2;
    }
}

COLLECTION OF COLLECTION primary_variable2
{
    LABEL "Primary Variable";
    HELP "Primary Variable Help";
    MEMBERS
    {
        // 3rd level descriptions (becomes top level descriptions if above is not defined)
        DEVICE_VARIABLE, deviceVariables2[0], "Dev Var 0", "Device Variable at variable code 0";
        DEVICE_VARIABLE2, deviceVariables2[0];

    }
}

ARRAY OF COLLECTION dynamic_variables_test
{
    LABEL "Dynamic Variables";
    ELEMENTS
    { 
        0, primary_variable,   "PV", "Primary Variable";
        1, secondary_variable, "SV", "Secondary Variable";
    }
}
COLLECTION OF COLLECTION primary_variable
{
    LABEL "Primary Variable";
    HELP "Primary Variable Help";
    MEMBERS
    {
        DEVICE_VARIABLE, deviceVariables[0], "Dev Var 0", "Device Variable at variable code 0";

    }
}

COLLECTION OF COLLECTION secondary_variable
{
    LABEL "Secondary Variable";
    MEMBERS
    {
        DEVICE_VARIABLE, deviceVariables[1], "Dev Var 1", "Device Variable at variable code 1";
    }
}
/****************************************************************************** */
/* Variables                                                              */
/****************************************************************************** */


VARIABLE pressureValue
{
    LABEL "Pressure Value";
    HELP "Pressure value variable help";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
    
    CONSTANT_UNIT "psi";

}

VARIABLE pressureUnits 
{
    LABEL "Pressure unit";
    HELP [digital_units_help];
    CLASS CORRECTION;

    HANDLING READ & WRITE;
    TYPE ENUMERATED  
    {
        /*One Byte Engr Unit */
        { 1,   [InH2O],     [inches_of_water_68_degrees_F_help] },
        { 2,   [InHg],      [inches_of_mercury_0_degrees_C_help] },
        { 3,   [FtH2O],     [feet_of_water_68_degrees_F_help] },
        { 4,   [mmH2O],     [millimeters_of_water_68_degrees_F_help] },
        { 5,   [mmHg],      [millimeters_of_mercury_0_degrees_C_help] },
        { 6,   [psi],       [pounds_per_square_inch_help] },
        { 7,   [bar],       [bars_help] },
        { 8,   [mbar],      [millibars_help] },
        { 9,   [g_SqCm],    [grams_per_square_centimeter_help] },
        { 10,  [kg_SqCm],   [kilograms_per_square_centimeter_help] },
        { 11,  [PA],        [pascals_help] },
        { 12,  [kPA],       [kilopascals_help] },
        { 13,  [torr],      [torr_help] },
        { 14,  [ATM],       [atmospheres_help] },
        { 145, [in_H2O_60_degrees_F], [inches_of_water_60_degrees_F_help] },
        { 237, [mega_pascals],      [megapascals_help] },
        { 238, [in_H2O_4_degrees_C],    [inches_of_water_4_degrees_C_help] },
        { 239, [mm_H2O_4_degrees_C],    [millimeters_of_water_4_degrees_C_help] }
    }
}

VARIABLE pressureFamily
{
    LABEL "Press Dev fmly";
    HELP [device_family_help];
    CLASS DEVICE;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 ,"Family 0"},
        {1 , "Family 1"},
        {2 , "Family 2"},
        {3 , "Family 3"}
    }
}

VARIABLE pressureClassification
{
    LABEL "Press Class";
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 ,"Press Class 0"},
        {1 , "Press Class 1"},
        {2 , "Press Class 2"},
        {3 , "Press Class 3"}
    }
}

VARIABLE pressurePDQ
{
    LABEL "Press PDQ";
    HELP [process_data_quality_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 , process_data_status(0)},
        {1 , process_data_status(1)},
        {2 , process_data_status(2)},
        {3 , process_data_status(3)}

            
    }
}

VARIABLE pressureLS
{
    LABEL "Press LS";
    HELP [limit_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 ,"Press LS 0"},
        {1 , "Press LS 1"},
        {2 , "Press LS 2"},
        {3 , "Press LS 3"}
    }
}

VARIABLE pressureStatus
{
    LABEL "Press Fmly stat";
    HELP [family_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE BIT_ENUMERATED
    {
         {0 ,"Press Fmly stat 0"},
        {1 , "Press Fmly stat 1"},
        {2 , "Press Fmly stat 2"},
        {3 , "Press Fmly stat 3"}
    } 
}

// Different Label and Help or Variable Test
VARIABLE pressureUSL
{

    LABEL "Read-only float variable";
    HELP "Help for read-only float variable";

    HANDLING READ;
    TYPE FLOAT;
}

// Different Label, Help and Display/Edit formats for Variable Test
// Actions for Environment Test
VARIABLE pressureLSL
{

    LABEL "Class diagnostic variable";
    HELP "Help for diagnostic float variable";
    CLASS DIAGNOSTIC;

    HANDLING READ;
    TYPE FLOAT;

}

// Different Label, Help and Display/Edit formats for Variable Test
VARIABLE pressureMinimumSpan
{

    LABEL "Min/Max";
    HELP "min 0.00, max 100.00";
    TYPE FLOAT
    {
        MAX_VALUE 100.0;
        MIN_VALUE   0.0;
    }

}

// Different Label, Help and Display/Edit formats for Variable Test
VARIABLE pressureDampingValue
{

    LABEL "Display/Edit format (.3f)";
    HELP "Help for Display/Edit format (.3f)";
    TYPE FLOAT
    {
        DISPLAY_FORMAT ".3f";
        EDIT_FORMAT    ".3f";
    }

    CONSTANT_UNIT [sec];
}

VARIABLE pressureSerialNumber
{

    LABEL "Disp/Edit format (7d)";
    HELP "Help for Display/Edit format (7d)";
    TYPE UNSIGNED_INTEGER (3)
    {
        DISPLAY_FORMAT  "7d";
        EDIT_FORMAT     "7d";
    }

    CLASS DEVICE;
    HANDLING READ & WRITE;

}


VARIABLE temperatureValue
{
    LABEL "Snsr temp";
    HELP "Sensor Temperature- Temperature of the sensor that is making the process measurement.";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE FLOAT;
}

VARIABLE temperatureUnits 
{
    LABEL "Temp unit";
    HELP [digital_units_help];
    CLASS CORRECTION;
    HANDLING READ & WRITE;
    TYPE ENUMERATED 
    {
        { 0x20,  [degC],                  [degrees_celsius_help] },
        { 0x21,  [degF],                  [degrees_fahrenheit_help] },
        { 0x22,  [degR],                  [degrees_rankine_help] },
        { 0x23,  [Kelvin],                [degrees_kelvin_help] }
    }
}

VARIABLE temperatureFamily
{
    LABEL "Temp fmly";
    HELP [device_family_help];
    CLASS DEVICE;
    HANDLING READ;
    TYPE ENUMERATED
    {
               {0 ,"Temp fmly 0"},
        {1 , "Temp fmly 1"},
        {2 , "Temp fmly 2"},
        {3 , "Temp fmly 3"}
    }
}

VARIABLE temperatureClassification
{
    LABEL "Temp Class";
    HELP [classification_help];
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 ,"Temp Class 0"},
        {1 , "Temp Class 1"},
        {2 , "Temp Class 2"},
        {3 , "Temp Class 3"}
    }
}

VARIABLE temperaturePDQ
{
    LABEL "Temp PDQ";
    HELP [process_data_quality_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 , process_data_status(0)},
        {1 , process_data_status(1)},
        {2 , process_data_status(2)},
        {3 , process_data_status(3)}

            
    }
}

VARIABLE temperatureLS
{
    LABEL "Temp LS";
    HELP [limit_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
       {0 ,"Temp LS 0"},
        {1 , "Temp LS 1"},
        {2 , "Temp LS 2"},
        {3 , "Temp LS 3"}

    }
}

VARIABLE temperatureStatus
{
    LABEL "Temp Fmly stat";
    HELP [family_status_help];
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE BIT_ENUMERATED
    {
        {0 ,"Temp Fmly stat 0"},
        {1 , "Temp Fmly stat 1"},
        {2 , "Temp Fmly stat 2"},
        {3 , "Temp Fmly stat 3"}
    } 
}

VARIABLE process_data_status 
{
    LABEL "Process Data Status";
    HELP "Process Data Status";
    CLASS CORRECTION & DYNAMIC;
    HANDLING READ;
    TYPE ENUMERATED
    {
        {0 ,"Process Data Status 0"},
        {1 , "Process Data Status 1"},
        {2 , "Process Data Status 2"},
        {3 , "Process Data Status 3"}
    }
}    