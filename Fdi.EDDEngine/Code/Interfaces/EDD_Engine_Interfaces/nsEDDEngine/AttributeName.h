#pragma once
#include <set>
//#include "typedefs.h"
#ifdef _WIN32

#if defined (EXPORT_INTERFACE)
#define _INTERFACE __declspec(dllexport)
#else
#define _INTERFACE __declspec(dllimport)
#endif
#else
#define _INTERFACE __attribute__((interface))
#include "PlatformCommon.h"
#endif


namespace nsEDDEngine
{
	enum AttributeName
	{ 
		item_information		= 0,
		label					= 1,
		help					= 2,
		validity				= 3,
		members					= 4,
		handling				= 5,
		response_codes			= 6,
		height					= 7,
		post_edit_actions		= 8,
		pre_edit_actions		= 9,
		refresh_actions			= 10,
		relation_update_list	= 11,
		relation_watch_list		= 12,
		relation_watch_variable	= 13,
		width					= 14,
		access					= 15,
		class_attr				= 16,
		constant_unit			= 17,
		cycle_time				= 18,
		emphasis				= 19,
		exit_actions			= 20,
		identity				= 21,
		init_actions			= 22,
		line_color				= 23,
		line_type				= 24,
		max_value				= 25,
		min_value				= 26,
		command_number			= 27,
		post_read_actions		= 28,
		post_write_actions		= 29,
		pre_read_actions		= 30,
		pre_write_actions		= 31,
		type_definition			= 32,
		y_axis					= 33,
		appinstance				= 34,
		axis_items				= 35,
		block_b					= 36,
		block_b_type			= 37,
		capacity				= 38,
		characteristics			= 39,
		chart_items				= 40,
		chart_type				= 41,
		charts					= 42,
		collection_items		= 43,
		block_number			= 44,
		count					= 45,
		default_value			= 46,
		default_values			= 47,
		definition				= 48,
		display_format			= 49,
		display_items			= 50,
		edit_display_items		= 51,
		edit_format				= 52,
		edit_items				= 53,
		elements				= 54,
		enumerations			= 55,
		file_items				= 56,
		first					= 57,
		graph_items				= 58,
		graphs					= 59,
		grid_items				= 60,
		grids					= 61,
		image_items				= 62,
		image_table_index		= 63,
		index					= 64,
		indexed					= 65,
		initial_value			= 66,
		items					= 67,
		keypoints_x_values		= 68,
		keypoints_y_values		= 69,
		last					= 70,
		length					= 71,
		link					= 72,
		list_items				= 73,
		lists					= 74,
		local_parameters		= 75,
		menu_items				= 76,
		menus					= 77,
		method_items			= 78,
		method_parameters		= 79,
		method_type				= 80,
		methods					= 81,
		number_of_elements		= 82,
		number_of_points		= 83,
		on_update_actions		= 84,
		operation				= 85,
		orientation				= 86,
		parameter_lists			= 87,
		parameters				= 88,
		post_user_actions		= 89,
		post_rqst_actions		= 90,
		uuid					= 91,
		reference_array_items	= 92,
		refresh_items			= 93,
		scaling					= 94,
		scaling_factor			= 95,
		slot					= 96,
		source_items			= 97,
		style					= 98,
		sub_slot				= 99,
		time_format				= 100,
		time_scale				= 101,
		transaction				= 102,
		unit_items				= 103,
		variable_type			= 104,
		vectors					= 105,
		waveform_items			= 106,
		waveform_type			= 107,
		write_as_one_items		= 108,
		component_byte_order	= 109,  
		x_axis					= 110,
		x_increment				= 111,
		x_initial				= 112,
		x_values				= 113,
		y_values				= 114,
		view_min				= 115,
		view_max				= 116,
		reserved117				= 117, //min_axis			
		reserved118				= 118, //max_axis			
		reserved119				= 119, //default_reference	
		addressing				= 120,
		can_delete				= 121,
		check_configuration		= 122,
		classification			= 123,
		component_parent		= 124,
		component_path			= 125,
		component_relations		= 126,
		components				= 127,
		declaration				= 128,
		detect					= 129,
		device_revision			= 130,
		device_type				= 131,
		edd						= 132,
		manufacturer			= 133,
		maximum_number			= 134,
		minimum_number			= 135,
		protocol				= 136,
		redundancy				= 137,
		relation_type			= 138,
		component_connect_point	= 139,
		scan					= 140,
		scan_list				= 141,
		private_attr			= 142,
		visibility				= 143,
		product_uri				= 144,
		api						= 145,
		header					= 146,
		write_mode				= 147,
		plugin_items			= 148,
		files					= 149,
		plugins					= 150,
		initial_values			= 151,
		post_rqstreceive_actions= 152,
		shared					= 153,
		variable_status			= 154,
		count_ref				= 155,
		min_value_ref			= 156,
		max_value_ref			= 157,
		image_id				= 158,
		_end_					= 159		// This must be the last value
	};



	class _INTERFACE AttributeNameSet
	{
    private:
        std::set<AttributeName> *m_set;

	public:
		// Constructor
		AttributeNameSet ();

		// Constructor
		AttributeNameSet( AttributeName attr );

		// Constructor
		AttributeNameSet( const AttributeNameSet &origSet );

		// Destructor
		~AttributeNameSet();

        // Initializer
        AttributeNameSet& operator= (const AttributeNameSet & origSet );

		// Initializer
		AttributeNameSet& operator= (AttributeName attr );

		// Equals operator
		bool operator== (AttributeNameSet &rhs );

		// Add to the set
		AttributeNameSet operator+ (AttributeName attr);

		// Add to the set
		AttributeNameSet& operator+= (AttributeName attr);

		// Add to the set with an |
		AttributeNameSet operator| ( AttributeName attr);

		// Add to the set with an |
		AttributeNameSet& operator|= ( AttributeName attr);

		// Remove this attribute from the set
		AttributeNameSet& operator-= (AttributeName attr);

		// Keep only the values in both sets
		AttributeNameSet operator- ( AttributeNameSet &rhs);

		// Return a set with only this AttributeName, if it is in the current set
		AttributeNameSet operator& ( AttributeName attr);

		// Keep only the values in both sets
		AttributeNameSet operator& ( AttributeNameSet &rhs);

		// Keep only the values in both sets
		AttributeNameSet& operator&= ( AttributeNameSet &rhs);

		// Keep only the values in both sets
		AttributeNameSet& operator&= ( AttributeName attr);

        // Find the attribute name in the set and return true if found
        bool isMember( AttributeName attr) const;

        // return the attributeName that is first in the list
        AttributeName getFirstItem() const;

        // Insert a member
        void insert(AttributeName attr );

        // Clear the set
        void clear( );

        // Return true if empty
        bool empty( ) const;

        // get the number of elements in the set
        size_t size() const;

        // get the number of elements matching the key
        size_t count( AttributeName attr) const;



#if 0
		// Assignment
		AttributeNameSet operator= ( AttributeName attr);
#endif

	};


	// Create an AttributeNameSet by "+" two AttributeNames
	inline AttributeNameSet operator+ (AttributeName a, AttributeName b)
	{
		AttributeNameSet result;

		result += a;
		result += b;

		return result;
	}

	// Create an AttributeNameSet by "|" two AttributeNames 
	inline AttributeNameSet operator| (AttributeName a, AttributeName b)
	{
		return a + b;
	}

	inline AttributeNameSet operator& (AttributeName attr, AttributeNameSet attr_set)
	{
		return attr_set & attr;
	}

}

