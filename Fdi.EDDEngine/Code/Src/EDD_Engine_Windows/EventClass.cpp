#include "stdafx.h"
#include "EventClass.h"


WindowsEventClass::WindowsEventClass(void)
{
	m_handle = NULL;
}

WindowsEventClass::~WindowsEventClass(void)
{
	Close();
}

void* WindowsEventClass::GetHandle()
{	
	return m_handle;
}

BOOL WindowsEventClass::Create()
{
	m_handle = ::CreateEvent(NULL, TRUE/*manual*/, FALSE/*init*/, NULL);
	
	if(m_handle == NULL)
	{
		return false;
	}
	return true;
}

BOOL WindowsEventClass::Close()
{
	BOOL bret = true;
	
	if (m_handle != NULL)
	{
		bret = CloseHandle(m_handle);
		m_handle = NULL;
	}

	return bret;
}

/**
*
* Set
* set an event to signaled
*
**/
BOOL WindowsEventClass::Set()
{
	return SetEvent(m_handle);
}

/**
*
* Wait
* wait for an event -- wait for an event object
* to be set to signaled.  must be paired with a
* call to reset within the same thread.
*
**/
BOOL WindowsEventClass::Wait()
{
	if (WaitForSingleObject(m_handle, INFINITE) != WAIT_OBJECT_0)
	{
		return false;
	}
	
	return true;
}

/**
*
* Reset
* reset an event flag to unsignaled
* wait must be paired with reset within the same thread.
*
**/
void WindowsEventClass::Reset()
{
	ResetEvent(m_handle);
}

