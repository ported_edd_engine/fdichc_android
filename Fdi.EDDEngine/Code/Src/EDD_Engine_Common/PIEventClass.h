#pragma once

#include <string>
#include "TypesCommon.h"

template <typename PlatformType>
class PIEventClass
{
private:

	PlatformType* m_pPlatformType;

public:
	// Constructors
	PIEventClass()
	{
		m_pPlatformType = new PlatformType();
	}

	virtual ~PIEventClass()
	{
		if (m_pPlatformType != NULL)
		{
			delete m_pPlatformType;
			m_pPlatformType = NULL;
		}
	}

	BOOL Create()
	{
		return m_pPlatformType->Create();
	}

	BOOL Close()
	{
		return m_pPlatformType->Close();
	}

	BOOL Set()
	{
		return m_pPlatformType->Set();
	}

	BOOL Wait()
	{
		return m_pPlatformType->Wait();
	}

	void Reset()
	{
		return m_pPlatformType->Reset();
	}

	void* GetHandle()
	{
		return m_pPlatformType->GetHandle();
	}

};
