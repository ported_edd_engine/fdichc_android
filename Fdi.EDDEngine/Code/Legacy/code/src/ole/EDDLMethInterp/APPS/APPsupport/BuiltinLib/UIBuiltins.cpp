#include "stdafx.h"
#include "ddbDevice.h"
#include "MethodInterfaceDefs.h"
#include "MEE.h"
#include "RTN_CODE.H"
#include "varientTag.h"
#include <climits>
#include "float.h"

#define UIEVENT_TIMEOUT		500				//0.5 second


/***************** static helper functions *****************************************************/
void add_textMsg( ACTION_UI_DATA& aud, tchar* pMsg)
{
	int len = (int)_tstrlen(pMsg);

	if(aud.textMessage.pchTextMessage != NULL)
	{
		delete[] aud.textMessage.pchTextMessage; 
		         aud.textMessage.pchTextMessage = NULL; 
	}
	aud.textMessage.iTextMessageLength = 0;

	aud.userInterfaceDataType = TEXT_MESSAGE;
	aud.textMessage.pchTextMessage     = new tchar[len + 1];
	aud.textMessage.iTextMessageLength = len+1;
	if(len > 0)
	{
		_tstrncpy(aud.textMessage.pchTextMessage ,pMsg, len);
	}
	aud.textMessage.pchTextMessage[len] = 0;
}
static
void add_optionList( ACTION_UI_DATA& aud, tchar* pListString)
{
	tchar* strList = NULL;
	int c, i, inLen = (int)_tstrlen(pListString);

	if (pListString == NULL)  return;

	if (inLen > 0)
	{
		strList = new tchar[inLen + 1];
		_tstrcpy(strList,pListString);

		for ( c = 0, i = 0; i < inLen; i++ ) 	
		{
			if ((strList[i] == _T(';'))   || 
				(strList[i] != _T(';')   &&  strList[i+1] == 0) )
			{
				c++;
			}
		}

		aud.ComboBox.pchComboElementText = new tchar[_tstrlen(strList) + 1];
		_tstrcpy(aud.ComboBox.pchComboElementText,strList);
		aud.ComboBox.iNumberOfComboElements = c;
	
		delete [] strList;
	
	}
	else
	{	//When no string is passed to combo box, make it blank by allocating 0ne byte
		aud.ComboBox.pchComboElementText = NULL;
		aud.ComboBox.iNumberOfComboElements = 0;
	}
}
/***********************************************************************************************/


int CBuiltIn::DELAY_TIME
			(
				int iTimeInSeconds
			)
{
	int retval = BI_SUCCESS;

	PS_Sleep(iTimeInSeconds * 1000);

	return (retval);
}

int CBuiltIn::put_message(tchar *message, long *glob_var_ids, unsigned long *glob_var_memberids, int iNumberOfItemIds)
{
	ACTION_UI_DATA structUIData;
	tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged=false;


	//int rc=m_pDevice->dictionary->get_string_translation(message,message,_tstrlen(message) +1);
	//Don't need to call get_string_translation()
	// because the same job is done in bltin_format_string()
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					message, NULL, NULL, (unsigned long*)glob_var_ids, glob_var_memberids, iNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData,out_buf);

	structUIData.bUserAcknowledge=false;
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = NULL;
	//info request
	int retval = m_pDevice->sendUIMethod(structUIData);

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;
	}

	if(retval == BI_ABORT)
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else
	{
		return (retval);
	}
}

int  CBuiltIn::PUT_MESSAGE( tchar *message )
{
	ACTION_UI_DATA structUIData;
	tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged=false;


	//int rc=m_pDevice->dictionary->get_string_translation(message,message,_tstrlen(message) +1);
	//Don't need to call get_string_translation()
	// because the same job is done in bltin_format_string()
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					message, NULL, NULL, NULL, NULL, 0, &bDynVarValsChanged);

	add_textMsg(structUIData,out_buf);

	structUIData.bUserAcknowledge=false;
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = NULL;
	//info request
	int retval = m_pDevice->sendUIMethod(structUIData);

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;
	}
	
	if(retval == BI_ABORT)
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else
	{
		return (retval);
	}

}


//This function is used to display status message
int CBuiltIn::ACKNOWLEDGE( tchar *message )
{
	ACTION_UI_DATA	structUIData;
	tchar			out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool			bDynVarValsChanged=false;


	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					message, NULL, NULL, NULL, NULL, 0, &bDynVarValsChanged);
			
	add_textMsg(structUIData,out_buf);

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;// just defensive
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
	int retval = m_pDevice->sendUIMethod(structUIData);

	//waiting for user to enter input
	if ((retval == BI_SUCCESS) )
	{
		bool bEventSignaled = m_pDevice->LockMethodUIEvent();
	    //get UI builtin response
		if (bEventSignaled)
		{
			retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
		}
		else
		{
			retval = BI_ERROR;
		}

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	}
	else
	{
		retval = BI_ERROR;
	}

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;  
		structUIData.textMessage.pchTextMessage = NULL;
	}

	if (BI_ABORT == retval) //cancelled
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else
	{
		retval = ConvertToBLTINCode(retval);
		return (retval);
	}
}


//This function is shared by procotol HART, PROFIBUS and FIELDBUS.
//When it is used by HART and PROFIBUS, glob_var_memberids is set to NULL.
int CBuiltIn::acknowledge(	tchar *message,long *glob_var_ids, unsigned long *glob_var_memberids, int iNumberOfItemIds	)
{
	ACTION_UI_DATA structUIData;
	tchar			disp_msg[DOUBLE_MAX_DD_STRING]={0};
	bool			bDynVarValsChanged=false;


	(void) bltin_format_string2 (disp_msg, NUM_ELEM(disp_msg),
					message, NULL, NULL, (unsigned long*)glob_var_ids, glob_var_memberids, iNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData,disp_msg);

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
	int retval = m_pDevice->sendUIMethod(structUIData);

	//update prompt message dynamically
	structUIData.bDisplayDynamic = true;
	if ((retval == BI_SUCCESS))
	{
		while (BI_SUCCESS == retval)
		{
			if( true == bDynVarValsChanged )	
			{
				_tstrcpy(disp_msg,mt_String);
				(void)bltin_format_string2 (disp_msg, NUM_ELEM(disp_msg),
						message, NULL, NULL, (unsigned long*)glob_var_ids, glob_var_memberids, iNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, disp_msg) != 0)
				{
					//Prompt string is changed
					add_textMsg(structUIData,disp_msg);
					//send UI Data
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}

			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}
			}
		}	//end of while loop

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	}
	else
	{
		retval = BI_ERROR;
	}

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL; 
	}

	if (BI_ABORT == retval)	//cancelled
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else
	{
		
		return (retval);
	}
}


int CBuiltIn::display(	tchar *message,long *glob_var_ids,int iNumberOfItemIds	)
{
	ACTION_UI_DATA structUIData;
	tchar	disp_msg[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged = true;


	(void) bltin_format_string2 (disp_msg, NUM_ELEM(disp_msg),
					message, NULL, NULL, (unsigned long*)glob_var_ids, NULL, iNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData,disp_msg);	// stevev 26dec07 - common code

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;

	int retval = m_pDevice->sendUIMethod(structUIData);

	//update prompt message dynamically
	structUIData.bDisplayDynamic = true;
	if (retval == BI_SUCCESS)
	{
		while (BI_SUCCESS == retval)
		{
			if( true == bDynVarValsChanged )	
			{
				_tstrcpy(disp_msg,mt_String);
				(void)bltin_format_string2 (disp_msg, NUM_ELEM(disp_msg),
						message, NULL, NULL, (unsigned long*)glob_var_ids, NULL, iNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, disp_msg) != 0)
				{
					add_textMsg(structUIData, disp_msg);
					//send UI Data
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}

			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}
			}
		}	//end of while loop

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	}
	else
	{
		retval = BI_ERROR;
	}

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage; 
		structUIData.textMessage.pchTextMessage = NULL;
	}
	
	if (BI_ABORT == retval) //cancelled
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else
	{
		return (retval);
	}
}


int CBuiltIn::_display_xmtr_status
		(
			long lItemId
			, int  iStatusValue
		)
{
	tstring status_str;
	hCitemBase* pIB = NULL;
	int		retVal = BI_ERROR;


	if (m_pDevice->getItemBySymNumber(lItemId, &pIB) == BI_SUCCESS)
	{
		hCVar *pVar=(hCVar *)pIB;
		switch(pVar->VariableType() )
		{
			case vT_Enumerated:
			{
				hCEnum *pEnum=(hCEnum *)pVar;
				pEnum->procureString(iStatusValue, status_str); // input value, get a string
				break;
			}		
			
			case vT_BitEnumerated:
			{
				hCBitEnum *pBitEnum=(hCBitEnum *)pVar;
				pBitEnum->procureString(iStatusValue, status_str); // input value, get a string
				break;
			}		
			default:
				//do something?
				break;
		}	

		if ((!status_str.empty()) && (status_str.length() >0))
		{
			retVal = ACKNOWLEDGE((tchar*)status_str.c_str());
		}
	}

	return (retVal);
}
	
int CBuiltIn::select_from_list
		(
			tchar *pchDisplayString
			, long *lItemId
			, int iNumberOfItemIds
			, tchar *pchList
		)
{
	ACTION_UI_DATA structUIData;
	tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged=false;


	int retval = bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					pchDisplayString, NULL, NULL, (unsigned long *)lItemId, NULL, iNumberOfItemIds, &bDynVarValsChanged);

	if (retval == BLTIN_SUCCESS)
	{
		tchar	*pchSelList = new wchar_t[_tstrlen(pchList)+1];
	
		//fill out userInterfaceDataType = TEXT_MESSAGE, textMessage.pchTextMessage and textMessage.iTextMessageLength
		add_textMsg(structUIData, out_buf);	// stevev 26dec07 - common code

		//*** CHECK ****
		//Each element in the ';' sep'd list may have multi languages AND variable names as PUT_MESSAGE
		retval = m_pDevice->dictionary->get_string_translation(pchList,pchSelList, (int)_tstrlen(pchList) + 1, m_pDevice->device_sLanguageCode);

		//fill out ComboBox.pchComboElementText and ComboBox.iNumberOfComboElements
		add_optionList(structUIData, pchSelList);
		structUIData.ComboBox.nCurrentIndex = 0;	//no selection yet

		structUIData.userInterfaceDataType = SELECTION;

		structUIData.bUserAcknowledge=true;
		structUIData.bDelayBuiltin = false;
		structUIData.bMethodAbortedSignalToUI = false;
		structUIData.bDisplayDynamic = false;
		//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
		retval = m_pDevice->sendUIMethod(structUIData);

		//waiting for user to enter input
		structUIData.bDisplayDynamic = true;
		if ((retval == BI_SUCCESS))
		{
			while (BI_SUCCESS == retval)
			{
				if ( true == bDynVarValsChanged )	
				{
					out_buf[0] = 0;
					(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
							pchDisplayString, NULL, NULL, (unsigned long *)lItemId, NULL, iNumberOfItemIds, &bDynVarValsChanged);
		
					if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
					{
						add_textMsg(structUIData, out_buf);

						structUIData.userInterfaceDataType = SELECTION;
						//send UI Data
						retval = m_pDevice->sendUIMethod(structUIData);
					}
				}

				//waiting for builtin consumer to send signal of response
				if (BI_SUCCESS == retval)
				{
					bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);				
					//get UI builtin response
					if (bEventSignalled)
					{
						retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
						break;
					}
				}
			}	//end of while loop

			//signal is received, event is done used.
			m_pDevice->ResetMethodUIEvent();
		}
		else
		{
			retval = BI_ERROR;
		}

  		structUIData.bDisplayDynamic = false;
	
		if (pchSelList)
		{
			delete [] pchSelList;
		}
	}

	if(NULL != structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;
	}
	
	if(NULL != structUIData.ComboBox.pchComboElementText)
	{
		delete[] structUIData.ComboBox.pchComboElementText;
		structUIData.ComboBox.pchComboElementText = NULL;
	}

	if (BI_ABORT == retval)	//cancelled
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else if ((BI_SUCCESS == retval) && (structUIData.userInterfaceDataType == SELECTION))
	{
		CValueVarient selection = structUIData.vUserRsp;
		
		structUIData.ComboBox.nCurrentIndex = CV_UI4(&selection);
		return structUIData.ComboBox.nCurrentIndex;
	}
	else
	{
		return BI_ERROR;
	}
}

/*Arun 190505 Start of code*/

tchar* CBuiltIn::BUILD_MESSAGE
		(
			tchar* dest,
			int destStrSize,
			tchar* message
		)
{        
	bool bDynVarValsChanged = false; 

	int retval = bltin_format_string2 (dest, destStrSize, message, NULL, NULL, NULL, NULL, 0, &bDynVarValsChanged);

	if (retval == BI_SUCCESS)
	{
		return dest;
	}
	else
	{
		return NULL;
	}
}

/*End of code*/

//Added By Anil September 26 2005 for _MenuDisplay
int CBuiltIn::_MenuDisplay(	long lMenuId,tchar *pchOptionList,long* lselection	)
{
	int nRetVal = BI_ERROR;
	int iNumOptionList = 0;
	ACTION_UI_DATA structUIData;

	//Fill the structure required for the Menu display
	structUIData.userInterfaceDataType = MENU;
	structUIData.opRef.addBlockID(0, 0);
	structUIData.opRef.addOperationalID(lMenuId, 0, iT_Menu);

	structUIData.bUserAcknowledge=true;
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;


	//Translate into the current language
	nRetVal = (m_pDevice->dictionary)->get_string_translation(pchOptionList,pchOptionList,
												(int)_tstrlen(pchOptionList) + 1, m_pDevice->device_sLanguageCode);

	//fill out ComboBox.pchComboElementText and ComboBox.iNumberOfComboElements
	structUIData.ComboBox.pchComboElementText = NULL;
	add_optionList(structUIData, pchOptionList);

	if(nRetVal == BLTIN_SUCCESS)
	{
		const wchar_t *StrDelim = L";";
		wchar_t *OptionStr = NULL, *OptionList = NULL;
        OptionStr = wcstok(pchOptionList, StrDelim, &OptionList);
		while(OptionStr != NULL)
		{
			iNumOptionList++;
			if(iNumOptionList > 3)
			{
				nRetVal = BI_ERROR;
				break;
			}
            OptionStr = wcstok(NULL, StrDelim, &OptionList);
	    }
	}

	//Before calling method supported interface function, check range of the number of options per HART spec501 
	if ((iNumOptionList > 0) && (iNumOptionList <= 3))
	{
	    nRetVal = m_pDevice->sendUIMethod(structUIData);

		if (nRetVal == BI_SUCCESS)
		{
			bool bEventSignalled = m_pDevice->LockMethodUIEvent();
			//get UI builtin response
			if (bEventSignalled)
			{
				nRetVal = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
			}
			else
			{
				nRetVal = BI_ERROR;
			}

			//signal is received, event is done used.
			m_pDevice->ResetMethodUIEvent();
		}
		else
		{
			nRetVal = BI_ERROR;
		}
	}
	else
	{
			nRetVal = BI_ERROR;
	}

	if(NULL != structUIData.ComboBox.pchComboElementText)
	{
		delete[] structUIData.ComboBox.pchComboElementText;
	}

	if (BI_ABORT == nRetVal) //cancelled
	{
		this->process_abort();
		return (METHOD_ABORTED);
	}
	else if ((nRetVal == BI_SUCCESS) && (lselection))
	{
		CValueVarient selection = structUIData.vUserRsp;
		

		*lselection = CV_UI4(&selection);
	}

	return nRetVal;

}//End of MenuDisplay

long CBuiltIn::select_from_menu(	tchar *pchDisplayString
								, unsigned long *pulItemIds
								, unsigned long *pulMemberIds
								, long	iNumberOfItemIds
								, tchar *pchOptionList, long* lselection )
{
	tchar out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged=false;
	ACTION_UI_DATA structUIData;


	//Embeded variable value into prompt string
	int nRetVal = bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					pchDisplayString, NULL, NULL, pulItemIds, pulMemberIds, iNumberOfItemIds, &bDynVarValsChanged);

	if (nRetVal == BLTIN_SUCCESS)
	{
		tchar	*pchMenuList = new wchar_t[_tstrlen(pchOptionList)+1];
	
		//Fill the structure required for the Menu display
		add_textMsg(structUIData, out_buf);
		structUIData.userInterfaceDataType = SELECTION;
		structUIData.ComboBox.nCurrentIndex = 0;	//default value

		//Translate into the current language
		nRetVal = (m_pDevice->dictionary)->get_string_translation(pchOptionList, pchMenuList,
													(int)_tstrlen(pchOptionList) + 1, m_pDevice->device_sLanguageCode);

		//fill out ComboBox.pchComboElementText and ComboBox.iNumberOfComboElements
		add_optionList(structUIData, pchMenuList);

		//Call the method support interface.
		structUIData.bUserAcknowledge=true;
		structUIData.bDelayBuiltin = false;
		structUIData.bMethodAbortedSignalToUI =false;
		structUIData.uDelayTime = 0;// just defensive
		structUIData.bDisplayDynamic = false;
		//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
		nRetVal = m_pDevice->sendUIMethod(structUIData);

		//waiting for user to enter input
		structUIData.bDisplayDynamic = true;
		if (nRetVal == BI_SUCCESS)
		{
			while (BLTIN_SUCCESS == nRetVal)
			{
				if( true == bDynVarValsChanged )	
				{
					(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
							pchDisplayString, NULL, NULL, pulItemIds, pulMemberIds, iNumberOfItemIds, &bDynVarValsChanged);

					if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
					{
						add_textMsg(structUIData,out_buf);
						structUIData.userInterfaceDataType = SELECTION;				
						//send UI Data and receive user input
						nRetVal = m_pDevice->sendUIMethod(structUIData);
					}
				}

				//waiting for builtin consumer to send signal of response
				if (BLTIN_SUCCESS == nRetVal)
				{
					bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);
					if (bEventSignalled)
					{
						nRetVal = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
						break;
					}
				}
			}	//end of while loop

			//signal is received, event is done used.
			m_pDevice->ResetMethodUIEvent();
		}
		else
		{
			nRetVal = BLTIN_NOT_IMPLEMENTED;
		}

		if (pchMenuList)
		{
			delete [] pchMenuList;
		}
	}

	//free memory in prompt
	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;	
	}
	//free memory in menu list
	if(NULL != structUIData.ComboBox.pchComboElementText)
	{
		delete[] structUIData.ComboBox.pchComboElementText;
		structUIData.ComboBox.pchComboElementText = NULL;
	}

	switch (nRetVal)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			return (METHOD_ABORTED);
		}
		break;

		case BI_SUCCESS:
		{
			if ( lselection ) //set the return value, if it exists.
			{
				CValueVarient selection = structUIData.vUserRsp;
				
				*lselection = CV_UI4(&selection) + 1;      // adding 1 to make selection 1-based as per FF spec
				
				nRetVal = BLTIN_SUCCESS;
			}
			else
			{
				nRetVal = BLTIN_VAR_NOT_FOUND;
			}
			break;
		}
		case BI_ERROR:
		{
			nRetVal = BLTIN_VAR_NOT_FOUND;
		}
		break;

		default:
			nRetVal = ConvertToBLTINCode(nRetVal);
			break;
	}
	return nRetVal;

}//End of select_from_menu


//for FF protocol
long CBuiltIn::get_acknowledgement2 (tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums,
											unsigned long *pulItemIds, unsigned long *pulMemberIds, long lNumberOfItemIds)
{
	ACTION_UI_DATA	structUIData;
	tchar			out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool			bDynVarValsChanged = false;


	//Embeded variable value into prompt string
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData,out_buf);

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;// just defensive
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	int retval = m_pDevice->sendUIMethod(structUIData);

	//waiting for user to enter input
	if (BI_SUCCESS == retval)
	{
		while (BI_SUCCESS == retval)
		{
			if( true == bDynVarValsChanged )	
			{
				_tstrcpy(out_buf,mt_String);
				(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
				{
					//Prompt string is changed
					add_textMsg(structUIData,out_buf);
					//send UI Data
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}
			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = false;

				bEventSignalled = m_pDevice->LockMethodUIEvent();
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}				
			}
		}	//end of while loop

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();

	}
	else
	{
		retval = BI_ERROR;
	}

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;  
		structUIData.textMessage.pchTextMessage = NULL;
	}

	switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
		}
		break;
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
		}
		break;
		case BI_ERROR:
		{
			retval = BLTIN_VAR_NOT_FOUND;
		}
		break;
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}

	return retval;
}


long CBuiltIn::display_dynamics2 (tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums,
									unsigned long *pulItemIds, unsigned long *pulMemberIds, long lNumberOfItemIds)
{
	ACTION_UI_DATA	structUIData;
	tchar			out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool			bDynVarValsChanged = true;	//this builtin is required to continue to update value


	//Embeded variable value into prompt string
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData, out_buf);

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;// just defensive
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
	int retval = m_pDevice->sendUIMethod(structUIData);

	//update prompt message dynamically
	structUIData.bDisplayDynamic = true;
	if (retval == BI_SUCCESS)
	{
		while (BI_SUCCESS == retval)
		{
			if(bDynVarValsChanged)
			{
				_tstrcpy(out_buf, mt_String);
				(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
											pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
				{
					add_textMsg(structUIData, out_buf);

					//send UI Data continuously
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}

			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}				
			}
		}	//end of while loop

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	}
	else
	{
		retval = BI_ERROR;
	}

	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;  
		structUIData.textMessage.pchTextMessage = NULL;
	}

	switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
		}
		break;
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
		}
		break;
		case BI_ERROR:
		{
			retval = BLTIN_VAR_NOT_FOUND;
		}
		break;
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}
	return retval;
}


long CBuiltIn::delayfor2
			(
				long iTimeInSeconds
				, tchar *pchDisplayString	/* DD_STRING */
				, unsigned long *pulBlkId
				, unsigned long *pulBlkNum
				, unsigned long *pulItemId
				, unsigned long *pulMemberId
				, long lNumberOfItemIds
			)
{

	ACTION_UI_DATA structUIData;
	tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged=false;


	structUIData.uDelayTime = iTimeInSeconds;

	//Embeded variable value into prompt string
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
		pchDisplayString, pulBlkId, pulBlkNum, pulItemId, pulMemberId, lNumberOfItemIds, &bDynVarValsChanged);


	add_textMsg(structUIData, out_buf);

	structUIData.bDelayBuiltin = true;
	structUIData.bUserAcknowledge=false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	
	//display delay message at the first time with the required delay time.
	int retval = m_pDevice->sendUIMethod(structUIData);

	if (BI_SUCCESS == retval)
	{
		if( false == bDynVarValsChanged )	
		{
			bool bEventSignalled = m_pDevice->LockMethodUIEvent();	
			//get UI builtin response
			if (bEventSignalled)
			{
				retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
			}
			else
			{
				retval = BI_ERROR;
			}
		}
		else
		{
			//update prompt message dynamically
			structUIData.bDisplayDynamic = true;
				

			while (BI_SUCCESS == retval)
			{
				_tstrcpy(out_buf,mt_String);
				(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
					pchDisplayString, pulBlkId, pulBlkNum, pulItemId, pulMemberId, lNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
				{
					//Prompt string is changed
					add_textMsg(structUIData,out_buf);
					//send UI Data
					retval = m_pDevice->sendUIMethod(structUIData);
				}

				//waiting for builtin consumer to send signal of response
				if (BI_SUCCESS == retval)
				{
					bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);
					//get UI builtin response
					if (bEventSignalled)
					{
						retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
						break;
					}
					else
					{
						//It's now time to update the prompt  
					}					
				}
			}
		}

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();

	}
	
    if(NULL != structUIData.textMessage.pchTextMessage )	
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL; 
	}

    switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
		}
		break;
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
		}
		break;
		case BI_ERROR:
		{
			retval = BLTIN_VAR_NOT_FOUND;
		}
		break;
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}

	return retval;
}


long CBuiltIn::display_message2(tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums, unsigned long *pulItemIds, unsigned long *pulMemberIds, long lNumberOfItemIds)
{
	ACTION_UI_DATA structUIData;
	tchar	out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged = false;


	//Embeded variable value into prompt string
	(void) bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

	add_textMsg(structUIData,out_buf);

	structUIData.bUserAcknowledge=false;
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = NULL;
	//info request
	int retval = m_pDevice->sendUIMethod(structUIData);

	switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
		}
		break;
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
		}
		break;
		case BI_ERROR:
		{
			retval = BLTIN_VAR_NOT_FOUND;
		}
		break;
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}
	
	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
	    structUIData.textMessage.pchTextMessage = NULL;
	}
		
	return retval;
}


//This function is shared by procotol HART, PROFIBUS and FIELDBUS.
long CBuiltIn::edit_device_value2 (tchar *pchString,
									unsigned long *pULongBlkIds, unsigned long *pULongBlkNums,
									unsigned long *pULongItemIds, unsigned long *pULongMemberIds,
									long lNumberOfItemIds,	// Size of the above arrays
									unsigned long ulBlkId,  unsigned long ulBlkNum,
									unsigned long ulItemId,  unsigned long ulMemberId
								  )
{
	ACTION_UI_DATA structUIData;
	tchar out_buf[DOUBLE_MAX_DD_STRING]={0};
	bool	bDynVarValsChanged = false;


	//Embeded variable value into prompt string
	long retval = bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchString, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

	if (retval != BI_SUCCESS)
	{
		if (BI_ABORT == retval)
		{
			this->process_abort();
			return (METHOD_ABORTED);
		}

		int iRet = ConvertToBLTINCode(retval);
		return iRet;
	}

	//	Get the type of the variable as well as the min and max value.
	structUIData.userInterfaceDataType = EDIT_DEVICE_PARAM;

	if (GetPARAM_REF(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &structUIData.opRef) != BLTIN_SUCCESS)
	{
		return BLTIN_VAR_NOT_FOUND;
	}

	UI_DATA_TYPE orig_userInterfaceDataType = structUIData.userInterfaceDataType;
	add_textMsg(structUIData,out_buf);
	structUIData.userInterfaceDataType = orig_userInterfaceDataType;

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI =false;
	structUIData.uDelayTime = 0;// just defensive
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();

	retval = m_pDevice->sendUIMethod(structUIData);

	structUIData.bDisplayDynamic = true;
	
		while (BI_SUCCESS == retval)
		{
			if (bDynVarValsChanged)
			{
				(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
									pchString, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
				{
					add_textMsg(structUIData, out_buf);
					structUIData.userInterfaceDataType = orig_userInterfaceDataType;

					//send UI Data and receive user input
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}

			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);			
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}				
			}
		}	//end of while loop

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	

	if(structUIData.textMessage.pchTextMessage)	
	{
		delete[] structUIData.textMessage.pchTextMessage;	
	    structUIData.textMessage.pchTextMessage = NULL;
	}
	
	switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
		}
		break;
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
		}
		break;
		case BI_ERROR:
		{
			retval = BLTIN_VAR_NOT_FOUND;
		}
		break;
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}

	return retval;
}


int CBuiltIn::edit_local_value2				
		(
			tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums
			, unsigned long *pulItemIds
			, unsigned long *pulMemberIds
			, long	lNumberOfItemIds
			, char *pchVariableName
		)
{
	ACTION_UI_DATA structUIData;
	tchar out_buf[DOUBLE_MAX_DD_STRING]={0};
	INTER_VARIANT varVal;
	bool	bDynVarValsChanged = false;
	

	//Embeded variable value into prompt string
	long retval = bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);
	if (retval != BI_SUCCESS)
	{
		if (BI_ABORT == retval)
		{
			this->process_abort();
			return (METHOD_ABORTED);
		}

		retval = ConvertToBLTINCode(retval);
		return retval;
	}

	// remove trailing whitespace from the char* (could be in InterpretedVisitor.cpp in visitFunctionExpression(...)) SF:EPM
	string sTrim(pchVariableName);
	size_t nLastPosition = sTrim.length() - 1;			// get the last position of the array
	while( isspace(sTrim[nLastPosition]) )
	{
		sTrim.erase(nLastPosition);					// remove the whitespace
		strcpy( pchVariableName, sTrim.c_str() );	// copy the new string to pchVariableName
		nLastPosition = sTrim.length() - 1;			// get the new last position of the array
	}
	//		Remove the Language code , if it was prepended <a tokenizer bug>
	char szLang[5] = {0};
	bool bLangPresent=false;
	GetLanguageCode( pchVariableName, szLang, &bLangPresent );//remove language code if present
//	Get the value of the variable as well as the min and max value.	
	hCitemBase *pIB = NULL;
	retval = m_pInterpreter->GetVariableValue(pchVariableName, varVal, &pIB);
	
	if (pIB || (retval != SUCCESS)) 
	{
		//if pchVariableName is not a local variable name or the value is not able to get, error out
		return BLTIN_VAR_NOT_FOUND;
	}
	
	add_textMsg(structUIData, out_buf);	// stevev 26dec07 - common code
	//add_textMsg sets the structUIData.userInterfaceDataType == TEXT_MESSAGE
	
	structUIData.userInterfaceDataType = EDIT_LOCAL;
	
	//set value
	varVal.CopyTo(&structUIData.EditBox.vtValue);

	//set type, size and ranges
	switch(varVal.GetVarType())
	{
/*		case RUL_NULL:
			break;*/
		
		case RUL_FLOAT:
		{
			structUIData.EditBox.varType = vT_FloatgPt;
			structUIData.EditBox.iMaxStringLength = 4;
			
			/* validating the list of ranges */
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.FloatMinMaxVal.fMinval = -FLT_MAX;  //Code corrected in the code review 
			tmpMinMaxVal.FloatMinMaxVal.fMaxval = FLT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_DOUBLE:
		{
			structUIData.EditBox.varType = vT_Double;
			structUIData.EditBox.iMaxStringLength = 8;
			
			/* validating the list of ranges */
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.FloatMinMaxVal.fMinval = -FLT_MAX;  //Code corrected in the code review 
			tmpMinMaxVal.FloatMinMaxVal.fMaxval = FLT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_CHAR:
		{
			structUIData.EditBox.varType = vT_Integer;
			structUIData.EditBox.iMaxStringLength = 1;
			
			/* validating the list of ranges */
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = SCHAR_MIN;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = SCHAR_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_BOOL:
		case RUL_UNSIGNED_CHAR:	
		{
			structUIData.EditBox.varType = vT_Unsigned;
			structUIData.EditBox.iMaxStringLength = 1;
			
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = 0;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = UCHAR_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_SHORT:	
		{
			structUIData.EditBox.varType = vT_Integer;
			structUIData.EditBox.iMaxStringLength = 2;
			
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = SHRT_MIN;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = SHRT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_USHORT:
		{
			structUIData.EditBox.varType = vT_Unsigned;
			structUIData.EditBox.iMaxStringLength = 2;
			
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = 0;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = USHRT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_INT:
		{
			structUIData.EditBox.varType = vT_Integer;
			structUIData.EditBox.iMaxStringLength = 4;
			
			/* validating the list of ranges */
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = INT_MIN;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = INT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_UINT:
		{
			structUIData.EditBox.varType = vT_Unsigned;
			structUIData.EditBox.iMaxStringLength = 4;
			
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = 0;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = UINT_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_LONGLONG:
		{
			structUIData.EditBox.varType = vT_Integer;
			structUIData.EditBox.iMaxStringLength = 8;
			
			MinMaxVal tmpMinMaxVal;
            tmpMinMaxVal.IntMinMaxVal.iMinval = _I64_MIN;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = _I64_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;
		case RUL_ULONGLONG:
		{
			structUIData.EditBox.varType = vT_Unsigned;
			structUIData.EditBox.iMaxStringLength = 8;
			
			MinMaxVal tmpMinMaxVal;
			tmpMinMaxVal.IntMinMaxVal.iMinval = 0;
			tmpMinMaxVal.IntMinMaxVal.iMaxval = _UI64_MAX;
			structUIData.EditBox.MinMaxVal.push_back(tmpMinMaxVal);
		}
		break;


	/******* assumption: the UI will only allow the editing of wide characters, all strings will 
							go to/from wide char to be edited.                        ***************/
		case RUL_SAFEARRAY:
		{
			structUIData.EditBox.varType = vT_Ascii;

			//this is local array such as char[] or int[] variable to a method definition.
			//providing maximum size
			INTER_SAFEARRAY *sa = varVal.GetSafeArray();
			structUIData.EditBox.iMaxStringLength = sa->MemoryAllocated();
		}
		break;
		case RUL_DD_STRING:
		case RUL_WIDECHARPTR:
		case RUL_CHARPTR:		//maybe local DD_STRING
		case RUL_BYTE_STRING: 
		{
			structUIData.EditBox.varType = vT_Ascii;

			structUIData.EditBox.iMaxStringLength = (int)varVal.GetMaxSize();
		}
		break;
	}

	UI_DATA_TYPE orig_userInterfaceDataType = structUIData.userInterfaceDataType;

	structUIData.bUserAcknowledge=true; 
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI = false;
	structUIData.uDelayTime = 0;// just defensive
	structUIData.bDisplayDynamic = false;
	//structUIData.m_hUIBuiltinEvent = m_pDevice->getMethodEvent();
	retval = m_pDevice->sendUIMethod(structUIData);

	//update prompt message dynamically
	structUIData.bDisplayDynamic = true;
	//waiting for user to enter input
	if (retval == BI_SUCCESS)
	{
		while (BI_SUCCESS == retval)
		{
			if ( true == bDynVarValsChanged )	
			{
				(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

				if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
				{
					add_textMsg(structUIData, out_buf);
					structUIData.userInterfaceDataType =  orig_userInterfaceDataType;				
					//send UI Data and receive user input
					retval = m_pDevice->sendUIMethod(structUIData);
				}
			}

			//waiting for builtin consumer to send signal of response
			if (BI_SUCCESS == retval)
			{
				bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);			
				//get UI builtin response
				if (bEventSignalled)
				{
					retval = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
					break;
				}
			}
		}

		//signal is received, event is done used.
		m_pDevice->ResetMethodUIEvent();
	}

	if (retval == BI_SUCCESS)
	{
		varVal = INTER_VARIANT::InitFrom (&structUIData.vUserRsp);

		if (m_pInterpreter->SetVariableValue(pchVariableName,varVal))
		{
			retval = BI_SUCCESS;
		}
		else
		{
			retval = BLTIN_VAR_NOT_FOUND;	//this is not a local variable
		}
	}
	
	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;	
	}

	switch (retval)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			retval = METHOD_ABORTED;
			break;
		}
		case BI_SUCCESS:
		{
			retval = BLTIN_SUCCESS;
			break;
		}
		case BI_ERROR:	
		{
			retval = BLTIN_VAR_NOT_FOUND;
			break;
		}
		default:
			retval = ConvertToBLTINCode(retval);
			break;
	}
	return retval;
}


long CBuiltIn::select_from_menu2 (	tchar *pchDisplayString
								, unsigned long *pulBlkIds
								, unsigned long *pulBlkNums
								, unsigned long *pulItemIds
								, unsigned long *pulMemberIds
								, long	lNumberOfItemIds
								, tchar *pchOptionList, long* lselection )
{
	tchar out_buf[DOUBLE_MAX_DD_STRING]={0};
	ACTION_UI_DATA structUIData;
	int nRetVal = BLTIN_NOT_IMPLEMENTED;
	bool	bDynVarValsChanged = false;


	//Embeded variable value into prompt string
	nRetVal = bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
										pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

	if (nRetVal == BI_SUCCESS)
	{
		tchar	*pchMenuList = new wchar_t[_tstrlen(pchOptionList)+1];
	
		//Fill the structure required for the Menu display
		add_textMsg(structUIData, out_buf);
		structUIData.userInterfaceDataType = SELECTION;
		structUIData.ComboBox.nCurrentIndex = 0;	//default value

		//Translate into the current language
		nRetVal = (m_pDevice->dictionary)->get_string_translation(pchOptionList, pchMenuList,
														(int)_tstrlen(pchOptionList) +1, m_pDevice->device_sLanguageCode);

		//fill out ComboBox.pchComboElementText and ComboBox.iNumberOfComboElements
		add_optionList(structUIData, pchMenuList);

		//Call the method support interface.
		structUIData.bUserAcknowledge=true;
		structUIData.bDelayBuiltin = false;
		structUIData.bMethodAbortedSignalToUI =false;
		structUIData.uDelayTime = 0;// just defensive
		structUIData.bDisplayDynamic = false;
		nRetVal = m_pDevice->sendUIMethod(structUIData);

		//waiting for user to enter input
		structUIData.bDisplayDynamic = true;
		if (nRetVal == BI_SUCCESS)
		{
			while (BLTIN_SUCCESS == nRetVal)
			{
				if (bDynVarValsChanged)
				{
					(void)bltin_format_string2 (out_buf, NUM_ELEM(out_buf),
												pchDisplayString, pulBlkIds, pulBlkNums, pulItemIds, pulMemberIds, lNumberOfItemIds, &bDynVarValsChanged);

					if (wcscmp(structUIData.textMessage.pchTextMessage, out_buf) != 0)
					{
						add_textMsg(structUIData, out_buf);
						structUIData.userInterfaceDataType = SELECTION;				
						//send UI Data and receive user input
						nRetVal = m_pDevice->sendUIMethod(structUIData);
					}
				}

				//waiting for builtin consumer to send signal of response
				if (BLTIN_SUCCESS == nRetVal)
				{
					bool bEventSignalled = m_pDevice->LockMethodUIEvent(UIEVENT_TIMEOUT);					
					if (bEventSignalled)
					{
						//get UI builtin response
						nRetVal = m_pDevice->receiveUIMethodRsp(structUIData.vUserRsp);
						break;
					}
				}
			}	//end of while loop

			//signal is received, event is done used.
			m_pDevice->ResetMethodUIEvent();
		}
		else
		{
			nRetVal = BLTIN_NOT_IMPLEMENTED;
		}

		if (pchMenuList)
		{
			delete [] pchMenuList;
		}
	}

	//free memory in prompt
	if(structUIData.textMessage.pchTextMessage)
	{
		delete[] structUIData.textMessage.pchTextMessage;
		structUIData.textMessage.pchTextMessage = NULL;	
	}
	//free memory in menu list
	if(NULL != structUIData.ComboBox.pchComboElementText)
	{
		delete[] structUIData.ComboBox.pchComboElementText;
		structUIData.ComboBox.pchComboElementText = NULL;
	}

	switch (nRetVal)
	{
		case BI_ABORT:	//cancelled
		{
			this->process_abort();
			return (METHOD_ABORTED);
		}
		break;

		case BI_SUCCESS:
		{
			if ( lselection ) //set the return value, if it exists.
			{
				CValueVarient selection = structUIData.vUserRsp;
				
				*lselection = CV_UI4(&selection) + 1;		// adding 1 to make selection 1-based as per FF spec

				nRetVal = BLTIN_SUCCESS;
			}
			else
			{
				nRetVal = BLTIN_VAR_NOT_FOUND;
			}
			break;
		}
		case BI_ERROR:
		{
			nRetVal = BLTIN_VAR_NOT_FOUND;
		}
		break;

		default:
			nRetVal = ConvertToBLTINCode(nRetVal);
			break;
	}
	

	return nRetVal;

}//End of select_from_menu2

/*************** <END> Methods UI Built-ins	**************************************/
