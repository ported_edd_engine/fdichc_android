// MyDdlDefs.h
/*
 *	@(#) $Id: ddldefs.h,v 1.4 1996/03/08 18:49:10 jimkiel Exp $
 *  Copyright 1992 Rosemount, Inc., all rights reserved.
 */

#ifndef DDLDEFS_H
#define DDLDEFS_H

#define DIAGNOSTIC_ROOT_MENU			_T("diagnostic_root_menu")
#define PROCESS_VARIABLES_ROOT_MENU		_T("process_variables_root_menu")
#define DEVICE_ROOT_MENU				_T("device_root_menu")
#define DIAGNOSTIC_ROOT_WHA_MENU			_T("diagnostic_root_wha_menu")
#define PROCESS_VARIABLES_ROOT_WHA_MENU		_T("process_variables_root_wha_menu")
#define DEVICE_ROOT_WHA_MENU				_T("device_root_wha_menu")
#define OFFLINE_ROOT_MENU				_T("offline_root_menu")
#define DEVICE_ICON_IMAGE				_T("device_icon")
#define FORMAT_5_EXT					_T("fms")
#define FORMAT_6_EXT					_T("fm6")
#define FORMAT_8_EXT					_T("fm8")


typedef unsigned long	ITEM_ID;
typedef unsigned short	ITEM_TYPE;
typedef unsigned short  ITEM_SIZE;
typedef unsigned long	DDL_UINT;	/* Unparsed integer from binary */
typedef	unsigned long	OFFSET;
typedef	unsigned short	OFFSET_5;
typedef unsigned long long	DDL_UINT_LONG;	/* Unparsed integer from binary */

// For FF only
#define DDL_HUGE_INTEGER ULONG_MAX  /* defines the maximum size of an */
									/* unsigned integer               */
#define INVALID_ITEM_ID  0L

// Parse integer macro for FF only
/*  
 *  Parse integer macro.
 *  This macro can handle parsing simple (one byte) integers.
 *  If the Most Significant Bit of CHUNK is set, the integer occupies
 *  multiple bytes.  In this case "ddl_parse_integer_func()" should
 *  be called.
 */
#define DDL_PARSE_INTEGER(C,L,V)    \
    {   \
        if (**(C) & 0x80) { \
            rc = ddl_parse_integer_func((C), (L), (V)); \
            if (rc != DDL_SUCCESS)  \
                return rc;  \
        } else {    \
            if ((V) != 0) {*(V) = **(C);}   \
            ++(*(C));   \
            --(*(L));   \
        }   \
    }

#define DDL_PARSE_INTEGER_LONG(C,L,V)    \
    {   \
        if (**(C) & 0x80) { \
            rc = ddl_parse_integer_long_func((C), (L), (V)); \
            if (rc != DDL_SUCCESS)  \
                return rc;  \
        } else {    \
            if ((V) != 0) {*(V) = **(C);}   \
            ++(*(C));   \
            --(*(L));   \
        }   \
    }


/*
 * Major and minor DDOD Binary revision numbers.
 * These are used by the Tokenizer and DDS to form a Version number:
 *
 * 		<major ddod revision>.<minor ddod revision>.<software revision>
 *
 * The major and minor binary revision numbers are assigned here. 
 * The software revision numbers are assigned independently by the
 * Tokenizer and DDS in separate header files.
 */
// Defines for FF only
#define FF_TOK_TYPE				0
#define HCF_TOK_TYPE			1

#define DDOD_REV_MAJOR_FF		5	
#define DDOD_REV_MINOR_FF		0

#define DDOD_REV_MAJOR_HCF		8
#define DDOD_REV_MINOR_HCF		1

#define DDOD_REVISION_MAJOR		DDOD_REV_MAJOR_HCF
#define DDOD_REVISION_MINOR		DDOD_REV_MINOR_HCF

#define DDOD_REVISION_MAJOR_5	5
#define DDOD_REVISION_MINOR_5	0

/*
 * Value assigned to unused table offsets in the Block Directories.
 */
#define UNUSED_OFFSET   ((OFFSET)~0)
#define UNUSED_OFFSET_5   ((OFFSET_5)~0)

/*
 *  Item Types.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the item_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */
#define	RESERVED_ITYPE1			0
#define VARIABLE_ITYPE 	       	1
#define COMMAND_ITYPE           2
#define MENU_ITYPE              3
#define EDIT_DISP_ITYPE         4
#define METHOD_ITYPE            5
#define REFRESH_ITYPE           6
#define UNIT_ITYPE              7
#define WAO_ITYPE 	       		8
#define ITEM_ARRAY_ITYPE        9
#define COLLECTION_ITYPE        10
#define	RESERVED_ITYPE2			11
#define BLOCK_ITYPE             12
#define PROGRAM_ITYPE           13
#define RECORD_ITYPE            14
#define ARRAY_ITYPE             15
#define VAR_LIST_ITYPE          16
#define RESP_CODES_ITYPE        17
#define DOMAIN_ITYPE            18
#define MEMBER_ITYPE            19
#define FILE_ITYPE				20
#define CHART_ITYPE				21
#define GRAPH_ITYPE				22
#define AXIS_ITYPE				23
#define WAVEFORM_ITYPE			24
#define SOURCE_ITYPE			25
#define LIST_ITYPE				26
#define GRID_ITYPE				27
#define	IMAGE_ITYPE				28
#define	SEPARATOR_ITYPE			29 //used by AMS
#define	ROWBREAK_ITYPE			30 //used by AMS
#define	COLUMNBREAK_ITYPE		31 //used by AMS
#define	ENUM_BIT_ITYPE	32 //used by AMS
#define	STRING_LITERAL_ITYPE	33 //used by AMS
#define	CONST_INTEGER_ITYPE		34 //used by AMS
#define	CONST_FLOAT_ITYPE		35 //used by AMS
#define	CONST_ITYPE				36 //used by AMS
#define	ATTR_ITYPE				37 //used by AMS
#define MAX_ITYPE				38	/* must be last in list */

/*
 *	Special object type values
 */

#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130

 /* these additional ITYPES are used by resolve to build resolve trails */

#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202

#define AMORPHOUS_ARRAY_ITYPE   250
/*
 * Types of variables.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the var_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define DDS_TYPE_UNUSED	0	// FF
#define DDSUNUSED 		0
#define INTEGER 		2
#define UNSIGNED 		3
#define FLOAT 			4
#define FLOATG_PT		4	// FF
#define DOUBLE 			5
#define DOUBLEG_PT 		5
#define ENUMERATED 		6
#define BIT_ENUMERATED 	7
#define INDEX 			8
#define ASCII 			9
#define PACKED_ASCII 	10		/* HART */
#define PASSWORD 		11
#define BITSTRING 		12
#define HART_DATE_FORMAT	13		/* HART */
#define TIME 			14
#define DATE_AND_TIME 	15
#define DURATION 		16
#define EUC				17
#define OCTETSTRING		18
#define VISIBLESTRING	19
#define TIME_VALUE		20
#define iMAX_TYPE		21	/* must be last in list (Hart types only)*/
#define BOOLEAN_T		21
#define FF_MAX_TYPE		22	/* must be last in list (FF types only)*/
#define HART_TIME_VALUE	255 /* HART and FF both have TIME_VALUE, but the meaning is different, so we define a new one for UI */
/*
 * Types of members.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the mem_type in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define	COLLECTION_MEM		1
#define	PARAM_MEM			2
#define	PARAM_LIST_MEM		3
#define	VAR_LIST_MEM		4
#define	RECORD_MEM			5
#define	CHARACTERISTIC_MEM  6	/* must be last item in list */

#define MEMBER_STRINGS  {\
	"",					/*required since items start at 1*/\
	"collection",\
	"parameter",\
	"parameter-list",\
	"variable-list",\
	"record",\
	"file",\
	"chart",\
	"source",\
	"graph",\
	"characteristic",\
	NULL}		/*array must be NULL terminated for searches*/
#define NO_ATTRTAG		0xffff

#define MEMBER2ATTRTAG { NO_ATTRTAG, COLLECTION_MEMBERS_ID, NO_ATTRTAG, NO_ATTRTAG,\
		VAR_LIST_MEMBERS_ID, RECORD_MEMBERS_ID, FILE_MEMBERS_ID, CHART_MEMBERS_ID, \
		SOURCE_MEMBERS_ID, GRAPH_MEMBERS_ID, BLOCK_CHARACTERISTIC_ID, NO_ATTRTAG}
/*
 * Classes of variables and methods.
 */

#define DIAGNOSTIC_CLASS			0x000001
#define DYNAMIC_CLASS				0x000002
#define SERVICE_CLASS				0x000004
#define CORRECTION_CLASS			0x000008
#define COMPUTATION_CLASS			0x000010
#define INPUT_BLOCK_CLASS			0x000020
#define ANALOG_OUTPUT_CLASS			0x000040
#define HART_CLASS					0x000080
#define LOCAL_DISPLAY_CLASS			0x000100
#define FREQUENCY_CLASS				0x000200
#define DISCRETE_CLASS				0x000400
#define DEVICE_CLASS				0x000800
#define LOCAL_CLASS					0x001000
// Not used							0x002000
#define BACKGROUND_CLASS			0x004000
#define ALARM_CLASS					0x008000
#define MODE_CLASS					0x010000
#define TUNE_CLASS					0x020000
#define IS_CONFIG_CLASS				0x040000
// Not used							0x080000
#define FACTORY_CLASS               0x100000


#if 0
#define DIAGNOSTIC_CLASS			0x000001
#define DYNAMIC_CLASS				0x000002
#define SERVICE_CLASS				0x000004
#define CORRECTION_CLASS			0x000008
#define COMPUTATION_CLASS			0x000010
#define INPUT_BLOCK_CLASS			0x000020
#define ANALOG_OUTPUT_CLASS			0x000040
#define HART_CLASS					0x000080
#define LOCAL_DISPLAY_CLASS			0x000100
#define FREQUENCY_CLASS				0x000200
#define DISCRETE_CLASS				0x000400
#define DEVICE_CLASS				0x000800
#define LOCAL_CLASS					0x001000
#define INPUT_CLASS					0x002000
#define OUTPUT_CLASS				0x004000
#define CONTAINED_CLASS				0x008000
#define OPERATE_CLASS				0x010000
#define ALARM_CLASS					0x020000
#define TUNE_CLASS					0x040000
#define FACTORY_CLASS               0x100000
#define LOOP_IN_MANUAL_CLASS        0x200000
/*
 * The following is not a valid class, but is used as a default for
 * a HART variable or method in which the class was omitted.
 * This is done to prevent an error condition from being set in
 * the tokenizer or DDS.
 * This will probably be removed in the long term.
 */
#define UNASSIGNED_CLASS			0x000000

#define FACTORY_CLASS				0x100000
#endif
/*
 * Handling of variables.
 */

#define READ_HANDLING 				0x01
#define WRITE_HANDLING 				0x02

/*
 * Ordinary status classes of bit enumerated variables.
 */

#define HARDWARE_STATUS				0x000001
#define SOFTWARE_STATUS				0x000002
#define PROCESS_STATUS				0x000004
#define MODE_STATUS					0x000008
#define DATA_STATUS					0x000010
#define MISC_STATUS					0x000020
#define EVENT_STATUS				0x000040
#define STATE_STATUS				0x000080
#define SELF_CORRECTING_STATUS		0x000100
#define CORRECTABLE_STATUS			0x000200
#define UNCORRECTABLE_STATUS		0x000400
#define SUMMARY_STATUS				0x000800
#define DETAIL_STATUS				0x001000
#define MORE_STATUS					0x002000
#define COMM_ERROR_STATUS			0x004000
#define IGNORE_IN_TEMPORARY_STATUS	0x008000
#define BAD_OUTPUT_STATUS			0x010000
#define IGNORE_IN_HOST_STATUS		0x020000
#define ERROR_STATUS				0x040000
#define WARNING_STATUS				0x080000
#define INFO_STATUS					0x100000

/*
 * ALTERNATIVE output status classes.
 */

#define OC_NORMAL					0	/* must be 0, see ddl_parse_one_enum() */

/*
 * HART output status classes.
 */

#define OC_DV 						1
#define OC_TV 						2
#define OC_AO 						3
#define OC_ALL 						4

#define OC_MANUAL 					0x01
#define	OC_BAD						0x02
#define OC_MARGINAL					0x04

/*
 * HART command operations.
 */

#define READ_OPERATION					1
#define WRITE_OPERATION					2
#define COMMAND_OPERATION				3

/*
 * Data item types.
 */

#define DATA_CONSTANT 					0
#define DATA_REFERENCE 					1
#define DATA_REF_FLAGS					2		/* HART */
#define DATA_REF_WIDTH					3		/* HART */
#define DATA_REF_FLAGS_WIDTH			4		/* HART */
#define DATA_FLOATING 					5
#define DATA_STRING						6

/*
 * Response code types.
 */

#define SUCCESS_RSPCODE 				1
#define MISC_WARNING_RSPCODE 			2
#define DATA_ENTRY_WARNING_RSPCODE 		3
#define DATA_ENTRY_ERROR_RSPCODE 		4
#define MODE_ERROR_RSPCODE 				5
#define PROCESS_ERROR_RSPCODE 			6
#define MISC_ERROR_RSPCODE 				7
#define INVALID_RSPCODE 				8


/*
 * HART data item flags.
 */

#define INFO_DATA_ITEM 					0x0001
#define INDEX_DATA_ITEM 				0x0002

/*
 * data item flags.
 */

#define WIDTH_PRESENT 					0x8000

/*
 * Menu item flags.
 */

#define READ_ONLY_ITEM 					0x01
#define DISPLAY_VALUE_ITEM 				0x02
#define REVIEW_ITEM 					0x04
#define NO_LABEL_ITEM					0x08
#define NO_UNIT_ITEM					0x10
#define INLINE_ITEM						0x20		/* only valid for IMAGEs */

#define FF_READ_ONLY_ITEM 				0x0001
#define FF_DISPLAY_VALUE_ITEM 			0x0002
#define FF_REVIEW_ITEM 					0x0004
#define FF_HIDDEN_ITEM					0x0008
#define FF_NO_LABEL_ITEM				0x0010
#define FF_NO_UNIT_ITEM					0x0020
#define FF_SEPARATOR_ITEM				0x0040
#define FF_STRING_ITEM					0x0080
#define FF_IMAGE_ITEM					0x0100
#define FF_INLINE_ITEM					0x0200
#define FF_ROWBREAK_ITEM				0x0400
#define FF_COLUMNBREAK_ITEM				0x0800
#define FF_BIT_ENUM_WITH_MASK_ITEM		0x1000

/*
 * Menu style types.
 */

#define NO_STYLE_TYPE				0
#define WINDOW_STYLE_TYPE			1
#define DIALOG_STYLE_TYPE			2
#define PAGE_STYLE_TYPE				3
#define GROUP_STYLE_TYPE			4
#define MENU_STYLE_TYPE				5
#define TABLE_STYLE_TYPE			6


/*
 * Method type flags
 */
#define SCALING_TYPE_METHOD			0x00000001
#define USER_TYPE_METHOD			0x00000010
/*
 * Method type (return value) flags
 */

#define TYPE_VOID   	0
#define TYPE_INT__8		1	/* signed char  */
#define TYPE_INT_16		2	/* short        */
#define TYPE_INT_32		3	/* int & long   */
#define TYPE_FLOAT		4	/* FLOATG_PT    */
#define TYPE_DOUBLE		5	/* DOUBLE_FLOAT */
#define TYPE_UINT__8 	6
#define TYPE_UINT_16 	7
#define TYPE_UINT_32	8
#define TYPE_INT_64 	9	/* currently unsupported */
#define TYPE_UINT_64	10	/* currently unsupported */
#define TYPE_DDSTRING	11
#define TYPE_DD_ITEM    12


/*
 * Method parameter details
 */

#define ARRAYFLAG   	1	/* isArray - is declared '[]' or '[' chain-expr ']' */
#define REFECFLAG		2	/* isReference - ( has a '&' in front of it */
#define CONSTFLAG		4	/* isConst - for posterity */


/*
 * Chart types.
 */

#define GAUGE_CTYPE			1
#define HORIZ_BAR_CTYPE		2
#define SCOPE_CTYPE			3
#define STRIP_CTYPE			4
#define SWEEP_CTYPE			5
#define VERT_BAR_CTYPE		6

#define FF_CHART_TYPE_GAUGE            0
#define FF_CHART_TYPE_HORIZONTAL_BAR   1
#define FF_CHART_TYPE_SCOPE            2
#define FF_CHART_TYPE_STRIP            3
#define FF_CHART_TYPE_SWEEP            4
#define FF_CHART_TYPE_VERTICAL_BAR     5

/*
 * Chart sizes.
 */

#define XX_SMALL_DISPSIZE	1
#define X_SMALL_DISPSIZE	2
#define SMALL_DISPSIZE		3
#define MEDIUM_DISPSIZE		4
#define LARGE_DISPSIZE		5
#define X_LARGE_DISPSIZE	6
#define XX_LARGE_DISPSIZE	7

#define FF_DISPLAY_SIZE_XX_SMALL         0
#define FF_DISPLAY_SIZE_X_SMALL          1
#define FF_DISPLAY_SIZE_SMALL            2
#define FF_DISPLAY_SIZE_MEDIUM           3
#define FF_DISPLAY_SIZE_LARGE            4
#define FF_DISPLAY_SIZE_X_LARGE          5
#define FF_DISPLAY_SIZE_XX_LARGE         6

/*
 * Waveform types.
 */

#define YT_WAVEFORM_TYPE	1
#define XY_WAVEFORM_TYPE	2
#define HORZ_WAVEFORM_TYPE	3
#define VERT_WAVEFORM_TYPE	4

#define FF_WAVEFORM_TYPE_XY            0
#define FF_WAVEFORM_TYPE_YT            1
#define FF_WAVEFORM_TYPE_HORIZONTAL    2
#define FF_WAVEFORM_TYPE_VERTICAL      3

/*
 * LINE types.
 */

#define DATA_LINETYPE		1
#define LOWLOW_LINETYPE		2
#define LOW_LINETYPE		3
#define HIGH_LINETYPE		4
#define HIGHHIGH_LINETYPE	5
#define TRANSPARENT_LINETYPE 6

#define FF_LINE_TYPE_LOW_LOW_LIMIT     0
#define FF_LINE_TYPE_LOW_LIMIT         1
#define FF_LINE_TYPE_HIGH_LIMIT        2
#define FF_LINE_TYPE_HIGH_HIGH_LIMIT   3
#define FF_LINE_TYPE_DATA_N            16  /* all higher values are reserved */

#define DDS_LINE_TYPE_DATA_N           FF_LINE_TYPE_DATA_N  /* all higher values are reserved */

/*
 * Grid Orientation.
 */

#define ORIENT_VERT			1
#define ORIENT_HORIZ		2

#define FF_ORIENT_VERT      0
#define FF_ORIENT_HORIZ     1

/*
 * SCALE types.
 */

#define LINEAR_SCALE		1
#define LOG_SCALE			2

#define FF_LINEAR_SCALE		0
#define FF_LOG_SCALE		1

/*
 * HALFPOINT types.
 */

#define X_VALUE				1
#define Y_VALUE				2


/*
 * Attribute Identifier Tags for DDOD Item Objects.
 * These tags are also used as bit positions for the
 * Item Attribute Masks defined below.
 */


/*Adding this definition for the Default Attribute ID,to initialize the
  by AttrId of Base Atributes*/

#define DEFAULT_ATTR_ID				255


/* BLOCK attributes */

#define BLOCK_CHARACTERISTIC_ID     0
#define BLOCK_LABEL_ID              1
#define BLOCK_HELP_ID               2
#define BLOCK_PARAM_ID              3
#define BLOCK_MENU_ID               4
#define BLOCK_EDIT_DISP_ID          5
#define BLOCK_METHOD_ID             6
#define BLOCK_REFRESH_ID            7
#define BLOCK_UNIT_ID               8
#define BLOCK_WAO_ID                9
#define BLOCK_COLLECT_ID            10
#define BLOCK_ITEM_ARRAY_ID         11
#define BLOCK_PARAM_LIST_ID         12
#define BLOCK_DEBUG_ID				13
#define MAX_BLOCK_ID                14	/* must be last in list */

/* VARIABLE attributes */

#define VAR_CLASS_ID                0
#define VAR_HANDLING_ID             1
#define VAR_UNIT_ID                 2
#define VAR_LABEL_ID                3
#define VAR_HELP_ID                 4
#define VAR_READ_TIME_OUT_ID        5
#define VAR_WRITE_TIME_OUT_ID       6
#define VAR_VALID_ID                7
#define VAR_PRE_READ_ACT_ID         8
#define VAR_POST_READ_ACT_ID        9
#define VAR_PRE_WRITE_ACT_ID        10
#define VAR_POST_WRITE_ACT_ID       11
#define VAR_PRE_EDIT_ACT_ID         12
#define VAR_POST_EDIT_ACT_ID        13
#define VAR_RESP_CODES_ID           14
#define VAR_TYPE_SIZE_ID            15
#define VAR_DISPLAY_ID              16
#define VAR_EDIT_ID                 17
#define VAR_MIN_VAL_ID              18
#define VAR_MAX_VAL_ID              19
#define VAR_SCALE_ID                20
#define VAR_ENUMS_ID                21
#define VAR_INDEX_ITEM_ARRAY_ID     22
#define VAR_DEFAULT_VALUE_ID		23
#define VAR_REFRESH_ACT_ID			24
#define VAR_DEBUG_ID				25

#define  VAR_POST_RQST_ACT_ID		26
#define  VAR_POST_USER_ACT_ID		27

#define	 VAR_TIME_FORMAT_ID			28
#define	 VAR_TIME_SCALE_ID			29
#define  MAX_VAR_ID                 30	/* must be last in list of 0 - 31*/

/*****************************************/
/* COMMON USAGE!!!  stevev				 */
/*****************************************/
// these are used in the tree but not implemented in 
// the binary file due to backwards compatability issues
#define COMMON_LABEL_ID		0
#define COMMON_HELP_ID		1
#define COMMON_VALIDITY_ID	2
#define COMMON_MEMBERS_ID	3
#define COMMON_LAST_ID		3	/* equals last common defined (COMMON_LAST+) */


/* MENU attributes SIZE 1 */
#define MENU_LABEL_ID               0
#define MENU_ITEMS_ID               1
#define MENU_HELP_ID				2
#define MENU_VALID_ID				3
#define MENU_STYLE_ID				4
#define MENU_DEBUG_ID				5
#define MAX_MENU_ID                 6	/* must be last in list */

/* EDIT_DISPLAY attributes */

#define EDIT_DISPLAY_LABEL_ID           0
#define EDIT_DISPLAY_EDIT_ITEMS_ID      1
#define EDIT_DISPLAY_DISP_ITEMS_ID      2
#define EDIT_DISPLAY_PRE_EDIT_ACT_ID    3
#define EDIT_DISPLAY_POST_EDIT_ACT_ID   4
#define EDIT_DISPLAY_HELP_ID			   5   /* EDDL*/
#define EDIT_DISPLAY_VALID_ID			   6   /* EDDL*/
#define EDIT_DISPLAY_DEBUG_ID			   7   /* EDDL*/
#define MAX_EDIT_DISPLAY_ID             8	/* must be last in list */

/* METHOD attributes */

#define METHOD_CLASS_ID             0
#define METHOD_LABEL_ID             1
#define METHOD_HELP_ID              2
#define METHOD_DEF_ID               3
#define METHOD_VALID_ID             4
#define METHOD_SCOPE_ID             5
#define METHOD_TYPE_ID              6   /* stevev new 13apr05 */
#define METHOD_PARAMS_ID            7   /* stevev new 13apr05 */
#define METHOD_DEBUG_ID             8   /* stevev new 13apr05 */
#define MAX_METHOD_ID               9	/* must be last in list */

/* REFRESH attributes */

#define REFRESH_ITEMS_ID            0
#define REFRESH_DEBUG_ID            1
#define MAX_REFRESH_ID              2	/* must be last in list */

/* UNIT attributes */

#define UNIT_ITEMS_ID               0
#define UNIT_DEBUG_ID               1
#define MAX_UNIT_ID                 2	/* must be last in list */

/* WRITE AS ONE attributes */

#define WAO_ITEMS_ID                0
#define WAO_DEBUG_ID                1
#define MAX_WAO_ID                  2	/* must be last in list */

/* ITEM_ARRAY attributes */

#define ITEM_ARRAY_ELEMENTS_ID      0
#define ITEM_ARRAY_LABEL_ID         1
#define ITEM_ARRAY_HELP_ID          2
#define ITEM_ARRAY_VALIDITY_ID		3
#define ITEM_ARRAY_DEBUG_ID         4
#define MAX_ITEM_ARRAY_ID           5	/* must be last in list */

/* COLLECTION attributes */

#define COLLECTION_MEMBERS_ID       0
#define COLLECTION_LABEL_ID         1
#define COLLECTION_HELP_ID          2
#define COLLECTION_VALIDITY_ID      3
#define COLLECTION_DEBUG_ID         4
#define MAX_COLLECTION_ID           5	/* must be last in list */

/* PROGRAM attributes */

#define PROGRAM_ARGS_ID             0
#define PROGRAM_RESP_CODES_ID       1
#define MAX_PROGRAM_ID              2	/* must be last in list */

/* RECORD attributes */

#define RECORD_MEMBERS_ID           0
#define RECORD_LABEL_ID             1
#define RECORD_HELP_ID              2
#define RECORD_RESP_CODES_ID        3
#define RECORD_DEBUG_ID				4
#define MAX_RECORD_ID               5	/* must be last in list */

/* ARRAY attributes */

//todo remove the ARRAY_RESP_CODES from DDS
#define ARRAY_RESP_CODES_ID         0
#define ARRAY_LABEL_ID              1
#define ARRAY_HELP_ID               2
#define ARRAY_VALID_ID				3
#define ARRAY_TYPE_ID				4
#define ARRAY_NUM_OF_ELEMENTS_ID    5
#define ARRAY_DEBUG_ID				6
#define MAX_ARRAY_ID                7	/* must be last in list */

/* VARIABLE LIST attributes */

#define VAR_LIST_MEMBERS_ID         0
#define VAR_LIST_LABEL_ID           1
#define VAR_LIST_HELP_ID            2
#define VAR_LIST_RESP_CODES_ID      3
#define VAR_LIST_DEBUG_ID			4
#define MAX_VAR_LIST_ID             5	/* must be last in list */

/* RESPONSE CODE attributes */

#define RESP_CODE_MEMBER_ID         0
#define MAX_RESP_CODE_ID            1	/* must be last in list */

/* DOMAIN attributes */

#define DOMAIN_HANDLING_ID          0
#define DOMAIN_RESP_CODES_ID        1
#define MAX_DOMAIN_ID               2	/* must be last in list */

/* COMMAND attributes */

#define COMMAND_NUMBER_ID           0
#define COMMAND_OPER_ID             1
#define COMMAND_TRANS_ID            2
#define COMMAND_RESP_CODES_ID       3
#define COMMAND_DEBUG_ID            4
#define MAX_COMMAND_ID              5	/* must be last in list */

/* AXIS attributes - SIZE 1 */

#define AXIS_LABEL_ID				0
#define AXIS_HELP_ID				1
#define AXIS_VALID_ID				2
#define AXIS_MINVAL_ID				3
#define AXIS_MAXVAL_ID				4
#define AXIS_SCALING_ID				5
#define AXIS_CONSTUNIT_ID			6
#define AXIS_DEBUG_ID				7
#define AXIS_VIEW_MIN_VAL_ID		8	/* NON-DD internal attribute */
#define AXIS_VIEW_MAX_VAL_ID		9	/* NON-DD internal attribute */
#define AXIS_MAX_VAL_REF_ID			10	/* NON-DD internal attribute */
#define AXIS_MIN_VAL_REF_ID			11	/* NON-DD internal attribute */
#define MAX_AXIS_ID					12	/* must be last in list */

/* CHART attributes  SIZE 2 */

#define CHART_LABEL_ID				0
#define CHART_HELP_ID				1
#define CHART_VALID_ID				2
#define CHART_HEIGHT_ID				3
#define CHART_WIDTH_ID				4
#define CHART_TYPE_ID				5
#define CHART_LENGTH_ID				6
#define CHART_CYCLETIME_ID			7
#define CHART_MEMBERS_ID			8
#define CHART_DEBUG_ID				9
#define MAX_CHART_ID				10	/* must be last in list */

/* FILE attributes  SIZE 1 */

#define FILE_MEMBERS_ID				0
#define FILE_LABEL_ID				1
#define FILE_HELP_ID				2
#define FILE_NO_VALIDITY			3
#define FILE_DEBUG_ID				4
#define MAX_FILE_ID					5

/* GRAPH attributes SIZE 2 */

#define GRAPH_LABEL_ID				0
#define GRAPH_HELP_ID				1
#define GRAPH_VALID_ID				2
#define GRAPH_HEIGHT_ID				3
#define GRAPH_WIDTH_ID				4
#define GRAPH_XAXIS_ID				5
#define GRAPH_MEMBERS_ID			6
#define GRAPH_DEBUG_ID				7
#define GRAPH_CYCLETIME_ID			8
#define MAX_GRAPH_ID				9	/* must be last in list */

/* LIST attributes  SIZE 1 */

#define LIST_LABEL_ID				0
#define LIST_HELP_ID				1
#define LIST_VALID_ID				2
#define LIST_TYPE_ID				3
#define LIST_COUNT_ID				4
#define LIST_CAPACITY_ID			5
#define LIST_DEBUG_ID				6
#define LIST_FIRST_ID				7	/* NON-DD internal attribute */
#define LIST_LAST_ID				8	/* NON-DD internal attribute */
#define LIST_COUNT_REF_ID			9	/* NON-DD internal attribute */
#define MAX_LIST_ID					10	/* must be last in list */


/* SOURCE constants */
#define LINE_COLOR_DEFAULT  0xffffffff


/* SOURCE attributes SIZE 2 */

#define SOURCE_LABEL_ID				0
#define SOURCE_HELP_ID				1
#define SOURCE_VALID_ID				2
#define SOURCE_EMPHASIS_ID			3
#define SOURCE_LINETYPE_ID			4
#define SOURCE_LINECOLOR_ID			5
#define SOURCE_YAXIS_ID				6
#define SOURCE_MEMBERS_ID			7
#define SOURCE_DEBUG_ID				8
#define SOURCE_INIT_ACTIONS_ID		9	/* added 22jan07, sjv - spec change*/
#define SOURCE_RFRSH_ACTIONS_ID		10	/* added 22jan07, sjv - spec change*/
#define SOURCE_EXIT_ACTIONS_ID		11	/* added 22jan07, sjv - spec change*/
#define MAX_SOURCE_ID				12	/* must be last in list */

/* WAVEFORM attributes  SIZE 3 */

#define WAVEFORM_LABEL_ID			 0
#define WAVEFORM_HELP_ID			 1
#define WAVEFORM_HANDLING_ID		 2
#define WAVEFORM_EMPHASIS_ID		 3
#define WAVEFORM_LINETYPE_ID		 4
#define WAVEFORM_LINECOLOR_ID		 5
#define WAVEFORM_YAXIS_ID			 6
#define WAVEFORM_KEYPTS_X_ID		 7
#define WAVEFORM_KEYPTS_Y_ID		 8	
#define WAVEFORM_TYPE_ID			 9
#define WAVEFORM_X_VALUES_ID		10
#define WAVEFORM_Y_VALUES_ID		11
#define WAVEFORM_X_INITIAL_ID		12
#define WAVEFORM_X_INCREMENT_ID		13
#define WAVEFORM_POINT_COUNT_ID		14
#define WAVEFORM_INIT_ACTIONS_ID	15
#define WAVEFORM_RFRSH_ACTIONS_ID	16
#define WAVEFORM_EXIT_ACTIONS_ID	17
#define WAVEFORM_DEBUG_ID			18
#define WAVEFORM_VALID_ID			19
#define MAX_WAVEFORM_ID				20	/* must be last in list */

/* IMAGE attributes SIZE 1 */

#define IMAGE_LABEL_ID				0
#define IMAGE_HELP_ID				1
#define IMAGE_VALID_ID				2
#define IMAGE_LINK_ID				3
#define IMAGE_PATH_ID				4
#define IMAGE_DEBUG_ID				5
#define IMAGE_BINARY_ID				6
#define MAX_IMAGE_ID				7	/* must be last in list */


/* GRID attributes SIZE 2 */

#define GRID_LABEL_ID				0
#define GRID_HELP_ID				1
#define GRID_VALID_ID				2
#define GRID_HEIGHT_ID				3
#define GRID_WIDTH_ID				4
#define GRID_ORIENT_ID				5
#define GRID_HANDLING_ID			6
#define GRID_MEMBERS_ID				7
#define GRID_DEBUG_ID				8
#define MAX_GRID_ID					9	/* must be last in list */
/*
 * Item Attribute Masks for DDOD Item Objects.
 */

/* BLOCK attribute masks */

#define BLOCK_CHARACTERISTIC        (1<<BLOCK_CHARACTERISTIC_ID)
#define BLOCK_LABEL                 (1<<BLOCK_LABEL_ID)
#define BLOCK_HELP                  (1<<BLOCK_HELP_ID)
#define BLOCK_PARAM                 (1<<BLOCK_PARAM_ID)
#define BLOCK_DEBUG					(1<<BLOCK_DEBUG_ID)

#define BLOCK_MENU                  (1<<BLOCK_MENU_ID)
#define BLOCK_EDIT_DISP             (1<<BLOCK_EDIT_DISP_ID)
#define BLOCK_METHOD                (1<<BLOCK_METHOD_ID)
#define BLOCK_UNIT                  (1<<BLOCK_UNIT_ID)

#define BLOCK_REFRESH               (1<<BLOCK_REFRESH_ID)
#define BLOCK_WAO                   (1<<BLOCK_WAO_ID)
#define BLOCK_COLLECT               (1<<BLOCK_COLLECT_ID)
#define BLOCK_ITEM_ARRAY            (1<<BLOCK_ITEM_ARRAY_ID)

#define BLOCK_PARAM_LIST            (1<<BLOCK_PARAM_LIST_ID)


#define BLOCK_ATTR_MASKS		   (BLOCK_CHARACTERISTIC | \
									BLOCK_LABEL | \
									BLOCK_HELP | \
									BLOCK_PARAM | \
									BLOCK_MENU | \
									BLOCK_EDIT_DISP | \
									BLOCK_METHOD | \
									BLOCK_UNIT | \
									BLOCK_REFRESH | \
									BLOCK_WAO | \
									BLOCK_COLLECT | \
									BLOCK_ITEM_ARRAY | \
									BLOCK_PARAM_LIST | \
									BLOCK_DEBUG)

/* VARIABLE attribute masks */

#define VAR_CLASS                   (1<<VAR_CLASS_ID)
#define VAR_HANDLING                (1<<VAR_HANDLING_ID)
#define VAR_UNIT                    (1<<VAR_UNIT_ID)
#define VAR_LABEL                   (1<<VAR_LABEL_ID)

#define VAR_HELP                    (1<<VAR_HELP_ID)
#define VAR_READ_TIME_OUT           (1<<VAR_READ_TIME_OUT_ID)
#define VAR_WRITE_TIME_OUT          (1<<VAR_WRITE_TIME_OUT_ID)
#define VAR_VALID                   (1<<VAR_VALID_ID)

#define VAR_PRE_READ_ACT            (1<<VAR_PRE_READ_ACT_ID)
#define VAR_POST_READ_ACT           (1<<VAR_POST_READ_ACT_ID)
#define VAR_PRE_WRITE_ACT           (1<<VAR_PRE_WRITE_ACT_ID)
#define VAR_POST_WRITE_ACT          (1<<VAR_POST_WRITE_ACT_ID)

#define VAR_PRE_EDIT_ACT            (1<<VAR_PRE_EDIT_ACT_ID)
#define VAR_POST_EDIT_ACT           (1<<VAR_POST_EDIT_ACT_ID)
#define VAR_RESP_CODES              (1<<VAR_RESP_CODES_ID)
#define VAR_TYPE_SIZE               (1<<VAR_TYPE_SIZE_ID)

#define VAR_DISPLAY                 (1<<VAR_DISPLAY_ID)
#define VAR_EDIT                    (1<<VAR_EDIT_ID)
#define VAR_MIN_VAL                 (1<<VAR_MIN_VAL_ID)
#define VAR_MAX_VAL                 (1<<VAR_MAX_VAL_ID)

#define VAR_SCALE                   (1<<VAR_SCALE_ID)
#define VAR_ENUMS                   (1<<VAR_ENUMS_ID)
#define VAR_INDEX_ITEM_ARRAY        (1<<VAR_INDEX_ITEM_ARRAY_ID)
#define VAR_DEFAULT_VALUE			(1<<VAR_DEFAULT_VALUE_ID)
#define VAR_REFRESH_ACT             (1<<VAR_REFRESH_ACT_ID)
#define VAR_DEBUG					(1<<VAR_DEBUG_ID)
#define VAR_TIME_FORMAT				(1<<VAR_TIME_FORMAT_ID)
#define VAR_TIME_SCALE				(1<<VAR_TIME_SCALE_ID)

// Encoded values for the TIME_SCALE of a variable
#define VAR_TIME_SCALE_NONE			0
#define VAR_TIME_SCALE_SECONDS		1
#define VAR_TIME_SCALE_MINUTES		2
#define VAR_TIME_SCALE_HOURS		4

#define VAR_MAIN_MASKS			   (VAR_CLASS | \
									VAR_HANDLING | \
									VAR_HELP | \
									VAR_LABEL | \
									VAR_TYPE_SIZE | \
									VAR_DISPLAY | \
									VAR_EDIT | \
									VAR_ENUMS | \
									VAR_INDEX_ITEM_ARRAY | \
									VAR_DEFAULT_VALUE | \
									VAR_RESP_CODES )

#define VAR_MISC_MASKS			   (VAR_UNIT | \
									VAR_READ_TIME_OUT | \
									VAR_WRITE_TIME_OUT | \
									VAR_MIN_VAL | \
									VAR_MAX_VAL | \
									VAR_SCALE | \
									VAR_VALID | \
									VAR_TIME_FORMAT | \
									VAR_TIME_SCALE | \
									VAR_DEBUG )

#define	VAR_ACT_MASKS			   (VAR_PRE_EDIT_ACT | \
									VAR_POST_EDIT_ACT | \
									VAR_PRE_READ_ACT | \
									VAR_POST_READ_ACT | \
									VAR_PRE_WRITE_ACT | \
									VAR_POST_WRITE_ACT | \
                                    VAR_REFRESH_ACT)

#define	VAR_ATTR_MASKS			   (VAR_MAIN_MASKS | \
									VAR_MISC_MASKS | \
									VAR_ACT_MASKS)


/*
 *	Macros used to limit the availability of some of the type subattributes
 *	based on type in eval_item_var().
 */

#define INVALID_VAR_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY | \
		VAR_TIME_FORMAT | VAR_TIME_SCALE)

#define INVALID_ARITH_TYPE_SUBATTR_MASK ~(VAR_ENUMS | VAR_INDEX_ITEM_ARRAY)

#define INVALID_ENUM_TYPE_SUBATTR_MASK ~(VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_INDEX_ITEM_ARRAY | \
		VAR_TIME_FORMAT | VAR_TIME_SCALE)

#define INVALID_STRING_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY | \
		VAR_TIME_FORMAT | VAR_TIME_SCALE)

#define INVALID_INDEX_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | \
		VAR_TIME_FORMAT | VAR_TIME_SCALE)

#define INVALID_DATE_TIME_TYPE_SUBATTR_MASK ~( VAR_DISPLAY | VAR_EDIT | \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY | \
		VAR_TIME_FORMAT | VAR_TIME_SCALE)

#define INVALID_TIME_VALUE_TYPE_SUBATTR_MASK ~(  \
		VAR_MIN_VAL | VAR_MAX_VAL | VAR_SCALE | VAR_ENUMS | VAR_INDEX_ITEM_ARRAY )

/* MENU attribute masks */

#define MENU_LABEL                  (1<<MENU_LABEL_ID)
#define MENU_ITEMS                  (1<<MENU_ITEMS_ID)
#define MENU_HELP					(1<<MENU_HELP_ID)
#define MENU_VALID					(1<<MENU_VALID_ID)
#define MENU_STYLE				(1<<MENU_STYLE_ID)
#define MENU_DEBUG				(1<<MENU_DEBUG_ID)

#define	MENU_ATTR_MASKS			   (MENU_LABEL  | \
									MENU_ITEMS  | \
									MENU_HELP   | \
									MENU_VALID  | \
									MENU_STYLE  | \
									MENU_DEBUG  )

/* EDIT_DISPLAY attribute masks */

#define EDIT_DISPLAY_LABEL          (1<<EDIT_DISPLAY_LABEL_ID)
#define EDIT_DISPLAY_EDIT_ITEMS     (1<<EDIT_DISPLAY_EDIT_ITEMS_ID)
#define EDIT_DISPLAY_DISP_ITEMS     (1<<EDIT_DISPLAY_DISP_ITEMS_ID)
#define EDIT_DISPLAY_PRE_EDIT_ACT   (1<<EDIT_DISPLAY_PRE_EDIT_ACT_ID)
#define EDIT_DISPLAY_POST_EDIT_ACT  (1<<EDIT_DISPLAY_POST_EDIT_ACT_ID)
#define EDIT_DISPLAY_HELP			(1<<EDIT_DISPLAY_HELP_ID)
#define EDIT_DISPLAY_VALID			(1<<EDIT_DISPLAY_VALID_ID)
#define EDIT_DISPLAY_DEBUG			(1<<EDIT_DISPLAY_DEBUG_ID)

#define EDIT_DISP_ATTR_MASKS	   (EDIT_DISPLAY_LABEL | \
									EDIT_DISPLAY_EDIT_ITEMS | \
									EDIT_DISPLAY_DISP_ITEMS | \
									EDIT_DISPLAY_PRE_EDIT_ACT | \
									EDIT_DISPLAY_POST_EDIT_ACT | \
									EDIT_DISPLAY_HELP | \
									EDIT_DISPLAY_VALID| \
									EDIT_DISPLAY_DEBUG)

/* METHOD attribute masks */

#define METHOD_CLASS                (1<<METHOD_CLASS_ID)
#define METHOD_LABEL                (1<<METHOD_LABEL_ID)
#define METHOD_HELP                 (1<<METHOD_HELP_ID)
#define METHOD_DEF                  (1<<METHOD_DEF_ID)
#define METHOD_VALID                (1<<METHOD_VALID_ID)
#define METHOD_SCOPE                (1<<METHOD_SCOPE_ID)
#define METHOD_TYPE					(1<<METHOD_TYPE_ID)
#define METHOD_PARAMS				(1<<METHOD_PARAMS_ID)
#define METHOD_DEBUG				(1<<METHOD_DEBUG_ID) 

#define	METHOD_ATTR_MASKS		   (METHOD_CLASS | \
									METHOD_LABEL | \
									METHOD_HELP | \
									METHOD_DEF | \
									METHOD_VALID | \
									METHOD_SCOPE | \
									METHOD_TYPE | \
									METHOD_PARAMS | \
									METHOD_DEBUG  )

/* REFRESH attribute masks */

#define REFRESH_ITEMS               (1<<REFRESH_ITEMS_ID)
#define REFRESH_DEBUG				(1<<REFRESH_DEBUG_ID)

#define REFRESH_ATTR_MASKS			(REFRESH_ITEMS | REFRESH_DEBUG)

/* UNIT attribute masks */

#define UNIT_ITEMS                  (1<<UNIT_ITEMS_ID)
#define UNIT_DEBUG					(1<<UNIT_DEBUG_ID)

#define UNIT_ATTR_MASKS				(UNIT_ITEMS | UNIT_DEBUG)

/* WRITE AS ONE attribute masks */

#define WAO_ITEMS                   (1<<WAO_ITEMS_ID)
#define WAO_DEBUG					(1<<WAO_DEBUG_ID)

#define WAO_ATTR_MASKS				(WAO_ITEMS | WAO_DEBUG)

/* ITEM_ARRAY attribute masks */

#define ITEM_ARRAY_ELEMENTS         (1<<ITEM_ARRAY_ELEMENTS_ID)
#define ITEM_ARRAY_LABEL            (1<<ITEM_ARRAY_LABEL_ID)
#define ITEM_ARRAY_HELP             (1<<ITEM_ARRAY_HELP_ID)
#define ITEM_ARRAY_VALIDITY			(1<<ITEM_ARRAY_VALIDITY_ID)
#define ITEM_ARRAY_DEBUG			(1<<ITEM_ARRAY_DEBUG_ID)

#define	ITEM_ARRAY_ATTR_MASKS	   (ITEM_ARRAY_ELEMENTS | \
									ITEM_ARRAY_LABEL | \
									ITEM_ARRAY_HELP | \
									ITEM_ARRAY_VALIDITY | \
									ITEM_ARRAY_DEBUG )

/* COLLECTION attribute masks */

#define COLLECTION_MEMBERS          (1<<COLLECTION_MEMBERS_ID)
#define COLLECTION_LABEL            (1<<COLLECTION_LABEL_ID)
#define COLLECTION_HELP             (1<<COLLECTION_HELP_ID)
#define COLLECTION_VALIDITY			(1<<COLLECTION_VALIDITY_ID)
#define COLLECTION_DEBUG			(1<<COLLECTION_DEBUG_ID)

#define	COLLECTION_ATTR_MASKS	   (COLLECTION_MEMBERS | \
									COLLECTION_LABEL | \
									COLLECTION_HELP  | \
									COLLECTION_VALIDITY | \
									COLLECTION_DEBUG )

/* PROGRAM attribute masks */

#define PROGRAM_ARGS                (1<<PROGRAM_ARGS_ID)
#define PROGRAM_RESP_CODES          (1<<PROGRAM_RESP_CODES_ID)

#define PROGRAM_ATTR_MASKS		   (PROGRAM_ARGS | \
									PROGRAM_RESP_CODES)

/* RECORD attribute masks */

#define RECORD_MEMBERS              (1<<RECORD_MEMBERS_ID)
#define RECORD_LABEL                (1<<RECORD_LABEL_ID)
#define RECORD_HELP                 (1<<RECORD_HELP_ID)
#define RECORD_RESP_CODES           (1<<RECORD_RESP_CODES_ID)
#define RECORD_DEBUG				(1<<RECORD_DEBUG_ID)

#define	RECORD_ATTR_MASKS		   (RECORD_MEMBERS | \
									RECORD_LABEL | \
									RECORD_HELP | \
									RECORD_RESP_CODES | \
									RECORD_DEBUG)

/* ARRAY attribute masks */

#define ARRAY_LABEL                 (1<<ARRAY_LABEL_ID)
#define ARRAY_HELP                  (1<<ARRAY_HELP_ID)
#define ARRAY_TYPE                  (1<<ARRAY_TYPE_ID)
#define ARRAY_NUM_OF_ELEMENTS       (1<<ARRAY_NUM_OF_ELEMENTS_ID)
#define ARRAY_VALID					   (1<<ARRAY_VALID_ID)
#define ARRAY_DEBUG					(1<<ARRAY_DEBUG_ID)

//Todo remove the ARRAY_RESP_CODES from DDS
#define ARRAY_RESP_CODES            (1<<ARRAY_RESP_CODES_ID)


#define	ARRAY_ATTR_MASKS		   (ARRAY_LABEL | \
									ARRAY_HELP  | \
									ARRAY_VALID |\
									ARRAY_TYPE  | \
									ARRAY_NUM_OF_ELEMENTS | \
									ARRAY_DEBUG )

/* VARIABLE LIST attribute masks */

#define VAR_LIST_MEMBERS            (1<<VAR_LIST_MEMBERS_ID)
#define VAR_LIST_LABEL              (1<<VAR_LIST_LABEL_ID)
#define VAR_LIST_HELP               (1<<VAR_LIST_HELP_ID)
#define VAR_LIST_RESP_CODES         (1<<VAR_LIST_RESP_CODES_ID)
#define VAR_LIST_DEBUG				(1<<VAR_LIST_DEBUG_ID)

#define	VAR_LIST_ATTR_MASKS		   (VAR_LIST_MEMBERS | \
									VAR_LIST_LABEL | \
									VAR_LIST_HELP | \
									VAR_LIST_RESP_CODES | \
									VAR_LIST_DEBUG)

/* RESPONSE CODE attribute masks */

#define RESP_CODE_MEMBER            (1<<RESP_CODE_MEMBER_ID)

#define RESP_CODE_ATTR_MASKS		RESP_CODE_MEMBER

/* DOMAIN attribute masks */

#define DOMAIN_HANDLING             (1<<DOMAIN_HANDLING_ID)
#define DOMAIN_RESP_CODES           (1<<DOMAIN_RESP_CODES_ID)

#define	DOMAIN_ATTR_MASKS		   (DOMAIN_HANDLING | \
									DOMAIN_RESP_CODES)

/* COMMAND attribute masks */

#define COMMAND_NUMBER              (1<<COMMAND_NUMBER_ID)
#define COMMAND_OPER                (1<<COMMAND_OPER_ID)
#define COMMAND_TRANS               (1<<COMMAND_TRANS_ID)
#define COMMAND_RESP_CODES          (1<<COMMAND_RESP_CODES_ID)
#define COMMAND_DEBUG				(1<<COMMAND_DEBUG_ID)

#define	COMMAND_ATTR_MASKS		   (COMMAND_NUMBER | \
									COMMAND_OPER | \
									COMMAND_TRANS | \
									COMMAND_RESP_CODES | \
									COMMAND_DEBUG )

/* AXIS attribute masks */

#define AXIS_LABEL					(1<<AXIS_LABEL_ID)	
#define AXIS_HELP					(1<<AXIS_HELP_ID)	
#define AXIS_VALID					(1<<AXIS_VALID_ID)	
#define AXIS_MINVAL					(1<<AXIS_MINVAL_ID)	
#define AXIS_MAXVAL					(1<<AXIS_MAXVAL_ID)	
#define AXIS_SCALING				(1<<AXIS_SCALING_ID)	
#define AXIS_CONSTUNIT				(1<<AXIS_CONSTUNIT_ID)
#define AXIS_DEBUG					(1<<AXIS_DEBUG_ID)
//#define AXIS_VIEW_MIN_VAL			(1<<AXIS_VIEW_MIN_VAL_ID)	// Unused
//#define AXIS_VIEW_MAX_VAL			(1<<AXIS_VIEW_MAX_VAL_ID)	// Unused
#define AXIS_MAX_VAL_REF			(1<<AXIS_MAX_VAL_REF_ID)
#define AXIS_MIN_VAL_REF			(1<<AXIS_MIN_VAL_REF_ID)

#define AXIS_ATTR_MASKS			   (AXIS_LABEL	| \
									AXIS_HELP	| \
									AXIS_VALID	| \
									AXIS_MINVAL	| \
									AXIS_MAXVAL	| \
									AXIS_SCALING | \
									AXIS_CONSTUNIT | \
									AXIS_DEBUG  | \
									AXIS_MAX_VAL_REF | \
									AXIS_MIN_VAL_REF)

/* CHART attribute masks */

#define CHART_LABEL					(1<<CHART_LABEL_ID)	
#define CHART_HELP					(1<<CHART_HELP_ID)
#define CHART_VALID					(1<<CHART_VALID_ID)
#define CHART_HEIGHT				(1<<CHART_HEIGHT_ID)
#define CHART_WIDTH					(1<<CHART_WIDTH_ID)
#define CHART_TYPE					(1<<CHART_TYPE_ID)
#define CHART_LENGTH				(1<<CHART_LENGTH_ID)
#define CHART_CYCLETIME				(1<<CHART_CYCLETIME_ID)
#define CHART_MEMBERS				(1<<CHART_MEMBERS_ID)
#define CHART_DEBUG					(1<<CHART_DEBUG_ID)

#define CHART_ATTR_MASKS		   (CHART_LABEL	| \
									CHART_HELP	| \
									CHART_VALID	| \
									CHART_HEIGHT| \
									CHART_WIDTH	| \
									CHART_TYPE	| \
									CHART_LENGTH | \
									CHART_CYCLETIME	| \
									CHART_MEMBERS | \
									CHART_DEBUG  )

/* FILE attribute masks */

#define FILE_MEMBERS				(1<<FILE_MEMBERS_ID	)
#define FILE_LABEL					(1<<FILE_LABEL_ID	)
#define FILE_HELP					(1<<FILE_HELP_ID	)
#define FILE_DEBUG					(1<<FILE_DEBUG_ID)

#define FILE_ATTR_MASKS			   (FILE_MEMBERS | \
									FILE_LABEL	| \
									FILE_HELP   | \
									FILE_DEBUG  ) 
									
/* GRAPH attribute masks */

#define GRAPH_LABEL					(1<<GRAPH_LABEL_ID		)
#define GRAPH_HELP					(1<<GRAPH_HELP_ID		)
#define GRAPH_VALID					(1<<GRAPH_VALID_ID		)
#define GRAPH_HEIGHT				(1<<GRAPH_HEIGHT_ID		)
#define GRAPH_WIDTH					(1<<GRAPH_WIDTH_ID		)
#define GRAPH_XAXIS					(1<<GRAPH_XAXIS_ID		)
#define GRAPH_MEMBERS				(1<<GRAPH_MEMBERS_ID	)
#define GRAPH_DEBUG					(1<<GRAPH_DEBUG_ID		)
#define GRAPH_CYCLETIME				(1<<GRAPH_CYCLETIME_ID	)

#define GRAPH_ATTR_MASKS		   (GRAPH_LABEL		| \
									GRAPH_HELP		| \
									GRAPH_VALID		| \
									GRAPH_HEIGHT	| \
									GRAPH_WIDTH		| \
									GRAPH_XAXIS		| \
									GRAPH_MEMBERS	| \
									GRAPH_DEBUG		| \
									GRAPH_CYCLETIME	)

/* LIST attribute masks */

#define LIST_LABEL					(1<<LIST_LABEL_ID	)
#define LIST_HELP					(1<<LIST_HELP_ID	)
#define LIST_VALID					(1<<LIST_VALID_ID	)
#define LIST_TYPE					(1<<LIST_TYPE_ID	)
#define LIST_COUNT					(1<<LIST_COUNT_ID	)
#define LIST_CAPACITY				(1<<LIST_CAPACITY_ID)
#define LIST_DEBUG					(1<<LIST_DEBUG_ID)
//#define LIST_FIRST				(1<<LIST_FIRST_ID)		- unused
//#define LIST_LAST					(1<<LIST_LAST_ID)		- unused
#define LIST_COUNT_REF				(1<<LIST_COUNT_REF_ID)


#define LIST_ATTR_MASKS			   (LIST_LABEL	| \
									LIST_HELP	| \
									LIST_VALID	| \
									LIST_TYPE	| \
									LIST_COUNT	| \
									LIST_CAPACITY | \
									LIST_DEBUG  | \
									LIST_COUNT_REF)

/* SOURCE attribute masks */

#define SOURCE_LABEL				(1<<SOURCE_LABEL_ID		)
#define SOURCE_HELP					(1<<SOURCE_HELP_ID		)
#define SOURCE_VALID				(1<<SOURCE_VALID_ID		)
#define SOURCE_EMPHASIS				(1<<SOURCE_EMPHASIS_ID	)
#define SOURCE_LINETYPE				(1<<SOURCE_LINETYPE_ID	)
#define SOURCE_LINECOLOR			(1<<SOURCE_LINECOLOR_ID	)
#define SOURCE_YAXIS				(1<<SOURCE_YAXIS_ID		)
#define SOURCE_MEMBERS				(1<<SOURCE_MEMBERS_ID	)
#define SOURCE_DEBUG				(1<<SOURCE_DEBUG_ID)
#define SOURCE_INIT_ACTIONS			(1<<SOURCE_INIT_ACTIONS_ID	)
#define SOURCE_RFRSH_ACTIONS		(1<<SOURCE_RFRSH_ACTIONS_ID	)
#define SOURCE_EXIT_ACTIONS			(1<<SOURCE_EXIT_ACTIONS_ID	)

#define SOURCE_ATTR_MASKS		   (SOURCE_LABEL	| \
									SOURCE_HELP		| \
									SOURCE_VALID	| \
									SOURCE_EMPHASIS	| \
									SOURCE_LINETYPE	| \
									SOURCE_LINECOLOR| \
									SOURCE_YAXIS	| \
									SOURCE_MEMBERS  | \
									SOURCE_INIT_ACTIONS  | \
									SOURCE_RFRSH_ACTIONS | \
									SOURCE_EXIT_ACTIONS  | \
									SOURCE_DEBUG    )
									
/* WAVEFORM attribute masks */

#define WAVEFORM_LABEL				(1<<WAVEFORM_LABEL_ID			)
#define WAVEFORM_HELP				(1<<WAVEFORM_HELP_ID			)
#define WAVEFORM_HANDLING			(1<<WAVEFORM_HANDLING_ID		)
#define WAVEFORM_EMPHASIS			(1<<WAVEFORM_EMPHASIS_ID		)
#define WAVEFORM_LINETYPE			(1<<WAVEFORM_LINETYPE_ID		)
#define WAVEFORM_LINECOLOR			(1<<WAVEFORM_LINECOLOR_ID		)
#define WAVEFORM_YAXIS				(1<<WAVEFORM_YAXIS_ID			)
#define WAVEFORM_KEYPTS_X			(1<<WAVEFORM_KEYPTS_X_ID		)
#define WAVEFORM_KEYPTS_Y			(1<<WAVEFORM_KEYPTS_Y_ID		)
#define WAVEFORM_TYPE				(1<<WAVEFORM_TYPE_ID			)
#define WAVEFORM_X_VALUES			(1<<WAVEFORM_X_VALUES_ID		)
#define WAVEFORM_Y_VALUES			(1<<WAVEFORM_Y_VALUES_ID		)
#define WAVEFORM_X_INITIAL			(1<<WAVEFORM_X_INITIAL_ID		)
#define WAVEFORM_X_INCREMENT		(1<<WAVEFORM_X_INCREMENT_ID		)
#define WAVEFORM_POINT_COUNT		(1<<WAVEFORM_POINT_COUNT_ID		)
#define WAVEFORM_INIT_ACTIONS		(1<<WAVEFORM_INIT_ACTIONS_ID	)
#define WAVEFORM_RFRSH_ACTIONS		(1<<WAVEFORM_RFRSH_ACTIONS_ID	)
#define WAVEFORM_EXIT_ACTIONS		(1<<WAVEFORM_EXIT_ACTIONS_ID	)
#define WAVEFORM_DEBUG				(1<<WAVEFORM_DEBUG_ID           )
#define WAVEFORM_VALID				(1<<WAVEFORM_VALID_ID       	)

#define WAVEFORM_ATTR_MASKS		   (WAVEFORM_LABEL			| \
									WAVEFORM_HELP			| \
									WAVEFORM_HANDLING		| \
									WAVEFORM_EMPHASIS		| \
									WAVEFORM_LINETYPE		| \
									WAVEFORM_LINECOLOR		| \
									WAVEFORM_YAXIS			| \
									WAVEFORM_KEYPTS_X		| \
									WAVEFORM_KEYPTS_Y		| \
									WAVEFORM_TYPE			| \
									WAVEFORM_X_VALUES		| \
									WAVEFORM_Y_VALUES		| \
									WAVEFORM_X_INITIAL		| \
									WAVEFORM_X_INCREMENT	| \
									WAVEFORM_POINT_COUNT	| \
									WAVEFORM_INIT_ACTIONS	| \
									WAVEFORM_RFRSH_ACTIONS	| \
									WAVEFORM_EXIT_ACTIONS   | \
									WAVEFORM_DEBUG          | \
									WAVEFORM_VALID			)
/* IMAGE attribute masks */

#define IMAGE_LABEL					(1<<IMAGE_LABEL_ID	)
#define IMAGE_HELP					(1<<IMAGE_HELP_ID	)
#define IMAGE_VALID					(1<<IMAGE_VALID_ID	)
#define IMAGE_LINK					(1<<IMAGE_LINK_ID	)
#define IMAGE_PATH				    (1<<IMAGE_PATH_ID)
#define IMAGE_DEBUG					(1<<IMAGE_DEBUG_ID)
#define IMAGE_BINARY                (1<<IMAGE_BINARY_ID)

#define IMAGE_ATTR_MASKS		   (IMAGE_LABEL	| \
									IMAGE_HELP	| \
									IMAGE_VALID	| \
									IMAGE_LINK	| \
									IMAGE_PATH  | \
									IMAGE_DEBUG | \
									IMAGE_BINARY)
/* GRID attribute masks */

#define GRID_LABEL					(1<<GRID_LABEL_ID	)
#define GRID_HELP					(1<<GRID_HELP_ID	)
#define GRID_VALID					(1<<GRID_VALID_ID	)
#define GRID_HEIGHT					(1<<GRID_HEIGHT_ID	)
#define GRID_WIDTH					(1<<GRID_WIDTH_ID	)
#define GRID_ORIENT					(1<<GRID_ORIENT_ID	)
#define GRID_HANDLING				(1<<GRID_HANDLING_ID)
#define GRID_MEMBERS			    (1<<GRID_MEMBERS_ID	)
#define GRID_DEBUG					(1<<GRID_DEBUG_ID)

#define GRID_ATTR_MASKS			   (GRID_LABEL	  | \
									GRID_HELP	  | \
									GRID_VALID	  | \
									GRID_HEIGHT	  | \
									GRID_WIDTH	  | \
									GRID_ORIENT	  | \
									GRID_HANDLING | \
									GRID_MEMBERS  | \
									GRID_DEBUG    )
/*
 * Table Identifiers for DDOD Directory Objects.
 * These are also used as bit positions for the
 * Directory Table Masks defined below.
 */

#define	BLOCK_NAME_SIZE		32	/* This is the max size of a block name */

/* DEVICE DIRECTORY tables */

#define	BLK_TBL_ID			 			0
#define	ITEM_TBL_ID					 	1
#define	PROG_TBL_ID					 	2
#define	DOMAIN_TBL_ID				 	3
#define	STRING_TBL_ID				 	4
#define	DICT_REF_TBL_ID				 	5
#define	MAX_FF_DEVICE_TBL_ID			15	/* must be last in list (FF only) */
                                           
#define	LOCAL_VAR_TBL_ID				6
#define	CMD_NUM_ID_TBL_ID				7
#define IMAGE_TBL_ID					8
#define	MAX_HCF_DEVICE_TBL_ID			9	/* must be last in list (Hart only) */

#define	MAX_DEVICE_TBL_ID			 	MAX_HCF_DEVICE_TBL_ID
#define	MAX_DEVICE_TBL_ID_5			 	IMAGE_TBL_ID

/* BLOCK DIRECTORY tables */
					// Found in tok_major_rev	5	6	8
#define	BLK_ITEM_TBL_ID				 	0	//	Y	Y	Y
#define	BLK_ITEM_NAME_TBL_ID		 	1	//	Y	Y	Y
#define PARAM_TBL_ID				 	2	//	Y	Y	Y
#define	PARAM_MEM_TBL_ID			 	3	//	Y	Y	
#define	PARAM_MEM_NAME_TBL_ID			4	//	Y	Y	
#define PARAM_ELEM_TBL_ID				5	//	Y	Y	
#define	PARAM_LIST_TBL_ID			 	6	//	Y	Y	
#define	PARAM_LIST_MEM_TBL_ID		 	7	//	Y	Y	
#define PARAM_LIST_MEM_NAME_TBL_ID		8	//	Y	Y	
#define	CHAR_MEM_TBL_ID				 	9	//	Y	Y	
#define CHAR_MEM_NAME_TBL_ID			10	//	Y	Y	
#define	REL_TBL_ID					 	11	//	Y	Y	Y
#define UPDATE_TBL_ID					12	//	Y	Y	Y
#define COMMAND_TBL_ID					13	//	Y	Y	Y
#define CRIT_PARAM_TBL_ID				14	//	Y	Y	Y
#define RESLV_REF_TBL_ID				15	//	 	 	Y (optional)
					// Number of tables			15	15	7 + 1(optional)
#define	MAX_HCF_BLOCK_TBL_ID			16	/* must be last in list (Hart only) */

#define NUM_REQ_BLOCK_TBLS_5	15
#define NUM_REQ_BLOCK_TBLS		15
#define NUM_REQ_BLOCK_TBLS_8	7		// Number of required Block Tables (RESLV_REF_TBL is optional)


// Default refresh action cycle time
#define DEFAULT_REFRESH_CYCLE_TIME		1000

/*
 *	Directory Table Masks.
 */

/* DEVICE DIRECTORY table masks */

#define	BLK_TBL_MASK					(1<<BLK_TBL_ID)
#define	ITEM_TBL_MASK					(1<<ITEM_TBL_ID)
#define	PROG_TBL_MASK					(1<<PROG_TBL_ID)
#define	DOMAIN_TBL_MASK					(1<<DOMAIN_TBL_ID)
#define	STRING_TBL_MASK					(1<<STRING_TBL_ID)
#define	DICT_REF_TBL_MASK				(1<<DICT_REF_TBL_ID)
#define	LOCAL_VAR_TBL_MASK				(1<<LOCAL_VAR_TBL_ID)
#define	CMD_NUM_ID_TBL_MASK				(1<<CMD_NUM_ID_TBL_ID)
#define IMAGE_TBL_MASK					(1<<IMAGE_TBL_ID)
#define RESV_DEV_MASK				IMAGE_TBL_MASK    // sjv add image table 9/17/04
#define RESV_DEV_MASK_6				IMAGE_TBL_MASK

#define DEVICE_TBL_MASKS_HCF	BLK_TBL_MASK | \
								ITEM_TBL_MASK | \
								PROG_TBL_MASK | \
								DOMAIN_TBL_MASK | \
								STRING_TBL_MASK | \
								DICT_REF_TBL_MASK | \
								LOCAL_VAR_TBL_MASK | \
								CMD_NUM_ID_TBL_MASK |\
								IMAGE_TBL_MASK

#define DEVICE_TBL_MASKS		DEVICE_TBL_MASKS_HCF

#define DEVICE_TBL_MASKS_FF		BLK_TBL_MASK | \
								ITEM_TBL_MASK | \
								PROG_TBL_MASK | \
								DOMAIN_TBL_MASK | \
								STRING_TBL_MASK | \
								DICT_REF_TBL_MASK


/* BLOCK DIRECTORY table masks */

#define	BLK_ITEM_TBL_MASK				(1<<BLK_ITEM_TBL_ID)
#define	BLK_ITEM_NAME_TBL_MASK			(1<<BLK_ITEM_NAME_TBL_ID)
#define PARAM_TBL_MASK					(1<<PARAM_TBL_ID)
#define	PARAM_MEM_TBL_MASK				(1<<PARAM_MEM_TBL_ID)
#define PARAM_MEM_NAME_TBL_MASK			(1<<PARAM_MEM_NAME_TBL_ID)
#define PARAM_ELEM_TBL_MASK				(1<<PARAM_ELEM_TBL_ID)
#define	PARAM_LIST_TBL_MASK				(1<<PARAM_LIST_TBL_ID)
#define	PARAM_LIST_MEM_TBL_MASK			(1<<PARAM_LIST_MEM_TBL_ID)
#define PARAM_LIST_MEM_NAME_TBL_MASK	(1<<PARAM_LIST_MEM_NAME_TBL_ID)
#define	CHAR_MEM_TBL_MASK				(1<<CHAR_MEM_TBL_ID)
#define CHAR_MEM_NAME_TBL_MASK			(1<<CHAR_MEM_NAME_TBL_ID)
#define	REL_TBL_MASK					(1<<REL_TBL_ID)
#define	UPDATE_TBL_MASK					(1<<UPDATE_TBL_ID)
#define COMMAND_TBL_MASK				(1<<COMMAND_TBL_ID)
#define CRIT_PARAM_TBL_MASK				(1<<CRIT_PARAM_TBL_ID)
#define RESLV_REF_TBL_MASK				(1<<RESLV_REF_TBL_ID)


#define BLOCK_TBL_MASKS_HCF		BLK_ITEM_TBL_MASK | \
								BLK_ITEM_NAME_TBL_MASK | \
								PARAM_TBL_MASK | \
								PARAM_MEM_TBL_MASK | \
								PARAM_MEM_NAME_TBL_MASK | \
								PARAM_ELEM_TBL_MASK | \
								PARAM_LIST_TBL_MASK | \
								PARAM_LIST_MEM_TBL_MASK | \
								PARAM_LIST_MEM_NAME_TBL_MASK | \
								CHAR_MEM_TBL_MASK | \
								CHAR_MEM_NAME_TBL_MASK | \
								REL_TBL_MASK | \
								UPDATE_TBL_MASK | \
								COMMAND_TBL_MASK | \
								CRIT_PARAM_TBL_MASK

#define BLOCK_TBL_MASKS_HCF_8	BLK_ITEM_TBL_MASK | \
								BLK_ITEM_NAME_TBL_MASK | \
								PARAM_TBL_MASK | \
								REL_TBL_MASK | \
								UPDATE_TBL_MASK | \
								COMMAND_TBL_MASK | \
								CRIT_PARAM_TBL_MASK | \
								RESLV_REF_TBL_MASK

#define BLOCK_TBL_MASKS			BLOCK_TBL_MASKS_HCF	

#define BLOCK_TBL_MASKS_8		BLOCK_TBL_MASKS_HCF_8

#define BLOCK_TBL_MASKS_ALL		(BLOCK_TBL_MASKS | BLOCK_TBL_MASKS_8)

#endif	/* DDLDEFS_H */

