/*
 * @(#) $Id: attrs.h,v 1.3 1996/03/14 04:08:00 stevbey Exp $
 */

#ifndef ATTRS_H
#define ATTRS_H

#ifndef DDLDEFS_H
#include "ddldefs.h"
#endif

#ifndef OD_DEFS_H
#include "od_defs.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif

// Values for STRING.flags below
#define	DONT_FREE_STRING	0X00	// When you clean this STRING, you don't have
									// free the str pointer because this string points
									// to the STRING_TABLE
#define	FREE_STRING			0X01	// When you clean this STRING, you must call
									// free on the str pointer, since this string
									// was malloc'ed for use by this STRING

enum OpType
{
	STANDARD_TYPE	= 0,
	COMPLEX_TYPE	= 1
};

typedef struct {
	wchar_t			*str;	/* the pointer to the string */
	unsigned short  len;	/* the length of the string */
	unsigned short  flags;	/* memory allocation flags */
}               STRING;

typedef struct 
{
	BYTE*		ptr;					// Start of bytestream
	int			len;					// Length of bytestream
} BINARY;

/*
 * Expressions
 */

/*
 * Types of expressions.
 * The permitted type fields are the same as variables types (ddldefs.h)
 */


typedef struct {
	unsigned short  type;	/* valid types are defined in ddldefs.h (ie. INTEGER) */
	unsigned int  size;
	union {
		float           f;		// FLOAT
		double          d;		// DOUBLE
		LONGLONG        ll;		// INTEGER
		ULONGLONG       ull;	// UNSIGNED
		STRING			s;		// ASCII

	}               val;
}               EXPR;

/*
 *  RANGE_DATA_LIST
 */

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	EXPR           *list;
}               RANGE_DATA_LIST;


/*
 * Defines for REFERENCE_ITEM types
 */


typedef struct {
	unsigned short  count;		/* Number of used item IDs */
	unsigned short  limit;		/* Total number of item IDs */
	ITEM_ID        *list;		/* Pointer to list of IDs */
}               ITEM_ID_LIST;


typedef struct {

	ITEM_ID         id;
	ITEM_TYPE       type;
}               DESC_REF;

typedef struct tag_OP_REF_INFO {
	ITEM_ID		id;
	ITEM_ID		member;
	ITEM_TYPE	type;

	bool operator< (const tag_OP_REF_INFO &rhs) const;
	bool operator== (const tag_OP_REF_INFO &rhs);
}				OP_REF_INFO;
// pseudo operator=
OP_REF_INFO& Assign_OP_REF_INFO (OP_REF_INFO &lhs, const OP_REF_INFO &rhs);

typedef struct tag_OP_REF_INFO_LIST {
	unsigned short	count;
	OP_REF_INFO		*list;
}				OP_REF_INFO_LIST;
// pseudo operator=
OP_REF_INFO_LIST& Assign_OP_REF_INFO_LIST (OP_REF_INFO_LIST &lhs, const OP_REF_INFO_LIST &rhs);


typedef struct tag_OP_REF {

	OpType				op_ref_type;
	OP_REF_INFO			op_info;
	OP_REF_INFO_LIST	op_info_list;

	tag_OP_REF& operator= (const tag_OP_REF &rhs);
	bool operator< (const tag_OP_REF &rhs)const;
}               OP_REF;


typedef struct tag_OP_REF_LIST {
	unsigned short  count;
	unsigned short  limit;
	OP_REF         *list;
}               OP_REF_LIST;



typedef struct {

	ITEM_TYPE       type;
	ITEM_ID         id;
	unsigned long   element;
}               RESOLVE_INFO;

typedef struct tag_OP_REF_TRAIL {
	OpType				op_ref_type;
	OP_REF_INFO			op_info;
	OP_REF_INFO_LIST	op_info_list;

	ITEM_ID         desc_id;
	ITEM_TYPE       desc_type;
    unsigned long   bit_mask;  /* Used for BIT_ENUM variables on menus, set to 0 if unused */

	unsigned short  trail_count;
	unsigned short  trail_limit;
	RESOLVE_INFO   *trail;
	EXPR			expr;

}               OP_REF_TRAIL;


typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OP_REF_TRAIL   *list;
}               OP_REF_TRAIL_LIST;


/*
 * Binary and Dependency Info
 */


typedef struct {
	unsigned long   size;
	unsigned char  *chunk;
}               BININFO;


typedef struct {
	OP_REF_LIST     dep;
	unsigned long   bin_size;
	unsigned char  *bin_chunk;
}               DEPBIN;



/* The masks for ITEM_ARRAY_ELEMENT */

#define IA_DESC_EVALED		0X01
#define IA_HELP_EVALED		0X02
#define IA_INDEX_EVALED		0X04
#define IA_REF_EVALED		0X08


typedef struct {
	unsigned short  evaled;
	unsigned long   index;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
}               ITEM_ARRAY_ELEMENT;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	ITEM_ARRAY_ELEMENT *list;
}               ITEM_ARRAY_ELEMENT_LIST;


/* The masks for MEMBER */

#define MEM_DESC_EVALED		0X01
#define MEM_HELP_EVALED		0X02
#define MEM_NAME_EVALED		0X04
#define MEM_REF_EVALED		0X08
#define MEM_NAME_STR_EVALED	0X10

typedef struct {
	unsigned short  evaled;
	unsigned long   name;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
	STRING          name_string;
}               MEMBER;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	MEMBER         *list;
}               MEMBER_LIST;

typedef struct {
	unsigned short  evaled;
	unsigned long   name;
	OP_REF_TRAIL    ref;
	STRING          desc;
	STRING          help;
	STRING          name_string;
}               OP_MEMBER;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OP_MEMBER       *list;
}               OP_MEMBER_LIST;

/*
 * Menu
 */

typedef struct {
	OP_REF_TRAIL    ref;
	unsigned short  qual;
}               MENU_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	MENU_ITEM      *list;
}               MENU_ITEM_LIST;


/*
 * Response Codes
 */

/* The masks for RESPONSE_CODE */

#define RS_DESC_EVALED		0X01
#define RS_HELP_EVALED		0X02
#define RS_TYPE_EVALED		0X04
#define RS_VAL_EVALED		0X08


typedef struct {
	unsigned short  evaled;
	unsigned short  val;
	unsigned short  type;
	STRING          desc;
	STRING          help;
}               RESPONSE_CODE;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	RESPONSE_CODE  *list;
}               RESPONSE_CODE_LIST;


/*
 * Relation Types
 */

typedef struct {

	OP_REF_TRAIL_LIST depend_items;
	OP_REF_TRAIL_LIST update_items;

}               REFRESH_RELATION;


typedef struct {

	OP_REF_TRAIL    var;
	OP_REF_TRAIL_LIST var_units;

}               UNIT_RELATION;


/*
 * Definitions
 */

typedef struct {
	unsigned long   size;
	char           *data;
}               DEFINITION;


/*
 * Variable Types
 */

typedef struct {
	unsigned short  type;
	unsigned short  size;
}               TYPE_SIZE;



/*
 * Enumerations
 */


typedef struct {
	unsigned short  kind;
	unsigned short  which;
	unsigned short  oclass;
}               OUTPUT_STATUS;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	OUTPUT_STATUS  *list;
}               OUTPUT_STATUS_LIST;



typedef struct {
	unsigned long   status_class;
	OUTPUT_STATUS_LIST oclasses;
}               BIT_ENUM_STATUS;


/* The masks for ENUM_VALUE */

#define ENUM_ACTIONS_EVALED		0X01
#define ENUM_CLASS_EVALED		0X02
#define ENUM_DESC_EVALED		0X04
#define ENUM_HELP_EVALED		0X08
#define ENUM_STATUS_EVALED		0X10
#define ENUM_VAL_EVALED			0X20


typedef struct {
	unsigned short  evaled;
	unsigned long   val;
	STRING          desc;
	STRING          help;
	unsigned long   func_class;	/* functional class */
	BIT_ENUM_STATUS status;
	ITEM_ID         actions;
}               ENUM_VALUE;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	ENUM_VALUE     *list;
}               ENUM_VALUE_LIST;

/*
 * Data Fields
 */

typedef struct {
	union {
		long  iconst;
		OP_REF_TRAIL    ref;
		float           fconst;
	}               data;
	unsigned short  type;
	unsigned short  flags;
	unsigned short  width;
	unsigned long   data_item_mask;
}               DATA_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	DATA_ITEM      *list;
}               DATA_ITEM_LIST;

/*
 * Vector Fields
 */

typedef struct {
	union {
		long  iconst;
		OP_REF_TRAIL    ref;
		float           fconst;
        STRING          str;
	}               vector;
	unsigned short  type;
}               VECTOR_ITEM;

typedef struct {
	unsigned short  count;
	unsigned short  limit;
	STRING			description;
	VECTOR_ITEM      *list;
}               VECTOR;


typedef struct {
	unsigned short  count;
	unsigned short  limit;
	VECTOR      *vectors;
}               VECTOR_LIST;


/*
 * Transactions
 */

typedef struct {
	unsigned long   number;
	DATA_ITEM_LIST  request;
	DATA_ITEM_LIST  reply;
	RESPONSE_CODE_LIST rcodes;
}               TRANSACTION;


typedef struct {
	unsigned short  count;
	unsigned short  limit;
	TRANSACTION    *list;
}               TRANSACTION_LIST;



#ifdef __cplusplus
    }
#endif

#endif	/* ATTRS_H */
