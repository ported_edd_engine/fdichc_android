
/******************************************************************************
 * $Header: cm_comm.cpp: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:24 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description: Connection Manager Communication module
 *													   
 * $Workfile: cm_comm.cpp$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#include "stdinc.h"
#include "ConnectionMgr.h"


/***********************************************************************
 *
 *	Name:  cm_find_block
 *
 *	ShortDesc:  Find a block with the specified tag.
 *
 *	Description:
 *
 *	Inputs:
 *		tag - the tag of the block that is sought.
 *
 *	Outputs:
 *		find_cnf - the corresponding Find Confirm.
 *
 *	Returns:
 *		Zero for success and non-zero for failure.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::cm_find_block(LPTSTR block_tag,ENV_INFO *env, BLK_LIST *blk_list, CM_OP /*operation*/)


{
	
	int					r_code = CM_SUCCESS;
	int					i ;

	ASSERT_RET(env && env->app_info && block_tag && blk_list, CM_BAD_POINTER);


	env->block_handle = blk_list->list[0].block_handle ;
			
			

	if (blk_list->count == 0) {
		r_code = CM_BLOCK_NOT_FOUND ;
	}
	else {
		r_code = CM_SUCCESS ;
		for (i = 0 ; i <= blk_list->count ; i++) {
			if (blk_list->list[i].status == CM_UPDATE_PENDING) {
				r_code = CM_UPDATE_PENDING ;
				break ;
			}
		}
	}
	return(r_code) ;			
}


