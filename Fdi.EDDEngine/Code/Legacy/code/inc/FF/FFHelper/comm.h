/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)comm.h	30.3  30.3  14 Nov 1996
 *
 *  This file contains common definitions needed for the FMS
 *	communication sub-system.
 *
 */
#ifndef COMM_H
#define COMM_H

#include <AMSDds/DDLDEFS.H>
#include <AMSDds/od_defs.h>

/*
 * Mask values for type in structures PC_P_REF and P_REF
 */

#define PC_PARAM_OFFSET_REF 0x1
#define PC_ITEM_ID_REF      0x2

 /*
 * The following structures are used to define references to parameters
 * when accessing the Parameter Cache.
 */

typedef struct {
    ITEM_ID     id;
    int         po;
    char	*name;	
    SUBINDEX    subindex;
    SUBINDEX    subsubindex;
    int         type;
} P_REF;

typedef int		BLOCK_HANDLE;
typedef	UINT8	BOOLEAN;





#endif /* COMM_H */
