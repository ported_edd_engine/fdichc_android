#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif

//#include <afxwin.h>     // CAY this module does NOT use pre-compiled header file
#include <stdarg.h>
#include "PlatformCommon.h"
#include "stdstring.h"
#include <../inc/Inf/NtcSpecies/ntcassert.h>
#include <../inc/Inf/NtcSpecies/BssProgLog.h>
//#include "../../../../../Src/EDD_Engine_Common/APIs.h"


//class NtAssert

//	Narrow verion


//void* _ams_assert2(const char* exp, const char* fileName, unsigned lineNumber)
//{
//	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"assert(%S) failed in file %S at line %u", exp, fileName, lineNumber);
//	return NULL;
//}

// UNICODE version
void* _ams_assert(const wchar_t* exp, const wchar_t* fileName, unsigned lineNumber)
{
	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"assert(%s) failed in file %s at line %u", exp, fileName, lineNumber);
	return NULL;
}

void DoDebugBreak( DebugBreakState DbgState, LPCTSTR mylpString )
{	
	bool bEnableDebugBreak = DbgState == EnableDebugBreak ? true : false;
	PS_DoDebugBreak(bEnableDebugBreak, mylpString);
}


/*****************************************************************************
 * @func void | BssProgLog |
 *
 * Logs an event.
 *
 * Some events (see below) are logged to Microsoft's event log; The filename,
 * lineNumber, severity, status, and formatted message are all stored as part
 * of a logged event.
 *
 * See the 'example' for full details.
 *
 * @precond
 * cFilename must point to a nul terminated filename string.
 *
 * wMsgFormat should point to a valid nul terminated wchar_t format string.
 *
 * All ... parameters should be valid for the usage in wMsgFormat.
 *
 * @postcond
 * none
 *
 * @globaleffects
 * Some events (see description) are reported to the Miscrosoft Event
 * Logging system.
 *
 * @exceptions
 * The break exception is sometimes raised as described above.
 *
 * @concurrency
 * Multi-task / Multi-thread safe.
 *
 * @notes
 * Final formated wMsgFormat string is limited to 2000 characters in length.
 * BssProgLog will truncate it as necessary.
 *
 * The Microsoft Event Logging system records the date/time when an event
 * is logged.
 *
 * The "example" has further information that may help clarify how
 * BssProgLog should and should not be used.
 *
 * @ex The following writeup and mail messages may help clarify how BssProgLog
 *     should and should not be used. |
 *
 * BssProgLog
 *
 * Logs an event.
 *
 * Some events (see below) are logged to Microsoft's event log; The filename,
 * lineNumber, severity, status, and formatted message are all stored as part
 * of a logged event.
 *
 * The SETTING controls what is logged and when breaks are instantiated.
 *
 * The SETTING is a 5 character string.
 *   SETTING[0] for BssError        severity
 *   SETTING[1] for BssWarning      severity
 *   SETTING[2] for BssInformation  severity
 *   SETTING[3] for BssAuditFailure severity
 *   SETTING[4] for BssAuditSuccess severity
 *
 * Each character of the SETTING can be one of the following:
 *   '.' - no message is logged, no break instantiated
 *   'L' - message is logged, no break instantiated
 *   'B' - message is logged, break instantiated
 *
 * The SETTING defaults are:
 *   "BLLLL" - For development (i.e. NDEBUG undefined)
 *   "L..L." - For release (i.e. NDEBUG defined)
 *
  * @syntax void BssProgLog(
 *     const char *cFilename,
 *     UINT32 lineNumber,
 *     WORD severity,
 *     BssStatus status,
 *     LPCWSTR wMsgFormat=NULL,
 *     ...
 *    );
 *
 * @parm const char* | cFilename | The source code filename,
 *                                 use __FILE__ macro.
 *
 * @parm UINT32 | lineNumber | The source code line number, use __LINE__ macro.
 *
 * @parm WORD | severity | The event severity. One of BssInformation
 * (informational), BssWarning (a warning), BssError (a severe error),
 * BssAuditSuccess (security check suceeded), BssAuditFailure (security
 * check failed).
 *
 * @parm BssStatus | status | Related BssStatus value.
 *
 * @parm LPCWSTR | wMsgFormat | NULL or event message format string
 * (like wprintf)
 *
 * @parmvar Additional parameters used in wMsgFormat.
 *
 *****************************************************************************/

void
BssProgLog (
   const char *cFilename,		// source code filename
   UINT32 lineNumber,			// source code line-number
   WORD severity,				// see documentation
   long status,					// a BssStatus value
   LPCWSTR wMsgFormat,			// a 'wprintf' format
   ...							//    and it's parameters
)
{

    if (wMsgFormat != NULL)
    {

        va_list  ap1 = {};
        va_start (ap1, wMsgFormat);
        PS_BssProgLog(cFilename, lineNumber, severity, status, wMsgFormat, ap1);
        va_end   (ap1);
    }
/*
	USES_CONVERSION;


	// Strings
	const TCHAR *cfilenameIsMissing  = L"FileName is Missing";
	const TCHAR *cnoMsgAvailable     = L"No Message is availible";

	// other constants
	static const long msgBfrDim = 3001;
	
		const int msgsDim = 4;        // dimension of msgAP

		// msg Array of Pointers to strings
		// to put in the event report
		// [0] = filename
		// [1] = lineNumber (decimal)
		// [2] = passed in event id
		// [3] = formatted message or raw msg if formatting fails
		const TCHAR *msgAP[msgsDim] = {0,0,0,0};

		// Verify a filename was passed in, and if not include this in the message
		//
		msgAP[0] = (cFilename != NULL) ? A2CT(cFilename) : cfilenameIsMissing;

		// lineNumber
		TCHAR cLineNumber[32] = { 0 }; // holds more than max UINT32 (4,ddd,ddd,ddd)
		PS_VsnwPrintf(cLineNumber, _countof(cLineNumber), _T("%lu"), lineNumber);
		msgAP[1] = cLineNumber;

		// EventId is set equal to status
		long eventId = status;

		TCHAR cEventID[60] = { 0 }; // holds more than 2 * max INT32 (4,ddd,ddd,ddd)
		PS_VsnwPrintf(cEventID, _countof(cEventID), _T("%d 0x%X"), eventId, eventId);
		msgAP[2] = cEventID;

		//
		// 'formated' message
		TCHAR msgBfr[msgBfrDim] = {0};

		if (wMsgFormat != NULL)
		{
			va_list  ap1 = {0};
			va_start (ap1, wMsgFormat);

			PS_VsnwPrintf(msgBfr, msgBfrDim, W2CT(wMsgFormat), ap1); // Need to convert wMsgFormat to a TCHAR since _vsntprintf needs it
			va_end   (ap1);

			// the formatted message
			msgAP[3] = msgBfr;
		}
		else
		{
			msgAP[3] = cnoMsgAvailable;
			//++msgsN;
		}



		// build debug string
		CStdString myString; 

		myString.Format(L"%s, %s, %s, %s\n", msgAP[0], msgAP[1], msgAP[2], msgAP[3]);


		// we should only break if we are not Information or Warning messages.
		if ( !(severity & (BssInformation | BssWarning)) )
		{
			if(PS_IsDebuggerPresent())
			{
				DoDebugBreak(EnableDebugBreak, myString);
			}
		}

		// Write a log file. can do time stamp in name latter
		errno_t err;
		FILE *fp;
		wchar_t buffer[100] = {0};

		wchar_t fileBuf[MAX_PATH] = {0};
		PS_GetSystemDirectory(fileBuf, _countof(fileBuf));
        PS_Wcsncat(fileBuf, _countof(fileBuf), L"/EDDLogFile.txt", _TRUNCATE);
		fp = PS_wfopen(fileBuf, L"a+");

		if(fp != NULL)
		{ 
			int writecnt;

			err = PS_Wstrdate(buffer);					// Output Date
			writecnt = fwprintf(fp, L"%s ", buffer);
			err = PS_Wstrtime(buffer);
			writecnt = fwprintf(fp, L"%s: ", buffer);	// Output Time

			writecnt = fwprintf(fp, myString);		// Output string

			// 
			PS_GetErrno( &err );


			writecnt = fflush(fp);
			writecnt = fclose(fp);
		}
		else
		{
			PS_GetErrno( &err );
		}
*/
}

