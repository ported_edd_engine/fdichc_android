// ComplexDDExpression.cpp: implementation of the CComplexDDExpression class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ComplexDDExpression.h"
#include "GrammarNodeVisitor.h"
//#include "SymbolTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CComplexDDExpression::CComplexDDExpression()
{
	m_pToken = 0;
}

CComplexDDExpression::CComplexDDExpression(CToken* pToken)
		:m_pToken(pToken)
{
}

CComplexDDExpression::~CComplexDDExpression()
{
	DELETE_PTR(m_pToken);

	size_t nSize = m_vecExpressions.size();
	for(size_t i=0;i<nSize;i++)
	{
		delete m_vecExpressions[i];
	}
	m_vecExpressions.clear();
}

_INT32 CComplexDDExpression::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,	
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)
{
	return pVisitor->visitComplexDDExpression(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);
}

_INT32 CComplexDDExpression::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	return 0;
}

void CComplexDDExpression::Identify(
			_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,m_pToken->GetLexeme());
	strcat(szData,">");

	size_t nSize = m_vecExpressions.size();
	for(size_t i=0;i<nSize;i++)
	{
		m_vecExpressions[i]->Identify(szData);
	}

	strcat(szData,"</");
	strcat(szData,m_pToken->GetLexeme());
	strcat(szData,">");

}

CToken* CComplexDDExpression::GetToken()
{
	return m_pToken;
}

EXPR_VECTOR* CComplexDDExpression::GetExpressions()
{
	return &m_vecExpressions;
}

void CComplexDDExpression::AddDimensionExpr(
			CExpression* pExpr)
{
	m_vecExpressions.push_back(pExpr);
}

_INT32 CComplexDDExpression::GetLineNumber()
{
	if (m_vecExpressions.size() > 0)
	{
		return m_vecExpressions[m_vecExpressions.size()-1]->GetLineNumber();
	}
	else
	{
		return 0;
	}
}

