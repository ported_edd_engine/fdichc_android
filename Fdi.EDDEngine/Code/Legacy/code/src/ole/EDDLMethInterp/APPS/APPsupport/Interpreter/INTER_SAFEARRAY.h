#if !defined(AFX_RUL_SAFEARRAY_H__D92CC21C_0888_4992_9C4F_0FCF39F2C5AC__INCLUDED_)
#define AFX_RUL_SAFEARRAY_H__D92CC21C_0888_4992_9C4F_0FCF39F2C5AC__INCLUDED_

#include "typedefs.h"
//#include "PlatformCommon.h"
#include "SafeVar.h"

extern const int size_ofs[];
int inline size_of( VARIANT_TYPE a ) { return (size_ofs[a]); };


class INTER_VARIANT;

class INTER_SAFEARRAY  
{
public:
	// constructors
	INTER_SAFEARRAY();
	INTER_SAFEARRAY( _CHAR* pszValue, _INT32 arraySize = 0 );
	INTER_SAFEARRAY( _UCHAR* pszValue );
	INTER_SAFEARRAY( const wchar_t* pszValue );
	INTER_SAFEARRAY( _BYTE_STRING* bsValue );
	INTER_SAFEARRAY( void* pmem, unsigned long ulNumOfElements, VARIANT_TYPE vt, unsigned short usElementSize);
	INTER_SAFEARRAY( INTER_SAFEARRAY& safearray);//copy constructor

	virtual ~INTER_SAFEARRAY();
	
	INTER_SAFEARRAY operator+( INTER_SAFEARRAY& );
    void Add(const INTER_SAFEARRAY&);

	INTER_SAFEARRAY& operator=( INTER_SAFEARRAY& safearray );//equal operator
	INTER_SAFEARRAY& operator=( _CHAR*   pSrc);		// converts if this type is USHORT
	INTER_SAFEARRAY& operator=( _UCHAR*  pSrc);
	INTER_SAFEARRAY& operator=( const wchar_t* pSrc);		// converts if this type is CHAR
	INTER_SAFEARRAY& operator=( _BYTE_STRING& src);

	operator _CHAR *(void);						//   assume string - w/conversion (wide 2 narrow)
	operator wchar_t *(void);					//   assume string - w/conversion (narrow 2 wide)
    void Merge(INTER_SAFEARRAY& var2);
    void Add(INTER_SAFEARRAY& var1, INTER_SAFEARRAY& var2);
	_INT32 Allocate();
	void makeEmpty(); // makes all elements zero(preserves type & length)

	_INT32 GetElement(
		_INT32 i32Idx,
		INTER_VARIANT* pvar);// pvar must be a passed in varient to be filled
	_INT32 GetDimension(
		_UINT32 u32Dim,
         INTER_VARIANT* pvar);// pvar must be a passed in varient to be filled

    _USHORT	GetDims(
		vector<_INT32>* pvecDims=0);

	void AddDim(
		INTER_SAFEARRAYBOUND* prgsaBound);

	_INT32 MemoryAllocated();


	void ExpandString(const wchar_t * pSrc, _UINT32 u32Dim = 0);

	void SetAllocationParameters(
		VARIANT_TYPE vt,
        _USHORT cDims,
		INTER_SAFEARRAYBOUND* prgsaBound);

	_INT32 GetNumberOfElements()	// aka .size()
	{
		if (m_data.cDims > 0)
		{
			return m_data.vecBounds[m_data.cDims - 1].cElements;
		}

		return 0;
	}

	_INT32 GetElementSize()
	{
		return (m_data.cbElements);
	}

	_INT32 SetElement(
		_INT32 i32Idx,
		INTER_VARIANT* pvar);

	_INT32 SetDimension(
		_UINT32 u32Dim,
		INTER_VARIANT* pvar);

	_INT32 Type()
		{
			return m_data.varType;
		};

	_INT32 XMLize(
		_CHAR* szData, size_t dataLen);

	_INT32 XMLizeForMethodDebugInfo(
		_CHAR* szData, size_t dataLen);

protected:
	INTER_SAFEARRAY_DATA m_data;
	_INT32				 m_i32mem;
private:
	wchar_t *m_wcharPtr;//this is intended to avoid memory leaks when casting strings between narrow and wide.
	
//public:
//stevev 13feb08	void* getDataPtr(){ return m_data.pvData; };
};

#endif
