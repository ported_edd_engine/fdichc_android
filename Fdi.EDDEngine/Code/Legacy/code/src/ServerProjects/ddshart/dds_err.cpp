/*
 *  @(#) $Id: dds_err.c,v 1.2 1995/12/08 06:33:02 stevbey Exp $
 *  Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	rtn_code.c -
 *  These are the return codes for all routines.  There is a set of
 *  general purpose codes and a set for each of the subsystems.
 */
#include "stdinc.h"


#define nelem(x) (sizeof(x)/sizeof(x[0]))


struct err_struct {
	int             code;
	wchar_t           *string;
};

static const
struct err_struct errstrs[] = {

#include "rtn_def.h"

};



wchar_t*
dds_error_string(int err)
{
    const struct err_struct	*ptr;
    const struct err_struct	*end;
    static wchar_t  buf[BUF_LEN]{0};

    end = errstrs + nelem(errstrs);
    ptr = errstrs;

    /*
     * Scan through the structure of error messages to find the one with
     * the specified error code.  If it has a message associated with it,
     * return the message.  If there is no message, get out of this loop
	 * and fill the output buffer with the 'Error not found' message, then return.
     */
 
    for (ptr = errstrs; ptr < end; ptr++)
	{
        if ((err == ptr->code) && (ptr->string != NULL))
		{
			return (ptr->string);
		}
    }
 
    wprintf(buf, L"Error #%d: not found", err);
 
    return (buf);
}

