#include "stdafx.h"
#include "DDSSupport.h"
#include "dds_upcl.h"
#include "env_info.h"
#include "evl_lib.h"

#include "cmTblManipulation.h"
#include "cm_loc.h"
#include "nsConsumer/IParamCache.h"


// this is the file to dump link dependencies on the ProfibusDDSAdapter and the ProfibusDDS library




void
panic(const char* /*file*/, unsigned /*line*/, const char* /*msg_format*/, ...)
{
}






int g_debugMain = FALSE;

int
CCmTblManipulation::BlockClose(
	BLOCK_HANDLE	/*blockHandle*/, 
	int				/*force*/)
{
	return 0;
}

//void dump_block_tables(BLOCK_HANDLE bh);
//void dump_all_block_tables(BLOCK_HANDLE bh);

void fatal_msg(LPCTSTR /*fmt*/,...)
{
}

 int app_func_get_block_handle(ENV_INFO *env_info, 
									ITEM_ID block_id,
                                    unsigned int /* instance_num */, 
									BLOCK_HANDLE *out_bh)
 {
	 IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	 CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();


	 nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir=NULL;
	 int rc = pConnectionMgr->get_adt_dd_dev_tbls(env_info->block_handle,(void **) &flat_device_dir);

	 if(rc)
		 return rc;

//	dump_all_block_tables(0);

	for(int block_index=0; block_index<flat_device_dir->blk_tbl.count; block_index++)
	{
		::nsEDDEngine::BLK_TBL_ELEM *bte=&flat_device_dir->blk_tbl.list[block_index];

		if(bte->blk_id==block_id)
		{
			*out_bh=block_index;
			return 0;
		}
	}

	*out_bh=0;
	return CM_BLOCK_NOT_FOUND;

 }


// void dump_all_block_tables(BLOCK_HANDLE /*bh*/)
// {
//	 nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir=NULL;
//	 int rc=::get_adt_dd_dev_tbls(0,(void **) &flat_device_dir);
//
//	 if(rc)
//		 return ;
//
//	for(int block_index=0; block_index<flat_device_dir->blk_tbl.count; block_index++)
//	{
//		::nsEDDEngine::BLK_TBL_ELEM *bte=&flat_device_dir->blk_tbl.list[block_index];
//
//		wchar_t buf[1000]{0};
//		wsprintf(buf,_T("\n\nBLOCK %02d %0x TABLES"), block_index, bte->blk_id);
//		PS_OutputDebugString(buf);
//		dump_block_tables(block_index);
//	}
//
//
////	return CM_BLOCK_NOT_FOUND;
//
// }

 //::nsEDDEngine::BLK_TBL_ELEM *get_adt_blk_tbl_elem(BLOCK_HANDLE h)
 //{
	// nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir=NULL;
	// int rc=::get_adt_dd_dev_tbls(0,(void **) &flat_device_dir);

	// if(rc)
	//	 return NULL;

	//for(int block_index=0; block_index<flat_device_dir->blk_tbl.count; block_index++)
	//{
	//	::nsEDDEngine::BLK_TBL_ELEM *bte=&flat_device_dir->blk_tbl.list[block_index];
	//	if(block_index==h)
	//	{
	//		return bte;
	//	}
	//}

	//return NULL;
 //}

 /*void dump_block_tables(BLOCK_HANDLE bh)
 {
	::nsEDDEngine::BLK_TBL_ELEM *bte= get_adt_blk_tbl_elem(bh);

	if(!bte)
		return;

	for(int i=0;i<bte->flat_block_dir.blk_item_name_tbl.count;i++)
	{
		::nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM *e=&bte->flat_block_dir.blk_item_name_tbl.list[i];
        wchar_t buf[1000]{0};
		wsprintf(buf,_T("\n    name = %x, offset = %d"), e->blk_item_name, e->item_tbl_offset);
		PS_OutputDebugString(buf);
	}
 }*/

 
// this is the only function that may need to be updated.  Right now it just returns 1 or 0 
// depending on the type of parameter being fetched.  It can be modified to return the default 
// value as well.
int app_func_get_param_value (ENV_INFO *env_info, ::nsEDDEngine::OP_REF *operand_ref, nsConsumer::EVAL_VAR_VALUE *param_value)
{
	int rc = PC_FAIL;

	if ((env_info->handle_type == ENV_INFO::BlockHandle) || (operand_ref->block_instance > 0))
	{
		IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
		rc = pDDSSupport->DDS_GetParamValue( env_info->block_handle, env_info->value_spec, operand_ref, param_value );
	}
			
	return rc;
}
