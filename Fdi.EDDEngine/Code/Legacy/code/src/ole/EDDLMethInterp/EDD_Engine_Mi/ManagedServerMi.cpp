#include "stdafx.h"

#include "MethodInterfaceDefs.h"
#include "ManagedServerMi.h"

#include <ServerProjects/rtn_code.h>
#include "RTN_CODE.H"
#include "HARTsupport.h"


ManagedServerMi::ManagedServerMi(IManagedMiEngineSig * engine)
{
	m_pIManagedMiEngineSig = engine;

	m_bThreadCompleted = true;

	m_pThread =  NULL;

	server_sLanguageCode = engine->engine_sLanguageCode;
}


ManagedServerMi::~ManagedServerMi()
{
	if (m_pThread != NULL) {
		delete m_pThread;
	}

	//delete upper class instance
	if (m_pIManagedMiEngineSig != NULL) {
		delete m_pIManagedMiEngineSig;
	}
}


void ManagedServerMi::SetCancelled()
{
	m_pThread->SetCancel(true);
}


// IManagedServerMi methods
int ManagedServerMi::ExecuteMethod(long lMethodItemID, ProtocolType protocol)
{
	int nRetCode = SUCCESS;
	if (m_pThread == NULL)
	{
		m_pThread = new CProfibusDPMethodThread((IManagedServerMi*)this, 
			lMethodItemID, 
			protocol);		

		m_bThreadCompleted = false;

		m_pThread->CreateMethodExecutionThread();

	
	}

	return nRetCode;
}

void ManagedServerMi::SetMethodUIEvent()
{
	if (m_pThread != NULL)
	{
		CMethodInterpreterDevice* pDevice = NULL;
		// get device pointer
		pDevice = m_pThread->GetDevicePointer();

		if (pDevice != NULL)
		{			
			//The UICallback should call UnlockMethodEvent regardless of the results of convertEvalToVariant
			pDevice->UnlockMethodUIEvent();
		}
	}
}

int ManagedServerMi::serverGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen )
{
    int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineGetDictionaryString(sDictionaryKey, sValue, iStringLen);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverGetDictionaryString(): Exception Caught", nsConsumer::LogSeverity::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::CommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize)
{
    int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnCommCmdExecute(commandNumber, transactionNumber, pucCmdStatus, pchMoreDataInfo, iMoreDataSize);
	}
	catch (...)
	{
		MILog(L"Unable to perform CommCmdExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::ListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount)
{
    int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnListOpExecute(ulListId, iListIndex, ulEmbListId, iIndex, ulItemId, iCount);
	}
	catch (...)
	{
		MILog(L"Unable to perform ListCmdExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::AccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType)
{
    int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnAccessDynamicAttributeExecute(pOpRef, pParamValue, ulAttribute, eAccessType);
	}
	catch (...)
	{
		MILog(L"Unable to perform AccessDynamicAttributeExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::ParamSendExecute(const PARAM_REF *pOpRef, AccessType iAccessType)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnSendValueExecute(pOpRef, iAccessType);
	}
	catch (...)
	{
		MILog(L"Unable to perform ParamSendExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::serverAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineAccessAllDeviceValues(eAccessType);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverAccessAllDeviceValues(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::UIExecute(ACTION_UI_DATA& stUIData)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnUIExecute(stUIData);
	}
	catch (...)
	{
		MILog(L"Unable to perform UIExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::UIMethRspExecute(CValueVarient& vtRsp)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnUIMethRspExecute(vtRsp);
	}
	catch (...)
	{
		MILog(L"Unable to perform UIMethRspExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::RspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnRspCodeStrExecute(nCommandNumber, nResponseCode, pvtValue, protocol);
	}
	catch (...)
	{
		MILog(L"Unable to perform RspCodeStrExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::DictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnDictStringExecute(ulIndex, pString, iStringLen);
	}
	catch (...)
	{
		MILog(L"Unable to perform DictStringExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::LitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnLitStringExecute(ulIndex, pString, iStringLen);
	}
	catch (...)
	{
		MILog(L"Unable to perform LitStringExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


void ManagedServerMi::CommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity)
{
	try
	{
		m_pIManagedMiEngineSig->OnCommitValueExecute(onExitActivity);
	}
	catch (...)
	{
		MILog(L"Unable to perform CommitValueExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}
}


//This function turns item name to either item ID or member ID
int ManagedServerMi::ResolveNameToIDExecute( CStdString sItemName, unsigned long *memberID )
{
	int iRetVal = BLTIN_VAR_NOT_FOUND;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->ResolveItemNameToID( sItemName, memberID );
	}
	catch (...)
	{
		MILog(L"Unable to perform ResolveNameToIDExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::GetValueExecute2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, unsigned long ulMemberID, long lParamType, CValueVarient *pvtValue )
{
	int iRetVal = BLTIN_CANNOT_READ_VARIABLE;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->GetValueByItemID2( ulBlkId, ulBlkNum, ulItemID, ulMemberID, lParamType, pvtValue );
		if (m_pThread)
		{
			m_pThread->SetError(iRetVal);
		}			
	}
	catch (...)
	{
		MILog(L"Unable to perform GetValueExecute2(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}


int ManagedServerMi::serverGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput)
{
	int iRetVal = BLTIN_CANNOT_READ_VARIABLE;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineGetItemActionList(ulBlkId, ulBlkNum, ulItemID, pOutput);
		if (m_pThread)
		{
			m_pThread->SetError(iRetVal);
		}
	}
	catch (...)
	{
		MILog(L"Unable to perform serverGetItemActionList(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

void ManagedServerMi::MethodComplete(nsEDDEngine::Mth_ErrorCode retVal)
{
	m_pIManagedMiEngineSig->OnMethodComplete(retVal);
	m_bThreadCompleted = true;
}


int ManagedServerMi::ResponseCodeStringExecute(int iItemId, int iMemberId, int iRsponseCode, CValueVarient *pvtValue)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnResponseCodeStringExecute(iItemId, iMemberId, iRsponseCode, pvtValue);
	}
	catch (...)
	{
		MILog(L"Unable to perform ResponseCodeStringExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::DdsErrorExecute(wchar_t* pErrorString)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnDdsErrorExecute(pErrorString);
	}
	catch (...)
	{
		MILog(L"Unable to perform DdsErrorExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

unsigned long ManagedServerMi::CommErrorExecute()
{
	unsigned long iRetVal = (unsigned long)BLTIN_CANNOT_READ_VARIABLE;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnCommErrorExecute();
	}
	catch (...)
	{
		MILog(L"Unable to perform CommErrorExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::ResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnResponseCodeExecute(pulResponseCode, pulErrorItemId, pulErrorMemberId);
	}
	catch (...)
	{
		MILog(L"Unable to perform ResponseCodeExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::serverAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineAssign(pdst_opRef, psrc_opRef);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverAssign(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

ITEM_ID ManagedServerMi::BlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType)
{
	ITEM_ID ulRetVal = 0;

	try
	{
		ulRetVal = m_pIManagedMiEngineSig->OnBlockRefExecute(ulBlockItemId, iOccurrence, ulMemberId, eResolveType);
	}
	catch (...)
	{
		MILog(L"Unable to perform BlockRefExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (ulRetVal );
}

int ManagedServerMi::serverAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eValType, AccessType eAccessType, long lSize)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineAccessTypeValue2(pOpRef, value, eValType, eAccessType, lSize);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverAccessTypeValue2(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::serverAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineAccessActionVar(pAction, pActionVal, eAccessType);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverAccessActionVar(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::serverWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineWriteMethRtnVar(pReturnedVal);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverGetActionVarType(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::BlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount)
{


	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnBlockInstanceCountExecute(ulBlockId, pulCount);
	}
	catch (...)
	{
		MILog(L"Unable to perform BlockInstanceCountExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);

}

int ManagedServerMi::BlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnBlockInstanceByObjIndexExecute(ulObjectIndex, pulRelativeIndex);
	}
	catch (...)
	{
		MILog(L"Unable to perform OnBlockInstanceByObjIndexExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);

}

int ManagedServerMi::BlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex)
{

	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnBlockInstanceByBlockTagExecute(ulBlockId, pchBlockTag, pulRelativeIndex);
	}
	catch (...)
	{
		MILog(L"Unable to perform BlockInstanceByBlockTagExecute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);

}

int ManagedServerMi::GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->OnGetListElement2Execute(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, data);
	}
	catch (...)
	{
		MILog(L"Unable to perform GetListElement2Execute(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

void ManagedServerMi::serverLogMessage(int priorityVal, wchar_t *msg)
{
	m_pIManagedMiEngineSig->engineLogMessage(priorityVal, msg);
}

void ManagedServerMi::serverMethodDebugMessage(unsigned long lineNumber, const wchar_t* currentMethodDebugXml, const wchar_t** modifiedMethodDebugXml)
{
	m_pIManagedMiEngineSig->engineMethodDebugMessage(lineNumber, currentMethodDebugXml, modifiedMethodDebugXml);
}

int ManagedServerMi::serverIsOffline()
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineIsOffline();
	}
	catch (...)
	{
		MILog(L"Unable to perform serverIsOffline(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

long ManagedServerMi::serverSendCommand(unsigned long commandId, long *commandError)
{
	long iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineSendCommand(commandId, commandError);
	}
	catch (...)
	{
		MILog(L"Unable to perform engineSendCommand(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

int ManagedServerMi::serverGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		iRetVal = m_pIManagedMiEngineSig->engineGetEnumVarString(ulItemId, ulEnumValue, sEnumString);
	}
	catch (...)
	{
		MILog(L"Unable to perform serverGetEnumVarString(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (iRetVal);
}

void ManagedServerMi::MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename, UINT32 lineNumber)
{
	m_pIManagedMiEngineSig->MILog(sMessage, eLogSeverity, sCategory, cFilename, lineNumber);
}


IManagedServerMiSig* ManagedServerMiFactory::CreateManagedServerMi(IManagedMiEngineSig* engine)
{
	return new ManagedServerMi(engine);
}
