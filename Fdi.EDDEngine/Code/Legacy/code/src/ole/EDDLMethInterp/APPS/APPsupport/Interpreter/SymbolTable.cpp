// SymbolTable.cpp //

#include "stdafx.h"
#include "SymbolTable.h"
#include "VMConstants.h"


CSymbolTable::CSymbolTable()
{
//Vibhor 080705: Adding following calls for cleaning the things; defensive
	m_symbol_table.clear();	
	m_constant_pool_table.clear();
	m_nHighestScopeIndex=0;
	//m_sScope has its own constructor
}

CSymbolTable::~CSymbolTable()
{
	size_t i, nSize = m_symbol_table.size();// WS - 9apr07 - VS2005 checkin
	for( i=0;i<nSize;i++)
	{
		delete m_symbol_table[i];
	}
	m_symbol_table.clear();

	nSize = m_constant_pool_table.size();
	for(i=0;i<nSize;i++)
	{
		//Commeneted below by TSRPRASAD 09MAR2004 Fix the memory leaks
		//delete[] m_constant_pool_table[i]->pBytes;
		//m_constant_pool_table[i]->pBytes = 0;
		delete m_constant_pool_table[i];	//TSRPRASAD 09MAR2004 Fix the memory leaks

	}
	m_constant_pool_table.clear();
	m_nHighestScopeIndex=0;
	//m_sScope has its own constructor
}

//this function is used by global symbol table only
_INT32 CSymbolTable::Insert(CToken& token)
{
	_INT32 nIdx = GetIndex(token.GetLexeme());
	if(-1 == nIdx)
	{
		CVariable* pNewToken = new CVariable(&token);
		pNewToken->SetSymbolTableIndex((long)m_symbol_table.size());
		if (m_nHighestScopeIndex < 1)
		{
			m_nHighestScopeIndex = 1;	//constant because of global variable
			m_sScope.push(m_nHighestScopeIndex);
		}
		pNewToken->SetSymbolTableScopeIndex(m_sScope.top());
		m_symbol_table.push_back(pNewToken);

		return (_INT32)m_symbol_table.size() - 1;
	}
	else
	{
		//update operative item ID
		m_symbol_table[nIdx]->SetParamRef(token.GetParamRef());
	}
	return nIdx;
}

//This function is used by local symbol table
_INT32 CSymbolTable::InsertInScope(CToken& token, bool bCreateNewScope)
{
	_INT32 nIdx = -1;

	if (bCreateNewScope)
	{
		//update cureent scope index and scope stack
		m_nHighestScopeIndex += 1;
		m_sScope.push(m_nHighestScopeIndex);
	}
	else
	{
		//find one item matched name and scope index
		nIdx = GetIndexAtTopScope(token.GetLexeme());
	}

	if(nIdx == -1)
	{
		//not found
		CVariable* pNewToken = new CVariable(&token);
		pNewToken->SetSymbolTableIndex((long)m_symbol_table.size());
		pNewToken->SetSymbolTableScopeIndex(m_sScope.top());
		
		m_symbol_table.push_back(pNewToken);

		nIdx = (_INT32)m_symbol_table.size() - 1;
	}

	return nIdx;
}

_INT32 CSymbolTable::InsertConstant(CToken& token)
{
	CONSTANT_POOL_UTF8* pconst_entry=0;
	pconst_entry = new CONSTANT_POOL_UTF8;
	m_constant_pool_table.push_back(pconst_entry);
	pconst_entry->tag = CONSTANT_Utf8;

	size_t i16Count	 = strlen(token.GetLexeme());
	pconst_entry->length = _MSB_INT16(i16Count);
	pconst_entry->length <<= 8;
	pconst_entry->length |= _LSB_INT16(i16Count);
	pconst_entry->pBytes = new _UCHAR[pconst_entry->length + 1];
	memset(pconst_entry->pBytes,0,pconst_entry->length + 1);
	memcpy(pconst_entry->pBytes,token.GetLexeme(),pconst_entry->length);

	return (_INT32)m_constant_pool_table.size() - 1;
}

//The OID AID combination is 9 bytes ==> 5 OID + 4 AID
_INT32 CSymbolTable::InsertOMConstant(_UCHAR* pchOID_AID,_UCHAR uchType)
{
	CONSTANT_POOL_UTF8* pconst_entry=0;
	pconst_entry = new CONSTANT_POOL_UTF8;
	m_constant_pool_table.push_back(pconst_entry);
	pconst_entry->tag = uchType;

	pconst_entry->length = 9;
	pconst_entry->pBytes = new _UCHAR[pconst_entry->length + 1];
	memcpy(pconst_entry->pBytes,pchOID_AID,pconst_entry->length);

	return (_INT32)m_constant_pool_table.size() - 1;
}

_INT32 CSymbolTable::Delete(const _CHAR* pszTokenName)
{
	_INT32 index = -1;
	if(-1 != (index = GetIndex(pszTokenName)))
	{
		delete m_symbol_table[index];
		m_symbol_table.erase(m_symbol_table.begin() + index);
	}
	return index;
}

//This function looks for token name in symbol table which parts match scope stack
CVariable* CSymbolTable::Find(const _CHAR* pszTokenName)
{
	size_t nSize = m_symbol_table.size();
	CVariable* pToken = 0;

	//search the right token within scope stack
	SCOPE_STACK sTheScope = m_sScope;

	while(sTheScope.size() > 0)
	{
		for(size_t i=0; i<nSize; i++)
		{
			pToken = m_symbol_table[i];

			//Return values from method calling methods.  Arguments to methods calling methods.  Device Variables.
			if( pToken->m_bIsReturnToken || pToken->m_bIsRoutineToken || pToken->m_bIsGlobal )
			{
				if(!strcmp(pToken->GetLexeme(),pszTokenName))
				{
					return pToken;
				}
			}
			else if ( (!strcmp(pToken->GetLexeme(),pszTokenName)) && (pToken->GetSymbolTableScopeIndex() == sTheScope.top()) )
			{	
				return pToken;
			}
		}

		sTheScope.pop();
	}

	//not found
	return NULL;
}

//This function returns symbol at the given position
CVariable* CSymbolTable::GetAt(_INT32 nIdx)
{
	if(((_UINT32)nIdx < m_symbol_table.size()) && (nIdx >= 0))
		return m_symbol_table[nIdx];
	return 0;
}

//This function looks for global variable only
_INT32 CSymbolTable::GetIndex(const _CHAR* pszTokenName)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();
	for(_INT32 i = nSize-1; i >= 0; i--)
	{
		CToken* pToken = m_symbol_table[i];
		if(!strcmp(pToken->GetLexeme(),pszTokenName))
			return i;
	}

	return -1;
}

//This function looks for only local variable within the scope
_INT32 CSymbolTable::GetIndexInScope(const _CHAR* pszTokenName)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();

	//search the right symbol in SCOPE_STACK
	SCOPE_STACK sScope = m_sScope;

	while (sScope.size() > 0)
	{
		for(_INT32 i=0; i<nSize; i++)
		{
			CToken* pToken = m_symbol_table[i];
			if((!strcmp(pToken->GetLexeme(), pszTokenName)) && (pToken->GetSymbolTableScopeIndex() == sScope.top()))
			{
				return i;
			}
		}

		sScope.pop();
	}

	return -1;	//not found
}

//This function looks for only local variable at the tope of the scope
_INT32 CSymbolTable::GetIndexAtTopScope(const _CHAR* pszTokenName)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();

	for(_INT32 i=0; i<nSize; i++)
	{
		CToken* pToken = m_symbol_table[i];
		if((!strcmp(pToken->GetLexeme(), pszTokenName)) && (pToken->GetSymbolTableScopeIndex() == m_sScope.top()))
		{
			return i;
		}
	}

	return -1;	//not found
}

//This function prints method local variable names and values
//input parameter is allocated of memory here so that the caller is responsible for deleting the momery.
_INT32 CSymbolTable::TraceDump(_CHAR* &szDumpFile)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();
	INTER_VARIANT var;
	const size_t oneLoopSize = 3300;	//in case of string variable in 1024 bytes including '<', '>' and variable name
	const size_t initSize = 3*oneLoopSize;
	_CHAR *pTempString = NULL;
	size_t dumpSize = initSize;
	
	szDumpFile = new _CHAR[dumpSize];
	if (szDumpFile == NULL)
	{
		return 0;
	}

	PS_Strncpy(szDumpFile, dumpSize, "<\0", _TRUNCATE);
	PS_Strncat(szDumpFile, dumpSize, "SymbolTable\0", _TRUNCATE);
	PS_Strncat(szDumpFile, dumpSize, ">\0", _TRUNCATE);
	for(_INT32 i=0;i<nSize;i++)
	{
		//if ((dumpSize - strlen(szDumpFile)) < oneLoopSize)
		pTempString = szDumpFile;
		if ((dumpSize - strlen(pTempString)) < oneLoopSize)
		{
			//create a temp. memory
			pTempString = new _CHAR[dumpSize];
			//save old string
			PS_Strncpy(pTempString, dumpSize, szDumpFile, _TRUNCATE);
			//add more memory
			dumpSize += initSize;
			delete [] szDumpFile;
			szDumpFile = new _CHAR[dumpSize];
			//put old string back
			PS_Strncpy(szDumpFile, dumpSize, pTempString, _TRUNCATE);
			delete [] pTempString;
		}

		PS_Strncat(szDumpFile, dumpSize, "<\0", _TRUNCATE);
		PS_Strncat(szDumpFile, dumpSize, m_symbol_table[i]->GetLexeme(), _TRUNCATE);
		PS_Strncat(szDumpFile, dumpSize, ">\0", _TRUNCATE);
		m_symbol_table[i]->GetValue().XMLize(szDumpFile, dumpSize);
		PS_Strncat(szDumpFile, dumpSize, "</\0", _TRUNCATE);
		PS_Strncat(szDumpFile, dumpSize, m_symbol_table[i]->GetLexeme(), _TRUNCATE);
		PS_Strncat(szDumpFile, dumpSize, ">\0", _TRUNCATE);
	}
	PS_Strncat(szDumpFile, dumpSize, "</\0", _TRUNCATE);
	PS_Strncat(szDumpFile, dumpSize, "SymbolTable\0", _TRUNCATE);
	PS_Strncat(szDumpFile, dumpSize, ">\0", _TRUNCATE);

	return 1;

}

_INT32 CSymbolTable::DumpMethodDebugInfo(_CHAR* &szMethodDebugFile)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();
	INTER_VARIANT var;
	const size_t oneLoopSize = 3300;	//in case of string variable in 1024 bytes including '<', '>' and variable name
	const size_t initSize = 3 * oneLoopSize;
	_CHAR *pTempString = NULL;
	size_t dumpSize = initSize;

	szMethodDebugFile = new _CHAR[dumpSize];
	if (szMethodDebugFile == NULL)
	{
		return 0;
	}

	PS_Strncpy(szMethodDebugFile, dumpSize, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\0", _TRUNCATE);
	PS_Strncat(szMethodDebugFile, dumpSize, "<EddMethodDebugInfo SchemaVersion=\"1.0\" xmlns=\"http://fdi.org/2018/edd/method/debug/information\">\0", _TRUNCATE);
	PS_Strncat(szMethodDebugFile, dumpSize, "<LocalVariables>\0", _TRUNCATE);
	for (_INT32 i = 1; i<nSize; i++)
	{
		//if ((dumpSize - strlen(szDumpFile)) < oneLoopSize)
		pTempString = szMethodDebugFile;
		if ((dumpSize - strlen(pTempString)) < oneLoopSize)
		{
			//create a temp. memory
			pTempString = new _CHAR[dumpSize];
			//save old string
			PS_Strncpy(pTempString, dumpSize, szMethodDebugFile, _TRUNCATE);
			//add more memory
			dumpSize += initSize;
			delete[] szMethodDebugFile;
			szMethodDebugFile = new _CHAR[dumpSize];
			//put old string back
			PS_Strncpy(szMethodDebugFile, dumpSize, pTempString, _TRUNCATE);
			delete[] pTempString;
		}
		
		PS_Strncat(szMethodDebugFile, dumpSize, "<Variable name=\0", _TRUNCATE);
		

		_CHAR str[MAX_DD_STRING];
		memset(str, 0, MAX_DD_STRING);
		sprintf(str, "\"%hs\"", m_symbol_table[i]->GetLexeme());

		PS_Strncat(szMethodDebugFile, dumpSize, str, _TRUNCATE);
		

		m_symbol_table[i]->GetValue().XMLizeForMethodDebug(szMethodDebugFile, dumpSize, false);
		if(m_symbol_table[i]->GetType() == RUL_ARRAY_VARIABLE)
		{
			PS_Strncat(szMethodDebugFile, dumpSize, "</Variable>", _TRUNCATE);
		}
		else
		{
			//PS_Strncat(szMethodDebugFile, dumpSize, "\"/>\0", _TRUNCATE);
		}
		
		
	}
	PS_Strncat(szMethodDebugFile, dumpSize, "</\0", _TRUNCATE);
	PS_Strncat(szMethodDebugFile, dumpSize, "LocalVariables\0", _TRUNCATE);
	PS_Strncat(szMethodDebugFile, dumpSize, ">\0", _TRUNCATE);
	PS_Strncat(szMethodDebugFile, dumpSize, "</EddMethodDebugInfo>", _TRUNCATE);
	return 1;

}
//This function prints method local variable names and values
//input parameter is allocated of memory here so that the caller is responsible for deleting the momery.
/*_INT32 CSymbolTable::DumpMethodDebugInfo(wchar_t* &szDumpFile)
{
	_INT32 nSize = (_INT32)m_symbol_table.size();
	INTER_VARIANT var;
	const size_t oneLoopSize = 3300;	//in case of string variable in 1024 bytes including '<', '>' and variable name
	const size_t initSize = 3 * oneLoopSize;
	wchar_t *pTempString = NULL;
	size_t dumpSize = initSize;

	szDumpFile = new wchar_t[dumpSize];
	if (szDumpFile == NULL)
	{
		return 0;
	}

	wcsncpy_s(szDumpFile, dumpSize, L"<", _TRUNCATE);
	wcsncpy_s(szDumpFile, dumpSize, L"SymbolTable", _TRUNCATE);
	wcsncpy_s(szDumpFile, dumpSize, L">", _TRUNCATE);
	for (_INT32 i = 0; i<nSize; i++)
	{
		//if ((dumpSize - strlen(szDumpFile)) < oneLoopSize)
		pTempString = szDumpFile;
		if ((dumpSize - wcslen(pTempString)) < oneLoopSize)
		{
			//create a temp. memory
			pTempString = new wchar_t[dumpSize];
			//save old string
			wcsncpy_s(pTempString, dumpSize, szDumpFile, _TRUNCATE);
			//add more memory
			dumpSize += initSize;
			delete[] szDumpFile;
			szDumpFile = new wchar_t[dumpSize];
			//put old string back
			wcsncpy_s(szDumpFile, dumpSize, pTempString, _TRUNCATE);
			delete[] pTempString;
		}

		wcsncat_s(szDumpFile, dumpSize, L"<", _TRUNCATE);
		size_t convertedChars = 0;
		

			const _CHAR* Lex = m_symbol_table[i]->GetLexeme();
			pTempString = new wchar_t[strlen(Lex) + 1];

			mbstowcs_s(&convertedChars, pTempString, (strlen(Lex) + 1), Lex, (strlen(Lex) + 1));
		wcsncat_s(szDumpFile, dumpSize, pTempString, _TRUNCATE);
		wcsncat_s(szDumpFile, dumpSize, L">", _TRUNCATE);
		//m_symbol_table[i]->GetValue().XMLize((_CHAR*)szDumpFile, dumpSize);
		wcsncat_s(szDumpFile, dumpSize, L"</", _TRUNCATE);
		//wcsncat_s(szDumpFile, dumpSize, (wchar_t*)m_symbol_table[i]->GetLexeme(), _TRUNCATE);
		wcsncat_s(szDumpFile, dumpSize, L">", _TRUNCATE);
	}
	wcsncat_s(szDumpFile, dumpSize, L"</", _TRUNCATE);
	wcsncat_s(szDumpFile, dumpSize, L"SymbolTable", _TRUNCATE);
	wcsncat_s(szDumpFile, dumpSize, L">", _TRUNCATE);

	return 1;

}*/
//Anil Octobet 5 2005 for handling Method Calling Method
_INT32 CSymbolTable::GetSymbTableSize()
{
	return (_INT32)m_symbol_table.size();
}
