////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "InvokeReparamHelper.cpp"
//
// Author: Mike DeCoster
//
// Creation Date: 5-24-07
//
// Purpose: Helper class that helps build the params for an InvokeFunction.  This is used
// to redirect new functionality to old functionality
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "InvokeReparamHelper.h"
#include "FunctionExpression.h"
#include "ComplexDDExpression.h"
#include "ddbDevice.h"
#include "BuiltInMapper.h"
#include "string"
#include "MEE.h"


////////////////////////////////////////////////////////////////////////////////////////////////////
CInvokeReparamHelper::CInvokeReparamHelper(hCddbDevice* pDevice)
: m_pDevice(pDevice), m_uNumParams(0), m_enName(BUILTIN_UNDEFINED), m_pReturnValue(0), m_pBuiltInReturnCode(0), m_pFuncExp(0)
{
	;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CInvokeReparamHelper::~CInvokeReparamHelper()
{
	;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam(INTER_VARIANT& param)
{
	if (m_uNumParams >= MAX_NUMBER_OF_FUNCTION_PARAMETERS-1)
		return false;

	m_pVarParams[m_uNumParams++] = param;
	return true;
}

bool CInvokeReparamHelper::PushFuncExpression(FunctionExpression* pFuncExp)
{
	m_pFuncExp = pFuncExp;
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam()
{
	_INT32 iID = 0;
	INTER_VARIANT newParam;
	newParam = iID;
	return PushParam(newParam);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam_DeviceGlobal(FunctionExpression* pFuncExp, unsigned int uFuncParamNum)
{
	if (!pFuncExp || !m_pDevice)
		return false;
	CComplexDDExpression *pExp = (CComplexDDExpression*)pFuncExp->GetExpParameter(uFuncParamNum);
	if (!pExp)
		return false;
	CToken* pToken = pExp->GetToken();
	if (!pToken)
		return false;
	const char* szGlobalParam = pToken->GetLexeme();
	if (!szGlobalParam)
		return false;

	// convert from symbolic to global param ID
	string strGlobalParam = szGlobalParam;
	_INT32 iID;
	if (!isdigit(szGlobalParam[0]))
	{
		hCitemBase* pItemBase;
		if (m_pDevice->getItemBySymName(strGlobalParam,&pItemBase) == FAILURE)
		{//ItemArray caused issues at this point. 
			if(pToken->GetType() == RUL_DD_ITEM)
			{//go get the array number
				string sArrayName = (const char*)pToken->GetDDItemName();
				if(m_pDevice->getItemBySymName(sArrayName , &pItemBase) == FAILURE)
				{
					return false;
				}
				else
				{//strGlobalParam has the full name like itemArray[3]
					
					pItemBase = NULL;
					m_pDevice->ResolveExp(sArrayName.c_str(), strGlobalParam.c_str() , (unsigned long)strlen(strGlobalParam.c_str()), &pItemBase);
				}
			}
			else
			{
				return false;
			}
		}

			
		if (!pItemBase)
			return false;
		iID = (int)pItemBase->getID();
	}
	else
		iID = atoi(szGlobalParam);

	INTER_VARIANT newParam;
	newParam.operator =(iID);
	if (newParam.GetVarType() != RUL_INT)
		return false;

	return PushParam(newParam);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam_LocalVar(FunctionExpression* pFuncExp, unsigned int uFuncParamNum)
{
	if (!pFuncExp || !m_pDevice)
		return false;
	CComplexDDExpression *pExp = (CComplexDDExpression*)pFuncExp->GetExpParameter(uFuncParamNum);
	if (!pExp)
		return false;
	CToken* pToken = pExp->GetToken();
	if (!pToken)
		return false;
	const char* szVarName = pToken->GetLexeme();
	if (!szVarName)
		return false;

	INTER_VARIANT newParam;
	newParam.operator =((_CHAR*)szVarName);

	return PushParam(newParam);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam_DictionaryID(FunctionExpression* pFuncExp, unsigned int uFuncParamNum)
{

	return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::PushParam_Literal(const char* szConst)
{
	INTER_VARIANT newParam;
	newParam.operator =((_CHAR*)szConst);
	return PushParam(newParam);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CInvokeReparamHelper::ReInvoke(CBuiltIn* pBuiltIn)
{
	if (!pBuiltIn)
		return false;

	CBuiltInMapper::GetInstance()->LockBIMapper((m_pDevice->getMEEPointer())->GetStartedProtocol());
	CBuiltInInfo* pInfo = CBuiltInMapper::GetInstance()->Get(m_enName);
	CBuiltInMapper::GetInstance()->ReleaseBIMapper();

	if (!pInfo)
		return false;

	return pBuiltIn->InvokeFunction((char*)(pInfo->GetName().c_str()),
									(int)m_uNumParams,
									m_pVarParams,
									m_pReturnValue,
									m_pBuiltInReturnCode,
									m_pFuncExp);
}
