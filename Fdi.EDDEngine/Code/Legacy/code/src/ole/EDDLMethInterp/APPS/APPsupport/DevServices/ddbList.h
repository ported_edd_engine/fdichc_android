/*************************************************************************************************
 *
 * $Workfile: ddbList.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003 
 *		
 * #include "ddbList.h".
 */

#ifndef _DDB_LIST_H
#define _DDB_LIST_H
#ifdef INC_DEBUG
#pragma message("In ddbList.h") 
#endif

#include "ddbGeneral.h"
#include "foundation.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "ddbGrpItmDesc.h"
#include "ddbVar.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbList.h") 
#endif



class hClist : public hCitemBase 
{
	hCitemBase*		pTypeDef;  // cannot be conditional so just resolve it once
	unsigned long   capacityValue;
	ddbItemList_t activeIL;// the actual data - vector of itembase ptrs	-- the list is dynamic

protected:
	hCinteger* getPsuedoInt(int initialValue);// helper function

public:
	hClist(DevInfcHandle_t h, aCitemBase* paItemBase);
	hClist(hClist* pSrc, hClist* pVal, itemIdentity_t newID);
	RETURNCODE destroy(void){ activeIL.clear(); return hCitemBase::destroy(); };
	virtual ~hClist();
	
	hCattrBase*   newHCattr(aCattrBase* pACattr)  // builder 
	{LOGIT(CERR_LOG,"ERROR: hClist.newHCattr() called.\n");return NULL;};

	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual ) 
					{/* for now */returnedSize.xval=returnedSize.yval = 0; return 0;};

public:
	RETURNCODE Label(wstring& retStr) { return baseLabel(LIST_LABEL, retStr); };
	RETURNCODE Read(wstring &value){/* TODO: read the succker*/ return FAILURE;};
	unsigned long getCapacity(void){ return capacityValue; };
	itemID_t      getAttrID   (unsigned attr, int which=0);// 03aug06 the psuedoVariable's ID for refs
	hCVar*        getAttrPtr  (unsigned attr, int which=0);// 03aug06 

	unsigned int  setCount(unsigned  newCnt);// 03aug06 COUNT access - do not access any other
	unsigned int  getCount(void);            // 03aug06       way!!!

	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);//match index
	RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes);
	RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);

	hCitemBase* operator[](int idx0);// returns NULL on bounds error, cast to correct type.
	bool       isInGroup (UINT32 indexValue) {  return (indexValue >= 0 && indexValue < getCount()); };

	itemType_t    getIType(void);
	RETURNCODE getList(ddbItemList_t& r2diL);
};


//Other classes
class hCchart : public hCitemBase 
{
public:
	hCchart(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual ~hCchart()  {};

	unsigned long long getLength(void);
	RETURNCODE Label(wstring& retStr) { return baseLabel(CHART_LABEL, retStr); };
	itemType_t  getIType(void) { return iT_Chart; };
};


class hCgraph : public hCitemBase 
{
private:
	hCitemBase*		pXAxisIB;

public:
	hCgraph(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual ~hCgraph()  {};

	RETURNCODE Label(wstring& retStr) { return baseLabel(GRAPH_LABEL, retStr); };
	itemType_t  getIType(void) { return iT_Graph; };
	hCitemBase* getXAxis() { return pXAxisIB; };
};


class hCwaveform : public hCitemBase 
{
private:
	hCitemBase*		pYAxisIB;

public:
	hCwaveform(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual ~hCwaveform()  {};

	RETURNCODE Label(wstring& retStr) { return baseLabel(WAVEFORM_LABEL, retStr); };
	itemType_t  getIType(void) { return iT_Waveform; };
	hCitemBase* getYAxis() { return pYAxisIB; };
};


class hCsource : public hCitemBase 
{
private:
	hCitemBase*		pYAxisIB;

public:
	hCsource(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual ~hCsource()  {};

	RETURNCODE Label(wstring& retStr) { return baseLabel(SOURCE_LABEL, retStr); };
	itemType_t  getIType(void) { return iT_Source; };
	hCitemBase* getYAxis() { return pYAxisIB; };
};


#endif	// _DDB_LIST_H
