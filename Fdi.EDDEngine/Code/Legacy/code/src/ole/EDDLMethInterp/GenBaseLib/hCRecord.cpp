#include "stdafx.h"
#include <hCRecord.h>
#include <CMethodInterpreterDevice.h>
#include <3rdParty/panic.h>
#include "varientTag.h"


//
//
//
RETURNCODE hCRecord::getByIndex(UINT32 lMemberID, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		sMoniker.Format(_T("Member!$%lu!ItemID"), lMemberID );
		if( pDevice->ReadCollectionData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(lMemberID);
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			cerr << "ERROR: Array Item getByIndex failed to find index '0x" 
					 << hex << lMemberID << dec <<" elements."<<endl;
		}
		rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
//
//
RETURNCODE hCRecord::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;
	CStdString sName = indexName.c_str();

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		sMoniker.Format(_T("Member!%s!ItemID"), indexName.c_str() );
		if( pDevice->ReadParameterData2( &vtItemID, sMoniker, 0, 0, itemId, 0  ) == BLTIN_SUCCESS ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( vtItemID.vValue.varSymbolID, 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			
			CValueVarient vtMemberID{};
			sMoniker.Format(_T("Member!%s!MemberId"), indexName.c_str() );
			if( pDevice->ReadCollectionData( itemId, sMoniker, &vtMemberID ) ) 
			pGid->setIndex(vtMemberID.vValue.varSymbolID);
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			cerr << "ERROR: Array Item getByIndex failed to find index '0x" 
					 << hex << dec <<" elements."<<endl;
		}
		rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
// Get the Label of the record
//
RETURNCODE hCRecord::Label(wstring& retStr) 
{ 
	RETURNCODE  rc = SUCCESS;

	retStr.clear();
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtValue{};
		rc = pDevice->ReadParameterData2( &vtValue, _T("Label"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			retStr = CV_WSTR(&vtValue);
		}
	}
	return rc;
}
