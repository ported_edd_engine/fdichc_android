/*
 *	@(#) $Id: evl_resp.cpp,v 1.2 2012/09/24 23:54:37 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  RESPONSE_ITEM_CODE and RESPONSE_CODE.
 */

#include "stdafx.h"
#include "evl_loc.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif


/*********************************************************************
 *
 *	Name: ddl_free_resp_codes
 *	ShortDesc: Free the list of RESPONSE_CODEs
 *
 *	Description:
 *		ddl_free_resp_codes will check the RESPONSE_CODE_LIST pointer and
 *		the list, if the are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero
 *	Inputs:
 *		resp_codes: pointer to the RESPONSE_CODE_LIST structure
 *      dest_flag:
 *          FREE_ATTR - free the entire list
 *          CLEAN_ATTR - cleanup (ie. destroy) any malloc'd memory associated
 *              with each element of the list.  The base list is left intact.
 *
 *	Outputs:
 *		resp_codes: pointer to the list of RESPONSE_CODE_LIST with an empty
 *		list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_resp_codes(
RESPONSE_CODE_LIST *resp_codes,
uchar           dest_flag)
{

	int             i;	/* loop counter */
	RESPONSE_CODE  *temp_resp;	/* temp ptr to a list of resp codes */

	if (resp_codes == NULL) {
		return;
	}

	if (resp_codes->list == NULL) {
		ASSERT_DBG(!resp_codes->count && !resp_codes->limit);
		resp_codes->count = 0;
		resp_codes->limit = 0;
		return;
	}
	else {

		/*
		 * Free STRINGs for each RESPONSE_CODE
		 */

		temp_resp = resp_codes->list;
		for (i = 0; i < resp_codes->limit; i++, temp_resp++) {
			ddl_free_string(&temp_resp->help);
			ddl_free_string(&temp_resp->desc);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of RESPONSE_CODES
			 */

			free((void *) resp_codes->list);
			resp_codes->list = NULL;
			resp_codes->limit = 0;
		}
		else {

			/**
			 *	Initializing the list of resp_code values to 0.
			 *	It is OK to initialize "evaled","val","type","desc",and "help"
			 */

			memset((char *) resp_codes->list, 0,
				(size_t) resp_codes->limit * sizeof(*resp_codes->list));
		}
		resp_codes->count = 0;
	}
}


/***********************************************************************
 *
 *	Name: ddl_free_response_codes
 *	ShortDesc: frees the response code attribute
 *	Description:
 *		ddl_free_actions will free a list of ITEM_IDSs or DEFINITION
 *		depending upon what is stored.
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		ref_list:   points to the empty list
 *
 *	Returns: void
 *
 *	Author: Chris Gustafson
 *
 ************************************************************************/

void
ddl_free_response_codes(
RESPONSE_CODES	*pResponseCodes,
uchar			dest_flag)
{

	if (pResponseCodes == NULL) {
		return;
	}

	switch (pResponseCodes->type)
	{
	case nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_NONE:		// Just zero it out
	case nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_REF:
		pResponseCodes->response_codes.resp_code_ref = 0;
		break;

	case nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_LIST:
		ddl_free_resp_codes(&pResponseCodes->response_codes.resp_code_list, dest_flag);
		break;
	}
	
	pResponseCodes->type = nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_NONE;
}



/*********************************************************************
 *
 *	Name: ddl_shrink_resp_codes
 *	ShortDesc: Shrink the list of RESPONSE_CODEs
 *
 *	Description:
 *		ddl_shrink_resp_codes reallocs the list of RESPONSE_CODEs to contain
 *		only the RESPONSE_CODEs being used
 *
 *	Inputs:
 *		resp_codes: pointer to the RESPONSE_CODE_LIST structure
 *
 *	Outputs:
 *		resp_codes: pointer to the resized RESPONSE_CODE_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

int
ddl_shrink_resp_codes(
RESPONSE_CODE_LIST *resp_codes)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_RESP_CODES);
#endif

	if (resp_codes == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (resp_codes->list == NULL) {
		ASSERT_DBG(!resp_codes->count && !resp_codes->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (resp_codes->count == resp_codes->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * if count == 0, free the list; if count >= 0, shrink the list;
	 */

	if (resp_codes->count == 0) {
		ddl_free_resp_codes(resp_codes, FREE_ATTR);
	}
	else {
		resp_codes->limit = resp_codes->count;

		resp_codes->list = (RESPONSE_CODE *) realloc((void *) resp_codes->list,
			(size_t) (resp_codes->limit * sizeof(RESPONSE_CODE)));

		if (resp_codes->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_resp_codes
 *
 *	ShortDesc: Parse a list of RESPONSE_CODEs
 *
 *	Description:
 *		ddl_parse_resp_codes parses the binary data and loads a RESPONSE_CODE_LIST
 *		structure if the pointer to the RESPONSE_CODE_LIST is not NULL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		resp_codes - pointer to an RESPONSE_CODE_LIST structure where the result
 *			will be stored.  If this is NULL, no result is computed or stored.
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		resp_codes - pointer to an RESPONSE_CODE_LIST structure containing the
 *				result
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from DDL_PARSE_INTEGER(),ddl_parse_string()
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR, DDL_ENCODING_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/


static int
ddl_parse_resp_codes(
unsigned char **chunkp,
DDL_UINT       *size,
RESPONSE_CODE_LIST *resp_codes,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	DDL_UINT        temp_int;	/* temp storage for a parsed integer */
	DDL_UINT        len;	/* length of binary of the parsed */
	DDL_UINT        tag;	/* identifier of a binary chunk */
	RESPONSE_CODE  *temp_resp_code;	/* temp ptr to a list of resp codes */

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_RESP_CODES);
#endif

	while (*size > 0) {

		/*
		 * Parse the tag, and make sure this is a RESPONSE_CODE.
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != RESPONSE_CODE_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		if (resp_codes) {

			/*
			 * Parse a series of RESPONSE_CODEs.  If we need more
			 * room in the list of structures, malloc more room.
			 * Then parse the next RESPONSE_CODE structure.
			 */

			if (resp_codes->count >= resp_codes->limit) {

				resp_codes->limit += RESPONSE_CODE_INC;

				temp_resp_code = (RESPONSE_CODE *) realloc(
					(void *) resp_codes->list,
					(size_t) resp_codes->limit * sizeof(RESPONSE_CODE));

				if (!temp_resp_code) {
					resp_codes->limit = resp_codes->count;
					ddl_free_resp_codes(resp_codes, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_resp_code[resp_codes->count], 0,
					(size_t) (RESPONSE_CODE_INC * sizeof(RESPONSE_CODE)));

				resp_codes->list = temp_resp_code;
			}
			temp_resp_code = resp_codes->list + resp_codes->count++;

			/*
			 * Parse the reponse code value, type, description and
			 * help (if present).
			 */

			DDL_PARSE_INTEGER(chunkp, &len, &temp_int);
			temp_resp_code->val = (unsigned short) temp_int;

			//DDL_PARSE_INTEGER(chunkp, &len, &temp_int);
			if(len>0)
			{
				temp_resp_code->type = (unsigned short)(*chunkp)[0];
				len--;
				(*chunkp)++;
			}
			else
				return DDL_ENCODING_ERROR;

			rc = ddl_parse_string(chunkp, &len, &temp_resp_code->desc,
				depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				return rc;
			}
			temp_resp_code->evaled |= RS_TYPE_EVALED | RS_VAL_EVALED | RS_DESC_EVALED;

			if (len) {
				rc = ddl_parse_string(chunkp, &len, &temp_resp_code->help,
					depinfo, env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_resp_code->evaled |= RS_HELP_EVALED;
			}
			else {

				/*
				 * If a HELP string was not found in the
				 * binary, use the default HELP string from the
				 * standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_HELP,
					&temp_resp_code->help);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_resp_code->help);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}

		}
		else {
			DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);

			DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);

			rc = ddl_parse_string(chunkp, &len, (STRING *) NULL, depinfo,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (len) {
				rc = ddl_parse_string(chunkp, &len, (STRING *) NULL, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}

		/*
		 * If there are any new fields, just ignore them.
		 */

		if (len) {
			*chunkp += len;
		}
	}

	return DDL_SUCCESS;
}





/*********************************************************************
 *
 *	Name: ddl_rspcodes_choice
 *	ShortDesc: Choose the correct list of response codes from a binary.
 *
 *	Description:
 *		ddl_rspcodes_choice will parse the binary for a list of response codes,
 *		according to the current conditionals (if any).  A list of
 *		the response codes is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		rspcodes - pointer to a RESPONSE_CODE_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		rspcodes - pointer to a RESPONSE_CODE_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_rspcodes().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_rspcodes_choice(
unsigned char **chunkp,
DDL_UINT       *size,
RESPONSE_CODE_LIST *rspcodes,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to a an element of the
					 * chunk list */
	DDL_UINT        len;	/* length of binary of the parsed */
	DDL_UINT        tag;	/* identifier of a binary chunk */

#ifdef DDSTEST
	TEST_FAIL(DDL_RSPCODES_CHOICE);
#endif


	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	switch(tag)
	{
	case 1:
		{
			ASSERT(0);
			OP_REF_TRAIL ort = {STANDARD_TYPE,0};
			int valid;	/* indicates the validity of the data */

			rc=ddl_op_ref_trail_choice(chunkp, size, &ort, depinfo, &valid,	env_info, var_needed);

			if(rc)
			{
				::ddl_free_op_ref_trail(&ort);
				return rc;
			}

			// Not finished - check out
			rspcodes->ref_id = ort.op_info.id;
			return DDL_SUCCESS;
		}
		break;

	case 2:
		break;

	default:
		return DDL_ENCODING_ERROR;
		break;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);
	
	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		RESPONSE_CODES_SEQLIST_TAG, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (rspcodes && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (rspcodes == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_resp_codes(&(chunk_ptr->chunk), &(chunk_ptr->size),
				rspcodes, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (rspcodes && data_valid) {
			*data_valid = TRUE;	/* rspcodes has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;

}

/********************************************************************
*
* Name: eval_attr_resp_codes
* ShortDesc: evaluate a response codes attribute
*
* Description:
*
*	The eval_attr_resp_codes function evaluates a response codes attribute.
*	The buffer pointed to by chunk should contain a response codes
*	attribute binary, and size should specify the size of the binary.
*	If resp_codes is not a null pointer, the response codes are
*	returned in resp_codes. If depinfo is not a null pointer, dependency
*	information about the response codes is returned in depinfo.
*
*	Inputs:
*		chunkp - pointer to the address of the binary
*		size - pointer to the size of the binary
*		resp_codes - pointer to a RESPONSE_CODE_LIST structure where the
*				result will be stored.  If this is NULL, no result is
*				computed or stored.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*				information will be stored.  If this is NULL, no
*				dependency information will be stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
*	Outputs:
*		chunkp - pointer to the address following this binary
*		size - pointer to the size of the binary following this one
*		resp_codes - pointer to a RESPONSE_CODE_LIST structure containing
*				the result
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*				dependency information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*	DDL_SUCCESS
*	DDL_MEMORY_ERROR
*	DDL_ENCODING_ERROR
*	return codes from:
*		ddl_rspcodes_choice()
*		ddl_parse_tag_func()
*		ddl_parse_integer_func()
*		ddl_parse_string()
*		ddl_shrink_depinfo()
*		ddl_free_depinfo()
*		ddl_shrink_resp_codes()
*		ddl_free_resp_codes()
*
* Author: Steve Beyerl
**********************************************************************/

int
eval_attr_resp_codes(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	RESPONSE_CODE_LIST* resp_codes = static_cast<RESPONSE_CODE_LIST*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* data valid flag */

	/* resp code list */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_RESP_CODES);
#endif

	valid = 0;

	ddl_free_resp_codes(resp_codes, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	/*
	 * Parse the response codes.
	 */

	rc = ddl_rspcodes_choice(&chunk, &size, resp_codes,
		depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_resp_codes(resp_codes);
	}

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (resp_codes && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	ddl_free_resp_codes(resp_codes, FREE_ATTR);
	return rc;
}

/********************************************************************
*
* Name: eval_response_codes
* ShortDesc: evaluate a response codes attribute
*
* Description:
*
*	The eval_response_codes function evaluates a response codes attribute.
*	The buffer pointed to by chunk should contain a response codes
*	attribute binary, and size should specify the size of the binary.
*	If resp_codes is not a null pointer, the response codes are
*	returned in resp_codes. If depinfo is not a null pointer, dependency
*	information about the response codes is returned in depinfo.
*
*	Inputs:
*		chunkp - pointer to the address of the binary
*		size - pointer to the size of the binary
*		response_codes - pointer to a RESPONSE_CODES structure where the
*				result will be stored.  If this is NULL, no result is
*				computed or stored.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*				information will be stored.  If this is NULL, no
*				dependency information will be stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
*	Outputs:
*		chunkp - pointer to the address following this binary
*		size - pointer to the size of the binary following this one
*		resp_codes - pointer to a RESPONSE_CODE_LIST structure containing
*				the result
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*				dependency information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*	DDL_SUCCESS
*	DDL_MEMORY_ERROR
*	DDL_ENCODING_ERROR

**********************************************************************/
int
eval_response_codes(
unsigned char **chunkp,
DDL_UINT       *size,
RESPONSE_CODES *response_codes,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int rc = DDL_SUCCESS;

	// Determine what is stored here

	rc = ddl_rspcodes_choice(chunkp, size, &response_codes->response_codes.resp_code_list,
		depinfo, data_valid, env_info, var_needed);

	if (rc == DDL_SUCCESS)
	{
		response_codes->type = nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_LIST;
		rc = ddl_shrink_resp_codes(&response_codes->response_codes.resp_code_list);
	}


	return rc;
}

/********************************************************************
*
* Name: eval_attr_response_codes
* ShortDesc: evaluate a response codes attribute
*
* Description:
*
*	The eval_response_codes function evaluates a response codes attribute.
*	The buffer pointed to by chunk should contain a response codes
*	attribute binary, and size should specify the size of the binary.
*	If resp_codes is not a null pointer, the response codes are
*	returned in resp_codes. If depinfo is not a null pointer, dependency
*	information about the response codes is returned in depinfo.
*
*	Inputs:
*		chunkp - pointer to the address of the binary
*		size - pointer to the size of the binary
*		voidP - pointer to a RESPONSE_CODES structure where the
*				result will be stored.  If this is NULL, no result is
*				computed or stored.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*				information will be stored.  If this is NULL, no
*				dependency information will be stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
*	Outputs:
*		chunkp - pointer to the address following this binary
*		size - pointer to the size of the binary following this one
*		resp_codes - pointer to a RESPONSE_CODE_LIST structure containing
*				the result
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*				dependency information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*	DDL_SUCCESS
*	DDL_MEMORY_ERROR
*	DDL_ENCODING_ERROR

**********************************************************************/
int
eval_attr_response_codes(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	RESPONSE_CODES* response_codes = static_cast<RESPONSE_CODES*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* data valid flag */

	/* resp code list */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_RESP_CODES);
#endif

	valid = 0;

	ddl_free_response_codes(response_codes, FREE_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = eval_response_codes( &chunk, &size, response_codes,
			depinfo, &valid, env_info, var_needed );

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (response_codes && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}

