#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"
#include "TestSupport/MethodHelper.h"
#include <nsEDDEngine/ProtocolType.h>

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        namespace Methods
        {
#pragma region test setup

            public ref class When_executing_a_action abstract
            {
            protected:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static nsEDDEngine::Mth_ErrorCode methodResult;

                static void ExecuteMethod(String^ testCaseName, String^ methodName)
                {
                    engine = gcnew EddEngineController(testCaseName);
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    engine->Initialize(nullptr, builtinSupportSimulator);                    

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, methodName);

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;
                    wchar_t* languageCode = L"en";
					ACTION methodAction; 
					methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
					methodAction.action.meth_definition.size = wcslen(test_method->def.data);
					methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, pValueSpec, &methodAction, pCallback, asyncState,languageCode);

                    methodResult = engine->Instance->EndMethod(&asyncRunningMethod);

					//this is not methodAction's data, please don't delete it
					methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupTestSetup()
                {
                    delete builtinSupportSimulator;
                    delete engine;
                }
            };

#pragma endregion

#pragma region send_command

            [TestClass]
            public ref class When_executing_a_action_with_send_command_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Send_Read_Command_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_201_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(201, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_send_command_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Send_Read_Command_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_201_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(201, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

#pragma endregion

#pragma region send_command_trans

            [TestClass]
            public ref class When_executing_a_action_with_send_command_trans_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Send_Read_Command_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_203_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(203, commandNumber);
                    Assert::AreEqual(1, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_send_command_trans_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Send_Read_Command_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_203_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(203, commandNumber);
                    Assert::AreEqual(1, transactionNumber);
                }
            };

#pragma endregion

#pragma region send

            [TestClass]
            public ref class When_executing_a_action_with_send_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Send_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_205_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(205, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_send_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Send_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_205_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(205, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

#pragma endregion

#pragma region send_trans

            [TestClass]
            public ref class When_executing_a_action_with_send_trans_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Send_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_204_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(204, commandNumber);
                    Assert::AreEqual(1, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_send_trans_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Send_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_204_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(204, commandNumber);
                    Assert::AreEqual(1, transactionNumber);
                }
            };

#pragma endregion

#pragma region ext_send_command

            [TestClass]
            public ref class When_executing_a_action_with_ext_send_command_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Ext_Send_Command_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_205_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(205, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_ext_send_command_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Ext_Send_Command_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_205_and_transaction_0_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(205, commandNumber);
                    Assert::AreEqual(0, transactionNumber);
                }
            };

#pragma endregion

#pragma region ext_send_command_trans

            [TestClass]
            public ref class When_executing_a_action_with_ext_send_command_trans_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Ext_Send_Command_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_send_trans_with_command_204_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(204, commandNumber);

                    Assert::AreEqual(1, transactionNumber);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_ext_send_command_trans_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Ext_Send_Command_Trans_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_send_trans_with_command_204_and_transaction_1_once()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");
                    int commandNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.cmd_num")));
                    int transactionNumber = *((int*) (builtinSupportSimulator->Recorder->GetParameter(L"SendCmdTrans.trans")));

                    Assert::AreEqual(1, callCount);
                    Assert::AreEqual(204, commandNumber);

                    Assert::AreEqual(1, transactionNumber);
                }
            };

#pragma endregion

#pragma region send

            [TestClass]
            public ref class When_executing_a_action_with_get_more_status_in_it_in_fm8 : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fm8", "Get_More_Status_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_never_call_the_send_cmd_trans()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");

                    Assert::AreEqual(0, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest"), TestCategory("fm8")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }
            };

            [TestClass]
            public ref class When_executing_a_action_with_get_more_status_in_it_in_fmA : When_executing_a_action
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    ExecuteMethod("Hart\\TestCase001A.fmA", "Get_More_Status_Method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupTestSetup();
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_return_true_as_method_result()
                {
                    Assert::IsTrue(methodResult == METH_SUCCESS);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_never_call_the_send_cmd_trans()
                {
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"SendCmdTrans");

                    Assert::AreEqual(0, callCount);
                }

                [TestMethod, TestCategory("CommunicationMethodsTest")]
                void It_should_call_the_get_more_status_once()
                {
                    // EDD Engine uses the status bytes for abort/fail_on_response_code() etc.
                    int callCount = builtinSupportSimulator->Recorder->GetCallCount(L"GetMoreStatus");

                    Assert::AreEqual(1, callCount);
                }
            };

#pragma endregion

        } // namespace Methods
    } // namespace Hart
} // namespace ComponentTests
