/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *      @(#)pc_loc.h	30.3  30  14 Nov 1996
 */

#ifndef PC_LOC_H
#define PC_LOC_H

#include "pc_lib.h"

/*
 *	PC Structures and Defines
 *
 * Note: Three value "buckets" and a special storage area exists to hold values:
 *		Device - Holds the scaled device value (sync'ed with device)
 *		Meth   - Holds the temporary method changed value
 *		Edit   - Holds the temporary user/appl. changed value (prior to write)
 *		Action_value - Holds a temporary un-scaled value (supports builtins)
 */

/*
 * Flag values for pc_get_param_info, info_type
 */

#define PC_POST_READ	0x0001
#define PC_PRE_WRITE	0x0002

/*
 *	Flag values
 */

#define PC_RECORD					0x00000001
#define PC_ARRAY					0x00000002
#define PC_VAL_NOT_READ				0x00000004
#define PC_DYNAMIC_VAR				0x00000008
#define PC_METH_NOT_READ			0x00000010
#define PC_POSTREAD_DYN				0x00000020
#define PC_PREWRITE_DYN				0x00000040
#define PC_LABEL_NOT_READ			0x00000080
#define PC_LABEL_DYN				0x00000100
#define PC_DISP_FMT_NOT_READ		0x00000200
#define PC_DISP_FMT_DYN				0x00000400
#define PC_EDIT_FMT_NOT_READ		0x00000800
#define PC_EDIT_FMT_DYN				0x00001000
#define PC_ARRAY_ID_NOT_READ		0x00002000
#define PC_PARAM_LOCKED_FOR_COMM  	0x00004000
#define PC_SUBPARAM_LOCKED_FOR_COMM	0x00008000

#define PC_HEAD_ELEMENT		(PC_ARRAY|PC_RECORD)
#define PC_FLAG_INIT		(PC_VAL_NOT_READ|PC_METH_NOT_READ|\
								PC_LABEL_NOT_READ|PC_DISP_FMT_NOT_READ|\
								PC_EDIT_FMT_NOT_READ|\
								PC_ARRAY_ID_NOT_READ)
#define PC_DYN_FLAG_INIT	(PC_FLAG_INIT|PC_DYNAMIC_VAR)
#define PC_ENTRY_LOCKED		(PC_PARAM_LOCKED_FOR_COMM|\
								PC_SUBPARAM_LOCKED_FOR_COMM)


typedef struct {
	unsigned short		str_len;	/* Size of string in bytes: not nec. = pc_size */
	char				*str_ptr;
} PC_STRING;

typedef struct pc_value_s {
	union {
		float			pc_float;
		double			pc_double;
		unsigned long	pc_ulong;
		long			pc_long;
        char			pc_chararray[8];
		PC_STRING		pc_string;
	} pc_value;
	/*
	 * We really only need one tag - decide later which to delete or
	 * rename.
	 */
	unsigned short		pc_method_tag;
	unsigned short		pc_appl_tag;
	unsigned short		pc_value_changed;
} PC_VALUE;

typedef struct {
	ITEM_ID				pc_rel_id;
	SUBINDEX			pc_rel_subindex;
	struct pc_elem		*pc_rel_pointer;
} PC_RELATION_ELEM;

typedef struct {
	int					pc_rel_count;
	PC_RELATION_ELEM	*pc_rel_list;
} PC_RELATION;

typedef struct {
	int					pc_action_count;
	ITEM_ID				*pc_action_list;
} PC_ACTION;

typedef struct pc_elem {
	ITEM_ID			pc_id;			/* The DDID of the variable, etc. */
	ITEM_ID			pc_member_id;	/* The MEMBER NAME of the variable */
	ITEM_TYPE		pc_type;		/* Type of variable or array element */
	unsigned short	pc_size;		/* Size of variable in elements or array entries */
	unsigned long	pc_class;		/* Class of variable or array elemenet */
	SUBINDEX		pc_subindex;	/* Member index in record or array */
	SUBINDEX		pc_subsubindex;	/* Member index of a 2nd record or array */
	int				pc_po;			/* The param_offset of the variable */
	unsigned long	pc_flags;
	PARAM_FLAG		pc_appl_flags;	/* Appl. set/tested/cleared only */
	ITEM_ID			pc_array_id;	/* ID of associated array (INDEX only*/
	wchar_t			*pc_label;		/* Variable label*/
	wchar_t			*pc_disp_fmt;	/* Variable display format*/
	wchar_t			*pc_edit_fmt;	/* Variable edit format */
	PC_ACTION		pc_postread;	/* Post-read methods */
	PC_ACTION		pc_prewrite;	/* Pre-write methods */
	PC_RELATION		pc_relation;	/* Relations from this entry */
	PC_VALUE		pc_dev_value;	/* The value at the device */
	PC_VALUE		pc_meth_value;	/* The value inside the method */
	PC_VALUE		pc_edit_value;	/* The value inside the method */
} PC_ELEM;

typedef struct {
	ITEM_ID			pc_id;				/* key field */
	int				pc_po;

} PC_ID_XREF;	

typedef struct {
	int				pc_po;
	PC_ELEM			*pc_elemp;
} PC_PO_XREF;	

typedef struct {

	int				pc_id_xref_count;
	PC_ID_XREF		*pc_id_xref;
	int				pc_po_xref_count;
	PC_PO_XREF		*pc_po_xref;
	int				pc_count;
	PC_ELEM			*pc_list;
} PC;

/*
 *	PC_method_close option values
 */

#define DISCARD_VALUES	1
#define SAVE_VALUES		2
#define SEND_VALUES		4
#define NO_VALUES		8

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*
 *	PC access routines (pc_lib.c)
 */

extern int pc_get_action_value P((ENV_INFO *env_info, EVAL_VAR_VALUE *evv));
extern int pc_put_action_value P((ENV_INFO *env_info, EVAL_VAR_VALUE *evv));

extern int pc_method_close P((ENV_INFO *env_info, int option));

extern int pc_get_param_array_id P((ENV_INFO *env_info, P_REF *pc_pr,
					ITEM_ID *item_idp));
extern int pc_get_param_disp_format P((ENV_INFO *env_info, P_REF *pc_pr,
					char *format_buf, int max_len));
extern int pc_get_param_edit_format P((ENV_INFO *env_info, P_REF *pc_pr,
					char *format_buf, int max_len));
extern int pc_get_param_label P((ENV_INFO *env_info, P_REF *pc_pr,
					char *label_buf, int max_len));

extern int pc_get_action_list P((ENV_INFO *env_info, P_REF *pr, 
				unsigned long req_type, PC_ACTION **actionpp));

extern int pc_conv_pc_p_ref_to_ip_ref P((BLOCK_HANDLE block_handle, 
				P_REF *pc_p_ref, P_REF *ip_ref));
extern int pc_conv_p_ref_to_ip_ref P((BLOCK_HANDLE block_handle, 
				P_REF *p_ref, P_REF *ip_ref));
extern int pc_free_action_storage P((ENV_INFO *env_info, P_REF *ipr));
extern int pc_get_action_storage P((ENV_INFO *env_info, P_REF *ipr, 
				int *param_count));
extern int pc_put_action_storage P((ENV_INFO *env_info, P_REF *ipr,
				int replicate_device_values));


/*
 * NOTE:
 * pc_put_param_value_list or pc_release_param_value_list must be called
 * after pc_get_param_value_list has been called.
 */

extern int pc_release_param_value_list P((ENV_INFO *env_info, 
			P_REF *p_ref));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef DEBUG
#define PC_CHECK(bh, n)  pc_check (bh, n)
extern void pc_check P((BLOCK_HANDLE bh, int printit));
#else
#define PC_CHECK(bh, n)
#endif /* DEBUG */

#endif  /*PC_LOC_H*/
