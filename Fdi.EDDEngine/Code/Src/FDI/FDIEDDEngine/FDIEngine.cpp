#include "stdafx.h"

#include "FDIEngine.h"
#include "FDIEngineFactory.h"

#include "DeviceTypeMgr.h"
#include "nsEDDEngine/FFBlockInfo.h"


extern int
eval_item_any(
::nsEDDEngine::ItemBase *var,
::nsEDDEngine::AttributeNameSet   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors);
#define MAX_SIZE 128

CFDIEngine::CFDIEngine( const wchar_t *szFilename, const wchar_t* /* pConfigXML */, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
	: CEDDEngineImpl( szFilename, pIEDDEngineLogger, nsEDDEngine::FDI_DDS, pErrorCode )
{

	//Locks the shared data in device type manager
	CLockDeviceTypeMgr dtMgrLock;

	// Class member initialization
	m_block_handle = -1;
	// m_mapBlockInst has own constructor
	m_deviceHandle = -1;
	m_deviceTypeHandle = -1;
	memset(&m_header, 0, sizeof(m_header));
	memset(&m_desc, 0, sizeof(m_desc));
	m_pflat_device_dir = nullptr;
	// m_connectionMgr has own constructor

	m_connectionMgr.cm_init(CMM_OFF_LINE);

	int rc = CM_SUCCESS;

	// we should cache the DDOD_HEADER but there isn't a clean way to get at it, the only
	// method available is by specifying a FILE pointer
	DDOD_HEADER	header = {0};
	FILE * file = NULL;

	file = PS_wfopen(szFilename, L"rb");
	if (!file) 
	{
		*pErrorCode = nsEDDEngine::BAD_FILE_OPEN_ERROR;
		return;
	}

	rc = ds_get_header(file, const_cast<wchar_t*>(szFilename), &header);


	if (rc != CM_SUCCESS)
	{
		*pErrorCode = nsEDDEngine::BAD_HEADER_FILE_ERROR;
		return;
	}
	// enumerations are the same so we can just cast this
	SetProtocol((nsEDDEngine::ProtocolType) header.edd_profile);

	if ((m_Protocol != nsEDDEngine::FF) && (m_Protocol != nsEDDEngine::ISA100))
	{
		m_mapBlockInst[0] = 0;	// For HART & PB, Initialize BlockInstance = Block_Handle = 0
	}

	if (rc == CM_SUCCESS)
	{
		CreateDevice(header);

		ENV_INFO env_info;
		Init_ENV_INFO( &env_info, nullptr, nsEDDEngine::DeviceLevel_BI, L"" );
		ROD_HANDLE rod_handle = 0;

		// load the dd. this call will build the flat_device_dir
                		rc = m_connectionMgr.ds_dd_device_load(&env_info, m_deviceHandle, &rod_handle);

		if( rc != SUCCESS )
		{
			*pErrorCode = nsEDDEngine::LOAD_DD_AND_BLOCK_OPEN_ERROR;;
			return;
		}

		if( rc == SUCCESS )
		{
			m_pflat_device_dir=this->GetFlatDeviceDir();
			if( m_pflat_device_dir )
			{
				m_pflat_device_dir->image_loc.header_size = header.header_size;
				m_pflat_device_dir->image_loc.meta_data_size = header.meta_data_size;
				m_pflat_device_dir->image_loc.item_objects_size = header.item_objects_size;
                PS_Wcscpy( m_pflat_device_dir->image_loc.filepath, wcslen(szFilename)+1, (LPCTSTR)szFilename );

				if(( m_Protocol != nsEDDEngine::FF ) && (m_Protocol != nsEDDEngine::ISA100))	// Only build a block for HART and Profi
				{
					BLOCK_HANDLE bh;
					rc = m_connectionMgr.CreateBlockEntry( &bh, m_deviceHandle );

					if( rc != SUCCESS )
					{
						*pErrorCode = nsEDDEngine::CREATE_BLOCK_ERROR;
						return;
					}

					if( rc == SUCCESS )
					{
						m_block_handle = bh;		// Set m_block_handle for this single block device
						m_mapBlockInst[0] = bh;

						m_connectionMgr.SET_ABT_DD_BLK_ID( bh, m_pflat_device_dir->blk_tbl.list[0].blk_id );

						rc = m_connectionMgr.ds_dd_block_load( &env_info, bh );

						if( rc != SUCCESS)
						{
							*pErrorCode = nsEDDEngine::LOAD_DD_AND_BLOCK_OPEN_ERROR;
							return;
						}
					}
				}
			}
			rc = m_connectionMgr.ds_get_ddef_desc(&env_info, file, &m_desc);
		}
	}
	fclose(file);
}


CFDIEngine::~CFDIEngine()
{
	//Locks the shared data in device type manager
	CLockDeviceTypeMgr dtMgrLock;

	m_connectionMgr.CleanTables(m_deviceHandle);
}


int CFDIEngine::GetBlockCount()
{
	if(this->m_pflat_device_dir)
				return m_pflat_device_dir->blk_tbl.count;

	return 0;
}

void CFDIEngine::CreateDevice( DDOD_HEADER header )
{
	m_header = header;

	m_connectionMgr.CreateActiveDeviceTableEntry(&m_deviceHandle);
	m_connectionMgr.SetActiveDeviceTableData(&m_deviceTypeHandle, m_deviceHandle, m_header);

}

nsEDDEngine::FLAT_DEVICE_DIR * CFDIEngine::GetFlatDeviceDir()
{
	return (nsEDDEngine::FLAT_DEVICE_DIR *)g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(m_deviceTypeHandle);
}


BLOCK_HANDLE CFDIEngine::BlockInstToBlockHandle(nsEDDEngine::BLOCK_INSTANCE iBlockInstance)
{
	BLOCK_HANDLE bh = -1;

	std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>::const_iterator cIter;

	cIter = m_mapBlockInst.find(iBlockInstance);

	if (cIter == m_mapBlockInst.cend())
	{
		assert(cIter != m_mapBlockInst.cend());	// MHD decide what to do here on error
	}
	else
	{
		bh = cIter->second;
	}

	return bh;
}


int CFDIEngine::BlockHandleToBlockInst(BLOCK_HANDLE bh, nsEDDEngine::BLOCK_INSTANCE *block_instance)
{
	int rc = DDS_SUCCESS;
	bool bFound = false;
	std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>::const_iterator cIter;

	for (cIter = m_mapBlockInst.cbegin(); cIter != m_mapBlockInst.cend(); cIter++)
	{
		if (cIter->second == bh)
		{
			*block_instance = cIter->first;
			bFound = true;
			break;
		}
	}
	if (!bFound)
	{
		rc = CM_BLOCK_NOT_FOUND;
	}

	return rc;
}


// Unit Convenience Functions
int CFDIEngine::GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID * pUnitItemId)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, pValueSpec, iBlockInstance, L"" );
	
	
	int rc = ddi_find_unit_ids( &env_info, &block_spec, AxisItemId, pUnitItemId, NULL );
	return rc;
}

int CFDIEngine::GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID * pUnitItemId)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, pValueSpec, iBlockInstance, L"" );

	ITEM_ID unit_item_id = {0};

	int rc = ddi_get_unit( &env_info, &block_spec, pParamSpec, &unit_item_id );
	if( rc == DDS_SUCCESS )
	{
		*pUnitItemId = unit_item_id;
	}
	return rc;
}


// Command Convenience Functions
int CFDIEngine::GetCommandList(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, nsEDDEngine::CommandType eCmdType, nsEDDEngine::FDI_COMMAND_LIST * pCommandList)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );
	
	int rc = ddi_get_ptoc( &env_info, &block_spec, pParamSpec, eCmdType, pCommandList );

	//If request is failed for individual List/Array element i.e. with type FDI_PS_ITEM_ID_SI
	// try with whole List/Array i.e. with param spec type FDI_ITEM_ID and subindex = 0.
	if(rc != DDS_SUCCESS && pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI)
	{
        nsEDDEngine::FDI_PARAM_SPECIFIER newParamSpec(*pParamSpec);

        newParamSpec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
        newParamSpec.subindex = 0;

		rc = ddi_get_ptoc( &env_info, &block_spec, &newParamSpec, eCmdType, pCommandList );

	}

	return rc;
}


int CFDIEngine::GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID * pCmdItemId)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );

	ITEM_ID itemId = {0};

	int rc = ddi_get_cmd_id( &env_info, &block_spec, ulCmdNumber, &itemId );
	if( rc == DDS_SUCCESS )
	{
		*pCmdItemId = itemId;
	}
	return rc;
}

//String Convenience Functions
wchar_t *CFDIEngine::GetErrorString(int iErrorNum)
{
	return dds_error_string(iErrorNum);
}


int CFDIEngine::GetStringTranslation( const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size )
{
	return ddi_get_string_translation(string,(wchar_t*)lang_code,outbuf, outbuf_size);
}


nsEDDEngine::ITEM_TYPE CFDIEngine::Set_ITEM_TYPE(ITEM_TYPE itemType)
{
	nsEDDEngine::ITEM_TYPE fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;

	switch(itemType)
	{
	case RESERVED_ITYPE1:
		fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;
		break;
	case VARIABLE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VARIABLE;
		break;
	case COMMAND_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMMAND;
		break;
	case MENU_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MENU;
		break;
	case EDIT_DISP_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_EDIT_DISP;
		break;
	case METHOD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_METHOD;
		break;
	case REFRESH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_REFRESH;
		break;
	case UNIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_UNIT;
		break;
	case WAO_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAO;
		break;
	case ITEM_ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ITEM_ARRAY;
		break;
	case COLLECTION_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLLECTION;
		break;
	case BLOCK_B_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK_B;
		break;
	case BLOCK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK;
		break;
	case RECORD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RECORD;
		break;
	case ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ARRAY;
		break;
	case VAR_LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VAR_LIST;
		break;
	case RESP_CODES_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RESP_CODES;
		break;
	case MEMBER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MEMBER;
		break;
	case FILE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_FILE;
		break;
	case CHART_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CHART;
		break;
	case GRAPH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRAPH;
		break;
	case AXIS_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_AXIS;
		break;
	case WAVEFORM_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAVEFORM;
		break;
	case SOURCE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SOURCE;
		break;
	case LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_LIST;
		break;
	case GRID_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRID;
		break;
	case IMAGE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_IMAGE;
		break;
	case BLOB_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_BLOB;
		break;
	case PLUGIN_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_PLUGIN;
		break;
	case TEMPLATE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_TEMPLATE;
		break;
	case 32:
		fdiItemType = nsEDDEngine::ITYPE_RESERVED;
		break;
	case COMPONENT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT;
		break;
	case COMPONENT_FOLDER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_FOLDER;
		break;
	case COMPONENT_REFERENCE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_REFERENCE;
		break;
	case COMPONENT_REL_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_RELATION;
		break;
	case SEPARATOR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SEPARATOR;
		break;
	case ROWBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ROWBREAK;
		break;
	case COLUMNBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLUMNBREAK;
		break;
	case ENUM_BIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ENUM_BIT;
		break;
	case STRING_LITERAL_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_STRING_LITERAL;
		break;
	case CONST_INTEGER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_INTEGER;
		break;
	case CONST_FLOAT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_FLOAT;
		break;
	case SELECTOR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SELECTOR;
		break;
	case LOCAL_PARAM_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_LOCAL_PARAM;
		break;
	case METH_ARGS_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_METH_ARGS;
		break;
	case MAX_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MAX;
		break;

	}
	return fdiItemType;
}


//Type Convenience Functions
int CFDIEngine::GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE * pItemType)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );

	ITEM_TYPE ItemType = {0};

	int rc = ddi_get_type( &env_info, &block_spec, pItemSpec, &ItemType );
	if( rc == DDS_SUCCESS )
	{
		*pItemType = Set_ITEM_TYPE( ItemType ); 
	}

	return rc;
}


int CFDIEngine::GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE * pItemType, ITEM_ID * pItemId)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );

	ITEM_TYPE ItemType = {0};
	ITEM_ID ItemID = {0};

	int rc = ddi_get_type_and_item_id( &env_info, &block_spec, pItemSpec, &ItemType, &ItemID );

	if( rc == DDS_SUCCESS )
	{
		*pItemType = Set_ITEM_TYPE( ItemType );
		*pItemId = ItemID;
	}
	return rc;
}


int CFDIEngine::GetParamType(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, nsEDDEngine::TYPE_SIZE * pTypeSize)
{
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );

	TYPE_SIZE type_size = {0};

	int rc = ddi_get_var_type( &env_info, &block_spec, pParamSpec, &type_size );
	if( rc == DDS_SUCCESS )
	{
		::FDI_ConvTYPE_SIZE::CopyFrom( &type_size, pTypeSize );
	}

	return rc;
}


void CFDIEngine::Init_BLOCK_SPECIFIER( DDI_BLOCK_SPECIFIER *pBlockSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInst )
{
	(void)memset((char *)pBlockSpec, 0, sizeof(*pBlockSpec));

	if (((m_Protocol == nsEDDEngine::FF) || (m_Protocol == nsEDDEngine::ISA100)) && (iBlockInst == nsEDDEngine::DeviceLevel_BI)) // Rule to determine device level
	{
		pBlockSpec->type = DDI_DEVICE_HANDLE;
		pBlockSpec->block.handle = m_deviceHandle;		// This is now a device handle
	}
	else	// HART, PB, or an FF Block
	{
		pBlockSpec->type = DDI_BLOCK_HANDLE;
		pBlockSpec->block.handle = BlockInstToBlockHandle(iBlockInst);
	}
}


void CFDIEngine::Init_ENV_INFO( ENV_INFO *env_info, void* pValueSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInstance, const wchar_t *lang_code )
{
	(void)memset((char *)env_info, 0, sizeof(ENV_INFO));
	
	if (((m_Protocol == nsEDDEngine::FF) || (m_Protocol == nsEDDEngine::ISA100)) && (iBlockInstance == nsEDDEngine::DeviceLevel_BI)) // Rule to determine device level
	{
		env_info->handle_type = ENV_INFO::DeviceHandle;
		env_info->block_handle = m_deviceHandle;			// This is now a device handle
	}
	else	// HART, PB, or an FF Block
	{
		env_info->handle_type = ENV_INFO::BlockHandle;
		env_info->block_handle = BlockInstToBlockHandle( iBlockInstance );
	}

	env_info->app_info = (IDDSSupport*)this;
	env_info->value_spec = pValueSpec;
	if(lang_code != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(env_info->lang_code, maxSize, lang_code, _TRUNCATE);	
	}

}


int CFDIEngine::GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size)
{	
	SYMINFO* syminfo = nullptr;

	int rc = g_DeviceTypeMgr.get_adtt_symbol_info( m_deviceTypeHandle, &syminfo );
	if( rc == DDS_SUCCESS )
	{
		rc = server_item_id_to_name( syminfo, item_id, item_name, outbuf_size );
	}
	return rc;
}


int CFDIEngine::GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* pItemId)
{
	
	SYMINFO* syminfo = nullptr;

	int rc = g_DeviceTypeMgr.get_adtt_symbol_info(m_deviceTypeHandle, &syminfo );
	if( rc == DDS_SUCCESS )
	{
		rc = server_item_name_to_id( syminfo, pItemName, pItemId );
	}
	return rc;
}


int CFDIEngine::GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec, nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, nsEDDEngine::FDI_GENERIC_ITEM * pGenericItem ) 
{ 
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info = {0};
	Init_ENV_INFO( &env_info, pValueSpec, iBlockInstance, lang_code );

	DDI_GENERIC_ITEM generic_item = {0};

	int r_code = ddi_get_item( &block_spec,  pItemSpec, &env_info, *pAttributeNameSet, &generic_item);
	
	if(r_code == SUCCESS || r_code == DDL_CHECK_RETURN_LIST || r_code == DDL_DEFAULT_ATTR)
	{
		pGenericItem->item=(nsEDDEngine::ItemBase *)generic_item.item;

        wchar_t tmpStr[MAX_SIZE+1]{0};
		int rc = GetSymbolNameFromItemId(pItemSpec->item.id, tmpStr, MAX_SIZE);
		pGenericItem->item->symbol_name = (rc == DDL_SUCCESS) ? tmpStr : L"" ;

		///////////////////////////////////////////////////////////////

		pGenericItem->item_type=(nsEDDEngine::ITEM_TYPE)generic_item.item_type;
		for(int i = 0; i < generic_item.errors.count; i++)
		{
			pGenericItem->errors.count							= generic_item.errors.count;
			pGenericItem->errors.list[i].bad_attr				= generic_item.errors.list[i].bad_attr;
			pGenericItem->errors.list[i].rc						= generic_item.errors.list[i].rc;
			
			pGenericItem->errors.list[i].var_needed = generic_item.errors.list[i].var_needed;
		}

		if(pGenericItem->item_type == nsEDDEngine::ITYPE_VARIABLE)
		{
			nsEDDEngine::FLAT_VAR *flat_var = dynamic_cast<nsEDDEngine::FLAT_VAR*>(pGenericItem->item);
			PostProcessFlatVar(flat_var);
		}

		if(pGenericItem->item_type == nsEDDEngine::ITYPE_TEMPLATE)
		{
			nsEDDEngine::FLAT_TEMPLATE *flat_template = dynamic_cast<nsEDDEngine::FLAT_TEMPLATE*>(pGenericItem->item);
			
			PostProcessFlatTemplate(iBlockInstance, flat_template);
			
		}
	}	
	
	return r_code;
}


#ifdef _DEBUG
int CFDIEngine::DisplayDebugInfo()
{
	::nsEDDEngine::AttributeNameSet mask;

	return eval_item_any( NULL, mask, NULL, NULL );
}
#endif


int CFDIEngine::GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir) 
{ 
	*pDeviceDir=this->m_pflat_device_dir; 
	return 0; 
}


void CFDIEngine::GetEDDFileHeader( nsEDDEngine::EDDFileHeader* pEDDFileHeader )
{
	pEDDFileHeader->magic_number	= m_header.magic_number;
	pEDDFileHeader->header_size		= m_header.header_size;
	pEDDFileHeader->metadata_size	= m_header.meta_data_size;
	pEDDFileHeader->item_objects_size= m_header.item_objects_size;

	pEDDFileHeader->device_id.dd_revision=m_header.dd_revision;
	pEDDFileHeader->device_id.device_revision=m_header.device_revision;
	pEDDFileHeader->device_id.device_type=m_header.device_type;
	pEDDFileHeader->device_id.manufacturer=m_header.manufacturer;

    pEDDFileHeader->major_rev		= m_header.major_revision;
    pEDDFileHeader->minor_rev		= m_header.minor_revision;

	pEDDFileHeader->edd_profile		= (::nsEDDEngine::EDD_Profile)m_header.edd_profile;
	pEDDFileHeader->signature		= m_header.signature;
			
	pEDDFileHeader->tool_release = m_header.tool_release;
	pEDDFileHeader->reserved3 = m_header.reserved3;
	pEDDFileHeader->reserved4 = m_header.reserved4;

	if (m_desc.desc_ext.layout == nsEDDEngine::LayoutType::COLUMNWIDTH_NONE)
	{
		switch (pEDDFileHeader->edd_profile)
		{
			case nsEDDEngine::EDD_Profile::PROFILE_HART:
			case nsEDDEngine::EDD_Profile::PROFILE_FF:
			case nsEDDEngine::EDD_Profile::PROFILE_ISA100:
			{
				pEDDFileHeader->layout = ::nsEDDEngine::LayoutType::COLUMNWIDTH_EQUAL;
			}
			break;
			case nsEDDEngine::EDD_Profile::PROFILE_PB:
			case nsEDDEngine::EDD_Profile::PROFILE_PN:
			case nsEDDEngine::EDD_Profile::PROFILE_GPE:
			case nsEDDEngine::EDD_Profile::PROFILE_COMMSERVER:
			{
				pEDDFileHeader->layout = ::nsEDDEngine::LayoutType::COLUMNWIDTH_OPTIMIZED;
			}
			break;
		}
	}
	else
	{
		pEDDFileHeader->layout = m_desc.desc_ext.layout;
	}

	if (m_desc.desc_ext.manufacturer_extended)
	{
        PS_Wcscpy(pEDDFileHeader->manufacturer_ext, wcslen(m_desc.desc_ext.manufacturer_extended)+1, (LPCTSTR)m_desc.desc_ext.manufacturer_extended);
	}
	if (m_desc.desc_ext.device_extended)
	{
        PS_Wcscpy(pEDDFileHeader->device_type_ext, wcslen(m_desc.desc_ext.device_extended)+1, (LPCTSTR)m_desc.desc_ext.device_extended);
	}
}


int CFDIEngine::AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo *pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount )
{
	CLockDeviceTypeMgr dtMgrLock;
	
	int rc = SUCCESS;

	for( int i = 0; i < iCount; i++ )
	{
		BLOCK_HANDLE iBlockHandle = BlockSearch( pFieldbusBlockInfo[i].m_usObjectIndex );
		if( m_connectionMgr.valid_block_handle( iBlockHandle ) )
		{
			rc = BlockHandleToBlockInst( iBlockHandle, &arrBlockInstance[i]);

			if (rc != SUCCESS) {
				break;
			}
		}
		else //need to add to Active Block Table
		{
			rc = m_connectionMgr.CreateBlockEntry(&iBlockHandle, m_deviceHandle ); // return the block handle it creates.
			if( rc == SUCCESS && m_connectionMgr.valid_block_handle( iBlockHandle ) )
			{
				if (!m_connectionMgr.valid_block_handle(m_block_handle) )
				{
					m_block_handle = iBlockHandle;	// Set m_block_handle to the first valid iBlockHandle
				}

				nsEDDEngine::BLOCK_INSTANCE iBlockInstance = (nsEDDEngine::BLOCK_INSTANCE)m_mapBlockInst.size()+1; // Calc the new iBlockInst number (1-n)

				m_mapBlockInst[iBlockInstance] = iBlockHandle;	// Associate this iBlockInst with the Block handle
				arrBlockInstance[i] = iBlockInstance;			// Store it in the output param
				
				LPCSTR blockTag = pFieldbusBlockInfo[i].m_CharRecord.m_pTag;
				 
				//Set Block Information
				m_connectionMgr.SET_ABT_TAG( iBlockHandle, PS_Strdup(blockTag) );
				m_connectionMgr.SET_ABT_ISP_OP_INDEX( iBlockHandle, pFieldbusBlockInfo[i].m_usObjectIndex );
				m_connectionMgr.SET_ABT_DD_BLK_ID( iBlockHandle, pFieldbusBlockInfo[i].m_CharRecord.m_ulDDItemId );
				m_connectionMgr.SET_ABT_CHAR_RECORD( iBlockHandle, pFieldbusBlockInfo[i].m_CharRecord );
				m_connectionMgr.SET_ABT_BLOCK_TYPE( iBlockHandle, pFieldbusBlockInfo[i].m_BlockType );

				ENV_INFO env_info;
				Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" ); // We pass in the iBlockInstance to Init_ENV_INFO

				rc = m_connectionMgr.ds_dd_block_load( &env_info, iBlockHandle );
			}

			if (rc != SUCCESS)
			{
				break;
			}
		}
	}

	return rc;
}

BLOCK_HANDLE CFDIEngine::BlockSearch( unsigned short objectIndex )
{
	BLOCK_HANDLE blockHandle;

	for( blockHandle = 0; blockHandle < m_connectionMgr.get_active_blk_tbl_count(); blockHandle++ )
	{
		if( m_connectionMgr.valid_block_handle(blockHandle) )
		{
			OBJECT_INDEX oi;
			m_connectionMgr.get_abt_op_index (blockHandle, &oi);
			if( (OBJECT_INDEX)objectIndex == oi )
			{
				return blockHandle;
			}
		}
	}

	return -1; //invalid block handle - not found in table
}


int CFDIEngine::GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info = {0};
	env_info.block_handle = m_block_handle;
	env_info.app_info = (IDDSSupport*)this;
	if(lang_code != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(env_info.lang_code, maxSize, lang_code, _TRUNCATE);	
	}

	STRING string = {0};

	int rc = app_func_get_dict_string( &env_info, ulIndex, &string );
	if( rc == DDS_SUCCESS )
	{
		if( string.len < iStringLen )
		{
			(void)PS_Wcscpy( pString, iStringLen, string.str );
		}
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}
	if(string.flags == ::tag_STRING::FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}

int CFDIEngine::GetDictionaryString( wchar_t* wsDictName,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info = {0};
	env_info.block_handle = m_block_handle;
	env_info.app_info = (IDDSSupport*)this;
	if(lang_code != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(env_info.lang_code, maxSize, lang_code, _TRUNCATE);	
	}

	STRING string = {0};

	int rc = app_func_get_dict_string_by_name( &env_info, wsDictName, &string );
	if( rc == DDS_SUCCESS )
	{
		if( string.len < iStringLen )
		{
			(void)PS_Wcscpy( pString, iStringLen, string.str );
		}
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}
	if(string.flags == ::tag_STRING::FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}

int CFDIEngine::GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info = {0};
	
	env_info.block_handle = m_block_handle;
	env_info.app_info = (IDDSSupport*)this;
    if(lang_code != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(env_info.lang_code, maxSize, lang_code, _TRUNCATE);	
	}
	
	STRING string = {0};

	DEV_STRING_INFO dev_string_Info = {0};

	dev_string_Info.id = ulIndex;
	
	int rc = app_func_get_dev_spec_string( &env_info, &dev_string_Info, &string );

	if (rc == DDL_SUCCESS) 
	{
		if (string.len < iStringLen)
		{
			(void)PS_Wcscpy(pString, iStringLen, string.str);	
		} 
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}
	
	if(string.flags == ::tag_STRING::FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}


int CFDIEngine::ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex)
{
	//memberId is dual purpose: if we are an array, the memberid is the subindex;
	//but if we are a record the memberid is the id of member of a record.
	int rc = 0;
	SUBINDEX subindex = {0};
	nsEDDEngine::FDI_ITEM_SPECIFIER item_spec;
	nsEDDEngine::ITEM_TYPE item_type;

	item_spec.eType = nsEDDEngine::FDI_ITEM_ID;
	item_spec.item.id = ItemId;
	item_spec.subindex = 0;

	ENV_INFO env_info;
	Init_ENV_INFO( &env_info, nullptr, iBlockInstance, L"" );

	rc = GetItemType( iBlockInstance, &item_spec, &item_type );

	if( item_type == nsEDDEngine::ITYPE_ARRAY )
	{
		if( MemberId == 0 )
		{
			rc = DDI_TAB_BAD_SUBINDEX; //should only be called from MI
		}
		else
		{
			*iSubIndex = MemberId;
		}
	}
	else if( item_type == nsEDDEngine::ITYPE_RECORD )
	{
		DDI_BLOCK_SPECIFIER block_spec = {0};
		Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
		
		if( MemberId != 0 )
		{
			rc = ddi_get_subindex( &env_info, &block_spec, ItemId, MemberId, &subindex );
			if( rc == DDS_SUCCESS )
			{
				*iSubIndex = subindex;
			}
		}
		else
		{
			rc = DDI_TAB_BAD_SUBINDEX;
		}
	}
	else
	{
		iSubIndex = (int*)MemberId;
		rc = DDI_INVALID_ITEM_TYPE; //should only be called from MI
	}
	return rc;
}



int CFDIEngine::DDS_GetParamValue( BLOCK_HANDLE bh, void* pValueSpec, nsEDDEngine::OP_REF *op_ref, nsConsumer::EVAL_VAR_VALUE *param_value )
{
	nsEDDEngine::FDI_PARAM_SPECIFIER param_spec;
	nsEDDEngine::BLOCK_INSTANCE iBlockInst = 0;
	int rc = DDS_SUCCESS;
	if( op_ref->op_ref_type == STANDARD_TYPE )
	{
		param_spec.id		= op_ref->op_info.id;
		param_spec.subindex	= op_ref->op_info.member;

		switch (op_ref->op_info.type)
		{
		case VARIABLE_ITYPE:
			param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
			break;
		case RECORD_ITYPE:
		case ARRAY_ITYPE:
			param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID_SI;
			break;
		default:
			return PC_BAD_PARAM_REQUEST;
		}
	}
	else
	{
		param_spec.RefList = op_ref->op_info_list;

		param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX;
	}
	if( bh == 0 && op_ref->block_instance > 0 )
	{
		iBlockInst = op_ref->block_instance;
	}
	else
	{
		rc = BlockHandleToBlockInst(bh, &iBlockInst);
	}

	if (rc == DDS_SUCCESS)
	{
		// Call Consumer GetParamValue with FDI data structures
        nsConsumer::PC_ErrorCode ec =  nsConsumer::PC_INVALID_EC;
        if (m_pIParamCache)
        {
            ec = m_pIParamCache->GetParamValue(iBlockInst, pValueSpec, &param_spec, param_value);
        }

		// Convert the PC_ErrorCode into our own return codes
		switch (ec)
		{
		case nsConsumer::PC_SUCCESS_EC:	rc = PC_SUCCESS;		break;
		case nsConsumer::PC_BUSY_EC:	rc = PC_BUSY;			break;
		case nsConsumer::PC_INVALID_EC:	rc = PC_VALUE_NOT_SET;	break;
		case nsConsumer::PC_CIRC_DEPEND_EC:	rc = L7_CIRCULAR_DEPENDENCY_DETECTED;	break;
		default:
		case nsConsumer::PC_OTHER_EC:	rc = PC_INTERNAL_ERROR;	break;
		}
	}
	return rc;
}

// Wrapper function for accesssing IParamCache function of the same name
nsConsumer::PC_ErrorCode CFDIEngine::DDS_GetDynamicAttribute( BLOCK_HANDLE bh, void* pValueSpec, DESC_REF* desc_ref,
    const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue)
{
	nsEDDEngine::BLOCK_INSTANCE iBlockInst = -1;
	int rc = BlockHandleToBlockInst(bh, &iBlockInst);
	if (rc == DDS_SUCCESS)
	{
		nsEDDEngine::OP_REF edd_op_ref;
		edd_op_ref.op_info.id = desc_ref->id;
		edd_op_ref.op_info.type = (nsEDDEngine::ITEM_TYPE) desc_ref->type;
		edd_op_ref.block_instance = iBlockInst;

		return m_pIParamCache->GetDynamicAttribute(iBlockInst, pValueSpec, &edd_op_ref, attributeName, pParamValue);
	}
	else
	{
		return nsConsumer::PC_ErrorCode::PC_OTHER_EC;
	}
}



CConnectionMgr* CFDIEngine::GetConnectionManager()
{
	return &m_connectionMgr;
}

void CFDIEngine::DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory)
{
	if(m_pIEDDEngineLogger)
	{
		m_pIEDDEngineLogger->Log(sMessage, eLogSeverity, sCategory);
	}
}

int CFDIEngine::GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle )
{
	int rc = ConvertToBlockInstance( ulItemId, iOccurrence, piBlockInstance );

	if (rc == SUCCESS)
	{
		*piBlockHandle = BlockInstToBlockHandle( *piBlockInstance );
	}
	else
	{	// Set output parameters to invalid values
		*piBlockInstance = -1;
		*piBlockHandle = -1;
	}

	return rc;
}
///////////////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceByObjectInstance
//
// Description: Obtains the OCCURRENCE number of the block instance at the 
//						 specified object index.
//
///////////////////////////////////////////////////////////////////////////////////
int CFDIEngine::GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence)
{

	int rc = CM_BLOCK_NOT_FOUND;
	ITEM_ID item_id = 0;
	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connectionMgr.get_active_blk_tbl_count()];
	int k = 0;

	// Loop through the active block table to find the instance in the table that matches 
	// the given Object Index and record its ITEM_ID to be used later.
	for( int i = 0; i < m_connectionMgr.get_active_blk_tbl_count(); i++ )
	{
		OBJECT_INDEX oi;
		m_connectionMgr.get_abt_op_index( i, &oi );
		if( oi == iObjectIndex )
		{
			item_id = m_connectionMgr.ABT_CR_ITEM_ID(i);
			rc = SUCCESS;
			break;
		}
	}

	if( rc == SUCCESS )
	{
		// Loop through the active block table in order to find all instances of the block instance
		// and store each instances object index
		for( int j = 0; j < m_connectionMgr.get_active_blk_tbl_count(); j++ )
		{
			if( m_connectionMgr.ABT_CR_ITEM_ID(j) == item_id )
			{
				rc = m_connectionMgr.get_abt_op_index( j, &(crossblockInfo[k].object_index) );
				k++;
			}
		}

		if( rc == SUCCESS )
		{
			//Sort the array by object index in order to get the correct occurrence number for the object index we are looking for.
			qsort(crossblockInfo, k , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);
			
			// Look through the array of blocks to find our matching object index of the block we are looking for and 
			// record the occurrence number
			for( int l = 0; l < k; l++ )
			{
				if( crossblockInfo[l].object_index == iObjectIndex )
				{
					*iOccurrence = l;
					rc = SUCCESS;
					break;
				}
			}
		}
	}

	delete crossblockInfo;
	return rc;
}

///////////////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceByTag
//
// Description: Obtains the OCCURRENCE number of the block instance at the 
//						 specified tag.
//
///////////////////////////////////////////////////////////////////////////////////
int CFDIEngine::GetBlockInstanceByTag(ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence)
{
	
	int rc = CM_BLOCK_NOT_FOUND;
	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connectionMgr.get_active_blk_tbl_count()];
	int k = 0;
	size_t tagSize = wcslen(pTag) +1;
	const size_t  ntagSize = 256;
    char tempTag[ntagSize]{0};
	ITEM_ID item_id = 0;

	PS_Wcstombs( 0, tempTag, tagSize, pTag, _TRUNCATE );

	if( iItemId == 0 )
	{
		// Find the item id of the specified block by the tag
		for( int i = 0; i < m_connectionMgr.get_active_blk_tbl_count(); i++ )
		{
			if( 0 == strcmp(m_connectionMgr.ABT_TAG(i), (tempTag)) )
			{
				item_id = m_connectionMgr.ABT_CR_ITEM_ID(i);
				rc = SUCCESS;
				break;
			}
		}
	}
	else
	{
		item_id = iItemId;
		rc = SUCCESS;
	}

	if( rc == SUCCESS )
	{
		// get all the blocks that correspond to the item id of the specified block and store their information
		for( int j = 0; j < m_connectionMgr.get_active_blk_tbl_count(); j++ )
		{
			if( m_connectionMgr.ABT_CR_ITEM_ID(j) == item_id )
			{
				rc = m_connectionMgr.get_abt_op_index( j, &(crossblockInfo[k].object_index) );
				PS_Strcpy(crossblockInfo[k].tag, (nsEDDEngine::BLOCK_TAG_SIZE+1), m_connectionMgr.ABT_CR_TAG(j));
				rc = SUCCESS;
				k++;
			}
		}

		if( rc == SUCCESS )
		{
			// sort the array of blocks by their block index
			qsort(crossblockInfo, k , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);

			// iterate through the array to find the corresponding occurrence number for the specified block
			for( int l = 0; l < k; l++ )
			{
				if( iItemId == 0 )
				{
					if( 0 == strcmp(crossblockInfo[l].tag, tempTag) )
					{
						*iOccurrence = l;
						rc = SUCCESS;
						break;
					}
				}
				else
				{
					if( (0 == strcmp(crossblockInfo[l].tag, tempTag)) && ( item_id == iItemId ) )
					{
						*iOccurrence = l;
						rc = SUCCESS;
						break;
					}
				}
			}
		}
	}

	delete crossblockInfo;
	return rc;
}

//////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceCount
//
// Description: returns the number of blocks that are instantiated
//						 in the device.
//
//
//////////////////////////////////////////////////////////////////////////
int CFDIEngine::GetBlockInstanceCount( ITEM_ID iItemId, int* piCount)
{
	int iCnt = 0;

	if( iItemId == 0 )
	{
		*piCount = m_connectionMgr.get_active_blk_tbl_count();
	}
	else // Iterate through the entire active block table to find the count of the specified type of block with iItemId
	{
		for ( int i = 0; i < m_connectionMgr.get_active_blk_tbl_count(); i++ )
		{
			if( m_connectionMgr.ABT_DD_BLK_ID(i) == iItemId )
			{
				iCnt++;
			}
		}

		*piCount = iCnt;
	}
	return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: ConvertToBlockInstance
//
// Description: Returns a Block Instance of a specified block type and its Occurrence number
//						 
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
int CFDIEngine::ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance)
{		

	int rc = CM_BLOCK_NOT_FOUND;

	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connectionMgr.get_active_blk_tbl_count()];
	
	int j = 0;
	//iterate through the active block table to find all blocks of the specified type and store in an array
	for( int i = 0; i < m_connectionMgr.get_active_blk_tbl_count(); i++ )
	{
		if( m_connectionMgr.ABT_CR_ITEM_ID(i) == ulItemId )
		{
			m_connectionMgr.get_abt_op_index(i, &(crossblockInfo[j].object_index) ); 
			crossblockInfo[j].block_handle = i;
			j++;
		}
	}
	
	// Sort the block info array by the block object index
	qsort(crossblockInfo, j , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);
	
	if( (iOccurrence >= 0 ) && (iOccurrence < j ) )
	{
		rc = BlockHandleToBlockInst( crossblockInfo[iOccurrence].block_handle, piBlockInstance);
	}

	delete crossblockInfo;
	return rc;
}

int CFDIEngine::CompareObjectIndex( nsEDDEngine::CCrossblockInfo* pElement1 , nsEDDEngine::CCrossblockInfo* pElement2 )
{
	
	if( pElement1->object_index < pElement2->object_index )
	{
		return -1;
	}
	else if( pElement1->object_index > pElement2->object_index )
	{
		return 1;
	}
	return 0;
}

int CFDIEngine::GetCriticalParams(int iBlockInstance, nsEDDEngine::CRIT_PARAM_TBL **pCriticalParamTbl)
{
	*pCriticalParamTbl = &m_pflat_device_dir->blk_tbl.list[iBlockInstance].flat_block_dir.crit_param_tbl;
	return 0;
}
 
int CFDIEngine::GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, ITEM_ID *pWAO_Item, ITEM_ID *pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList)
{
	int r_code = -1;
	ENV_INFO env_info = {0};
	Init_ENV_INFO(&env_info, pValueSpec, iBlockInstance, L"");

	//Initialize output Vars to empty
	if ( pWAO_Item != nullptr)
	{
		*pWAO_Item = 0;
	}

	if ( pUNIT_Item != nullptr)
	{
		*pUNIT_Item = 0;
	}

	if((pUpdateList != nullptr) && (pUpdateList->list != nullptr))
	{
		pUpdateList->~OP_REF_LIST();
	}

	if((pDominantList != nullptr) && (pDominantList->list != nullptr))
	{
		pDominantList->~OP_REF_LIST();
	}

	// invalid instances for this call.  Encountered primarily in regression testing
	if (((this->GetProtocol() == nsEDDEngine::ProtocolType::FF) || (this->GetProtocol() == nsEDDEngine::ProtocolType::ISA100)) && (iBlockInstance == nsEDDEngine::DeviceLevel_BI))
	{
		return 0;
	}

	// Get the data that was requested

	// Init a block_spec
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	if ( pWAO_Item != nullptr)
	{
		r_code = ddi_get_wao(&env_info, &block_spec, pParamSpec, pWAO_Item);

		if (r_code == DDI_TAB_NO_WAO)	// This is an okay return. It just means that we don't have any WAOs
		{								// In this case, *pWAO_Item will remain as 0.
			r_code = DDS_SUCCESS;
		}
	} // pWAO_Item

	if ( pUNIT_Item != nullptr)
	{
		r_code = ddi_get_unit(&env_info, &block_spec, pParamSpec, pUNIT_Item);

		if (r_code == DDI_TAB_NO_UNIT)	// This is an okay return. It just means that we don't have any UNITs
		{								// In this case, *pUNIT_Item will remain as 0.
			r_code = DDS_SUCCESS;
		}
	} // pUNIT_Item

	if ( pUpdateList != nullptr)
	{
		OP_DESC_LIST op_desc_list = {0};
		op_desc_list.count = 0;

		r_code = ddi_get_update_items(&env_info, &block_spec, pParamSpec, &op_desc_list);

		if(r_code == DDS_SUCCESS)
		{
			pUpdateList->list = (nsEDDEngine::OP_REF *) calloc(op_desc_list.count, sizeof(nsEDDEngine::OP_REF));

			pUpdateList->limit = (unsigned short)op_desc_list.count;
			pUpdateList->count = (unsigned short)op_desc_list.count;

			for (int i = 0; i < op_desc_list.count ; i++)
			{
				pUpdateList->list[i] = op_desc_list.list[i].op_ref;
			}

		}
		else if (r_code == DDI_TAB_NO_UPDATE) // This is an okay return. It just means that we don't have any UPDATES
		{									  // In this case, *pUpdateList will remain as 0.
			r_code = DDS_SUCCESS;
		}

		free(op_desc_list.list);
		op_desc_list.list = nullptr;
		} // pUpdateList

	if (pDominantList != nullptr)
	{
		//BLOCK_HANDLE blockHandle = BlockInstToBlockHandle(iBlockInstance);
		int iBlockTableOffset = m_connectionMgr.ABT_DD_BLK_TBL_OFFSET(env_info.block_handle);

		// this is a common starting point for table references.  Use to keep code readable
		nsEDDEngine::FLAT_BLOCK_DIR* pFlatBlockDir = &m_pflat_device_dir->blk_tbl.list[iBlockTableOffset].flat_block_dir;


		unsigned long long key = pParamSpec->id;

		if(pParamSpec->eType == nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_SI)
		{
			key |= ((unsigned long long)pParamSpec->subindex) << 32;
		}

		nsEDDEngine::DOMINANT_TBL::iterator it;

		it = pFlatBlockDir->dominant_tbl->find(key);

		if(it != pFlatBlockDir->dominant_tbl->end())
		{
			nsEDDEngine::OP_REF_LIST *opList = it->second;
				
			pDominantList->count = opList->count;
			pDominantList->limit = opList->limit;
			pDominantList->list = (nsEDDEngine::OP_REF *) calloc(opList->count, sizeof(nsEDDEngine::OP_REF));

			for(int i = 0; i < pDominantList->count ; i++)
			{
				pDominantList->list[i] = opList->list[i];
			}
		}
	} // pDominantList

	return 0;
}

void CFDIEngine::PostProcessFlatVar( nsEDDEngine::FLAT_VAR* pFlatVar )
{
	pFlatVar->default_value.SetExprAsPerVariableType(pFlatVar->type_size);
	pFlatVar->initial_value.SetExprAsPerVariableType(pFlatVar->type_size);
	
	if ((pFlatVar->type_size.type == nsEDDEngine::VT_OCTETSTRING) && (m_Protocol != nsEDDEngine::HART))
	{
		PostProcessOctetVariable(pFlatVar);
	}
}

void CFDIEngine::PostProcessOctetVariable(nsEDDEngine::FLAT_VAR* pFlatVar)
{
	if (((m_Protocol == nsEDDEngine::PROFILE_PB) || (m_Protocol == nsEDDEngine::PROFIBUS_PN)) || (m_Protocol == nsEDDEngine::COMSERVER) || (m_Protocol == nsEDDEngine::GPE))
	{
		// As per -3 spec 9.1.15.11.2 When the DISPLAY_FORMAT attribute is not specified, a default format of �#x� shall be used for CS, GPE, PB, PN
		if (pFlatVar->display.length() == 0)
		{
			pFlatVar->display.assign(L"#x");
		}
		// As per -3 Spec 9.1.15.11.3 if EDIT_FORMAT is not specified, the DISPLAY_FORMAT should be used. 
		if (pFlatVar->edit.length() == 0)
		{
			pFlatVar->edit = pFlatVar->display;
		}
	}
	else if((m_Protocol == nsEDDEngine::FF) || (m_Protocol == nsEDDEngine::ISA100))
	{
		// This is special case for FF "__tag_desc" (ItemId="2147615104(__tag_desc)") . 
		// Since "__tag_desc" is always understood to be a printable string, 
		// the EDD Engine should default this DISPLAY/EDIT_FORMAT to "s" so that the consumer will know
		if(!wcscmp(pFlatVar->symbol_name.c_str(), L"__tag_desc"))
		{
			pFlatVar->display.assign(L"s");
			pFlatVar->edit.assign(L"s");
		}
		else
		{
			// As per -3 spec 9.1.15.11.2 When the DISPLAY_FORMAT attribute is not specified, a default format of �X� shall be used for FF
			if (pFlatVar->display.length() == 0)
			{
				pFlatVar->display.assign(L"X");
			}
		}
		// As per -3 Spec 9.1.15.11.13 if EDIT_FORMAT is not specified, the DISPLAY_FORMAT should be used. 
		if (pFlatVar->edit.length() == 0)
		{
			pFlatVar->edit = pFlatVar->display;
		}

	}
}
void CFDIEngine::PostProcessFlatTemplate( int iBlockInstance, nsEDDEngine::FLAT_TEMPLATE* pFlatTemplate )
{
	nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::variable_type;

	for(int i = 0; i < pFlatTemplate->default_values.count; i++ )
	{
		if( pFlatTemplate->default_values.list[i].ref.desc_type == nsEDDEngine::ITYPE_VARIABLE)
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec;
			
			item_spec.eType = nsEDDEngine::FDI_ITEM_ID;
			item_spec.item.id = pFlatTemplate->default_values.list[i].ref.desc_id;
			item_spec.subindex = 0;
			
			nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_VARIABLE);
	
			int r_code = GetItem(iBlockInstance, 0, &item_spec, &attribute_list, L"|en|", &generic_item);

			if(r_code == SUCCESS) 
			{
				
				nsEDDEngine::FLAT_VAR *flat_var = dynamic_cast<nsEDDEngine::FLAT_VAR * >(generic_item.item);
			
				pFlatTemplate->default_values.list[i].value.SetExprAsPerVariableType(flat_var->type_size);

			}
			// else There is nothing we can do if GetItem fails.
		} 
		// else There is nothing we can do if it's not a variable.
		
	}

}

CEDDEngineImpl* FDIEngineFactory::CreateFDIDDS( const wchar_t *sEDDBinaryFilename, 
	  const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
{
	CEDDEngineImpl *pImpl = (CEDDEngineImpl*)new CFDIEngine(sEDDBinaryFilename, pConfigXML, pIEDDEngineLogger,
        pErrorCode);

	if (*pErrorCode != nsEDDEngine::EDDE_SUCCESS)	// If there is an error in the constructor, delete and return null
	{
		delete pImpl;
		pImpl = nullptr;
	}

	return pImpl;
}

