#pragma once


#include "cm_lib.h"
#include "AMSDds/table.h"
#include "cm_rod.h"
#include "rmalloc.h"

#define ROD_HEAP_SIZE		(10 * 1024 * 1024)	// 10 Meg
class CRodMgr
{
public:
	CRodMgr();
	~CRodMgr();

	int	valid_rod_handle( ROD_HANDLE ) ;
	unsigned long rod_get_version(ROD_HANDLE rhandle);
	int rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);	
	int ds_ddod_file_load (wchar_t *filename, ROD_HANDLE *rod_handle);
	int rod_close(ROD_HANDLE rod_handle);

private:
	ROD_TBL				rod_tbl;
	RHEAP				*rheap;
	long				rod_heap[ROD_HEAP_SIZE/sizeof(long)];
	
	char *rod_malloc(int size);
	char *rod_calloc(int number, int size);
	char *rod_realloc(char *pointer, int size);
	void rod_free(char *pointer);
	int rod_member(char *pointer);
	void rod_object_free(OBJECT *object);	
	int rod_object_heap_check(OBJECT *object);
	int rod_object_copy(OBJECT **destination_object, OBJECT *source_object);
	ROD_HANDLE rod_new_rod(wchar_t * filename);
	int ds_get_object_value(FILE *file, DDOD_HEADER *header, OBJECT *object);	
	ROD_HANDLE rod_open(OBJECT *object_0, ROD_HANDLE rod_handle);	
	int rod_set_version(ROD_HANDLE rod_handle, unsigned long version);
	int ds_get_object(FILE *file, ROD_HANDLE rod_handle);
	int rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX	object_index, OBJECT *object);	
	int rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
};
