//#ifndef lint
//static char SCCSID[] = "@(#)cm_init.c	30.4  30  14 Nov 1996";
//#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_init.c - Connection Manager Initialization Module
 *
 *	This file contains all functions pertaining to the
 *	initialization of the Connection Manager.  It also
 *	contains any functions that are common to all of the
 *	Connection Manager modules.
 *
 */
#include "stdinc.h"
#include "DDI_LIB.H"
#include "cm_lib.h"
#include "panic.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

extern int ds_get_header(FILE *file, DDOD_HEADER *header);
/***********************************************************************
 *
 *	Name:  cm_init
 *	ShortDesc:  Initialize the Connection Manager.
 *
 *	Description:
 *		The cm_init function initializes the Connection Manager by
 *		initializing each of its sub-systems.
 *
 *	Inputs:
 *		path - path to the DD files.
 *		mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_init function.
 *		Return values from rod_init function.
 *		Return values from ds_init function.
 *		Return values from cr_init function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::cm_init(wchar_t *path, int mode)
{

	int		r_code;

	/*
	 *	Initialize each of the Connection Manager's sub-systems.
	 */

	/* Table Manager */

	r_code = ct_init();
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}


	/* DD Support */

	r_code = ds_init(path);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/* Communication Support */

	r_code = cr_init(mode);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  cm_cleanup
 *	ShortDesc:  Cleanup the Connection Manager.
 *
 *	Description:
 *		The cm_init function initializes the Connection Manager by
 *		initializing each of its sub-systems.
 *
 *	Inputs:
 *		path - path to the DD files.
 *		mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_init function.
 *		Return values from rod_init function.
 *		Return values from ds_init function.
 *		Return values from cr_init function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::cm_cleanup()
{
	/*
	 *	Cleanup each of the Connection Manager's sub-systems.
	 */

	/* Table Manager */

	ct_cleanup();

	/* Communication Support */

	cr_cleanup();
}


/***********************************************************************
 *
 *	Name:  cm_init_dds_tbl_fn_ptrs
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The cm_init_dds_tbl_fn_ptrs function links the pointers of the
 *		DDS functions that the Connection Manager calls when any DDS
 *		tables need to be built or removed.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::cm_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that build the DDS tables.
	 */

	ds_init_dds_tbl_fn_ptrs(dds_tbl_fn_ptrs);

	/*
	 *	Link the pointers of the functions that remove the DDS tables.
	 */

	ct_init_dds_tbl_fn_ptrs(dds_tbl_fn_ptrs);
}


/******************************************************************************
Name:		init_dds_tbls

ShortDesc: Initialize DDS table
	
Description: 

Inputs:		none
				
Outputs:    none                 
	
Returns:    none

Author:		
******************************************************************************/
void CConnectionMgr::init_dds_tbls(const wchar_t* pReleaseDir)
{
	// Initialize pointers to build and remove device and block tables
	DDS_TBL_FN_PTRS  dds_tbl_fn_ptrs;

	dds_tbl_fn_ptrs.build_dd_dev_tbl_fn_ptr = ddi_build_dd_dev_tbls;    //**s1*313
	dds_tbl_fn_ptrs.build_dd_blk_tbl_fn_ptr = ddi_build_dd_blk_tbls;
	dds_tbl_fn_ptrs.remove_dd_dev_tbl_fn_ptr = ddi_remove_dd_dev_tbls;
	dds_tbl_fn_ptrs.remove_dd_blk_tbl_fn_ptr = ddi_remove_dd_blk_tbls;

	cm_init_dds_tbl_fn_ptrs(&dds_tbl_fn_ptrs);

	// Initialize the connection manager
	int r_code = cm_init(const_cast<wchar_t*>(pReleaseDir), CMM_OFF_LINE);    //**z1*15
	if (r_code)
	{
		wchar_t szTemp[256]= {0};
		dds_error_string_ff(r_code, szTemp, 256);
        panic("Error initializing Connection Manager:\n    %s\n", szTemp);
	}
}

/******************************************************************************
Name:		cleanup_dds_tbls

ShortDesc: Cleanup DDS table
	
Description: 

Inputs:		none
				
Outputs:    none                 
	
Returns:    none

Author:		
******************************************************************************/
void CConnectionMgr::cleanup_dds_tbls()
{
	// Cleanup the connection manager
	cm_cleanup();
}



/******************************************************************************
Name:		open_blk

ShortDesc:	Obtain a handle to a record corresponding to a block
	
Description:  Based on a Object Index, uniquely identifying a block, open the block,
			  obtain its handle and do the associated initializations

Inputs:		usObjectIndex
				
Outputs:    block_handle             
	
Returns:    none

Author:		
******************************************************************************/
int CConnectionMgr::open_blk(ENV_INFO *env_info, const wchar_t* sDeviceName, ITEM_ID blockId, OBJECT_INDEX usObjectIndex, BLOCK_HANDLE* block_handle, const DDOD_HEADER *header)
{
	ASSERT_DBG(block_handle);

	// Get the DD_DEVICE_ID info from the header
	DD_DEVICE_ID dd_device_id = {0};
	dd_device_id.ddid_manufacturer	= header->manufacturer;
	dd_device_id.ddid_device_type	= header->device_type;
	dd_device_id.ddid_device_rev	= header->device_revision;
	dd_device_id.ddid_dd_rev		= header->dd_revision;

	//
	// Open the block - creates active table entries
	//
	int r_code = ct_block_open(sDeviceName, usObjectIndex, &dd_device_id, block_handle);    //**s1*339
	if (r_code) 
	{
		panic("ct_block_open(%u) failed: %d\n", usObjectIndex, r_code);    //**rt
	}

	if (!blockId) 
	{
		panic(	"No DDID for block - missing symbol file or\n"
				"    wrong reference to DD information located in .ffo or .ff5 file\n");
	}

	r_code = set_abt_dd_blk_id(*block_handle, blockId) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	//
	// Load the DD into memory and the Device and Block tables.
	//
	ROD_HANDLE rod_handle = 0;

	r_code = ds_dd_block_load(env_info, *block_handle, &rod_handle);
	if (r_code) 
	{
		wchar_t szTemp[256]= {0};
		dds_error_string_ff(r_code, szTemp, 256);
		panic("ds_dd_block_load failed: %s\n", szTemp);
	}

	return CM_SUCCESS;
}



/******************************************************************************
Name:		open_device

ShortDesc:	Obtain a handle to a record corresponding to a device
	
Description:  Based on a block_tag, uniquely identifying a block, open the block,
			  obtain its handle and do the associated initializations

Inputs:		block_tag
				
Outputs:    block_handle             
	
Returns:    none

Author:		
******************************************************************************/
int CConnectionMgr::open_device(ENV_INFO* env_info, const wchar_t *szFilename, const wchar_t* sDeviceName, DEVICE_HANDLE* device_handle, DDOD_HEADER *header )
{
	ASSERT_DBG(sDeviceName);
	ASSERT_DBG(device_handle);

	/*
	 *	Open the DDOD file and get the Header information.
	 */

	FILE *file = PS_wfopen(szFilename, L"rb");
	if (!file) {
		return(CM_BAD_FILE_OPEN);
	}

	int r_code = ds_get_header(file, header);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	r_code = fclose(file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}

	//
	// Store the DD_DEVICE_ID info from this file
	//
	DD_DEVICE_ID dd_device_id = {0};

	dd_device_id.ddid_manufacturer	= header->manufacturer;
	dd_device_id.ddid_device_type	= header->device_type;
	dd_device_id.ddid_device_rev	= header->device_revision;
	dd_device_id.ddid_dd_rev		= header->dd_revision;
	wcscpy(dd_device_id.dd_path,szFilename);
			

	//
	// Open the block - creates active table entries
	//
	r_code = ct_device_open(sDeviceName, &dd_device_id, device_handle);    //**s1*339
	if (r_code) 
	{
		panic("ct_device_open(%s) failed: %d\n", sDeviceName, r_code);    //**rt
	}

	//
	// Load the DD into memory and the Device and Block tables.
	//

	ROD_HANDLE rod_handle = ADT_DD_HANDLE(*device_handle);
	// if rod_handle is valid, then we are sharing data, so no need to reload.
	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle))
	{
		r_code = ds_dd_device_load_ex(env_info, *device_handle, &rod_handle);
	}

	return r_code;
}
