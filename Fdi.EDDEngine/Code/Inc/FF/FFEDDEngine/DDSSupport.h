
#include "AMSDds/DDI_LIB.H"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"
#include "nsEDDEngine/Common.h"
#include "nsConsumer/IEDDEngineLogger.h"


class IDDSSupport
{
public:
	virtual int DDS_GetParamValue( BLOCK_HANDLE bh, void* pValueSpec, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value) = 0;
	virtual CConnectionMgr* GetConnectionManager() = 0;

	virtual int GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle ) = 0;
	virtual void DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory) = 0;
};
