/******************************************************************************
 * $Header: Uc2A.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:55 AM$
 *
 * Copyright 1999 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *		Include file for Uc2A class.
 *													   
 * $Workfile: Uc2A.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#if !defined(AFX_UC2A_H__C1079B43_04BE_11D3_81D9_006008AF8E7D__INCLUDED_)
#define AFX_UC2A_H__C1079B43_04BE_11D3_81D9_006008AF8E7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <AMSMiscUtilExport.h>
#include "PlatformCommon.h"

/******************************************************************************
Class:	Uc2A

Description:
	Unicode conversion (Uc) utility class for converting to const ANSI string.

Responsibilites:
	* Convert string classes (e.g. CStdString, etc.) to const ANSI string
	* Convert strings to const ANSI string.
	* Convert non-terminated strings to const ANSI string.
	* Encapsulate string-buffer memory allocation/deallocation.
	* Avoid dynamic allocation when possible.

Author:	Kevin Mixter
******************************************************************************/
class AMSMISCUTIL_EXPORT Uc2A  
{
public:
	Uc2A(LPCWSTR);						// Construct from Unicode string
	Uc2A(LPCWSTR, const int);			// Construct from non-terminated Unicode string
	Uc2A(LPCSTR);						// Construct from ANSI String
	Uc2A(LPCSTR, const int);			// Construct from non-terminated ANSI string
	virtual ~Uc2A();					// Destruct

	LPCSTR asLPCSTR() const;			// Conversion to const ANSI (narrow) string

	int getLengthInChars() const;		// Get length (not including terminator) of converted string in characters
	int getSizeInBytes() const;			// Get size (including terminator) of converted string in bytes

protected:
	void constructW2CA(LPCWSTR, const int); // Construct converted/terminated string from non-terminated Unicode string
	Uc2A();								// Construct default conversion object - for derived classes

    //LPSTR m_paSzBuffer;					// ANSI string buffer, allocated as needed
	LPCSTR m_pcaSz;						// ANSI string ptr

private:
	// Prevent the use of these methods by making them private and not providing any implementation
	Uc2A(const Uc2A&);
	Uc2A& operator=(const Uc2A&) const;
};

#endif // !defined(AFX_UC2A_H__C1079B43_04BE_11D3_81D9_006008AF8E7D__INCLUDED_)
