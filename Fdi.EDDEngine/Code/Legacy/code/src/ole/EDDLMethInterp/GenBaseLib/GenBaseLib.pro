#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T14:02:10
#
#-------------------------------------------------

QT       -= gui

TARGET = GenBaseLib
TEMPLATE = lib
CONFIG += staticlib
#CONFIG +=plugin
DEFINES += GENBASELIB_LIBRARY \
           UNICODE

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKESPEC=linux-g++-32
QMAKE_CXXFLAGS += -std=gnu++11
QMAKE_CXXFLAGS += -std=c++11

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += CDictionary.cpp \
    CMethInterpddbGlblSrv.cpp \
    CMethInterpGlobals.cpp \
    CMethodInterpreterDevice.cpp \
    ddbArray.cpp \
    ddbAxis.cpp \
    ddbCollAndArr.cpp \
    ddbMethod.cpp \
    ddbVar.cpp \
    hCRecord.cpp \
    MethodThread.cpp \
    ProtectedDeviceList.cpp \
    ServerDictionary.cpp \
    stdafx.cpp \

HEADERS += ../../../../inc/Ole/EDDLMethInterp/MiServerShared.h\
    ../../../../inc/ServerProjects/CommonToolkitStrings.h\
    ../../../../inc/ServerProjects/PrePostReadWriteMethodMessages.h\
    ../APPS/APPsupport/DDParser/Attributes.h\
    ../APPS/APPsupport/DDParser/DDlConditional.h\
    ../APPS/APPsupport/DDParser/Dictionary.h\
    ../APPS/APPsupport/DevServices/ddbGlblSrvInfc.h\
    ../COMMON/CMethodInterpreterDevice.h\
    ../COMMON/Endian.h\
    ../COMMON/errors.h\
    ../COMMON/hCRecord.h\
    ../COMMON/logging.h\
    ../COMMON/MethodThread.h\
    ../COMMON/ServerDictionary.h\
    ../COMMON/StdAfx.h\
    ../COMMON/TAGS_SA.H\
    ProtectedDeviceList.h \
    ../COMMON\

unix {
    target.path = /usr/lib/
    INSTALLS += target
}
DESTDIR = ../../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}

include($$DESTDIR/../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../APPS/APPsupport/BuiltinLib/
INCLUDEPATH += $$PWD/../APPS/APPsupport/DDParser/
INCLUDEPATH += $$PWD/../APPS/APPsupport/DevServices/
INCLUDEPATH += $$PWD/../APPS/APPsupport/Interpreter/
INCLUDEPATH += $$PWD/../APPS/APPsupport/MEE/
INCLUDEPATH += $$PWD/../APPS/APPsupport/ParserInfc/
INCLUDEPATH += $$PWD/../COMMON/
INCLUDEPATH += $$PWD/../../../../inc/
INCLUDEPATH += $$PWD/../../../../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Linux
}


unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR
unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a
