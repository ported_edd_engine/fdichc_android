/*
 * @(#) $Id: flats.h,v 1.3 2012/09/03 22:13:11 rgretta Exp $
 */

#ifndef FLATS_H
#define FLATS_H

#include "std.h"

#include "attrs.h"


/*
 *	FLAT structures
 */


typedef struct tag_FLAT_MASKS {

	ulong           bin_exists;	/* There is binary for this attribute */

	ulong           bin_hooked;	/* Binary is attached for this
					 * attribute */

	ulong           attr_avail;	/* The attribute has been evaluated	 */

	ulong           dynamic;/* The attribute is dynamic */

}               FLAT_MASKS;


#endif
