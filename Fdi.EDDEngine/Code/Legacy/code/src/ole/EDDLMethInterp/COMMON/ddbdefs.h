/*************************************************************************************************
 *
 * $Workfile: ddbdefs.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This holds the definitions that support various classes.
 *		7/31/2	sjv	started creation
 *
 *		
 * #include "ddbDefs.h"
 */

#ifndef _DDBDEFS_H
#define _DDBDEFS_H
#ifdef INC_DEBUG
#pragma message("In DDBdefs.h") 
#endif

#include "nsEDDEngine/ProtocolType.h"

// this file needs to be a primative - no includes

#ifdef INC_DEBUG
#pragma message("    Finished includes from DDBdefs.h") 
#endif

bool AlmostEqual2sComplement(float A, float B, int maxUlps); 
bool AlmostEqual2sComplement(double A, double B, int maxUlps);


/*=*=*=*=*=*=*=*=*=*=*=* General Support =*=**=*=*=*=*=*=*=*=*/

#ifndef SUB_INDEX
#define SUB_INDEX int
#endif 


/*=*=*=*=*=*=*=*=*=*=* Expression Support *=**=*=*=*=*=*=*=*=*/

typedef enum  arithOp_e
{	    
	    aOp_dontCare,	// 0
		aOp_wantMax,	// MaxVal arithOption
		aOp_wantMin		// MinVal arithOption
	#ifdef REALLYSYMETRICALL /* if the language was symetrical*/
	,	aOp_wantScale,
		// aOp_wantDisp,
		// aOp_wantEdit
	#endif
} /*typedef*/ arithOp_t;

typedef enum expressionType_e
{
		eT_Unknown = 0,
		eT_IF_expr,     // 1
		eT_SEL_expr,	// 2
		eT_CASE_expr,	// 3
		eT_Direct,      // 4
		// the rest imply that: expressionL & exprDependsL are MT
		eT_IF_isTRUE,    // THEN
		eT_IF_isFALSE,   // ELSE
		et_SEL_isDEFAULT // always TRUE
} /*typedef*/ expressionType_t;

/*****---- data status moved from ddbVar.cpp ----****/

typedef enum data_avail  {
	DA_HAVE_DATA, DA_NOT_VALID, DA_STALE_OK, DA_STALEUNK
} DATA_QUALITY_T;

enum AccessType		//copied from BuiltIn.h
{
	WRITE,	// 0
	READ   = 1	
};


/*****---- expressions are now broken down into finer grain definitions----****/

/* this must be global so varient and expression can use it */
typedef enum expressionElementType_e
{/* note that these are equivalent to the tag values */
	eeT_Unknown = 0,
	eet_NOT,			//_OPCODE 1			bool
	eet_NEG,			//_OPCODE 2			Value
	eet_BNEG,			//_OPCODE 3			Value
	eet_ADD,			//_OPCODE 4			Value
	eet_SUB,			//_OPCODE 5			Value
	eet_MUL,			//_OPCODE 6			Value
	eet_DIV,			//_OPCODE 7			Value
	eet_MOD,			//_OPCODE 8			Value
	eet_LSHIFT,			//_OPCODE 9			Value
	eet_RSHIFT,			//_OPCODE 10		Value
	eet_AND,			//_OPCODE 11		Value
	eet_OR,				//_OPCODE 12		Value
	eet_XOR,			//_OPCODE 13		bool
	eet_LAND,			//_OPCODE 14		bool
	eet_LOR,			//_OPCODE 15		bool
	eet_LT,				//_OPCODE 16		bool
	eet_GT,				//_OPCODE 17		bool
	eet_LE,				//_OPCODE 18		bool
	eet_GE,				//_OPCODE 19		bool
	eet_EQ,				//_OPCODE 20		bool
	eet_NEQ,			//_OPCODE 21		bool

	eet_INTCST,			//_CONST  22		Value
	eet_FPCST,			//_CONST  23		Value

	eet_VARID,			//_VARVAL 24	isRef = F
	eet_MAXVAL,			//_VARVAL 25	isRef = F,isMax = T
	eet_MINVAL,			//_VARVAL 26	isRef = F,isMax = F

	eet_VARREF,			//_VARVAL 27	isRef = T
	eet_MAXREF,			//_VARVAL 28	isRef = T,isMax = T
	eet_MINREF			//_VARVAL 29	isRef = T,isMax = F

//#ifdef NOTUSED4HART
,	eet_BLOCK,			//_VARVAL 30	isRef = F ????
	eet_BLOCKID,		//_VARVAL 31	isRef = F
	eet_BLOCKREF		//_VARVAL 32	isRef = T
//#endif
	
,	eet_STRCONST,		//STRCST_OPCODE  33
	eet_SYSENUM,		//SYSTEMENUM_OPCODE 34
	eet_COUNTREF,		//CNTREF_OPCODE	35
	eet_CAPACITYREF,	//CAPREF_OPCODE	36
	eet_FIRSTREF,		//FSTREF_OPCODE	37 // stevev 17aug06 - this is a VARREF
	eet_LASTREF			//LSTREF_OPCODE	38 // stevev 17aug06 - this is a VARREF
/* stevev 17aug06 - rest of legal types - not in Tokenizer yet all reference.xxx*/
	,eet_DFLT_VALREF	//DFLTVAL_OPCODE 39
	,eet_VIEW_MINREF	//VMIN_OPCODE    40
	,eet_VIEW_MAXREF	//VMAX_OPCODE    41


} /*typedef*/ expressElemType_t;

/*=*=*=*=*=*=*=*=* End Expression Support *=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*=*= Type Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 * Types of payloads.
 */
typedef enum payloadType_e
{
	pltUnknown		= 0,
	pltddlStr,			// 1	COND2PAYDDLSTR
	pltddlLong,			// 2	COND2PAYDDLONG
	pltbitStr,			// 3	COND2PAYDDLBSTR
	pltEnumList,		// 4	COND2PAYENUMLIST
	pltDataItmLst,		// 5	COND2DILIST
	pltMenuList,			// 6	COND2PAYMLIST
	pltRespCdList,		// 7	COND2RCLIST
//	pltEdDispLst,		// 8	COND2PAYEDDISPLIST

//	pltTransList,		// 9	COND2TRLIST
//	pltMenuList,		//10	COND2PAYMENU
//	pltRespCdList,		//11	COND2PAYRESPCD
//	pltTypeType,		//12	COND2PAYTYPETYPE
//	pltEnumList,		//13	COND2PAYENUM
//	pltItmArrElemLst,	//14	COND2PAYELIST
//	pltCollMemList,		//15	COND2PAYMEMLIST
	pltGroupItemList,
//	pltRecMemList,		//16	COND2PAYRECLIST
//	pltWrtAoneList,     //17    COND2PAYWAOLIST
//	pltUnitList,        //18    COND2PAYUNITLIST
	//pltRefreshList,     //19    COND2PAYREFRESHLIST
	pltReferenceList,	//19    COND2PAYREFLIST
	pltReference,       //20	COND2PAYREFERENCE
	pltExpression,		//21	post DB.JITParser - expression payload for min/max/scale
	pltMinMax,			//22	
	pltIntWhich,
	pltGridMemberList,  // Unique 'members' - NOT true members w/ desc & help
//see groupitem	pltMemberElemList,  // 23 - generic array/collection,graph member list
//	pltMemListOLists,	// 24 - list of the above
			// COND2PAYTRANSACTION
			// COND2PAYDATAITEM
//    DEST2COND
//    DEST2CONDLIST
//    COND
	pltLastLdType
}  /*typedef*/ payloadType_t;

/*=*=*=*=*=*=*=*=*=*=*=* End Type Support *=**=*=*=*=*=*=*=*=*/

/*=*=*=*=*=*=*=*=*=*=*= Variable Support =*=**=*=*=*=*=*=*=*=*/
/*
 * Types of variables.
 */
typedef enum variableType_e
{
	vT_unused			= 0,
	vT_undefined		= 1,
	vT_Integer			= 2,
	vT_Unsigned			= 3,
	vT_FloatgPt			= 4,
	vT_Double			= 5,
	vT_Enumerated		= 6,
	vT_BitEnumerated	= 7,
	vT_Index			= 8,
	vT_Ascii			= 9,
	vT_PackedAscii		= 10,
	vT_Password			= 11,
	vT_BitString		= 12,
	vT_HartDate			= 13,
	vT_Time				= 14,
	vT_DateAndTime		= 15,
	vT_Duration			= 16,
	vT_EUC				= 17,
	vT_OctetString		= 18,
	vT_VisibleString	= 19,
	vT_TimeValue		= 20,
	vT_ObjectReference	= 21,
	vT_Boolean			= 22,
	vT_MaxType			= 23
}  /*typedef*/ variableType_t;


/**** variable attribute types ****/
/* from ddldefs.h but converted to an enum */
/* mask conversion to enum:  en = (varAttrType_t)mask2value(attr_mask); */
/* enum conversion to mask   msk= 0x00000001<< ((unsigned long)en)      */
/*							 see maskFromInt(a) macro in dllapi.h       */
typedef enum varAttrType_e
{									   // default values
	 varAttrClass			= 0,	//*- none - required??
	 varAttrHandling		= 1,	//*READ_HANDLING | WRITE_HANDLING
	 varAttrConstUnit		= 2,	//*len=0, flags=0
	 varAttrLabel			= 3,	//*BASE:  DEFAULT_STD_DICT_LABEL
	 varAttrHelp			= 4,	//*BASE:  DEFAULT_STD_DICT_HELP
	 varAttrRdTimeout		= 5,	// 0
	 varAttrWrTimeout		= 6,	// 0
	 varAttrValidity		= 7,	//*BASE:  TRUE
	 varAttrPreReadAct		= 8,	// count = 0
	 varAttrPostReadAct		= 9,	// count = 0
	 varAttrPreWriteAct		= 10,	// count = 0
	 varAttrPostWriteAct	= 11,	// count = 0
	 varAttrPreEditAct		= 12,	// count = 0
	 varAttrPostEditAct		= 13,	// count = 0
	 varAttrResponseCodes	= 14,	// 0
	 varAttrTypeSize		= 15,	//*- none - required
	 varAttrDisplay			= 16,	//*DEFAULT_STD_DICT_DISP_INT
									   // DEFAULT_STD_DICT_DISP_UINT
									   // DEFAULT_STD_DICT_DISP_FLOAT
									   // DEFAULT_STD_DICT_DISP_DOUBLE
	 varAttrEdit			= 17,	//*DEFAULT_STD_DICT_EDIT_INT
									   // DEFAULT_STD_DICT_EDIT_UINT
									   // DEFAULT_STD_DICT_EDIT_FLOAT
									   // DEFAULT_STD_DICT_EDIT_DOUBLE
	 varAttrMinValue		= 18,	// count = 0
	 varAttrMaxValue		= 19,	// count = 0
	 varAttrScaling			= 20,	//*1 byte int with value of 1
	 varAttrEnums			= 21,	// count = 0
	 varAttrIndexItemArray	= 22,
	 varAttrDefaultValue	= 23,
	 varAttrRefreshAct		= 24,
	 varAttrDebugInfo		= 25,
#ifdef XMTR	
	varAttrPostRequestAct	= 26,
	varAttrPostUserAct		= 27,
#endif
    varAttrViewMin			= 28,
    varAttrViewMax			= 29,
    varAttrCount			= 30,
    varAttrFirst			= 31,
    varAttrLast				= 32,
    varAttrVariableStatus	= 33,
    varAttrCapacity			= 34,
    varAttrXAxis			= 35,
    varAttrYAxis			= 36,
    varAttrScalingFactor	= 37,
	varAttrLastvarAttr		= 38,	/* must be last in list for scanning*/
}
/*typedef*/ varAttrType_t;


/************** variable attribute supporting types **********/
/**** CLASS ****/
/* for variables and methods - derived from ddldefs.h */
/* note that the class attribute is a bitstring so multiple
 * classes may be set - the masks follow
 * 1 << (attrClass_t-1)  will give the mask bit for that class
 */

typedef enum maskClass_e
{
	maskNoClass			=  0x00000000,
	maskDiagnostic		=  0x00000001,
	maskDynamic			=  0x00000002,
	maskService			=  0x00000004,
	maskCorrection		=  0x00000008,
	maskComputation		=  0x00000010,
	maskInputBlock		=  0x00000020,
	maskAnalogOut		=  0x00000040,
	maskHART			=  0x00000080,
	maskLocalDisplay	=  0x00000100,
	maskFrequency		=  0x00000200,
	maskDiscrete		=  0x00000400,
	maskDevice			=  0x00000800,
	maskLocal			=  0x00001000,
	maskInput			=  0x00002000,
	maskFactory			=  0x00100000	// originator proprietary I imagine
}
/*typedef*/ maskClass_t;

/**** HANDLING ****/
/* note that these are both values AND masks */
typedef enum attrHandling_e
{
	handlingUnknown	= 0,
	handlingRead    = 1,
	handlingWrite   = 2,
	handlingRdWr    = 3
#ifdef XMTR
	, handlingConfigUnknown = 4,
	handlingConfigRead    = 5,
	handlingConfigWrite   = 6,
	handlingConfigRdWr    = 7
#endif
}
/*typedef*/ attrHandling_t;


/*=*=*=*=*=*=*=*=*=*=*= Method Support =*=**=*=*=*=*=*=*=*=*/
/* METHOD attributes */
/*
 * Method type flags
 */
typedef enum methodTypeFlag_e
{
	methodType_NonFlaged    =   0x00000000,
	methodType_Array		=	0x00000001,
	methodType_Reference	=	0x00000002,
	methodType_Const    	=	0x00000004	// unsupported
}
/*typedef */ methodTypeFlag_t;

typedef enum methodVarType_e
{
	methodVarVoid			= 0,
	methodVarChar,
	methodVarShort,
	methodVarLongInt,
	methodVarFloat,
	methodVarDouble,		// 5
	methodVar_U_Char,
	methodVar_U_Short,
	methodVar_U_LongInt,
	methodVarInt64,
	methodVar_U_Int64,		//10
	methodVarDDString,
	methodVarDDItem,
	methodVar_Unknown
}
/*typedef */ methodVarType_t;


/*=*=*=*=*=*=*=*=*=*=*= Axis Support =*=**=*=*=*=*=*=*=*=*/
/* AXIS attributes */
typedef enum axisAttrType_e
{
	axisAttrLabel			= 0,
	axisAttrHelp,
	axisAttrValidity,		/* no longer there, we'll leave this as a placeholder*/
	axisAttrMinVal,
	axisAttrMaxVal,
	axisAttrScale,
	axisAttrUnit,
	axisAttrDebugInfo,
	axisAttrViewMinVal,
	axisAttrViewMaxVal,
	axisAttr_Unknown
}
/*typedef */ axisAttrType_t;


/*=*=*=*=*=*=*=*=*=*=*= List Support =*=**=*=*=*=*=*=*=*=*/
/* LIST attributes */
typedef enum listAttrType_e
{
	listAttrLabel			= 0,
	listAttrHelp,
	listAttrValidity,
	listAttrType,
	listAttrCount,
	listAttrCapacity,
	listAttrDebugInfo,
	listAttrFirst,
	listAttrLast,
	listAttr_Unknown
}
/*typedef */ listAttrType_t;

		
/*=*=*=*=*=*=*=*=*=*=*=*= Item Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 *  Types of items
 *
 */
typedef enum itemType_e
{
	iT_ReservedZeta			= 0,
	iT_Variable				= 1,
	iT_Command				= 2,
	iT_Menu 				= 3,
	iT_EditDisplay 			= 4,
	iT_Method 				= 5,
	iT_Refresh /*relation*/	= 6,
	iT_Unit /*relation*/	= 7,
	iT_WaO 					= 8,
	iT_ItemArray 			= 9,
	iT_Collection 			= 10,
	iT_Block_B				= 11,
	iT_Block 				= 12,
	iT_Program 				= 13,
	iT_Record 				= 14,
	iT_Array 				= 15,
	iT_VariableList 		= 16,
	iT_ResponseCodes 		= 17,
	iT_Domain 				= 18,
	iT_Member 				= 19,
	iT_File 				= 20,
	iT_Chart 				= 21,
	iT_Graph 				= 22,
	iT_Axis 				= 23,
	iT_Waveform 			= 24,
	iT_Source 				= 25,
	iT_List 				= 26,
	iT_Grid 				= 27,
	iT_Image 				= 28,
	iT_BLOB 				= 29,
	iT_Plugin 				= 30,
	iT_Template 			= 31,
	iT_Reserved				= 32,
	iT_Component			= 33,
	iT_Component_folder		= 34,
	iT_Component_reference	= 35,
	iT_Component_relation	= 36,
	iT_MaxType 				= 37,
/*
 *	Special object type values
 */
	iT_FormatObject			= 128,
	iT_DeviceDirectory 		= 129,
	iT_BlockDirectory 		= 130,
/* these additional ITYPES are used by DDS
 *       'resolve' function to build resolve trails 
 */
	iT_Parameter			= 200,
	iT_ParameterList 		= 201,
	iT_BlockCharacteristic 	= 202,

	iT_NotAnItemType		= 255,	//  probably a constant
	iT_Critical_Item		= 256

}  /*typedef*/ itemType_t;

const wchar_t* GetItemTypeString(itemType_t type);


typedef enum referenceType_e
{	
	rT_Item_id,			// 0___undocumented					// to ZERO
	rT_ItemArray_id,		// 1								// to ITEM_ARRAY_ITYPE
	rT_Collect_id,		// 2								// to COLLECTION_ITYPE
	rT_viaItemArray,	// 3  exp-index, ref-array			// to  ITEM_ARRAY_ITYPE
	rT_via_Collect,		// 4  member-itemId, ref-collection // to  COLLECTION_ITYPE
	rT_via_Record,		// 5  member-itemId, ref-record 	// to  RECORD_ITYPE,
	rT_via_Array,		// 6  exp-index, ref-array			// to  ARRAY_ITYPE,
	rT_via_VarList,		// 7  member-itemId, ref-var list	// to  VAR_LIST_ITYPE,
	rT_via_Param,		// 8  int: parameter member id		// to  PARAM_ITYPE,-------special  200
	rT_via_ParamList,	// 9  int: param-list-name			// to  PARAM_LIST_ITYPE---special  201
	rT_via_Block,		//10  int: member name				// to  BLOCK_ITYPE,<BLOCK_CHAR_ITYPE in resolve>-special 202
	rT_Block_id,		//11								// to BLOCK_ITYPE,
	rT_Variable_id,		//12								// to VARIABLE_ITYPE,
	rT_Menu_id,			//13								// to MENU_ITYPE,
	rT_Edit_Display_id,	//14								// to EDIT_DISP_ITYPE,
	rT_Method_id,		//15								// to METHOD_ITYPE,
	rT_Refresh_id,		//16								// to REFRESH_ITYPE,
	rT_Unit_id,			//17								// to UNIT_ITYPE,
	rT_WAO_id,			//18								// to WAO_ITYPE,
	rT_Record_id,		//19								// to RECORD_ITYPE,
	rT_Array_id,		//20___undocumented					// to ARRAY_ITYPE,
	rT_VarList_id,		//21___undocumented					// to VAR_LIST_ITYPE,
	rT_Program_id,		//22___undocumented					// to PROGRAM_ITYPE,
	rT_Domain_id,		//23___undocumented					// to DOMAIN_ITYPE,
	rT_ResponseCode_id,	//24___undocumented					// to RESP_CODES_ITYPE

	/* new reference types */
	rT_File_id,
	rT_Chart_id,
	rT_Graph_id,
	rT_Axis_id,
	rT_Waveform_id,
	rT_Source_id,		//30
	rT_List_id,

	rT_Image,
	rT_Separator,		/* == colbreak */
	rT_Constant,
	rT_via_File,		// 35
	rT_via_List,
	rT_via_BitEnum,
	rT_Grid_id,
	rT_Row_Break,
	rT_via_Chart,		// 40
	rT_via_Graph,
	rT_via_Source,
	rT_via_Attr,
	rT_NotA_RefType		// 44
}
/*typedef*/referenceType_t;


/*=*=*=*=*=*=*=*=*=*=*=*= Graphics Support =*=*=**=*=*=*=*=*=*=*=*/
/*
 *  Types of ...stuff
 *
 */
/*Vibhor 071204: Start of code*/
typedef enum actionType_e
{
	 eT_actionInit = 0
	,eT_actionRefresh
	,eT_actionExit
}	actionType_t;
/*Vibhor 071204: End of code*/


// ResolveType enum is defined for resolve builtin use.
enum ResolveType
{
	RESOLVE_NONE			= 0,
	RESOLVE_LOCAL_REF		= 1,
	RESOLVE_PARAM_LIST_REF	= 2,
	RESOLVE_PARAM_REF		= 3,
	RESOLVE_BLOCK_REF		= 4,
};


/*=*=*=*=*=*=*=*=*=*=* Parameter Reference Structure (copied from Attrs.h) *=**=*=*=*=*=*=*=*=*/

	enum ParamRefType
	{
			STANDARD_TYPE	= 0,
			COMPLEX_TYPE	= 1
	};

	class PARAM_REF_INFO
	{
	public:
		unsigned long		id;
		unsigned long		member;
		itemType_t			type;

		PARAM_REF_INFO()
		{
			id		= 0;
			member	= 0;
			type	= iT_ReservedZeta;
		};
		~PARAM_REF_INFO() 
		{
			id		= 0;
			member	= 0;
			type	= iT_ReservedZeta;
		};

        //Overloaded Assignment Operator 
		PARAM_REF_INFO& operator= (const PARAM_REF_INFO &op_ref_info)
        {
            id      = op_ref_info.id;
            member  = op_ref_info.member;
            type    = op_ref_info.type;
            return * this;
        }

	};

	class PARAM_REF_INFO_LIST
	{
	public:
		unsigned short  count;
		PARAM_REF_INFO  *list;

		PARAM_REF_INFO_LIST()
		{
			count	= 0;
			list	= nullptr;
		}

		//Copy Constructor 
		PARAM_REF_INFO_LIST(const PARAM_REF_INFO_LIST &op_ref_info_list);

		//Overloaded Assignment Operator 
		PARAM_REF_INFO_LIST& operator= (const PARAM_REF_INFO_LIST &op_ref_info_list);

		~PARAM_REF_INFO_LIST();

    private:

        void AssignParamRefInfoList( const PARAM_REF_INFO_LIST &op_ref_info_list );
	};

	class PARAM_REF {
	public:

		ParamRefType		op_ref_type;
		PARAM_REF_INFO		op_info;
		PARAM_REF_INFO_LIST	op_info_list;
		unsigned long		ulBlockItemId;
		unsigned long		ulOccurrence;	
		
		PARAM_REF();
		//Copy Constructor 
		PARAM_REF(const PARAM_REF &op_ref);

		//Overloaded Assignment Operator 
		PARAM_REF& operator= (const PARAM_REF &op_ref);
		bool operator==(const PARAM_REF &op_ref);

		~PARAM_REF();

		void addOperationalID (unsigned long itemId, unsigned long memberId, itemType_t itemType);
		bool isOperationalIDValid();
		void addBlockID (unsigned long blockItemId, unsigned long occurrence);

	private:
		//AssignOp_Ref is used to assign all the attributes of OP_REF
		void AssignParam_Ref(const PARAM_REF &op_ref);
		bool bDirectRef;	//the value FALSE means that the instance only has initial value or tempted to add indirection item ID and member ID but rejected.
							//the value TRUE mean that the class instance has at least one pair of direct reference item ID and member ID.
	};


#endif // _DDBDEFS_H
 /*programming note:
   pStyleStr isa pointer to an array of const char arrays 
   const char (*pStyleStr)[MENUSTYLESTRCOUNT][MENUSTYLEMAXLEN] = &menuStyleStrings;
*/
