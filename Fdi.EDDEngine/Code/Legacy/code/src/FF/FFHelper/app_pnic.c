#ifndef lint
static char SCCSID[] = "@(#)app_pnic.c	30.3  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

#if defined(__BORLANDC__) || defined(_MSC_VER) || defined(SVR4)
#include	<stdarg.h>
#else
//TODO GNUC does not have varargs implemented
#include    <stdarg.h>
#endif

#include	<stdlib.h>
#include	<stdio.h>
#include    "panic.h"

#if defined(__BORLANDC__) || defined(_MSC_VER) || defined(SVR4)
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
void
panic (const char *file, unsigned int line, const char *format, ...)
{
	fprintf(stdout, "%s(%d):", file, line);

	// Everything below here is like the "panic" defined below
	va_list         ap;

	va_start(ap, format);

	(void) vfprintf(stdout, format, ap);

	va_end(ap);
	throw 1;
}

/* VARARGS */
void
panic(char *format, ...)
{
	va_list         ap;

	va_start(ap, format);

	(void) vfprintf(stdout, format, ap);

	va_end(ap);
	throw 1;
}

#else
/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
/* VARARGS */
void
panic (const char *file, unsigned int line, const char *format, ...)
{
    fprintf(stdout, "%s(%d):", file, line);

    // Everything below here is like the "panic" defined below
    va_list         ap;

    va_start(ap, format);

    (void) vfprintf(stdout, format, ap);

    va_end(ap);
    throw 1;
}

/* VARARGS */
void
panic(char *format, ...)
{
    va_list         ap;
    va_start(ap, format);

    (void) vfprintf(stdout, format, ap);

    va_end(ap);
    throw 1;
}

#endif
