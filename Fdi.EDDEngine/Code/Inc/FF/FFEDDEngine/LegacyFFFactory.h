#pragma once

#include "EDDEngineImpl.h"


#if defined (EXPORT_LEGACY_FF_API)
	#define __LEGACY_FF_API __declspec(dllexport)
#else
	#define __LEGACY_FF_API __declspec(dllimport)
#endif


class __LEGACY_FF_API LegacyFFFactory
{
public: 
	static CEDDEngineImpl* CreateLegacyFFDDS(const wchar_t *sEDDBinaryFilename, 
		const wchar_t* pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode);
private:
	LegacyFFFactory& operator=(const LegacyFFFactory &rhs);
};
