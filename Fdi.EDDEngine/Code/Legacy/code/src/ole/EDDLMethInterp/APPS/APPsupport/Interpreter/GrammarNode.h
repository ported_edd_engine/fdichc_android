
#if !defined(INCLUDE_BA3E4BD7_51E6_4BFF_8877_A4F53533F0DB)
#define INCLUDE_BA3E4BD7_51E6_4BFF_8877_A4F53533F0DB

#include "LexicalAnalyzer.h"
#include "PlatformCommon.h"

class CGrammarNodeVisitor;
class CSymbolTable;
class INTER_VARIANT;
class CRIDEError;

// The return codes for the Execute function calls
#define		VISIT_BREAK		1	//used by switch statement
#define		VISIT_CONTINUE	2	//used by switch statement
#define		VISIT_RETURN	3	//used by return statement in method calling method
#define		VISIT_NORMAL	4	//success

//Anil 240107 Defined this when declaration come after some statement list
//Basically for the scope of the variable
#define     VISIT_SCOPE_VAR 5
#define		VISIT_ABORT		6	//abort builtin is executed
#define		VISIT_ERROR		7	//expression is not able to be resolved


enum GRAMMAR_NODE_TYPE
{
	NODE_TYPE_INVALID
	, NODE_TYPE_ASSIGN
	, NODE_TYPE_EXPRESSION
};

class CGrammarNode  
{
private:
	GRAMMAR_NODE_TYPE	m_NodeType;
	SCOPE_STACK			m_sStmtScope;

public:
	CGrammarNode();
	virtual ~CGrammarNode();

//	Identify self
	virtual void Identify(
		_CHAR* szData);

	virtual _INT32 Execute(
		CGrammarNodeVisitor*	pVisitor,
		CSymbolTable*			pSymbolTable,
		INTER_VARIANT*		pvar=0,
		ERROR_VEC*				pvecErrors=0,
		RUL_TOKEN_SUBTYPE	AssignType = RUL_ASSIGN	);//Anil August 26 2005 to Fix a[exp1] += exp2

	virtual _INT32 CreateParseSubTree(
		CLexicalAnalyzer*	plexAnal, 
		CSymbolTable*		pSymbolTable,
		ERROR_VEC*			pvecErrors);

	virtual _INT32 GetLineNumber();

	void SetNodeType(GRAMMAR_NODE_TYPE nodeType)
	{
		m_NodeType = nodeType;

	}

	GRAMMAR_NODE_TYPE GetNodeType()
	{
		return m_NodeType;
	}

		//This will return the nested depth of the symbols---Felix
	SCOPE_STACK GetStmtScopeStack();
	void SetStmtScopeStack(SCOPE_STACK sScope);

	bool isErrorIn(_INT32 ErrNumber, ERROR_VEC*	pvecErrors);

};

class CRIDEError
{
private:
	char m_psRuleName[RULENAMELEN];
	_INT32 m_iMIErrorNum;

public:
    CRIDEError()
	{
		m_psRuleName[0] = '\0';
		m_iMIErrorNum = 0;
	}

    CRIDEError(char *ErrMsg, _INT32 ErrNo)
	{
		m_iMIErrorNum = ErrNo;

		if (strlen(ErrMsg) > 0)
		{
			PS_Strcpy(m_psRuleName, RULENAMELEN, ErrMsg);
			PS_Strcat(m_psRuleName, RULENAMELEN, ": ");
		}
		else
		{
			//initialize memory in m_psRuleName
			m_psRuleName[0] = '\0';
		}

		switch(m_iMIErrorNum)
		{
		case C_UM_ERROR_UNKNOWNERROR:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Unknown error.");
			break;
		case C_UM_ERROR_LOWMEMORY:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Low memory.");
			break;
		case C_UM_ERROR_METHODERROR:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Missing a statement.");
			break;
		case C_UM_ERROR_MISSINGTOKEN:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Token error.");
			break;
		case C_UM_ERROR_COMM_ABORTED:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to command communication failure.");
			break;
		case C_UM_ERROR_COMM_RETRY_ABORTED:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to command retry limit exceeded.");
			break;
		case C_UM_ERROR_COMM_NO_DEVICE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to device not responding.");
			break;
		case C_UM_ERROR_COMM_FAIL_RESPONSE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to command response code or device status.");
			break;
		case C_UM_ERROR_INDEX_OUT_OF_RANGE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Array index out of range.");
			break;
		case C_UM_ERROR_VAR_ABORTED:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to invalid variable.");
			break;
		case C_UM_ERROR_OPER_ABORTED:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Aborting due to illegal operation.");
			break;
		case C_UM_ERROR_UI_CANCELLED:
			m_psRuleName[0] = '\0';				//none message so that the AbortRequest() is not called
			break;
		case C_UM_ERROR_METH_CANCELLED:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Interface function IMethods::CancelMethod() has been called.");
			break;
		case C_UM_ERROR_DDS_PARAM:
			PS_Strcat(m_psRuleName, RULENAMELEN, "DDS error or wrong parameter type.");
			break;

		case C_LEX_ERROR_IDLONG:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Lexical analyzer identifier too long.");
			break;
		case C_LEX_ERROR_ILLEGALCHAR:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Lexical analyzer illgal character.");
			break;
		case C_LEX_ERROR_ILLEGALITEM:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Lexical analyzer non-method and non-enumeration item with parenthesis.");
			break;

		case C_AP_ERROR_LVALUE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Assignment parser l-value error.");
			break;
		case C_AP_ERROR_MISSINGEQ:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Assignment parser missing equal.");
			break;
		case C_AP_ERROR_MISSINGEXP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Assignment parser missing expression.");
			break;
		case C_AP_ERROR_MISSINGSC:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Assignment parser missing semicolon.");
			break;

		case C_EP_ERROR_ILLEGALOP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Expression parser illegal operation.");
			break;
		case C_EP_ERROR_MISSINGOP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Expression parser missing operation.");
			break;
		case C_EP_ERROR_MISSINGPAREN:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Expression parser missing parenthesis.");
			break;
		case C_EP_ERROR_MISSINGSC:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Expression parser missing semicolon.");
			break;
		case C_EP_ERROR_LEXERROR:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Lexical analyzer error.");
			break;

		case C_IF_ERROR_MISSINGLP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Selection statement missing left parenthesis.");
			break;
		case C_IF_ERROR_MISSINGEXP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Selection statement missing an expression.");
			break;
		case C_IF_ERROR_MISSINGRP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Selection statement missing right parenthesis.");
			break;
		case C_IF_ERROR_MISSINGSTMT:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Selection statement missing a statement.");
			break;

		case C_WHILE_ERROR_MISSINGLP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Iteration statement missing left parenthesis.");
			break;
		case C_WHILE_ERROR_MISSINGEXP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Iteration statement missing an expression.");
			break;
		case C_WHILE_ERROR_MISSINGRP:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Iteration statement missing right parenthesis.");
			break;
		case C_WHILE_ERROR_MISSINGSTMT:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Iteration statement missing a statement.");
			break;

		case C_CS_ERROR_MISSINGRBRACK:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Compound statement missing right curly bracket.");
			break;

		case C_ES_ERROR_MISSINGSTMT:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Else statement error.");
			break;

		case C_RS_ERROR_MISSINGSCOPE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing scope.");
			break;
		case C_RS_ERROR_MISSINGINVOKE:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing invoke.");
			break;
		case C_RS_ERROR_MISSINGLPAREN:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing left parenthesis.");
			break;
		case C_RS_ERROR_MISSINGRNAME:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing name.");
			break;
		case C_RS_ERROR_MISSINGRPAREN:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing right parenthesis.");
			break;
		case C_RS_ERROR_MISSINGSC:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Rule service statement missing semicolon.");
			break;


		case C_DECL_ERROR_EXPRESSION:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations expression error.");
			break;
		case C_DECL_ERROR_LBOX:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations left bracket error.");
			break;
		case C_DECL_ERROR_RBOX:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations right bracket error.");
			break;
		case C_DECL_ERROR_NUM:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations number error in brackets.");
			break;
		case C_DECL_ERROR_IDMISSING:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations identifier missing.");
			break;
		case C_DECL_ERROR_COMMAMISSING:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations comma missing.");
			break;
		case C_DECL_ERROR_UNKNOWN:
			PS_Strcat(m_psRuleName, RULENAMELEN, "Declarations unknown error.");
			break;

		}
	}

    ~CRIDEError()
	{
	}

	char* getRuleName() { return m_psRuleName;}
	_INT32 getErrorNumber() { return m_iMIErrorNum;}

};

#endif
