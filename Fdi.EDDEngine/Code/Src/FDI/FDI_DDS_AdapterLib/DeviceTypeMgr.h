#pragma once

#include "RodMgr.h"
#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"
#include "sym.h"
#include <mutex>

//ClockDeviceTypeMgr is used to make the shared data in DeviceTypeMgr thread-safe.
//Use the function by declaring a local variable in sections of code that need to be protected:
//For example, CLockTypeDeviceTypeMgr dtLockDeviceTypeMgr;
//The code makes the functions threadsafe by locking a private member variable m_cs in the DeviceTypeMgr.
class CLockDeviceTypeMgr 
{
public:
	CLockDeviceTypeMgr();
	~CLockDeviceTypeMgr();
};

//global DeviceTypeMgr class for FDI sharing

class CDeviceTypeMgr
{
	friend class CLockDeviceTypeMgr;

public:
	CDeviceTypeMgr();
	~CDeviceTypeMgr();

	DD_DEVICE_ID* ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID* ddi);

	void ADTT_DEVICE_NAMES(DEVICE_TYPE_HANDLE dth, CStdString &strMfgName, CStdString &strDevTypeName);
	void SET_ADTT_DEVICE_NAMES(DEVICE_TYPE_HANDLE dth, const CStdString &strMfgName, const CStdString &strDevTypeName);

	ULONG ADTT_ALARM_BLOCK_INDEX(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_ALARM_BLOCK_INDEX(DEVICE_TYPE_HANDLE dth, ULONG ulAlarmBlockIndex);

	ROD_HANDLE ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth, ROD_HANDLE ddh);

	SYMINFO* ADTT_SYMBOL_INFO(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_SYMBOL_INFO(DEVICE_TYPE_HANDLE dth, SYMINFO* pSymInfo);

	int ADTT_USAGE(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_USAGE(DEVICE_TYPE_HANDLE dth, int u);

	CM_COMPAT ADTT_COMPATIBILITY(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_COMPATIBILITY(DEVICE_TYPE_HANDLE dth, CM_COMPAT c);

	nsEDDEngine::FLAT_DEVICE_DIR* ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth, nsEDDEngine::FLAT_DEVICE_DIR* ddt);

	int get_adtt_symbol_info (DEVICE_TYPE_HANDLE dth, SYMINFO **pSymInfo);
	int set_adtt_symbol_info (DEVICE_TYPE_HANDLE dth, SYMINFO *pSymInfo);

	int get_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void **ddt);
	int set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void *ddt);
	
	int get_adtt_dd_device_id (DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi);

	int valid_device_type_handle (DEVICE_TYPE_HANDLE);

	int get_adtt_dd_handle (DEVICE_TYPE_HANDLE dth, ROD_HANDLE *ddh);

	int CreateActiveDeviceTypeTableEntry(DEVICE_TYPE_HANDLE *deviceTypeHandle);

	DEVICE_TYPE_HANDLE ct_device_type_search(DD_DEVICE_ID *dd_device_id);

	void ct_remove_device_type(DEVICE_TYPE_HANDLE device_type_handle);

	//Remote Object Dictionary(ROD) wrapper functions.
	int rod_close(ROD_HANDLE rod_handle);
	int rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
	unsigned long rod_get_version(ROD_HANDLE rhandle);
	int ds_ddod_file_load(ENV_INFO *env_info,/* fd_fdi */wchar_t *filename,  ROD_HANDLE *rod_handle);
	int VALID_ROD_HANDLE(ROD_HANDLE rh);
	int ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc);
private:
	
	 std::recursive_mutex m_cs;
	ACTIVE_DEV_TYPE_TBL active_dev_type_tbl;
	CRodMgr m_RodMgr;
};

//global shared device type manager
extern CDeviceTypeMgr g_DeviceTypeMgr;