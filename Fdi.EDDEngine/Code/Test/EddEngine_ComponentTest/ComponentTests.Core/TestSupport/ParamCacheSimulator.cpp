#include "ParamCacheSimulator.h"
#include <iostream>
#include <sstream>

using namespace nsEDDEngine;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            ParamCacheSimulator::ParamCacheSimulator() :
            _Convenience(nullptr), _ParameterValues()
            {
            };
            
            ParamCacheSimulator::~ParamCacheSimulator()
            {
             
            }
            
            void ParamCacheSimulator::SetEddEngine(nsEDDEngine::IConvenience *convenience)
            {
                _Convenience = convenience;
            }
            
            /// Sets the value of a specific parameter in the Parameter Chache.
            /// 
            /// This function takes ownership of the paramValue parameter.
            /// If paramValue is nullptr, the parameter is regarded as not available.
            void ParamCacheSimulator::SetParamValue(
                nsEDDEngine::FDI_PARAM_SPECIFIER *paramSpecifier,
                nsConsumer::EVAL_VAR_VALUE *paramValue)
            {
                _ParameterValues[ParamSpecifierToString(paramSpecifier)] = paramValue;
            }
            
            nsConsumer::PC_ErrorCode ParamCacheSimulator::GetParamValue(
                int iBlockInstance,
                void* pValueSpec,
                nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec,
                nsConsumer::EVAL_VAR_VALUE * pParamValue)
            {
                // remember the requested parameters...
                RequestedParameters.push_back(*pParamSpec);

                // check pParamValue argument
                if (pParamValue == NULL)
                {
                    return nsConsumer::PC_INVALID_EC;
                }

                // try to get the parameter value from the internal cache
                ParameterValueMap::iterator valueIter = _ParameterValues.find(ParamSpecifierToString(pParamSpec));
                if (valueIter != _ParameterValues.end())
                {
                    nsConsumer::EVAL_VAR_VALUE *paramValue = valueIter->second;
                    if (paramValue == nullptr)
                    {
                        return nsConsumer::PC_INVALID_EC;
                        //return -1;
                    }
                    else
                    {
                        *pParamValue = *paramValue;
                        return nsConsumer::PC_SUCCESS_EC;
                    }
                }
                // parameter value is not found in the internal cache, so we return the default value

                // Get the variable type by calling IConvenience::GetParamType
                TYPE_SIZE TypeSize;

                int rcode = _Convenience->GetParamType(0, pParamSpec, &TypeSize );
                if (rcode != 0)
                {
                    return nsConsumer::PC_INVALID_EC;
                }

                // set EVAL_VAR_VALUE::type and EVAL_VAR_VALUE::size
                pParamValue->type = TypeSize.type;
                pParamValue->size = TypeSize.size;

                switch (pParamValue->type)
                {
                case VT_INTEGER:
                    pParamValue->val.i = 1;
                    break;
                case VT_UNSIGNED:
                case VT_ENUMERATED:
                case VT_BIT_ENUMERATED:
                case VT_INDEX:
                    pParamValue->val.u = 0;
                    break;
                case VT_FLOAT:
                    pParamValue->val.f = 1.0;
                    break;
                case VT_DOUBLE:
                    pParamValue->val.d = 1.0;
                    break;
                case VT_ASCII:
                case VT_PACKED_ASCII:
                case VT_PASSWORD:
                case VT_EUC:  
                case VT_BITSTRING:
                    pParamValue->val.s.assign(L"",false);
                    break;
                    //case VT_HART_DATE_FORMAT:
                case VT_DATE_AND_TIME:
                case VT_DURATION:
                    break;
                default:
                    break;
                }
                return nsConsumer::PC_SUCCESS_EC;
            }

		    nsConsumer::PC_ErrorCode ParamCacheSimulator::GetDynamicAttribute(int /*iBlockInstance*/, void* pValueSpec, const nsEDDEngine::OP_REF* op_ref,
                const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue)
            {
                return nsConsumer::PC_ErrorCode::PC_INVALID_EC;
            }

            
          	nsConsumer::PC_ErrorCode ParamCacheSimulator::SetDynamicAttribute( int /*iBlockInstance*/, void* /*pValueSpec*/, const nsEDDEngine::OP_REF *pOpRef,
					const nsEDDEngine::AttributeName /*attributeName*/, const nsConsumer::EVAL_VAR_VALUE *pParamValue)
			{
				return nsConsumer::PC_ErrorCode::PC_SUCCESS_EC;
			}
            
            std::string ParamCacheSimulator::ParamSpecifierToString(nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier)
            {
                std::stringstream result;
            
                if (paramSpecifier == nullptr)
                {
                    throw "paramSpecifier is nullptr";
                }
            
                switch (paramSpecifier->eType)
                {
                case FDI_PS_ITEM_ID:
                    result << "ITEM_ID:" << paramSpecifier->id;
                    break;
                case FDI_PS_ITEM_ID_SI:
                    result << "ITEM_ID_SI:" << paramSpecifier->id << ":" << paramSpecifier->subindex;
                    break;
                case FDI_PS_PARAM_OFFSET:
                    result << "PARAM_OFFSET:" << paramSpecifier->param;
                    break;
                case FDI_PS_PARAM_OFFSET_SI:
                    result << "PARAM_OFFSET_SI:" << paramSpecifier->param << ":" << paramSpecifier->subindex;
                    break;
                default:
                    throw "FDI_PARAM_SPECIFIER type unknown";
                    break;
                }
            
                return result.str();
            }

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests
