/*************************************************************************************************
 *
 * $Workfile: varient.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c)2002 - 2008, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		home of the varient class
 *		4/5/2	sjv	created
 *
 * #include "varient.h"
 */

#ifndef _VARIENT_H
#define _VARIENT_H
#ifdef INC_DEBUG
#pragma message("In varient.h")
#endif

#ifdef _WIN32
#include <comutil.h>		//for CValueVarient type
#endif
#include "ddbGeneral.h"
#include "Char.h"
#include "ddbdefs.h"
#include "logging.h"
#include "typedefs.h"
#ifdef _WIN32
#include "varient.h"
#endif
#include "PlatformCommon.h"
#include "math.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::varient.h")
#endif

#define SUPPORT_LONGLONG	/* undefine for CE (among others) */

#define VARIENTTYPESTRING {"invalid"},{"Bool"},{"Opcode"},{"IntConst"},{"FloatConst"},\
		{"DependIndex"},{"String"},{"SymbolID"}
#define VARIENTYPECOUNT  8
#define VARIENTTYPE_LEN  13

extern const char varientTypeStrings[VARIENTYPECOUNT][VARIENTTYPE_LEN];

//I8 and UI8 do not work for ChangeType in Windows CE, so SafeChangeType is used in the place of ChangeType


#define BOOLTSIZE	1
#define OPCODESIZE	(sizeof(int))
#define INTSIZE		(sizeof(int))
#define DOUBLESIZE	(sizeof(double))
#define FLOATSIZE	(sizeof(float))
#define IDSIZE		(sizeof(int))
#define LLSIZE		(sizeof(__int64))

class CValueVarient
{
    char tmpBuf[40]{0};// for weird float conversions
public:
	typedef enum valueType_e
	{
		invalid,
		isBool,
		isOpcode,
		isIntConst,
		isFloatConst,
		isDepIndex,
		isSymID,
#ifdef SUPPORT_LONGLONG
		isVeryLong,
#endif
		isString,
                isWideString,
		isBitString
	}valueType_t;

	typedef enum valueTagType_e
	{
		CVT_BOOL,
		CVT_I1,
		CVT_I2,
		CVT_INT,
		CVT_I4,
		CVT_I8,
		CVT_UI1,
		CVT_UI2,
		CVT_UINT,
		CVT_UI4,
		CVT_UI8,
		CVT_R4,
		CVT_R8,
		CVT_STR,
		CVT_WSTR,
		CVT_BITSTR,
		CVT_EMPTY
	}valueTagType_t;

	union vValue_u
	{	
		bool				bIsTrue;    // constant T/F
		expressElemType_t   iOpCode;	// type 1 - 21
		int					iIntConst;	// type 22
		double				fFloatConst;// type 23 - float
		int					depIndex;   // type 24 through 29
		itemID_t            varSymbolID;// other uses like index into dependency list
#ifdef SUPPORT_LONGLONG
                long long				longlongVal;// undefined in DDs for now
#endif	
	} 
	/* union */  vValue;
	string				    sStringVal;
	wstring				    sWideStringVal;
    _BYTE_STRING         bsVal;		//unsgined char*
    valueType_t         vType;
    valueTagType_t      vTagType;
	int	         vSize;
	unsigned long vIndex;   /* only for bit-enum'd bit reference resolution */
							/* also holds 'which' for attribute id resolution*/
	bool		vIsValid;
	bool        vIsUnsigned;
	bool        vIsDouble;
	bool        vIsBit;	/* set when bit-Enum SymbolID:: vIndex is Valid */

	CValueVarient() : vSize(0),vIsValid(false),vType(invalid),vIsUnsigned(false),
					  vIsDouble(false),vIndex(0),vIsBit(false)	
	{ 
		vValue.fFloatConst=0.0;

		//BITSTRING initialization
		bsVal.bs = NULL;
		bsVal.bsLen = 0;
		vTagType = CVT_EMPTY;
	};
	CValueVarient(const CValueVarient& src)
	{ 
		//BITSTRING initialization
		bsVal.bs = NULL;
		bsVal.bsLen = 0;

		(*this) = src;
	};
	virtual ~CValueVarient(){clear();};

	CValueVarient& operator=(const CValueVarient& src) 
	{
		vType=src.vType;
		vTagType = src.vTagType;
		vValue=src.vValue; vIsUnsigned = src.vIsUnsigned;vIsDouble=src.vIsDouble;
		if ( ! src.sStringVal.empty() ) 
		{ 
			sStringVal = (src.sStringVal.c_str());
		}
		else
		{
			sStringVal.erase();
		}
		if ( ! src.sWideStringVal.empty() )
		{ 
			sWideStringVal = (src.sWideStringVal.c_str());
		}
		else
		{
			sWideStringVal.erase();
		}
		vSize=src.vSize; vIsValid=src.vIsValid;vIndex=src.vIndex;vIsBit=src.vIsBit;
		if (src.vType == isBitString)
		{
			*this = bitStringAssign(src.bsVal.bs, src.bsVal.bsLen);
		}
		return *this;
	}
	
	void clear(void)
	{
		vType = invalid; vSize = 0; vValue.fFloatConst = 0.; vIndex= 0;
		vIsValid=false; vIsUnsigned= false; vIsDouble= false; vIsBit=false;
		sWideStringVal.erase(); sStringVal.erase();
		if (bsVal.bsLen > 0)
		{
			delete [] bsVal.bs;
			bsVal.bs = nullptr;
			bsVal.bsLen = 0;
		}
	};

	bool valueIsZero(void) 
	{    if (vType == isBool)       return ((vValue.bIsTrue)?false:true);
	else if (vType == isIntConst)   return ((vValue.iIntConst == 0)?true:false);
	else if (vType == isFloatConst) return ((vValue.fFloatConst == 0.0)?true:false);
	else if (vType == isSymID)      return ((vValue.varSymbolID == 0)?true:false);
	else return ((bool) -1);
	};

	bool isNumeric()
	{	if ( (vType == isBool)       || (vType == isIntConst)  || 
			 (vType == isFloatConst) || (vType == isVeryLong)  )
		{	return true;
		}
		else
		{	return false;
		}
	};

	CValueVarient& bitStringAssign(const unsigned char *src, unsigned long size) 
	{
		//clear bs
		if (bsVal.bsLen > 0)
		{
			delete [] bsVal.bs;
			bsVal.bs = NULL;
			bsVal.bsLen = 0;
		}

		if ( (size > 0) && (src != nullptr) )
		{
			vType = isBitString;
			vSize       = (int)size;
			bsVal.bs = new uchar[size];
			memcpy(bsVal.bs, src, size);
			bsVal.bsLen = size;
			vIsValid    = true; 
			vIsUnsigned = false;
			vIsDouble   = false;
			vIsBit      = false;
			vIndex      = 0;
		}

		return *this;
	};

	//cast operators
	//////////////
    operator bool(void)
	{	bool bVal;

		switch(vType)
		{
		case isBool:
			{	bVal = vValue.bIsTrue;  }							break;
		case isIntConst:
			{	bVal = ((vValue.iIntConst)? true : false);}			break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{	bVal = ((vValue.longlongVal)? true : false); }		break;
#endif
		case isFloatConst:
			{		bVal = ((vValue.fFloatConst)? true : false); }	break;
		case isString:
			{	bVal = ((sStringVal.size())? true : false); }		break;
		case isWideString:
			{	bVal = ((sWideStringVal.size())? true : false); }	break;
		case isSymID:	/* aka unsigned long */
			{	bVal = ((vValue.varSymbolID)? true : false);}		break;
		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				bVal = false; // error.
			}
		}
		return bVal;
	};
	
	//////////////
	operator char(void)
	{	char cVal;

		switch(vType)
		{
		case isBool:
			{	cVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					cVal = ((unsigned char)vValue.iIntConst);      // auto cast to char
				}
				else
				{
					cVal =(char) vValue.iIntConst;      // auto cast to char
				}
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if ( vIsUnsigned )
				{
					cVal = (unsigned char)vValue.longlongVal;  // auto cast to char
				}
				else
				{
					cVal = (char) vValue.longlongVal;    // auto cast to char
				}
			}	
			break;
#endif
		case isFloatConst:
			{	cVal =  (char) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%hhd", &cVal ) <= 0) {  cVal = 0;  }
#else
				if ( sscanf(sStringVal.c_str(),"%d",&cVal) <= 0) {  cVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                                if ( wscanf( sWideStringVal.c_str(), L"%hhd", &cVal ) <= 0) {  cVal = 0;  }
			} break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		case isSymID:
		default:
			{
				cVal = 0; // error.
			}
		}
		return cVal;
	};
	operator unsigned char(void)
	{	unsigned char uVal;

		switch(vType)
		{
		case isBool:
			{	uVal = ((vValue.bIsTrue)? 1 : 0); }			break;
		case isIntConst:
			{	uVal = (unsigned char)  vValue.iIntConst;}	break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					uVal = (unsigned char)vValue.longlongVal; // auto cast to unsigned char
				}
				else
				{
					uVal = (unsigned char) vValue.longlongVal;  // auto cast to unsigned
				}
			}
			break;
#endif
		case isFloatConst:
			{	uVal =  (unsigned char) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%hhu", &uVal ) <= 0) {  uVal = 0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%u",&uVal) <= 0) {  uVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%hhu",&uVal ) <= 0) {  uVal = 0;  }
			} break;
		case isSymID:
			{	uVal = (unsigned char)  vValue.varSymbolID;  } break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				uVal = 0; // error.
			}
		}
		return uVal;
	};
	operator short(void)
	{	short cVal;

		switch(vType)
		{
		case isBool:
			{	cVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					cVal = ((short)vValue.iIntConst);      // auto cast to short
				}
				else
				{
					cVal =(short) vValue.iIntConst;      // auto cast to short
				}
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if ( vIsUnsigned)
				{
					cVal = (short)vValue.longlongVal;  // auto cast to short
				}
				else
				{
					cVal = (short) vValue.longlongVal;    // auto cast to short
				}
			}
			break;
#endif
		case isFloatConst:
			{	cVal =  (short) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%hd", &cVal ) <= 0) {  cVal = 0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%d",&cVal) <= 0) {  cVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%hd", &cVal) <= 0 ) {  cVal = 0;  }
			} break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		case isSymID:
		default:
			{
				cVal = 0; // error.
			}
		}
		return cVal;
	};
	operator unsigned short(void)
	{	unsigned short uVal;

		switch(vType)
		{
		case isBool:
			{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{	uVal = (unsigned short)  vValue.iIntConst;  } break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					uVal = (unsigned short)vValue.longlongVal; // auto cast to unsigned short
				}
				else
				{
					uVal = (unsigned short) vValue.longlongVal;  // auto cast to unsigned
				}
			}
			break;
#endif
		case isFloatConst:
			{	uVal =  (unsigned short) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%hu", &uVal ) <= 0) {  uVal = 0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%u",&uVal) <= 0) {  uVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%hu", &uVal ) <= 0) {  uVal = 0;  }
			} break;

		case isSymID:
			{	uVal = (unsigned short)  vValue.varSymbolID;  } break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				uVal = 0; // error.
			}
		}
		return uVal;
	};
	operator double(void)
	{	double fVal;

		switch(vType)
		{
		case isBool:
			{	fVal = ((vValue.bIsTrue)? 1.0 : 0.0); } break;
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					fVal = (double) ((unsigned int)vValue.iIntConst);  
				}
				else
				{
					fVal = (double) vValue.iIntConst;  
				}
			} 	break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if ( vIsUnsigned)
				{
					fVal = (double) vValue.longlongVal;  
					if ( (vValue.longlongVal & 0x8000000000000000) && fVal < 0 )
					{	fVal *= -1;    fVal += 0x8000000000000000;   }
				}
				else
				{
					fVal = (double) vValue.longlongVal;  
				}
			}
			break;
#endif
		case isFloatConst:
			{	fVal =          vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%lf", &fVal ) <= 0) {  fVal = 0.0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%f",&fVal) <= 0) {  fVal = 0.0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%lf", &fVal ) <= 0) {  fVal = 0;  }
			} break;


		case invalid:
		case isOpcode:
		case isDepIndex:
		case isSymID:
		default:
			{
				fVal = 0.0; // error.
			}
		}
		return fVal;
	};
	operator float(void)
	{	float fVal;

		switch(vType)
		{
		case isBool:
			{	fVal = (float)((vValue.bIsTrue)? 1.0 : 0.0); } break; // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					fVal = (float) ((unsigned int)vValue.iIntConst);  
				}
				else
				{
					fVal = (float) vValue.iIntConst;  
				}
			}	break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					fVal = (float) vValue.longlongVal;  
					if ( (vValue.longlongVal & 0x8000000000000000) && fVal < 0 )
					{	fVal *= -1;    fVal += 0x8000000000000000;   }
				}
				else
				{
					fVal = (float) vValue.longlongVal;  
				}
			}
			break;
#endif
		case isFloatConst:
			{	fVal =  (float)vValue.fFloatConst;} break;  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		case isString:
			{// never works::	if ( sscanf(sStringVal.c_str(),"%f",&fVal) <= 0) {  fVal = 0.0;  }
				fVal = (float)atof(sStringVal.c_str()); // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
			} break;
		case isWideString:
			{
                fVal = (float)wcstof(sWideStringVal.c_str(),nullptr);
			} break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		case isSymID:
		default:
			{
				fVal = 0.0; // error.
			}
		}
		return fVal;
	};
	//////////////
    operator int(void)
	{	int iVal;

		switch(vType)
		{
		case isBool:
			{	iVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					iVal = ((unsigned int)vValue.iIntConst);  
				}
				else
				{
					iVal = vValue.iIntConst;  
				}
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					iVal = (unsigned int)vValue.longlongVal;  
				}
				else
				{
					iVal = (int) vValue.longlongVal;  
				}
			}
			break;
#endif
		case isSymID:	/* aka unsigned long */
			{	iVal = (unsigned int)  vValue.varSymbolID;  } break;
		case isFloatConst:
			{	iVal =  (int) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%d", &iVal ) <= 0) {  iVal = 0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%d",&iVal) <= 0) {  iVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%d", &iVal ) <= 0) {  iVal = 0;  }
			} break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				iVal = 0; // error.
			}
		}
		return iVal;
	};
	//////////////
    operator unsigned int(void)
	{	unsigned int uVal;

		switch(vType)
		{
		case isBool:
			{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				uVal = (unsigned int)  vValue.iIntConst;  
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					uVal = (unsigned int)vValue.longlongVal;  
				}
				else
				{
					uVal = (int) vValue.longlongVal;  // auto cast to unsigned
				}
			}
			break;
#endif
		case isFloatConst:
			{	uVal =  (unsigned int) vValue.fFloatConst;} break;
		case isString:
			{
#if _MSC_VER >= 1400	/* WS 9apr07 VS2005 checkin */
				if ( sscanf_s( sStringVal.c_str(), "%u", &uVal ) <= 0) {  uVal = 0;  }
#else				
				if ( sscanf(sStringVal.c_str(),"%u",&uVal) <= 0) {  uVal = 0;  }
#endif				
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%u", &uVal ) <= 0) {  uVal = 0;  }
			} break;
		case isSymID:	/* aka unsigned long */
			{	uVal = (unsigned int)  vValue.varSymbolID;  } break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				uVal = 0; // error.
			}
		}
		return uVal;
        }
	
    operator unsigned long(void)
	{	unsigned long uVal = (unsigned int)(*this);
		return uVal;
        }
#ifdef SUPPORT_LONGLONG
        operator long long(void)
        {	long long iVal;

		switch(vType)
		{
		case isBool:
			{	iVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				if (vIsUnsigned)
				{
					iVal = ((unsigned int)vValue.iIntConst);   // autocast 2 longlong
				}
				else
				{
					iVal = vValue.iIntConst;    // autocast
				}
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{	iVal = vValue.longlongVal;  // autocast
			}
			break;
#endif
		case isFloatConst:
                        {	iVal =  (long long) vValue.fFloatConst;} break;
		case isString:
            {	if ( sscanf( sStringVal.c_str(),"%lld", &iVal ) <= 0) {  iVal = 0;  }
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%lld", &iVal ) <= 0) {  iVal = 0;  }
			} break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		case isSymID:
		default:
			{
				iVal = 0; // error.
			}
		}
		return iVal;
	};
        operator unsigned long long(void)
        {	unsigned long long uVal;

		switch(vType)
		{
		case isBool:
			{	uVal = ((vValue.bIsTrue)? 1 : 0); } break;
		case isIntConst:
			{
				uVal = vValue.iIntConst;  
			} break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (vIsUnsigned)
				{
					uVal = ((unsigned long long)vValue.longlongVal);   // autocast 2 longlong
				}
				else
				{
					uVal = vValue.longlongVal;    // autocast
				}
			}
			break;
#endif
		case isFloatConst:
			{	uVal =  (unsigned  long long) vValue.fFloatConst;} break;
		case isString:
            {	if ( sscanf( sStringVal.c_str(), "%llu", &uVal ) <= 0) {  uVal = 0;  }
			} break;
		case isWideString:
			{
                if ( wscanf( sWideStringVal.c_str(), L"%llu", &uVal ) <= 0) {  uVal = 0;  }
			} break;
		case isSymID:
			{	uVal = (unsigned int)  vValue.varSymbolID;  } break;

		case invalid:
		case isOpcode:
		case isDepIndex:
		default:
			{
				uVal = 0; // error.
			}
		}
		return uVal;
	};
#endif
	/////////////////
	// use unsigned int for this
	//operator itemID_t(void)
	//{
	//	itemID_t sVal;
	//	sVal = 0;
	//	if (vType == isSymID)
	//	{
	//		sVal = varSymbolID;
	//	}
	//	return sVal;
	//};
	/////////////////
	operator string(void)
	{
		string sVal;
		sVal = "";
		if (vType == isString)
		{
			sVal = sStringVal;
			// we won't deal with converting numerics to strings right now
		}
		// ws 12mar08
		else if (vType == isWideString)
		{
			sVal = TStr2AStr(sWideStringVal);
			// we won't deal with converting numerics to strings right now
		}
		// end ws 
		return sVal;
	};
	operator wstring(void)
	{
		wstring sVal;
		sVal = L"";
		if (vType == isWideString)
		{
			sVal = sWideStringVal;
			// we won't deal with converting numerics to strings right now
		}
		else if (vType == isString)
		{
			sVal = AStr2TStr(sStringVal);
			// we won't deal with converting numerics to strings right now
		}
		return sVal;
	};

/*************************************************************************************************/
	// ************ operator = *******************************************************************
	CValueVarient& operator=(const unsigned char src) 
	{// sign extend all for storage
		vType=isIntConst;
		vValue.iIntConst= (int)src; 
		sStringVal.erase();
		vIsUnsigned = true;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 1; 
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const char src) 
	{// sign extend all for storage
		int l = 0; l |= src;
		if ( src & 0x80 ) l |= 0xffffff00;
		vType=isIntConst;
		vValue.iIntConst= l; 
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 1;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const unsigned short src) 
	{// sign extend all for storage
		vType=isIntConst;
		vValue.iIntConst= (int)src; 
		sStringVal.erase();
		vIsUnsigned = true;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 2;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const short src) 
	{// sign extend all for storage
		int l = 0; l |= src;
		if ( src & 0x80 ) l |= 0xffff0000;
		vType=isIntConst;
		vValue.iIntConst= l; 
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 2;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const unsigned int src) 
	{
		vType=isIntConst;
		vValue.iIntConst= (int)src; 
		sStringVal.erase();
		vIsUnsigned = true;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 4;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const itemID_t src) 
	{
		vType=isSymID;
		vValue.varSymbolID= src; 
		sStringVal.erase();
		vIsUnsigned = true;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = sizeof(itemID_t);  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const int src) 
	{
		vType = isIntConst;
		vValue.iIntConst= src; 
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 4;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const double src) 
	{
		vType = isFloatConst;
		vValue.fFloatConst= src; 
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = true;
		vIsValid    = true;
		vSize       = 8;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const float src) 
	{
		vType = isFloatConst;
//		vValue.fFloatConst= src; 
		// stevev 16feb06 - the only way I have seen to cast a float to a double
		//	without introducing an error that is smaller than FLT_EPSILON but
		//	much greater than DBL_EPSILON
		// stevev 24feb06 - the following will NOT convert nans!!!!!!!!!!!!!

        if (isnan(src))
		{
			vValue.fFloatConst = src;
		}
		else
		if ( src <= -numeric_limits<float>::infinity() )
		{
			vValue.fFloatConst = -numeric_limits<double>::infinity();
		}
		else
		if ( src >= numeric_limits<float>::infinity() )
		{
			vValue.fFloatConst = numeric_limits<double>::infinity();
		}
		else
		{	  
            gcvt(src,7,tmpBuf);
			vValue.fFloatConst = atof(tmpBuf);
		}
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 4;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
#ifdef SUPPORT_LONGLONG
    CValueVarient& operator=(const  _UINT64 src)
	{
		vType=isVeryLong;
        vValue.longlongVal= (__int64)src;
		sStringVal.erase();
		vIsUnsigned = true;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 8;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	
    CValueVarient& operator=(const __int64 src)
	{
		vType=isVeryLong;
		vValue.longlongVal= src; 
		sStringVal.erase();
		vIsUnsigned = false;
		vIsDouble   = false;
		vIsValid    = true;
		vSize       = 8;  
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
#endif
	CValueVarient& operator=(const string src) 
	{
		vType = isString;
		sStringVal= src; 
		vIsUnsigned = false;
		vIsDouble   = false;
		vSize       = (int)sStringVal.size(); // WS 9apr07 VS2005 checkin 
		vIsValid    =true; 
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};
	CValueVarient& operator=(const wstring src)
	{
		vType = isWideString;
		sWideStringVal= src;
		vIsUnsigned = false;
		vIsDouble   = false;
		vSize       = (int)sWideStringVal.size(); // WS 9apr07 VS2005 checkin
		vIsValid    =true;
		vIndex      = 0;
		vIsBit      = false;
		return *this;
	};

	bool operator<=(const CValueVarient& src) 
	{
		switch( vType )
		{
		case isBool:
			return (vValue.bIsTrue == src.vValue.bIsTrue);// there is no relativity
			break;
#ifdef SUPPORT_LONGLONG
		case isVeryLong:
			{
				if (src.vType == isVeryLong)
				{
					if ( vIsUnsigned )
					{// we are unsigned - compare magnitudes
						return ( ((unsigned  long long)vValue.longlongVal) <=
								 ((unsigned  long long)src.vValue.longlongVal) );
					}
					else
						return (vValue.longlongVal <= src.vValue.longlongVal); 
				}
				else // he's shorter
				if (src.vType == isIntConst)
				{
					if ( vIsUnsigned )
					{// we are unsigned - compare magnitudes
						return ( ((unsigned  long long)vValue.longlongVal) <=
								 ((unsigned  long long)src.vValue.iIntConst) );
					}
					else // we are signed
					{// sign extend shorter (compiler may do it, we'll make sure)
						__int64 lll = src.vValue.iIntConst;
						if ((! src.vIsUnsigned) && src.vValue.iIntConst & 0x80000000)
						{
							lll |= 0xffffffff00000000;
						}
						return ( vValue.longlongVal <= lll );
					}
				}
				// that's all we're testing for now... more later
				else
					return false;
			}
			break;
#endif
		case isIntConst:
			{				
				if(src.isVeryLong)// we are not LL, src is
				{
					if ( vIsUnsigned )
					{// we are unsigned - compare magnitudes
						return ( ((unsigned  long long)vValue.iIntConst ) <=
								 ((unsigned  long long)src.vValue.longlongVal) );
					}
					else // we are signed
					{// sign extend shorter (compiler may do it, we'll make sure)
						__int64 lll = vValue.iIntConst;
						if ((! src.vIsUnsigned) && src.vValue.iIntConst & 0x80000000)
						{
							lll |= 0xffffffff00000000;
						}
						return ( vValue.longlongVal <= lll );
					}
				}

				if ( src.vType == isIntConst)
				{
					if (vIsUnsigned)// modified 24jan06 - previously, 0 was never < 0xffffffff
					{
						return ( ((unsigned int)vValue.iIntConst) <= ((unsigned int)src.vValue.iIntConst));
					}
					else
					{
						return (vValue.iIntConst <= src.vValue.iIntConst);
					}
				}
				else
				if ( src.vType == isFloatConst)
				{
					return (vValue.iIntConst <= src.vValue.fFloatConst);
				}
				else
				if ( src.vType == isBool)
				{// only '=='
					return (  (    src.vValue.bIsTrue  && vValue.iIntConst != 0) ||
							  ( (! src.vValue.bIsTrue) && vValue.iIntConst == 0)  );
				}
				else
				{
					return false;// error
				}
			}
			break;
		case isFloatConst:
			{
				if ( src.vType == isIntConst)
				{
					return (vValue.fFloatConst <= src.vValue.iIntConst);
				}
				else
				if ( src.vType == isFloatConst)
				{
					return (vValue.fFloatConst <= src.vValue.fFloatConst);
				}
				else
				if ( src.vType == isBool)
				{
					return (  (    src.vValue.bIsTrue  && vValue.fFloatConst != 0) ||
							  ( (! src.vValue.bIsTrue) && vValue.fFloatConst == 0)  );
				}
				else
				{
					return false; // error
				}
			}
			break;
		default:
			return false; // error
		}// end switch
	};
	bool operator>=(const CValueVarient& src) 
	{		switch( vType )
		{
		case isBool:
			return (vValue.bIsTrue == src.vValue.bIsTrue);// there is no relativity
			break;
		case isIntConst:
			{
				if ( src.vType == isIntConst)
				{
					if (vIsUnsigned)// added 08feb06
					{
						return ( ((unsigned int)vValue.iIntConst) >= ((unsigned int)src.vValue.iIntConst));
					}
					else
					{
						return (vValue.iIntConst >= src.vValue.iIntConst);
					}
					
				}
				else
				if ( src.vType == isFloatConst)
				{
					return (vValue.iIntConst >= src.vValue.fFloatConst);
				}
				else
				if ( src.vType == isBool)
				{// only '=='
					return (  (    src.vValue.bIsTrue  && vValue.iIntConst != 0) ||
							  ( (! src.vValue.bIsTrue) && vValue.iIntConst == 0)  );
				}
				else
				{
					return false;// error
				}
			}
			break;
		case isFloatConst:
			{
				if ( src.vType == isIntConst)
				{
					return (vValue.fFloatConst >= src.vValue.iIntConst);
				}
				else
				if ( src.vType == isFloatConst)
				{
					return (vValue.fFloatConst >= src.vValue.fFloatConst);
				}
				else
				if ( src.vType == isBool)
				{// only '=='
					return (  (    src.vValue.bIsTrue  && vValue.fFloatConst != 0) ||
							  ( (! src.vValue.bIsTrue) && vValue.fFloatConst == 0)  );
				}
				else
				{
					return false; // error
				}
			}
			break;
		default:
			return false; // error
		}// end switch
	};

	/* new 06oct06 - stevev */
	bool operator==(const CValueVarient& src) 
	{	bool ret = false;
		if (/*my*/vType != isOpcode && /*my*/vType != isDepIndex && /*my*/vType != invalid 
		 &&   src.vType != isOpcode &&   src.vType != isDepIndex &&   src.vType != invalid  )
		{	switch( /*my*/ vType )
			{
			case isBool:
				{	bool s = false;
					if ( src.vType == isBool )
					{	ret = (vValue.bIsTrue == src.vValue.bIsTrue);
					}
					else
					if ( src.vType == isIntConst )
					{	if (src.vValue.iIntConst != 0) s = true;
						ret = (vValue.bIsTrue == s);
					}
					else
					if ( src.vType == isFloatConst)
					{	if (src.vValue.fFloatConst != 0.0) s = true;
						ret = (vValue.bIsTrue == s);
					}
					else
					if ( src.vType == isString )
					{	ret = false; // not comparable
					}
					else
					if ( src.vType == isSymID )
					{	if (src.vValue.varSymbolID != 0) s = true;
						ret = (vValue.bIsTrue == s);
					}
					// else unknown type - leave ret false
				}
				break;
			case isIntConst:
				{	if ( src.vType == isBool ) // promote self to bool
					{	bool k = false;if (vValue.iIntConst != 0) k = true;
						ret = (src.vValue.bIsTrue == k);
					}
					else
					if ( src.vType == isIntConst )// no change
					{	ret = (vValue.iIntConst == src.vValue.iIntConst);
					}
					else
					if ( src.vType == isFloatConst)// promote self to float
					{	double f = (double) vValue.iIntConst;
						//ret = (src.vValue.fFloatConst == f);
						ret = AlmostEqual2sComplement(f, src.vValue.fFloatConst, 4);
					}
					else
					if ( src.vType == isString )// can't get there from here
					{	ret = false; // not comparable
					}
					else
					if ( src.vType == isSymID )// promote self to unsigned
					{	unsigned long y = (unsigned long) vValue.iIntConst;
						ret = (src.vValue.varSymbolID == y);
					}
					// else unknown type - leave ret false
				}
				break;
			case isFloatConst:
				{	if ( src.vType == isBool )// promote self to bool
					{	bool b = false; if (vValue.fFloatConst != 0) b = true;
						ret = (src.vValue.bIsTrue == b);
					}
					else
					if ( src.vType == isIntConst )// promote src to float
					{	double d = (double) src.vValue.iIntConst;
						//ret = (d == vValue.fFloatConst);					
						ret = AlmostEqual2sComplement(d, vValue.fFloatConst, 4);
					}
					else
					if ( src.vType == isFloatConst)
					{	//ret = (vValue.fFloatConst == src.vValue.fFloatConst);					
						ret = AlmostEqual2sComplement(src.vValue.fFloatConst, vValue.fFloatConst, 4);
					}
					else
					if ( src.vType == isString )
					{	ret = false; // not comparable
					}
					else
					if ( src.vType == isSymID )// promote src to float
					{	double d = (double) src.vValue.varSymbolID;
						//ret = (d == vValue.fFloatConst);				
						ret = AlmostEqual2sComplement(d, vValue.fFloatConst, 4);
					}
					// else unknown type - leave ret false
				}
				break;
			case isString:
				{	
					if ( src.vType == isString )
					{	ret = (src.sStringVal == sStringVal);
					}
					// else leave it false
				}
				break;
			case isWideString:
				{
					if ( src.vType == isWideString )
					{	ret = (src.sWideStringVal == sWideStringVal);
					}
					// else leave it false
				}
				break;
			case isSymID:  // unsigned long
				{	if ( src.vType == isBool )// promote self to bool
					{	bool b = false; if (vValue.varSymbolID != 0) b = true;
						ret = (b == src.vValue.bIsTrue);
					}
					else
					if ( src.vType == isIntConst )// promote src to self
					{	unsigned long y = (unsigned long) src.vValue.iIntConst;
						ret = (vValue.varSymbolID == y);
					}
					else
					if ( src.vType == isFloatConst)// promote self to float
					{	double f = (double) vValue.varSymbolID;
						//ret = (src.vValue.fFloatConst == f);
						ret = AlmostEqual2sComplement(f, src.vValue.fFloatConst, 4);
					}
					else
					if ( src.vType == isString )
					{	ret = false; // not comparable
					}
					else
					if ( src.vType == isSymID )
					{	ret = (vValue.varSymbolID == src.vValue.varSymbolID);
					}
					// else unknown type - leave ret false
				}
				break;

				
			default: // leave ret false (unknown type)
				break;
			}//endswitch
		}
		// else these cannot be compared so cannot equal ever..
		// leave ret false
		return ret;
	};
/*		bool				bIsTrue;    // constant T/F
		int					iIntConst;	// type 22
		double				fFloatConst;// type 23 - float
		itemID_t            varSymbolID;// other uses like index into dependency list
*/


	/* endnew 06oct06 */


	/* conversion functions */
/*	bool InitFrom(CValueVarient *pvtSrc, const variableType_t iValType, const int iValSize)
	{
		if ((VT_EMPTY == pvtSrc->vTagType) || (iValType <= vT_undefined) || (iValSize == 0))
		{
			clear();
			return false;
		}

		bool bRet = true;	//good until proven bad later
		switch (iValType)
		{
			case vT_Boolean:
			*this = (unsigned char)V_BOOL(pvtSrc);	//per FDI spec
			break;

			case vT_HartDate:
			case vT_Integer:
			{
				switch( iValSize )
				{
					case 1:
						{
							
							*this = (char)CV_I1(pvtSrc);
						}
						break;
					case 2:
						{
							
							*this = (short)CV_I2(pvtSrc);
						}
						break;
					case 3:
					case 4:
						
						*this = (long)(int)CV_I4(pvtSrc);
						break;
					case 8:
					default:
						
						*this = (long long)CV_I8(pvtSrc);
						break;
				}
			}
			break;

			case vT_Index:
			case vT_Enumerated:
			case vT_BitEnumerated:
			case vT_Time:
			case vT_DateAndTime:
			case vT_Unsigned:
			case vT_Duration:
			{
				switch( iValSize )
				{
					case 1:
						{
							
							*this = (unsigned char)CV_UI1(pvtSrc);
						}
						break;
					case 2:
						{
							
							*this = (unsigned short)CV_UI2(pvtSrc);
						}
						break;
					case 3:
					case 4:
						{
							
							*this = (unsigned int)CV_UI4(pvtSrc);
						}
						break;
					case 8:
					default:
						
						*this = (unsigned long long)CV_UI8(pvtSrc);
						break;
				}
			}
			break;

			case vT_TimeValue:
			{
				if (iValSize == 4)
				{
					
					*this = (unsigned int)CV_UI4(pvtSrc);
				}
				else
				{
					
					*this = (long long)CV_I8(pvtSrc);
				}
			}
			break;

			case vT_Double:
			
			*this = (double)CV_R8(pvtSrc);
			break;

			case vT_FloatgPt:
			
			*this = (float)CV_R4(pvtSrc);
			break;

			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password:
			case vT_EUC:
			case vT_VisibleString:
			{
				
				wstring localStr(CV_WSTR(pvtSrc), SysStringLen(CV_WSTR(pvtSrc)));
				*this = localStr;
			}
			break;

			case vT_BitString:	// unsigned char[]
			case vT_OctetString:
			{
				if (pvtSrc->vTagType == (VT_ARRAY | CVT_UI1) )
				{
					ULONG uSize = VariantGetElementCount(*pvtSrc);
					uchar* tmp = new uchar[uSize];
					VariantToBuffer(*pvtSrc, tmp, uSize);
					*this = bitStringAssign(tmp, uSize);
					delete(tmp);
				}
				else
				{
					bRet = false;
				}
			}
			break;

			default:
			{
				clear();
				bRet = false;
			}
			break;
		}

		return bRet;
	}

	bool CopyTo(CValueVarient *pvtDest, const variableType_t iValType, const int iValSize)
	{
		// check if source is valid
		if ((!this->vIsValid) || (iValType <= vT_undefined) || (iValSize == 0))
		{
			pvtDest->Clear();
			return false;
		}

		bool bRet = true;	//good until proven bad later
		switch (iValType)
		{
			case vT_Boolean:
			{
				CV_VT(pvtDest, ValueTagType::CVT_BOOL);
				V_BOOL(pvtDest) = (this->vValue.bIsTrue) ? (unsigned char) 0xFF : (unsigned char) 0;	//per FDI spec
			}
			break;

			case vT_HartDate:
			case vT_Integer:
			{
				switch( iValSize )
				{
					case 1:
						{
							char cValue = (char)*this;
							CV_VT(pvtDest, ValueTagType::CVT_I1);
							CV_I1(pvtDest) = cValue;
						}
						break;
					case 2:
						{
							short sValue = (short)*this;
							CV_VT(pvtDest, ValueTagType::CVT_I2);
							CV_I2(pvtDest) = sValue;
						}
						break;
					case 3:
					case 4:
						{
							CV_VT(pvtDest, ValueTagType::CVT_I4);
							CV_I4(pvtDest) = (long)(int)*this;;
						}
						break;
					case 8:
					default:
						{
							CV_VT(pvtDest, ValueTagType::CVT_I8);
							CV_I8(pvtDest) = (long long)*this;
						}
						break;
				}
			}
			break;

			case vT_Index:
			case vT_Enumerated:
			case vT_BitEnumerated:
			case vT_Time:
			case vT_DateAndTime:
			case vT_Unsigned:
			case vT_Duration:
			{
				switch( iValSize )
				{
					case 1:
						{
							unsigned char ucValue = (unsigned char)*this;
							CV_VT(pvtDest, ValueTagType::CVT_UI1);
							CV_UI1(pvtDest) = (unsigned char)ucValue;
						}
						break;
					case 2:
						{
							unsigned short usValue = (unsigned short)*this;
							CV_VT(pvtDest, ValueTagType::CVT_UI2);
							CV_UI2(pvtDest) = (unsigned short)usValue;
						}
						break;
					case 3:
					case 4:
						{
							CV_VT(pvtDest, ValueTagType::CVT_UI4);
							CV_UI4(pvtDest) = (unsigned long)*this;
						}
						break;
					case 8:
					default:
						{
							CV_VT(pvtDest, ValueTagType::CVT_UI8);
							CV_UI8(pvtDest) = (unsigned long long)*this;
						}
						break;
				}
			}
			break;

			case vT_TimeValue:
			{
				if (iValSize == 4)
				{
					CV_VT(pvtDest, ValueTagType::CVT_UI4);
					CV_UI4(pvtDest) = (unsigned long)*this;
				}
				else
				{
					CV_VT(pvtDest, ValueTagType::CVT_I8);
					CV_I8(pvtDest) = (long long)*this;
				}
			}
			break;

			case vT_Double:
			{
				CV_VT(pvtDest, ValueTagType::CVT_R8);
				CV_R8(pvtDest) = (double)*this;
			}
			break;

			case vT_FloatgPt:
			{
				CV_VT(pvtDest, ValueTagType::CVT_R4);
				CV_R4(pvtDest) = (float)*this;
			}
			break;

			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password:
			case vT_EUC:
			case vT_VisibleString:
			{
				CStdString sValue;
				if (this->vType == CValueVarient::isWideString)
				{
					sValue = this->sWideStringVal.c_str();
				}
				else	// (newValue.isString)
				{
					sValue = this->sStringVal.c_str();
				}
				CV_VT(pvtDest, ValueTagType::VT_BSTR);
				CV_WSTR(pvtDest) = SysAllocString(sValue);
			}
			case vT_BitString:	// unsigned char[]
			case vT_OctetString:
			{
				InitVariantFromBuffer(bsVal.bs, bsVal.bsLen, pvtDest);
			}
			break;

			default:
			{
				pvtDest->Clear();
				bRet = false;
			}
			break;
		}

		return bRet;
	}*/


};


typedef vector<CValueVarient>  varientList_t;
typedef vector<CValueVarient*> varientPtrList_t;

#ifdef _OSTREAM_

inline ostream &operator<<(ostream& ostr, CValueVarient& vv )
{
	if (vv.vIsValid)
	{
		switch (vv.vType)
		{
		case CValueVarient::invalid:
			ostr << "*INVALID*";	                    break;
		case CValueVarient::isBool:
			ostr << (vv.vValue.bIsTrue)?"TRUE":"FALSE";	break;
		case CValueVarient::isOpcode:
			{
				switch ((expressElemType_t)(vv.vValue.iOpCode))
				{
				case eet_NOT:			//_OPCODE 1
					ostr << "NOT";		break;
				case eet_NEG:			//_OPCODE 2
					ostr << "NEG";		break;
				case eet_BNEG:			//_OPCODE 3
					ostr << "BNEG";		break;
				case eet_ADD:			//_OPCODE 4
					ostr << "ADD";		break;
				case eet_SUB:			//_OPCODE 5
					ostr << "SUB";		break;
				case eet_MUL:			//_OPCODE 6
					ostr << "MUL";		break;
				case eet_DIV:			//_OPCODE 7
					ostr << "DIV";		break;
				case eet_MOD:			//_OPCODE 8
					ostr << "MOD";		break;
				case eet_LSHIFT:		//_OPCODE 9
					ostr << "LSHIFT";		break;
				case eet_RSHIFT:		//_OPCODE 10
					ostr << "RSHIFT";		break;
				case eet_AND:			//_OPCODE 11
					ostr << "AND";		break;
				case eet_OR:			//_OPCODE 12
					ostr << "OR";		break;
				case eet_XOR:			//_OPCODE 13
					ostr << "XOR";		break;
				case eet_LAND:			//_OPCODE 14
					ostr << "LOGICALAND";		break;
				case eet_LOR:			//_OPCODE 15
					ostr << "LOGICALOR";		break;
				case eet_LT:			//_OPCODE 16
					ostr << "LESSTHAN";		break;
				case eet_GT:			//_OPCODE 17
					ostr << "GREATERTHAN";		break;
				case eet_LE:			//_OPCODE 18
					ostr << "LESSTHANOREQUAL";		break;
				case eet_GE:			//_OPCODE 19
					ostr << "GREATERTHANOREQUAL";		break;
				case eet_EQ:			//_OPCODE 20
					ostr << "EQUAL";		break;
				case eet_NEQ:			//_OPCODE 21
					ostr << "NOTEQUAL";		break;
				default:
					LOGIT(CERR_LOG,"VARIENT: Tried to output an unknown OpCode: %d\n" 
						 ,vv.vValue.iOpCode);
					ostr << "*UNKNOWN_OPCODE*";	
					break;
				}
			}
			break;
		case CValueVarient::isIntConst:
			ostr << "const " << vv.vValue.iIntConst  << " (0x" << hex <<vv.vValue.iIntConst<<dec<<")";	break;
		case CValueVarient::isFloatConst:
			ostr << "const " << vv.vValue.fFloatConst<< " (0x" << hex <<vv.vValue.fFloatConst<<dec<<")";break;
		case CValueVarient::isDepIndex:
			ostr << "*DEPIDX(" << vv.vValue.depIndex << ")*";break;
		case CValueVarient::isString:
			ostr << "String const |" << vv.sStringVal<< "|" ;break;
		case CValueVarient::isSymID:
			ostr << "Symbol(0x"<<hex<<vv.vValue.varSymbolID<<dec<<")";
			if (vv.vIsBit) ostr << "[0x"<<hex<<vv.vIndex<<dec<<"]";break;
		default:
			LOGIT(CERR_LOG,"VARIENT: Tried to output an unknown type:%d\n",vv.vType);
			//ostr << "*UNKNOWN_TYPE*";	
			break;
		}
	}
	else
	{
		ostr << "*FLAGGED_INVALID*";
	}
	return ostr;
}
#endif //_OSTREAM_

#endif// _VARIENT_H

/*************************************************************************************************
 *
 *   $History: varient.h $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/02/03    Time: 11:02a
 * Updated in $/DD Tools/DDB/Common
 * Added unsigned long cast operator that is essensially the unsigned int
 * (removes errors during compile).
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
