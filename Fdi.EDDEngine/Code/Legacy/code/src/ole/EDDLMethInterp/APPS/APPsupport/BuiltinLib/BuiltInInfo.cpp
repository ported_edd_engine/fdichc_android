////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "BuiltInInfo.cpp"
//
// Author: Mike DeCoster
//
// Creation Date: 5-2-07
//
// Purpose: Encapsulates all meta info concerning a built in function
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BuiltInInfo.h"
//#include "atlstr.h"


////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo::CBuiltInInfo()
{
	Clear();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo::CBuiltInInfo(const CBuiltInInfo &rs)
{
	Set(rs);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo::~CBuiltInInfo()
{
	Clear();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo& CBuiltInInfo::operator =(const CBuiltInInfo &rs)
{
	Set(rs);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInInfo::Clear()
{
	m_strName = "";
	m_enBuiltIn = BUILTIN_UNDEFINED;
	m_enTokenType = RUL_TYPE_NONE;
	m_enTokenSubType = RUL_SUBTYPE_NONE;
	m_enReturnType = RUL_TYPE_NONE;
	m_uNumberOfParams = 0;
	for (int i=0; i<MAX_NUMBER_OF_FUNCTION_PARAMETERS; m_penParameterType[i] = RUL_TYPE_NONE, ++i) ;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInInfo::DebugDump(FILE* pFile)
{
	fwprintf(pFile, L"-----------------------------------------\n");
	fwprintf(pFile, L"BUILTIN_NAME: %u\n", (unsigned int)m_enBuiltIn);
	fwprintf(pFile, L"m_strName: %S\n", m_strName.c_str() );
	fwprintf(pFile, L"m_enTokenType: %u\n", (unsigned int)m_enTokenType);
	fwprintf(pFile, L"m_enTokenSubType: %u\n", (unsigned int)m_enTokenSubType);
	fwprintf(pFile, L"m_enReturnType: %u\n", (unsigned int)m_enReturnType);
	fwprintf(pFile, L"m_uNumberOfParams: %u\n", (unsigned int)m_uNumberOfParams);
	for (int i=0; i<MAX_NUMBER_OF_FUNCTION_PARAMETERS; ++i)
		fwprintf(pFile, L"m_penParameterType[%i]: %u\n", i, (unsigned int)m_penParameterType[i]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInInfo::Set(const CBuiltInInfo &rs)
{
	Clear();
	m_enBuiltIn = rs.m_enBuiltIn;
	m_strName = rs.m_strName;
	m_enTokenType = rs.m_enTokenType;
	m_enTokenSubType = rs.m_enTokenSubType;
	m_enReturnType = rs.m_enReturnType;
	m_uNumberOfParams = rs.m_uNumberOfParams;
	for (_UINT32 i=0; i<m_uNumberOfParams; ++i)
		m_penParameterType[i] = rs.m_penParameterType[i];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInInfo::Set(
			BUILTIN_NAME		enBuiltIn,
			const char*			strName,
			RUL_TOKEN_TYPE		enTokenType,
			RUL_TOKEN_SUBTYPE	enTokenSubType,
			RUL_TOKEN_TYPE		enReturnType,
            int				uNumberOfParams,
			RUL_TOKEN_TYPE		enParam1,
			RUL_TOKEN_TYPE		enParam2,
			RUL_TOKEN_TYPE		enParam3,
			RUL_TOKEN_TYPE		enParam4,
			RUL_TOKEN_TYPE		enParam5,
			RUL_TOKEN_TYPE		enParam6,
			RUL_TOKEN_TYPE		enParam7,
			RUL_TOKEN_TYPE		enParam8,
			RUL_TOKEN_TYPE		enParam9,
			RUL_TOKEN_TYPE		enParam10)
{
	Clear();
	m_enBuiltIn = enBuiltIn;
	m_strName = strName;
	m_enTokenType = enTokenType;
	m_enTokenSubType = enTokenSubType;
	m_enReturnType = enReturnType;
	m_uNumberOfParams = uNumberOfParams;
	m_penParameterType[0] = enParam1;
	m_penParameterType[1] = enParam2;
	m_penParameterType[2] = enParam3;
	m_penParameterType[3] = enParam4;
	m_penParameterType[4] = enParam5;
	m_penParameterType[5] = enParam6;
	m_penParameterType[6] = enParam7;
	m_penParameterType[7] = enParam8;
	m_penParameterType[8] = enParam9;
	m_penParameterType[9] = enParam10;
}
