#include "TestSupport/EddEngineController.h"


using namespace nsEDDEngine;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        namespace CriticalParameter
        {          
            [TestClass, Ignore]
            public ref class BaseTestCriticalParameters
            {
            private:

                static String^ criticalParameter;
                static String^ expectedParameter;

            public:
                static void SetupBase(String^ testCaseName, String^ expected)
                {
                    expectedParameter = expected;

                    EddEngineController^ engine = gcnew EddEngineController(testCaseName);                
                    engine->Initialize();
                    int blockInstance = 0;
                    nsEDDEngine::CRIT_PARAM_TBL *pCriticalParamTbl;
                    engine->Instance->GetCriticalParams(blockInstance, &pCriticalParamTbl);

                    List<String^>^ params = gcnew List<String^>();
                    for (int i = 0; i < pCriticalParamTbl->count; i++)
                    {
                        RESOLVED_REFERENCE& ref = pCriticalParamTbl->list[i];
                        if (ref.size == 1)
                        {
                            params->Add(String::Format("{0}", ref.pResolvedRef[0]));
                        }
                        else if (ref.size == 2)
                        {
                            params->Add(String::Format("{0}.{1}", ref.pResolvedRef[0], ref.pResolvedRef[1]));
                        }
                    }
                    delete engine;


                    criticalParameter = String::Join(", ", params);
                }

                [TestMethod, TestCategory("CriticalParameterTest")]
                void It_should_return_the_expected_critical_parameters()
                {
                    Assert::AreEqual(expectedParameter, criticalParameter);
                }

            };

            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0001_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0001.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0002_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0002.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0003_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0004_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0004.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0005_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0005.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0006_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0006.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0007_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "7";
                    SetupBase("ProfiBus\\FDI_PB_Test_0007.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0008_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0008.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0009_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0009.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000A_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000A.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000B_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000B.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000C_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000C.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000D_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000D.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000E_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000E.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_000F_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_000F.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0010_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0010.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0011_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0011.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0012_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0012.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0013_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0013.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0014_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0015_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0015.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0016_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0016.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0017_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0017.DDL.bin", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_0018_PB : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "";
                    SetupBase("ProfiBus\\FDI_PB_Test_0018.DDL.bin", expectedResult);
                }
            };
           
           
        }
    } // namespace ProfiBus
} // namespace ComponentTests
