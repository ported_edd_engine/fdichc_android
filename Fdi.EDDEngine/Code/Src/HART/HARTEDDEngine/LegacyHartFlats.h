#include "ddi_lib.h"
#include <cm_dds.h>
#include <cm_loc.h>
#include <langspec.h>
#include <StandardDictionary.h>

//#include "nsEDDEngine\Flats.h"

// derived class from DEPBIN
class Hart_ConvDEPBIN : public nsEDDEngine::DEPBIN
{
public:
	Hart_ConvDEPBIN( ::DEPBIN * );

	virtual ~Hart_ConvDEPBIN() {};

private:
	Hart_ConvDEPBIN();
};

// FLAT_ARRAY classes
class Hart_ConvFLAT_ARRAY : public nsEDDEngine::FLAT_ARRAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_ARRAY( ::FLAT_ARRAY * );  

	virtual ~Hart_ConvFLAT_ARRAY() {};

private:
	Hart_ConvFLAT_ARRAY();

};

class Hart_ConvARRAY_DEPBIN : public nsEDDEngine::ARRAY_DEPBIN
{
public:
	Hart_ConvARRAY_DEPBIN( ::ARRAY_DEPBIN * );

	virtual ~Hart_ConvARRAY_DEPBIN() {};

private:
	Hart_ConvARRAY_DEPBIN();
};


// FLAT_COLLECTION classes
class Hart_ConvFLAT_COLLECTION : public nsEDDEngine::FLAT_COLLECTION
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_COLLECTION( ::FLAT_COLLECTION * );

	virtual ~Hart_ConvFLAT_COLLECTION() {};

private:
	Hart_ConvFLAT_COLLECTION();
};

class Hart_ConvCOLLECTION_DEPBIN : public nsEDDEngine::COLLECTION_DEPBIN
{
public:
	Hart_ConvCOLLECTION_DEPBIN( ::COLLECTION_DEPBIN * );

	virtual ~Hart_ConvCOLLECTION_DEPBIN() {};

private:
	Hart_ConvCOLLECTION_DEPBIN();
};


// FLAT_ITEM_ARRAY classes
class Hart_ConvFLAT_ITEM_ARRAY : public nsEDDEngine::FLAT_ITEM_ARRAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_ITEM_ARRAY( ::FLAT_ITEM_ARRAY * );

	virtual ~Hart_ConvFLAT_ITEM_ARRAY() {};

private:
	Hart_ConvFLAT_ITEM_ARRAY();
};

class Hart_ConvITEM_ARRAY_DEPBIN : public nsEDDEngine::ITEM_ARRAY_DEPBIN
{
public:
	Hart_ConvITEM_ARRAY_DEPBIN( ::ITEM_ARRAY_DEPBIN * );

	virtual ~Hart_ConvITEM_ARRAY_DEPBIN() {};

private:
	Hart_ConvITEM_ARRAY_DEPBIN();
};


// FLAT_COMMAND classes
class Hart_ConvFLAT_COMMAND : public nsEDDEngine::FLAT_COMMAND
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_COMMAND( ::FLAT_COMMAND * );

	virtual ~Hart_ConvFLAT_COMMAND() {};

private:
	Hart_ConvFLAT_COMMAND();
};

class Hart_ConvCOMMAND_DEPBIN : public nsEDDEngine::COMMAND_DEPBIN
{
public:
	Hart_ConvCOMMAND_DEPBIN( ::COMMAND_DEPBIN * );

	virtual ~Hart_ConvCOMMAND_DEPBIN() {};

private:
	Hart_ConvCOMMAND_DEPBIN();
};


// FLAT_LIST classes
class Hart_ConvFLAT_LIST : public nsEDDEngine::FLAT_LIST
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_LIST( ::FLAT_LIST * );

	virtual ~Hart_ConvFLAT_LIST() {};

private:
	Hart_ConvFLAT_LIST();
};

class Hart_ConvLIST_DEPBIN : public nsEDDEngine::LIST_DEPBIN
{
public:
	Hart_ConvLIST_DEPBIN( ::LIST_DEPBIN * );

	virtual ~Hart_ConvLIST_DEPBIN() {};

private:
	Hart_ConvLIST_DEPBIN();
};


// FLAT_FILE classes
class Hart_ConvFLAT_FILE : public nsEDDEngine::FLAT_FILE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	Hart_ConvFLAT_FILE( ::FLAT_FILE * );

	virtual ~Hart_ConvFLAT_FILE() {};

private:
	Hart_ConvFLAT_FILE();
};

class Hart_ConvFILE_DEPBIN : public nsEDDEngine::FILE_DEPBIN
{
public:
	Hart_ConvFILE_DEPBIN( ::FILE_DEPBIN * );

	virtual ~Hart_ConvFILE_DEPBIN() {};

private:
	Hart_ConvFILE_DEPBIN();
};

// FLAT_RESP_CODE classes
class Hart_ConvFLAT_RESP_CODE : public nsEDDEngine::FLAT_RESP_CODE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_RESP_CODE( ::FLAT_RESP_CODE * );

	virtual ~Hart_ConvFLAT_RESP_CODE() {};

private:
	Hart_ConvFLAT_RESP_CODE();
};

class Hart_ConvRESP_CODE_DEPBIN : public nsEDDEngine::RESP_CODE_DEPBIN
{
public:
	Hart_ConvRESP_CODE_DEPBIN( ::RESP_CODE_DEPBIN * );

	virtual ~Hart_ConvRESP_CODE_DEPBIN() {};

private:
	Hart_ConvRESP_CODE_DEPBIN();
};


// FLAT_VAR classes
class Hart_ConvFLAT_VAR : public nsEDDEngine::FLAT_VAR
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_VAR( ::FLAT_VAR * );

	virtual ~Hart_ConvFLAT_VAR() {};

private:
	Hart_ConvFLAT_VAR();
	void PostProcessDefaultValue( nsEDDEngine::FLAT_VAR* pFlatVar );
};

class Hart_ConvVAR_DEPBIN : public nsEDDEngine::VAR_DEPBIN
{
public:
	Hart_ConvVAR_DEPBIN( ::FLAT_VAR * );

	virtual ~Hart_ConvVAR_DEPBIN() {};

private:
	Hart_ConvVAR_DEPBIN();
};

class Hart_ConvFLAT_CHART : public nsEDDEngine::FLAT_CHART
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_CHART( ::FLAT_CHART * );

	virtual ~Hart_ConvFLAT_CHART() {};

private:
	Hart_ConvFLAT_CHART();

};

class Hart_ConvCHART_DEPBIN : public nsEDDEngine::CHART_DEPBIN
{
public:
	Hart_ConvCHART_DEPBIN( ::CHART_DEPBIN * );

	virtual ~Hart_ConvCHART_DEPBIN() {};

private:
	Hart_ConvCHART_DEPBIN();
};


class Hart_ConvFLAT_GRAPH : public nsEDDEngine::FLAT_GRAPH
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_GRAPH( ::FLAT_GRAPH * );

	virtual ~Hart_ConvFLAT_GRAPH() {};

private:
	Hart_ConvFLAT_GRAPH();

};

class Hart_ConvGRAPH_DEPBIN : public nsEDDEngine::GRAPH_DEPBIN
{
public:
	Hart_ConvGRAPH_DEPBIN( ::GRAPH_DEPBIN * );

	virtual ~Hart_ConvGRAPH_DEPBIN() {};

private:
	Hart_ConvGRAPH_DEPBIN();
};

class Hart_ConvFLAT_AXIS : public nsEDDEngine::FLAT_AXIS
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_AXIS( ::FLAT_AXIS * );

	virtual ~Hart_ConvFLAT_AXIS() {};

private:
	Hart_ConvFLAT_AXIS();

};

class Hart_ConvAXIS_DEPBIN : public nsEDDEngine::AXIS_DEPBIN
{
public:
	Hart_ConvAXIS_DEPBIN( ::AXIS_DEPBIN * );

	virtual ~Hart_ConvAXIS_DEPBIN() {};

private:
	Hart_ConvAXIS_DEPBIN();
};

class Hart_ConvFLAT_WAVEFORM : public nsEDDEngine::FLAT_WAVEFORM
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_WAVEFORM( ::FLAT_WAVEFORM * );

	virtual ~Hart_ConvFLAT_WAVEFORM() {};

private:
	Hart_ConvFLAT_WAVEFORM();

};

class Hart_ConvWAVEFORM_DEPBIN : public nsEDDEngine::WAVEFORM_DEPBIN
{
public:
	Hart_ConvWAVEFORM_DEPBIN( ::WAVEFORM_DEPBIN * );

	virtual ~Hart_ConvWAVEFORM_DEPBIN() {};

private:
	Hart_ConvWAVEFORM_DEPBIN();
};

class Hart_ConvFLAT_SOURCE : public nsEDDEngine::FLAT_SOURCE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_SOURCE( ::FLAT_SOURCE * );

	virtual ~Hart_ConvFLAT_SOURCE() {};

private:
	Hart_ConvFLAT_SOURCE();

};

class Hart_ConvSOURCE_DEPBIN : public nsEDDEngine::SOURCE_DEPBIN
{
public:
	Hart_ConvSOURCE_DEPBIN( ::SOURCE_DEPBIN * );

	virtual ~Hart_ConvSOURCE_DEPBIN() {};

private:
	Hart_ConvSOURCE_DEPBIN();
};

class Hart_ConvFLAT_GRID : public nsEDDEngine::FLAT_GRID
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_GRID( ::FLAT_GRID * );

	virtual ~Hart_ConvFLAT_GRID() {};

private:
	Hart_ConvFLAT_GRID();

};

class Hart_ConvGRID_DEPBIN : public nsEDDEngine::GRID_DEPBIN
{
public:
	Hart_ConvGRID_DEPBIN( ::GRID_DEPBIN * );

	virtual ~Hart_ConvGRID_DEPBIN() {};

private:
	Hart_ConvGRID_DEPBIN();
};

class Hart_ConvFLAT_IMAGE : public nsEDDEngine::FLAT_IMAGE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_IMAGE( ::FLAT_IMAGE * );

	virtual ~Hart_ConvFLAT_IMAGE() {};

private:
	Hart_ConvFLAT_IMAGE();

};

class Hart_ConvIMAGE_DEPBIN : public nsEDDEngine::IMAGE_DEPBIN
{
public:
	Hart_ConvIMAGE_DEPBIN( ::IMAGE_DEPBIN * );

	virtual ~Hart_ConvIMAGE_DEPBIN() {};

private:
	Hart_ConvIMAGE_DEPBIN();
};

class Hart_ConvFLAT_METHOD : public nsEDDEngine::FLAT_METHOD
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_METHOD( ::FLAT_METHOD * );

	virtual ~Hart_ConvFLAT_METHOD() {};

private:
	Hart_ConvFLAT_METHOD();

};

class Hart_ConvMETHOD_DEPBIN : public nsEDDEngine::METHOD_DEPBIN
{
public:
	Hart_ConvMETHOD_DEPBIN( ::METHOD_DEPBIN * );

	virtual ~Hart_ConvMETHOD_DEPBIN() {};

private:
	Hart_ConvMETHOD_DEPBIN();
};

class Hart_ConvFLAT_MENU : public nsEDDEngine::FLAT_MENU
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_MENU( ::FLAT_MENU * );

	virtual ~Hart_ConvFLAT_MENU() {};

private:
	Hart_ConvFLAT_MENU();

};

class Hart_ConvMENU_DEPBIN : public nsEDDEngine::MENU_DEPBIN
{
public:
	Hart_ConvMENU_DEPBIN( ::MENU_DEPBIN * );

	virtual ~Hart_ConvMENU_DEPBIN() {};

private:
	Hart_ConvMENU_DEPBIN();
};


class Hart_ConvFLAT_EDIT_DISPLAY : public nsEDDEngine::FLAT_EDIT_DISPLAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_EDIT_DISPLAY( ::FLAT_EDIT_DISPLAY * );

	virtual ~Hart_ConvFLAT_EDIT_DISPLAY() {};

private:
	Hart_ConvFLAT_EDIT_DISPLAY();

};

class Hart_ConvEDIT_DISPLAY_DEPBIN : public nsEDDEngine::EDIT_DISPLAY_DEPBIN
{
public:
	Hart_ConvEDIT_DISPLAY_DEPBIN( ::EDIT_DISPLAY_DEPBIN * );

	virtual ~Hart_ConvEDIT_DISPLAY_DEPBIN() {};

private:
	Hart_ConvEDIT_DISPLAY_DEPBIN();
};

class Hart_ConvFLAT_REFRESH : public nsEDDEngine::FLAT_REFRESH
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_REFRESH( ::FLAT_REFRESH * );

	virtual ~Hart_ConvFLAT_REFRESH() {};

private:
	Hart_ConvFLAT_REFRESH();

};

class Hart_ConvREFRESH_DEPBIN : public nsEDDEngine::REFRESH_DEPBIN
{
public:
	Hart_ConvREFRESH_DEPBIN( ::REFRESH_DEPBIN * );

	virtual ~Hart_ConvREFRESH_DEPBIN() {};

private:
	Hart_ConvREFRESH_DEPBIN();
};

class Hart_ConvFLAT_UNIT : public nsEDDEngine::FLAT_UNIT
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_UNIT( ::FLAT_UNIT * );

	virtual ~Hart_ConvFLAT_UNIT() {};

private:
	Hart_ConvFLAT_UNIT();

};

class Hart_ConvUNIT_DEPBIN : public nsEDDEngine::UNIT_DEPBIN
{
public:
	Hart_ConvUNIT_DEPBIN( ::UNIT_DEPBIN * );

	virtual ~Hart_ConvUNIT_DEPBIN() {};

private:
	Hart_ConvUNIT_DEPBIN();
};

class Hart_ConvFLAT_WAO : public nsEDDEngine::FLAT_WAO
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_WAO( ::FLAT_WAO * );

	virtual ~Hart_ConvFLAT_WAO() {};

private:
	Hart_ConvFLAT_WAO();

};

class Hart_ConvWAO_DEPBIN : public nsEDDEngine::WAO_DEPBIN
{
public:
	Hart_ConvWAO_DEPBIN( ::WAO_DEPBIN * );

	virtual ~Hart_ConvWAO_DEPBIN() {};

private:
	Hart_ConvWAO_DEPBIN();
};

class Hart_ConvFLAT_BLOCK : public nsEDDEngine::FLAT_BLOCK
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_BLOCK( ::FLAT_BLOCK * );

	virtual ~Hart_ConvFLAT_BLOCK() {};

private:
	Hart_ConvFLAT_BLOCK();

};

class Hart_ConvBLOCK_DEPBIN : public nsEDDEngine::BLOCK_DEPBIN
{
public:
	Hart_ConvBLOCK_DEPBIN( ::BLOCK_DEPBIN * );

	virtual ~Hart_ConvBLOCK_DEPBIN() {};

private:
	Hart_ConvBLOCK_DEPBIN();
};

class Hart_ConvFLAT_RECORD : public nsEDDEngine::FLAT_RECORD
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	Hart_ConvFLAT_RECORD( ::FLAT_RECORD * );

	virtual ~Hart_ConvFLAT_RECORD() {};

private:
	Hart_ConvFLAT_RECORD();

};

class Hart_ConvRECORD_DEPBIN : public nsEDDEngine::RECORD_DEPBIN
{
public:
	Hart_ConvRECORD_DEPBIN( ::RECORD_DEPBIN * );

	virtual ~Hart_ConvRECORD_DEPBIN() {};

private:
	Hart_ConvRECORD_DEPBIN();
};