#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T12:59:50
#
#-------------------------------------------------

QT       -= gui

TARGET = FDI_DDS_AdapterLib
TEMPLATE = lib
CONFIG += staticlib
DEFINES += FDI_DDS_ADAPTERLIB_LIBRARY \
           UNICODE \

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CC = $$QMAKE_CXX
    QMAKE_CFLAGS  = $$QMAKE_CXXFLAGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += ConnectionMgr.cpp \
    DeviceTypeMgr.cpp \
    EddEngineLog.cpp \
    NtcSpeciesLog.cpp \
    cm_dds.cpp \
    cm_init.cpp \
    cm_rod.cpp \
    cmDdsTblAcc.cpp \
    cmtblacc.cpp \
    dev_type.cpp \
    ff_sup.cpp \
    rmalloc.cpp \
    sm_strfn.cpp \
    STDINC.CPP \

HEADERS += ../inc/AMSMiscUtilExport.h\
    ../inc/amsversion.h \
    ../inc/BssProgLog.h \
    ../inc/EddEngineLog.h \
    ../inc/ntcassert.h \
    ../inc/Ntcassert2.h \
    ../inc/pathtype_global.h \
    ../inc/pathtype_server.h \
    ../inc/ServerRes.h \
    ../inc/std.h \
    ../inc/Uc.h \
    ../inc/Uc2A.h \
    ../inc/Uc2T.h \
    ../inc/Uc2W.h \
    cmTblManipulation.h \
    cm_dds.h \
    cm_loc.h \
    cm_rod.h \
    cm_struc.h \
    ConnectionMgr.h \
    dci_srv.h \
    DeviceTypeMgr.h \
    pathtype.h \
    RodMgr.h \
    sym.h \
    sysproto.h \
    debug.h \
    debugon.h \
    rmalloc.h \
    STDINC.H \


unix {
    target.path = /usr/lib/
    INSTALLS += target
}


DESTDIR = $$PWD/../lib

CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}

include($$DESTDIR/../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += ../inc/ \
               ../FDI_DDS_Lib/\
               ../../../Interfaces/EDD_Engine_Interfaces \
               ../../../Inc/\
               ../../../Src/EDD_Engine_Common \

android {
    INCLUDEPATH +=../../../Src/EDD_Engine_Android \
}
else {
    INCLUDEPATH +=../../../Src/EDD_Engine_Linux \
}


INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR
