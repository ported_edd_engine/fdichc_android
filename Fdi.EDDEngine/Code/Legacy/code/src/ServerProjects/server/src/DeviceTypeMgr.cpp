#include "stdinc.h"
#include "DeviceTypeMgr.h"
CDeviceTypeMgr g_DeviceTypeMgr;

CLockDeviceTypeMgr::CLockDeviceTypeMgr()
{
	g_DeviceTypeMgr.m_cs.lock();
}

CLockDeviceTypeMgr::~CLockDeviceTypeMgr()
{
	g_DeviceTypeMgr.m_cs.unlock();
}

CDeviceTypeMgr::CDeviceTypeMgr()
{
	active_dev_type_tbl.count = 0;
	active_dev_type_tbl.list = nullptr;

	
}

CDeviceTypeMgr::~CDeviceTypeMgr()
{
	free(active_dev_type_tbl.list) ;
	active_dev_type_tbl.list = NULL ;
	active_dev_type_tbl.count = 0 ;

	
}

ActiveDevManufacturerTblElem* CDeviceTypeMgr::GetActiveDevManufacturerTblElem(DEVICE_MANUFACTURER_HANDLE dmh) 
{
	ActiveDevManufacturerTblElem* pMfgTblElem = active_dev_manufacturer_tbl.GetElement(dmh);
	return pMfgTblElem;
}