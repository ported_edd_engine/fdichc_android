
/**
 *	@(#) $Id: cm_rod.cpp,v 1.2 1995/11/09 15:53:41 jonreu Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_rod.c - Connection Manager - ROD Manager Module
 *
 *	This file contains all functions pertaining to ROD's (Remote
 *	Object Dictionaries).
 *
 *	A Remote Object Dictionary (ROD) is a host's copy of an Object
 *	Dictionary (OD) that exists in an actual device.  The data
 *	structures used to describe a ROD are local to this system and
 *	are defined in "cm_rod.h".  These structures are not identical
 *	memory copies of what is in the device's OD, but contain the
 *	same information.
 *
 *	All data within a ROD must be allocated in the ROD heap.  The
 *	following functions are used to maintain the ROD heap:
 *
 *	rod_init			rod_malloc			rod_calloc
 *	rod_realloc			rod_free			rod_member
 *
 *	A ROD is initiated by using the rod_open function with an OD
 *	description object (OD Object #0) as its parameter.  The OD
 *	description object contains information that dictates the size
 *	of the four sections of an OD.
 *
 *	A ROD's information may be accessed using the following functions:
 *
 *	rod_get		rod_put		rod_read		rod_write
 *
 */

#include "stdinc.h"

#include <Inf/NtcSpecies/Ntcassert2.h>
#include "ConnectionMgr.h"
#include "RodMgr.h"
#include "DeviceTypeMgr.h"


/*
 *	The ROD Heap
 */

#define INITIAL_ROD_HEAP_SIZE		0x80000
#define SUBSEQUENT_ROD_HEAP_SIZE	0x40000

//CRodMgr g_RodMgr;	// Global Rod Manager

CRodMgr::CRodMgr()
{
		/*
	 *	Initialize the ROD heap.
	 */

	rcreateheap(&rod_heap, INITIAL_ROD_HEAP_SIZE,
			SUBSEQUENT_ROD_HEAP_SIZE);

	/*
	 *	Initialize the ROD Table.
	 */
	rod_tbl.count = 0;
	rod_tbl.list = (OBJECT_DICTIONARY **)rod_malloc(1);
}

CRodMgr::~CRodMgr()
{
	/*
	 *	Free the ROD heap.
	 */
	rfreeheap(&rod_heap);

}






/***********************************************************************
 *
 *	Name:  rod_malloc
 *
 *	ShortDesc:  Allocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_malloc function takes a size and allocates a segment
 *		in the ROD heap.
 *
 *	Inputs:
 *		size - number of bytes to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_malloc(int size)
{
	/*
	 *	Allocate a segment in the ROD heap.
	 */

	return((char *)rmalloc(&rod_heap,size));
}


/***********************************************************************
 *
 *	Name:  rod_calloc
 *
 *	ShortDesc:  Allocate and clear a segment in the ROD heap.
 *
 *	Description:
 *		The rod_calloc function takes in a number and size and
 *		allocates and clears a segment in the ROD heap.
 *
 *	Inputs:
 *		number - the number of elements to allocate.
 *		size - the size of each element to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated and cleared memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_calloc(int number, int size)
{
	/*
	 *	Allocate and clear a segment in the ROD heap.
	 */

	return((char *)rcalloc(&rod_heap,number,size));
}


/***********************************************************************
 *
 *	Name:  rod_realloc
 *
 *	ShortDesc:  Reallocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_realloc function takes a pointer and size and
 *		reallocates the corresponding segment in the ROD heap
 *		to the new size.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to reallocate.
 *		size - the new size.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the reallocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_realloc(char *pointer, int size)
{
	/*
	 *	Reallocate the segment in the ROD heap.
	 */

	return((char *)rrealloc(&rod_heap,pointer,size));
}


/***********************************************************************
 *
 *	Name:  rod_free
 *
 *	ShortDesc:	Free a segment in the ROD heap
 *
 *	Description:
 *		The rod_free function takes a pointer and frees the
 *		corresponding segment in the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to free.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CRodMgr::rod_free(char *pointer)
{
	/*
	 *	Free the segment in the ROD heap.
	 */

	rfree(&rod_heap,pointer);
}


/*********************************************************************
 *
 *	Name:  rod_member
 *
 *	ShortDesc:  Determine if a pointer is a member of the ROD heap.
 *
 *	Description:
 *		The rod_member function takes a pointer and determines if
 *		it is a memeber of the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to be identified.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		1 if the pointer is a member of the ROD heap.
 *		0 if the pointer is not a member of the ROD heap.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_member(char *pointer)
{
	/*
	 *	Determine if the pointer is a member of the ROD heap.
	 */

	return(rmemberof(&rod_heap,pointer));
}


/***********************************************************************
 *
 *	Name:  rod_object_heap_check
 *
 *	ShortDesc:  Check if an object is in the ROD heap.
 *
 *	Description:
 *		The rod_object_heap_check function takes an object and checks
 *		if all of its components are located in the ROD heap.
 *
 *	Inputs:
 *		object - the object to be checked.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_OBJECT_NOT_IN_HEAP.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_object_heap_check(OBJECT *object)
{
	if (!object) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}
	if (!rod_member((char *)object)) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}

	if (object->name) {
		if (!rod_member((char *)object->name)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->extension) {
		if (!rod_member((char *)object->extension)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->value) {
		if (!rod_member((char *)object->value)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	if (object->specific) {
		if (!rod_member((char *)object->specific)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}

		switch (object->code) {

			case OC_PROGRAM_INVOCATION:
				if (!rod_member((char *)object->specific->
						program_invocation.domain_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_DATA_TYPE:
				if (!rod_member((char *)object->specific->
						data_type.symbol)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:
				if (!rod_member((char *)object->specific->
						data_type_structure.type_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				if (!rod_member((char *)object->specific->
						data_type_structure.size_list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			case OC_VARIABLE_LIST:
				if (!rod_member((char *)object->specific->
						variable_list.list)) {
					return(CM_OBJECT_NOT_IN_HEAP);
				}
				break;

			default:
				break;
		}
	}

	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  rod_image_heap_check
 *
 *	ShortDesc:  Check if the image data is in the ROD heap.
 *
 *	Description:
 *		The rod_image_heap_check function takes an BINARY_IMAGE and checks
 *		if all of its components are located in the ROD heap.
 *
 *	Inputs:
 *		image - the image data to be checked.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_OBJECT_NOT_IN_HEAP.
 *
 *	Authors:
 *		Ying Xu
 *
 **********************************************************************/

int CRodMgr::rod_image_heap_check(BINARY_IMAGE *image)
{
	if (!image) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}
	if (!rod_member((char *)image)) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}

	if (image->data) {
		if (!rod_member((char *)image->data)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  rod_new_rod
 *
 *	ShortDesc:  Create a new ROD in the ROD Table.
 *
 *	Description:
 *		The rod_new_rod function creates a new ROD either in the
 *		first unused (deallocated) ROD space or at the end of the
 *		existing ROD Table list.  The corresponding ROD handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the new ROD.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

ROD_HANDLE CRodMgr::rod_new_rod(wchar_t * filename)
{
	ROD_HANDLE	rod_handle = 0;

	/*
	 *	Go through each ROD in the ROD Table.
	 */

	for (rod_handle = 0; rod_handle < rod_tbl.count; rod_handle ++) {

		/*
		 *	Check if the ROD has been deallocated.
		 */

		if (!rod_tbl.list[rod_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire ROD Table was gone through without
	 *	finding any deallocated ROD's.
	 */

	if (rod_handle >= rod_tbl.count) {

		/*
		 *	Extend the ROD Table.
		 */

		rod_tbl.count ++;
		rod_tbl.list = (OBJECT_DICTIONARY **) rod_realloc(
				(char *)rod_tbl.list,
				sizeof(OBJECT_DICTIONARY *) * rod_tbl.count);
		if (!rod_tbl.list) {
			return(CM_NO_ROD_MEMORY);
		}

		rod_handle = rod_tbl.count - 1;
	}

	/*
	 *	Allocate a new ROD where the ROD handle is.
	 */

	rod_tbl.list[rod_handle] =
			(OBJECT_DICTIONARY *) rod_calloc(1,
			sizeof(OBJECT_DICTIONARY));
	if (!rod_tbl.list[rod_handle]) {
		return(CM_NO_ROD_MEMORY);
	}
	wcscpy(rod_tbl.list[rod_handle]->EDDBinaryFilename,filename);
	return(rod_handle);
}


/***********************************************************************
 *
 *	Name:  rod_open
 *
 *	ShortDesc:  Open a ROD.
 *
 *	Description:
 *		The rod_open function takes an OD description object and 
 *		opens a new ROD.  The maximum number of objects will be
 *		set.  All objects will be empty.  The corresponding ROD
 *		handle will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		object_0 - the OD description object.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the opened ROD.
 *		Return values from rod_new_rod function (CM_NO_ROD_MEMORY).
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

ROD_HANDLE CRodMgr::rod_open(OBJECT *object_0,ROD_HANDLE rod_handle)
{
	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 r_code;

	/*
	 *	Check to make sure that the OD description object is in the
	 *	ROD heap.
	 */

	r_code = rod_object_heap_check(object_0);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	
	rod = rod_tbl.list[rod_handle];
	rod->od_description = object_0;
	od_description = &object_0->specific->od_description;

	/*
	 *	Allocate the four sections of the ROD.
	 *
	 *	(Note that this does not allocate any objects - just
	 *	object pointers.)
	 */

	if (od_description->stod_object_count) {
		rod->stod = (OBJECT **) rod_calloc((int)od_description->
				stod_object_count,sizeof(OBJECT *));
		if (!rod->stod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->sod_object_count) {
		rod->sod = (OBJECT **) rod_calloc((int)od_description->
				sod_object_count,sizeof(OBJECT *));
		if (!rod->sod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->dvod_object_count) {
		rod->dvod = (OBJECT **) rod_calloc((int)od_description->
				dvod_object_count,sizeof(OBJECT *));
		if (!rod->dvod)
			return (CM_NO_ROD_MEMORY);
	}

	if (od_description->dpod_object_count) {
		rod->dpod = (OBJECT **) rod_calloc((int)od_description->
				dpod_object_count,sizeof(OBJECT *));
		if (!rod->dpod)
			return (CM_NO_ROD_MEMORY);
	}

	rod->tok_major_rev =0;

	return(rod_handle);
}


/***********************************************************************
 *
 *	Name:  rod_object_free
 *
 *	ShortDesc:  Free an object and all of its allocated contents.
 *
 *	Description:
 *		The rod_object_free function takes an object and frees it
 *		from the ROD heap along with any of its allocated contents.
 *
 *	Inputs:
 *		object - the object to be freed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CRodMgr::rod_object_free(OBJECT *object)
{
	/*
	 *	Go through all possible allocated components of an object
	 *	and free them if necessary.
	 */

	if (object->name) {
		rod_free((char *)object->name);
	}
	if (object->extension) {
		rod_free((char *)object->extension);
	}
	if (object->value) {
		rod_free((char *)object->value);
	}
	if (object->specific) {

		switch (object->code) {

			case OC_NULL:
				break;

			case OC_OD_DESCRIPTION:
				break;

			case OC_DOMAIN:
				break;

			case OC_PROGRAM_INVOCATION:
				if (object->specific->program_invocation.domain_list) {
					rod_free((char *)object->specific->
							program_invocation.domain_list);
				}
				break;

			case OC_EVENT:
				break;

			case OC_DATA_TYPE:
				if (object->specific->data_type.symbol) {
					rod_free((char *)object->specific->
							data_type.symbol);
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:
				if (object->specific->data_type_structure.type_list) {
					rod_free((char *)object->specific->
							data_type_structure.type_list);
				}
				if (object->specific->data_type_structure.size_list) {
					rod_free((char *)object->specific->
							data_type_structure.size_list);
				}
				break;

			case OC_SIMPLE_VARIABLE:
				break;

			case OC_ARRAY:
				break;

			case OC_RECORD:
				break;

			case OC_VARIABLE_LIST:
				if (object->specific->variable_list.list) {
					rod_free((char *)object->specific->
							variable_list.list);
				}
				break;

			default:
				break;
		}

		rod_free((char *)object->specific);
	}

	rod_free((char *)object);
}


/***********************************************************************
 *
 *	Name:  rod_close
 *
 *	ShortDesc:  Close a ROD.
 *
 *	Description:
 *		The rod_close function takes a ROD handle and closes a
 *		specific ROD in the ROD Table.  Note that the ROD will be
 *		deallocated but the ROD Table list will not be reduced.
 *		This function will be successful unless an invalid ROD
 *		handle is passed in.
 *
 *	Inputs:
 *		rod_handle - the handle of the specific ROD to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_close(ROD_HANDLE rod_handle)
{
	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;
	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int		object_num;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Go through each object in the Static List of Object
	 *	Types (ST-OD) and free the object if necessary.
	 *	Then free the ST-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->stod_object_count;
			object_num ++) {

		if (rod->stod[object_num]) {
			rod_object_free(rod->stod[object_num]);
		}
	}
	rod_free((char *)rod->stod);

	/*
	 *	Go through each object in the Static Object Dictionary
	 *	(S-OD) and free the object if necessary.
	 *	Then free the S-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {

		if (rod->sod[object_num]) {
			rod_object_free(rod->sod[object_num]);
		}
	}
	rod_free((char *)rod->sod);

	/*
	 *	Go through each object in the Dynamic List of Variable
	 *	Lists (DV-OD) and free the object if necessary.
	 *	Then free the DV-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->dvod_object_count;
			object_num ++) {

		if (rod->dvod[object_num]) {
			rod_object_free(rod->dvod[object_num]);
		}
	}
	rod_free((char *)rod->dvod);

	/*
	 *	Go through each object in the Dynamic List of Program
	 *	Invocations (DP-OD) and free the object if necessary.
	 *	Then free the DP-OD.
	 */
	 
	for (object_num = 0; object_num < od_description->dpod_object_count;
			object_num ++) {

		if (rod->dpod[object_num]) {
			rod_object_free(rod->dpod[object_num]);
		}
	}
	rod_free((char *)rod->dpod);

	/*
	 *	Free the ROD description object.
	 */

	rod_object_free(rod->od_description);

	/* Free image data */
	if (rod->image_data)
	{
		if (rod->image_data->data)
		{
			rod_free((char *)rod->image_data->data);
		}
		rod_free((char *)rod->image_data);	
	}

	/*
	 *	Free the ROD.
	 */

	rod_free((char *)rod);
	rod_tbl.list[rod_handle] = (OBJECT_DICTIONARY *)0;

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_close(ROD_HANDLE rod_handle)
{
	return m_RodMgr.rod_close(rod_handle);
}

/***********************************************************************
 *
 *	Name:  rod_object_lookup
 *
 *	ShortDesc:  Look up an object in a ROD.
 *
 *	Description:
 *		The rod_object_lookup function takes a ROD handle and object
 *		index and searches the four sections of the ROD for the
 *		corresponding object.  The (found) object output is a pointer
 *		to an object.  If this object pointer is null, it indicates
 *		that the object index is valid, but the object is not yet
 *		allocated.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the object that is being looked up.
 *
 *	Outputs:
 *		object - the found object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_INDEX.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check which section of the ROD the object index resides.
	 */

	if (object_index == 0) {
		*object = rod->od_description;
	}
	else if (object_index <= od_description->stod_object_count) {
		*object = rod->stod[object_index - 1];
	}
	else if ((object_index >= od_description->sod_first_index) &&
			(object_index < od_description->sod_first_index +
			od_description->sod_object_count)) {
		*object = rod->sod[object_index -
				od_description->sod_first_index];
	}
	else if ((object_index >= od_description->dvod_first_index) &&
			(object_index < od_description->dvod_first_index +
			od_description->dvod_object_count)) {
		*object = rod->dvod[object_index -
				od_description->dvod_first_index];
	}
	else if ((object_index >= od_description->dpod_first_index) &&
			(object_index < od_description->dpod_first_index +
			od_description->dpod_object_count)) {
		*object = rod->dpod[object_index -
				od_description->dpod_first_index];
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get
 *
 *	ShortDesc:  Get an object from a ROD.
 *
 *	Description:
 *		The rod_get function takes a ROD handle and object index and
 *		gets the corresponding object.  The (requested) object output
 *		is a pointer to an object.  This object and any of its
 *		allocated contents reside in the ROD.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the requested object.
 *
 *	Outputs:
 *		object - the requested object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_OBJECT.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	int		r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object.
	 */

	r_code = rod_object_lookup(rod_handle,object_index,object);
	if (r_code != CM_SUCCESS) {
		return (r_code);
	}

	/*
	 *	Check to make sure that the object exists.
	 */

	if (!*object) {
		return (CM_NO_OBJECT);
	}

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	return m_RodMgr.rod_get(rod_handle,object_index,object);
}

/***********************************************************************
 *
 *	Name:  rod_get_image
 *
 *	ShortDesc:  Get an image from a ROD.
 *
 *	Description:
 *		The rod_get rod_get_image takes a ROD handle, image's offset and size and
 *		gets the corresponding image.  The (requested) image output
 *		is a pointer to an image.  This image and any of its
 *		allocated contents reside in the ROD.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the image exists in.
 *		offset	   - the offset for the requested image in the image data chunk.
 *		size	   - the size of the requested image.
 *
 *	Outputs:
 *		image - the requested image.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_OBJECT.
 *
 *	Authors:
 *		Ying Xu
 *
 **********************************************************************/
int CRodMgr::rod_get_image(ROD_HANDLE rod_handle, DDL_UINT offset, DDL_UINT size, BINARY_LANGUAGE_IMAGE* image)
{
	OBJECT_DICTIONARY			*rod;

	/*
	 *	Check to make sure that the ROD handle
	 *	is valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Get the ROD and description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];

	if (rod->image_data->data ==NULL)
	{
		return (CM_NO_VALUE);
	}

	if (offset+size > rod->image_data->length)
	{
		return (CM_BAD_PARAM_OFFSET);
	}

	image->data = rod->image_data->data+offset;

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_get_image(ROD_HANDLE rod_handle, DDL_UINT offset, DDL_UINT size, BINARY_LANGUAGE_IMAGE* image)
{
	return m_RodMgr.rod_get_image(rod_handle, offset,  size, image);
}


/***********************************************************************
 *
 *	Name:  rod_put_image_data
 *
 *	ShortDesc:  Put the image data a ROD.
 *
 *	Description:
 *		The rod_put_image_data function takes a ROD handle, and
 *		image data and puts the image data into the corresponding ROD.  The
 *		image data and any of its allocated contents must be allocated
 *		in the ROD heap, and checking is done to ensure this.  If 
 *		image data exists where the new image datat is being put, the existing
 *		image data will be freed.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD where the image data will be put.
 *		image - the image data to be put.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_ILLEGAL_PUT.
 *		CM_BAD_INDEX.
 *		Return values from rod_object_heap_check function.
 *
 *	Authors:
 *		Ying Xu
 *
 **********************************************************************/
int CRodMgr::rod_put_image_data(BINARY_IMAGE *image_data, ROD_HANDLE rod_handle)
{
 	OBJECT_DICTIONARY			*rod;
	int							 r_code;

	/*
	 *	Check to make sure that the ROD handle and the object
	 *	are valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}
	if (!image_data) {
		return(CM_NO_OBJECT);
	}

	/*
	 *	Get the ROD and description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];

	/*
	 *	Check to make sure that the image to be put is in the
	 *	ROD heap.
	 */

	r_code = rod_image_heap_check(image_data);
	if (r_code != CM_SUCCESS)
	{
		return(r_code);
	}

	rod->image_data = image_data;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_put
 *
 *	ShortDesc:  Put an object into a ROD.
 *
 *	Description:
 *		The rod_put function takes a ROD handle, object index, and
 *		object and puts the object into the corresponding ROD.  The
 *		object and any of its allocated contents must be allocated
 *		in the ROD heap, and checking is done to ensure this.  If an
 *		object exists where the new object is being put, the existing
 *		object will be freed.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD where the object will be put.
 *		object_index - the index of the object to be put.
 *		object - the object to be put.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_ILLEGAL_PUT.
 *		CM_BAD_INDEX.
 *		Return values from rod_object_heap_check function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT *object)
{
	OBJECT_DICTIONARY			*rod;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 r_code;

	/*
	 *	Check to make sure that the ROD handle and the object
	 *	are valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}
	if (!object) {
		return(CM_NO_OBJECT);
	}

	/*
	 *	Get the ROD and description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check to make sure that OD description object is not
	 *	being overwritten.
	 */

	if (object_index == 0) {
		return(CM_ILLEGAL_PUT);
	}

	/*
	 *	Check to make sure that the object to be put is in the
	 *	ROD heap.
	 */

	r_code = rod_object_heap_check(object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check which section of the Object Dictionary the object
	 *	index resides.  Then free any existing object and put
	 *	in the new object.
	 */

	if (object_index <= od_description->stod_object_count) {

		if (rod->stod[object_index - 1]) {

			rod_object_free(rod->stod[object_index - 1]);
		}
		rod->stod[object_index - 1] = object;
	}
	else if ((object_index >= od_description->sod_first_index) &&
			(object_index < od_description->sod_first_index +
			od_description->sod_object_count)) {

		if (rod->sod[object_index -
				od_description->sod_first_index]) {

			rod_object_free(rod->sod[object_index -
					od_description->sod_first_index]);
		}
		rod->sod[object_index - od_description->sod_first_index] =
				object;
	}
	else if ((object_index >= od_description->dvod_first_index) &&
			(object_index < od_description->dvod_first_index +
			od_description->dvod_object_count)) {

		if (rod->dvod[object_index -
				od_description->dvod_first_index]) {

			rod_object_free(rod->dvod[object_index -
					od_description->dvod_first_index]);
		}
		rod->dvod[object_index - od_description->dvod_first_index] =
				object;
	}
	else if ((object_index >= od_description->dpod_first_index) &&
			(object_index < od_description->dpod_first_index +
			od_description->dpod_object_count)) {

		if (rod->dpod[object_index -
				od_description->dpod_first_index]) {

			rod_object_free(rod->dpod[object_index -
					od_description->dpod_first_index]);
		}
		rod->dpod[object_index - od_description->dpod_first_index] =
				object;
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}

int CRodMgr::rod_set_header(ROD_HANDLE rhandle, DDOD_HEADER header)
{
    OBJECT_DICTIONARY *rod = NULL;
	if (!valid_rod_handle(rhandle)) {
		return CM_INVALID_ROD_HANDLE;
	}
	rod = rod_tbl.list[rhandle];
    rod->header = header;
	
    return CM_SUCCESS;
}

int CRodMgr::rod_set_tok_major_rev(ROD_HANDLE rhandle, int tok_major_rev)
{
    OBJECT_DICTIONARY *rod = NULL;
	if (!valid_rod_handle(rhandle)) {
		return CM_INVALID_ROD_HANDLE;
	}
	rod = rod_tbl.list[rhandle];
    rod->tok_major_rev = tok_major_rev;
	
    return CM_SUCCESS;
}

int CRodMgr::rod_get_tok_major_rev(ROD_HANDLE rhandle)
{
	int tok_major_rev = 0;
    OBJECT_DICTIONARY *rod = NULL;
	if (!valid_rod_handle(rhandle)) {
		ntcassert2(false);
	}
	else
	{
		rod = rod_tbl.list[rhandle];
		tok_major_rev = rod->tok_major_rev;
	}

	return tok_major_rev;
}
int CDeviceTypeMgr::rod_get_tok_major_rev(ROD_HANDLE rHandle)
{
	return m_RodMgr.rod_get_tok_major_rev(rHandle);
}

 
DDOD_HEADER * CRodMgr::get_header(ROD_HANDLE rhandle)
{
	DDOD_HEADER * header = NULL;

  	if (!valid_rod_handle(rhandle)) 
	{
		ntcassert2(false);
	}
	else
	{
		header = &(rod_tbl.list[rhandle]->header);
	}

	return header;
}

DDOD_HEADER * CDeviceTypeMgr::get_header(ROD_HANDLE rhandle)
{
	return m_RodMgr.get_header(rhandle);
}

DDOD_HEADER * CConnectionMgr::get_header(ROD_HANDLE rhandle)
{
	return g_DeviceTypeMgr.get_header(rhandle);
}
bool CRodMgr::is_tok_major_rev_6_or_8(ROD_HANDLE rhandle)
{
	bool tok6or8 = false;
    OBJECT_DICTIONARY *rod = NULL;
	if (!valid_rod_handle(rhandle)) {
		ntcassert2(false);
	}
	else
	{
		rod = rod_tbl.list[rhandle];
		if (rod->tok_major_rev ==6 || rod->tok_major_rev ==8)
		{
			tok6or8 = true;
		}
	}

	return tok6or8;
}

bool CDeviceTypeMgr::is_tok_major_rev_6_or_8(ROD_HANDLE rhandle)
{
	return m_RodMgr.is_tok_major_rev_6_or_8(rhandle);
}

bool CRodMgr::is_string_encoded_in_utf8(ROD_HANDLE rhandle)
{
	bool bUTF8 = false;

	int  tok_major_rev = rod_get_tok_major_rev(rhandle);

	if (tok_major_rev >= 8)
	{
		bUTF8 = true;
	}

	return bUTF8;
}

bool CDeviceTypeMgr::is_string_encoded_in_utf8(ROD_HANDLE rhandle)
{
	return m_RodMgr.is_string_encoded_in_utf8(rhandle);
}
