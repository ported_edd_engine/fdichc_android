/*************************************************************************************************
 *
 * $Workfile: ddbDevice.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the Device class  - holds the error device services too
 *		5/7/2	sjv	creation
 */

#include "stdafx.h"
#include "ddbDevice.h"
#include "MEE.h"



int hCddbDevice::ResolveExp(const char* szDDitemName,const char* szDDExp,unsigned long ulLenOfexpToResolve,hCitemBase**  pIBFinal)
{

	if(NULL != pMEE)
	{
		return (pMEE->ResolveExp(szDDitemName, szDDExp, ulLenOfexpToResolve, pIBFinal));
	}
	else
	{
		return FAILURE;
	}
}
unsigned int hCddbDevice::getMEEdepth()
{
	if(NULL != pMEE)
	{
		return (pMEE->GetOneMethRefNo());
	}
	return 0;

}

hCddbDevice::hCddbDevice(DevInfcHandle_t h) : hCobject(h)
{
	// Null the pointers
	dictionary = NULL;

	//instatiate our pointer.
	pMEE = new MEE;
}

hCddbDevice::~hCddbDevice( )
{
	if(pMEE)
	{
		delete pMEE;
		pMEE = NULL;
	}

};



