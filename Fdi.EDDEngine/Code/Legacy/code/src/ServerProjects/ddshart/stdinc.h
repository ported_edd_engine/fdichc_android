/**********************************************************************
*          stdinc.h    precomiled header file for dds HART
**********************************************************************/

#pragma once

#pragma warning(disable: 4127)
#pragma warning(disable: 4482)

#include "std.h"			
#include "ddi_lib.h"		
#include "ddi_tab.h"
#include "dds_tab.h"
#include "fch_lib.h"
#include "evl_loc.h"

#include "PlatformCommon.h"
#include "stdstring.h"
//#define _TEST_TOKENIZER_	// Define this to side-step eval and just return attribute binary chucks.

