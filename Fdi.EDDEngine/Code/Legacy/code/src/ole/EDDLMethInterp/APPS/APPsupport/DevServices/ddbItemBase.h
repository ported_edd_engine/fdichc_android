/*************************************************************************************************
 *
 * $Workfile: ddbItemBase.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The base class and support classes for item's bases class
 *		7/26/02	sjv	created fom itembase.h
 *
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 *
 * #include "ddbItemBase.h"
 */

#ifndef _DDBITEMBASE_H
#define _DDBITEMBASE_H
#ifdef INC_DEBUG
#pragma message("In ddbItemBase.h") 
#endif

#pragma once

#include "ddbGlblSrvInfc.h"
#include "ddbReferenceNexpression.h"


#define NO_LABEL -1 /* never found key */

class hCattrValid;	// references so we don't have a  circular with ddbattributes
class hCattrLabel;
class hCattrHelp;
class hCVar;

#ifndef _DDBTRACKING_H   // HOMZ 2003 port
class hCgroupItemDescriptor;
#endif

class hCattrDebug;
class hCattrBase;

typedef vector<hCattrBase*>  hAttrPtrList_t;


class hCitemBase : public hCobject
{
protected:
//<<<<<<<< DATA >>>>>>>>>>>>>
	ITEM_ID			itemId;
	CitemType       itemType;	// see itemType_t
	ITEM_SIZE       itemSize;
	CitemType		itemSubType;
	string          itemName;
	hAttrPtrList_t  attrLst;
	bool			isConditional;
	hCattrValid*     pValid;
	hCattrLabel*     pLabel;
	hCattrHelp*      pHelp;
	hCattrDebug*     pDebug;

protected:
	RETURNCODE baseLabel(int key, wstring &l);          /*returns the label if the item (or null)*/

public:
	virtual void   fillCondDepends(ddbItemList_t&  iDependOn); // it will do the best it can
	virtual void   fillContainList(ddbItemList_t&  iContain);  // over ride by container classes

	virtual RETURNCODE setSize( hv_point_t& returnedSize, ulong qual );
	ITEM_ID	      getID(void);
	virtual itemType_t getIType(void);
	unsigned int  getTypeVal(void);
	const char*   getTypeName(void);
	unsigned int  getSize(void);
	bool  IsConditional(void);
	hCattrLabel*  getLabelAttr(void);
    virtual bool HasHelp();
	virtual bool IsDynamic(void); //the only class the counts
	virtual bool IsReadOnly(void); // these need to be overridden in the hCvar class
	virtual bool IsVariable(void);
	virtual bool IsCommand(void);//CJK added this 9/3/2009
	virtual bool IsMethod(void);
	virtual hCattrBase*  newHCattr(aCattrBase* pACattr);
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getAllindexes(vector<UINT32>& allindexedIDsReturned);
	virtual RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes);

	bool isValidityConditional(void);
	string&       getName(void) ;
	void          setName(string& s);
    virtual RETURNCODE Label(wstring &l) = 0;          /*returns the label of the item (or empty)*/
    virtual RETURNCODE Help(wstring &h);
    virtual bool        IsValid();     /*returns true if the item is currently valid*///return validity
	virtual bool        IsValidTest(); /*returns true if valid and CACHED (does not read to resolve) */
	virtual bool        is_UNKNOWN(void);/* stevev 21jan05---tests for 3rd state */
	virtual void        setDependentsUNK(void);/* for dependentents to be recalculated */
	virtual hCattrBase*  getaAttr(varAttrType_t attrType);/*Variables ONLY - override for others*/
	virtual itemID_t    getAttrID (unsigned  attr, int which = 0);
	virtual hCVar*		getAttrPtr(unsigned  attr, int which = 0); 
	hCitemBase(DevInfcHandle_t h);
	hCitemBase(const hCitemBase& ib) :hCobject((hCobject)ib){*this=ib;};
	hCitemBase( hCitemBase* pSrc, itemIdentity_t newID);/* from-typedef constructor */
	hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, itemID_t  iId,  unsigned long Dkey);
	hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, ITEM_SIZE  iSz);
	hCitemBase(DevInfcHandle_t h, aCitemBase* apIb );// from dllapi class
	virtual ~hCitemBase();
	virtual RETURNCODE destroy(void);

	///// overloads /////
	hCitemBase& operator=(const aCitemBase& ib);
	hCitemBase& operator=(const hCitemBase& ib);

	virtual RETURNCODE dumpSelf(int indent = 0, char* typeName = NULL);

	operator hCVar*();
};


typedef vector<hCVar*> varPtrList_t;

#endif
