/**
 *	@(#) $Id: cm_struc.h,v 1.1 2012/07/10 20:59:28 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_struct.h - Connection Manager Structure Definitions
 *
 *	This file contains all structure definitions for
 *	the Connection Manager Library.
 */

#ifndef CM_STRUCT_H
#define CM_STRUCT_H

#include <rtn_code.h>
#include "dds_upcl.h"
#include "ddi_lib.h"
#include "cm_dds.h"
/*
 *	Connection Manager Modes
 */

#define	CMM_ON_LINE		1
#define	CMM_OFF_LINE	2


/*
 * CM_COMPAT types
 */

#define OLDER_DD_LOADED		0
#define CURRENT_DD_LOADED	1
#define	NEWER_DD_LOADED		2
#define DD_NOT_LOADED		3


/*
 *	Connection Manager Typedefs
 */

typedef	int	CM_COMPAT ;


typedef struct tag_DD_DEVICE_ID {
	UINT32          ddid_manufacturer	;
	UINT16          ddid_device_type;
	UINT16          ddid_device_rev;
	int           ddid_dd_rev;  // Changed to int so we can know when this has been initialized. DDC
    wchar_t			dd_file_path[512];
	DDOD_HEADER		header;
} DD_DEVICE_ID;



#endif	/* CM_STRUCT_H */
