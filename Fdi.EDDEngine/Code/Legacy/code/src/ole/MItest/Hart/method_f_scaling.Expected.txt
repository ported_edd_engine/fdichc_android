﻿<Method>
{
 float  x;
 double  y;

 x = fgetval();
 postreadtest = method_multiply(x,2.0);
 x = postreadtest;
 y = fsetval(x);
 save_values();
}
</Method>
<Function>
Function Name: fgetval
  Number Of Input Parameters: 0
  Returned Param Type: RUL_DOUBLE
  Returned Param Value: 55.700001
End of Function fgetval

<Sub-Method: method_multiply(float A = 55.700001, float B = 2.000000)>
{
 return A * B;
}
</Sub-Method>
<Function>
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><A>55.70000,</A><B>2.00000,</B><__RetVar__XXX>111.40000,</__RetVar__XXX></SymbolTable>

IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 16418, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: FLOAT,  Value: 111.400002
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 16418, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: FLOAT,  Value: 111.400002
Function Name: fsetval
  Number Of Input Parameters: 1
    Param 1 Type: RUL_FLOAT
    Param 1 Value: 111.400002
  Returned Param Type: RUL_DOUBLE
  Returned Param Value: 111.400002
End of Function fsetval
IBuiltinSupport ProcessChangedValues() is being called
IBuiltinSupport ProcessChangedValues with returned value 0 () is returned
Function Name: save_values
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function save_values
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><x>111.40000,</x><y>111.400001526,</y></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
