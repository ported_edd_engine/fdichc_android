QMAKE_CXXFLAGS += -Wno-unused-parameter -Wno-unused-parameter -Wno-unknown-pragmas -Wno-attributes -Wint-to-pointer-cast
QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter -Wno-unknown-pragmas -Wno-attributes -Wint-to-pointer-cast
DEFINES += UNICODE

#QMAKE_CXXFLAGS += -wd4100
#QMAKE_CXXFLAGS += -Wno-unused-parameter
#QMAKE_CXXFLAGS += /wd4100 /wd4101 /wd4102 /wd4189 /wd4996
#QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter
#QMAKE_CXXFLAGS += -wd4100 -wd4101 -wd4102 -wd4189 -wd4996
