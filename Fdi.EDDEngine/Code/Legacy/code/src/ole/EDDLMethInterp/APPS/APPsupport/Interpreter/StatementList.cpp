// StatementList.cpp //

#include "stdafx.h"
#include "StatementList.h"
#include "Statement.h"
#include "ParserBuilder.h"
#include "Expression.h"
#include "GrammarNodeVisitor.h"

#include "ErrorDefinitions.h"


CStatementList::CStatementList()
{

}

CStatementList::~CStatementList()
{
	stmtListIterator = m_stmtList.begin ();
	while (0 != m_stmtList.size())
	{
		CStatement*pStmt = NULL;
		pStmt = m_stmtList.back();
		//pStmt = (CStatement*)*stmtListIterator;
		if (pStmt != NULL) 
	{
			
			if (pStmt->GetNodeType () == NODE_TYPE_EXPRESSION)
			{
				delete (CExpression *)pStmt;
			}
			else
			{
				delete pStmt;
			}
			pStmt = NULL;
		}

		m_stmtList.pop_back ();
	}

	m_stmtList.clear();
}

_INT32 CStatementList::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitStatementList(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

//1.	Query the builder for the next parser.
//2.	Ask the parser to build the corrsponding parse tree.
_INT32 CStatementList::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CParserBuilder builder;
	CGrammarNode* pStmt=0;
	_INT32 i32Ret = PARSE_SUCCESS;
	int i = 0;

	while(true)
	{
		try
		{
			i++;
			pStmt = builder.CreateParser(plexAnal, STMT_asic);
			if (pStmt == 0)
			{
				if (plexAnal->IsEndOfSource())
				{
					i32Ret = PARSE_SUCCESS;
					break;
				}
				else
				{
					CToken* pToken = 0;

					try
					{
						if((LEX_FAIL != plexAnal->GetNextToken(&pToken,pSymbolTable))
							&& pToken)
						{
							if (pToken->GetSubType() == RUL_SEMICOLON)
							{
								DELETE_PTR(pToken);
								continue;
							}
							else
							{
								plexAnal->UnGetToken ();
							}
						}
					
						DELETE_PTR(pToken);
						DELETE_PTR(pStmt);
						i32Ret = PARSE_FAIL;
						break;
					}
					catch (_INT32 error)
					{
						DELETE_PTR(pToken);
						DELETE_PTR(pStmt);
						throw (error);
					}
				}
			}
			AddStatement((CStatement*)pStmt);
			i32Ret = pStmt->CreateParseSubTree(
				plexAnal,
				pSymbolTable,
				pvecErrors);
			pStmt->SetStmtScopeStack(pSymbolTable->m_sScope);
			if (i32Ret == PARSE_FAIL)
			{
				break;
			}
		}
		catch(_INT32 error)
		{
			ADD_ERROR(error);
		}
	}//end of while loop
	
	return i32Ret;
}

void CStatementList::Identify(
			_CHAR* szData)
{
	_CHAR szNum1[15];
	_CHAR szNum2[15];
	strcat(szData,"<StatementList>");
	int nSize = (int)m_stmtList.size();
	for(int i=0; i<nSize; i++)
	{
		memset(szNum1,0,15);
		memset(szNum2,0,15);
		sprintf(szNum1,"<Statement%02d>",i);
		sprintf(szNum2,"</Statement%02d>",i);
		strcat(szData,szNum1);
		m_stmtList[i]->Identify(szData);
		strcat(szData,szNum2);
	}
	strcat(szData,"</StatementList>");
}

STATEMENT_LIST* CStatementList::GetStmtList()
{
	return &m_stmtList;
}

_INT32 CStatementList::GetLineNumber()
{
	if (m_stmtList.size() > 0)
	{
		return m_stmtList[m_stmtList.size()-1]->GetLineNumber();
	}
	else
	{
		return 0;
	}
}

bool CStatementList::AddStatement(CGrammarNode* pStmt)
{
	m_stmtList.push_back((CStatement*)pStmt);
	return true;
}
