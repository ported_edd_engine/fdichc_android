// Declaration.cpp //

#include "stdafx.h"
#include "Declaration.h"
#include "INTER_SAFEARRAY.h"

#include "ErrorDefinitions.h"
#include "SynchronisationSet.h"
//#include "PlatformCommon.h"

void	TokenType_to_VariantType(
			RUL_TOKEN_TYPE token,
			RUL_TOKEN_SUBTYPE subtoken,
			VARIANT_TYPE& vt);

CDeclaration::CDeclaration()
{

}

CDeclaration::~CDeclaration()
{

}

_INT32 CDeclaration::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	//Anil 180107 Commented Below. The call come here where we are executing the Statement list which has declaration
	//This is a bug fix to get rid of the variable which is declared withing the scope
	//the below Dd Method code was not handled
	/*MethodDefination 
	{
		int x;
		x = 0;
		if(int x == 0)
		{
			int y; 
			ACKNOWLEDGE("This was not executing");
		}
		

	}*/
	//return 0;
	return VISIT_SCOPE_VAR;
}

/*

Declaration =>	Type Id_List;
Id_List		=>	Single_Elt, Id_List | Single_Elt
Single_Elt	=>	Id Static_Dim_List | Id
Id			=>	letter(letter|digit)*

This is implemented as a State Machine.
Initial State
=============
	lboxState = rboxState = numState = commaState = false
	idState = true

Final State
===========
	lboxState = X
	rboxState = numState = idState = false
	commaState = true;
	
*/
_INT32 CDeclaration::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CToken* pToken = 0;
	CToken* pSymToken = 0;

	try
	{
		if((LEX_FAIL ==plexAnal->GetNextToken(&pToken,pSymbolTable))
			|| !pToken)
		{
			DELETE_PTR(pToken);//clean up memory, even on errors
			throw(C_UM_ERROR_MISSINGTOKEN);
		}

		RUL_TOKEN_SUBTYPE SubType = pToken->GetSubType();
		DELETE_PTR(pToken);

		bool lboxState=false;
		bool rboxState = false;
		bool numState = false;
		bool idState = true;
		bool commaState = false;
		CToken* pArrToken=0;

		while((LEX_FAIL !=plexAnal->GetNextVarToken(&pToken,pSymbolTable,SubType))
				&& pToken 
				&&(!pToken->IsEOS()))
		{
			if((!pToken->IsSymbol()
				&& (pSymToken = pSymbolTable->Find(pToken->GetLexeme())) )
 				||  (RUL_LBOX == pToken->GetSubType())
				||  (RUL_RBOX == pToken->GetSubType())
				||	(pToken->IsNumeric()))
			{
				if(pToken->IsArrayVar())
				{
					lboxState = true;
					rboxState = false;
					numState = false;
					idState = false;
					commaState = false;

					pSymToken->SetSubType(SubType);
					//Make a copy of the array Token
					DELETE_PTR(pArrToken);
					pArrToken = new CToken(*pToken);
				}
				else if(lboxState)
				{
					if(pToken->GetSubType() != RUL_LBOX)
					{
						plexAnal->SynchronizeTo(
							DECLARATION,
							pSymbolTable);
						DELETE_PTR(pArrToken);
						throw(C_DECL_ERROR_LBOX);
					}
					lboxState = false;
					rboxState = true;
					numState = false;
					idState = false;
					commaState = false;
				}
				else if(rboxState)
				{
					bool bIsOpenCloseSquareBracket = false;
					if(RUL_RBOX == pToken->GetSubType())
					{//to support Profibus doing char a[]; it is possible our state machine
					//gets here and we are expecting a number... well it aint there
					//so lets make it up on the fly CJK REDO
						bIsOpenCloseSquareBracket = true;
					}
					else if((0==pArrToken) || !pToken->IsNumeric())
					{
						plexAnal->SynchronizeTo(
							DECLARATION,
							pSymbolTable);
						DELETE_PTR(pArrToken);
						throw(C_DECL_ERROR_NUM);
					}

					lboxState = false;
					rboxState = false;
					numState  = true;
					idState   = false;
					commaState= false;
			//This is a number and with pArrToken get the symbol table token 
					
					_INT32 i32Idx = pArrToken->GetSymbolTableIndex(); 
					CVariable* pVar = pSymbolTable->GetAt(i32Idx);
					INTER_VARIANT& varArray = pVar->GetValue();
					VARIANT_TYPE vtSafeArray;

			//increment the dimension and set the limit of that dimension
					if(varArray.GetVarType() != RUL_SAFEARRAY)
					{//CJK REDO do we get here for char a[]
						// WS:EMP-17jul07:varArray.Clear();
						// WS:EMP-17jul07:varArray.varType = RUL_SAFEARRAY;
						// WS:EMP-17jul07:__VAL& val = (__VAL&)varArray.GetValue();
						// stevev-14feb08:make it more flexible...INTER_SAFEARRAYBOUND rgbound[1] = {atoi(pToken->GetLexeme())};
						if(bIsOpenCloseSquareBracket)
						{
											
							INTER_SAFEARRAYBOUND rgbound[1] = { 1 };//We will rely on Inter_Var to make it bigger if needed
							INTER_SAFEARRAY sa; 
                            _USHORT cDims = 0;  //
							TokenType_to_VariantType(RUL_NUMERIC_CONSTANT,	RUL_CHAR_DECL, vtSafeArray);
								
							// WS:EMP-17jul07 was::>(val.prgsa)->SetAllocationParameters(vtSafeArray, ++cDims, rgbound);
							sa.SetAllocationParameters(vtSafeArray, ++cDims, rgbound);
							varArray = &sa; // added WS:EMP-17jul07
							plexAnal->UnGetToken();

							

						}
						else if (SubType == RUL_DD_STRING_DECL)
						{
							//handling DD_STRING[] array varible, assuming DD_STRING array is one dimension
							//DD_STRING itself is an ushort array in one dimension 
							//plus DD_STRING array assuming one dimension. therefore,
							INTER_SAFEARRAYBOUND rgbound[2] = { 1, strtoul(pToken->GetLexeme(), NULL, 0) };
							INTER_SAFEARRAY sa;
							_USHORT cDims = 2;

							TokenType_to_VariantType(pToken->GetType(), SubType, vtSafeArray);

							sa.SetAllocationParameters(vtSafeArray, cDims, rgbound);
							varArray = &sa;
						}
						else
						{
							INTER_SAFEARRAYBOUND rgbound[1] = { strtoul(pToken->GetLexeme(),NULL,0) };
							INTER_SAFEARRAY sa; // WS:EMP-17jul07 was::>val.prgsa = new INTER_SAFEARRAY();
                            _USHORT cDims = 0;  // WS:EMP-17jul07 was::>_USHORT cDims = (val.prgsa)->GetDims();

							TokenType_to_VariantType(pToken->GetType(),	SubType, vtSafeArray);
								
							// WS:EMP-17jul07 was::>(val.prgsa)->SetAllocationParameters(vtSafeArray, ++cDims, rgbound);
							sa.SetAllocationParameters(vtSafeArray, ++cDims, rgbound);
							varArray = &sa; // added WS:EMP-17jul07
						}
					}
					else
					{
						__VAL& val = (__VAL&)varArray.GetValue();
						INTER_SAFEARRAYBOUND rgbound[1] = { strtoul(pToken->GetLexeme(),NULL,0) };
						(val.prgsa)->AddDim(rgbound);
					}
					
				}
				else if(numState)
				{
					if(pToken->GetSubType() != RUL_RBOX)
					{
						plexAnal->SynchronizeTo(
							DECLARATION,
							pSymbolTable);
						DELETE_PTR(pArrToken);
						throw(C_DECL_ERROR_RBOX);
					}

					//accept a Right box.
					lboxState = true;
					rboxState = false;
					numState = false;
					commaState = true;
					idState	= false;
				}
				else
				{
					if(idState)
					{
						if(pToken->GetType() != RUL_SIMPLE_VARIABLE)
						{
							plexAnal->SynchronizeTo(
								DECLARATION,
								pSymbolTable);
							DELETE_PTR(pArrToken);
							throw(C_DECL_ERROR_IDMISSING);
						}

						//if this is a DD_STRING variable
						if (SubType == RUL_DD_STRING_DECL)
						{
							//create initial array in symbol table
							_INT32 i32Idx = pSymToken->GetSymbolTableIndex();
							CVariable* pVar = pSymbolTable->GetAt(i32Idx);
							INTER_VARIANT& varArray = pVar->GetValue();
							VARIANT_TYPE vtSafeArray;

							INTER_SAFEARRAYBOUND rgbound[1] = { 1 };//We will rely on Inter_Var to make it bigger if needed
							INTER_SAFEARRAY sa;
                            _USHORT cDims = 1;

							TokenType_to_VariantType(RUL_NUMERIC_CONSTANT, RUL_UNSIGNED_SHORT_INTEGER_DECL, vtSafeArray);

							// WS:EMP-17jul07 was::>(val.prgsa)->SetAllocationParameters(vtSafeArray, ++cDims, rgbound);
							sa.SetAllocationParameters(vtSafeArray, cDims, rgbound);
							varArray = &sa;
						}

						pSymToken->SetSubType(SubType);
					}
					else
					{
						plexAnal->SynchronizeTo(
							DECLARATION,
							pSymbolTable);
						DELETE_PTR(pArrToken);
						throw(C_DECL_ERROR_COMMAMISSING);
					}
					lboxState = false;
					rboxState = false;
					numState = false;
					idState = false;
					commaState = true;
				}
			}
			else if(commaState)
			{
				if(pToken->GetSubType() != RUL_COMMA)
				{
					plexAnal->SynchronizeTo(
						DECLARATION,
						pSymbolTable);
					DELETE_PTR(pArrToken);
					throw(C_DECL_ERROR_COMMAMISSING);
				}

				DELETE_PTR(pArrToken);
				idState = true;
				commaState = false;
				lboxState = false;
				rboxState = false;
				numState = false;
			}
			else
			{
			//Of course, this is a problem case. 
			//Unfortunately, expressions in the declarations are not handled
				//ADD_ERROR(C_DECL_ERROR_EXPRESSION);
				plexAnal->SynchronizeTo(DECLARATION,
					pSymbolTable);

				//accept a Right box. //VMKP added on 030404
				/*  Synchronizing was not proper when an expression
				 present in the variable declaration, With that the 
				 below lines one declaration next to the expression
				 declaration is skipping */
				lboxState = true;
				rboxState = false;
				numState = false;
				commaState = true;
				idState	= false;
			}
			DELETE_PTR(pToken);//delete token within the while loop

			//Record current scope index for one local variable declaration by one variable
			if ( (plexAnal->m_sDeclarationStack.size() == 0) || (plexAnal->m_nIndentIndex > plexAnal->m_sDeclarationStack.top()) )
			{
				plexAnal->m_sDeclarationStack.push(plexAnal->m_nIndentIndex);
			}
		}//end of while loop

//Validate the exit criteria...
		if(!(rboxState == numState == idState == false)
			|| !(commaState == true))
		{
			plexAnal->SynchronizeTo(DECLARATION,
				pSymbolTable);
			DELETE_PTR(pArrToken);//delete the token outside the while loop
			throw(C_DECL_ERROR_UNKNOWN);
		}

		DELETE_PTR(pToken);//delete the token outside the while loop
		DELETE_PTR(pArrToken);//delete the token outside the while loop
		return PARSE_SUCCESS;
	}
	catch(_INT32 error)
	{
		plexAnal->MovePast(
			RUL_SYMBOL,
			RUL_SEMICOLON,
			pSymbolTable);
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}

	return PARSE_FAIL;
}

void CDeclaration::Identify(
		_CHAR* szData)
{
}

_INT32 CDeclaration::GetLineNumber()
{
	return -1;
}
