/*
 *	@(#) $Id: tags_sa.h,v 1.2 2012/09/24 23:54:37 rgretta Exp $
 *
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 */

#pragma once

#undef FMS_DD_STRS		// MHD We might want this! If Format Object goes away.

/*******************************************
 *******************************************
 **										  **
 **		Attribute Related Definitions	  **
 **										  **
 *******************************************
 *******************************************/


/*
 * Attribute tags.
 */

#define MIN_ATTR	(CLASS_TAG-1)	/* This must be < Minimum Attr number */

#define TYPE_TAG			33				// Not in binary
#define DISPLAY_FORMAT_TAG	34
#define EDIT_FORMAT_TAG		35
#define MAX_VALUE_TAG		36
#define MIN_VALUE_TAG		37
#define SCALING_FACTOR_TAG	38

#define DEFINITION_TAG		47
#define ITEM_ARRAYNAME_TAG	50

#define NO_TAG						0
#define ITEM_INFORMATION_TAG		50
#define ATTR_INFO_SPECIFIER			51
#define EXPRESSION_TAG				52
//#define DEFAULT_REFERENCE_VALUE_TAG 53
#define DEFAULT_VALUES_LIST_TAG		54
#define STATUS_CLASS_TAG			55
#define UUID_TAG					56
#define BLOCK_B_TAG					57
#define CHART_TYPE_TAG				58
#define LINE_TYPE_TAG				59

#define BOOLEAN_TAG					60
#define ASCII_STRING_TAG			61
#define ARGUMENT_LIST_TAG			62
#define STRING_TAG					63
#define AXIS_SCALING_TAG			64

#define CLASS_TAG					65
#define HANDLING_TAG				66
#define MIN_MAX_TAG					67
#define ACCESS_TAG					68
#define ORIENTATION_TAG				69

#define BYTE_ORDER_TAG				70
#define TIME_SCALE_TAG				71
#define TRANSACTION_TAG				72

#define ACTION_LIST_ELEMENT_TAG		73
#define ACTIONS_SEQLIST_TAG			74
#define DATA_ELEMENT_TAG			75
#define DATA_FIELD_SPECIFIER_TAG	76
#define ELEMENT_TAG					77
#define ELEMENT_SEQLIST_TAG			78

#define ENUMERATOR_TAG				79 
#define ENUMERATOR_SEQLIST_TAG		80 

#define GRID_ELEMENT_TAG			81
#define VECTOR_SEQLIST_TAG			82
#define MEMBER_TAG					83
#define MEMBER_SEQLIST_TAG			84
#define MENU_TAG					85
#define MENU_ITEM_SEQLIST_TAG		86 

#define METHOD_PARAMETER_TAG		87
#define METHOD_PARAMETER_SEQLIST_TAG		88
#define REFERENCE_TAG				89
#define REFERENCE_LIST_TAG			90
#define REFERENCE_SEQLIST_TAG		91
#define RESPONSE_CODE_TAG			92
#define RESPONSE_CODES_SEQLIST_TAG	93

#define COMPONENT_SEQLIST_TAG		94
#define COMPONENT_DEPICTION_TAG		95
#define RANGE_LIST_TAG				96


#define WRITE_MODE_TAG				97
#define EXPRESSION_LIST_TAG			98
#define RANGE_SET_TAG				99
#define EXTENDED_DESC_TAG			100

#define LAYOUT	0
#define MANUFACUTRER_EXT 1
#define DEVICE_TYPE_EXT 2
/*
 * Miscellaneous tags.
 */

 /*
 * component reference opcodes.
 */
#define MAXIMUM_NUMBER		1
#define MINIMUM_NUMBER		2
#define AUTO_CREATE			3
#define FILTER				4
#define REQUIRED_RANGES		5

/*
 * Expression opcodes.
 */
#define TRANSACTION_REQUEST_TAG				0
#define TRANSACTION_REPLY_TAG				1
#define TRANSACTION_RESP_CODES_TAG			2
#define TRANSACTION_POST_REQ_ACTIONS_TAG	3

#define NOT_OPCODE		1
#define NEG_OPCODE		2
#define BNEG_OPCODE		3

#define ADD_OPCODE		4
#define SUB_OPCODE		5

#define MUL_OPCODE		6
#define DIV_OPCODE		7
#define MOD_OPCODE		8

#define LSHIFT_OPCODE	9
#define RSHIFT_OPCODE	10

#define AND_OPCODE		11
#define OR_OPCODE		12
#define XOR_OPCODE		13

#define LAND_OPCODE		14
#define LOR_OPCODE		15

#define LT_OPCODE		16
#define GT_OPCODE		17
#define LE_OPCODE		18
#define GE_OPCODE		19
#define EQ_OPCODE		20
#define NEQ_OPCODE		21

#define	CONDITIONAL_OPCODE		22		// encoded like B-expr, T-expr, F-expr, "?:"

#define INTCST_OPCODE	25
#define UNSIGNEDCST_OPCODE	26
#define FPCST_OPCODE	27
#define STRCST_OPCODE	28
#define BOOLCST_OPCODE	29

#define REF_OPCODE	30

#define ATTRIBUTE_VALUE_OPCODE 31

#define MINIMAX_REF_OPCODE	32
#define METHOD_VALUE_REF_OPCODE	33

#define ARRAY_INDEX_OPCODE	35
#define SCALING_TYPE_OPCODE 36
#define OCTET_STRING_CONST_OPCODE 37
#define LAST_OPCODE			38





/*
 * Import by type flags.
 */

#define IMPORT_VARIABLES 1
#define IMPORT_COMMANDS 2
#define IMPORT_METHODS 4
#define IMPORT_MENUS 8
#define IMPORT_EDIT_DISPLAYS 16
#define IMPORT_RELATIONS 32
#define IMPORT_ITEM_ARRAYS 64
#define IMPORT_COLLECTIONS 128


/*
 *	conditional tags
 */

#define IF_TAG 		0
#define SELECT_TAG 	1
#define CASE_TAG	0
#define DEFAULT_TAG	1
#define OBJECT_TAG	2
#define DEFAULT_TAG_VALUE	0xff


/*
 * 	STRING tags
 */

#define DEV_SPEC_STRING_TAG     0	/* string device specific id */
#define DICTIONARY_STRING_TAG   1	/* dictionary string id */
#define VAR_REF_STRING_TAG      2	/* variable_reference_string */
#define ENUMERATION_STRING_TAG  3	/* enumeration string information */
#define ATTR_REF_STRING_TAG  4	/* enumeration string information */
#define EXPRESSION_STRING_TAG  5
#define ENUM_EXPR_STRING_TAG  6 /*enumeration string expression*/

/*
 *	Enumeration tags
 */

#define	ENUM_VALUE_TAG		0
#define	ENUM_DESC_TAG		1
#define	ENUM_HELP_TAG		2
#define	ENUM_STATUS_TAG		3
#define	ENUM_ACTIONS_TAG	4
#define	ENUM_CLASS_TAG		5
#define	ENUM_ENUMVALUE_TAG	6

#define ENUM_DV_STATUS_TAG	21
#define ENUM_TV_STATUS_TAG	22
#define ENUM_AO_STATUS_TAG	23
#define ENUM_ALL_STATUS_TAG	24	

/*
 *	Transaction tags
 */

#define	TRANS_REQ_TAG			0
#define	TRANS_REPLY_TAG			1
#define	TRANS_RESP_CODES_TAG	2
#define	TRANS_POST_ACTIONS		3


/*
 *	Block Item Name Table tags
 */

#define BINT_BLK_ITEM_NAME_TAG				0
#define BINT_ITEM_TBL_OFFSET_TAG			1
#define BINT_PARAM_TBL_OFFSET_TAG			2
#define BINT_PARAM_LIST_TBL_OFFSET_TAG		3
#define BINT_REL_TBL_OFFSET_TAG				4
#define BINT_ITEM_TO_CMD_TBL_OFFSET_TAG		5


/*
 *	Parameter Table tags
 */

#define PT_BLK_ITEM_NAME_TBL_OFFSET_TAG		0
#define PT_PARAM_MEM_TBL_OFFSET_TAG			1
#define PT_PARAM_MEM_COUNT_TAG				2
#define PT_PARAM_ELEM_TBL_OFFSET_TAG		3
#define PT_PARAM_ELEM_COUNT_TAG				4

#define PT_PARAM_ELEM_MAX_COUNT_TAG			5
#define PT_ARRAY_ELEM__ITEM_TBL_OFFSET_TAG	6

#define PT_ARRAY_ELEM_TYPE_OR_VAR_TYPE_TAG	7
#define PT_ARRAY_ELEM_SIZE_OR_VAR_SIZE_TAG	8
#define PT_ARRAY_ELEM_CLASS_VAR_CLASS_TAG	9

 /*
 *	Range Set tags
 */

#define	RANGE_SET_MAX_VAL			1
#define	RANGE_SET_MIN_VAL			2
#define	RANGE_SET_MAX_NUM			3
#define	RANGE_SET_MIN_NUM			4

/*
 * Reference types.
 * NOTE- An array is created as part of the tokenizer
 * verify subsystem which is indexed by these reference
 * type numbers.  For this reason, the numbers must be
 * positive numbers which start at or near zero and
 * are reasonably consecutive.  The array which is indexed
 * is created at run-time, so the order/values of the defines
 * may be changed without reflecting the change elsewhere.
 */
#define DEFAULT_TYPE_REF			-1

#define ITEM_ID_REF					0
#define VARIABLE_ID_REF				1
#define MENU_ID_REF					2
#define EDIT_DISP_ID_REF			3
#define METHOD_ID_REF				4
#define REFRESH_ID_REF				5
#define UNIT_ID_REF					6
#define WAO_ID_REF					7
#define ITEM_ARRAY_ID_REF			8
#define COLLECTION_ID_REF			9
#define BLOCK_B_ID_REF				10
#define BLOCK_ID_REF				11
#define RECORD_ID_REF				12
#define ARRAY_ID_REF				13
#define VAR_LIST_ID_REF				14
#define RESP_CODES_ID_REF			15
#define FILE_ID_REF					16
#define CHART_ID_REF				17
#define GRAPH_ID_REF				18
#define AXIS_ID_REF					19
#define WAVEFORM_ID_REF				20
#define SOURCE_ID_REF				21
#define LIST_ID_REF					22
#define GRID_ID_REF					23	
#define IMAGE_ID_REF				24
#define BLOB_ID_REF					25
#define PLUGIN_ID_REF				26
#define TEMPLATE_ID_REF				27
#define COMPONENT_ID_REF			28
#define COMPONENT_FOLDER_ID_REF		29
#define COMPONENT_REFERENCE_ID_REF	30
#define COMPONENT_RELATION_ID_REF	31
#define INTERFACE_ID_REF			32
#define ROWBREAK_REF				33
#define SEPARATOR_REF				34  // AKA COLUMNBREAK_REF
#define CONSTANT_REF				35	/* see Expression opcodes for type */
#define VIA_ITEM_ARRAY_REF			36
#define VIA_COLLECTION_REF			37
#define VIA_RECORD_REF				38
#define VIA_ARRAY_REF				39
#define VIA_VAR_LIST_REF			40		// Not supported in PROFIBUS
#define VIA_FILE_REF				41
#define VIA_CHART_REF				42
#define VIA_GRAPH_REF				43
#define VIA_SOURCE_REF				44
#define VIA_LIST_REF				45
#define VIA_BITENUM_REF				46
#define VIA_ATTRIBUTE_REF			47
#define VIA_RESOLVED_REF			48
#define VIA_XBLOCK_REF				49
#define VIA_BLOCK_REF				50		// Not supported in PROFIBUS
#define VIA_PARAM_REF				51
#define VIA_PARAM_LIST_REF			52		// Not supported in PROFIBUS
#define BLOCK_CHAR_REF				53
#define VIA_LOCAL_PARAM_REF			54
#define REF_WITH_ARGS_REF			55	// Method call or enumeration string
#define VIA_COMPONENT_REF			56
#define VIA_COMPONENT_RELATION_REF	57
#define VIA_OBJECT_REF				58
#define EXTERNAL_REF				59
#define VIA_NUMBERED_ATTRIBUTE_REF	60


/*******************************************
 *******************************************
 **										  **
 **		 Object Related Definitions		  **
 **										  **
 *******************************************
 *******************************************/


/*
 *	Definitions pertaining to the Header.
 */

	/*   Sizes of the Header data fields (in octets).   */

#define	MAGIC_NUMBER_SIZE			4
#define	HEADER_SIZE_SIZE			4

#define	META_DATA_SIZE_SIZE				4
#define	ITEM_OBJECTS_SIZE_SIZE			4

#define	MANUFACTURER_SIZE			3
#define	DEVICE_TYPE_SIZE			2
#define	DEVICE_REV_SIZE				1
#define	DD_REV_SIZE					1
#define LIT80_MAJOR_REV_SIZE		1
#define LIT80_MINOR_REV_SIZE		1
#define HEADER_EDD_PROFILE_SIZE		1
#define	TOOL_RELEASE_SIZE			1

#define	SIGNATURE_SIZE				4
#define	RESERVED3_SIZE				4
#define	RESERVED4_SIZE				4

#ifdef FMS_DD_STRS			// MHD We may want this if format object goes away

#define MANUFACTURER_STRING_SIZE	256
#define DEVICE_TYPE_STRING_SIZE		256

#define	HEADER_SIZE					(MAGIC_NUMBER_SIZE +	    \
									 HEADER_SIZE_SIZE +		    \
									 OBJECTS_SIZE_SIZE +	    \
									 DATA_SIZE_SIZE +		    \
									 MANUFACTURER_SIZE +	    \
									 DEVICE_TYPE_SIZE +		    \
									 DEVICE_REV_SIZE +		    \
									 DD_REV_SIZE +			    \
									 MANUFACTURER_STRING_SIZE + \
									 DEVICE_TYPE_STRING_SIZE +  \
									 TOOL_RELEASE_SIZE +		\
									 RESERVED2_SIZE +		    \
									 RESERVED3_SIZE +		    \
									 RESERVED4_SIZE)

#else
#define	HEADER_SIZE					(MAGIC_NUMBER_SIZE +	\
									 HEADER_SIZE_SIZE +		\
									 META_DATA_SIZE_SIZE +	\
									 ITEM_OBJECTS_SIZE_SIZE +		\
									 MANUFACTURER_SIZE +	\
									 DEVICE_TYPE_SIZE +		\
									 DEVICE_REV_SIZE +		\
									 DD_REV_SIZE +			\
									LIT80_MAJOR_REV_SIZE +	\
									LIT80_MINOR_REV_SIZE +	\
									HEADER_EDD_PROFILE_SIZE+		\
									TOOL_RELEASE_SIZE +	    \
									 SIGNATURE_SIZE +		\
									 RESERVED3_SIZE +		\
									 RESERVED4_SIZE)
#endif /*FMS_DD_STRS*/

	/*   Offsets of the Header data fields (in octets).   */

#define	MAGIC_NUMBER_OFFSET			0

#define	HEADER_SIZE_OFFSET			(MAGIC_NUMBER_OFFSET +	\
									 MAGIC_NUMBER_SIZE)

#define	META_DATA_SIZE_OFFSET		(HEADER_SIZE_OFFSET +	\
									 HEADER_SIZE_SIZE)

#define	ITEM_OBJECTS_SIZE_OFFSET	(META_DATA_SIZE_OFFSET +	\
									 META_DATA_SIZE_SIZE)

#define	MANUFACTURER_OFFSET			(ITEM_OBJECTS_SIZE_OFFSET +		\
									 ITEM_OBJECTS_SIZE_SIZE)

#define	DEVICE_TYPE_OFFSET			(MANUFACTURER_OFFSET +	\
									 MANUFACTURER_SIZE)

#define	DEVICE_REV_OFFSET			(DEVICE_TYPE_OFFSET +	\
									 DEVICE_TYPE_SIZE)

#define	DD_REV_OFFSET				(DEVICE_REV_OFFSET +	\
									 DEVICE_REV_SIZE)
#ifdef FMS_DD_STRS

#define	MANUFACTURER_STRING_OFFSET	(DD_REV_OFFSET +		\
									 DD_REV_SIZE)

#define	DEVICE_TYPE_STRING_OFFSET	(MANUFACTURER_STRING_OFFSET +		\
									 MANUFACTURER_STRING_SIZE)

#define	TOOL_RELEASE_OFFSET			(DEVICE_TYPE_STRING_OFFSET +		\
									 DEVICE_TYPE_STRING_SIZE)
#else

#define LIT80_MAJOR_REV_OFFSET		(DD_REV_OFFSET +		\
									 DD_REV_SIZE)

#define LIT80_MINOR_REV_OFFSET			(LIT80_MAJOR_REV_OFFSET + \
									 LIT80_MAJOR_REV_SIZE   )

#define HEADER_EDD_PROFILE_OFFSET			(LIT80_MINOR_REV_OFFSET + \
										LIT80_MINOR_REV_SIZE )

#define	TOOL_RELEASE_OFFSET			(HEADER_EDD_PROFILE_OFFSET +  \
									 HEADER_EDD_PROFILE_SIZE)
/* pre 10/1/04
#define	TOOL_RELEASE_OFFSET			(DD_REV_OFFSET +		\
									 DD_REV_SIZE)
*/



#endif /*FMS_DD_STRS*/

#define	SIGNATURE_OFFSET			(TOOL_RELEASE_OFFSET +		\
									 TOOL_RELEASE_SIZE)

#define	RESERVED3_OFFSET			(SIGNATURE_OFFSET +		\
									 SIGNATURE_SIZE)

#define	RESERVED4_OFFSET			(RESERVED3_OFFSET +		\
									 RESERVED3_SIZE)

/*
 *	Definitions pertaining to Format Object extension.
 *
 *   Sizes of the extension data fields (in octets).
 */

#define	CODING_FMT_MAJOR_SIZE		1

/* Sizes of the data fields (in octets) */
#define	EXTEN_LENGTH_SIZE			2
#define	CODING_FMT_MAJOR_SIZE		1
#define	CODING_FMT_MINOR_SIZE		1
#define	DD_REVISION_SIZE			1
#define	PROFILE_NUM_SIZE			2
#define	EDD_PROFILE_SIZE			2
#define	EDD_VERSION_SIZE			2
#define	MANUFACTURER_EXT_SIZE		1


/* Offsets of the data fields (in octets) */
#define	EXTEN_LENGTH_OFFSET			0
#define	CODING_FMT_MAJOR_OFFSET		(EXTEN_LENGTH_OFFSET +	\
									 EXTEN_LENGTH_SIZE)
#define	CODING_FMT_MINOR_OFFSET		(CODING_FMT_MAJOR_OFFSET +	\
									 CODING_FMT_MAJOR_SIZE)
#define	DD_REVISION_OFFSET			(CODING_FMT_MINOR_OFFSET +	\
									 CODING_FMT_MINOR_SIZE)
#define	PROFILE_NUM_OFFSET			(DD_REVISION_OFFSET +		\
									 DD_REVISION_SIZE)
#define EDD_PROFILE_OFFSET			(PROFILE_NUM_OFFSET +			\
									 PROFILE_NUM_SIZE)
#define EDD_VERSION_OFFSET			(EDD_PROFILE_OFFSET +		\
									 EDD_PROFILE_SIZE)
#define MANUFACTURER_EXT_OFFSET		(EDD_VERSION_OFFSET +		\
									 EDD_VERSION_SIZE)


	/*   Offsets of the extension data fields (in octets).   */

/*
 *	Definitions pertaining to directory object extensions.
 */

	/*   Sizes of the extension data fields (in octets).   */

#define DATAFIELD_OFFSET_SIZE		4
#define DATAFIELD_SIZE_SIZE		4

	/*   Offsets of the extension data fields (in octets).   */

#define TABLE_REF_LEN				(DATAFIELD_OFFSET_SIZE + \
									 DATAFIELD_SIZE_SIZE) 

#define	DIRECTORY_DATA_OFFSET			(EXTEN_LENGTH_OFFSET +	\
										 EXTEN_LENGTH_SIZE)

	/* Offsets of the Device Tables in the Device Directory */

#define BLK_REF_OFFSET				DIRECTORY_DATA_OFFSET

#define ITEM_REF_OFFSET				(BLK_REF_OFFSET + \
										 TABLE_REF_LEN)
#define STRING_REF_OFFSET				(ITEM_REF_OFFSET + \
										 TABLE_REF_LEN)
#define DICT_REF_REF_OFFSET				(STRING_REF_OFFSET + \
										 TABLE_REF_LEN)
#define LOCAL_VAR_REF_OFFSET			(DICT_REF_REF_OFFSET + \
										 TABLE_REF_LEN)
#define IMAGE_REF_OFFSET				(LOCAL_VAR_REF_OFFSET + \
										 TABLE_REF_LEN)
#define DICT_STRING_REF_OFFSET			(IMAGE_REF_OFFSET + \
										 TABLE_REF_LEN)
#define SYMBOL_REF_OFFSET				(DICT_STRING_REF_OFFSET + \
										 TABLE_REF_LEN)

	/* Offsets of Block Tables in the Block Directory */

#define BLK_ITEM_REF_OFFSET				DIRECTORY_DATA_OFFSET

#define BLK_ITEM_NAME_REF_OFFSET		(BLK_ITEM_REF_OFFSET + \
										 TABLE_REF_LEN)
#define PARAM_REF_OFFSET				(BLK_ITEM_NAME_REF_OFFSET + \
										 TABLE_REF_LEN)
#define PARAM_MEM_REF_OFFSET			(PARAM_REF_OFFSET + \
										 TABLE_REF_LEN)
#define PARAM_MEM_NAME_REF_OFFSET		(PARAM_MEM_REF_OFFSET + \
										 TABLE_REF_LEN)
#define PARAM_ELEM_REF_OFFSET			(PARAM_MEM_NAME_REF_OFFSET + \
										 TABLE_REF_LEN)

#define PARAM_LIST_REF_OFFSET			(PARAM_ELEM_REF_OFFSET + \
										 TABLE_REF_LEN)

#define PARAM_LIST_MEM_REF_OFFSET		(PARAM_LIST_REF_OFFSET + \
										 TABLE_REF_LEN)

#define PARAM_LIST_MEM_NAME_REF_OFFSET	(PARAM_LIST_MEM_REF_OFFSET + \
										 TABLE_REF_LEN)

#define CHAR_MEM_REF_OFFSET				(PARAM_LIST_MEM_NAME_REF_OFFSET + \
										 TABLE_REF_LEN)
#define CHAR_MEM_NAME_REF_OFFSET		(CHAR_MEM_REF_OFFSET + \
										 TABLE_REF_LEN)
#define REL_REF_OFFSET					(CHAR_MEM_NAME_REF_OFFSET + \
										 TABLE_REF_LEN)
#define UPDATE_REF_OFFSET				(REL_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define COMMAND_REF_OFFSET				(UPDATE_REF_OFFSET + \
                                         TABLE_REF_LEN)
#define CRIT_PARAM_REF_OFFSET		    (COMMAND_REF_OFFSET + \
                                         TABLE_REF_LEN)

/*
 *	Definitions pertaining to item object extensions.
 */

	/*   Sizes of the extension data fields (in octets).   */

#define	ITEM_TYPE_SIZE				1
#define	ITEM_SUBTYPE_SIZE			1
#define	ITEM_ID_SIZE				4
#define ATTR_MASK_SIZE				4

	/*   Offsets of the extension data fields (in octets).   */

#define	ITEM_TYPE_OFFSET			(EXTEN_LENGTH_OFFSET +	\
									 EXTEN_LENGTH_SIZE)

#define	ITEM_SUBTYPE_OFFSET			(ITEM_TYPE_OFFSET +		\
									 ITEM_TYPE_SIZE)

#define	ITEM_ID_OFFSET				(ITEM_SUBTYPE_OFFSET +	\
									 ITEM_SUBTYPE_SIZE)

#define	ATTR_MASK_OFFSET			(ITEM_ID_OFFSET +		\
									 ITEM_ID_SIZE)

#define HEADER_OFFSET				(ATTR_MASK_OFFSET +		\
									 ATTR_MASK_SIZE)


/*
 *	Definitions pertaining to attribute identifiers.
 */

// FD_FDI delete 
#define MAX_OBJ_EXTN_LEN	      0x10000	/* leave a 5 byte pad */


/*
 *	Definitions pertaining to object indices.
 */

#define	DEVICE_DIRECTORY_INDEX		1

#if 0
/*
 *	Selector tags.
 */

#define	SELECTOR_CAPACITY_TAG					0
#define	SELECTOR_CONSTANT_UNIT_TAG				1
#define	SELECTOR_COUNT_TAG						2
#define	SELECTOR_DEFAULT_VALUE_TAG				3
#define	SELECTOR_FIRST_TAG						4	//List.FIRST
#define	SELECTOR_HELP_TAG						5
#define	SELECTOR_INITIAL_VALUE_TAG				6
#define	SELECTOR_LABEL_TAG						7
#define	SELECTOR_LAST_TAG						8	//List.LAST
#define	SELECTOR_MAX_VALUE_N_TAG				9
#define	SELECTOR_MIN_VALUE_N_TAG				10
#define	SELECTOR_SCALING_TAG					11
#define	SELECTOR_SCALING_FACTOR_TAG				12
#define	SELECTOR_VARIABLE_STATUS_TAG			13
#define	SELECTOR_VIEW_MAX_TAG					14
#define	SELECTOR_VIEW_MIN_TAG					15
#define	SELECTOR_X_AXIS_TAG						16
#define	SELECTOR_Y_AXIS_TAG						17
#endif
/*
 *	Action Tags
 */
#define ACTION_SPECIFIER_REFERENCE	0
#define ACTION_SPECIFIER_DEFINITION	1

/*
 *	Response Codes Tags
 */
#define RESP_CODE_REFERENCE	0
#define RESP_CODE_LIST		1

