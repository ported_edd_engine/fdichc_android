/******************************************************************************
 * $Header: Uc2W.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:55 AM$
 *
 * Copyright 1999 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *		Include file for Uc2W class.
 *													   
 * $Workfile: Uc2W.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#if !defined(AFX_UC2W_H__D5899663_191C_11D3_81E6_006008AF8E7D__INCLUDED_)
#define AFX_UC2W_H__D5899663_191C_11D3_81E6_006008AF8E7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <AMSMiscUtilExport.h>

/******************************************************************************
Class:	Uc2W

Description:
	Unicode conversion (Uc) utility class for converting to const Unicode string.

Responsibilites:
	* Convert strings to const Unicode (wide) string.
	* Convert non-terminated strings to const Unicode (wide) string.
	* Encapsulate string-buffer memory allocation/deallocation.
	* Avoid dynamic allocation when possible.

Author:	Kevin Mixter
******************************************************************************/

class AMSMISCUTIL_EXPORT Uc2W  
{
public:
	Uc2W(LPCSTR);						// Construct from ANSI string
	Uc2W(LPCSTR, const int);			// Construct from non-terminated ANSI string
	Uc2W(LPCWSTR);						// Construct from Unicode string
	Uc2W(LPCWSTR, const int);			// Construct from non-terminated Unicode string
	virtual ~Uc2W();					// Destruct

	LPCWSTR asLPCWSTR() const;			// Conversion to const Unicode (wide) string

	int getLengthInChars() const;		// Get length (not including terminator) of converted string
	int getSizeInBytes() const;			// Get size (including terminator) of converted string

protected:
	Uc2W();
	void constructA2CW(LPCSTR, const int);	// Construct converted/terminated string from non-terminated generic string

	LPWSTR m_pwSzBuffer;				// Unicode (wide) string buffer, allocated as needed
	LPCWSTR m_pcwSz;					// Unicode (wide) string ptr

private:
	// Prevent the use of these methods (there is no implementation to force link errors)
	Uc2W(const Uc2W&);
	Uc2W& operator=(const Uc2W&) const;
};

#endif // !defined(AFX_UC2W_H__D5899663_191C_11D3_81E6_006008AF8E7D__INCLUDED_)
