// MethodThread.cpp : Implementation of CMethodThread
#include "stdafx.h"

#include "MethodInterfaceDefs.h"	//has to be prior to MethodThread.h
#include "MethodThread.h"
#include <Ole/EDDLMethInterp/pathtype.h>
#include <ServerProjects/PrePostReadWriteMethodMessages.h>
#include <Inf/NtcSpecies/BssProgLog.h>
#include <ServerProjects/rtn_code.h>
#include <BI_CODES.H>
#include "varientTag.h"

//
//
//
CMethodThread::CMethodThread( /*HWND hDeviceWindow,*/ long lMethodID, ProtocolType protocol )
{
	m_bCancel = false;

//	m_hDeviceWindow = hDeviceWindow;
	m_lMethodID = lMethodID;

	m_protocol = protocol;
	m_lErrorCode=0;
}

void CMethodThread::SetCancel(bool isCancelled)
{
	m_bCancel = isCancelled;
}

//
//
//
CMethodThread::~CMethodThread()
{
}

//
//
//
BOOL CMethodThread::InitInstance()
{
    /*LPARAM*/ int lParam = METH_METHOD_ABORT;//means failure
	if( ExecuteMethodFunction() )
	{
		lParam = SUCCESS;//means success
	}
    //CWinThread is not used anymore, so commenting the below line
    //::PostMessage( m_hDeviceWindow, WM_METHOD_COMPLETE, 0, lParam );
	return FALSE;
}

//
//The last argument represents either array index or member ID or member name
//
long CMethodThread::MapStringToPPType( CStdString sProperty, CValueVarient& vMember )
{
	long lRetVal = 0;

	sProperty.MakeUpper();
	
	if( sProperty.Compare(_T("KIND")) == 0 )
	{
		lRetVal = PP_Kind;
	}
	else if( sProperty.Compare(_T("NAME")) == 0 )
	{
		lRetVal = PP_Name;
	}
	else if( sProperty.Compare(_T("ITEMID")) == 0 )
	{
		lRetVal = PP_ItemID;
	}
	else if( sProperty.Compare(_T("DEFAULTVALUE")) == 0 )
	{
		lRetVal = PP_DefaultValue;
	}
	else if( sProperty.Compare(_T("HELP")) == 0 )
	{
		lRetVal = PP_Help;
	}
	else if( sProperty.Compare(_T("DEFINITION")) == 0 )
	{
		lRetVal = PP_Definition;
	}
	else if( sProperty.Compare(_T("PARAMS")) == 0 )
	{
		lRetVal = PP_MethodParams;
	}
	else if( sProperty.Compare(_T("VALUE")) == 0 )
	{
		lRetVal = PP_Value;
	}
	else if( sProperty.Compare(_T("VALUEASSTRING")) == 0 )
	{
		lRetVal = PP_ValueAsString;
	}
	else if( sProperty.Compare(_T("LABEL")) == 0 )
	{
		lRetVal = PP_Label;
	}
	else if( sProperty.Compare(_T("TYPE")) == 0 )
	{
		lRetVal = PP_Type;
	}
	else if( sProperty.Compare(_T("HANDLING")) == 0 )
	{
		lRetVal = PP_Handling;
	}
	else if( sProperty.Compare(_T("VALIDITY")) == 0 )
	{
		lRetVal = PP_Validity;
	}
	else if( sProperty.Compare(_T("SIZE")) == 0 )
	{
		lRetVal = PP_Size;
	}
	else if( sProperty.Compare(_T("SCALINGFACTOR")) == 0 )
	{
		lRetVal = PP_ScalingFactor;
	}
	else if( sProperty.Compare(_T("DDL")) == 0 )
	{
		lRetVal = PP_DDL;
	}
	else if(sProperty.Compare(_T("ITEMLABEL")) == 0 )
	{
		lRetVal = PP_ItemLabel;
	}
	else if( sProperty.Compare(_T("INDEXEDITEMARRAY!ITEMID")) == 0 )
	{
		lRetVal = PP_IndexItemArray;
	}
	else if( (sProperty.Compare(_T("INDEXEDITEMARRAY!ELEMENT!COUNT")) == 0)	//This check must be prior to "ELEMENT" check
			 || (sProperty.Compare(_T("MEMBER!COUNT")) == 0) )	//This check must be prior to "ELEMENT" check
	{
		lRetVal = PP_Count;
	}
	else if( sProperty.Find( _T("ELEMENT!")) != -1 )
	{
		int nIndex1 = sProperty.Find( _T("!"));
		int nIndex2 = sProperty.Find( _T("!"), nIndex1+1 );
		CStdString sNumber = sProperty.Mid(nIndex1+1, nIndex2-nIndex1-1);
		CV_VT(&vMember, ValueTagType::CVT_UI4);
        CV_UI4(&vMember) = tcstoul(sNumber, NULL, 10);
		CStdString sRemainder = sProperty.Mid(nIndex2+1);
		lRetVal = MapStringToPPType( sRemainder, vMember );
	}
	else if( sProperty.Find( _T("MEMBER!")) != -1 )
	{
		int nIndex1 = sProperty.Find( _T("!"));
		int nIndex2 = sProperty.Find( _T("!"), nIndex1+1 );
		if (sProperty[nIndex1+1] == '$')
		{
			CStdString sNumber = sProperty.Mid(nIndex1+2, nIndex2-nIndex1-2);	//get rid of $
			CV_VT(&vMember, ValueTagType::CVT_UI4);
            CV_UI4(&vMember) = tcstoul(sNumber, NULL, 10);		//member id
		}
		else
		{
			CStdString memberName = sProperty.Mid(nIndex1+1, nIndex2-nIndex1-1);	//no $
			CV_VT(&vMember, ValueTagType::CVT_WSTR);
			CV_WSTR(&vMember) = memberName;
		}
		CStdString sRemainder = sProperty.Mid(nIndex2+1);
		lRetVal = MapStringToPPType( sRemainder, vMember );
	}
	else if( sProperty.Compare(_T("MEMBERID")) == 0 )
	{
		lRetVal = PP_MemberID;
	}
	else if(sProperty.Compare(_T("ENUMASXML")) == 0)
	{
		lRetVal = PP_ENUMS_AS_XML;
	}
	else if(sProperty.Compare(_T("CLASS")) == 0)
	{
		lRetVal = PP_Class;
	}
	else if(sProperty.Compare(_T("EDITFORMAT")) == 0)
	{
		lRetVal = PP_EditFormat;
	}
	else if((sProperty.Compare(_T("MINCOUNT")) == 0) || (sProperty.Compare(_T("MAXCOUNT")) == 0))
	{
		lRetVal = PP_MinMaxCount;
	}
	else if(sProperty.Compare(_T("MINVALUE")) == 0)
	{
		lRetVal = PP_MinValue1;
	}
	else if(sProperty.Compare(_T("MAXVALUE")) == 0)
	{
		lRetVal = PP_MaxValue1;
	}
	else if(sProperty.Compare(_T("UNITS")) == 0)
	{
		lRetVal = PP_Units;
	}
	else if(sProperty.Compare(_T("NUMBEROFELEMENTS")) == 0)
	{
		lRetVal = PP_NumberOfElements;
	}
	else if(sProperty.Compare(_T("DISPLAYFORMAT")) == 0)
	{
		lRetVal = PP_DisplayFormat;
	}
	else if(sProperty.Compare(_T("SCALING")) == 0)
	{
		lRetVal = PP_AxisScaling;
	}
	else if(sProperty.Compare(_T("CAPACITY")) == 0)
	{
		lRetVal = PP_Capacity;
	}
	else if(sProperty.Compare(_T("XAXISITEMID")) == 0)
	{
		lRetVal = PP_XAxisItemID;
	}
	else if(sProperty.Compare(_T("YAXISITEMID")) == 0)
	{
		lRetVal = PP_YAxisItemID;
	}
	else if(sProperty.Compare(_T("INDEXEDVALUELIST")) == 0)
	{
		lRetVal = PP_IndexedValueList;
	}
	else if(sProperty.Compare(_T("LENGTH")) == 0)
	{
		lRetVal = PP_Length;
	}
	else
	{
		CStdString strLog;
		strLog.Format(L"Unsupported property=%s", sProperty);
		threadMILog(strLog.GetBuffer(), nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}
	

	return lRetVal;
}
