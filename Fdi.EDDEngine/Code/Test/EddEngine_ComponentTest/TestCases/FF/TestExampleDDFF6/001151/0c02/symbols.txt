member          UPLOAD_NOTIFY             parameter       0x4002000F 
member          METER_TUBE_DIAMETER_UNITS parameter       0x4002001C 
variable        status_contained_man      enumerated      0x20036 
variable        mfb_fluid_phase           enumerated      0x20019 
member          PRESSURE_LOWER_RANGE_VALUE parameter       0x40020020 
variable        mfb_gas_expansion_factor  float           0x20010 
variable        mfb_temperature_lower_range_value float           0x20024 
member          VL_FLUID_NAME             variable-list   0x40020034 
member          FLUID_VISCOSITY           parameter       0x40020009 
var-list        mass_flow_view_1                          0x2002A 
var-list        mass_flow_view_2                          0x2002B 
var-list        mass_flow_view_3                          0x2002C 
var-list        mass_flow_view_4                          0x2002D 
member          ORIFICE_BORE_DIAMETER     parameter       0x40020018 
variable        mfb_download_status       bit-enumerated  0x20013 
member          FLUID_DENSITY             parameter       0x40020008 
member          DIFF_PRESSURE_LOWER_RANGE_VALUE parameter       0x40020023 
variable        xa_segment_static         octetstring     0x20018 
member          GAS_EXPANSION_FACTOR      parameter       0x4002000A 
member          VL_ORIFICE_BORE_DIAMETER  variable-list   0x40020038 
member          TEMPERATURE_SOURCE        parameter       0x40020006 
member          PRIMARY_ELEMENT_CATEGORY  parameter       0x40020015 
variable        float_contained_inH2o68   float           0x20034 
record          mass_flow_character                       0x20001 
member          METER_TUBE_DIAMETER       parameter       0x4002001B 
member          FLUID_NAME                parameter       0x40020014 
member          XA_HEADER                 parameter       0x40020011 
member          VL_DISCHARGE_COEFFICIENT  variable-list   0x4002002C 
member          VL_DIFF_PRESSURE_UPPER_RANGE_VALUE variable-list   0x40020042 
variable        mfb_meter_tube_material   enumerated      0x20020 
member          TEMPERATURE_UPPER_RANGE_VALUE parameter       0x4002001D 
member          VL_FLUID_VISCOSITY        variable-list   0x4002002A 
member          VL_PRIMARY_ELEMENT_TYPE   variable-list   0x40020036 
variable        mfb_fluid_viscosity       float           0x2000F 
member          XA_TOKEN                  parameter       0x40020010 
array           mfb_collection_directory                  0x20005 
variable        float_contained_ro_lbPerSec float           0x20033 
variable        mfb_license_status        enumerated      0x2000D 
variable        mfb_fluid_density         float           0x2000E 
variable        mfb_transducer_directory_entry unsigned        0x20030 
variable        mfb_temperature_source    enumerated      0x2000C 
variable        mfb_diff_pressure_upper_range_value float           0x20028 
variable        mfb_temperature_upper_range_value float           0x20023 
member          DIFFERENTIAL_PRESSURE     parameter       0x40020001 
member          VL_ORIFICE_BORE_DIAMETER_UNITS variable-list   0x40020039 
variable        status_contained_ro       enumerated      0x20037 
member          VL_PRESSURE_SOURCE        variable-list   0x40020031 
member          VL_METER_TUBE_DIAMETER_UNITS variable-list   0x4002003C 
variable        mfb_differential_pressure_source enumerated      0x20008 
member          VL_PRIMARY_ELEMENT_CATEGORY variable-list   0x40020035 
member          ORIFICE_BORE_DIAMETER_UNITS parameter       0x40020019 
member          VL_REYNOLDS_NUMBER        variable-list   0x4002002D 
variable        upload_notify_static      boolean         0x20015 
member          PRIMARY_ELEMENT_TYPE      parameter       0x40020016 
variable        xa_header_static          octetstring     0x20017 
member          DISCHARGE_COEFFICIENT     parameter       0x4002000B 
member          LICENSE_STATUS            parameter       0x40020007 
record          mfb_pressure                              0x20009 
member          VL_DIFFERENTIAL_PRESSURE_SOURCE variable-list   0x40020030 
record          mfb_temperature                           0x2000B 
unit            orifice_bore_diameter_units_relation                 0x2002E 
variable        mfb_orifice_bore_material enumerated      0x2001D 
variable        mfb_meter_tube_diameter_units enumerated      0x20022 
member          VL_DIFF_PRESSURE_LOWER_RANGE_VALUE variable-list   0x40020043 
variable        mfb_transducer_type       enumerated      0x20003 
variable        mfb_diff_pressure_lower_range_value float           0x20029 
variable        mfb_discharge_coefficient float           0x20011 
member          PRESSURE_UPPER_RANGE_VALUE parameter       0x4002001F 
variable        mfb_meter_tube_diameter   float           0x20021 
unit            meter_tube_diameter_units_relation                 0x2002F 
member          PRESSURE                  parameter       0x40020003 
member          VL_TEMPERATURE_SOURCE     variable-list   0x40020032 
member          VL_METER_TUBE_DIAMETER    variable-list   0x4002003B 
member          VL_ORIFICE_BORE_MATERIAL  variable-list   0x40020037 
member          TEMPERATURE_LOWER_RANGE_VALUE parameter       0x4002001E 
member          VL_TEMPERATURE_UPPER_RANGE_VALUE variable-list   0x4002003D 
variable        mfb_pressure_upper_range_value float           0x20025 
member          VL_DOWNLOAD_INSTALL       variable-list   0x4002002F 
member          VL_DIFFERENTIAL_PRESSURE  variable-list   0x40020025 
variable        float_contained_psiA      float           0x20035 
variable        mfb_collection_directory_entry unsigned        0x20031 
member          VL_MASS_FLOW              variable-list   0x40020024 
member          PRESSURE_SOURCE           parameter       0x40020004 
variable        xa_token_static           octetstring     0x20016 
member          PRESSURE_TYPE             parameter       0x40020021 
member          FLUID_PHASE               parameter       0x40020013 
member          VL_PRESSURE_UPPER_RANGE_VALUE variable-list   0x4002003F 
member          VL_FLUID_DENSITY          variable-list   0x40020029 
member          DIFF_PRESSURE_UPPER_RANGE_VALUE parameter       0x40020022 
array           mfb_transducer_directory                  0x20002 
block           mass_flow_block                           0x20000 
member          DOWNLOAD_INSTALL          parameter       0x4002000E 
member          VL_LICENSE_STATUS         variable-list   0x40020028 
variable        mfb_primary_element_category enumerated      0x2001B 
member          VL_GAS_EXPANSION_FACTOR   variable-list   0x4002002B 
member          VL_PRESSURE               variable-list   0x40020026 
variable        float_contained_degF      float           0x20032 
member          VL_METER_TUBE_MATERIAL    variable-list   0x4002003A 
variable        mfb_fluid_name            visiblestring   0x2001A 
variable        mfb_reynolds_number       float           0x20012 
variable        mfb_orifice_bore_diameter float           0x2001E 
variable        mfb_pressure_lower_range_value float           0x20026 
member          VL_PRESSURE_TYPE          variable-list   0x40020041 
member          VL_DOWNLOAD_STATUS        variable-list   0x4002002E 
variable        mfb_xd_error              enumerated      0x20004 
record          mfb_differential_pressure                 0x20007 
variable        mfb_pressure_source       enumerated      0x2000A 
member          DIFFERENTIAL_PRESSURE_SOURCE parameter       0x40020002 
member          ORIFICE_BORE_MATERIAL     parameter       0x40020017 
member          MASS_FLOW                 parameter       0x40020000 
record          mfb_mass_flow                             0x20006 
variable        mfb_download_install      enumerated      0x20014 
member          TEMPERATURE               parameter       0x40020005 
variable        mfb_primary_element_type  enumerated      0x2001C 
member          VL_TEMPERATURE            variable-list   0x40020027 
member          REYNOLDS_NUMBER           parameter       0x4002000C 
member          VL_PRESSURE_LOWER_RANGE_VALUE variable-list   0x40020040 
member          XA_SEGMENT                parameter       0x40020012 
member          DOWNLOAD_STATUS           parameter       0x4002000D 
variable        mfb_pressure_type         enumerated      0x20027 
variable        mfb_orifice_bore_diameter_units enumerated      0x2001F 
member          METER_TUBE_MATERIAL       parameter       0x4002001A 
member          VL_FLUID_PHASE            variable-list   0x40020033 
member          VL_TEMPERATURE_LOWER_RANGE_VALUE variable-list   0x4002003E 
