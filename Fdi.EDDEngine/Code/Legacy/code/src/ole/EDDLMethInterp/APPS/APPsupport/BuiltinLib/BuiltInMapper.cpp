////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "BuiltInMapper.cpp"
//
// Author: Mike DeCoster
//
// Creation Date: 5-2-07
//
// Purpose: Handles a list of info about built-ins
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BuiltInMapper.h"
#include <assert.h>
#include "BuiltIn.h"

std::mutex CBuiltInMapper::m_mutex;			//protecting m_pInst
CBuiltInMapper* CBuiltInMapper::m_pInst = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInMapper::CBuiltInMapper()
: m_enStartedProtocol((ProtocolType)0), m_bStarted_hart(false), m_bStarted_fieldbus(false), m_bStarted_profibus(false)
{
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInMapper::~CBuiltInMapper()
{
	Shutdown();
	Clear();
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInMapper* CBuiltInMapper::GetInstance()
{
	m_mutex.lock();
	if (m_pInst == 0)
		m_pInst = new CBuiltInMapper;
	m_mutex.unlock();
	return m_pInst;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::DestroyInstance()
{
	m_mutex.lock();
	if (m_pInst)
	{
		delete m_pInst;
		m_pInst = 0;
	}
	m_mutex.unlock();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::StartUp(ProtocolType type)
{
	Clear();

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Add protocol independant mappings
	////////////////////////////////////////////////////////////////////////////////////////////////
	CBuiltInInfo tmp;

	////////////////////////////////////////////////////////////////////////////////////////////////
	// *********** IMPORTANT ***********
	////////////////////////////////////////////////////////////////////////////////////////////////
	// Don't change the order in which the below operators are pushed onto the list.  During
	// the parsing process the tokenizer identifies operators by grabbing 1 from this list and
	// comparing it to the current string it is building from the source, if the string being
	// built has a length == to the operator from the list it checks for a match....
	//
	// what this means is that if two operators share chars (such as "!" and "!=") then the
	// "!=" _MUST_ come prior to the "!" in the list to avoid a "!=" always being erroneously
	// identified as a "!" followed by a "="........... MTD
	////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//		Enum Mapping		   OP	 Rule Type					Rule Subtype				Return Type	  # Param Types
	//		====================== ===== ========================== =========================== ============= = =============
	tmp.Set(BUILTIN_LANG_INTRINSIC,"//", RUL_COMMENT,				RUL_SUBTYPE_NONE,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"+=", RUL_ASSIGNMENT_OPERATOR,	RUL_PLUS_ASSIGN,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"-=", RUL_ASSIGNMENT_OPERATOR,	RUL_MINUS_ASSIGN,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"/=", RUL_ASSIGNMENT_OPERATOR,	RUL_DIV_ASSIGN,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"%=", RUL_ASSIGNMENT_OPERATOR,	RUL_MOD_ASSIGN,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"*=", RUL_ASSIGNMENT_OPERATOR,	RUL_MUL_ASSIGN,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"!=",RUL_RELATIONAL_OPERATOR,	RUL_NOT_EQ,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"&=", RUL_ASSIGNMENT_OPERATOR,	RUL_BIT_AND_ASSIGN,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"|=", RUL_ASSIGNMENT_OPERATOR,	RUL_BIT_OR_ASSIGN,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"^=", RUL_ASSIGNMENT_OPERATOR,	RUL_BIT_XOR_ASSIGN,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,">>=",RUL_ASSIGNMENT_OPERATOR,	RUL_BIT_RSHIFT_ASSIGN,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"<<=",RUL_ASSIGNMENT_OPERATOR,	RUL_BIT_LSHIFT_ASSIGN,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"++",RUL_ARITHMETIC_OPERATOR,	RUL_PLUS_PLUS,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"--",RUL_ARITHMETIC_OPERATOR,	RUL_MINUS_MINUS,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"+", RUL_ARITHMETIC_OPERATOR,	RUL_PLUS,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"-", RUL_ARITHMETIC_OPERATOR,	RUL_MINUS,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"*", RUL_ARITHMETIC_OPERATOR,	RUL_MUL,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"/", RUL_ARITHMETIC_OPERATOR,	RUL_DIV,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"%", RUL_ARITHMETIC_OPERATOR,	RUL_MOD,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"&&",RUL_LOGICAL_OPERATOR,		RUL_LOGIC_AND,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"||",RUL_LOGICAL_OPERATOR,		RUL_LOGIC_OR,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"!", RUL_LOGICAL_OPERATOR,		RUL_LOGIC_NOT,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"&", RUL_ARITHMETIC_OPERATOR,	RUL_BIT_AND,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"|", RUL_ARITHMETIC_OPERATOR,	RUL_BIT_OR,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"^", RUL_ARITHMETIC_OPERATOR,	RUL_BIT_XOR,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"~", RUL_ARITHMETIC_OPERATOR,	RUL_BIT_NOT,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,">>",RUL_ARITHMETIC_OPERATOR,	RUL_BIT_RSHIFT,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"<<",RUL_ARITHMETIC_OPERATOR,	RUL_BIT_LSHIFT,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"<=",RUL_RELATIONAL_OPERATOR,	RUL_LE,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,">=",RUL_RELATIONAL_OPERATOR,	RUL_GE,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"<", RUL_RELATIONAL_OPERATOR,	RUL_LT,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,">", RUL_RELATIONAL_OPERATOR,	RUL_GT,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"==",RUL_RELATIONAL_OPERATOR,	RUL_EQ,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"=",  RUL_ASSIGNMENT_OPERATOR,	RUL_ASSIGN,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"{", RUL_SYMBOL,					RUL_LBRACK,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"}", RUL_SYMBOL,					RUL_RBRACK,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"(", RUL_SYMBOL,					RUL_LPAREN,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,")", RUL_SYMBOL,					RUL_RPAREN,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"[", RUL_SYMBOL,					RUL_LBOX,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"]", RUL_SYMBOL,					RUL_RBOX,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,";", RUL_SYMBOL,					RUL_SEMICOLON,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,":", RUL_SYMBOL,					RUL_COLON,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,",", RUL_SYMBOL,					RUL_COMMA,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,".", RUL_SYMBOL,					RUL_DOT,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"?", RUL_SYMBOL,					RUL_QMARK,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"if",			RUL_KEYWORD,	RUL_IF,						RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"else",			RUL_KEYWORD,	RUL_ELSE,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"switch",		RUL_KEYWORD,	RUL_SWITCH,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"case",			RUL_KEYWORD,	RUL_CASE,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"default",		RUL_KEYWORD,	RUL_DEFAULT,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"while",			RUL_KEYWORD,	RUL_WHILE,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"for",			RUL_KEYWORD,	RUL_FOR,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"do",			RUL_KEYWORD,	RUL_DO,						RUL_TYPE_NONE,0);  Add(tmp);	
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned long long", RUL_KEYWORD,	RUL_UNSIGNED_LONG_LONG_DECL,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned long", RUL_KEYWORD,	RUL_UNSIGNED_INTEGER_DECL,			RUL_TYPE_NONE,0);  Add(tmp);	
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned int",	RUL_KEYWORD,	RUL_UNSIGNED_INTEGER_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned short",RUL_KEYWORD,	RUL_UNSIGNED_SHORT_INTEGER_DECL,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned char", RUL_KEYWORD,	RUL_UNSIGNED_CHAR_DECL,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"unsigned",		RUL_KEYWORD,	RUL_UNSIGNED_INTEGER_DECL,			RUL_TYPE_NONE,0);  Add(tmp);	
	tmp.Set(BUILTIN_LANG_INTRINSIC,"signed long long",	RUL_KEYWORD,	RUL_LONG_LONG_DECL,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"signed long",	RUL_KEYWORD,	RUL_LONG_DECL,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"signed int",	RUL_KEYWORD,	RUL_INTEGER_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"signed short",	RUL_KEYWORD,	RUL_SHORT_INTEGER_DECL,	RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"signed char",	RUL_KEYWORD,	RUL_CHAR_DECL,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"long long",		RUL_KEYWORD,	RUL_LONG_LONG_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"long",			RUL_KEYWORD,	RUL_LONG_DECL,				RUL_TYPE_NONE,0);  Add(tmp);	
	tmp.Set(BUILTIN_LANG_INTRINSIC,"int",			RUL_KEYWORD,	RUL_INTEGER_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"short",			RUL_KEYWORD,	RUL_SHORT_INTEGER_DECL,		RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"char",			RUL_KEYWORD,	RUL_CHAR_DECL,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"float",			RUL_KEYWORD,	RUL_REAL_DECL,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"double",		RUL_KEYWORD,	RUL_DOUBLE_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"break",			RUL_KEYWORD,	RUL_BREAK,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"continue",		RUL_KEYWORD,	RUL_CONTINUE,				RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"return",		RUL_KEYWORD,	RUL_RETURN,					RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"DD_STRING",		RUL_KEYWORD,	RUL_DD_STRING_DECL,			RUL_TYPE_NONE,0);  Add(tmp);
	tmp.Set(BUILTIN_LANG_INTRINSIC,"time_t",		RUL_KEYWORD,	RUL_LONG_DECL,				RUL_TYPE_NONE,0);  Add(tmp);
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Add builtins to mapping by requesting the protocol specific class 
	// to build what it deals with
	CBuiltIn* pThrowAway = CBuiltIn::ConstructChild(type);
	if (pThrowAway)
	{
		pThrowAway->BuildMapper();
		delete pThrowAway;  pThrowAway = 0;
		switch (type)
		{
		case nsEDDEngine::HART:
			m_bStarted_hart = true;
			break;
		case nsEDDEngine::FF:
			m_bStarted_fieldbus = true;
			break;
		case nsEDDEngine::PROFIBUS:
			m_bStarted_profibus = true;
			break;
		default:
			break;
		}
	}

#if 0
	DebugDump();
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::DebugDump()
{
	FILE* pFile = NULL;
    PS_wfopen_s(&pFile, L"BuiltInMap.txt", L"wt");

	std::vector<CBuiltInInfo> m_lsInfo;
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		m_lsInfo = m_lsInfo_hart;
		break;
	case nsEDDEngine::FF:
		m_lsInfo = m_lsInfo_fieldbus;
		break;
	case nsEDDEngine::PROFIBUS:
		m_lsInfo = m_lsInfo_profibus;
		break;
	}

	int iSize = (int)m_lsInfo.size();
	for (int i=0; i<iSize; ++i)
	{
		fwprintf(pFile, L"-----------------------------------------\n");
		fwprintf(pFile, L"%i:\n",i);
		m_lsInfo[i].DebugDump(pFile);
	}
	fclose(pFile);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::Shutdown()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::Clear()
{
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		m_lsInfo_hart.clear();
		break;
	case nsEDDEngine::FF:
		m_lsInfo_fieldbus.clear();
		break;
	case nsEDDEngine::PROFIBUS:
		m_lsInfo_profibus.clear();
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void CBuiltInMapper::Add(const CBuiltInInfo& info)
{
	/* check if info is in m_lsInfo already */
	std::vector<CBuiltInInfo> m_lsInfo;
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		m_lsInfo = m_lsInfo_hart;
		break;
	case nsEDDEngine::FF:
		m_lsInfo = m_lsInfo_fieldbus;
		break;
	case nsEDDEngine::PROFIBUS:
		m_lsInfo = m_lsInfo_profibus;
		break;
	}

	unsigned int iSize = (unsigned int)m_lsInfo.size();
	unsigned int i;
	for (i=0; i<iSize; ++i)
	{
		CBuiltInInfo temp = m_lsInfo[i];
		string cName = temp.GetName();
		string iName = info.GetName_copy();
		_UINT32 cNumParams = temp.GetNumberOfParams();
		const RUL_TOKEN_TYPE* cParam = temp.GetParamTypes();
		const RUL_TOKEN_TYPE* iParam = info.GetParamTypes();
		if ((temp.GetBuiltInEn() == info.GetBuiltInEn()) &&
			(temp.GetTokenType() == info.GetTokenType()) &&
			(temp.GetTokenSubtype() == info.GetTokenSubtype()) &&
			(temp.GetTokenReturnType() == info.GetTokenReturnType()) &&
			(cName.compare(iName) == 0) &&
			(cNumParams == info.GetNumberOfParams()) &&
			(memcmp((void *)cParam, (void *)iParam, sizeof(RUL_TOKEN_TYPE)*cNumParams) == 0)
			)
		{
			break;	//found
		}
	}
	if (i == iSize)
	{
		m_lsInfo.push_back(info);	//not found, then add it
	}

	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		m_lsInfo_hart.clear();
		m_lsInfo_hart = m_lsInfo;
		break;
	case nsEDDEngine::FF:
		m_lsInfo_fieldbus.clear();
		m_lsInfo_fieldbus = m_lsInfo;
		break;
	case nsEDDEngine::PROFIBUS:
		m_lsInfo_profibus.clear();
		m_lsInfo_profibus = m_lsInfo;
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo& CBuiltInMapper::operator[](unsigned int uIndex)
{
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		assert(uIndex >= 0 && uIndex <= m_lsInfo_hart.size());
		return m_lsInfo_hart[uIndex];
		break;
	case nsEDDEngine::FF:
		assert(uIndex >= 0 && uIndex <= m_lsInfo_fieldbus.size());
		return m_lsInfo_fieldbus[uIndex];
		break;
	case nsEDDEngine::PROFIBUS:
		assert(uIndex >= 0 && uIndex <= m_lsInfo_profibus.size());
		return m_lsInfo_profibus[uIndex];
		break;
	default:
		assert(uIndex >= 0 && uIndex <= m_lsInfo_hart.size());
		return m_lsInfo_hart[uIndex];
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo* CBuiltInMapper::operator[](const char *szName)
{
	std::vector<CBuiltInInfo>::iterator iter;
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		for (iter = m_lsInfo_hart.begin(); iter != m_lsInfo_hart.end(); ++iter)
		{
			if ( (*iter).GetName() == szName )
				return &(*iter);
		}
		break;
	case nsEDDEngine::FF:
		for (iter = m_lsInfo_fieldbus.begin(); iter != m_lsInfo_fieldbus.end(); ++iter)
		{
			if ( (*iter).GetName() == szName )
				return &(*iter);
		}
		break;
	case nsEDDEngine::PROFIBUS:
		for (iter = m_lsInfo_profibus.begin(); iter != m_lsInfo_profibus.end(); ++iter)
		{
			if ( (*iter).GetName() == szName )
				return &(*iter);
		}
		break;
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
CBuiltInInfo* CBuiltInMapper::Get(BUILTIN_NAME enName)
{
	std::vector<CBuiltInInfo>::iterator iter;
	switch (m_enStartedProtocol)
	{
	case nsEDDEngine::HART:
		for (iter = m_lsInfo_hart.begin(); iter != m_lsInfo_hart.end(); ++iter)
		{
			if ( (*iter).GetBuiltInEn() == enName )
				return &(*iter);
		}
		break;
	case nsEDDEngine::FF:
		for (iter = m_lsInfo_fieldbus.begin(); iter != m_lsInfo_fieldbus.end(); ++iter)
		{
			if ( (*iter).GetBuiltInEn() == enName )
				return &(*iter);
		}
		break;
	case nsEDDEngine::PROFIBUS:
		for (iter = m_lsInfo_profibus.begin(); iter != m_lsInfo_profibus.end(); ++iter)
		{
			if ( (*iter).GetBuiltInEn() == enName )
				return &(*iter);
		}
		break;
	}
	
	return 0;
}

void CBuiltInMapper::LockBIMapper(ProtocolType type)
{
	m_IsInfo_cs.lock();
	m_enStartedProtocol = type;
}

void CBuiltInMapper::ReleaseBIMapper()
{
	m_IsInfo_cs.unlock();
}
