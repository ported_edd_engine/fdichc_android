#include "stdafx.h"

#include "LegacyHart.h"
#include "LegacyHartFlats.h"
#include "LegacyHartFactory.h"

#include "DeviceTypeMgr.h"

CLegacyHart::CLegacyHart(const wchar_t *szFilename, const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode, UINT32 manufacturer, UINT16 device_type, UINT8 device_revision)
	: CEDDEngineImpl(szFilename, pIEDDEngineLogger, nsEDDEngine::LegacyHART_DDS, pErrorCode)
{
	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;
	int r_code = 0;
	BLK_LIST blk_list = {0};
	unsigned char hart_rev = 5;
	TCHAR tag[8] = {0};		
	SIMPLE_OPEN simple_open;
	m_Flat_Device_Dir = NULL;
    wchar_t xmlReleaseDir[MAX_PATH]{0};
    wchar_t xmlMfgDictPath[MAX_PATH]{0};

	SetProtocol(nsEDDEngine::HART);


	//
	//Set up the dictionary path and make sure we are reading the from the correct file
	//
    if (!GetFieldFromXML(L"HARTPath", pConfigXML, xmlReleaseDir, MAX_PATH - 1))
	{
		*pErrorCode = nsEDDEngine::CONFIG_FILE_ERROR;
		return;
	}

	m_connection_mgr.set_hart_dd_path( xmlReleaseDir );	
	
	ENV_INFO env_info(0, (IDDSSupport*)this, nullptr, L"");
	DWORD drs = 0;
	drs = g_StdDictTable.LoadDictionary(&env_info, xmlReleaseDir);
    if (drs != 0)
    {
		//ASSERT_DBG (drs == 0); // Replace this code with appropriate real error handling.
		*pErrorCode = nsEDDEngine::DICTIONARY_FILE_ERROR;
		return;
    }

	// This part is from CDDSXMLBuilder::AmsHartDDS_get_loaded_dd function


	if ( r_code == CM_SUCCESS )
	{
		simple_open.mfrid = manufacturer;
		simple_open.mfr_dev_type = device_type;
		if( (manufacturer > UCHAR_MAX) ||(device_type > UCHAR_MAX) )
		{
			hart_rev=7;
		}
		simple_open.xmtr_spec_rev = (UINT8)device_revision;
		simple_open.hart_rev = (UINT8)hart_rev;
        PS_Tcscpy( tag, _T("MOCK1") );
		simple_open.tag = tag;
		simple_open.bUseBasePathUnmodified = true;
		


		r_code = m_connection_mgr.ct_simple_hart_open( &env_info, m_szFilename.GetBuffer(), &simple_open, &blk_list );
		
		
        if ( r_code != CM_SUCCESS )
		{
			*pErrorCode = nsEDDEngine::LOAD_DD_AND_BLOCK_OPEN_ERROR;
			return;
		}
		else
		{
			m_block_handle = blk_list.list[0].block_handle;

			// Set the Manufacturer Dictionary Path.
			bool bHasMfgrDict = GetFieldFromXMLByID(L"HARTManufacturerDictPath",
				simple_open.mfrid, pConfigXML, xmlMfgDictPath, MAX_PATH - 1);
			if (bHasMfgrDict)
			{
				DEVICE_HANDLE dh = 0;
				m_connection_mgr.get_abt_adt_offset(m_block_handle, &dh);

				DEVICE_MANUFACTURER_HANDLE dmh = m_connection_mgr.get_adt_man_tbl_offset(dh);

				ActiveDevManufacturerTblElem *pMfgTblElem = g_DeviceTypeMgr.GetActiveDevManufacturerTblElem(dmh);

				pMfgTblElem->SetMfgDictPath(xmlMfgDictPath);
			}
		}
	}
	else
	{
		EddEngineLog(&env_info,  __FILE__, __LINE__, Error, L"LegacyHART", L"File is corrupt...Returned with error code of CM_FILE_ERROR file_size - current_offset < header_size" );
	}
	
}

CLegacyHart::~CLegacyHart()
{
	// make sure to clean FDI FLAT_DEVICE_DIR first and then DDS FLAT_DEVICE_DIR
	CLockDeviceTypeMgr dtMgrLock;
	
	Clean_FLAT_DEVICE_DIR(m_Flat_Device_Dir);
	free(m_Flat_Device_Dir);
	m_Flat_Device_Dir = nullptr;
	
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");
	
	m_connection_mgr.ct_block_close(&env_info, m_block_handle);

	m_connection_mgr.cm_cleanup (&env_info);
}



int CLegacyHart::GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID * pUnitItemId)
{
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, pValueSpec, L"");
	// Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert pParamSpec
	DDI_PARAM_SPECIFIER param_spec = {0};
	Init_DDI_PARAM_SPECIFIER(&env_info, &param_spec, pParamSpec);

	// Create a DDS TYPE_SIZE
    ITEM_ID unit_item_id = 0;
    int rc = ddi_get_unit(&env_info, &block_spec,&param_spec, &unit_item_id);
    if(rc == DDS_SUCCESS)
    {
		*pUnitItemId = unit_item_id;
    }
    return rc;
}

int CLegacyHart::GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID* pUnitItemId)
{
    // Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, pValueSpec, L"");	    
	int rc = ddi_find_unit_ids(&env_info, &block_spec, AxisItemId, pUnitItemId, NULL);
    return rc;
}

// Retrieves the List of Commands that can be used to read/write this parameter
int CLegacyHart::GetCommandList(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, nsEDDEngine::CommandType eCmdType, nsEDDEngine::FDI_COMMAND_LIST * pCommandList)
{
    // Convert iBlockInstance
    DDI_BLOCK_SPECIFIER block_spec = {0};
    Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

    unsigned int cmd_type = Set_DDI_COMMAND_TYPE(eCmdType);
    DDI_COMMAND_LIST cmdlist = {0};
	int rc = DDI_NO_COMMANDS_FOR_PARAM; 

	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

	if( pParamSpec->eType != nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX  )
	{
		// Convert pParamSpec
		DDI_PARAM_SPECIFIER param_spec = {0};
		Init_DDI_PARAM_SPECIFIER(&env_info, &param_spec, pParamSpec);

		rc = ddi_get_ptoc(&env_info, &block_spec, &param_spec, cmd_type, &cmdlist);
	}
	
	// If it is a complex reference or the easy lookup didn't work, create an OP_REF_INFO_LIST and try using the new extended PTOC table.
	if ( (pParamSpec->eType == nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX)
		|| (rc == DDI_NO_COMMANDS_FOR_PARAM) )
	{
		// Convert the FDI_PARAM_SPECIFIER into a OP_REF_INFO_LIST
		OP_REF_INFO_LIST OpRefInfoList = {0};
		Init_OP_REF_INFO_LIST( &OpRefInfoList, &env_info, &block_spec, pParamSpec );

		// Call a function to use the OP_REF_INFO_LIST as input to find the corresponding DDI_COMMAND_LIST
		rc = ddi_get_ext_ptoc_8(&env_info, &block_spec, &OpRefInfoList, (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID), cmd_type, &cmdlist);

		free ( OpRefInfoList.list );
		OpRefInfoList.list = NULL;
	}

	// if successful,
	//     Call Fill_COMMAND_LIST
	if(DDS_SUCCESS == rc)
	{
		Fill_COMMAND_LIST(iBlockInstance, pCommandList, &cmdlist);
		ddi_clean_command_list(&cmdlist);
	}
    return rc;
}

// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
int CLegacyHart::GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID *pCmdItemId)
{
    // Convert iBlockInstance
    DDI_BLOCK_SPECIFIER block_spec = {0};
    Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
    ITEM_ID itemId = {0};
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

    int rc = ddi_get_cmd_id(&env_info, &block_spec, ulCmdNumber, &itemId );
    if(DDS_SUCCESS == rc)
    {
        *pCmdItemId = itemId;
    }
    return rc;
}

// Converts an EDD Engine Error code to a string
wchar_t *CLegacyHart::GetErrorString(int iErrorNum)
{
    return dds_error_string(iErrorNum);
}

// GetStringTranslation() : Gets the specified language from multi-language string.
int CLegacyHart::GetStringTranslation(const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size)
{
	return ddi_get_string_translation(string,(wchar_t*)lang_code,outbuf, outbuf_size);
}

CConnectionMgr* CLegacyHart::GetConnectionManager()
{
	return &m_connection_mgr;
}


int CLegacyHart::GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType)
{
    // Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	
	
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

	// Convert pParamSpec
	DDI_ITEM_SPECIFIER item_spec = {0};
	Init_DDI_ITEM_SPECIFIER(&env_info, &item_spec, pItemSpec);

	// Create a DDS TYPE_SIZE
	ITEM_TYPE ItemType = {0};

    int rc = ddi_get_type(&env_info, &block_spec,&item_spec,&ItemType);
    if(DDS_SUCCESS == rc)
    {
        *pItemType = Set_ITEM_TYPE(ItemType);
    }
    return rc;
}

int CLegacyHart::GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE *pItemType, ITEM_ID *pItemId)
{
    // Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

	// Convert pParamSpec
	DDI_ITEM_SPECIFIER item_spec = {0};
	Init_DDI_ITEM_SPECIFIER(&env_info, &item_spec, pItemSpec);

	// Create a DDS TYPE_SIZE
	ITEM_TYPE ItemType = {0}; 
    // Create a DDS TYPE_SIZE
	ITEM_ID ItemID = {0};
	
	// Make the call into Hart DDS
	//                                  [in]         [in]				   [out]
	int rc = ddi_get_type_and_item_id(&env_info, &block_spec, &item_spec, &ItemType, &ItemID);
	
	// Convert to pItemType
	if(rc == DDS_SUCCESS)
	{
		*pItemType = Set_ITEM_TYPE(ItemType);
        *pItemId = ItemID;
	}

	return rc;
}

//GetParamType() : Gets the type and size of the specified paramaeter
int CLegacyHart::GetParamType(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, nsEDDEngine::TYPE_SIZE *pTypeSize)
{
	// Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

	// Convert pParamSpec
	DDI_PARAM_SPECIFIER param_spec = {0};
	Init_DDI_PARAM_SPECIFIER(&env_info, &param_spec, pParamSpec);

	// Create a DDS TYPE_SIZE
	TYPE_SIZE type_size = {0}; 
	
	// Make the call into Hart DDS
	//                         [in]         [in]         [out]
	int rc = ddi_get_var_type(&env_info, &block_spec, &param_spec, &type_size);
	
	// Convert to pTypeSize
	if(rc == DDS_SUCCESS)
	{
		Fill_EDD_TYPE_SIZE(pTypeSize, type_size);
	}
	return rc;	
}

//GetSymbolNameFromItemId(): Returns symbol name from sym info.
int CLegacyHart::GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size)
{
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");
	SYMINFO* syminfo = nullptr;
	DEVICE_TYPE_HANDLE dev_handle;
	m_connection_mgr.get_abt_adtt_offset( m_block_handle, &dev_handle );

 	int rc = g_DeviceTypeMgr.get_adtt_syminfo( dev_handle, &syminfo ) ;
	if(rc == DDS_SUCCESS)
	{
		rc = server_item_id_to_name(&env_info, syminfo, item_id, item_name, outbuf_size);
	}
	return rc;
}

//GetItemIdFromSymbolName(): Returns item id from sym info.
int CLegacyHart::GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* IptemId)
{
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");
	SYMINFO* syminfo = nullptr;
	DEVICE_TYPE_HANDLE dev_handle;
	m_connection_mgr.get_abt_adtt_offset( m_block_handle, &dev_handle );
 	
	int rc = g_DeviceTypeMgr.get_adtt_syminfo( dev_handle, &syminfo) ;
	if(rc == DDS_SUCCESS)
	{
		rc = server_item_name_to_id(&env_info, syminfo, pItemName, IptemId);
	}
	return rc;
}

// GetItem : Fills in an item data structure with the requested attributes
int CLegacyHart::GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec, nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, nsEDDEngine::FDI_GENERIC_ITEM * pGenericItem)
{
	
	DDI_ITEM_SPECIFIER		item_spec = {0};
	DDI_BLOCK_SPECIFIER		block_spec = {0};
	DDI_GENERIC_ITEM        generic_item = {0};
	 
	(void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));
	// Initializes DDI_BLOCK_SPECIFIER
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert to DDI_GENERIC_ITEM from FDI_GENERIC_ITEM
	Init_DDI_GENERIC_ITEM(&generic_item, pGenericItem);
	//ParamMap *pParamMap = new ParamMap();
	ENV_INFO env_info (m_block_handle, (IDDSSupport*)this, pValueSpec, lang_code);
	// Convert to DDI_ITEM_SPECIFIER from FDI_ITEM_SPECIFIER
	Init_DDI_ITEM_SPECIFIER(&env_info, &item_spec, pItemSpec);
	int rc = -1;
	DDI_ATTR_REQUEST mask = 0;
	switch(pGenericItem->item_type)
	{
	case nsEDDEngine::ITYPE_VARIABLE:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_VARIABLE);
			mask |= VAR_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_COMMAND:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_COMMAND);
			mask |= COMMAND_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_COLLECTION:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_COLLECTION);
			mask |= COLLECTION_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_ITEM_ARRAY:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_ITEM_ARRAY);
			mask |= ITEM_ARRAY_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_LIST:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_LIST);
			mask |= LIST_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_FILE:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_FILE);
			mask |= FILE_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_ARRAY:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_ARRAY);
			mask |= ARRAY_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_CHART:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_CHART);
			mask |= CHART_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_GRAPH:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_GRAPH);
			mask |= GRAPH_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_AXIS:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_AXIS);
			mask |= AXIS_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_WAVEFORM:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_WAVEFORM);
			mask |= WAVEFORM_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_SOURCE:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_SOURCE);
			mask |= SOURCE_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_GRID:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_GRID);
			mask |= GRID_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_IMAGE:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_IMAGE);
			mask |= IMAGE_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_METHOD:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_METHOD);
			mask |= METHOD_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_MENU:
		{
			*pAttributeNameSet += nsEDDEngine::style; 
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_MENU);
			mask |= MENU_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_EDIT_DISP:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_EDIT_DISP);
			mask |= EDIT_DISPLAY_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_REFRESH:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_REFRESH);
			mask |= REFRESH_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_UNIT:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_UNIT);
			mask |= UNIT_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_WAO:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_WAO);
			mask |= WAO_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_BLOCK:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_BLOCK);
			mask |= BLOCK_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_RESP_CODES:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_RESP_CODES);
			//mask |= RESP_CODE_DEBUG;	// Always add to get symbol_name
		}
		break;
	case nsEDDEngine::ITYPE_RECORD:
		{
			Fill_Bitmask( mask, *pAttributeNameSet,  nsEDDEngine::ITYPE_RECORD);
			mask |= RECORD_DEBUG;	// Always add to get symbol_name
		}
		break;
	default:
		break;
	}
	
	rc = ddi_get_item( &block_spec, &item_spec, &env_info, mask, &generic_item);

	if( (rc == DDS_SUCCESS) || (rc == DDL_DEFAULT_ATTR) || (rc == DDL_CHECK_RETURN_LIST) )
	{
		Fill_FDI_GENERIC_ITEM(pGenericItem, generic_item);

		// Only set rc from ddi_clean_item if is is non-zero
		// otherwise keep the DDL_DEFAULT_ATTTR or DDL_CHECK_RETURN_LIST from before
		int temp_rc = ddi_clean_item(&generic_item);
		if (temp_rc != DDS_SUCCESS)
		{
			rc = temp_rc;
		}
	}
	
	return rc;
}


// GetDeviceDir() : Gives pointer to device directory tables i.e. FLAT_DEVICE_DIR
int CLegacyHart::GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir)
{
	CLockDeviceTypeMgr dtMgrLock;

	int r_code = -1;
	if(m_Flat_Device_Dir == NULL)
	{	
		m_Flat_Device_Dir = (nsEDDEngine::FLAT_DEVICE_DIR*)calloc(1,sizeof(nsEDDEngine::FLAT_DEVICE_DIR));
		FLAT_DEVICE_DIR *pDdiFlatDeviceDir = NULL; 
		DDI_DEVICE_DIR_REQUEST		ddi_dir_req;
		ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

		r_code = m_connection_mgr.get_abt_dd_dev_tbls( m_block_handle, (void**)&pDdiFlatDeviceDir );

		if ( r_code == CM_SUCCESS )
		{
			if (!(pDdiFlatDeviceDir->attr_avail & STRING_TBL_MASK))
			{
				(void) memset((char  *)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
				ddi_dir_req.type = DD_DT_HANDLE;
				ddi_dir_req.mask = STRING_TBL_MASK;
				r_code = m_connection_mgr.get_abt_adtt_offset( m_block_handle, &ddi_dir_req.spec.device_type_handle ); 
				
				if(r_code == DDS_SUCCESS)
				{
					r_code = ddi_device_dir_request( &env_info, &ddi_dir_req, pDdiFlatDeviceDir );
				}
			}
		}
		else
		{
			//IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info.app_info);
			//pDDSSupport->DDS_Log(L"Device Tables not set... returned with error code %d", nsConsumer::LogSeverity::Error, L"" );
			EddEngineLog(&env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"Device Tables not set... returned with error code %d", r_code);
		}

		if(r_code == DDS_SUCCESS)
		{
			Fill_FLAT_DEVICE_DIR(m_Flat_Device_Dir,pDdiFlatDeviceDir);
			*pDeviceDir = m_Flat_Device_Dir;
		}
		
		//return r_code;
	}
	else
	{
		*pDeviceDir = m_Flat_Device_Dir;
		r_code = 0;
	}

	return r_code;
}

//GetEDDFileHeader : 
void CLegacyHart::GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader)
{	
	ROD_HANDLE rod_handle = 0;
	m_connection_mgr.get_abt_dd_handle(m_block_handle, &rod_handle);
	DDOD_HEADER * pDDOD_HEADER = g_DeviceTypeMgr.get_header(rod_handle);

	if (pDDOD_HEADER != nullptr)	// Make sure that it was set.
	{
		pEDDFileHeader->magic_number				= pDDOD_HEADER->magic_number;
		pEDDFileHeader->header_size					= pDDOD_HEADER->header_size;
		pEDDFileHeader->metadata_size				= pDDOD_HEADER->data_size;
		pEDDFileHeader->item_objects_size			= pDDOD_HEADER->objects_size;
		pEDDFileHeader->device_id.manufacturer		= pDDOD_HEADER->manufacturer;
		pEDDFileHeader->device_id.device_type		= pDDOD_HEADER->device_type;
		pEDDFileHeader->device_id.device_revision	= pDDOD_HEADER->device_revision;
		pEDDFileHeader->device_id.dd_revision		= pDDOD_HEADER->dd_revision;
		pEDDFileHeader->major_rev					= pDDOD_HEADER->lit80_major_rev;
		pEDDFileHeader->minor_rev					= pDDOD_HEADER->lit80_mminor_rev;
		pEDDFileHeader->edd_profile					= nsEDDEngine::EDD_Profile::PROFILE_HART;
//		pEDDFileHeader->reserved2					= pDDOD_HEADER->reserved2;
		pEDDFileHeader->reserved3					= pDDOD_HEADER->reserved3;
		pEDDFileHeader->reserved4					= pDDOD_HEADER->reserved4;
		pEDDFileHeader->layout						= nsEDDEngine::LayoutType::COLUMNWIDTH_EQUAL;
	}
}

// Init_DDI_BLOCK_SPECIFIER() - Initializes a DDI_BLOCK_SPECIFIER from Block handle
void CLegacyHart::Init_DDI_BLOCK_SPECIFIER( DDI_BLOCK_SPECIFIER *pBlockSpec, int iBlockInstance)
{
	pBlockSpec->type = DDI_BLOCK_HANDLE;
	pBlockSpec->block.handle = FindBlockHandle(iBlockInstance);  // lookup block handle for iBlockInstance
}

//Init_DDI_ITEM_SPECIFIER() - Initializes a DDI_ITEM_SPECIFIER from an FDI_ITEM_SPECIFIER
void CLegacyHart::Init_DDI_ITEM_SPECIFIER( ENV_INFO *env_info, DDI_ITEM_SPECIFIER *pItemSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pFdiItemSpec)
{
	switch(pFdiItemSpec->eType)
	{
	case nsEDDEngine::FDI_ITEM_ID :
		pItemSpec->type = DDI_ITEM_ID;
		pItemSpec->item.id = pFdiItemSpec->item.id;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_ID_SI :
		pItemSpec->type = DDI_ITEM_ID_SI;
		pItemSpec->item.id = pFdiItemSpec->item.id;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	case nsEDDEngine::FDI_ITEM_PARAM :
		pItemSpec->type = DDI_ITEM_PARAM;
		pItemSpec->item.param = pFdiItemSpec->item.param;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_PARAM_SI :
		pItemSpec->type = DDI_ITEM_PARAM_SI;
		pItemSpec->item.param = pFdiItemSpec->item.param;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	case nsEDDEngine::FDI_ITEM_BLOCK :
		pItemSpec->type = DDI_ITEM_BLOCK;
		pItemSpec->item.id = 0;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_CHARACTERISTICS :
		pItemSpec->type = DDI_ITEM_CHARACTERISTICS;
		pItemSpec->item.id = 0;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	default:
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
			L"CLegacyHart::Init_DDI_ITEM_SPECIFIER() cannot convert ItemSpecifierType: %d", pFdiItemSpec->eType );
	}

}

// Init_DDI_PARAM_SPECIFIER() - Initializes a DDI_PARAM_SPECIFIER from an FDI_PARAM_SPECIFIER
//                                                      	[in/out]			                     [in]
void CLegacyHart::Init_DDI_PARAM_SPECIFIER( ENV_INFO *env_info, DDI_PARAM_SPECIFIER *pDdiParamSpec, const nsEDDEngine::FDI_PARAM_SPECIFIER *pFdiParamSpec)
{
	
	switch(pFdiParamSpec->eType)
	{
	case nsEDDEngine::FDI_PS_ITEM_ID:
		pDdiParamSpec->type = DDI_PS_ITEM_ID;
		pDdiParamSpec->subindex = 0;
		pDdiParamSpec->item.id = pFdiParamSpec->id;
		break;
	case nsEDDEngine::FDI_PS_ITEM_ID_SI:
		pDdiParamSpec->type = DDI_PS_ITEM_ID_SI;
		pDdiParamSpec->subindex = pFdiParamSpec->subindex;
		pDdiParamSpec->item.id = pFdiParamSpec->id;
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET:
		pDdiParamSpec->type = DDI_PS_PARAM_OFFSET;
		pDdiParamSpec->subindex = 0;
		pDdiParamSpec->item.param = pFdiParamSpec->param;
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET_SI:
		pDdiParamSpec->type = DDI_PS_PARAM_OFFSET_SI;
		pDdiParamSpec->subindex = pFdiParamSpec->subindex;
		pDdiParamSpec->item.param = pFdiParamSpec->param;
		break;
	default:
		EddEngineLog( env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
			L"CLegacyHart::Init_DDI_PARAM_SPECIFIER() cannot convert ParamSpecifierType: %d", pFdiParamSpec->eType );
	}
}


// Init_OP_REF_INFO_LIST() - Initializes a OP_REF_INFO_LIST from an FDI_PARAM_SPECIFIER
//                              [in/out]                     [in]
void CLegacyHart::Init_OP_REF_INFO_LIST(	OP_REF_INFO_LIST *pOpRefInfoList,
								ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *pBlockSpec,
								const nsEDDEngine::FDI_PARAM_SPECIFIER *pFdiParamSpec)
{
	int rs = DDS_SUCCESS;

	switch(pFdiParamSpec->eType)
	{
	case nsEDDEngine::FDI_PS_ITEM_ID:
	case nsEDDEngine::FDI_PS_ITEM_ID_SI:
		{
			pOpRefInfoList->list = (OP_REF_INFO*)calloc(1, sizeof(OP_REF_INFO));
			pOpRefInfoList->count = 1;

			pOpRefInfoList->list[0].id		= pFdiParamSpec->id;
			pOpRefInfoList->list[0].member	= pFdiParamSpec->subindex;

			// type
			DDI_ITEM_SPECIFIER is = {0};
			is.type = DDI_ITEM_ID;
			is.item.id = pFdiParamSpec->id;

			ITEM_TYPE item_type = 0;

			rs = ddi_get_type(env_info, pBlockSpec, &is, &item_type);
			pOpRefInfoList->list[0].type = item_type;

		}
		break;

	case nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX:
		{
			// Figure out how many entries you need
			unsigned short cnt = pFdiParamSpec->RefList.count;

			pOpRefInfoList->list = (OP_REF_INFO*)calloc(cnt, sizeof(OP_REF_INFO));
			pOpRefInfoList->count = cnt;

			// Loop to copy
			for (int i=0; i < cnt; i++)
			{
				pOpRefInfoList->list[i].id		= pFdiParamSpec->RefList.list[i].id;
				pOpRefInfoList->list[i].type	= Set_DDI_ITEM_TYPE(pFdiParamSpec->RefList.list[i].type);
				pOpRefInfoList->list[i].member	= pFdiParamSpec->RefList.list[i].member;
			}
		}
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET:
		EddEngineLog( env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
			L"CLegacyHart::Init_OP_REF_INFO_LIST() cannot convert ParamSpecifierType: FDI_PS_PARAM_OFFSET");
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET_SI:
		EddEngineLog( env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
			L"CLegacyHart::Init_OP_REF_INFO_LIST() cannot convert ParamSpecifierType: FDI_PS_PARAM_OFFSET_SI" );
		break;
	default:
		EddEngineLog( env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
			L"CLegacyHart::Init_OP_REF_INFO_LIST() cannot convert ParamSpecifierType: %d", pFdiParamSpec->eType );
	}
}


//Init_DDI_GENERIC_ITEM() - Initializes a DDI_GENERIC_ITEM from an FDI_GENERIC_ITEM
void CLegacyHart::Init_DDI_GENERIC_ITEM( DDI_GENERIC_ITEM *pDdiGenericItem, nsEDDEngine::FDI_GENERIC_ITEM * pFdiGenericItem)
{
	// RETURN_LIST i.e.Errors is output parameter only.
	//pDdiGenericItem->errors.count = pFdiGenericItem->errors.count;
	
	pDdiGenericItem->item = pFdiGenericItem->item;
	pDdiGenericItem->item_type = Set_DDI_ITEM_TYPE(pFdiGenericItem->item_type);

}

// FindBlockHandle() - Gets block handle 
BLOCK_HANDLE CLegacyHart::FindBlockHandle(int /*iBlockInstance*/)
{
	BLOCK_HANDLE block_handle = 0;
	
	return block_handle;
}

//Fill_FDI_GENERIC_ITEM() - Fills in an FDI_GENERIC_ITEM from DDI_GENERIC_ITEM
void CLegacyHart::Fill_FDI_GENERIC_ITEM(nsEDDEngine::FDI_GENERIC_ITEM *pFdiGenericItem, DDI_GENERIC_ITEM pDdiGenericItem)
{
	if(pDdiGenericItem.errors.count != 0)
	{
		for(int i = 0; i < pDdiGenericItem.errors.count; i++)
		{
            nsEDDEngine::AttributeNameSet ans; 
			pFdiGenericItem->errors.count						= pDdiGenericItem.errors.count;
            Fill_AttributeNameSet(ans, pDdiGenericItem.errors.list[i].bad_attr, Set_ITEM_TYPE(pDdiGenericItem.item_type));
            if(ans.empty())
            {
                // Error condition
                pFdiGenericItem->errors.list[i].bad_attr        = nsEDDEngine::_end_;
            }
            else
            {
			    pFdiGenericItem->errors.list[i].bad_attr        = ans.getFirstItem();
            }
			pFdiGenericItem->errors.list[i].rc					= pDdiGenericItem.errors.list[i].rc;
			

			Fill_OP_REF(&pFdiGenericItem->errors.list[i].var_needed, &pDdiGenericItem.errors.list[i].var_needed);
		}
	}

	switch(pDdiGenericItem.item_type)
	{
	case VARIABLE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_VARIABLE;
			Hart_ConvFLAT_VAR *pFlatVar = new Hart_ConvFLAT_VAR( (::FLAT_VAR*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatVar;
			pFdiGenericItem->item = dynamic_cast< nsEDDEngine::FLAT_VAR *>(pFlatVar);
			break;
		}
	case ITEM_ARRAY_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_ITEM_ARRAY;
			Hart_ConvFLAT_ITEM_ARRAY *pFlatItemArray = new Hart_ConvFLAT_ITEM_ARRAY( (FLAT_ITEM_ARRAY*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatItemArray;
			break;
		}
	case COLLECTION_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_COLLECTION;
			Hart_ConvFLAT_COLLECTION *pFlatCollection = new Hart_ConvFLAT_COLLECTION( (FLAT_COLLECTION*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatCollection;
			break;
		}
	case ARRAY_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_ARRAY;
			Hart_ConvFLAT_ARRAY *pArray = new Hart_ConvFLAT_ARRAY( (FLAT_ARRAY*)pDdiGenericItem.item );
			pFdiGenericItem->item = pArray;
			break;
		}
	case COMMAND_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_COMMAND;
			Hart_ConvFLAT_COMMAND *pFlatCommand = new Hart_ConvFLAT_COMMAND( (FLAT_COMMAND*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatCommand;
			break;
		}
	case RESP_CODES_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_RESP_CODES;
			Hart_ConvFLAT_RESP_CODE *pFlatRespCode = new Hart_ConvFLAT_RESP_CODE( (FLAT_RESP_CODE*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatRespCode;
			break;
		}
	case LIST_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_LIST;
			Hart_ConvFLAT_LIST *pFlatList = new Hart_ConvFLAT_LIST( (FLAT_LIST*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatList;
			break;
		}
	case FILE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_FILE;
			Hart_ConvFLAT_FILE *pFlatFile = new Hart_ConvFLAT_FILE( (FLAT_FILE*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatFile;
			break;
		}
	case CHART_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_CHART;
			Hart_ConvFLAT_CHART *pFlatChart = new Hart_ConvFLAT_CHART( (FLAT_CHART*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatChart;
			break;
		}
	case GRAPH_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_GRAPH;
			Hart_ConvFLAT_GRAPH *pFlatGraph = new Hart_ConvFLAT_GRAPH( (FLAT_GRAPH*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatGraph;
			break;
		}
	case AXIS_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_AXIS;
			Hart_ConvFLAT_AXIS *pFlatAxis = new Hart_ConvFLAT_AXIS( (FLAT_AXIS*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatAxis;
			break;
		}
	case WAVEFORM_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_WAVEFORM;
			Hart_ConvFLAT_WAVEFORM *pFlatWaveform = new Hart_ConvFLAT_WAVEFORM((FLAT_WAVEFORM*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatWaveform;
			break;
		}
	case SOURCE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_SOURCE;
			Hart_ConvFLAT_SOURCE *pFlatSource = new Hart_ConvFLAT_SOURCE((FLAT_SOURCE*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatSource;
			break;
		}
	case GRID_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_GRID;
			Hart_ConvFLAT_GRID *pFlatGrid = new Hart_ConvFLAT_GRID((FLAT_GRID*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatGrid;
			break;
		}
	case IMAGE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_IMAGE;
			Hart_ConvFLAT_IMAGE *pFlatImage = new Hart_ConvFLAT_IMAGE((FLAT_IMAGE*)pDdiGenericItem.item );

			// Get the image link information if available
			if(pFlatImage->link.id != 0)
			{
				nsEDDEngine::FDI_ITEM_SPECIFIER      item_spec;
				nsEDDEngine::ITEM_TYPE               item_type;

				item_spec.eType			= nsEDDEngine::FDI_ITEM_ID;
				item_spec.item.id       = pFlatImage->link.id;
				item_spec.subindex      = 0;            /* not used just set for completeness */

				GetItemType(0, &item_spec, &item_type);

				pFlatImage->link.type = item_type;
			}

			pFdiGenericItem->item = pFlatImage;
			break;
		}
	case METHOD_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_METHOD;
			Hart_ConvFLAT_METHOD *pFlatMethod = new Hart_ConvFLAT_METHOD((FLAT_METHOD*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatMethod;
			break;
		}
	case MENU_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_MENU;
			Hart_ConvFLAT_MENU *pFlatMenu = new Hart_ConvFLAT_MENU((FLAT_MENU*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatMenu;
			break;
		}
	case EDIT_DISP_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_EDIT_DISP;
			Hart_ConvFLAT_EDIT_DISPLAY *pFlatEditDisplay = new Hart_ConvFLAT_EDIT_DISPLAY((FLAT_EDIT_DISPLAY*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatEditDisplay;
			break;
		}
	case REFRESH_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_REFRESH;
			Hart_ConvFLAT_REFRESH *pFlatRefresh = new Hart_ConvFLAT_REFRESH((FLAT_REFRESH*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatRefresh;
			break;
		}
	case UNIT_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_UNIT;
			Hart_ConvFLAT_UNIT *pFlatUnit = new Hart_ConvFLAT_UNIT((FLAT_UNIT*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatUnit;
			break;
		}
	case WAO_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_WAO;
			Hart_ConvFLAT_WAO *pFlatWao = new Hart_ConvFLAT_WAO((FLAT_WAO*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatWao;
			break;
		}
	case BLOCK_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_BLOCK;
			Hart_ConvFLAT_BLOCK *pFlatBlock = new Hart_ConvFLAT_BLOCK((FLAT_BLOCK*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatBlock;
			break;
		}
	case RECORD_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_RECORD;
			Hart_ConvFLAT_RECORD *pFlatRecord = new Hart_ConvFLAT_RECORD((FLAT_RECORD*)pDdiGenericItem.item );
			pFdiGenericItem->item = pFlatRecord;
			break;
		}
	default:
		break;
	}	
}

// Fill_COMMAND_LIST() - Fills in FDI Command List structure
void CLegacyHart::Fill_COMMAND_LIST(int iBlockInstance, nsEDDEngine::FDI_COMMAND_LIST* pFdiCommandList, DDI_COMMAND_LIST* pDdiCommandList)
{
	pFdiCommandList->Dispose();		// Make sure we start with a clean one

	if(pDdiCommandList->count != 0)
	{
		pFdiCommandList->count = pDdiCommandList->count;
        pFdiCommandList->list = new nsEDDEngine::COMMAND_ELEM [pDdiCommandList->count];

		for(int i = 0; i < pFdiCommandList->count; i++)
		{
			GetCmdIdFromNumber(iBlockInstance, pDdiCommandList->list[i].number, &pFdiCommandList->list[i].command_id);
			
			pFdiCommandList->list[i].command_number = pDdiCommandList->list[i].number;
			pFdiCommandList->list[i].transaction	= pDdiCommandList->list[i].transaction;
			pFdiCommandList->list[i].weight			= pDdiCommandList->list[i].weight;
			pFdiCommandList->list[i].count			= pDdiCommandList->list[i].count;

			if (pDdiCommandList->list[i].count > 0)
			{
				int iCount = pDdiCommandList->list[i].count;

				pFdiCommandList->list[i].index_list = new nsEDDEngine::COMMAND_INDEX [iCount];

				nsEDDEngine::COMMAND_INDEX* pFdiCommandIndex	= pFdiCommandList->list[i].index_list;
				           ::COMMAND_INDEX* pDdiCommandIndex	= pDdiCommandList->list[i].index_list;

				for(int j = 0; j < iCount; j++)
				{
					pFdiCommandIndex[j].id		= pDdiCommandIndex[j].id;
					pFdiCommandIndex[j].value	= pDdiCommandIndex[j].value;
				}
			}
		}
	}
}

//Set_ITEM_TYPE(): Retunrs FDI item type 
nsEDDEngine::ITEM_TYPE CLegacyHart::Set_ITEM_TYPE(ITEM_TYPE itemType)
{
	nsEDDEngine::ITEM_TYPE fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;

	switch(itemType)
	{
	case RESERVED_ITYPE1:
		fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;
		break;
	case VARIABLE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VARIABLE;
		break;
	case COMMAND_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMMAND;
		break;
	case MENU_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MENU;
		break;
	case EDIT_DISP_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_EDIT_DISP;
		break;
	case METHOD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_METHOD;
		break;
	case REFRESH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_REFRESH;
		break;
	case UNIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_UNIT;
		break;
	case WAO_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAO;
		break;
	case ITEM_ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ITEM_ARRAY;
		break;
	case COLLECTION_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLLECTION;
		break;
	case RESERVED_ITYPE2:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK_B;
		break;
	case BLOCK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK;
		break;
	case RECORD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RECORD;
		break;
	case ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ARRAY;
		break;
	case VAR_LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VAR_LIST;
		break;
	case RESP_CODES_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RESP_CODES;
		break;
	case MEMBER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MEMBER;
		break;
	case FILE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_FILE;
		break;
	case CHART_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CHART;
		break;
	case GRAPH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRAPH;
		break;
	case AXIS_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_AXIS;
		break;
	case WAVEFORM_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAVEFORM;
		break;
	case SOURCE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SOURCE;
		break;
	case LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_LIST;
		break;
	case GRID_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRID;
		break;
	case IMAGE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_IMAGE;
		break;
#if 0 // These item types are not supported by legacy hart
	case 29:
		fdiItemType = nsEDDEngine::ITYPE_BLOB;
		break;
	case 30:
		fdiItemType = nsEDDEngine::ITYPE_PLUGIN;
		break;
	case 31:
		fdiItemType = nsEDDEngine::ITYPE_TEMPLATE;
		break;
	case 32:
		fdiItemType = nsEDDEngine::ITYPE_RESERVED;
		break;
	case 33:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT;
		break;
	case 34:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_FOLDER;
		break;
	case 35:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_DESCRIPTOR;
		break;
	case 36:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_RELATION;
		break;
#endif
	case SEPARATOR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SEPARATOR;
		break;
	case ROWBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ROWBREAK;
		break;
	case COLUMNBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLUMNBREAK;
		break;
	case ENUM_BIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ENUM_BIT;
		break;
	case STRING_LITERAL_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_STRING_LITERAL;
		break;
	case CONST_INTEGER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_INTEGER;
		break;
	case CONST_FLOAT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_FLOAT;
		break;
#if 0 // These item types are not supported by legacy hart
	case 48:
		fdiItemType = nsEDDEngine::ITYPE_SELECTOR;
		break;
	case 49:
		fdiItemType = nsEDDEngine::ITYPE_LOCAL_PARAM;
		break;
	case 50:
		fdiItemType = nsEDDEngine::ITYPE_METH_ARGS;
		break;
#endif
	case ATTR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ATTR;
		break;
	case MAX_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MAX;
		break;

	}
	return fdiItemType;
}


//Set_ITEM_TYPE(): Retunrs FDI item type 
ITEM_TYPE CLegacyHart::Set_DDI_ITEM_TYPE(nsEDDEngine::ITEM_TYPE itemType)
{
	ITEM_TYPE DdiItemType = RESERVED_ITYPE1;

	switch(itemType)
	{
	case nsEDDEngine::ITYPE_NO_VALUE:
		DdiItemType = RESERVED_ITYPE1;
		break;
	case nsEDDEngine::ITYPE_VARIABLE:
		DdiItemType = VARIABLE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COMMAND:
		DdiItemType = COMMAND_ITYPE;
		break;
	case nsEDDEngine::ITYPE_MENU:
		DdiItemType = MENU_ITYPE;
		break;
	case nsEDDEngine::ITYPE_EDIT_DISP:
		DdiItemType = EDIT_DISP_ITYPE;
		break;
	case nsEDDEngine::ITYPE_METHOD:
		DdiItemType = METHOD_ITYPE;
		break;
	case nsEDDEngine::ITYPE_REFRESH:
		DdiItemType = REFRESH_ITYPE;
		break;
	case nsEDDEngine::ITYPE_UNIT:
		DdiItemType = UNIT_ITYPE;
		break;
	case nsEDDEngine::ITYPE_WAO:
		DdiItemType = WAO_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ITEM_ARRAY:
		DdiItemType = ITEM_ARRAY_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COLLECTION:
		DdiItemType = COLLECTION_ITYPE;
		break;
	case nsEDDEngine::ITYPE_BLOCK_B:
		DdiItemType = RESERVED_ITYPE2;
		break;
	case nsEDDEngine::ITYPE_BLOCK:
		DdiItemType = BLOCK_ITYPE;
		break;
	case nsEDDEngine::ITYPE_RECORD:
		DdiItemType = RECORD_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ARRAY:
		DdiItemType = ARRAY_ITYPE;
		break;
	case nsEDDEngine::ITYPE_VAR_LIST:
		DdiItemType = VAR_LIST_ITYPE;
		break;
	case nsEDDEngine::ITYPE_RESP_CODES:
		DdiItemType = RESP_CODES_ITYPE;
		break;
	case nsEDDEngine::ITYPE_MEMBER:
		DdiItemType = MEMBER_ITYPE;
		break;
	case nsEDDEngine::ITYPE_FILE:
		DdiItemType = FILE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CHART:
		DdiItemType = CHART_ITYPE;
		break;
	case nsEDDEngine::ITYPE_GRAPH:
		DdiItemType = GRAPH_ITYPE;
		break;
	case nsEDDEngine::ITYPE_AXIS:
		DdiItemType = AXIS_ITYPE;
		break;
	case nsEDDEngine::ITYPE_WAVEFORM:
		DdiItemType = WAVEFORM_ITYPE;
		break;
	case nsEDDEngine::ITYPE_SOURCE:
		DdiItemType = SOURCE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_LIST:
		DdiItemType = LIST_ITYPE;
		break;
	case nsEDDEngine::ITYPE_GRID:
		DdiItemType = GRID_ITYPE;
		break;
	case nsEDDEngine::ITYPE_IMAGE:
		DdiItemType = IMAGE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_SEPARATOR:
		DdiItemType = SEPARATOR_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ROWBREAK:
		DdiItemType = ROWBREAK_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COLUMNBREAK:
		DdiItemType = COLUMNBREAK_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ENUM_BIT:
		DdiItemType = ENUM_BIT_ITYPE;
		break;
	case nsEDDEngine::ITYPE_STRING_LITERAL:
		DdiItemType = STRING_LITERAL_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CONST_INTEGER:
		DdiItemType = CONST_INTEGER_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CONST_FLOAT:
		DdiItemType = CONST_FLOAT_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ATTR:
		DdiItemType = ATTR_ITYPE;
		break;
	case nsEDDEngine::ITYPE_MAX:
		DdiItemType = MAX_ITYPE;
		break;

	}
	return DdiItemType;
}


// SetClassType() - converts unsigned long value to FDI ClassType enum value
nsEDDEngine::ClassType CLegacyHart::SetClassType(ulong class_attr)
{
	nsEDDEngine::ClassType eTempType = nsEDDEngine::CT_NONE;

	if(class_attr & DIAGNOSTIC_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DIAGNOSTIC; 
	}
	if(class_attr & DYNAMIC_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DYNAMIC; 
	}
	if(class_attr & SERVICE_CLASS)
	{
		eTempType |= nsEDDEngine::CT_SERVICE; 
	}
	if(class_attr & CORRECTION_CLASS)
	{
		eTempType |= nsEDDEngine::CT_CORRECTION; 
	}
	if(class_attr & COMPUTATION_CLASS)
	{
		eTempType |= nsEDDEngine::CT_COMPUTATION; 
	}
	if(class_attr & INPUT_BLOCK_CLASS)			// INPUT_BLOCK_CLASS is not present in FDI. We are assuming  CT_INPUT as INPUT_BLOCK_CLASS
	{
		eTempType |= nsEDDEngine::CT_INPUT; 
	}
	if(class_attr & ANALOG_OUTPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_ANALOG_OUTPUT; 
	}
	if(class_attr & HART_CLASS)
	{
		eTempType |= nsEDDEngine::CT_HART; 
	}
	if(class_attr & LOCAL_DISPLAY_CLASS)
	{
		eTempType |= nsEDDEngine::CT_LOCAL_DISPLAY; 
	}
	if(class_attr & FREQUENCY_CLASS)			    
	{									    
		eTempType |= nsEDDEngine::CT_FREQUENCY;		
	}													
	if(class_attr & DISCRETE_CLASS)			
	{						
		eTempType |= nsEDDEngine::CT_DISCRETE; 
	}													
	if(class_attr & DEVICE_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DEVICE; 
	}
	if(class_attr & LOCAL_CLASS)
	{
		eTempType |= nsEDDEngine::CT_LOCAL_A; 
	}
	if(class_attr & BACKGROUND_CLASS)
	{
		eTempType |= nsEDDEngine::CT_BACKGROUND; 
	}
	if(class_attr & ALARM_CLASS)				
	{											//	Values are different in Legacy HART and FDI
		eTempType |= nsEDDEngine::CT_ALARM;		//  DDS value is 0x020000 			
	}											//	FDI value is 0x008000
	if(class_attr & MODE_CLASS)				
	{											//	Values are different in Legacy HART and FDI
		eTempType |= nsEDDEngine::CT_MODE;		//  DDS value is 0x010000 			
	}											//	FDI value is 0x08000000
	if(class_attr & TUNE_CLASS)
	{											//	Values are different in Legacy HART and FDI
		eTempType |= nsEDDEngine::CT_TUNE;		//  DDS value is 0x100000
	}											//	FDI value is 0x00400000
	if(class_attr & IS_CONFIG_CLASS)
	{											//	Values are different in Legacy HART and FDI
		eTempType |= nsEDDEngine::CT_IS_CONFIG;	//  DDS value is 0x040000	
	}											//	FDI value is 0x4000000000
	if(class_attr & FACTORY_CLASS)
	{											//	Values are different in Legacy HART and FDI
		eTempType |= nsEDDEngine::CT_FACTORY;	//  DDS value is 0x040000
	}											//	FDI value is 0x80000000

	return eTempType;
}

nsEDDEngine::Handling CLegacyHart::SetHandling(unsigned long hadling)
{
	nsEDDEngine::Handling eHandling = (nsEDDEngine::Handling)0;

	if(hadling & READ_HANDLING)
	{
		eHandling |= nsEDDEngine::FDI_READ_HANDLING;
	}
	if(hadling & WRITE_HANDLING)
	{
		eHandling |= nsEDDEngine::FDI_WRITE_HANDLING;
	}

	return eHandling;
}

nsEDDEngine::Boolean CLegacyHart::SetBoolean(unsigned long bVal)
{
	nsEDDEngine::Boolean eValid = nsEDDEngine::False;

	if(bVal == 0)
	{
		eValid = nsEDDEngine::False;
	}
	else 
	{
		eValid = nsEDDEngine::True;
	}

	return eValid;
}

unsigned int CLegacyHart::Set_DDI_COMMAND_TYPE(nsEDDEngine::CommandType commandType)
{
    unsigned int cmd_type = 0;
    switch(commandType)
    {
    case nsEDDEngine::FDI_READ_COMMAND:
        cmd_type = DDI_READ_COMMAND;
        break;
    case nsEDDEngine::FDI_WRITE_COMMAND:
        cmd_type = DDI_WRITE_COMMAND;
        break;
    }
    return cmd_type; 
}

nsEDDEngine::DisplaySize CLegacyHart::SetDisplaySize(unsigned long dispsize)
{
	nsEDDEngine::DisplaySize eSize =  (nsEDDEngine::DisplaySize)0;
	
	switch(dispsize)
	{
	case XX_SMALL_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_XX_SMALL;
		break;
	case X_SMALL_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_X_SMALL;
		break;
	case SMALL_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_SMALL;
		break;
	case MEDIUM_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_MEDIUM;
		break;
	case LARGE_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_LARGE;
		break;
	case X_LARGE_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_X_LARGE;
		break;
	case XX_LARGE_DISPSIZE:
		eSize = nsEDDEngine::DISPSIZE_XX_LARGE;
		break;
	default:
		break;
	}

	return eSize;
}

nsEDDEngine::LineType CLegacyHart::SetLineType(unsigned long linetype)
{
	nsEDDEngine::LineType eLineType = (nsEDDEngine::LineType)0;

	switch(linetype)
	{
	case LOWLOW_LINETYPE:
		eLineType = nsEDDEngine::FDI_LOWLOW_LINETYPE;
		break;
	case LOW_LINETYPE:
		eLineType = nsEDDEngine::FDI_LOW_LINETYPE;
		break;
	case HIGH_LINETYPE:
		eLineType = nsEDDEngine::FDI_HIGH_LINETYPE;
		break;
	case HIGHHIGH_LINETYPE:
		eLineType = nsEDDEngine::FDI_HIGHHIGH_LINETYPE;
		break;
	case TRANSPARENT_LINETYPE:
		eLineType = nsEDDEngine::FDI_TRANSPARENT_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N:
		eLineType = nsEDDEngine::FDI_DATA0_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 1:
		eLineType = nsEDDEngine::FDI_DATA1_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 2:
		eLineType = nsEDDEngine::FDI_DATA2_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 3:
		eLineType = nsEDDEngine::FDI_DATA3_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 4:
		eLineType = nsEDDEngine::FDI_DATA4_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 5:
		eLineType = nsEDDEngine::FDI_DATA5_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 6:
		eLineType = nsEDDEngine::FDI_DATA6_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 7:
		eLineType = nsEDDEngine::FDI_DATA7_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 8:
		eLineType = nsEDDEngine::FDI_DATA8_LINETYPE;
		break;
	case DDS_LINE_TYPE_DATA_N + 9:
		eLineType = nsEDDEngine::FDI_DATA9_LINETYPE;
		break;
	default:
		break;
	}

	return eLineType;
}

int CLegacyHart::DDS_GetParamValue(ENV_INFO *env_info, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value )
{
	int rc = PC_FAIL;

	//Initialize FDI data structures
	nsConsumer::EVAL_VAR_VALUE fdi_param_value;

	nsEDDEngine::FDI_PARAM_SPECIFIER	param_spec;
	if( op_ref->op_ref_type == STANDARD_TYPE )
	{
		param_spec.id		= op_ref->op_info.id;
		param_spec.subindex	= op_ref->op_info.member;

		switch (op_ref->op_info.type)
		{
		case VARIABLE_ITYPE:
			param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
			break;
		case RECORD_ITYPE:
		case ARRAY_ITYPE:
			param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID_SI;
			break;
		default:
			return PC_BAD_PARAM_REQUEST;
		}
	}
	else
	{
		param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX;

		param_spec.RefList.count = op_ref->op_info_list.count;
		param_spec.RefList.list = new nsEDDEngine::OP_REF_INFO[param_spec.RefList.count];

		for ( int i = 0; i < param_spec.RefList.count; i++ )
		{
			param_spec.RefList.list[i].id		= op_ref->op_info_list.list[i].id;
			param_spec.RefList.list[i].member	= op_ref->op_info_list.list[i].member;
			param_spec.RefList.list[i].type		= Set_ITEM_TYPE(op_ref->op_info_list.list[i].type);
		}
	}
	// Call Consumer GetParamValue with FDI data structures

        nsConsumer::PC_ErrorCode ec =  nsConsumer::PC_INVALID_EC;
        if (m_pIParamCache)
        {
            ec = m_pIParamCache->GetParamValue(env_info->block_handle, env_info->value_spec, &param_spec, &fdi_param_value);
        }

	if (ec == nsConsumer::PC_SUCCESS_EC)
	{
		// Fill DDS EVAL_VAR_VALUE with FDI EVAL_VAR_VALUE
		Fill_DDI_EVAL_VAR_VALUE(env_info, param_value, &fdi_param_value );
	}
	
	switch (ec)		// Convert the PC_ErrorCode into our own return codes
	{
		case nsConsumer::PC_SUCCESS_EC:		rc = PC_SUCCESS;		break;
		case nsConsumer::PC_BUSY_EC:		rc = PC_BUSY;			break;
		case nsConsumer::PC_INVALID_EC:		rc = PC_VALUE_NOT_SET;	break;
		case nsConsumer::PC_CIRC_DEPEND_EC:	rc = L7_CIRCULAR_DEPENDENCY_DETECTED;	break;
		default:
		case nsConsumer::PC_OTHER_EC:		rc = PC_INTERNAL_ERROR;	break;
	}

	return rc;
}

void CLegacyHart::DDS_Log(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory)
{
	if (m_pIEDDEngineLogger)
	{
		m_pIEDDEngineLogger->Log(sMessage, eLogSeverity, sCategory);
	}
}

void CLegacyHart::Fill_DDI_EVAL_VAR_VALUE(ENV_INFO *env_info, ::EVAL_VAR_VALUE *pDdiEvalVarValue, nsConsumer::EVAL_VAR_VALUE *pFdiEvalVarValue )
{

	pDdiEvalVarValue->size = pFdiEvalVarValue->size;
	
	switch(pFdiEvalVarValue->type)
	{
	case nsEDDEngine::VT_DDS_TYPE_UNUSED:
		pDdiEvalVarValue->type = DDSUNUSED;
		break;
	case nsEDDEngine::VT_INTEGER:
		pDdiEvalVarValue->type = INTEGER;
		pDdiEvalVarValue->val.ll =  pFdiEvalVarValue->val.i;
		break;
	case nsEDDEngine::VT_UNSIGNED:
		pDdiEvalVarValue->type = UNSIGNED;
		pDdiEvalVarValue->val.ull =  pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_FLOAT:
		pDdiEvalVarValue->type = FLOAT;
		pDdiEvalVarValue->val.f =  pFdiEvalVarValue->val.f;
		break;
	case nsEDDEngine::VT_DOUBLE:
		pDdiEvalVarValue->type = DOUBLE;
		pDdiEvalVarValue->val.d =  pFdiEvalVarValue->val.d;
		break;
	case nsEDDEngine::VT_ENUMERATED:
		pDdiEvalVarValue->type = ENUMERATED;
		pDdiEvalVarValue->val.ull =  pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_BIT_ENUMERATED:
		pDdiEvalVarValue->type = BIT_ENUMERATED;
		pDdiEvalVarValue->val.ull =  pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_INDEX:
		pDdiEvalVarValue->type = INDEX;
		pDdiEvalVarValue->val.ull =  pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_ASCII:
		pDdiEvalVarValue->type = ASCII;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_PACKED_ASCII:
		pDdiEvalVarValue->type = PACKED_ASCII;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_PASSWORD:
		pDdiEvalVarValue->type = PASSWORD;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_BITSTRING:
		{
			pDdiEvalVarValue->type = BITSTRING;
			pDdiEvalVarValue->val.b.len = pFdiEvalVarValue->val.b.length();
			pDdiEvalVarValue->val.b.ptr = (uchar *)malloc(sizeof(uchar)*pDdiEvalVarValue->val.b.len);
			memcpy(pDdiEvalVarValue->val.b.ptr, pFdiEvalVarValue->val.b.ptr(), pDdiEvalVarValue->val.b.len);
		}
		break;
	case nsEDDEngine::VT_EDD_DATE:
		pDdiEvalVarValue->type = HART_DATE_FORMAT;
		pDdiEvalVarValue->val.ull = pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_EUC:
		pDdiEvalVarValue->type = EUC;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_TIME_VALUE:
		pDdiEvalVarValue->type = TIME_VALUE;
		if(pFdiEvalVarValue->size == 4)
		{
			pDdiEvalVarValue->val.ll =  pFdiEvalVarValue->val.u;
		}
		else
		{
			pDdiEvalVarValue->val.ll =  pFdiEvalVarValue->val.i;
		}
		break;
	case nsEDDEngine::VT_TIME:     // These types are not supported by HART profile.
	case nsEDDEngine::VT_DATE_AND_TIME:
	case nsEDDEngine::VT_DURATION:
	case nsEDDEngine::VT_OCTETSTRING:
	case nsEDDEngine::VT_VISIBLESTRING:
	default:
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"CLegacyHart::Fill_DDI_EVAL_VAR_VALUE(...) pFdiEvalVarValue->type is not supported by HART.");
		break;
	}
}

void CLegacyHart::Fill_DDS_STRING(STRING *pDdiString, nsEDDEngine::STRING *pFdiString)
{
	pDdiString->len = (unsigned short)pFdiString->length();
	pDdiString->flags = FREE_STRING;
	pDdiString->str = (wchar_t*)calloc(1,sizeof(wchar_t)*(pFdiString->length()+ 1));
	PS_Wcscpy(pDdiString->str, pFdiString->length()+ 1, pFdiString->c_str());  

}

int CLegacyHart::GetCriticalParams(int iBlockInstance, nsEDDEngine::CRIT_PARAM_TBL **pCriticalParamTbl)
{
	*pCriticalParamTbl = &m_Flat_Device_Dir->blk_tbl.list[iBlockInstance].flat_block_dir.crit_param_tbl;
    return 0;
}

int CLegacyHart::GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, 
	ITEM_ID *pWAO_Item, ITEM_ID *pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList)
{
	// Get the legacy flat_device_dir 
	int r_code = -1;
	FLAT_DEVICE_DIR *pDdiFlatDeviceDir = NULL; 
	DDI_DEVICE_DIR_REQUEST		ddi_dir_req;
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, pValueSpec, L"");

	//Initialize output Vars to empty
	if ( pWAO_Item != nullptr)
	{
		*pWAO_Item = 0;
	}

	if ( pUNIT_Item != nullptr)
	{
		*pUNIT_Item = 0;
	}

	if((pUpdateList != nullptr) && (pUpdateList->list != nullptr))
	{
		pUpdateList->~OP_REF_LIST();
	}

	if((pDominantList != nullptr) && (pDominantList->list != nullptr))
	{
		pDominantList->~OP_REF_LIST();
	}

	// Get the data that was requested
	r_code = m_connection_mgr.get_abt_dd_dev_tbls( m_block_handle, (void**)&pDdiFlatDeviceDir );

	if ( r_code == CM_SUCCESS )
	{
		if (!(pDdiFlatDeviceDir->attr_avail & STRING_TBL_MASK))
		{
			(void) memset((char  *)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
			ddi_dir_req.type = DD_DT_HANDLE;
			ddi_dir_req.mask = STRING_TBL_MASK;
			r_code = m_connection_mgr.get_abt_adtt_offset( m_block_handle, &ddi_dir_req.spec.device_type_handle ); 
			
			if(r_code == DDS_SUCCESS)
			{
				r_code = ddi_device_dir_request( &env_info, &ddi_dir_req, pDdiFlatDeviceDir );
			}
		}
	}


	if( r_code == CM_SUCCESS )
	{
		int iBlkIndex = -1;

		// this is a common starting point for table references.  Use to keep code readable
		FLAT_BLOCK_DIR* pFlatBlockDir = &pDdiFlatDeviceDir->blk_tbl.list[m_block_handle].flat_block_dir;

		// Get the BLK_ITEM_NAME_TBL index by searching pParamSpec->item.id in 
		for (int i = 0; i < pFlatBlockDir->blk_item_tbl.count; i++)
		{
			if (pFlatBlockDir->blk_item_tbl.list[i].blk_item_id == pParamSpec->id)
			{
				iBlkIndex = pFlatBlockDir->blk_item_tbl.list[i].blk_item_name_tbl_offset;
				break;
			}
		}

		if(iBlkIndex != -1)
		{
			// Get the index into BLK_ITEM_NAME_TBL to get RELATION_TBL index
			int iRelIndex = pFlatBlockDir->blk_item_name_tbl.list[iBlkIndex].rel_tbl_offset;

			// Now We have RELATIONS_TBL index i.e. iRelIndex. Use this index straight into the RELATIONS_TBL to get the UPDATE_TBL information
			if(iRelIndex != -1)
			{
				if ( pWAO_Item != nullptr)
				{
					int iWaoIndex = pFlatBlockDir->rel_tbl.list[iRelIndex].wao_item_tbl_offset;

					if(iWaoIndex != -1)
					{
						// get the item id for wao index.
						*pWAO_Item = pDdiFlatDeviceDir->item_tbl.list[iWaoIndex].item_id;
					}
				} // pWAO_Item

				if ( pUNIT_Item != nullptr)
				{
					// Get the unit item id using ddi_get_unit;
					DDI_BLOCK_SPECIFIER block_spec = {0};
					Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

					DDI_PARAM_SPECIFIER param_spec = {0};
					Init_DDI_PARAM_SPECIFIER(&env_info, &param_spec, pParamSpec);

					r_code = ddi_get_unit(&env_info, &block_spec, &param_spec, pUNIT_Item);

					if (r_code == DDI_TAB_NO_UNIT)	// This is an okay return. It just means that we don't have any UNITs
					{								// In this case, *pUNIT_Item will remain as 0.
						r_code = DDS_SUCCESS;
					}
				} // pUNIT_Item

				if ( pUpdateList != nullptr)
				{
					DDI_BLOCK_SPECIFIER block_spec = {0};
					Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);


					DDI_PARAM_SPECIFIER param_spec = {0};
					Init_DDI_PARAM_SPECIFIER(&env_info, &param_spec, pParamSpec);

					OP_DESC_LIST op_desc_list = {0};
					op_desc_list.count = 0;

					r_code = ddi_get_update_items(&env_info, &block_spec, &param_spec, &op_desc_list);

					if(r_code == DDS_SUCCESS)
					{
						pUpdateList->list = (nsEDDEngine::OP_REF *) malloc((size_t) (op_desc_list.count * sizeof(nsEDDEngine::OP_REF)));

						pUpdateList->limit = (unsigned short)op_desc_list.count;
						pUpdateList->count = (unsigned short)op_desc_list.count;

						for (int i = 0; i < op_desc_list.count ; i++)
						{
							nsEDDEngine::OP_REF *pOpRef = &pUpdateList->list[i];

							// Set the dependent item id to UPDATE item
							pOpRef->op_ref_type =  nsEDDEngine::STANDARD_TYPE;
							pOpRef->block_instance = iBlockInstance;
							pOpRef->op_info.id = op_desc_list.list[i].op_ref.op_info.id;
							pOpRef->op_info.member = op_desc_list.list[i].op_ref.op_info.member;
							pOpRef->op_info.type = Set_ITEM_TYPE(op_desc_list.list[i].op_ref.op_info.type);
							pOpRef->op_info_list.count = 0;
							pOpRef->op_info_list.list = nullptr;

						}

					}
					else if (r_code == DDI_TAB_NO_UPDATE) // This is an okay return. It just means that we don't have any UPDATES
					{									  // In this case, *pUpdateList will remain as 0.
						r_code = DDS_SUCCESS;
					}

					free(op_desc_list.list);
					op_desc_list.list = nullptr;
				} // pUpdateList

			}
		}

		if (pDominantList != nullptr)
		{
			OP_REF_LIST *opList;
			DOMINANT_TBL::iterator it; 
			it = pFlatBlockDir->dominant_tbl->find(pParamSpec->id);

			if(it != pFlatBlockDir->dominant_tbl->end())
			{
				opList = it->second;

				pDominantList->count = opList->count;
				pDominantList->limit = opList->limit;
				pDominantList->list = (nsEDDEngine::OP_REF *) malloc((size_t) (opList->count * sizeof(nsEDDEngine::OP_REF)));
				
				
				for(int i = 0; i < pDominantList->count ; i++)
				{
					//nsEDDEngine::OP_REF *pOpRef = &pDominantList->list[i];
					Fill_OP_REF(&pDominantList->list[i], &opList->list[i]);
				}
			}
		} // pDominantList

	}
	
	return r_code;
}

int CLegacyHart::GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, lang_code);
	STRING string = {0};
	
	int rc = app_func_get_dict_string(&env_info, ulIndex, &string);
	
	if (rc == DDL_SUCCESS) 
	{
		if (string.len < iStringLen)
		{
			(void)PS_Wcscpy(pString, iStringLen, string.str);
		} 
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}

	if(string.flags == FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}

int CLegacyHart::GetDevSpecString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, lang_code);

	STRING string = {0};

	DEV_STRING_INFO dev_string_Info = {0};

	dev_string_Info.id = ulIndex;
	
	int rc = app_func_get_dev_spec_string(&env_info, &dev_string_Info, &string);

	if (rc == DDL_SUCCESS) 
	{
		if (string.len < iStringLen)
		{
			(void)PS_Wcscpy(pString, iStringLen, string.str);	
		} 
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}
	
	if(string.flags == FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}


int CLegacyHart::ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex)
{
	int rs;
	
	SUBINDEX sub_index = {0}; 
	nsEDDEngine::FDI_ITEM_SPECIFIER      item_spec;
	nsEDDEngine::ITEM_TYPE               item_type;

	item_spec.eType			= nsEDDEngine::FDI_ITEM_ID;
	item_spec.item.id       = ItemId;
	item_spec.subindex      = 0;            /* not used just set for completeness */

	ENV_INFO env_info(m_block_handle, (IDDSSupport*)this, nullptr, L"");

	rs = GetItemType(iBlockInstance, &item_spec, &item_type);
	
	if (rs == DDS_SUCCESS)
	{
		switch (item_type) 
		{

		case ARRAY_ITYPE:
			if (MemberId == 0)
			{
				rs = DDI_TAB_BAD_SUBINDEX;
			}
			else
			{
				*iSubIndex = MemberId;
			}
			break;

		case RECORD_ITYPE:
			{
				// Convert iBlockInstance
				DDI_BLOCK_SPECIFIER block_spec = {0};
				Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

				if (MemberId != 0)
				{				
					rs = ddi_get_subindex (&env_info, &block_spec, ItemId, MemberId, &sub_index);

					if(rs == DDS_SUCCESS)
					{
						*iSubIndex = sub_index;
					}
				}
			}
			break;

		default:
			*iSubIndex = MemberId;
			rs = DDI_INVALID_ITEM_TYPE;
			break;

		}
	}

	return rs;
}


CEDDEngineImpl* LegacyHartFactory::CreateLegacyHartDDS(const wchar_t *sEDDBinaryFilename, 
	const wchar_t* pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode, UINT32 manufacturer, UINT16	device_type, UINT8 device_revision)
{
	CEDDEngineImpl *pImpl = (CEDDEngineImpl*)new CLegacyHart(sEDDBinaryFilename, pConfigXML, pIEDDEngineLogger,
        pErrorCode, manufacturer, device_type, device_revision);

	if (*pErrorCode != nsEDDEngine::EDDE_SUCCESS)	// If there is an error in the constructor, delete and return null
	{
		delete pImpl;
		pImpl = nullptr;
	}

	return pImpl;
}

