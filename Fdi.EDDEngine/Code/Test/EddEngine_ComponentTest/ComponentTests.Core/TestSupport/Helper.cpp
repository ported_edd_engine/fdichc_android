#include "Helper.h"

using namespace std;
using namespace nsConsumer;
using namespace nsEDDEngine;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            Helper::Helper()
            {
            }

            STRING* Helper::CreateSTRINGFromWstring(const wstring& name)
            {
                STRING* result = new STRING;
				result->assign(name.c_str(),false);

                return result;
            }

            wstring Helper::CreateWstringFromSTRING(const STRING& text)
            {
                size_t len = text.length();
                while (len > 0 && text.c_str()[len-1] == L'\0') --len;

                return wstring(text.c_str(), len);
            }

            void Helper::SetAcknowledgeFunction(BuiltinSupportSimulator* builtinSupport, BS_ErrorCode errorCode)
            {
                builtinSupport->AcknowledgementRequestFunction = function<BS_ErrorCode(void*, wchar_t*, IBuiltinSupport::AckCallback, void*)>(
                    [errorCode](void*, wchar_t*, IBuiltinSupport::AckCallback pCallback, void* state) -> BS_ErrorCode
                    {
                        pCallback(new EVAL_VAR_VALUE(), nsConsumer::BSEC_SUCCESS , state);
                        return errorCode;
                    }
                );
            }

            void Helper::SetSelectionRequestFunction(BuiltinSupportSimulator* builtinSupport, BS_ErrorCode errorCode, unsigned selectionResult)
            {
                builtinSupport->SelectionRequestFunction = function<BS_ErrorCode(void*, wchar_t*, wchar_t*, IBuiltinSupport::AckCallback, void*)>(
                    [errorCode, selectionResult](void*, wchar_t*, wchar_t*, IBuiltinSupport::AckCallback pCallback, void* state) -> BS_ErrorCode
                    {
                        EVAL_VAR_VALUE selectionVarValue;
                        selectionVarValue.type = VT_UNSIGNED;
                        selectionVarValue.size = 4;
                        selectionVarValue.val.u = selectionResult;

                        pCallback(&selectionVarValue, nsConsumer::BSEC_SUCCESS, state);
                        return errorCode;
                    }
                );
            }

            void Helper::SetListInsertFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode errorCode, int* indexCalledWith)
            {
                builtinSupport->ListInsertFunction = function<BS_ErrorCode(int, void*, OP_REF*, int, OP_REF*)> (
                    [errorCode, indexCalledWith](int, void*, OP_REF*, int indexToInsert, OP_REF*) -> BS_ErrorCode
                {
                    *indexCalledWith = indexToInsert;
                    return errorCode;
                }
                );
            }

            void Helper::SetListDeleteElementAtFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode errorCode, int* indexProvided, int* countProvided)
            {
                builtinSupport->ListDeleteElementAtFunction = function<BS_ErrorCode(int, void*, OP_REF*, int, int)> (
                    [errorCode, indexProvided, countProvided](int, void*, OP_REF*, int startIndex, int deleteCount) -> BS_ErrorCode
                {
                    *indexProvided = startIndex;
                    *countProvided = deleteCount;
                    return errorCode;
                }
                );
            }

            void Helper::SetGetDynamicAttributeFunctionSingleCall(BuiltinSupportSimulator* builtinSupport, nsConsumer::PC_ErrorCode errorCode, nsConsumer::EVAL_VAR_VALUE& valueToReturn)
            {
                builtinSupport->GetDynamicAttributeFunction = function<nsConsumer::PC_ErrorCode(int, void*, const OP_REF*, const AttributeName, EVAL_VAR_VALUE*)>(
                    [errorCode, valueToReturn](int, void*, const OP_REF*, const AttributeName, EVAL_VAR_VALUE* returnedValue) -> PC_ErrorCode
                {
                    *returnedValue = valueToReturn;
                    return errorCode;
                }
                );
            }

            void Helper::SetGetParamValueFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::PC_ErrorCode errorCode, ITEM_ID* calledItemId, SUBINDEX* calledSubindex, ParamSpecifierType * calledType, nsConsumer::EVAL_VAR_VALUE& valueToReturn)
            {
                builtinSupport->GetParamValueFunction = function<nsConsumer::PC_ErrorCode(int , void*, nsEDDEngine::FDI_PARAM_SPECIFIER*, nsConsumer::EVAL_VAR_VALUE*)>(
                    [errorCode, calledType, calledItemId, calledSubindex, valueToReturn](int, void*, FDI_PARAM_SPECIFIER* pParamSpec, EVAL_VAR_VALUE *pValue) -> PC_ErrorCode
                {
                    *pValue = valueToReturn;
                    *calledType = pParamSpec->eType;
                    *calledItemId = pParamSpec->id;
                    *calledSubindex = pParamSpec->subindex;
                    return errorCode;
                }
                );
            }

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests
