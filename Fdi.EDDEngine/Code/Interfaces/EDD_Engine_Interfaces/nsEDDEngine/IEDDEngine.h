#pragma once

#include "nsEDDEngine/IItemInfo.h"
#include "nsEDDEngine/ITableInfo.h"
#include "nsEDDEngine/IConvenience.h"
#include "nsEDDEngine/IMethods.h"
#include "nsEDDEngine/FFBlockInfo.h"
#include "nsConsumer/IBuiltinSupport.h"
#include "nsConsumer/IParamCache.h"


namespace nsEDDEngine
{

	class IEDDEngine :	public IItemInfo,
						public ITableInfo,
						public IConvenience,
						public IMethods
	{
	public:
		virtual ~IEDDEngine(void) {};

		virtual void GetEDDFileHeader(EDDFileHeader* pEDDFileHeader) = 0;
		
		// Adds Blocks to your FF EDDEngine instance.
		//   This is called to initialize the EDDEngine with the set of FF blocks that you want to access.
		//
		//   The "arrFieldbusBlockInfo" array contains the set of blocks that exists in this device. "iCount" is the size of this array.
		//   The returned "arrBlockInstance" array contains the iBlockInstance numbers for the corresponding block found in "arrFieldbusBlockInfo" array.
		virtual int AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo *arrFieldbusBlockInfo,
											nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount ) = 0;
	};

}

