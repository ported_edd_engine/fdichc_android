#include "stdafx.h"
#include "Interpreter.h"
#include "MEE.h" //Vibhor 010705: Added


CInterpreter::CInterpreter():m_bInitialized(false)
					, m_intStatus(INTERPRETER_STATUS_INVALID)
					, m_pchSourceCode(NULL)
					, m_pParser(NULL)
					, m_pMEE(NULL)	//Vibhor 010705: Added
{
}

INTERPRETER_STATUS CInterpreter::ExecuteCode
							(
#ifdef STANDALONE_PARSER
								CBuiltInLib  *pBuiltInLibParam
#else
								CBuiltIn *pBuiltInLibParam //WHS May 24 2007 BUILTIN SUBCLASSING
#endif
								, char *pchSource
								, char *pchSourceName
								, char *pchCodeData
								, char *&pchSymbolDump
								, MEE  *pMEE			//Vibhor 010705: Added
							)
{
	if (m_bInitialized == true)
	{
		m_bInitialized = false;

		m_intStatus = INTERPRETER_STATUS_INVALID;

		if (m_pParser)
		{
			delete m_pParser;
		}
	}

	if (pchSource == NULL)
	{
		return INTERPRETER_STATUS_PARSE_ERROR;
	}
	else
	{
		m_pParser = new CParser;
		if (NULL == m_pParser)
		{
			return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}

		m_pMEE = pMEE;	//Vibhor 010705: Added
		m_pParser->SetIsRoutineFlag(m_pMEE->GetIsMethReturnType());

		if (false == m_pParser->Initialize (pBuiltInLibParam, m_pMEE))
		{
			return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}

		m_pchSourceCode = pchSource;
		m_bInitialized = true;

//CJK DP GETS INTO HERE  REDO
		m_intStatus = (INTERPRETER_STATUS)m_pParser->Execute 
											(
												(unsigned char *)pchSource
												, (unsigned char *)pchSourceName
												, pchCodeData
												, pchSymbolDump
											);
		return m_intStatus;
	}
}

//This function gets value from constant or local variable or global variable into argument varValue and gets variable data pointer into argument ppDevObjVar.
//This function returns MEE_* error code
int CInterpreter::GetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
					, hCitemBase**     ppDevObjVar
				)
{
	bool isDone = m_pParser->GetVariableValue(pchVariableName, varValue);
																//actually 'GetLocalVariableValue'
	if ( false == isDone )
	{
		PARAM_REF opRef;

		//looking for variable in global symbol table
		CVariable *pVariable = m_pMEE->m_GlobalSymTable.Find(pchVariableName);
		if (pVariable != NULL)
		{
			//update operative item ID based on variable expression
			opRef = *(pVariable->GetParamRef());
			m_pMEE->ResolveVarExp(pchVariableName, false, NULL, &opRef);
		}

		//get variable value based on either operative item ID or variable name
		hCitemBase *pIB = NULL;
		RETURNCODE iretCode = m_pMEE->ResolveDDExp((const char*)pchVariableName, &opRef, &varValue, &pIB);
		if( ppDevObjVar )
		{
			*ppDevObjVar = pIB;
		}

		return iretCode;
	}
	else
	{
		// use the varValue that determined by the GetVariableValue
		if (ppDevObjVar)// we were given a pointer to fill
		{
			*ppDevObjVar = NULL;// tell it we have no DeviceObjectPointer
		}
	}

	return SUCCESS;
}

bool CInterpreter::SetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
				)
{
	bool isDone = m_pParser->SetVariableValue(pchVariableName,varValue);

	if ( false ==  isDone)
	{
		PARAM_REF opRef;

		//looking for variable in global symbol table
		CVariable *pVariable = m_pMEE->m_GlobalSymTable.Find(pchVariableName);
		if (pVariable != NULL)
		{
			//update operative item ID based on variable expression
			opRef = *(pVariable->GetParamRef());
			m_pMEE->ResolveVarExp(pchVariableName, false, NULL, &opRef);
		}

		//set variable value based on either operative item ID or variable name
		hCitemBase *pIB = NULL;
		RETURNCODE iReturnValue = m_pMEE->ResolveNUpdateDDExp((const char*)pchVariableName,	&opRef, &varValue, RUL_ASSIGN);

		if (iReturnValue == SUCCESS)
		{
			isDone = true;
		}
		else
		{
			isDone = false;
		}
	}
	return isDone;
}

bool CInterpreter::SetVariableValue
				(
					CExpression *pExp
					, INTER_VARIANT &varValue
				)
{
	return m_pParser->SetVariableValue(pExp,varValue);
}

void CInterpreter::FindInLocalSymbolTable (/*in*/char *pchVariableName, /*out*/CVariable **ppVar, /*out*/ bool *pbIsMethodLocal)
{
	return m_pParser->FindInLocalSymbolTable(pchVariableName, ppVar, pbIsMethodLocal);
}

void CInterpreter::GetLocalVariables(wchar_t **pDebugInfoXml)
{
	return m_pParser->GetLocalVariables(pDebugInfoXml);
}

CInterpreter::~CInterpreter()
{
	m_bInitialized = false;

	m_intStatus = INTERPRETER_STATUS_INVALID;

	if (m_pParser)
	{
		delete m_pParser;
	}
}


//this is an OverLoaded function for the Methods calling Methods
//this will be called only by MEE in the " called Method" case
//METHOD_ARG_INFO_VECTOR is a vector of Method argument, which contails all the related info regarding the Method parameter
//vectInterVar-- Value of the each Argument passed
//should be filled by by interpreted vistor if it is passed by reference

INTERPRETER_STATUS CInterpreter::ExecuteCode
							(
#ifdef STANDALONE_PARSER
								CBuiltInLib  *pBuiltInLibParam
#else
								CBuiltIn *pBuiltInLibParam //WHS May 24 2007 BUILTIN SUBCLASSING
#endif
								, char *pchSource
								, char *pchSourceName
								, char *pchCodeData
								, char *&pchSymbolDump
								, MEE  *pMEE
								, METHOD_ARG_INFO_VECTOR* vectMethArg
								, vector<INTER_VARIANT>* vectInterVar
							)
{
	if (m_bInitialized == true)
	{
		m_bInitialized = false;

		m_intStatus = INTERPRETER_STATUS_INVALID;

		if (m_pParser)
		{
			delete m_pParser;
		}
	}

	if (pchSource == NULL)
	{
		return INTERPRETER_STATUS_PARSE_ERROR;
	}
	else
	{
		m_pParser = new CParser;
		if (NULL == m_pParser)
		{
			return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}

		m_pMEE = pMEE;	//Vibhor 010705: Added
		//This is required to differentite whether it is called method or it is a method called from menu
		m_pParser->SetIsRoutineFlag(true);

		if (false == m_pParser->Initialize (pBuiltInLibParam, m_pMEE))
		{
			return INTERPRETER_STATUS_UNKNOWN_ERROR;
		}

		
		m_pchSourceCode = pchSource;
		m_bInitialized = true;


		m_intStatus = (INTERPRETER_STATUS)m_pParser->Execute 
											(
												(unsigned char *)pchSource
												, (unsigned char *)pchSourceName
												, pchCodeData
												, pchSymbolDump
												, vectMethArg
												, vectInterVar
											);
		return m_intStatus;
	}
}



