#pragma once
#include<iostream>
#include<vector>

template<typename T>
class PI_CtypedptrArray
{
private:
    std::vector<T> *m_pvector;
public:
	PI_CtypedptrArray();
	virtual ~PI_CtypedptrArray();
	int GetSize(void);
	int GetCount(void);
	T GetAt( int);
	void RemoveAll();
	void Add(T);
	T* GetData();
};

template<typename T>
PI_CtypedptrArray<T>::PI_CtypedptrArray()
{
    m_pvector = new std::vector<T>();

}
template<typename T>
PI_CtypedptrArray<T>::~PI_CtypedptrArray()
{

    RemoveAll();
    if (NULL != m_pvector)
    {
		delete m_pvector;
		m_pvector = NULL;
    }
}

template<typename T>
int PI_CtypedptrArray<T>::GetSize(void)
{
    if (NULL != m_pvector)
        return (int)m_pvector->size();
    else
        return 0;
}
template<typename T>
int PI_CtypedptrArray<T>::GetCount(void)
{
    if (NULL != m_pvector)
        return (int)m_pvector->size();
    else
        return 0;
}

template<typename T>
T PI_CtypedptrArray<T>::GetAt(int pos)
{
    if (NULL != m_pvector)
        return m_pvector->at(pos);
    else
        return NULL;
}

template<typename T>
void PI_CtypedptrArray<T>::RemoveAll()
{
    if (NULL != m_pvector)
    {
		std::vector<T>().swap(*m_pvector);
    }
}

template<typename T>
void PI_CtypedptrArray<T>::Add(T ptr)
{
    if (NULL != m_pvector)
        m_pvector->push_back(ptr);
}

template<typename T>
T *  PI_CtypedptrArray<T>::GetData()
{
    if (NULL != m_pvector)
        return m_pvector->data();
    else
        return NULL;
}

