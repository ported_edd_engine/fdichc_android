#include "stdafx.h"

#pragma warning (disable : 4786)
#include "ddbDevice.h"
#include "INTER_VARIANT.h"
#include "Interpreter.h"
#include "BuiltIn.h"//WHS:Change done for StingrayProject
#include "BuiltInMapper.h"//WHS:Change done for StingrayProject
#include "FunctionExpression.h"  // MTD 052407: added to pass func exp * to InvokeFunction for global param issues
#include "MEE.h"
#include "RTN_CODE.H"
#include "messageUI.h"
#include <ServerProjects/CommonToolkitStrings.h>
#include "varientTag.h"


#define UINT DWORD


/************************Vibhor 230204: NOTE Start ************************
The following changes are being made to the _?assign family and
?setval family of builtin calls

1.For _?assign family if the first param is not a long (garbage itemId),
 then the call returns a false

2.For both the families if the data type of value being assigned (the second arg for _?assign,
and the lone arg for ?setval family) is being ignored, bcoz there are DDs which use ,
non matching data value in builtins. eg. fsetval(10), and _iassign(16483,2.5)

The implementations of the builtins do check if the destination (and source if any) var
is atleast a Numeric.

*************************Vibhor 230204: Note End   ************************/

// stevev 25jun07 - add a helper # define 
#define RETURN_AT_NOT_NUMERIC( idx) {if (! pVarParameters[idx].isNumeric()) return false;}
		
//FCG -4 spec section 6.1.3 Device Data says "The method shall abort if invalid data is accessed (read or written) by the method."
#define RETURN_AT_DD_ERROR( error ) {if (error == BLTIN_DDS_ERROR) return false;}

#define RETURN_AT_COMM_ERROR( error ) \
{\
	if ((error == BUILTIN_COMM_ABORTED) || (error == BUILTIN_RETRY_FAILED) || (error == BUILTIN_COMM_NO_DEVICE) || (error == BUILTIN_COMM_FAIL_RESPONSE)) \
	{\
		*pBuiltinReturnCode = error;	\
		return true;					\
	}\
}


bool CBuiltIn::InvokeFunction(
				char *pchFunctionName
				, int iNumberOfParameters
				, INTER_VARIANT *pVarParameters
				, INTER_VARIANT *pVarReturnValue
				, int	*pBuiltinReturnCode
				, FunctionExpression* pFuncExp
				)
{
	wchar_t pchUIPrompt[DOUBLE_MAX_DD_STRING] = _T("");

	CBuiltInMapper::GetInstance()->LockBIMapper(m_pMeth->GetStartedProtocol());
	CBuiltInMapper *pMapper = CBuiltInMapper::GetInstance();
	CBuiltInInfo* pInfo = (*pMapper)[pchFunctionName];
	CBuiltInMapper::GetInstance()->ReleaseBIMapper();

	if (!pInfo)//WHS:Change done for StingrayProject
	{
		return false;	//builtin name is not found 
	}

	PreDebugLogParams(pchFunctionName,iNumberOfParameters, pVarParameters, pVarReturnValue, pBuiltinReturnCode);

	//builtin process
	switch(pInfo->GetBuiltInEn())//WHS:Change done for StingrayProject
	{
	  //This builtin is used by HART and PROFIBUS

	case BUILTIN_send_value: {
		unsigned long lItemId=0;
		unsigned long memberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);

		lItemId=(unsigned int)pVarParameters[0];	
		memberId=(unsigned int)pVarParameters[1];

		long lReturnValue = AccessDeviceValue(0, 0, lItemId, memberId, WRITE);
		RETURN_AT_DD_ERROR(lReturnValue);
		RETURN_AT_COMM_ERROR(lReturnValue);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;
	
	case BUILTIN_send_all_values:	//for FF
	{

		long lReturnValue = AccessAllDeviceValues(nsEDDEngine::Send);
		lReturnValue = ConvertToBLTINCode(lReturnValue);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_send_on_exit:	//for Profibus and FF
	{

		int iReturnValue = send_on_exit();

		return true;
	}
	break;
	case BUILTIN_save_on_exit:
	{

		int iReturnValue = save_on_exit();

		return true;
	}
	break;
	case BUILTIN_put_unsigned:
	{
		UINT32 iValue=0;

		if (pVarParameters[0].isNumeric())
		{
			iValue=(UINT32)pVarParameters[0];	
		}
		else
		{
			return false;
		}
		
		long iReturnValue = put_unsigned(iValue);
		RETURN_AT_DD_ERROR(iReturnValue);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_put_string:
	{	
		RETURN_AT_NOT_NUMERIC(1);
		long  pSize = (long)(int)pVarParameters[1];

		int iReturnValue = put_action_string(&(pVarParameters[0]), pSize, nullptr);

		if (iReturnValue == BLTIN_WRONG_DATA_TYPE)
		{
			return false;	//wrong parameter type
		}

		RETURN_AT_DD_ERROR(iReturnValue);

		int retCode = ConvertToBLTINCode(iReturnValue);
		pVarReturnValue->SetValue(&retCode, RUL_INT);

		return true;
	}	
	break;
	case BUILTIN_put_date:
	{
		RETURN_AT_NOT_NUMERIC(1);
		long pSize = (long)(int)pVarParameters[1];

		unsigned char pchInputString[MAX_DD_STRING]={0};
		if ( ! GetUnsignedCharArrayParam (pchInputString,pSize,pVarParameters,0) )
		{
			return false;
		}

		int iReturnValue = put_date(pchInputString, pSize);
		RETURN_AT_DD_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_ret_double_value:
	{

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);

		unsigned long ulItemId=(unsigned int)pVarParameters[0];	
		unsigned long ulMemberId=(unsigned int)pVarParameters[1];

		CValueVarient vValue{};
		long lReturnValue = AccessTypeValue2(0, 0, ulItemId, ulMemberId, &vValue, READ);
		
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			if (vValue.vTagType == ValueTagType::CVT_R4)
			{
				float fVal = static_cast<float>(CV_R4(&vValue));
				pVarReturnValue->SetValue(&(fVal), RUL_FLOAT);
			}
			else if (vValue.vTagType == ValueTagType::CVT_R8)
			{
				pVarReturnValue->SetValue(&(CV_R8(&vValue)), RUL_DOUBLE);
			}
			else
			{
				return false;
			}
		}
		else if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		return true;
	}
	break;
	case BUILTIN_ret_signed_value:
	{

		long retValue = 0;
		int iReturnValue = BLTIN_VAR_NOT_FOUND;
		unsigned long lItemId=0;
		unsigned long memberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);

		lItemId=(unsigned int)pVarParameters[0];	
		memberId=(unsigned int)pVarParameters[1];

		iReturnValue = get_signed_value(lItemId, memberId, &retValue);

		RETURN_AT_DD_ERROR(iReturnValue);
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&retValue, RUL_INT);
		}

		return true;
	}
	break;
	case BUILTIN_ret_unsigned_value:
		{

		unsigned long retValue = 0;
		int iReturnValue = BLTIN_VAR_NOT_FOUND;
		unsigned long lItemId=0;
		unsigned long memberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);

		lItemId=(unsigned int)pVarParameters[0];	
		memberId=(unsigned int)pVarParameters[1];

		iReturnValue = get_unsigned_value(lItemId, memberId, &retValue);

		RETURN_AT_DD_ERROR(iReturnValue);
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&retValue, RUL_UINT);
		}

		return true;
	}
	break;
	  //if (strcmp("delay",pchFunctionName)==0)
	  case BUILTIN_delay:
	  {
		int iTimeinSecs=0;


		if (pVarParameters[0].isNumeric()) 
		{
			iTimeinSecs=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}	
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters,1) )
		{
			//return false; Empty string is okay for prompt
		}
	
		unsigned long pULongItemIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[2],pULongItemIds,iNumberOfItemIds);
		}
		else
		{
			iNumberOfItemIds = 0;
		}		

		long iReturnValue = delayfor2(iTimeinSecs, pchUIPrompt, NULL, NULL, pULongItemIds, NULL, iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	  }
	  break;
	  //else
	  //if (strcmp("DELAY",pchFunctionName)==0)
	  case BUILTIN_DELAY:
	  {
		int TimeInSecs=0;

		if (pVarParameters[0].isNumeric()) 
		{
			TimeInSecs=(int)pVarParameters[0];
		}
		else	
		{	
			return false;
		}		
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 1) )
		{
			//return false; Empty string is okay for prompt
		}
	
		long iReturnValue = delayfor2(TimeInSecs, pchUIPrompt, NULL, NULL, NULL, NULL, 0);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//else
	//if (strcmp("DELAY_TIME",pchFunctionName)==0)
	case BUILTIN_DELAY_TIME:
	{
		int TimeInSecs=0;

		if (pVarParameters[0].isNumeric()) 
		{
			TimeInSecs=(int)pVarParameters[0];	
		}	
		else
		{
			return false;
		}
		
		int iReturnValue = DELAY_TIME(TimeInSecs);

		return true;
	}
	/*Arun 200505 Start of code*/
	break;
	  //This builtin is used by FF
	  case BUILTIN_delayfor:
	  {
		int iTimeinSecs=0;

		if (pVarParameters[0].isNumeric()) 
		{
			iTimeinSecs=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}	
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 1) )
		{
			//return false; Empty string is okay for prompt
		}
	
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(4);
		iNumberOfItemIds = (int)pVarParameters[4];

		long iReturnValue = delayfor2(iTimeinSecs, pchUIPrompt, NULL, NULL, pULongItemIds, pULongMemberIds, iNumberOfItemIds);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	  break;
	  case BUILTIN_delayfor2:
	  {
		long iTimeinSecs=0;

		RETURN_AT_NOT_NUMERIC(0);
		iTimeinSecs=(int)pVarParameters[0];		
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 1) )
		{
			//return false; Empty string is okay for prompt
		}
	
		unsigned long pULongBlkIds[100]={0};
		unsigned long pULongBlkNums[100]={0};
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongBlkIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongBlkNums,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[4],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		if (pVarParameters[5].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[5],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[5].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[5].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(6);
		iNumberOfItemIds = (int)pVarParameters[6];

		long iReturnValue = delayfor2(iTimeinSecs, pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	  break;

	  //This builtin is used by FF
	  case BUILTIN_display_message:
	  {
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
	
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];

		int iReturnValue = put_message(pchUIPrompt, (long*)pULongItemIds, pULongMemberIds, iNumberOfItemIds);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	  break;
	  
	  //This builtin is used by FF
	  case BUILTIN_display_message2:
	  {
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
	
		unsigned long pULongBlkIds[100]={0};
		unsigned long pULongBlkNums[100]={0};
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongBlkIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongBlkNums,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[4],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];

		long iReturnValue = display_message2 (pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	  break;
	  
	  //This builtin is used by FF
	  case BUILTIN_display_dynamics:
	  {
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
	
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];

		long iReturnValue = display_dynamics2(pchUIPrompt, NULL, NULL, pULongItemIds, pULongMemberIds, (long)iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	  break;
	case BUILTIN_display_dynamics2:
	{
		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNums[100] = { 0 };
		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
			
		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[1], pULongBlkIds, iNumberOfItemIds);
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[2], pULongBlkNums, iNumberOfItemIds);
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[3], pULongItemIds, iNumberOfItemIds);
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[4], pULongMemberIds, iNumberOfItemIds);
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];
		
		long iReturnValue = display_dynamics2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, (long)iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	//else
	//if (strcmp("BUILD_MESSAGE",pchFunctionName)==0)
	case BUILTIN_BUILD_MESSAGE:
	{
		tchar pchDestString[DOUBLE_MAX_DD_STRING]={0};
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
		        
        tchar *pDestStr = BUILD_MESSAGE( pchDestString, NUM_ELEM(pchDestString), pchUIPrompt );

        pVarReturnValue->SetValue( pchDestString, RUL_DD_STRING );

        return true;
	}
	/*End of code*/
	break;
	//else
	//if (strcmp("PUT_MESSAGE",pchFunctionName)==0)
	case BUILTIN_PUT_MESSAGE:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
		
		int iReturnValue = PUT_MESSAGE(pchUIPrompt);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//This builtin is used by HART and PROFIBUS
	case BUILTIN_put_message:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
		
		long pLongItemIds[100]={0};
		int iNumberOfItemIds = 0;
		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetLongArray(pVarParameters[1],pLongItemIds,iNumberOfItemIds);
		}
		else
		{
				iNumberOfItemIds = 0;
		}

		int iReturnValue = put_message(pchUIPrompt, pLongItemIds, NULL, iNumberOfItemIds);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//else
	//if (strcmp("ACKNOWLEDGE",pchFunctionName)==0)
	case BUILTIN_ACKNOWLEDGE:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
		
		//Call the lowercase version of acknowledge, so that the code meets HCF_SPEC-501, sec 6.1.4.  This allows use
		//of DD Variables.
		int iReturnValue = acknowledge(pchUIPrompt, NULL, NULL, 0);
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBICode(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);		
		}

		return true;
	}
	break;
	//else
	//if (strcmp("acknowledge",pchFunctionName)==0)
	case BUILTIN_acknowledge:
	{
		//BUILTIN_acknowledge is used by HART and PROFIBUS only
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		long pLongItemIds[100]={0};
		int iNumberOfItemIds = 0;
		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetLongArray(pVarParameters[1],pLongItemIds,iNumberOfItemIds);
		}
		else
		{
			iNumberOfItemIds = 0;
		}

		int iReturnValue = acknowledge(pchUIPrompt, pLongItemIds, NULL, iNumberOfItemIds);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBICode(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);		
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_get_dev_var_value",pchFunctionName)==0)
	case BUILTIN__get_dev_var_value:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		ITEM_ID pULongItemIds[100] = { 0 };
		int iNumberOfItemIds = 0;
		ITEM_ID ulItemId = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if (!GetULongArray(pVarParameters[1], pULongItemIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else
		{
			iNumberOfItemIds = 0;
		}

		if (pVarParameters[2].isNumeric())
		{
			ulItemId = (unsigned int)pVarParameters[2];
		}
		else
		{
			return false;
		}


		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNums[100] = { 0 };
		unsigned long ulBlkId = 0;
		unsigned long ulBlkNum = 0;

		long iReturnValue = edit_device_value2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, NULL,
												(long)iNumberOfItemIds, ulBlkId, ulBlkNum, ulItemId, 0);
		
		ProtocolType iProtocol = m_pMeth->GetStartedProtocol();

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBICode(iReturnValue);
			
			if(iProtocol == nsEDDEngine::HART)
			{
				if(iReturnValue != BI_SUCCESS)
				{
					iReturnValue = BI_ERROR;	
				}
			}
			
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}
		return true;
	}
	break;
	//This builtin is used by FF
	case BUILTIN_edit_device_value:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];

		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);
		ulItemId = (unsigned int)pVarParameters[4];
		ulMemberId = (unsigned int)pVarParameters[5];

		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNums[100] = { 0 };
		unsigned long ulBlkId=0;
		unsigned long ulBlkNum=0;

		long iReturnValue = edit_device_value2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds,
												(long)iNumberOfItemIds, ulBlkId, ulBlkNum, ulItemId, ulMemberId);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			RETURN_AT_DD_ERROR(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);		
		}

		return true;
	}
	break;
	//This builtin is used by FF
	case BUILTIN_edit_device_value2:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNums[100] = { 0 };
		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;
		unsigned long ulBlkId=0;
		unsigned long ulBlkNum=0;
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongBlkIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongBlkNums,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[4],pULongMemberIds,iNumberOfItemIds);
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];

		RETURN_AT_NOT_NUMERIC(6);
		RETURN_AT_NOT_NUMERIC(7);
		RETURN_AT_NOT_NUMERIC(8);
		RETURN_AT_NOT_NUMERIC(9);
		ulBlkId=(unsigned int)pVarParameters[6];
		ulBlkNum=(unsigned int)pVarParameters[7];
		ulItemId=(unsigned int)pVarParameters[8];
		ulMemberId=(unsigned int)pVarParameters[9];

		long iReturnValue = edit_device_value2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds,
												(long)iNumberOfItemIds, ulBlkId, ulBlkNum, ulItemId, ulMemberId);
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);		
		}

		return true;
	}
	break;

	//This builtin is used by HART and PROFIBUS
	case BUILTIN__get_local_var_value:
	{
		char xlated_var_name[MAX_DD_STRING]={0};

		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		unsigned long pULongItemIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds);
		}
		else
		{
			iNumberOfItemIds = 0;
		}
		//                     use an alias, should be type char so it'll go in
		if ( ! GetCharStringParam(xlated_var_name, MAX_DD_STRING, pVarParameters, 2) )
		{
			return false;
		}

		int iReturnValue = edit_local_value2(pchUIPrompt, NULL, NULL, pULongItemIds, NULL, iNumberOfItemIds, xlated_var_name);
		RETURN_AT_DD_ERROR(iReturnValue);

		ProtocolType iProtocol = m_pMeth->GetStartedProtocol();
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBICode(iReturnValue);
			if(iProtocol == nsEDDEngine::HART)
			{
				if(iReturnValue != BI_SUCCESS)
				{
					iReturnValue = BI_ERROR;	
				}
			}
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}
		return true;
	}
	break;
	//This builtin is used by FIELDBUS
	case BUILTIN_edit_local_value:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];

		char var_name[MAX_DD_STRING]={0};

		//                     use an alias, should be type char so it'll go in
		if ( ! GetCharStringParam(var_name, MAX_DD_STRING, pVarParameters, 4) )
		{
			return false;
		}

		int iReturnValue = edit_local_value2(pchUIPrompt, NULL, NULL, pULongItemIds, pULongMemberIds, iNumberOfItemIds, var_name);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//This builtin is used by FIELDBUS
	case BUILTIN_edit_local_value2:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNumIds[100] = { 0 };
		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongBlkIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongBlkNumIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[4],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];

		tchar wide_var_name[MAX_DD_STRING];
		char var_name[MAX_DD_STRING]={0};

		//                     use an alias, should be type char so it'll go in
		if ( ! GetStringParam(wide_var_name, MAX_DD_STRING, pVarParameters,6) )
		{
			return false;
		}
		wcstombs( var_name, wide_var_name, MAX_DD_STRING );

		int iReturnValue = edit_local_value2(pchUIPrompt, pULongBlkIds, pULongBlkNumIds, pULongItemIds, pULongMemberIds, iNumberOfItemIds, var_name);
		iReturnValue = ConvertToBLTINCode(iReturnValue);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	//else
	//if( strcmp("_display_xmtr_status",pchFunctionName)==0)
	case BUILTIN__display_xmtr_status:
	{
		long lItemId=0;
		int iStatusval=0;

		if (pVarParameters[0].isNumeric()) // was '=' stevev 30may07
		{
			lItemId=(int)pVarParameters[0];
		}
		else// added else - stevev 30may07
		{
			return false;
		}

		if (pVarParameters[1].isNumeric()) // was '=' stevev 30may07
		{
			iStatusval=(int)pVarParameters[1];
		}
		else// added else - stevev 30may07
		{
			return false;
		}

		int iReturnValue = _display_xmtr_status(lItemId, iStatusval);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//else
	//if( strcmp("display_response_status",pchFunctionName)==0)
	case BUILTIN_display_response_status:
	{
		long lCommandVal=0;
		int iStatusval=0;

		if (pVarParameters[0].isNumeric())
		{
			lCommandVal=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(1);
		iStatusval=(int)pVarParameters[1];

		int iReturnValue = display_response_status(lCommandVal, iStatusval);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;

	case BUILTIN_DISPLAY:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		int iReturnValue = display(pchUIPrompt, NULL, 0);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//else
	//if (strcmp("display",pchFunctionName)==0)
	case BUILTIN_display:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}

		long pLongItemIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetLongArray(pVarParameters[1],pLongItemIds,iNumberOfItemIds);
		}
		else
		{
			iNumberOfItemIds = 0;
		}


		int iReturnValue = display(pchUIPrompt, pLongItemIds, iNumberOfItemIds);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		//Spec says return value should be void.
		return true;
	}
	break;
	//else
	//if (strcmp("SELECT_FROM_LIST",pchFunctionName)==0)
	case BUILTIN_SELECT_FROM_LIST:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//Empty string is okay for prompt
		}
		
		tchar pchString_02[MAX_DD_STRING]={0};
		if ( ! GetStringParam(pchString_02, MAX_DD_STRING, pVarParameters, 1) )
		{
			return false;
		}

		int iReturnValue = select_from_list(pchUIPrompt, nullptr, 0, pchString_02);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("select_from_list",pchFunctionName)==0)
	case BUILTIN_select_from_list:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//Empty string is okay for prompt
		}


		long pLongItemIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			GetLongArray(pVarParameters[1],pLongItemIds,iNumberOfItemIds);
		}
		else
		{
			iNumberOfItemIds = 0;
		}
		
		tchar pchString_02[MAX_DD_STRING]={0};
		if ( ! GetStringParam(pchString_02, MAX_DD_STRING, pVarParameters, 2) )
		{
			return false;
		}

		int iReturnValue = select_from_list(pchUIPrompt, pLongItemIds,iNumberOfItemIds,pchString_02);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_vassign",pchFunctionName)==0)
	case BUILTIN__vassign:
	{
		long lItemIdDest=0;
		long lItemIdSrc=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		lItemIdDest=(int)pVarParameters[0];	

		RETURN_AT_NOT_NUMERIC(1);
		lItemIdSrc=(int)pVarParameters[1];
	
		int iReturnValue = _vassign(lItemIdDest,lItemIdSrc);
		
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_dassign",pchFunctionName)==0)
	case BUILTIN__dassign:
	{
		long lItemId=0;
		double dVal=0.0;
	
		RETURN_AT_NOT_NUMERIC(0);		
		lItemId=(int)pVarParameters[0];	

		RETURN_AT_NOT_NUMERIC(1);		
		dVal=(double)pVarParameters[1];
		
		int iReturnValue = _dassign(lItemId,dVal);
		
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_fassign",pchFunctionName)==0)
	case BUILTIN__fassign:	
	{
		long lItemId=0;
		float fVal=0.0;
	
		RETURN_AT_NOT_NUMERIC(0);
		lItemId=(int)pVarParameters[0];	

		RETURN_AT_NOT_NUMERIC(1);// stevev added 18feb08
		fVal=(float)pVarParameters[1];
	
		int iReturnValue = _fassign(lItemId,fVal);
		
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_lassign",pchFunctionName)==0)
	case BUILTIN__lassign:
	{
		long lItemId=0;
		INT64 lVal=0;
	
		if (pVarParameters[0].isNumeric())
		{
			lItemId=(int)pVarParameters[0];	
		}
/*Vibhor 230204: Start of Code*/
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(1);// stevev added 18feb08
		lVal=(INT64)pVarParameters[1];
		
/*Vibhor 230204: End of Code*/	
	
		int iReturnValue = _lassign(lItemId,lVal);

		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_iassign",pchFunctionName)==0)
	case BUILTIN__iassign:
	{
		long lItemId=0;
		INT64  iVal=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		lItemId=(int)pVarParameters[0];	
		
		RETURN_AT_NOT_NUMERIC(1);
		iVal=(INT64)pVarParameters[1];
	
		int iReturnValue = _iassign(lItemId,iVal);
		
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("_fvar_value",pchFunctionName)==0)
	case BUILTIN__fvar_value:
	{
		long lItemId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		lItemId=(int)pVarParameters[0];	

		float fReturnValue = 0.0;
		
		int iRetVal = _fvar_value(fReturnValue, lItemId);

		RETURN_AT_DD_ERROR(iRetVal);
		if (iRetVal == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&fReturnValue, RUL_FLOAT);
		}
		return true;
	}
	break;
	//else
	//if (strcmp("_ivar_value",pchFunctionName)==0)
	case BUILTIN__ivar_value:
	{
		long lItemId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		lItemId=(int)pVarParameters[0];	

		INT64 iReturnValue = 0;
		
		int iRetVal = _ivar_value(iReturnValue, lItemId);

		RETURN_AT_DD_ERROR(iRetVal);
		if (iRetVal == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_LONGLONG);
		}
		return true;
	}
	break;
	//else
	//if (strcmp("_lvar_value",pchFunctionName)==0)
	case BUILTIN__lvar_value:
	{
		long lItemId=0;
	
		if (pVarParameters[0].isNumeric())
		{
			lItemId=(int)pVarParameters[0];	
		}
		else// added else - stevev 30may07
		{
			return false;
		}

	
		INT64 iReturnValue = 0;
		
		int iRetVal = _ivar_value(iReturnValue, lItemId);

		RETURN_AT_DD_ERROR(iRetVal);
		if (iRetVal == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_LONGLONG);
		}

		return true;
	}
	break;
	//else
	//if (strcmp("sassign",pchFunctionName)==0)
	case BUILTIN_sassign:	//not FDI
	{
		int iReturnValue = BI_ERROR;//default to failure
		long lItemId = 0;
		char* szValue = NULL;

		if (pVarParameters[0].isNumeric())//do we have an ItemID?
		{
			lItemId=(int)pVarParameters[0];	
		}
		else// added else - stevev 30may07
		{
			return false;
		}

		//Get the String value 
		pVarParameters[1].GetStringValue( &szValue, RUL_DD_STRING );
		
		if( szValue )//do we have a valid string?
		{
			char szLang[5] = {0};
			bool bLangPresent=false;
			//		Remove the Language code , if it was prepended <a tokenizer bug>
			GetLanguageCode( szValue, szLang, &bLangPresent );//remove language code if present
		
			iReturnValue = sassign( lItemId, szValue );//do operation and save return value.

			delete[] szValue;//clean up memory
			szValue = NULL;
		}

		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);//set return value
		}

		return true;
	}
	break;
	//Added By Anil June 20 2005 --Ends here

	case BUILTIN_save_values:	//HART and Profibus
	{
		int iReturnValue = AccessAllDeviceValues(nsEDDEngine::Save);
		iReturnValue = ConvertToBICode(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	
	//Added By Anil July 01 2005 --starts here
	case BUILTIN_discard_on_exit:
	{
		int iReturnValue = discard_on_exit();
		return true;
	}
	break;
	//Added By Anil July 01 2005 --Ends here
	//else
	//if (strcmp("get_more_status",pchFunctionName)==0)
	case BUILTIN_get_more_status:
	{// stevev 25dec07 - this returns the raw bytes - NOT strings!
		int  iReturnValue, moreInfoSize=MAX_XMTR_STATUS_LEN;
		BYTE_STRING( status, STATUS_SIZE + 1 );
		BYTE_STRING( info,   MAX_XMTR_STATUS_LEN + 1);

		iReturnValue = get_more_status(status.bs,info.bs,moreInfoSize) ;		
		info.bsLen   = moreInfoSize + 1;

		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 0, status) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( info );
			return false;
		}	
	
		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 1, info) )//more_data_info
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( info );
			return false;
		}	

		DEL_BYTE_STR( status );
		DEL_BYTE_STR( info );

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}
	break;
	//else
	//if (strcmp("_get_status_code_string",pchFunctionName)==0)
	case BUILTIN__get_status_code_string:
	{
		long lItemId = 0;
		int iStatusCode = 0;
		int iStatusStringlength = 0;
	
		if (pVarParameters[0].isNumeric())
		{
			lItemId=(int)pVarParameters[0];	
		}

		if (pVarParameters[1].isNumeric())
		{
			iStatusCode=(int)pVarParameters[1];	
		}
			
		if (pVarParameters[3].isNumeric())
		{
			iStatusStringlength=(int)pVarParameters[3];	
		}		
		
		int iReturnValue = _get_status_code_string(lItemId, iStatusCode, pchUIPrompt, iStatusStringlength);

		if(iReturnValue != BI_SUCCESS)
		{
			iReturnValue = BI_ERROR;
		}

		if ( ! SetStringParam(pFuncExp, pVarParameters, 2, pchUIPrompt) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}	
	break;
	/*Arun 200505 Start of code*/
	//else
	//if (strcmp("get_enum_string",pchFunctionName)==0)
	case BUILTIN_get_enum_string:
	{	
		long lItemId=0;
		int iVariableValue=0;
		int iMaxStringLength=MAX_DD_STRING;
		tchar pchString[MAX_DD_STRING+1] = {0};
		int iReturnValue=0;

		if (iNumberOfParameters != 3)
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);

		lItemId = (int)pVarParameters[0];
		iVariableValue = (int)pVarParameters[1];

		iReturnValue = get_enum_string( lItemId,iVariableValue,pchString, iMaxStringLength);
		RETURN_AT_DD_ERROR(iReturnValue);

		if(iReturnValue != BI_SUCCESS)
		{
			iReturnValue = BI_ERROR;
		}

		if ( ! SetStringParam(pFuncExp, pVarParameters, 2, pchString) )
		{
			return false;
		}
		
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	/*End of code*/
	//This builtin is used by HART and PROFIBUS
	case BUILTIN__get_dictionary_string:
	{	
		long lDictId=0;
		int iReturnValue = BI_SUCCESS;

		if (iNumberOfParameters != 3)
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(2);
	//	Do not truncate the string.

		if (pVarParameters[0].isNumeric())
		{
			lDictId = (int)pVarParameters[0];

			iReturnValue = _get_dictionary_string(lDictId, pchUIPrompt, NUM_ELEM(pchUIPrompt));
		}
		else
		{
			//dictionary string is provided by parser
			if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
			{
				return false;
			}
		}

		if (!SetStringParam(pFuncExp, pVarParameters, 1, pchUIPrompt, true))
		{
			return false;
		}
		
		iReturnValue = ConvertToBICode(iReturnValue);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//This builtin is used by HART and PROFIBUS
	case BUILTIN__dictionary_string:
	{	
		long lItemId=0;	

		if (iNumberOfParameters != 1)
		{
			return false;
		}
		
		if (pVarParameters[0].isNumeric())
		{
			lItemId = (int)pVarParameters[0];

			(void)_get_dictionary_string(lItemId, pchUIPrompt, NUM_ELEM(pchUIPrompt));
		}
		else
		{
			//dictionary string is provided by parser
			if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
			{
				return false;
			}
		}

		if(pchUIPrompt[0] != L'\0')
		{
			pVarReturnValue->SetValue(pchUIPrompt, RUL_DD_STRING);//WS:EPM 24may07
		}

		return true;
	}
	break;
	//else
	//if (strcmp("resolve_array_ref",pchFunctionName)==0)
	case BUILTIN_resolve_array_ref:
	{
		unsigned long ulItemId = 0;
		unsigned long ulIndex = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		ulItemId = (unsigned int)pVarParameters[0];	
		ulIndex = (unsigned int)pVarParameters[1];	

		unsigned int iReturnValue = resolve_array_ref2(0, 0, ulItemId, ulIndex);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}	
	break;
	case BUILTIN_resolve_array_ref2:	//for FF
	{
		unsigned long ulBlockId = 0;
		unsigned long ulblockInstance = 0;
		unsigned long ulItemId = 0;
		unsigned long ulIndex = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		ulBlockId = (unsigned int)pVarParameters[0];	
		ulblockInstance = (unsigned int)pVarParameters[1];	
		ulItemId = (unsigned int)pVarParameters[2];	
		ulIndex = (unsigned int)pVarParameters[3];	

		unsigned int iReturnValue = resolve_array_ref2(ulBlockId, ulblockInstance, ulItemId, ulIndex);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}	
	break;
	//else
	//if (strcmp("resolve_record_ref",pchFunctionName)==0)
	case BUILTIN_resolve_record_ref:
	{
		unsigned long ulItemId = 0;
		unsigned long ulIndex = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		ulItemId = (unsigned int)pVarParameters[0];	
		ulIndex = (unsigned int)pVarParameters[1];	

		unsigned long iReturnValue = resolve_record_ref2(0, 0, ulItemId, ulIndex);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}
	break;
	case BUILTIN_resolve_record_ref2:
	{
		unsigned long ulBlockId;
		unsigned long ulBlockInstance;
		unsigned long ulItemId=0;
		unsigned long ulIndex=0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		ulBlockId = (unsigned int)pVarParameters[0];	
		ulBlockInstance = (unsigned int)pVarParameters[1];	
		ulItemId = (unsigned int)pVarParameters[2];	
		ulIndex = (unsigned int)pVarParameters[3];	

		unsigned long iReturnValue = resolve_record_ref2(ulBlockId, ulBlockInstance, ulItemId, ulIndex);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}
	break;
	//else
	//if (strcmp("resolve_param_ref",pchFunctionName)==0)
	case BUILTIN_resolve_param_ref:
	{
		long lItemId=0;
		
		if (pVarParameters[0].isNumeric())
		{
			lItemId=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}	

		unsigned int iReturnValue = (unsigned int)resolve_param_ref(lItemId);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}
	break;


	case BUILTIN_resolve_local_ref:
	{	
		unsigned long ulMemberId = 0;
		

		RETURN_AT_NOT_NUMERIC(0);
		
		ulMemberId = (unsigned int)pVarParameters[0];	
			
		unsigned long ulReturnValue = resolve_block_ref(0, 0, ulMemberId, RESOLVE_LOCAL_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_HT_resolve_local_ref:	//used for HART tokenizer only
	{	
		RETURN_AT_NOT_NUMERIC(0);
		unsigned long ulReturnValue = (unsigned int)pVarParameters[0];	

		if (ulReturnValue > 0)
		{
			pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);
			return true;
		}
		else
		{
			ulResolveStatus = BLTIN_BAD_ID;
			return false;	//it may happen in cross block
		}
	}
	break;

	//else
	//if (strcmp("rspcode_string",pchFunctionName)==0)
	case BUILTIN_rspcode_string:	//for HART
	{
		RETURN_AT_NOT_NUMERIC(0);
		int iCmdNumber = (int)pVarParameters[0];	

		RETURN_AT_NOT_NUMERIC(1);
		int iRespCode = (int)pVarParameters[1];	
	
		RETURN_AT_NOT_NUMERIC(3);
		int iRespCodeLength = (int)pVarParameters[3];	
		
		int iReturnValue = rspcode_string(iCmdNumber, iRespCode, pchUIPrompt, iRespCodeLength, nsEDDEngine::HART);
		
		if ( ! SetStringParam(pFuncExp, pVarParameters, 2, pchUIPrompt) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
	case BUILTIN__get_rspcode_string_by_id:	//for Profibus
	{
		RETURN_AT_NOT_NUMERIC(0);
		ITEM_ID commandId = (unsigned int)pVarParameters[0];	

		RETURN_AT_NOT_NUMERIC(1);
		int iRespCode = (int)pVarParameters[1];	
	
		
		int iReturnValue = rspcode_string(commandId, iRespCode, pchUIPrompt, NUM_ELEM(pchUIPrompt), nsEDDEngine::PROFIBUS);
		
		if ( ! SetStringParam(pFuncExp, pVarParameters, 2, pchUIPrompt) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
	//else
	//if (strcmp("_set_comm_status",pchFunctionName)==0)
	case BUILTIN__set_comm_status:
	{
		int icomm_status=0;
		int iAbortIgnoreRetry=0;
		
		// stevev 11feb08  if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			icomm_status=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		// stevev 11feb08  if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_comm_status(icomm_status,iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_device_status",pchFunctionName)==0)
	case BUILTIN__set_device_status:
	{
		int idev_status=0;
		int iAbortIgnoreRetry=0;
		
		//stevev 11feb08 if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			idev_status=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		//stevev 11feb08 if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_device_status(idev_status,iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_resp_code",pchFunctionName)==0)
	case BUILTIN__set_resp_code:
	{
		int iResp_code;
		int iAbortIgnoreRetry;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iResp_code=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		// stevev 11feb08   if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_resp_code(iResp_code,iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_all_resp_code",pchFunctionName)==0)
	case BUILTIN__set_all_resp_code:
	{
		int iAbortIgnoreRetry=0;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_all_resp_code(iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_no_device",pchFunctionName)==0)
	case BUILTIN__set_no_device:
	{
		int iAbortIgnoreRetry=0;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_no_device(iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_IGNORE_NO_DEVICE:
	{
		int iReturnValue = _set_no_device(__IGNORE__);

		return true;
	}
	break;
	//else
	//if (strcmp("SET_NUMBER_OF_RETRIES",pchFunctionName)==0)
	case BUILTIN_SET_NUMBER_OF_RETRIES:
	{
		int iNo_of_retries=0;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iNo_of_retries=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = SET_NUMBER_OF_RETRIES(iNo_of_retries);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_comm_status",pchFunctionName)==0)
	case BUILTIN__set_xmtr_comm_status:
	{
		int iCommStatus=0;
		int iAbortIgnoreRetry=0;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iCommStatus=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		// stevev 11feb08   if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_xmtr_comm_status(iCommStatus, iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_device_status",pchFunctionName)==0)
	case BUILTIN__set_xmtr_device_status:
	{
		int iDeviceStatus=0;
		int iAbortIgnoreRetry=0;
		
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iDeviceStatus=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
		
		// stevev 11feb08   if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_xmtr_device_status(iDeviceStatus, iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_resp_code",pchFunctionName)==0)
	case BUILTIN__set_xmtr_resp_code:
	{
		int iRespCode=0;
		int iAbortIgnoreRetry=0;
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iRespCode=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		// stevev 11feb08   if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[1];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_xmtr_resp_code(iRespCode, iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_all_resp_code",pchFunctionName)==0)
	case BUILTIN__set_xmtr_all_resp_code:
	{
		int iAbortIgnoreRetry=0;
		
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[0];	
		}		
		else
		{
			return false;
		}	
	
		int iReturnValue = _set_xmtr_all_resp_code( iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_no_device",pchFunctionName)==0)
	case BUILTIN__set_xmtr_no_device:
	{
		int iAbortIgnoreRetry=0;
		
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
			
		int iReturnValue = _set_xmtr_no_device( iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_all_data",pchFunctionName)==0)
	case BUILTIN__set_xmtr_all_data:
	{
		int iAbortIgnoreRetry=0;
		
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[0];	
		}		
		else
		{
			return false;
		}		
		
		int iReturnValue = _set_xmtr_all_data( iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_set_xmtr_data",pchFunctionName)==0)
	case BUILTIN__set_xmtr_data:
	{
		int iByteCode=0;
		int iBitMask=0;
		int iAbortIgnoreRetry=0;
		
		
		// stevev 11feb08   if (pVarParameters[0].GetVarType() == RUL_INT)
		if (pVarParameters[0].isNumeric())
		{
			iByteCode=(int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
		
		// stevev 11feb08   if (pVarParameters[1].GetVarType() == RUL_INT)
		if (pVarParameters[1].isNumeric())
		{
			iBitMask=(int)pVarParameters[1];	
		}
		else
		{
			return false;
		}

		// stevev 11feb08   if (pVarParameters[2].GetVarType() == RUL_INT)
		if (pVarParameters[2].isNumeric())
		{
			iAbortIgnoreRetry=(int)pVarParameters[2];	
		}
		else
		{
			return false;
		}
	
		int iReturnValue = _set_xmtr_data( iByteCode,iBitMask,iAbortIgnoreRetry);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("abort",pchFunctionName)==0)
	case BUILTIN_abort:
	{
		int iReturnValue = abort(M_METHOD_ABORTED);
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_ABORTED;
		}
		return true;
	}
	break;
	//else
	//if (strcmp("process_abort",pchFunctionName)==0)
	case BUILTIN_process_abort:
	{
		int iReturnValue = process_abort();
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_ABORTED;
		}
		return true;
	}
	break;
	case BUILTIN_method_abort:
	{
		wchar_t pchString[MAX_DD_STRING]={0};
		int  pSize = MAX_DD_STRING;

		//Always have a prompt. Otherwise abort() won't wait for user acknowledgement
		if ( ! GetStringParam(pchString, pSize, pVarParameters, 0) )
		{
			wcscpy(pchString, M_METHOD_ABORTED);
		}

		int iReturnValue = abort(pchString);
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_ABORTED;
		}

		return true;
	}
	//else
	//if (strcmp("_add_abort_method",pchFunctionName)==0)
	case BUILTIN__add_abort_method:	//version A for HART and Profibus
	{
		long lMethodId=0;

		if (pVarParameters[0].isNumeric()) 
		{
			lMethodId=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}
		
		int iReturnValue = _add_abort_method(lMethodId);

		if (iReturnValue != BI_SUCCESS)
		{
			iReturnValue = BI_ERROR;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
	case BUILTIN_add_abort_method:	//version B for Fieldbus
	{
		unsigned long ulMethodId=0;

		if (pVarParameters[0].isNumeric()) 
		{
			ulMethodId=(unsigned int)pVarParameters[0];
		}
		else
		{
			return false;
		}
		
		int iReturnValue = _add_abort_method(ulMethodId);

		iReturnValue = ConvertToBLTINCode(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("_remove_abort_method",pchFunctionName)==0)
	case BUILTIN__remove_abort_method:	//version A for HART and Profibus
	{
		long lMethodId=0;

		if (pVarParameters[0].isNumeric()) 
		{
			lMethodId=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}
		
		int iReturnValue = _remove_abort_method(lMethodId);

		if (iReturnValue != BI_SUCCESS)
		{
			iReturnValue = BI_ERROR;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
	case BUILTIN_remove_abort_method:	//version B for Fieldbus
	{
		unsigned long ulMethodId=0;

		if (pVarParameters[0].isNumeric()) 
		{
			ulMethodId=(unsigned int)pVarParameters[0];
		}
		else
		{
			return false;
		}
		
		int iReturnValue = _remove_abort_method(ulMethodId);

		iReturnValue = ConvertToBLTINCode(iReturnValue);
	
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("remove_all_abort",pchFunctionName)==0)
	case BUILTIN_remove_all_abort_methods:
	case BUILTIN_remove_all_abort:
	{
		remove_all_abort();
		return true;
	}
/*Arun 190505 Start of code*/
	break;
	//else
	//if (strcmp("push_abort_method",pchFunctionName)==0)
	case BUILTIN_push_abort_method:
	case BUILTIN__push_abort_method:
	{
		long lMethodId=0;

		if (pVarParameters[0].isNumeric()) 
		{
			lMethodId=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

		int iReturnValue = push_abort_method(lMethodId);
		
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
	//else
	//if (strcmp("pop_abort_method",pchFunctionName)==0)
	case BUILTIN_pop_abort_method:
	{
		int iReturnValue = pop_abort_method();

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;

/*End of code*/
	
	//else
	//if (strcmp("NaN_value",pchFunctionName)==0)
	case BUILTIN_NaN_value:		//for HART
	{
        UINT ulReturnValue = NaN_value();

		float fReturnValue = *reinterpret_cast<float *>(&ulReturnValue);

		pVarReturnValue->SetValue(&fReturnValue, RUL_FLOAT);
		return true;
	}	
	break;

    // This NaN_Value builtin is for FF and Profibus
	case BUILTIN_FDI_NaN_value:
	{
        double nanval;
        long lReturnValue = NaN_value(&nanval);
        pVarParameters[0] = nanval;
        OutputParameterValue( pFuncExp, 0, pVarParameters[0], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;
	}	
	break;

	case BUILTIN_nan:
	{	
		wchar_t pchString[MAX_DD_STRING]={0};
		int  pSize   = MAX_DD_STRING;

		if ( ! GetStringParam(pchString, pSize, pVarParameters, 0) )
		{
			//input is okay to be NULL
		}

		wstring lW(pchString);
		string lS;
		lS = TStr2AStr(lW);
		double dReturnValue = nan(lS.c_str());

		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		
		return true;
	}
	break;
	case BUILTIN_nanf:
	{		
		wchar_t pchString[MAX_DD_STRING]={0};
		int  pSize   = MAX_DD_STRING;

		if ( ! GetStringParam(pchString, pSize, pVarParameters, 0) )
		{
			//return false; Empty string is okay for prompt
		}

		wstring lW(pchString);
		string lS;
		lS = TStr2AStr(lW);
		float fReturnValue = nanf(lS.c_str());

		pVarReturnValue->SetValue(&fReturnValue, RUL_FLOAT);
		
		return true;
	}	
	break;
	case BUILTIN_fpclassify:
	{	
		double dValue = 0.0;
		int    retVal = 0;

		if ( (pVarParameters[0].GetVarType() == RUL_DOUBLE)
			|| (pVarParameters[0].GetVarType() == RUL_FLOAT) )
		{
			dValue=(double)pVarParameters[0];
			retVal = fpclassify(dValue);	
		}
		else
		{
			return false;
		}
		pVarReturnValue->SetValue(&retVal, RUL_INT);
		return true;
	}	
	break;
	/* stevev 25jun07 - end stubouts ***/
	//else
	//if (strcmp("isetval",pchFunctionName)==0)
	case BUILTIN_isetval:
	{	
		RETURN_AT_NOT_NUMERIC(0);
        long long llValue = (long long)pVarParameters[0];	

		int iRetVal = isetval(llValue);
		RETURN_AT_DD_ERROR(iRetVal);
		pVarReturnValue->SetValue(&llValue, RUL_LONGLONG);

		return true;
	}
	break;
	//else
	//if (strcmp("lsetval",pchFunctionName)==0)
	case BUILTIN_lsetval:
	{	
		RETURN_AT_NOT_NUMERIC(0);
		int	lValue = (int)pVarParameters[0];

		int iRetVal = lsetval(lValue);
		RETURN_AT_DD_ERROR(iRetVal);
		pVarReturnValue->SetValue(&lValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("fsetval",pchFunctionName)==0)
	case BUILTIN_fsetval:
	{	
		RETURN_AT_NOT_NUMERIC(0);
		double dValue = (double)pVarParameters[0];

		int iRetVal = fsetval(dValue);
		RETURN_AT_DD_ERROR(iRetVal);
		pVarReturnValue->SetValue(&dValue, RUL_DOUBLE);

		return true;
	}
	break;
	case BUILTIN_put_double:
	{	
		//used by builtins put_double() and put_float()
		RETURN_AT_NOT_NUMERIC(0);
		double dValue = (double)pVarParameters[0];

		int iReturnValue = put_double(dValue);
		RETURN_AT_DD_ERROR(iReturnValue);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_put_signed:
	{	
		RETURN_AT_NOT_NUMERIC(0);
		long lValue = (long)(int)pVarParameters[0];

		int iReturnValue = put_signed(lValue);
		RETURN_AT_DD_ERROR(iReturnValue);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("igetval",pchFunctionName)==0)
	case BUILTIN_igetval:
	{	
		INT64 llReturnValue = 0;
		int iRetVal = igetval(llReturnValue);
		RETURN_AT_DD_ERROR(iRetVal);
		pVarReturnValue->SetValue(&llReturnValue, RUL_LONGLONG);

		return true;
	}
	break;
	//else
	//if (strcmp("fgetval",pchFunctionName)==0)
	case BUILTIN_fgetval:
	{	
		double dReturnValue = 0.0;
		int iRetVal = fgetval(dReturnValue);
		RETURN_AT_DD_ERROR(iRetVal);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	/*Arun 200505 Start of code */
	//else
	//if (strcmp("sgetval",pchFunctionName)==0)
	case BUILTIN_sgetval:
	{	
		int  pSize = MAX_DD_STRING - 1;

		int iRetVal = get_action_string(pFuncExp, pSize, &(pVarParameters[0]));

		if (iRetVal == BLTIN_WRONG_DATA_TYPE)
		{
			return false;
		}

		RETURN_AT_DD_ERROR(iRetVal);

		int iReturnValue = ConvertToBICode(iRetVal);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	//else
	//if (strcmp("ssetval",pchFunctionName)==0)
	case BUILTIN_ssetval:
	{	
		int  pSize = MAX_DD_STRING - 1;
		wchar_t pchString[MAX_DD_STRING] = {0};

		int iRetVal = put_action_string(&(pVarParameters[0]), pSize, pchString);

		if (iRetVal == BLTIN_WRONG_DATA_TYPE)
		{
			return false;
		}

		RETURN_AT_DD_ERROR(iRetVal);

		pVarReturnValue->SetValue((void *)pchString, RUL_DD_STRING);

		return true;
	}	
	break;
	/*End of code*/
	//else
	//if (strcmp("send",pchFunctionName)==0)
	case BUILTIN_send:
	{	
		int iCmd_no=-1;
		//uchar pchString[MAX_DD_STRING]={0};
		BYTE_STRING( byteString, STATUS_SIZE + 1);

		if (pVarParameters[0].isNumeric())
		{
			iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			DEL_BYTE_STR( byteString );
			return false;
		}

		int iReturnValue = send(iCmd_no,byteString.bs);

		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 1, byteString) )
		{
			DEL_BYTE_STR( byteString );
			return false;
		}

		DEL_BYTE_STR( byteString );

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("send_command",pchFunctionName)==0)
	case BUILTIN_send_command:
	{	
		int iCmd_no;
	
		if (pVarParameters[0].isNumeric())
		{
				iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

				
		int iReturnValue = send_command(iCmd_no);

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("send_command_trans",pchFunctionName)==0)
	case BUILTIN_send_command_trans:
	{	
		int iCmd_no=-1;
		int iTrans_no=-1;
		
		if (pVarParameters[0].isNumeric())
		{
				iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

		if (pVarParameters[1].isNumeric())
		{
				iTrans_no=(int)pVarParameters[1];
		}	
		else
		{
			return false;
		}	
				
		int iReturnValue = send_command_trans(iCmd_no,iTrans_no);

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("send_trans",pchFunctionName)==0)
	case BUILTIN_send_trans:
	{	
		int iCmd_no=-1;
		int iTrans_no=-1;
		BYTE_STRING( byteString, STATUS_SIZE + 1);
		
		if (pVarParameters[0].isNumeric())
		{
			iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			DEL_BYTE_STR( byteString );
			return false;
		}

		if (pVarParameters[1].isNumeric())
		{
			iTrans_no=(int)pVarParameters[1];
		}	
		else
		{
			DEL_BYTE_STR( byteString );
			return false;
		}

				
		int iReturnValue = send_trans(iCmd_no,iTrans_no,byteString.bs);//was  pchString);
		
		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 2, byteString) )
		{
			DEL_BYTE_STR( byteString );
			return false;
		}

		DEL_BYTE_STR( byteString );

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("ext_send_command",pchFunctionName)==0)
	case BUILTIN_ext_send_command:
	{	
		int iCmd_no=-1;
		BYTE_STRING( status,     (STATUS_SIZE + 1));
		BYTE_STRING( morestatus, (STATUS_SIZE + 1));
		BYTE_STRING( info,       (MAX_XMTR_STATUS_LEN + 1));

		int  InfoSize = 0;

		
		if (pVarParameters[0].isNumeric())
		{
				iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}

						
		int iReturnValue = ext_send_command(iCmd_no, status.bs, morestatus.bs,info.bs,InfoSize);
		info.bsLen = InfoSize + 1;
		
		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 1, status) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}
		
		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 2, morestatus) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}
		
		if ( ! SetByteStringParam(pFuncExp, pVarParameters, 3, info) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}

		DEL_BYTE_STR( status );
		DEL_BYTE_STR( morestatus );
		DEL_BYTE_STR( info );

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("ext_send_command_trans",pchFunctionName)==0)
	case BUILTIN_ext_send_command_trans:
	{	
		int iCmd_no=-1;
		int iTrans_no=-1;
		BYTE_STRING( status,     (STATUS_SIZE + 1));
		BYTE_STRING( morestatus, (STATUS_SIZE + 1));
		BYTE_STRING( info,       (MAX_XMTR_STATUS_LEN + 1));

		int  InfoSize = 0;


		
		if (pVarParameters[0].isNumeric())// cmd number
		{
				iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}

		if (pVarParameters[1].isNumeric())// transaction number
		{
				iTrans_no=(int)pVarParameters[1];
		}
		else
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}

						
		int iReturnValue = ext_send_command_trans(iCmd_no,iTrans_no,status.bs,morestatus.bs,
																	info.bs,InfoSize);
		info.bsLen = InfoSize + 1;
	
		if ( !SetByteStringParam(pFuncExp, pVarParameters, 2, status) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}
		
		if ( !SetByteStringParam(pFuncExp, pVarParameters, 3, morestatus) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}
		
		if ( !SetByteStringParam(pFuncExp, pVarParameters, 4, info) )
		{
			DEL_BYTE_STR( status );
			DEL_BYTE_STR( morestatus );
			DEL_BYTE_STR( info );
			return false;
		}

		DEL_BYTE_STR( status );
		DEL_BYTE_STR( morestatus );
		DEL_BYTE_STR( info );

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("tsend_command",pchFunctionName)==0)
	case BUILTIN_tsend_command:
	{	
		int iCmd_no=-1;
	
		if (pVarParameters[0].isNumeric())
		{
			iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

				
		int iReturnValue = tsend_command(iCmd_no);

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}	
	break;
	//else
	//if (strcmp("tsend_command_trans",pchFunctionName)==0)
	case BUILTIN_tsend_command_trans:
	{	
		int iCmd_no=-1;
		int iTrans_no=-1;
		
		if (pVarParameters[0].isNumeric())
		{
				iCmd_no=(int)pVarParameters[0];
		}
		else
		{
			return false;
		}

		if (pVarParameters[1].isNumeric())
		{
				iTrans_no=(int)pVarParameters[1];
		}	
		else
		{
			return false;
		}	
				
		int iReturnValue = tsend_command_trans(iCmd_no,iTrans_no);

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);	

		return true;
	}
	break;
		
	// Anil December 16 2005 deleted the Plot builtins case

/*Arun 110505 Start of code*/
/*****************************************Math Builtions (eDDL) ****************************/
	//else
	//if (strcmp("abs",pchFunctionName)==0)
	case BUILTIN_abs:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =(double)abs(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("acos",pchFunctionName)==0)
	case BUILTIN_acos:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =acos(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("asin",pchFunctionName)==0)
	case BUILTIN_asin:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =asin(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("atan",pchFunctionName)==0)
	case BUILTIN_atan:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =atan(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("cbrt",pchFunctionName)==0)
	case BUILTIN_cbrt:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =cbrt(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("ceil",pchFunctionName)==0)
	case BUILTIN_ceil:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =ceil(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("cos",pchFunctionName)==0)
	case BUILTIN_cos:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =cos(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("cosh",pchFunctionName)==0)
	case BUILTIN_cosh:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =cosh(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("exp",pchFunctionName)==0)
	case BUILTIN_exp:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =exp(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("floor",pchFunctionName)==0)
	case BUILTIN_floor:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =floor(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("fmod",pchFunctionName)==0)
	case BUILTIN_fmod:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValueX=(double)pVarParameters[0];	
		RETURN_AT_NOT_NUMERIC(1);// added stevev 18feb08		
		double dValueY=(double)pVarParameters[1];	
		double dReturnValue =fmod(dValueX,dValueY);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
#ifdef XMTR
    case BUILTIN_frand:
	{
		double dReturnValue =frand();
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
#endif
	//else
	//if (strcmp("log",pchFunctionName)==0)
	case BUILTIN_log:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =log(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("log10",pchFunctionName)==0)
	case BUILTIN_log10:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =log10(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;
	}
	break;
	//else
	//if (strcmp("log2",pchFunctionName)==0)
	case BUILTIN_log2:	
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =log2(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("pow",pchFunctionName)==0)
	case BUILTIN_pow:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValueX=(double)pVarParameters[0];	
		RETURN_AT_NOT_NUMERIC(1);// added stevev 18feb08		
		double dValueY=(double)pVarParameters[1];	
		double dReturnValue =pow(dValueX,dValueY);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("round",pchFunctionName)==0)
	case BUILTIN_round:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =round(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("sin",pchFunctionName)==0)
	case BUILTIN_sin:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =sin(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("sinh",pchFunctionName)==0)
	case BUILTIN_sinh:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =sinh(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("sqrt",pchFunctionName)==0)
	case BUILTIN_sqrt:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =sqrt(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("tan",pchFunctionName)==0)
	case BUILTIN_tan:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =tan(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("tanh",pchFunctionName)==0)
	case BUILTIN_tanh:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =tanh(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("trunc",pchFunctionName)==0)
	case BUILTIN_trunc:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		double dValue=(double)pVarParameters[0];	
		double dReturnValue =trunc(dValue);
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
		return true;	
	}
	break;
	//else
	//if (strcmp("atof",pchFunctionName)==0)
	case BUILTIN_atof:
	{// stevev 11feb08 -results of inter_varient rework
	//	char* dValue=(char*)pVarParameters[0];	
		char* dValue = NULL;
		pVarParameters[0].GetStringValue(&dValue);
		if (dValue)
		{
			double dReturnValue =atof(dValue);
			pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
			delete[] dValue;
			return true;
		}
		return false;
	}
	break;
	//else
	//if (strcmp("atoi",pchFunctionName)==0)
	case BUILTIN_atoi:
	{// stevev 11feb08 -results of inter_varient rework
	//	char* dValue=(char*)pVarParameters[0];	
		char* dValue = NULL;
		pVarParameters[0].GetStringValue(&dValue);
		if (dValue)
		{
			int dReturnValue =atoi(dValue);
			pVarReturnValue->SetValue(&dReturnValue, RUL_INT);
			delete[] dValue;
			return true;
		}
		return false;
	}
	break;

	// This is itoa builtin version B. It takes three input arguments.
	// dValue1 -- Value to be converted to a string. 
	// dValue2 -- Array in memory where to store the resulting null-terminated string. 
	// dValue3 -- Numerical base used to represent the value as a string, between 2 and 36, where 10 means decimal base, 16 hexadecimal, 8 octal, and 2 binary. 
	case BUILTIN__itoa:
	{
		RETURN_AT_NOT_NUMERIC(0);// added stevev 18feb08		
		int dValue1=(int)pVarParameters[0];	
		wchar_t dValue2[MAX_DD_STRING+1]={0};// NOT Unicode
		RETURN_AT_NOT_NUMERIC(2);// added stevev 18feb08		
		int dValue3=(int)pVarParameters[2];	
		wchar_t* dReturnValue =itoa(dValue1,dValue2,dValue3);

		//Update the dValue2 to DDS/user
		if ( ! SetStringParam(pFuncExp, pVarParameters, 1, dValue2) )
		{
			return false;
		}
		pVarReturnValue->SetValue(dReturnValue, RUL_DD_STRING);

		return true;	
	}
	break;

	// This is itoa builtin version A. It takes three input arguments.
	// dValue1 -- Value to be converted to a string. 
	case BUILTIN_itoa:
	{
		RETURN_AT_NOT_NUMERIC(0);		
		int nValue1=(int)pVarParameters[0];	
		char pszValue[MAX_DD_STRING+1]={0};// NOT Unicode
        char* pszReturnValue = ::itoa(nValue1, pszValue, 10);// note that _itoa() itself does offer radix
		////Return value shoul be set to pszValue and not pszReturnValue
		pVarReturnValue->SetValue(pszValue, RUL_CHARPTR);

		return true;	
	}
	break;

	case BUILTIN_ftoa:
	{
		RETURN_AT_NOT_NUMERIC(0);
		wchar_t* pszReturnValue =ftoa((float)pVarParameters[0]);
		pVarReturnValue->SetValue(pszReturnValue, RUL_DD_STRING);
		if(pszReturnValue)
		{
			delete pszReturnValue;
			pszReturnValue = NULL;
		}
		return true;
	}
	break;

	case BUILTIN_ByteToDouble:
	{
		for (int i=0; i<iNumberOfParameters; i++)
		{
			RETURN_AT_NOT_NUMERIC(i);
		}
		double pszReturnValue = ByteToDouble(pVarParameters[0], pVarParameters[1], pVarParameters[2], pVarParameters[3],
									pVarParameters[4], pVarParameters[5], pVarParameters[6], pVarParameters[7]);
		pVarReturnValue->SetValue(&pszReturnValue, RUL_DOUBLE);
		return true;
	}
	break;

	case BUILTIN_ByteToFloat:
	{
		for (int i=0; i<iNumberOfParameters; i++)
		{
			RETURN_AT_NOT_NUMERIC(i);
		}
		float pszReturnValue = ByteToFloat(pVarParameters[0], pVarParameters[1], pVarParameters[2], pVarParameters[3]);
		pVarReturnValue->SetValue(&pszReturnValue, RUL_FLOAT);
		return true;
	}
	break;

	case BUILTIN_ByteToLong:
	{
		for (int i=0; i<iNumberOfParameters; i++)
		{
			RETURN_AT_NOT_NUMERIC(i);
		}
		long pszReturnValue = ByteToLong(pVarParameters[0], pVarParameters[1], pVarParameters[2], pVarParameters[3]);
		pVarReturnValue->SetValue(&pszReturnValue, RUL_INT);
		return true;
	}
	break;

	case BUILTIN_ByteToShort:
	{
		for (int i=0; i<iNumberOfParameters; i++)
		{
			RETURN_AT_NOT_NUMERIC(i);
		}
		short pszReturnValue = ByteToShort(pVarParameters[0], pVarParameters[1]);
		pVarReturnValue->SetValue(&pszReturnValue, RUL_SHORT);
		return true;
	}
	break;

	case BUILTIN_DoubleToByte:
	{
		RETURN_AT_NOT_NUMERIC(0);	//return false if the first input is not numeric
		unsigned char output[8];
		int iReturnValue = DoubleToByte(pVarParameters[0], &output[0], &output[1], &output[2],
										&output[3], &output[4], &output[5], &output[6], &output[7]);
		if ( ! SetCharArrayParam(pFuncExp, pVarParameters, 1, output, 8) )
		{
			return false;
		}
		return true;
	}
	break;

	case BUILTIN_FloatToByte:
	{
		RETURN_AT_NOT_NUMERIC(0);
		unsigned char output[4];
		int iReturnValue = FloatToByte(pVarParameters[0], &output[0], &output[1], &output[2], &output[3]);
		if ( ! SetCharArrayParam(pFuncExp, pVarParameters, 1, output, 4) )
		{
			return false;
		}
		return true;
	}
	break;

	case BUILTIN_LongToByte:
	{
		RETURN_AT_NOT_NUMERIC(0);
		unsigned char output[4];
		int iReturnValue = LongToByte(pVarParameters[0], &output[0], &output[1], &output[2], &output[3]);
		if ( ! SetCharArrayParam(pFuncExp, pVarParameters, 1, output, 4) )
		{
			return false;
		}
		return true;
	}
	break;

	case BUILTIN_ShortToByte:
	{
		RETURN_AT_NOT_NUMERIC(0);
		unsigned char output[2];
		int iReturnValue = ShortToByte(pVarParameters[0], &output[0], &output[1]);
		if ( ! SetCharArrayParam(pFuncExp, pVarParameters, 1, output, 2) )
		{
			return false;
		}
		return true;
	}
	break;


/*****************************************End of Math Builtins (eDDL) *********************/

/*End of code*/

/* Arun 160505 Start of code */

/****************************************Date Time Builtins (eDDL)*************************/
	//else
	//if (strcmp("YearMonthDay_to_Date",pchFunctionName)==0)
//	case BUILTIN_YearMonthDay_to_Date:/* WS:EPM Not a builtin-25jun07 */
//	{
//		int dValue1=(int)pVarParameters[0];	
//		int dValue2=(int)pVarParameters[1];	
//		int dValue3=(int)pVarParameters[2];	
//		long dReturnValue =YearMonthDay_to_Date(dValue1,dValue2,dValue3);
//		pVarReturnValue->SetValue(&dReturnValue, RUL_INT);
//		return true;	
//	}
//	break;
	//else
	//if (strcmp("Date_to_Year",pchFunctionName)==0)
	case BUILTIN_Date_to_Year:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		long lValue=(int)pVarParameters[0];	/* WS:EPM-changed types-25jun07*/
		int lReturnValue =Date_to_Year(lValue);// WS - 9apr07 - 2005 checkin
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("Date_to_Month",pchFunctionName)==0)
	case BUILTIN_Date_to_Month:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		long lValue=(int)pVarParameters[0];	/* WS:EPM-changed types-25jun07*/
		int lReturnValue =Date_to_Month(lValue); // WS - 9apr07 - 2005 checkin
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("Date_to_DayOfMonth",pchFunctionName)==0)
	case BUILTIN_Date_to_DayOfMonth:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		long lValue=(int)pVarParameters[0];	/* WS:EPM-changed types-25jun07*/
		int lReturnValue =Date_to_DayOfMonth(lValue); // WS - 9apr07 - 2005 checkin
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_DATE_to_days:
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		long lValue0=(int)pVarParameters[0];
		long lValue1=(int)pVarParameters[1];
		long lReturnValue = DATE_to_days(lValue0, lValue1);
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_days_to_DATE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		long lValue0=(int)pVarParameters[0];
		long lValue1=(int)pVarParameters[1];
		long lReturnValue = days_to_DATE(lValue0, lValue1);
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_From_DATE_AND_TIME_VALUE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		long lValue=(int)pVarParameters[0];
		unsigned long ulValue=(unsigned int)pVarParameters[1];
		long lReturnValue = From_DATE_AND_TIME_VALUE(lValue, ulValue);
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_From_TIME_VALUE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		long lReturnValue = -1;

		if (pVarParameters[0].GetMaxSize() <= 4)
		{
			unsigned long ulValue=(unsigned int)pVarParameters[0];
			lReturnValue = From_TIME_VALUE(ulValue);
		}
		else
		{
			unsigned long long ullValue=(unsigned long long)pVarParameters[0];
			lReturnValue = From_TIME_VALUE(ullValue);
		}
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("GetCurrentDate",pchFunctionName)==0)
	case BUILTIN_GetCurrentDate:
	{
		long dReturnValue = GetCurrentDate();
		pVarReturnValue->SetValue(&dReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("GetCurrentTime",pchFunctionName)==0)
	case BUILTIN_GetCurrentTime:
	{
		long lReturnValue = GetMICurrentTime();
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("GetCurrentDateAndTime",pchFunctionName)==0)
	case BUILTIN_GetCurrentDateAndTime:
	{
		long lReturnValue = GetCurrentDateAndTime();
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;	
	}
	break;
	//else
	//if (strcmp("To_Date_and_Time",pchFunctionName)==0)
	case BUILTIN_To_Date_and_Time:/* WS:EPM Not a builtin-25jun07 */
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);

		int days = (int)pVarParameters[0];	
		int hour = (int)pVarParameters[1];	
		int minute = (int)pVarParameters[2];	
		int second = (int)pVarParameters[3];	
		int millisecond = (int)pVarParameters[4];	

        _UINT64 ullReturnValue = To_Date_and_Time(days, hour, minute, second, millisecond);
		pVarReturnValue->SetValue(&ullReturnValue, RUL_ULONGLONG);
		return true;	
	}
	break;
	
/***************************************Date Time Builtins (eDDL)**************************/


/****************************Start of DD_STRING  Builtins  (eDDL) ********************/
//Added By Anil June 17 2005 --starts here
	//else
	//if (strcmp("STRSTR",pchFunctionName)==0)
	case BUILTIN_strstr:
	{
		wchar_t* string_var = NULL;
		wchar_t* substring_to_find = NULL;
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);
		pVarParameters[1].GetStringValue(&substring_to_find);
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		GetLanguageCode(substring_to_find,szLangCode,&bLanCodePrese);
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);	
		
		wchar_t* szTemp = STRSTR(string_var,substring_to_find);
		//As per Vibhor's Suggestion if the string Not Found this is made ad terminated string
		if(szTemp == NULL)
		{
            pVarReturnValue->SetValue((void *)_T("\0"), RUL_DD_STRING);
		}
		else
		{
			if(bLanCodePrese ==true)

			{	
				size_t nLen = wcslen(szTemp)+1+4;//Length + 1 + Language Code
				wchar_t *szTempValue = new wchar_t[nLen]; 
				memset(szTempValue,0,sizeof(szTempValue));
				wcscpy(szTempValue,szLangCode);
				wcscat(szTempValue,szTemp);
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTempValue, RUL_DD_STRING);
				if(szTempValue)
				{
					delete[] szTempValue;
					szTempValue = NULL;
				}
			}
			else
			{
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTemp, RUL_DD_STRING);

			}				
		}		
		//Delete all the Memory Allocated;
		if(string_var)
		{
			delete[] string_var;
			string_var = NULL;
		}
		if(substring_to_find)
		{
			delete[] substring_to_find;
			substring_to_find = NULL;
		}
		return true;	
	}
	break;

	//else
	//if (strcmp("STRUPR",pchFunctionName)==0)
	case BUILTIN_strupr:
	{
		wchar_t* string_var = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);	
		
		wchar_t* szTemp = STRUPR(string_var);
		//As per Vibhor's Suggestion if the string Not Found this is made ad terminated string
		if(szTemp == NULL)
		{
            pVarReturnValue->SetValue((void *)_T("\0"), RUL_DD_STRING);
		}
		else
		{
			if(bLanCodePrese ==true)

			{
				size_t nLen = wcslen(szTemp)+1+4;//Length + 1 + Language Code
				wchar_t *szTempValue = new wchar_t[nLen]; 
				memset(szTempValue,0,sizeof(szTempValue));
				wcscpy(szTempValue,szLangCode);
				wcscat(szTempValue,szTemp);
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTempValue, RUL_DD_STRING);
				if(szTempValue)
				{
					delete[] szTempValue;
					szTempValue	= NULL;
				}

			}
			else
			{
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTemp, RUL_DD_STRING);

			}	
			
		}		
		//Delete all the Memory Allocated;
		if(string_var)
		{
			delete[] string_var;
			string_var= NULL;
		}
		return true;	
	}
	break;

	//else
	//if (strcmp("STRLWR",pchFunctionName)==0)
	case BUILTIN_strlwr:
	{
		wchar_t* string_var = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);	
		
		wchar_t* szTemp = STRLWR(string_var);
		//As per Vibhor's Suggestion if the string Not Found this is made ad terminated string
		if(szTemp == NULL)
		{
            pVarReturnValue->SetValue((void *)_T("\0"), RUL_DD_STRING);
		}
		else
		{
			if(bLanCodePrese ==true)

			{
				size_t nLen = wcslen(szTemp)+1+4;//Length + 1 + Language Code
				wchar_t *szTempValue = new wchar_t[nLen]; 
				memset(szTempValue,0,sizeof(szTempValue));
				wcscpy(szTempValue,szLangCode);
				wcscat(szTempValue,szTemp);
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTempValue, RUL_DD_STRING);
				if(szTempValue)
				{
					delete[] szTempValue;
					szTempValue = NULL;
				}

			}
			else
			{
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTemp, RUL_DD_STRING);

			}	
			
		}		
		//Delete all the Memory Allocated;
		if(string_var)
		{
			delete[] string_var;
			string_var = NULL;
		}
		return true;	
	}
	break;

	//else
	//if (strcmp("STRLEN",pchFunctionName)==0)
	case BUILTIN_strlen:
	{
		wchar_t* string_var = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);	
		
		int istrLen = STRLEN(string_var);
		pVarReturnValue->SetValue(&istrLen, RUL_INT);
		if(string_var)
		{
			delete[] string_var;
			string_var = NULL;
		}
		return true;	
	}
	break;


	//else
	//if (strcmp("STRCMP",pchFunctionName)==0)
	case BUILTIN_strcmp:
	{
		wchar_t* string_var1 = NULL;
		wchar_t* string_var2 = NULL;
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var1);
		pVarParameters[1].GetStringValue(&string_var2);
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		GetLanguageCode(string_var1,szLangCode,&bLanCodePrese);	
		GetLanguageCode(string_var2,szLangCode,&bLanCodePrese);	
		
		int iCmp = STRCMP(string_var1,string_var2);
		pVarReturnValue->SetValue(&iCmp, RUL_INT);
		//Delete all the Memory Allocated;
		if(string_var1)
		{
			delete[] string_var1;
			string_var1 = NULL;
		}
		if(string_var2)
		{
			delete[] string_var2;
			string_var2 = NULL;
		}
		return true;
	}
	break;

	//else
	//if (strcmp("STRTRIM",pchFunctionName)==0)
	case BUILTIN_strtrim:
	{
		wchar_t* string_var = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);	
		
		wchar_t* szTemp = STRTRIM(string_var);
		//As per Vibhor's Suggestion if the string Not Found this is made ad terminated string
		if(szTemp == NULL)
		{
            pVarReturnValue->SetValue((void *)_T("\0"), RUL_DD_STRING);
		}
		else
		{
			if(bLanCodePrese ==true)

			{
				size_t nLen = wcslen(szTemp)+1+4;//Length + 1 + Language Code
				wchar_t *szTempValue = new wchar_t[nLen]; 
				memset(szTempValue,0,sizeof(szTempValue));
				wcscpy(szTempValue,szLangCode);
				wcscat(szTempValue,szTemp);
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTempValue, RUL_DD_STRING);
				if(szTempValue)
				{
					delete[] szTempValue;
					szTempValue = NULL;
				}

			}
			else
			{
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTemp, RUL_DD_STRING);

			}	
			
		}
		//Delete all the Memory Allocated;
		if(string_var)
		{
			delete[] string_var;
			string_var = NULL;
		}
		return true;	
	}
	break;
	
	//else
	//if (strcmp("STRMID",pchFunctionName)==0)
	case BUILTIN_strmid:
	{
		wchar_t* string_var = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		
		//Get the String value 
		pVarParameters[0].GetStringValue(&string_var);
		
		//Append the Language code  When ur Returning
		GetLanguageCode(string_var,szLangCode,&bLanCodePrese);
		RETURN_AT_NOT_NUMERIC(1);// added stevev 18feb08		
		int iStrat=(int)pVarParameters[1];	
		RETURN_AT_NOT_NUMERIC(2);// added stevev 18feb08		
		int iLen=(int)pVarParameters[2];	
		
		wchar_t* szTemp = STRMID(string_var,iStrat,iLen);
		//As per Vibhor's Suggestion if the string Not Found this is made ad terminated string
		if(szTemp == NULL)
		{
            pVarReturnValue->SetValue((void *)_T("\0"), RUL_DD_STRING);
		}
		else
		{
			if(bLanCodePrese ==true)

			{
				size_t nLen = wcslen(szTemp)+1+4;//Length + 1 + Language Code
				wchar_t *szTempValue = new wchar_t[nLen]; 
				memset(szTempValue,0,sizeof(szTempValue));
				wcscpy(szTempValue,szLangCode);
				wcscat(szTempValue,szTemp);
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTempValue, RUL_DD_STRING);
				if(szTempValue) //Uncomment latter Boss as it was commnted to make it compile as nesting was going beyond limit
				{
					delete[] szTempValue;				
					szTempValue = NULL;
				}
			}
			else
			{
				//Set this vlue in the variant
				pVarReturnValue->SetValue(szTemp, RUL_DD_STRING);

			}	
			
		}		
	
		//Delete all the Memory Allocated;
		if(string_var)
		{
			delete[] string_var;
			string_var = NULL;
		}
		
		if(szTemp)
		{
			delete[] szTemp;
			szTemp = NULL;
		}

		return true;	
	}
	break;

	case BUILTIN_strleft:
	{
		wchar_t string_var[MAX_DD_STRING]={0};	
		int pSize = MAX_DD_STRING;
		//Get the String value 
        if ( ! GetStringParam(string_var, pSize, pVarParameters, 0) )
        {
            return false;
        }

		RETURN_AT_NOT_NUMERIC(1);		
		int length = (int)pVarParameters[1];	
		
		string_var[length] = _T('\0');

		//Set this vlue in the variant
		pVarReturnValue->SetValue(string_var, RUL_DD_STRING);
	
		return true;	
	}
	break;

	case BUILTIN_strright:
	{
		wchar_t string_var[MAX_DD_STRING]={0};	
		int pSize = MAX_DD_STRING;
		//Get the String value 
        if ( ! GetStringParam(string_var, pSize, pVarParameters, 0) )
        {
            return false;
        }

		RETURN_AT_NOT_NUMERIC(1);
		size_t length = (int)pVarParameters[1];
		if( length < 0)
		{
			return false;
		}	
		
		wchar_t *wsPtr;
		if( wcslen(string_var) >= length)
			wsPtr = &string_var[wcslen(string_var) - length];
		else
			wsPtr = string_var;

		//Set this vlue in the variant
		pVarReturnValue->SetValue(wsPtr, RUL_DD_STRING);
	
		return true;	
	}
	break;

	/*Vibhor 200905: Start of Code*/
	case BUILTIN__ListInsert:
	{
		long lListId = 0;
		int iIndx = -1;
		long lItemId = 0;

		RETURN_AT_NOT_NUMERIC(0);		
		RETURN_AT_NOT_NUMERIC(1);		
		RETURN_AT_NOT_NUMERIC(2);		

		//Get the List Id
		lListId = (int)pVarParameters[0];

		//Get the Index at which the insertion is required
		iIndx = (int)pVarParameters[1];

		//Get the Id of the item which needs to be inserted in the list
		lItemId = (int)pVarParameters[2];

		int iReturnValue = ListInsert2(lListId,iIndx, 0, 0, lItemId);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;

	case BUILTIN__ListDeleteElementAt:
	{
		long lListId = 0;
		int iIndx = -1;

		RETURN_AT_NOT_NUMERIC(0);		
		RETURN_AT_NOT_NUMERIC(1);		

		//Get the List Id
		lListId = (int)pVarParameters[0];

		//Get the Index of the element which needs to be deleted.
		iIndx = (int)pVarParameters[1];

		int iReturnValue = ListDeleteElementAt2(lListId, iIndx, 0, 0, 0);
		
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}
	break;
/*Vibhor 200905: End of Code*/

	//Anil September 26 2005 added MenuDisplay Start of Code
	case BUILTIN__MenuDisplay:
	{
		long lMenuId = 0;
		char szLocaVarName[MAX_DD_STRING];
		long lselection = 0;

		//Get the Menu Id
		RETURN_AT_NOT_NUMERIC(0);		
		lMenuId = (int)pVarParameters[0];
		
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 1) )
		{
			return false;
		}

		//This is the DD Local Varible which is passes by reference
		//So when we go out of this function, We need to update this value with the out param of
		//_MenuDisplay below
		ProtocolType iProtocol = m_pMeth->GetStartedProtocol();
		if (iProtocol != nsEDDEngine::FF)
		{
			if ( ! GetCharStringParam(szLocaVarName, MAX_DD_STRING, pVarParameters, 2) )
			{
				return false;
			}
		}
			
		int iReturnValue = _MenuDisplay(lMenuId, pchUIPrompt, &lselection);
	
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else if ((iProtocol == nsEDDEngine::FF) || (iProtocol == nsEDDEngine::PROFIBUS))
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);
	
			if (iReturnValue == BLTIN_SUCCESS)
			{
				pVarParameters[2].SetValue(&lselection, RUL_INT);
				bool bRet = OutputParameterValue( pFuncExp, 2, pVarParameters[2], 0);

				if (!bRet)
				{
					iReturnValue = BLTIN_VAR_NOT_FOUND;
				}
			}
			else if (iProtocol == nsEDDEngine::PROFIBUS)
			{
				iReturnValue = BI_ERROR;
			}

			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}
		else	//nsEDDEngine::HART
		{
			if (iReturnValue == BI_SUCCESS)
			{
				if (szLocaVarName[0] != '0')
				{
					// This is workaround for bug #2702. If there is space after the last agument of MenuDisplay builtin, It should be handled. 
					// This is bug in HART tokenizer where it is adding spaces after the argumnet name while storing in string table.
					// FF and PB tokenizer works fine in this case.

					// remove trailing whitespace from the szLocaVarName
					string sTrim(szLocaVarName);
					size_t nLastPosition = sTrim.length() - 1;			// get the last position of the array
					while (isspace(sTrim[nLastPosition]))
					{
						sTrim.erase(nLastPosition);					// remove the whitespace
						strcpy(szLocaVarName, sTrim.c_str());	// copy the new string to pchVariableName
						nLastPosition = sTrim.length() - 1;			// get the new last position of the array
					}

					//We got the selection which, actually has to be stored in szLocaVarName
					//Hence the below additional code
					//Create a inter varinat with value equal to lselection and data type as RUL_INT
					//Note: Function OutputParameterValue() should not be used here for HART
					//because last parameter is a function of literal_string()
					INTER_VARIANT varTemp;
					char szLang[5] = {0};
					bool bLangPresent=false;
					//		Remove the Language code , if it was apended
					GetLanguageCode(szLocaVarName,szLang,&bLangPresent);			
					varTemp.SetValue(&lselection,RUL_INT);	
					//		Update the DD local var szLocaVarName with the value lselection
					bool bRet = m_pInterpreter->SetVariableValue(szLocaVarName,varTemp);

					if (!bRet)
					{
						iReturnValue = BI_ERROR;
					}
				}
				//else, it is okay because it is Menu builtin in HART
			}
			else
			{
				iReturnValue = BI_ERROR;
			}

			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}
	
		return true;
	}
	break;

	//This builtin is useed by Profibus only
	case BUILTIN_Menu:
	{
		//Get the Menu Id
		RETURN_AT_NOT_NUMERIC(0);
		unsigned ulMenuId = (int)pVarParameters[0];

		//this builtin is the same as builtin MenuDisplay(reference, "OK", 0);
		pchUIPrompt[0] = L'O';
		pchUIPrompt[1] = L'K';
		pchUIPrompt[2] = L'\0';

		long lselection = 0;

		int iReturnValue = _MenuDisplay(ulMenuId, pchUIPrompt, &lselection);

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);

			if (iReturnValue != BLTIN_SUCCESS)
			{
				iReturnValue = BI_ERROR;
			}

			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}
		return true;
	}
	break;

	//This builtin is useed by FIELDBUS
	case BUILTIN_select_from_menu:
	{
		//get prompt string
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//Empty string is okay
		}

		//Get the item ID, member ID and count
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];

		tchar szOptionList[MAX_DD_STRING]={0};
		if ( ! GetStringParam(szOptionList, MAX_DD_STRING, pVarParameters, 4) )
		{
			return false;
		}

		long lselection = 0;
		int iReturnValue = select_from_menu(pchUIPrompt, pULongItemIds, pULongMemberIds, iNumberOfItemIds, szOptionList, &lselection);
		RETURN_AT_DD_ERROR(iReturnValue);
			
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{	//We got the selction which,actually has to be stored in pVarParameters[5]
			pVarParameters[5].SetValue(&lselection, RUL_INT);
			OutputParameterValue( pFuncExp, 5, pVarParameters[5], 0);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;
	//This builtin is useed by FIELDBUS
	case BUILTIN_select_from_menu2:
	{
		//get prompt string
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//return false;
		}

		//Get the item ID, member ID and count
		unsigned long pULongBlkIds[100]={0};
		unsigned long pULongBlkNums[100]={0};
		unsigned long pULongItemIds[100]={0};
		unsigned long pULongMemberIds[100]={0};
		int iNumberOfItemIds = 0;

		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1],pULongBlkIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2],pULongBlkNums,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3],pULongItemIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[4],pULongMemberIds,iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}
		else
		{
			return false;
		}	

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];

		tchar szOptionList[MAX_DD_STRING]={0};
		if ( ! GetStringParam(szOptionList, MAX_DD_STRING, pVarParameters, 6) )
		{
			return false;
		}

		long lselection = 0;
		int iReturnValue = select_from_menu2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, (long)iNumberOfItemIds, szOptionList, &lselection);
		RETURN_AT_DD_ERROR(iReturnValue);
			
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{	//We got the selction which,actually has to be stored in pVarParameters[7]
			pVarParameters[7].SetValue(&lselection, RUL_INT);
			OutputParameterValue( pFuncExp, 7, pVarParameters[7], 0);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN_DiffTime:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(1);/* stevev-added check-25jun07*/
		// WS - 25jun07 - changed data types, fixed index //
		long time_t1=(int)pVarParameters[0];// WS - 9apr07 - 2005 checkin	
		long time_t0=(int)pVarParameters[1];// WS - 9apr07 - 2005 checkin	

		double dDiffTime =DiffTime(time_t1,time_t0);// WS - 9apr07 - 2005 checkin

		pVarReturnValue->SetValue(&dDiffTime, RUL_DOUBLE);
		return true;	
	}
	break;

	case BUILTIN_AddTime:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(1);/* stevev-added check-25jun07*/
		// WS - 25jun07 - changed data types //
		long time_t1 =(int)pVarParameters[0];// WS - 9apr07 - 2005 checkin	
		double dSeconds=(double)pVarParameters[1];	
		long lAddedTime =AddTime(time_t1,dSeconds);
		pVarReturnValue->SetValue(&lAddedTime, RUL_INT);
		return true;	
	}
	break;

	case BUILTIN_Make_Time:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(1);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(2);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(3);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(4);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(5);/* stevev-added check-25jun07*/
		int year		= (int)pVarParameters[0];
		int month		= (int)pVarParameters[1];
		int dayofmonth	= (int)pVarParameters[2];
		int hour		= (int)pVarParameters[3]; 
		int minute		= (int)pVarParameters[4]; 
		int second		= (int)pVarParameters[5]; 
		int isDST		= (int)pVarParameters[6];

		long lConvTime	=	Make_Time(year,month , dayofmonth, hour, minute, second ,isDST);

		pVarReturnValue->SetValue(&lConvTime, RUL_INT);//WS-fixed return type 25jun07
		return true;	
	}
	break;

	case BUILTIN_To_Time:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(1);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(2);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(3);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(4);/* stevev-added check-25jun07*/
		long date		= (int)pVarParameters[0];// WS - 9apr07 - 2005 checkin 
		int hour		= (int)pVarParameters[1]; 
		int minute		= (int)pVarParameters[2]; 
		int second		= (int)pVarParameters[3]; 
		int isDST		= (int)pVarParameters[4];

		long lConvTime	=	To_Time(date, hour, minute, second, isDST);
		pVarReturnValue->SetValue(&lConvTime, RUL_INT);//WS-fixed return type 25jun07
		return true;	
	}
	break;

	case BUILTIN_Date_To_Time:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		long date = (int)pVarParameters[0];// WS - 9apr07 - 2005 checkin 

		long lConvTime =Date_To_Time (date);
		pVarReturnValue->SetValue(&lConvTime, RUL_INT);//WS-fixed return type 25jun07
		return true;	
	}
	break;

	case BUILTIN_To_Date:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(1);/* stevev-added check-25jun07*/
		RETURN_AT_NOT_NUMERIC(2);/* stevev-added check-25jun07*/
		int Year		= (int)pVarParameters[0]; 
		int month		= (int)pVarParameters[1]; 
		int DayOfMonth	= (int)pVarParameters[2]; 

		long lConvDate =To_Date(Year, month, DayOfMonth);
		pVarReturnValue->SetValue(&lConvDate, RUL_INT);
		return true;	
	}
	break;

	case BUILTIN_Time_To_Date:
	{
		RETURN_AT_NOT_NUMERIC(0);/* stevev-added check-25jun07*/
		long time_t1 = (int)pVarParameters[0];// WS - 9apr07 - 2005 checkin	
		
		long lConvDate =Time_To_Date(time_t1);
		pVarReturnValue->SetValue(&lConvDate, RUL_INT);// WS-fixed return type 25jun07
		return true;	
	}
	break;

	case BUILTIN_seconds_to_TIME_VALUE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double seconds = (double)pVarParameters[0];	
		
		unsigned long time_value = seconds_to_TIME_VALUE(seconds);
		pVarReturnValue->SetValue(&time_value, RUL_UINT);
		return true;	
	}
	break;
	case BUILTIN_seconds_to_TIME_VALUE8:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double seconds = (double)pVarParameters[0];	
		
		unsigned long long time_value = seconds_to_TIME_VALUE8(seconds);
		pVarReturnValue->SetValue(&time_value, RUL_ULONGLONG);
		return true;	
	}
	break;

	case BUILTIN_TIME_VALUE_to_seconds:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double time_value = (double)pVarParameters[0];		//maybe TIME_VALUE(4) or TIME_VALUE(8)

		double seconds = TIME_VALUE_to_seconds(time_value);
		pVarReturnValue->SetValue(&seconds, RUL_DOUBLE);
		return true;	
	}
	break;

	case BUILTIN_TIME_VALUE_to_Hour:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double time_value = (double)pVarParameters[0];	

		int hour = TIME_VALUE_to_Hour(time_value);
		pVarReturnValue->SetValue(&hour, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_TIME_VALUE_to_Minute:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double time_value = (double)pVarParameters[0];	

		int min = TIME_VALUE_to_Minute(time_value);
		pVarReturnValue->SetValue(&min, RUL_INT);
		return true;	
	}
	break;
	case BUILTIN_TIME_VALUE_to_Second:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double time_value = (double)pVarParameters[0];	

		int second = TIME_VALUE_to_Second(time_value);
		pVarReturnValue->SetValue(&second, RUL_INT);
		return true;	
	}
	break;

	case BUILTIN_DATE_AND_TIME_VALUE_to_string:
	{
		wchar_t* format = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;

		//Get the String value 
		pVarParameters[1].GetStringValue(&format);		
		//For Second string strip off the Language Code. 
		GetLanguageCode(format, szLangCode, &bLanCodePrese);
		
		RETURN_AT_NOT_NUMERIC(2);
		long date = (int)pVarParameters[2];
		RETURN_AT_NOT_NUMERIC(3);
		unsigned long time_value = (unsigned int)pVarParameters[3];
		
		
		wchar_t output_str[MAX_DD_STRING];
		int iSize = 0;
		iSize = DATE_AND_TIME_VALUE_to_string(output_str, format, date, time_value);

		//Update the output_str to user
		if ( ! SetStringParam(pFuncExp, pVarParameters, 0, output_str) )
		{
			return false;
		}
		pVarReturnValue->SetValue(&iSize, RUL_INT);

		//Delete all the Memory Allocated;
		if(format)
		{
			delete[] format;
			format= NULL;
		}
		return true;	
	}
	break;

	case BUILTIN_DATE_to_string:
	{
		wchar_t* format = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		wchar_t date_str[MAX_DD_STRING];
		int iSize = 0;

		RETURN_AT_NOT_NUMERIC(2);
		long date = (int)pVarParameters[2];
		
		//Get the String value 
		pVarParameters[1].GetStringValue(&format);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		//Remove the Language code from the first argument to the second argument
		GetLanguageCode(format, szLangCode, &bLanCodePrese);

		iSize = DATE_to_string(date_str, format, date);

		//Update the time_value_str to user
		if ( ! SetStringParam(pFuncExp, pVarParameters, 0, date_str) )
		{
			return false;
		}
		pVarReturnValue->SetValue(&iSize, RUL_INT);

		//Delete all the Memory Allocated;
		if(format)
		{
			delete[] format;
			format= NULL;
		}
		return true;	
	}
	break;

	case BUILTIN_TIME_VALUE_to_string:
	{
		wchar_t* format = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;

		//Get the String value 
		pVarParameters[1].GetStringValue(&format);		
		//For Second string strip off the Language Code. 
		GetLanguageCode(format, szLangCode, &bLanCodePrese);
		
		RETURN_AT_NOT_NUMERIC(2);
		wchar_t time_value_str[MAX_DD_STRING];
		int iSize = 0;

		if (pVarParameters[2].GetMaxSize() <= 4)
		{
			unsigned long time_value = (unsigned int)pVarParameters[2];
			iSize = TIME_VALUE_to_string(time_value_str, format, time_value);
		}
		else
		{
			unsigned long long time_value = (unsigned long long)pVarParameters[2];
			iSize = TIME_VALUE_to_string(time_value_str, format, time_value);
		}

		//Update the time_value_str to user
		if ( ! SetStringParam(pFuncExp, pVarParameters, 0, time_value_str) )
		{
			return false;
		}
		pVarReturnValue->SetValue(&iSize, RUL_INT);

		//Delete all the Memory Allocated;
		if(format)
		{
			delete[] format;
			format= NULL;
		}
		return true;	
	}
	break;

	case BUILTIN_timet_to_string:
	{
		wchar_t* format = NULL;		
		wchar_t szLangCode[5] = {0};
		bool bLanCodePrese = false;
		wchar_t time_t_str[MAX_DD_STRING];
		int iSize = 0;

		RETURN_AT_NOT_NUMERIC(2);
		long time_t = (int)pVarParameters[2];
		
		//Get the String value 
		pVarParameters[1].GetStringValue(&format);		
		
		//For Second string strip off the Language Code. Ideally SPeaking For first also we need to strip off and then Campare and then APend it banck
		//Remove the Language code from the first argument to the second argument
		GetLanguageCode(format, szLangCode, &bLanCodePrese);

		iSize = timet_to_string(time_t_str, format, time_t);

		//Update the time_value_str to user
		if ( ! SetStringParam(pFuncExp, pVarParameters, 0, time_t_str) )
		{
			return false;
		}
		pVarReturnValue->SetValue(&iSize, RUL_INT);

		//Delete all the Memory Allocated;
		if(format)
		{
			delete[] format;
			format= NULL;
		}
		return true;	
	}
	break;

	case BUILTIN_timet_to_TIME_VALUE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		long timet_value = (int)pVarParameters[0];	
		
		unsigned long time_value = timet_to_TIME_VALUE(timet_value);
		pVarReturnValue->SetValue(&time_value, RUL_UINT);
		return true;	
	}
	break;
	case BUILTIN_timet_to_TIME_VALUE8:
	{
		RETURN_AT_NOT_NUMERIC(0);
		long timet_value = (int)pVarParameters[0];	
		
		unsigned long long time_value = timet_to_TIME_VALUE8(timet_value);
		pVarReturnValue->SetValue(&time_value, RUL_ULONGLONG);
		return true;	
	}
	break;

	case BUILTIN_To_TIME_VALUE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		int hours = (int)pVarParameters[0];	
		int minutes = (int)pVarParameters[1];	
		int seconds = (int)pVarParameters[2];	
		
		unsigned long time_value = To_TIME_VALUE(hours, minutes, seconds);
		pVarReturnValue->SetValue(&time_value, RUL_UINT);
		return true;	
	}
	break;
	case BUILTIN_To_TIME_VALUE8:
	{
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);
		int year = (int)pVarParameters[0];	
		int month = (int)pVarParameters[1];	
		int day = (int)pVarParameters[2];	
		int hour = (int)pVarParameters[3];	
		int minute = (int)pVarParameters[4];	
		int second = (int)pVarParameters[5];	
		
		unsigned long long time_value = To_TIME_VALUE8(year, month, day, hour, minute, second);
		pVarReturnValue->SetValue(&time_value, RUL_ULONGLONG);
		return true;	
	}
	break;

	//stevev 29jan08 for literal strings in the methods
	case BUILTIN_literal_string:
	{	
		tchar *pchString = NULL;	
		long lItemId=0;	//WS:EPM 24may07	
		
		if (pVarParameters[0].isNumeric())
		{
			lItemId=(int)pVarParameters[0];
		}

		int iReturnValue = literal_string(lItemId, &pchString);

		if(pchString)
		{
			pVarReturnValue->SetValue(pchString, RUL_DD_STRING);
			delete[]    pchString;
			pchString = NULL;
		}
		return true;
	  }
	  break;

	  case BUILTIN_get_response_code_string:
	  {
		int  iItemId = -1;
		int  iMemberId = 0;
		int iRespCode = 0;
		int  iRespCodeLength = 0;
			
		if (pVarParameters[0].isNumeric())
		{
			iItemId = (int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		if (pVarParameters[1].isNumeric())
		{
			iMemberId = (int)pVarParameters[1];	
		}
		else
		{
			return false;
		}

		if (pVarParameters[2].isNumeric())
		{
			iRespCode = (int)pVarParameters[2];	
		}
		else
		{
			return false;
		}
		
		if (pVarParameters[4].isNumeric())
		{
			iRespCodeLength = (int)pVarParameters[4];	
		}
		else
		{
			return false;
		}
		
		int iReturnValue = get_response_code_string(iItemId, iMemberId, iRespCode, pchUIPrompt, iRespCodeLength);
		
		if ( ! SetStringParam(pFuncExp, pVarParameters, 3, pchUIPrompt) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	  }
	  break;

	  case BUILTIN_display_response_code:
	  {
		unsigned long  ulItemId = -1;
		unsigned long  ulMemberId = 0;
		int iRespCode = 0;
			
		if (pVarParameters[0].isNumeric())
		{
			ulItemId = (unsigned int)pVarParameters[0];	
		}
		else
		{
			return false;
		}

		if (pVarParameters[1].isNumeric())
		{
			ulMemberId = (unsigned int)pVarParameters[1];	
		}
		else
		{
			return false;
		}

		if (pVarParameters[2].isNumeric())
		{
			iRespCode = (int)pVarParameters[2];	
		}
		else
		{
			return false;
		}
		
		int iReturnValue = get_response_code_string(ulItemId, ulMemberId, iRespCode, pchUIPrompt, NUM_ELEM(pchUIPrompt));
		RETURN_AT_DD_ERROR(iReturnValue);

		if (iReturnValue == BLTIN_SUCCESS)
		{
			iReturnValue = ACKNOWLEDGE(pchUIPrompt);
		}

		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	break;

	  case BUILTIN_get_acknowledgement:
	  {
		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
			
		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1], pULongItemIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2], pULongMemberIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}

		RETURN_AT_NOT_NUMERIC(3);
		iNumberOfItemIds = (int)pVarParameters[3];
		
		int iReturnValue = acknowledge(pchUIPrompt, (long*)pULongItemIds, pULongMemberIds, iNumberOfItemIds);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			iReturnValue = ConvertToBLTINCode(iReturnValue);
			RETURN_AT_DD_ERROR(iReturnValue);
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	  }
	break;
	//This builtin is useed by FIELDBUS
	case BUILTIN_get_acknowledgement2:
	{
		unsigned long pULongBlkIds[100] = { 0 };
		unsigned long pULongBlkNums[100] = { 0 };
		unsigned long pULongItemIds[100] = { 0 };
		unsigned long pULongMemberIds[100] = { 0 };
		int iNumberOfItemIds = 0;

		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			return false;
		}
			
		if (pVarParameters[1].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[1], pULongBlkIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[1].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[1].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[2].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[2], pULongBlkNums, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[2].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[2].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[3].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[3], pULongItemIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[3].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[3].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}


		if (pVarParameters[4].GetVarType() == RUL_SAFEARRAY)
		{
			if( !GetULongArray(pVarParameters[4], pULongMemberIds, iNumberOfItemIds) )
			{
				return false;
			}
		}
		else if (pVarParameters[4].GetVarType() == RUL_CHAR)
		{
			__VAL paramVal = pVarParameters[4].GetValue();
			if (paramVal.cValue != 0)
			{
				return false;
			}
		}

		RETURN_AT_NOT_NUMERIC(5);
		iNumberOfItemIds = (int)pVarParameters[5];
		
		long iReturnValue = get_acknowledgement2(pchUIPrompt, pULongBlkIds, pULongBlkNums, pULongItemIds, pULongMemberIds, (long)iNumberOfItemIds);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN_get_dds_error:
	{
		tchar pchString[MAX_DD_STRING+1] = {0};
		int  iMaxLength = 0;

		if (pVarParameters[1].isNumeric())
		{
			iMaxLength = (int)pVarParameters[1];	
		}
		else
		{
			return false;
		}
		
		int iReturnValue = get_dds_error(pchString, iMaxLength);
		

		if ( ! SetStringParam(pFuncExp, pVarParameters, 0, pchString) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		return true;
	}

	case BUILTIN_display_builtin_error:
	{
		int  errorCode = 0;

		if (pVarParameters[0].isNumeric())
		{
			errorCode = (int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
		
		int iReturnValue = display_builtin_error(errorCode);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}

	case BUILTIN_display_comm_error:
	{
		unsigned int  errorCode = 0;

		if (pVarParameters[0].isNumeric())
		{
			errorCode = (unsigned int)pVarParameters[0];	
		}
		else
		{
			return false;
		}
		
		int iReturnValue = display_comm_error(errorCode);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		if (iReturnValue == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
		else
		{
			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}

	case BUILTIN_get_comm_error:
	{
		unsigned long iReturnValue = get_comm_error();
		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}

	case BUILTIN_get_comm_error_string:
	{
		if (iNumberOfParameters != 3)
		{
			return false;
		}
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(2);
		
		unsigned long lErrorCode = (unsigned int)pVarParameters[0];
		long lMaxStringLength =(int)pVarParameters[2];

		int iReturnValue = get_comm_error_string(lErrorCode, pchUIPrompt, lMaxStringLength);
		RETURN_AT_DD_ERROR(iReturnValue);

		if ( ! SetStringParam(pFuncExp, pVarParameters, 1, pchUIPrompt) )
		{
			return false;
		}
		
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}

break;

    case BUILTIN_is_NaN:
        {	
            RETURN_AT_NOT_NUMERIC(0);  
            double dValue = (double)pVarParameters[0];
            long lReturnVal = is_NaN(dValue);
            pVarReturnValue->SetValue(&lReturnVal, RUL_INT);
            return true;
        }	
        break;

    case BUILTIN_get_double:
    {
		//used by builtins get_double() and get_float()
        double dValue;
        long lReturnValue = get_double(&dValue);
		RETURN_AT_DD_ERROR(lReturnValue);
        pVarParameters[0] = dValue;
        OutputParameterValue( pFuncExp, 0, pVarParameters[0], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;

    case BUILTIN_get_string:
    {
		RETURN_AT_NOT_NUMERIC(1);
        long lLength = (int)pVarParameters[1];

		int iRetVal = get_action_string(pFuncExp, lLength, &(pVarParameters[0]));

		if (iRetVal == BLTIN_WRONG_DATA_TYPE)
		{
			return false;	//wrong parameter type
		}

		RETURN_AT_DD_ERROR(iRetVal);

		//update string length
		pVarParameters[1] = (int)pVarParameters[0].GetMaxSize();
        OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);

		long lReturnValue = ConvertToBLTINCode(iRetVal);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;
    case BUILTIN_get_date:
    {
        unsigned char pchString[8]={0};
        long lReturnValue=0;

		RETURN_AT_NOT_NUMERIC(1);
        long lLength = (long)(int)pVarParameters[1];

		lReturnValue = get_date(pchString, &lLength);
		RETURN_AT_DD_ERROR(lReturnValue);
		
		_BYTE_STRING Bss;
		Bss.bs = pchString;
		Bss.bsLen = lLength;

        if ( ! SetByteStringParam(pFuncExp, pVarParameters, 0, Bss) )
        {
			return false;
        }

		pVarParameters[1] = lLength;
        OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;

    case BUILTIN_get_signed:
    {
        long lval;
        long lReturnValue = get_signed(&lval);
		RETURN_AT_DD_ERROR(lReturnValue);
        pVarParameters[0] = lval;
        OutputParameterValue( pFuncExp, 0, pVarParameters[0], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;

    case BUILTIN_get_unsigned:
    {
        unsigned long ulval;
        long lReturnValue = get_unsigned(&ulval);
		RETURN_AT_DD_ERROR(lReturnValue);
        pVarParameters[0] = (unsigned int)ulval;
        OutputParameterValue( pFuncExp, 0, pVarParameters[0], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
		
	case BUILTIN_get_date_value:
	{	
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
		long lDateSize;	//shall be <= 8 per FDI spec?
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(3);
		ulItemId = (unsigned int)pVarParameters[0];	
		ulMemberId = (unsigned int)pVarParameters[1];
		lDateSize = (int)pVarParameters[3];

		unsigned char pchDate[8] = {0};

		long lReturnValue = get_date_value2(0, 0, ulItemId, ulMemberId, pchDate, &lDateSize);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{

		    _BYTE_STRING Bss;
		    Bss.bs = pchDate;
		    Bss.bsLen = lDateSize + 1;

		    if ( ! SetByteStringParam(pFuncExp, pVarParameters, 2, Bss) )
			{
			return false;
			}
			pVarParameters[3] = (int)lDateSize;
			OutputParameterValue( pFuncExp, 3, pVarParameters[3], 0);

			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN_get_double_value:
	{	
		//used by builtins get_double_value() and get_float_value()
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		unsigned long ulItemId = (unsigned int)pVarParameters[0];	
		unsigned long ulMemberId = (unsigned int)pVarParameters[1];

		double dValue = 0;
		CValueVarient vValue{};

		long lReturnValue = AccessTypeValue2(0, 0, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			if (vValue.vTagType == ValueTagType::CVT_R4)
			{
				dValue = (double)vValue.vValue.fFloatConst;
			}
			else if (vValue.vTagType == ValueTagType::CVT_R8)
			{
				dValue = vValue.vValue.fFloatConst;
			}
			else
			{
				return false;
			}

			pVarParameters[2] = (double)dValue;
			OutputParameterValue( pFuncExp, 2, pVarParameters[2], 0);
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_signed_value:
	{	
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		ulItemId = (unsigned int)pVarParameters[0];	
		ulMemberId = (unsigned int)pVarParameters[1];

		long lValue = 0;
		long lReturnValue;

		lReturnValue = get_signed_value(ulItemId, ulMemberId, &lValue);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarParameters[2] = (int)lValue;
			OutputParameterValue( pFuncExp, 2, pVarParameters[2], 0);

			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN_get_unsigned_value:
	{	
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		ulItemId = (unsigned int)pVarParameters[0];	
		ulMemberId = (unsigned int)pVarParameters[1];

		unsigned long ulValue = 0;
		long lReturnValue;

		lReturnValue = get_unsigned_value(ulItemId, ulMemberId, &ulValue);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarParameters[2] = (unsigned int)ulValue;
			OutputParameterValue( pFuncExp, 2, pVarParameters[2], 0);

			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}
	break;

    case BUILTIN_get_string_value:
    {
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
        long lLength = 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(3);

		ulItemId = (unsigned int)pVarParameters[0];	
		ulMemberId = (unsigned int)pVarParameters[1];
		lLength = (int)pVarParameters[3];

		//check if variable is BITSTRING
        long lReturnValue = BLTIN_NOT_IMPLEMENTED;
		if (m_pDevice)
		{
			//get variable type and verify item ID
			CValueVarient vtType{};
			CStdString sProperty;

			sProperty.Format(DCI_PROPERTY_TYPE);
			ulItemId = VerifyItemIdNGetProperty(0, 0, ulItemId, ulMemberId, sProperty, &vtType);
			

			if ((ulItemId != 0) && (CV_I4(&vtType) > vT_undefined))
			{
				switch ((variableType_t)CV_I4(&vtType))
				{
				case vT_BitString:
				case nsEDDEngine::VT_OCTETSTRING:
					{
						BYTE_STRING( bitStr, lLength );

						lReturnValue = get_bitstring_value2(0, 0, ulItemId, ulMemberId, &bitStr);

						if (lReturnValue == BI_ABORT)
						{
							*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
						}
						else if (lReturnValue == BLTIN_SUCCESS)
						{
							if ( ! SetByteStringParam(pFuncExp, pVarParameters, 2, bitStr) )
							{
								DEL_BYTE_STR( bitStr );
								return false;
							}
						}

						DEL_BYTE_STR( bitStr );
					}
					break;
				case nsEDDEngine::VT_ASCII:
				case nsEDDEngine::VT_PACKED_ASCII:
				case nsEDDEngine::VT_PASSWORD:
				case nsEDDEngine::VT_EUC:
				case nsEDDEngine::VT_VISIBLESTRING:
					{
						wchar_t pchString[MAX_DD_STRING]={0};

						lLength = MAX_DD_STRING;
						lReturnValue = get_string_value2(0, 0, ulItemId, ulMemberId, pchString, &lLength);

						if (lReturnValue == BI_ABORT)
						{
							*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
						}
						else if (lReturnValue == BLTIN_SUCCESS)
						{
							if ( ! SetStringParam(pFuncExp, pVarParameters, 2, pchString) )
							{
								return false;
							}
						}
					}
					break;
				default:
					lReturnValue = BLTIN_DDS_ERROR;
					break;
				}

				RETURN_AT_DD_ERROR(lReturnValue);

				//update string length
				pVarParameters[3] = (int)pVarParameters[2].GetMaxSize();
				OutputParameterValue( pFuncExp, 3, pVarParameters[3], 0);
			}
			else
			{
				lReturnValue = BLTIN_WRONG_DATA_TYPE;
			}
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		
		return true;
    }
    break;

    case BUILTIN_assign:
    {
		long lReturnValue;
		unsigned long uldst_ItemId=0;
		unsigned long uldst_MemberId=0;
		unsigned long ulsrc_ItemId=0;
		unsigned long ulsrc_MemberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		uldst_ItemId = (unsigned int)pVarParameters[0];	
		uldst_MemberId = (unsigned int)pVarParameters[1];
		ulsrc_ItemId = (unsigned int)pVarParameters[2];	
		ulsrc_MemberId = (unsigned int)pVarParameters[3];

        lReturnValue = assign(0, 0, uldst_ItemId, uldst_MemberId, 0, 0, ulsrc_ItemId, ulsrc_MemberId);
		RETURN_AT_DD_ERROR(lReturnValue);

        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;


	case BUILTIN_get_response_code:
	{
		
		unsigned long ulResponseCode;
		unsigned long ulErrorItemId;
		unsigned long ulErrorMemberId;

		
		long iReturnValue = get_response_code(&ulResponseCode, &ulErrorItemId, &ulErrorMemberId);		
		RETURN_AT_DD_ERROR(iReturnValue);
		
		pVarParameters[0] = (unsigned int)ulResponseCode;
		OutputParameterValue( pFuncExp, 0, pVarParameters[0], 0); 
		pVarParameters[1] = (unsigned int)ulErrorItemId;
		OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);
		pVarParameters[2] = (unsigned int)ulErrorMemberId;
		OutputParameterValue( pFuncExp, 2, pVarParameters[2], 0);
		
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_read_value:
	{
		unsigned long ulItemId;
		unsigned long ulMemberId;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		
		ulItemId = (unsigned int)pVarParameters[0];
		ulMemberId = (unsigned int)pVarParameters[1];

		long lReturnValue = AccessDeviceValue(0, 0, ulItemId, ulMemberId, READ);
		RETURN_AT_DD_ERROR(lReturnValue);

		RETURN_AT_COMM_ERROR(lReturnValue);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);	

		return true;
	}
	break;

	case BUILTIN_abort_on_comm_error:
	{
		unsigned long ulCommErrorCode;

		RETURN_AT_NOT_NUMERIC(0);
		
		ulCommErrorCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_comm_status(ulCommErrorCode, __ABORT__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_retry_on_comm_error:
	{
		unsigned long ulCommErrorCode;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulCommErrorCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_comm_status(ulCommErrorCode, __RETRY__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_abort_on_response_code:
	{
		unsigned long ulResponseCode;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulResponseCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_resp_code(ulResponseCode, __ABORT__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_retry_on_response_code:
	{
		unsigned long ulResponseCode;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulResponseCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_resp_code(ulResponseCode, __RETRY__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_fail_on_response_code:
	{
		unsigned long ulResponseCode;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulResponseCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_resp_code(ulResponseCode, __IGNORE__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_fail_on_comm_error:
	{
		unsigned long ulCommErrorCode;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulCommErrorCode = (unsigned int)pVarParameters[0];

		long lReturnValue = set_ff_comm_status(ulCommErrorCode, __IGNORE__);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_abort_on_all_comm_errors:
	{
		(void)set_ff_all_comm_status(__ABORT__);

		return true;
		
	}
	break;

	case BUILTIN_abort_on_all_response_codes:
	{
		(void)set_ff_all_resp_code(__ABORT__);
		
		return true;
	}
	break;

	case BUILTIN_fail_on_all_comm_errors:
	{
		(void)set_ff_all_comm_status(__IGNORE__);
		
		return true;
	}
	break;

	case BUILTIN_fail_on_all_response_codes:
	{
		(void)set_ff_all_resp_code(__IGNORE__);
		
		return true;
	}
	break;

	case BUILTIN_retry_on_all_comm_errors:
	{
		(void)set_ff_all_comm_status(__RETRY__);
		
		return true;
	}
	break;

	case BUILTIN_retry_on_all_response_codes:
	{
		(void)set_ff_all_resp_code(__RETRY__);
		
		return true;
	}
	break;

    case BUILTIN_put_double_value:
    {          
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);

        unsigned long ulItemId = (unsigned int)pVarParameters[0];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[1];
        double dValue = (double)pVarParameters[2];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_R8);
		CV_R8(&vValue) = dValue;
        long lReturnValue = AccessTypeValue2(0, 0, ulItemId, ulmemberId, &vValue, WRITE);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

    case BUILTIN_put_float_value:
    {          
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);

        unsigned long ulItemId = (unsigned int)pVarParameters[0];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[1];
        float fValue = (float)pVarParameters[2];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_R4);
		CV_R4(&vValue) = fValue;
        long lReturnValue = AccessTypeValue2(0, 0, ulItemId, ulmemberId, &vValue, WRITE);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

    case BUILTIN_put_string_value:
    {
        unsigned long ulItemId=0;
        unsigned long ulmemberId=0;
        long len=0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(3);

        ulItemId = (int)pVarParameters[0];
        ulmemberId = (int)pVarParameters[1];
        len = (int)pVarParameters[3];

		//check if variable is BITSTRING
        long lReturnValue = BLTIN_NOT_IMPLEMENTED;
		if (m_pDevice)
		{
			//get variable type and verify item ID
			CValueVarient vtType{};
			CStdString sProperty;

			sProperty.Format(DCI_PROPERTY_TYPE);
			ulItemId = VerifyItemIdNGetProperty(0, 0, ulItemId, ulmemberId, sProperty, &vtType);
			

			if ((ulItemId != 0) && (CV_I4(&vtType) > vT_undefined))
			{

				switch ((variableType_t)CV_I4(&vtType))
				{
				case vT_BitString: // unsigned char[]
				case nsEDDEngine::VT_OCTETSTRING:
					{
						unsigned char pchInputString[MAX_DD_STRING]={0};

						if ( ! GetUnsignedCharArrayParam(pchInputString, len, pVarParameters, 2) )
						{
							return false;
						}

						BYTE_STRING( bitStr, len );

						memcpy(bitStr.bs, pchInputString, (size_t)len);
						lReturnValue = put_bitstring_value2( 0, 0, ulItemId, ulmemberId, &bitStr );	//do operation and save return value.

						DEL_BYTE_STR( bitStr );
					}
					break;
				case nsEDDEngine::VT_ASCII:
				case nsEDDEngine::VT_PACKED_ASCII:
				case nsEDDEngine::VT_PASSWORD:
				case nsEDDEngine::VT_EUC:
				case nsEDDEngine::VT_VISIBLESTRING:
					{
						wchar_t pchInputString[MAX_DD_STRING]={0};
						len = MAX_DD_STRING;

						if ( ! GetStringParam(pchInputString, len, pVarParameters, 2) )
						{
							return false;
						}

						lReturnValue = put_string_value2( 0, 0, ulItemId, ulmemberId, pchInputString );	//do operation and save return value.
					}
					break;
				default:
					lReturnValue = BLTIN_DDS_ERROR;
					break;
				}

				RETURN_AT_DD_ERROR(lReturnValue);
				if (lReturnValue == BI_ABORT)
				{
					*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
				}
			}
			else
			{
				lReturnValue = BLTIN_WRONG_DATA_TYPE;
			}
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);//set return value
		
		return true;
    }
    break;

    case BUILTIN_put_signed_value:
    {          
        long lValue;
        unsigned long ulItemId=0;
        unsigned long ulmemberId=0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);

        ulItemId = (unsigned int)pVarParameters[0];	
        ulmemberId = (unsigned int)pVarParameters[1];
        lValue = (int)pVarParameters[2];

        long lReturnValue = put_signed_value(ulItemId, ulmemberId, lValue);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

    case BUILTIN_put_unsigned_value:
    {
        unsigned long ulValue;
        unsigned long ulItemId=0;
        unsigned long ulmemberId=0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);

        ulItemId = (unsigned int)pVarParameters[0];	
        ulmemberId = (unsigned int)pVarParameters[1];
        ulValue = (unsigned int)pVarParameters[2];

        long lReturnValue = put_unsigned_value(ulItemId, ulmemberId, ulValue);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

	case BUILTIN_get_double_lelem:
		{
			unsigned long ulListId = 0;
			unsigned long ulIndx = 0;

            unsigned long ulelmtId = 0;
            unsigned long ulSubelmtId = 0;

            RETURN_AT_NOT_NUMERIC(0);
            RETURN_AT_NOT_NUMERIC(1);
            RETURN_AT_NOT_NUMERIC(2);
            RETURN_AT_NOT_NUMERIC(3);

            //Get the List Id
            ulListId = (unsigned int)pVarParameters[0];
            ulIndx = (unsigned int)pVarParameters[1];
            ulelmtId = (unsigned int)pVarParameters[2];
            ulSubelmtId = (unsigned int)pVarParameters[3];

            double dReturnValue = get_double_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId);
			pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);

			return true;
		}
		break;

	case BUILTIN_get_float_lelem:
	{
		unsigned long ulListId = 0;
		unsigned long ulIndx = 0;

        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];
        ulIndx = (unsigned int)pVarParameters[1];
        ulelmtId = (unsigned int)pVarParameters[2];
        ulSubelmtId = (unsigned int)pVarParameters[3];

        float fReturnValue = get_float_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId);
		pVarReturnValue->SetValue(&fReturnValue, RUL_FLOAT);

		return true;
	}
	break;

	case BUILTIN_get_signed_lelem:
	{
		unsigned long ulListId = 0;
		unsigned long ulIndx = 0;

        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);

        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];
        ulIndx = (unsigned int)pVarParameters[1];           
        ulelmtId = (unsigned int)pVarParameters[2];
        ulSubelmtId = (unsigned int)pVarParameters[3];

        long lReturnValue = get_signed_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId);
		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_unsigned_lelem:
	{
		unsigned long ulListId = 0;
		unsigned long ulIndx = 0;

        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);

        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];
        ulIndx = (unsigned int)pVarParameters[1];
        ulelmtId = (unsigned int)pVarParameters[2];
        ulSubelmtId = (unsigned int)pVarParameters[3];

        unsigned long ulReturnValue = get_unsigned_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId);
        pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_get_string_lelem:
	{
		unsigned long ulListId = 0;
		unsigned long ulIndx = 0;

        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;
        wchar_t pchString[MAX_DD_STRING]={0};
        int length = 0;

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];

        //Get the Index of the element
        ulIndx = (unsigned int)pVarParameters[1];

        ulelmtId = (unsigned int)pVarParameters[2];
        ulSubelmtId = (unsigned int)pVarParameters[3];

        // Get the length
        length = (int)pVarParameters[5];

        long lReturnValue = get_string_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId, pchString, &length);
		if ( ! SetStringParam(pFuncExp, pVarParameters, 4, pchString) )
		{
			return false;
		}

		//update string length
		pVarParameters[5] = (int)pVarParameters[4].GetMaxSize();
        OutputParameterValue( pFuncExp, 5, pVarParameters[5], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

    case BUILTIN_get_date_lelem:
    {
        unsigned long ulListId = 0;
		unsigned long ulIndx = 0;

        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;
        int length = 0;
        unsigned char pchDate[MAX_DD_STRING]={0};

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);

        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];

        //Get the Index of the element
        ulIndx = (unsigned int)pVarParameters[1];

        ulelmtId = (unsigned int)pVarParameters[2];
        ulSubelmtId = (unsigned int)pVarParameters[3];

        // Get the length
        length = (int)pVarParameters[5];

        long lReturnValue = get_date_lelem(ulListId, ulIndx, ulelmtId, ulSubelmtId, pchDate, &length);
		
		_BYTE_STRING Bss;
		Bss.bs = pchDate;
		Bss.bsLen = length + 1;

        if ( ! SetByteStringParam(pFuncExp, pVarParameters, 4,Bss) )
        {
			return false;
        }
        pVarParameters[5] = length;
        OutputParameterValue( pFuncExp, 5, pVarParameters[5], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;

	case BUILTIN_resolve_list_ref:
	{
		unsigned long ulItemId = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulItemId = (unsigned int)pVarParameters[0];	
			
		unsigned long ulReturnValue = resolve_list_ref(ulItemId);
		RETURN_AT_DD_ERROR(ulResolveStatus);
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_FF_resolve_param_ref:
	{
		unsigned long ulMemberId = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulMemberId = (unsigned int)pVarParameters[0];	
			
		unsigned long ulReturnValue = resolve_block_ref(0, 0, ulMemberId, RESOLVE_PARAM_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_resolve_param_list_ref:
	{
		unsigned long ulMemberId = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulMemberId = (unsigned int)pVarParameters[0];	
			
		unsigned long ulReturnValue = resolve_block_ref(0, 0, ulMemberId, RESOLVE_PARAM_LIST_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);

		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);
		
		return true;
	}
	break;

	case BUILTIN_resolve_block_ref:
	{
		unsigned long ulMemberId = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		
		ulMemberId = (unsigned int)pVarParameters[0];	
			
		unsigned long ulReturnValue = resolve_block_ref(0, 0, ulMemberId, RESOLVE_BLOCK_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_put_date_value:
	{          
		unsigned long ulItemId = 0;
		unsigned long ulmemberId = 0;
		unsigned char pchDate[MAX_DD_STRING]={0};
		long lSize = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(3);

		ulItemId = (unsigned int)pVarParameters[0];	
		ulmemberId = (unsigned int)pVarParameters[1];
		lSize = (int)pVarParameters[3];
		if ( ! GetUnsignedCharArrayParam(pchDate, lSize, pVarParameters, 2) )
        {
			return false;
        }
		
		long lReturnValue =  put_date_value(ulItemId, ulmemberId, pchDate, lSize);
		
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}	
    break;

	case BUILTIN_get_resolve_status:
	{
		unsigned long ulReturnValue = get_resolve_status();
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_get_status_string:
	{
		unsigned long ulItemId = 0;
		unsigned long ulMemberId = 0;
		unsigned long ulStatusCode = 0;

		tchar pchString[MAX_DD_STRING+1] = {0};
		long  lLength = 0;
			
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(4);
		
		ulItemId = (unsigned int)pVarParameters[0];	
		ulMemberId = (unsigned int)pVarParameters[1];
		ulStatusCode = (unsigned int)pVarParameters[2];	
		lLength = (unsigned int)pVarParameters[4];	


		long lReturnValue = get_status_string(ulItemId, ulMemberId, ulStatusCode, pchString,lLength);
		RETURN_AT_DD_ERROR(lReturnValue);
		
		if ( ! SetStringParam(pFuncExp, pVarParameters, 3, pchString) )
		{
			return false;
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_resolve_local_ref2:
	{	
		unsigned long ulBlockItemId	= 0;
		unsigned long ulOccurrence	= 0;
		unsigned long ulMemberId	= 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		
		ulBlockItemId = (unsigned int)pVarParameters[0];	
		ulOccurrence  = (unsigned int)pVarParameters[1];	
		ulMemberId	  = (unsigned int)pVarParameters[2];	
			
		unsigned long ulReturnValue = resolve_block_ref(ulBlockItemId, ulOccurrence, ulMemberId, RESOLVE_LOCAL_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_resolve_param_ref2:
	{
		unsigned long ulBlockItemId = 0;
		unsigned long ulOccurrence	= 0;
		unsigned long ulMemberId    = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		
		ulBlockItemId = (unsigned int)pVarParameters[0];	
		ulOccurrence  = (unsigned int)pVarParameters[1];	
		ulMemberId    = (unsigned int)pVarParameters[2];	
			
		unsigned long ulReturnValue = resolve_block_ref(ulBlockItemId, ulOccurrence, ulMemberId, RESOLVE_PARAM_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	case BUILTIN_send_value2:
	{
		unsigned long ulBlockItemId	= 0;
		unsigned long ulOccurrence	= 0;
		unsigned long ulItemId		= 0;
		unsigned long ulMemberId	= 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);

		ulBlockItemId = (unsigned int)pVarParameters[0];	
		ulOccurrence = (unsigned int)pVarParameters[1];	
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];

		long lReturnValue = AccessDeviceValue(ulBlockItemId, ulOccurrence, ulItemId, ulMemberId, WRITE);
		RETURN_AT_DD_ERROR(lReturnValue);

		RETURN_AT_COMM_ERROR(lReturnValue);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);	

		return true;
	}
	break;

	case BUILTIN_read_value2:
	{
		unsigned long ulBlockItemId	= 0;
		unsigned long ulOccurrence	= 0;
		unsigned long ulItemId = 0;
		unsigned long ulMemberId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		
		ulBlockItemId = (unsigned int)pVarParameters[0];	
		ulOccurrence = (unsigned int)pVarParameters[1];	
		ulItemId = (unsigned int)pVarParameters[2];
		ulMemberId = (unsigned int)pVarParameters[3];

		long lReturnValue = AccessDeviceValue(ulBlockItemId, ulOccurrence, ulItemId, ulMemberId, READ);
		RETURN_AT_DD_ERROR(lReturnValue);

		RETURN_AT_COMM_ERROR(lReturnValue);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);	

		return true;
	}
	break;

	case BUILTIN_assign2:
    {
		long lReturnValue;
		unsigned long uldst_BlockItemId	= 0;
		unsigned long uldst_Occurrence	= 0;
		unsigned long uldst_ItemId = 0;
		unsigned long uldst_MemberId = 0;
		unsigned long ulsrc_BlockItemId	= 0;
		unsigned long ulsrc_Occurrence	= 0;
		unsigned long ulsrc_ItemId = 0;
		unsigned long ulsrc_MemberId = 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);
		RETURN_AT_NOT_NUMERIC(6);
		RETURN_AT_NOT_NUMERIC(7);

		uldst_BlockItemId	= (unsigned int)pVarParameters[0];	
		uldst_Occurrence	= (unsigned int)pVarParameters[1];	
		uldst_ItemId	= (unsigned int)pVarParameters[2];	
		uldst_MemberId	= (unsigned int)pVarParameters[3];
		
		ulsrc_BlockItemId	= (unsigned int)pVarParameters[4];	
		ulsrc_Occurrence	= (unsigned int)pVarParameters[5];	
		ulsrc_ItemId	= (unsigned int)pVarParameters[6];	
		ulsrc_MemberId	= (unsigned int)pVarParameters[7];

        lReturnValue = assign(uldst_BlockItemId, uldst_Occurrence, uldst_ItemId, uldst_MemberId, ulsrc_BlockItemId, ulsrc_Occurrence, ulsrc_ItemId, ulsrc_MemberId);
		RETURN_AT_DD_ERROR(lReturnValue);

        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
        
		return true;
    }
    break;

	//This builtin is for FF only
	case BUILTIN_get_stddict_string:	//similar to BUILTIN__get_dictionary_string
	{
		if (iNumberOfParameters != 3)
		{
			return false;
		}

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(2);

		unsigned long ulDictId = (unsigned int)pVarParameters[0];
		//Do not truncate the string so that the memory size is ignored here

		int iReturnValue = _get_dictionary_string(ulDictId, pchUIPrompt, NUM_ELEM(pchUIPrompt));

		if (!SetStringParam(pFuncExp, pVarParameters, 1, pchUIPrompt, true))
		{
			return false;
		}
		
		iReturnValue = ConvertToBLTINCode(iReturnValue);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;

	//This builtin is for FF only
	case BUILTIN_get_date_value2:
	{
		unsigned long ulBlkId=0;
		unsigned long ulBlkNum=0;
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
		long lDateSize;	//shall be <= 8 per FDI spec?
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(5);
		ulBlkId = (unsigned int)pVarParameters[0];	
		ulBlkNum = (unsigned int)pVarParameters[1];
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];
		lDateSize = (int)pVarParameters[5];
		if (lDateSize > 8)
		{
			lDateSize = 8;
		}

		unsigned char pchDate[8] = {0};

		long lReturnValue = get_date_value2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, pchDate, &lDateSize);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{

			_BYTE_STRING Bss;
			Bss.bs = pchDate;
			Bss.bsLen = lDateSize + 1;

			if ( ! SetByteStringParam(pFuncExp, pVarParameters, 4,Bss) )
			{
				return false;
			}
			pVarParameters[5] = (int)lDateSize;
			OutputParameterValue( pFuncExp, 5, pVarParameters[5], 0);

			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN_get_double_value2:
	{	
		//used by builtins get_double_value2() and get_float_value2()
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
		unsigned long ulItemId = (unsigned int)pVarParameters[2];	
		unsigned long ulMemberId = (unsigned int)pVarParameters[3];

		double dValue = 0;
		CValueVarient vValue{};

		long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			if (vValue.vTagType == ValueTagType::CVT_R4)
			{
				dValue = (double) CV_R8(&vValue);
			}
			else if (vValue.vTagType == ValueTagType::CVT_R8)
			{
				dValue = CV_R8(&vValue);
			}
			else
			{
				return false;
			}

			pVarParameters[4] = dValue;
			OutputParameterValue( pFuncExp, 4, pVarParameters[4], 0);
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_signed_value2:
	{	
		unsigned long ulBlkId=0;
		unsigned long ulBlkNum=0;
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		ulBlkId = (unsigned int)pVarParameters[0];	
		ulBlkNum = (unsigned int)pVarParameters[1];
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];

		long lValue = 0;
		long lReturnValue;
		CValueVarient vValue{};

		lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			
			lValue = CV_I4(&vValue);

			pVarParameters[4] = (int)lValue;
			OutputParameterValue( pFuncExp, 4, pVarParameters[4], 0);
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_unsigned_value2:
	{	
		unsigned long ulBlkId=0;
		unsigned long ulBlkNum=0;
		unsigned long ulItemId=0;
		unsigned long ulMemberId=0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		ulBlkId = (unsigned int)pVarParameters[0];	
		ulBlkNum = (unsigned int)pVarParameters[1];
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];

		unsigned long ulValue = 0;
		long lReturnValue;
		CValueVarient vValue{};

		lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			
			ulValue = CV_UI4(&vValue);

			pVarParameters[4] = (unsigned int)ulValue;
			OutputParameterValue( pFuncExp, 4, pVarParameters[4], 0);
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

    case BUILTIN_get_string_value2:
    {
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(5);
		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
		unsigned long ulItemId = (unsigned int)pVarParameters[2];	
		unsigned long ulMemberId = (unsigned int)pVarParameters[3];
		long lLength = (int)pVarParameters[5];

        long lReturnValue = BLTIN_NOT_IMPLEMENTED;

		if (m_pDevice)
		{
			//get variable type and verify item ID
			CValueVarient vtType{};
			CStdString sProperty;

			sProperty.Format(DCI_PROPERTY_TYPE);
			ulItemId = VerifyItemIdNGetProperty(ulBlkId, ulBlkNum, ulItemId, ulMemberId, sProperty, &vtType);
			

			if ((ulItemId != 0) && (CV_I4(&vtType) > vT_undefined))
			{

				switch ((variableType_t)CV_I4(&vtType))
				{
				case vT_BitString:
				case nsEDDEngine::VT_OCTETSTRING:
					{
						BYTE_STRING( bitStr, lLength );

						lReturnValue = get_bitstring_value2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &bitStr);

						if (lReturnValue == BI_ABORT)
						{
							*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
						}
						else if (lReturnValue == BLTIN_SUCCESS)
						{
							if ( ! SetByteStringParam(pFuncExp, pVarParameters, 4, bitStr) )
							{
								DEL_BYTE_STR( bitStr );
								return false;
							}
						}

						DEL_BYTE_STR( bitStr );
					}
					break;
				case nsEDDEngine::VT_ASCII:
				case nsEDDEngine::VT_PACKED_ASCII:
				case nsEDDEngine::VT_PASSWORD:
				case nsEDDEngine::VT_EUC:
				case nsEDDEngine::VT_VISIBLESTRING:
					{
						wchar_t pchString[MAX_DD_STRING]={0};

						lLength = MAX_DD_STRING;
						lReturnValue = get_string_value2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, pchString, &lLength);

						if (lReturnValue == BI_ABORT)
						{
							*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
						}
						else if (lReturnValue == BLTIN_SUCCESS)
						{
							if ( ! SetStringParam(pFuncExp, pVarParameters, 4, pchString) )
							{
								return false;
							}
						}
					}
					break;
				default:
					lReturnValue = BLTIN_DDS_ERROR;
					break;
				}

				RETURN_AT_DD_ERROR(lReturnValue);

				//update string length
				pVarParameters[5] = (int)pVarParameters[4].GetMaxSize();
				OutputParameterValue( pFuncExp, 5, pVarParameters[5], 0);
			}
			else
			{
				lReturnValue = BLTIN_WRONG_DATA_TYPE;
			}
		}

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
    }
    break;

 	case BUILTIN_put_date_value2:
	{          
		unsigned char pchDate[MAX_DD_STRING] = {0};
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(5);

		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
		unsigned long ulItemId = (unsigned int)pVarParameters[2];	
		unsigned long ulmemberId = (unsigned int)pVarParameters[3];
		long lSize = (int)pVarParameters[5];
		if (lSize > 8)
		{
			lSize = 8;
		}
		if ( ! GetUnsignedCharArrayParam(pchDate, lSize, pVarParameters, 4) )
        {
			return false;
        }
		pchDate[lSize] = L'\0';

		//convert date value to a integer variable
		unsigned long long ullDate = 0;
		convert8CharToUll ((unsigned char *)pchDate, lSize, ullDate);

		//get date variable type
		CValueVarient vtValue{};
		CV_VT(&vtValue, ValueTagType::CVT_UI8);
		CV_UI8(&vtValue) = ullDate;
		long lReturnValue = put_variable_value(ulBlkId, ulBlkNum, ulItemId, ulmemberId, &vtValue);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}	
    break;

    case BUILTIN_put_double_value2:
    {          
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
 		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);

 		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
        unsigned long ulItemId = (unsigned int)pVarParameters[2];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[3];
        double dValue = (double)pVarParameters[4];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_R8);
		CV_R8(&vValue) = dValue;
        long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulmemberId, &vValue, WRITE);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

	case BUILTIN_put_float_value2:
	{	
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
 		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);

 		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
        unsigned long ulItemId = (unsigned int)pVarParameters[2];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[3];
        float fValue = (float)pVarParameters[4];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_R4);
		CV_R4(&vValue) = fValue;
        long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulmemberId, &vValue, WRITE);
		RETURN_AT_DD_ERROR(lReturnValue);

		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
	}
	break;

    case BUILTIN_put_signed_value2:
    {          
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
        RETURN_AT_NOT_NUMERIC(4);

  		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
        unsigned long ulItemId = (unsigned int)pVarParameters[2];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[3];
        long lValue = (int)pVarParameters[4];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_I4);
		CV_I4(&vValue) = lValue;
        long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulmemberId, &vValue, WRITE);
		RETURN_AT_DD_ERROR(lReturnValue);

		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

    case BUILTIN_put_unsigned_value2:
    {          
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
        RETURN_AT_NOT_NUMERIC(4);

  		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
        unsigned long ulItemId = (unsigned int)pVarParameters[2];	
        unsigned long ulmemberId = (unsigned int)pVarParameters[3];
        unsigned long ulValue = (unsigned int)pVarParameters[4];

		CValueVarient vValue{};

		CV_VT(&vValue, ValueTagType::CVT_UI4);
		CV_UI4(&vValue) = ulValue;
        long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulmemberId, &vValue, WRITE);
		RETURN_AT_DD_ERROR(lReturnValue);

		if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		}

		return true;
    }
    break;

    case BUILTIN_put_string_value2:
    {
		RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
 		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
        RETURN_AT_NOT_NUMERIC(5);

 		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
        unsigned long ulItemId = (int)pVarParameters[2];
        unsigned long ulmemberId = (int)pVarParameters[3];
        long len = (int)pVarParameters[5];
           
		//check if variable is BITSTRING
        int iReturnValue = BLTIN_NOT_IMPLEMENTED;
		if (m_pDevice)
		{
			//get variable type and verify item ID
			CValueVarient vtType{};
			CStdString sProperty;

			sProperty.Format(DCI_PROPERTY_TYPE);
			ulItemId = VerifyItemIdNGetProperty(ulBlkId, ulBlkNum, ulItemId, ulmemberId, sProperty, &vtType);
			

			if ((ulItemId != 0) && (CV_I4(&vtType) > vT_undefined))
			{

				switch ((variableType_t)CV_I4(&vtType))
				{
				case vT_BitString: // unsigned char[]
				case nsEDDEngine::VT_OCTETSTRING:
					{
						unsigned char pchInputString[MAX_DD_STRING]={0};

						if ( ! GetUnsignedCharArrayParam(pchInputString, len, pVarParameters, 4) )
						{
							return false;
						}

						BYTE_STRING( bitStr, len );

						memcpy(bitStr.bs, pchInputString, (size_t)len);
						iReturnValue = put_bitstring_value2( ulBlkId, ulBlkNum, ulItemId, ulmemberId, &bitStr );	//do operation and save return value.
					
						DEL_BYTE_STR( bitStr );
					}
					break;
				case nsEDDEngine::VT_ASCII:
				case nsEDDEngine::VT_PACKED_ASCII:
				case nsEDDEngine::VT_PASSWORD:
				case nsEDDEngine::VT_EUC:
				case nsEDDEngine::VT_VISIBLESTRING:
					{
						wchar_t pwchString[MAX_DD_STRING]={0};
						len = MAX_DD_STRING;
						
						if ( ! GetStringParam(pwchString, len, pVarParameters, 4) )
						{
							return false;
						}

						iReturnValue = put_string_value2( ulBlkId, ulBlkNum, ulItemId, ulmemberId, pwchString );	//do operation and save return value.
					}
					break;
				default:
					iReturnValue = BLTIN_DDS_ERROR;
					break;
				}

				RETURN_AT_DD_ERROR(iReturnValue);
				if (iReturnValue == BI_ABORT)
				{
					//variable is invalid
					*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
				}
			}
			else
			{
				iReturnValue = BLTIN_WRONG_DATA_TYPE;
			}
		}

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);//set return value

		return true;
    }
    break;

	case BUILTIN_get_block_instance_count: 
	{
		unsigned long ulBlockId = 0;
		unsigned long ulCount = 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		ulBlockId = (unsigned int)pVarParameters[0];

		long iReturnValue = get_block_instance_count(ulBlockId, &ulCount);
		RETURN_AT_DD_ERROR(iReturnValue);
	
		pVarParameters[1] = (unsigned int)ulCount;
        OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);
		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_block_instance_by_object_index: 
	{
	
		unsigned long ulObjectIndex = 0;
		unsigned long ulRelativeIndex = 0;

		RETURN_AT_NOT_NUMERIC(0);
		ulObjectIndex = (unsigned int)pVarParameters[0];

		long iReturnValue = get_block_instance_by_object_index(ulObjectIndex, &ulRelativeIndex);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		pVarParameters[1] = (unsigned int)ulRelativeIndex;
        OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_get_block_instance_by_tag:
	{
		unsigned long ulBlockId = 0;
		unsigned long ulRelativeIndex = 0;
		tchar pchString[MAX_DD_STRING] = {0};
		int pSize = MAX_DD_STRING;

		if ( ! GetStringParam(pchString, pSize, pVarParameters, 1) )
		{
			return false;
		}
	 
		RETURN_AT_NOT_NUMERIC(0);

		ulBlockId = (unsigned int)pVarParameters[0];

		int iReturnValue = get_block_instance_by_block_tag(ulBlockId, pchString, &ulRelativeIndex);
		RETURN_AT_DD_ERROR(iReturnValue);
		
		pVarParameters[1] = (unsigned int)ulRelativeIndex;
        OutputParameterValue( pFuncExp, 1, pVarParameters[1], 0);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		
		return true;
	}
	break;

	case BUILTIN_get_date_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;
        int length = 0;
        unsigned char pchDate[MAX_DD_STRING]={0};
		
        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
        RETURN_AT_NOT_NUMERIC(5);

        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];

        //Get the Index of the element
        iIndex = pVarParameters[1];

		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];

        ulelmtId = (unsigned int)pVarParameters[4];
        ulSubelmtId = (unsigned int)pVarParameters[5];

        // Get the length
        length = (int)pVarParameters[7];

        long lReturnValue = get_date_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId, pchDate, &length);
		RETURN_AT_DD_ERROR(lReturnValue);
        
		_BYTE_STRING Bss;
		Bss.bs = pchDate;
		Bss.bsLen = length + 1;

        if ( ! SetByteStringParam(pFuncExp, pVarParameters, 6,Bss) )
		{
			return false;
		}
       
		pVarParameters[7] = length;
        OutputParameterValue( pFuncExp, 7, pVarParameters[7], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		
		return true;
	}
	break;

	case BUILTIN_get_double_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);

		//Get the List Id
		ulListId = (unsigned int)pVarParameters[0];
		iIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];
		ulelmtId = (unsigned int)pVarParameters[4];
		ulSubelmtId = (unsigned int)pVarParameters[5];

		double dReturnValue = get_double_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId);
		
		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);
	
		return true;
	}
	break;

	case BUILTIN_get_float_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);

		//Get the List Id
		ulListId = (unsigned int)pVarParameters[0];
		iIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];
		ulelmtId = (unsigned int)pVarParameters[4];
		ulSubelmtId = (unsigned int)pVarParameters[5];

		float dReturnValue = get_float_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId);
		
		pVarReturnValue->SetValue(&dReturnValue, RUL_FLOAT);
	
		return true;
	}
	break;

	case BUILTIN_get_signed_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);

		//Get the List Id
		ulListId = (unsigned int)pVarParameters[0];
		iIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];
		ulelmtId = (unsigned int)pVarParameters[4];
		ulSubelmtId = (unsigned int)pVarParameters[5];

		long dReturnValue = get_signed_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId);
		
		pVarReturnValue->SetValue(&dReturnValue, RUL_INT);
	
		return true;
	}
	break;

	case BUILTIN_get_string_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;
        int length = 11;
        tchar pchDate[MAX_DD_STRING]={0};	//mm/dd/yyyy

        RETURN_AT_NOT_NUMERIC(0);
        RETURN_AT_NOT_NUMERIC(1);
        RETURN_AT_NOT_NUMERIC(2);
        RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
        RETURN_AT_NOT_NUMERIC(5);

        //Get the List Id
        ulListId = (unsigned int)pVarParameters[0];

        //Get the Index of the element
        iIndex = pVarParameters[1];

		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];

        ulelmtId = (unsigned int)pVarParameters[4];
        ulSubelmtId = (unsigned int)pVarParameters[5];

        // Get the length
        length = (int)pVarParameters[7];

        long lReturnValue = get_string_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId, pchDate, &length);
		RETURN_AT_DD_ERROR(lReturnValue);
        
		if ( ! SetStringParam(pFuncExp, pVarParameters, 6, pchDate) )
		{
			return false;
		}
       
		//update string length
		pVarParameters[7] = (int)pVarParameters[6].GetMaxSize();
        OutputParameterValue( pFuncExp, 7, pVarParameters[7], 0);
        pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		
		return true;
	}
	break;

	case BUILTIN_get_unsigned_lelem2:
	{
		unsigned long ulListId = 0;
		int iIndex = 0;
		unsigned long ulEmbListId = 0;
		int iEmbListIndex = 0;
        unsigned long ulelmtId = 0;
        unsigned long ulSubelmtId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		RETURN_AT_NOT_NUMERIC(5);

		//Get the List Id
		ulListId = (unsigned int)pVarParameters[0];
		iIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];
        iEmbListIndex = pVarParameters[3];
		ulelmtId = (unsigned int)pVarParameters[4];
		ulSubelmtId = (unsigned int)pVarParameters[5];

		unsigned long dReturnValue = get_unsigned_lelem2(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulelmtId, ulSubelmtId);
		
		pVarReturnValue->SetValue(&dReturnValue, RUL_UINT);
	
		return true;
	}
	break;

	case BUILTIN_ret_double_value2:
	{
		//used by builtins ret_double_value2() and ret_float_value2()
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);

		unsigned long ulBlkId = (unsigned int)pVarParameters[0];	
		unsigned long ulBlkNum = (unsigned int)pVarParameters[1];
		unsigned long ulItemId = (unsigned int)pVarParameters[2];	
		unsigned long ulMemberId = (unsigned int)pVarParameters[3];

		CValueVarient vValue{};
		long lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);

		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			if (vValue.vTagType == ValueTagType::CVT_R4)
			{
				float fVal = static_cast<float>(vValue.vValue.fFloatConst);
				pVarReturnValue->SetValue(&(fVal), RUL_FLOAT);
			}
			else if (vValue.vTagType == ValueTagType::CVT_R8)
			{
				pVarReturnValue->SetValue(&(CV_R8(&vValue)), RUL_DOUBLE);
			}
			else
			{
				return false;
			}
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		return true;
	}
	break;

	case BUILTIN_ret_signed_value2:
	{	
		unsigned long ulBlkId	= 0;
		unsigned long ulBlkNum	= 0;
		unsigned long ulItemId	= 0;
		unsigned long ulMemberId= 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);

		ulBlkId = (unsigned int)pVarParameters[0];	
		ulBlkNum = (unsigned int)pVarParameters[1];
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];

		long lValue = 0;
		long lReturnValue;
		CValueVarient vValue{};

		lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{
			
			lValue = CV_I4(&vValue);
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&lValue, RUL_INT);
		return true;
	}
	break;

	case BUILTIN_ret_unsigned_value2:
	{	
		unsigned long ulBlkId	= 0;
		unsigned long ulBlkNum	= 0;
		unsigned long ulItemId	= 0;
		unsigned long ulMemberId= 0;
	
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);

		ulBlkId = (unsigned int)pVarParameters[0];	
		ulBlkNum = (unsigned int)pVarParameters[1];
		ulItemId = (unsigned int)pVarParameters[2];	
		ulMemberId = (unsigned int)pVarParameters[3];

		unsigned long ulValue = 0;
		long lReturnValue;
		CValueVarient vValue{};

		lReturnValue = AccessTypeValue2(ulBlkId, ulBlkNum, ulItemId, ulMemberId, &vValue, READ);
		RETURN_AT_DD_ERROR(lReturnValue);
		if (lReturnValue == BLTIN_SUCCESS)
		{			
			ulValue = vValue.vValue.iIntConst;
		}
		else if (lReturnValue == BI_ABORT)
		{
			//variable is invalid
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}

		pVarReturnValue->SetValue(&ulValue, RUL_UINT);
		return true;
	}
	break;

	case BUILTIN_ListInsert2:
	{
		unsigned long ulListId	= 0;
		int iListIndex = 0;
		unsigned long ulEmbListId = 0;
		int iIndex = 0;
		unsigned long ulItemId = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);

		ulListId = (unsigned int)pVarParameters[0];	
		iListIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];	
		iIndex = (unsigned int)pVarParameters[3];
		ulItemId = (unsigned int)pVarParameters[4];

		long lReturnValue = ListInsert2( ulListId, iListIndex, ulEmbListId, iIndex, ulItemId);	

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;
	}
	break;

	case BUILTIN_drand:
	{
		double randVal = drand();
		pVarReturnValue->SetValue(&randVal, RUL_DOUBLE);
		return true;	
	}
	break;

	case BUILTIN_dseed:
	{
		RETURN_AT_NOT_NUMERIC(0);
		double dVal = (double)pVarParameters[0];
		dseed(dVal);
		return true;	
	}
	break;

	case BUILTIN_LOG_MESSAGE:
	{
		RETURN_AT_NOT_NUMERIC(0);
		int priorityVal = (int)pVarParameters[0];

		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 1) )
        {
			//return false;
        }

		LOG_MESSAGE(priorityVal, pchUIPrompt);
		return true;	
	}
	break;

	case BUILTIN_isOffline:
	{
		int iVal = isOffline();
		pVarReturnValue->SetValue(&iVal, RUL_INT);
		return true;	
	}
	break;
	
	case BUILTIN_ListDeleteElementAt2:
	{
		unsigned long ulListId	= 0;
		int iListIndex = 0;
		unsigned long ulEmbListId = 0;
		int iIndex = 0;
		int iCount = 0;

		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);

		ulListId = (unsigned int)pVarParameters[0];	
		iListIndex = (unsigned int)pVarParameters[1];
		ulEmbListId = (unsigned int)pVarParameters[2];	
		iIndex = (unsigned int)pVarParameters[3];
		iCount = (unsigned int)pVarParameters[4];

		long lReturnValue;

		lReturnValue = ListDeleteElementAt2(ulListId, iListIndex, ulEmbListId, iIndex, iCount);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);
		return true;

	}
	break;

	case BUILTIN_resolve_block_ref2:
	{
		unsigned long ulBlockItemId	= 0;
		unsigned long ulOccurrence	= 0;
		unsigned long ulMemberId	= 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		
		ulBlockItemId = (unsigned int)pVarParameters[0];	
		ulOccurrence  = (unsigned int)pVarParameters[1];	
		ulMemberId	  = (unsigned int)pVarParameters[2];	
			
		unsigned long ulReturnValue = resolve_block_ref(ulBlockItemId, ulOccurrence, ulMemberId, RESOLVE_BLOCK_REF);
		RETURN_AT_DD_ERROR(ulResolveStatus);
		
		pVarReturnValue->SetValue(&ulReturnValue, RUL_UINT);

		return true;
	}
	break;

	//The following builtins are used by Profibus
	case BUILTIN_GET_DD_REVISION:
	{
		int iReturnValue = GET_DDL((DDLInfoType)GET_DD_REVISION);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_GET_DEVICE_REVISION:
	{
		int iReturnValue = GET_DDL((DDLInfoType)GET_DEVICE_REVISION);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_GET_DEVICE_TYPE:
	{
		int iReturnValue = GET_DDL((DDLInfoType)GET_DEVICE_TYPE);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_GET_MANUFACTURER:
	{
		int iReturnValue = GET_DDL((DDLInfoType)GET_MANUFACTURER);

		pVarReturnValue->SetValue(&iReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_GET_TICK_COUNT:
	{
        long lReturnValue = GetTickCount();

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;

	case BUILTIN_ReadWriteCommand:
	{
		RETURN_AT_NOT_NUMERIC(0);
		unsigned long commandId = (unsigned int)pVarParameters[0];
		long responseCode = 0;
		int iReturnValue = ProfibusSendCommand(commandId, &responseCode);

		RETURN_AT_COMM_ERROR(iReturnValue);

		pVarReturnValue->SetValue(&responseCode, RUL_INT);	

		return true;
	}
	break;

	case BUILTIN__get_variable_string:
	{
		RETURN_AT_NOT_NUMERIC(0);
		ITEM_ID ulItemId = (unsigned int)pVarParameters[0];
		wchar_t pValueDisp[MAX_DD_STRING+1] = {0};

		int iReturnValue = get_variable_string(ulItemId, pValueDisp, MAX_DD_STRING);

		RETURN_AT_DD_ERROR(iReturnValue);
		if (iReturnValue == BI_ABORT)
		{
			*pBuiltinReturnCode = BUILTIN_VAR_ABORTED;
		}
		else
		{
			if ( ! SetStringParam(pFuncExp, pVarParameters, 1, pValueDisp) )
			{
			return false;
			}

			pVarReturnValue->SetValue(&iReturnValue, RUL_INT);
		}

		return true;
	}
	break;

	case BUILTIN__ERROR:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//return false;
		}

		LOG_MESSAGE(LOG_ERROR_TYPE, pchUIPrompt);

		return true;
	}
	break;
	case BUILTIN__WARNING:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//return false;
		}

		LOG_MESSAGE(LOG_WARNING_TYPE, pchUIPrompt);

		return true;
	}
	break;
	case BUILTIN__TRACE:
	{
		if ( ! GetStringParam(pchUIPrompt, NUM_ELEM(pchUIPrompt), pVarParameters, 0) )
		{
			//return false;
		}

		LOG_MESSAGE(LOG_TRACE_TYPE, pchUIPrompt);

		return true;
	}
	break;
	case BUILTIN__display_bitenum:
	{
		RETURN_AT_NOT_NUMERIC(0);
		ITEM_ID ulItemId = (unsigned int)pVarParameters[0];

		int iRetVal = display_bitenum(ulItemId);

		RETURN_AT_DD_ERROR(iRetVal);
		if (iRetVal == METHOD_ABORTED)
		{
			*pBuiltinReturnCode = BUILTIN_UI_CANCELLED;
		}
 
		return true;
	}
	break;

	case BUILTIN_GET_SEL_DOUBLE:
	{
		unsigned long ulItemId		= 0;
		unsigned long ulSelector	= 0;
		unsigned long ulAdditional	= 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		
		ulItemId		= (unsigned int)pVarParameters[0];	
		ulSelector		= (unsigned int)pVarParameters[1];	
		ulAdditional	= (unsigned int)pVarParameters[2];	
			
		double dReturnValue = get_sel_double2(ulItemId, 0, 0, ulSelector, ulAdditional);

		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);

		return true;
	}
	break;

	case BUILTIN_GET_SEL_DOUBLE2:
	{
		unsigned long ulItemId		= 0;
		int iIndex					= 0;
		unsigned long ulEmbeddedId	= 0;
		unsigned long ulSelector	= 0;
		unsigned long ulAdditional	= 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		RETURN_AT_NOT_NUMERIC(4);
		
		ulItemId		= (unsigned int)pVarParameters[0];	
		iIndex			= (int)pVarParameters[1];	
		ulEmbeddedId	= (unsigned int)pVarParameters[2];	
		ulSelector		= (unsigned int)pVarParameters[3];	
		ulAdditional	= (unsigned int)pVarParameters[4];	
			
		double dReturnValue = get_sel_double2(ulItemId, iIndex, ulEmbeddedId, ulSelector, ulAdditional);

		pVarReturnValue->SetValue(&dReturnValue, RUL_DOUBLE);

		return true;
	}
	break;

	case BUILTIN_GET_SEL_STRING:
	{
        char pchString[MAX_DD_STRING]={0};		/*output*/
		unsigned long ulItemId		= 0;
		unsigned long ulSelector	= 0;
		unsigned long ulAdditional	= 0;
		long length					= 0;		/*output*/
		
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		RETURN_AT_NOT_NUMERIC(3);
		
		ulItemId		= (unsigned int)pVarParameters[1];	
		ulSelector		= (unsigned int)pVarParameters[2];	
		ulAdditional	= (unsigned int)pVarParameters[3];	
			
		long lReturnValue = get_sel_string(pchString, ulItemId, ulSelector, ulAdditional, &length);
        if ( ! SetCharStringParam(pFuncExp, pVarParameters, 0, pchString, length) )
		{
			return false;
		}
       
		//update string length
		pVarParameters[4] = (int)pVarParameters[0].GetMaxSize();
        OutputParameterValue( pFuncExp, 4, pVarParameters[4], 0);

		pVarReturnValue->SetValue(&lReturnValue, RUL_INT);

		return true;
	}
	break;
	case BUILTIN_RESOLVE_SELECTOR_REF: /* Builtin resolve_selector_ref() is obsolete for FDI. But keep it for legacy FF. */
	{
		unsigned long ulItem_id = 0;
		unsigned long ulIndex = 0;
		unsigned long ulselector = 0;
		
		RETURN_AT_NOT_NUMERIC(0);
		RETURN_AT_NOT_NUMERIC(1);
		RETURN_AT_NOT_NUMERIC(2);
		ulItem_id = (unsigned int)pVarParameters[0];	
		ulIndex = (unsigned int)pVarParameters[1];	
		ulselector = (unsigned int)pVarParameters[2];	

		unsigned long iReturnValue = resolve_selector_ref(ulItem_id, ulIndex, ulselector);

		pVarReturnValue->SetValue(&iReturnValue, RUL_UINT);
		return true;
	}	
	break;
	case BUILTIN_LINE_NUMBER : 
	{
		unsigned long ulLineNo = 0;
		wchar_t pLineNo[MAX_DD_STRING] = { 0 };
		if (pVarParameters[0].isNumeric())
		{
			ulLineNo = (unsigned int)pVarParameters[0];
		}
		else
		{
			return false;
		}

		MethodDebugMessage(ulLineNo);
		return true;
	}
	break;

	default:
      break;

        //Added By Anil June 17 2005 --Ends here
        /****************************End of DD_STRING  Builtins (eDDL) ********************/
    }	//end of switch()

    /* End of code */

    return false;
}


//Added By Walter Sigtermans --starts here
//This is the utility Function to take care of the Language Code while doing string operations
// for unicode strings
void CBuiltIn::GetLanguageCode(wchar_t* szString, wchar_t* szLanguageCode, bool* bLangCodePresent)
{
	if( szString )
	{
		*bLangCodePresent =false;
		if( (szString[0] == _T('|')) &&  (szString[3] == _T('|')) )
		{
			wcsncpy(szLanguageCode, szString, 4);
			szLanguageCode[3] = _T('|');
			szLanguageCode[4] = _T('\0');

			*bLangCodePresent = true;
			size_t count,itemp = wcslen(szString);// WS - 9apr07 - 2005 checkin
			for(count = 4; count <itemp ;count++)// WS - 9apr07 - 2005 checkin
			{
				szString[count-4] = szString[count];
			}
			szString[count-4] = _T('\0');
		}
	}
	return;

}


//Added By Anil June 17 2005 --starts here
//This is the utility Function to take care of the Language Code while doing string operations
void CBuiltIn::GetLanguageCode(char* szString, char* szLanguageCode, bool* bLangCodePresent)
{
	if( szString )
	{
		*bLangCodePresent =false;
		if( (szString[0] == '|') &&  (szString[3] == '|') )
		{
			*bLangCodePresent = true;
			size_t count,itemp = strlen(szString);// WS - 9apr07 - 2005 checkin
			for(count = 4; count <itemp ;count++)// WS - 9apr07 - 2005 checkin
			{
				if(count<8)
					szLanguageCode[count-4] = szString[count-4];
				szString[count-4] = szString[count];
			}
			szString[count-4] = '\0';
			szLanguageCode[4] = '\0';
		}
	}
	return;

}

//Added By Anil June 17 2005 --Ends here

// added by stevev 30may07 - start
// This is a utility to get all this widely used coding in one place
//   returns success=true, failure = false
bool CBuiltIn::GetStringParam(wchar_t* retString, int retStringLen/*in wide chars*/,
									                  INTER_VARIANT *pParamArray, int paramNumber)
{
	wchar_t *pRet = NULL;
	
	pParamArray[paramNumber].GetStringValue(&pRet);
	if ( pRet != NULL )// allocated in GetString
	{
		wchar_t szLang[5] = {0};
		bool bLangPresent=false;
		//		Remove the Language code , if it was prepended <a tokenizer bug> for FF
		GetLanguageCode( pRet, szLang, &bLangPresent );//remove language code if present

		size_t stringLen  = min(wcslen(pRet), (size_t)retStringLen-1) + 1;
		PS_Wcsncpy(retString, stringLen, pRet, _TRUNCATE);
		delete[] pRet;
		return true;
	}

	return false;
}
// ByteString is NOT required to be null terminated and is L bytes long
// CharString is used to null terminator or L length, which ever comes first

bool CBuiltIn::GetCharStringParam(char* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber)
{
	wchar_t *pRet = NULL;
	
	pParamArray[paramNumber].GetStringValue(&pRet);
	if ( pRet != NULL )// allocated in GetString
	{
		size_t convertedChars = 0;
		PS_Wcstombs(&convertedChars, retString, retStringLen, pRet, _TRUNCATE);
		delete[] pRet;
		return true;
	}

	return false;
}

//// Description: this function handles unsigned char buffer for date-time. It takes care of null-characters
/// inbetween date-time buffer
bool CBuiltIn::GetUnsignedCharArrayParam(unsigned char* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber)
{
	string Src;

	if (pParamArray[paramNumber].GetCharBuffValue(Src))
	{
		for (int i = 0; i<retStringLen; i++){
			retString[i] =Src[i]; 
		}

		return true;
	}
	else 
	{
		return false;
	}
}

//
//
//
bool CBuiltIn::SetCharStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, int paramNumber, char* paramString, int L)
{
	bool ret = true;
	INTER_VARIANT * pParam =  &(pParamArray[paramNumber]);
	if ((pParam->GetVarType() == RUL_CHARPTR)	  ||	/* needs L */
		(pParam->GetVarType() == RUL_WIDECHARPTR) ||	/* needs L */ 
		(pParam->GetVarType() == RUL_DD_STRING)	  ||	/* needs L */ 
	    (pParam->GetVarType() == RUL_BYTE_STRING) ||
		(pParam->GetVarType() == RUL_SAFEARRAY)    )
	{
		pParamArray[paramNumber] = paramString;//will convert destination type as required
		ret = OutputParameterValue( pFuncExp, paramNumber, pParamArray[paramNumber], (int)strlen(paramString) );// added WS:EPM 17jul07
	}
	else 
	{// string to numeric unsupported
		ret = false;
	}
	return ret;
}

// Write unsigned char array for output
bool CBuiltIn::SetCharArrayParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, int paramNumber, unsigned char* param, int L)
{
	bool ret = true;
	for (int i=0; i < L; i++)
	{
		if (pParamArray[paramNumber].GetVarType() == RUL_UNSIGNED_CHAR)
		{
			pParamArray[paramNumber] = param[i];//will convert destination type as required
			ret = OutputParameterValue( pFuncExp, paramNumber, pParamArray[paramNumber], sizeof(param[i]) );
			paramNumber++;
		}
		else 
		{// the variable type to numeric unsupported
			ret = false;
			break;
		}
	}
	return ret;
}

//
//
//
bool CBuiltIn::SetByteStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray,
	int paramNumber, _BYTE_STRING& bsS)
{// must handle nulls in the byte string!!
	int strLength = MAX_DD_STRING;
	int maxLength = 0;
	//	char* pC = (char*) paramString;
	bool ret = false;
	INTER_VARIANT * pParam = &(pParamArray[paramNumber]);

	if ((pParam->GetVarType() == RUL_CHARPTR) ||	/* needs L */
		(pParam->GetVarType() == RUL_WIDECHARPTR) ||	/* needs L */
		(pParam->GetVarType() == RUL_DD_STRING) ||	/* needs L */
		(pParam->GetVarType() == RUL_BYTE_STRING) ||
		(pParam->GetVarType() == RUL_SAFEARRAY))
	{
		pParamArray[paramNumber] = bsS;//will convert destination type as required
		ret = OutputParameterValue(pFuncExp, paramNumber, pParamArray[paramNumber], bsS.bsLen);// added WS:EPM 17jul07

	}
	//else string to numeric unsupported

	return ret;
}

bool CBuiltIn::GetByteStringParam(uchar* retString,  int retStringLen, INTER_VARIANT *pParamArray, int paramNumber)
{
	cerr<<"ERROR: *** CHart_Builtins::GetByteStringParam is Not implemented!!"<<endl;
	return false;
}

bool CBuiltIn::SetStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, 
									int paramNumber,    wchar_t* paramString, bool bExpandString)
{
	int strLength = (int)wcslen(paramString);
	bool ret = true;

	if ((pParamArray[paramNumber].GetVarType() == RUL_CHARPTR)	   ||
		(pParamArray[paramNumber].GetVarType() == RUL_WIDECHARPTR) || 
		(pParamArray[paramNumber].GetVarType() == RUL_DD_STRING)   || 
	    (pParamArray[paramNumber].GetVarType() == RUL_BYTE_STRING) || 
		(pParamArray[paramNumber].GetVarType() == RUL_SAFEARRAY)    )
	{
		ret = OutputLiteralString( pFuncExp, paramNumber, &(pParamArray[paramNumber]), paramString );
		if (!ret)
		{
			if ((bExpandString) && (pParamArray[paramNumber].GetVarType() == RUL_SAFEARRAY) 
				&& (pParamArray[paramNumber].GetSafeArray()->Type() == RUL_CHAR))
			{
				//this must be called by FF builtins get_dictionary_string and get_stddict_string
				//In order to be back compatible, we allow memory expansion
				pParamArray[paramNumber].GetSafeArray()->ExpandString(paramString);
				
				//find the local variable
				CExpression *pExp = (CExpression*)pFuncExp->GetExpParameter(paramNumber);
				if (pExp)
				{
					CToken* pToken = pExp->GetToken();
					if (pToken)
					{
						const char* szVarName = pToken->GetLexeme();
						if (szVarName)
						{
							char szLocalVarName[MAX_PATH] = { 0 };
							char szLang[5] = { 0 };
							bool bLangPresent = false;

							strcpy(szLocalVarName, szVarName);

							//		Remove the Language code , if it was appended <a tokenizer bug>
							GetLanguageCode(szLocalVarName, szLang, &bLangPresent);
							//		Update the local variable szLocaVarName with the value
							//bRetVal = m_pInterpreter->SetVariableValue(szLocalVarName, pParamArray[paramNumber]);
							bool bLocal = false;
							CVariable *pVar = NULL;
							m_pInterpreter->FindInLocalSymbolTable(szLocalVarName, &pVar, &bLocal);
							
							//output the string to the local char[] array variable
							if ((pVar != NULL) && bLocal && ((pVar->GetValue()).GetVarType() == RUL_SAFEARRAY))
							{
								(pVar->GetValue()).GetSafeArray()->ExpandString(paramString);
								ret = true;
							}
						}
					}
				}
			}
			
			if (!bExpandString || !ret)
			{
				pParamArray[paramNumber] = paramString; //will convert to pParamArray type
				ret = OutputParameterValue(pFuncExp, paramNumber, pParamArray[paramNumber], strLength);
			}
		}
	}
	else
	{
		ret = false;// we don't do string to numeric here
	}
	return ret;
}


/* added by WS:EPM 17jul07 checkin */
// stevev 11feb08 - added length to deal with byte-strings
bool CBuiltIn::OutputParameterValue( FunctionExpression* pFuncExp, 
										  int nParamNumber, INTER_VARIANT &NewVarValue, int L)
{
	bool bRetVal = true;

	CExpression *pExp = (CExpression*)pFuncExp->GetExpParameter(nParamNumber);
	if (!pExp)
	{
		bRetVal = false;
	}

	if( bRetVal )
	{
		CToken* pToken = pExp->GetToken();
		
		if ( !pToken )
		{
			//this may be a compound expression, ex. a token in parentheses
			CCompoundExpression *pCompoundExp = (CCompoundExpression *)pExp;
			pExp = pCompoundExp->GetFirstExpression();
			if (pExp)
			{
				pToken = pExp->GetToken();
			}
		}

		bRetVal = false;
		if( pToken )
		{
			switch(pToken->GetType())
			{
			case RUL_ARRAY_VARIABLE:
			case RUL_DD_ITEM:
				//		Update local array or DD variable with the value
				bRetVal = m_pInterpreter->SetVariableValue( pExp, NewVarValue );
				break;
			case RUL_SIMPLE_VARIABLE:
			{
				const char* szVarName = pToken->GetLexeme();
				if (szVarName)
				{
					char szLocalVarName[MAX_PATH] = {0};
					char szLang[5] = {0};
					bool bLangPresent = false;

					strcpy( szLocalVarName, szVarName );

					//		Remove the Language code , if it was appended <a tokenizer bug>
					GetLanguageCode( szLocalVarName, szLang, &bLangPresent );
					//		Update the local variable szLocaVarName with the value
					bRetVal = m_pInterpreter->SetVariableValue( szLocalVarName, NewVarValue );
				}
			}
			break;
			default:
				bRetVal = false;
				break;
			}
		}
		//else it may be DD error
	}

	return bRetVal;
}


bool CBuiltIn::OutputLiteralString( FunctionExpression* pFuncExp, int nParamNumber, INTER_VARIANT *pParam, wchar_t* pParamString )
{
	bool bRetVal = false;

	CComplexDDExpression *pExp = (CComplexDDExpression*)pFuncExp->GetExpParameter(nParamNumber);

	if( pExp )
	{
		CToken* pToken = pExp->GetToken();
		if( pToken == NULL )
		{
			//maybe literal_string
			const char* sFuncName = (char*)((FunctionExpression *)pExp)->GetFunctionName();		//pointer, no memory allocated
			if ( (sFuncName != NULL) && (strcmp(sFuncName, "literal_string") == 0) )
			{
				char *psLocalVarName = NULL;

				//get variable name if existing in pParam
				pParam->GetStringValue(&psLocalVarName, RUL_CHARPTR);	//memory allocated
				if (strlen(psLocalVarName) > 0)
				{
					char szLang[5] = {0};
					bool bLangPresent=false;
					//		Remove the Language code , if it was appended <a tokenizer bug>
					GetLanguageCode( psLocalVarName, szLang, &bLangPresent );

					//		Update the DD local var szLocaVarName with the value lselection
					INTER_VARIANT paramVal;
					paramVal.SetValue((void *)pParamString, RUL_DD_STRING);
					bRetVal = m_pInterpreter->SetVariableValue( psLocalVarName, paramVal );
				}

				if (psLocalVarName)
				{
					delete psLocalVarName;
					psLocalVarName = NULL;
				}
			}
		}
	}

	return bRetVal;
}
