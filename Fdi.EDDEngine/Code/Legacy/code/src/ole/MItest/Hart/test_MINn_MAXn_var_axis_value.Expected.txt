﻿<Method>
{
 float  minValue;
 float  maxValue;

 minValue = minN_maxN_var.MIN_VALUE1;
 PUT_MESSAGE(literal_string(xxx));
 maxValue = minN_maxN_var.MAX_VALUE3;
 PUT_MESSAGE(literal_string(xxx));
 minValue = min_max_axis.MIN_VALUE;
 PUT_MESSAGE(literal_string(xxx));
 maxValue = min_max_axis.MAX_VALUE;
 PUT_MESSAGE(literal_string(xxx));
 menu_display_test_float_var5 = 50.5;
 maxValue = ChartConditionalCommonUnitRelatAxis.MAX_VALUE;
 PUT_MESSAGE(literal_string(xxx));
}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: minN_maxN_var.MIN_VALUE1 = %{minValue}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "minN_maxN_var.MIN_VALUE1 =         10.5"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: minN_maxN_var.MIN_VALUE1 = %{minValue}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: minN_maxN_var.MAX_VALUE3 = %{maxValue}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "minN_maxN_var.MAX_VALUE3 =         15.5"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: minN_maxN_var.MAX_VALUE3 = %{maxValue}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: min_max_axis.MIN_VALUE = %{minValue}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "min_max_axis.MIN_VALUE =         13.5"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: min_max_axis.MIN_VALUE = %{minValue}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: min_max_axis.MAX_VALUE = %{maxValue}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "min_max_axis.MAX_VALUE =         30.8"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: min_max_axis.MAX_VALUE = %{maxValue}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 16567, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: FLOAT,  Value: 50.500000
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: ChartConditionalCommonUnitRelatAxis.MAX_VALUE = %{maxValue}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "ChartConditionalCommonUnitRelatAxis.MAX_VALUE =         50.5"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: ChartConditionalCommonUnitRelatAxis.MAX_VALUE = %{maxValue}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><minValue>13.50000,</minValue><maxValue>50.50000,</maxValue></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
