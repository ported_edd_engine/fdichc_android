// IterationFor.cpp //

#include "stdafx.h"
#include "IterationFor.h"
#include "ExpParser.h"
#include "ParserBuilder.h"
#include "GrammarNodeVisitor.h"
#include "PrimaryExpression.h"

#include "ErrorDefinitions.h"


CIterationForStatement::CIterationForStatement()
{
	m_pStatement = 0;

	m_pExpressionStatement = NULL;
	m_pExpression = 0;
	m_pIncrementStatement = NULL;
	m_pIncrementExpression = 0;
	m_pInitializationStatement = NULL;
	m_pInitializationExpression = 0;
}

CIterationForStatement::~CIterationForStatement()
{
	DELETE_PTR(m_pStatement);

	DELETE_PTR(m_pExpressionStatement);
	DELETE_PTR(m_pExpression);
	DELETE_PTR(m_pIncrementStatement);
	DELETE_PTR(m_pIncrementExpression);
	DELETE_PTR(m_pInitializationStatement);
	DELETE_PTR(m_pInitializationExpression);
}

_INT32 CIterationForStatement::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitIterationStatement(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

// Iteration Statement is of the form
//	<FOR><(><Initialization Statement><;><(><Expression><)><;><Increment Statement><)><Statement>;
_INT32 CIterationForStatement::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CToken* pToken=0;
	try
	{
//Munch a <FOR>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !pToken->IsFORStatement())
		{
			DELETE_PTR(pToken);
			throw(C_UM_ERROR_MISSINGTOKEN);
		}
		DELETE_PTR(pToken);

//Munch a <(>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !(pToken->GetSubType() == RUL_LPAREN))
		{
			plexAnal->UnGetToken();
			throw(C_WHILE_ERROR_MISSINGLP);
		}
		DELETE_PTR(pToken);

//Munch a Initialization Statement or expression
		CParserBuilder builder;

		CGrammarNode *pNode;
		pNode = builder.CreateParser(plexAnal, STMT_asic);

		m_pInitializationStatement = NULL;
		m_pInitializationExpression = NULL;
		if (NULL != pNode)
		{
			incrementNodeType = pNode->GetNodeType();
			if (incrementNodeType == NODE_TYPE_ASSIGN)
			{
				m_pInitializationStatement = (CAssignmentStatement*)pNode;

				m_pInitializationStatement->CreateParseSubTree(
												plexAnal,
												pSymbolTable,
												pvecErrors,
												STMT_ASSIGNMENT
												);
			}
			else if (incrementNodeType == NODE_TYPE_EXPRESSION)
			{
				CExpParser expParser;
				m_pInitializationExpression 
					= expParser.ParseExpression(plexAnal,pSymbolTable,EXPR_FOR, pvecErrors);

				if(!m_pInitializationExpression)
				{
					DELETE_PTR(pNode);
					throw(C_WHILE_ERROR_MISSINGEXP);
				}

				DELETE_PTR(pNode);
			}
			else
			{
				//empty expression
				DELETE_PTR(pNode);
			}
		}

//Munch a <;>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !(pToken->GetSubType() == RUL_SEMICOLON))
		{
			plexAnal->UnGetToken();
			DELETE_PTR(pToken);
			throw(C_EP_ERROR_MISSINGSC);
		}
		DELETE_PTR(pToken);

//Munch & Parse the expression.
//we got to give the expression string to the expression parser.
//	
		CGrammarNode *pNode2;
		pNode2 = builder.CreateParser(plexAnal, STMT_asic);

		m_pExpression = NULL;
		m_pExpressionStatement = NULL;
		if (NULL != pNode2)
		{
			expressionNodeType = pNode2->GetNodeType();
			if (expressionNodeType == NODE_TYPE_ASSIGN)
			{
				m_pExpressionStatement = (CAssignmentStatement*)pNode2;

				m_pExpressionStatement->CreateParseSubTree(
					plexAnal,
					pSymbolTable,
					pvecErrors,
					STMT_asic
					);
			}
			else if (expressionNodeType == NODE_TYPE_EXPRESSION)
			{
				CExpParser expParser;
				m_pExpression 
					= expParser.ParseExpression(plexAnal,pSymbolTable,EXPR_FOR, pvecErrors);

				if(!m_pExpression)
				{
					DELETE_PTR(pNode2);
					throw(C_WHILE_ERROR_MISSINGEXP);
				}

				DELETE_PTR(pNode2);
			}
			else
			{
				pToken = 0;

				try
				{
					if( (LEX_FAIL != (plexAnal->LookAheadToken(&pToken))) && pToken)
					{
						if ( (pToken->GetType() == RUL_SYMBOL) && (pToken->GetSubType() == RUL_SEMICOLON) )
						{
							// This code is to handle for(;;) for Yokagawa EJX
							expressionNodeType = NODE_TYPE_EXPRESSION;
							CToken *pToken2 = new CToken("1");
							pToken2->SetType(RUL_NUMERIC_CONSTANT);
							m_pExpression = new CPrimaryExpression(pToken2);
						}
					}
					DELETE_PTR(pToken);
				}
				catch(_INT32 error)
				{
					DELETE_PTR(pNode2);
					DELETE_PTR(pToken);
					ADD_ERROR(error);
				}

				DELETE_PTR(pNode2);
			}
		}

//Munch a <;>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !(pToken->GetSubType() == RUL_SEMICOLON))
		{
			plexAnal->UnGetToken();
			DELETE_PTR(pToken);
			throw(C_EP_ERROR_MISSINGSC);
		}
		DELETE_PTR(pToken);

//Munch a Increment Statement...
		CGrammarNode *pNode3;
		pNode3 = builder.CreateParser(plexAnal, STMT_asic);

		m_pIncrementExpression = NULL;
		m_pIncrementStatement = NULL;
		if (NULL != pNode3)
		{
			incrementNodeType = pNode3->GetNodeType();
			if (incrementNodeType == NODE_TYPE_ASSIGN)
			{
				m_pIncrementStatement = (CAssignmentStatement*)pNode3;

				m_pIncrementStatement->CreateParseSubTree(
					plexAnal,
					pSymbolTable,
					pvecErrors,
					STMT_ASSIGNMENT_FOR
					);
			}
			else if (incrementNodeType == NODE_TYPE_EXPRESSION)
			{
				CExpParser expParser;
				m_pIncrementExpression 
					= expParser.ParseExpression(plexAnal,pSymbolTable,EXPR_FOR, pvecErrors);

				if(!m_pIncrementExpression)
				{
					DELETE_PTR(pNode3);
					throw(C_WHILE_ERROR_MISSINGEXP);
				}
	
				DELETE_PTR(pNode3);
			}
			else
			{
				//empty expression
				DELETE_PTR(pNode3);
			}
		}

//Munch a <)>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !(pToken->GetSubType() == RUL_RPAREN))
		{
			plexAnal->UnGetToken();
			DELETE_PTR(pToken);
			throw(C_WHILE_ERROR_MISSINGRP);
		}
		DELETE_PTR(pToken);

//Munch the statement
		CParserBuilder builder3;
		if(0!=(m_pStatement = (CStatement*)builder.CreateParser(plexAnal, STMT_asic)))
		{
			m_pStatement->CreateParseSubTree(
				plexAnal,
				pSymbolTable,
				pvecErrors);
		}
		else
		{
			throw(C_WHILE_ERROR_MISSINGSTMT);
		}

		return PARSE_SUCCESS;
	}
	catch(_INT32 error)
	{
		plexAnal->MovePast(
			RUL_SYMBOL,
			RUL_SEMICOLON,
			pSymbolTable);
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}
	return PARSE_FAIL;
}

void CIterationForStatement::Identify(
		_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,"FORStatement");
	strcat(szData,">");

	if (m_pStatement)
		m_pStatement->Identify(szData);

	strcat(szData,"<");
	strcat(szData,"Expression");
	strcat(szData,">");
	if (m_pExpression)
		m_pExpression->Identify(szData);
	strcat(szData,"</");
	strcat(szData,"Expression");
	strcat(szData,">");
	strcat(szData,"</");

	strcat(szData,"FORStatement");
	strcat(szData,">");

}

CExpression* CIterationForStatement::GetExpression()
{
	return m_pExpression;
}

CAssignmentStatement* CIterationForStatement::GetExpressionStatement()
{
	return m_pExpressionStatement;
}

CStatement* CIterationForStatement::GetStatement()
{
	return m_pStatement;
}

CExpression* CIterationForStatement::GetInitializationExpression()
{
	return m_pInitializationExpression;
}

CStatement* CIterationForStatement::GetInitializationStatement()
{
	return m_pInitializationStatement;
}

CExpression* CIterationForStatement::GetIncrementExpression()
{
	return m_pIncrementExpression;
}

CStatement* CIterationForStatement::GetIncrementStatement()
{
	return m_pIncrementStatement;
}

_INT32 CIterationForStatement::GetLineNumber()
{
	if (m_pStatement)
	{
		return m_pStatement->GetLineNumber();
	}
	else
	{
		return 0;
	}
}
