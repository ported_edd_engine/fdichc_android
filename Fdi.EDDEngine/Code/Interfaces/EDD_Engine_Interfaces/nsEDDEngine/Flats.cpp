#include "stdafx.h"
#include "nsEDDEngine/Flats.h"
#include "nsEDDEngine/Attrs.h"

void nsEDDEngine::DEPBIN::CopyTo(void *depbin_ptr)
{
	::nsEDDEngine::DEPBIN *db = (::nsEDDEngine::DEPBIN *)depbin_ptr;

	db->bin_chunk=bin_chunk;
	db->bin_size=bin_size;

	db->dep = dep;
}

void nsEDDEngine::DEPBIN::CopyFromWithDelete(DEPBIN *pSource, DEPBIN **pDestination)
{
	if(*pDestination != nullptr)
	{
		delete *pDestination;
		*pDestination = nullptr;
	}

	if(pSource == NULL) 
	{ 
		*pDestination = NULL; 
	} 
	else 
	{ 
		*pDestination = new DEPBIN(*pSource); 
	}


}

//copy constructor for DEPBIN
nsEDDEngine::DEPBIN::DEPBIN(const DEPBIN &depbin)
{
         AssignDepbin(depbin);
}

//Overloaded Assignment Operator for DEPBIN
nsEDDEngine::DEPBIN& nsEDDEngine::DEPBIN::operator= (const DEPBIN &depbin)
{
         if(this != &depbin)
         {
                 // Do not delete a bin_chunk not allocated by this instance
                 // doing so would corrupt the reference from another
                 if(bin_chunk != nullptr && allocated)
                 {
                          delete [] bin_chunk;
                          bin_chunk = nullptr;
                 }

                 AssignDepbin(depbin);
         }
         return (*this);
}

void nsEDDEngine::DEPBIN::AssignDepbin(const DEPBIN &depbin)
{

         if(depbin.bin_chunk == NULL)
         {
                 bin_size = 0;
                 bin_chunk = nullptr;
                 allocated = false;
         }
         else
         {
                 bin_size = depbin.bin_size;
                 bin_chunk        = new unsigned char[bin_size];
                 memcpy(this->bin_chunk,depbin.bin_chunk, bin_size);
                 allocated = true;
         }
}




nsEDDEngine::ItemBase::~ItemBase()
{
//	symbol_name.Dispose();
}

//copy constructor for ItemBase
nsEDDEngine::ItemBase::ItemBase(const ItemBase &item_base)
{
	AssignItemBase(item_base);
}

//Overloaded Assignment Operator for ItemBase
nsEDDEngine::ItemBase& nsEDDEngine::ItemBase::operator= (const ItemBase &item_base)
{
	if(this != &item_base)
	{
		AssignItemBase(item_base);
	}
	return (*this);
}

void nsEDDEngine::ItemBase::AssignItemBase(const ItemBase &item_base)
{
	id			= item_base.id;
	symbol_name	= item_base.symbol_name;	
	item_type	= item_base.item_type;		
	masks		= item_base.masks;
	
	memcpy(_dummy,item_base._dummy, 50);
}

void nsEDDEngine::ItemBase::FreeDepbins()
{
	std::map<AttributeName, AttributeItem> items;
	GetDepbinMap(items);

	for(std::map<::nsEDDEngine::AttributeName, 
		::nsEDDEngine::AttributeItem>::iterator attribute_item=items.begin();
		attribute_item!=items.end(); 
		attribute_item++)
	{
		::nsEDDEngine::AttributeItem item=(*attribute_item).second;
		delete *item.depbin_storage;
		*item.depbin_storage = nullptr;
	}
}


bool nsEDDEngine::ItemBase::isAvailable(AttributeName eAttrName)
{
	bool bFound = false;
	if(masks.attr_avail.isMember(eAttrName))
	{
		bFound = true;
	}
	
	return bFound;
}

bool nsEDDEngine::ItemBase::IsDynamic(AttributeName eAttrName)
{
	bool bFound = false;

	if(masks.dynamic.isMember(eAttrName))
	{
		bFound = true;
	}

	return bFound;
}

//copy constructor for FLAT_ARRAY
nsEDDEngine::FLAT_ARRAY::FLAT_ARRAY(const FLAT_ARRAY &flat_array):ItemBase(ITYPE_ARRAY)
{
	AssignFlatArray(flat_array);
}

//Overloaded Assignment Operator for FLAT_ARRAY
nsEDDEngine::FLAT_ARRAY& nsEDDEngine::FLAT_ARRAY:: operator=(const FLAT_ARRAY &flat_array)
{
	if(this != &flat_array)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatArray(flat_array);
	}
    return (*this);
}

void nsEDDEngine::FLAT_ARRAY::AssignFlatArray(const FLAT_ARRAY &flat_array)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_array);

	item_info		= flat_array.item_info;
	help			= flat_array.help;
	label			= flat_array.label;
	valid			= flat_array.valid;
	type			= flat_array.type;
	num_of_elements	= flat_array.num_of_elements;
	resp_codes		= flat_array.resp_codes;
	private_attr	= flat_array.private_attr;
	visibility		= flat_array.visibility;
	write_mode		= flat_array.write_mode;

	depbin		    = new ARRAY_DEPBIN(*flat_array.depbin);
}

nsEDDEngine::FLAT_ARRAY::~FLAT_ARRAY()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_ARRAY::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::type_definition | 
	nsEDDEngine::response_codes | nsEDDEngine::number_of_elements | nsEDDEngine::private_attr | 
	nsEDDEngine::visibility | nsEDDEngine::write_mode;

//copy constructor for ARRAY_DEPBIN
nsEDDEngine::ARRAY_DEPBIN::ARRAY_DEPBIN(const ARRAY_DEPBIN &array_depbin): db_item_information(0), db_num_of_elements(0), db_help(0), db_label(0), db_valid(0), db_type(0), 
			db_resp_codes(0), db_private_attr(0), db_visibility(0), db_write_mode(0)
{
	AssignArrayDepbin(array_depbin);
}

//Overloaded Assignment Operator for ARRAY_DEPBIN
nsEDDEngine::ARRAY_DEPBIN& nsEDDEngine::ARRAY_DEPBIN:: operator=(const ARRAY_DEPBIN &array_depbin)
{
	if(this != &array_depbin)
	{
		AssignArrayDepbin(array_depbin);
	}
    return (*this);
}

//AssignArrayDepbin is used to assign all the attributes of ARRAY_DEPBIN
void nsEDDEngine::ARRAY_DEPBIN::AssignArrayDepbin(const ARRAY_DEPBIN &array_depbin)
{
	DEPBIN::CopyFromWithDelete(array_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(array_depbin.db_num_of_elements,&db_num_of_elements);
	DEPBIN::CopyFromWithDelete(array_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(array_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(array_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(array_depbin.db_type,&db_type);
	DEPBIN::CopyFromWithDelete(array_depbin.db_resp_codes,&db_resp_codes);
	DEPBIN::CopyFromWithDelete(array_depbin.db_private_attr,&db_private_attr);
	DEPBIN::CopyFromWithDelete(array_depbin.db_visibility,&db_visibility);
	DEPBIN::CopyFromWithDelete(array_depbin.db_write_mode,&db_write_mode);
}
nsEDDEngine::ARRAY_DEPBIN::~ARRAY_DEPBIN()
{
}

//copy constructor for FLAT_COLLECTION
nsEDDEngine::FLAT_COLLECTION::FLAT_COLLECTION(const FLAT_COLLECTION &flat_collection):ItemBase(ITYPE_COLLECTION)
{
	AssignFlatCollection(flat_collection);
}

//Overloaded Assignment Operator for FLAT_COLLECTION
nsEDDEngine::FLAT_COLLECTION& nsEDDEngine::FLAT_COLLECTION:: operator=(const FLAT_COLLECTION &flat_collection)
{
	if(this != &flat_collection)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatCollection(flat_collection);
	}
    return (*this);
}

void nsEDDEngine::FLAT_COLLECTION::AssignFlatCollection(const FLAT_COLLECTION &flat_collection)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_collection);

	item_info		= flat_collection.item_info;
	subtype			= flat_collection.subtype;
	op_members		= flat_collection.op_members;
	help			= flat_collection.help;
	label			= flat_collection.label;
	valid			= flat_collection.valid;
	visibility		= flat_collection.visibility;
	private_attr	= flat_collection.private_attr;

	depbin	  = new COLLECTION_DEPBIN(*flat_collection.depbin);
}

nsEDDEngine::FLAT_COLLECTION::~FLAT_COLLECTION()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COLLECTION::All_Attrs =
	 nsEDDEngine::label| nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::members | nsEDDEngine::visibility;

//copy constructor for COLLECTION_DEPBIN
nsEDDEngine::COLLECTION_DEPBIN::COLLECTION_DEPBIN(const COLLECTION_DEPBIN &collection_depbin): db_item_information(0), db_members(0), db_help(0), db_label(0), db_valid(0), db_private_attr(0), db_visibility(0)
{
	AssignCollectionDepbin(collection_depbin);
}

//Overloaded Assignment Operator for COLLECTION_DEPBIN
nsEDDEngine::COLLECTION_DEPBIN& nsEDDEngine::COLLECTION_DEPBIN:: operator=(const COLLECTION_DEPBIN &collection_depbin)
{
	if(this != &collection_depbin)
	{
		AssignCollectionDepbin(collection_depbin);
	}
    return (*this);
}

//AssignCollectionDepbin is used to assign all the attributes of COLLECTION_DEPBIN
void nsEDDEngine::COLLECTION_DEPBIN::AssignCollectionDepbin(const COLLECTION_DEPBIN &collection_depbin)
{
	DEPBIN::CopyFromWithDelete(collection_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_visibility,&db_visibility);
	DEPBIN::CopyFromWithDelete(collection_depbin.db_private_attr,&db_private_attr);
}

nsEDDEngine::COLLECTION_DEPBIN::~COLLECTION_DEPBIN()
{
}

//copy constructor for FLAT_ITEM_ARRAY
nsEDDEngine::FLAT_ITEM_ARRAY::FLAT_ITEM_ARRAY(const FLAT_ITEM_ARRAY &flat_item_array):ItemBase(ITYPE_ITEM_ARRAY)
{
	AssignFlatItemArray(flat_item_array);
}

//Overloaded Assignment Operator for FLAT_ITEM_ARRAY
nsEDDEngine::FLAT_ITEM_ARRAY& nsEDDEngine::FLAT_ITEM_ARRAY:: operator=(const FLAT_ITEM_ARRAY &flat_item_array)
{
	if(this != &flat_item_array)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }

		AssignFlatItemArray(flat_item_array);
	}
    return (*this);
}

void nsEDDEngine::FLAT_ITEM_ARRAY::AssignFlatItemArray(const FLAT_ITEM_ARRAY &flat_item_array)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_item_array);

	item_info	= flat_item_array.item_info;
	subtype		= flat_item_array.subtype;
	elements	= flat_item_array.elements;
	help		= flat_item_array.help;
	label		= flat_item_array.label;
	valid		= flat_item_array.valid;
	visibility	= flat_item_array.visibility;
	private_attr= flat_item_array.private_attr;

	depbin      = new ITEM_ARRAY_DEPBIN(*flat_item_array.depbin);
}

nsEDDEngine::FLAT_ITEM_ARRAY::~FLAT_ITEM_ARRAY()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_ITEM_ARRAY::All_Attrs =
	nsEDDEngine::label| nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::elements | nsEDDEngine::visibility;

//copy constructor for ITEM_ARRAY_DEPBIN
nsEDDEngine::ITEM_ARRAY_DEPBIN::ITEM_ARRAY_DEPBIN(const ITEM_ARRAY_DEPBIN &item_array_depbin): db_item_information(0), db_elements(0), db_help(0), db_label(0), db_valid(0), db_private_attr(0), db_visibility(0)
{
	AssignItemArrayDepbin(item_array_depbin);
}

//Overloaded Assignment Operator for ITEM_ARRAY_DEPBIN
nsEDDEngine::ITEM_ARRAY_DEPBIN& nsEDDEngine::ITEM_ARRAY_DEPBIN:: operator=(const ITEM_ARRAY_DEPBIN &item_array_depbin)
{
	if(this != &item_array_depbin)
	{
			AssignItemArrayDepbin(item_array_depbin);
	}
    return (*this);
}

//AssignItemArrayDepbin is used to assign all the attributes of ITEM_ARRAY_DEPBIN
void nsEDDEngine::ITEM_ARRAY_DEPBIN::AssignItemArrayDepbin(const ITEM_ARRAY_DEPBIN &item_array_depbin)
{
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_elements,&db_elements);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_visibility,&db_visibility);
	DEPBIN::CopyFromWithDelete(item_array_depbin.db_private_attr,&db_private_attr);
}

nsEDDEngine::ITEM_ARRAY_DEPBIN::~ITEM_ARRAY_DEPBIN()
{
}

//copy constructor for FLAT_COMMAND
nsEDDEngine::FLAT_COMMAND::FLAT_COMMAND(const FLAT_COMMAND &flat_command):ItemBase(ITYPE_COMMAND)
{
	AssignFlatCommand(flat_command);
}

//Overloaded Assignment Operator for FLAT_COMMAND
nsEDDEngine::FLAT_COMMAND& nsEDDEngine::FLAT_COMMAND:: operator=(const FLAT_COMMAND &flat_command)
{
	if(this != &flat_command)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatCommand(flat_command);
	}
    return (*this);
}

void nsEDDEngine::FLAT_COMMAND::AssignFlatCommand(const FLAT_COMMAND &flat_command)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_command);

	item_info	= flat_command.item_info;
	number		= flat_command.number;
	oper		= flat_command.oper;
	trans		= flat_command.trans;
	resp_codes	= flat_command.resp_codes;
	index		= flat_command.index;
	slot		= flat_command.slot;
	sub_slot	= flat_command.sub_slot;
	block_b		= flat_command.block_b;
	connection	= flat_command.connection;
	api			= flat_command.api;
	header		= flat_command.header;

	depbin		= new COMMAND_DEPBIN(*flat_command.depbin);
}

nsEDDEngine::FLAT_COMMAND::~FLAT_COMMAND()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COMMAND::All_Attrs =
	nsEDDEngine::response_codes | nsEDDEngine::command_number | nsEDDEngine::block_b |nsEDDEngine::index | 
	nsEDDEngine::operation | nsEDDEngine::slot | nsEDDEngine::sub_slot | nsEDDEngine::transaction | nsEDDEngine::api | nsEDDEngine::header;

//copy constructor for COMMAND_DEPBIN
nsEDDEngine::COMMAND_DEPBIN::COMMAND_DEPBIN(const COMMAND_DEPBIN &command_depbin): db_item_information(0), db_number(0), db_oper(0), db_trans(0), db_resp_codes(0), 
				db_index(0), db_slot(0), db_sub_slot(0), db_block_b(0), db_connection(0), db_api(0), db_header(0)
{
	AssignFlatCommandDepbin(command_depbin);
}

//Overloaded Assignment Operator for COMMAND_DEPBIN
nsEDDEngine::COMMAND_DEPBIN& nsEDDEngine::COMMAND_DEPBIN:: operator=(const COMMAND_DEPBIN &command_depbin)
{
	if(this != &command_depbin)
	{
		AssignFlatCommandDepbin(command_depbin);
	}
    return (*this);
}

//AssignFlatCommand is used to assign all the attributes of COMMAND_DEPBIN
void nsEDDEngine::COMMAND_DEPBIN::AssignFlatCommandDepbin(const COMMAND_DEPBIN &command_depbin)
{
	DEPBIN::CopyFromWithDelete(command_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(command_depbin.db_number,&db_number);
	DEPBIN::CopyFromWithDelete(command_depbin.db_oper,&db_oper);
	DEPBIN::CopyFromWithDelete(command_depbin.db_trans,&db_trans);
	DEPBIN::CopyFromWithDelete(command_depbin.db_resp_codes,&db_resp_codes);
	DEPBIN::CopyFromWithDelete(command_depbin.db_index,&db_index);
	DEPBIN::CopyFromWithDelete(command_depbin.db_slot,&db_slot);
	DEPBIN::CopyFromWithDelete(command_depbin.db_sub_slot,&db_sub_slot);
	DEPBIN::CopyFromWithDelete(command_depbin.db_block_b,&db_block_b);
	DEPBIN::CopyFromWithDelete(command_depbin.db_connection,&db_connection);
	DEPBIN::CopyFromWithDelete(command_depbin.db_api,&db_api);
	DEPBIN::CopyFromWithDelete(command_depbin.db_header,&db_header);
}

nsEDDEngine::COMMAND_DEPBIN::~COMMAND_DEPBIN()
{
}

//copy constructor for FLAT_LIST
nsEDDEngine::FLAT_LIST::FLAT_LIST(const FLAT_LIST &flat_list):ItemBase(ITYPE_LIST)
{
	AssignFlatList(flat_list);
}

//Overloaded Assignment Operator for FLAT_LIST
nsEDDEngine::FLAT_LIST& nsEDDEngine::FLAT_LIST:: operator=(const FLAT_LIST &flat_list)
{
	if(this != &flat_list)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatList(flat_list);
	}
    return (*this);
}

void nsEDDEngine::FLAT_LIST::AssignFlatList(const FLAT_LIST &flat_list)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_list);

	item_info	= flat_list.item_info;
	help		= flat_list.help;
	label		= flat_list.label;
	valid		= flat_list.valid;
	type		= flat_list.type;
	capacity	= flat_list.capacity;
	count		= flat_list.count;
	count_ref	= flat_list.count_ref;
	
	private_attr= flat_list.private_attr;
	visibility	= flat_list.visibility;

	depbin		= new LIST_DEPBIN(*flat_list.depbin);
}

nsEDDEngine::FLAT_LIST::~FLAT_LIST()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_LIST::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::type_definition | 
	nsEDDEngine::capacity | nsEDDEngine::count | nsEDDEngine::private_attr | nsEDDEngine::visibility | nsEDDEngine::count_ref;

//copy constructor
nsEDDEngine::LIST_DEPBIN::LIST_DEPBIN(const LIST_DEPBIN &list_depbin): db_item_information(0), db_help(0), db_label(0), db_valid(0), db_type(0),
						db_capacity(0), db_count(0), db_private_attr(0), db_visibility(0)
{
	AssignListDepbin(list_depbin);
}

//Overloaded Assignment Operator for LIST_DEPBIN
nsEDDEngine::LIST_DEPBIN& nsEDDEngine::LIST_DEPBIN:: operator=(const LIST_DEPBIN &list_depbin)
{
	if(this != &list_depbin)
	{
		AssignListDepbin(list_depbin);
	}
    return (*this);
}

//AssignListDepbin is used to assign all the attributes of LIST_DEPBIN
void nsEDDEngine::LIST_DEPBIN::AssignListDepbin(const LIST_DEPBIN &list_depbin)
{
	DEPBIN::CopyFromWithDelete(list_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(list_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(list_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(list_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(list_depbin.db_type,&db_type);
	DEPBIN::CopyFromWithDelete(list_depbin.db_capacity,&db_capacity);
	DEPBIN::CopyFromWithDelete(list_depbin.db_count,&db_count);
	DEPBIN::CopyFromWithDelete(list_depbin.db_private_attr,&db_private_attr);
	DEPBIN::CopyFromWithDelete(list_depbin.db_visibility,&db_visibility);
}


nsEDDEngine::LIST_DEPBIN::~LIST_DEPBIN()
{
}

//copy constructor for FLAT_FILE
nsEDDEngine::FLAT_FILE::FLAT_FILE(const FLAT_FILE &flat_file):ItemBase(ITYPE_FILE)
{
	AssignFlatFile(flat_file);
}

//Overloaded Assignment Operator for FLAT_FILE
nsEDDEngine::FLAT_FILE& nsEDDEngine::FLAT_FILE:: operator=(const FLAT_FILE &flat_file)
{
	if(this != &flat_file)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatFile(flat_file);
	}
    return (*this);
}

void nsEDDEngine::FLAT_FILE::AssignFlatFile(const FLAT_FILE &flat_file)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_file);

	item_info			= flat_file.item_info;
	help				= flat_file.help;
	label				= flat_file.label;
	members				= flat_file.members;
	handling			= flat_file.handling;
	identity			= flat_file.identity;
	on_update_actions	= flat_file.on_update_actions;
	shared				= flat_file.shared;

	depbin				= new FILE_DEPBIN (*flat_file.depbin);
}

nsEDDEngine::FLAT_FILE::~FLAT_FILE()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_FILE::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::members | nsEDDEngine::handling |
	nsEDDEngine::identity | nsEDDEngine::on_update_actions | nsEDDEngine::shared ;

//copy constructor
nsEDDEngine::FILE_DEPBIN::FILE_DEPBIN(const FILE_DEPBIN &file_depbin): db_item_information(0), db_help(0), db_label(0), db_members(0), db_handling(0), 
			db_identity(0), db_on_update_actions(0), db_shared(0)
{
	AssignFileDepbin(file_depbin);
}

//Overloaded Assignment Operator
nsEDDEngine::FILE_DEPBIN& nsEDDEngine::FILE_DEPBIN:: operator=(const FILE_DEPBIN &file_depbin)
{
	if(this != &file_depbin)
	{
		AssignFileDepbin(file_depbin);
	}
    return (*this);

}

//AssignFileDepbin is used to assign all the attributes of FILE_DEPBIN
void nsEDDEngine::FILE_DEPBIN::AssignFileDepbin(const FILE_DEPBIN &file_depbin)
{
	DEPBIN::CopyFromWithDelete(file_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(file_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(file_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(file_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(file_depbin.db_handling,&db_handling);
	DEPBIN::CopyFromWithDelete(file_depbin.db_identity,&db_identity);
	DEPBIN::CopyFromWithDelete(file_depbin.db_on_update_actions,&db_on_update_actions);
	DEPBIN::CopyFromWithDelete(file_depbin.db_shared,&db_shared);
}

nsEDDEngine::FILE_DEPBIN::~FILE_DEPBIN()
{
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_PLUGIN::All_Attrs = 
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::uuid | nsEDDEngine::visibility;

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_TEMPLATE::All_Attrs = 
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::default_values | nsEDDEngine::validity;

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_RESP_CODE::All_Attrs = nsEDDEngine::help | nsEDDEngine::members;

//copy constructor for FLAT_RESP_CODE
nsEDDEngine::FLAT_RESP_CODE::FLAT_RESP_CODE(const FLAT_RESP_CODE &flat_resp_code):ItemBase(ITYPE_RESP_CODES)
{
	AssignFlatRespCode(flat_resp_code);
}

//Overloaded Assignment Operator for FLAT_RESP_CODE
nsEDDEngine::FLAT_RESP_CODE& nsEDDEngine::FLAT_RESP_CODE:: operator=(const FLAT_RESP_CODE &flat_resp_code)
{
	if(this != &flat_resp_code)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatRespCode(flat_resp_code);
	}
    return (*this);
}

void nsEDDEngine::FLAT_RESP_CODE::AssignFlatRespCode(const FLAT_RESP_CODE &flat_resp_code)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_resp_code);

	item_info	= flat_resp_code.item_info;
	member		= flat_resp_code.member;

	depbin		= new RESP_CODE_DEPBIN(*flat_resp_code.depbin);
}

nsEDDEngine::FLAT_RESP_CODE::~FLAT_RESP_CODE()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

//copy constructor for RESP_CODE_DEPBIN
nsEDDEngine::RESP_CODE_DEPBIN::RESP_CODE_DEPBIN(const RESP_CODE_DEPBIN &resp_code_depbin) : db_item_information(0), db_member(0)
{
	AssignRespCodeDepbin(resp_code_depbin);
}

//Overloaded Assignment Operator for RESP_CODE_DEPBIN
nsEDDEngine::RESP_CODE_DEPBIN& nsEDDEngine::RESP_CODE_DEPBIN:: operator=(const RESP_CODE_DEPBIN &resp_code_depbin)
{
	if(this != &resp_code_depbin)
	{
			AssignRespCodeDepbin(resp_code_depbin);
	}
    return (*this);
}

//AssignRespCodeDepbin is used to assign all the attributes of RESP_CODE_DEPBIN
void nsEDDEngine::RESP_CODE_DEPBIN::AssignRespCodeDepbin(const RESP_CODE_DEPBIN &resp_code_depbin)
{
	DEPBIN::CopyFromWithDelete(resp_code_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(resp_code_depbin.db_member,&db_member);
}

nsEDDEngine::RESP_CODE_DEPBIN::~RESP_CODE_DEPBIN()
{
}

//copy constructor for FLAT_VAR
nsEDDEngine::FLAT_VAR::FLAT_VAR(const FLAT_VAR &flat_var):ItemBase(ITYPE_VARIABLE)
{
	AssignFlatVar(flat_var);
}

//Overloaded Assignment Operator for FLAT_VAR
nsEDDEngine::FLAT_VAR& nsEDDEngine::FLAT_VAR:: operator=(const FLAT_VAR &flat_var)
{
	if(this != &flat_var)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatVar(flat_var);
	}
    return (*this);
}

void nsEDDEngine::FLAT_VAR::AssignFlatVar(const FLAT_VAR &flat_var)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_var);

	item_info	= flat_var.item_info;

	class_attr	= flat_var.class_attr;			
	handling	= flat_var.handling;			
	label		= flat_var.label;
	help		= flat_var.help;				

	read_time_out	= flat_var.read_time_out;		
	write_time_out	= flat_var.write_time_out;
	valid			= flat_var.valid;


	pre_read_act	= flat_var.pre_read_act;
	post_read_act	= flat_var.post_read_act;
	pre_write_act	= flat_var.pre_write_act;
	post_write_act	= flat_var.post_write_act;
	pre_edit_act	= flat_var.pre_edit_act;
	post_edit_act	= flat_var.post_edit_act;

	resp_codes		= flat_var.resp_codes;
	type_size		= flat_var.type_size;	// Need to implement
	display			= flat_var.display;
	edit			= flat_var.edit;

	min_val			= flat_var.min_val;
	max_val			= flat_var.max_val;

	scaling_factor	= flat_var.scaling_factor;
	enums			= flat_var.enums;	// Need to implement

	default_value	= flat_var.default_value;
	refresh_act		= flat_var.refresh_act;

	style			= flat_var.style;
	constant_unit	= flat_var.constant_unit;

	height			= flat_var.height;
	width			= flat_var.width;

	time_format		= flat_var.time_format;
	time_scale		= flat_var.time_scale;

	indexed			= flat_var.indexed;
	initial_value	= flat_var.initial_value;
	
	post_user_action	= flat_var.post_user_action;
	post_rqst_actions	= flat_var.post_rqst_actions;

	private_attr		= flat_var.private_attr;
	visibility			= flat_var.visibility;
	write_mode			= flat_var.write_mode;	// Need to implement

	depbin	= new VAR_DEPBIN(*flat_var.depbin);

}

nsEDDEngine::FLAT_VAR::~FLAT_VAR()
{
	FreeDepbins();
	delete depbin; 
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_VAR::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::variable_type | nsEDDEngine::handling |
	nsEDDEngine::response_codes | nsEDDEngine::post_edit_actions | nsEDDEngine::pre_edit_actions | nsEDDEngine::refresh_actions |
	nsEDDEngine::class_attr | nsEDDEngine::constant_unit |nsEDDEngine::max_value | nsEDDEngine::min_value |  nsEDDEngine::post_read_actions |
	nsEDDEngine::post_write_actions |nsEDDEngine::pre_read_actions | nsEDDEngine::pre_write_actions | nsEDDEngine::indexed | 
	nsEDDEngine::default_value | nsEDDEngine::display_format | nsEDDEngine::edit_format | nsEDDEngine::enumerations | 
	nsEDDEngine::initial_value | nsEDDEngine::post_user_actions | nsEDDEngine::post_rqst_actions |
	nsEDDEngine::scaling_factor |nsEDDEngine::time_format | 
	nsEDDEngine::time_scale | nsEDDEngine::height | nsEDDEngine::width |nsEDDEngine::private_attr |
	nsEDDEngine::visibility | nsEDDEngine::write_mode;

//copy constructor for VAR_DEPBIN
nsEDDEngine::VAR_DEPBIN::VAR_DEPBIN(const VAR_DEPBIN &var_depbin): db_class(0), db_handling(0), db_constant_unit(0), db_label(0), db_help(0), db_valid(0), db_pre_read_act(0), db_post_read_act(0),
			db_pre_write_act(0), db_post_write_act(0), db_pre_edit_act(0), db_post_edit_act(0), db_type_size(0), db_display(0)
			, db_edit(0), db_min_val(0), db_max_val(0), db_enums(0), db_default_value(0), db_refresh_act(0)
			, db_indexed(0), db_initial_value(0)
			, db_post_user_action(0), db_post_rqst_actions(0), db_private_attr(0), db_visibility(0)
			, db_height(0), db_width(0), db_time_scale(0), db_time_format(0), db_scaling_factor(0), db_response_codes(0), db_item_information(0), db_write_mode(0)
{
	AssignVarDepbin(var_depbin);
}

//Overloaded Assignment Operator for VAR_DEPBIN
nsEDDEngine::VAR_DEPBIN& nsEDDEngine::VAR_DEPBIN:: operator=(const VAR_DEPBIN &var_depbin)
{
	if(this != &var_depbin)
	{
		AssignVarDepbin(var_depbin);
	}
    return (*this);
}

//AssignVarDepbin function to assign all the values
void nsEDDEngine::VAR_DEPBIN::AssignVarDepbin(const VAR_DEPBIN &var_depbin)
{
	DEPBIN::CopyFromWithDelete(var_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(var_depbin.db_class,&db_class);
	DEPBIN::CopyFromWithDelete(var_depbin.db_handling,&db_handling);
	DEPBIN::CopyFromWithDelete(var_depbin.db_constant_unit,&db_constant_unit);
	DEPBIN::CopyFromWithDelete(var_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(var_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(var_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(var_depbin.db_pre_read_act,&db_pre_read_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_post_read_act,&db_post_read_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_pre_write_act,&db_pre_write_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_post_write_act,&db_post_write_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_pre_edit_act,&db_pre_edit_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_post_edit_act,&db_post_edit_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_type_size,&db_type_size);
	DEPBIN::CopyFromWithDelete(var_depbin.db_display,&db_display);
	DEPBIN::CopyFromWithDelete(var_depbin.db_edit,&db_edit);
	DEPBIN::CopyFromWithDelete(var_depbin.db_min_val,&db_min_val);
	DEPBIN::CopyFromWithDelete(var_depbin.db_max_val,&db_max_val);
	DEPBIN::CopyFromWithDelete(var_depbin.db_enums,&db_enums);
	DEPBIN::CopyFromWithDelete(var_depbin.db_default_value,&db_default_value);
	DEPBIN::CopyFromWithDelete(var_depbin.db_refresh_act,&db_refresh_act);
	DEPBIN::CopyFromWithDelete(var_depbin.db_indexed,&db_indexed);
	DEPBIN::CopyFromWithDelete(var_depbin.db_initial_value,&db_initial_value);
	DEPBIN::CopyFromWithDelete(var_depbin.db_post_user_action,&db_post_user_action);
	DEPBIN::CopyFromWithDelete(var_depbin.db_post_rqst_actions,&db_post_rqst_actions);
	DEPBIN::CopyFromWithDelete(var_depbin.db_private_attr,&db_private_attr);
	DEPBIN::CopyFromWithDelete(var_depbin.db_visibility,&db_visibility);
	DEPBIN::CopyFromWithDelete(var_depbin.db_height,&db_height);
	DEPBIN::CopyFromWithDelete(var_depbin.db_width,&db_width);
	DEPBIN::CopyFromWithDelete(var_depbin.db_time_scale,&db_time_scale);
	DEPBIN::CopyFromWithDelete(var_depbin.db_time_format,&db_time_format);
	DEPBIN::CopyFromWithDelete(var_depbin.db_scaling_factor,&db_scaling_factor);
	DEPBIN::CopyFromWithDelete(var_depbin.db_response_codes,&db_response_codes);
	DEPBIN::CopyFromWithDelete(var_depbin.db_write_mode,&db_write_mode);
}



nsEDDEngine::VAR_DEPBIN::~VAR_DEPBIN()
{
}

//copy constructor for FLAT_CHART
nsEDDEngine::FLAT_CHART::FLAT_CHART(const FLAT_CHART &flat_chart):ItemBase(ITYPE_CHART)
{
	AssignFlatChart(flat_chart);
}

//Overloaded Assignment Operator for FLAT_CHART
nsEDDEngine::FLAT_CHART& nsEDDEngine::FLAT_CHART:: operator=(const FLAT_CHART &flat_chart)
{
	if(this != &flat_chart)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatChart(flat_chart);
	}
    return (*this);
}

//AssignFlatChart is used to assign all the attributes of FLAT_CHART
void nsEDDEngine::FLAT_CHART::AssignFlatChart(const FLAT_CHART &flat_chart)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_chart);

	item_info	= flat_chart.item_info;
	help		= flat_chart.help;
	label		= flat_chart.label;
	members		= flat_chart.members;
	height		= flat_chart.height;
	width		= flat_chart.width;
	cycle_time	= flat_chart.cycle_time;

	length		= flat_chart.length;
	type		= flat_chart.type;
	valid		= flat_chart.valid;
	visibility	= flat_chart.visibility;

	depbin		= new CHART_DEPBIN(*flat_chart.depbin);
}

nsEDDEngine::FLAT_CHART::~FLAT_CHART()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_CHART::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::chart_type |
	nsEDDEngine::members | nsEDDEngine::height | nsEDDEngine::width | nsEDDEngine::cycle_time |
	nsEDDEngine::length | nsEDDEngine::visibility;

nsEDDEngine::CHART_DEPBIN::CHART_DEPBIN(const CHART_DEPBIN &chart_depbin): db_item_information(0), db_help(0), db_label(0), db_members(0), db_cycle_time(0), db_height(0), 
			db_width(0), db_length(0), db_type(0), db_valid(0), db_visibility(0)
{
	AssignChartDepbin(chart_depbin);
}

//Overloaded Assignment Operator for FLAT_CHART
nsEDDEngine::CHART_DEPBIN& nsEDDEngine::CHART_DEPBIN:: operator=(const CHART_DEPBIN &chart_depbin)
{
	if(this != &chart_depbin)
	{
		AssignChartDepbin(chart_depbin);
	}
    return (*this);

}

//AssignChartDepbin is used to assign all the attributes of CHART_DEPBIN
void nsEDDEngine::CHART_DEPBIN::AssignChartDepbin(const CHART_DEPBIN &chart_depbin)
{
	DEPBIN::CopyFromWithDelete(chart_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_cycle_time,&db_cycle_time);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_height,&db_height);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_width,&db_width);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_length,&db_length);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_type,&db_type);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(chart_depbin.db_visibility,&db_visibility);
}

//copy constructor for CHART_DEPBIN
nsEDDEngine::CHART_DEPBIN::~CHART_DEPBIN()
{
}

//copy constructor for FLAT_GRAPH
nsEDDEngine::FLAT_GRAPH::FLAT_GRAPH(const FLAT_GRAPH &flat_graph):ItemBase(ITYPE_GRAPH)
{
	AssignFlatGraph(flat_graph);
}

//Overloaded Assignment Operator for FLAT_GRAPH
nsEDDEngine::FLAT_GRAPH& nsEDDEngine::FLAT_GRAPH:: operator=(const FLAT_GRAPH &flat_graph)
{
	if(this != &flat_graph)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatGraph(flat_graph);
	}
    return (*this);
}

void nsEDDEngine::FLAT_GRAPH::AssignFlatGraph(const FLAT_GRAPH &flat_graph)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_graph);

	item_info	= flat_graph.item_info;
	help		= flat_graph.help;
	label		= flat_graph.label;
	members		= flat_graph.members;
	height		= flat_graph.height;
	width		= flat_graph.width;
	x_axis		= flat_graph.x_axis;
	valid		= flat_graph.valid;
	cycle_time	= flat_graph.cycle_time;
	visibility	= flat_graph.visibility;

	depbin		= new GRAPH_DEPBIN(*flat_graph.depbin);
}

nsEDDEngine::FLAT_GRAPH::~FLAT_GRAPH()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_GRAPH::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity  | nsEDDEngine::members | nsEDDEngine::height | 
	nsEDDEngine::width | nsEDDEngine::cycle_time | nsEDDEngine::x_axis | nsEDDEngine::visibility;

//Overloaded Copy Constructor
nsEDDEngine::GRAPH_DEPBIN::GRAPH_DEPBIN(const GRAPH_DEPBIN &graph_depbin): db_item_information(0), db_help(0), db_label(0), db_members(0), db_cycle_time(0), db_height(0), 
			db_width(0), db_x_axis(0), db_valid(0), db_visibility(0)
{
	AssignGraphDepbin(graph_depbin);
}

//Overloaded Assignment Operator for GRAPH_DEPBIN
nsEDDEngine::GRAPH_DEPBIN& nsEDDEngine::GRAPH_DEPBIN:: operator=(const GRAPH_DEPBIN &graph_depbin)
{
	if(this != &graph_depbin)
	{
		AssignGraphDepbin(graph_depbin);
	}
    return (*this);
}

//AssignGraphDepbin is used to assign all the attributes of GRAPH_DEPBIN
void nsEDDEngine::GRAPH_DEPBIN::AssignGraphDepbin(const GRAPH_DEPBIN &graph_depbin)
{
	DEPBIN::CopyFromWithDelete(graph_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_help	,&db_help);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_cycle_time,&db_cycle_time);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_height,&db_height);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_width,&db_width);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_x_axis,&db_x_axis);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(graph_depbin.db_visibility,&db_visibility);
}

nsEDDEngine::GRAPH_DEPBIN::~GRAPH_DEPBIN()
{
}

//copy constructor for FLAT_AXIS
nsEDDEngine::FLAT_AXIS::FLAT_AXIS(const FLAT_AXIS &flat_axis):ItemBase(ITYPE_AXIS)
{
	AssignFlatAxis(flat_axis);
}

//Overloaded Assignment Operator for FLAT_AXIS
nsEDDEngine::FLAT_AXIS& nsEDDEngine::FLAT_AXIS:: operator=(const FLAT_AXIS &flat_axis)
{
	if(this != &flat_axis)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatAxis(flat_axis);
	}
    return (*this);
}

void nsEDDEngine::FLAT_AXIS::AssignFlatAxis(const FLAT_AXIS &flat_axis)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_axis);

	item_info	= flat_axis.item_info;
	help		= flat_axis.help;
	label		= flat_axis.label;

	min_axis	= flat_axis.min_axis;
	min_axis_ref= flat_axis.min_axis_ref;

	max_axis	= flat_axis.max_axis;
	max_axis_ref= flat_axis.max_axis_ref;

	scaling		= flat_axis.scaling;
	constant_unit	= flat_axis.constant_unit;

	depbin		= new AXIS_DEPBIN(*flat_axis.depbin);
}

nsEDDEngine::FLAT_AXIS::~FLAT_AXIS()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_AXIS::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::constant_unit | nsEDDEngine::scaling 
	// FDI-2041 v3.0 uses min_axis and max_axis: HART and FF Tokenizers currently use this.
	// This should become obsolete when everyone moves to v4.?
	//| nsEDDEngine::max_axis | nsEDDEngine::min_axis 
	// FDI-2041 v3.1 uses min_value and max_value: PB uses this
	| nsEDDEngine::max_value | nsEDDEngine::min_value 
	| nsEDDEngine::max_value_ref | nsEDDEngine::min_value_ref; 

//copy constructor for AXIS_DEPBIN
nsEDDEngine::AXIS_DEPBIN::AXIS_DEPBIN(const AXIS_DEPBIN &axis_depbin) : db_item_information(0), db_help(0), db_label(0), db_min_axis(0), db_max_axis(0),
			db_scaling(0), db_constant_unit(0)
{
	AssignAxisDepbin(axis_depbin);
}

//Overloaded Assignment Operator for AXIS_DEPBIN
nsEDDEngine::AXIS_DEPBIN& nsEDDEngine::AXIS_DEPBIN:: operator=(const AXIS_DEPBIN &axis_depbin)
{
	if(this != &axis_depbin)
	{
		AssignAxisDepbin(axis_depbin);
	}
    return (*this);
}

//AssignAxisDepbin function to assign all the attributes of AXIS_DEPBIN
void nsEDDEngine::AXIS_DEPBIN::AssignAxisDepbin(const AXIS_DEPBIN &axis_depbin)
{
	DEPBIN::CopyFromWithDelete(axis_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_min_axis,&db_min_axis);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_max_axis,&db_max_axis);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_scaling,&db_scaling);
	DEPBIN::CopyFromWithDelete(axis_depbin.db_constant_unit,&db_constant_unit);
}


nsEDDEngine::AXIS_DEPBIN::~AXIS_DEPBIN()
{
}

//copy constructor for FLAT_WAVEFORM
nsEDDEngine::FLAT_WAVEFORM::FLAT_WAVEFORM(const FLAT_WAVEFORM &flat_waveform):ItemBase(ITYPE_WAVEFORM)
{
	AssignFlatWaveform(flat_waveform);
}

//Overloaded Assignment Operator for FLAT_WAVEFORM
nsEDDEngine::FLAT_WAVEFORM& nsEDDEngine::FLAT_WAVEFORM:: operator=(const FLAT_WAVEFORM &flat_waveform)
{
	if(this != &flat_waveform)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatWaveform(flat_waveform);
	}
    return (*this);
}

void nsEDDEngine::FLAT_WAVEFORM::AssignFlatWaveform(const FLAT_WAVEFORM &flat_waveform)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_waveform);

	item_info		= flat_waveform.item_info;
	help			= flat_waveform.help;
	label			= flat_waveform.label;
	valid			= flat_waveform.valid;
	emphasis		= flat_waveform.emphasis;
	line_type		= flat_waveform.line_type;
	handling		= flat_waveform.handling;
	init_actions	= flat_waveform.init_actions;
	refresh_actions	= flat_waveform.refresh_actions;
	exit_actions	= flat_waveform.exit_actions;
	x_initial		= flat_waveform.x_initial;
	x_increment		= flat_waveform.x_increment;
	number_of_points= flat_waveform.number_of_points;
	x_values		= flat_waveform.x_values;
	y_values		= flat_waveform.y_values;
	key_x_values	= flat_waveform.key_x_values;
	key_y_values	= flat_waveform.key_y_values;
	line_color		= flat_waveform.line_color;
	y_axis			= flat_waveform.y_axis;
	waveform_type	= flat_waveform.waveform_type;

	visibility		= flat_waveform.visibility;

	depbin			= new WAVEFORM_DEPBIN(*flat_waveform.depbin);
}

nsEDDEngine::FLAT_WAVEFORM::~FLAT_WAVEFORM()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_WAVEFORM::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::waveform_type | nsEDDEngine::handling | 
	nsEDDEngine::refresh_actions | nsEDDEngine::emphasis | nsEDDEngine::exit_actions | nsEDDEngine::init_actions | 
	nsEDDEngine::line_color | nsEDDEngine::line_type | nsEDDEngine::y_axis | nsEDDEngine::keypoints_x_values | 
	nsEDDEngine::keypoints_y_values | nsEDDEngine::number_of_points | nsEDDEngine::x_increment | nsEDDEngine::x_initial |
	nsEDDEngine::x_values | nsEDDEngine::y_values | nsEDDEngine::visibility;


nsEDDEngine::WAVEFORM_DEPBIN::WAVEFORM_DEPBIN(const WAVEFORM_DEPBIN &waveform_depbin) : db_item_information(0), db_help(0), db_label(0), db_valid(0), db_emphasis(0), db_line_type(0),
			db_handling(0), db_init_actions(0), db_refresh_actions(0), db_exit_actions(0), db_type(0), 
			db_x_initial(0), db_x_increment(0), db_number_of_points(0), db_x_values(0), db_y_values(0), 
			db_key_x_values(0), db_key_y_values(0), db_line_color(0), db_y_axis(0), db_visibility(0), db_waveform_type(0)
{
	AssignWaveformDepbin(waveform_depbin);
}

//Overloaded Assignment Operator for WAVEFORM_DEPBIN
nsEDDEngine::WAVEFORM_DEPBIN& nsEDDEngine::WAVEFORM_DEPBIN:: operator=(const WAVEFORM_DEPBIN &waveform_depbin)
{
	if(this != &waveform_depbin)
	{
		AssignWaveformDepbin(waveform_depbin);
	}
    return (*this);
}

//AssignWaveformDepbin is used to assign all the attributes of  WAVEFORM_DEPBIN
void nsEDDEngine::WAVEFORM_DEPBIN::AssignWaveformDepbin(const WAVEFORM_DEPBIN &waveform_depbin)
{
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_emphasis,&db_emphasis);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_line_type,&db_line_type);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_handling,&db_handling);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_init_actions,&db_init_actions);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_refresh_actions,&db_refresh_actions);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_exit_actions,&db_exit_actions);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_type,&db_type);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_x_initial,&db_x_initial);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_x_increment,&db_x_increment);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_number_of_points,&db_number_of_points);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_x_values,&db_x_values);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_y_values,&db_y_values);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_key_x_values,&db_key_x_values);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_key_y_values,&db_key_y_values);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_line_color,&db_line_color);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_y_axis,&db_y_axis);
	DEPBIN::CopyFromWithDelete(waveform_depbin.db_waveform_type,&db_waveform_type);
}

nsEDDEngine::WAVEFORM_DEPBIN::~WAVEFORM_DEPBIN()
{
}

//copy constructor for FLAT_SOURCE
nsEDDEngine::FLAT_SOURCE::FLAT_SOURCE(const FLAT_SOURCE &flat_source):ItemBase(ITYPE_SOURCE)
{
	AssignFlatSource(flat_source);
}

//Overloaded Assignment Operator for FLAT_SOURCE
nsEDDEngine::FLAT_SOURCE& nsEDDEngine::FLAT_SOURCE:: operator=(const FLAT_SOURCE &flat_source)
{
	if(this != &flat_source)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatSource(flat_source);
	}
    return (*this);
}

void nsEDDEngine::FLAT_SOURCE::AssignFlatSource(const FLAT_SOURCE &flat_source)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_source);

	item_info	= flat_source.item_info;
	help		= flat_source.help;
	label		= flat_source.label;
	op_members	= flat_source.op_members;
	emphasis	= flat_source.emphasis;
	y_axis		= flat_source.y_axis;
	line_type	= flat_source.line_type;
	line_color	= flat_source.line_color;
	init_actions= flat_source.init_actions;
	refresh_actions	= flat_source.refresh_actions;
	exit_actions	= flat_source.exit_actions;
	valid		= flat_source.valid;
	visibility	= flat_source.visibility;

	depbin		= new SOURCE_DEPBIN(*flat_source.depbin);
}

nsEDDEngine::FLAT_SOURCE::~FLAT_SOURCE()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_SOURCE::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity  | nsEDDEngine::members | nsEDDEngine::refresh_actions | 
	nsEDDEngine::emphasis | nsEDDEngine::exit_actions | nsEDDEngine::init_actions | nsEDDEngine::line_color | 
	nsEDDEngine::line_type | nsEDDEngine::y_axis | nsEDDEngine::visibility; 


nsEDDEngine::SOURCE_DEPBIN::SOURCE_DEPBIN(const SOURCE_DEPBIN &source_depbin):db_item_information(0), db_help(0), db_label(0), db_members(0), db_emphasis(0), db_y_axis(0), db_line_type(0), 
			db_line_color(0), db_init_actions(0), db_refresh_actions(0), db_exit_actions(0), db_valid(0), db_visibility(0)
{
	AssignSourceDepbin(source_depbin);
}


//Overloaded Assignment Operator for SOURCE_DEPBIN
nsEDDEngine::SOURCE_DEPBIN& nsEDDEngine::SOURCE_DEPBIN:: operator= (const SOURCE_DEPBIN &source_depbin)
{
	if(this != &source_depbin)
	{
		AssignSourceDepbin(source_depbin);
	}
    return (*this);
}

//AssignSourceDepbin is used to assign all the attributes of SOURCE_DEPBIN
void nsEDDEngine::SOURCE_DEPBIN::AssignSourceDepbin(const SOURCE_DEPBIN &source_depbin)
{
	DEPBIN::CopyFromWithDelete(source_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(source_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(source_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(source_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(source_depbin.db_emphasis,&db_emphasis);
	DEPBIN::CopyFromWithDelete(source_depbin.db_y_axis,&db_y_axis);
	DEPBIN::CopyFromWithDelete(source_depbin.db_line_type,&db_line_type);
	DEPBIN::CopyFromWithDelete(source_depbin.db_line_color,&db_line_color);
	DEPBIN::CopyFromWithDelete(source_depbin.db_init_actions,&db_init_actions);
	DEPBIN::CopyFromWithDelete(source_depbin.db_refresh_actions,&db_refresh_actions);
	DEPBIN::CopyFromWithDelete(source_depbin.db_exit_actions,&db_exit_actions);
	DEPBIN::CopyFromWithDelete(source_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(source_depbin.db_visibility,&db_visibility);
}
nsEDDEngine::SOURCE_DEPBIN::~SOURCE_DEPBIN()
{
}

//copy constructor for FLAT_GRID
nsEDDEngine::FLAT_GRID::FLAT_GRID(const FLAT_GRID &flat_grid):ItemBase(ITYPE_GRID)
{
	AssignFlatGrid(flat_grid);
}

//Overloaded Assignment Operator for FLAT_GRID
nsEDDEngine::FLAT_GRID& nsEDDEngine::FLAT_GRID:: operator=(const FLAT_GRID &flat_grid)
{
	if(this != &flat_grid)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatGrid(flat_grid);
	}
    return (*this);
}

void nsEDDEngine::FLAT_GRID::AssignFlatGrid(const FLAT_GRID &flat_grid)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_grid);

	item_info	= flat_grid.item_info;
	help		= flat_grid.help;
	label		= flat_grid.label;
	vectors		= flat_grid.vectors;
	height		= flat_grid.height;
	width		= flat_grid.width;
	handling	= flat_grid.handling;
	valid		= flat_grid.valid;
	orientation	= flat_grid.orientation;
	visibility	= flat_grid.visibility;

	depbin		= new GRID_DEPBIN(*flat_grid.depbin);
}

nsEDDEngine::FLAT_GRID::~FLAT_GRID()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_GRID::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity  | nsEDDEngine::handling |
	nsEDDEngine::height | nsEDDEngine::width | nsEDDEngine::orientation | nsEDDEngine::vectors | nsEDDEngine::visibility; 


nsEDDEngine::GRID_DEPBIN::GRID_DEPBIN(const GRID_DEPBIN &grid_depbin): db_item_information(0), db_help(0), db_label(0), db_vectors(0), db_height(0), db_width(0), 
			db_handling(0), db_valid(0), db_orientation(0), db_visibility(0)
{
	AssignGridDepbin(grid_depbin);
}

//Overloaded Assignment Operator for GRID_DEPBIN
nsEDDEngine::GRID_DEPBIN& nsEDDEngine::GRID_DEPBIN:: operator=(const GRID_DEPBIN &grid_depbin)
{
	if(this != &grid_depbin)
	{
		AssignGridDepbin(grid_depbin);
	}
    return (*this);
}

//AssignGridDepbin is used to assign all the attributes of GRID_DEPBIN
void nsEDDEngine::GRID_DEPBIN::AssignGridDepbin(const GRID_DEPBIN &grid_depbin)
{
	DEPBIN::CopyFromWithDelete(grid_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_vectors,&db_vectors);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_height,&db_height);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_width,&db_width);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_handling,&db_handling);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_orientation,&db_orientation);
	DEPBIN::CopyFromWithDelete(grid_depbin.db_visibility,&db_visibility);
}

nsEDDEngine::GRID_DEPBIN::~GRID_DEPBIN()
{
}

//copy constructor for FLAT_IMAGE
nsEDDEngine::FLAT_IMAGE::FLAT_IMAGE(const FLAT_IMAGE &flat_image):ItemBase(ITYPE_IMAGE)
{
	AssignFlatImage(flat_image);
}

//Overloaded Assignment Operator for FLAT_IMAGE
nsEDDEngine::FLAT_IMAGE& nsEDDEngine::FLAT_IMAGE:: operator=(const FLAT_IMAGE &flat_image)
{
	if(this != &flat_image)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatImage(flat_image);
	}
    return (*this);
}

void nsEDDEngine::FLAT_IMAGE::AssignFlatImage(const FLAT_IMAGE &flat_image)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_image);

	item_info			= flat_image.item_info;
	help				= flat_image.help;
	label				= flat_image.label;
	link				= flat_image.link;
	valid				= flat_image.valid;
	entry				= flat_image.entry;
	image_table_index	= flat_image.image_table_index;
	image_id			= flat_image.image_id;
	visibility			= flat_image.visibility;

	depbin		= new IMAGE_DEPBIN(*flat_image.depbin);
}

nsEDDEngine::FLAT_IMAGE::~FLAT_IMAGE()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_IMAGE::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity  | nsEDDEngine::image_table_index | nsEDDEngine::link | nsEDDEngine::visibility | nsEDDEngine::image_id;


nsEDDEngine::IMAGE_DEPBIN::IMAGE_DEPBIN(const IMAGE_DEPBIN &image_depbin) : db_item_information(0), db_help(0), db_label(0), db_path(0), db_link(0), db_valid(0), db_image_table_index(0), db_visibility(0)
{
	AssignImageDepbin(image_depbin);
}

//Overloaded Assignment Operator for FLAT_CHART
nsEDDEngine::IMAGE_DEPBIN& nsEDDEngine::IMAGE_DEPBIN:: operator=(const IMAGE_DEPBIN &image_depbin)
{
	if(this != &image_depbin)
	{
		AssignImageDepbin(image_depbin);
	}
    return (*this);
}

//AssignImageDepbin is used to assign all the attributes of IMAGE_DEPBIN
void nsEDDEngine::IMAGE_DEPBIN::AssignImageDepbin(const IMAGE_DEPBIN &image_depbin)
{
	DEPBIN::CopyFromWithDelete(image_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(image_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(image_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(image_depbin.db_path,&db_path);
	DEPBIN::CopyFromWithDelete(image_depbin.db_link,&db_link);
	DEPBIN::CopyFromWithDelete(image_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(image_depbin.db_image_table_index,&db_image_table_index);
	DEPBIN::CopyFromWithDelete(image_depbin.db_visibility,&db_visibility);
}


nsEDDEngine::IMAGE_DEPBIN::~IMAGE_DEPBIN()
{
}

//copy constructor for FLAT_METHOD
nsEDDEngine::FLAT_METHOD::FLAT_METHOD(const FLAT_METHOD &flat_method):ItemBase(ITYPE_METHOD)
{
	AssignFlatMethod(flat_method);
}

//Overloaded Assignment Operator for FLAT_METHOD
nsEDDEngine::FLAT_METHOD& nsEDDEngine::FLAT_METHOD:: operator=(const FLAT_METHOD &flat_method)
{
	if(this != &flat_method)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatMethod(flat_method);
	}
    return (*this);
}

void nsEDDEngine::FLAT_METHOD::AssignFlatMethod(const FLAT_METHOD &flat_method)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_method);

	item_info		= flat_method.item_info;
	class_attr		= flat_method.class_attr;
	def				= flat_method.def;
	help			= flat_method.help;
	label			= flat_method.label;
	valid			= flat_method.valid;
	type			= flat_method.type;
	params			= flat_method.params;
	access			= flat_method.access;
	private_attr	= flat_method.private_attr;
	visibility		= flat_method.visibility;

	depbin			= new METHOD_DEPBIN(*flat_method.depbin);
}

nsEDDEngine::FLAT_METHOD::~FLAT_METHOD()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_METHOD::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::method_type |  
	nsEDDEngine::access | nsEDDEngine::class_attr | nsEDDEngine::method_parameters | nsEDDEngine::definition |
	nsEDDEngine::private_attr | nsEDDEngine::visibility;


nsEDDEngine::METHOD_DEPBIN::METHOD_DEPBIN(const METHOD_DEPBIN &method_depbin): db_item_information(0), db_class(0), db_def(0), db_help(0), db_label(0), db_valid(0), db_type(0),
			db_params(0), db_access(0), db_private_attr(0), db_visibility(0)
{
	AssignMethodDepbin(method_depbin);
}

//Overloaded Assignment Operator for METHOD_DEPBIN
nsEDDEngine::METHOD_DEPBIN& nsEDDEngine::METHOD_DEPBIN:: operator=(const METHOD_DEPBIN &method_depbin)
{
	if(this != &method_depbin)
	{
		AssignMethodDepbin(method_depbin);
	}
    return (*this);
}

//AssignMethodDepbin is used to assign all the attributes of  METHOD_DEPBIN
void nsEDDEngine::METHOD_DEPBIN::AssignMethodDepbin(const METHOD_DEPBIN &method_depbin)
{
	DEPBIN::CopyFromWithDelete(method_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(method_depbin.db_class,&db_class);
	DEPBIN::CopyFromWithDelete(method_depbin.db_def,&db_def);
	DEPBIN::CopyFromWithDelete(method_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(method_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(method_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(method_depbin.db_type,&db_type);
	DEPBIN::CopyFromWithDelete(method_depbin.db_params,&db_params);
	DEPBIN::CopyFromWithDelete(method_depbin.db_access,&db_access);
	DEPBIN::CopyFromWithDelete(method_depbin.db_private_attr,&db_private_attr);
	DEPBIN::CopyFromWithDelete(method_depbin.db_visibility,&db_visibility);
}

nsEDDEngine::METHOD_DEPBIN::~METHOD_DEPBIN()
{
}

//copy constructor for FLAT_MENU
nsEDDEngine::FLAT_MENU::FLAT_MENU(const FLAT_MENU &flat_menu):ItemBase(ITYPE_MENU)
{
	AssignFlatMenu(flat_menu);
}

//Overloaded Assignment Operator for FLAT_MENU
nsEDDEngine::FLAT_MENU& nsEDDEngine::FLAT_MENU:: operator=(const FLAT_MENU &flat_menu)
{
	if(this != &flat_menu)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatMenu(flat_menu);
	}
    return (*this);
}

void nsEDDEngine::FLAT_MENU::AssignFlatMenu(const FLAT_MENU &flat_menu)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_menu);

	item_info		= flat_menu.item_info;
	help			= flat_menu.help;
	label			= flat_menu.label;
	valid			= flat_menu.valid;
	style			= flat_menu.style;
	items			= flat_menu.items;
	access			= flat_menu.access;
	pre_edit_act	= flat_menu.pre_edit_act;
	post_edit_act	= flat_menu.post_edit_act;
	pre_read_act	= flat_menu.pre_read_act;
	post_read_act	= flat_menu.post_read_act;
	pre_write_act	= flat_menu.pre_write_act;
	post_write_act	= flat_menu.post_write_act;
	exit_actions	= flat_menu.exit_actions;
	init_actions	= flat_menu.init_actions;
	visibility		= flat_menu.visibility;

	depbin			= new MENU_DEPBIN(*flat_menu.depbin);
}

nsEDDEngine::FLAT_MENU::~FLAT_MENU()
{	

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_MENU::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::post_edit_actions |   
	nsEDDEngine::pre_edit_actions |nsEDDEngine::access |nsEDDEngine::post_read_actions |
	nsEDDEngine::post_write_actions |nsEDDEngine::pre_read_actions |nsEDDEngine::pre_write_actions |
	nsEDDEngine::items | nsEDDEngine::style | nsEDDEngine::visibility | nsEDDEngine::exit_actions | nsEDDEngine::init_actions;


nsEDDEngine::MENU_DEPBIN::MENU_DEPBIN(const MENU_DEPBIN &menu_depbin): db_item_information(0), db_label(0), db_help(0),  db_items(0), db_valid(0), db_style(0),
			db_access(0), db_pre_edit_act(0),  db_post_edit_act(0), db_pre_read_act(0), 
			db_post_read_act(0), db_pre_write_act(0), db_post_write_act(0), db_visibility(0),
			db_exit_actions(0), db_init_actions(0)
{
	AssignMenuDepbin(menu_depbin);
}



//Overloaded Assignment Operator for MENU_DEPBIN
nsEDDEngine::MENU_DEPBIN& nsEDDEngine::MENU_DEPBIN:: operator=(const MENU_DEPBIN &menu_depbin)
{
	if(this != &menu_depbin)
	{
		AssignMenuDepbin(menu_depbin);
	}
    return (*this);
}

//AssignMenuDepbin is used to assign all the attributes of MENU_DEPBIN
void nsEDDEngine::MENU_DEPBIN::AssignMenuDepbin(const MENU_DEPBIN &menu_depbin)
{
	DEPBIN::CopyFromWithDelete(menu_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_items,&db_items);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_style,&db_style);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_access,&db_access);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_pre_edit_act,&db_pre_edit_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_post_edit_act,&db_post_edit_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_pre_read_act,&db_pre_read_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_post_read_act,&db_post_read_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_pre_write_act,&db_pre_write_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_post_write_act,&db_post_write_act);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_exit_actions,&db_exit_actions);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_init_actions,&db_init_actions);
	DEPBIN::CopyFromWithDelete(menu_depbin.db_visibility,&db_visibility);
}

nsEDDEngine::MENU_DEPBIN::~MENU_DEPBIN()
{
}

//copy constructor for FLAT_EDIT_DISPLAY
nsEDDEngine::FLAT_EDIT_DISPLAY::FLAT_EDIT_DISPLAY(const FLAT_EDIT_DISPLAY &flat_edit_display):ItemBase(ITYPE_EDIT_DISP)
{
	AssignFlatEditDisplay(flat_edit_display);
}

//Overloaded Assignment Operator for FLAT_EDIT_DISPLAY
nsEDDEngine::FLAT_EDIT_DISPLAY& nsEDDEngine::FLAT_EDIT_DISPLAY:: operator=(const FLAT_EDIT_DISPLAY &flat_edit_display)
{
	if(this != &flat_edit_display)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatEditDisplay(flat_edit_display);
	}
    return (*this);
}

void nsEDDEngine::FLAT_EDIT_DISPLAY::AssignFlatEditDisplay(const FLAT_EDIT_DISPLAY &flat_edit_display)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_edit_display);

	item_info		= flat_edit_display.item_info;
	disp_items		= flat_edit_display.disp_items;
	edit_items		= flat_edit_display.edit_items;
	label			= flat_edit_display.label;
	help			= flat_edit_display.help;
	valid			= flat_edit_display.valid;
	pre_edit_act	= flat_edit_display.pre_edit_act;
	post_edit_act	= flat_edit_display.post_edit_act;
	visibility		= flat_edit_display.visibility;

	depbin			= new EDIT_DISPLAY_DEPBIN(*flat_edit_display.depbin);
}

nsEDDEngine::FLAT_EDIT_DISPLAY::~FLAT_EDIT_DISPLAY()
{	

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_EDIT_DISPLAY::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | nsEDDEngine::post_edit_actions |
	nsEDDEngine::pre_edit_actions | nsEDDEngine::display_items |  nsEDDEngine::edit_items | nsEDDEngine::visibility;


nsEDDEngine::EDIT_DISPLAY_DEPBIN::EDIT_DISPLAY_DEPBIN(const EDIT_DISPLAY_DEPBIN &edit_display_depbin): db_item_information(0), db_disp_items(0), db_edit_items(0),  db_label(0), db_help(0), db_valid(0),
			db_pre_edit_act(0), db_post_edit_act(0), db_visibility(0)
{
	AssignEditDisplaydepbin(edit_display_depbin);
}

//Overloaded Assignment Operator for EDIT_DISPLAY_DEPBIN
nsEDDEngine::EDIT_DISPLAY_DEPBIN& nsEDDEngine::EDIT_DISPLAY_DEPBIN:: operator=(const EDIT_DISPLAY_DEPBIN &edit_display_depbin)
{
	if(this != &edit_display_depbin)
	{
		AssignEditDisplaydepbin(edit_display_depbin);
	}
    return (*this);
}

//AssignEditDisplaydepbin is used to assign all the attributes of EDIT_DISPLAY_DEPBIN
void nsEDDEngine::EDIT_DISPLAY_DEPBIN::AssignEditDisplaydepbin(const EDIT_DISPLAY_DEPBIN &edit_display_depbin)
{
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_disp_items,&db_disp_items);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_edit_items,&db_edit_items);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_pre_edit_act,&db_pre_edit_act);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_post_edit_act,&db_post_edit_act);
	DEPBIN::CopyFromWithDelete(edit_display_depbin.db_visibility,&db_visibility);
}

nsEDDEngine::EDIT_DISPLAY_DEPBIN::~EDIT_DISPLAY_DEPBIN()
{
}

//copy constructor for FLAT_REFRESH
nsEDDEngine::FLAT_REFRESH::FLAT_REFRESH(const FLAT_REFRESH &flat_refresh):ItemBase(ITYPE_REFRESH)
{
	AssignFlatRefresh(flat_refresh);
}

//Overloaded Assignment Operator for FLAT_REFRESH
nsEDDEngine::FLAT_REFRESH& nsEDDEngine::FLAT_REFRESH:: operator=(const FLAT_REFRESH &flat_refresh)
{
	if(this != &flat_refresh)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatRefresh(flat_refresh);
	}
    return (*this);
}

void nsEDDEngine::FLAT_REFRESH::AssignFlatRefresh(const FLAT_REFRESH &flat_refresh)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_refresh);

	item_info		= flat_refresh.item_info;
	depend_items	= flat_refresh.depend_items;
	update_items	= flat_refresh.update_items;

	depbin			= new REFRESH_DEPBIN(*flat_refresh.depbin);
}

nsEDDEngine::FLAT_REFRESH::~FLAT_REFRESH()
{	
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_REFRESH::All_Attrs = nsEDDEngine::relation_watch_list | nsEDDEngine::relation_update_list; 


//copy constructor for REFRESH_DEPBIN
nsEDDEngine::REFRESH_DEPBIN::REFRESH_DEPBIN(const REFRESH_DEPBIN &refresh_depbin): db_item_information(0), db_depend_items(0), db_update_items(0)
{
	AssignRefreshDepbin(refresh_depbin);
}

//Overloaded Assignment Operator for REFRESH_DEPBIN
nsEDDEngine::REFRESH_DEPBIN& nsEDDEngine::REFRESH_DEPBIN:: operator=(const REFRESH_DEPBIN &refresh_depbin)
{
	if(this != &refresh_depbin)
	{
			AssignRefreshDepbin(refresh_depbin);
	}
    return (*this);
}

//AssignRefreshDepbin is used to assign all the attributes of REFRESH_DEPBIN
void nsEDDEngine::REFRESH_DEPBIN::AssignRefreshDepbin(const REFRESH_DEPBIN &refresh_depbin)
{
	DEPBIN::CopyFromWithDelete(refresh_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(refresh_depbin.db_depend_items,&db_depend_items);
	DEPBIN::CopyFromWithDelete(refresh_depbin.db_update_items,&db_update_items);
}

nsEDDEngine::REFRESH_DEPBIN::~REFRESH_DEPBIN()
{
}

//copy constructor for FLAT_UNIT
nsEDDEngine::FLAT_UNIT::FLAT_UNIT(const FLAT_UNIT &flat_unit):ItemBase(ITYPE_UNIT)
{
	AssignFlatUnit(flat_unit);
}

//Overloaded Assignment Operator for FLAT_UNIT
nsEDDEngine::FLAT_UNIT& nsEDDEngine::FLAT_UNIT:: operator=(const FLAT_UNIT &flat_unit)
{
	if(this != &flat_unit)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatUnit(flat_unit);
	}
    return (*this);
}

void nsEDDEngine::FLAT_UNIT::AssignFlatUnit(const FLAT_UNIT &flat_unit)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_unit);

	item_info	= flat_unit.item_info;
	var			= flat_unit.var;
	var_units	= flat_unit.var_units;

	depbin		= new UNIT_DEPBIN(*flat_unit.depbin);
}

nsEDDEngine::FLAT_UNIT::~FLAT_UNIT()
{	
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_UNIT::All_Attrs = nsEDDEngine::relation_watch_variable | nsEDDEngine::relation_update_list; 

//copy constructor for FLAT_UNIT
nsEDDEngine::UNIT_DEPBIN::UNIT_DEPBIN(const UNIT_DEPBIN &unit_depbin) : db_item_information(0), db_var(0), db_var_units(0)
{
	AssignUnitDepbin(unit_depbin);
}

//Overloaded Assignment Operator for FLAT_UNIT
nsEDDEngine::UNIT_DEPBIN& nsEDDEngine::UNIT_DEPBIN:: operator=(const UNIT_DEPBIN &unit_depbin)
{
	if(this != &unit_depbin)
	{
		AssignUnitDepbin(unit_depbin);
	}
    return (*this);
}

//AssignUnitDepbin is used to assign all the attributes of UNIT_DEPBIN
void nsEDDEngine::UNIT_DEPBIN::AssignUnitDepbin(const UNIT_DEPBIN &unit_depbin)
{
	DEPBIN::CopyFromWithDelete(unit_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(unit_depbin.db_var,&db_var);
	DEPBIN::CopyFromWithDelete(unit_depbin.db_var_units,&db_var_units);
}

nsEDDEngine::UNIT_DEPBIN::~UNIT_DEPBIN()
{
}

//copy constructor for FLAT_WAO
nsEDDEngine::FLAT_WAO::FLAT_WAO(const FLAT_WAO &flat_wao):ItemBase(ITYPE_WAO)
{
	AssignFlatWao(flat_wao);
}

//Overloaded Assignment Operator for FLAT_WAO
nsEDDEngine::FLAT_WAO& nsEDDEngine::FLAT_WAO:: operator=(const FLAT_WAO &flat_wao)
{
	if(this != &flat_wao)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatWao(flat_wao);
	}
    return (*this);
}

void nsEDDEngine::FLAT_WAO::AssignFlatWao(const FLAT_WAO &flat_wao)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_wao);

	item_info	= flat_wao.item_info;
	items		= flat_wao.items;

	depbin		= new WAO_DEPBIN(*flat_wao.depbin);
}

nsEDDEngine::FLAT_WAO::~FLAT_WAO()
{	
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_WAO::All_Attrs = nsEDDEngine::relation_update_list; 

//copy constructor for WAO_DEPBIN
nsEDDEngine::WAO_DEPBIN::WAO_DEPBIN(const WAO_DEPBIN &wao_depbin): db_item_information(0), db_items(0) 
{
	AssignWaoDepbin(wao_depbin);
}

//Overloaded Assignment Operator for WAO_DEPBIN
nsEDDEngine::WAO_DEPBIN& nsEDDEngine::WAO_DEPBIN:: operator=(const WAO_DEPBIN &wao_depbin)
{
	if(this != &wao_depbin)
	{
			AssignWaoDepbin(wao_depbin);
	}
    return (*this);
}

//AssignWaoDepbin is used to assign all the attributes of WAO_DEPBIN
void nsEDDEngine::WAO_DEPBIN::AssignWaoDepbin(const WAO_DEPBIN &wao_depbin)
{
	DEPBIN::CopyFromWithDelete(wao_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(wao_depbin.db_items,&db_items);
}

nsEDDEngine::WAO_DEPBIN::~WAO_DEPBIN()
{
}


//copy constructor for FLAT_BLOCK
nsEDDEngine::FLAT_BLOCK::FLAT_BLOCK(const FLAT_BLOCK &flat_block):ItemBase(ITYPE_BLOCK)
{
	AssignFlatBlock(flat_block);
}

//Overloaded Assignment Operator for FLAT_BLOCK
nsEDDEngine::FLAT_BLOCK& nsEDDEngine::FLAT_BLOCK:: operator=(const FLAT_BLOCK &flat_block)
{
	if(this != &flat_block)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatBlock(flat_block);
	}
    return (*this);
}

void nsEDDEngine::FLAT_BLOCK::AssignFlatBlock(const FLAT_BLOCK &flat_block)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_block);

	item_info		= flat_block.item_info;
	characteristic	= flat_block.characteristic;
	help			= flat_block.help;
	label			= flat_block.label;
	
	param			= flat_block.param;
	item_array		= flat_block.item_array;
	collect			= flat_block.collect;
	menu			= flat_block.menu;
	edit_disp		= flat_block.edit_disp;

	method			= flat_block.method;
	unit			= flat_block.unit;
	refresh			= flat_block.refresh;
	wao				= flat_block.wao;
	plugin_items	= flat_block.plugin_items;
	chart			= flat_block.chart;
	graph			= flat_block.graph;
	grid			= flat_block.grid;
	image			= flat_block.image;
	axis			= flat_block.axis;
	source			= flat_block.source;
	waveform		= flat_block.waveform;
	file			= flat_block.file;
	list			= flat_block.list;
	
	charts			= flat_block.charts;
	graphs			= flat_block.graphs;
	grids			= flat_block.grids;
	lists			= flat_block.lists;
	local_param		= flat_block.local_param;
	menus			= flat_block.menus;
	methods			= flat_block.methods;

	param_lists		= flat_block.param_lists;
	files			= flat_block.files;
	plugins			= flat_block.plugins;

	depbin			= new BLOCK_DEPBIN(*flat_block.depbin);
}

nsEDDEngine::FLAT_BLOCK::~FLAT_BLOCK()
{	
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_BLOCK::Every_Attrs = 
		nsEDDEngine::item_information		
		| nsEDDEngine::label					
		| nsEDDEngine::help					
		| nsEDDEngine::validity				
		| nsEDDEngine::members				
		| nsEDDEngine::handling				
		| nsEDDEngine::response_codes			
		| nsEDDEngine::height					
		| nsEDDEngine::post_edit_actions		
		| nsEDDEngine::pre_edit_actions		
		| nsEDDEngine::refresh_actions		
		| nsEDDEngine::relation_update_list	
		| nsEDDEngine::relation_watch_list	
		| nsEDDEngine::relation_watch_variable
		| nsEDDEngine::width					
		| nsEDDEngine::access					
		| nsEDDEngine::class_attr				
		| nsEDDEngine::constant_unit			
		| nsEDDEngine::cycle_time				
		| nsEDDEngine::emphasis				
		| nsEDDEngine::exit_actions			
		| nsEDDEngine::identity				
		| nsEDDEngine::init_actions			
		| nsEDDEngine::line_color				
		| nsEDDEngine::line_type				
		| nsEDDEngine::max_value				
		| nsEDDEngine::min_value				
		| nsEDDEngine::command_number					
		| nsEDDEngine::post_read_actions		
		| nsEDDEngine::post_write_actions		
		| nsEDDEngine::pre_read_actions		
		| nsEDDEngine::pre_write_actions		
		| nsEDDEngine::type_definition		
		| nsEDDEngine::y_axis					
		| nsEDDEngine::appinstance			
		| nsEDDEngine::axis_items				
		| nsEDDEngine::block_b				
		| nsEDDEngine::block_b_type			
		| nsEDDEngine::capacity				
		| nsEDDEngine::characteristics		
		| nsEDDEngine::chart_items			
		| nsEDDEngine::chart_type				
		| nsEDDEngine::charts					
		| nsEDDEngine::collection_items		
		| nsEDDEngine::block_number			
		| nsEDDEngine::count					
		| nsEDDEngine::default_value			
		| nsEDDEngine::default_values			
		| nsEDDEngine::definition				
		| nsEDDEngine::display_format			
		| nsEDDEngine::display_items			
		| nsEDDEngine::edit_display_items		
		| nsEDDEngine::edit_format			
		| nsEDDEngine::edit_items				
		| nsEDDEngine::elements				
		| nsEDDEngine::enumerations			
		| nsEDDEngine::file_items				
		| nsEDDEngine::first					
		| nsEDDEngine::graph_items			
		| nsEDDEngine::graphs					
		| nsEDDEngine::grid_items				
		| nsEDDEngine::grids					
		| nsEDDEngine::image_items			
		| nsEDDEngine::image_table_index		
		| nsEDDEngine::index					
		| nsEDDEngine::indexed				
		| nsEDDEngine::initial_value			
		| nsEDDEngine::items					
		| nsEDDEngine::keypoints_x_values		
		| nsEDDEngine::keypoints_y_values		
		| nsEDDEngine::last					
		| nsEDDEngine::length					
		| nsEDDEngine::link					
		| nsEDDEngine::list_items				
		| nsEDDEngine::lists					
		| nsEDDEngine::local_parameters		
		| nsEDDEngine::menu_items				
		| nsEDDEngine::menus					
		| nsEDDEngine::method_items			
		| nsEDDEngine::method_parameters		
		| nsEDDEngine::method_type			
		| nsEDDEngine::methods				
		| nsEDDEngine::number_of_elements		
		| nsEDDEngine::number_of_points		
		| nsEDDEngine::on_update_actions		
		| nsEDDEngine::operation				
		| nsEDDEngine::orientation			
		| nsEDDEngine::parameter_lists		
		| nsEDDEngine::parameters				
		| nsEDDEngine::post_user_actions		
		| nsEDDEngine::post_rqst_actions		
		| nsEDDEngine::uuid					
		| nsEDDEngine::reference_array_items	
		| nsEDDEngine::refresh_items			
		| nsEDDEngine::scaling				
		| nsEDDEngine::scaling_factor			
		| nsEDDEngine::slot					
		| nsEDDEngine::source_items			
		| nsEDDEngine::style					
		| nsEDDEngine::sub_slot				
		| nsEDDEngine::time_format			
		| nsEDDEngine::time_scale				
		| nsEDDEngine::transaction			
		| nsEDDEngine::unit_items				
		| nsEDDEngine::variable_type			
		| nsEDDEngine::vectors				
		| nsEDDEngine::waveform_items			
		| nsEDDEngine::waveform_type			
		| nsEDDEngine::write_as_one_items		
		| nsEDDEngine::component_byte_order	
		| nsEDDEngine::x_axis					
		| nsEDDEngine::x_increment			
		| nsEDDEngine::x_initial				
		| nsEDDEngine::x_values				
		| nsEDDEngine::y_values				
		| nsEDDEngine::view_min				
		| nsEDDEngine::view_max				
		| nsEDDEngine::reserved117 //min_axis				
		| nsEDDEngine::reserved118 //max_axis				
		| nsEDDEngine::reserved119 //default_reference		
		| nsEDDEngine::addressing				
		| nsEDDEngine::can_delete				
		| nsEDDEngine::check_configuration	
		| nsEDDEngine::classification			
		| nsEDDEngine::component_parent		
		| nsEDDEngine::component_path			
		| nsEDDEngine::component_relations	
		| nsEDDEngine::components				
		| nsEDDEngine::declaration			
		| nsEDDEngine::detect					
		| nsEDDEngine::device_revision		
		| nsEDDEngine::device_type			
		| nsEDDEngine::edd					
		| nsEDDEngine::manufacturer			
		| nsEDDEngine::maximum_number			
		| nsEDDEngine::minimum_number			
		| nsEDDEngine::protocol				
		| nsEDDEngine::redundancy				
		| nsEDDEngine::relation_type			
		| nsEDDEngine::component_connect_point
		| nsEDDEngine::scan					
		| nsEDDEngine::scan_list				
		| nsEDDEngine::private_attr			
		| nsEDDEngine::visibility				
		| nsEDDEngine::product_uri			
		| nsEDDEngine::api					
		| nsEDDEngine::header					
		| nsEDDEngine::write_mode				
		| nsEDDEngine::plugin_items			
		| nsEDDEngine::files					
		| nsEDDEngine::plugins				
		| nsEDDEngine::initial_values			
		| nsEDDEngine::post_rqstreceive_actions
		| nsEDDEngine::shared
		| nsEDDEngine::variable_status
		| nsEDDEngine::count_ref 
		| nsEDDEngine::min_value_ref 
		| nsEDDEngine::max_value_ref
		| nsEDDEngine::image_id;

// NOTE: Add the AttributeNames for "plugin_items", "plugins", and "files" after they are assigned in FDI-2041.doc
_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_BLOCK::All_Attrs = 
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::parameters | nsEDDEngine::axis_items |
	nsEDDEngine::characteristics | nsEDDEngine::chart_items | nsEDDEngine::charts | nsEDDEngine::collection_items | 
	nsEDDEngine::edit_display_items | nsEDDEngine::file_items | nsEDDEngine::graph_items | nsEDDEngine::graphs | 
	nsEDDEngine::grid_items | nsEDDEngine::grids | nsEDDEngine::image_items | nsEDDEngine::list_items |
	nsEDDEngine::lists | nsEDDEngine::local_parameters | nsEDDEngine::menu_items | nsEDDEngine::menus |
	nsEDDEngine::method_items | nsEDDEngine::methods | nsEDDEngine::parameter_lists | nsEDDEngine::reference_array_items |
	nsEDDEngine::refresh_items | nsEDDEngine::source_items | nsEDDEngine::unit_items | nsEDDEngine::waveform_items |
	nsEDDEngine::write_as_one_items;

//copy constructor for BLOCK_DEPBIN
nsEDDEngine::BLOCK_DEPBIN::BLOCK_DEPBIN(const BLOCK_DEPBIN &block_depbin): db_item_information(0), db_characteristic(0), db_help(0), db_label(0), db_param(0),
			db_item_array(0), db_collect(0), db_menu(0), db_edit_disp(0),db_method(0), 
			db_unit(0), db_refresh(0), db_wao(0), db_chart(0), db_graph(0), db_grid(0), 
			db_image(0), db_axis(0), db_source(0), db_waveform(0), db_file(0), db_list(0), 
			db_charts(0), db_graphs(0), db_grids(0), db_lists(0), db_local_param(0), db_menus(0),
			db_methods(0), db_param_list(0),
			db_plugin_items(0),db_files(0),db_plugins(0)
{
	AssignBlockDepbin(block_depbin);
}

//Overloaded Assignment Operator for BLOCK_DEPBIN
nsEDDEngine::BLOCK_DEPBIN& nsEDDEngine::BLOCK_DEPBIN:: operator=(const BLOCK_DEPBIN &block_depbin)
{
	if(this != &block_depbin)
	{
		AssignBlockDepbin(block_depbin);
	}
    return (*this);
}

//AssignBlockDepbinis used to assign all the attributes of BLOCK_DEPBIN
void nsEDDEngine::BLOCK_DEPBIN::AssignBlockDepbin(const BLOCK_DEPBIN &block_depbin)
{
	DEPBIN::CopyFromWithDelete(block_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(block_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(block_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(block_depbin.db_param,&db_param);
	DEPBIN::CopyFromWithDelete(block_depbin.db_characteristic,&db_characteristic);
	DEPBIN::CopyFromWithDelete(block_depbin.db_item_array,&db_item_array);
	DEPBIN::CopyFromWithDelete(block_depbin.db_collect,&db_collect);
	DEPBIN::CopyFromWithDelete(block_depbin.db_menu,&db_menu);
	DEPBIN::CopyFromWithDelete(block_depbin.db_edit_disp,&db_edit_disp);
	DEPBIN::CopyFromWithDelete(block_depbin.db_method,&db_method);
	DEPBIN::CopyFromWithDelete(block_depbin.db_unit,&db_unit);
	DEPBIN::CopyFromWithDelete(block_depbin.db_refresh,&db_refresh);
	DEPBIN::CopyFromWithDelete(block_depbin.db_wao,&db_wao);
	DEPBIN::CopyFromWithDelete(block_depbin.db_chart,&db_chart);
	DEPBIN::CopyFromWithDelete(block_depbin.db_graph,&db_graph);
	DEPBIN::CopyFromWithDelete(block_depbin.db_grid,&db_grid);
	DEPBIN::CopyFromWithDelete(block_depbin.db_image,&db_image);
	DEPBIN::CopyFromWithDelete(block_depbin.db_axis,&db_axis);
	DEPBIN::CopyFromWithDelete(block_depbin.db_source,&db_source);
	DEPBIN::CopyFromWithDelete(block_depbin.db_waveform,&db_waveform);
	DEPBIN::CopyFromWithDelete(block_depbin.db_file,&db_file);
	DEPBIN::CopyFromWithDelete(block_depbin.db_list,&db_list);
	DEPBIN::CopyFromWithDelete(block_depbin.db_charts,&db_charts);
	DEPBIN::CopyFromWithDelete(block_depbin.db_graphs,&db_graphs);
	DEPBIN::CopyFromWithDelete(block_depbin.db_grids,&db_grids);
	DEPBIN::CopyFromWithDelete(block_depbin.db_lists,&db_lists);
	DEPBIN::CopyFromWithDelete(block_depbin.db_local_param,&db_local_param);
	DEPBIN::CopyFromWithDelete(block_depbin.db_menus,&db_menus);
	DEPBIN::CopyFromWithDelete(block_depbin.db_methods,&db_methods);
	DEPBIN::CopyFromWithDelete(block_depbin.db_param_list,&db_param_list);
	DEPBIN::CopyFromWithDelete(block_depbin.db_plugin_items,&db_plugin_items);
	DEPBIN::CopyFromWithDelete(block_depbin.db_files,&db_files);
	DEPBIN::CopyFromWithDelete(block_depbin.db_plugins,&db_plugins);	
}
nsEDDEngine::BLOCK_DEPBIN::~BLOCK_DEPBIN()
{
}

//copy constructor for FLAT_BLOCK_B
nsEDDEngine::FLAT_BLOCK_B::FLAT_BLOCK_B(const FLAT_BLOCK_B &flat_block_b):ItemBase(ITYPE_BLOCK_B)
{
	AssignFlatBlockB(flat_block_b);
}

//Overloaded Assignment Operator for FLAT_BLOCK_B
nsEDDEngine::FLAT_BLOCK_B& nsEDDEngine::FLAT_BLOCK_B:: operator=(const FLAT_BLOCK_B &flat_block_b)
{
	if(this != &flat_block_b)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatBlockB(flat_block_b);
	}
    return (*this);
}

void nsEDDEngine::FLAT_BLOCK_B::AssignFlatBlockB(const FLAT_BLOCK_B &flat_block_b)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_block_b);

	item_info	= flat_block_b.item_info;
	number		= flat_block_b.number;
	type		= flat_block_b.type;

	depbin		= new BLOCK_B_DEPBIN(*flat_block_b.depbin);
}

nsEDDEngine::FLAT_BLOCK_B::~FLAT_BLOCK_B()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_BLOCK_B::All_Attrs = nsEDDEngine::block_b_type | nsEDDEngine::block_number;

//copy constructor for BLOCK_B_DEPBIN
nsEDDEngine::BLOCK_B_DEPBIN::BLOCK_B_DEPBIN(const BLOCK_B_DEPBIN &block_b_depbin): db_item_information(0), db_number(0), db_type(0)
{
	AssignBlockBDepbin(block_b_depbin);
}

//Overloaded Assignment Operator for BLOCK_B_DEPBIN
nsEDDEngine::BLOCK_B_DEPBIN& nsEDDEngine::BLOCK_B_DEPBIN:: operator= (const BLOCK_B_DEPBIN &block_b_depbin)
{
	if(this != &block_b_depbin)
	{
		AssignBlockBDepbin(block_b_depbin);
	}
    return (*this);
}

//AssignBlockBDepbin is used to assign all the attributes of BLOCK_B_DEPBIN
void nsEDDEngine::BLOCK_B_DEPBIN::AssignBlockBDepbin(const BLOCK_B_DEPBIN &block_b_depbin)
{
	DEPBIN::CopyFromWithDelete(block_b_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(block_b_depbin.db_number,&db_number);
	DEPBIN::CopyFromWithDelete(block_b_depbin.db_type,&db_type);
}

nsEDDEngine::BLOCK_B_DEPBIN::~BLOCK_B_DEPBIN()
{
}

//copy constructor for FLAT_RECORD
nsEDDEngine::FLAT_RECORD::FLAT_RECORD(const FLAT_RECORD &flat_record):ItemBase(ITYPE_RECORD)
{
	AssignFlatRecord(flat_record);
}

//Overloaded Assignment Operator for FLAT_RECORD
nsEDDEngine::FLAT_RECORD& nsEDDEngine::FLAT_RECORD:: operator=(const FLAT_RECORD &flat_record)
{
	if(this != &flat_record)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatRecord(flat_record);
	}
    return (*this);
}

void nsEDDEngine::FLAT_RECORD::AssignFlatRecord(const FLAT_RECORD &flat_record)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_record);

	item_info		= flat_record.item_info;
	members			= flat_record.members;
	help			= flat_record.help;
	label			= flat_record.label;
	valid			= flat_record.valid;
	resp_codes		= flat_record.resp_codes;
	write_mode		= flat_record.write_mode;
	private_attr	= flat_record.private_attr;
	visibility		= flat_record.visibility;

	depbin	= new RECORD_DEPBIN(*flat_record.depbin);
}

nsEDDEngine::FLAT_RECORD::~FLAT_RECORD()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_RECORD::All_Attrs = nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::validity | 
	nsEDDEngine::members | nsEDDEngine::response_codes | nsEDDEngine::private_attr | nsEDDEngine::visibility |
	nsEDDEngine::write_mode ;


//copy constructor for RECORD_DEPBIN
nsEDDEngine::RECORD_DEPBIN::RECORD_DEPBIN(const RECORD_DEPBIN &record_depbin): db_item_information(0), db_members(0), db_help(0), db_label(0), db_valid(0), db_resp_codes(0), 
			db_private_attr(0), db_visibility(0), db_write_mode(0) 
{
	AssignRecordDepbin(record_depbin);
}

//Overloaded Assignment Operator for RECORD_DEPBIN
nsEDDEngine::RECORD_DEPBIN& nsEDDEngine::RECORD_DEPBIN:: operator=(const RECORD_DEPBIN &record_depbin)
{
	if(this != &record_depbin)
	{
		AssignRecordDepbin(record_depbin);
	}
    return (*this);
}

//AssignRecordDepbin is used to assign all the attributes of RECORD_DEPBIN
void nsEDDEngine::RECORD_DEPBIN::AssignRecordDepbin(const RECORD_DEPBIN &record_depbin)
{
	DEPBIN::CopyFromWithDelete(record_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(record_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(record_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(record_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(record_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(record_depbin.db_resp_codes,&db_resp_codes);
	DEPBIN::CopyFromWithDelete(record_depbin.db_private_attr,&db_private_attr);
	DEPBIN::CopyFromWithDelete(record_depbin.db_visibility,&db_visibility);
	DEPBIN::CopyFromWithDelete(record_depbin.db_write_mode,&db_write_mode);
}

nsEDDEngine::RECORD_DEPBIN::~RECORD_DEPBIN()
{
}

//copy constructor for FLAT_VAR_LIST
nsEDDEngine::FLAT_VAR_LIST::FLAT_VAR_LIST(const FLAT_VAR_LIST &flat_var_list):ItemBase(ITYPE_VAR_LIST)
{
	AssignFlatVarList(flat_var_list);
}

//Overloaded Assignment Operator for FLAT_VAR_LIST
nsEDDEngine::FLAT_VAR_LIST& nsEDDEngine::FLAT_VAR_LIST:: operator=(const FLAT_VAR_LIST &flat_var_list)
{
	if(this != &flat_var_list)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatVarList(flat_var_list);
	}
    return (*this);
}

void nsEDDEngine::FLAT_VAR_LIST::AssignFlatVarList(const FLAT_VAR_LIST &flat_var_list)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_var_list);

	item_info	= flat_var_list.item_info;
	members		= flat_var_list.members;
	help		= flat_var_list.help;
	label		= flat_var_list.label;
	resp_codes	= flat_var_list.resp_codes;

	depbin		= new VAR_LIST_DEPBIN(*flat_var_list.depbin);
}

nsEDDEngine::FLAT_VAR_LIST::~FLAT_VAR_LIST()
{
	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_VAR_LIST::All_Attrs = nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::members | nsEDDEngine::response_codes;

//copy constructor for VAR_LIST_DEPBIN
nsEDDEngine::VAR_LIST_DEPBIN::VAR_LIST_DEPBIN(const VAR_LIST_DEPBIN &var_list_depbin): db_item_information(0), db_members(0), db_help(0), db_label(0), db_resp_codes(0)
{
	AssignVarListDepbin(var_list_depbin);
}

//Overloaded Assignment Operator for VAR_LIST_DEPBIN
nsEDDEngine::VAR_LIST_DEPBIN& nsEDDEngine::VAR_LIST_DEPBIN:: operator=(const VAR_LIST_DEPBIN &var_list_depbin)
{
	if(this != &var_list_depbin)
	{
		AssignVarListDepbin(var_list_depbin);
	}
    return (*this);
}

//AssignVarListDepbin is used to assign all the attributes of VAR_LIST_DEPBIN
void nsEDDEngine::VAR_LIST_DEPBIN::AssignVarListDepbin(const VAR_LIST_DEPBIN &var_list_depbin)
{
	DEPBIN::CopyFromWithDelete(var_list_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(var_list_depbin.db_members,&db_members);
	DEPBIN::CopyFromWithDelete(var_list_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(var_list_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(var_list_depbin.db_resp_codes,&db_resp_codes);
}

nsEDDEngine::VAR_LIST_DEPBIN::~VAR_LIST_DEPBIN()
{
}

//copy constructor for MEMBER_DEPBIN
nsEDDEngine::MEMBER_DEPBIN::MEMBER_DEPBIN(const MEMBER_DEPBIN &member_depbin): db_item_information(0)
{
	AssignMemberDepbin(member_depbin);
}

//Overloaded Assignment Operator for MEMBER_DEPBIN
nsEDDEngine::MEMBER_DEPBIN& nsEDDEngine::MEMBER_DEPBIN:: operator=(const MEMBER_DEPBIN &member_depbin)
{
	if(this != &member_depbin)
	{
		AssignMemberDepbin(member_depbin);
	}
    return (*this);
}

//AssignMemberDepbin is used to assign all the attributes of MEMBER_DEPBIN
void nsEDDEngine::MEMBER_DEPBIN::AssignMemberDepbin(const MEMBER_DEPBIN &member_depbin)
{
	DEPBIN::CopyFromWithDelete(member_depbin.db_item_information,&db_item_information);
}

//copy constructor for FLAT_MEMBER
nsEDDEngine::FLAT_MEMBER::FLAT_MEMBER(const FLAT_MEMBER &flat_member):ItemBase(ITYPE_MEMBER)
{
	AssignFlatMember(flat_member);
}

//Overloaded Assignment Operator for FLAT_MEMBER
nsEDDEngine::FLAT_MEMBER& nsEDDEngine::FLAT_MEMBER:: operator=(const FLAT_MEMBER &flat_member)
{
	if(this != &flat_member)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatMember(flat_member);
	}
    return (*this);
}

//AssignFlatMember is used to assign all the attributes of FLAT_MEMBER
void nsEDDEngine::FLAT_MEMBER::AssignFlatMember(const FLAT_MEMBER &flat_member)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_member);

	item_info	= flat_member.item_info;

	depbin		= new MEMBER_DEPBIN(*flat_member.depbin);
}

//copy constructor for BLOB_DEPBIN
nsEDDEngine::BLOB_DEPBIN::BLOB_DEPBIN(const BLOB_DEPBIN &blob_depbin): db_item_information(0), db_label(0), db_help(0), db_handling(0), db_identity(0), db_count(0)
{
	AssignBlobDepbin(blob_depbin);
}

//Overloaded Assignment Operator for BLOB_DEPBIN
nsEDDEngine::BLOB_DEPBIN& nsEDDEngine::BLOB_DEPBIN:: operator=(const BLOB_DEPBIN &blob_depbin)
{
	if(this != &blob_depbin)
	{
		AssignBlobDepbin(blob_depbin);
	}
    return (*this);
}

//AssignBlobDepbin is used to assign all the attributes of BLOB_DEPBIN
void nsEDDEngine::BLOB_DEPBIN::AssignBlobDepbin(const BLOB_DEPBIN &blob_depbin)
{
	DEPBIN::CopyFromWithDelete(blob_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(blob_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(blob_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(blob_depbin.db_handling,&db_handling);
	DEPBIN::CopyFromWithDelete(blob_depbin.db_identity,&db_identity);
	DEPBIN::CopyFromWithDelete(blob_depbin.db_count,&db_count);
}

//copy constructor for FLAT_BLOB
nsEDDEngine::FLAT_BLOB::FLAT_BLOB(const FLAT_BLOB &flat_blob):ItemBase(ITYPE_BLOB)
{
	AssignFlatBlob(flat_blob);
}

//Overloaded Assignment Operator for FLAT_BLOB
nsEDDEngine::FLAT_BLOB& nsEDDEngine::FLAT_BLOB:: operator=(const FLAT_BLOB &flat_blob)
{
	if(this != &flat_blob)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
			depbin = nullptr;
        }
		AssignFlatBlob(flat_blob);
	}
    return (*this);
}

void nsEDDEngine::FLAT_BLOB::AssignFlatBlob(const FLAT_BLOB &flat_blob)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_blob);

	item_info	= flat_blob.item_info;
	label		= flat_blob.label;
	help		= flat_blob.help;
	handling	= flat_blob.handling;
	identity	= flat_blob.identity;
	count		= flat_blob.count;

	depbin		= new BLOB_DEPBIN(*flat_blob.depbin);
}

//copy constructor for PLUGIN_DEPBIN
nsEDDEngine::PLUGIN_DEPBIN::PLUGIN_DEPBIN(const PLUGIN_DEPBIN &plugin_depbin): db_item_information(0), db_label(0), db_help(0), db_uuid(0), db_valid(0), db_visibility(0)
{
	AssignPluginDepbin(plugin_depbin);
}

//Overloaded Assignment Operator for PLUGIN_DEPBIN
nsEDDEngine::PLUGIN_DEPBIN& nsEDDEngine::PLUGIN_DEPBIN:: operator=(const PLUGIN_DEPBIN &plugin_depbin)
{
	if(this != &plugin_depbin)
	{
		AssignPluginDepbin(plugin_depbin);
	}
    return (*this);
}

//AssignPluginDepbin is used to assign all the attributes of PLUGIN_DEPBIN
void nsEDDEngine::PLUGIN_DEPBIN::AssignPluginDepbin(const PLUGIN_DEPBIN &plugin_depbin)
{
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_uuid,&db_uuid);
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_valid,&db_valid);
	DEPBIN::CopyFromWithDelete(plugin_depbin.db_visibility,&db_visibility);
}
//copy constructor for FLAT_PLUGIN
nsEDDEngine::FLAT_PLUGIN::FLAT_PLUGIN(const FLAT_PLUGIN &flat_plugin):ItemBase(ITYPE_PLUGIN)
{
	AssignFlatPlugin(flat_plugin);
}

//Overloaded Assignment Operator for FLAT_PLUGIN
nsEDDEngine::FLAT_PLUGIN& nsEDDEngine::FLAT_PLUGIN:: operator=(const FLAT_PLUGIN &flat_plugin)
{
	if(this != &flat_plugin)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatPlugin(flat_plugin);
	}
    return (*this);
}

void nsEDDEngine::FLAT_PLUGIN::AssignFlatPlugin(const FLAT_PLUGIN &flat_plugin)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_plugin);

	item_info	= flat_plugin.item_info;
	label		= flat_plugin.label;
	help		= flat_plugin.help;
	uuid		= flat_plugin.uuid;
	valid		= flat_plugin.valid;
	visibility	= flat_plugin.visibility;

	depbin		= new PLUGIN_DEPBIN(*flat_plugin.depbin);
}

//copy constructor for TEMPLATE_DEPBIN
nsEDDEngine::TEMPLATE_DEPBIN::TEMPLATE_DEPBIN(const TEMPLATE_DEPBIN &template_depbin): db_item_information(0), db_label(0), db_help(0), db_default_values(0), db_validity(0)
{
	AssignTemplateDepbin(template_depbin);
}

//Overloaded Assignment Operator for TEMPLATE_DEPBIN
nsEDDEngine::TEMPLATE_DEPBIN& nsEDDEngine::TEMPLATE_DEPBIN:: operator=(const TEMPLATE_DEPBIN &template_depbin)
{
	if(this != &template_depbin)
	{
		AssignTemplateDepbin(template_depbin);
	}
    return (*this);
}

//AssignTemplateDepbin is used to assign all the attributes of TEMPLATE_DEPBIN
void nsEDDEngine::TEMPLATE_DEPBIN::AssignTemplateDepbin(const TEMPLATE_DEPBIN &template_depbin)
{
	DEPBIN::CopyFromWithDelete(template_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(template_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(template_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(template_depbin.db_default_values,&db_default_values);
	DEPBIN::CopyFromWithDelete(template_depbin.db_validity,&db_validity);
}

//copy constructor for FLAT_TEMPLATE
nsEDDEngine::FLAT_TEMPLATE::FLAT_TEMPLATE(const FLAT_TEMPLATE &flat_template):ItemBase(ITYPE_TEMPLATE)
{
	AssignFlatTemplate(flat_template);
}

//Overloaded Assignment Operator for FLAT_TEMPLATE
nsEDDEngine::FLAT_TEMPLATE& nsEDDEngine::FLAT_TEMPLATE:: operator=(const FLAT_TEMPLATE &flat_template)
{
	if(this != &flat_template)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatTemplate(flat_template);
	}
    return (*this);
}

void nsEDDEngine::FLAT_TEMPLATE::AssignFlatTemplate(const FLAT_TEMPLATE &flat_template)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_template);

	item_info	= flat_template.item_info;
	label		= flat_template.label;
	help		= flat_template.help;
	default_values	= flat_template.default_values;
	valid	= flat_template.valid;

	depbin		= new TEMPLATE_DEPBIN(*flat_template.depbin);
}

//copy constructor for COMPONENT_DEPBIN
nsEDDEngine::COMPONENT_DEPBIN::COMPONENT_DEPBIN(const COMPONENT_DEPBIN &component_depbin):	db_item_information	(0),db_label(0),db_help	(0),db_can_delete (0),
								db_check_configuration (0),db_classification (0),db_component_byte_order(0),db_component_connect_point(0),
								db_component_parent(0),db_component_path(0),db_component_relations(0),db_declaration(0),db_detect(0),db_edd(0),
								db_product_uri(0),db_initial_values(0),db_protocol(0),db_redundancy(0),db_scan(0),db_scan_list(0)
{
	AssignComponentDepbin(component_depbin);
}

//Overloaded Assignment Operator for COMPONENT_DEPBIN
nsEDDEngine::COMPONENT_DEPBIN& nsEDDEngine::COMPONENT_DEPBIN:: operator=(const COMPONENT_DEPBIN &component_depbin)
{
	if(this != &component_depbin)
	{
		AssignComponentDepbin(component_depbin);
	}
    return (*this);
}

//AssignComponentDepbin is used to assign all the attributes of COMPONENT_DEPBIN
void nsEDDEngine::COMPONENT_DEPBIN::AssignComponentDepbin(const COMPONENT_DEPBIN &component_depbin)
{
	DEPBIN::CopyFromWithDelete(component_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(component_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(component_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(component_depbin.db_can_delete,&db_can_delete);
	DEPBIN::CopyFromWithDelete(component_depbin.db_check_configuration,&db_check_configuration);
	DEPBIN::CopyFromWithDelete(component_depbin.db_classification,&db_classification);
	DEPBIN::CopyFromWithDelete(component_depbin.db_component_byte_order,&db_component_byte_order);
	DEPBIN::CopyFromWithDelete(component_depbin.db_component_connect_point,&db_component_connect_point);
	DEPBIN::CopyFromWithDelete(component_depbin.db_component_parent,&db_component_parent);
	DEPBIN::CopyFromWithDelete(component_depbin.db_component_path,&db_component_path);
	DEPBIN::CopyFromWithDelete(component_depbin.db_component_relations,&db_component_relations);
	DEPBIN::CopyFromWithDelete(component_depbin.db_declaration,&db_declaration);
	DEPBIN::CopyFromWithDelete(component_depbin.db_detect,&db_detect);
	DEPBIN::CopyFromWithDelete(component_depbin.db_edd,&db_edd);
	DEPBIN::CopyFromWithDelete(component_depbin.db_product_uri,&db_product_uri);
	DEPBIN::CopyFromWithDelete(component_depbin.db_initial_values,&db_initial_values);
	DEPBIN::CopyFromWithDelete(component_depbin.db_protocol,&db_protocol);
	DEPBIN::CopyFromWithDelete(component_depbin.db_redundancy,&db_redundancy);
	DEPBIN::CopyFromWithDelete(component_depbin.db_scan,&db_scan);
	DEPBIN::CopyFromWithDelete(component_depbin.db_scan_list,&db_scan_list);
}

//copy constructor for FLAT_COMPONENT
nsEDDEngine::FLAT_COMPONENT::FLAT_COMPONENT(const FLAT_COMPONENT &flat_component):ItemBase(ITYPE_COMPONENT)
{
	AssignFlatComponent(flat_component);
}

//Overloaded Assignment Operator for FLAT_COMPONENT
nsEDDEngine::FLAT_COMPONENT& nsEDDEngine::FLAT_COMPONENT:: operator=(const FLAT_COMPONENT &flat_component)
{
	if(this != &flat_component)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatComponent(flat_component);
	}
    return (*this);
}

//AssignFlatComponent is used to assign all the attributes of FLAT_COMPONENT
void nsEDDEngine::FLAT_COMPONENT::AssignFlatComponent(const FLAT_COMPONENT &flat_component)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_component);

	item_info				= flat_component.item_info;
	label					= flat_component.label;
	help					= flat_component.help;
	can_delete				= flat_component.can_delete;
	check_configuration		= flat_component.check_configuration;
	classification			= flat_component.classification;
	component_byte_order	= flat_component.component_byte_order;
	component_connect_point	= flat_component.component_connect_point;
	component_parent		= flat_component.component_parent;
	component_path			= flat_component.component_path;
	component_relations		= flat_component.component_relations;
	declaration				= flat_component.declaration;
	detect					= flat_component.detect;
	edd						= flat_component.edd;
	product_uri				= flat_component.product_uri;
	initial_values			= flat_component.initial_values;
	protocol				= flat_component.protocol;
	redundancy				= flat_component.redundancy;
	scan					= flat_component.scan;
	scan_list				= flat_component.scan_list;

	depbin					= new COMPONENT_DEPBIN(*flat_component.depbin);
}

nsEDDEngine::FLAT_COMPONENT::~FLAT_COMPONENT()
{

	FreeDepbins();
	delete depbin;
	depbin = nullptr;
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COMPONENT::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::can_delete | nsEDDEngine::check_configuration |
	nsEDDEngine::classification | nsEDDEngine::component_byte_order | nsEDDEngine::component_connect_point |
	nsEDDEngine::component_parent | nsEDDEngine::component_path | nsEDDEngine::component_relations |
	nsEDDEngine::declaration | nsEDDEngine::detect | nsEDDEngine::edd | nsEDDEngine::product_uri | nsEDDEngine::initial_values |
	nsEDDEngine::protocol | nsEDDEngine::redundancy | nsEDDEngine::scan | nsEDDEngine::scan_list;


//copy constructor for COMPONENT_FOLDER_DEPBIN
nsEDDEngine::COMPONENT_FOLDER_DEPBIN::COMPONENT_FOLDER_DEPBIN(const COMPONENT_FOLDER_DEPBIN &component_folder_depbin): db_item_information(0),db_label(0),db_help(0),
									db_classification(0),db_component_parent(0),db_component_path(0),db_protocol(0)
{
	AssignComponentFolderDepbin(component_folder_depbin);
}

//Overloaded Assignment Operator for COMPONENT_FOLDER_DEPBIN
nsEDDEngine::COMPONENT_FOLDER_DEPBIN& nsEDDEngine::COMPONENT_FOLDER_DEPBIN:: operator=(const COMPONENT_FOLDER_DEPBIN &component_folder_depbin)
{
	if(this != &component_folder_depbin)
	{
		AssignComponentFolderDepbin(component_folder_depbin);
	}
    return (*this);
}

//AssignComponentFolderDepbin is used to assign all the attributes of COMPONENT_FOLDER_DEPBIN
void nsEDDEngine::COMPONENT_FOLDER_DEPBIN::AssignComponentFolderDepbin(const COMPONENT_FOLDER_DEPBIN &component_folder_depbin)
{
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_classification,&db_classification);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_component_parent,&db_component_parent);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_component_path,&db_component_path);
	DEPBIN::CopyFromWithDelete(component_folder_depbin.db_protocol,&db_protocol);
}

//copy constructor for FLAT_COMPONENT_FOLDER
nsEDDEngine::FLAT_COMPONENT_FOLDER::FLAT_COMPONENT_FOLDER(const FLAT_COMPONENT_FOLDER &flat_component_folder):ItemBase(ITYPE_COMPONENT_FOLDER)
{
	AssignFlatComponentFolder(flat_component_folder);
}

//Overloaded Assignment Operator for FLAT_COMPONENT_FOLDER
nsEDDEngine::FLAT_COMPONENT_FOLDER& nsEDDEngine::FLAT_COMPONENT_FOLDER:: operator=(const FLAT_COMPONENT_FOLDER &flat_component_folder)
{
	if(this != &flat_component_folder)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatComponentFolder(flat_component_folder);
	}
    return (*this);
}

void nsEDDEngine::FLAT_COMPONENT_FOLDER::AssignFlatComponentFolder(const FLAT_COMPONENT_FOLDER &flat_component_folder)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_component_folder);

	item_info			= flat_component_folder.item_info;
	label				= flat_component_folder.label;
	help				= flat_component_folder.help;
	classification		= flat_component_folder.classification;
	component_parent	= flat_component_folder.component_parent;
	component_path		= flat_component_folder.component_path;
	protocol			= flat_component_folder.protocol;

	depbin				= new COMPONENT_FOLDER_DEPBIN(*flat_component_folder.depbin);
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COMPONENT_FOLDER::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::classification | nsEDDEngine::component_parent |
	nsEDDEngine::component_path | nsEDDEngine::protocol;

//copy constructor for COMPONENT_REFERENCE_DEPBIN
nsEDDEngine::COMPONENT_REFERENCE_DEPBIN::COMPONENT_REFERENCE_DEPBIN(const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin): db_item_information(0),db_classification(0),
										db_component_parent(0),db_component_path(0),db_protocol(0),
										db_manufacturer(0),db_device_type(0),db_device_revision(0)
{
	AssignComponentReferenceDepbin(component_reference_depbin);
}

//Overloaded Assignment Operator for COMPONENT_REFERENCE_DEPBIN
nsEDDEngine::COMPONENT_REFERENCE_DEPBIN& nsEDDEngine::COMPONENT_REFERENCE_DEPBIN:: operator=(const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin)
{
	if(this != &component_reference_depbin)
	{
		AssignComponentReferenceDepbin(component_reference_depbin);
	}
    return (*this);
}

//AssignComponentReferenceDepbin is used to assign all the attributes of COMPONENT_REFERENCE_DEPBIN
void nsEDDEngine::COMPONENT_REFERENCE_DEPBIN::AssignComponentReferenceDepbin(const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin)
{
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_classification,&db_classification);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_component_parent,&db_component_parent);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_component_path,&db_component_path);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_protocol,&db_protocol);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_manufacturer,&db_manufacturer);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_device_type,&db_device_type);
	DEPBIN::CopyFromWithDelete(component_reference_depbin.db_device_revision,&db_device_revision);
}

//copy constructor for FLAT_COMPONENT_REFERENCE
nsEDDEngine::FLAT_COMPONENT_REFERENCE::FLAT_COMPONENT_REFERENCE(const FLAT_COMPONENT_REFERENCE &flat_component_reference):ItemBase(ITYPE_COMPONENT_REFERENCE)
{
	AssignFlatComponentReference(flat_component_reference);
}

//Overloaded Assignment Operator for FLAT_COMPONENT_REFERENCE
nsEDDEngine::FLAT_COMPONENT_REFERENCE& nsEDDEngine::FLAT_COMPONENT_REFERENCE:: operator=(const FLAT_COMPONENT_REFERENCE &flat_component_reference)
{
	if(this != &flat_component_reference)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatComponentReference(flat_component_reference);
	}
    return (*this);
}

void nsEDDEngine::FLAT_COMPONENT_REFERENCE::AssignFlatComponentReference(const FLAT_COMPONENT_REFERENCE &flat_component_reference)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_component_reference);

	item_info			= flat_component_reference.item_info;
	classification		= flat_component_reference.classification;
	component_parent	= flat_component_reference.component_parent;
	component_path		= flat_component_reference.component_path;
	protocol			= flat_component_reference.protocol;
	manufacturer		= flat_component_reference.manufacturer;
	device_type			= flat_component_reference.device_type;
	device_revision		= flat_component_reference.device_revision;

	depbin			= new COMPONENT_REFERENCE_DEPBIN(*flat_component_reference.depbin);
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COMPONENT_REFERENCE::All_Attrs =
nsEDDEngine::classification | nsEDDEngine::component_parent | nsEDDEngine::component_path
| nsEDDEngine::protocol | nsEDDEngine::manufacturer | nsEDDEngine::device_type | nsEDDEngine::device_revision;

//copy constructor for COMPONENT_RELATION_DEPBIN
nsEDDEngine::COMPONENT_RELATION_DEPBIN::COMPONENT_RELATION_DEPBIN(const COMPONENT_RELATION_DEPBIN &component_relation_depbin): db_item_information(0),db_label(0),
					db_help(0),db_maximum_number(0),db_minimum_number(0),db_addressing(0),db_relation_type(0),db_components(0)
{
	AssignComponentRelationDepbin(component_relation_depbin);
}

//Overloaded Assignment Operator for COMPONENT_RELATION_DEPBIN
nsEDDEngine::COMPONENT_RELATION_DEPBIN& nsEDDEngine::COMPONENT_RELATION_DEPBIN:: operator=(const COMPONENT_RELATION_DEPBIN &component_relation_depbin)
{
	if(this != &component_relation_depbin)
	{
		AssignComponentRelationDepbin(component_relation_depbin);
	}
    return (*this);
}

//AssignComponentRelationDepbin is used to assign all the attributes of COMPONENT_RELATION_DEPBIN
void nsEDDEngine::COMPONENT_RELATION_DEPBIN::AssignComponentRelationDepbin(const COMPONENT_RELATION_DEPBIN &component_relation_depbin)
{
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_item_information,&db_item_information);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_label,&db_label);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_help,&db_help);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_maximum_number,&db_maximum_number);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_minimum_number,&db_minimum_number);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_addressing,&db_addressing);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_relation_type,&db_relation_type);
	DEPBIN::CopyFromWithDelete(component_relation_depbin.db_components,&db_components);
}


//copy constructor for FLAT_COMPONENT_RELATION
nsEDDEngine::FLAT_COMPONENT_RELATION::FLAT_COMPONENT_RELATION(const FLAT_COMPONENT_RELATION &flat_component_relation):ItemBase(ITYPE_COMPONENT_RELATION)
{
	AssignFlatComponentRelation(flat_component_relation);
}

//Overloaded Assignment Operator for FLAT_COMPONENT_RELATION
nsEDDEngine::FLAT_COMPONENT_RELATION& nsEDDEngine::FLAT_COMPONENT_RELATION:: operator=(const FLAT_COMPONENT_RELATION &flat_component_relation)
{
	if(this != &flat_component_relation)
	{
		if(depbin != nullptr)
        {
			FreeDepbins();
            delete depbin;
        }
		AssignFlatComponentRelation(flat_component_relation);
	}
    return (*this);
}

void nsEDDEngine::FLAT_COMPONENT_RELATION::AssignFlatComponentRelation(const FLAT_COMPONENT_RELATION &flat_component_relation)
{
	//copying base class members by calling assignment operator explicitly
	ItemBase::operator=(flat_component_relation);

	item_info		= flat_component_relation.item_info;
	label			= flat_component_relation.label;
	help			= flat_component_relation.help;
	maximum_number	= flat_component_relation.maximum_number;
	minimum_number	= flat_component_relation.minimum_number;
	addressing		= flat_component_relation.addressing;
	relation_type	= flat_component_relation.relation_type;
	components		= flat_component_relation.components;

	depbin			= new COMPONENT_RELATION_DEPBIN(*flat_component_relation.depbin);
}

_INTERFACE const nsEDDEngine::AttributeNameSet nsEDDEngine::FLAT_COMPONENT_RELATION::All_Attrs =
	nsEDDEngine::label | nsEDDEngine::help | nsEDDEngine::maximum_number | nsEDDEngine::minimum_number | 
	nsEDDEngine::addressing | nsEDDEngine::relation_type | nsEDDEngine::components;

//copy constructor for FLAT_MASKS
nsEDDEngine::FLAT_MASKS::FLAT_MASKS(const FLAT_MASKS &flat_masks)
{
	bin_exists	= flat_masks.bin_exists;
	bin_hooked	= flat_masks.bin_hooked;
	attr_avail	= flat_masks.attr_avail;
	dynamic		= flat_masks.dynamic;
}

//Overloaded Assignment Operator for FLAT_MASKS
nsEDDEngine::FLAT_MASKS& nsEDDEngine::FLAT_MASKS::operator= (const FLAT_MASKS &flat_masks)
{
	if(this != &flat_masks)
	{
		bin_exists	= flat_masks.bin_exists;
		bin_hooked	= flat_masks.bin_hooked;
		attr_avail	= flat_masks.attr_avail;
		dynamic		= flat_masks.dynamic;
	}
	return (*this);
}



//copy constructor for RETURN_INFO
nsEDDEngine::RETURN_INFO::RETURN_INFO(const RETURN_INFO &return_info)
{
	bad_attr	= return_info.bad_attr;
	rc			= return_info.rc;
	var_needed	= return_info.var_needed;
}
		
//Overloaded Assignment Operator for RETURN_INFO
nsEDDEngine::RETURN_INFO& nsEDDEngine::RETURN_INFO::operator= (const RETURN_INFO &return_info)
{
	if(this != &return_info)
	{
		bad_attr	= return_info.bad_attr;
		rc			= return_info.rc;
		var_needed	= return_info.var_needed;
	}
	return (*this);
}

//copy constructor for RETURN_LIST
nsEDDEngine::RETURN_LIST::RETURN_LIST(const RETURN_LIST &return_list)
{
	count	= return_list.count;

	for(int i = 0; i < count; i++)
	{
		list[i] = return_list.list[i];
	}
}
		
//Overloaded Assignment Operator for RETURN_LIST
nsEDDEngine::RETURN_LIST& nsEDDEngine::RETURN_LIST::operator= (const RETURN_LIST &return_list)
{
	if(this != &return_list)
	{
		count	= return_list.count;

		for(int i = 0; i < count; i++)
		{
			list[i] = return_list.list[i];
		}
	}
	return (*this);
}

//copy constructor for FDI_GENERIC_ITEM
nsEDDEngine::FDI_GENERIC_ITEM::FDI_GENERIC_ITEM(const FDI_GENERIC_ITEM &generic_item)
{
	AssignGenericItem(generic_item);
}
		
//Overloaded Assignment Operator for FDI_GENERIC_ITEM
nsEDDEngine::FDI_GENERIC_ITEM& nsEDDEngine::FDI_GENERIC_ITEM::operator= (const FDI_GENERIC_ITEM &generic_item)
{
	if(this != &generic_item)
	{
		if(item != NULL)
		{
			delete item;
			item = nullptr;
		}
		AssignGenericItem(generic_item);
	}
	return (*this);
}

void nsEDDEngine::FDI_GENERIC_ITEM::AssignGenericItem(const FDI_GENERIC_ITEM &generic_item)
{
	item_type	= generic_item.item_type;	
	errors		= generic_item.errors;
	item		= generic_item.item->Replicate();	
}
