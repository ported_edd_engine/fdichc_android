#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;
using namespace nsEDDEngine;
using namespace nsConsumer;

namespace ComponentTests
{
    namespace ProfiBus
    {

        [TestClass]
        public ref class When_loading_Test_Block_1_from_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_GENERIC_ITEM* GenericItem;
            static nsEDDEngine::FLAT_BLOCK_B* PB_Block;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                ITEM_ID itemId;
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Test_Block_1", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                GenericItem = engine->GetGenericItem(itemId);
                PB_Block = dynamic_cast<nsEDDEngine::FLAT_BLOCK_B*>(GenericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_an_item_of_type_BLOCK_B()
            {
                Assert::AreEqual(int(nsEDDEngine::ITYPE_BLOCK_B), int(GenericItem->item_type));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_object()
            {
                Assert::AreNotEqual(0, int(PB_Block));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_with_number_1()
            {
                Assert::AreEqual(1, int(PB_Block->number));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FUNCTION_BLOCK_TYPE()
            {
                Assert::AreEqual((int) BlockBType::FUNCTION_BLOCK_TYPE, int(PB_Block->type));
            }
        };

        [TestClass]
        public ref class When_loading_Test_Block_2_from_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_GENERIC_ITEM* GenericItem;
            static nsEDDEngine::FLAT_BLOCK_B* PB_Block;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                ITEM_ID itemId;
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Test_Block_2", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                GenericItem = engine->GetGenericItem(itemId);
                PB_Block = dynamic_cast<nsEDDEngine::FLAT_BLOCK_B*>(GenericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_an_item_of_type_BLOCK_B()
            {
                Assert::AreEqual(int(nsEDDEngine::ITYPE_BLOCK_B), int(GenericItem->item_type));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_object()
            {
                Assert::AreNotEqual(0, int(PB_Block));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_with_number_2()
            {
                Assert::AreEqual(2, int(PB_Block->number));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_PHYSICAL_BLOCK_TYPE()
            {
                Assert::AreEqual((int) BlockBType::PHYSICAL_BLOCK_TYPE, int(PB_Block->type));
            }

        };

        [TestClass]
        public ref class When_loading_Test_Block_3_from_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_GENERIC_ITEM* GenericItem;
            static nsEDDEngine::FLAT_BLOCK_B* PB_Block;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                ITEM_ID itemId;
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Test_Block_3", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                GenericItem = engine->GetGenericItem(itemId);
                PB_Block = dynamic_cast<nsEDDEngine::FLAT_BLOCK_B*>(GenericItem->item);

            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_an_item_of_type_BLOCK_B()
            {
                Assert::AreEqual(int(nsEDDEngine::ITYPE_BLOCK_B), int(GenericItem->item_type));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_object()
            {
                Assert::AreNotEqual(0, int(PB_Block));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_FLAT_BLOCK_B_with_number_3()
            {
                Assert::AreEqual(3, int(PB_Block->number));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_a_TRANSDUCER_BLOCK_TYPE()
            {
                Assert::AreEqual((int) BlockBType::TRANSDUCER_BLOCK_TYPE, int(PB_Block->type));
            }
        };

        [TestClass]
        public ref class When_calling_GetCommandList_to_read_test_var_integer_in_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_COMMAND_LIST* CommandList;
            static ITEM_ID* commandReadTestVar;
            static ITEM_ID* commandReadTestVarBlock1;
            static ITEM_ID* commandReadTestVarBlock2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                commandReadTestVar = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Slot", commandReadTestVar);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                commandReadTestVarBlock1 = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Test_Block_1", commandReadTestVarBlock1);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                commandReadTestVarBlock2 = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Test_Block_2", commandReadTestVarBlock2);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                ITEM_ID itemId;
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_integer", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                const int blockInstance = 0;
                FDI_PARAM_SPECIFIER paramSpec;
                paramSpec.eType = FDI_PS_ITEM_ID;
                paramSpec.id = itemId;
                CommandType eCmdType = FDI_READ_COMMAND;
                CommandList = new nsEDDEngine::FDI_COMMAND_LIST();

                returnCode = engine->Instance->GetCommandList(blockInstance, &paramSpec, eCmdType, CommandList);
                Assert::IsTrue(returnCode == 0, "GetCommandList failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

#pragma region Helper
            bool CommandListContainsItem(const nsEDDEngine::FDI_COMMAND_LIST& commandList, ITEM_ID itemId)
            {     
                for (int i = 0; i < commandList.count; i++)
                {
                    if (commandList.list[i].command_id == itemId)
                    {
                        return true;
                    }
                }
                return false;
            }
#pragma endregion Helper

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_three_commands_for_read()
            {
                Assert::AreEqual(3, CommandList->count);
            }

            // check every command in list is a FLAT_COMMAND
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command1_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[0].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command2_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[1].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command3_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[2].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            // check if all expected Commands are in the list
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Slot_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVar));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_2_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVarBlock2));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_1_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVarBlock1));
            }

            // check Slot and Index for some commands
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Slot_with_slot_23()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVar);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(23u, command->slot);
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Slot_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVar);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_1_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVarBlock1);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_2_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVarBlock2);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }

        };

        [TestClass]
        public ref class When_calling_GetCommandList_to_read_test_var_unsigned_in_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_COMMAND_LIST* CommandList;

            static ITEM_ID* commandReadTestVarBlock1;
            static ITEM_ID* commandReadTestVarBlock2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                commandReadTestVarBlock1 = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Test_Block_1", commandReadTestVarBlock1);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                commandReadTestVarBlock2 = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Test_Block_2", commandReadTestVarBlock2);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                ITEM_ID itemId;
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_unsigned", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                const int blockInstance = 0;
                FDI_PARAM_SPECIFIER paramSpec;
                paramSpec.eType = FDI_PS_ITEM_ID;
                paramSpec.id = itemId;
                CommandType eCmdType = FDI_READ_COMMAND;
                CommandList = new nsEDDEngine::FDI_COMMAND_LIST();

                returnCode = engine->Instance->GetCommandList(blockInstance, &paramSpec, eCmdType, CommandList);
                Assert::IsTrue(returnCode == 0, "GetCommandList failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

#pragma region Helper
            bool CommandListContainsItem(const nsEDDEngine::FDI_COMMAND_LIST& commandList, ITEM_ID itemId)
            {     
                for (int i = 0; i < commandList.count; i++)
                {
                    if (commandList.list[i].command_id == itemId)
                    {
                        return true;
                    }
                }
                return false;
            }
#pragma endregion Helper

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_two_commands_for_read()
            {
                Assert::AreEqual(2, CommandList->count);
            }

            // check every command in list is a FLAT_COMMAND
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command1_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[0].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command2_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[1].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            // check if all expected Commands are in the list
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_2_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVarBlock2));
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_1_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVarBlock1));
            }

            // check Slot and Index for some commands
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_1_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVarBlock1);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }

            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_2_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVarBlock2);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }
        };

        [TestClass]
        public ref class When_calling_GetCommandList_to_read_test_var_float_in_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_COMMAND_LIST* CommandList;

            static ITEM_ID* commandReadTestVarBlock1;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                commandReadTestVarBlock1 = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Read_test_vars_from_Test_Block_1", commandReadTestVarBlock1);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                ITEM_ID itemId;
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_float", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                const int blockInstance = 0;
                FDI_PARAM_SPECIFIER paramSpec;
                paramSpec.eType = FDI_PS_ITEM_ID;
                paramSpec.id = itemId;
                CommandType eCmdType = FDI_READ_COMMAND;
                CommandList = new nsEDDEngine::FDI_COMMAND_LIST();

                returnCode = engine->Instance->GetCommandList(blockInstance, &paramSpec, eCmdType, CommandList);
                Assert::IsTrue(returnCode == 0, "GetCommandList failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

#pragma region Helper
            bool CommandListContainsItem(const nsEDDEngine::FDI_COMMAND_LIST& commandList, ITEM_ID itemId)
            {     
                for (int i = 0; i < commandList.count; i++)
                {
                    if (commandList.list[i].command_id == itemId)
                    {
                        return true;
                    }
                }
                return false;
            }
#pragma endregion Helper
            
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_one_command_for_read()
            {
                Assert::AreEqual(1, CommandList->count);
            }

            // check every command in list is a FLAT_COMMAND
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command1_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[0].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            // check if all expected Commands are in the list
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_1_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandReadTestVarBlock1));
            }

            // check Slot and Index for some commands
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_1_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandReadTestVarBlock1);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }
        };

        [TestClass]
        public ref class When_calling_GetCommandList_to_write_test_var_integer_in_FDI_PB_Test_0002
        {
        public:
            static EddEngineController^ engine;
            static nsEDDEngine::FDI_COMMAND_LIST* CommandList;
            static ITEM_ID* commandWriteBlock1;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();

                commandWriteBlock1 = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Write_test_vars_to_Test_Block_1", commandWriteBlock1);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                ITEM_ID itemId;
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_integer", &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                const int blockInstance = 0;
                FDI_PARAM_SPECIFIER paramSpec;
                paramSpec.eType = FDI_PS_ITEM_ID;
                paramSpec.id = itemId;
                CommandType eCmdType = FDI_WRITE_COMMAND;
                CommandList = new nsEDDEngine::FDI_COMMAND_LIST();

                returnCode = engine->Instance->GetCommandList(blockInstance, &paramSpec, eCmdType, CommandList);
                Assert::IsTrue(returnCode == 0, "GetCommandList failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
            }

#pragma region Helper
            bool CommandListContainsItem(const nsEDDEngine::FDI_COMMAND_LIST& commandList, ITEM_ID itemId)
            {     
                for (int i = 0; i < commandList.count; i++)
                {
                    if (commandList.list[i].command_id == itemId)
                    {
                        return true;
                    }
                }
                return false;
            }
#pragma endregion Helper
            
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_one_command_for_read()
            {
                Assert::AreEqual(1, CommandList->count);
            }

            // check every command in list is a FLAT_COMMAND
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command1_as_FLAT_COMMAND()
            {
                ITEM_ID itemId = CommandList->list[0].command_id;
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(itemId);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::IsFalse(command == nullptr, "FDI_GENERIC_ITEM is no FLAT_COMMAND");
            }

            // check if all expected Commands are in the list
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_the_command_Read_test_vars_from_Test_Block_1_in_the_commandList()
            {
                Assert::IsTrue(CommandListContainsItem(*CommandList, *commandWriteBlock1));
            }

            // check Slot and Index for some commands
            [TestMethod, TestCategory("SlotIndexCommunicationTest")]
            void It_should_return_command_Read_test_vars_from_Test_Block_1_with_index_25()
            {
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(*commandWriteBlock1);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                Assert::AreEqual(25u, command->index);
            }
        };

    }
}
