
#include "stdafx.h"
#include "nsEDDEngine/IConvenience.h"

nsEDDEngine::COMMAND_ELEM::~COMMAND_ELEM()
{
	delete [] index_list;
	index_list = nullptr;
	count = 0;
}

void nsEDDEngine::FDI_COMMAND_LIST::Dispose()
{
	delete [] list;
	list = nullptr;
	count = 0;
}

nsEDDEngine::FDI_COMMAND_LIST::~FDI_COMMAND_LIST()
{
	this->Dispose();
}
