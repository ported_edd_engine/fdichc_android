/*************************************************************************************************
 *
 * $Workfile: ddbDevice.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the Device class
 *		4/30/2	sjv	started creation
 *
 *		
 * #include "ddbDevice.h"
 */

#ifndef _DDBDEVICE_H
 #define _DDBDEVICE_H
 #ifdef INC_DEBUG
  #pragma message("In ddbDevice.h") 
 #endif

#include "ddbdefs.h"
#include "ddbGeneral.h"

extern bool openIni();
extern bool closeIni();

#include <nsEDDEngine/ProtocolType.h>
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IEDDEngineLogger.h"
#include "ddbVar.h"
#include "ddbItemBase.h"
#include "Dictionary.h"
#include "ddbDeviceService.h"
#include "ddbGlblSrvInfc.h"
#include "ddbCollAndArr.h"
#include "ddbArray.h"
#include "ddbList.h"
#include "ddbMethod.h"
#include "ddbAxis.h"
#include "ddbList.h"
#include "ddbCmdList.h"
#include "MethodInterfaceDefs.h"
#include "nsEDDEngine/Attrs.h"
#include <condition_variable>
#include "stdstring.h"


/************************************************************************************************/
/************************************************************************************************/
class hCddbDevice : public hCobject, public hCdeviceSrvc    // the device instance class
{
protected:	
	MEE* pMEE;
	
public:  //public member variables
	/* Dictinary (Get Dicitionary pointer from device manager )*/
	CDictionary *dictionary;
	const wchar_t* device_sLanguageCode;

public:
	hCddbDevice( DevInfcHandle_t h );		// base constructor
	virtual ~hCddbDevice();
	unsigned int getMEEdepth();
	MEE* getMEEPointer() {return pMEE;}

	virtual itemType_e GetItemType( long lKind ) = 0;

	//  pure virtual methods
	virtual bool IsDeviceCancelled() = 0;
	virtual void ClearDeviceCancel() = 0;
	virtual bool ReadMethodData( long lItemId, CStdString sValue, CValueVarient *pOutput)=0;
	virtual RETURNCODE getItemBySymName  ( string& symName,hCitemBase** ppReturnedItempointer)=0;
	virtual RETURNCODE sendCommMethodCmd(int commandNumber, int transactionNumber, uchar* pucCmdStatus, uchar* pchMoreDataInfo, int& iMoreDataSize)=0;

	virtual RETURNCODE AccessDynamicAttributeMethod(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType) = 0;

	virtual RETURNCODE sendListOpMethod(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int icount) = 0;
	virtual RETURNCODE sendUIMethod(ACTION_UI_DATA& stUIData)=0;
	virtual bool LockMethodUIEvent(unsigned long milliSeconds = 0) = 0;
	virtual void UnlockMethodUIEvent() = 0;
	virtual void ResetMethodUIEvent() = 0;
	virtual RETURNCODE receiveUIMethodRsp(CValueVarient& vtRsp)=0;
	virtual RETURNCODE get_rspcode_string( int nCommandNumber, int nResponseCode, tchar *pchResponseString, int nLength, nsEDDEngine::ProtocolType protocol )=0;
	virtual RETURNCODE getDictionaryString(unsigned long ulIndex, wchar_t* pString, int iStringLen)=0;
	devMode_t whatCompatability(){ return dm_Standard; }
	int ResolveExp(const char* szDDitemName,const char* szDDExp,unsigned long ulLenOfexpToResolve,hCitemBase**  pIBFinal);

	virtual RETURNCODE getResponseCodeString(int iItemId, int iMemberId, int iRsponseCode, tchar *pchResponseCodeString, int iResponseCodeStringLength)=0;
	virtual RETURNCODE sendParamValues(const PARAM_REF *pOpRef, AccessType iAccessType) =0;
	virtual RETURNCODE getDdsError(tchar *pchErrorString, int iMaxLength)=0;
	virtual unsigned long getCommError() = 0;
	virtual RETURNCODE getResponseCode(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId) = 0;
	virtual RETURNCODE assignValue(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef) = 0;
	virtual ITEM_ID ResolveBlockRef(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType) = 0;
	virtual RETURNCODE ReadParameterData2( CValueVarient *pOutput, CStdString sValue, unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId ) = 0;
	virtual RETURNCODE ReadParameterActionList( /*in*/ unsigned long ulBlkId, /*in*/ unsigned long ulBlkNum, /*in*/ unsigned long ulItemId, /*out*/ nsEDDEngine::ACTION_LIST *pOutput) = 0;
	virtual RETURNCODE devAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, AccessType eAccessType) = 0;
	virtual RETURNCODE devAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /* in */ AccessType eAccessType) = 0;
	virtual RETURNCODE devWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal) = 0;
	
	virtual long getBlockInstanceCount(unsigned long ulBlockId, unsigned long *pulCount) = 0;
	virtual long getBlockByObjectIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex) = 0;
	virtual long getBlockByBlockTag(unsigned long lBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex) = 0;
	virtual long getListElem2 (unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data) = 0;
	virtual void deviceLogMessage(int priorityVal, wchar_t *msg) = 0;
	virtual void deviceMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml) = 0;
	virtual int deviceIsOffline() = 0;
	virtual RETURNCODE deviceGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString)=0;

	virtual long deviceSendCommand(unsigned long commandId, long *commandError) = 0;
	virtual RETURNCODE devAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType) = 0;

	virtual int getElementOrMemberPointer(unsigned long ulItemId, unsigned long ulMemberId, hCitemBase **pIB) = 0;
	virtual int GetMemberIdByName( CStdString sMemberName, unsigned long *pMemberId ) = 0;
	virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0) = 0;
};

#endif//_DDBDEVICE_H
