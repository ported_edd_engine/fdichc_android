/**
 *              Device Description Services Rel. 4.2
 *              Copyright 1994-1996 - Fieldbus Foundation
 *              All rights reserved.
 */
/*
 *              @(#)int_diff.h	21.5  21  14 Nov 1996
 */

#ifndef INT_DIFF_H
#define INT_DIFF_H

#include "int_meth.h"

#if defined _WIN32
#if	defined MI
#			define DEBUGOUT		WPrint
#   		define OUTFILE      "out.txt"
#			define DEBUGERR		WPrint
#   		define ERRFILE      "err.txt"
#			define FMSASSERT	WASSERT
#	else
#			define DEBUGOUT		(void)fprintf
#   		define OUTFILE      stdout
#			define DEBUGERR		(void)fprintf
#   		define ERRFILE      stderr
#			define FMSASSERT	assert
#	endif
#else
#	define DEBUGOUT		(void)fprintf
#   define OUTFILE      stdout
#	define DEBUGERR		(void)fprintf
#   define ERRFILE      stdout
#	define FMSASSERT	assert
#endif


#define	USE_DEV_VALUE		0x01
#define USE_METH_VALUE		0x02
#define USE_EDIT_VALUE		0x04
#define USE_MASK	(USE_DEV_VALUE|USE_METH_VALUE|USE_EDIT_VALUE)
#define HIDDEN_RCODE_ERROR	0x08
#define NO_ERROR_PROCESSING	0x10
#define NO_COMM_ON_GET		0x20

#ifdef MI

/******************************************************************************
 *
 *  app_info structure for the FMS Method Executor
 *
 ******************************************************************************/

#define DEFAULTED_STATUS_STRING        0x01

typedef struct {
    int                dds_return_code;
    RETURN_LIST        dds_errors;
    UINT32             comm_err;
    UINT32             device_status;
    UINT32             response_code;
    ITEM_ID            id_in_error;
    int                po_in_error;
    SUBINDEX           subindex_in_error;
    ITEM_ID            member_id_in_error;
    unsigned short     return_flag;
} ENV_RETURNS;

typedef struct {

    unsigned short    method_tag;
    unsigned short    status_info;

    ENV_RETURNS       env_returns;
    void              *pMethod;
} APP_INFO;

void peek(void);

#endif

/******************************************************************************
 *
 *  app_info structure for the FieldBus Foundation Method Executor
 *
 ******************************************************************************/

#define DEFAULTED_STATUS_STRING        0x01

typedef struct {
    int                dds_return_code;
    RETURN_LIST        dds_errors;
    UINT32             comm_err;
    UINT32             device_status;
    UINT32             response_code;
    ITEM_ID            id_in_error;
    int                po_in_error;
    SUBINDEX           subindex_in_error;
    ITEM_ID            member_id_in_error;
    unsigned short     return_flag;
} ENV_RETURNS;

typedef struct {
    unsigned short    method_tag;
    unsigned short    status_info;
    void              *action_storage;
    void              *display_info;
    ENV_RETURNS       env_returns;
} APP_INFO;


/******************************************************************************
 *
 *  Prototypes of platform specific functions
 *
 ******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

int FmsGetActionStorage(ENV_INFO *env_info, P_REF *ip_ref, P_REF *p_ref);

int FmsPutActionStorage(ENV_INFO *env_info, P_REF *ip_ref, int replicate);

int FmsInitBiSymbols(ENVIRON* env,BLOCK_HANDLE block_handle);

int FmsCloseMethod(ENV_INFO* env_info, int option);

int FmsCleanItem(DDI_GENERIC_ITEM*    generic_item);

int FmsGetMethodDefinition(DDI_BLOCK_SPECIFIER* block_spec, 
                           DDI_ITEM_SPECIFIER*  item_spec,
                           ENV_INFO*            env_info,
                           DDI_GENERIC_ITEM*    generic_item);

void FmsPut(char* msg);

void FmsWrite(const void* buffer, size_t size, size_t count, FILE* stream);

#ifdef __cplusplus
}
#endif

#endif   /* INT_DIFF_H */
