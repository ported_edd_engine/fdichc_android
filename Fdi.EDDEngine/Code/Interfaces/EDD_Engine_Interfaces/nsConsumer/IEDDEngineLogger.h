#pragma once

namespace nsConsumer
{
	typedef enum LogSeverity
	{
       Critical,
       Error,
	   Warning,
       Information,
       Verbose,
	} LogSeverity;


	class IEDDEngineLogger
	{
	public:

		virtual ~IEDDEngineLogger(void) {};

		virtual bool ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory) = 0;
        virtual void Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory) = 0;

		//FILE* m_pMethodLogFile;
	};
}