/**
 *	@(#) $Id: fch_lib.h,v 1.3 1995/05/23 14:32:58 kenta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains data structures for the fetch interface functions
 */

#ifndef FETCH_LIB_H
#define FETCH_LIB_H

#include "std.h"
#include "rtn_code.h"
#include "table.h"
#include "tags_sa.h"
#include "dds_upcl.h"
#ifdef DEBUG
#include "dds_chk.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif



typedef struct {
	unsigned long   used;	/* Amount of scratchpad memory consumed */
	unsigned long   size;	/* Total amount of scratchpad memory */
	unsigned char  *pad;	/* Pointer to beginning of scratchpad
							   memory area */
}               SCRATCH_PAD;



/************************************************
 *                                              *
 *     Fetch Interface Function Definitions     *
 *                                              *
 ************************************************/


/*
 *	General Interface Function Definitions
 */

extern int fch_item_spad_size P((ENV_INFO *, DEVICE_HANDLE, ITEM_ID, unsigned long *, unsigned long, ITEM_TYPE));

extern int fch_item P((ENV_INFO *, DEVICE_HANDLE, ITEM_ID, SCRATCH_PAD *, unsigned long *, unsigned long, void *, ITEM_TYPE));

extern int fch_block_dir_spad_size P((ENV_INFO *, DEVICE_HANDLE, ITEM_ID, unsigned long *, unsigned long));

extern int fch_block_dir P((ENV_INFO *, DEVICE_HANDLE, ITEM_ID, SCRATCH_PAD *, unsigned long *, unsigned long, BIN_BLOCK_DIR *));

extern int fch_device_dir_spad_size P((ENV_INFO *, DEVICE_HANDLE, unsigned long *, unsigned long));

extern int fch_device_dir P((ENV_INFO *, DEVICE_HANDLE, SCRATCH_PAD *, unsigned long *, unsigned long, BIN_DEVICE_DIR *));


/*
 *	ROD Interface Function Definitions
 */

extern int fch_rod_item_spad_size P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, unsigned long *, unsigned long, ITEM_TYPE));

extern int fch_rod_item P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long *, unsigned long, void *, ITEM_TYPE));

extern int fch_rod_dir_spad_size P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, unsigned long *, unsigned long, unsigned short));

extern int fch_rod_device_dir P((ENV_INFO *, ROD_HANDLE, SCRATCH_PAD *, unsigned long *, unsigned long, BIN_DEVICE_DIR *));

extern int fch_rod_block_dir P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long *, unsigned long, BIN_BLOCK_DIR *));

extern int get_rod_object P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, OBJECT **, SCRATCH_PAD *));

extern int get_local_data P((OBJECT *, unsigned long, unsigned long, SCRATCH_PAD *));


/*
 *	Flat Interface Function Definitions
 */

extern int fch_flat_item_spad_size P((FLAT_HANDLE, FILE_OFFSET, unsigned long *, unsigned long, ITEM_TYPE));

extern int fch_flat_item P((FLAT_HANDLE, FILE_OFFSET, SCRATCH_PAD *, unsigned long *, unsigned long, void *, ITEM_TYPE));

extern int fch_flat_dir_spad_size P((FLAT_HANDLE, FILE_OFFSET, unsigned long *, unsigned long, unsigned short));

extern int fch_flat_device_dir P((FLAT_HANDLE, FLAT_DEVICE_DIR *));

extern int fch_flat_block_dir P((FLAT_HANDLE, FILE_OFFSET, FLAT_BLOCK_DIR * ));


/*** stub functions for fetch function ***/







#ifdef __cplusplus
    }
#endif

#endif
