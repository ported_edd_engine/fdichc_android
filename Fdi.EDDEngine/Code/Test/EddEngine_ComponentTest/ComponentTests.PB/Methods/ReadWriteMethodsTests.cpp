#include "TestSupport/ParameterAccessMethodHelper.h"
#include "TestSupport/Helper.h"

using namespace std;
using namespace nsEDDEngine;
using namespace System;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_executing_the_add_100_method_to_the_int_value_10_syncron_with_PB
        {
            static ParameterAccessMethodHelper^ _Result;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                nsConsumer::EVAL_VAR_VALUE actionValue;
                actionValue.type = VT_INTEGER;
                actionValue.size = 1;
                actionValue.val.i = 10;

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000A.DDL.bin")->
                    RegisterActionValueBegin(&actionValue)->
                    RegisterOnMethodExiting()->
                    ExecuteScalingMethodSynchronously("add_100");
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_the_value_110_in_the_builtin_support()
            {
                nsConsumer::EVAL_VAR_VALUE* result = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->ReplayReturnValue(L"ActionValue.End");
                Assert::AreEqual(110, (int) result->val.i);
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_the_value_in_the_builtin_support_with_size_1()
            {
                nsConsumer::EVAL_VAR_VALUE* result = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->ReplayReturnValue(L"ActionValue.End");
                Assert::AreEqual(1, (int) result->size);
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_get_unscaled_value_in_the_build_in_interface()
            {
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"GetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_set_unscaled_value_in_the_build_in_interface()
            {
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"SetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_as_method_result_true()
            {
                Assert::IsTrue(_Result->GetMethodResult() == METH_SUCCESS);
            }
        };

        [TestClass]
        public ref class When_executing_the_add_100_method_to_the_int_value_10_asyncron_with_PB 
        {
            static ParameterAccessMethodHelper^ _Result;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                nsConsumer::EVAL_VAR_VALUE actionValue;
                actionValue.type = VT_UNSIGNED;
                actionValue.size = 4;
                actionValue.val.u = 10;

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000A.DDL.bin")->
                    RegisterActionValueBegin(&actionValue)->
                    RegisterOnMethodExiting()->
                    ExecuteScalingMethodAsynchronously("add_100");

                int i = 0;
                //wait at least 5sec or until callback was called
                while(i < 500 && !_Result->WasAsyncCalled())
                {
                    System::Threading::Thread::Sleep(10);
                    i++;
                }
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_the_value_110_in_the_builtin_support()
            {
                nsConsumer::EVAL_VAR_VALUE* result = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->ReplayReturnValue(L"ActionValue.End");
                Assert::AreEqual(110ull, result->val.u);
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_the_value_in_the_builtin_support_with_size_4()
            {
                nsConsumer::EVAL_VAR_VALUE* result = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->ReplayReturnValue(L"ActionValue.End");
                Assert::AreEqual(4, (int) result->size);
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_get_unscaled_value_in_the_build_in_interface()
            {
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"GetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_set_unscaled_value_in_the_build_in_interface()
            {
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"SetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_call_the_async_callback()
            {
                Assert::IsTrue(_Result->WasAsyncCalled());
            }
        };

        [TestClass]
        public ref class When_executing_the_method_division_by_zero_in_the_var_float_with_division_by_zero_with_PB
        {
            static ParameterAccessMethodHelper^ _Result;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                nsConsumer::EVAL_VAR_VALUE actionValue;
                actionValue.type = VT_FLOAT;
                actionValue.size = 1;
                actionValue.val.f = 10.1f;

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000A.DDL.bin")->
                    RegisterActionValueBegin(&actionValue)->
                    RegisterOnMethodExiting()->
                    ExecuteScalingMethodSynchronously("division_by_zero");
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;
            }

			[TestMethod, TestCategory("ReadWriteMethodsTests")]
			void It_should_return_positive_infinity_for_a_division_by_zero()
			{			
				nsConsumer::EVAL_VAR_VALUE* result = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->ReplayReturnValue(L"ActionValue.End");			
				Assert::IsTrue(_fpclass(result->val.f) == _FPCLASS_PINF);
			}

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_get_unscaled_value_in_the_build_in_interface()
            {
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"GetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_not_call_the_set_unscaled_value_in_the_build_in_interface()
            {               
                Assert::AreEqual(0, _Result->GetBuildinRecorder()->GetCallCount(L"SetUnscaledValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_return_as_method_result_true()
            {
                Assert::IsTrue(_Result->GetMethodResult() == METH_SUCCESS);
            }
        };

        [TestClass]
        public ref class When_executing_a_method_that_retrieves_and_sets_a_string_with_non_ASCII_chars_with_PB
        {
            static ParameterAccessMethodHelper^ _Result;
            static wstring* _InputString = new wstring(L"����|R�}32");
            static nsConsumer::EVAL_VAR_VALUE* returnValue;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_ASCII;
                returnValue->size = 10;		//this size matches the DD item test_variable_ascii size
                returnValue->val.s = *Helper::CreateSTRINGFromWstring(*_InputString);

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000C.DDL.bin")->
                    RegisterGetParamValue(returnValue)->
                    RegisterSetParamValue()->
                    ExecuteMethodSynchronously("Simple_Method_With_Get_And_Set_Value_And_Save_Changes_for_ascii");
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;                
                delete returnValue;
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_call_GetParamValue_in_the_builtin_support()
            {
                Assert::IsTrue(_Result->GetBuildinRecorder()->WasCalled(L"GetParamValue"));
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_call_SetParamValue_with_the_previously_recorded_string()
            {
                nsConsumer::EVAL_VAR_VALUE* resultValue = (nsConsumer::EVAL_VAR_VALUE*)_Result->GetBuildinRecorder()->GetParameter(L"SetParamValue.pValue");
                wstring resultWString = Helper::CreateWstringFromSTRING(resultValue->val.s);
                System::String^ actual = gcnew String(resultWString.c_str());

                System::String^ expected = gcnew String(_InputString->c_str());

                Assert::AreEqual(expected, actual);
            }
        };

        [TestClass]
        public ref class When_executing_a_method_that_performs_save_values_call_with_PB
        {
            static ParameterAccessMethodHelper^ _Result;
            static nsConsumer::EVAL_VAR_VALUE* returnValue;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_ASCII;
                returnValue->size = 20;
                returnValue->val.s = *Helper::CreateSTRINGFromWstring(L"");

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000C.DDL.bin")->
                    RegisterGetParamValue(returnValue)->
                    RegisterSetParamValue()->
                    ExecuteMethodSynchronously("Simple_Method_With_Set_Value_And_Save_Changes_for_device_ascii_variable");
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;
                delete returnValue;
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_call_OnMethodExiting_with_SaveOnExit()
            {
                Int32^ expected = gcnew Int32(nsEDDEngine::Save);
                Int32^ actual = gcnew Int32(*(nsEDDEngine::ChangedParamActivity*)_Result->GetBuildinRecorder()->GetParameter(L"OnMethodExiting.eTermAction"));
                Assert::AreEqual(expected, actual);
            }
        };

        [TestClass]
        public ref class When_executing_a_method_that_doesnt_perform_save_values_call_with_PB
        {
            static ParameterAccessMethodHelper^ _Result;
            static nsConsumer::EVAL_VAR_VALUE* returnValue;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_ASCII;
                returnValue->size = 20;
                returnValue->val.s = *Helper::CreateSTRINGFromWstring(L"123456790abcdefghij");

                _Result = ParameterAccessMethodHelper::
                    LoadEdd("ProfiBus\\FDI_PB_Test_000C.DDL.bin")->
                    RegisterGetParamValue(returnValue)->
                    RegisterSetParamValue()->
                    ExecuteMethodSynchronously("Simple_Method_With_Get_And_Set_Value_Without_Saving_Changes_for_ascii");
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete _Result;
                delete returnValue;
            }

            [TestMethod, TestCategory("ReadWriteMethodsTests")]
            void It_should_call_OnMethodExiting_with_SaveOnExit()
            {
                Int32^ expected = gcnew Int32(nsEDDEngine::Save);
                Int32^ actual = gcnew Int32(*(nsEDDEngine::ChangedParamActivity*)_Result->GetBuildinRecorder()->GetParameter(L"OnMethodExiting.eTermAction"));
                Assert::AreEqual(expected, actual);
            }
        };

    }
}
