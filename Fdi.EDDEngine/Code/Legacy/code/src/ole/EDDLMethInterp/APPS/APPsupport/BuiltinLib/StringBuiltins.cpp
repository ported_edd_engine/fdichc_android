//Start Of file
//Added By Anil June 17 2005 --starts here
#include "stdafx.h"
#include "BuiltIn.h"
#include "ddbdefs.h"
#include "string.h"
#include <cmath>

/****************************Start of DD_STRING  Builtins  (eDDL) ********************/

wchar_t* CBuiltIn::STRSTR(wchar_t*  string_var,wchar_t* substring_to_find)
{
	wchar_t* szRetVal = NULL;
	if( string_var && substring_to_find )
	{
		szRetVal = wcsstr( string_var, substring_to_find );
	}
	return szRetVal;
}

wchar_t* CBuiltIn::STRUPR(wchar_t* string_var)
{
	if (string_var)
	{
		PS_Wcsupr(string_var, wcslen(string_var) + 1);
	}	
	return string_var;

}

wchar_t* CBuiltIn::STRLWR(wchar_t* string_var)
{	
	if( string_var )
	{
        PS_Wcslwr(string_var, ::wcslen(string_var) + 1);
	}
	return string_var;
}

int CBuiltIn::STRLEN(wchar_t* string_var)
{
	int nRetVal = 0;
	if( string_var )
	{
		nRetVal = (int)wcslen(string_var);
	}
	return nRetVal;
}

int CBuiltIn::STRCMP(wchar_t* string_var1, wchar_t* string_var2)
{
	int nRetVal = -1;
	if( string_var1 == NULL && string_var2 == NULL )//beware of being passed NULL pointers
	{
		nRetVal = 0;//if they are both NULL (empty) then say they match.
	}
	else if( string_var1 == NULL || string_var2 == NULL )//beware of being passed NULL pointers
	{
		nRetVal = -1;
	}
	else
	{
		nRetVal = wcscmp(string_var1,string_var2);
    }
	return nRetVal;
}


wchar_t* CBuiltIn::STRTRIM(wchar_t* string_var)
{
	if( string_var ) //check for NULL pointer
	{
		int istrLen = (int)wcslen(string_var);
		int iNoOfspace = 0;
		for(int icount = 0; icount < (istrLen - iNoOfspace); icount++)
		{
			if( (string_var[icount] == _T(' ')) || (string_var[icount] == _T('\t')) || (string_var[icount] == _T('\r'))|| (string_var[icount] == _T('\n')) )
			{				
				for(int iRef= icount ;  iRef < (istrLen - iNoOfspace); iRef++)
				{
					string_var[iRef] = string_var[iRef+1];	
				}
				iNoOfspace++;
				icount--;
			}
			else
			{
					break;
			}
		}
		istrLen = (int)wcslen(string_var);
		for(int icount = istrLen; icount > 0; icount--)
		{
			if( (string_var[icount-1] == _T(' ')) || (string_var[icount-1] == _T('\t')) || (string_var[icount-1] == _T('\r'))|| (string_var[icount-1] == _T('\n')) )
			{
				string_var[icount-1]=_T('\0');
			}
			else
			{
					break;
			}
		}
	}
	return string_var;

}

//Get a substring from a string specifying starting position and length.
wchar_t* CBuiltIn::STRMID(wchar_t* string_var,int start, int len)
{
	wchar_t* szReturnValue = NULL;
	if( string_var )
	{
		int nSourceStringLength = (int)wcslen(string_var);

		if(  (start < nSourceStringLength) && (len != 0) && (0 != nSourceStringLength))  	
		{
			int strLen = ((nSourceStringLength - start + 1) > len) ? len : (nSourceStringLength - start + 1);	//taking the minimum
			szReturnValue = new wchar_t[strLen+1];	//this is NOT a memory leak, it will be freed by the calling function.
			memset(szReturnValue, 0, strLen+1);

			for(int nIndex=0; nIndex<strLen; nIndex++)
			{
				szReturnValue[nIndex] = string_var[start+nIndex];
			}
			szReturnValue[strLen] = 0;
		}
	}
	return szReturnValue;
}


//Added By Anil June 17 2005 --Ends here
/****************************End of DD_STRING  Builtins (eDDL) ********************/

//End Of File
