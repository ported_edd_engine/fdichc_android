/**
 *	@(#) $Id: cm_rod.h,v 1.1 2012/07/10 20:59:28 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *	
 *	This file contains all definitions for ROD manager objects.
 */

#pragma once

#include "od_defs.h"
#include "cm_struc.h"
#define EDD_BINARY_FILE_NAME_LENGTH 256
/*
 *	Object Codes
 */

#define OC_NULL				 	 0
#define OC_OD_DESCRIPTION		 1
#define OC_DOMAIN				 2 


/*
 *	Object Dictionary
 */

typedef struct tag_OBJECT_DICTIONARY {
	DDEF_DESC	ddef_desc;
	OBJECT	**sod;
    unsigned long version;
	DDOD_HEADER header;
    wchar_t EDDBinaryFilename[EDD_BINARY_FILE_NAME_LENGTH];
} OBJECT_DICTIONARY;

/*
 *	ROD Table
 */

typedef struct tag_ROD_TBL {
	int					  count;
	OBJECT_DICTIONARY	**list;
} ROD_TBL;
