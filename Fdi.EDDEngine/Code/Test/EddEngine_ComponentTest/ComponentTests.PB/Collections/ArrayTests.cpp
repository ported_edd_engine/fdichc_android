#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_requesting_an_ARRAY_OF_VARIABLES_with_PB
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_array_item_id;
            static ITEM_ID* test_array_member_item_id;
            static FLAT_ITEM_ARRAY* test_array;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000D.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_array_variable", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_ITEM_ARRAY);

                test_array = dynamic_cast<FLAT_ITEM_ARRAY*>(genericItem->item);

                // get the item ids for all parameters
                test_array_item_id = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_array_variable", test_array_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_array_member_item_id = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_variable_in_array", test_array_member_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_array_item_id;
                delete genericItem;
                delete test_array_member_item_id;
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_array()
            {
                Assert::AreEqual(test_array->id, *test_array_item_id );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_number_of_members()
            {
                Assert::AreEqual(2u, (UINT32)test_array->elements.count );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_first_member()
            {
                Assert::AreEqual(*test_array_member_item_id, test_array->elements.list[0].ref.id );
            }
        };

        [TestClass]
        public ref class When_requesting_an_ARRAY_OF_COLLECTION_with_PB
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_array_item_id;
            static ITEM_ID* test_array_member_item_id;
            static FLAT_ITEM_ARRAY* test_array;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000D.DDL.bin");                
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_array_collection", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_ITEM_ARRAY);

                test_array = dynamic_cast<FLAT_ITEM_ARRAY*>(genericItem->item);

                // get the item ids for all parameters
                test_array_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_array_collection", test_array_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_array_member_item_id = new ITEM_ID();
                returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_collection_by_array", test_array_member_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_array_item_id;
                delete genericItem;
                delete test_array_member_item_id;
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_array()
            {
                Assert::AreEqual(test_array->id,  *test_array_item_id );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_number_of_members()
            {
                Assert::AreEqual(1u,(UINT32) test_array->elements.count );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_first_member()
            {
                Assert::AreEqual(*test_array_member_item_id, test_array->elements.list[0].ref.id );
            }
        };

        [TestClass]
        public ref class When_reading_values_from_a_VALUE_ARRAY_with_PB
        {
        private: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_array_item_id;
            static ITEM_ID* test_array_type_item_id;
            static FLAT_ARRAY* test_array;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000D.DDL.bin");                
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("array_of_values", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_ARRAY);

                test_array = dynamic_cast<FLAT_ARRAY*>(genericItem->item);

                // get the item ids for all parameters
                test_array_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"array_of_values", test_array_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_array_type_item_id = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_uint", test_array_type_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_array_item_id;
                delete genericItem;
                delete test_array_type_item_id;
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_array()
            {
                Assert::AreEqual(test_array->id,  *test_array_item_id );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_number_of_members()
            {
                Assert::AreEqual(2u,(UINT32) test_array->num_of_elements );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_array_type()
            {
                Assert::AreEqual(*test_array_type_item_id, test_array->type);
            }
        };

    }
}
