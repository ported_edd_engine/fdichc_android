﻿<Method>
{
 unsigned long  ulVar;

 ulVar = method_time_value.DEFAULT_VALUE;
 PUT_MESSAGE(literal_string(xxx));
}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: test_time_value_DEFAULT_VALUE = 0x%[08x]{ulVar}
End of Function literal_string
IBuiltinSupport InfoRequest() is being called with "test_time_value_DEFAULT_VALUE = 0x00000001"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: test_time_value_DEFAULT_VALUE = 0x%[08x]{ulVar}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><ulVar>1,</ulVar></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
