#pragma once

#include "nsEDDEngine/Common.h"
#include "nsEDDEngine/Attrs.h"
#include "nsEDDEngine/Table.h"

namespace nsEDDEngine
{
	// The IConvenience interface provides several functions 
	// which the Consumer can use to answer common questions.
	class IConvenience
	{
	public:
		virtual ~IConvenience(void) {};

		//
		// Unit Convenience functions
		//

		// Gets the Unit Relation ItemId for the given Axis
		virtual int GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID *pUnitItemId) = 0;

		// Gets the Unit Relation ItemId for the specified parameter
		virtual int GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID *pUnitItemId) = 0;

		//
		// Command Convenience functions
		//

		// Retrieves the List of Commands that can be used to read/write this parameter
		virtual int GetCommandList(int iBlockInstance, FDI_PARAM_SPECIFIER *pParamSpec, CommandType eCmdType, FDI_COMMAND_LIST *pCommandList) = 0;

		// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
		virtual int GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID *pCmdItemId) = 0;

		//
		// String Convenience functions
		//

		// Converts an EDD Engine Error code to a string
		virtual wchar_t *GetErrorString(int iErrorNum) = 0;

		// Given a multi-language string, returns only the language specified
		virtual int GetStringTranslation(const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size) = 0;

		//
		// Type Convenience functions
		//

		// Returns the Item Type of the specified Item
		virtual int GetItemType(int iBlockInstance, FDI_ITEM_SPECIFIER* pItemSpec, ITEM_TYPE *pItemType) = 0;

		// Returns the Item Type and Item Id of the specified Item
		virtual int GetItemTypeAndItemId(int iBlockInstance, FDI_ITEM_SPECIFIER* pItemSpec, ITEM_TYPE *pItemType, ITEM_ID *pItemId) = 0;

		// Returns the Variable Type of the specified parameter
		virtual int GetParamType(int iBlockInstance, FDI_PARAM_SPECIFIER* pParamSpec, TYPE_SIZE *pTypeSize) = 0;

		//
		virtual int GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size) = 0; 

		// Returns the corresponding ItemId of a specifed Symbol Name
		virtual int GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* IptemId) = 0; 

	};

}
