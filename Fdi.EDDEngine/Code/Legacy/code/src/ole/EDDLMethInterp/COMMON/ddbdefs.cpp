/*************************************************************************************************
 *
 * $Workfile: ddbdefs.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		holds global definitions and instantiates string helpers
 *		
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */
// stevev 20feb07 - merge- noway...use the general   #include "stdafx.h"  // HOMZ Port to 2003

#include "stdafx.h"
#include "ddbdefs.h"
#include "dllapi.h"

/* * * * * global strings * * * * */
const char varientTypeStrings[VARIENTYPECOUNT][VARIENTTYPE_LEN] = {VARIENTTYPESTRING};

const wchar_t* GetItemTypeString(itemType_t type)
{
	const wchar_t *pStr = nullptr;

	switch(type)
	{
	case iT_ReservedZeta:
		pStr = L"Reserved Zeta";
		break;
	case iT_Variable:
		pStr = L"VARIABLE";
		break;
	case iT_Command:
		pStr = L"COMMAND";
		break;
	case iT_Menu:
		pStr = L"MENU";
		break;
	case iT_EditDisplay:
		pStr = L"EDIT_DISPLAY";
		break;
	case iT_Method:
		pStr = L"METHOD";
		break;
	case iT_Refresh:
		pStr = L"REFRESH";
		break;
	case iT_Unit:
		pStr = L"UNIT";
		break;
	case iT_WaO:
		pStr = L"WRITE_AS_ONE";
		break;
	case iT_ItemArray:
		pStr = L"ITEM_ARRAY";
		break;
	case iT_Collection:
		pStr = L"COLLECTION";
		break;
	case iT_Block_B:
		pStr = L"BLOCK_B";		//11
		break;
	case iT_Block:
		pStr = L"BLOCK";			//12
		break;
	case iT_Program:
		pStr = L"PROGRAM";		//13
		break;
	case iT_Record:
		pStr = L"RECORD";		//14
		break;
	case iT_Array:
		pStr = L"ARRAY";
		break;
	case iT_VariableList:
		pStr = L"VARIABLE_LIST";
		break;
	case iT_ResponseCodes:
		pStr = L"RESPONSE_CODES";
		break;
	case iT_Member:
		pStr = L"MEMBER";
		break;
	case iT_Domain:
		pStr = L"DOMAIN";
		break;
	case iT_File:
		pStr = L"FILE";
		break;
	case iT_Chart:
		pStr = L"CHART";
		break;
	case iT_Graph:
		pStr = L"GRAPH";
		break;
	case iT_Axis:
		pStr = L"AXIS";
		break;
	case iT_Waveform:
		pStr = L"WAVEFORM";
		break;
	case iT_Source:
		pStr = L"SOURCE";
		break;
	case iT_List:
		pStr = L"LIST";
		break;
	case iT_Grid:
		pStr = L"GRID";
		break;
	case iT_Image:
		pStr = L"IMAGE";
		break;
	case iT_BLOB:
		pStr = L"BLOB";
		break;
	case iT_Plugin:
		pStr = L"PLUGIN";
		break;
	case iT_Template:
		pStr = L"TEMPLATE";
		break;
	case iT_Reserved:
		pStr = L"RESERVED";
		break;
	case iT_Component:
		pStr = L"COMPONENT";
		break;
	case iT_Component_folder:
		pStr = L"COMPONENT_FOLDER";
		break;
	case iT_Component_reference:
		pStr = L"COMPONENT_REFERENCE";
		break;
	case iT_Component_relation:
		pStr = L"COMPONENT_RELATION";
		break;

	default:
		pStr = L"Item Type String not Found";
		break;
	}

	return pStr;
}


/*=*=*=*=*=*=*=*=*=*=* Parameter Reference Structure (copied from Attrs.h) *=**=*=*=*=*=*=*=*=*/

// copy constructor for PARAM_REF_INFO_LIST
PARAM_REF_INFO_LIST::PARAM_REF_INFO_LIST( const PARAM_REF_INFO_LIST &op_ref_info_list )
{
    AssignParamRefInfoList(op_ref_info_list);
}

// Overloaded Assignment Operator for PARAM_REF_INFO_LIST
PARAM_REF_INFO_LIST& PARAM_REF_INFO_LIST::operator= (const PARAM_REF_INFO_LIST &op_ref_info_list )
{
	if (this != &op_ref_info_list)
    {
        delete [] this->list;
        this->list = nullptr;
        AssignParamRefInfoList(op_ref_info_list);
    }
    return *this;
}
// AssignOpRefInfoList is used to assign all the attributes of list PARAM_REF_INFO_LIST
void PARAM_REF_INFO_LIST::AssignParamRefInfoList( const PARAM_REF_INFO_LIST &op_ref_info_list )
{
    count   = op_ref_info_list.count;
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list		= new PARAM_REF_INFO[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for MEMBER is called
			list[i]   = op_ref_info_list.list[i];
		}
	}

}


// destructor for PARAM_REF_INFO_LIST
PARAM_REF_INFO_LIST::~PARAM_REF_INFO_LIST()
{
	delete [] this->list;
	this->list = nullptr;
	this->count = 0;
}

PARAM_REF::PARAM_REF()
{
    op_ref_type = STANDARD_TYPE;
    // op_info has own constructor
    // op_info_list has own constructor
	ulBlockItemId = 0;
	ulOccurrence = 0;
	bDirectRef = false;
};


//Copy Constructor for PARAM_REF
PARAM_REF::PARAM_REF(const PARAM_REF &op_ref)
{
	AssignParam_Ref(op_ref);
}

//Overloaded assignment operator for PARAM_REF
PARAM_REF& PARAM_REF:: operator= (const PARAM_REF &op_ref)
{
	if (this != &op_ref)
	{
		AssignParam_Ref(op_ref);
	}
	return *this;
}

//Compare PARAM_REF
bool PARAM_REF:: operator==(const PARAM_REF &op_ref)
{
	if (op_ref.op_ref_type != op_ref_type)
	{
		return false;
	}
	else if ((op_ref.ulBlockItemId != ulBlockItemId) || (op_ref.ulOccurrence != ulOccurrence))
	{
		return false;
	}

	if (op_ref_type == STANDARD_TYPE)
	{
		if ( (op_ref.op_info.id != op_info.id) || (op_ref.op_info.member != op_info.member) || (op_ref.op_info.type != op_info.type) )
		{
			return false;
		}
	}
	else if (op_ref_type == COMPLEX_TYPE)
	{
		if (op_ref.op_info_list.count != op_info_list.count)
		{
			return false;
		}
		for (int i = 0; i < op_info_list.count; i++)
		{
			if ( (op_ref.op_info_list.list[i].id != op_info_list.list[i].id) || (op_ref.op_info_list.list[i].member != op_info_list.list[i].member) || (op_ref.op_info_list.list[i].type != op_info_list.list[i].type) )
			{
				return false;
			}
		}
	}

	return true;
}

 //AssignOp_Ref is used to assign all the attributes of PARAM_REF
 void PARAM_REF::AssignParam_Ref(const PARAM_REF &op_ref)
 {
	 op_ref_type    = op_ref.op_ref_type;
	 op_info        = op_ref.op_info;
	 op_info_list   = op_ref.op_info_list;
	 ulBlockItemId	= op_ref.ulBlockItemId;
	 ulOccurrence	= op_ref.ulOccurrence;
	 bDirectRef		= op_ref.bDirectRef;
 }

PARAM_REF::~PARAM_REF()
{
    op_ref_type = STANDARD_TYPE;
    // op_info has own destructor
    // op_info_list has own destructor
	ulBlockItemId = 0;
	ulOccurrence = 0;
	bDirectRef = false;
}

void PARAM_REF::addOperationalID (unsigned long itemId, unsigned long memberId, itemType_t itemType)
{
	//check if this reference is indirection
	if (bDirectRef == false)
	{
		//this instance has initial value or tempted to add indirection item ID and member ID rejected
		switch (itemType)
		{
		case iT_ItemArray:		//indirection
		case iT_Collection:		//indirection
		case iT_VariableList:	//indirection
		case iT_File:			//kind of indirection for method interpreter only
			break;
		default:
			bDirectRef = true;
			break; 
		}
	}

	//if the reference idem id can be resolved, no operative item id is generated and this function returns. 
	if (bDirectRef == false)
	{
		return;
	}

	if ((this->op_info.id == 0) && (this->op_ref_type == STANDARD_TYPE))
	{
		//first entry
		this->op_info.id = itemId;
		this->op_info.member = memberId;
		this->op_info.type = itemType;
	}
	else
	{
		if (this->op_ref_type == STANDARD_TYPE)
		{
			if ( (this->op_info.id == itemId) && (this->op_info.type == itemType)
				 && ((this->op_info.member == 0) || (itemType == iT_Array) || (itemType == iT_List)) )
			{
				this->op_info.member = memberId;
			}
			else if ( (this->op_info.id != itemId) || (this->op_info.member != memberId) || (this->op_info.type != itemType) )
			{
				//first complex reference entry
				this->op_ref_type = COMPLEX_TYPE;

				//create a new PARAM_REF_INFO list
				this->op_info_list.list = new PARAM_REF_INFO[2];

				//update PARAM_REF_INFO_LIST
				this->op_info_list.list[0].id = this->op_info.id;
				this->op_info_list.list[0].member = this->op_info.member;
				this->op_info_list.list[0].type = this->op_info.type;
				this->op_info.id = 0;
				this->op_info.member = 0;
				this->op_info.type = iT_ReservedZeta;

				this->op_info_list.list[1].id = itemId;
				this->op_info_list.list[1].member = memberId;
				this->op_info_list.list[1].type = itemType;

				this->op_info_list.count = 2;
			}
		}
		else
		{
			if ( (this->op_info_list.list[this->op_info_list.count-1].id == itemId) 
				 && (this->op_info_list.list[this->op_info_list.count-1].type == itemType)
				 && ((this->op_info_list.list[this->op_info_list.count-1].member == 0) || (itemType == iT_Array) || (itemType == iT_List)) )
			{
				//over write the group member ID
				this->op_info_list.list[this->op_info_list.count-1].member = memberId;
			}
			else if ( (this->op_info_list.list[this->op_info_list.count-1].id != itemId) 
				 || (this->op_info_list.list[this->op_info_list.count-1].type != itemType) )
			{
				//save the pointer to the existing PARAM_REF_INFO list
				PARAM_REF_INFO *pExistList = this->op_info_list.list;

				//create a new PARAM_REF_INFO list
				this->op_info_list.list = new PARAM_REF_INFO[this->op_info_list.count + 1];

				//update PARAM_REF_INFO_LIST
				for (int i = 0; i < this->op_info_list.count; i++)
				{
					this->op_info_list.list[i] = pExistList[i];
				}
				this->op_info_list.list[this->op_info_list.count].id = itemId;
				this->op_info_list.list[this->op_info_list.count].member = memberId;
				this->op_info_list.list[this->op_info_list.count].type = itemType;
				this->op_info_list.count += 1;

				//delete the existing one
				delete [] pExistList;
			}
		}
	}
}

bool PARAM_REF::isOperationalIDValid ()
{
	bool bRetVal = false;

	if ((this->op_ref_type == STANDARD_TYPE) && (this->op_info.id == 0) && (this->op_info.type == iT_ReservedZeta))
	{
		bRetVal = false;
	}
	else
	{
		bRetVal = true;
	}

	return bRetVal;
}

void PARAM_REF::addBlockID (unsigned long blockItemId, unsigned long occurrence)
{
	ulBlockItemId = blockItemId;
	ulOccurrence = occurrence;
}


/* * * * * global helper functions * * * * */


// double
// SXXX XXXX XXXX MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM MMMM 
// float
// SXXX XXXX XMMM MMMM MMMM MMMM MMMM MMMM 

/************************************************************************************************
 * Usable AlmostEqual function
 * by Bruce Dawson
 *
 * parameters:	two floats to compare
 *				maxUlps - number of FLT_EPSILONs between the two floats where they
 *							will be considered equal
 *				
 * returns:		true  on floats almost equal (within maxUlps * FLT_EPSILON of each other)
 *				false when floats are > (maxUlps * FLT_EPSILON apart)
 *
 ***********************************************************************************************/
bool AlmostEqual2sComplement(float A, float B, int maxUlps)
{   // Make sure maxUlps is non-negative and small enough that the

    // default NAN won't compare as equal to anything.

    if (! (maxUlps > 0 && maxUlps < 4 * 1024 ) )// make sure we are sane
		throw 4;

    int aInt = *(int*)&A;	// float to int (see bit pattern above)

    // Make aInt lexicographically ordered using a twos-complement int
    if (aInt < 0) // 2's complement abs(aInt) via aInt = -0 - aInt;
        aInt = 0x80000000 - aInt;

    int bInt = *(int*)&B;	// float to int (see bit pattern above)

    // Make bInt lexicographically ordered using a twos-complement int
    if (bInt < 0)
        bInt = 0x80000000 - bInt;

	// aInt and bInt are conceptually the number of FLT_EPSILONs in the values
    int intDiff = abs(aInt - bInt);// absolute value of the difference

    if (intDiff <= maxUlps)// number of FLT_EPSILONs difference is within spec'ed difference
        return true;

    return false;
}

bool AlmostEqual2sComplement(double A, double B, int maxUlps)
{   // Make sure maxUlps is non-negative and small enough that the

    // default NAN won't compare as equal to anything.

    if ( !(maxUlps > 0 && maxUlps < 4 * 1024 ) )// make sure we are sane
		throw 5;

    __int64 aInt = *(__int64*)&A;	// float to int (see bit pattern above)

    // Make aInt lexicographically ordered using a twos-complement int
    if (aInt < 0)
        aInt = 0x8000000000000000 - aInt;

    __int64 bInt = *(__int64*)&B;	// float to int (see bit pattern above)

    // Make bInt lexicographically ordered using a twos-complement int
    if (bInt < 0)
        bInt = 0x8000000000000000 - bInt;

	// aInt and bInt are conceptually the number of FLT_EPSILONs in the values

#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS 7
	// error C2668: 'abs' : ambiguous call to overloaded function
	// SOLUTION - added double logic
	double dblDiff = double(aInt - bInt);
    unsigned __int64 intDiff = (unsigned __int64)abs(dblDiff);// absolute value of the difference
#else
	int intDiff = abs(aInt = bInt);
#endif

    if (intDiff <= (unsigned  long long)maxUlps)// number of FLT_EPSILONs difference is within spec'ed difference
        return true;

    return false;
}

/*************************************************************************************************
 *
 *   $History: ddbdefs.cpp $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/Common
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
