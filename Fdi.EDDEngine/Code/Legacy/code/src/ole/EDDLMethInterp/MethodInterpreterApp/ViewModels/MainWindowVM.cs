using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Input;
using EDDEngineCOM.Interop;
using MethodInterpreterApp.Classes;
using System.IO;


namespace MethodInterpreterApp.ViewModels
{
    public interface IMainWindowXaml
    {
        ICommand LoadDDCommand { get; }
        ICommand RunMethodCommand { get; }
        ICommand EndMethodCommand { get; }
        ICommand CancelMethodCommand { get; }
        string runMethodBtnColor { get; set; }
        string beginMethodName { get; set; }
        ICommand OKRespCommand { get; }
        ICommand CancelRespCommand { get; }
        ICommand MethodExhaustCommand { get; }
        ICommand ThreadSafetyCommand { get; }
        string UIDisplayText { get; set; }
        string UIInputText { get; set; }
        uint FavoriteUISel { get; set; }
        string sFavoriteMethod { get; set; }
        string MethodArgText { get; set; }
        string ActionVarItemId { get; set; }
        ICommand LoadActionCommand { get; }
        int iFavoriteAction { get; set; }   //zero based
    }

    public interface IMainWindow    //used in MainWindow.xaml.cs
    {
        bool Stop { get; set; }
		string FileToRun { get; set; }
        string sFFBlockName { get; set; }
        short Protocol { get; set; }
        short MethodType { get; set; }
	}

    public class MainWindowVM : INotifyPropertyChanged/*ViewModel*/, ICommand, IMainWindow, IMainWindowXaml
    {
        #region private
		private string _filetorun = "";
        private string _sFFBlockName = "";
        private string _sUIDisplay = "User interface message displays here";
        private string _sUIInput = "";
        private uint _uiUIFavoriteSel = 0;
        private List<string> _lUISelections = new List<string>();
        private uint _ulInputSelectionType = 0;
        private Queue<Task> _workQueue;
        private AutoResetEvent _are;
        private bool _stop;
        private short _protocol = 1;    //default selection is ProtocolType::HART, see ProtocolType.h
        private string _methodBtnColor = "White";
        private string _beginMethodBtnName = "Begin Method";
        private IEDDMethods iDDSPB = null;     //COM instance is deleted when the window is closed
        private static short _countRun = 0;
        private object oUIInput;
        private Object oUISelect;
        private sbyte bButton = 0;       //cancel button hit
        private ButtonState eBtnState = ButtonState.INIT;
        private Object oMethodList;
        private List<string> _lMethodList = new List<string>();
        private List<string> _lActionList = new List<string>();
        private string _sFavoriteMethSel = "";
        private string _sMethodArg = "";
        private int _iFavoriteActSel = 0;
        private short _methodType = 1;    //default selection is method (1). Action selection is 2.
        private string _sActionVarItemId = "";

        private enum ButtonType
        {
            BEGIN = 0,
            END = 1,
            CANCEL = 2,
            LOADDD = 3,
            LOADACTION = 4
        };

        //used for combo input, selection and menu rewquest
        private enum ButtonState
        {
            INIT = 0,       //UI selction window is empty
            WAITING = 1,    //Waiting for button action: okay or cancel
            HIT = 2         //button action occurred
        };

        private enum enumUIRequestType
        {
            WINNONE = 0,
            WINEDIT = 1,
            WINSELECT = 2		//for combo input, selection and menu rewquest
        };


        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChange(string propertyName)
        {  
            if (PropertyChanged != null)  
            {  
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));  
            }  
        }

		private void RunMethod( string filename, string sFFBlockName )
		{
			AddRunMethod( filename, sFFBlockName );
		}
        private void EndMethod(string filename)
        {
            EndRunMethod(filename);
        }
        private void CancelMethod(string filename)
        {
            CancelRunMethod(filename);
        }

        private void WorkerQueue(object state)
        {
            sbyte bUseCallback = 1;   //1 == use callback

            if (iDDSPB == null)
            {
                iDDSPB = new EDDMethodsClass();
            }

            while (!Stop)
            {
                //_are.WaitOne();
                _are.WaitOne(100);  //in millisecond

                // update method execution progress indicator in every 100 milliseconds
                sbyte result = 1;
                if (beginMethodName != "Begin Method")
                {
                    iDDSPB.isMethodCompleted(out result);    //get_IsCompleted()
                    if (result != 0)
                    {
                        if ((bUseCallback != 0) || (_countRun == 0))
                        {
                            beginMethodName = "Begin Method";
                        }
                        if ((bUseCallback != 0) && (_countRun > 0))
                        {
                            runMethodBtnColor = "White";
                            _countRun--;    //no need to hit EndMethod button
                        }
                    }
                }

                //task is driven by "Run Method", "End Method" and "Cancel Method" buttons
                while (!Stop)
                {
                    Task task = null;
                    bool bWork = false;
                    lock (_workQueue)
                    {
                        if (_workQueue.Count > 0)
                        {
                            task = _workQueue.Dequeue();
                            bWork = true;
                        }
                    }

                    if (bWork)
                    {
                        for (int i = 0; i < task.WorkItems.Count; i++)
                        {
                            DDSWorkItem ddsitem = (DDSWorkItem)task.WorkItems[i];

                            try
                            {
                                int error = 0;

                                switch (ddsitem.btnType)
                                {
                                    case (int)ButtonType.LOADDD:
                                        if (!System.IO.File.Exists(ddsitem.Filename))
                                        {
                                            Console.WriteLine("Failed: The DD binary file doesn't exist.", ddsitem.Filename);
                                            error = 1;
                                            break;
                                        }

                                        iDDSPB.CreateDeviceInstanceNLoadDD(ddsitem.Filename, Protocol, ddsitem.sFFBlockName ,out oMethodList, out error);
                                        string[] tempStr = (string[])oMethodList;
                                        _lMethodList = new List<string>(tempStr.Length);
                                        _lMethodList.AddRange(tempStr);
                                        OnPropertyChange("MethodList");
                                        break;
                                    case (int)ButtonType.LOADACTION:
                                        if ((_methodType != 2) || (ddsitem.Filename == ""))
                                        {
                                            Console.WriteLine("Failed: The DD Item ID is not entered.", ddsitem.Filename);
                                            error = 1;
                                            break;
                                        }

                                        iDDSPB.LoadActionList(_sActionVarItemId, out oMethodList, out error);    //Filename here is actually item ID in string format
                                        string[] strArray = (string[])oMethodList;
                                        _lActionList = new List<string>(strArray.Length);
                                        _lActionList.AddRange(strArray);
                                        OnPropertyChange("ActionList");
                                        break;

                                    case (int)ButtonType.BEGIN:
                                        runMethodBtnColor = "Orange";
                                        beginMethodName = "Running...";
                                        if (_methodType == 1)   //method
                                        {
                                            if (_sMethodArg != "")
                                            {
                                                string sMethod = _sFavoriteMethSel;
                                                int iIndex1 = sMethod.IndexOf('(', 0) + 1;
                                                int iIndex2 = sMethod.IndexOf(')', iIndex1);
                                                sMethod = sMethod.Remove(iIndex1, iIndex2 - iIndex1 + 1);   //remove argument string
                                                sMethod = sMethod + _sMethodArg + ")";                //attach argument value string
                                                iDDSPB.BeginMethodInterp(sMethod, (sbyte)bUseCallback, out error);
                                            }
                                            else
                                            {
                                                iDDSPB.BeginMethodInterp(_sFavoriteMethSel, (sbyte)bUseCallback, out error);
                                            }
                                        }
                                        else if (_methodType == 2)  //action
                                        {
                                            iDDSPB.BeginVarActionMethodInterp(_sActionVarItemId, _iFavoriteActSel, (sbyte)bUseCallback, out error);
                                        }
                        
                                        _uiUIFavoriteSel = 0;
                                        _lUISelections.Clear();
                                        eBtnState = ButtonState.INIT;
                                        break;
                                    case (int)ButtonType.END:
                                        runMethodBtnColor = "White";
                                        iDDSPB.EndMethodInterp(out error);
                                        break;
                                    case (int)ButtonType.CANCEL:
                                        runMethodBtnColor = "White";
                                        iDDSPB.CancelMethodInterp();
                                        break;
                                }
                            }
                            catch (COMException ex)
                            {
                                Trace.TraceWarning(String.Format("RunMethod - RunMethodInterp({0}), ({1}), ({2})", ddsitem.Filename, Protocol, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                //send out user edit item or user selected item for input request
                if (result == 0)
                {
                    //get prompt string, option string and win type
                    //string userMessage;
                    iDDSPB.GetUIPromptNOption(out _sUIDisplay, out oUISelect, out _ulInputSelectionType);
                    OnPropertyChange("UIDisplayText");

                    //receive user selection list for input request
                    if (((_ulInputSelectionType == (uint)enumUIRequestType.WINSELECT)) && (oUISelect != null) && (eBtnState == ButtonState.INIT))
                    {
                        //It is enum input request or selection request or menu request
                        string[] tempStr = (string[])oUISelect;
                        _lUISelections = new List<string>(tempStr.Length);
                        _lUISelections.AddRange(tempStr);
                        OnPropertyChange("UISelections");
                        eBtnState = ButtonState.WAITING;
                    }

                    if (eBtnState == ButtonState.HIT)
                    {
                        iDDSPB.SetUIUserInputNButton(oUIInput, _uiUIFavoriteSel, bButton);
                        _uiUIFavoriteSel = 0;
                        _lUISelections.Clear();
                        eBtnState = ButtonState.INIT;
                    }
                }
            }

            //window is closed
            if (_countRun != 0)
            {
                Console.WriteLine("{0} Method hasn't ended yet. Closing window will cause memory leaks", _countRun);
            }

            if (iDDSPB != null)
            {
                Marshal.ReleaseComObject(iDDSPB);
                iDDSPB = null;
            }
        }

        #endregion

        #region public
        public MainWindowVM()
        {
            _workQueue = new Queue<Task>();
            _are = new AutoResetEvent(false);

            ThreadPool.QueueUserWorkItem(WorkerQueue);
        }

        public bool Stop
        {
            get { return _stop; }
            set
            {
                _stop = value;
                _are.Set();
            }
        }
	
		public string FileToRun
		{
			get { return _filetorun; }
            set { _filetorun = value; } //textbox is updated in MainWindow.xaml.cs
		}

        public string sFFBlockName
        {
            get { return _sFFBlockName; }
            set { _sFFBlockName = value; } //textbox is updated in MainWindow.xaml.cs
        }


        public ICommand LoadDDCommand
        {
            get { return this; }
        }
        public ICommand RunMethodCommand
		{
			get { return this; }
		}
        public ICommand EndMethodCommand
        {
            get { return this; }
        }
        public ICommand CancelMethodCommand
        {
            get { return this; }
        }

        public ICommand OKRespCommand
        {
            get { return this; }
        }
        public ICommand CancelRespCommand
        {
            get { return this; }
        }

        public ICommand MethodExhaustCommand
        {
            get { return this; }
        }
        public ICommand ThreadSafetyCommand
        {
            get { return this; }
        }

        public void AddRunMethod(string file, string sFFBlockName)
		{
			Task t = new Task();
			t.WorkItems = new List<WorkItem>();

			DDSWorkItem ddsitem = new DDSWorkItem();
			ddsitem.Filename = file;
            ddsitem.sFFBlockName = sFFBlockName;
            ddsitem.btnType = (int)ButtonType.BEGIN;        // RunMethod
			t.WorkItems.Add( ddsitem );

			lock (_workQueue)
			{
				_workQueue.Enqueue( t );
				_are.Set();
			}
		}
        public void EndRunMethod(string file)
        {
            Task t = new Task();
            t.WorkItems = new List<WorkItem>();

            DDSWorkItem ddsitem = new DDSWorkItem();
            ddsitem.Filename = file;
            ddsitem.btnType = (int)ButtonType.END;        // EndMethod
            t.WorkItems.Add(ddsitem);

            lock (_workQueue)
            {
                _workQueue.Enqueue(t);
                _are.Set();
            }
        }
        public void CancelRunMethod(string file)
        {
            Task t = new Task();
            t.WorkItems = new List<WorkItem>();

            DDSWorkItem ddsitem = new DDSWorkItem();
            ddsitem.Filename = file;
            ddsitem.btnType = (int)ButtonType.CANCEL;        // CancelMethod
            t.WorkItems.Add(ddsitem);

            lock (_workQueue)
            {
                _workQueue.Enqueue(t);
                _are.Set();
            }
        }
        public ICommand LoadActionCommand
        {
            get { return this; }
        }

        private void AddLoadDD()
        {
            Task t = new Task();
            t.WorkItems = new List<WorkItem>();

            DDSWorkItem ddsitem = new DDSWorkItem();
            ddsitem.Filename = FileToRun;
            ddsitem.sFFBlockName = sFFBlockName;

            ddsitem.btnType = (int)ButtonType.LOADDD;        // Load DD button
            t.WorkItems.Add(ddsitem);

            lock (_workQueue)
            {
                _workQueue.Enqueue(t);
                _are.Set();
            }
        }

        private void AddLoadAction()
        {
            if (_methodType == 2)   //action
            {
                Task t = new Task();
                t.WorkItems = new List<WorkItem>();

                DDSWorkItem ddsitem = new DDSWorkItem();

                ddsitem.btnType = (int)ButtonType.LOADACTION;       // Load action button
                t.WorkItems.Add(ddsitem);

                lock (_workQueue)
                {
                    _workQueue.Enqueue(t);
                    _are.Set();
                }
            }
        }

        public List<string> MethodList
        {
            get { 
                return _lMethodList;
            }
            set
            {
                _lMethodList = value;
                OnPropertyChange("MethodList");
            }
        }
        public string sFavoriteMethod
        {
            get { return (_sFavoriteMethSel); }
            set
            {
                _sFavoriteMethSel = value;
                OnPropertyChange("sFavoriteMethod");
            }
        }
        public string MethodArgText
        {
            get { return _sMethodArg; }
            set
            {
                _sMethodArg = value;
                OnPropertyChange("MethodArgText");
            }
        }

        public List<string> ActionList
        {
            get
            {
                return _lActionList;
            }
            set
            {
                _lActionList = value;
                OnPropertyChange("ActionList");
            }
        }
        public int iFavoriteAction
        {
            get { return (_iFavoriteActSel); }
            set
            {
                _iFavoriteActSel = value;
                OnPropertyChange("iFavoriteAction");
            }
        }
       
        public bool CanExecute(object parameter)
        {
            return true;
        }	

#pragma warning disable 67
        public event EventHandler CanExecuteChanged;
#pragma warning restore 67

        public void Execute(object parameter)
        {
            string s = parameter as string;

            if (s.CompareTo("LoadDD") == 0)
            {
                if (_countRun == 0)
                {
                    AddLoadDD();   //load DD only when the method is not running
                }
            }
            else if (s.CompareTo("RunMethod") == 0)
			{
                if ((FileToRun.CompareTo("") != 0) && (_countRun == 0))
                {
                    RunMethod(FileToRun, sFFBlockName);
                    _countRun++;
                }
			}
            else if (s.CompareTo("EndMethod") == 0)
            {
                if (_countRun > 0)
                {
                    EndMethod(FileToRun);
                    _countRun--;
                }
            }
            else if (s.CompareTo("CancelMethod") == 0)
            {
                CancelMethod(FileToRun);
                if (_countRun > 0)
                {
                    _countRun--;
                }
            }
            else if (s.CompareTo("OKResponse") == 0)
            {
                bButton = 1;       //ok
                eBtnState = ButtonState.HIT;
            }
            else if (s.CompareTo("CancelResponse") == 0)
            {
                bButton = 0;        //cancel
                eBtnState = ButtonState.HIT;
            }
            else if (s.CompareTo("LoadAction") == 0)
            {
                if ((_countRun == 0) && (_sActionVarItemId != ""))
                {
                    AddLoadAction();   //load variable action list only when the method is not running and when DD item ID is not empty
                }
            }
            else if (s.CompareTo("MethodExhaustResponse") == 0)
            {
                int loopCount = 0;

                if (_countRun == 0)
                {
                    //testing method, not action
                    _methodType = 1;
                    FileToRun = Directory.GetCurrentDirectory() + "\\..\\Devices\\HART\\000026\\008f\\0101.fm8"; //default binaray Hart file
                    sFFBlockName = "DATATYPESTB";   // not used in this test
                    //_sFavoriteMethSel = "16584, division_by_zero";
                    _sFavoriteMethSel = "16681, method_calling_method_with_complex_ref";

                    AddLoadDD();                    //load DD only when the method is not running
                }

                while (loopCount < 50000)
                {
                    if ((runMethodBtnColor == "White") && (_countRun == 0))
                    {
                        RunMethod(FileToRun, sFFBlockName);     //method will automatically exit without calling EndMethod()
                        _countRun++;

                        loopCount++;
                    }
                    else
                    {
                        bButton = 1;       //ok
                        eBtnState = ButtonState.HIT;
                    }
                    Thread.Sleep(10);  //0.01 second
                }
            }
            else if (s.CompareTo("ThreadSafetyResponse") == 0)
            {
            }
        }

        public short Protocol
        {
            get { return _protocol; }
            set { _protocol = value; }
        }

        public short MethodType
        {
            get { return _methodType; }
            set { _methodType = value; }
        }
        
        public string runMethodBtnColor
        {
            get { return _methodBtnColor; }
            set { _methodBtnColor = value; 
                  OnPropertyChange("runMethodBtnColor");
                }
        }

        public string beginMethodName
        {
            get { return _beginMethodBtnName; }
            set
            {
                _beginMethodBtnName = value;
                OnPropertyChange("beginMethodName");
            }
        }

        public string UIDisplayText
        {
            get { return _sUIDisplay; }
            set
            {
                _sUIDisplay = value;
                OnPropertyChange("UIDisplayText");
            }
        }
        public string UIInputText
        {
            get { return _sUIInput; }
            set 
            { 
                _sUIInput = value; 
                OnPropertyChange("UIInputText");
                oUIInput = _sUIInput;
            }
        }
        public uint FavoriteUISel
        {
            get { return (_uiUIFavoriteSel); }
            set 
            {
                _uiUIFavoriteSel = value;
                OnPropertyChange("FavoriteUISel");
            }
        }
        public List<string> UISelections
        {
            get { return _lUISelections; }
            set
            {
                _lUISelections = value;
                OnPropertyChange("UISelections");
            }
        }

        public string ActionVarItemId
        {
            get { return _sActionVarItemId; }
            set
            {
                if (_methodType == 2)
                {
                    _sActionVarItemId = value;
                    OnPropertyChange("ActionVarItemId");
                }
            }
        }

        #endregion
    }
}
