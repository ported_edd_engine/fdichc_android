#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0001_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0001.DDL.bin");
                engine->Initialize();                

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0002_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0002.DDL.bin");
                engine->Initialize();                

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(2u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0003_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");
                engine->Initialize();                

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(3u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0004_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0004.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(4u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(3u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(9u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0005_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0005.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(5u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0006_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0006.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(6u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(6u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(7u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0007_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0007.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(7u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(2u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(3u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0008_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0008.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(8u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(120u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(42u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_0009_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0009.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(9u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(50u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(60u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000A_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000A.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(10u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(9u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(12u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000B_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000B.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(11u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(5u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(123u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000C_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(12u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(15u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(16u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000D_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000D.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(13u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(1u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(1u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000E_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(14u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(28u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(56u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_the_DD_for_Testcase_000F_for_PB
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {       
                Assert::AreEqual(15u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {       
                Assert::AreEqual(9u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {       
                Assert::AreEqual(23u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {       
                Assert::AreEqual(0xfdu,(UINT32) header->device_id.manufacturer);
            }
        };

    }
}
