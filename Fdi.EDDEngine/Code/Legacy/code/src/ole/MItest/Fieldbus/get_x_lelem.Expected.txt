<Method>
{int iR1,iR2,iR3,iR4,iR5,iR6; char dateval[10]; int iDateLength; char sVal[30]; long lStrLen; double dValue; float fValue; long lValue; unsigned long ulValue; unsigned long id,member_id,lst_id,elt_id,subelt_id; int index; index=0;
subelt_id=0;
id=132188;
member_id=3221291131;
id=132225;
elt_id=132220;
dValue=get_double_lelem(id,index,elt_id,subelt_id);
id=131959;
elt_id=131953;
fValue=get_float_lelem(id,index,elt_id,subelt_id);
id=132226;
elt_id=132221;
lValue=get_signed_lelem(id,index,elt_id,subelt_id);
id=132227;
elt_id=132222;
ulValue=get_unsigned_lelem(id,index,elt_id,subelt_id);
id=132228;
elt_id=132224;
lStrLen=32;
iR5=get_string_lelem(id,index,elt_id,subelt_id,sVal,&lStrLen);
iDateLength=11;
id=132229;
elt_id=132223;
iR1=get_date_lelem(id,index,elt_id,subelt_id,dateval,&iDateLength);
}
</Method>
<Function>
Function Name: get_double_lelem
  Number Of Input Parameters: 4
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 132225
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 132220
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
  Returned Param Type: RUL_DOUBLE
  Returned Param Value: 5.550000
End of Function get_double_lelem
Function Name: get_float_lelem
  Number Of Input Parameters: 4
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 131959
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 131953
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
  Returned Param Type: RUL_FLOAT
  Returned Param Value: 3.550000
End of Function get_float_lelem
Function Name: get_signed_lelem
  Number Of Input Parameters: 4
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 132226
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 132221
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 75
End of Function get_signed_lelem
Function Name: get_unsigned_lelem
  Number Of Input Parameters: 4
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 132227
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 132222
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
  Returned Param Type: RUL_UNSIGNED_INT
  Returned Param Value: 75
End of Function get_unsigned_lelem
Function Name: get_string_lelem
  Number Of Input Parameters: 6
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 132228
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 132224
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
    Param 5 Type: RUL_SAFEARRAY
    Param 5 Array: I
    Param 6 Type: RUL_INT
    Param 6 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function get_string_lelem
Function Name: get_date_lelem
  Number Of Input Parameters: 6
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 132229
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_UNSIGNED_INT
    Param 3 Value: 132223
    Param 4 Type: RUL_UNSIGNED_INT
    Param 4 Value: 0
    Param 5 Type: RUL_SAFEARRAY
    Param 5 Array: 0
    Param 6 Type: RUL_INT
    Param 6 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function get_date_lelem
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><iR1>0,</iR1><iR2>0,</iR2><iR3>0,</iR3><iR4>0,</iR4><iR5>0,</iR5><iR6>0,</iR6><dateval>[0]0,[1][2][3][4][5][6][7][8][9]</dateval><iDateLength>1,</iDateLength><sVal>[0]I,[1][2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24][25][26][27][28][29]</sVal><lStrLen>1,</lStrLen><dValue>5.55,</dValue><fValue>3.55000,</fValue><lValue>75,</lValue><ulValue>75,</ulValue><id>132229,</id><member_id>3221291131,</member_id><lst_id>0,</lst_id><elt_id>132223,</elt_id><subelt_id>0,</subelt_id><index>0,</index></SymbolTable>
