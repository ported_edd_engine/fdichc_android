#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        public ref class ImageHelper
        {
        private:
            EddEngineController^ _engine;
            static FDI_GENERIC_ITEM* _genericItem;

            ImageHelper(System::String^ eddFileName)
            {
                _engine = gcnew EddEngineController(eddFileName);
                _engine->Initialize();
                _genericItem = nullptr;
            }

            ~ImageHelper()
            {
                delete _engine;
                delete _genericItem;
            }

        public:
            FLAT_IMAGE* GetImage(System::String ^name)
            {
                FDI_PARAM_SPECIFIER imageToFind;
                _engine->FillParamSpecifier(name, &imageToFind);
                _genericItem = _engine->GetGenericItem(imageToFind.id, nsEDDEngine::ITYPE_IMAGE);
                return dynamic_cast<FLAT_IMAGE*>(_genericItem->item);
            }

            static ImageHelper^ LoadEdd(System::String^ eddFile)
            {
                return gcnew ImageHelper(eddFile);
            }
        };

        public ref class VariableHelper
        {
        private:
            EddEngineController^ _engine;
            static FDI_GENERIC_ITEM* _genericItem; 

            VariableHelper(System::String^ eddFileName)
            {
                _engine = gcnew EddEngineController(eddFileName);
                _engine->Initialize();
            }

            ~VariableHelper()
            { 
                delete _engine;
                delete _genericItem;
            }

        public:
            FLAT_VAR* GetVariable(System::String^ name)
            {
                FDI_PARAM_SPECIFIER elementToFind;
                _engine->FillParamSpecifier(name, &elementToFind);
                _genericItem = _engine->GetGenericItem(elementToFind.id, nsEDDEngine::ITYPE_VARIABLE);
                return dynamic_cast<FLAT_VAR*>(_genericItem->item);
            }

            FLAT_IMAGE* GetImage(System::String^ name)
            {
                FDI_PARAM_SPECIFIER elementToFind;
                _engine->FillParamSpecifier(name, &elementToFind);
                _genericItem = _engine->GetGenericItem(elementToFind.id, nsEDDEngine::ITYPE_IMAGE);
                return dynamic_cast<FLAT_IMAGE*>(_genericItem->item);
            }

            static VariableHelper^ LoadEdd(System::String^ eddFile)
            {
                return gcnew VariableHelper(eddFile);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestVarWithMandatoryValues
        {
        public: 
            static FLAT_VAR* _result;
            static VariableHelper^ helper;

            static void SetupBase(String^ testCaseName)
            {
                helper = VariableHelper::LoadEdd(testCaseName);
                _result = helper->GetVariable("var_only_mandetory_values");
            }

            static void CleanupBase()
            {
                delete helper;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_of_type_integer()
            {
                Assert::AreEqual((int) VT_INTEGER, (int)_result->type_size.type);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_with_class_local()
            {
                Assert::AreEqual((int) CT_LOCAL_A, (int)_result->class_attr);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }


            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_visibility_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::visibility), "visibility");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_height_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::height), "height");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_width_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::width), "width");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_scaling_factor_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::scaling_factor), "scaling_factor");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_display_Format_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::display_format), "display_format");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_edit_Format_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::edit_format), "edit_format");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_validity_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::validity), "validity");
            }

        };

        [TestClass, Ignore]
        public ref class BaseTestVarWithAllValues
        {
        public: 
            static FLAT_VAR* _result;
            static VariableHelper^ helper;
            static void SetupBase(String^ testCaseName)
            {
                helper = VariableHelper::LoadEdd(testCaseName);
                _result = helper->GetVariable("var_all_values");
            }

            static void CleanupBase()
            {
                delete helper;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_of_type_integer()
            {
                Assert::AreEqual((int) VT_INTEGER, (int)_result->type_size.type);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_with_class_local()
            {
                Assert::AreEqual((int) CT_TUNE, (int)_result->class_attr);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_label_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_constant_unit_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::constant_unit), "constant_unit");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_value_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::default_value), "default_value");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_handling_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::handling), "handling");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_help_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::help), "help");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_validity_as_mask()
            {
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::validity), "validity");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_as_constant_unit_my_unit()
            {
                Assert::AreEqual("my unit", gcnew String(_result->constant_unit.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_as_default_value_3()
            {
                Assert::AreEqual((Int64)3, gcnew Int64(_result->default_value.val.i));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_handling_read()
            {
                Assert::AreEqual((int) FDI_READ_HANDLING, (int) _result->handling);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_help_hallo()
            {
                Assert::AreEqual("hallo", gcnew String(_result->help.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_label_abc()
            {
                Assert::AreEqual("abc", gcnew String(_result->label.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_validity_false()
            {
                Assert::AreEqual((int) FALSE, (int) _result->valid);
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassImageWithAttributes
        {
        public:
            static FLAT_IMAGE* _result;
            static ImageHelper^ helper;

            static void SetupBase(String^ testCaseName)
            {
                helper = ImageHelper::LoadEdd(testCaseName);
                _result = helper->GetImage("test_image");
            }

            static void CleanupBase()
            {
                delete helper;
            }

            [TestMethod, TestCategory("ConditionedImageTests")]
            void It_should_provide_a_type_for_the_image_link()
            {
                Assert::AreEqual((int)ITYPE_MENU,(int) _result->link.type);
            }

            [TestMethod, TestCategory("ConditionedImageTests")]
            void It_should_provide_a_label_attribute()
            {
                Assert::IsTrue(1 <= _result->masks.attr_avail.count(label), "No label attribute found");
            }

            [TestMethod, TestCategory("ConditionedImageTests")]
            void It_should_not_have_an_empty_string_as_label()
            {
                Assert::IsTrue(0 < _result->label.length(), "label is an empty string");
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassConditionedImageWithAttributes
        {
        public:
            static FLAT_IMAGE* _result;
            static ImageHelper^ helper;

            static void SetupBase(String^ testCaseName)
            {
                helper = ImageHelper::LoadEdd(testCaseName);
                _result = helper->GetImage("test_image2");
            }

            static void CleanupBase()
            {
                delete helper;
            }

            [TestMethod, TestCategory("ConditionedImageTests")]
            void It_should_provide_a_label()
            {
                Assert::IsTrue(1 <= _result->masks.attr_avail.count(label), "No image_items attribute found");
            }

            [TestMethod, TestCategory("ConditionedImageTests")]
            void It_should_not_have_an_empty_string_as_label()
            {
                Assert::IsTrue(0 < _result->label.length(), "label is an empty string");
            }
        };


        [TestClass]
        public ref class When_checking_the_var_only_mandetory_values_with_FM8 : BaseTestVarWithMandatoryValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0014.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_constant_unit_as_mask()
            {
                // for legacy, Constant_Unit is defaulted
                Assert::IsTrue(_result->isAvailable(nsEDDEngine::constant_unit), "constant_unit");
            }
        };

        [TestClass]
        public ref class When_checking_the_var_only_mandetory_values_with_FMA : BaseTestVarWithMandatoryValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0014.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_not_contain_the_constant_unit_as_mask()
            {
                // for FMA, Constant_Unit should not be defaulted
                Assert::IsFalse(_result->isAvailable(nsEDDEngine::constant_unit), "constant_unit");
            }
        };

        [TestClass]
        public ref class When_checking_the_var_all_values_with_FM8 : BaseTestVarWithAllValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0014.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_checking_the_var_all_values_with_FMA : BaseTestVarWithAllValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0014.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_an_image_with_path_in_FMA : BaseClassImageWithAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0010.fmA");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_an_image_with_path_in_FM8 : BaseClassImageWithAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0010.fm8");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_an_image_with_conditioned_path_in_FMA : BaseClassConditionedImageWithAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0010.fmA");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_an_image_with_conditioned_path_in_FM8 : BaseClassConditionedImageWithAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0010.fm8");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_GetParamType_for_variable_response_code_in_fm8
        {
            FDI_PARAM_SPECIFIER* _param_specifier;
            TYPE_SIZE* _param_type_size;
            int _return_code;

        public:
            [TestInitialize]
            void Setup()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase0001.fm8");
                engine->Initialize();

                _param_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("response_code", _param_specifier);

                _param_type_size = new TYPE_SIZE();
                _return_code = engine->Instance->GetParamType(0, _param_specifier, _param_type_size);
            }

            [TestCleanup]
            void Cleanup()
            {
                delete _param_specifier;
                delete _param_type_size;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_error_code_0()
            {
                Assert::AreEqual(0, _return_code);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_item_id_150()
            {
                Assert::AreEqual(150u, _param_specifier->id);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_type_enumerated()
            {
                Assert::AreEqual(int(VT_ENUMERATED), int(_param_type_size->type));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_size_1()
            {
                Assert::AreEqual(unsigned int(1), _param_type_size->size);
            }
        };

        [TestClass]
        public ref class When_calling_GetParamType_for_variable_device_status_in_fm8
        {
            FDI_PARAM_SPECIFIER* _param_specifier;
            TYPE_SIZE* _param_type_size;
            int _return_code;

        public:
            [TestInitialize]
            void Setup()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase0001.fm8");
                engine->Initialize();

                _param_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("device_status", _param_specifier);

                _param_type_size = new TYPE_SIZE();
                _return_code = engine->Instance->GetParamType(0, _param_specifier, _param_type_size);
            }

            [TestCleanup]
            void Cleanup()
            {
                delete _param_specifier;
                delete _param_type_size;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_error_code_0()
            {
                Assert::AreEqual(0, _return_code);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_item_id_151()
            {
                Assert::AreEqual(151u, _param_specifier->id);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_type_bit_enumerated()
            {
                Assert::AreEqual(int(VT_BIT_ENUMERATED), int(_param_type_size->type));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_size_1()
            {
                Assert::AreEqual(unsigned int(1), _param_type_size->size);
            }
        };

        [TestClass]
        public ref class When_calling_GetParamType_for_variable_comm_status_in_fm8
        {
            FDI_PARAM_SPECIFIER* _param_specifier;
            TYPE_SIZE* _param_type_size;
            int _return_code;

        public:
            [TestInitialize]
            void Setup()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase0001.fm8");
                engine->Initialize();

                _param_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("comm_status", _param_specifier);

                _param_type_size = new TYPE_SIZE();
                _return_code = engine->Instance->GetParamType(0, _param_specifier, _param_type_size);
            }

            [TestCleanup]
            void Cleanup()
            {
                delete _param_specifier;
                delete _param_type_size;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_error_code_0()
            {
                Assert::AreEqual(0, _return_code);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_item_id_152()
            {
                Assert::AreEqual(152u, _param_specifier->id);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_type_bit_enumerated()
            {
                Assert::AreEqual(int(VT_BIT_ENUMERATED), int(_param_type_size->type));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_parameter_size_1()
            {
                Assert::AreEqual(unsigned int(1), _param_type_size->size);
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassDefaultValues
        {
        protected:
            static FLAT_VAR* elementToCheck = nullptr;

        public: 
            static void SetupBase(String^ eddFilename, String^ variableName)
            {
                EddEngineController^ engine = gcnew EddEngineController(eddFilename);
                engine->Initialize();
                FDI_PARAM_SPECIFIER paramSpec;
                engine->FillParamSpecifier(variableName, &paramSpec);
                elementToCheck =  dynamic_cast<FLAT_VAR*>(engine->GetGenericItem(paramSpec.id, ITYPE_VARIABLE)->item);
            }

            static void CleanupBase()
            {
                if(elementToCheck) delete elementToCheck;
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_convertible_to_FLAT_VAR()
            {
                Assert::IsFalse(elementToCheck == nullptr);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_have_a_default_value()
            {
                Assert::IsTrue(elementToCheck->isAvailable(AttributeName::default_value));
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_String_variable_defaulted_in_exact_length_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_short_defaulted_String_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_short_defaulted");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_dd_padded_with_whitespaces()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Test", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_long_defaulted_String_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_too_long_defaulted");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_the_first_10_letters_of_the_string_defined_in_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("too long t", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_an_integer_variable_from_testcase_000E_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_int");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_integer()
            {
                Assert::AreEqual((int) VariableType::VT_INTEGER, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_INTEGER, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(-23ll, elementToCheck->default_value.val.i);
            }
        };

        [TestClass]
        public ref class When_reading_an_unsigned_integer_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_uint");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) VariableType::VT_UNSIGNED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(23ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_an_enumerated_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_enumerated");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_enumerated()
            {
                Assert::AreEqual((int) VariableType::VT_ENUMERATED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_enumerated()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(2ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_a_float_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_float");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_float()
            {
                Assert::AreEqual((int) VariableType::VT_FLOAT, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_float()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_FLOAT, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                float varToCheck = elementToCheck->default_value.val.f - 4.2f;
                Assert::IsTrue(varToCheck <0.001 && varToCheck > -0.001, "float value differs more than 0.001 from expected value");
            }
        };

        [TestClass]
        public ref class When_reading_a_double_variable_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_double");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_double()
            {
                Assert::AreEqual((int) VariableType::VT_DOUBLE, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_double()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_DOUBLE, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                double varToCheck = elementToCheck->default_value.val.d - 4.2;
                Assert::IsTrue(varToCheck > -0.001 && varToCheck < 0.001, "double value differs more than 0.001 from expected value");
            }
        };

        [TestClass]
        public ref class When_reading_a_Password_variable_defaulted_in_exact_length_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_password");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_password_string()
            {
                Assert::AreEqual((int) VariableType::VT_PASSWORD, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("TestString", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_Packed_ASCII_variable_defaulted_in_exact_length_from_TestCase_000e_fm8 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_packed_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_packed_ascii_string()
            {
                Assert::AreEqual((int) VariableType::VT_PACKED_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_Teststring()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_String_variable_defaulted_in_exact_length_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_short_defaulted_String_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fm8", "test_variable_short_defaulted");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_dd_padded_with_whitespaces()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Test", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_long_defaulted_String_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_too_long_defaulted");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_the_first_10_letters_of_the_string_defined_in_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("too long t", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_an_integer_variable_from_testcase_000E_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_int");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_integer()
            {
                Assert::AreEqual((int) VariableType::VT_INTEGER, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_INTEGER, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(-23ll, elementToCheck->default_value.val.i);
            }
        };

        [TestClass]
        public ref class When_reading_an_unsigned_integer_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_uint");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) VariableType::VT_UNSIGNED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(23ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_an_enumerated_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_enumerated");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_enumerated()
            {
                Assert::AreEqual((int) VariableType::VT_ENUMERATED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_enumerated()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(2ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_a_float_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_float");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_float()
            {
                Assert::AreEqual((int) VariableType::VT_FLOAT, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_float()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_FLOAT, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                float varToCheck = elementToCheck->default_value.val.f - 4.2f;
                Assert::IsTrue(varToCheck <0.001 && varToCheck > -0.001, "float value differs more than 0.001 from expected value");
            }
        };

        [TestClass]
        public ref class When_reading_a_double_variable_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_variable_double");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_double()
            {
                Assert::AreEqual((int) VariableType::VT_DOUBLE, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_double()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_DOUBLE, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                double varToCheck = elementToCheck->default_value.val.d - 4.2;
                Assert::IsTrue(varToCheck > -0.001 && varToCheck < 0.001, "double value differs more than 0.001 from expected value");
            }
        };

        [TestClass]
        public ref class When_reading_a_Password_variable_defaulted_in_exact_length_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_password");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_password_string()
            {
                Assert::AreEqual((int) VariableType::VT_PASSWORD, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("TestString", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_Packed_ASCII_variable_defaulted_in_exact_length_from_TestCase_000e_fmA : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000e.fmA", "test_packed_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_packed_ascii_string()
            {
                Assert::AreEqual((int) VariableType::VT_PACKED_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_Teststring()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassDateType
        {
        private:
            static TYPE_SIZE *_expectedType, *_receivedType;

        public:
            static void SetupBase(String ^filepath, String ^variableName, TYPE_SIZE expected)
            {
                _expectedType= new TYPE_SIZE(expected);
                _receivedType = nullptr;
                EddEngineController^ engine = gcnew EddEngineController(filepath);
                engine->Initialize();
                FDI_PARAM_SPECIFIER paramSpec;
                engine->FillParamSpecifier(variableName, &paramSpec);
                FLAT_VAR* elementToCheck =  dynamic_cast<FLAT_VAR*>(engine->GetGenericItem(paramSpec.id, ITYPE_VARIABLE)->item);
                _receivedType = new TYPE_SIZE(elementToCheck->type_size);
                Assert::IsFalse(_receivedType == nullptr, "Could not get type_size from elementToCheck");
                delete elementToCheck;
            }

            static void CleanupBase()
            {
                if(_receivedType) delete(_receivedType);
                if(_expectedType) delete(_expectedType);
            }

            [TestMethod, TestCategory("VariableTypeTests")]
            void It_should_have_the_expected_type()
            {
                Assert::AreEqual((int) _expectedType->type, (int) _receivedType->type);
            }

            [TestMethod, TestCategory("VariableTypeTests")]
            void It_should_have_the_expected_size()
            {
                Assert::AreEqual(_expectedType->size, _receivedType->size);
            }
        };

        [TestClass]
        public ref class When_reading_a_DATE_variable_from_TestCase_0003_fmA : BaseClassDateType
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToTest;
                typeSizeToTest.type = VT_EDD_DATE;
                typeSizeToTest.size = 3;
                SetupBase("Hart\\TestCase0003.fmA", "test_date", typeSizeToTest);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_DATE_variable_from_TestCase_0003_fm8 : BaseClassDateType
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToTest;
                typeSizeToTest.type = VT_EDD_DATE;
                typeSizeToTest.size = 3;
                SetupBase("Hart\\TestCase0003.fm8", "test_date", typeSizeToTest);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_TIME_VALUE_variable_from_TestCase_0003_fmA : BaseClassDateType
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToTest;
                typeSizeToTest.type = VT_TIME_VALUE;
                typeSizeToTest.size = 4;
                SetupBase("Hart\\TestCase0003.fmA", "test_time_value", typeSizeToTest);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_TIME_VALUE_variable_from_TestCase_0003_fm8 : BaseClassDateType
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToTest;
                typeSizeToTest.type = VT_TIME_VALUE;
                typeSizeToTest.size = 4;
                SetupBase("Hart\\TestCase0003.fm8", "test_time_value", typeSizeToTest);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
