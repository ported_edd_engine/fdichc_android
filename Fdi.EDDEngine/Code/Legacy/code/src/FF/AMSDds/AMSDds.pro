#-------------------------------------------------
#
# Project created by QtCreator 2017-12-05T17:14:24
#
#-------------------------------------------------

QT       -= gui

TARGET = AMSDds
TEMPLATE = lib
#CONFIG +=plugin
CONFIG += staticlib
DEFINES += AMSDDS_LIBRARY
QMAKESPEC=linux-g++-32
QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CC = $$QMAKE_CXX
    QMAKE_CFLAGS  = $$QMAKE_CXXFLAGS


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += DDI_CMI.C \
    DDI_CONV.C \
    EVL_TMPL.C \
    ddi_dir.c \
    DDI_ITEM.C \
    DDI_LANG.C \
    DDI_TAB.C \
    DDS_CHK.C \
    DDS_ERR.C \
    DDS_TAB.C \
    EVL_BASE.C \
    EVL_CLN.C \
    EVL_COND.C \
    EVL_DIR.C \
    EVL_ENUM.C \
    EVL_EXPR.C \
    EVL_IARY.C \
    EVL_ITEM.C \
    EVL_MEM.C \
    EVL_MENU.C \
    EVL_REF.C \
    EVL_REL.C \
    EVL_RESP.C \
    EVL_RSLV.C \
    EVL_STR.C \
    EVL_TRAN.C \
    EVL_TYPE.C \
    FCH_RDIR.C \
    FCH_RITM.C \
    FCH_RODF.C \
    FCH_SLCT.C \


HEADERS += ../../../inc/FF/AMSDds/APP_XMAL.H \
    ../../../inc/FF/AMSDds/ATTRS.H \
    ../../../inc/FF/AMSDds/DDI_LIB.H \
    ../../../inc/FF/AMSDds/DDI_TAB.H \
    ../../../inc/FF/AMSDds/DDLDEFS.H \
    ../../../inc/FF/AMSDds/DDS_CHK.H \
    ../../../inc/FF/AMSDds/DDS_TAB.H \
    ../../../inc/FF/AMSDds/dds_upcl.h \
    ../../../inc/FF/AMSDds/evl_lib.h \
    ../../../inc/FF/AMSDds/evl_loc.h \
    ../../../inc/FF/AMSDds/evl_ret.h \
    ../../../inc/FF/AMSDds/fch_lib.h \
    ../../../inc/FF/AMSDds/flats.h \
    ../../../inc/FF/AMSDds/od_defs.h \
    ../../../inc/FF/AMSDds/table.h \
    ../../../inc/FF/AMSDds/tags_sa.h \
    ../../../inc/FF/AMSDds/TST_FAIL.H \
    ../../../inc/FF/AmsGenFiles/env_info.h \
    ../../../inc/ServerProjects/rtn_code.h \
    ../../../inc/ServerProjects/rtn_def.h \


unix {
    target.path = /usr/lib/
    INSTALLS += target
}

DESTDIR = ../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../Src/SuppressWarning.pri)
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u


INCLUDEPATH += $$PWD/../../../inc/FF/AmsGenFiles
INCLUDEPATH += $$PWD/../../../inc/FF/AMSDds/
INCLUDEPATH += $$PWD/../../../inc/FF/FFHelper
INCLUDEPATH += $$PWD/../../../inc/FF/
INCLUDEPATH += $$PWD/../../../inc/
INCLUDEPATH += $$PWD/../../../../../Interfaces/EDD_Engine_Interfaces
INCLUDEPATH += $$PWD/../../../../../Inc
INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Linux
}

unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common

DEPENDPATH += $$DESTDIR/EDD_Engine_Common

unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a
