
#ifndef MAP_DICT_H
#define MAP_DICT_H
//#include <afxtempl.h>
#include <ServerProjects/DDS/attrs.h>
#include <mutex>
#include "stdstring.h"
//#include "PlatformCommon.h"

#define   READ_VIEW			567
#define   WRITE_VIEW		678

// Forward declaration
class ActiveDevManufacturerTblElem;
// Entry and table classes used for the HCF extension table and Mfgr dictionaries

class DictionaryExtensionEntry
{
public:

	DictionaryExtensionEntry(unsigned long lIndex, const wchar_t* str);

	~DictionaryExtensionEntry();

	unsigned long	getDictIndex()	{ return m_dictIndex; }
	const wchar_t*	getDictStr()	{ return m_dictStr; }
	size_t			getDictStrlen()	{ return m_dictStrlen; }

private:
	DictionaryExtensionEntry();		// Private with no implementation, so it can never be used.
	
	// Data
	unsigned long	m_dictIndex;	// dictionary index where upper word is the section number and the lower word is the offset
	wchar_t*		m_dictStr;		// dictionary string from an entry in the file
	size_t			m_dictStrlen;	// Length of the dictionary string
};

class DictionaryTable
{

public:
	DictionaryTable() {}
	~DictionaryTable();

	int Create(ENV_INFO* env_info, const wchar_t* pDictPath);
	void Install(ulong ref, const wchar_t* str);
	bool Search(unsigned long index, STRING* str, const wchar_t *lang_code);

private:

	void dump();
	int LoadFromArchive(ENV_INFO* env_info, const wchar_t* pArchive);
	int WriteToArchive(ENV_INFO* env_info, const wchar_t* pArchive);

	static int CompareDictExtEntries(const void* p1, const void* p2);
	
    PI_CtypedptrArray<DictionaryExtensionEntry*> m_installArray;
};




class StdDictTable
{
private:
	bool m_bLoaded;

	HANDLE hMap;
	HANDLE hStrMap;

	LPVOID lpView;
	LPVOID lpStrView;

    CStdString GetMappedFilename(const char* basename);

	unsigned long dict_table_create(DWORD dwSize);

	unsigned long dict_table_map_view(unsigned long view_type);
	unsigned long dict_table_unmap_view();
	unsigned long dict_table_close(void);
	int    create_manufacturer_dct_tbl(ActiveDevManufacturerTblElem *pMfgTblElem, ENV_INFO *env_info );

	unsigned long dict_table_open(void);

	int LoadDictionaryArchive(ENV_INFO* env_info, LPCTSTR binaryDictionaryFile);
	int WriteDictionaryArchive(ENV_INFO* env_info, LPCTSTR binaryDictionaryFile);


	DictionaryTable		m_HartDictExtTable; // HCF extension table containing array of DictionaryExtensionEntries

	// Formerly Static entry and section counters
	unsigned long  lNumberSections;
	unsigned long  num_dict_table_entries;
	long           offset;

	int			   iFileSize;
	std::recursive_mutex st_Mutex;

public:

	StdDictTable();

	~StdDictTable();

	int LoadDictionary(ENV_INFO *env_info, const wchar_t* sPath);
	int	 LookUp(ENV_INFO *env_info, DDL_UINT index, STRING *str);
	DictionaryTable* GetDictExtTable();
	void   dict_table_install(ENV_INFO* env_info, ulong ref, LPCTSTR str);
	static bool ChooseDictionaryFile(const wchar_t* DictName, const wchar_t* DictNameDar, DWORD* dwTableSize);

};


// global dictionary manager
extern StdDictTable g_StdDictTable;

//////////////////////////////// End Of File ////////////////////////////////
#endif
