#pragma once
#include "TypesCommon.h"
#include <ctime>

template <typename PlatformType>
class PIFileFind
{
private:

	PlatformType* m_pPlatformType;

public:
	// Constructors
	PIFileFind()
	{
		m_pPlatformType = new PlatformType();
	}

	virtual ~PIFileFind()
	{
		if (m_pPlatformType != NULL)
		{
			delete m_pPlatformType;
			m_pPlatformType = NULL;
		}			
	}

	BOOL FindFileNext()
	{
		return m_pPlatformType->FindFileNext();
	}

	BOOL FindFile(const wchar_t* fileName)
	{
		return m_pPlatformType->FindFile(fileName);
	}

	BOOL GetLastWriteTime(time_t& fileModifiedTime)
	{
		return m_pPlatformType->GetLastWriteTime(fileModifiedTime);
	}

	unsigned long long GetLength()
	{
		return m_pPlatformType->GetLength();
	}

	void CloseFile()
	{
		m_pPlatformType->CloseFile();
	}
};
