﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VerifyMethodInterpreter
{
    [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestClass]
    public class FF : MainWindowVMTest
    {

        private void CallMainVMTest(string strTestName, bool bRunTest)
        {
            RunOneMethodTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.FF, strTestName, bRunTest);
        }

        private void CallLegacyMainVMTest(string strTestName, bool bRunTest)
        {
            RunOneMethodTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.LGC_FF, strTestName, bRunTest);
        }

        private void CallActionTest(string strTestName)
        {
            RunOneActionTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.FF, strTestName);
        }

        // common_ui_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_common()
        {
            CallMainVMTest("132212, method_common", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_common()
        {
            CallMainVMTest("132212, method_common", false);
        }

        // meth_get_x_value ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_get_x_value()
        {
            CallMainVMTest("132213, meth_get_x_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_get_x_value()
        {
            CallMainVMTest("132213, meth_get_x_value", false);
        }

        // meth_user_interface ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_user_interface()
        {
            CallMainVMTest("132214, meth_user_interface", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_user_interface()
        {
            CallMainVMTest("132214, meth_user_interface", false);
        }

        // test_var_get_value ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_var_get_value()
        {
            CallMainVMTest("132218, test_var_get_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_var_get_value()
        {
            CallMainVMTest("132218, test_var_get_value", false);
        }

        // meth_isNaN ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_isNaN()
        {
            CallMainVMTest("132230, meth_isNaN", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_isNaN()
        {
            CallMainVMTest("132230, meth_isNaN", false);
        }


        //meth_get_x_lelem ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_get_x_lelem()
        {
            CallMainVMTest("132233, meth_get_x_lelem", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_get_x_lelem()
        {
            CallMainVMTest("132233, meth_get_x_lelem", false);
        }


        //meth_put_x_value ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_put_x_value()
        {
            CallMainVMTest("132235, meth_put_x_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_put_x_value()
        {
            CallMainVMTest("132235, meth_put_x_value", false);
        }

        //meth_resolve_x_ref ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_resolve_x_ref()
        {
            CallMainVMTest("132251, meth_resolve_x_ref", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_resolve_x_ref()
        {
            CallMainVMTest("132251, meth_resolve_x_ref", false);
        }

        //meth_math_string_time ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_math_string_time()
        {
            CallMainVMTest("132257, meth_math_string_time", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_math_string_time()
        {
            CallMainVMTest("132257, meth_math_string_time", false);
        }

        //meth_local_scope_test ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_local_scope_test()
        {
            CallMainVMTest("132259, meth_local_scope_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_local_scope_test()
        {
            CallMainVMTest("132259, meth_local_scope_test", false);
        }

        //meth_local_scope_test2 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_local_scope_test2()
        {
            CallMainVMTest("132358, meth_local_scope_test2", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_local_scope_test2()
        {
            CallMainVMTest("132358, meth_local_scope_test2", false);
        }

        //meth_unsigned_variable_sizes ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_unsigned_variable_sizes()
        {
            CallMainVMTest("132260, meth_unsigned_variable_sizes", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_unsigned_variable_sizes()
        {
            CallMainVMTest("132260, meth_unsigned_variable_sizes", false);
        }

        //meth_error ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_error()
        {
            CallMainVMTest("132265, meth_error", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_error()
        {
            CallMainVMTest("132265, meth_error", false);
        }

        //test_c_type_specifiers ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_c_type_specifiers()
        {
            CallMainVMTest("132268, test_c_type_specifiers", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_c_type_specifiers()
        {
            CallMainVMTest("132268, test_c_type_specifiers", false);
        }

        //test_put_string_value_builtin ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_put_string_value_builtin()
        {
            CallMainVMTest("132270, test_put_string_value_builtin", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_put_string_value_builtin()
        {
            CallMainVMTest("132270, test_put_string_value_builtin", false);
        }

        //test_stddict_no_truncation ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_stddict_no_truncation()
        {
            CallMainVMTest("132336, test_stddict_no_truncation", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_stddict_no_truncation()
        {
            CallMainVMTest("132336, test_stddict_no_truncation", false);
        }

        //meth_get_date_value_with_null_character ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_get_date_value_with_null_character()
        {
            CallMainVMTest("132337, meth_get_date_value_with_null_character", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_get_date_value_with_null_character()
        {
            CallMainVMTest("132337, meth_get_date_value_with_null_character", false);
        }

        //meth_print_duration ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_print_duration()
        {
            CallMainVMTest("132339, meth_print_duration", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_print_duration()
        {
            CallMainVMTest("132339, meth_print_duration", false);
        }

        //meth_test_switch_statement ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_switch_statement()
        {
            CallMainVMTest("132341, meth_test_switch_statement", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_switch_statement()
        {
            CallMainVMTest("132341, meth_test_switch_statement", false);
        }

        //meth_get_date_value_get_acknowledgement ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_get_date_value_get_acknowledgement()
        {
            CallMainVMTest("132342, meth_get_date_value_get_acknowledgement", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_get_date_value_get_acknowledgement()
        {
            CallMainVMTest("132342, meth_get_date_value_get_acknowledgement", false);
        }

        //meth_test_read_only ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_read_only()
        {
            CallMainVMTest("132343, meth_test_read_only", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_read_only()
        {
            CallMainVMTest("132343, meth_test_read_only", false);
        }

        //meth_acknowledge_time_method ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_acknowledge_time_method()
        {
            CallMainVMTest("132348, meth_acknowledge_time_method", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_acknowledge_time_method()
        {
            CallMainVMTest("132348, meth_acknowledge_time_method", false);
        }

        //TC_File_001_step_1 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_File_001_step_1()
        {
            CallMainVMTest("131979, TC_File_001_step_1", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_File_001_step_1()
        {
            CallMainVMTest("131979, TC_File_001_step_1", false);
        }

        ////TC_File_001_step_2 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_File_001_step_2()
        {
            CallMainVMTest("131980, TC_File_001_step_2", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_File_001_step_2()
        {
            CallMainVMTest("131980, TC_File_001_step_2", false);
        }

        ////TC_File_003_step_1 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_File_003_step_1()
        {
            CallMainVMTest("131983, TC_File_003_step_1", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_File_003_step_1()
        {
            CallMainVMTest("131983, TC_File_003_step_1", false);
        }

        //tc_meth_001 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void tc_meth_001()
        {
            CallMainVMTest("132011, tc_meth_001", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_tc_meth_001()
        {
            CallMainVMTest("132011, tc_meth_001", false);
        }

        //tc_meth_003 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void tc_meth_003()
        {
            CallMainVMTest("132019, tc_meth_003", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_tc_meth_003()
        {
            CallMainVMTest("132019, tc_meth_003", false);
        }

        //TC_List_001 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_List_001()
        {
            CallMainVMTest("131991, TC_List_001", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_List_001()
        {
            CallMainVMTest("131991, TC_List_001", false);
        }

        //TC_List_003 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_List_003()
        {
            CallMainVMTest("131993, TC_List_003", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_List_003()
        {
            CallMainVMTest("131993, TC_List_003", false);
        }

        //TC_List_005 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_List_005()
        {
            CallMainVMTest("131995, TC_List_005", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_List_005()
        {
            CallMainVMTest("131995, TC_List_005", false);
        }

        //TC_List_006 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void TC_List_006()
        {
            CallMainVMTest("131996, TC_List_006", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_TC_List_006()
        {
            CallMainVMTest("131996, TC_List_006", false);
        }

        //meth_communication_abort //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_communication_abort()
        {
            CallMainVMTest("132267, meth_communication_abort", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_communication_abort()
        {
            CallMainVMTest("132267, meth_communication_abort", false);
        }

        //meth_edit_device_value_bitstring_var ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_edit_device_value_bitstring_var()
        {
            CallMainVMTest("132349, meth_edit_device_value_bitstring_var", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_edit_device_value_bitstring_var()
        {
            CallMainVMTest("132349, meth_edit_device_value_bitstring_var", false);
        }

        //initialize_collection ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void initialize_collection()
        {
            CallMainVMTest("132354, initialize_collection", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_initialize_collection()
        {
            CallMainVMTest("132354, initialize_collection", false);
        }

        //meth_variable_list_array_reference ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_variable_list_array_reference()
        {
            CallMainVMTest("132355, meth_variable_list_array_reference", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_variable_list_array_reference()
        {
            CallMainVMTest("132355, meth_variable_list_array_reference", false);
        }

        //meth_attribute_scaling_factor ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_attribute_scaling_factor()
        {
            CallMainVMTest("132356, meth_attribute_scaling_factor", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_attribute_scaling_factor()
        {
            CallMainVMTest("132356, meth_attribute_scaling_factor", false);
        }

        //meth_attribute_min_max_value ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_attribute_min_max_value()
        {
            CallMainVMTest("132357, meth_attribute_min_max_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_attribute_min_max_value()
        {
            CallMainVMTest("132357, meth_attribute_min_max_value", false);
        }

        //meth_for_loop_test ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_for_loop_test()
        {
            CallMainVMTest("132359, meth_for_loop_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_for_loop_test()
        {
            CallMainVMTest("132359, meth_for_loop_test", false);
        }

        //method_calling_method_with_complex_ref //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_calling_method_with_complex_ref()
        {
            CallMainVMTest("132363, method_calling_method_with_complex_ref", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_calling_method_with_complex_ref()
        {
            CallMainVMTest("132363, method_calling_method_with_complex_ref", false);
        }

        //meth_test_List //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_List()
        {
            CallMainVMTest("132379, meth_test_List", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_List()
        {
            CallMainVMTest("132379, meth_test_List", false);
        }

        //meth_configure_display //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_configure_display()
        {
            CallMainVMTest("132382, meth_configure_display", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_configure_display()
        {
            CallMainVMTest("132382, meth_configure_display", false);
        }

        //method_scope_Start_Totals //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_scope_Start_Totals()
        {
            CallMainVMTest("132383, method_scope_Start_Totals", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_scope_Start_Totals()
        {
            CallMainVMTest("132383, method_scope_Start_Totals", false);
        }

        //method_Loop_Condition //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_Loop_Condition()
        {
            CallMainVMTest("132401, method_Loop_Condition", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_Loop_Condition()
        {
            CallMainVMTest("132401, method_Loop_Condition", false);
        }


        /////////////////////////////////////LEGACY METHODS

        //meth_test_List_lgc //////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_List_lgc()
        {
            CallLegacyMainVMTest("132379, meth_test_List_lgc", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_List_lgc()
        {
            CallLegacyMainVMTest("132379, meth_test_List_lgc", false);
        }


        /////////////////////////////////////ACTION METHODS


        //scale_PV_write_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_write_value()
        {
            CallActionTest("0x2047B, scale_PV_write_value");
        }

        //scale_PV_read_float_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_float_value()
        {
            CallActionTest("0x20491, scale_PV_read_float_value");
        }

        //scale_PV_read_double_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_double_value()
        {
            CallActionTest("0x20492, scale_PV_read_double_value");
        }

        //scale_PV_read_string_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_string_value()
        {
            CallActionTest("0x2042C, scale_PV_read_string_value");
        }

        //scale_PV_read_signed_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_signed_value()
        {
            CallActionTest("0x20496, scale_PV_read_signed_value");
        }

        //scale_PV_read_unsigned_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_unsigned_value()
        {
            CallActionTest("0x20497, scale_PV_read_unsigned_value");
        }

        //scale_put_get_date//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_put_get_date()
        {
            CallActionTest("0x20518, scale_put_get_date");
        }

        //scale_PV_read_write_ascii_string_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_PV_read_write_ascii_string_value()
        {
            CallActionTest("0x20519, scale_PV_read_write_ascii_string_value");
        }

        //scale_put_get_time_value8//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_put_get_time_value8()
        {
            CallActionTest("0x20521, scale_put_get_time_value8");
        }

    }

}
