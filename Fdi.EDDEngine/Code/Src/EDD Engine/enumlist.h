#pragma once

#include "pugixml.hpp"

class EnumList
{
public: 
	//Utthunga Fix: ItemId must be bigger number
	EnumList(unsigned long long i);
	bool AddEnumElement(unsigned long long nNumber, wchar_t* sDescription, wchar_t* sHelp );
#if 0
	BSTR GetList();
#else
	std::wstring GetList();
#endif

private:
	std::wstring StripOffNPChars(wchar_t* EnumDescription );
	pugi::xml_document m_DOMDoc;
	pugi::xml_node m_DDItem; //Appending to this one
};