#include "stdinc.h"


#include "ConnectionMgr.h"



CConnectionMgr::CConnectionMgr()
{
	active_blk_tbl.count = 0;
	active_blk_tbl.list = nullptr;	

	active_dev_tbl.count = 0;
	active_dev_tbl.list = nullptr;

}

CConnectionMgr::~CConnectionMgr()
{
	active_blk_tbl.count = 0;
	active_blk_tbl.list = nullptr;

	active_dev_tbl.count = 0;
	active_dev_tbl.list = nullptr;

}

void CConnectionMgr::set_hart_dd_path(  wchar_t* sHartPath )
{
    wcscpy( hart_dd_path, sHartPath );
}

