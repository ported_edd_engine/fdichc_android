/*************************************************************************************************
 *
 * $Workfile: ddbAxis.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbAxis.h".
 */

#ifndef _DDB_AXIS_H
#define _DDB_AXIS_H
#ifdef INC_DEBUG
#pragma message("In ddbAxis.h") 
#endif

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include <ddbCollAndArr.h>

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbAxis.h") 
#endif


class hCaxis : public hCgrpItmBasedClass 
{
public:
	hCaxis(DevInfcHandle_t h, aCitemBase* paItemBase);
	virtual ~hCaxis()  {};
	
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	CValueVarient getAttrValue(unsigned attrType, int which = 0);

public:
	virtual RETURNCODE Label(wstring& retStr);
	virtual RETURNCODE Help(wstring& retStr);
	virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	bool ReadParameterData(CValueVarient *pOutput, wchar_t *sValue );
	virtual RETURNCODE getMinMaxList(hCRangeList& retList);//true @ val2Check inside min-max set
};


#endif	// _DDB_AXIS_H
