#pragma once

#include <Ole/EDDLMethInterp/ManagedMiInterfaces.h>
#include "AMSMethodFunction.h"
		

class ManagedServerMi : public IManagedServerMi, public IManagedServerMiSig
{
public:
	ManagedServerMi(IManagedMiEngineSig* engine);
	~ManagedServerMi();
	void SetCancelled();
	CMethodThread* GetMethodThread() { return m_pThread; }
	void SetMethodUIEvent();

private:	// IManagedServerMi methods
	int serverGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen );
	void MethodComplete(nsEDDEngine::Mth_ErrorCode retVal);
	int CommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize);
	void CommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity);
	int GetValueExecute2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, unsigned long ulMemberID, long lParamType, CValueVarient *pvtValue );
	int serverGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput);
	int ResolveNameToIDExecute( CStdString sItemName, unsigned long *memberID );
	int ListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount);
	int UIExecute(ACTION_UI_DATA& stUIData);
	int UIMethRspExecute(CValueVarient& vtRsp);
	int RspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol);
	int DictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen);
	int LitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen);
	int ResponseCodeStringExecute(int iItemId, int iMemberId, int iRsponseCode, CValueVarient *pvtValue);
	int DdsErrorExecute( wchar_t* pErrorString);
	unsigned long CommErrorExecute();
	int ParamSendExecute(const PARAM_REF *pOpRef, AccessType iAccessType);
	int ResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId);
	int serverAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef);

    int AccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType);

	ITEM_ID BlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType);
	int serverAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eValType, AccessType eAccessType, long lSize);
	int serverAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType);
	int serverWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal);
	int BlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount);
	int BlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex);
	int BlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex);
	int GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data);
	void serverLogMessage(int priorityVal, wchar_t *msg);
	void serverMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml);
	int serverIsOffline();
	long serverSendCommand(unsigned long commandId, long *commandError);
	int serverAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType);
	virtual int serverGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString);
	void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0);

private:	// IManagedServerMiSig methods
	int ExecuteMethod(long lMethodItemID, ProtocolType protocol);
	

	IManagedMiEngineSig * m_pIManagedMiEngineSig;
	CProfibusDPMethodThread* m_pThread;
	bool m_bThreadCompleted;
};
