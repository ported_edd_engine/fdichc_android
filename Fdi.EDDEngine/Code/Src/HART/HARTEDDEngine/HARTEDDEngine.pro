#-------------------------------------------------
#
# Project created by QtCreator 2017-11-30T15:35:07
#
#-------------------------------------------------

QT       -= gui

TARGET = HARTEDDEngine
TEMPLATE = lib
CONFIG += shared_and_static build_all
#CONFIG +=plugin
DEFINES += HARTEDDENGINE_LIBRARY \
            UNICODE
!android {
LIBS += -lrt
}
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_LFLAGS += -Wl,--version-script=$$PWD/export_hart
QMAKE_CXXFLAGS += -std=gnu++11

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKESPEC=linux-g++-32
SOURCES += globals.cpp \
    LegacyHart.cpp \
    LegacyHartAttrs.cpp \
    LegacyHartFlats.cpp \
    LegacyHartTables.cpp \


HEADERS += ../../../Inc/HART/HARTEDDEngine/DDSSupport.h\
    ../../../Inc/HART/HARTEDDEngine/LegacyHartFactory.h\
    LegacyHart.h \
    LegacyHartFlats.h \
    resource.h \


unix {
    target.path = /usr/lib/
    INSTALLS += target
}



CONFIG(debug, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/debug
    BUILD_DIR = $$PWD/debug

}
CONFIG(release, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/release
    BUILD_DIR = $$PWD/release
}
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u
include($$DESTDIR/../../../../Src/SuppressWarning.pri)

INCLUDEPATH += $$PWD/../../../Legacy/code/inc/ServerProjects
INCLUDEPATH += $$PWD/../../../Legacy/code/inc/ServerProjects/DDS/
INCLUDEPATH += $$PWD/../../../Legacy/code/src/ServerProjects/server/src/
INCLUDEPATH += $$PWD/../../../Legacy/code/src/ServerProjects/hartpt/Share/
INCLUDEPATH += $$PWD/../../../Legacy/code/src/ServerProjects/server/hrt_cstk/
INCLUDEPATH += $$PWD/../../../Legacy/code/src/ServerProjects/server/interp/
INCLUDEPATH += $$PWD/../../../Legacy/code/inc/
INCLUDEPATH += $$PWD/../../../Inc/$$"EDD Engine"/
INCLUDEPATH += $$PWD/../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../Inc/HART/HARTEDDEngine/
INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Common

LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lEDD_Engine_Common -Wl,--no-whole-archive
android {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Android
    LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lEDD_Engine_Android -Wl,--no-whole-archive
}
else {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Linux
    unix:!macx: LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lEDD_Engine_Linux -Wl,--no-whole-archive
}
LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lddsHart  -Wl,--no-whole-archive
LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lHARTDDSHelper -Wl,--no-whole-archive
LIBS += -L$$DESTDIR/ -lEDD_Engine_Interfaces

INCLUDEPATH += $$DESTDIR/../../lib
DEPENDPATH += $$DESTDIR/../../lib
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR

PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Interfaces.a
android {
    PRE_TARGETDEPS += $$DESTDIR/../../lib/libEDD_Engine_Android.a
#    PRE_TARGETDEPS += $$DESTDIR/../../lib/libddsHart.a******
#    PRE_TARGETDEPS += $$DESTDIR/../../lib/libHARTDDSHelper.a******
}
else {
    unix:!macx: PRE_TARGETDEPS += $$DESTDIR/../../lib/libEDD_Engine_Linux.a
}

#contains(ANDROID_TARGET_ARCH,x86) {
#    ANDROID_EXTRA_LIBS =
#}
#LIBS += -landroid
