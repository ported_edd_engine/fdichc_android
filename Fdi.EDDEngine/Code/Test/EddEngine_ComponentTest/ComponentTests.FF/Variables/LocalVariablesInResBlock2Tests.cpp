#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_checking_the_number_of_local_variables_in_the_resource_block_2_ff_dd
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_block_item_ID;
            static FLAT_BLOCK* test_block;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\resblock2_with_all_variables\\0101.ff6");
                engine->Initialize();
                
                test_block_item_ID = new ITEM_ID();
                int errorCode = engine->Instance->GetItemIdFromSymbolName(L"__resource_block_2", test_block_item_ID);

                if (errorCode != 0)
                    Assert::Fail();

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = 0x80020AF0;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                errorCode = engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                
                nsEDDEngine::FLAT_DEVICE_DIR* dir = new nsEDDEngine::FLAT_DEVICE_DIR();
                engine->Instance->GetDeviceDir(&dir);

                if (errorCode != 0)
                    Assert::Fail();
                
                genericItem = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("LocalVariablesInResBlock2Tests")]
            void It_should_contain_16_local_parameters()
            {
                Assert::AreEqual(ushort(16), test_block->local_param.count);
            }

            [TestMethod, TestCategory("LocalVariablesInResBlock2Tests")]
            void It_should_contain_57_parameters()
            {
                Assert::AreEqual(ushort(57), test_block->param.count);
            }
        };

    } // namespace FF
} // namespace ComponentTests
