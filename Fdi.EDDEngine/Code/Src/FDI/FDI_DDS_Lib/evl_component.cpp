
#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif

void
ddl_free_component_range_list(
	RANGE_LIST *range_list,
	uchar           dest_flag)
{

	int             i;
	RANGE_SET      *temp_list;

	if (range_list == NULL) {
		return;
	}

	if (range_list->list == NULL) {

		ASSERT_DBG(!range_list->count && !range_list->limit);
		range_list->limit = 0;
	}
	else {

		temp_list = range_list->list;
		for (i = 0; i < range_list->count; temp_list++, i++) {
			ddl_free_expr(&temp_list->max_num.value);
			ddl_free_expr(&temp_list->min_num.value);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			* Free the list of MENU_ITEMs
			*/

			free((void *)range_list->list);
			range_list->list = NULL;
			range_list->limit = 0;
		}
	}

	range_list->count = 0;
}

static int
ddl_shrink_range_list(
	RANGE_LIST *range_list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_MENUITEMS_LIST);
#endif

	if (range_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	* If there is no list, make sure the sizes are consistent, and return.
	*/

	if (range_list->list == NULL) {
		ASSERT_DBG(!range_list->count && !range_list->limit);
		return DDL_SUCCESS;
	}

	/*
	* Shrink the list to size elements.  If it is already at size
	* elements, just return.
	*/

	if (range_list->count == range_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	* If count = 0, free the list. If count != 0, shrink the list
	*/

	if (range_list->count == 0) {
		ddl_free_component_range_list(range_list, FREE_ATTR);
	}
	else {
		range_list->limit = range_list->count;

		range_list->list = (RANGE_SET *)realloc((void *)range_list->list,
			(size_t)(range_list->limit * sizeof(RANGE_SET)));

		if (range_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

void
ddl_free_components_list(
COMPONENT_SPECIFIER_LIST *component,
uchar           dest_flag)
{

	int             i;
	COMPONENT_SPECIFIER      *temp_list;

	if (component == NULL) {
		return;
	}

	if (component->list == NULL) {

		ASSERT_DBG(!component->count && !component->limit);
		component->limit = 0;
	}
	else {

		temp_list = component->list;
		for (i = 0; i < component->count; temp_list++, i++) {
			ddl_free_op_ref_trail(&temp_list->cr_ref);
			ddl_free_component_range_list(&temp_list->range_list, FREE_ATTR);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of MENU_ITEMs
			 */

			free((void *)component->list);
			component->list = NULL;
			component->limit = 0;
		}
	}

	component->count = 0;
}


static int
ddl_shrink_component_list(
COMPONENT_SPECIFIER_LIST *components)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_MENUITEMS_LIST);
#endif

	if (components == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (components->list == NULL) {
		ASSERT_DBG(!components->count && !components->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (components->count == components->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (components->count == 0) {
		ddl_free_components_list(components, FREE_ATTR);
	}
	else {
		components->limit = components->count;

		components->list = (COMPONENT_SPECIFIER *) realloc((void *)components->list,
			(size_t) (components->limit * sizeof(COMPONENT_SPECIFIER)));

		if (components->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

#define RANGE_INC 16 // I am not sure about this size.

static
int
ddl_parse_range_set(
	unsigned char **chunkp,
	DDL_UINT       *size,
	RANGE_LIST *range_list,
	nsEDDEngine::OP_REF_LIST    *depinfo,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	RANGE_SET      *temp_range_set = NULL;	/* temp ptr to the list of menu items */
	DDL_UINT        tag;				/* stores the binary REFERENCE type number */
	DDL_UINT len = 0;
#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_MENUITEMS);
#endif

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	if (tag != RANGE_SET_TAG)
	{
		return DDL_ENCODING_ERROR;
	}
	*size -= len;

		while (len > 0) {
				if (range_list->count == range_list->limit) {

					range_list->limit += RANGE_INC;
					temp_range_set = (RANGE_SET *)realloc((void *)range_list->list,
						(size_t)range_list->limit * sizeof(RANGE_SET));

					if (temp_range_set == NULL) {
						range_list->limit = range_list->count;
						ddl_free_component_range_list(range_list, FREE_ATTR);
						return DDL_MEMORY_ERROR;
					}

					memset((char *)&temp_range_set[range_list->count], 0,
						(RANGE_INC * sizeof(RANGE_SET)));

					range_list->list = temp_range_set;

				}
			
			if (range_list->count == 0)
			{
				temp_range_set = &range_list->list[range_list->count++];
			}
			

			rc = ddl_parse_tag_func(chunkp, &len, &tag, (DDL_UINT *)NULL_PTR);
			
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}
			int  data_valid;
			DDL_UINT which = 0;
			switch (tag) {
			case RANGE_SET_MAX_VAL: // MAX_VALUE
				temp_range_set->max_num.which = 0;
				rc = ddl_expr_choice(chunkp, &len, &temp_range_set->max_num.value, depinfo, &data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				break;
			case RANGE_SET_MIN_VAL: // MIN_VALUE
				temp_range_set->min_num.which = 0;
				rc = ddl_expr_choice(chunkp, &len, &temp_range_set->min_num.value, depinfo, &data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				break;
			case RANGE_SET_MAX_NUM: 
			{
				
				rc = ddl_parse_integer_func(chunkp, &len, &which);

				if (rc != DDL_SUCCESS) {
					return rc;
				}
				
				temp_range_set = &range_list->list[which - 1];
				
				if(range_list->count < which)
					range_list->count = (unsigned short)which;
			
				temp_range_set->max_num.which = which;
				
				rc = ddl_expr_choice(chunkp, &len, &temp_range_set->max_num.value, depinfo, &data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			break;
			case RANGE_SET_MIN_NUM: 
			{
				
				rc = ddl_parse_integer_func(chunkp, &len, &which);

				if (rc != DDL_SUCCESS) {
					return rc;
				}
				
				temp_range_set = &range_list->list[which - 1];
				
				if (range_list->count < which)
					range_list->count = (unsigned short)which;
				
				temp_range_set->min_num.which = which;
				
				rc = ddl_expr_choice(chunkp, &len, &temp_range_set->min_num.value, depinfo, &data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
				break;
			}
		}
	
	return DDL_SUCCESS;

err_exit:
	ddl_free_component_range_list(range_list, FREE_ATTR);	
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}

static int
ddl_parse_rangelist(
	unsigned char **chunkp,
	DDL_UINT       *size,
	RANGE_LIST *rangelist,
	nsEDDEngine::OP_REF_LIST    *depinfo,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */

	CHUNK val = { 0 };
	DDL_UINT        tag;				/* stores the binary REFERENCE type number */
	DDL_UINT        len = 0;
#ifdef DDSTEST
	TEST_FAIL(DDL_DATAITEMS_CHOICE);
#endif


	DDL_PARSE_TAG(chunkp, size, &tag, &len);
	
	if (tag != RANGE_LIST_TAG)
	{
		return DDL_ENCODING_ERROR;
	}
	*size -= len;
	// Parse the variable reference first.
	rc = ddl_parse_desc_ref(chunkp, &len, &rangelist->var_refrences,
		depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}
	


	if (rangelist) {

		while (len > 0) {	/* Parse them */
			rc = ddl_parse_range_set(chunkp, &len,
				rangelist, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	rc = DDL_SUCCESS;

	return rc;

}

#define COMPONENT_INC 16 // This is calculate based on the size I am getting for my DD examples 
						 
static
int
ddl_parse_components(
unsigned char **chunkp,
DDL_UINT       *size,
COMPONENT_SPECIFIER_LIST *component,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	COMPONENT_SPECIFIER      *temp_component_item;	/* temp ptr to the list of menu items */
	DDL_UINT        tag;				/* stores the binary REFERENCE type number */
	DDL_UINT len = 0;

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_MENUITEMS);
#endif

	while (*size > 0) {
		
		/*
		* Strip off COMPONENT_DEPICTION_TAG
		*/

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != COMPONENT_DEPICTION_TAG)
		{
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		if (component) {
			if (component->count == component->limit) {

				component->limit += COMPONENT_INC;
				temp_component_item = (COMPONENT_SPECIFIER *)realloc((void *)component->list,
					(size_t)component->limit * sizeof(COMPONENT_SPECIFIER));

				if (temp_component_item == NULL) {
					component->limit = component->count;
					ddl_free_components_list(component, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *)&temp_component_item[component->count], 0,
					(COMPONENT_INC * sizeof(COMPONENT_SPECIFIER)));

				component->list = temp_component_item;

			}

			temp_component_item = &component->list[component->count++];

			
			
			OP_REF_TRAIL var = { STANDARD_TYPE, 0 };


			int valid = 0;
			rc = ddl_op_ref_trail_choice(chunkp, &len, &temp_component_item->cr_ref, depinfo, &valid,
				env_info, var_needed);
			
			int data_valid = 0;
			while (len > 0)
			{
				rc = ddl_parse_tag_func(chunkp, &len, &tag, (DDL_UINT *)NULL_PTR);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}


				switch (tag)
				{
				case MAXIMUM_NUMBER:
				{
					rc = ddl_expr_choice(chunkp, &len, &temp_component_item->maximum_number, depinfo, &data_valid, env_info, var_needed);

					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
				break;
				case MINIMUM_NUMBER:
				{
					rc = ddl_expr_choice(chunkp, &len, &temp_component_item->minimum_number, depinfo, &data_valid, env_info, var_needed);

					if (rc != DDL_SUCCESS) {
						return rc;
					}
					
				}
				break;
				case AUTO_CREATE:
				{
					rc = ddl_expr_choice(chunkp, &len, &temp_component_item->auto_create, depinfo, &data_valid, env_info, var_needed);
						
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
				break;
				case FILTER:
				{
					// Currently "FILTER" is not supported by PB tokenizer. So below code is not yet tested.
					// Once the PB tokenizer fixed to support "FILTER", below code will be revisited. 
					nsEDDEngine::ATTRIBUTE_DATA tg = { 0 };
					tg.tag = BOOLEAN_TAG;

					rc = eval_attr_ulong(*chunkp,
						*size,
						&temp_component_item->filter,
						depinfo,
						env_info,
						var_needed,
						&tg
					);

					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
				break;
				case REQUIRED_RANGES:
					
					rc = ddl_parse_rangelist(chunkp, &len, &temp_component_item->range_list, depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
					
					if (rc != DDL_SUCCESS) {
						ddl_free_depinfo(depinfo, FREE_ATTR);
						return rc;
					}

					rc = ddl_shrink_depinfo(depinfo);

					if (rc == DDL_SUCCESS) {
						rc = ddl_shrink_range_list(&temp_component_item->range_list);
					}

					if (rc != DDL_SUCCESS) {
						ddl_free_depinfo(depinfo, FREE_ATTR);
						ddl_free_component_range_list(&temp_component_item->range_list, FREE_ATTR);
						return rc;
					}

					
					break;
				default:
					break;
				}
			}
		}
	}
	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}



static int
ddl_component_choice(
unsigned char **chunkp,
DDL_UINT       *size,
COMPONENT_SPECIFIER_LIST *items,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk
					 * list */

#ifdef DDSTEST
	TEST_FAIL(DDL_MENUITEMS_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		COMPONENT_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (items && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (items == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_components(&(chunk_ptr->chunk), &(chunk_ptr->size),
				items, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (items && data_valid) {
			*data_valid = TRUE;	/* items has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


int
eval_attr_components(
uchar          *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	COMPONENT_SPECIFIER_LIST* component = static_cast<COMPONENT_SPECIFIER_LIST*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in menu */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_MENUITEMS);
#endif

	valid = 0;

	if (component) {
		ddl_free_components_list(component, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	/*
	 * Parse the binary.
	 */

	rc = ddl_component_choice(&chunk, &size, component, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_components_list(component, FREE_ATTR);
		return rc;
	}

	/*
	 * Condense the components and the dependency list.
	 */

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_component_list(component);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_components_list(component, FREE_ATTR);
		return rc;
	}

	if (component && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}