#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_requesting_a_block_with_methods_list_FF
        {
        public:
            static FLAT_BLOCK* test_block;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0008\\TestCase0008.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_1000", test_method_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            { 
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_four_members_in_the_list()
            {
                Assert::AreEqual(4u,(UINT32) test_block->methods.count);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_as_second_member_the_correct_method_reference()
            {
                Assert::AreEqual(*test_method_item_ID, test_block->methods.list[1].ref.id);
            }
        };

        [TestClass]
        public ref class When_requesting_a_block_with_methods_list_from_TestCase000D_FF
        {
        public: 
            static FLAT_BLOCK* test_block;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static  EddEngineController^  engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase000D\\TestCase000D.ff6");
                engine->Initialize();

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_method_read_value", test_method_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            { 
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_four_members_in_the_list()
            {
                Assert::AreEqual(3u,(UINT32) test_block->methods.count);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_as_second_member_the_correct_method_reference()
            {
                Assert::AreEqual(*test_method_item_ID, test_block->methods.list[1].ref.id);
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_pre_actions_with_FF
        {
        public: 
            static ITEM_ID* test_block_item_ID;
            static FLAT_VAR* test_var_pre_action;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0008\\TestCase0008.ff6");
                engine->Initialize();

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_pre_specifier;
                engine->FillParamSpecifier("var_one_pre_read_write_action", &test_var_pre_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_pre_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);
                test_var_pre_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100",test_method_item_ID);
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_block_item_ID;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_defined_label()
            {
                Assert::AreEqual("Test Variable var_one_pre_read_write_action", gcnew String(test_var_pre_action->label.c_str()) );
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_pre_action->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_pre_action->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_metod()
            {
                Assert::AreEqual(*test_method_item_ID, test_var_pre_action->pre_read_act.list->action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_pre_action->pre_read_act.list->eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_pre_action->pre_write_act.list->eType);
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_post_actions_with_FF
        {
        public: 
            static ITEM_ID* test_block_item_ID;
            static FLAT_VAR* test_var_post_action;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static  EddEngineController^  engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0008\\TestCase0008.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_post_specifier;
                engine->FillParamSpecifier("var_one_post_read_write_action", &test_var_post_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_post_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);
                test_var_post_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100",test_method_item_ID);
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_block_item_ID;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_post_action->post_read_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_post_action->post_write_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_metod()
            {
                Assert::AreEqual(*test_method_item_ID, test_var_post_action->post_read_act.list->action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_post_action->post_read_act.list->eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_post_action->post_write_act.list->eType);
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_actions_with_FF
        {
        public: 
            static ITEM_ID* test_block_item_ID;
            static FLAT_VAR* test_var_multiple_action;
            static ITEM_ID* test_method_add_100_item_ID;
            static ITEM_ID* test_method_add_1000_item_ID;
            static ITEM_ID* test_method_remove_100_item_ID;
            static ITEM_ID* test_method_remove_1000_item_ID;
            static  FDI_GENERIC_ITEM* genericItem;
            static  EddEngineController^  engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0008\\TestCase0008.ff6");
                engine->Initialize();

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_multiple_specifier;
                engine->FillParamSpecifier("var_with_multiple_actions", &test_var_multiple_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_multiple_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);
                test_var_multiple_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_add_100_item_ID = new ITEM_ID();
                test_method_add_1000_item_ID = new ITEM_ID();
                test_method_remove_100_item_ID = new ITEM_ID();
                test_method_remove_1000_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100", test_method_add_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"add_1000", test_method_add_1000_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_100", test_method_remove_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_1000", test_method_remove_1000_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_method_add_100_item_ID;
                delete test_method_add_1000_item_ID;
                delete test_method_remove_100_item_ID;
                delete test_method_remove_1000_item_ID;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->post_read_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->post_write_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, test_var_multiple_action->pre_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, test_var_multiple_action->pre_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, test_var_multiple_action->post_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, test_var_multiple_action->post_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, test_var_multiple_action->pre_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, test_var_multiple_action->pre_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, test_var_multiple_action->post_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, test_var_multiple_action->post_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_write_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_write_act.list[1].eType);
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_edit_actions_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static FLAT_VAR* var_with_multiple_actions;
            static ITEM_ID* test_method_add_100_item_ID;
            static ITEM_ID* test_method_add_1000_item_ID;
            static ITEM_ID* test_method_remove_100_item_ID;
            static ITEM_ID* test_method_remove_1000_item_ID;
            static  FDI_GENERIC_ITEM* genericItem ;
            static  EddEngineController^  engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0008\\TestCase0008.ff6");
                engine->Initialize();

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_multiple_specifier;
                engine->FillParamSpecifier("var_with_multiple_actions", &test_var_multiple_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_multiple_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);
                var_with_multiple_actions = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_add_100_item_ID = new ITEM_ID();
                test_method_add_1000_item_ID = new ITEM_ID();
                test_method_remove_100_item_ID = new ITEM_ID();
                test_method_remove_1000_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100", test_method_add_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"add_1000", test_method_add_1000_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_100", test_method_remove_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_1000", test_method_remove_1000_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem;
                delete engine;
                delete test_block_item_ID;
                delete test_method_add_100_item_ID;
                delete test_method_add_1000_item_ID;
                delete test_method_remove_100_item_ID;
                delete test_method_remove_1000_item_ID;
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_edit_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_edit_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_edit_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_edit_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_edit_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_edit_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_edit_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_edit_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_edit_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_edit_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_edit_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_edit_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_edit_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_edit_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_edit_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_edit_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_read_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_write_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_write_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_write_act.list[1].eType);
            }
        };

    } // namespace FF
} // namespace ComponentTests
