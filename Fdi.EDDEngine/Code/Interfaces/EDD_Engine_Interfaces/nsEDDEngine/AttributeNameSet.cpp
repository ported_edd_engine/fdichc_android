

#include "stdafx.h"

#include "nsEDDEngine/AttributeName.h"
#include <iterator>
#include <algorithm>


#ifdef _WIN32
__declspec(dllexport)
#else
__attribute__((dllexport))
#endif

const char *attribute_names[]=
	{ 
		"item_information",
		"label",
        "help",
		"validity",
		"members",
		"handling",
		"response_codes",
		"height",
		"post_edit_actions",
		"pre_edit_actions",
		"refresh_actions",
		"relation_update_list",
		"relation_watch_list",
		"relation_watch_variable",
		"width",
		"access",
		"class_attr",
		"constant_unit",
		"cycle_time",
		"emphasis",
		"exit_actions",
		"identity",
		"init_actions",
		"line_color",
		"line_type",
		"max_value",
		"min_value",
		"number",
		"post_read_actions",
		"post_write_actions",
		"pre_read_actions",
		"pre_write_actions",
		"type_definition",
		"y_axis",
		"appinstance",
		"axis_items",
		"block_b",
		"block_b_type",
		"capacity",
		"characteristics",
		"chart_items",
		"chart_type",
		"charts",
		"collection_items",
		"block_number",
		"count",
		"default_value",
		"default_values",
		"definition",
		"display_format",
		"display_items",
		"edit_display_items",
		"edit_format",
		"edit_items",
		"elements",
		"enumerations",
		"file_items",
		"first",
		"graph_items",
		"graphs",
		"grid_items",
		"grids",
		"image_items",
		"image_table_index",
		"index",
		"indexed",
		"initial_value",
		"items",
		"keypoints_x_values",
		"keypoints_y_values",
		"last",
		"length",
		"link",
		"list_items",
		"lists",
		"local_parameters",
		"menu_items",
		"menus",
		"method_items",
		"method_parameters",
		"method_type",
		"methods",
		"number_of_elements",
		"number_of_points",
		"on_update_actions",
		"operation",
		"orientation",
		"parameter_lists",
		"parameters",
		"post_user_actions",
		"post_rqst_actions",
		"uuid", 
		"reference_array_items",
		"refresh_items",
		"scaling",
		"scaling_factor",
		"slot",
		"source_items",
		"style",
		"sub_slot",
		"time_format",
		"time_scale",
		"transaction",
		"unit_items",
		"variable_type",
		"vectors",
		"waveform_items",
		"waveform_type",
		"write_as_one_items",
		"component_byte_order",  
		"x_axis",
		"x_increment",
		"x_initial",
		"x_values",
		"y_values",
		"view_min",
		"view_max",
		"reserved117",
		"reserved118",
		"reserved119",
		"addressing",
		"can_delete",
		"check_configuration",
		"classification",
		"component_parent",
		"component_path",
		"component_relations",
		"components",
		"declaration",
		"detect",
		"device_revision",
		"device_type",
		"edd",
		"manufacturer",
		"maximum_number",
		"minimum_number",
		"protocol",
		"redundancy",
		"relation_type",
		"component_connect_point",
		"scan",
		"scan_list",
		"private_attr",
		"visibility",
		"product_uri",
		"api"						,
		"header"					,
		"write_mode"				,
		"plugin_items"				,
		"files"						,
		"plugins"					,
		"initial_values"			,
		"post_rqstreceive_actions"	,
		"shared"					,
		"variable_status"			,
		"count_ref"					,
		"min_value_ref"				,
		"max_value_ref"				,
		"image_id"					,
		"_end_"							// This must be the last value
	};



namespace nsEDDEngine
{
	// Constructor
	AttributeNameSet::AttributeNameSet()
	{
        m_set = new std::set<AttributeName>;
    }

	// Constructor
	AttributeNameSet::AttributeNameSet( AttributeName attr )
	{
        m_set = new std::set<AttributeName>;
		m_set->insert(attr);
	}

	// Constructor
	AttributeNameSet::AttributeNameSet( const AttributeNameSet & origSet )
	{
        m_set = new std::set<AttributeName>;
        *m_set = *(origSet.m_set);
    }

	// Destructor
	AttributeNameSet::~AttributeNameSet()
	{
        delete m_set;
    }

    
	// Initializer
	AttributeNameSet& AttributeNameSet::operator= (const AttributeNameSet & origSet )
	{
		*m_set = *(origSet.m_set);
		return *this;
	}


	// Initializer
	AttributeNameSet& AttributeNameSet::operator= (AttributeName attr )
	{
		m_set->clear();
		m_set->insert(attr);
		return *this;
	}

	// Equals operator
	bool AttributeNameSet::operator== (AttributeNameSet &rhs)
	{
		bool ret = false;

		if (m_set->size() != rhs.m_set->size())	// If they are not the same size, they aren't equal
		{
			ret = false;
		}
		else
		{		// They are the same size
			if (m_set->size() == 0)		// They are both zero
			{
				ret = true;
			}
			else	// Otherwise, compare them
			{
				ret = equal(m_set->begin( ), m_set->end( ), rhs.m_set->begin( ) );
			}
		}

		return ret;
	}

	// Add to the set
	AttributeNameSet AttributeNameSet::operator+ (AttributeName attr)
	{
		AttributeNameSet result (*this);
			
		result.m_set->insert(attr);
		return result;
	}

	// Add to the set
	AttributeNameSet& AttributeNameSet::operator+= (AttributeName attr)
	{
		m_set->insert(attr);
		return *this;
	}

	// Add to the set with an |
	AttributeNameSet AttributeNameSet::operator| ( AttributeName attr)
	{
		AttributeNameSet result;

		result = *this + attr;
		return result;
	}

	// Add to the set with an |
	AttributeNameSet& AttributeNameSet::operator|= ( AttributeName attr)
	{
		*this += attr;
		return *this;
	}

	// Remove this attribute from the set
	AttributeNameSet& AttributeNameSet::operator-= (AttributeName attr)
	{
		std::set<AttributeName>::iterator it = m_set->find(attr);

		if (it != m_set->end())
		{
			m_set->erase(it);
		}

		return *this;
	}

	// Keep only the values in both sets
	AttributeNameSet AttributeNameSet::operator- ( AttributeNameSet &rhs)
	{
		AttributeNameSet result;
		set_difference(m_set->begin(), m_set->end(), rhs.m_set->begin(), rhs.m_set->end(), std::inserter(*(result.m_set), result.m_set->begin()) );

		return result;
	}

	// Return a set with only this AttributeName, if it is in the current set
	AttributeNameSet AttributeNameSet::operator& ( AttributeName attr)
	{
		AttributeNameSet result;

		if (m_set->find(attr) != m_set->end())
		{
			result.m_set->insert(attr);
		}

		return result;
	}

	// Keep only the values in both sets
	AttributeNameSet AttributeNameSet::operator& ( AttributeNameSet &rhs)
	{
		AttributeNameSet result;
		set_intersection(m_set->begin(), m_set->end(), rhs.m_set->begin(), rhs.m_set->end(), std::inserter(*(result.m_set), result.m_set->begin()) );

		return result;
	}

	// Keep only the values in both sets
	AttributeNameSet& AttributeNameSet::operator&= ( AttributeNameSet &rhs)
	{
		*this = *this & rhs;

		return *this;
	}

	// Keep only the values in both sets
	AttributeNameSet& AttributeNameSet::operator&= ( AttributeName attr)
	{
		*this = *this & attr;

		return *this;
	}

        
    // return the attributeName that is first in the list
	AttributeName AttributeNameSet::getFirstItem() const
	{
        return *(m_set->begin());
	}

	// Find the attribute name in the set and return true if found
	bool AttributeNameSet::isMember( AttributeName attr) const
	{
        return m_set->find(attr) != m_set->end();
	}
    
	// Insert a member
	void AttributeNameSet::insert(AttributeName attr )
	{
		m_set->insert(attr);
	}
    
	// clear the set
	void AttributeNameSet::clear( )
	{
		 m_set->clear();
	}
     
	// Return true if empty
	bool AttributeNameSet::empty( ) const
	{
		 return m_set->empty();
	}
   

    // get the number of elements in the set
	size_t AttributeNameSet::size( ) const
	{
		return m_set->size();
	}

    // get the number of elements matching the key
	size_t AttributeNameSet::count( AttributeName attr) const
	{
		return m_set->count(attr);
	}

#if 0
	// Assignment
	AttributeNameSet AttributeNameSet::operator= ( AttributeName attr)
	{
		m_set->clear();
		m_set->insert(attr);
	}
#endif

}
