#pragma once
#include <stdio.h>

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <wchar.h>
#include <wtypes.h>
// XXX: For CTypedPtrArray inclusion
// TODO: Implement platform independent implementation of CTypedPtrArray
#ifdef _INC_NEW //TODO fixit - disable loading afx multi time
#include <afxtempl.h>
#endif
enum PIFileMapView {
	PI_FILE_MAP_ALL_ACCESS,
	PI_FILE_MAP_READ_ONLY,
	PI_FILE_MAP_READ_WRITE
};

enum PIPageAccess
{
	PI_PAGE_NO_ACCESS,
	PI_PAGE_READ_ONLY,
	PI_PAGE_READ_WRITE,
	PI_PAGE_WRITE_COPY
};

extern DWORD  PS_GetLastError(void);
extern UINT PS_GetSystemDirectory(wchar_t* lpBuffer, UINT   uSize);
extern  void* PS_MapViewOfFile(void* hFileMappingObject,
	int  dwDesiredAccess,
	DWORD  dwFileOffsetHigh,
	DWORD  dwFileOffsetLow,
	unsigned long  dwNumberOfBytesToMap);

extern  void * PS_CreateFileMapping(
	void*                hFile,
	BOOL				  bSetSecurityAttr,
	int                   nDesiredAccess,
	DWORD                 dwMaximumSizeHigh,
	DWORD                 dwMaximumSizeLow,
	const wchar_t*        lpName
);

extern  void* PS_OpenFileMapping(int  nDesiredAccess, BOOL   bInheritHandle, const wchar_t* lpName);
extern  BOOL PS_UnmapViewOfFile(const void* lpBaseAddress);
extern  BOOL PS_CloseHandle( void* hObject);
extern  int PS_CrtDbgReport(int reportType,const char *filename,int linenumber,const char *moduleName,const char *format);
extern  void PS_CrtDbgBreak();
extern BOOL PS_IsDebuggerPresent(void);
extern errno_t PS_GetErrno(int* err);
extern  void PS_Sleep(unsigned long lMilliSeconds);
extern   DWORD  PS_GetTicketCount();
extern   DWORD  PS_GetFileAttributes(const wchar_t* lpFileAttributes);
extern  BOOL PS_TRACE(const wchar_t *format, ...);
extern  BOOL PS_CreateDirectory(const wchar_t*, void*);
extern  DWORD PS_GetCurrentProcessId();
DWORD GetFileMapAccess(int nFileAccess);
extern void PS_DoDebugBreak(bool bEnableDebugBreak, const wchar_t* mylpString);

extern FILE * PS_wfopen(const wchar_t *filename, const wchar_t* mode);

extern errno_t PS_wfopen_s(FILE** fileptr, const wchar_t *filename, const wchar_t* mode);

// XXX: Windows shall use the utf8 flag to set the appropriate CODE_PAGE settings
// XXX: Other platforms can safely ignore these flags
// TODO: Evaluate if the Windows will still require the CODE_PAGE before conversion!!!!
extern int PS_MultiByteToWideChar(
	unsigned int CodePage,
	unsigned long dwFlags,
	const char* lpMultiByteStr,
	int cbMultiByte,
	wchar_t* lpWideCharStr,
	int cchWideChar
);

extern int PS_WideCharToMultiByte(
	unsigned int CodePage,
	unsigned long dwFlags,
	const wchar_t* lpWideCharStr,
	int cchWideChar,
	char* lpMultiByteStr,
	int cbMultiByte,
	void* lpDefaultChar,
	void* lpUsedDefaultChar
);

extern void  PS_OutputDebugString(const wchar_t* lpOutputString);

//TODO: FIXIT implement
inline extern void
PS_BssProgLog(
	const char *cFilename,		// source code filename
	UINT32 lineNumber,			// source code line-number
	WORD severity,				// see documentation
	long status,					// a BssStatus value
	const wchar_t* wMsgFormat,			// a 'wprintf' format
	...							//    and it's parameters
)
{
	(void)wMsgFormat;
	(void)status;
	(void)severity;
	(void)lineNumber;
	(void)cFilename;
}

extern void  PS_GetFileExtension(const wchar_t* pFileName, wchar_t* pFileExt);


