#ifndef ntcassert2_H
#define ntcassert2_H

// This is being provided due to a conflict with existing assert functionality.
// (for example - DeltaV has exposed their assert macro.)

#include <Inf/NtcSpecies/ntcassert.h>
#ifdef _WIN32
#define ntcassert2(exp) (static_cast<void>( (exp) ? 0 : (DoDebugBreak(), _ams_assert(_T(#exp), _T(__FILE__), __LINE__)) ))
#else
//TODO fixit
#define EXPAND(x) _T(x)
#define WFILE EXPAND(__FILE__)
#define ntcassert2(exp) (static_cast<void>( (exp) ? 0 : (DoDebugBreak(), _ams_assert(_T(#exp), WFILE, __LINE__)) ))
#endif

#endif
