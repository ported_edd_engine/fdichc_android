#include "StdAfx.h"
#include "EDDEngineLogger.h"


CEDDEngineLogger::CEDDEngineLogger()
{	
	m_pEddEngineLogFile = nullptr;
	//m_eProtocolType = eProtocolType;
//	m_bIsActionMethod = bIsActionMethod;
//	m_bIsLogFileCreated = false;
}

CEDDEngineLogger::~CEDDEngineLogger(void)
{

}

void CEDDEngineLogger::OpenLogFile()
{
		errno_t err;
		
		err = _wfopen_s(&m_pEddEngineLogFile, L"C:\\EDDEngineLog.txt", L"a+");

		if(err != 0) 
		{
			_get_errno( &err );
			m_pEddEngineLogFile = nullptr;
		}
}

void CEDDEngineLogger::CloseLogFile()
{
	if(m_pEddEngineLogFile)
	{
		fclose(m_pEddEngineLogFile);
		m_pEddEngineLogFile = nullptr;
	}
}

bool CEDDEngineLogger::ShouldLog(LogSeverity eLogSeverity, wchar_t*)
{
	bool rc = false;

	if ((eLogSeverity == Error) || (eLogSeverity == Critical))
	{
		rc = true;	
	}

	return rc;
}
void CEDDEngineLogger::Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory)
{
	if (ShouldLog(eLogSeverity, sCategory))
	{

		if (m_pEddEngineLogFile != NULL)
		{

			wchar_t buffer[100] = {0};
			int writecnt;

			_wstrdate_s(buffer);					// Output Date
			writecnt = fwprintf_s(m_pEddEngineLogFile, L"%s ", buffer);
			_wstrtime_s(buffer);
			writecnt = fwprintf_s(m_pEddEngineLogFile, L"%s: ", buffer);	// Output Time
			writecnt = fwprintf_s(m_pEddEngineLogFile, L"%s", sMessage);		// Output string

			fflush(m_pEddEngineLogFile);
		}

	}
}

