/*
 *	@(#) $Id: evl_str.c,v 1.2 1996/01/04 20:12:47 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  STRING.
 */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"

#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif


/*********************************************************************
 *
 *	Name: ddl_free_string
 *	ShortDesc: Free STRING structure.
 *
 *	Description:
 *		ddl_free_string will check the flags in the STRING structure.
 *      If flags indicate to free string then the STRING structure will
 *      be freed.
 *
 *	Inputs:
 *		str: pointer to the STRING structure
 *
 *	Outputs:
 *		str: pointer to the empty STRING structure
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_string(
STRING         *str)
{

	if (str->flags & FREE_STRING) {
		free((void *) str->str);
	}

	str->flags = 0;
	str->len = 0;
	str->str = NULL;

	return;
}

/*********************************************************************
 *
 *	Name: string_enum_lookup
 *
 *	ShortDesc: lookup an enumerated string
 *
 *	Description:
 *		string_enum_lookup will find the string referred to by an
 *		enumeration value string. This string contains the ITEM_ID
 *		of an enumerated variable, and the enumeration value which
 *		corresponds to the description string desired
 *
 *	Inputs:
 *		enum_id - ITEM_ID of the enumeration
 *		enum_val - value of the enumeration
 *      string - a pointer to the STRING struct to be loaded
 *		depinfo - a pointer to the OP_REF_LIST struct
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string - the loaded STRING structure
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				  dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		FAILURE
 *		return codes from fetch_var(), eval_attr_enum(), append_depinfo()
 *		eval_clean_var()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static int
string_enum_lookup(
ITEM_ID         enum_id,
unsigned long   enum_val,
STRING         *string,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed,
EnumMap        *pEnumMap = nullptr)
{
	int rc = DDL_SUCCESS;

	FLAT_VAR *pVar = nullptr;

	if (pEnumMap)	// If we are using an Enum Cache, look up the enum_id
	{
		EnumMap::iterator it;

		it = pEnumMap->find(enum_id);

		if (it != pEnumMap->end())	// Set pVar, if it is found
		{
			pVar = it->second;
		}
	}

	if (pVar == nullptr )	// We could not set this from our Enum Cache, so we must fetch, eval, and insert it in the cache
	{
		pVar = (FLAT_VAR*)calloc(1, sizeof(FLAT_VAR));	/* flat variable struct for calling fetch with */

		SCRATCH_PAD     scratch[2];	/* scratch pads for fetch */

		/*
		 * Fetch the binary for the enumerated variable
		 */

		rc = resolve_fetch(env_info, enum_id,
			(unsigned long) VAR_ENUMS,
			(void *) pVar, (unsigned short) VARIABLE_ITYPE,
			env_info->block_handle, scratch);

		if (rc == DDL_SUCCESS) {

			if (!(pVar->masks.attr_avail & VAR_ENUMS)) {

				/*
				 * Evaluate the binary for the enumerated value list
				 */
				DEPBIN *cur_depbin = pVar->depbin->db_enums;

				rc = eval_attr_enum(cur_depbin->bin_chunk,
					cur_depbin->bin_size,
					&pVar->enums,
					&cur_depbin->dep,
					env_info,
					var_needed);


				if (depinfo != NULL) {
					int rc2 = append_depinfo(depinfo, &cur_depbin->dep);
					if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
						rc = rc2;
					}
				}

				ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
			}
		}

		// free the memory used by fetch
		resolve_fetch_free(env_info, (void *) pVar, (unsigned short) VARIABLE_ITYPE, scratch, env_info->block_handle);

		if ((rc == DDL_SUCCESS) && (pEnumMap))	// Add this FLAT_VAR to our Enum Cache, if we are using one
		{
			std::pair<EnumMap::iterator, bool> pr = pEnumMap->insert(EnumMap::value_type (enum_id, pVar));
		}
	}

	if (rc == DDL_SUCCESS) {	// We must now search the enum list for the entry we want

		ENUM_VALUE *enum_ptr = pVar->enums.list;
		unsigned short count = pVar->enums.count;

		/*
		 * Find the match in the list
		 */
		bool bIsFound = false;

		for (unsigned short inc = 0; inc < count; enum_ptr++, inc++) {

			if (enum_ptr->val == enum_val) {

                string->str = wcsdup(enum_ptr->desc.str);	// Copy the string into our output
				string->len = enum_ptr->desc.len;
				string->flags = FREE_STRING;

				bIsFound = true;
				break;
			}
		}

		if (bIsFound == false) {	/* The enum was not found */
            STRING strDictString = { 0 };

            rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_UNDEFINED, &strDictString);

			CStdString strNumUndefined;

            strNumUndefined.Format(L"0x%x %s", enum_val, strDictString.str);

            string->str = wcsdup(strNumUndefined);
            string->len = (unsigned short)wcslen(string->str);
            string->flags = FREE_STRING;

            ddl_free_string(&strDictString);

			rc = ENUM_NOT_FOUND;
		}
	}

	if (pEnumMap == nullptr) {	// If we didn't save it in our Enum Cache, we must clean it here.
		ddl_free_enum_list(&pVar->enums, FREE_ATTR);
		free(pVar);
		pVar = nullptr;
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_string(chunkp, size, string)
 *
 *	ShortDesc: ddl_parse_string takes a binary chunk and loads a STRING structure with it.
 *
 *	Description:
 *		ddl_parse_string takes a binary chunk and reads the tag to determine which string type
 *		the string is: DICTIONARY_STRING, ENUMERATION_STRING, DEV_SPEC_STRING, TEXT_STRING,
 *		UNUSED_STRING or VARIABLE_STRING, it loads the string info structure with that type
 *		and looks up the corresponding string.
 *		If the string argument is NULL the structure is not loaded but the chunk pointer is
 *		updated.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to an STRING structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from DDL_PARSE_INTEGER(), ddl_parse_item_id(),
 *		app_func_get_dev_spec_string(), ddl_add_depinfo(), app_func_get_param_value()
 *		app_func_get_dict_string(), string_enum_lookup()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/


int
ddl_parse_string(
unsigned char **chunkp,
DDL_UINT       *size,
STRING         *string,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed,
EnumMap        *pEnumMap)
{
	int             rc;	/* return code */
	DEV_STRING_INFO dev_str_info;	/* holds device specific string info */
	DDL_UINT        tag;	/* temporary tag */
	DDL_UINT        temp;	/* temporary storage for casting conversion */
	DDL_UINT        tag_len;/* length of binary assoc. w/ parsed tag */
	OP_REF          oper_ref = {STANDARD_TYPE, 0};	/* used by variable strings */
	ITEM_ID         enum_id;/* used for enumeration strings */
	unsigned long   enum_val;	/* used for enumeration strings */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_STRING);
#endif

	/*
	 * initialize string struct
	 */

	if (string != NULL) {
		string->flags = DONT_FREE_STRING;
		string->len = 0;
		string->str = NULL;
	}

	/*
	 * Parse the tag to find out what kind of string it is.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &tag_len);

	switch (tag) {

	case NAME_STR_TAG:
		{
			DEVICE_TYPE_HANDLE dth = -1;
			ROD_HANDLE rod_handle = 0;
			rc = pConnMgr->get_abt_adtt_offset(env_info->block_handle, &dth);

			if (rc  == DDS_SUCCESS)
			{
				rc = g_DeviceTypeMgr.get_adtt_dd_handle (dth, &rod_handle);
			}

			bool string_encoded_in_utf8 = false;
			if (rc  == DDS_SUCCESS)
			{
				string_encoded_in_utf8 = g_DeviceTypeMgr.is_string_encoded_in_utf8(rod_handle);
			}

			if (rc != DDS_SUCCESS)
			{
				return rc;
			}


			UINT code_page = SB_CODE_PAGE;
			if (string_encoded_in_utf8)
			{
				code_page = CP_UTF8;
			}
			
			unsigned long name_str_len;
			DDL_PARSE_INTEGER(chunkp, size, &name_str_len);
			if (string)
			{
				string->str = (wchar_t *) malloc((size_t) ((name_str_len + 1) * sizeof(wchar_t)));
				if (string->str == NULL)
				{
					return DDL_MEMORY_ERROR;
				}

				int numChars = PS_MultiByteToWideChar(code_page, 0, (const char*)*chunkp, name_str_len,
															string->str, name_str_len+1);
				if (numChars == 0)
				{
					return DDL_ENCODING_ERROR;
				}

				string->str[numChars] = L'\0';	// Null terminate
				string->len = (unsigned short)numChars;
				string->flags = FREE_STRING;
			}
			*chunkp += name_str_len;
			*size -= name_str_len;
		}
		break;
	
	case DEV_SPEC_STRING_TAG:

		/*
		 * device specific string (ie. string number). Parse the file
		 * information and string number.
		 */

		if (string) {

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.mfg = (unsigned long) temp;

			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.dev_type = (unsigned short) temp;


			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.rev = (unsigned char) temp;


			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.ddrev = (unsigned char) temp;


			DDL_PARSE_INTEGER(chunkp, size, &temp);
			dev_str_info.id = (unsigned int) temp;

			rc = app_func_get_dev_spec_string(env_info, &dev_str_info,
				string);


			/*
			 * If a string was not found, get the default error
			 * string.
			 */

			if (rc != DDL_SUCCESS) {
				rc = app_func_get_dict_string(env_info, DEFAULT_DEV_SPEC_STRING,
					string);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
		else {
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	case VARIABLE_STRING_TAG:
	case VAR_REF_STRING_TAG:

		/*
		 * variable/variable_reference string. Parse the variable id.
		 */

		if (tag == VARIABLE_STRING_TAG) {
			DDL_PARSE_INTEGER(chunkp, size, &temp);
			oper_ref.op_info.id = (ITEM_ID) temp;
			oper_ref.op_info.member = 0;
			oper_ref.op_info.type = VARIABLE_ITYPE;
		}
		else {		/* VAR_REF_STRING_TAG 	 */
			rc = ddl_parse_op_ref(chunkp, size, &oper_ref, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		if (depinfo) {

			/*
			 * Add item_id of string to dependency info.
			 */

			rc = ddl_add_depinfo(&oper_ref, depinfo);

		}

		if (string)
		{
			EVAL_VAR_VALUE  var_value;	/* used by app_func_get_param_value */

			rc = app_func_get_param_value(env_info, &oper_ref, &var_value);
			if (rc != DDL_SUCCESS) {
				var_needed->op_info.id = oper_ref.op_info.id;
				var_needed->op_info.member = oper_ref.op_info.member;
				var_needed->op_info.type = oper_ref.op_info.type;
				return rc;
			}

			switch(var_value.type)
			{
			case ENUMERATED:
			case BIT_ENUMERATED:
                {
                    // If the var_value type is ENUMERATED/BIT_ENUMERATED, we need to lookup for the string associated with enum value
                    rc = string_enum_lookup(oper_ref.op_info.id, (unsigned long)var_value.val.ull, string,
                                                depinfo, env_info, var_needed, pEnumMap);

                    if ((rc != DDL_SUCCESS) && (rc != ENUM_NOT_FOUND)) // If the enum was not found, we manufacture one.
                    {
                        return rc;  // Return the failed error code
                    }

                    rc = DDL_SUCCESS;
                }
				break;
			case ASCII:
			case PACKED_ASCII:
			case PASSWORD:
			case EUC:
			case VISIBLESTRING:
				{
                    string->str = wcsdup(var_value.val.s.str);	// Copy the string into our output
					string->len = var_value.val.s.len;
					string->flags = FREE_STRING;
				}
				break;
			default:
				// If var_value type is not string or enumerated type, error will be returned
				return DDL_ENCODING_ERROR;

			}
		}
		break;

	case ENUMERATION_STRING_TAG:
	case ENUM_REF_STRING_TAG:

		/*
		 * enumeration/enumeration_reference string. Parse the
		 * enumeration ID, and the value within the enumeration.
		 */

		if (string) {

			if (tag == ENUMERATION_STRING_TAG) {
				DDL_PARSE_INTEGER(chunkp, size, &temp);
				enum_id = (ITEM_ID) temp;
			}
			else {	/* ENUM_REF_STRING_TAG */
				rc = ddl_parse_item_id(chunkp, size, &oper_ref.op_info.id, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				enum_id = oper_ref.op_info.id;
			}

			DDL_PARSE_INTEGER(chunkp, size, &enum_val);
			rc = string_enum_lookup(enum_id, enum_val, string,
				                        depinfo, env_info, var_needed, pEnumMap);

            if ((rc != DDL_SUCCESS) && (rc != ENUM_NOT_FOUND)) // If the enum was not found, we manufacture one.
            {
                return (rc);  // Return the failed error code
			}

            rc = DDL_SUCCESS;
        }
		else {
			if (tag == ENUMERATION_STRING_TAG) {
				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			}
			else {
				rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	case DICTIONARY_STRING_TAG:

		/*
		 * dictionary string. Parse the dictionary string number.
		 */

		if (string) {

			DDL_PARSE_INTEGER(chunkp, size, &temp);

			rc = app_func_get_dict_string(env_info, temp, string);


			/*
			 * If a string was not found, get the default error
			 * string.
			 */

			if (rc != DDL_SUCCESS) {
				rc = app_func_get_dict_string(env_info,
					DEFAULT_STD_DICT_STRING, string);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
		else {
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		break;

	default:

		/*
		 * Unknown tag. Default this string.
		 */

		if (string) {
			rc = app_func_get_dict_string(env_info,
				DEFAULT_STD_DICT_STRING, string);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		*chunkp += tag_len;
		*size -= tag_len;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_string_choice
 *	ShortDesc: Choose the correct string from a binary.
 *
 *	Description:
 *		ddl_string_choice will parse the binary for an string,
 *		according to the current conditionals (if any).  The value of
 *		the string is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to a STRING structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_string().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_string_choice(
unsigned char **chunkp,
DDL_UINT       *size,
STRING         *string,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef ISPTEST
	TEST_FAIL(DDL_STRING_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}


	/*
	 * A chunk was found and a value is requested.
	 */

	if (string && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/*
		 * We have finally gotten to the actual value!  Parse it.
		 */

		rc = ddl_parse_string(&val.chunk, &val.size, string, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_string
 *	ShortDesc: Evaluate a string
 *
 *	Description:
 *		eval_string parses a chunk of binary data and loads string
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		string - space for parsed string
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string is loaded with the string
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		return codes from ddl_string_choice(), ddl_shrink_depinfo()
 *		DDL_DEFAULT_ATTR - invalid string
 *		DDL_SUCCESS - chunk was parsed and string was loaded successfully
 *
 *	Author: Jon Westbrock
 *
 *********************************************************************/
int
eval_string(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	STRING*			string = static_cast<STRING*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* data valid flag */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_STRING);
#endif


	valid = 0;

	if (string) {
		ddl_free_string(string);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_string_choice(&chunk, &size, string, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (string && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: link_string()
 *	ShortDesc: collect dependencies from strings for the HART linker
 *
 *	Author: Christian Gustafson
 *
 ***********************************************************************/

int
link_string(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc;	/* return code */
	DDL_UINT        temp;	/* temporary storage for casting conversion */
	DDL_UINT        tag;	/* temporary tag */
	DDL_UINT        tag_len;/* length of binary assoc. w/ parsed tag */
	OP_REF          oper_ref = {STANDARD_TYPE, 0};	/* used by variable strings */



	ASSERT_DBG(chunkp && *chunkp && size && depinfo);

	/*
	 * Parse the tag to find out what kind of string it is.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &tag_len);

	switch (tag) {

	case DEV_SPEC_STRING_TAG:
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		break;

	case VARIABLE_STRING_TAG:
	case VAR_REF_STRING_TAG:

		/*
		 * variable/variable_reference string. Parse the variable id.
		 */
		if (tag == VARIABLE_STRING_TAG) {
			DDL_PARSE_INTEGER(chunkp, size, &temp);
			oper_ref.op_info.id = (ITEM_ID) temp;
			oper_ref.op_info.member = 0;
			oper_ref.op_info.type = 0;
		}
		else {		/* VAR_REF_STRING_TAG 	 */
			rc = ddl_parse_op_ref(chunkp, size, &oper_ref, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		/*
		 * Add item_id of string to dependency info.
		 */

		rc = ddl_add_depinfo(&oper_ref, depinfo);
		break;

	case ENUMERATION_STRING_TAG:
	case ENUM_REF_STRING_TAG:

		/*
		 * enumeration/enumeration_reference string. Parse the
		 * enumeration ID, and the value within the enumeration.
		 */
		if (tag == ENUMERATION_STRING_TAG) {
			DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		}
		else {
			rc = link_ref(chunkp, size, depinfo, env_info, var_needed, (DDL_UINT) 0);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		break;

	case DICTIONARY_STRING_TAG:

		/*
		 * dictionary string. Parse the dictionary string number.
		 */
		DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
		break;

	default:

		/*
		 * Unknown tag. Default this string.
		 */
		*chunkp += tag_len;
		*size -= tag_len;
	}

	return DDL_SUCCESS;
}
