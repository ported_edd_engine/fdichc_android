#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"
#include "TestSupport/MethodHelper.h"
#include "TestSupport/Helper.h"
#include "nsEDDEngine/ProtocolType.h"
#include <time.h>

#define make_time_t( a)    ( (long)   (a) )	/* returns Htime_t, a long */

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        namespace Methods
        {

            [TestClass, Ignore]
            public ref class BaseTestEmptyMethod
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method ;

                static void SetupBase(String^ testCaseName)
                {
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_empty");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestSimpleMessageMethod 
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method;

                static void SetupBase(String^ testCaseName)
                {
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_put_message_simple");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("InfoRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("InfoRequest(Hello EDDL!),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodWithLabel
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method ;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                    pValue->size = 1;
                    pValue->type = VT_INTEGER;
                    pValue->val.i = 0;
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_put_message_with_label");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("InfoRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("InfoRequest(Label is test variable 1),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodWithUnit
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                    pValue->size = 1;
                    pValue->type = VT_INTEGER;
                    pValue->val.i = 0;
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_put_message_with_unit");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("InfoRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("InfoRequest(Unit is unit1),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodWithVarids
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                    pValue->size = 1;
                    pValue->type = VT_INTEGER;
                    pValue->val.i = 5;
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_put_message_with_varids");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("GetParamValue,GetParamValue,InfoRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("GetParamValue(0,00000000,16399),GetParamValue(0,00000000,16400),"
                        "InfoRequest(var1: 5\nvar2: 5),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodAcknowledge
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    Helper::SetAcknowledgeFunction(builtinSupportSimulator, BSEC_SUCCESS);

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "test_method_acknowledge_simple");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("AcknowledgementRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("AcknowledgementRequest(Hello EDDL!),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodAcknowledgeWithVarids 
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    Helper::SetAcknowledgeFunction(builtinSupportSimulator, BSEC_SUCCESS);
                    EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                    pValue->size = 1;
                    pValue->type = VT_INTEGER;
                    pValue->val.i = 5;
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "test_method_acknowledge_with_varids");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod);

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("GetParamValue,GetParamValue,AcknowledgementRequest,"
                        "OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {                    
                    Assert::AreEqual(
                        "GetParamValue(0,00000000,16399),GetParamValue(0,00000000,16400),"
                        "AcknowledgementRequest(var1: 5\nvar2: 5),"
                        "OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodFromListSimple
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                    Helper::SetSelectionRequestFunction(builtinSupportSimulator, BSEC_SUCCESS, 2);

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "test_method_select_from_list_simple");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {

                    Assert::AreEqual("SelectionRequest,InfoRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {

                    Assert::AreEqual("SelectionRequest(What do you want?,Option 1;Option 2;Option 3),InfoRequest(2),OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodSimpleDelay 
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method;

                static void SetupBase(String^ testCaseName)
                {
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "test_method_delay_simple");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual("DelayMessageRequest,OnMethodExiting",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual("DelayMessageRequest(2,delay!),"
                        "OnMethodExiting(0)",
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodMultipleDelay 
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static FLAT_METHOD* test_method;

                static void SetupBase(String^ testCaseName)
                {

                   
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();
                  

                    EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                    pValue->size = 4;
                    pValue->type = VT_INTEGER;
                    pValue->val.i = 14;
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                    builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));
                    
                    builtinSupportSimulator->SetInvokeCallBackDelay(700);

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    test_method = MethodHelper::GetMethod(engine, "method_DELAY_DDVAR_formatD");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                    delete test_method;
                }
               
                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_DelayRequest_five_times()
                {
                    Assert::AreEqual(5, builtinSupportSimulator->DelayMessageRequestCallCount);
                }
                
                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_GetParamValue_ten_or_more_times()
                {
                    Assert::IsTrue((builtinSupportSimulator->GetParamValueCallCount >= 9), "GetParamValue was called less than 9 times. It should be called twice for each call to method_DELAY_DDVAR_formatD.");
                }
                
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodWithVisibility
            {
            public:
                static EddEngineController^ engine;
                static FLAT_METHOD* result;

                static void SetupBase(String^ testCaseName)
                {
                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, nullptr);

                    result = MethodHelper::GetMethod(engine, "My_Second_Test_Method");
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete result;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_provide_the_visibility_attribute()
                {
                    Assert::IsTrue(result->isAvailable(visibility) );
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_provide_the_visibility_true()
                {
                    Assert::IsTrue(result->visibility == nsEDDEngine::True);
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodWithLongName
            {
            public:
                static EddEngineController^ engine;
                static FLAT_METHOD* result;

                static void SetupBase(String^ testCaseName)
                {
                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, nullptr);

                    result = MethodHelper::GetMethod(engine, "Method_with_very_long_name_which_circumstance_should_not_have_any_negative_effects_whatsoever_and_everything_should_run_smoothly");
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete result;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_be_possible_to_get_the_method_item_with_correct_item_ID_from_EDD_Engine()
                {
                    Assert::AreEqual(16386u, result->id);
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestAbortingMethod
            {
            protected:
                static EddEngineController^ engine;
                static bool exceptionThrown = false;
                static nsEDDEngine::Mth_ErrorCode executionSuccess = METH_FAILED;
                static BuiltinSupportSimulator *pBuiltinSupport = nullptr;

            public:
                static void SetupBase(String^ eddFileName, String^ methodName)
                {
                    engine = gcnew EddEngineController(eddFileName);

                    try
                    {
                        ParamCacheSimulator* paramCacheSimulator = new ParamCacheSimulator();
                        pBuiltinSupport = new BuiltinSupportSimulator();

                        engine->Initialize(paramCacheSimulator, pBuiltinSupport);
                        paramCacheSimulator->SetEddEngine(engine->Instance);

                        wchar_t* langCode = L"en";
                        FLAT_METHOD* method = MethodHelper::GetMethod(engine, methodName);

                        ACTION methodAction; 
                        methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                        methodAction.action.meth_definition.size = wcslen(method->def.data);
                        methodAction.action.meth_definition.data = method->def.data;
                        nsEDDEngine::IAsyncResult* callResult = engine->Instance->BeginMethod(0, nullptr, &methodAction, nullptr, nullptr, langCode);

                        executionSuccess = engine->Instance->EndMethod(&callResult);

                        //this is not methodAction's data, please don't delete it
                        methodAction.action.meth_definition.data = nullptr;
                    }
                    catch (...)
                    {
                        exceptionThrown = true;
                    }

                }

                static void CleanupBase()
                {
                    if(pBuiltinSupport != nullptr) delete pBuiltinSupport;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_crash()
                {
                    Assert::IsFalse(exceptionThrown);
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_inform_the_caller_of_the_abortion()
                {
                     Assert::IsTrue(executionSuccess == METH_ABORTED);
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_OnMethodExiting()
                {
                    Assert::IsTrue(pBuiltinSupport->Recorder->WasCalled(L"OnMethodExiting"));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_set_eTermActivity_to_Discard()
                {
                    Assert::AreEqual((int) nsEDDEngine::ChangedParamActivity::Discard,*((int*) (pBuiltinSupport->Recorder->GetParameter(L"OnMethodExiting.eTermAction"))));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestAbortingScalingMethod
            {
            protected:
                static EddEngineController^ engine;
                static bool exceptionThrown = false;
                static nsEDDEngine::Mth_ErrorCode executionSuccess = METH_FAILED;
                static BuiltinSupportSimulator *pBuiltinSupport = nullptr;
                static EVAL_VAR_VALUE *scalingValue = nullptr;

            public:
                static void SetupBase(String^ eddFileName, String^ methodName)
                {
                    engine = gcnew EddEngineController(eddFileName);

                    try
                    {
                        ParamCacheSimulator* paramCacheSimulator = new ParamCacheSimulator();
                        pBuiltinSupport = new BuiltinSupportSimulator();

                        engine->Initialize(paramCacheSimulator, pBuiltinSupport);
                        paramCacheSimulator->SetEddEngine(engine->Instance);

                        wchar_t* langCode = L"en";
                        FLAT_METHOD* method = MethodHelper::GetMethod(engine, methodName);
                        scalingValue = new EVAL_VAR_VALUE();
                        scalingValue->size = 4;
                        scalingValue->type = VariableType::VT_INTEGER;
                        scalingValue->val.i = 12;

                        ACTION methodAction; 
                        methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                        methodAction.action.meth_definition.size = wcslen(method->def.data);
                        methodAction.action.meth_definition.data = method->def.data;
                        nsEDDEngine::IAsyncResult* callResult = engine->Instance->BeginVariableActionMethod(0, nullptr, scalingValue, &methodAction, nullptr, nullptr, langCode);

                        executionSuccess = engine->Instance->EndVariableActionMethod(&callResult,  scalingValue);

                        //this is not methodAction's data, please don't delete it
                        methodAction.action.meth_definition.data = nullptr;
                    }
                    catch (...)
                    {
                        exceptionThrown = true;
                    }

                }

                static void CleanupBase()
                {
                    if(pBuiltinSupport != nullptr) delete pBuiltinSupport;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_crash()
                {
                    Assert::IsFalse(exceptionThrown);
                }


                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_OnMethodExiting()
                {
                    Assert::IsTrue(pBuiltinSupport->Recorder->WasCalled(L"OnMethodExiting"));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_set_eTermActivity_to_Discard()
                {
                    Assert::AreEqual((int) nsEDDEngine::ChangedParamActivity::Discard,*((int*)(pBuiltinSupport->Recorder->GetParameter(L"OnMethodExiting.eTermAction"))));
                }

				[TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_AcknowledgementRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->AcknowledgementRequestCallCount, "AcknowledgeRequest was called");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_DelayMessageRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->DelayMessageRequestCallCount, "DelayMessageRequest was called");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_InfoRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->InfoRequestCallCount, "InfoRequest was called");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_InputRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->InputRequestCallCount, "InputRequest was called");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_ParameterInputRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->ParameterInputRequestCallCount, "ParameterInputRequest was called");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_UIDRequest()
                {
                    Assert::AreEqual(0, pBuiltinSupport->UIDRequestCallCount, "UIDRequest was called");
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestNonAbortingScalingMethod
            {
            protected:
                static EddEngineController^ engine;
                static bool exceptionThrown = false;
                static nsEDDEngine::Mth_ErrorCode executionSuccess = METH_FAILED;
                static BuiltinSupportSimulator *pBuiltinSupport = nullptr;

            public:
                static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE* valToScale)
                {
                    engine = gcnew EddEngineController(eddFileName);

                    try
                    {
                        ParamCacheSimulator* paramCacheSimulator = new ParamCacheSimulator();
                        pBuiltinSupport = new BuiltinSupportSimulator();

                        engine->Initialize(paramCacheSimulator, pBuiltinSupport);
                        paramCacheSimulator->SetEddEngine(engine->Instance);

                        wchar_t* langCode = L"en";
                        FLAT_METHOD* method = MethodHelper::GetMethod(engine, methodName);

                        ACTION methodAction; 
                        methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                        methodAction.action.meth_definition.size = wcslen(method->def.data);
                        methodAction.action.meth_definition.data = method->def.data;
                        nsEDDEngine::IAsyncResult* callResult = engine->Instance->BeginVariableActionMethod(0, nullptr, valToScale, &methodAction, nullptr, nullptr, langCode);

                        executionSuccess = engine->Instance->EndVariableActionMethod(&callResult,  valToScale);

                        //this is not methodAction's data, please don't delete it
                        methodAction.action.meth_definition.data = nullptr;
                    }
                    catch (...)
                    {
                        exceptionThrown = true;
                    }

                }

                static void CleanupBase()
                {
                    if(pBuiltinSupport != nullptr) delete pBuiltinSupport;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_crash()
                {
                    Assert::IsFalse(exceptionThrown);
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_inform_the_caller_of_the_successful_execution()
                {
                    Assert::IsTrue(executionSuccess == METH_SUCCESS);
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_OnMethodExiting()
                {
                    Assert::IsTrue(pBuiltinSupport->Recorder->WasCalled(L"OnMethodExiting"));
                }
            };

            [TestClass, Ignore]
            public ref class BaseTestMethodList
            {
            public:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator* builtinSupportSimulator;
                static String^ _expectedOrder;
                static String^ _expectedParameter;

                static void SetupBase(
                    String^ testCaseName,
                    String^ methodName,
                    String^ expectedOrder,
                    String^ expectedParameter)
                {
                    _expectedOrder = expectedOrder;
                    _expectedParameter = expectedParameter;
                    // setup BuiltinSupportSimulator
                    builtinSupportSimulator = new BuiltinSupportSimulator();

                    engine = gcnew EddEngineController(testCaseName);
                    engine->Initialize(nullptr, builtinSupportSimulator);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, methodName);

                    ACTION methodAction;
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    wchar_t* languageCode = L"en";

                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        0, 
                        nullptr, 
                        &methodAction,
                        nullptr, 
                        nullptr,
                        languageCode);

                    engine->Instance->EndMethod( & asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }

                static void CleanupBase()
                {
                    delete engine;
                    delete builtinSupportSimulator;
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual(_expectedOrder,
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual(_expectedParameter,
                        gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
                }
            };

            [TestClass, Ignore]
            public ref class BaseClassParameterInputRequests
            {
            private:
                static BuiltinSupportSimulator *builtinSupport;
                static EddEngineController^ engine;
                static ITEM_ID expectedItemIdParameter;
                static String^ _expectedOrder;
                static String^ _expectedParameter;

            public:

                static void SetupBase(
                    String^ eddFileName, 
                    String^ methodName, 
                    String^ parameterName,
                    String^ expectedOrder,
                    String^ expectedParameter)
                { 

                    _expectedOrder = expectedOrder;
                    _expectedParameter = expectedParameter;

                    builtinSupport = new BuiltinSupportSimulator();
                    engine = gcnew EddEngineController(eddFileName);
                    engine->Initialize(nullptr, builtinSupport);

                    FDI_PARAM_SPECIFIER test_parameter_specifier;
                    engine->FillParamSpecifier(parameterName, &test_parameter_specifier);
                    expectedItemIdParameter = test_parameter_specifier.id;

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, methodName);

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;

                }

                static void CleanupBase()
                {
                    if (builtinSupport) delete builtinSupport;

                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_ParameterInputRequest_once()
                {
                    Assert::AreEqual(1, builtinSupport->ParameterInputRequestCallCount);
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_have_the_expected_itemid_for_the_requested_parameter()
                {
                    nsEDDEngine::OP_REF_TRAIL *providedValue = nullptr;
                    providedValue = (nsEDDEngine::OP_REF_TRAIL*)builtinSupport->Recorder->GetParameter(L"ParameterInputRequest.pParameter");

                    Assert::AreEqual( expectedItemIdParameter, providedValue->desc_id);
                }   

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_in_correct_order()
                {
                    Assert::AreEqual(_expectedOrder, gcnew System::String(builtinSupport->GetCalledMethodsAsString().c_str()));
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_the_builtin_callbacks_with_correct_parameters()
                {
                    Assert::AreEqual(_expectedParameter, gcnew System::String(builtinSupport->GetCalledMethodParametersAsString().c_str()));
                }
            };

            // WORKAROUND: use TestInitalize instead ClassInitalize
            // Impediment
            // - MSTest runs all test in a not controllable order 
            // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
            // -> Some of the tests runs with an other configuration as wanted
            // -> DANGER: Most tests passes anyway, but not with the wanted configuration
            // possible Improvement:
            // -> Change TestEnvironment to nonstatic 
            // -> Split BaseClass to avoid initializing every item every time (example: Chart with items 7, but every test use only 1)

            [TestClass]
            public ref class When_running_test_method_empty_with_FM8 : BaseTestEmptyMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_empty_with_FMA : BaseTestEmptyMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_simple_with_FM8 : BaseTestSimpleMessageMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_simple_with_FMA : BaseTestSimpleMessageMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_label_with_FM8 : BaseTestMethodWithLabel
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_label_with_FMA : BaseTestMethodWithLabel
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_unit_with_FM8 : BaseTestMethodWithUnit
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_unit_with_FMA : BaseTestMethodWithUnit
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_varids_with_FM8 : BaseTestMethodWithVarids
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_put_message_with_varids_with_FMA : BaseTestMethodWithVarids
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_acknowledge_simple_with_FM8 : BaseTestMethodAcknowledge
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_acknowledge_simple_with_FMA : BaseTestMethodAcknowledge
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_acknowledge_with_varids_with_FM8 : BaseTestMethodAcknowledgeWithVarids
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_acknowledge_with_varids_with_FMA : BaseTestMethodAcknowledgeWithVarids
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_select_from_list_simple_with_FM8 : BaseTestMethodFromListSimple
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_select_from_list_simple_with_FMA : BaseTestMethodFromListSimple
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_delay_simple_with_FM8 : BaseTestMethodSimpleDelay
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_delay_simple_with_FMA : BaseTestMethodSimpleDelay
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0013.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_delay_multiple_with_FM8 : BaseTestMethodMultipleDelay
            {
            public:
                [TestInitialize]
                void Setup()
                {
                   
                    SetupBase("Hart\\TestCase0019.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_running_test_method_delay_multiple_with_FMA : BaseTestMethodMultipleDelay
            {
            public:
                [TestInitialize]
                void Setup()
                {
                   
                    SetupBase("Hart\\TestCase0019.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };


            [TestClass]
            public ref class When_reading_a_method_with_visibility_from_the_edd_engine_with_FMA : BaseTestMethodWithVisibility
                // This doesn�t work for legacy HART!
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0018.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_loading_an_EDD_containing_a_method_with_a_long_name_with_FM8 : BaseTestMethodWithLongName
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase001d.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_loading_an_EDD_containing_a_method_with_a_long_name_with_FMA : BaseTestMethodWithLongName
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase001d.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    //CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_aborts_in_TestCase_0027_fmA : BaseTestAbortingMethod
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0027.fmA", "abort_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_aborts_in_TestCase_0027_fm8 : BaseTestAbortingMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0027.fm8", "abort_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_scaling_method_that_aborts_in_testCase_0027_fmA : BaseTestAbortingScalingMethod
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0027.fmA", "abort_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_AbortRequest()
                {                   
                    Assert::AreEqual(1, pBuiltinSupport->AbortRequestCallCount, "AbortRequest was called during execution");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_AbortRequest()
                {     
                    Assert::IsTrue(true, "For this Method, AbortRequest should called");
                }
            };

            [TestClass]
            public ref class When_calling_a_scaling_method_that_aborts_in_testCase_0027_fm8 : BaseTestAbortingScalingMethod
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0027.fm8", "abort_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_AbortRequest()
                {                   
                    Assert::AreEqual(1, pBuiltinSupport->AbortRequestCallCount, "AbortRequest was called during execution");
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_not_call_AbortRequest()
                {     
                    Assert::IsTrue(true, "For this Method, AbortRequest should called");
                }
            };

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_that_does_not_explicitely_call_save_values_in_TestCase0025_fmA : BaseTestNonAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    valueToTest = new EVAL_VAR_VALUE();
                    valueToTest->size = 4;
                    valueToTest->type = VariableType::VT_INTEGER;
                    valueToTest->val.i = 42;
                    SetupBase("Hart\\TestCase0025.fmA", "add_100", valueToTest);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_scale_the_value_correctly()
                {
                    Assert::AreEqual((unsigned int)4, valueToTest->size, "scaling function changed size of EVAL_VAR_VALUE");
                    Assert::AreEqual((int) VariableType::VT_INTEGER, (int) valueToTest->type, "scaling function changed type of EVAL_VAR_VALUE");
                    Assert::AreEqual((__int64)142, valueToTest->val.i);
                }
            };

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_that_does_not_explicitely_call_save_values_in_TestCase0025_fm8 : BaseTestNonAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    valueToTest = new EVAL_VAR_VALUE();
                    valueToTest->size = 4;
                    valueToTest->type = VariableType::VT_INTEGER;
                    valueToTest->val.i = 42;
                    SetupBase("Hart\\TestCase0025.fm8", "add_100", valueToTest);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_scale_the_value_correctly()
                {
                    Assert::AreEqual((unsigned int)4, valueToTest->size, "scaling function changed size of EVAL_VAR_VALUE");
                    Assert::AreEqual((int) VariableType::VT_INTEGER, (int) valueToTest->type, "scaling function changed type of EVAL_VAR_VALUE");
                    Assert::AreEqual((__int64)142, valueToTest->val.i);
                }
            };

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_that_does_cause_an_error_in_TestCase0025_fm8 : BaseTestAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0025.fm8", "divide_by_zero");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_AbortRequest()
                {
					Assert::AreEqual(1, pBuiltinSupport->AbortRequestCallCount, "AbortRequest was not called during execution of divide_by_zero for .fm8");
                }
            };                  

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_that_does_cause_an_error_in_TestCase0025_fmA : BaseTestAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0025.fmA", "divide_by_zero");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
                [TestMethod, TestCategory("SimpleMethodTests")]
                void It_should_call_AbortRequest()
                {
 				   Assert::AreEqual(1, pBuiltinSupport->AbortRequestCallCount, "AbortRequest was not called during execution of divide_by_zero for fma");
                }
            };

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_in_TestCase0025_fm8 : BaseTestAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0025.fm8", "simple_scaling_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

            };

            [TestClass]
            public ref class When_calling_an_integer_scaling_method_in_TestCase0025_fmA : BaseTestAbortingScalingMethod
            {
            private:
                EVAL_VAR_VALUE *valueToTest;
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0025.fma", "simple_scaling_method");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }

            };

            [TestClass]
            public ref class When_calling_a_method_that_should_assign_a_date_value_in_TestCase0025_fmA
            {
            private:
                static BuiltinSupportSimulator *builtinSupport;
                static EddEngineController^ engine;
            public:
                [ClassInitialize]
                static void Setup(TestContext^ context)
                {

                    builtinSupport = new BuiltinSupportSimulator();

                    engine = gcnew EddEngineController("Hart\\TestCase002A.fmA");
                    engine->Initialize(nullptr, builtinSupport);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "simple_assign_of_date_values");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }
                [ClassCleanup]
                static void Cleanup()
                {
                    if (builtinSupport) delete builtinSupport;
                }

                [TestMethod]
                void The_provided_value_should_be_a_VT_EDD_DATE()
                {
                    EVAL_VAR_VALUE *providedValue = (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((int)nsEDDEngine::VariableType::VT_EDD_DATE, (int)providedValue->type);
                }

                [TestMethod]
                void The_provided_value_should_have_a_size_of_3()
                {
                    EVAL_VAR_VALUE *providedValue =  (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((unsigned int)3, providedValue->size);
                }

                [TestMethod]
                void The_provided_value_should_have_the_value_specified_in_method()
                {
                    EVAL_VAR_VALUE *providedValue = (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((__int64) 721522, providedValue->val.i);
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_should_assign_a_time_value_in_TestCase0025_fmA
            {
            private:
                static BuiltinSupportSimulator *builtinSupport;
                static EddEngineController^ engine;
            public:
                [ClassInitialize]
                static void Setup(TestContext^ context)
                {
                    builtinSupport = new BuiltinSupportSimulator();

                    engine = gcnew EddEngineController("Hart\\TestCase002A.fmA");
                    engine->Initialize(nullptr, builtinSupport);

                    FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "simple_assign_of_time_values");

                    int iBlockInstance = 0;
                    void* pValueSpec = 0;
                    nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                    void* asyncState = 0;

                    wchar_t* languageCode = L"en";
                    ACTION methodAction; 
                    methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
                    methodAction.action.meth_definition.size = wcslen(test_method->def.data);
                    methodAction.action.meth_definition.data = test_method->def.data;
                    nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                        iBlockInstance, 
                        pValueSpec, 
                        &methodAction, 
                        pCallback, 
                        asyncState,
                        languageCode);

                    engine->Instance->EndMethod(&asyncRunningMethod );

                    //this is not methodAction's data, please don't delete it
                    methodAction.action.meth_definition.data = nullptr;
                }
                [ClassCleanup]
                static void Cleanup()
                {
                    if (builtinSupport) delete builtinSupport;
                }

                [TestMethod]
                void The_provided_value_should_be_a_VT_TIME_VALUE()
                {
                    EVAL_VAR_VALUE *providedValue = (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((int)nsEDDEngine::VariableType::VT_TIME_VALUE, (int)providedValue->type);
                }

                [TestMethod]
                void The_provided_value_should_have_a_size_of_4()
                {
                    EVAL_VAR_VALUE *providedValue =  (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((unsigned int)4, providedValue->size);
                }

                [TestMethod]
                void The_provided_value_should_have_the_value_specified_in_method()
                {
                    EVAL_VAR_VALUE *providedValue = (EVAL_VAR_VALUE*)(builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue"));
                    Assert::AreEqual((unsigned __int64) 1441440000, providedValue->val.u);
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_starts_an_ParameterInputRequest_for_a_TIME_VALUE_in_TestCase002A_fmA :BaseClassParameterInputRequests
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedOrder = "ParameterInputRequest,OnMethodExiting";
                    String^ expectedParameter = "ParameterInputRequest(Please enter time information:),OnMethodExiting(0)";
                    String^ parameterName = "test_variable_time_value";
                    String^ methodName = "test_method_input_request_for_time_value";

                    SetupBase(
                        "Hart\\TestCase002A.fmA", 
                        methodName, 
                        parameterName,
                        expectedOrder,
                        expectedParameter);
                }

                [TestCleanup]
                void Cleanup()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_starts_an_ParameterInputRequest_for_a_DATE_in_TestCase002A_fmA :BaseClassParameterInputRequests
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedOrder = "ParameterInputRequest,OnMethodExiting";
                    String^ expectedParameter = "ParameterInputRequest(Please enter date information:),OnMethodExiting(0)";
                    String^ parameterName = "test_variable_date";
                    String^ methodName = "test_method_input_request_for_date";

                    SetupBase("Hart\\TestCase002A.fmA",
                        methodName, 
                        parameterName,
                        expectedOrder,
                        expectedParameter);
                }

                [TestCleanup]
                void Cleanup()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListInsert_fmA : BaseTestMethodList
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="add_to_list";
                    String^ expectedOrder="ListInsert,OnMethodExiting";
                    String^ expectedParameter="ListInsert(16402,0,16403),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fmA",methodName,expectedOrder,expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListInsert_fm8 : BaseTestMethodList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="add_to_list";
                    String^ expectedOrder="ListInsert,OnMethodExiting";
                    String^ expectedParameter="ListInsert(16402,0,16403),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fm8", methodName, expectedOrder, expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListDeleteElementAt_fmA : BaseTestMethodList
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="remove_from_list";
                    String^ expectedOrder="ListDeleteElementAt,OnMethodExiting";
                    String^ expectedParameter="ListDeleteElementAt(16394,0,1),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fmA",methodName,expectedOrder,expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListDeleteElementAt_fm8 : BaseTestMethodList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="remove_from_list";
                    String^ expectedOrder="ListDeleteElementAt,OnMethodExiting";
                    String^ expectedParameter="ListDeleteElementAt(16394,0,1),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fm8", methodName, expectedOrder, expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListCount_fmA : BaseTestMethodList
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="check_list_count";
                    String^ expectedOrder="GetDynamicAttribute,OnMethodExiting";
                    String^ expectedParameter="GetDynamicAttribute(16402,45),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fmA",methodName,expectedOrder,expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListCount_fm8 : BaseTestMethodList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="check_list_count";
                    String^ expectedOrder="GetDynamicAttribute,OnMethodExiting";
                    String^ expectedParameter="GetDynamicAttribute(16402,45),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fm8", methodName, expectedOrder, expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListInsert_and_Count_fmA : BaseTestMethodList
            {

            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="add_to_the_list_end";
                    String^ expectedOrder="GetDynamicAttribute,ListInsert,OnMethodExiting";
                    String^ expectedParameter="GetDynamicAttribute(16394,45),ListInsert(16394,0,16404),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fmA",methodName,expectedOrder,expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_calls_ListInsert_and_Count_fm8 : BaseTestMethodList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ methodName="add_to_the_list_end";
                    String^ expectedOrder="GetDynamicAttribute,ListInsert,OnMethodExiting";
                    String^ expectedParameter="GetDynamicAttribute(16394,45),ListInsert(16394,0,16404),OnMethodExiting(0)";
                    SetupBase("Hart\\TestCase0031.fm8", methodName, expectedOrder, expectedParameter);
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_accesses_item_array_elements
            {
            private:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator *builtinHelper;
                static bool crashed;
				static nsEDDEngine::Mth_ErrorCode methodSuccess;
                
            public:
                [ClassInitialize]
                static void Setup(TestContext^ context)
                {
                    crashed = false;
                    methodSuccess = METH_FAILED;
                    try
                    {
                        builtinHelper = new BuiltinSupportSimulator();
                        IParamCache* paramCache = new ParamCacheSimulator();
                        
                        engine = gcnew EddEngineController("Hart\\TestCase0036.fmA");
                        engine->Initialize(paramCache, builtinHelper);

                        FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "write_to_item_array");

                        int iBlockInstance = 0;
                        void* pValueSpec = 0;
                        nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                        void* asyncState = 0;

                        wchar_t* languageCode = L"en";
                        ACTION methodAction; 
                        methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                        methodAction.action.meth_ref = test_method->id;
                        nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                            iBlockInstance, 
                            pValueSpec, 
                            &methodAction, 
                            pCallback, 
                            asyncState,
                            languageCode);

                        methodSuccess = engine->Instance->EndMethod(&asyncRunningMethod );
                    }
                    catch(...)
                    {
                        crashed = true;
                    }
                }

                [ClassCleanup]
                static void Cleanup()
                {
                    if(builtinHelper) delete builtinHelper;
                }

                [TestMethod]
                void It_should_not_crash()
                {
                    Assert::IsFalse(crashed);
                }

                [TestMethod]
                void It_should_finish_the_method_successfully()
                {
                    Assert::IsTrue(methodSuccess == METH_SUCCESS);
                }

                [TestMethod]
                void It_should_not_call_GetParamValue()
                {
                    Assert::AreEqual(0, builtinHelper->GetParamValueCallCount);
                }

                [TestMethod]
                void It_should_call_SetParamValue_twice()
                {
                    Assert::AreEqual(2, builtinHelper->SetParamValueCallCount);
                }
            };

            [TestClass]
            public ref class When_calling_a_method_that_accesses_FILE_members
            {
            private:
                static EddEngineController^ engine;
                static BuiltinSupportSimulator *builtinHelper;
                static bool crashed;
				static nsEDDEngine::Mth_ErrorCode methodSuccess;

            public:
                [ClassInitialize]
                static void Setup(TestContext^ context)
                {
                    crashed = false;
                    methodSuccess = METH_FAILED;
                    try
                    {
                        builtinHelper = new BuiltinSupportSimulator();
                        IParamCache* paramCache = new ParamCacheSimulator();
                        engine = gcnew EddEngineController("Hart\\TestCase0036.fmA");
                        engine->Initialize(paramCache, builtinHelper);

                        FLAT_METHOD* test_method = MethodHelper::GetMethod(engine, "read_file_members");

                        int iBlockInstance = 0;
                        void* pValueSpec = 0;
                        nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                        void* asyncState = 0;

                        wchar_t* languageCode = L"en";
                        ACTION methodAction; 
                        methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                        methodAction.action.meth_ref = test_method->id;
                        nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                            iBlockInstance, 
                            pValueSpec, 
                            &methodAction, 
                            pCallback, 
                            asyncState,
                            languageCode);

                        methodSuccess = engine->Instance->EndMethod(&asyncRunningMethod );
                    }
                    catch(...)
                    {
                        crashed = true;
                    }
                }

                [ClassCleanup]
                static void Cleanup()
                {
                    if(builtinHelper) delete builtinHelper;
                }

                [TestMethod]
                void It_should_not_crash()
                {
                    Assert::IsFalse(crashed);
                }

                [TestMethod]
                void It_should_finish_the_method_successfully()
                {
                    Assert::IsTrue(methodSuccess == METH_SUCCESS);
                }

                [TestMethod]
                void It_should_call_GetParamValue_4_times()
                {
                    Assert::AreEqual(4, builtinHelper->GetParamValueCallCount);
                }

                [TestMethod]
                void It_should_call_AcknowledgementRequest_once()
                {
                    Assert::AreEqual(1, builtinHelper->AcknowledgementRequestCallCount);
                }
            };

			[TestClass, Ignore]
			public ref class BaseClassCurrentTime
			{
			private:
				static BuiltinSupportSimulator *builtinSupport;
				static EddEngineController^ engine;
				static EVAL_VAR_VALUE *expectedSet, *providedValue;
				static String^ theMethodName;

			public:
				static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE expectedValue)
				{
					theMethodName = methodName;

					expectedSet = new EVAL_VAR_VALUE(expectedValue);
					builtinSupport = new BuiltinSupportSimulator();
					builtinSupport->SetParamValueReturnValue = PC_SUCCESS_EC;

					engine = gcnew EddEngineController(eddFileName);
					engine->Initialize(nullptr, builtinSupport);

					FDI_PARAM_SPECIFIER test_method_specifier;
					engine->FillParamSpecifier(methodName, &test_method_specifier);

					int iBlockInstance = 0;
					void* pValueSpec = 0;
					nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
					void* asyncState = 0;

					wchar_t* languageCode = L"en";
					ACTION methodAction; 
					methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
					methodAction.action.meth_ref = test_method_specifier.id;
					nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
						iBlockInstance, 
						pValueSpec, 
						&methodAction, 
						pCallback, 
						asyncState,
						languageCode);

					engine->Instance->EndMethod(&asyncRunningMethod);
					providedValue = (EVAL_VAR_VALUE*)builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue");
				}

				static void CleanupBase()
				{
					if (builtinSupport) delete builtinSupport;
					if (engine) delete engine;
					if (expectedSet) delete expectedSet;
				}

				[TestMethod]
				void The_provided_value_for_SetParamValue_should_have_the_expected_type()
				{
					Assert::AreEqual((int)expectedSet->type, (int)providedValue->type);
				}

				[TestMethod]
				void The_provided_value_for_SetParamValue_should_have_the_expected_size()
				{
					Assert::AreEqual(expectedSet->size, providedValue->size);
				}

				[TestMethod]
				void The_provided_value_for_SetParamValue_should_have_the_expected_value()
				{
					int expectedVal = (int)expectedSet->val.i;
					int returnedVal = (int)providedValue->val.i;
					bool bCheck = ((returnedVal - expectedVal) <= 1);
					Assert::IsTrue(bCheck, ("DD test method " + theMethodName + " failed."));
				}
			};

		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentTime_in_Testcase_002A : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
				__time32_t	ltime;
				_time32( &ltime );

				struct tm tm_value;
				_gmtime32_s(&tm_value, &ltime);

				// current time in second
				long curr_time =  (tm_value.tm_hour * 60 + tm_value.tm_min) * 60 + tm_value.tm_sec;

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_time;

				SetupBase("Hart\\TestCase002A.fmA", "meth_current_time", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

		[TestClass]
		public ref class When_calling_a_method_that_tests_From_TIME_VALUE_in_Testcase_002A : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				/* calculation for result of From_TIME_VALUE builtin */
				unsigned long time_value = 0x290390;
				ltime += (__time32_t)((double)time_value / 32000);

				// current time_value
				long curr_time_val = make_time_t( ltime );

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_time_val;

				SetupBase("Hart\\TestCase002A.fmA", "meth_from_time_value", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};
			
      }
    }
}

