#ifndef NAN_DEF_H
#define NAN_DEF_H

//
// Define the largest single and double values.
//

#define SINGLE_MAXIMUM_VALUE 0x7f7fffff

#define DOUBLE_MAXIMUM_VALUE_HIGH 0x7fefffff
#define DOUBLE_MAXIMUM_VALUE_LOW 0xffffffff

//
// Define single and double quiet and signaling Nan values
// (these are identical to X86 formats; Mips is different).
//

#define SINGLE_QUIET_NAN_PREFIX 0x7fc00000
#define SINGLE_SIGNAL_NAN_PREFIX 0x7f800000
#define SINGLE_QUIET_NAN_VALUE 0xffc00000
/* the  NaN Signaling Infinity divided by Infinity is the HART NAN!! stevev 11jul05 */
#define SINGLE_INF_DIVBY_INF_NAN (SINGLE_SIGNAL_NAN_PREFIX | 0x00200000) 
#define HART_NAN      SINGLE_INF_DIVBY_INF_NAN

#define DOUBLE_QUIET_NAN_PREFIX_HIGH 0x7ff80000
#define DOUBLE_SIGNAL_NAN_PREFIX_HIGH 0x7ff00000
#define DOUBLE_QUIET_NAN_VALUE_HIGH 0xfff80000
#define DOUBLE_QUIET_NAN_VALUE_LOW 0x0

//
// Define positive single and double infinity values.
//

#define SINGLE_INFINITY_VALUE 0x7f800000

#define DOUBLE_INFINITY_VALUE_HIGH 0x7ff00000
#define DOUBLE_INFINITY_VALUE_LOW 0x0

//
// Quadword versions of the above.
//

#define DOUBLE_MAXIMUM_VALUE        ((ULONGLONG)0x7fefffffffffffff)
#define DOUBLE_INFINITY_VALUE       ((ULONGLONG)0x7ff0000000000000)
#define DOUBLE_QUIET_NAN_VALUE      ((ULONGLONG)0xfff8000000000000)
#define DOUBLE_POS_QUIET_NAN_VALUE      (0x7ff8000000000000)
#define DOUBLE_POS_SIGNAL_NAN_VALUE     (0x7ff4000000000000)

#define EXP_MASK	(0x7ff0000000000000)
#define FRAC_MASK	(0x000fffffffffffff)
#define QUIET_MASK	(0x0008000000000000)

#endif /* NAN_DEF_H */