#include "stdafx.h"
#include "ddbDevice.h"
#include "MEE.h"
#include <3rdParty/panic.h>
#include "BuiltInMapper.h"
#include "varientTag.h"

// Component History: 
// 
// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version
//
// 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
//
// WHS EP June of 2008 - huge rework of function GetDDVarValNEvalAssType()
//
//////////////////////////////////////////////////////////////////////////////////////////////////////


INTERPRETER_STATUS OneMeth::ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId)
{
	if (pDevice == NULL)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	m_pMEE = pMEE; //Initialize the MEE pointer;

	m_pDevice = pDevice;

	INTERPRETER_STATUS intStatus = ExecMethod(lMethodItemId);

	if (m_bMethodAborted == true)
	{

		for( ListIterator = abortMethodList.begin();ListIterator != abortMethodList.end();ListIterator++ )
		{
			//when running abort method, turn off cancel signal
			m_pDevice->ClearDeviceCancel();

			long lQueueMethodId = *ListIterator;
			INTERPRETER_STATUS intAbortListStatus = ExecMethod(lQueueMethodId);
			if (intAbortListStatus != INTERPRETER_STATUS_OK)
			{
				m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"Abort Method Failed\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


				if (intStatus != INTERPRETER_STATUS_EXECUTION_CANCEL)
				{
					intStatus = intAbortListStatus;
				}
				break;
			}
			else
			{
				m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"Abort Method Passed\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");	
			}
		}

		m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"Method Process Aborted\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


		//After abort list execution is done and the original method return code is OKay, mark the return code to be aborted here
		if (intStatus == INTERPRETER_STATUS_OK)
		{
			intStatus = INTERPRETER_STATUS_EXECUTION_ABORT;
		}
	}

	return intStatus;
}

int OneMeth::process_abort()
{
	m_bMethodAborted = true;
	return 0;
}

int OneMeth::_add_abort_method(long lMethodId)
{
	int nRetVal = BI_ERROR;

	//Set the maximum method list to 20
	if(abortMethodList.size() < MAX_METHOD_LIST ){

		abortMethodList.push_back(lMethodId);
		nRetVal = BI_SUCCESS;
	}
	
	return nRetVal;

}

int OneMeth::_remove_abort_method(long lMethodId)
{
	int rc = BI_ERROR;
	if(GetMethodAbortStatus() == false)
	{
		ListIterator = abortMethodList.begin();
		while (ListIterator != abortMethodList.end())
		{
			long lQueueMethodId = *ListIterator;
			if (lQueueMethodId == lMethodId)
			{
				abortMethodList.erase(ListIterator);
				ListIterator = abortMethodList.begin();
				rc = BI_SUCCESS;
				break;/*Vibhor 030305: Bug Fix : As per the definition in the spec it should only 
					  remove the first occurence of a method in the abort list
					  So,loop no further after the first one !!*/
			}
			else
			{
				ListIterator++;
			}
		}
	}
	return rc;
}

int OneMeth::remove_all_abort()
{
	int rc = BI_ERROR;
	if(GetMethodAbortStatus() == false)
	{
		if( !abortMethodList.empty() ) 
		{ 
			abortMethodList.clear(); 
			rc = BI_SUCCESS;
		} 
	}
	return rc;
}


bool OneMeth::GetMethodAbortStatus()
{
	return m_bMethodAborted;	
}

int OneMeth::_push_abort_method(long lMethodId)
{
	int nRetVal = BI_ERROR;

	//Set the maximum method list to 20
	if(abortMethodList.size() < MAX_METHOD_LIST ){

		abortMethodList.push_front(lMethodId);
		nRetVal = BI_SUCCESS	;
	}
	
	return nRetVal;
}

int OneMeth::_pop_abort_method()
{
	if(GetMethodAbortStatus() == false)
	{
		if(abortMethodList.size() !=0)
		{
			abortMethodList.pop_front();
			return BI_SUCCESS;
		}
	}
	return BI_ERROR;
}

void OneMeth::AllocLibrary()
{
	FreeLibrary();
	m_pInterpreter = new CInterpreter;
	m_pBuiltinLib = CBuiltIn::ConstructChild(GetStartedProtocol());

	if ((m_pInterpreter == NULL) || (m_pBuiltinLib == NULL))
	{
		LOGIT(CERR_LOG,"OneMeth::AllocLibrary() Allocation Failed.");
		FreeLibrary();
		return;
	}

	m_pMEE->SetInterpreterPtr(m_pInterpreter);
	return;
}

void OneMeth::FreeLibrary()
{
	if (m_pInterpreter != NULL)
	{
		delete m_pInterpreter;
		m_pInterpreter = NULL;
	}
	if (m_pBuiltinLib != NULL)
	{
		delete m_pBuiltinLib;
		m_pBuiltinLib = NULL;
	}
}

INTERPRETER_STATUS	OneMeth::ExecMethod(long lMethodItemId)
{
	/* First check if the Item Id is valid */
	hCitemBase*  p_ib = NULL;
	RETURNCODE rc = m_pDevice->getItemBySymNumber(lMethodItemId, &p_ib);
	if (!(rc == SUCCESS && p_ib != NULL && p_ib->getIType() == iT_Method))
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	if (m_lMethodItemId != lMethodItemId)
	{
		m_lMethodItemId = lMethodItemId;
		if (m_pszDefinition != NULL)
		{
			delete [] m_pszDefinition;
			m_pszDefinition = NULL;
		}
	}

	wchar_t *wOrgMethDef = NULL;	//remember to release the memory

	hCmethod*	p_m = (hCmethod*)p_ib;
	size_t   sLen = p_m->getDef(m_pszDefinition, wOrgMethDef);

	if ((sLen == 0) || (m_pszDefinition == NULL))
	{
		if (wOrgMethDef != NULL)
		{
			delete [] wOrgMethDef;
			wOrgMethDef = NULL;
		}
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	//save method definition into log file
	//Opens an empty file for writing. If the given file exists, its contents are destroyed.
	
	
	{
		//definition string used for searched by literal_string
		size_t defLen = wcslen(wOrgMethDef);
		wchar_t *sLogSearchPtr = wOrgMethDef;

		//adjusted definition used for printed into log file
		wchar_t *wNewMethDef = new wchar_t[2*defLen];	//make enough memory for new method definition
		PS_Wcscpy(wNewMethDef, 2*defLen, wOrgMethDef);
		wchar_t *pNewDefPtr = wNewMethDef;

		while (sLogSearchPtr)
		{
			wchar_t *sFoundPtr = wcsstr(sLogSearchPtr, _T("literal_string"));
			if (sFoundPtr != NULL)
			{
				//found
				size_t i = (sFoundPtr - sLogSearchPtr + strlen("literal_string"));
				pNewDefPtr += i;
				do
				{
					if (sLogSearchPtr[i] == _T('('))
					{
						//overwrite the literal_string argument with "xxx"
						wcsncpy(pNewDefPtr, _T("(xxx)"), 5);
						pNewDefPtr += 5;
					}
				} while (sLogSearchPtr[i++] != _T(')'));

				sLogSearchPtr += (i);
				//overwrite the rest of method definition
				wcscpy(pNewDefPtr, sLogSearchPtr);
			}
			else
			{
				//overwrite the rest of method definition
				wcscpy(pNewDefPtr, sLogSearchPtr);
				break;
			}
		}

		m_pDevice->MILog(L"<Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(wNewMethDef, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"</Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"<Function>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


		if (wNewMethDef)
		{
			delete [] wNewMethDef;
		}

	}

	if (wOrgMethDef)
	{
		delete [] wOrgMethDef;
	}

	char pErrorData[RULENAMELEN] = "";
	char *pbySymbolTable = NULL;
	INTERPRETER_STATUS intStatus = INTERPRETER_STATUS_INVALID;

	try
	{
		//check if method return type is defined
		if (CheckMethodReturnedVariable(p_m) != SUCCESS)
		{
			return INTERPRETER_STATUS_EXECUTION_ERROR;
		}

		AllocLibrary();
		m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,this);
	//	char pchCode[] = {"{float tmp;long lValue[3];tmp=2.8;lValue[0] = 150;lValue[1] = 151;lValue[2] = 152;acknowledge(\"Test string%{tmp} and val is %{0}\", lValue);}"};

		m_pMEE->methodNameString = p_m->getName();

		intStatus =	m_pInterpreter->ExecuteCode (m_pBuiltinLib, m_pszDefinition, "Test", pErrorData, pbySymbolTable, m_pMEE);

		m_pDevice->MILog(L"</Function>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		
		if (pbySymbolTable != NULL)
		{
			wchar_t *wStr = c2w(pbySymbolTable);
			//wchar_t* wStrMessage = new wchar_t[wcslen(wStr) + 2];
			//PS_VsnwPrintf(wStrMessage, wcslen(wStr) + 2, L"%s\n", wStr);
			m_pDevice->MILog(wStr, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");

			if (wStr != NULL)
			{
				delete [] wStr;
			}
			//if (wStrMessage != NULL)
			//{
			//	delete[] wStrMessage;
			//}

		}
		else
		{
			m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		}
		

		if (intStatus != INTERPRETER_STATUS_OK)
		{
			DisplayMessageAndAbort(pErrorData);
		}
	}
	catch(_INT32 error)
	{
		if (error == C_UM_ERROR_METH_CANCELLED)
		{
			intStatus = INTERPRETER_STATUS_EXECUTION_CANCEL;
		}
		else
		{
			intStatus = (INTERPRETER_STATUS)error;
		}
	}

	if (pbySymbolTable != NULL)
	{
		delete [] pbySymbolTable;
	}

	FreeLibrary();
	return intStatus;
}


void OneMeth::send_on_exit(void)
{
	m_pMEE->m_eOnExitActivity = nsEDDEngine::Send;
}

void OneMeth::save_on_exit(void)
{
	m_pMEE->m_eOnExitActivity = nsEDDEngine::Save;
}

void OneMeth::discard_on_exit(void)
{
	m_pMEE->m_eOnExitActivity = nsEDDEngine::Discard;
}

//This function is used to display error message
void OneMeth::DisplayMessageAndAbort(char *pchMessage)
{
	if (pchMessage[0] != '\0')
	{
		//if method hasn't been aborted, abort it here
		size_t convertedChars = 0;
		size_t newsize = strlen(pchMessage) + 1;
		tchar wcstring[MAX_DD_STRING];
		PS_Mbtowcs(&convertedChars, wcstring, newsize, pchMessage, _TRUNCATE);

		m_pBuiltinLib->abort(wcstring);
	}
	else
	{
		//mark m_bMethodAborted to true for aborted sub-method
		process_abort();
	}
	return;
}


/****************************************************************************************************************************** 
 * Function UpdateGlobalToken() 
 * updates the device variable entry with inputs in global symbol table
 *
 * Note:
 * This function is used by HART and Profibus only
 *
 * Inputs:
 *	ulProxy: device variable reference proxy which
 *			 the most left byte is 0xFF and the rest of bytes is index of the device variable in global symbol table
 *	ulMemberId: device variable member ID.
 *
 * Output:
 *	output is device variable reference proxy if the function succeeds 
 *  Otherwise, output is 0, if the function fails.
 *****************************************************************************************************************************/
unsigned long OneMeth::UpdateGlobalToken(unsigned long ulProxy, ITEM_ID ulMemberId)
{
	unsigned long	ulRefProxy = 0;

	if (m_pMEE && ((ulProxy & REF_PROXY_MASK) == REF_PROXY_MASK))
	{
		//it is Hart device variable reference proxy
		//Get device variable entry in global symbol table
		CVariable *pCVariable = m_pMEE->m_GlobalSymTable.GetAt(ulProxy & REF_PROXY_BITS);
		if (pCVariable)
		{
			PARAM_REF *pOpRef = pCVariable->GetParamRef();
			if (pOpRef)
			{
				//retrieve item ID
				ITEM_ID ulLastItemId = 0, ulLastMemberId = 0;		//item ID and member ID of the last layer in complex reference

				if (pOpRef->op_ref_type == STANDARD_TYPE)
				{
					ulLastItemId = pOpRef->op_info.id;
					ulLastMemberId = pOpRef->op_info.member;
				}
				else
				{
					ulLastItemId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
					ulLastMemberId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].member;
				}

				//get item ID
				CValueVarient vtIndirItemId{};
				if (m_pDevice->ReadParameterData2(&vtIndirItemId, _T("ItemID"), 0, 0, ulLastItemId, ulLastMemberId) == BLTIN_SUCCESS)
				{
					
					ITEM_ID ulItemId = CV_UI4(&vtIndirItemId);

					//Get the item type
					CValueVarient vtItemType{};
					(void) m_pDevice->ReadParameterData2(&vtItemType, _T("Kind"), 0, 0, ulItemId, 0);
					
					itemType_e eItemtype = m_pDevice->GetItemType(CV_I4(&vtItemType));

					//before calling addOperationalID(), adjust value array and list index and update operative item ID
					switch (eItemtype)
					{
					case iT_Array:
					case iT_List:
						if ( m_startedProtocol != nsEDDEngine::FF )
						{
							pOpRef->addOperationalID(ulItemId, ulMemberId+1, eItemtype);
						}
						else
						{
							pOpRef->addOperationalID(ulItemId, ulMemberId, eItemtype);
						}

						//return the same reference proxy
						ulRefProxy = ulProxy;
						break;
					case iT_Record:
					case iT_File:
					case iT_ItemArray:
					case iT_Collection:
					case iT_VariableList:
						pOpRef->addOperationalID(ulItemId, ulMemberId, eItemtype);

						//return the same reference proxy
						ulRefProxy = ulProxy;
						break;
					default:
						break;	//error
					}	//end of switch
				}
			}	//pOpRef
		}
	}

	return ulRefProxy;
}

/****************************************************************************************************************************** 
 * Function InsertGlobalToken() 
 * returns resolved item ID if the input item is indirection item;
 * or inserts a new device variable entry into global symbol table if inputs are real item ID and member ID.
 *
 * Note:
 * This function is used by HART and Profibus only
 * Device variable reference proxy consists of the most left byte is 0xFF and the rest of bytes is index of 
 * the device variable entry in global symbol table.
 *
 * Inputs:
 *	ulItemId: device variable item ID
 *	ulMemberId: device variable member ID
 *
 * Output:
 *	output is resolved indirection item ID or device variable reference proxy if the function succeeds 
 *  Otherwise, output is 0, if the function fails.
 *****************************************************************************************************************************/
unsigned long OneMeth::InsertGlobalToken(unsigned long ulBlockId, unsigned long ulBlockInst, ITEM_ID ulItemId, ITEM_ID ulMemberId)
{
	unsigned long	ulRetValue = 0;

	if (m_pDevice && ((ulItemId & REF_PROXY_MASK) != REF_PROXY_MASK))
	{
		//Get the item type first withe the given item ID
		CValueVarient vtItemType{};
		if (m_pDevice->ReadParameterData2(&vtItemType, _T("Kind"), ulBlockId, ulBlockInst, ulItemId, 0) == BLTIN_SUCCESS)
		{
			
			itemType_e eItemtype = m_pDevice->GetItemType(CV_I4(&vtItemType));

			//Generate a new token
			RUL_TOKEN_TYPE		tokenType = RUL_SIMPLE_VARIABLE; 
			RUL_TOKEN_SUBTYPE	tokenSubType = RUL_SUBTYPE_NONE;

			switch(eItemtype)
			{
			case iT_Record:
			case iT_Array:
			case iT_List:
				{	
					//its a non-variable DD item
					tokenType = RUL_DD_ITEM;			
					tokenSubType = RUL_DD_COMPLEX;			
				}
				break;
			case iT_File:
			case iT_ItemArray:
			case iT_Collection:
			case iT_VariableList:
				{	
					//it's an indirection DD item
					/* resolve indirection item ID with the given item ID and member ID */
					CValueVarient vtIndirItemId{};
					if (m_pDevice->ReadParameterData2(&vtIndirItemId, _T("ItemID"), ulBlockId, ulBlockInst, ulItemId, ulMemberId) == BLTIN_SUCCESS)
					{
						
						ulItemId = CV_UI4(&vtIndirItemId);
					}
					else
					{
						ulItemId = 0;	//error
					}
				}
				break;
			default:
				break;
			}

			if (tokenSubType == RUL_DD_COMPLEX)
			{
				string strTokenName = "Reference Proxy ";
				char itemBuff[9];	//buffer for item ID or member ID in string. size 9 = 8 + 1

                itoa(ulItemId, itemBuff, 16);
				strTokenName += itemBuff;						//attach item ID to a general reference proxy name
				strTokenName += ", ";
                itoa(ulMemberId, itemBuff, 16);
				strTokenName += itemBuff;						//attach member ID to a general reference proxy name
				CToken localToken(strTokenName.c_str(), tokenType, tokenSubType, 0);
				localToken.m_bIsGlobal = true;

				//before calling addOperationalID(), adjust value array and list index
				PARAM_REF opRef;
				switch (eItemtype)
				{
				case iT_Array:
				case iT_List:
					if ( m_startedProtocol != nsEDDEngine::FF )
					{
						opRef.addOperationalID(ulItemId, ulMemberId+1, eItemtype);
					}
					else
					{
						//shouldn't be here. but in case...
						opRef.addOperationalID(ulItemId, ulMemberId, eItemtype);
					}
					break;
				case iT_Record:
					opRef.addOperationalID(ulItemId, ulMemberId, eItemtype);
					break;
				default:
					break;	//error
				}

				if (opRef.isOperationalIDValid())
				{
					opRef.addBlockID(ulBlockId, ulBlockInst);

					localToken.SetParamRef(&opRef);

					//insert the new token into global symbal table
					long lRetInd = m_pMEE->m_GlobalSymTable.Insert(localToken);//SymbolTable::Insert makes a copy of the token
	
					if (lRetInd != -1)
					{
						ulRetValue = (unsigned long)lRetInd | REF_PROXY_MASK;		//reference proxy
					}
				}
			}
			else
			{
				//indirection item ID or error
				ulRetValue = ulItemId;
			}
		}
	}

	return ulRetValue;
}

/****************************************************************************************************************************** 
 * Function ReturnOrInsertGlobalToken() 
 * returns resolved item ID if the input item is indirection item;
 * or inserts a new device variable entry into global symbol table if inputs are real item ID and member ID;
 * or updates the device variable entry with inputs in global symbol table if the first input is device reference proxy.
 *
 * Note:
 * This function is used by HART and Profibus only
 * Device variable reference proxy consists of the most left byte is 0xFF and the rest of bytes is index of 
 * the device variable entry in global symbol table.
 *
 * Inputs:
 *	ulItemId: device variable item ID or Hart device variable reference proxy which
 *			  the most left byte is 0xFF and the rest of bytes is index of the device variable in global symbol table
 *	ulMemberId: device variable member ID.
 *
 * Output:
 *	output is resolved indirection item ID or device variable reference proxy if the function succeeds 
 *  Otherwise, output is 0, if the function fails.
 *****************************************************************************************************************************/
unsigned long OneMeth::ReturnOrInsertGlobalToken(unsigned long ulBlockId, unsigned long ulBlockInst, unsigned long ulItemId, ITEM_ID ulMemberId)
{
	unsigned long	ulRetValue = 0;

	//check if it is Hart device variable reference proxy
	if ((ulItemId & REF_PROXY_MASK) == REF_PROXY_MASK)
	{
		//it is device variable reference proxy
		ulRetValue = UpdateGlobalToken(ulItemId, ulMemberId);
	}
	else
	{
		//it is not device variable reference proxy
		ulRetValue = InsertGlobalToken(ulBlockId, ulBlockInst, ulItemId, ulMemberId);
	}

	return ulRetValue;
}

/****************************************************************************************************************************** 
 * This function returns pointer of token operative reference found in global symbol table by the given index.
 * If the operative member ID is not zero, resolve the member ID and update operative reference.
 * This function is used by HART and Profibus only
 *
 * Input 'ulProxy' is the device variable reference proxy.
*******************************************************************************************************************************/
PARAM_REF* OneMeth::GetGlobalTokenOperative(const unsigned long ulProxy)
{
	if ((ulProxy & REF_PROXY_MASK) != REF_PROXY_MASK)
	{
		return nullptr;	//error
	}

	PARAM_REF* pOpRef = nullptr;

	CVariable *pCVariable = m_pMEE->m_GlobalSymTable.GetAt(ulProxy & REF_PROXY_BITS);
	if (pCVariable)
	{
		pOpRef = pCVariable->GetParamRef();

		if (pOpRef && pOpRef->isOperationalIDValid())
		{
			//get the last item ID, member ID and item type
			ITEM_ID ulItemId = 0, ulMemberId = 0;

			if (pOpRef->op_ref_type == STANDARD_TYPE)
			{
				ulItemId = pOpRef->op_info.id;
				ulMemberId = pOpRef->op_info.member;
			}
			else
			{
				ulItemId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
				ulMemberId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].member;
			}

			if (ulMemberId != 0)
			{
				//resolve the variable member to simple variable
				CValueVarient vtItemId{};
				if (m_pDevice->ReadParameterData2( &vtItemId, _T("ItemID"), pOpRef->ulBlockItemId, pOpRef->ulOccurrence, ulItemId, ulMemberId ) == BLTIN_SUCCESS)
				{
					
					ulItemId = CV_UI4(&vtItemId);
					ulMemberId = 0;

					// get item type again
					CValueVarient vtItemType{};
					(void)m_pDevice->ReadParameterData2( &vtItemType, _T("Kind"), pOpRef->ulBlockItemId, pOpRef->ulOccurrence, ulItemId, 0 );
					
					itemType_t itemType = m_pDevice->GetItemType(CV_I4(&vtItemType));
					
					//update PARAM_REF
					pOpRef->addOperationalID(ulItemId, ulMemberId, itemType);
				}
				else
				{
					pOpRef = nullptr;
				}
			}
		}
	}

	return pOpRef;
}

INTERPRETER_STATUS MEE ::ExecuteMethod(hCddbDevice *pDevice,long lMethodItemId)
{
	m_pDevice = pDevice; //Anil 12oct05: Vibhor, you missed this one too !!!

	OneMeth *pMeth = new OneMeth();
	IncreaseOneMethRefNo();

	//propagate protocol type from MEE to OneMeth instance
	pMeth->SetStartedProtocol(GetStartedProtocol());
	INTERPRETER_STATUS intStatus = INTERPRETER_STATUS_EXECUTION_ERROR;

	try
	{
		intStatus = pMeth->ExecuteMethod(this,pDevice,lMethodItemId);
	}
	catch(_INT32 error)
	{
		if (error == C_UM_ERROR_METH_CANCELLED)
		{
			intStatus = INTERPRETER_STATUS_EXECUTION_CANCEL;
		}
		else
		{
			intStatus = (INTERPRETER_STATUS)error;
		}
	}

	DecreaseOneMethRefNo();
	delete pMeth;

	return intStatus;
}/*End ExecuteMethod*/


//ExecuteRefreshActionsInMethod executes refresh actions associated with a device variable 
//right before the variable value is displayed in a UI builtin prompt
int OneMeth::ExecuteRefreshActionsInMethod(/*in*/ hCVar *pVar, /*in, out*/ INTER_VARIANT *pActionVal)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if ((pVar == nullptr) || (GetStartedProtocol() != nsEDDEngine::HART))
	{
		//error check
		return iRetVal;
	}

	//Get refresh action list first
	nsEDDEngine::ACTION_LIST actionList;
	iRetVal = m_pDevice->ReadParameterActionList( 0, 0, pVar->getID(), &actionList);

	if ( (iRetVal == BLTIN_SUCCESS) && (actionList.count > 0) )
	{
		//Read the original ACTION and action value from MI engine. Save them here until the refresh action list is done
		nsEDDEngine::ACTION origAction;
		nsConsumer::EVAL_VAR_VALUE origActionVal;
		iRetVal = m_pDevice->devAccessActionVar(&origAction, &origActionVal, READ);

		if (iRetVal == BLTIN_SUCCESS)
		{
			INTERPRETER_STATUS intStatus = INTERPRETER_STATUS_INVALID;

			nsConsumer::EVAL_VAR_VALUE evvActionVal;
			pActionVal->CopyTo(&evvActionVal);

			//execute refresh actoin method
			for (int i = 0; i < actionList.count; i++)
			{
				//write the current ACTION and action value to MI engine
				if (i == 0)
				{
					iRetVal = m_pDevice->devAccessActionVar(&(actionList.list[i]), &evvActionVal, WRITE);
				}
				else
				{
					iRetVal = m_pDevice->devAccessActionVar(&(actionList.list[i]), NULL, WRITE);
				}

				if (iRetVal == BLTIN_SUCCESS)
				{
					//execute refresh action method in another OneMeth instance with the same m_pDevice and m_pMEE
					intStatus = m_pMEE->ExecuteMethod(m_pDevice, EMBEDDED_ACTION_ID);
					
					//recover the original CInterpreter pointer as the current CInterpreter instance to MEE instance
					m_pMEE->SetInterpreterPtr(m_pInterpreter);

					if (intStatus != INTERPRETER_STATUS_OK)
					{
						break;
					}
				}
			}

			if (intStatus != INTERPRETER_STATUS_OK)
			{
				if (iRetVal == BLTIN_SUCCESS)		//only over-write iRetVal if it doesn't already contain an error code
				{
					iRetVal = BLTIN_CANNOT_READ_VARIABLE;	//actually bad refresh action method execution, maybe aborted
				}
			}

			if (iRetVal == BLTIN_SUCCESS)		//update output action value
			{			
				//read the final action value from MI Engine
				(void)m_pDevice->devAccessActionVar(NULL, &evvActionVal, READ);
				*pActionVal = INTER_VARIANT::InitFrom(&evvActionVal);
			}
			else
			{
				pActionVal->Clear();
			}

			//restore the original ACTION and action value back to MI engine
			(void)m_pDevice->devAccessActionVar(&origAction, &origActionVal, WRITE);
		}
	}
	else if (actionList.count == 0)
	{
		iRetVal = BLTIN_NOT_IMPLEMENTED;
	}

	return iRetVal;
}


long MEE::FindGlobalToken(const _CHAR* pszTokenName, const char* szFullLexeme, long lLineNum, DD_ITEM_TYPE &DDitemType)
{
	RUL_TOKEN_TYPE		tokenType = RUL_SIMPLE_VARIABLE; 
	RUL_TOKEN_SUBTYPE  tokenSubType = RUL_SUBTYPE_NONE;

	DDitemType = DD_ITEM_VAR;
	_INT32 nIndx = -1;
		
	nIndx = m_GlobalSymTable.GetIndex(szFullLexeme);

	//if not already there in the GlobalSymbolTable, then search the device
	if(-1 == nIndx) 
	{
		hCitemBase*  pIB = NULL;
		string strTokenName = pszTokenName;
		itemType_t itemtype = iT_ReservedZeta;

		//check if the token name is one of the following special names.
		if (strTokenName == "BLOCK")
		{
			itemtype = iT_VariableList;
		}
		else if (strTokenName == "PARAM")
		{
			itemtype = iT_VariableList;
		}
		else if (strTokenName == "LOCAL_PARAM")
		{
			itemtype = iT_VariableList;
		}
		else if (strTokenName == "PARAM_LIST")
		{
			itemtype = iT_VariableList;
		}//default is none

		if ((itemtype != iT_ReservedZeta) || 
			((SUCCESS == m_pDevice->getItemBySymName(strTokenName, &pIB))
			 && NULL != pIB
			 && (pIB)->IsValidTest())
		   )
		{
			bool bIsEnumVar = false;

			if ((pIB) && (itemtype == iT_ReservedZeta))
			{
				itemtype = pIB->getIType();
			}

			//Added to check for method also Anil September 16 2005
			switch (itemtype)
			{
			case iT_Variable:
				{
					//ENum and Bit_Enum variable can be refered by variable name following "(n)" while n is digit
					if ( (((hCVar *)pIB)->VariableType() == (int)vT_BitEnumerated) ||
						 (((hCVar *)pIB)->VariableType() == (int)vT_Enumerated) )
					{
						bIsEnumVar = true;
					}
					tokenSubType = RUL_DD_SIMPLE;
				}
				//continue
			case iT_Command:
			case iT_Menu:
				{
					DDitemType = DD_ITEM_VAR;
					tokenSubType = RUL_DD_SIMPLE;
				}
				break;
			case iT_Method:
				{
					DDitemType = DD_ITEM_METHOD;
					tokenSubType = RUL_DD_METHOD;
				}
				break;
			case iT_File:
			case iT_Axis:
			case iT_ItemArray:
			case iT_Collection:
			case iT_Record:
			case iT_Array:
			case iT_List:
			case iT_VariableList:
				{	//its a non-variable DD item
					DDitemType = DD_ITEM_NONVAR;
					tokenSubType = RUL_DD_COMPLEX;			
				}
				break;
			default:
				DDitemType = DD_ITEM_NONVAR;
				tokenSubType = RUL_SUBTYPE_NONE;
				break;
			}
			tokenType = RUL_DD_ITEM;			
			CToken localToken(szFullLexeme, tokenType, tokenSubType, lLineNum);
			localToken.m_bIsGlobal = true; 
			localToken.SetEnumToken(bIsEnumVar);

			nIndx = m_GlobalSymTable.Insert(localToken);//SymbolTable::Insert makes a copy of the token

		}//end if SUCCESS
	}//endif -1

	return nIndx;
}

int MEE::SetVariableValue(const char* pszVariableName, int iValue)
{
	int iRetVal = SUCCESS;
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV{};
	string sVarName(pszVariableName);
	if((SUCCESS== m_pDevice->getItemBySymName(sVarName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Index:
			case vT_Enumerated:
			case vT_BitEnumerated:
			case vT_Integer:
			case vT_Unsigned:
				tmpCV = iValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			case vT_FloatgPt:
				tmpCV = (float)iValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			default:
				LOGIT(CERR_LOG,"ERROR: Unhandled variable type set int\n");
				break;

		}//end switch
		
	}//endif
	 
	return iRetVal;
}


int MEE::SetVariableValue(const char* pszVariableName, float fValue)
{
	int iRetVal = SUCCESS;
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV{};
	string sVarName(pszVariableName);
	if((SUCCESS== m_pDevice->getItemBySymName(sVarName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Index:
			case vT_BitEnumerated:
			case vT_Enumerated:
			case vT_Integer:
			case vT_Unsigned:
				tmpCV = (int)fValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			case vT_FloatgPt:
				tmpCV = fValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			default:
				LOGIT(CERR_LOG,"ERROR: Unhandled variable type set float\n");
				break;

		}//end switch
		
	}//endif
	 
	return iRetVal;
}

int MEE::SetVariableValue(const char* pszVariableName, double dValue)
{
	int iRetVal = SUCCESS;
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV{};
	string sVarName(pszVariableName);
	if((SUCCESS== m_pDevice->getItemBySymName(sVarName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Double:
				tmpCV = dValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			default:
				LOGIT(CERR_LOG,"ERROR: Unhandled variable type set double\n");
				break;

		}//end switch
		
	}//endif
	 
	return iRetVal;
}


int MEE::SetVariableValue(const char* pszVariableName, bool bValue)
{
	int iRetVal = SUCCESS;
	hCitemBase* pIB = NULL;
	CValueVarient tmpCV{};
	string sVarName(pszVariableName);
	if((SUCCESS== m_pDevice->getItemBySymName(sVarName, &pIB))
	   && NULL != pIB
	   && pIB->IsVariable()
	   && pIB->IsValidTest() )
	{
		hCVar * pVar = (hCVar*)pIB;

		switch(pVar->VariableType())
		{
			case vT_Boolean:
				tmpCV.vType = CValueVarient::isBool;
				tmpCV.vValue.bIsTrue = (bool)bValue;
				iRetVal = pVar->setValue(tmpCV);
				break;
			default:
				LOGIT(CERR_LOG,"ERROR: Unhandled variable type set bool\n");
				break;

		}//end switch
		
	}//endif
	 
	return iRetVal;
}

/*Vibhor 060705: End of Code*/



//Added By Anil July 11 2005 --starts here
//This is the utility Function to take care of the Language Code while doing string operations
void MEE::RemoveLanguageCode(const char* szOriginalString, char** szWithoutLanguageCode)
{	
	size_t istrLen = strlen(szOriginalString);
	if( (szOriginalString[0] == '|') &&  (szOriginalString[3] == '|') )
	{
		*szWithoutLanguageCode = new char[istrLen - 4 + 1];//4- Lang Code   1- Null char		
		memset(*szWithoutLanguageCode,0,(istrLen - 4 + 1));
		size_t count = 0;
		for(count = 4; count < istrLen ;count++)
		{
			szWithoutLanguageCode[0][count-4] = szOriginalString[count];
		}
		szWithoutLanguageCode[0][count-4] = '\0';		
	}
	else
	{
		*szWithoutLanguageCode = new char[istrLen + 1];///1 - Null char
		memset(*szWithoutLanguageCode,0,istrLen);
		strcpy(*szWithoutLanguageCode,szOriginalString);
		szWithoutLanguageCode[0][istrLen] = '\0';

	}
	return;

}

void MEE::RemoveLanguageCode(const wchar_t* szOriginalString, wchar_t** szWithoutLanguageCode)
{	
	size_t istrLen = wcslen(szOriginalString);
	if( (szOriginalString[0] == _T('|')) && (szOriginalString[3] == _T('|')) )
	{
		*szWithoutLanguageCode = new wchar_t[istrLen - 4 + 1];//4- Lang Code   1- Null char		
		memset(*szWithoutLanguageCode, 0, (istrLen - 4 + 1));
		for(size_t count = 4; count < istrLen; count++)
		{
			szWithoutLanguageCode[0][count-4] = szOriginalString[count];
		}
		szWithoutLanguageCode[0][istrLen-4] = _T('\0');		
	}
	else
	{
		*szWithoutLanguageCode = new wchar_t[istrLen + 1];///1 - Null char
		memset(*szWithoutLanguageCode, 0, istrLen);
		wcsncpy(*szWithoutLanguageCode, szOriginalString, istrLen);
		szWithoutLanguageCode[0][istrLen] = _T('\0');		
	}
	return;

}

//Added By Anil July 11 2005 --Ends here




/******************************************************************************************************/

/*	This Function is for  building the Map which of Primary attributes to its data type and Unique ID
	When I say Primary
		1)This Should not have any further Attribute
		2)its "C" data type has to be defines
	If any new Primary attribute has to be added, Please define Unique ID, before putting in the Map	
	Refer DOT_OP_ATTR Structure defination for more Clarity
	
/************************************************************************************************/


void MEE::BuildDotOpNameToAttrMap()
{
	                 //                         non-aggregates cannot be initialized with initializer list
	DotOpAttr[DOT_ID_NONE]      = DOT_OP_ATTR("NONE"		,DOT_ID_NONE	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_LABEL]     = DOT_OP_ATTR("LABEL"		,DOT_ID_LABEL	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_HELP]      = DOT_OP_ATTR("HELP"		,DOT_ID_HELP	   ,DOT_TYPE_STRING);
	DotOpAttr[DOT_ID_MIN_VALUE] = DOT_OP_ATTR("MIN_VALUE"	,DOT_ID_MIN_VALUE  ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_MAX_VALUE] = DOT_OP_ATTR("MAX_VALUE"	,DOT_ID_MAX_VALUE  ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_DFLT_VALUE]= DOT_OP_ATTR("DEFAULT_VALUE",DOT_ID_DFLT_VALUE,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_VIEW_MIN]  = DOT_OP_ATTR("VIEW_MIN"	,DOT_ID_VIEW_MIN   ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_VIEW_MAX]  = DOT_OP_ATTR("VIEW_MAX"	,DOT_ID_VIEW_MAX   ,DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_COUNT]     = DOT_OP_ATTR("COUNT"		,DOT_ID_COUNT	   ,DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_CAPACITY]  = DOT_OP_ATTR("CAPACITY"	,DOT_ID_CAPACITY   ,DOT_TYPE_INTEGER);	
	DotOpAttr[DOT_ID_FIRST]     = DOT_OP_ATTR("FIRST"		,DOT_ID_FIRST	   ,DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_LAST]      = DOT_OP_ATTR("LAST"		,DOT_ID_LAST	   ,DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_VARIABLE_STATUS]	= DOT_OP_ATTR("VARIABLE_STATUS",	DOT_ID_VARIABLE_STATUS,	DOT_TYPE_INTEGER);
	DotOpAttr[DOT_ID_SCALING_FACTOR]	= DOT_OP_ATTR("SCALING_FACTOR",		DOT_ID_SCALING_FACTOR,	DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_LENGTH]	= DOT_OP_ATTR("LENGTH",		DOT_ID_LENGTH,		DOT_TYPE_REAL);
	DotOpAttr[DOT_ID_NUMBER_OF_ELEMENTS] = DOT_OP_ATTR("NUMBER_OF_ELEMENTS", DOT_ID_NUMBER_OF_ELEMENTS, DOT_TYPE_INTEGER);

	int i; // declare out here to (legally) use in both loops
	for( i=0; i<DOT_ID_TOTAL; i++ )
	{
		m_MapDotOpNameToAttr[ DotOpAttr[i].strDotOpName ] = DotOpAttr[i];
	}
#define PUSH_IT( a, b ) AvailAttrOP[ a ].push_back( b )
	
	PUSH_IT( iT_Variable,DOT_ID_LABEL );     PUSH_IT( iT_Variable,DOT_ID_HELP);
	PUSH_IT( iT_Variable,DOT_ID_MIN_VALUE ); PUSH_IT( iT_Variable,DOT_ID_MAX_VALUE);
	PUSH_IT( iT_Variable, DOT_ID_DFLT_VALUE );		PUSH_IT( iT_Variable, DOT_ID_VARIABLE_STATUS );
	PUSH_IT( iT_Variable, DOT_ID_SCALING_FACTOR );
	
	PUSH_IT( iT_Axis,DOT_ID_LABEL );		 PUSH_IT( iT_Axis,DOT_ID_HELP);
	PUSH_IT( iT_Axis,DOT_ID_MIN_VALUE );	 PUSH_IT( iT_Axis,DOT_ID_MAX_VALUE);
	PUSH_IT( iT_Axis,DOT_ID_VIEW_MIN );		 PUSH_IT( iT_Axis,DOT_ID_VIEW_MAX);

	PUSH_IT( iT_List,DOT_ID_LABEL );		 PUSH_IT( iT_List,DOT_ID_HELP);
	PUSH_IT( iT_List,DOT_ID_COUNT );		 PUSH_IT( iT_List,DOT_ID_CAPACITY);
	PUSH_IT( iT_List,DOT_ID_FIRST );		 PUSH_IT( iT_List,DOT_ID_LAST);

	PUSH_IT( iT_Menu,       DOT_ID_LABEL );	 PUSH_IT( iT_Menu,       DOT_ID_HELP);
	PUSH_IT( iT_Grid,       DOT_ID_LABEL );	 PUSH_IT( iT_Grid,       DOT_ID_HELP);
	PUSH_IT( iT_Image,      DOT_ID_LABEL );	 PUSH_IT( iT_Image,      DOT_ID_HELP);
	PUSH_IT( iT_EditDisplay,DOT_ID_LABEL );  PUSH_IT( iT_EditDisplay,DOT_ID_HELP);
	PUSH_IT( iT_Method,     DOT_ID_LABEL );	 PUSH_IT( iT_Method,     DOT_ID_HELP);

	PUSH_IT( iT_Collection, DOT_ID_LABEL );  PUSH_IT( iT_Collection, DOT_ID_HELP);//members
	PUSH_IT( iT_File,       DOT_ID_LABEL );	 PUSH_IT( iT_File,       DOT_ID_HELP);//members

	PUSH_IT( iT_Chart,      DOT_ID_LABEL );	 PUSH_IT( iT_Chart,   DOT_ID_HELP);//members-source
	PUSH_IT( iT_Chart,      DOT_ID_LENGTH );

	PUSH_IT( iT_Graph,      DOT_ID_LABEL );	 PUSH_IT( iT_Graph,   DOT_ID_HELP);//members-waveform
	PUSH_IT( iT_Source,     DOT_ID_LABEL );	 PUSH_IT( iT_Source,  DOT_ID_HELP);//members-numeric
	PUSH_IT( iT_Waveform,   DOT_ID_LABEL );  PUSH_IT( iT_Waveform,DOT_ID_HELP);

	PUSH_IT( iT_ItemArray,  DOT_ID_LABEL );  PUSH_IT( iT_ItemArray,DOT_ID_HELP);//elements
	PUSH_IT( iT_Array,      DOT_ID_LABEL );	 PUSH_IT( iT_Array,    DOT_ID_HELP);//elements
	PUSH_IT(iT_Array, DOT_ID_NUMBER_OF_ELEMENTS);
	

	//initialize m_MapVarStatusNameToValue
	m_MapVarStatusNameToValue["VARIABLE_STATUS_NONE"] = nsEDDEngine::VARIABLE_STATUS_NONE;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_INVALID"] = nsEDDEngine::VARIABLE_STATUS_INVALID;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_NOT_ACCEPTED"] = nsEDDEngine::VARIABLE_STATUS_NOT_ACCEPTED;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_NOT_SUPPORTED"] = nsEDDEngine::VARIABLE_STATUS_NOT_SUPPORTED;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_CHANGED"] = nsEDDEngine::VARIABLE_STATUS_CHANGED;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_LOADED"] = nsEDDEngine::VARIABLE_STATUS_LOADED;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_INITIAL"] = nsEDDEngine::VARIABLE_STATUS_INITIAL;
	m_MapVarStatusNameToValue["VARIABLE_STATUS_NOT_CONVERTED"] = nsEDDEngine::VARIABLE_STATUS_NOT_CONVERTED;
}


/***************************************************************************/
/*	This Function is for  having Secondary attributes in string vector
	When I say Secondary
		1)This Should have further Attribute to be resolved
		2)its "C" data type could not be defined	
/***************************************************************************/

void MEE::BuildSecondaryAttrList()
{
	m_svSecondaryAtt.push_back("X_AXIS");
	m_svSecondaryAtt.push_back("Y_AXIS");
//stevev 23jul07 - only attribute groups that hold specified accessible attributes are allowed here
//                 the following are not allowed.
/* 15aug07 - removed
	m_svSecondaryAtt.push_back("KEY_POINTS");
	m_svSecondaryAtt.push_back("X_VALUES");
	m_svSecondaryAtt.push_back("Y_VALUES");	
*/
}

/**********************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x  Where 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap()) 
					3)secondary attribute ( X_AXIS and Y_AXIS -- refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szTokenName      :DDItem
	pvar			 :Which has to be resolved out of szComplexDDExpre

/**********************************************************************************************/

RETURNCODE MEE::ResolveDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef, /* out */INTER_VARIANT* pvar, /* out */hCitemBase** ppDevObjVar)
{
	if ((szComplexDDExpre == NULL) && (pOpRef == NULL))
	{
		return FAILURE;
	}
	
	hCitemBase* pIBFinal = NULL;	//output as item data pointer
	CValueVarient lcv{};
	RETURNCODE rc = ResolveOperItemIDOrDDExp((const char*)szComplexDDExpre, pOpRef, &lcv, pIBFinal);
	if( SUCCESS != rc )
	{
		return rc;
	}
	if(pIBFinal == NULL)
	{
		return FAILURE;
	}

	if (ppDevObjVar)
	{
		*ppDevObjVar = pIBFinal;
	}

	//read r-value
	string attrName = (string)lcv;
	if ((attrName != "") && isAttr((char *)attrName.c_str()))
	{
		//Is it getting attribte?
		//write attribute value into variable pvar
		rc = GetAttrValue(pIBFinal, (char *)attrName.c_str(), pOpRef, pvar);
	}
	else
	{
		switch(pIBFinal->getIType())
		{
		case iT_Variable:
			{
				hCVar *pVar = (hCVar *) pIBFinal;

				//Is it getting enum/bit-enum string?
				if ( (lcv.isNumeric()) && 
					 ((pVar->VariableType() == (int)vT_BitEnumerated) ||
					  (pVar->VariableType() == (int)vT_Enumerated)) )
				{
					//get enum or bit_enum variable string by providing variable ID and variable value
					unsigned long enumValue = (unsigned long)lcv;
					CValueVarient enumString{};
					rc = m_pDevice->deviceGetEnumVarString (pVar->getID(), enumValue, &enumString);
					
					if ((rc == SUCCESS) && !CV_WSTR(&enumString).empty())
					{
						pvar->SetValue(const_cast<wchar_t*>(CV_WSTR(&enumString).c_str()), RUL_DD_STRING);
					}
					else
					{
						rc = FAILURE;
					}
				}
				else	//Is it getting device value?
				{
					//Get the dip r-value and update pvar
					rc = GetDDVarVal(pOpRef, pvar);
					if( SUCCESS != rc )	//maybe aborted or other error
					{
						return rc;
					}
				}
			}
			break;

		case iT_Array:
			{
				hCarray* phCArr = (hCarray*)pIBFinal;
				rc = GetDDArrayVal(phCArr, pOpRef, pvar);
				if( SUCCESS != rc )	//maybe aborted or other error
				{
					return rc;
				}
			}
			break;

		case iT_Method:
		case iT_Command:
		case iT_Axis:
		case iT_Menu:
		case iT_List:
			*pvar = (long)(int)pIBFinal->getID();
			break;

		default:
			return FAILURE;
			break;
		}
	}
	
	if (rc != SUCCESS)
	{
		//failed to read reference value
		rc = MEE_INVALID_REFERENCE;
	}
	return rc;

}

/******************************************************************************************************/

/*	This Function is for resolving the Primary attribute
	
	Function Input Parameters: 
	pIB				: given item base pointer to DD item
	pAttrName	    : given attribute name should be one in BuildDotOpNameToAttrMap()
	pvar			: Which has to be filled after getting the Primary attribute value
	pOpRef			: pointer to DD item operative ID

/*************************************************************************************************/

//This function gets both primary attribute and secondary attribute
RETURNCODE MEE::GetAttrValue(/* in */ hCitemBase* pIB, /* in */ char* pAttrName, /* in */ PARAM_REF *pOpRef, /* out */ INTER_VARIANT* pvar)
{
	if ( pIB == NULL || pAttrName == NULL || (pvar == NULL))
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}

	string OrigAttrName = pAttrName;
	int  minmaxNumber   = 0;// this must have a value when we have M??_VALUEn 'cause n==0 illegal
	size_t  strlenmaxvalue = strlen("MAX_VALUE");//Same as strlen(MIN_VALUE)
	//If the char szMinOrMax[10]; is Either MAX_VALUEn or MIN_VALUEn  
	//then hold orig and make char szMinOrMax[10]; as only Either   MAX_VALUE or  MIN_VALUE
	if(	( ( strstr(pAttrName, "MAX_VALUE") ) != NULL || 
		  ( strstr(pAttrName, "MIN_VALUE") ) != NULL   ) &&
		strlen(pAttrName) != strlenmaxvalue   )// isa numbered attribute
	{
		minmaxNumber = atoi(  pAttrName + strlenmaxvalue );
		minmaxNumber -= 1; 
		pAttrName[ strlenmaxvalue ] = '\0';//terminate at numeric
	}

	RETURNCODE     rc = SUCCESS;
	DOT_OP_ITER_t  ID_iter;
	DOT_OP_ATTR    DotOpAtt;
	for ( ID_iter  = AvailAttrOP[pIB->getIType()].begin();
		  ID_iter != AvailAttrOP[pIB->getIType()].end();   ++ID_iter)
	{
		long idNumber = *ID_iter;
		if ( DotOpAttr[idNumber].strDotOpName == pAttrName )
		{
			DotOpAtt = DotOpAttr[idNumber];
			break;
		}
	}

	if ( ID_iter == AvailAttrOP[pIB->getIType()].end()) 
	{
		//must be secondary attribute
		if ( pAttrName == "X_AXIS" ) // possible reference to a DD item
		{// a secondary attribute
//#			verify valid attribute for item
			if ( pIB->getIType() == iT_Graph)
			{
//#				lookup attribute in item 
//				rpDevObjVar = ((hCgraph*)pLeftItem)->getXAxis();
//#				return error if not found or not an item	
				if (pIB == NULL || pIB->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: X_AXIS of %s did not resolve.\n",
																pIB->getName().c_str());
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}// else - all is well
//#				fill ptr, clear varient and return
				return SUCCESS;
			}
			else // - invalid
			{
				SDCLOG(CERR_LOG,"Method Reference: 'X_AXIS' is invalid reference for %s\n",
																	pIB->getName().c_str());
				return MEE_ITEM_ATTRIBUTE_MISMATCH;
			}
		}
		else
		if ( pAttrName == "Y_AXIS" ) // possible reference to a DD item
		{// a secondary attribute

//#			verify valid attribute for item
			if ( pIB->getIType() == iT_Source)
			{
//#				lookup attribute in item 
//				rpDevObjVar = ((hCsource*)pLeftItem)->getYAxis();
//#				return error if not found or not an item	
				if (pIB == NULL || pIB->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Source Y_AXIS of %s did not resolve.\n",
																	pIB->getName().c_str());
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}// else - all is well
//#				fill ptr, clear varient and return
				return SUCCESS;
			}
			else
			if ( pIB->getIType() == iT_Waveform)
			{
//#			lookup attribute in item 
//				rpDevObjVar = ((hCwaveform*)pLeftItem)->getYAxis();
//#			return error if not found or not an item	
				if (pIB == NULL || pIB->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Waveform Y_AXIS of %s did not resolve.\n",
																	pIB->getName().c_str());
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}// else - all is well
//#			fill ptr, clear varient and return
				return SUCCESS;
			}
			else
			{
				SDCLOG(CERR_LOG,"Method Reference: 'Y_AXIS' is invalid reference for %s\n",
																	pIB->getName().c_str());
				return MEE_ITEM_ATTRIBUTE_MISMATCH;
			}
		}

		return MEE_ITEM_ATTRIBUTE_MISMATCH;
	}

	//must be primary attribute
	wstring        tmpStr;
	CValueVarient tmpCV{};
	hCVar*        pTmpVar = NULL;
	//depending On the Unique Id Call the apropriate interface	
	switch(DotOpAtt.uiUniqueID)
	{
		case DOT_ID_LABEL:
			{
				rc  = pIB->Label(tmpStr);
				if(rc != SUCCESS)
				{
					return MEE_ATTR_NOT_AVAILABLE;
				}
				if ( pvar )
				{
					*pvar = (wchar_t*)tmpStr.c_str();
				}
			}
			break;
		case DOT_ID_HELP:
			{
				rc = pIB->Help(tmpStr);
				if(rc != SUCCESS)
				{
					return MEE_ATTR_NOT_AVAILABLE;
				}
				if ( pvar )
				{
					*pvar = (wchar_t*)tmpStr.c_str();
				}
			}
			break;
/* stevev 15aug07 - 
   not specified:removed:DOT_ID_DISPLAY_FORMAT, DOT_ID_EDIT_FORMAT */
		case DOT_ID_MIN_VALUE:
			{
				CValueVarient locVar{};
				hCRangeList RangeList;
				RangeList_t::iterator RangeIterator;
				if ( pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric() )
				{
					RangeList = ((hCNumeric*)pIB)->getRangeList();
					rc = SUCCESS;
				}
				else if( pIB->getIType() == iT_Axis )
				{					
					rc = ((hCaxis*)pIB)->getMinMaxList(RangeList);
					if(rc != SUCCESS)
					{
						return MEE_NON_NUMERIC_4_MINMAX;
					}
				}
				else
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a numeric to "
									"resolve MIN_VALUE\n",	pIB->getName().c_str() );
					return MEE_NON_NUMERIC_4_MINMAX;
				}

				if ( minmaxNumber == 0 )
				{
					if (RangeList.size() > 1)
					{
						SDCLOG(CERR_LOG,"Method Reference: WARNING '%s' has %d MIN-MAX entries "
						"but %s was accessed\n",pIB->getName().c_str(), RangeList.size(),
						     OrigAttrName.c_str()  );
					}
					//locVar = RangeList[0].minVal;
					hCRangeItem ri = RangeList.begin()->second;
					locVar = ri.minVal;
				}
				else
				{// we are doing a number MIN_VALUEn
					RangeIterator = RangeList.find(minmaxNumber);
					if ( RangeIterator == RangeList.end() )
					{
						SDCLOG(CERR_LOG,"Method Reference: Did not find MIN_VALUE%d in '%s'\n",
							pIB->getName().c_str(), minmaxNumber  );
						return MEE_MINMAX_FIND_FAILURE;
					}// else we found it
					locVar = (RangeIterator->second).minVal;
				}

				if (pvar) *pvar = (double)locVar;
			}
			break;
		case DOT_ID_MAX_VALUE:
			{				
				CValueVarient locVar{};
				hCRangeList RangeList;
				RangeList_t::iterator RangeIterator;
				if ( pIB->IsVariable() && ((hCVar*)pIB)->IsNumeric() )
				{
					RangeList = ((hCNumeric*)pIB)->getRangeList();
					rc = SUCCESS;
				}
				else if( pIB->getIType() == iT_Axis )
				{					
					rc = ((hCaxis*)pIB)->getMinMaxList(RangeList);
					if(rc != SUCCESS)
					{
						return MEE_NON_NUMERIC_4_MINMAX;
					}
				}
				else
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a numeric to "
									"resolve MAX_VALUE\n",	pIB->getName().c_str() );
					return MEE_NON_NUMERIC_4_MINMAX;
				}

				if ( minmaxNumber == 0 )
				{
					if (RangeList.size() > 1)
					{
						SDCLOG(CERR_LOG,"Method Reference: WARNING '%s' has %d MIN-MAX entries "
						"but %s was accessed\n",pIB->getName().c_str(), RangeList.size(),
						     OrigAttrName.c_str()  );
					}
					// stevev 18oct07 changed from::> locVar = RangeList[0].maxVal;//assumed which == 0
					hCRangeItem ri = RangeList.begin()->second;
					locVar = ri.maxVal;
				}
				else
				{// we are doing a number MAX_VALUEn
					RangeIterator = RangeList.find(minmaxNumber);
					if ( RangeIterator == RangeList.end() )
					{
						SDCLOG(CERR_LOG,"Method Reference: Did not find MAX_VALUE%d in '%s'\n",
							pIB->getName().c_str(), minmaxNumber  );
						return MEE_MINMAX_FIND_FAILURE;
					}// else we found it
					locVar = (RangeIterator->second).maxVal;
				}

				if (pvar) *pvar = (double)locVar;
			}
		break;
		case DOT_ID_DFLT_VALUE:
			{
   				itemType_t item_type = (itemType_t) pIB->getIType();

				if (item_type == iT_Variable)
				{
          	        hCVar *pVar = (hCVar *) pIB;

					CValueVarient paramValue{};
					pVar->getaAttr(varAttrDefaultValue, &paramValue);

					*pvar = INTER_VARIANT::InitFrom(&paramValue);
				}
				else
				{
                     SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a DefaultValue because item is not variable\n",
                                                                                         pIB->getName().c_str() );
                     return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
			break;
		case DOT_ID_VIEW_MIN:
			{
				itemType_t item_type = (itemType_t) pIB->getIType();
				if(item_type == iT_Axis)
				{
					CValueVarient paramValue{};
					rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrViewMin, READ);

					if (rc != BLTIN_SUCCESS)
					{
						SDCLOG(CERR_LOG,"DOT_ID_VIEW_MIN returned a BS_ErrorCode of %d\n", rc );
						return MEE_ATTR_NOT_AVAILABLE;
					}

					*pvar = INTER_VARIANT::InitFrom(&paramValue);
				}
				else
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a VIEW_MIN \n",
																		pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
		break;
		case DOT_ID_VIEW_MAX:
			{
				itemType_t item_type = (itemType_t) pIB->getIType();
				if(item_type == iT_Axis)
				{
					CValueVarient paramValue{};
					rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrViewMax, READ);

					if (rc != BLTIN_SUCCESS)
					{
						SDCLOG(CERR_LOG,"DOT_ID_VIEW_MAX returned a BS_ErrorCode of %d\n", rc );
						return MEE_ATTR_NOT_AVAILABLE;
					}

					*pvar = INTER_VARIANT::InitFrom(&paramValue);
				}
				else	// some other type get a non-dynamic version
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a VIEW_MAX \n",
																		pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
		break;
		case DOT_ID_COUNT:
			{
   				itemType_t item_type = (itemType_t) pIB->getIType();

				if (item_type == iT_List)	// List count is dynamic, get it from the client
				{
					CValueVarient paramValue{};
					rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrCount, READ);

					if (rc != BLTIN_SUCCESS)
					{
						SDCLOG(CERR_LOG,"DOT_ID_LAST returned a BS_ErrorCode of %d\n", rc );
						return MEE_ATTR_NOT_AVAILABLE;
					}

					*pvar = INTER_VARIANT::InitFrom(&paramValue);
				}
				else
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a COUNT\n",
																	pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
		break;
		case DOT_ID_CAPACITY:
			{
   				itemType_t item_type = (itemType_t) pIB->getIType();
				if (item_type == iT_List)
				{
					hClist* pListPtr = (hClist *)pIB;
					if (pvar) *pvar = (unsigned int)pListPtr->getCapacity();
				}
				else
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a CAPACITY\n",
																	pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
			break;
		
		case DOT_ID_FIRST:
			{
                CValueVarient paramValue{};
                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrFirst, READ);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_FIRST returned a BS_ErrorCode of %d\n", rc );
					return MEE_ATTR_NOT_AVAILABLE;
				}

				*pvar = INTER_VARIANT::InitFrom(&paramValue);
			}
		break;
		case DOT_ID_LAST:
			{
                CValueVarient paramValue{};
                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrLast,READ);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_LAST returned a BS_ErrorCode of %d\n", rc );
					return MEE_ATTR_NOT_AVAILABLE;
				}

				*pvar = INTER_VARIANT::InitFrom(&paramValue);
			}
		break;
		case DOT_ID_VARIABLE_STATUS:
			{				
				if ( !pIB->IsVariable() )
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a variable to "
									"resolve VARIABLE_STATUS\n",	pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}

                CValueVarient paramValue{};
                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrVariableStatus, READ);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_VARIABLE_STATUS returned a BS_ErrorCode of %d\n", rc );
					return MEE_ATTR_NOT_AVAILABLE;
				}

				*pvar = INTER_VARIANT::InitFrom(&paramValue);
			}
		break;
		case DOT_ID_SCALING_FACTOR:
			{				
				if ( !pIB->IsVariable() )
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a variable to "
									"resolve SCALING_FACTOR\n",	pIB->getName().c_str() );
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}

                CValueVarient paramValue{};
                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &paramValue, varAttrScalingFactor, READ);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_SCALING_FACTOR returned a BS_ErrorCode of %d\n", rc );
					return MEE_ATTR_NOT_AVAILABLE;
				}

				*pvar = INTER_VARIANT::InitFrom(&paramValue);
			}
		break;
		case DOT_ID_LENGTH:
			{
   				itemType_t item_type = (itemType_t) pIB->getIType();

				if (item_type == iT_Chart)
				{
          	        hCchart *pChart = (hCchart *) pIB;

					CValueVarient paramValue{};
					CV_VT(&paramValue, ValueTagType::CVT_UI8);
					CV_UI8(&paramValue) = pChart->getLength();

					*pvar = INTER_VARIANT::InitFrom(&paramValue);
				}
				else
				{
                     SDCLOG(CERR_LOG,"Method Reference: '%s' does not return a LENGTH because item is not chart\n",
                                                                                         pIB->getName().c_str() );
                     return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
			break;
		case DOT_ID_NUMBER_OF_ELEMENTS:
			{
				itemType_t item_type = (itemType_t)pIB->getIType();

				if (item_type == iT_Array)
				{
					hCarray *pArray = (hCarray *)pIB;
					if (pvar) *pvar = (unsigned int)pArray->getCount();
				}
				else
				{
					SDCLOG(CERR_LOG, "Method Reference: '%s' does not return a NumberOfElements because item is not array\n",
						pIB->getName().c_str());
					return MEE_ITEM_ATTRIBUTE_MISMATCH;
				}
			}
			break;
		default:
			rc = MEE_ITEM_ATTRIBUTE_MISMATCH;
		break;
		
	}//end switch				
		
	return rc;

}

RETURNCODE MEE::PutDynamicAttrValue(/* in */ hCitemBase* pIB, /* in */ string pAttrName, /* in */ PARAM_REF *pOpRef, /* out */ CValueVarient* pVal)
{
	if ( pIB == NULL || pAttrName == "" || (pVal == NULL))
	{
		return MEE_BAD_PARAM_PASSED_IN;
	}

	RETURNCODE     rc = SUCCESS;
	DOT_OP_ITER_t  ID_iter;
	DOT_OP_ATTR    DotOpAtt;
	for ( ID_iter  = AvailAttrOP[pIB->getIType()].begin();
		  ID_iter != AvailAttrOP[pIB->getIType()].end();   ++ID_iter)
	{
		long idNumber = *ID_iter;
		if ( DotOpAttr[idNumber].strDotOpName == pAttrName )
		{
			DotOpAtt = DotOpAttr[idNumber];
			break;
		}
	}

	if ( ID_iter == AvailAttrOP[pIB->getIType()].end()) 
	{
		return MEE_ITEM_ATTRIBUTE_MISMATCH;
	}

	//must be primary attribute
	wstring        tmpStr;
	CValueVarient tmpCV{};
	hCVar*        pTmpVar = (hCVar *)pIB;


	//depending On the Unique Id Call the apropriate interface	
	switch(DotOpAtt.uiUniqueID)
	{
		case DOT_ID_VIEW_MIN:
			{
				//AXIS
                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, pVal, varAttrViewMin, WRITE);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_VIEW_MIN returned a BS_ErrorCode of %d\n", rc );
					rc = MEE_ATTR_NOT_AVAILABLE;
				}
			}
		break;
		case DOT_ID_VIEW_MAX:
			{
				//AXIS
				rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, pVal, varAttrViewMax, WRITE);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_VIEW_MAX returned a BS_ErrorCode of %d\n", rc );
					rc = MEE_ATTR_NOT_AVAILABLE;
				}
			}
		break;
		
		case DOT_ID_VARIABLE_STATUS:
			{				
				if ( !pIB->IsVariable() )
				{// only numeric variables have a min - max
					SDCLOG(CERR_LOG,"Method Reference: '%s' must be a variable to "
									"resolve VARIABLE_STATUS\n",	pIB->getName().c_str() );
					return MEE_ATTR_NOT_AVAILABLE;
				}

                rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, pVal, varAttrVariableStatus, WRITE);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_VARIABLE_STATUS returned a BS_ErrorCode of %d\n", rc );
					rc = MEE_ATTR_NOT_AVAILABLE;
				}
			}
		break;

		case DOT_ID_FIRST:
			{
				//LIST
				rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, pVal, varAttrFirst, WRITE);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_FIRST returned a BS_ErrorCode of %d\n", rc );
					rc = MEE_ATTR_NOT_AVAILABLE;
				}
			}
		break;
		case DOT_ID_LAST:
			{
				//LIST
				rc = m_pDevice->AccessDynamicAttributeMethod(pOpRef, pVal, varAttrLast, WRITE);

				if (rc != BLTIN_SUCCESS)
				{
					SDCLOG(CERR_LOG,"DOT_ID_FIRST returned a BS_ErrorCode of %d\n", rc );
					rc = MEE_ATTR_NOT_AVAILABLE;
				}
			}
		break;
		
		default:
			rc = MEE_BAD_PARAM_PASSED_IN;
		break;
		
	}//end switch
		
	return rc;
}


/******************************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x  and 
	also updating the disp value for the same with the proper operator 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szTokenName      :DDItem
	pvar			 :is an in parameter to whhic the dd Item has to be assigned

	if the Lavalue turns out to be a attribute of DDitem , then please through the error , even without resolving.
	
	  

/******************************************************************************************************/


RETURNCODE MEE::ResolveNUpdateDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef, /* in/out */INTER_VARIANT* ivVariableValue, /* in */RUL_TOKEN_SUBTYPE	AssignType)
{
	if ((szComplexDDExpre == NULL) && (pOpRef == NULL))
	{
		return FAILURE;
	}

	//No we need to resolve the Coplex Expression in to an Var ptr which is pIBFinal
	hCitemBase* pIBFinal = NULL;	//output as item data pointer
	CValueVarient lcv{};
	RETURNCODE rc = ResolveOperItemIDOrDDExp((const char*)szComplexDDExpre, pOpRef, &lcv, pIBFinal);
	if( SUCCESS != rc )
	{
		return rc;
	}
	if(pIBFinal == NULL)
	{
		return FAILURE;
	}

	//write l-value
	string attrName = (string)lcv;
	if ((attrName != "") && isAttr((char *)attrName.c_str()))
	{
		//Assign dynamic attribute
	    CValueVarient paramValue{};
		rc = GetDDAttrNEvalAssType(attrName, pOpRef, ivVariableValue, &paramValue, AssignType);

		if( SUCCESS ==  rc)
		{
			//write attribute value into variable pvar
			rc = PutDynamicAttrValue(pIBFinal, attrName, pOpRef, &paramValue);
		}
	}
	else if (pIBFinal)
	{
		CValueVarient paramValue{};
		rc = GetDDVarValNEvalAssType(pIBFinal, pOpRef, ivVariableValue, &paramValue, AssignType);
		if( (m_pDevice) && (SUCCESS ==  rc) )
		{
			rc = m_pDevice->devAccessTypeValue2(pOpRef, &paramValue, WRITE);
			rc = convertMixedCodeToMEECode(rc);
		}
	}

	return rc;
	
}


/******************************************************************************************************/

/*	This Function is for getting the CgroupItemDescriptor and casting accordingly


/******************************************************************************************************/

RETURNCODE MEE::GetGroupItemPtr(hCitemBase* pIBFinal,int iIndexValue,hCgroupItemDescriptor** pGID)
{
	
	if(pIBFinal == NULL)
		return FAILURE;


	int  rc = SUCCESS;
	itemType_t typeCheck = pIBFinal->getIType();
	switch (typeCheck)
	{
		
		case iT_ItemArray:
			rc= ((hCitemArray*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Collection:
			rc = ((hCcollection*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_Array:			
			rc = ((hCarray*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;
			
		case iT_List:
			rc = ((hClist*)pIBFinal)->getByIndex(iIndexValue,pGID);
			break;

			
		default:			
			return FAILURE;
			break;
		
	}//end of switch AssignType
	if ( rc != SUCCESS && *pGID == NULL )
	{
		return FAILURE;
	}
	
	return SUCCESS;
}


/************************************************************************************************/

/*	This Function is for resolving the DD complex Expression such as  DDItem[i].member[j].x and 
		etiurs the item base  ptr 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() 
																		function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szDDitemName     :DDItem
	iEndofEpression	 :Till what lenght of szComplexDDExpre it needs resolution
	hCitemBase       :Gives back the Itembase ptr which will refer to DD item, Which is after 
																	Resolving szComplexDDExpre	  

/************************************************************************************************/
RETURNCODE MEE::ResolveExp(const char* szDDitemName,const char* szComplexDDExpre,
											unsigned long iEndofEpression, hCitemBase** pIBFinal)
{
	//CJK this is important 9/2/2009
	if( (szDDitemName == NULL)||(szComplexDDExpre == NULL))
	{
		return FAILURE;
	}
	hCitemBase*  pIB = NULL;
	string strDDitemName = szDDitemName;
	//get pIB from the szTokenName which is DD item Name
	if((SUCCESS == m_pDevice->getItemBySymName(strDDitemName,&pIB))
	   && NULL != pIB
	   && (pIB)->IsValidTest()) // Invalids are not supported
	{
		if( strcmp(szComplexDDExpre,szDDitemName) == 0 )
		{
			pIB->setName(strDDitemName);//CJK added this; when we deal with DD_ITEM as a reference the GetName was called and this wasnt set
		}
		*pIBFinal = pIB;	
		
		// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast to iEndOfEpression>
		for( long int lCount = (long)strlen(szDDitemName); 
					  lCount < (long)iEndofEpression ; lCount++)
		{			
			//Check the Expression has any index ,ie in DDItem[i],   value of i
			if(szComplexDDExpre[lCount] == '[')
			{
				// you got left open backet , which mens u need to get the hCgroupItemDescriptor 
				//																	for the same 
				long int lCurrentPos = lCount;
				while(szComplexDDExpre[lCount] != ']')
				{
					lCount++;
				}				
				char* szIndexValue = new char[lCount - lCurrentPos];
				memset(szIndexValue,0,(lCount - lCurrentPos));
				strncpy(szIndexValue,&szComplexDDExpre[lCurrentPos+1], lCount - lCurrentPos-1);
				szIndexValue[lCount - lCurrentPos -1] = '\0'; 

				//Got the integer value as string So cast it to int
				int iIndexValue = atoi(szIndexValue);// stevev 23jul07 - index is allowed 
													//					to be an expression***
				//Delete the string, No more usefull
				if(szIndexValue)
				{
					delete[] szIndexValue;
					szIndexValue = NULL;
				}

				itemType_t typeCheck = (*pIBFinal)->getIType();
				if(iT_Variable == typeCheck)
				{
					hCVar* pVar = (hCVar*)pIBFinal;
					int iVarTpe = pVar->VariableType();
					if(vT_BitEnumerated == iVarTpe)
					{
						// stevev 23jul07 - this entire check ends in empty?????????????????????
					}

				}

				//The DD item must be value array, item array or list.
				//after reference by '[', get item final data pointer and update operative IDs
				int  rc = SUCCESS;
				ITEM_ID ulItemId = (*pIBFinal)->getID();
				if ( ((typeCheck == iT_Array) || (typeCheck == iT_List)) && (m_startedProtocol != nsEDDEngine::FF) )
				{
					*pIBFinal = NULL;
					rc = m_pDevice->getElementOrMemberPointer(ulItemId, iIndexValue + 1, pIBFinal);
				}
				else
				{
					*pIBFinal = NULL;
					rc = m_pDevice->getElementOrMemberPointer(ulItemId, iIndexValue, pIBFinal);
				}
				if (rc != SUCCESS)
				{
					*pIBFinal = NULL;
					return FAILURE;
				}
			}
			//or Case may be member of DD item or Secondary attribute or one of LAST, FIRST
			//This list may increase as more attribute gets added
			//For Primary attribute, Update BuildDotOpNameToAttrMap() function
			//Secondary attribute Update in BuildSecondaryAttrList()
			//For thing like  LAST and FIRST , we need to really look in to Spec as it is little 
			//special case 
			else if(szComplexDDExpre[lCount] == '.')
			{
				//You got dot which mens this should be either Member name 
				//							( currently not implimented) or Secondary attrubute
				long int lCurrentPos = lCount;
				lCount++;
				while((szComplexDDExpre[lCount] != '[') &&
					  (szComplexDDExpre[lCount] != '.') &&
					  (szComplexDDExpre[lCount] != NULL)   )
				{
					lCount++;
				}

				char* szStringAfterDot = new char[lCount - lCurrentPos];
				memset(szStringAfterDot,0,(lCount - lCurrentPos));

				if( szComplexDDExpre[lCurrentPos+1] == ' ' )
				{
					strncpy(szStringAfterDot,&szComplexDDExpre[lCurrentPos+2], lCount - lCurrentPos -1 );
				}
				else
				{
					strncpy(szStringAfterDot,&szComplexDDExpre[lCurrentPos+1], lCount - lCurrentPos);
				}
				//todo remove spaces.
				szStringAfterDot[lCount - lCurrentPos -1] = '\0'; 
				string strAfterDot = szStringAfterDot;
				lCount--;
				if(szStringAfterDot)
				{
					delete[] szStringAfterDot;
					szStringAfterDot = NULL;
				}

				if(iT_Variable == (*pIBFinal)->getIType())
				{
					// do nothing
				}
				else if(strAfterDot ==  "LAST" || strAfterDot ==  "FIRST")
				{
					if(iT_List != (*pIBFinal)->getIType())
					{
						//This is an eror as other than list has LAST dot operator
						//TODO  call the Correspondin API
							return FAILURE;
					}
					
					unsigned int iListCount = 0;
					if(strAfterDot ==  "LAST")
					{
						hClist* pList = (hClist*)(*pIBFinal);
						iListCount = pList->getCount();
						iListCount--;
					}
					//With this index, query for the hCgroupItemDescriptor, 
					int  rc = SUCCESS;
					hCgroupItemDescriptor*  pGID = NULL ;
					if(GetGroupItemPtr((*pIBFinal),iListCount,&pGID) == FAILURE)	
					{			
						RAZE( pGID ); // stevev 21feb07 - it may have been alloc'd in getbyindex 
									  // in getgroupitemptr
						//Some time it may happen that this index is out of range					
						return FAILURE;
					}					
					//We got valid hCgroupItemDescriptor from this index , so get fresh pIBFinal
					*pIBFinal = NULL;
					hCreference ClocalRef(pGID->getRef());
					rc = ClocalRef.resolveID((*pIBFinal));	
					RAZE( pGID ); // stevev 21feb07 - was alloc'd in getbyindex in getgroupitemptr
					//again resolution of ClocalRef may faol
					if ( rc != SUCCESS)
					{
						return FAILURE;
					}
							
				
				}
				else if(strAfterDot == "KEY_POINTS")
				{
					lCurrentPos = lCount+1;
					//if it is KEY_POINTS, it should be either  KEY_POINTS.X_VALUES  or 
					//											KEY_POINTS.Y_VALUES
					//if not then error
					if(szComplexDDExpre[lCount] != '.')
					{
						return FAILURE;
					}
					while(szComplexDDExpre[lCount] != '[' || szComplexDDExpre[lCount] != '.')
					{
						lCount++;
					}
					char* szAfterKEYPOINTS = new char[lCount - lCurrentPos];
					memset(szAfterKEYPOINTS,0,(lCount - lCurrentPos));
					strncpy(szAfterKEYPOINTS,  &szComplexDDExpre[lCurrentPos+1], 
															               lCount - lCurrentPos-1);
					szAfterKEYPOINTS[lCount - lCurrentPos -1] = '\0'; 
					string strAfterKEYPOINTS = szStringAfterDot;
					//if it is any  of KEY_POINTS.X_VALUES  or KEY_POINTS.Y_VALUES, then error
					if( (strAfterKEYPOINTS != "X_VALUES") || (strAfterKEYPOINTS != "Y_VALUES") )
					{
						//Deallocate all memory
						if(szAfterKEYPOINTS)
						{
							delete[] szAfterKEYPOINTS;
							szAfterKEYPOINTS = NULL;
						}
						return FAILURE;
					}
					//TODO  call the Correspondin API
					if(szAfterKEYPOINTS)
					{
						delete[] szAfterKEYPOINTS;
						szAfterKEYPOINTS = NULL;
					}

				}
				else
				{
					//This could be member of Collection or Record or File
					//get member ID
					ITEM_ID ulMemberId = 0;
					int rc = m_pDevice->GetMemberIdByName(strAfterDot.c_str(), &ulMemberId);
					if (rc == SUCCESS)
					{
						ITEM_ID ulItemId = (*pIBFinal)->getID();
						//get item final data pointer
						*pIBFinal = NULL;
						rc = m_pDevice->getElementOrMemberPointer(ulItemId, ulMemberId, pIBFinal);
					}
					if (rc != SUCCESS)
					{
						*pIBFinal = NULL;
						return FAILURE;
					}
				}
			}
			
		}
	}

	return SUCCESS;

}




/******************************************************************************************************/

/*	This Function is for Validating the expression involving DD items
	for resolving the DD complex Expression such as  DDItem[i].member[j].x 
	DDItem  :is DD item, 
	i,j		:are Constant integers and 
	member  :is member of "DDItem"  
	x		:can be	1)member of "DDItem"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap() function for this ) 
					3)secondary attribute ( KEY_POINTS --refer BuildSecondaryAttrList for this ) 
					4)one of LAST, FIRST 

	Function Input Parameters: With respect to Above case a[i].b[j].x
	szComplexDDExpre :DDItem[i].member[j].x
	szLastAttr	     :out param which is "x"
	bPrimaryAttr	 : out param , is true if it is primary attribute 
	Retun vale		 :returns false is szLastAttr is secondary attribute
	
/******************************************************************************************************/
RETURNCODE MEE::ValidateExp(const char* szComplexDDExpre, char** szLastAttr, bool &bPrimaryAttr)
{
	if(szComplexDDExpre == NULL)
	{
		return FAILURE;
	}

	size_t iDDExpreLen = strlen(szComplexDDExpre);
	int iDotCount = 0;
	size_t iLastDotPos = 0;
	//Get the position of last dot operator , if it is present
	for(size_t i = 0; i < iDDExpreLen ; i++)
	{
		if( szComplexDDExpre[i] == '.')
		{
			iDotCount++;
			iLastDotPos = i;
		}
	}
	
	//Get the Expression followed by Last dot	
	if(iDotCount != 0)
	{
		*szLastAttr = new char[iDDExpreLen - iLastDotPos + 3];
		memset ( *szLastAttr, 0, iDDExpreLen - iLastDotPos + 3 );
		//todo remove spaces
		
		if( szComplexDDExpre[iLastDotPos+1] == ' ' )
		{
			strncpy( *szLastAttr,&szComplexDDExpre[iLastDotPos+2], iDDExpreLen - iLastDotPos - 1) ;
		}
		else
		{
			strncpy(*szLastAttr,&szComplexDDExpre[iLastDotPos+1], iDDExpreLen - iLastDotPos) ;
		}

		char *szTemp = NULL;
		
		//if Expression followed by Last dot is secondary attribute , then it is not valid !! 
		//So return FAILURE
		string strDotExpression = *szLastAttr;
		for(int i = 0; i < (int)m_svSecondaryAtt.size() ; i++)  	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
		{
			if(strDotExpression == m_svSecondaryAtt[i])
			{
				return FAILURE;
			}
		}
		//Before Checking it against primary attribute, because it may happen that it is MAX_VALUEn or MIN_VALUEn
		if(	( szTemp = strstr(*szLastAttr, "MAX_VALUE") ) != NULL || ( szTemp = strstr(*szLastAttr, "MIN_VALUE") ) != NULL )
		{
			char szMinOrMax[10];
			//As strlen(MIN_VALUE) is same as strlen("MAX_VALUE") only ine is Considered
			strncpy( szMinOrMax, *szLastAttr , strlen("MAX_VALUE"));
			szMinOrMax[strlen("MAX_VALUE")] = '\0';
			strDotExpression = szMinOrMax;

		}		
		//Check whether it is primary attribute
		m_MapDotOpNameToAttrIter = m_MapDotOpNameToAttr.find(strDotExpression);
		if(m_MapDotOpNameToAttrIter == m_MapDotOpNameToAttr.end())
		{
			//it is not a primary attribute. Good!! Reduces my work of resolution in latter part
			bPrimaryAttr = false;						
		}
		else
		{
			//it is a primary attribute . Not bad!! I need to take care of this in latter part
			bPrimaryAttr = true;
		}		
	}
	return SUCCESS;
}


/******************************************************************************************************/

/*	This Function is for To Get the DDexpression value and also Evalute the Assignment statement

	pIBFinal		 :ptr to an DD item of type variable
	pOpRef			 :DD item operative IDs
	ivVariableValue	 :Current var value
	CValueVarient	 :Value after evaluating 
	AssignType		 :Assignment type	
/******************************************************************************************************/
RETURNCODE MEE::GetDDVarValNEvalAssType(hCitemBase* pIBFinal, PARAM_REF *pOpRef, INTER_VARIANT* ivVariableValue, CValueVarient* pvtCV, RUL_TOKEN_SUBTYPE AssignType)
{
	int iRetVal = BLTIN_SUCCESS;


	if( (NULL == pIBFinal) || (pOpRef == NULL) )
	{
		return FAILURE;
	}

	//Get the current value Which may be needed if it has += kind of operation
	CValueVarient vtValue{};
	if ((m_pDevice) && (AssignType != RUL_ASSIGN))
	{
		iRetVal = m_pDevice->devAccessTypeValue2(pOpRef, &vtValue, READ);
		
		iRetVal = convertMixedCodeToMEECode(iRetVal);

		if (iRetVal != SUCCESS)
		{
			return iRetVal;
		}
	}


	INTER_VARIANT ivCurrentValue = INTER_VARIANT::InitFrom(&vtValue);
	INTER_VARIANT ivNewValue ;

	//find the variable item ID and member ID
	ITEM_ID itemID;
	ITEM_ID memberID;

	if (pOpRef->op_ref_type == STANDARD_TYPE)
	{
		itemID = pOpRef->op_info.id;
		memberID = pOpRef->op_info.member;
	}
	else	//COMPLEX_TYPE
	{
		itemID = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
		memberID = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].member;
	}

	//find the variable type from DDS
	CValueVarient vtType{};
	CStdString sProperty;
	sProperty.Format(_T("Type"));
	iRetVal = m_pDevice->ReadParameterData2( &vtType, sProperty, 0, 0, itemID, memberID );
	

	//get DD variable type
	variableType_t iVarType = vT_undefined;
	if (iRetVal == BLTIN_SUCCESS)
	{
		iVarType = (variableType_t)CV_I4(&vtType);
	}
	else
	{
		return FAILURE;
	}

	//find variable size from DDS
	CValueVarient vtSize{};
	sProperty.Empty();
	sProperty.Format(_T("Size"));
	iRetVal = m_pDevice->ReadParameterData2( &vtSize, sProperty, 0, 0, itemID, memberID );
	

	//get DD variable size
	unsigned int uiVarSize = 0;
	if (iRetVal == BLTIN_SUCCESS)
	{
		uiVarSize = CV_I4(&vtSize);
	}
	else
	{
		return FAILURE;
	}

	//do the operation with the Valid Operators
    //make sure the data wrapping happened 
    switch (AssignType)
	{
	case RUL_ASSIGN:
		{
			switch(iVarType)
			{
			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password: //Added By Anil 11th December 2005
			case  vT_EUC:
			case  vT_VisibleString:
				{
					wchar_t *szValue = NULL;	//using wchar_t* instead of char_t* to avoid calling TStr2AStr()
					
					ivVariableValue->GetStringValue(&szValue);
					wchar_t *szWithoutLanguageCode = NULL;					
					
					if(szValue != NULL)
					{
						RemoveLanguageCode(szValue,&szWithoutLanguageCode);
						ivNewValue  = szWithoutLanguageCode;
					}

					delete[] szValue;
					szValue = NULL;
						
					if(szWithoutLanguageCode)
					{
						delete[] szWithoutLanguageCode;
						szWithoutLanguageCode = NULL;
					}


				}
				break;

			case  vT_BitString:	// unsigned char[]
			case  vT_OctetString:
				{
					_BYTE_STRING theBS;

					theBS.bsLen = (unsigned long)ivVariableValue->GetMaxSize();
					uchar *pMem = new uchar[theBS.bsLen];
					theBS.bs = pMem;
					ivVariableValue->GetValue(theBS.bs, ivVariableValue->GetVarType());
					ivNewValue = theBS;

					delete [] pMem;
				}
				break;

			default:
				{
					VARIANT_TYPE type = variableType2VARIANTtype((variableType_t)iVarType, uiVarSize);
					ivNewValue = ivVariableValue->AssignWithType(type);
				}
			}
			break;
		}
	case RUL_PLUS_ASSIGN:
		{
			switch(iVarType)
			{
			case vT_Ascii:
			case vT_PackedAscii:
			case vT_Password: 
			case  vT_EUC:
			case  vT_VisibleString:
				{
					wchar_t *szValue = NULL;	//using wchar_t* instead of char_t* to avoid calling TStr2AStr()
					wchar_t *szCurrentValue = NULL;

					ivVariableValue->GetStringValue(&szValue);
					ivCurrentValue.GetStringValue(&szCurrentValue);

					wchar_t *szWithoutLanguageCode = NULL;					
					wchar_t *szCurrentWithoutLanguageCode = NULL;	

					if(szValue != NULL)
					{
						RemoveLanguageCode(szValue,&szWithoutLanguageCode);
						RemoveLanguageCode(szCurrentValue,&szCurrentWithoutLanguageCode);
						
						wstring tempString = (wstring)szCurrentWithoutLanguageCode + (wstring)szWithoutLanguageCode;
						ivNewValue  = tempString.c_str();
					}

					delete[] szValue;
					szValue = NULL;
						
					if(szWithoutLanguageCode)
					{
						delete[] szWithoutLanguageCode;
						szWithoutLanguageCode = NULL;
					}

					if(szCurrentValue)
					{
						delete[] szCurrentValue;
						szCurrentValue = NULL;
					}
					if(szCurrentWithoutLanguageCode)
					{
						delete[] szCurrentWithoutLanguageCode;
						szCurrentWithoutLanguageCode = NULL;
					}
				
				}
				break;

			case  vT_BitString:	// unsigned char[]
			case  vT_OctetString:
				return FAILURE;	// no addition operation
				break;

			default:
				{
					ivNewValue = ivCurrentValue  + (*ivVariableValue);
				}
			}
			break;
		}
	case RUL_MINUS_ASSIGN:
		{	
			ivNewValue = ivCurrentValue - (*ivVariableValue);
			break;
		}
	case RUL_DIV_ASSIGN:
		{	
			ivNewValue = ivCurrentValue / (*ivVariableValue);
			break;
		}
	case RUL_MUL_ASSIGN:
		{
			ivNewValue = ivCurrentValue * (*ivVariableValue);
			break;
		}
	case RUL_MOD_ASSIGN:
		{
			ivNewValue = ivCurrentValue % (*ivVariableValue);
			break;
		}			
	case RUL_BIT_AND_ASSIGN:
		{
			ivNewValue = ivCurrentValue & (*ivVariableValue);	
			break;
		}
	case RUL_BIT_OR_ASSIGN:
		{
			ivNewValue = ivCurrentValue | (*ivVariableValue);
			break;
		}
	case RUL_BIT_XOR_ASSIGN:
		{
			ivNewValue = ivCurrentValue ^ (*ivVariableValue);
			break;
		}
	case RUL_BIT_RSHIFT_ASSIGN:
		{
			ivNewValue = ivCurrentValue >> (*ivVariableValue);
			break;
		}
	case RUL_BIT_LSHIFT_ASSIGN:
		{
			ivNewValue = ivCurrentValue << (*ivVariableValue);
			break;
		}
	default:
		{
			return FAILURE;
			break;
		}			

	}
	//adjust output variable based on type and size
	switch (iVarType)
	{
	case vT_TimeValue:
		{
			switch (uiVarSize)
			{
			case 4:
				{
				unsigned int uiLocal = (unsigned int) ivNewValue;;
				uiLocal &= 0xFFFFFFFF;
				ivNewValue.Clear();	//clear old type before seting to a new type
				ivNewValue = (unsigned int)uiLocal;
				}
				break;
			default:
				{
				__int64 iLocal = (__int64) ivNewValue;;
				iLocal &= 0xFFFFFFFFFFFFFFFF;
				ivNewValue.Clear();	//clear old type before seting to a new type
				ivNewValue = (__int64)iLocal;
				}
				break;
			}
		}
		break;
	case vT_Integer:
	case vT_HartDate:
		{
			switch (uiVarSize)
			{
				case 1:
				{
				char cLocal = (char) ivNewValue;;
				cLocal &= 0xFF;
				ivNewValue.Clear();	//clear old type before seting to a new type
				ivNewValue = (char)cLocal;
				}
				break;
			case 2:
				{
				short sLocal = (short) ivNewValue;;
				sLocal &= 0xFFFF;
				ivNewValue.Clear();
				ivNewValue = (short)sLocal;
				}
				break;
			case 3:
				{
				int iLocal = (int) ivNewValue;
				if (iLocal & 0x800000)
				{
					//negative value
					iLocal |= 0xFF000000;
				}
				else
				{
					iLocal &= 0xFFFFFF;
				}
				ivNewValue.Clear();
				ivNewValue = (int)iLocal;
				}
				break;
			case 4:
				{
				int iLocal = (int) ivNewValue;;
				iLocal &= 0xFFFFFFFF;
				ivNewValue.Clear();
				ivNewValue = (int)iLocal;
				}
				break;
			case 5:
				{
				__int64 iLocal = (__int64) ivNewValue;;
				if (iLocal & 0x8000000000)
				{
					//negative value
					iLocal |= 0xFFFFFF0000000000;
				}
				else
				{
					iLocal &= 0xFFFFFFFFFF;
				}
				ivNewValue.Clear();
				ivNewValue = (__int64)iLocal;
				}
				break;
			case 6:
				{
				__int64 iLocal = (__int64) ivNewValue;;
				if (iLocal & 0x800000000000)
				{
					//negative value
					iLocal |= 0xFFFF000000000000;
				}
				else
				{
					iLocal &= 0xFFFFFFFFFFFF;
				}
				ivNewValue.Clear();
				ivNewValue = (__int64)iLocal;
				}
				break;
			case 7:
				{
				__int64 iLocal = (__int64) ivNewValue;;
				if (iLocal & 0x80000000000000)
				{
					//negative value
					iLocal |= 0xFF00000000000000;
				}
				else
				{
					iLocal &= 0xFFFFFFFFFFFFFF;
				}
				ivNewValue.Clear();
				ivNewValue = (__int64)iLocal;
				}
				break;
			default:
				{
				__int64 iLocal = (__int64) ivNewValue;;
				iLocal &= 0xFFFFFFFFFFFFFFFF;
				ivNewValue.Clear();
				ivNewValue = (__int64)iLocal;
				}
				break;
			}
		}
		break;
	case vT_Unsigned:
	case vT_Time:
	case vT_DateAndTime:
	case vT_Duration:
	case vT_Boolean:
	case vT_Enumerated:
	case vT_BitEnumerated:
	case vT_Index:
		{
			switch (uiVarSize)
			{
			case 1:
				{
				unsigned char ucLocal = (unsigned char) ivNewValue;;
				ucLocal &= 0xFF;
				ivNewValue.Clear();
				ivNewValue = (unsigned char)ucLocal;
				}
				break;
			case 2:
				{
				unsigned short usLocal = (unsigned short) ivNewValue;;
				usLocal &= 0xFFFF;
				ivNewValue.Clear();
				ivNewValue = (unsigned short)usLocal;
				}
				break;
			case 3:
				{
				unsigned int uiLocal = (unsigned int) ivNewValue;;
				uiLocal &= 0xFFFFFF;
				ivNewValue.Clear();
				ivNewValue = (unsigned int)uiLocal;
				}
				break;
			case 4:
				{
				unsigned int uiLocal = (unsigned int) ivNewValue;;
				uiLocal &= 0xFFFFFFFF;
				ivNewValue.Clear();
				ivNewValue = (unsigned int)uiLocal;
				}
				break;
			case 5:
				{
                _UINT64 uiLocal = (_UINT64) ivNewValue;;
				uiLocal &= 0xFFFFFFFFFF;
				ivNewValue.Clear();
                ivNewValue = (_UINT64)uiLocal;
				}
				break;
			case 6:
				{
                _UINT64 uiLocal = (_UINT64) ivNewValue;;
				uiLocal &= 0xFFFFFFFFFFFF;
				ivNewValue.Clear();
                ivNewValue = (_UINT64)uiLocal;
				}
				break;
			case 7:
				{
                _UINT64 uiLocal = (_UINT64) ivNewValue;;
				uiLocal &= 0xFFFFFFFFFFFFFF;
				ivNewValue.Clear();	//clear old type before seting to a new type
                ivNewValue = (_UINT64)uiLocal;
				}
				break;
			default:
				{
                _UINT64 uiLocal = (_UINT64) ivNewValue;;
				uiLocal &= 0xFFFFFFFFFFFFFFFF;
				ivNewValue.Clear();	//clear old type before seting to a new type
                ivNewValue = (_UINT64)uiLocal;
				}
				break;
			}
		}
		break;
	case vT_FloatgPt:
		{
			float fLocal = (float) ivNewValue;
			ivNewValue.Clear();	//clear old type before seting to a new type
			ivNewValue = (float) fLocal;
		}
		break;
	case vT_Double:
		{
			double dLocal = (double) ivNewValue;;
			ivNewValue.Clear();	//clear old type before seting to a new type
			ivNewValue = (double) dLocal;
		}
		break;
	}

	//convert INTER_VARIANT to CValueVarient
	ivNewValue.CopyTo(pvtCV);
	
	return iRetVal;
}


//This function returns a proper r-value based on input r-value and arithmetic operation
//pAttrName, pOpRef, ivVariableValue AND AssignType are inputs
//pvtValue is output
RETURNCODE MEE::GetDDAttrNEvalAssType(/* in */ string pAttrName, /* in */ PARAM_REF *pOpRef, /* in */ INTER_VARIANT* ivVariableValue, /* out */ CValueVarient* pvtValue, /* in */ RUL_TOKEN_SUBTYPE AssignType)
{
	int iRetVal = SUCCESS;


	if(pOpRef == NULL)
	{
		return FAILURE;
	}

	//Get the current value Which may be needed if it has += kind of operation
	CValueVarient vtCurrentValue{};
	if ((m_pDevice) && (AssignType != RUL_ASSIGN))
	{
		//convert dynamic attribute name to type
		varAttrType_t eAttrType = varAttrViewMin;
		if (pAttrName == "VIEW_MIN")
		{
			eAttrType = varAttrViewMin;
		}
		else if (pAttrName == "VIEW_MAX")
		{
			eAttrType = varAttrViewMax;
		}
		else if (pAttrName == "VARIABLE_STATUS")
		{
			eAttrType = varAttrVariableStatus;
		}
		else if (pAttrName == "FIRST")
		{
			eAttrType = varAttrFirst;
		}
		else if (pAttrName == "LAST")
		{
			eAttrType = varAttrLast;
		}
		//add here if there is more dynamic attribute

		iRetVal = m_pDevice->AccessDynamicAttributeMethod(pOpRef, &vtCurrentValue, eAttrType, READ);
		if (iRetVal != BLTIN_SUCCESS)
		{
			if (iRetVal == BI_ABORT)
			{
				iRetVal = MEE_ABORTED;
			}
			else
			{
				pvtValue->clear();
				iRetVal = MEE_ATTR_NOT_AVAILABLE;
			}
			return iRetVal;
		}
	}

	INTER_VARIANT ivCurrentValue = INTER_VARIANT::InitFrom(&vtCurrentValue);
	INTER_VARIANT ivNewValue ;

	//do the operation with the Valid Operators
    //make sure the data wrapping happened 
    switch (AssignType)
	{
	case RUL_ASSIGN:
		{
			ivNewValue = (*ivVariableValue);
			break;
		}
	case RUL_PLUS_ASSIGN:
		{
			ivNewValue = ivCurrentValue  + (*ivVariableValue);
			break;
		}
	case RUL_MINUS_ASSIGN:
		{	
			ivNewValue = ivCurrentValue - (*ivVariableValue);
			break;
		}
	case RUL_DIV_ASSIGN:
		{	
			ivNewValue = ivCurrentValue / (*ivVariableValue);
			break;
		}
	case RUL_MUL_ASSIGN:
		{
			ivNewValue = ivCurrentValue * (*ivVariableValue);
			break;
		}
	case RUL_MOD_ASSIGN:
		{
			ivNewValue = ivCurrentValue % (*ivVariableValue);
			break;
		}			
	case RUL_BIT_AND_ASSIGN:
		{
			ivNewValue = ivCurrentValue & (*ivVariableValue);	
			break;
		}
	case RUL_BIT_OR_ASSIGN:
		{
			ivNewValue = ivCurrentValue | (*ivVariableValue);
			break;
		}
	case RUL_BIT_XOR_ASSIGN:
		{
			ivNewValue = ivCurrentValue ^ (*ivVariableValue);
			break;
		}
	case RUL_BIT_RSHIFT_ASSIGN:
		{
			ivNewValue = ivCurrentValue >> (*ivVariableValue);
			break;
		}
	case RUL_BIT_LSHIFT_ASSIGN:
		{
			ivNewValue = ivCurrentValue << (*ivVariableValue);
			break;
		}
	default:
		{
			iRetVal = MEE_ATTR_NOT_AVAILABLE;
			break;
		}			
	}

	if (iRetVal == SUCCESS)
	{
		ivNewValue.CopyTo(pvtValue);
	}
	else
	{
		pvtValue->clear();
	}

	return iRetVal;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Helper function to get the Value of the DD item of type variable 
RETURNCODE MEE::GetDDVarVal(PARAM_REF *pOpRef, INTER_VARIANT* pvar)
{
	int iRetVal = MEE_BAD_PARAM_PASSED_IN;


	if (NULL == pOpRef)
	{
		return FAILURE;
	}

	if (m_pDevice)
	{
		CValueVarient vtValue{};
		iRetVal = m_pDevice->devAccessTypeValue2(pOpRef, &vtValue, READ);
		iRetVal = convertMixedCodeToMEECode(iRetVal);
		if (iRetVal == SUCCESS)
		{
			*pvar = INTER_VARIANT::InitFrom(&vtValue);
		}
		
	}

	return iRetVal;
}


//Helper function to get the Value of the DD item of type value array 
RETURNCODE MEE::GetDDArrayVal(/* in */ hCarray* phCArr, /* in */ PARAM_REF *pOpRef, /* out */ INTER_VARIANT* pvar)
{
	int iRetVal = MEE_BAD_PARAM_PASSED_IN;


	if( (NULL == phCArr) || (NULL == pOpRef) )
	{
		return FAILURE;
	}

	if (m_pDevice)
	{
		unsigned long ulCount = phCArr->getCount();
		unsigned short lSize = 0;
		VARIANT_TYPE vtArrElementType;
		_UCHAR *pMemory = NULL;
		_UCHAR *pTempMem = NULL;
		PARAM_REF opRef = *pOpRef;

		CValueVarient vtValue{};
		for (unsigned long i = 0; i < ulCount; i++)
		{
			//get each element item id and index as one based in MI
			if ((opRef.op_ref_type == STANDARD_TYPE) && (opRef.op_info.type != iT_ReservedZeta))
			{
				opRef.op_info.member = i + 1;
			}
			else if (opRef.op_ref_type == COMPLEX_TYPE)
			{
				opRef.op_info_list.list[opRef.op_info_list.count - 1].member = i + 1;
			}

			//get each element value
			iRetVal = m_pDevice->devAccessTypeValue2(&opRef, &vtValue,READ);
			iRetVal = convertMixedCodeToMEECode(iRetVal);
			if (iRetVal == SUCCESS)
			{
				switch (vtValue.vTagType)
				{
				case ValueTagType::CVT_I1:
					if (i == 0)
					{
						lSize = 1;
						vtArrElementType = RUL_CHAR;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_BOOL(&vtValue)), 1);
					pTempMem += 1;
					break;
				case ValueTagType::CVT_BOOL:
					if (i == 0)
					{
						lSize = 1;
						vtArrElementType = RUL_CHAR;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					
					memcpy(pTempMem, &(CV_I1(&vtValue)), 1);
					pTempMem += 1;
					break;
				case ValueTagType::CVT_UI1:
					if (i == 0)
					{
						lSize = 1;
						vtArrElementType = RUL_UNSIGNED_CHAR;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_UI1(&vtValue)), 1);
					pTempMem += 1;
					break;
				case ValueTagType::CVT_I2:
					if (i == 0)
					{
						lSize = 2;
						vtArrElementType = RUL_SHORT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_I2(&vtValue)), 2);
					pTempMem += 2;
					break;
				case ValueTagType::CVT_UI2:
					if (i == 0)
					{
						lSize = 2;
						vtArrElementType = RUL_USHORT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_UI2(&vtValue)), 2);
					pTempMem += 2;
					break;
				case ValueTagType::CVT_I4:
					if (i == 0)
					{
						lSize = 4;
						vtArrElementType = RUL_INT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}

					memcpy(pTempMem, &(CV_I4(&vtValue)), 4);
					pTempMem += 4;
					break;
				case ValueTagType::CVT_INT:
					if (i == 0)
					{
						lSize = 4;
						vtArrElementType = RUL_INT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					
					memcpy(pTempMem, &(CV_INT(&vtValue)), 4);
					pTempMem += 4;
					break;
				case ValueTagType::CVT_UI4:
					if (i == 0)
					{
						lSize = 4;
						vtArrElementType = RUL_UINT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}

					memcpy(pTempMem, &(CV_UI4(&vtValue)), 4);
					pTempMem += 4;
					break;
				case ValueTagType::CVT_UINT:
					if (i == 0)
					{
						lSize = 4;
						vtArrElementType = RUL_UINT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					
					memcpy(pTempMem, &(CV_UINT(&vtValue)), 4);
					pTempMem += 4;
					break;
				case ValueTagType::CVT_I8:
					if (i == 0)
					{
						lSize = 8;
						vtArrElementType = RUL_LONGLONG;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_I8(&vtValue)), 8);
					pTempMem += 8;
					break;
				case ValueTagType::CVT_UI8:
					if (i == 0)
					{
						lSize = 8;
						vtArrElementType = RUL_ULONGLONG;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
					memcpy(pTempMem, &(CV_UI8(&vtValue)), 8);
					pTempMem += 8;
					break;
				case ValueTagType::CVT_R4:
				{					
					if (i == 0)
					{
						lSize = 4;
						vtArrElementType = RUL_FLOAT;
						pMemory = new _UCHAR[ulCount * lSize];
						pTempMem = pMemory;
					}
				
					float fVal = (float)CV_R4(&vtValue);
					memcpy(pTempMem, &fVal, 4);
					pTempMem += 4;
					break;
				}
				case ValueTagType::CVT_R8:
					if (i == 0)
					{
						lSize = 8;
						vtArrElementType = RUL_DOUBLE;
						pTempMem = pMemory;
						pMemory = new _UCHAR[ulCount * lSize];
					}
					memcpy(pTempMem, &(CV_R8(&vtValue)), 8);
					pTempMem += 8;
					break;
				}
			}
		}	//end of for loop

		if (iRetVal == BLTIN_SUCCESS)
		{
			INTER_SAFEARRAY isArray(pMemory, ulCount, vtArrElementType, lSize);

			//output INTER_VARIANT
			*pvar = &isArray;
		}

		if (pMemory != NULL)
		{
			delete [] pMemory;
		}
	}

	return iRetVal;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This method is called by the DDComplex Expression with the Type DD_METHOD
//METHOD_ARG_INFO_VECTOR is a vector of Method argument, which contails all the related info regarding the Method parameter
//vectInterVar-- Value of the each Argument passed if it is passed by value and 
//should be filled by by interpreted vistor if it is passed by reference
//This function uses all the Helper functions for easy flow--this will easy for debugging in case of any issues
//szTokenName -- is the Called Method Name
//szComplexDDExpre- is the Called meth name with argument name
INTERPRETER_STATUS MEE::ResolveMethodExp(const char* /*szComplexDDExpre*/,const char* szTokenName,INTER_VARIANT* /*pvar*/,vector<INTER_VARIANT>* vectInterVar,METHOD_ARG_INFO_VECTOR* vectMethArgInfo)
{	

	hCitemBase*  pIB = NULL;
	string strDDitemName = szTokenName;
	long lMethodItemId =0;
	ParamList_t TempParamList;
	hCmethodParam phCmethodReturnValue;
	//get pIB from the szTokenName -- which is the Method name
	if((SUCCESS == m_pDevice->getItemBySymName(strDDitemName,&pIB))
	   && NULL != pIB
	   && (pIB)->IsValidTest()) // Invalids are not supported
	{
		hCmethod* phCmethod = (hCmethod*)pIB ;
		lMethodItemId = phCmethod->getID();		
		//Get the Parameter list which is used latter to fill the METHOD_ARG_INFO_VECTOR
		if(FAILURE == phCmethod->getMethodParams(TempParamList))
		{
			throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
		}
		//Get the return type Parameter : which is used latter to fill the METHOD_ARG_INFO_VECTOR for return type
		if(FAILURE == phCmethod->getMethodType(phCmethodReturnValue) )
		{
			throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
		}

	}
	else
	{
		throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
	}
	
	vector<string> strvCallerArgList;
	GetCallerArgList(vectMethArgInfo,&strvCallerArgList);

	//Update the caller Arg info
	if( FAILURE == UpdateCalledMethArgInfo( vectMethArgInfo, &TempParamList, &strvCallerArgList ))
	{
		throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
	}

	// Validate the Data type and other stuffs--Please go inside this method to understand
	if( FAILURE == ValidateMethodArgs( vectMethArgInfo,&TempParamList))
	{
		throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
	}

	//Cast Arg value to right type
	CastMethodArgValues(vectMethArgInfo, vectInterVar);

	//Update the retun Arg info
	if( FAILURE == UpdateRetunMethArgInfo( vectMethArgInfo,&phCmethodReturnValue,vectInterVar) )
	{
		throw(C_UM_ERROR_DDS_PARAM);		//wrong method parameter type
	}

	char szMessage[1024]={0};
	sprintf( szMessage, "Method-> Calling Method: %s\n", szTokenName );
	DEBUGLOG(CLOG_LOG,szMessage);

	//Now call the one meth and then instantiate the Separate interpreter for this called method
	IncreaseOneMethRefNo();
	OneMeth *pMeth = new OneMeth();
	INTERPRETER_STATUS intStatus = INTERPRETER_STATUS_EXECUTION_ERROR;

	try
	{
		//propagate protocol type from MEE to OneMeth instance
		pMeth->SetStartedProtocol(GetStartedProtocol());

		//save the original CInterpreter pointer
		CInterpreter	*orgMEEInterpreter = m_pMEEInterpreter;

		intStatus = pMeth->ExecuteMethod(this, m_pDevice, lMethodItemId, vectMethArgInfo, vectInterVar);
		DecreaseOneMethRefNo();
		DEBUGLOG(CLOG_LOG,"Method <-- Returning from method calling method.\n\n");
	
		//recover the original CInterpreter pointer
		m_pMEEInterpreter = orgMEEInterpreter;
	}
	catch (_INT32 error)
	{
		if (pMeth)
		{
			delete pMeth;
			pMeth = NULL;
		}
		throw error;
	}

	if(pMeth)
	{
		delete pMeth;
		pMeth  = NULL;
	}

	return intStatus;
}


//This function writes the input parameter to CMiEngine::m_evvMethodInputValue
void MEE::WriteMethodRetunedValue(INTER_VARIANT* pvar)
{
	nsConsumer::EVAL_VAR_VALUE returnedVal;

	pvar->CopyTo(&returnedVal);
	(void)m_pDevice->devWriteMethRtnVar(&returnedVal);
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This is an Overaloaded function and Copy cat of the earlier but with two additional Parameter
//METHOD_ARG_INFO_VECTOR - mContains the all the Argument info of the callded method
//vectInterVar -- Contains the value of the argument passed
//These two additional parameter is passed till Parser level
INTERPRETER_STATUS OneMeth::ExecuteMethod(MEE * pMEE, hCddbDevice *pDevice, long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg, vector<INTER_VARIANT>* vectInterVar)
{

	if (pDevice == NULL)
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	m_pMEE = pMEE; //Initialize the MEE pointer;

	m_pDevice = pDevice;

	INTERPRETER_STATUS intStatus = ExecMethod(lMethodItemId, vectMethArg, vectInterVar);

	if (m_bMethodAborted == true)
	{

		for( ListIterator = abortMethodList.begin();ListIterator != abortMethodList.end();ListIterator++ )
		{
			//when running abort method, turn off cancel signal
			m_pDevice->ClearDeviceCancel();

			long lQueueMethodId = *ListIterator;
			INTERPRETER_STATUS intAbortListStatus = ExecMethod(lQueueMethodId);
			if (intAbortListStatus != INTERPRETER_STATUS_OK)
			{
				m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"Abort Method Failed\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


				if (intStatus != INTERPRETER_STATUS_EXECUTION_CANCEL)
				{
					intStatus = intAbortListStatus;
				}
				break;
			}
			else
			{
				m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"Abort Method Passed\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");	

			}
		}

		m_pDevice->MILog(L"<Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"Method Process Aborted\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		m_pDevice->MILog(L"</Abort Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


		//After abort list execution is done and the original method return code is OKay, mark the return code to be aborted here
		if (intStatus == INTERPRETER_STATUS_OK)
		{
			intStatus = INTERPRETER_STATUS_EXECUTION_ABORT;
		}
	}

	return intStatus;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This is an Overaloaded function and Copy cat of the earlier but with two additional Parameter
//METHOD_ARG_INFO_VECTOR - mContains the all the Argument info of the callded method
//vectInterVar -- Contains the value of the argument passed
//These two additional parameter is passed till Parser level

INTERPRETER_STATUS	OneMeth::ExecMethod(long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar)
{
	/* First check if the Item Id is valid */
	hCitemBase*  p_ib = NULL;
	RETURNCODE rc = m_pDevice->getItemBySymNumber(lMethodItemId, &p_ib);
	if (!(rc == SUCCESS && p_ib != NULL && p_ib->getIType() == iT_Method))
	{
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	if (m_lMethodItemId != lMethodItemId)
	{
		m_lMethodItemId = lMethodItemId;
		if (m_pszDefinition != NULL)
		{
			delete [] m_pszDefinition;
			m_pszDefinition = NULL;
		}
	}

	wchar_t *wOrgMethDef = NULL;	//remember to release to memory

	hCmethod*	p_m = (hCmethod*)p_ib;
	size_t   sLen = p_m->getDef(m_pszDefinition, wOrgMethDef);

	if ((sLen == 0) || (m_pszDefinition == NULL))
	{
		if (wOrgMethDef != NULL)
		{
			delete [] wOrgMethDef;
			wOrgMethDef = NULL;
		}
		return INTERPRETER_STATUS_EXECUTION_ERROR;
	}

	//save method definition into log file
	//definition string used for searched by literal_string
	size_t defLen = wcslen(wOrgMethDef);
	wchar_t *sLogSearchPtr = wOrgMethDef;

	//adjusted definition used for printed into log file
	wchar_t *wNewMethDef = new wchar_t[2*defLen];	//make enough memory for new method definition
	PS_Wcscpy(wNewMethDef, 2*defLen, wOrgMethDef);
	wchar_t *pNewDefPtr = wNewMethDef;

	while (sLogSearchPtr)
	{
		wchar_t *sFoundPtr = wcsstr(sLogSearchPtr, _T("literal_string"));
		if (sFoundPtr != NULL)
		{
			//found
			size_t i = (sFoundPtr - sLogSearchPtr + strlen("literal_string"));
			pNewDefPtr += i;
			do
			{
				if (sLogSearchPtr[i] == _T('('))
				{
					//overwrite the literal_string argument with "xxx"
					wcsncpy(pNewDefPtr, _T("(xxx)"), 5);
					pNewDefPtr += 5;
				}
			} while (sLogSearchPtr[i++] != _T(')'));

			sLogSearchPtr += (i);
			//overwrite the rest of method definition
			wcscpy(pNewDefPtr, sLogSearchPtr);
		}
		else
		{
			//overwrite the rest of method definition
			wcscpy(pNewDefPtr, sLogSearchPtr);
			break;
		}
	}

	wstring wsMethName = AStr2TStr(p_m->getName());
	wstring wsMethArgStr = GetMethodArgString(vectMethArg, vectInterVar);
	wstring wsMethTitle = L"\n<Sub-Method: " + wsMethName + L"(" + wsMethArgStr + L")>\n";

	m_pDevice->MILog((wchar_t*)wsMethTitle.c_str(), nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	m_pDevice->MILog(wNewMethDef, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	m_pDevice->MILog(L"</Sub-Method>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	m_pDevice->MILog(L"<Function>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");


	if (wNewMethDef)
	{
		delete [] wNewMethDef;
	}

	if (wOrgMethDef)
	{
		delete [] wOrgMethDef;
	}

	char pErrorData[RULENAMELEN] = "";
	char *pbySymbolTable = NULL;
	INTERPRETER_STATUS intStatus = INTERPRETER_STATUS_INVALID;

	try
	{
		AllocLibrary();
		m_pBuiltinLib->Initialise (m_pDevice,m_pInterpreter,this);


		intStatus =	m_pInterpreter->ExecuteCode (m_pBuiltinLib, m_pszDefinition, "Test", pErrorData, pbySymbolTable, m_pMEE, vectMethArg, vectInterVar);


		m_pDevice->MILog(L"</Function>\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		if (pbySymbolTable != NULL)
		{
			wchar_t *wStr = c2w(pbySymbolTable);
			wchar_t* wStrMessage = new wchar_t[wcslen(wStr)+3];
			PS_VsnwPrintf(wStrMessage, wcslen(wStr) + 3, L"%s\n\n", wStr);
			m_pDevice->MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			//m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			//m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			if (wStr != NULL)
			{
				delete [] wStr;
			}
			if (wStrMessage != NULL)
			{
				delete[] wStrMessage;
			}
		}
		else
		{
			m_pDevice->MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		}
		

		if (intStatus != INTERPRETER_STATUS_OK)
		{
			DisplayMessageAndAbort(pErrorData);
		}

	}
	catch(_INT32 error)
	{
		if (error == C_UM_ERROR_METH_CANCELLED)
		{
			intStatus = INTERPRETER_STATUS_EXECUTION_CANCEL;
		}
		else
		{
			intStatus = (INTERPRETER_STATUS)error;
		}
	}

	if (pbySymbolTable != NULL)
	{
		delete [] pbySymbolTable;
	}

	FreeLibrary();
	return intStatus;
}


/***********************************************************************
 *
 *	Name:  GetMethodArgString
 *
 *	Description:
 *		This function get method argument type, argument name and argument value
 *
 *	Inputs:
 *		vectMethArg is a vector of argument variables
 *		vectInterVar is a vactor of argument values
 *
 *	Returns:
 *		wide string of method argument type, argument name and argument value
 *
 **********************************************************************/
wstring OneMeth::GetMethodArgString(METHOD_ARG_INFO_VECTOR* vectMethArg, vector<INTER_VARIANT>* vectInterVar)
{
	wstring wsMethArg;
	RUL_TOKEN_SUBTYPE eTokenSubType = RUL_SUBTYPE_NONE;
	size_t uiNoOfArgs = vectMethArg->size() - 1;		//don't get returned parameter
	bool bIsDD_ITEM = false;


	//Extract all the Method parameter from the Parameter list
	for(size_t i = 0; i < uiNoOfArgs; i++)
	{
		if (i > 0)
		{
			wsMethArg += _T(", ");
		}

		bIsDD_ITEM = false;
		eTokenSubType = (*vectMethArg)[i].GetSubType();

		//Extract the Method parameter type
		switch(eTokenSubType)
		{
			case RUL_BOOLEAN_DECL:
				{
					wsMethArg += _T("bool ");
				}
				break;
			case RUL_CHAR_DECL:
				{
					wsMethArg += _T("char ");
				}
				break;
			case RUL_UNSIGNED_CHAR_DECL:
				{
					wsMethArg += _T("unsigned char ");
				}
				break;
			case RUL_SHORT_INTEGER_DECL:
				{
					wsMethArg += _T("short ");
				}
				break;

			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
				{
					wsMethArg += _T("unsigned short ");
				}
				break;
			case RUL_LONG_DECL:
				{
					wsMethArg += _T("long ");
				}
				break;
			case RUL_INTEGER_DECL:			
				{
					wsMethArg += _T("int ");
				}
				break;
			case RUL_UNSIGNED_INTEGER_DECL:			
				{
					wsMethArg += _T("unsigned int ");
				}
				break;
			case RUL_LONG_LONG_DECL:			
				{
					wsMethArg += _T("long long ");
				}
				break;
			case RUL_UNSIGNED_LONG_LONG_DECL:
				{
					wsMethArg += _T("unsigned long long ");
				}
				break;
			case RUL_DOUBLE_DECL:
				{
					wsMethArg += _T("double ");
				}
				break;
			case RUL_REAL_DECL:			
				{
					wsMethArg += _T("float ");
				}
				break;
			case RUL_STRING_DECL:
				{
					wsMethArg += _T("char* ");
				}
				break;
			case RUL_DD_STRING_DECL:			
				{
					wsMethArg += _T("DD_STRING ");
				}
				break;
			case RUL_DD_SIMPLE:
			case RUL_DD_COMPLEX:
				{
					wsMethArg += _T("DD_ITEM ");
					bIsDD_ITEM = true;
				}
				break;
			case RUL_TIMET_DECL:
				{
					wsMethArg += _T("time_t ");
				}
				break;
		
			case RUL_ARRAY_DECL:
			default:
				{
					wsMethArg += _T("");
				}
				break;
		}//end switch

		//if the method parameter is a reference such as DD_ITEM or reference declared by '&' operator but the method parameter is not an array,
		//get the reference item IDs or reference name without input value
		//for example, if a function is declared as f(DD_ITEM x, int &y) and the caller is f(x1, y1),
		//the log will look like: f(DD_ITEM ref ID=? Member=? Type=?, int &y = &y1) 
		//where ? marks are DD simple variable x item ID, member ID and item type respectively
		if ( ((*vectMethArg)[i].ePassedType == DD_METH_AGR_PASSED_BYREFERENCE) 
			 && ((*vectMethArg)[i].GetType() != RUL_ARRAY_VARIABLE) && (eTokenSubType != RUL_DD_STRING_DECL) )
		{
			wstring wsTemp;

			//a reference declared by '&' operator may or may not be a DD_ITEM
			if (!bIsDD_ITEM)
			{
				//It must be a reference declared by '&' operator which is not a DD_ITEM,
				//get the reference name led by '&' and followed by '='
				wsMethArg += _T("&");
				wsTemp = AStr2TStr((*vectMethArg)[i].GetCalledArgName());
				wsMethArg += wsTemp.c_str();
				wsMethArg += _T(" = ");
			}

			if ((*vectMethArg)[i].prDDItemId.isOperationalIDValid())
			{
				if (bIsDD_ITEM)
				{
					//It must be a reference declared by '&' operator which is a DD_ITEM,
					//Get DD_ITEM reference name followed by '='
					wsTemp = AStr2TStr((*vectMethArg)[i].GetCalledArgName());
					wsMethArg += wsTemp.c_str();
					wsMethArg += _T(" = ");
				}

				//It is a DD_ITEM, get the reference item IDs
                wchar_t wsParamRef[MAX_DD_STRING]{0};
				PARAM_REF paramRef = (*vectMethArg)[i].prDDItemId;

				if (paramRef.op_ref_type == COMPLEX_TYPE)
				{		
					wsMethArg += _T("Complex Ref:\n");

					for (unsigned short i = 0; i < paramRef.op_info_list.count; i++)
					{
						wsTemp = GetItemTypeString(paramRef.op_info_list.list[i].type);
						PS_VsnwPrintf(wsParamRef, MAX_DD_STRING, L"\tItem ID=%u Member=%u Type=%s", paramRef.op_info_list.list[i].id, paramRef.op_info_list.list[i].member, wsTemp.c_str());
						wsMethArg += wsParamRef;
					}				
				}
				else	//standard
				{
					wsTemp = GetItemTypeString(paramRef.op_info.type);
					PS_VsnwPrintf(wsParamRef, MAX_DD_STRING, L"Ref ID=%u Member=%u Type=%s", paramRef.op_info.id, paramRef.op_info.member, wsTemp.c_str());
					wsMethArg += wsParamRef;
				}
			}
			else
			{
				//It is a reference declared by '&' operator, get the assigning variable name led by '&'.
				wsMethArg += _T("&");
				wsTemp = AStr2TStr((*vectMethArg)[i].GetCallerArgName());
				wsMethArg += wsTemp.c_str();
			}

			//for refrence parameter, don't print value so that continue the loop here
			continue;
		}

		//Extract the Method parameter name
		if ((*vectMethArg)[i].GetType() == RUL_ARRAY_VARIABLE)
		{
			wsMethArg += AStr2TStr((*vectMethArg)[i].GetCalledArgName()) + _T("[] = ");
		}
		else
		{
			wsMethArg += AStr2TStr((*vectMethArg)[i].GetCalledArgName()) + _T(" = ");
		}

		//Extract the Method parameter value
		switch((*vectInterVar)[i].GetVarType())
		{
			case RUL_BOOL:
				{
					bool value = (bool)(*vectInterVar)[i];
					if (value)
					{
						wsMethArg += _T("true");
					}
					else
					{
						wsMethArg += _T("false");
					}
				}
				break;
			case RUL_CHAR:
				{
					long long value = (char)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_UNSIGNED_CHAR:
				{
					unsigned long long value = (unsigned char)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_SHORT:
				{
					long long value = (short)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;

			case RUL_USHORT:
				{
					unsigned long long value = (unsigned short)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_LONGLONG:			
				{
					long long value = (long long)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_ULONGLONG:
				{
					unsigned long long value = (unsigned long long)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_INT:
				{
					long long value = (int)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_UINT:			
				{
					unsigned long long value = (unsigned int)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_DOUBLE:
				{
					long double value = (double)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			case RUL_FLOAT:
				{
					long double value = (float)(*vectInterVar)[i];
					wsMethArg += to_wstring(value);
				}
				break;
			default:
				{
					//must be string value
					if (!(*vectInterVar)[i].isNumeric())
					{
						wchar_t *pwsValue = NULL;
						(*vectInterVar)[i].GetStringValue(&pwsValue, true);
						wsMethArg += pwsValue;

						if(pwsValue != NULL)
						{
							delete[] pwsValue;	//delete memory that is allocated by GetStringValue()
						}
					}
				}
				break;
		}//end switch
	}	//end for

	return wsMethArg;
}


//This function checks if method return type is defined.
//If it is, this function initialize the returned variable.
int OneMeth::CheckMethodReturnedVariable(hCmethod* pMeth)
{
	m_pMEE->SetIsMethReturnType(false);

	hCmethodParam phCmethodReturnValue;
	int retVal = pMeth->getMethodType(phCmethodReturnValue);
	if(SUCCESS != retVal)
	{
		return retVal;
	}

	m_pMEE->SetIsMethReturnType(true);

	nsConsumer::EVAL_VAR_VALUE returnedVal;
	switch(phCmethodReturnValue.getType())
	{
		case methodVarChar:
			{
				returnedVal.size = 1;
				returnedVal.type = nsEDDEngine::VT_INTEGER;
				returnedVal.val.i = 0;
			}
			break;
		case methodVar_U_Char:
			{
				returnedVal.size = 1;
				returnedVal.type = nsEDDEngine::VT_UNSIGNED;
				returnedVal.val.u = 0;
			}
			break;
		case methodVarShort:
			{
				returnedVal.size = 2;
				returnedVal.type = nsEDDEngine::VT_INTEGER;
				returnedVal.val.i = 0;
			}
			break;
		case methodVar_U_Short:
			{
				returnedVal.size = 2;
				returnedVal.type = nsEDDEngine::VT_UNSIGNED;
				returnedVal.val.u = 0;
			}
			break;
		case methodVarLongInt:
			{
				returnedVal.size = 4;
				returnedVal.type = nsEDDEngine::VT_INTEGER;
				returnedVal.val.i = 0;
			}
			break;
		case methodVar_U_LongInt:
			{
				returnedVal.size = 4;
				returnedVal.type = nsEDDEngine::VT_UNSIGNED;
				returnedVal.val.u = 0;
			}
			break;
		case methodVar_U_Int64:
			{
				returnedVal.size = 8;
				returnedVal.type = nsEDDEngine::VT_INTEGER;
				returnedVal.val.i = 0;
			}
			break;
		case methodVarInt64:
			{
				returnedVal.size = 8;
				returnedVal.type = nsEDDEngine::VT_UNSIGNED;
				returnedVal.val.u = 0;
			}
			break;

		case methodVarDouble:
			{
				returnedVal.size = 8;
				returnedVal.type = nsEDDEngine::VT_DOUBLE;
				returnedVal.val.d = 0.0;
			}
			break;
		case methodVarFloat:			
			{
				returnedVal.size = 4;
				returnedVal.type = nsEDDEngine::VT_FLOAT;
				returnedVal.val.f = 0.0;
			}
			break;

		case methodVarDDString:			
			{
				returnedVal.size = 1024;
				returnedVal.type = nsEDDEngine::VT_ASCII;
				returnedVal.val.s.assign(L"",false);;
			}
			break;

		case methodVarDDItem:
		case methodVarVoid:
		default:
			m_pMEE->SetIsMethReturnType(false);
			break;
	}//end switch

	//The following code is used to initialize method returned variable with type, size and initial value.
	//The following code shall be un-commented if the initialization is important to catch DD mistake.
	//if (m_pMEE->GetIsMethReturnType())
	//{
	//	retVal = m_pDevice->devWriteMethRtnVar(&returnedVal);
	//}

	return retVal;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Helper function  ot get the Caller Argument name list which ar estored in the vector
RETURNCODE MEE::GetCallerArgList(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,vector<string>* strvCallerArgList)
{
	//Loop through each of the entries in the 
	size_t iSize = vectMethArgInfo->size();
	for(size_t iCount = 0; iCount < iSize ; iCount++)
	{
		METHOD_ARG_INFO* cMethodArgInfo = &(*vectMethArgInfo)[iCount] ;
		if( cMethodArgInfo )
		{
		strvCallerArgList->push_back(cMethodArgInfo->GetCallerArgName());
		}// else ??? error, logit?????

	}
	return SUCCESS;
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This function is to validate the methos Agrument, basically the data type of the Arguement
//Please refer the below one to how the maping is done from the Param list got from the Method
//Class to the interpreter data type, so basically these should match when one method calls the other
//i.e from the the caller, data type which he is passed should match to the called method
RETURNCODE MEE::ValidateMethodArgs(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,ParamList_t* TempParamList)
{
	if(vectMethArgInfo->size() != TempParamList->size() )
	{
		//return failure as it is not a valid arg list
		return FAILURE;
		
	}
	size_t uiNoOfArgs = vectMethArgInfo->size();
	//Extract all the Method parameters from the Parameter list
	for(size_t i = 0; i< uiNoOfArgs ; i++)
	{
		RUL_TOKEN_TYPE eTokenType = RUL_TYPE_NONE;
		RUL_TOKEN_SUBTYPE eTokenSubType = RUL_SUBTYPE_NONE;
		METHOD_ARG_INFO cMethodArgInfo;
		hCmethodParam chCmethodParam;


		//Get the hcMetod param for each param
		chCmethodParam = (*TempParamList)[i];

		//Chech the data type paased and Data type Defined in the Method
		cMethodArgInfo = (*vectMethArgInfo)[i];

		//Below is the mapping from DevService Variable type to interpreter Var type
		/*enum methodVarType_e
		{
			methodVarVoid--------------RUL_NULL	
			
			methodVarChar--------------RUL_CHAR
			methodVarShort-------------RUL_CHAR

			methodVarLongInt-----------RUL_INT
			methodVarFloat-------------RUL_FLOAT
			methodVarDouble------------RUL_DOUBLE
			methodVar_U_Char-----------?
			methodVar_U_Short----------?
			methodVar_U_LongInt--------?
			methodVarInt64-------------?
			methodVar_U_Int64----------?
			methodVarDDString----------RUL_DD_STRING
			methodVarDDItem------------RUL_DD_COMPLEX //I would set this!!
			methodVar_Unknow-----------RUL_NULL
		}*/
		//
		//Now Compare with the the passed type
		switch(chCmethodParam.getType())
		{
			case methodVarChar:
			// Walt EPM 08sep08 - added/modified
				{
					eTokenSubType = RUL_CHAR_DECL;
				}
				break;
			case methodVar_U_Char:
				{
					eTokenSubType = RUL_UNSIGNED_CHAR_DECL;
				}
				break;
			case methodVarShort:
				{
					eTokenSubType = RUL_SHORT_INTEGER_DECL;
				}
				break;

			case methodVar_U_Short:
				{
					eTokenSubType = RUL_UNSIGNED_SHORT_INTEGER_DECL;
				}
				break;
			case methodVar_U_LongInt:			
				{
					eTokenSubType = RUL_UNSIGNED_INTEGER_DECL;
				}
				break;
			case methodVar_U_Int64:
				{
					eTokenSubType = RUL_UNSIGNED_LONG_LONG_DECL;
				}
				break;
			case methodVarInt64:			
				{
					eTokenSubType = RUL_LONG_LONG_DECL;
				}
				break;
				// Walt EPM 08sep08 - finish added / modified

			case methodVarLongInt:			
				{
					eTokenSubType = RUL_INTEGER_DECL;
				}
				break;

			case methodVarDouble:
				{
					eTokenSubType = RUL_DOUBLE_DECL;//WS:EPM 10aug07
				}
				break;

			case methodVarFloat:			
				{
					eTokenSubType = RUL_REAL_DECL;
				}
				break;

			case methodVarDDString:			
				{
					eTokenSubType = RUL_DD_STRING_DECL;
				}
				break;
			case methodVarDDItem:
				{
					eTokenSubType = RUL_DD_COMPLEX;
				}
				break;
		
			default:
				{
					
				//error!!!!
				}
				break;

		}//end switch

		//check for the DD_ITEM, Simple variable and Array type
		// Get the token Type from the param list
		eTokenType = (chCmethodParam.getType() == methodVarDDItem) ? RUL_DD_ITEM : (chCmethodParam.isArray() ? RUL_ARRAY_VARIABLE : RUL_SIMPLE_VARIABLE);
		
		//If the token Sub type is not matching, then it is an error
		// Walt EPM 08sep08 - added a couple
		switch( cMethodArgInfo.GetSubType() )
		{
			case RUL_UNSIGNED_CHAR_DECL:
			case RUL_SHORT_INTEGER_DECL:
			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
			case RUL_INTEGER_DECL:
			case RUL_UNSIGNED_INTEGER_DECL:
			case RUL_LONG_LONG_DECL:
			case RUL_UNSIGNED_LONG_LONG_DECL:
			case RUL_LONG_DECL:
			case RUL_REAL_DECL: 
			case RUL_DOUBLE_DECL:			
			case RUL_TIMET_DECL:
				switch( eTokenSubType )
				{
					case RUL_CHAR_DECL:
					case RUL_UNSIGNED_CHAR_DECL:
					case RUL_SHORT_INTEGER_DECL:
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					case RUL_INTEGER_DECL:
					case RUL_UNSIGNED_INTEGER_DECL:
					case RUL_LONG_LONG_DECL:
					case RUL_UNSIGNED_LONG_LONG_DECL:
					case RUL_LONG_DECL:
					case RUL_REAL_DECL: 
					case RUL_DOUBLE_DECL:
					case RUL_TIMET_DECL:
						if ((eTokenType == RUL_SIMPLE_VARIABLE)
							&& (cMethodArgInfo.GetType() == RUL_ARRAY_VARIABLE))
						{
							//input argument is passed by an array element. we treat it as simple variable here.
							cMethodArgInfo.SetType(RUL_SIMPLE_VARIABLE);
							(*vectMethArgInfo)[i].SetType(RUL_SIMPLE_VARIABLE);
						}
						break;
					default:
						return FAILURE;
				}		
				break;
			case RUL_CHAR_DECL:
			case RUL_CHAR_CONSTANT:
			case RUL_INT_CONSTANT:
			case RUL_REAL_CONSTANT:
			case RUL_BOOL_CONSTANT:
			case RUL_STRING_CONSTANT:
				switch( eTokenSubType )
				{
					case RUL_CHAR_DECL:
					case RUL_UNSIGNED_CHAR_DECL:
					case RUL_SHORT_INTEGER_DECL:
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					case RUL_INTEGER_DECL:
					case RUL_UNSIGNED_INTEGER_DECL:
					case RUL_LONG_LONG_DECL:
					case RUL_UNSIGNED_LONG_LONG_DECL:
					case RUL_LONG_DECL:
					case RUL_REAL_DECL: 
					case RUL_DOUBLE_DECL:
					case RUL_STRING_DECL:
					case RUL_DD_STRING_DECL:
					case RUL_TIMET_DECL:
						break;
					default:
						return FAILURE;
				}
				break;
			case RUL_STRING_DECL:
			case RUL_DD_STRING_DECL:
				switch( eTokenSubType )
				{
					case RUL_STRING_DECL:
					case RUL_DD_STRING_DECL:
					case RUL_CHAR_DECL:
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
						break;
					default:
						return FAILURE;
				}
				break;
			case RUL_DD_COMPLEX:
			case RUL_DD_SIMPLE:
			case RUL_ARRAY_DECL:
				break;
			case RUL_SUBTYPE_NONE:
				//set type and subtype here from parameter definition for compound expression
				if (eTokenType != RUL_DD_ITEM)
				{
					cMethodArgInfo.SetType(eTokenType);
					cMethodArgInfo.SetSubType(eTokenSubType);
					(*vectMethArgInfo)[i].SetType(eTokenType);
					(*vectMethArgInfo)[i].SetSubType(eTokenSubType);
				}
				else
				{
					return FAILURE;
				}
				break;
			default: 
				if( cMethodArgInfo.GetSubType() != eTokenSubType )
				{
					return FAILURE;
				}
		}	
		// Walt EPM 08sep08 - finished adding a couple	

		//Update argument sub-type or type with the type defined in DD if two types are different
		//This update is important so that the function CastMethodArgValues() can cast argument value to correct value type.
		//For example, if the method is f(int x) and the caller is f(a) while value 'a' is in type of long long,
		//the following code detects whether variable 'x' type is the same as value 'a' type. If they are different,
		//change the argument 'a' type from long long to int.
		if (cMethodArgInfo.GetSubType() != eTokenSubType)
		{
			if ( (cMethodArgInfo.GetType() == RUL_SIMPLE_VARIABLE) && (eTokenType == RUL_ARRAY_VARIABLE) 
				 && (cMethodArgInfo.GetSubType() == RUL_DD_STRING_DECL) && (eTokenSubType == RUL_CHAR_DECL) )
			{
				//update both type and subtype from DD_STRING to char array
				(*vectMethArgInfo)[i].SetType(RUL_ARRAY_VARIABLE);
				(*vectMethArgInfo)[i].SetSubType(RUL_CHAR_DECL);
				cMethodArgInfo.SetType(RUL_ARRAY_VARIABLE);
			}
			else if ( (cMethodArgInfo.GetType() == RUL_ARRAY_VARIABLE) && (eTokenType == RUL_SIMPLE_VARIABLE) 
				 && (cMethodArgInfo.GetSubType() == RUL_CHAR_DECL) && (eTokenSubType == RUL_DD_STRING_DECL) )
			{
				//update both type and subtype from char array to DD_STRING
				(*vectMethArgInfo)[i].SetType(RUL_SIMPLE_VARIABLE);
				(*vectMethArgInfo)[i].SetSubType(RUL_DD_STRING_DECL);
				cMethodArgInfo.SetType(RUL_SIMPLE_VARIABLE);
			}
			else if (cMethodArgInfo.GetType() != RUL_ARRAY_VARIABLE)
			{
				//Change the argument type to the same one defined in DD
				(*vectMethArgInfo)[i].SetSubType(eTokenSubType);
			}
		}

		//method parameter type has to be RUL_DD_ITEM if the argument is a reference defined in DD such as & operator
		if (cMethodArgInfo.GetType() == RUL_DD_ITEM)
		{
			eTokenType = RUL_DD_ITEM;
		}

		//if the token type is not same then it
		if(eTokenType != cMethodArgInfo.GetType() )
		{
			return FAILURE;
		}

	} //end of 'for' loop
		
	return SUCCESS;

}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This method id to get the Called method information and update the METHOD_ARG_INFO_VECTOR class 
//for each of the parameter
//Get the Arguement name , and fill it in the class
//get the Passed type, i.e whether it is passed by value or by reference
RETURNCODE MEE::UpdateCalledMethArgInfo(METHOD_ARG_INFO_VECTOR*  vectMethArgInfo, 
										ParamList_t*             TempParamList,
										vector<string>*          strvCallerArgList)
{
	if(vectMethArgInfo->size() != TempParamList->size() )
	{
		//return failure as it is not a valid arg list
		return FAILURE;		
	}

	size_t uiNoOfArgs = vectMethArgInfo->size();
	for(size_t uiArgIndex = 0; uiArgIndex< uiNoOfArgs ; uiArgIndex++)
	{
		METHOD_ARG_INFO* cMethodArgInfo = &(*vectMethArgInfo)[uiArgIndex] ;
		hCmethodParam chCmethodParam = (*TempParamList)[uiArgIndex];

		//Get each of the argument
		string strCalledName = chCmethodParam.getName();
		cMethodArgInfo->SetCalledArgName(strCalledName.c_str());
		
		//get the passed type
		cMethodArgInfo->ePassedType = (chCmethodParam.isRef()) ? DD_METH_AGR_PASSED_BYREFERENCE
															   : DD_METH_AGR_PASSED_BYVALUE;
		//if method argument is in reference type (DD item or & item), do the following:
		if (chCmethodParam.isRef())
		{
			string strTemp = (*strvCallerArgList)[uiArgIndex];
			char* szLastAttr = NULL;
			bool bPrimaryAttr = false;
			char* pchPassedValue = new char[strTemp.size()+1];
			PS_Strcpy(pchPassedValue, strTemp.size()+1, strTemp.c_str());

			//Just Validate the Expression , if it is an DD expression
			if(FAILURE == ValidateExp(pchPassedValue, &szLastAttr,bPrimaryAttr) )
			{
				DELETE_ARR(szLastAttr);
				DELETE_ARR(pchPassedValue);
				return FAILURE;
			}

			//If it is primary attribute like LABEL, then it is an error as it is not DD item
			if(bPrimaryAttr == true)
			{
				DELETE_ARR(szLastAttr);
				DELETE_ARR(pchPassedValue);
				return FAILURE;
			}

			DELETE_ARR(szLastAttr);
			DELETE_ARR(pchPassedValue);
		}		
		else if (cMethodArgInfo->prDDItemId.isOperationalIDValid())
		{
			//clear cMethodArgInfo->prDDItemId so that method will not output argument value
			(cMethodArgInfo->prDDItemId).~PARAM_REF();
			cMethodArgInfo->SetType(RUL_TYPE_NONE);
			cMethodArgInfo->SetSubType(RUL_SUBTYPE_NONE);
		}
	}
	return SUCCESS;
}


/****************************************************************************************
 *
 *	Name:  CastMethodArgValues
 *
 *	Description:
 *		This function casts method argument value to the type that input vectMethArgInfo
 *		provides, if the argument value type is different.
 *		This function is called before a sub-method is executed.
 *
 *	Input:
 *		vectMethArgInfo is a list of method argument variable info.
 *		The info contains argument type of simple variable, of DD_ITEM, of array,
 *		and argument sub-type, such as int, float, DD_STRING etc.
 *		If the argument type is DD_ITEM, the info also contains
 *		reference item IDs.
 *
 *	Input/Output:
 *		vectInterVar is a list of argument values.
 *		The argument value actually contains method caller intial value
 *		and intial value type. As output, the argument value
 *		shall be casted to the same type as vectMethArgInfo provides.
 *
 *	Returns:
 *		void
 *
 ****************************************************************************************/
void MEE::CastMethodArgValues(/*in*/METHOD_ARG_INFO_VECTOR* vectMethArgInfo, /*in/out*/vector<INTER_VARIANT>* vectInterVar)
{
	RUL_TOKEN_SUBTYPE eTokenSubType = RUL_SUBTYPE_NONE;
	size_t uiNoOfArgs = vectMethArgInfo->size();
	__VAL value;


	for(size_t i = 0; i < uiNoOfArgs; i++)
	{
		//skip output variable on DD_ITEM and array
		value = (*vectInterVar)[i].GetValue();
		if (((*vectMethArgInfo)[i].ePassedType == DD_METH_AGR_PASSED_BYREFERENCE)
			&& (((*vectMethArgInfo)[i].prDDItemId.isOperationalIDValid())
				|| (((*vectMethArgInfo)[i].GetType() == RUL_ARRAY_VARIABLE) && ((*vectMethArgInfo)[i].GetSubType() != RUL_CHAR_DECL) && ((*vectInterVar)[i].GetVarType() == RUL_SAFEARRAY) && ((value.prgsa)->Type() != RUL_CHAR))
				|| (((*vectMethArgInfo)[i].GetType() == RUL_ARRAY_VARIABLE) && ((*vectMethArgInfo)[i].GetSubType() == RUL_CHAR_DECL) && ((*vectInterVar)[i].GetVarType() == RUL_SAFEARRAY) && ((value.prgsa)->Type() == RUL_CHAR))
				|| (((*vectMethArgInfo)[i].GetType() == RUL_SIMPLE_VARIABLE) && ((*vectMethArgInfo)[i].GetSubType() == RUL_DD_STRING_DECL) && ((*vectInterVar)[i].GetVarType() == RUL_SAFEARRAY) && ((value.prgsa)->Type() == RUL_USHORT))
			   )
		   )
		{
			continue;
		}

		//cast argument value if the type is not right
		eTokenSubType = (*vectMethArgInfo)[i].GetSubType();
		switch(eTokenSubType)
		{
			case RUL_BOOLEAN_DECL:
			case RUL_BOOL_CONSTANT:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_BOOL)
					{
						bool bVal = (bool)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&bVal, RUL_BOOL);
					}
				}
				break;
			case RUL_CHAR_DECL:
				{
					if ((*vectMethArgInfo)[i].GetType() == RUL_SIMPLE_VARIABLE)
					{
						if ((*vectInterVar)[i].GetVarType() != RUL_CHAR)
						{
							char cVal = (char)(*vectInterVar)[i];
							(*vectInterVar)[i].SetValue(&cVal, RUL_CHAR);
						}
					}
					else if ((*vectMethArgInfo)[i].GetType() == RUL_ARRAY_VARIABLE)
					{
						if ((*vectInterVar)[i].GetVarType() == RUL_SAFEARRAY)
						{
							if ((value.prgsa)->Type() == RUL_USHORT)
							{
								//this is a char[] variable. The array type shall be RUL_CHAR
								INTER_VARIANT temp = (*vectInterVar)[i];

								_UINT32 numberOfElements = (value.prgsa)->GetNumberOfElements();
								_UINT32 elementSize = (value.prgsa)->GetElementSize();

								(*vectInterVar)[i].Clear();

								//initial destination array with the same memory size
								INTER_SAFEARRAYBOUND rgbound[1] = { numberOfElements * elementSize };
								INTER_SAFEARRAY sa;

								sa.SetAllocationParameters(RUL_CHAR, 1, rgbound);

								//cast wchar_t* string to char* string
								INTER_VARIANT temp2;
								temp2 = &sa;
								temp2 = temp;

								//element in a vector shall be assinged only once.
								//Otherwise, HEAP corruption may occur.
								(*vectInterVar)[i] = temp2;
							}
						}
					}
				}
				break;
			case RUL_CHAR_CONSTANT:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_CHAR)
					{
						char cVal = (char)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&cVal, RUL_CHAR);
					}
				}
				break;
			case RUL_UNSIGNED_CHAR_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_UNSIGNED_CHAR)
					{
						unsigned char ucVal = (unsigned char)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&ucVal, RUL_UNSIGNED_CHAR);
					}
				}
				break;
			case RUL_SHORT_INTEGER_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_SHORT)
					{
						short stVal = (short)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&stVal, RUL_SHORT);
					}
				}
				break;

			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_USHORT)
					{
						unsigned short ustVal = (unsigned short)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&ustVal, RUL_USHORT);
					}
				}
				break;
			case RUL_INTEGER_DECL:			
			case RUL_INT_CONSTANT:
			case RUL_LONG_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_INT)
					{
						int iVal = (int)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&iVal, RUL_INT);
					}
				}
				break;
			case RUL_UNSIGNED_INTEGER_DECL:			
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_UINT)
					{
						unsigned int uiVal = (unsigned int)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&uiVal, RUL_UINT);
					}
				}
				break;
			case RUL_LONG_LONG_DECL:			
			case RUL_TIMET_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_LONGLONG)
					{
						long long llVal = (long long)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&llVal, RUL_LONGLONG);
					}
				}
				break;
			case RUL_UNSIGNED_LONG_LONG_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_ULONGLONG)
					{
						unsigned long long ullVal = (unsigned long long)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&ullVal, RUL_ULONGLONG);
					}
				}
				break;
			case RUL_DOUBLE_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_DOUBLE)
					{
						double dVal = (double)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&dVal, RUL_DOUBLE);
					}
				}
				break;
			case RUL_REAL_DECL:			
			case RUL_REAL_CONSTANT:
				{
					if ((*vectInterVar)[i].GetVarType() != RUL_FLOAT)
					{
						float fVal = (float)(*vectInterVar)[i];
						(*vectInterVar)[i].SetValue(&fVal, RUL_FLOAT);
					}
				}
				break;
			case RUL_DD_STRING_DECL:
				{
					if ((*vectInterVar)[i].GetVarType() == RUL_SAFEARRAY)
					{
						if ((value.prgsa)->Type() == RUL_CHAR)
						{
							//this is a DD_STRING variable. The array type shall be RUL_USHORT
							INTER_VARIANT temp = (*vectInterVar)[i];


							_UINT32 numberOfElements = (value.prgsa)->GetNumberOfElements();

							(*vectInterVar)[i].Clear();

							//initial destination array with the same memory size
							INTER_SAFEARRAYBOUND rgbound[1] = { numberOfElements / sizeof(unsigned short) };
							INTER_SAFEARRAY sa;

							sa.SetAllocationParameters(RUL_USHORT, 1, rgbound);

							//cast char* string to wchar_t* string
							INTER_VARIANT temp2;
							temp2 = &sa;
							temp2 = temp;

							//element in a vector shall be assinged only once.
							//Otherwise, HEAP corruption may occur.
							(*vectInterVar)[i] = temp2;
						}
					}

				}
				break;

			default:
				break;
		}//end switch
	}
}


//Added:Anil Octobet 5 2005 for handling Method Calling Method
//This function is to fill the return arg name and its type
//The assumptions made here is the ,  for the caller and called method, Return agrument variable 
//name is "__RetVar_" + Called Method Name + "_XXX"; //I am really sorry, if some body in Called 
//method has an variable by this name... hopefully not!!
//Data type is filled apropriatly , and when coming out from called method, this variable is 
//filled with the return value. also there one more flag m_IsReturnVar set for this return variable
//so that in the called method , when this variable is on symbol table, can be known as returnn 
//variable There can be only one return variable in the called method Symbol table and is of only 
//the following data type
RETURNCODE MEE::UpdateRetunMethArgInfo(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,
									   hCmethodParam* phCmethodReturnValue,
									   vector<INTER_VARIANT>* vectInterVar    ) 
{
	//get the method name to form the return variable name	
	string StrMethName = phCmethodReturnValue->getName();
	//Return variable name
	string strUnikRetVarName = "__RetVar_" + StrMethName + "_XXX";
	INTER_VARIANT varTemp;

	METHOD_ARG_INFO cMethReturnArgInfo;
	RUL_TOKEN_TYPE eTokenType;
	RUL_TOKEN_SUBTYPE eTokenSubType;
	//Get the data type of the return variable
	//Refer above in ValidateMethodArgs,  for  forming the token type and sub type
	eTokenType = RUL_SIMPLE_VARIABLE;
	eTokenSubType = RUL_SUBTYPE_NONE;
	switch(phCmethodReturnValue->getType())
	{
		case methodVarChar:
			{
				eTokenSubType = RUL_CHAR_DECL;
				varTemp = (char)0;//WS: 25jun07 - set value and type

			}
			// Walt EPM 08sep08 added modified
			break; // stevev
		case methodVar_U_Char:
			{
				eTokenSubType = RUL_UNSIGNED_CHAR_DECL;
				varTemp = (unsigned char)0;//WS: 25jun07 - set value and type
			}
			break; // stevev
		case methodVarShort:
			{
				eTokenSubType = RUL_SHORT_INTEGER_DECL;
				varTemp = (short)0;//WS: 25jun07 - set value and type
			}
			break;
		case methodVar_U_Short:
			{
				eTokenSubType = RUL_UNSIGNED_SHORT_INTEGER_DECL;
				varTemp = (unsigned short)0;//WS: 25jun07 - set value and type

			}
			break;
		case methodVarLongInt:
			{
				eTokenSubType = RUL_INTEGER_DECL;
				varTemp = (long)0;//WS: 25jun07 - set value and type
			}
			break;
		case methodVar_U_LongInt:
			{
				eTokenSubType = RUL_UNSIGNED_INTEGER_DECL;
				varTemp = (unsigned int)0;
			}
			break;
		case methodVar_U_Int64:
			{
				eTokenSubType = RUL_UNSIGNED_LONG_LONG_DECL;
                varTemp = (_UINT64)0;
			}
			break;
		case methodVarInt64:
			{
				eTokenSubType = RUL_LONG_LONG_DECL;
				varTemp = (__int64)0;
			}
			break;
			// Walt EPM 08sep08 - end added/modified
		case methodVarDDItem:
			{
				eTokenSubType = RUL_INTEGER_DECL;
				varTemp = (long)0;//WS: 25jun07 - set value and type
			}
			break;

		case methodVarDouble:
			{
				eTokenSubType = RUL_DOUBLE_DECL;//WS:EPM 10aug07
				varTemp = (double)0.0;//WS: 25jun07 - set value and type
			}
			break;
		case methodVarFloat:			
			{
				eTokenSubType = RUL_REAL_DECL;
				varTemp = (float)0.0;//WS: 25jun07 - set value and type
			}
			break;

		case methodVarDDString:			
			{
				eTokenSubType = RUL_DD_STRING_DECL;

				//re-construct destination variable with array type and number of elements
				INTER_SAFEARRAYBOUND rgbound[1] = { 1 };
				INTER_SAFEARRAY sa;

				sa.SetAllocationParameters(RUL_USHORT, 1, rgbound);
				varTemp = &sa;
		}
			break;
		case methodVarVoid:
			{
				eTokenSubType = RUL_SUBTYPE_NONE;
			}
			break;


		default:
			{
				//TO DO We may need to validate the   whether it id DD item and Check accordingle
			//error!!!!
			}
			break;

	}//end switch
	//So Create new  METHOD_ARG_INFO class for the return variable and push it on the vector
	
	cMethReturnArgInfo.SetCalledArgName(strUnikRetVarName.c_str());
	cMethReturnArgInfo.SetCallerArgName(strUnikRetVarName.c_str());
	cMethReturnArgInfo.SetType(eTokenType);
	cMethReturnArgInfo.SetSubType(eTokenSubType);
	cMethReturnArgInfo.ePassedType = DD_METH_AGR_PASSED_BYREFERENCE;
	//this is very imp!! Set this flag only for the return variable
	cMethReturnArgInfo.m_IsReturnVar = true;
	vectMethArgInfo->push_back(cMethReturnArgInfo);

	vectInterVar->push_back(varTemp);
	return SUCCESS;

}


/************************************************************************************************/
/* stevev 15aug07 --- resolution of complex references is revisited.
	Earlier code had restrictions like indexes had to be constant integers.
	Earlier code was not designed to be recursive either, requiring more code to deal with fewer 
	conditions.

  The Following functions:   ResolveComplexReference()  ResolveDot()  ResolveBrack()
  are designed to overcome these deficiencies.
  They are coded separately here so they may be tested separately and then wrapped as required to
  fill the MEE requirements.

  NOTE:  There are some TODO: comments embedded in the following code.  Most of these have
  to do with needing to call higher level code in order to make the functionality correct.
  For example:an array index may be a DD-variable or a constant OR A METHOD_LOCAL OR METHOD-CALL.
  This function will handle the DD-variable and constant, but there are TODO comments getting
  the resolution to a built-in, method-call, or method-local is required.
  
  PLEASE-Someone that knows or wants to learn needs to take ownership of these TODOs and
  complete the functionality of this section!

*/


#define A_DOT	'.'
#define A_BRACK '['
#define A_PARENTH '('
#define A_EMPTY '\0'
#define A_CLOSE ']'

// helpers
char* skipWhite(char* src)
{//TODO: make language insensitive....
	if ( src )
	{
		while ( (*src == '\t' || *src == ' ' || *src == '\n') && *src != '\0' ) 
		{	src++;}// skip whitespace  
	}
	return src;
}

//This function returns true if the input is a primary attribute name. Otherwise, it returns false. 
bool MEE::isAttr(/* in */ const char* pAttrStr)
{
	bool bRetVal = false;

	size_t attrLen = strlen(pAttrStr) + 1;
	char *attrName = new char[attrLen];
	PS_Strncpy(attrName, attrLen, pAttrStr, attrLen);

	if(	(strstr(attrName, "MAX_VALUE") ) != NULL || (strstr(attrName, "MIN_VALUE") ) != NULL )
	{
		attrName[strlen("MAX_VALUE")] = '\0';
	}	


	if (attrName == NULL)
	{
		return bRetVal;
	}

	//Check  whether it is valid primary attribute	
	m_MapDotOpNameToAttrIter = m_MapDotOpNameToAttr.find(attrName);
	if(m_MapDotOpNameToAttrIter == m_MapDotOpNameToAttr.end())
	{	//it is not valide primary attribute, Sorry man hang up!!
		bRetVal = false;
	}
	else
	{
		bRetVal = true;
		//Get the Attribute Structure Which contains " C" Data type of attribute and its unique ID
		DOT_OP_ATTR stDotOpAtt = m_MapDotOpNameToAttrIter->second;	// for debugging
	}
	
	if (attrName != NULL)
	{
		delete [] attrName;
	}

	
	return bRetVal;
}


//This function returns true if the first input parameter is a secondary attribute name. Otherwise, it returns false.
//The second input parameter pIB is a pointer to hCitemBase class which is a DD item that the secondary attribute comes from.
//The output parameter rpDevObjVar is a pointer to hCitemBase class which is a DD item selector reference
bool MEE::isSecondaryAttr(/* in */ const char* pAttrStr, /* in */ hCitemBase* pIB, /* out */ hCitemBase*& rpDevObjVar)
{
	bool bRetVal = false;

	if (pAttrStr == NULL)
	{
		return bRetVal;
	}

	string strDotExpression = pAttrStr;
	for(int i = 0; i < (int)m_svSecondaryAtt.size() ; i++)  	// warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		if(strDotExpression == m_svSecondaryAtt[i])
		{
			bRetVal = true;
			break;
		}
	}

	if (bRetVal)
	{
		//Secondary attribute is found. Get the attribute hCitemBase pointer
		if ( strDotExpression == "X_AXIS" ) // possible reference to a DD item
		{// a secondary attribute
//#			verify valid attribute for item
			if ( pIB->getIType() == iT_Graph)
			{
//#				lookup attribute in item 
				rpDevObjVar = ((hCgraph*)pIB)->getXAxis();
//#				return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: X_AXIS of %s did not resolve.\n", pIB->getName().c_str());
					bRetVal = false;
				}// else - all is well
			}
			else // - invalid
			{
				SDCLOG(CERR_LOG,"Method Reference: 'X_AXIS' is invalid reference for %s\n",	pIB->getName().c_str());
				bRetVal = false;
			}
		}
		else
		if ( strDotExpression == "Y_AXIS" ) // possible reference to a DD item
		{// a secondary attribute

//#			verify valid attribute for item
			if ( pIB->getIType() == iT_Source)
			{
//#				lookup attribute in item 
				rpDevObjVar = ((hCsource*)pIB)->getYAxis();
//#				return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Source Y_AXIS of %s did not resolve.\n",	pIB->getName().c_str());
					bRetVal = false;
				}// else - all is well
			}
			else
			if ( pIB->getIType() == iT_Waveform)
			{
//#			lookup attribute in item 
				rpDevObjVar = ((hCwaveform*)pIB)->getYAxis();
//#			return error if not found or not an item	
				if (rpDevObjVar == NULL || rpDevObjVar->getIType() != iT_Axis)
				{
					SDCLOG(CERR_LOG,"Method Reference: Waveform Y_AXIS of %s did not resolve.\n", pIB->getName().c_str());
					bRetVal = false;
				}// else - all is well
			}
			else
			{
				SDCLOG(CERR_LOG,"Method Reference: 'Y_AXIS' is invalid reference for %s\n",	pIB->getName().c_str());
				bRetVal = false;
			}
		}
	}
	
	return bRetVal;
}


// note that pMemberName is just the membername, not the right string, not attribute name either
RETURNCODE MEE::getMemberInfo(/* in */ hCitemBase* pLeftItem, /* in */ const char*   pMemberName, 
							 /* out */ CValueVarient* pValue, /* out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef)
{
	RETURNCODE rc = SUCCESS;
	if ( pLeftItem == NULL  || pMemberName == NULL)
	{
		return MEE_DDITEM_NAME_NOT_RESOLVED;
	}
	pMemberName = skipWhite((char*)pMemberName );
//#			verify itempointer has members	
	itemType_t it = pLeftItem->getIType();

	string memNm                = pMemberName;
	hCitemBase*   pLft          = NULL;

//#			get by name in item	
//#			return error if not found	
//#			fill value&ptr and return
	switch (it)
	{
	case iT_Collection:
	case iT_Record:
	case iT_File:
	case iT_Chart:
	case iT_Graph:
	case iT_Source:
	case iT_VariableList:
		{
			//get member ID
			ITEM_ID ulMemberId = 0;
			rc = m_pDevice->GetMemberIdByName(memNm.c_str(), &ulMemberId);
			if ( rc == SUCCESS )
			{
				//set item operative IDs based on member ID
				pOpRef->addOperationalID(pLeftItem->getID(), ulMemberId, pLeftItem->getIType());

				//get item final data pointer
				rc = m_pDevice->getElementOrMemberPointer(pLeftItem->getID(), ulMemberId, &pLft);
				if ( rc == SUCCESS )
				{
					rpDevObjVar = pLft;

					//set item operative IDs based on resolved item ID
					pOpRef->addOperationalID(pLft->getID(), 0, pLft->getIType());	//if this is not a simple variable, the member ID as 0 is a group id.
				}
				else
				{// no member
					SDCLOG(CERR_LOG,"Method Reference: '%s' failed to resolve membername '%s'\n",
														pLeftItem->getName().c_str(),pMemberName);
					rc = MEE_MEMBER_NOT_FOUND;
				}
			}
			else
			{// no member
				SDCLOG(CERR_LOG,"Method Reference: '%s' failed to resolve membername '%s'\n",
													pLeftItem->getName().c_str(),pMemberName);
				rc = MEE_MEMBER_NOT_FOUND;
			}
		}
		break;
	default:
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' Does not have members to access via '%s'\n",
														pLeftItem->getName().c_str(),pMemberName);
			rc = MEE_NO_MEMBERS_IN_ITEM;
		}
		break;
	}// end switch
	return rc;	
}

/************************************************************************************************/

/*	This is a Helper Function is for resolving complex DD references like DDItem[i].member[j].x  
	See ResolveComplexReference() for a discussion of these.
	This Function handles the bracketed portion of the string  e.g.
	sLeftString  is 'DDItem[i]'
	RightString  is '[i].member[j].x'

    This function will Resolve the DDItem[i] part and then call ResolveComplexReference() to handle the rest.
	ie   return ResolveComplexReference( ptr2dditem_i, ".member[j].x", pValue, ppDevObjVar)

	Function Input Parameters: 
	sLeftString		is a string of the DD item resolved by the left part of the reference to 
					this point.  This MUST be a string of a list or value array OR reference ARRAY. 
					All others will fail.
	RightString		The string to the right of the portion resolved to pLeftItem.  This MUST
					start with a '[' and have a ']' in the rest of it or it will fail.
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
*/
/************************************************************************************************/


RETURNCODE MEE::ResolveBracket(/* in */ string sLeftString,  /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef)		
{
	RETURNCODE rc = SUCCESS;

	if ( pRightString == NULL || pOpRef == NULL)
	{
		return MEE_DDITEM_NAME_NOT_RESOLVED;
	}
	

	//look for left string variable instance pointer
	hCitemBase* pLeftItem	= NULL;
	if (sLeftString != "")
	{
		rc = m_pDevice->getItemBySymName(sLeftString, &pLeftItem);

		if (rc != SUCCESS || pLeftItem == NULL )
		{
			SDCLOG(CERR_LOG, "Method Reference: '%s' could not resolve '%s'.\n",
				sLeftString.c_str(), pRightString);
			rc = MEE_DDITEM_NAME_NOT_RESOLVED;
		}
	}
	else if (rpDevObjVar != NULL)
	{
		//if the left string is empty, the input data pointer is what we are looking for
		pLeftItem = rpDevObjVar;
		rc = SUCCESS;
	}
	else
	{
		rc = MEE_DDITEM_NAME_NOT_RESOLVED;
	}

	if ((rc == SUCCESS) && (pLeftItem != NULL))
	{
		pRightString = skipWhite(pRightString);

		char    *pNxt = pRightString;
		char	*pIdx = NULL;
	

	//#	verify LeftItem has elements	
		if ( (pLeftItem->getIType() != iT_ItemArray) && (pLeftItem->getIType() != iT_Array) && (pLeftItem->getIType() != iT_List) )
		{// the ONLY types that have elements, it it ain't one of dem, its an error
			SDCLOG(CERR_LOG,"Method Reference: '%s' is expected to be an Array to resolve '%s'\n",
														pLeftItem->getName().c_str(),pRightString);
			return MEE_NOT_AN_ARRAY_TYPE;
		}
	//#	get RightString till ']' || mt
		if ( *pNxt != '[' )
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' is expected to start with '['\n", pRightString);
			return MEE_INVALID_INDEX;
		}
	
		pIdx = pNxt = skipWhite(++pNxt);// twixt '[' and index
		while ( *pNxt != ']' && *pNxt != '\0')
		{
			pNxt++;
		}
	//#	if MT	
		if ( (*pNxt != ']') || (pNxt == pIdx) )
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid index.(from '%s')\n", pIdx,
																					pRightString);
			return MEE_INVALID_INDEX;
		}	
	//#	else - no error	

		*pNxt  = A_EMPTY;	// terminate index string
		string sIndex = pIdx;		// retrieve index string inside '[' and ']'
		*pNxt  = ']';		// put it back before we forget.

		//Get numeric value inside '[' and ']'
		INTER_VARIANT varVal;
		rc = m_pMEEInterpreter->GetVariableValue((char*)sIndex.c_str(), varVal);
	
		//#		exit on error or no Value	
		if ( (rc != SUCCESS) || (!varVal.isNumeric() ) || ((int)varVal < 0) )
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid index.(from '%s')\n", sIndex.c_str(), pRightString);
		}
		else
		{
			//#		else - no error		
			//The DD item must be value array, item array or list. Look for LeftItem operational ID
			itemType_t typeCheck = pLeftItem->getIType();
			hCitemBase* pLft  = NULL;
			if ( ((typeCheck == iT_Array) || (typeCheck == iT_List)) && (m_startedProtocol != nsEDDEngine::FF) )
			{
				rc = m_pDevice->getElementOrMemberPointer(pLeftItem->getID(), (unsigned int)varVal + 1, &pLft);
				if (rc == SUCCESS)
				{
					pOpRef->addOperationalID(pLeftItem->getID(), (unsigned int)varVal + 1, pLeftItem->getIType());
				}
				else
				{
					rc = FAILURE;
				}
			}
			else
			{
				rc = m_pDevice->getElementOrMemberPointer(pLeftItem->getID(), (unsigned int)varVal, &pLft);
				if (rc == SUCCESS)
				{
					pOpRef->addOperationalID(pLeftItem->getID(), (unsigned int)varVal, pLeftItem->getIType());
				}
				else
				{
					rc = FAILURE;
				}
			}
			if (( rc == SUCCESS ) && pLft)
			{
				pOpRef->addOperationalID(pLft->getID(), 0, pLft->getIType());
			}

			rpDevObjVar = pLft;
			pNxt   = skipWhite(++pNxt);// from ']' to rest of right
			if (( rc == SUCCESS ) && (*pNxt != A_EMPTY))
			{
				//not done yet, continue...
				rc = ResolveComplexReference(pNxt, pValue, rpDevObjVar, pOpRef);
			}
		}
	}

	return rc;
}


/************************************************************************************************/

/*	This is a Helper Function is for resolving complex DD references like DDItem[i].member[j].x  
	See ResolveComplexReference() for a discussion of these.
	This Function handles the .def portion of the string  e.g.
	sLeftString  is 'DDItem[i]'
	RightString  is '.member[j].x'

    This function will Resolve the DDItem[i].member part and then call ResolveComplexReference() to handle
	the rest.
	ie   return ResolveComplexReference( ptr2dditem_i_mem, "[j].x", pValue, ppDevObjVar)

	Function Input Parameters: 
	sLeftString		is a string of the DD item resolved by the left part of the reference to 
					this point.  This MUST be a string of a non-relational DD item (eg not a 
					unit-relation,refresh-relation, wao <or cmd>).  All others will fail.
	RightString		The string to the right of the portion resolved to pLeftItem.  This MUST
					start with a '.' and have a symbol to the right of it or it will fail.
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
*/
/************************************************************************************************/


RETURNCODE MEE::ResolveDot(/* in */ string sLeftString, /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef)		
{		
	RETURNCODE rc = SUCCESS;

	if (pRightString == NULL || pOpRef == NULL)
	{
		return MEE_DDITEM_NAME_NOT_RESOLVED;
	}

	pRightString = skipWhite(pRightString);

	char    *pNxt     = pRightString + 1;// skip past the leading '.'
	char    foundChar = A_EMPTY;
	hCitemBase* pLeftItem  = NULL;
	string  ls;

//#	get RightString to  '.' || '[' || '(' || eos 	
	while ( *pNxt != '\0' )
	{
		if ( *pNxt == A_DOT )
		{
			foundChar = A_DOT;
			break;
		}
		else if ( *pNxt == A_BRACK )
		{
			foundChar = A_BRACK;
			break;
		}
		else if ( *pNxt == A_PARENTH )
		{
			foundChar = A_PARENTH;
			break;
		}

		pNxt++;
	}
	*pNxt = A_EMPTY;			// temporary
	ls    = pRightString + 1;	// up to the dot/brack
	*pNxt = foundChar;        // put it back before we forget.

	
	//look for left string variable instance pointer
	if (sLeftString != "")
	{
		//check some special block-level references with DOT
		ResolveType eResolveType = RESOLVE_NONE;	//initialized to none
		if (sLeftString == "BLOCK")
		{
			eResolveType = RESOLVE_BLOCK_REF;
		}
		else if (sLeftString == "PARAM")
		{
			eResolveType = RESOLVE_PARAM_REF;
		}
		else if (sLeftString == "LOCAL_PARAM")
		{
			eResolveType = RESOLVE_LOCAL_REF;
		}
		else if (sLeftString == "PARAM_LIST")
		{
			eResolveType = RESOLVE_PARAM_LIST_REF;
		}//default is none

		if (eResolveType != RESOLVE_NONE)
		{
			//It is one of the special block-level references with DOT
			//get member ID
			ITEM_ID ulMemberId = 0;
			rc = m_pDevice->GetMemberIdByName(ls.c_str(), &ulMemberId);

			//resolve member ID to block-level item ID
			ITEM_ID ulItemId = m_pDevice->ResolveBlockRef(0, 0, ulMemberId, eResolveType);
			if (ulItemId != 0)
			{
				rc = m_pDevice->getItemBySymNumber(ulItemId, &pLeftItem);
					
				//get the rest of expression string resolved
				if (rc == SUCCESS)
				{
					pOpRef->addOperationalID(pLeftItem->getID(), 0, pLeftItem->getIType());		//group item at this time
					rpDevObjVar = pLeftItem;
					pLeftItem = NULL;			//skip the following member data pointer fetch
				}
			}
			else
			{
				rc = MEE_DDITEM_NAME_NOT_RESOLVED;
			}
		}
		else
		{
			//It is a general reference with DOT
			rc = m_pDevice->getItemBySymName(sLeftString, &pLeftItem);
		}
	}
	else
	{
		pLeftItem = rpDevObjVar;
		rc = SUCCESS;		//continue
	}

	//get member data pointer and operational ID
	if ((rc == SUCCESS) && (pLeftItem != NULL))
	{
		hCitemBase* pLft  = NULL;

		if ( (ls != "") && isAttr((char *)ls.c_str()) )//	if string is primary attribute		
		{
			if (pValue)
			{
				*pValue = ls;
			}
			pOpRef->addOperationalID(pLeftItem->getID(), 0, pLeftItem->getIType());
			pLft = pLeftItem;
			rc = SUCCESS;
		}
		else if ( isSecondaryAttr((char *)ls.c_str(), pLeftItem, pLft) )//	if string is seconary attribute		
		{
			pOpRef->addOperationalID(pLft->getID(), 0, pLft->getIType());	//AXIS reference
			rc = SUCCESS;
		}
		else // must be a membername		
		{	
	//#			fill value&ptr and return	
			rc = getMemberInfo(pLeftItem, ls.c_str(), pValue, pLft, pOpRef);
	//#			return error if not found
			if ( rc != SUCCESS || pLft == NULL )// has to resolve to an item to deal w/dot|brack
			{
				SDCLOG(CERR_LOG,"Method Reference: '%s' is invalid reference for %s\n",
													ls.c_str(),	pLeftItem->getName().c_str());
				rc = MEE_DDITEM_NAME_NOT_RESOLVED;
			}
			// else pLft is filled, exit resolving the rest
		}

		rpDevObjVar = pLft;
	}

	if (( rc == SUCCESS ) && (*pNxt != A_EMPTY))
	{
		//not done yet, continue...
		rc = ResolveComplexReference(pNxt, pValue, rpDevObjVar, pOpRef);
	}

	return rc;
}


RETURNCODE MEE::ResolveParenth(/* in */ string sLeftString, /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef)		
{		
	RETURNCODE rc = SUCCESS;

	if (pRightString == NULL || pOpRef == NULL)
	{
		return MEE_DDITEM_NAME_NOT_RESOLVED;
	}


	//look for left string variable instance pointer
	hCitemBase* pLeftItem	= NULL;
	if (sLeftString != "")
	{
		rc = m_pDevice->getItemBySymName(sLeftString, &pLeftItem);

		if ( (rc != SUCCESS) || (pLeftItem == NULL) || !pLeftItem->IsVariable() )
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' could not resolve '%s'.\n",	sLeftString.c_str(), pRightString);
			rc = MEE_DDITEM_NAME_NOT_RESOLVED;
		}
	}
	else if (rpDevObjVar != NULL)
	{
		//if the left string is empty, the input data pointer is what we are looking for
		pLeftItem = rpDevObjVar;
		rc = SUCCESS;
	}
	else
	{
		rc = MEE_DDITEM_NAME_NOT_RESOLVED;
	}

	if ((rc == SUCCESS) && (pLeftItem != NULL))
	{
		pRightString = skipWhite(pRightString);

		//if this is not enum or bit_enum variable, error
		hCVar* phCVar = (hCVar*)pLeftItem;
		if ( (phCVar->VariableType() != (int)vT_Enumerated) && (phCVar->VariableType() != (int)vT_BitEnumerated) )
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' is not enum or bit_enum variable '%s'.\n", sLeftString.c_str(), pRightString);
			rc = MEE_DDITEM_NAME_NOT_RESOLVED;
		}
		else
		{
			//get DD used enum or bit_enum variable value
			char    *pNxt			= pRightString + 1;// skip past the leading '('
			char*	pInx = pNxt;

			while ( *pInx != ')' && *pInx != '\0')
			{
				pInx++;
			}

			//check error
			if ( (*pRightString != '(') || (*pInx != ')') || (pNxt == pInx) )	//no ')' or no number between '(' and ')'
			{
				SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid index.(from '%s')\n", pInx, pRightString);
				rc = MEE_INVALID_INDEX;
			}
			else
			{
				//get index string inside '(' and ')'
				pInx[0] = A_EMPTY;	// terminate index string
				string ls = pNxt;
				pInx[0]  = ')';		// put it back before we forget.

				//Get numeric value inside '(' and ')'
				INTER_VARIANT varVal;
				rc = m_pMEEInterpreter->GetVariableValue((char*)ls.c_str(), varVal);

				//#		exit on error or no Value	
				if ( (rc != SUCCESS) || (!varVal.isNumeric() ) || ((int)varVal < 0) )
				{
					SDCLOG(CERR_LOG,"Method Reference: '%s' is not a valid reference.(from '%s')\n", ls.c_str(), pRightString);
				}	
				else
				{
					//#		else - no error		
					if (pValue)
					{
						*pValue = (unsigned int)varVal;	//don't cast to unsigned long because the type becomes wrong isSymID
					}
					rpDevObjVar = pLeftItem;
					pOpRef->addOperationalID(pLeftItem->getID(), (unsigned int)varVal, pLeftItem->getIType());		//ENUM and BIT-ENUM variable

					pNxt   = skipWhite(++pInx);// from ')' to rest of right
					if (*pNxt != A_EMPTY)
					{
						//not done yet, continue...
						rc = ResolveComplexReference(pNxt, pValue, rpDevObjVar, pOpRef);
					}
					else
					{
						rc = SUCCESS;
					}
				}
			}
		}
	}

	return rc;
}


/************************************************************************************************/

/*	This is the Function for resolving complex DD references like DDItem[i].member[j].x  
	Where 
	DDItem  :is a DD item (an list or array of something with members), 
	i,j		:are Constant integers, numeric DD-items or complex numeric DD references 
	member  :is a member of "DDItem[i]" that resolves to an array or list  
	x		:can be	1)member of "DDItem[i].member[j]"
					2)primary attribute   ( like LABEL, HELP -- refer BuildDotOpNameToAttrMap()) 
					3)secondary attribute ( like Y_AXIS --refer BuildSecondaryAttrList for this) 


	This Function is the principle entry to resolve this type of reference.

	Function Input Parameters: 
	ReferenceString is a pointer to the entire reference string
	
	Function Output Parameters:
	pValue          a passed-in varient that will be filled with the value of the item or 
					attribute when found.
	rpDevObjVar		the reference to a pointer which will be filled by the pointer to the item
					when a reference resolves to an item.  Set to NULL if only the value is valid.
					This pointer can only be used for getting item properties.
	pOpRef			pointer to DD item operative item ID

	Returned value may be SUCCESS, FAILURE, MEE_DDITEM_NAME_NOT_RESOLVED, MEE_INVALID_INDEX, MEE_NUMERIC_ARRAYNAME, MEE_NOT_AN_ARRAY_TYPE.
*/
/************************************************************************************************/


RETURNCODE MEE::ResolveComplexReference(/* in */ char* ReferenceString, /* out */ CValueVarient* pValue, /* out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef)
{
	RETURNCODE rc = SUCCESS;

	if (ReferenceString == NULL || ReferenceString[0] == '\0' || pOpRef == NULL)
	{
		return MEE_DDITEM_NAME_NOT_RESOLVED;
	}

	//check if the expression is a local variable
	bool bLocal = false;
	m_pMEEInterpreter->FindInLocalSymbolTable(ReferenceString, NULL, &bLocal);
	if (bLocal)
	{
		return MEE_NOT_DDITEM;
	}

	char*   pNxt      = ReferenceString;
	bool    isNumeric = true;// will soon be disproven
	char    foundChar = A_EMPTY;
	hCitemBase* pLft  = NULL;
	string  ls;

//#	get from beginning to  '.' || '[' || '(' || eos    { flag if Numeric }	
	while ( *pNxt != A_EMPTY )
	{
		if ( ! isdigit(*pNxt) )
		{
			isNumeric = false;
		}

		if ( *pNxt == A_DOT )
		{
			foundChar = A_DOT;
			break;
		}
		else if ( *pNxt == A_BRACK )
		{
			foundChar = A_BRACK;
			break;
		}
		else if ( *pNxt == A_PARENTH )
		{
			foundChar = A_PARENTH;
			break;
		}

		pNxt++;
	}

	if (foundChar == A_BRACK)
	{
//#	if Brack  {Numeric Left is error}
//#		get left as item - error if not found				
		*pNxt = A_EMPTY;// temporary
		ls    = ReferenceString;// up to the brack
		*pNxt = A_BRACK;// put it back before we forget.
		if (isNumeric)
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' cannot handle '%s'.\n",
																	ReferenceString,ls.c_str());
			rc = MEE_NUMERIC_ARRAYNAME;
		}
		else
		{
//#			get left item to Ptr
			rc = ResolveBracket(ls, pNxt, pValue, rpDevObjVar, pOpRef);
		}
	}
	else	
	if (foundChar == A_DOT)
	{
		//Is it a floating point?
		*pNxt = A_EMPTY;        // temporary
		ls    = ReferenceString;// up to the brack
		*pNxt = A_DOT;        // put it back before we forget.
		if ( isNumeric)
		{
//#			lookahead right for all digits to end	
			char* pL = pNxt + 1;// past the dot
			size_t length = strlen(pL);
			size_t index = 0;
			while ( index < length )
			{	
				if ( !isdigit(*pL) ) 
				{
					isNumeric = false;
					break;
				}
				pL++;
				index++;
			}
//#			if true - all digits to end	
			if ( isNumeric )
			{
//#				convert string to floating point
				double lD = atof(ReferenceString);
//#				put float into Value, Clear Ptr
				if (pValue)
				{
					*pValue     = (double)lD;
				}
				rpDevObjVar = NULL;
				rc = SUCCESS;
			}
			else // non-numeric to right of DP ( could be exponent but I'm not supporting that )
			{
				SDCLOG(CERR_LOG,"Method Reference: could not resolve '%s'.\n", ReferenceString);
				rc = MEE_DDITEM_NAME_NOT_RESOLVED;
			}
		}
		else //- left not numeric, must be an item 
		{
//#			get left item to Ptr
			rc = ResolveDot(ls, pNxt, pValue, rpDevObjVar, pOpRef);
		}
	}
	else if (foundChar == A_PARENTH)
	{
//#	if parenthesis  {Numeric Left is error}
//#		get left as item - error if not found				
		*pNxt = A_EMPTY;// temporary
		ls    = ReferenceString;// up to the brack
		*pNxt = A_PARENTH;// put it back before we forget.
		if (isNumeric)
		{
			SDCLOG(CERR_LOG,"Method Reference: '%s' cannot handle '%s'.\n",
																	ReferenceString,ls.c_str());
			rc = MEE_NUMERIC_ARRAYNAME;
		}
		else
		{
//#			get left item to Ptr
			rc = ResolveParenth(ls, pNxt, pValue, rpDevObjVar, pOpRef);
		}
	}
	else //- must be MT (we're at the end)			
	{
		char *stopstring;

		if ( isNumeric )
		{
//#			convert string to numeric value	
			double dVarVal = strtod(ReferenceString, &stopstring);
			if (stopstring[0] == '\0')
			{
//#				put integer into Value, Clear Ptr	& leave
				if (pValue)
				{
					*pValue     = dVarVal;
				}
				rpDevObjVar = NULL;
				rc =  SUCCESS;
			}
			else
			{
				rc = MEE_DDITEM_NAME_NOT_RESOLVED;
			}
		}
		else //- left not numeric		
		{
			// TODO: Recognize a leading + or - and handle correctly
//#			check for 0x entry
			pNxt      = ReferenceString;
			if (*pNxt == '0' && (*(pNxt+1) == 'x' || *(pNxt+1) == 'X') )
			{
//#				convert Hex string to integer
				unsigned long long loc = strtoull(ReferenceString, &stopstring, 16);
				if (stopstring[0] == '\0')
				{
//#					put integer into output pValue & leave
					if (pValue)
					{
						*pValue     = loc;
					}
					rpDevObjVar = NULL;
					rc =      SUCCESS;
				}
				else
				{
					rc = MEE_DDITEM_NAME_NOT_RESOLVED;
				}
			}
			else // not numeric, not hex, must be a simple variable
			{
//#				try to find as a DD item
				ls = ReferenceString;
				
				rc = m_pDevice->getItemBySymName(ls, &pLft);

				if (rc != SUCCESS || pLft == NULL )
				{
/*  At this point, the string we are looking at could be a non-DDitem reference
	- it could be a method-local variable name or it could be a call to
	a built-in  or another method that  returns a value to be used here.
	TODO:  detect and get a value for these types of references !!!!!!!!!!!!!!
*/					SDCLOG(CERR_LOG,"Method Reference: '%s' could not be resolved.\n",
																				ReferenceString);
					rc = MEE_DDITEM_NAME_NOT_RESOLVED;
				}// else - return the info
				else
				{
					rpDevObjVar = pLft;
					pOpRef->addOperationalID(pLft->getID(), 0, pLft->getIType());	//member 0 means the entire variable
				}
			}
		}
	}

	return      rc;
}


/* This function resolves either operative item ID or DD reference expression to DD item data pointer, and DD attribute name, and enumeration/bit-enumeration variable index.
 */
RETURNCODE MEE::ResolveOperItemIDOrDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef,
									     /* out */ CValueVarient* pValue, /* out */ hCitemBase*& pIBFinal)
{
	RETURNCODE rc = MEE_DDITEM_NAME_NOT_RESOLVED;

	if ( pOpRef && pOpRef->isOperationalIDValid() )
	{
		//find hCitemBase* pointer based on operative item ID
		unsigned long ulItemId = 0;
		unsigned long ulMemberId = 0;

		if (pOpRef->op_ref_type == STANDARD_TYPE)
		{
			ulItemId = pOpRef->op_info.id;
			ulMemberId = pOpRef->op_info.member;
		}
		else
		{
			ulItemId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
			ulMemberId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].member;
		}
		
		if(ulMemberId == 0)
		{
			rc = m_pDevice->getItemBySymNumber(ulItemId, &pIBFinal);
		}
		else
		{
			rc = m_pDevice->getElementOrMemberPointer(ulItemId, ulMemberId, &pIBFinal);
		}

		if (rc == SUCCESS)
		{
			//find the variable attribute name and enum/bit-enum index in reference name
			const char *pSubName = strrchr(szComplexDDExpre, '.');
			if (pSubName != NULL)
			{
				*pValue = (char *)(pSubName + 1);
			}
			else if (pIBFinal->getIType() == iT_Variable)
			{
				hCVar *pVar = (hCVar *) pIBFinal;
				if ((pVar->VariableType() == (int)vT_BitEnumerated) || (pVar->VariableType() == (int)vT_Enumerated))
				{
					//find enum or bit-enum index
					char sIndex[10];
					pSubName = strrchr(szComplexDDExpre, '(');
					if (pSubName != NULL)
					{
						int i = 0;
						pSubName++;
						while((pSubName[i] != ')') && (i < 9))
						{
							sIndex[i] = pSubName[i];
							i++;
						}
						sIndex[i] = '\0';
						char *stopstring = NULL;
						unsigned long ulIndexValue = strtoul(sIndex, &stopstring, 0);	//sIndex could be hex or decimal string
						if (stopstring[0] == '\0')
						{
							*pValue = (unsigned int)ulIndexValue;						//cast to unsigned int because unsigned long is used for isSymID only
						}
						else
						{
							rc = MEE_DDITEM_NAME_NOT_RESOLVED;									//DD error
						}
					}
				}
			}
		}
		else
		{
			rc = MEE_DDITEM_NAME_NOT_RESOLVED;
		}
	}
	else
	{
		//find hCitemBase* pointer, variable attribute name and enum/bit-enum index based on reference name
		rc = ResolveComplexReference((char*)szComplexDDExpre, pValue, pIBFinal, pOpRef);
	}

	return      rc;
}


/* This function resolves variable expression to array index if there is any, and operative item ID if it is DD item.
 * The argument ivIndex are used only for local array variable. The last argument pOpRef is used for DD_ITEM variable.
 * This function returns true if the functionality succeeds. Otherwise, this function returns false.
 */
void MEE::ResolveVarExp(/* in */const char* pchVarExp, /* in */bool bMethParam, /* out */INTER_VARIANT *pivIndex, /* in, out */PARAM_REF *pOpRef)
{
	if (pchVarExp == NULL)
	{
		return;
	}

	//get variable index and adjust operative item ID if the variable is DD item
	int iNoOfBrackExpre = 0;
	size_t iLeftBrackCount = 0;
	size_t lStrlen = strlen(pchVarExp);
	char* szIndex = new char[lStrlen];							//more memory than we need

	//check if it is value array or list
	for(size_t iTemp = 0; iTemp < lStrlen ; iTemp++)
	{
		if(pchVarExp[iTemp] == '[')
		{
			iTemp++;
			iLeftBrackCount = iTemp;

			while(iTemp < lStrlen)
			{
				if(pchVarExp[iTemp] == ']')
				{
					//get variable index
					PS_Strncpy(szIndex, lStrlen, &(pchVarExp[iLeftBrackCount]), (iTemp - iLeftBrackCount));
					INTER_VARIANT ivIndex;
					int iRetVal = m_pMEEInterpreter->GetVariableValue(szIndex, ivIndex);
					if (pivIndex) *pivIndex = ivIndex;
					
					//update operative item ID
					if ((iRetVal == SUCCESS) && pOpRef && pOpRef->isOperationalIDValid())
					{
						//looking for right value array or list depth
						_INT32 j = iNoOfBrackExpre;
						if ( bMethParam && (pOpRef->op_ref_type == COMPLEX_TYPE) )
						{
							//This is a method parameter. The depth is counted from method parameter current depth.
							j += pOpRef->op_info_list.count - 1;
						}

						//adjust item array or value array or list index as one based index in MI
						UpdatePARAM_REF_withItemArrayListIndex((unsigned int)ivIndex, j, pOpRef);
					}

					iNoOfBrackExpre++;
					break;
				}
				iTemp++;
			}
		}
	}

	//clear memory
	DELETE_ARR(szIndex);

	return;
}


//This function adds new index to the existing item array or value array or list member variable.
//The argument ulIndex is new index; the argument ulDepth is depth in the list.
//The argument ppOpRef is input variable operative item ID and will be updated as output.
void MEE::UpdatePARAM_REF_withItemArrayListIndex (/* in */unsigned long ulIndex, /* in */unsigned long ulDepth, /* in, out */PARAM_REF *pOpRef)
{
	nsEDDEngine::ProtocolType protocol = GetStartedProtocol();
	
	if (pOpRef->op_ref_type == STANDARD_TYPE)
	{
		if ( ((pOpRef->op_info.type == iT_Array) || (pOpRef->op_info.type == iT_List)) )
		{
			if (protocol != nsEDDEngine::FF)
			{
				pOpRef->op_info.member = ulIndex + 1;
			}
			else
			{
				pOpRef->op_info.member = ulIndex;
			}
		}
		else if (pOpRef->op_info.type == iT_ItemArray)
		{
			hCitemBase* pIBFinal = NULL;
			int rc = m_pDevice->getElementOrMemberPointer(pOpRef->op_info.id, ulIndex, &pIBFinal);
			if (rc == SUCCESS)
			{
				pOpRef->op_info.id = pIBFinal->getID();
				pOpRef->op_info.member = 0;
				pOpRef->op_info.type = pIBFinal->getIType();
			}
		}
	}
	else
	{
		//looking for right value array or list depth
		for (_INT32 j = ulDepth; j < pOpRef->op_info_list.count; j++)
		{
			if ( (pOpRef->op_info_list.list[j].type == iT_Array) 
				 || (pOpRef->op_info_list.list[j].type == iT_List) )
			{
				//over write the group member ID
				if (protocol != nsEDDEngine::FF)
				{
					pOpRef->op_info_list.list[j].member = ulIndex + 1;
				}
				else
				{
					pOpRef->op_info_list.list[j].member = ulIndex;
				}
				break;
			}
			else if (pOpRef->op_info_list.list[j].type == iT_ItemArray)
			{
				hCitemBase* pIBFinal = NULL;
				int rc = m_pDevice->getElementOrMemberPointer(pOpRef->op_info_list.list[j].id, ulIndex, &pIBFinal);
				if (rc == SUCCESS)
				{
					pOpRef->op_info_list.list[j].id = pIBFinal->getID();
					pOpRef->op_info_list.list[j].member = 0;
					pOpRef->op_info_list.list[j].type = pIBFinal->getIType();
				}
				break;
			}
		}
	}
}


//Used to get a dictionary string from the parsers perspective
//Profibus uses [string_key] and we needed to get the dictionary string
RETURNCODE MEE::GetDictionaryString(char* chStringKey, char* chReturnBuffer, int nMaxSize )
{

	RETURNCODE nReturn = FAILURE;

	//Convert char* string to a wchar_t* string
	size_t newsize = strlen(chStringKey) + 1;
	size_t convertedChars = 0;
	wchar_t *wchStringKey = new wchar_t[newsize];
    PS_Mbtowcs(&convertedChars, wchStringKey, newsize, chStringKey, _TRUNCATE);

	wchar_t *newstr = new wchar_t[nMaxSize];	//dictionary string
	if(m_pDevice->dictionary->get_dictionary_string(wchStringKey, newstr, nMaxSize) == DDL_SUCCESS)
	{
		newsize = wcslen(newstr) + 1;
		wchar_t *str = new wchar_t[newsize];	//language translated dictionary string
		(m_pDevice->dictionary)->get_string_translation(newstr, str, (int)newsize, m_pDevice->device_sLanguageCode);
		if( (str[0] != _T('\0')) && (wcslen(str) < (unsigned int)newsize) )
		{
			// Convert the wchar_t string to a char* string
			PS_Wcstombs(&convertedChars, chReturnBuffer, nMaxSize, str, _TRUNCATE );
			nReturn = SUCCESS;
		}
		else
		{
			chReturnBuffer[0] = _T(' ');
			chReturnBuffer[1] = _T('\0');
		}
		delete [] str;
	}

	delete [] wchStringKey;
	delete [] newstr;

	return nReturn;
}


//The function converts mixed VISIT_* error code and MEE error code to VISIT_* error code
RETURNCODE MEE::convertMEEcodeToVISITcode(RETURNCODE mixedMEEcode)
{
	RETURNCODE iReturnValue = mixedMEEcode;
	
	if (mixedMEEcode == FAILURE)
	{
		throw(C_UM_ERROR_DDS_PARAM);
	}

	switch (mixedMEEcode)
	{
	case SUCCESS:
		iReturnValue = VISIT_NORMAL;
		break;
	case MEE_ABORTED:
		throw(C_UM_ERROR_VAR_ABORTED);
		break;
	case MEE_BAD_PARAM_PASSED_IN:
	case MEE_DDITEM_NAME_NOT_RESOLVED:
	case MEE_NUMERIC_ARRAYNAME:
	case MEE_INVALID_REFERENCE:
	case MEE_NOT_AN_ARRAY_TYPE:
	case MEE_INVALID_INDEX:
	case MEE_ITEM_ATTRIBUTE_MISMATCH:
	case MEE_NON_NUMERIC_4_MINMAX:
	case MEE_MINMAX_FIND_FAILURE:
	case MEE_UNIMPLEMENTED:
	case MEE_ATTR_NOT_AVAILABLE:
	case MEE_NO_MEMBERS_IN_ITEM:
	case MEE_MEMBER_NOT_FOUND:
		iReturnValue = VISIT_ERROR;
		break;
	default:
		break;	//maybe VISIT_* code
	}

	return iReturnValue;
}

//The function converts mixed error code to MEE error code 
RETURNCODE MEE::convertMixedCodeToMEECode(RETURNCODE mixedMEEcode)
{
	RETURNCODE iReturnValue = mixedMEEcode;
	
	switch (mixedMEEcode)
	{
	case BI_SUCCESS:
		iReturnValue = SUCCESS;
		break;
	case BI_COMM_ERR:
	case BI_ERROR:
	case BI_NO_DEVICE:
		iReturnValue = MEE_INVALID_REFERENCE;
		break;
	case BI_ABORT:
		iReturnValue = MEE_ABORTED;	//method will be aborted
		break;
	case BLTIN_DDS_ERROR:
	case BLTIN_BAD_ID:
	case BLTIN_BAD_METHOD_ID:
		iReturnValue = MEE_ABORTED;	//method will be aborted
		break;
	case BLTIN_FAIL_COMM_ERROR:
	case BLTIN_VAR_NOT_FOUND:
	case BLTIN_NO_MEMORY:
	case BLTIN_NO_DATA_TO_SEND:
	case BLTIN_WRONG_DATA_TYPE:
	case BLTIN_NO_RESP_CODES:
	case BLTIN_BUFFER_TOO_SMALL:
	case BLTIN_CANNOT_READ_VARIABLE:
	case BLTIN_INVALID_PROMPT:
	case BLTIN_NO_LANGUAGE_STRING:
	case BLTIN_FAIL_RESPONSE_CODE:
	case BLTIN_NOT_IMPLEMENTED:
	case BLTIN_BAD_ITEM_TYPE:
	case BLTIN_VALUE_NOT_SET:
	case BLTIN_BLOCK_NOT_FOUND:
	default:
		iReturnValue = MEE_INVALID_REFERENCE;
		break;
	}

	return iReturnValue;
}

bool MEE::IsMethodCancelled()
{ 
	return (m_pDevice->IsDeviceCancelled());
}
