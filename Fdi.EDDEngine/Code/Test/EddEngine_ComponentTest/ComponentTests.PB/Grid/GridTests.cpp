#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_reading_GRID_from_PB
        {
        public: 
            static FLAT_GRID* test_grid_1;
            static FLAT_GRID* test_grid_2;
            static FLAT_GRID* test_grid_3;
            static FLAT_GRID* test_grid_4;
            static FLAT_GRID* test_grid_5;

            static FDI_GENERIC_ITEM* genericItem_grid_1;
            static FDI_GENERIC_ITEM* genericItem_grid_2;
            static FDI_GENERIC_ITEM* genericItem_grid_3;
            static FDI_GENERIC_ITEM* genericItem_grid_4;
            static FDI_GENERIC_ITEM* genericItem_grid_5;

            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0008.DDL.bin");
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_grid_1_specifier;
                engine->FillParamSpecifier("grid_1", &test_grid_1_specifier);
                genericItem_grid_1 = engine->GetGenericItem(test_grid_1_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_1 = dynamic_cast<FLAT_GRID*>(genericItem_grid_1->item);

                FDI_PARAM_SPECIFIER test_grid_2_specifier;
                engine->FillParamSpecifier("grid_2", &test_grid_2_specifier);
                genericItem_grid_2 = engine->GetGenericItem(test_grid_2_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_2 = dynamic_cast<FLAT_GRID*>(genericItem_grid_2->item);

                FDI_PARAM_SPECIFIER test_grid_3_specifier;
                engine->FillParamSpecifier("grid_3", &test_grid_3_specifier);
                genericItem_grid_3 = engine->GetGenericItem(test_grid_3_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_3 = dynamic_cast<FLAT_GRID*>(genericItem_grid_3->item);

                FDI_PARAM_SPECIFIER test_grid_4_specifier;
                engine->FillParamSpecifier("grid_4", &test_grid_4_specifier);
                genericItem_grid_4 = engine->GetGenericItem(test_grid_4_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_4 = dynamic_cast<FLAT_GRID*>(genericItem_grid_4->item);

                FDI_PARAM_SPECIFIER test_grid_5_specifier;
                engine->FillParamSpecifier("grid_5", &test_grid_5_specifier);
                genericItem_grid_5 = engine->GetGenericItem(test_grid_5_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_5 = dynamic_cast<FLAT_GRID*>(genericItem_grid_5->item);

            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem_grid_1;
                delete genericItem_grid_2;
                delete genericItem_grid_3;
                delete genericItem_grid_4;
                delete genericItem_grid_5;
                delete engine;
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_default_value_for_Handling()
            {
                // Default is Read & Write
                Assert::AreEqual(3, (int) test_grid_3->handling);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_default_value_for_Height()
            {
                // Default is Medium
                //             XX_SMALL (0),
                //                 X_SMALL (1),
                //                 SMALL (2),
                //                 MEDIUM (3),
                //                 LARGE (4),
                //                 X_LARGE (5),
                //                 XX_LARGE (6)
                Assert::AreEqual(3, (int) test_grid_3->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_default_value_for_Width()
            {
                // Default is Medium
                Assert::AreEqual(3, (int) test_grid_3->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_default_value_for_Orientation()
            {
                //             HORIZONTAL (0),
                //                 VERTICAL (1)
                // Default is Vertical
                Assert::AreEqual(1, (int) test_grid_3->orientation);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_default_value_for_Validity()
            {
                // Default is true
                Assert::AreEqual(1, (int) test_grid_3->valid);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_default_validity_for_grid_1()
            {
                Assert::AreEqual(1, (int) test_grid_3->valid);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_an_empty_help_attribute_for_grid_3()
            {
                Assert::AreEqual("", gcnew String(test_grid_3->help.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_an_empty_label_attribute_for_grid_3()
            {
                Assert::AreEqual("", gcnew String(test_grid_3->label.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_label_for_grid_1()
            {
                Assert::AreEqual("Grid 1", gcnew String(test_grid_1->label.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_help_for_grid_2()
            {
                Assert::AreEqual("this is grid 2", gcnew String(test_grid_2->help.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_orientation_for_grid_1()
            {
                // HORIZONTAL (0),
                // VERTICAL (1)
                // Default is Vertical
                Assert::AreEqual(0, (int) test_grid_1->orientation);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_orientation_for_grid_2()
            {
                // HORIZONTAL (0),
                // VERTICAL (1)
                // Default is Vertical
                Assert::AreEqual(1, (int) test_grid_2->orientation);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_handling_for_grid_1()
            {
                // read (0), -- 0x01
                // write (1) -- 0x02 � makes 0x03 read&write
                Assert::AreEqual(1, (int) test_grid_1->handling);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_handling_for_grid_2()
            {
                // read (0), -- 0x01
                // write (1) -- 0x02 � makes 0x03 read&write
                Assert::AreEqual(3, (int) test_grid_2->handling);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_handling_for_grid_4()
            {
                // read (0), -- 0x01
                // write (1) -- 0x02 � makes 0x03 read&write
                Assert::AreEqual(2, (int) test_grid_4->handling);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_validity_for_grid_2()
            {
                Assert::AreEqual(0, (int) test_grid_2->valid);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_width_for_grid_1()
            {
                Assert::AreEqual(1, (int) test_grid_1->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_width_for_grid_2()
            {
                Assert::AreEqual(2, (int) test_grid_2->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_width_for_grid_4()
            {
                Assert::AreEqual(4, (int) test_grid_4->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_width_for_grid_5()
            {
                Assert::AreEqual(0, (int) test_grid_5->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_height_for_grid_1()
            {
                // Default is Medium
                // XX_SMALL (0),
                // X_SMALL (1),
                // SMALL (2),
                // MEDIUM (3),
                // LARGE (4),
                // X_LARGE (5),
                // XX_LARGE (6)
                Assert::AreEqual(0, (int) test_grid_1->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_height_for_grid_2()
            {
                // Default is Medium
                // XX_SMALL (0),
                // X_SMALL (1),
                // SMALL (2),
                // MEDIUM (3),
                // LARGE (4),
                // X_LARGE (5),
                // XX_LARGE (6)
                Assert::AreEqual(3, (int) test_grid_2->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_height_for_grid_4()
            {
                Assert::AreEqual(5, (int) test_grid_4->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_height_for_grid_5()
            {
                // Default is Medium
                // XX_SMALL (0),
                // X_SMALL (1),
                // SMALL (2),
                // MEDIUM (3),
                // LARGE (4),
                // X_LARGE (5),
                // XX_LARGE (6)
                Assert::AreEqual(6, (int) test_grid_5->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_1()
            {
                Assert::AreEqual(6, (int) test_grid_1->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_2()
            {
                Assert::AreEqual(4, (int) test_grid_2->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_3()
            {
                Assert::AreEqual(3, (int) test_grid_3->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_4()
            {
                Assert::AreEqual(8, (int) test_grid_4->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_5()
            {
                Assert::AreEqual(1, (int) test_grid_5->vectors.count);
            }
        };

        [TestClass]
        public ref class When_reading_VECTORS_from_PB
        {
        public:
            static FLAT_GRID* test_grid_1;
            static FLAT_GRID* test_grid_2;
            static FLAT_GRID* test_grid_3;
            static FLAT_GRID* test_grid_4;
            static FLAT_GRID* test_grid_5;

            static FDI_GENERIC_ITEM* genericItem_grid_1;
            static FDI_GENERIC_ITEM* genericItem_grid_2;
            static FDI_GENERIC_ITEM* genericItem_grid_3;
            static FDI_GENERIC_ITEM* genericItem_grid_4;
            static FDI_GENERIC_ITEM* genericItem_grid_5;
            static  EddEngineController^  engine;

            static ITEM_ID* test_int_item_ID;
            static ITEM_ID* test_uint_item_ID;
            static ITEM_ID* test_ascii_item_ID;
            static ITEM_ID* test_float_item_ID;
            static ITEM_ID* test_double_item_ID;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0008.DDL.bin");
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_grid_1_specifier;
                engine->FillParamSpecifier("grid_1", &test_grid_1_specifier);
                genericItem_grid_1 = engine->GetGenericItem(test_grid_1_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_1 = dynamic_cast<FLAT_GRID*>(genericItem_grid_1->item);

                FDI_PARAM_SPECIFIER test_grid_2_specifier;
                engine->FillParamSpecifier("grid_2", &test_grid_2_specifier);
                genericItem_grid_2 = engine->GetGenericItem(test_grid_2_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_2 = dynamic_cast<FLAT_GRID*>(genericItem_grid_2->item);

                FDI_PARAM_SPECIFIER test_grid_3_specifier;
                engine->FillParamSpecifier("grid_3", &test_grid_3_specifier);
                genericItem_grid_3 = engine->GetGenericItem(test_grid_3_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_3 = dynamic_cast<FLAT_GRID*>(genericItem_grid_3->item);

                FDI_PARAM_SPECIFIER test_grid_4_specifier;
                engine->FillParamSpecifier("grid_4", &test_grid_4_specifier);
                genericItem_grid_4 = engine->GetGenericItem(test_grid_4_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_4 = dynamic_cast<FLAT_GRID*>(genericItem_grid_4->item);

                FDI_PARAM_SPECIFIER test_grid_5_specifier;
                engine->FillParamSpecifier("grid_5", &test_grid_5_specifier);
                genericItem_grid_5 = engine->GetGenericItem(test_grid_5_specifier.id, nsEDDEngine::ITYPE_GRID);
                test_grid_5 = dynamic_cast<FLAT_GRID*>(genericItem_grid_5->item);

                // get the item ids for all parameters
                test_int_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_int", test_int_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_uint_item_ID = new ITEM_ID();
                returnCode =engine->Instance->GetItemIdFromSymbolName(L"test_uint", test_uint_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_ascii_item_ID = new ITEM_ID();
                returnCode =engine->Instance->GetItemIdFromSymbolName(L"test_ascii", test_ascii_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_float_item_ID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_float", test_float_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_double_item_ID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_double", test_double_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem_grid_1;
                delete genericItem_grid_2;
                delete genericItem_grid_3;
                delete genericItem_grid_4;
                delete genericItem_grid_5;
                delete engine;

                delete test_int_item_ID;
                delete test_uint_item_ID;
                delete test_ascii_item_ID;
                delete test_float_item_ID;
                delete test_double_item_ID;
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_1()
            {
                Assert::AreEqual(6, (int) test_grid_1->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_2()
            {
                Assert::AreEqual(4, (int) test_grid_2->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_3()
            {
                Assert::AreEqual(3, (int) test_grid_3->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_4()
            {
                Assert::AreEqual(8, (int) test_grid_4->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_vectors_for_grid_5()
            {
                Assert::AreEqual(1, (int) test_grid_5->vectors.count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_elements_for_first_vector_in_grid_1()
            {
                Assert::AreEqual(1, (int) test_grid_1->vectors.vectors[0].count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_elements_for_second_vector_in_grid_1()
            {
                Assert::AreEqual(1, (int) test_grid_1->vectors.vectors[1].count);

            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_elements_for_third_vector_in_grid_1()
            {
                Assert::AreEqual(3, (int) test_grid_1->vectors.vectors[2].count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_elements_for_forth_vector_in_grid_1()
            {
                Assert::AreEqual(3, (int) test_grid_1->vectors.vectors[3].count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_number_of_elements_for_fifth_vector_in_grid_1()
            {
                Assert::AreEqual(4, (int) test_grid_1->vectors.vectors[4].count);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_desciption_for_first_vector_in_grid_1()
            {
                Assert::AreEqual("Row1", gcnew String(test_grid_1->vectors.vectors[0].description.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_desciption_for_second_vector_in_grid_2()
            {
                Assert::AreEqual("Column2", gcnew String(test_grid_2->vectors.vectors[1].description.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_first_value_for_first_vector_in_grid_1()
            {
                Assert::AreEqual(9, (int) test_grid_1->vectors.vectors[0].list[0].vector.uconst);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_type_for_first_value_for_first_vector_in_grid_1()
            {
                Assert::AreEqual(7, (int) test_grid_1->vectors.vectors[0].list[0].type);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_first_value_for_third_vector_in_grid_1()
            {
                Assert::AreEqual(7.8, test_grid_1->vectors.vectors[2].list[0].vector.dconst);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_type_for_first_value_in_third_vector_in_grid_1()
            {
                Assert::AreEqual(8, (int) test_grid_1->vectors.vectors[2].list[0].type);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_type_for_second_value_for_fifth_vector_in_grid_1()
            {
                auto myType =  test_grid_1->vectors.vectors[4].list[1].type;
                Assert::AreEqual(6, (int) test_grid_1->vectors.vectors[4].list[1].type);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_second_value_for_fifth_vector_in_grid_1()
            {
                Assert::AreEqual("field 2", gcnew String(test_grid_1->vectors.vectors[4].list[1].vector.str.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_item_id_for_first_value_in_second_vector_in_grid_1()
            {
                Assert::AreEqual(*test_int_item_ID, test_grid_1->vectors.vectors[1].list[0].vector.ref.op_info.id);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_item_id_for_second_value_in_third_vector_in_grid_1()
            {
                Assert::AreEqual(*test_uint_item_ID, test_grid_1->vectors.vectors[2].list[1].vector.ref.op_info.id);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_item_id_for_third_value_in_third_vector_in_grid_1()
            {
                Assert::AreEqual(*test_ascii_item_ID, test_grid_1->vectors.vectors[2].list[2].vector.ref.op_info.id);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_item_id_for_first_value_in_forth_vector_in_grid_1()
            {
                Assert::AreEqual(*test_float_item_ID, test_grid_1->vectors.vectors[3].list[0].vector.ref.op_info.id);

            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_return_the_correct_item_id_for_third_value_in_forth_vector_in_grid_1()
            {
                Assert::AreEqual(*test_double_item_ID, test_grid_1->vectors.vectors[3].list[2].vector.ref.op_info.id);

            }
        };

    }
}
