// Parser.cpp //

#include "stdafx.h"
#include "Parser.h"
#include "MEE.h"

#include "ErrorDefinitions.h"


CParser::CParser()
{
	pBuiltInLib = NULL;
	m_pMEE		= NULL; //Vibhor 010705: Added
	m_bIsRoutine =false;//Anil Octobet 5 2005 for handling Method Calling Method
}

CParser::~CParser()
{

}

#ifdef STANDALONE_PARSER
bool CParser::Initialize(CBuiltInLib *pBuiltInLibParam)
#else
bool CParser::Initialize(CBuiltIn *pBuiltInLibParam,MEE *pMEE) //Vibhor 010705: Added
#endif
{
	if (pBuiltInLibParam)
	{
		pBuiltInLib = pBuiltInLibParam;
		m_pMEE = pMEE; //Vibhor 010705: Added
		interpretor.Initialize (pBuiltInLibParam,m_pMEE);
		//Anil Octobet 5 2005 for handling Method Calling Method
		//This is required to differentite whether it is called method or it is a method called from menu
		interpretor.SetIsRoutineFlag(m_bIsRoutine);
		if(lexAnal.InitMeeInterface(m_pMEE))
		return true;
		else
			return false;

	}
	else
	{
		return false;
	}
}


_INT32 CParser::BuildParseTree(
			_UCHAR* pszSource,
			_UCHAR*	pszRuleName,
			_CHAR* szData,
			_CHAR* szSymbolTable)
{
	lexAnal.Load(
		pszSource,
		pszRuleName,
		0);

	pgm.CreateParseSubTree(
		&lexAnal,
		&SymbolTable,
		0);

	pgm.Execute(&typeChecker,&SymbolTable,0);
	
	pgm.Identify(szData);

	return 0;
}

// This function builds a parse tree and executes the parse tree directly.
// This is done by CInterpretedVisitor.
_INT32 CParser::Execute(
			_UCHAR* pszSource,
			_UCHAR*	pszRuleName,
			_CHAR* szData,
			_CHAR* &szSymbolTable)
{
	lexAnal.Load(
		(_UCHAR*)pszSource,
		pszRuleName,
		0);

	CLexicalAnalyzer	lexAnalPredefinedDeclarations;
	//propagate MEE pointer to CLexicalAnalyzer incstance
	lexAnalPredefinedDeclarations.InitMeeInterface(m_pMEE);

	char pchDecSource[] = "{int _bi_rc;}";

	lexAnalPredefinedDeclarations.Load 
		(
			(_UCHAR*)pchDecSource,
			pszRuleName,
			0
		);
#ifdef _DEBUG
	int point = 0;
#endif

	ERROR_VEC pvecErrors;
	try
	{
		/*<START>TSRPRASAD 09MAR2004 Fix the memory leaks	*/
#ifdef _DEBUG
	point = 1;
#endif
		CProgram	pgm1;
		int iRet32 = pgm1.CreateParseSubTree
									(
										&lexAnalPredefinedDeclarations
										, &SymbolTable
										, &pvecErrors
									);
		/*<END>TSRPRASAD 09MAR2004 Fix the memory leaks	*/
		lexAnalPredefinedDeclarations.~CLexicalAnalyzer();
		pgm1.~CProgram();

#ifdef _DEBUG
	point = 2;
#endif
		if (iRet32 == PARSE_FAIL)
		{
			return (ExecutionErrorHandling(C_UM_ERROR_UNKNOWNERROR, &pvecErrors, szData));
		}

		iRet32 = pgm.CreateParseSubTree
									(
										&lexAnal
										, &SymbolTable
										, &pvecErrors
									);
#ifdef _DEBUG
	point = 3;
#endif
		if (iRet32 == PARSE_FAIL)
		{
			return (ExecutionErrorHandling(C_UM_ERROR_UNKNOWNERROR, &pvecErrors, szData));
		}
#ifdef STANDALONE_PARSER
		pgm.Identify(szData);
#endif
#ifdef _DEBUG
	point = 4;
#endif
	}
	catch(_INT32 error)
	{
		return (ExecutionErrorHandling(error, &pvecErrors, szData));
	}

#ifdef PARSER_TEST
	return INTERPRETER_STATUS_OK;
#endif /* PARSER_TEST */
// The following code visits all the nodes in the Parse tree
// and tries to interpret it...
//CJK THIS IS IT REDO THIS calls CPROGRAM Execute which gets to C
	try
	{
		_INT32 iRet32 = pgm.Execute(
							&interpretor,
							&SymbolTable,
							NULL,
							&pvecErrors);

		//save method results into a string
		SymbolTable.TraceDump(szSymbolTable);

#ifdef STANDALONE_PARSER
		SymbolTable.TraceDump(szSymbolTable);
#endif

		return (ExecutionErrorHandling(iRet32, &pvecErrors, szData));
	}
	catch(_INT32 error)
	{
		return (ExecutionErrorHandling(error, &pvecErrors, szData));
	}

	return INTERPRETER_STATUS_OK;
}

bool CParser::GetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
				)
{
	bool isDone = false;

	//check if it is a numeric value
	char *stopstring;
	double dVarVal = strtod(pchVariableName, &stopstring);
	if (stopstring[0] == '\0')
	{
		//This name string is a numeric value string
		varValue = (double)dVarVal;

		isDone = true;
	}
	else if ( (pchVariableName[0] == '0') && ((pchVariableName[1] == 'x') || (pchVariableName[1] == 'X')) )
	{
		//convert Hex string to integer
		unsigned long long ullVal = strtoull(pchVariableName, &stopstring, 16);

		if (stopstring[0] == '\0')
		{
			//This name string is a numeric value string
			varValue = (unsigned long long)ullVal;

			isDone = true;
		}
	}
	
	if (!isDone)
	{
		//it must be an expression
		CVariable *pVar = NULL;
		bool bLocal = false;
		FindInLocalSymbolTable(pchVariableName, &pVar, &bLocal);
		if (pVar != NULL)
		{
			char *pchBracket = strchr(pchVariableName, '[');
			if (bLocal && (!pchBracket))
			{
				//get local variable or local string value from token
				varValue = pVar->GetValue();
				isDone = true;
			}
			else
			{
				//get array index and update operative item ID based on variable expression
				INTER_VARIANT ivIndex;
				PARAM_REF opRef = *(pVar->GetParamRef());
				m_pMEE->ResolveVarExp(pchVariableName, pVar->m_bIsRoutineToken, &ivIndex, &opRef);

				if (pVar->IsDDItem())
				{
					//get value from parameter cache
					hCitemBase *pIB = NULL;
					RETURNCODE iretCode = m_pMEE->ResolveDDExp((const char*)pchVariableName, &opRef, &varValue, &pIB);
					if (iretCode == SUCCESS)
					{
						isDone = true;
					}
				}
				else
				{
					//it must be a local array
					INTER_SAFEARRAY *theArray = (pVar->GetValue()).GetSafeArray();
					if (theArray != NULL)
					{
						int idims = theArray->GetDims(NULL);
						if (idims == 1)
						{
							//one dimension
							theArray->GetElement(((int)ivIndex * theArray->GetElementSize()), &varValue);
						}
						else
						{
							//multiple dimensions
							theArray->GetDimension((int)ivIndex, &varValue);
						}

						isDone = true;
					}
				}
			}
		}
	}

	return isDone;
}

bool CParser::SetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
				)
{
	bool isDone = false;

	CVariable *pVar = NULL;
	bool bLocal = false;
	FindInLocalSymbolTable(pchVariableName, &pVar, &bLocal);
	if (pVar != NULL)
	{
		//local variable is found
		//Write the value to parameter cache if it is necessary or to token
		if (!bLocal)
		{
			hCitemBase *pIB = NULL;
			RETURNCODE iReturnValue = m_pMEE->ResolveNUpdateDDExp((const char*)pchVariableName,	pVar->GetParamRef(), &varValue, RUL_ASSIGN);
			if (iReturnValue == SUCCESS)
			{
				isDone = true;
			}
		}
		else
		{
			pVar->GetValue() = varValue;
			isDone = true;
		}
	}

	return isDone;
}

bool CParser::SetVariableValue
				(
					CExpression *pExp
					, INTER_VARIANT &varValue
				)
{
	//set L-VALUE
	interpretor.SetLValueFlag(true);
	_INT32 iRetVal = pExp->Execute(&interpretor, &SymbolTable, &varValue, NULL, RUL_ASSIGN);
	if (iRetVal == VISIT_NORMAL)
	{
		return true;
	}
	else
	{
		return false;
	}
}


//This function finds a given variable name in local SymbolTable.
//If it is found and it is wanted, the variable pointer is output.
//This function also returns whether it is method local variable.
void CParser::FindInLocalSymbolTable (/*in*/char *pchVariableName, /*out*/CVariable **ppVar, /*out*/ bool *pbIsMethodLocal)
{
	size_t lCount = strlen(pchVariableName);
	char *pchBracket = strchr(pchVariableName, '[');

	//get the variable name
	if (pchBracket)
	{
		lCount = pchBracket - pchVariableName;
	}
    char *sLocalVarName = (char *)malloc(lCount+1);	//allocate memory in stack so that execution is faster
	PS_Strncpy(sLocalVarName, lCount+1, pchVariableName, lCount);

	CVariable *pVar = SymbolTable.Find(sLocalVarName);
	if ((pVar != NULL) && !pVar->IsDDItem())
	{
		*pbIsMethodLocal = true;
	}
	else
	{
		*pbIsMethodLocal = false;
	}

	//clear sLocalVarName memory
	if (sLocalVarName)
	{
        free(sLocalVarName); //_freea
	}

	if (ppVar)
	{
		*ppVar = pVar;
	}
}

void CParser::GetLocalVariables(wchar_t **pDebugInfoXml)
{
	char *pbySymbolTable = NULL;
	
	SymbolTable.DumpMethodDebugInfo(pbySymbolTable);
	*pDebugInfoXml = new wchar_t[strlen(pbySymbolTable) + 1];

	size_t size = 0;
	PS_Mbtowcs(&size, *pDebugInfoXml, strlen(pbySymbolTable) + 1, pbySymbolTable, strlen(pbySymbolTable));

	delete pbySymbolTable;
}

//This is an OverLoaded function for the Methods calling Methods
//This will be called only by MEE in the " called Method" case
//METHOD_ARG_INFO_VECTOR is a vector of Method argument, which contains all the related info regarding the Method parameter
//Here even before calling pgm.CreateParseSubTree, We need to push the Method argument to the Symbol
//Table. If it is passed by the Value, then this need to be initilized with the corresponding value
//in the case of Simple variable, Just create tokemn and Push it on symbol table and then push the vale
//In case of Array variables, Create the variable name with dadta type dynamically and execute the Declaration list
//and initilize with its value----also do not Excute this Declaration list for these variables


_INT32 CParser::Execute (
			_UCHAR* pszSource,
			_UCHAR*	pszRuleName,
			_CHAR* szData,
			_CHAR* &szSymbolTable,	
			METHOD_ARG_INFO_VECTOR* vectMethArg,
			vector<INTER_VARIANT>* vectInterVar )
{
	lexAnal.Load(
		(_UCHAR*)pszSource,
		pszRuleName,
		0);

	//As some of the Dd have this variacle
	CLexicalAnalyzer	lexAnalPredefinedDeclarations;
	//propagate MEE pointer to CLexicalAnalyzer incstance
	lexAnalPredefinedDeclarations.InitMeeInterface(m_pMEE);

	char pchDecSource[] = "{int _bi_rc;}";

	lexAnalPredefinedDeclarations.Load 
		(
			(_UCHAR*)pchDecSource,
			pszRuleName,
			0
		);

	ERROR_VEC pvecErrors;
	try
	{		
		CProgram	pgm1;
		int iRet32 = pgm1.CreateParseSubTree
									(
										&lexAnalPredefinedDeclarations
										, &SymbolTable
										, &pvecErrors
									);
	
		lexAnalPredefinedDeclarations.~CLexicalAnalyzer();
		pgm1.~CProgram();

		if (iRet32 == PARSE_FAIL)
		{
			return (ExecutionErrorHandling(C_UM_ERROR_UNKNOWNERROR, &pvecErrors, szData));
		}

		int iNoOfArgs = (int)vectMethArg->size();
		//Loop through each of the argument, including return 
		for(int i =0; i<iNoOfArgs; i++)
		{
			
			METHOD_ARG_INFO* pMethArgInfo;
			pMethArgInfo = &(*vectMethArg)[i];
			//For each of the  Function Parameter, Get the token type and Token Sub type
			RUL_TOKEN_TYPE	   tokenType = pMethArgInfo->GetType();
			RUL_TOKEN_SUBTYPE  tokenSubType = pMethArgInfo->GetSubType();
			INTER_VARIANT* vartemp = &(*vectInterVar)[i];

			
			//Create method argument token and save it into local variable symbol table
			//check for the array type based on method argument info and argument value type,
			//if not it should be Simple or DD item
			if ((RUL_ARRAY_VARIABLE != tokenType) || (vartemp->GetVarType() != RUL_SAFEARRAY))
			{
				//Create a Token with the function Arg name
				// changed to below WS:EPM 17jul07 CToken* pToken = new CToken(pMethArgInfo->GetCalledArgName(),
				CToken localToken(pMethArgInfo->GetCalledArgName(),
											tokenType,
											tokenSubType,
											0);
				//add DD item name into localToken
				localToken.SetDDItemName(pMethArgInfo->GetCallerArgName());
				//add DD operative item ID
				localToken.SetParamRef(&(pMethArgInfo->prDDItemId));

				//Push it on to the Symbol table--
				//after this it is similar to   declaration int Varname;
				bool bCreateNewScope = (i == 0) ? true : false;
				int nIndx = SymbolTable.InsertInScope(localToken, bCreateNewScope);//symbolTable::Insert makes a copy of this token.
				
				//Assign the value-- similar to excution of Varname = 10;
				CVariable* pStore = SymbolTable.GetAt(nIndx);
				//Initilize this to the routine token to Distiguish
				pStore->m_bIsRoutineToken = true;
				pStore->m_bIsReturnToken = pMethArgInfo->m_IsReturnVar;
				//Assigh the value that is in the Vector of variant. Input argument is assigned into local variable table.
				pStore->GetValue() = *vartemp;
			}
			else // this argument is in array type
			{
				//In this case u need to Execute the Declaration list 
				//becasue each of the array element has to be assigned seperately as safe array		
				
				//Extract the caller array value
				
				INTER_SAFEARRAY* prgsaCaller = vartemp->GetValue().prgsa;
				vector<_INT32> vecDims;
				//vecDims[0] gives the dimension of the array
				prgsaCaller->GetDims(&vecDims);
				_INT32 i32mem = prgsaCaller->MemoryAllocated();
				
				//iMemsize is the each element size in the array
				int iMemsize = i32mem/vecDims[0];
				int iArraysize = vecDims[0];
				char szBufSize[50] ;
                itoa(iArraysize,szBufSize,10);


				//We got array size noe, We need to form a declaration statement lik say int Paramname[10]; 
				//and execute this declaration statement	

				char szDataTypeIdentifier[20] = {0};
				

				switch(tokenSubType)
				{
					// Walt EPM 08sep08- added
					case RUL_CHAR_DECL:			
						{
							strcpy(szDataTypeIdentifier ,"char");							
						}
						break;
					case RUL_UNSIGNED_CHAR_DECL:			
						{
							strcpy(szDataTypeIdentifier ,"unsigned char");							
						}
						break;
					case RUL_SHORT_INTEGER_DECL:			
						{
							strcpy(szDataTypeIdentifier ,"short");							
						}
						break;
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:			
						{
							strcpy(szDataTypeIdentifier ,"unsigned short");							
						}
						break;
					case RUL_INTEGER_DECL:
						{
							strcpy(szDataTypeIdentifier, "int");						
						}
						break;
					case RUL_LONG_DECL:
						{
							strcpy(szDataTypeIdentifier ,"long");
						}
						break;
					case RUL_UNSIGNED_INTEGER_DECL:
						{
							strcpy(szDataTypeIdentifier, "unsigned int");						
						}
						break;
					case RUL_LONG_LONG_DECL:
						{
							strcpy(szDataTypeIdentifier ,"long long");
						}
						break;
					case RUL_UNSIGNED_LONG_LONG_DECL:
						{
							strcpy(szDataTypeIdentifier ,"unsigned long long");
						}
						break;
					case RUL_DD_STRING_DECL:			
						{
							strcpy(szDataTypeIdentifier ,"DD_STRING");							
						}
						break;
					// Walt EPM 08sep08 - end added
					case RUL_REAL_DECL:
						{
							strcpy(szDataTypeIdentifier ,"float");							
						}
						break;

					case RUL_DOUBLE_DECL:
						{
							strcpy(szDataTypeIdentifier ,"double");							
						}
						break;
					case RUL_TIMET_DECL:
						{
							strcpy(szDataTypeIdentifier ,"time_t");
						}
						break;

					default:
						{
							DEBUGLOG(CLOG_LOG,"Unhandled sub-token type\n");
							throw (C_UM_ERROR_UNKNOWNERROR);
							//TO DO We may need to validate the   whether it id DD item and Check accordingle
						//error!!!!
						}
						break;

				}//end switch


				//Following is the way to Declarethe arry variable Dynamically--Please follow properly
				//3- for  "{  "
				//szDataTypeIdentifier 
				//3- for "   "
				//pMethArgInfo->GetCalledArgName()
				//1-for "["
				//szBufSize
				//3- "];}"
				//1- NULL
				size_t ilen = 3 + strlen(szDataTypeIdentifier) + 3 + 
					           strlen( pMethArgInfo->GetCalledArgName() ) + 1 + 
							   strlen(szBufSize) + 
							   strlen(szDataTypeIdentifier) + 3 + 1;			

				
				//form the dynamic declaration for array
				char* pchVarDecl;
				pchVarDecl = new char[ilen];

				PS_Strcpy(pchVarDecl, ilen, "{  ");
				PS_Strcat(pchVarDecl, ilen, szDataTypeIdentifier);
				PS_Strcat(pchVarDecl, ilen, "   ");

				PS_Strcat(pchVarDecl , ilen,pMethArgInfo->GetCalledArgName());
				PS_Strcat(pchVarDecl , ilen,"[");
				PS_Strcat(pchVarDecl , ilen,szBufSize);
				PS_Strcat(pchVarDecl , ilen,"];}");


				
				//_UCHAR	pszRuleName[] = "Test";	//this is an input and the value is already "Test"
				CLexicalAnalyzer	lexAnalPredefinedDeclarations1;
				//propagate MEE pointer to CLexicalAnalyzer incstance
				lexAnalPredefinedDeclarations1.InitMeeInterface(m_pMEE);

				//Now Execute this by loading in to Temporory CLexicalAnalyzer
				lexAnalPredefinedDeclarations1.Load((_UCHAR*)pchVarDecl,pszRuleName,0);

				CProgram	pgm2;
				iRet32 = pgm2.CreateParseSubTree(&lexAnalPredefinedDeclarations1, &SymbolTable, &pvecErrors);

				lexAnalPredefinedDeclarations1.~CLexicalAnalyzer();
				pgm2.~CProgram();

				if (iRet32 == PARSE_FAIL)
				{
					if (pchVarDecl != NULL)
					{
						delete[] pchVarDecl;
					}
					return (ExecutionErrorHandling(C_UM_ERROR_UNKNOWNERROR, &pvecErrors, szData));
				}
				
				//Below is the the way to initilize any array value in the interpreter
				CVariable* pVariable = SymbolTable.Find( pMethArgInfo->GetCalledArgName());
				pVariable->m_bIsRoutineToken = true;
				pVariable->m_bIsReturnToken = pMethArgInfo->m_IsReturnVar;
				//add DD item name into localToken
				pVariable->SetDDItemName(pMethArgInfo->GetCallerArgName());
				//add DD operative item ID
				pVariable->SetParamRef(&(pMethArgInfo->prDDItemId));
				
				
				 
				//Now intitialize each element of the arry to the passed value
				INTER_VARIANT& var = pVariable->GetValue();
				if(RUL_SAFEARRAY == var.GetVarType())
				{
					(var.GetValue().prgsa)->Allocate();
				}
				//Get the called 
				INTER_SAFEARRAY* prgsaCalled= pVariable->GetValue().GetValue().prgsa;

				//Assigh the value that is in the Vector of variant. Input argument is assigned into local variable table.
				for(int iCount = 0;iCount<iArraysize ; iCount++)
				{
					INTER_VARIANT VarTemp;
					VarTemp.Clear();
					//Get the element and then set the element--This is the way to initilize any arry value in the interpreter
					prgsaCaller->GetElement(iMemsize*iCount,&VarTemp);
					prgsaCalled->SetElement(iMemsize*iCount,&VarTemp);			
				
				}	

				if (pchVarDecl != NULL)
				{
					delete [] pchVarDecl;
				}
			}// end else RUL_ARRAY_VARIABLE == tokenType
		}// next arg

		//When I come here, I am assured that all the  
		//Parameters that are passed in the function are in the symbol table with its value initilized to the
		//the passed value

		iRet32 = pgm.CreateParseSubTree
									(
										&lexAnal
										, &SymbolTable
										, &pvecErrors
									);
		if (iRet32 == PARSE_FAIL)
		{
			return (ExecutionErrorHandling(C_UM_ERROR_UNKNOWNERROR, &pvecErrors, szData));
		}
#ifdef STANDALONE_PARSER
		pgm.Identify(szData);
#endif
	}
	catch(_INT32 error)
	{
		return (ExecutionErrorHandling(error, &pvecErrors, szData));
	}

#ifdef PARSER_TEST
	return INTERPRETER_STATUS_OK;
#endif /* PARSER_TEST */
// The following code visits all the nodes in the Parse tree
// and tries to interpret it...

	try
	{
		_INT32 iRet32 = pgm.Execute(&interpretor, &SymbolTable, NULL, &pvecErrors);


	//save method results into a string
	SymbolTable.TraceDump(szSymbolTable);

#ifdef STANDALONE_PARSER
		SymbolTable.TraceDump(szSymbolTable);
#endif

		//Update the return value and also the parameter which are passed by reference
		//
		int iNoOfArgs = (int)vectMethArg->size();
		for(int i =0; i<iNoOfArgs; i++)
		{
			
			METHOD_ARG_INFO* pMethArgInfo;
			pMethArgInfo = &(*vectMethArg)[i];

			if(pMethArgInfo->ePassedType == DD_METH_AGR_PASSED_BYREFERENCE)
			{
				CVariable* pStore=0;
				pStore = SymbolTable.Find(pMethArgInfo->GetCalledArgName());

				if( pStore && (pMethArgInfo->GetType() != RUL_DD_ITEM) )
				{
					//save value into method argument list which is in "passed by reference" type
					(*vectInterVar)[i] = pStore->GetValue();
				}
				
			}	
		}

		return (ExecutionErrorHandling(iRet32, &pvecErrors, szData));
	}
	catch(_INT32 error)
	{
		return (ExecutionErrorHandling(error, &pvecErrors, szData));
	}

	return INTERPRETER_STATUS_OK;
}

_INT32 CParser::ExecutionErrorHandling(/* in */ _INT32 iErr, /* in */ ERROR_VEC *pvecErrors, /* out */_CHAR* szData)
{
	_INT32 iThrowError = 0;
	if (pvecErrors->size() > 0 )
	{
		for (ERROR_VEC::iterator iT = pvecErrors->begin(); iT != pvecErrors->end(); iT++)
		{
			if (*iT)
			{
				if (!iThrowError)
				{
					/* output first error message */
					PS_Strcpy(szData, RULENAMELEN, (*iT)->getRuleName());
					/* get first error code */
					iThrowError = (*iT)->getErrorNumber();
				}
				delete (*iT);
			}
		}
		pvecErrors->clear();
	}
	
	switch (iThrowError)
	{
	case C_UM_ERROR_METHODERROR:
	case C_UM_ERROR_DDS_PARAM:
	case C_UM_ERROR_INDEX_OUT_OF_RANGE:
	case C_UM_ERROR_MISSINGTOKEN:
		return INTERPRETER_STATUS_EXECUTION_ERROR;
		break;
	case C_UM_ERROR_UI_CANCELLED:
	case C_UM_ERROR_METH_CANCELLED:
		return INTERPRETER_STATUS_EXECUTION_CANCEL;
		break;
	case C_UM_ERROR_COMM_ABORTED:
	case C_UM_ERROR_COMM_RETRY_ABORTED:
	case C_UM_ERROR_VAR_ABORTED:
	case C_UM_ERROR_OPER_ABORTED:
	case C_UM_ERROR_COMM_NO_DEVICE:
	case C_UM_ERROR_COMM_FAIL_RESPONSE:
		return INTERPRETER_STATUS_EXECUTION_ABORT;
		break;
	case C_UM_ERROR_UNKNOWNERROR:
		return INTERPRETER_STATUS_PARSE_ERROR;
		break;

	default:
		switch (iErr)
		{
		case VISIT_ERROR:
			return INTERPRETER_STATUS_EXECUTION_ERROR;
			break;
		case VISIT_ABORT:		//abort builtin
			return INTERPRETER_STATUS_EXECUTION_ABORT;
			break;
		case VISIT_BREAK:		//break statement
		case VISIT_CONTINUE:	//continue statement
		case VISIT_RETURN:		//return statement
		case VISIT_NORMAL:
			return INTERPRETER_STATUS_OK;
			break;
		default:
			return INTERPRETER_STATUS_UNKNOWN_ERROR;
			break;
		}
		break;
	}

	return INTERPRETER_STATUS_OK;	//shall never be here
}
	
//Function to set the FLAG that whether it is a routine call( ie Called method), ie it is not called by the menu
void CParser::SetIsRoutineFlag(bool bIsRoutine)
{
	m_bIsRoutine = bIsRoutine;
	return;
}
//To get the Routine flag
bool CParser::GetIsRoutineFlag()
{
	return m_bIsRoutine;
}
