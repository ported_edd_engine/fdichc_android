/**
 *	@(#) $Id: evl_base.c,v 1.3 1996/01/04 20:12:20 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions that are common to all
 *	evaluation data structure parsing and evaluation.  This is
 *	the "base" set of functions.
 */


#include "stdinc.h"

#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif


/*********************************************************************
 *  Name: 		ddl_parse_integer_func
 *  ShortDesc: 	parse an integer from the binary
 *
 *  Description:
 *		ddl_parse_integer_func parses an integer (1 to 4 bytes long)
 *	 	from the binary pointed at by "chunkp".  The parsed integer
 *		is stored in "value".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		value - contains the value of the integer parsed from the binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_integer_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *value)
{
	DDL_UINT        cnt;	/* temp ptr to size of binary */
	DDL_UINT        val;	/* temp storage for parsed integer */
	unsigned char  *chunk;	/* temp ptr to binary */
	uchar           c;	/* temp ptr to binary */
	int             more_indicator;	/* need to parse another byte */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_INTEGER_FUNC);
#endif

	/*
	 * Read each character, building the ulong until the high order bit is
	 * not set
	 */

	val = 0;
	chunk = *chunkp;
	cnt = *size;

	do {
		if (cnt == 0) {
			return DDL_INSUFFICIENT_OCTETS;
		}

		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7f;

		if (val > ((unsigned) DDL_HUGE_INTEGER >> 7)) {
			return DDL_LARGE_VALUE;
		}

		val <<= 7;
		if (val > (unsigned) DDL_HUGE_INTEGER - c) {
			return DDL_LARGE_VALUE;
		}

		val |= c;
		--cnt;
	} while (more_indicator);

	/*
	 * Update the pointer and size, and return the value
	 */

	*size = cnt;
	*chunkp = chunk;

	if (value) {
		*value = val;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	memdump
 *
 *	ShortDesc:	dump memory
 *
 *	Description:
 *		sends memory chunk to trace 
 *
 *	Inputs:
 *		chunk -			pointer
 *
 *
 *********************************************************************/

void memdump(unsigned char *chunk)
{
	(void)chunk;
#ifdef _DEBUG
	int bytecnt=20;
	PS_TRACE(L"... memdump last %d bytes -> ( ",bytecnt);

    wchar_t buf[40];
	memset(buf,0,sizeof(buf));
	for(int i=0;i<bytecnt;i++)
	{
		unsigned char uc=((chunk-bytecnt)[i]);
		wsprintf(buf,L"%02x ",uc);
		PS_TRACE(buf);
	}
#endif
}

/*********************************************************************
 *  Name: 		ddl_parse_integer_long_func
 *  ShortDesc: 	parse an integer from the binary
 *
 *  Description:
 *		ddl_parse_integer_func parses an integer (1 to 4 bytes long)
 *	 	from the binary pointed at by "chunkp".  The parsed integer
 *		is stored in "value".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		value - contains the value of the integer parsed from the binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *
 *  Author: 
 *********************************************************************/
int
ddl_parse_integer_long_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT_LONG       *value)
{
	DDL_UINT        cnt;	/* temp ptr to size of binary */
	DDL_UINT_LONG   val;	/* temp storage for parsed integer */
	unsigned char  *chunk;	/* temp ptr to binary */
	uchar           c;	/* temp ptr to binary */
	int             more_indicator;	/* need to parse another byte */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_INTEGER_FUNC);
#endif

	/*
	 * Read each character, building the ulong until the high order bit is
	 * not set
	 */

	val = 0;
	chunk = *chunkp;
	cnt = *size;

	do {
		if (cnt == 0) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : not enough octets left to decode int");
			memdump(chunk);

			return DDL_INSUFFICIENT_OCTETS;
		}

		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7f;

		if (val > ((unsigned long long) DDL_HUGE_LONG_INTEGER >> 7)) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val <<= 7;
		if (val > (unsigned long long) DDL_HUGE_LONG_INTEGER - c) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val |= c;
		--cnt;
	} while (more_indicator);

	/*
	 * Update the pointer and size, and return the value
	 */

	*size = cnt;
	*chunkp = chunk;

	if (value) {
		*value = val;
	}

	return DDL_SUCCESS;
}

 /*********************************************************************
 *  Name: 		ddl_parse_tag_func
 *  ShortDesc: 	parse a tag from the binary
 *
 *  Description:
 *		ddl_parse_tag_func parses a tag, and the length of the binary
 *		chunk associated with this tag, from the binary pointed at
 *		by "chunkp".  The parsed tag is stored in "tagp".  The length
 *		of the associated binary is stored in "lenp".  "Chunkp" and
 *		"size" are then updated to point to the remaining binary chunk
 *		 and size.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		tagp - contains the value of the tag parsed from the binary.
 *		lenp - contains the lenght of binary chunk associated with the tag.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_ENCODING_ERROR
 *		DDL_SUCCESS
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_tag_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *tagp,
DDL_UINT       *lenp)
{
	int             lenflag;/* indicates implicit/explicit binary length */
	unsigned char  *chunk;	/* temp ptr to the binary chunk */
	DDL_UINT        cnt;	/* temp ptr to the size of the binary chunk */
	uchar           c;	/* current value of the char pointed at by
				 * chunk */
	int             rc;	/* return code */
	DDL_UINT        tag;	/* temp storage of parsed tag */
	DDL_UINT        length;	/* temp storage of length of binary assoc with
				 * tag */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_TAG_FUNC);
#endif

	chunk = *chunkp;
	cnt = *size;
	if (cnt == 0) {
		return DDL_INSUFFICIENT_OCTETS;
	}

	/*
	 * Read the first character, and determine if there is an explicit
	 * length specified (bit 7 == 1)
	 */

	c = *chunk++;
	cnt--;
	lenflag = c & 0x80;
	tag = c & 0x7f;

	/*
	 * If the tag from the first character is <= 126, we are through. If
	 * the tag is == 127, we need to build the tag id from the following
	 * characters.
	 */

	if (tag == 127) {
		DDL_PARSE_INTEGER(&chunk, &cnt, &tag);
		tag += 126;
	}

	if (tagp) {
		*tagp = tag;
	}

	/*
	 * If there is an explicit length, get it.
	 */

	if (!lenflag) {
		length = 0;
	}
	else {
		DDL_PARSE_INTEGER(&chunk, &cnt, &length);
	}

	if (length > cnt) {
		return DDL_ENCODING_ERROR;
	}

	if (lenp) {
		*lenp = length;
	}
	*size = cnt;
	*chunkp = chunk;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_float
 *	ShortDesc: Parse a floating point value from the binary
 *
 *	Description:
 *		ddl_parse_float parses a floating point value from the binary
 *		pointed at by "chunkp".  The parsed value is stored in "value".
 *		"Chunkp" and "size" are then updated to point to the remaining
 *		binary chunk and size.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		value - pointer to a float where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		value - pointer to a float containing the result
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_INSUFFICIENT_OCTETS
 *
 *	Author: Steve Beyerl
 *********************************************************************/

REQUIRE(sizeof(float) == 4);	/* assumes that a float will be IEEE 754 format */

int
ddl_parse_float(
unsigned char **chunkp,
DDL_UINT       *size,
float          *value)
{
	float           temp = 0.0;	/* temporary storage */

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_FLOAT);
#endif

	if (*size < (DDL_UINT) sizeof(float)) {
		return DDL_INSUFFICIENT_OCTETS;
	}

	/*
	 * Move the four bytes into the float, taking care to handle the
	 * different byte ordering.
	 */

#if defined(BIG_ENDIAN)		/* BIG_ENDIAN byte order! */

	(void) memcpy((char *) &temp, (char *) *chunkp, sizeof(float));

#else
	#if defined(LITTLE_ENDIAN)	/* LITTLE_ENDIAN byte order! */
		{
			unsigned char  *chunk = *chunkp;
			unsigned char  *bytep = (unsigned char *)&temp;
			bytep[3] = *(chunk++);//this code makes two assumptions:
			bytep[2] = *(chunk++);//1. The tokenizer defines float as four bytes (msb to lsb)
			bytep[1] = *(chunk++);//2. This compiler defines float as four bytes (lsb to msb)
			bytep[0] = *(chunk++);
		}
	#else
		/* Must define one or the other */

		REQUIRE(BIG_ENDIAN || LITTLE_ENDIAN);

	#endif
#endif

	*chunkp += sizeof(float);
	*size -= (DDL_UINT) sizeof(float);

	if (value) {
		*value = temp;
	}

	return DDL_SUCCESS;
}





/*********************************************************************
 *
 *	Name: ddl_ulong_choice
 *	ShortDesc: Choose the correct ulong from a binary.
 *
 *	Description:
 *		ddl_ulong_choice will parse the binary for an unsigned long int,
 *		according to the current conditionals (if any).  The value of
 *		the ulong and/or dependency information is returned.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UINT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and DDL_PARSE_INTEGER().
 *
 *	Author: Steve Beyerl
 *********************************************************************/

static int
ddl_ulong_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef ISPTEST
	TEST_FAIL(DDL_ULONG_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (intval && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		DDL_PARSE_INTEGER(&val.chunk, &val.size, intval);

		*data_valid = TRUE;	/* intval has been modified */
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_source_line_type_choice
 *	ShortDesc: Choose the correct ulong from a binary.
 *
 *	Description:
 *		ddl_source_line_type_choice will parse the binary for an unsigned long int,
 *		according to the current conditionals (if any).  The value of
 *		the ulong and/or dependency information is returned.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UINT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and DDL_PARSE_INTEGER().
 *
 *	Author: Ying Xu
 *********************************************************************/

static int
ddl_source_line_type_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef ISPTEST
	TEST_FAIL(DDL_ULONG_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (intval && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		DDL_PARSE_INTEGER(&val.chunk, &val.size, intval);
		
		if (*intval  == DATA_LINETYPE)
		{
			DDL_PARSE_INTEGER(&val.chunk, &val.size, intval);
			*intval = *intval + DDS_LINE_TYPE_DATA_N;
		}

		*data_valid = TRUE;	/* intval has been modified */
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_ulong
 *	ShortDesc: Evaluate an unsigned long integer
 *
 *	Description:
 *  	The eval_attr_ulong function evaluates a unsigned long integer.
 *  	The buffer pointed to by chunk should contain the binary
 *  	for an unsigned long int.  Size should specify the size.
 *		of the binary chunk.  If ulong is not a null pointer, the
 *		value is returned in u_long.  If depinfo is not a null pointer,
 *		dependency information about the reference is returned in
 *		depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		ulong - space for parsed ulong
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the remaining chunk of binary data
 *		size - size of the remaining chunk of binary data
 *		ulong - contains the value of the parsed unsigned long integer.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_ulong_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: steve beyerl
 *********************************************************************/
int
eval_attr_ulong(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);
	int             rc, valid;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ULONG);
#endif

	valid = 0;
	*u_long = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_ulong_choice(&chunk, &size, u_long, depinfo, &valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}
	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (u_long && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_source_waveform_line_type
 *	ShortDesc: Evaluate source line type
 *
 *	Description:
 *  	The eval_attr_source_waveform_line_type function evaluates a unsigned long integer.
 *  	The buffer pointed to by chunk should contain the binary
 *  	for an unsigned long int.  Size should specify the size.
 *		of the binary chunk.  If ulong is not a null pointer, the
 *		value is returned in u_long.  If depinfo is not a null pointer,
 *		dependency information about the reference is returned in
 *		depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		ulong - space for parsed ulong
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the remaining chunk of binary data
 *		size - size of the remaining chunk of binary data
 *		ulong - contains the value of the parsed unsigned long integer.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_ulong_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: ying xu
 *********************************************************************/
int
eval_attr_source_waveform_line_type(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);
	int             rc, valid;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ULONG);
#endif

	valid = 0;
	*u_long = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_source_line_type_choice(&chunk, &size, u_long, depinfo, &valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}
	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (u_long && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}
/*********************************************************************
 *  Name: 		ddl_parse_bitstring
 *  ShortDesc: 	parse a bitstring from the binary
 *
 *  Description:
 *		ddl_parse_bitstring parses a bitstring (up to 32 bits long)
 *	 	from the binary pointed at by "chunkp".  The parsed bitstring
 *		is stored in "bitstring".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - pointer to the remaining binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		bitstring - contains the value of the bitstring parsed from the
 *			binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_bitstring(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *bitstring)
{
	unsigned char  *chunk;	/* temp ptr to the binary chunk */
	DDL_UINT        read_mask;	/* mask used to read bits from the
					 * binary */
	DDL_UINT        write_mask;	/* mask used to store the bits  */
	DDL_UINT        value;	/* temp storage of parsed bits */
	DDL_UINT        count;	/* local value of binary size */
	int             more_indicator;	/* indicators more bits to be read */
	int             last_unused;	/* # of bits to ignore in last octet */
	int             bitlimit;	/* loop counter */
	unsigned char   c;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_BITSTRING);
#endif

	/*
	 * Read the first character
	 */

	chunk = *chunkp;
	count = *size;
	value = 0;
	count--;
	c = *chunk++;

	/*
	 * Special case the situation where there is no bitstring (that is, the
	 * bit string is zero).
	 */

	if (c == 0x80) {
		*size = count;
		*chunkp = chunk;

		if (bitstring) {
			*bitstring = value;
		}
		return DDL_SUCCESS;
	}

	/*
	 * Pull out the number of bits to ignore in the last octet, and the
	 * indicator of whether there are more octets.
	 */

	more_indicator = c & 0x10;
	last_unused = (c & 0xE0) >> 5;
	c &= 0xF;

	/*
	 * Build up the return value.  Note that in each octet, the highest
	 * order bit corresponds to the lowest order bit in the returned bit
	 * mask.  That is, in the first octet, bit 3 corresponds to bit 0, bit
	 * 2 to bit 1, etc.  In the next octet, bit 6 corresponds to bit 4, bit
	 * 5 to bit 5, bit 4 to bit 6, ...  In the next octet, bit 6
	 * corresponds to bit 11, bit 5 to bit 12, ...
	 */

	write_mask = 1;
	read_mask = 0x8;
	bitlimit = 4;

	while (more_indicator) {

		if (!count) {

			return DDL_INSUFFICIENT_OCTETS;
		}

		for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

			if (c & read_mask) {

				value |= write_mask;
			}
		}

		/*
		 * Read the next octet.
		 */

		count--;
		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7F;
		read_mask = 0x40;
		bitlimit = 7;
	}

	/*
	 * In the last octet, some of the bits may be ignored.  The number of
	 * bits to ignore was specified in the very first octet, and remembered
	 * in "last_unused".
	 */

	bitlimit -= last_unused;

	if (bitlimit <= 0) {

		return DDL_ENCODING_ERROR;
	}

	for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

		if (!write_mask) {

			return DDL_LARGE_VALUE;
		}
		if (c & read_mask) {

			value |= write_mask;
		}
	}

	*size = count;
	*chunkp = chunk;

	if (bitstring) {

		*bitstring = value;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_bitstring_choice
 *	ShortDesc: Choose the correct bitstring from a binary.
 *
 *	Description:
 *		ddl_bitstring_choice will parse the binary for a bitstring,
 *		according to the current conditionals (if any).  The value of
 *		the bitstring is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UNIT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_bitstring().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_bitstring_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef ISPTEST
	TEST_FAIL(DDL_BITSTRING_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is desired
	 */

	if (intval && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL.
		 */

		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		rc = ddl_parse_bitstring(&val.chunk, &val.size, intval);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*data_valid = TRUE;	/* intval has been updated */
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_attr_bitstring
 *	ShortDesc: Evalute a bitstring.
 *
 *	Description:
 *		The function eval_attr_bitstring parses a binary chunk of data and loads
 *		bitstr with a bitstring
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		bitstr - place for parsed bitstring
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		bitstr contains the parsed bitstring
 *		depinfo - dependency information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_bitstring_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: steve beyerl
 *
 *********************************************************************/

int
eval_attr_bitstring(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	DDL_UINT*		bitstr = static_cast<DDL_UINT*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in
				 * "bitstr" */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_BITSTRING);
#endif

	valid = 0;
	*bitstr = 0;	/**	initialize the output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_bitstring_choice(&chunk, &size, bitstr, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	if (bitstr && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_attr_definition
 *	ShortDesc: Evaluate a definition
 *
 *	Description:
 *		eval_attr_definition takes the definition char pointer and points
 *		it at the binary chunk.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		def - char pointer
 *
 *	Outputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		def is loaded with the size and pointer to the definition
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_NULL_POINTER
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

/* ARGSUSED */

int
eval_attr_definition(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *,
ENV_INFO       *,
OP_REF         *)
{
	DEFINITION*			def = static_cast<DEFINITION*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_DEFINITION);
#endif

	def->size = (unsigned long) size;
	if (def->data) {
		free((void *) def->data);
	}
	def->data = (char *)malloc((size_t)size);
	if (!def->data) {
		return DDL_MEMORY_ERROR;
	}
	strcpy((char *) def->data, (char *) chunk);

	return DDL_SUCCESS;
}
#pragma warning (default : 4100)



/*********************************************************************
 *
 *	Name: ddl_shrink_depinfo
 *	ShortDesc: 	Shrink OP_REF_LIST list to size of value depinfo.limit
 *
 *	Description:
 *		ddl_shrink_depinfo checks OP_REF_LIST list to the size of value
 *		depinfo.size. If OP_REF_LIST is NULL it returns with no change.
 *		If depinfo.limit is equal to depinfo.size it returns with no change.
 *		If depinfo.size is equal to zero it frees the list. Otherwise it
 *		reallocates memory for the resized list.
 *
 *	Inputs:
 *		depinfo - pointer to OP_REF_LIST structure
 *
 *	Outputs:
 *		depinfo - pointer to the resized list
 *
 *	Returns:
 *		DDL_MEMORY_ERROR -  unable to allocate memory for new list
 *		DDL_SUCCESS - OP_REF_LIST has been shrunk
 *
 *	Author: steve beyerl
 *********************************************************************/

int
ddl_shrink_depinfo(
OP_REF_LIST    *depinfo)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_DEPINFO);
#endif

	if (!depinfo) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure count and limit are consistent, then
	 * return.
	 */

	if (!depinfo->list) {
		ASSERT_DBG(!depinfo->count && !depinfo->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (depinfo->count == depinfo->limit) {
		return DDL_SUCCESS;
	}

	/*
	 *	If count = 0, free the list.
	 *	If count != 0, shrink the list
	 */

	if ( depinfo->count == 0 ) {
		ddl_free_depinfo( depinfo, FREE_ATTR );
	}
	else {
		depinfo->limit = depinfo->count;
		depinfo->list = (OP_REF *) realloc((void *) depinfo->list,
			(size_t) (depinfo->limit * sizeof *depinfo->list));
		if (!depinfo->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_depinfo
 *	ShortDesc: Free the dependency information.
 *
 *	Description:
 *		ddl_free_depinfo frees the dependency information (ie. the
 *		OP_REF_LIST structure).
 *
 *	Inputs:
 *		depinfo - ptr to the OP_REF_LIST structure to be freed.
 *		dest_flag -
 *          FREE_ATTR - free the entire list
 *          CLEAN_ATTR - cleanup (ie. destroy) any malloc'd memory associated
 *              with each element of the list.  The base list is left intact.
 *
 *	Outputs:
 *		depinfo - ptr to the OP_REF_LIST structure to be freed.
 *
 *	Returns: *		Void
 *
 *	Author: steve beyerl
 *********************************************************************/
void
ddl_free_depinfo(
OP_REF_LIST    *depinfo,
uchar           dest_flag)
{

	if (depinfo == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (depinfo->list == NULL) {

		ASSERT_DBG(!depinfo->count && !depinfo->limit);
		depinfo->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list, and set the values in depinfo.
		 */

		free((void *) depinfo->list);
		depinfo->list = NULL;
		depinfo->limit = 0;
	}

	depinfo->count = 0;
}

/*********************************************************************
 *
 *	Name: append_depinfo
 *	ShortDesc: Add a dependency item to the existing dependency list.
 *
 *	Description:
 *		append_depinfo adds the source depinfo list to the end of the
 *		destination depinfo list.
 *
 *	Inputs:
 *		destination - pointer to a depinfo list
 *		source - pointer to a depinfo list
 *
 *	Outputs:
 *		destination - pointer to the appended depinfo list
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *********************************************************************/

int
append_depinfo(
OP_REF_LIST    *destination,
OP_REF_LIST    *source)
{

	int				add;
	OP_REF         *temp;

	ASSERT_DBG(destination);

#ifdef ISPTEST
	TEST_FAIL(APPEND_DEPINFO);
#endif

	if (!source) {
		return DDL_SUCCESS;
	}

	if (source->count == 0) {
		return DDL_SUCCESS;
	}

	add = (int)source->count - ((int)destination->limit - (int)destination->count);

	if (add > 0) {
		temp = (OP_REF *) realloc((void *) destination->list,
			(size_t) ((destination->limit + add) * sizeof(OP_REF)));

		if (!temp) {

			/*
			 * destination->limit and count have not been modified
			 */

			ddl_free_depinfo(destination, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}
#ifndef SUN
#pragma warning (disable : 4244)
#endif
		destination->limit += add;
		destination->list = temp;
	}

	memcpy((char *) &destination->list[destination->count],
		(char *) source->list, (size_t) (source->count * sizeof(OP_REF)));

	destination->count += source->count;
#ifndef SUN
#pragma warning (default : 4244)
#endif
	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_add_depinfo
 *	ShortDesc: Adds the item to the dependency list.
 *
 *	Description:
 *		ddl_add_depinfo will add the item id and value type to the
 *		dependency list, if it is not already present.  If it is
 *		already in the list, only the value type is recorded.
 *
 *	Inputs:
 *		oper_ref - contains the operational reference (ie. dependency
 *				information) to be added to the oper_ref_list.
 *		depinfo - list of operational references (ie. list of
 *				dependency items) for the current item.
 *
 *	Outputs:
 *		depinfo - list of operational references (ie. list of
 *				dependency items) for the current item.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is corrupted
 *		DDL_MEMORY_ERROR - Out of memory
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

#define OP_REF_LIST_INCR	6	/* Increment size for OP_REF_LIST array */

int
ddl_add_depinfo(
OP_REF         *oper_ref,
OP_REF_LIST    *depinfo)
{
	OP_REF         *dep;	/* temp pointer */
	OP_REF         *end;	/* points to the end of the array */

	/*
	 * If the dependency item is already in the list (ie. the item_id and
	 * the subindex of the "oper_ref" match an element of the list, return
	 * success.
	 */

	for (dep = depinfo->list, end = dep + depinfo->count; dep < end; dep++)
		if ((dep->op_info.id == oper_ref->op_info.id) &&
			(dep->op_info.member == oper_ref->op_info.member)) {
			return DDL_SUCCESS;
		}

	/*
	 * Extend the list if necessary.
	 */

	if (depinfo->count >= depinfo->limit) {
		depinfo->limit += OP_REF_LIST_INCR;
		dep = (OP_REF *) realloc((void *) depinfo->list,
			(size_t) (depinfo->limit * sizeof(*dep)));
		if (!dep) {
			depinfo->limit = depinfo->count;
			ddl_free_depinfo(depinfo, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}

		depinfo->list = dep;
	}

	/*
	 * Add the new dependency to the end, and increment size.
	 */

	dep = depinfo->list + depinfo->count++;

	dep->op_ref_type = STANDARD_TYPE;

	dep->op_info.id = oper_ref->op_info.id;
	dep->op_info.member = oper_ref->op_info.member;
	dep->op_info.type = oper_ref->op_info.type;

	dep->op_info_list.count = 0;
	dep->op_info_list.list = nullptr;

	return DDL_SUCCESS;
}


