#include "stdafx.h"
#include "INTER_VARIANT.h"
#include "INTER_SAFEARRAY.h"
#include "BuiltIn.h"

bool CBuiltIn::GetLongArray
				(
					INTER_VARIANT &varValue
					, long *plongArray
					, int  &iArraySize
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY)
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);

		if (idims == 1)
		{
			INTER_VARIANT varValue;
			for (int i = 0,index = 0;index < sa->GetNumberOfElements();i+=sa->GetElementSize(),index++)
			{
				sa->GetElement(i, &varValue);
				if (varValue.GetVarType() != RUL_INT)
				{
					iArraySize = 0;
					return false;
				}				
				plongArray[index] = (int)varValue;
			}
			iArraySize = sa->GetNumberOfElements();
			return true;
		}
		else
		{
			iArraySize = 0;
			return false;
		}
	}
	else
	{
		iArraySize = 0;
		return false;
	}
}

bool CBuiltIn::GetULongArray
				(
					INTER_VARIANT &varValue
					, unsigned long *pulongArray
					, int  &iArraySize
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY)
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);

		if (idims == 1)
		{
			INTER_VARIANT varValue;
			for (int i = 0,index = 0;index < sa->GetNumberOfElements();i+=sa->GetElementSize(),index++)
			{
				sa->GetElement(i, &varValue);
				if ( (varValue.GetVarType() != RUL_UINT)
					 && (varValue.GetVarType() != RUL_INT) )	//tolerance int type because HART is still using int type for item ID
				{
					iArraySize = 0;
					return false;
				}				
				pulongArray[index] = (unsigned int)varValue;
			}
			iArraySize = sa->GetNumberOfElements();
			return true;
		}
		else
		{
			iArraySize = 0;
			return false;
		}
	}
	else
	{
		iArraySize = 0;
		return false;
	}
}

bool CBuiltIn::GetCharArray
				(
					INTER_VARIANT &varValue
					, char *pchArray
					, int  &iArraySize /* stevev 30may07: passed in MAX_LEN, returned actual Len*/
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY && iArraySize > 0 )
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);

		if (idims == 1)
		{
			int saLen = sa->GetNumberOfElements();
			if ( saLen > iArraySize - 1 )
			{
				saLen = iArraySize - 1;
			}
			INTER_VARIANT varValue;
			for (int i = 0,index = 0; index < saLen;  i+=sa->GetElementSize(),index++)
			{
				sa->GetElement(i, &varValue);
				if (varValue.GetVarType() == RUL_CHAR)
				{
					varValue.GetValue((void *)&pchArray[index],RUL_CHAR);
				}
				else if (varValue.GetVarType() == RUL_UNSIGNED_CHAR)
				{
					varValue.GetValue((void *)&pchArray[index],RUL_UNSIGNED_CHAR);
				}
				else
				{
					iArraySize = 0;
					return false;
				}				
			}

			iArraySize = saLen;		//updating number of valid chars
			pchArray[iArraySize] = 0;
			return true;
		}		
	}
	iArraySize = 0;
	return false;	
}

bool CBuiltIn::SetCharArray
				(
					INTER_VARIANT &varValue
					, char *pchArray
					, int  aSize  /* aSize added to deal with more_data_info[] where zero is not a terminator*/
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY)
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);
		int saLen = sa->GetNumberOfElements();
		int chLen = 0;
		if (aSize)
			chLen = min(aSize,MAX_DD_STRING);
		else
		    chLen = (int)strlen(pchArray) +1;// WS:EPM 04jun07 - take null terminator into account
										// handles bug where second string has remnants of the first in it.
		if (idims == 1)
		{
			saLen = min (saLen,chLen);
			INTER_VARIANT variantTemp;
			int iArrayIndex = 0;
			for (int i = 0,index = 0;index < saLen;/*sa->GetNumberOfElements();*/i+=sa->GetElementSize(),index++)
			{
				variantTemp = (char) pchArray[iArrayIndex];
				sa->SetElement(i, &variantTemp);
				iArrayIndex++;
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool CBuiltIn::GetWCharArray
				(
					INTER_VARIANT &varValue
					, tchar *pchArray
					, int  &iArraySize /* stevev 30may07: passed in MAX_LEN, returned actual Len*/
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY && iArraySize > 0 )
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);

		if (idims == 1)
		{
			int saLen = sa->GetNumberOfElements();
			if ( saLen > iArraySize )
			{
				saLen = iArraySize;
			}
			INTER_VARIANT varValue;
			uchar  U = 0;
			char   C = 0;
			for (int i = 0,index = 0; index < saLen;  i+=sa->GetElementSize(),index++)
			{
				sa->GetElement(i, &varValue);
				if (varValue.GetVarType() == RUL_CHAR)
				{
					varValue.GetValue(&C,RUL_CHAR);
					pchArray[index]  = C; // narrow to wide conversion
				}
				else 
				if (varValue.GetVarType() == RUL_SHORT)
				{
					varValue.GetValue((void *)&pchArray[index],RUL_SHORT);
				}
				else 
				if (varValue.GetVarType() == RUL_USHORT)
				{
					varValue.GetValue((void *)&pchArray[index],RUL_USHORT);
				}
				else 
				if (varValue.GetVarType() == RUL_UNSIGNED_CHAR)
				{
					varValue.GetValue(&U,RUL_UNSIGNED_CHAR);
					pchArray[index] = U; // narrow to wide conversion
				}
				else
				{
					iArraySize = 0;
					return false;
				}				
			}

			iArraySize = saLen;
			return true;
		}// else silent error???		
	}// else silent error???
	iArraySize = 0;
	return false;	
}

bool CBuiltIn::SetWCharArray
				(
					INTER_VARIANT &varValue
					, tchar *pchArray
					, int  aSize  /* aSize added to deal with more_data_info[] where zero is not a terminator*/
				)
{
	INTER_SAFEARRAY *sa=NULL;
	if (varValue.GetVarType() == RUL_SAFEARRAY)
	{
		sa = varValue.GetSafeArray();

		int idims = sa->GetDims(NULL);
		int saLen = sa->GetNumberOfElements();
		int chLen = 0;
		if (aSize)
			chLen = min(aSize,MAX_DD_STRING);
		else
		    chLen = (int)_tstrlen(pchArray) +1;// WS:EPM 04jun07 - take null terminator into account
									// handles bug where second string has remnants of the first in it.
		if (idims == 1)
		{
			saLen = min (saLen,chLen);
			INTER_VARIANT variantTemp;
			int iArrayIndex = 0;
			for (int i = 0,index = 0;index < saLen; i+=sa->GetElementSize(),index++)
			{
				variantTemp = pchArray[iArrayIndex];
				sa->SetElement(i, &variantTemp);
				iArrayIndex++;
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
