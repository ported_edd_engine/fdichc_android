#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BasicTestMenuBitmask
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID expectedItemId;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);
                ITEM_ID menuItemItemId;
                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
                int errCode = engine->Instance->GetItemIdFromSymbolName(L"test_bitEnum", &menuItemItemId);
                Assert::AreEqual(0, errCode,"EDDEngine failed to get item id of 'test_bitEnum'");
                expectedItemId = menuItemItemId;
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void EDDEngine_should_return_a_FLAT_MENU()
            {
                Assert::IsFalse(nullptr == test_menu);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_menu_should_have_three_items()
            {
                Assert::AreEqual((UInt16)3, test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_first_item_is_an_unbitmasked_ITYPE_VARIABLE()
            {
                Assert::AreEqual((unsigned long) 0, test_menu->items.list[0].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_VARIABLE, (int)(test_menu->items.list[0].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_second_item_is_an_ITYPE_VARIABLE_with_bitmask_of_03()
            {
                Assert::AreEqual(0x03u, test_menu->items.list[1].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_ENUM_BIT, (int)(test_menu->items.list[1].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_third_item_is_an_ITYPE_VARIABLE_with_bitmask_FC()
            {
                Assert::AreEqual(0xFCu, test_menu->items.list[2].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_ENUM_BIT, (int)(test_menu->items.list[2].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void All_three_items_reference_test_bitEnum()
            {
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[0].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[1].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[2].ref.desc_id));
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_a_bitmasked_item_in_FM8 : BasicTestMenuBitmask
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0022.fm8");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_a_bitmasked_item_in_FMA : BasicTestMenuBitmask
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0022.fmA");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestMenuText
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
           
            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_17_elements_in_item_list()
            {
                Assert::AreEqual(17u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_text_in_the_first_element_in_the_item_list()
            {
                Assert::AreEqual("Next is a rowbreak.",gcnew String(  test_menu->items.list[0].ref.expr.val.s.c_str()));
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_text_in_the_last_element_in_the_item_list()
            {               
                Assert::AreEqual("This is the end.",gcnew String(  test_menu->items.list[16].ref.expr.val.s.c_str()));
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_first_element_in_the_item_list()
            {               
                Assert::AreEqual((int)ITYPE_STRING_LITERAL,(int)test_menu->items.list[0].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_second_element_in_the_item_list()
            {               
                Assert::AreEqual((int)ITYPE_ROWBREAK,(int)test_menu->items.list[1].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_sixth_element_in_the_item_list()
            {               
                Assert::AreEqual((int)ITYPE_COLUMNBREAK,(int)test_menu->items.list[5].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_twelth_element_in_the_item_list()
            {               
                Assert::AreEqual((int)ITYPE_COLUMNBREAK,(int)test_menu->items.list[11].ref.op_info.type);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestMenuWithListElement
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID expectedItemIdType, expectedItemIdList;
            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);


                FDI_PARAM_SPECIFIER test_type_specifier, test_list_specifier;
                engine->FillParamSpecifier("test_var_2", &test_type_specifier);
                expectedItemIdType = test_type_specifier.id;

                engine->FillParamSpecifier("list_of_test_var_2", &test_list_specifier);
                expectedItemIdList = test_list_specifier.id;
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_15_elements_in_item_list()
            {
                Assert::AreEqual(15u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_item_id_for_the_ninth_menu_item()
            {
                 Assert::AreEqual(expectedItemIdType, test_menu->items.list[8].ref.desc_id); // the final element is the test_var_2
                 Assert::AreEqual(expectedItemIdList, test_menu->items.list[8].ref.op_info.id); // the first ref in trail is the list_of_test_var_2
            }

            [TestMethod, TestCategory("MenuTest")]
            void It_should_have_the_expected_type_in_ninth_menu_item()
            {
                Assert::AreEqual((int)ITYPE_VARIABLE, (int)test_menu->items.list[8].ref.desc_type);// the final element is a variable
                Assert::AreEqual((int)ITYPE_LIST, (int)test_menu->items.list[8].ref.op_info.type);// the first access step is a LIST reference
            }

           
        };

        [TestClass]
        public ref class When_reading_a_menu_with_static_text_for_FM8 : BaseTestMenuText
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000C.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_static_text_for_FMA : BaseTestMenuText
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase000C.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_List_element_for_FM8 : BaseTestMenuWithListElement
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0031.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_List_element_for_FMA : BaseTestMenuWithListElement
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0031.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
