

// Manufacturer 0xFD = "Special"
MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0016, DEVICE_REVISION 9, DD_REVISION 12


/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    STYLE MENU;
    ITEMS
    {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */

MENU main_test_menu
{
    LABEL "Main test menu";
    ITEMS
    {
        test_variable_uint,
        test_variable_string
    }
}

VARIABLE test_variable_uint
{
    CLASS DEVICE;
    TYPE UNSIGNED_INTEGER(4)
	{
	    MIN_VALUE 1;
		MAX_VALUE 100;
	}
	DEFAULT_VALUE 42;
}

VARIABLE test_variable_string
{
    CLASS DEVICE;
    TYPE ASCII(10);
}


/**********************************************************
 *  COMMANDS for transactions                             *
 **********************************************************/
 
 RESPONSE_CODES global_response
{
	0, SUCCESS,      "";
	16, MISC_ERROR,  "device not ready";
	25, MISC_WARNING, "something's weird";
}
	
	
COMMAND read_test_var_uint
{
    SLOT 22;
    INDEX 23;
    OPERATION READ;
    TRANSACTION
    {
        REQUEST
        {
        }
        REPLY
        {
            test_variable_uint
        }
    }
	RESPONSE_CODES
	{
	    0, SUCCESS,      "";
		17, MISC_ERROR,  "device not ready";
		22, MISC_WARNING, "something's weird";
	}
}

COMMAND write_test_var_uint
{
    SLOT 40;
    INDEX 41;
    OPERATION WRITE;
    TRANSACTION
    {
        REQUEST
        {
            test_variable_uint
        }
        REPLY
        {
        }
    }
	RESPONSE_CODES
	{
	    0, SUCCESS,      "";
		17, MISC_ERROR,  "device not ready";
		22, MISC_WARNING, "something's weird";
	}
}

COMMAND read_test_var_string
{
    SLOT 24;
    INDEX 25;
    OPERATION READ;
    TRANSACTION
    {
        REQUEST
        {
        }
        REPLY
        {
            test_variable_string
        }
		RESPONSE_CODES global_response;
    }
}

COMMAND Write_test_var_string
{
    SLOT 42;
    INDEX 43;
    OPERATION WRITE;
    TRANSACTION
    {
        REQUEST
        {
            test_variable_string
        }
        REPLY
        {
        }
		RESPONSE_CODES global_response;
    }
}


