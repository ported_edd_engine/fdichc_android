#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"

using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            public ref class MethodHelper
            {
            public:
                static nsEDDEngine::FLAT_METHOD* GetMethod(EddEngineController^ engine, System::String^ methodName);                
            };
        }
    }
}
