////////////////////////////////////////////////////////////////////////
////	Device Type Load/Unload Functions.
////
////	@(#) $Id: dev_type.h,v 1.1 1995/09/12 17:35:41 kenta Exp $
////////////////////////////////////////////////////////////////////////

#ifndef	DEV_TYPE_H
#define	DEV_TYPE_H


#include "cm_lib.h"

extern int load_sym_info(ENV_INFO *, DEVICE_TYPE_HANDLE) ;
extern int remove_sym_info P((ENV_INFO *, DEVICE_TYPE_HANDLE)) ;

#endif
