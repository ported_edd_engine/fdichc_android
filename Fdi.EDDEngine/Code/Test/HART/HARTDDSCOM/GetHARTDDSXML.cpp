// GetHARTDDSXML.cpp : Implementation of CGetHARTDDSXML

#include "stdafx.h"

#include "GetHARTDDSXML.h" 
#include "BuildDDSXML.h"

using namespace nsEDDEngine;
// CGetHARTDDSXML

CGetHARTDDSXML::CGetHARTDDSXML()
{
	//_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(68402);
	//_CrtDumpMemoryLeaks();

	m_bLoadXML = true;//Always load the XML unless someone calls a function to shut it off.
}
CGetHARTDDSXML::~CGetHARTDDSXML()
{
	nsEDDEngine::IEDDEngine* deletedEDDEngine;
	while (m_EDDEngineArray.GetHeadPosition()!= NULL)
	{
		deletedEDDEngine = m_EDDEngineArray.RemoveHead();
		delete deletedEDDEngine;
	}

}

STDMETHODIMP CGetHARTDDSXML::GetDDSInfo(BSTR sFileName, BSTR* sXMLOutput)
{
	USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

	CString szFilename = OLE2T(sFileName);
	xmlBuilder.SetLoadXML(m_bLoadXML);
	xmlBuilder.CreateXMLFrom( szFilename , sXMLOutput );
	m_iDeviceInstanceMemoryUsage = xmlBuilder.GetDeviceInstanceMemoryUsage();

	return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetErrorString(BSTR bstrFilename, int ErrorCode, BSTR* sErrorString)
{
    // Eddengine
    // Iconvenience
    // call geterrorstring
    //return string
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetErrorString( bstrFilename, ErrorCode, sErrorString );
    return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetItemType(BSTR bstrFilename, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItemType)
{
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetItemType( bstrFilename, iBlockInstance, eType, id, subindex, pItemType );
    return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetItemTypeAndItemId(BSTR bstrFilename, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItemType, unsigned long *pItemId)
{
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetItemTypeAndItemId( bstrFilename, iBlockInstance, eType, id, subindex, pItemType, pItemId );
    return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetCmdIdFromNumber(BSTR bstrFilename, int iBlockInstance, unsigned long ulCmdNumber, unsigned long * pCmdItemId)
{
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetCmdIdFromNumber(bstrFilename, iBlockInstance, ulCmdNumber, pCmdItemId);

    return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetAxisUnitRelItemId(BSTR bstrFilename, int iBlockInstance, unsigned long AxisItemId, unsigned long* pUnitItemId)
{
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetAxisUnitRelItemId(bstrFilename, iBlockInstance, AxisItemId, pUnitItemId);
    return S_OK;
}

STDMETHODIMP CGetHARTDDSXML::GetParamUnitRelItemId(BSTR bstrFilename, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, unsigned long* pUnitItemId)
{
    USES_CONVERSION;

	CDDSXMLBuilder xmlBuilder;

    CString szFilename = OLE2T(bstrFilename);

    xmlBuilder.GetParamUnitRelItemId( bstrFilename, iBlockInstance, eType, id, subindex, pUnitItemId );
    return S_OK;
}

//
///* Functions to automated testing 
//* These functions will generate text files which can be used to compare and verify the output 
//*/
//
//STDMETHODIMP CGetHARTDDSXML::GetItemType_Auto(BSTR bstrFilename, int *pItemType)
//{
//    USES_CONVERSION;
//
//    if( m_builder )
//    {
//        delete m_builder;
//        m_builder = NULL;
//    }
//    m_builder=new CDDSXMLBuilder();
//
//    CString szFilename = OLE2T(bstrFilename);
//
//    m_builder->GetItemType( bstrFilename, pItemType );
//    return S_OK;
//}
//
//STDMETHODIMP CGetHARTDDSXML::GetErrorString_Auto(BSTR bstrFilename, BSTR* sErrorString)
//{
//    USES_CONVERSION;
//
//    if( m_builder )
//    {
//        delete m_builder;
//        m_builder = NULL;
//    }
//    m_builder=new CDDSXMLBuilder();
//
//    CString szFilename = OLE2T(bstrFilename);
//
//    m_builder->GetErrorString( bstrFilename, sErrorString );
//    return S_OK;
//}




STDMETHODIMP CGetHARTDDSXML::OnlyCreateEDDEngine(BSTR bstrFileName, LONG * lMemoryFootprint)
{
	wchar_t						configXML[ CONFIG_XML_MAX ]; // arbitrary max string length
	
	nsEDDEngine::IEDDEngine*	pEDDEngine;

	_CrtMemState s1, s2, s3;
	(void)s1;
	(void)s2;
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	USES_CONVERSION;	
    if (!CDDSXMLBuilder::ReadConfigXML(configXML, CONFIG_XML_MAX))
    {
        ASSERT(0);
    }

	_CrtMemCheckpoint( &s1 );
	wchar_t *wstr_name = T2W(bstrFileName);

	nsEDDEngine::EDDEngineFactoryError eError = nsEDDEngine::EDDE_SUCCESS;

	pEDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, NULL, NULL, NULL, configXML, &eError);
	
	m_EDDEngineArray.AddTail(pEDDEngine);
	
	_CrtMemCheckpoint(&s2);
	_CrtMemDifference( &s3, &s1, &s2 );
	
	*lMemoryFootprint = s3.lSizes[_NORMAL_BLOCK];
	return S_OK;
}


STDMETHODIMP CGetHARTDDSXML::DeleteOneEDDEngineElement(VARIANT_BOOL* bDone, LONG* lMemoryFootprint)
{
	// TODO: Add your implementation code here
	*bDone = false;

	_CrtMemState s1, s2, s3;
	(void)s1;
	(void)s2;
	nsEDDEngine::IEDDEngine* deletedEDDEngine;

	_CrtMemCheckpoint( &s1 );

	if (m_EDDEngineArray.GetHeadPosition()!= NULL)
	{
		deletedEDDEngine = m_EDDEngineArray.RemoveHead();
		delete deletedEDDEngine;
	}
	else
	{
		*bDone = true;
	}

	_CrtMemCheckpoint(&s2);
	_CrtMemDifference( &s3, &s2, &s1);
	*lMemoryFootprint = s3.lSizes[_NORMAL_BLOCK];

	return S_OK;
}


STDMETHODIMP CGetHARTDDSXML::SetLoadXML(VARIANT_BOOL bLoadXML)
{
	m_bLoadXML = (bLoadXML !=0);
	return S_OK;
}


STDMETHODIMP CGetHARTDDSXML::GetDeviceInstanceMemoryUsage(LONG* lMemoryUsage)
{
	*lMemoryUsage = m_iDeviceInstanceMemoryUsage;
	return S_OK;
}
