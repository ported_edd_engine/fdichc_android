#ifndef NTCASSERT_H
#define NTCASSERT_H

void * _ams_assert(const wchar_t* , const wchar_t* , unsigned);

#ifdef assert
#undef assert
#endif
#ifdef _WIN32
#define assert(exp) (static_cast<void>( (exp) ? 0 : (DoDebugBreak(), _ams_assert(_T(#exp), _T(__FILE__), __LINE__)) ))
#else
//TODO fixit
#define EXPAND(x) _T(x)
#define WFILE EXPAND(__FILE__)
#define assert(exp) (static_cast<void>( (exp) ? 0 : (DoDebugBreak(), _ams_assert(_T(#exp), WFILE, __LINE__)) ))
#endif
#endif
