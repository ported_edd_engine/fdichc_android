
#pragma once


#include <map>

#include "nsConsumer/IParamCache.h"
#include "nsEDDEngine/IConvenience.h"
#include "nsEDDEngine/IEDDEngine.h"


	class ParamCache : public nsConsumer::IParamCache
	{
	public:
		ParamCache();
		~ParamCache();

		virtual nsConsumer::PC_ErrorCode GetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ nsConsumer::EVAL_VAR_VALUE * pParamValue);

		virtual nsConsumer::PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ nsConsumer::EVAL_VAR_VALUE * pParamValue);

	void SetEddEngine(nsEDDEngine::IEDDEngine* engine);

	std::string ParamSpecifierToString(nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier);

	private:
		nsEDDEngine::IEDDEngine* eddEngine;
		std::map<std::string,nsConsumer::EVAL_VAR_VALUE> cache;
		nsEDDEngine::FLAT_DEVICE_DIR* myDir;
		ITEM_ID rec_helper;
};
