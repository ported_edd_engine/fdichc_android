/*
 * CHARTBreaker is used to dump EDD binary into files.
 * The intended use is for testing the PROFIBUS Tokenizer
 *   to see if it is dumping out the same binary encoded attributes.
 */

#pragma once

#ifdef _HARTBREAKER_

#include "dds_upcl.h"


class CHARTBreaker
{
public:
	CHARTBreaker(void);
	~CHARTBreaker(void);

				// Used to capture the directory where this EDD is found
	static void SetEDDDirectory(DEVICE_HANDLE dh, char *filename) ;

				// Called to dump an attribute
	static void DumpBinary(	BLOCK_HANDLE bh, unsigned long ItemId,
							char *sItemType, char *sAttrName,
							unsigned char* pChunk, unsigned long iSize);
};


#endif	// _HARTBREAKER_
