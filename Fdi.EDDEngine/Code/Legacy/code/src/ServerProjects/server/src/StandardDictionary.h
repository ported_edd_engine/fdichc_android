#pragma once


class StandardDictionaryEntry
{
public:
	StandardDictionaryEntry(unsigned long section, 
							unsigned long offset, 
							unsigned long value,
							LPCTSTR text);
	virtual ~StandardDictionaryEntry(void);

	unsigned long GetSection();
	unsigned long GetOffset();
	unsigned long GetValue();
	LPCTSTR GetText();

private:
	unsigned long m_section;
	unsigned long m_offset;
	unsigned long m_value;
	CStdString m_text;
};

class StandardDictionary
{
public:
	StandardDictionary();
	virtual ~StandardDictionary(void);

	virtual int CreateDictionary(ENV_INFO* env_info, LPCTSTR dictionaryFile, StdDictTable *stdDictTable, DictionaryTable *dictTable);

	static int SortStandardDictionary(const void* p1, const void* p2);

protected:
	PI_CtypedptrArray<StandardDictionaryEntry*> m_dictionary;
	CStdString m_token;
	int m_nextchar;
	int m_dictionaryLineCount;
	int m_dictionaryErrorCount;
	wchar_t* m_dictionaryBufferU;
	size_t m_dictionaryBufferUSize;
	size_t m_dictionaryBufferULocation;

	virtual bool ReadDictionaryFile(LPCTSTR dictionaryFile);
	virtual void ParseDictionary();
//	virtual CStdString GetDictionaryText();
	virtual wchar_t GetNextCharacter();
	virtual int GetToken();
	virtual bool IsHighTokenText(CStdString& sDictionaryText);
	virtual int ReadDictionaryComment();
	virtual int ReadDictionaryUntilValidToken();
	virtual void UpdateCaller(StdDictTable *stdDictTable, DictionaryTable *dictTable);
	virtual void Cleanup();

	virtual void LogDictionaryError(const char *cFilename, UINT32 lineNumber, LPCTSTR strError);
	virtual void LogDictionaryErrorFromResource(const char *cFilename, UINT32 lineNumber, int errorID);
	virtual void LogDictionaryErrorFromResource(const char *cFilename, UINT32 lineNumber, int errorID, CStdString& errorText);
	
	static const int TOKEN_ERROR = 0;
	static const int TOKEN_DONE = 1;
	static const int TOKEN_NUMBER = 2;
	static const int TOKEN_NAME = 3;
	static const int TOKEN_TEXT = 4;
	static const int TOKEN_COMMA = 5;

	static const TCHAR *high_TOKEN_TEXT;

	inline void NullTerminate()
	{
		m_token.append(1, L'\0');
	}

	ENV_INFO* m_Env_Info;
};
