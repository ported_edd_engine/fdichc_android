#include "stdafx.h"
#include "Semaphore.h"


WindowsSemaphore::WindowsSemaphore()
{
	m_pSemaphore = NULL;
}

WindowsSemaphore::~WindowsSemaphore()
{
	
}

BOOL WindowsSemaphore::InitSemaphore(long nInitialCount, long nMaxCount, std::wstring semaphoreName)
{
	BOOL bRetVal = FALSE;

	m_pSemaphore = ::CreateSemaphore(NULL, nInitialCount, nMaxCount, semaphoreName.c_str());

	if (m_pSemaphore != NULL)
	{
		bRetVal = TRUE;
	}

	return bRetVal;
}

BOOL WindowsSemaphore::WaitSemaphore()
{
	BOOL bRetVal = FALSE;
	DWORD dResult = 0;	

	dResult = ::WaitForSingleObject(m_pSemaphore, INFINITE);

	if (dResult == WAIT_OBJECT_0)
	{
		bRetVal = TRUE;
	}
	
	return bRetVal;
}

BOOL WindowsSemaphore::ReleaseSemaphore(long lCurrentCount, long* plPrevCount)
{
	return (::ReleaseSemaphore(m_pSemaphore, lCurrentCount, plPrevCount));
}

BOOL WindowsSemaphore::CloseSemaphore()
{
	if (m_pSemaphore != NULL)
	{
		CloseHandle(m_pSemaphore);
		m_pSemaphore = NULL;
	}

	return TRUE;
}