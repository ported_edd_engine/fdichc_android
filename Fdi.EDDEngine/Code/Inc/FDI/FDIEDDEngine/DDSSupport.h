

#include "nsConsumer/IParamCache.h"
#include "ConnectionMgr.h"
#include <nsConsumer/IBuiltinSupport.h>
#include "nsConsumer/IEDDEngineLogger.h"

class IDDSSupport
{
public:
	virtual int DDS_GetParamValue( BLOCK_HANDLE bh, void* pValueSpec, nsEDDEngine::OP_REF *op_ref, nsConsumer::EVAL_VAR_VALUE *param_value ) = 0;
	virtual CConnectionMgr* GetConnectionManager() = 0;
	virtual int GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle ) = 0;


    // Wrapper function for accesssing IParamCache function of the same name
	virtual nsConsumer::PC_ErrorCode DDS_GetDynamicAttribute(BLOCK_HANDLE bh, void* pValueSpec, DESC_REF* desc_ref,
        const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue) = 0;
	
	virtual void DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory) = 0;
};
