namespace MethodInterpreterApp.Classes
{
    public class DDSWorkItem : WorkItem
    {
        private string _filename;
        private string _sFFBlockName;

        public string Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public string sFFBlockName
        {
            get { return _sFFBlockName; }
            set { _sFFBlockName = value; }
        } 

        private int _btnType;
        public int btnType
        {
            get { return _btnType; }
            set { _btnType = value; }
        }
    }
}
