// EDD Engine.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "std_asserts.h"

#include "DDSChooser.h"
#include "nsEDDEngine/EDDEngineFactory.h"
#include "MiDataFinder.h"
#include "MethodInterfaceDefs.h"
#include "Ole/EDDLMethInterp/AsyncResult.h"
#include "Ole/EDDLMethInterp/MiEngine.h"
#include <assert.h>



//#define __TEST_MIDATAFINDER__

	class EDDEngine : public IEDDEngine
	{
	private:
		CStdString  m_szFilename;
		CEDDEngineImpl* m_EDDEngineImpl;
		CMiDataFinder m_MiDataFinder;
		ProtocolType m_eProtocol;
		nsConsumer::IBuiltinSupport *m_pIBuiltinSupport;

		/*bool MyWaitWithMessageLoop(HANDLE hEvent)
		{
			DWORD dwRet;
			MSG msg;

			while(1)
			{
				dwRet = MsgWaitForMultipleObjectsEx(1, &hEvent, INFINITE, QS_ALLINPUT, MWMO_INPUTAVAILABLE);

				if (dwRet == WAIT_OBJECT_0)
					return TRUE;    // The event was signaled

				if (dwRet != WAIT_OBJECT_0 + 1)
					break;          // Something else happened

				// There is one or more window message available. Dispatch them
				while(PeekMessage(&msg,0,0,0,PM_NOREMOVE))
				{
					BOOL bRet;

					bRet = ::GetMessage(&msg, NULL, 0, 0);

					if (bRet > 0)
					{
						::TranslateMessage(&msg);
						::DispatchMessage(&msg);
					}

					if (WaitForSingleObject(hEvent, 0) == WAIT_OBJECT_0)
						return TRUE; // Event is now signaled.
				}
			}
			return FALSE;
		}*/

	public:
		//public variables
		


#ifdef __TEST_MIDATAFINDER__
		bool m_bMiFlag;
#endif	// __TEST_MIDATAFINDER__
	public:	

		// EDDEngine Constructor
		EDDEngine(const wchar_t *sEDDBinaryFilename,  
			nsConsumer::IParamCache *pIParamCache, nsConsumer::IBuiltinSupport *pIBuiltinSupport, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, const wchar_t* pConfigXML, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
		{
			m_EDDEngineImpl = CDDSChooser::ChooseDDSLib( sEDDBinaryFilename, 
				pConfigXML, pIEDDEngineLogger, pErrorCode);

			if (m_EDDEngineImpl == nullptr)		// An error happened earlier, so just exit
			{
				//assert(*piErrorCode != 0);
				return;
			}

			m_MiDataFinder.SetEDDEngineImpl(m_EDDEngineImpl);
			m_EDDEngineImpl->SetParamCache( pIParamCache );
			m_pIBuiltinSupport = pIBuiltinSupport;
			m_eProtocol = m_EDDEngineImpl->GetProtocol();

#ifdef __TEST_MIDATAFINDER__		
			m_bMiFlag = false;
#endif	// __TEST_MIDATAFINDER__
		}

		~EDDEngine()
		{
			delete m_EDDEngineImpl;
		}

		void GetEDDFileHeader(EDDFileHeader* pEDDFileHeader)
		{
			// Verify all required pointers are non-null
			ASSERT_DBG(pEDDFileHeader);

			m_EDDEngineImpl->GetEDDFileHeader(pEDDFileHeader);
		}


		int AddFFBlocks(const nsEDDEngine::CFieldbusBlockInfo *pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount)
		{ 
			// Verify all required pointers are non-null
			ASSERT_RET(pFieldbusBlockInfo, DDI_INVALID_PARAM);
			ASSERT_RET(arrBlockInstance, DDI_INVALID_PARAM);

			return m_EDDEngineImpl->AddFFBlocks( pFieldbusBlockInfo, arrBlockInstance, iCount ); 
		}


		// ITableInfo functions
		int GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pDeviceDir, DDI_INVALID_PARAM);

			return m_EDDEngineImpl->GetDeviceDir(pDeviceDir);
		}

		int GetCriticalParams(int iBlockInstance, nsEDDEngine::CRIT_PARAM_TBL **pCriticalParamTbl)
		{
			return m_EDDEngineImpl->GetCriticalParams(iBlockInstance, pCriticalParamTbl);
		}

		int GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, ITEM_ID *pWAO_Item, ITEM_ID* pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList = NULL)
		{
			return m_EDDEngineImpl->GetRelationsAndUpdateInfo(iBlockInstance, pValueSpec, pParamSpec, pWAO_Item, pUNIT_Item, pUpdateList, pDominantList);
		}
		//IItemInfo functions

		int GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec, nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, FDI_GENERIC_ITEM * pGenericItem )
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pItemSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pAttributeNameSet, DDI_INVALID_PARAM);
			ASSERT_RET(lang_code, DDI_INVALID_PARAM);
			ASSERT_RET(pGenericItem, DDI_INVALID_PARAM);

#ifdef __TEST_MIDATAFINDER__
			if (m_bMiFlag == false)
			{
				m_bMiFlag = true;
				int iReturnCode = -1;
				CValueVarient *pvtValue = new CValueVarient(); 
				CV_VT(pvtValue, ValueTagType::CV_I4);
				CV_I4(pvtValue) = 0;

				// call to GetDictionaryString
				wchar_t dictString[128] = {0};
				iReturnCode = m_MiDataFinder.GetDictionaryString(17367042, dictString, 75, lang_code); 

				// call to GetDevSpecString
				wchar_t devSpecString[128] = {0};
				iReturnCode = m_MiDataFinder.GetDevSpecString(155, devSpecString, 3, lang_code); 

				// call to ResolveNameToId
				wchar_t itemName[128] = {0};

				iReturnCode = GetSymbolNameFromItemId(pItemSpec->item.id, itemName, sizeof(itemName)/sizeof(itemName[0]));

				assert(iReturnCode == 0);

				ITEM_ID item_id;
				iReturnCode = m_MiDataFinder.ResolveNameToID( itemName, &item_id); 

				assert(item_id == pItemSpec->item.id);

				if(pGenericItem->item_type == ITYPE_ITEM_ARRAY)
				{
					// Get kind property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 27, pvtValue);
					assert(iReturnCode == 0);

					// call to ResolveRefToID
					m_MiDataFinder.ResolveRefToID(iBlockInstance, pItemSpec->item.id, 2, lang_code);

					// call to GetArrayElementLabel
					iReturnCode = m_MiDataFinder.GetArrayElementLabel(iBlockInstance, pItemSpec->item.id, 2, pvtValue);
					assert(iReturnCode == 0);
				}

				// call to GetResponseCodeString		
				if(pGenericItem->item_type == ITYPE_COMMAND)
				{
					// Get kind property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 27, pvtValue);
					assert(iReturnCode == 0);

					iReturnCode = m_MiDataFinder.GetResponseCodeString(iBlockInstance, 0, 0, pvtValue);
					assert(iReturnCode == 0);
				}

				// call to GetValue funtion 


				// Get name property
				iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 35, pvtValue);
				assert(iReturnCode == 0);

				// Get item id property
				iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 22, pvtValue);
				assert(iReturnCode == 0);

				if(pGenericItem->item_type == ITYPE_VARIABLE) // variable specific properties
				{
					// Get kind property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 27, pvtValue);
					assert(iReturnCode == 0);

					// Get Class property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 4, pvtValue);
					assert(iReturnCode == 0);

					// Get parametr type property
					if((pItemSpec->item.id != 150) && (pItemSpec->item.id != 152))
					{
						iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 50, pvtValue);
						assert(iReturnCode == 0);
					}

					// Get parameter handling property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 17, pvtValue);
					assert(iReturnCode == 0);

					// Get paramter label property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 29, pvtValue);
					assert(iReturnCode == 0);

					// Get scalling factor property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 44, pvtValue);
					assert(iReturnCode == 0);

					// Get size property
					if((pItemSpec->item.id != 150) && (pItemSpec->item.id != 152))
					{
						iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 45, pvtValue);
						assert(iReturnCode == 0);
					}

					// Get help property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 19, pvtValue);
					assert(iReturnCode == 0);

					// Get default value property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 129, pvtValue);
					assert(iReturnCode == 0);

					// Get enum as xml property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 105, pvtValue);
					assert(iReturnCode == 0);

					// Get edit format property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 12, pvtValue);
					assert(iReturnCode == 0);

					// Get FLAT_VAR
					FLAT_VAR* pFlat = nullptr;
					iReturnCode = m_MiDataFinder.GetVarInfo(iBlockInstance, pItemSpec->item.id, &pFlat);
					assert(iReturnCode == 0);
					delete pFlat;

				}

				if(pGenericItem->item_type == ITYPE_METHOD) // Method specific properties
				{
					// Get kind property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 27, pvtValue);
					assert(iReturnCode == 0);

					// Get method definition property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 7, pvtValue);
					assert(iReturnCode == 0);

					// Get parametr type property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 50, pvtValue);
					assert(iReturnCode == 0);

					// Get method parametr property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 57, pvtValue);
					assert(iReturnCode == 0);
				}

				if(pGenericItem->item_type == ITYPE_COLLECTION) // Collection specific properties
				{
					// Get kind property
					iReturnCode = m_MiDataFinder.GetValue(iBlockInstance, pItemSpec->item.id, 27, pvtValue);
					assert(iReturnCode == 0);
				}

				delete pvtValue;

				m_bMiFlag = false;
			}
#endif	// __TEST_MIDATAFINDER__

			return m_EDDEngineImpl->GetItem(iBlockInstance,pValueSpec,pItemSpec, pAttributeNameSet, lang_code, pGenericItem);
		}

		//
		// IConvenience Functions
		//

		// Unit Convenience functions

		// Gets the Unit Relation ItemId for the given Axis
		int GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID *pUnitItemId) 
        {
			// Verify all required pointers are non-null
			ASSERT_RET(pUnitItemId, DDI_INVALID_PARAM);

			int rc = m_EDDEngineImpl->GetAxisUnitRelItemId(iBlockInstance, pValueSpec, AxisItemId, pUnitItemId); 
			return rc;
        }

		// Gets the Unit Relation ItemId for the specified parameter
		int GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, FDI_PARAM_SPECIFIER *pParamSpec, ITEM_ID *pUnitItemId)
        {
			// Verify all required pointers are non-null
			ASSERT_RET(pParamSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pUnitItemId, DDI_INVALID_PARAM);

			*pUnitItemId = 0;

            int rc = m_EDDEngineImpl->GetParamUnitRelItemId(iBlockInstance, pValueSpec, pParamSpec, pUnitItemId);
            return rc;
        }

		// Command Convenience functions

		// Retrieves the List of Commands that can be used to read/write this parameter
		int GetCommandList(int iBlockInstance, FDI_PARAM_SPECIFIER * pParamSpec, CommandType eCmdType, FDI_COMMAND_LIST * pCommandList)
        {
			// Verify all required pointers are non-null
			ASSERT_RET(pParamSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pCommandList, DDI_INVALID_PARAM);
			int rc = DDI_NO_COMMANDS_FOR_PARAM;

			if(( m_eProtocol != FF ) && (m_eProtocol != ISA100))
			{
				rc = m_EDDEngineImpl->GetCommandList(iBlockInstance, pParamSpec, eCmdType, pCommandList);
			}
            return rc; 
        }


		// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
		int GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID * pCmdItemId) 
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pCmdItemId, DDI_INVALID_PARAM);

			 return m_EDDEngineImpl->GetCmdIdFromNumber(iBlockInstance, ulCmdNumber, pCmdItemId);
		}

		// String Convenience functions

		// Converts an EDD Engine Error code to a string
		wchar_t *GetErrorString(int iErrorNum)
		{
			return m_EDDEngineImpl->GetErrorString(iErrorNum);
		}

		// Given a multi-language string, returns only the language specified
		int GetStringTranslation(const wchar_t * string, const wchar_t * lang_code, wchar_t* outbuf, int outbuf_size)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(string, DDI_INVALID_PARAM);
			ASSERT_RET(lang_code, DDI_INVALID_PARAM);
			ASSERT_RET(outbuf, DDI_INVALID_PARAM);

			return m_EDDEngineImpl->GetStringTranslation(string,(wchar_t*)lang_code,outbuf, outbuf_size);
		}

		// Type Convenience functions

		// Returns the Item Type of the specified Item
		int GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE *pItemType)
		{ 
			// Verify all required pointers are non-null
			ASSERT_RET(pItemSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pItemType, DDI_INVALID_PARAM);
	
			return m_EDDEngineImpl->GetItemType(iBlockInstance, pItemSpec, pItemType);
		}

		// Returns the Item Type and Item Id of the specified Item
		int GetItemTypeAndItemId(int iBlockInstance, FDI_ITEM_SPECIFIER* pItemSpec, ITEM_TYPE * pItemType, ITEM_ID * pItemId)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pItemSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pItemType, DDI_INVALID_PARAM);
			ASSERT_RET(pItemId, DDI_INVALID_PARAM);

			return m_EDDEngineImpl->GetItemTypeAndItemId(iBlockInstance, pItemSpec, pItemType, pItemId);
		}

		// Returns the Variable Type of the specified parameter
		int GetParamType(int iBlockInstance, FDI_PARAM_SPECIFIER* pParamSpec, TYPE_SIZE *pTypeSize)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pParamSpec, DDI_INVALID_PARAM);
			ASSERT_RET(pTypeSize, DDI_INVALID_PARAM);

			int rc = m_EDDEngineImpl->GetParamType(iBlockInstance, pParamSpec, pTypeSize);
			return rc;
		}

		int GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(item_name, DDI_INVALID_PARAM);

			// Verify item_id is not 0
			if(item_id == 0)
			{
				return DDI_INVALID_PARAM;
			}

			return m_EDDEngineImpl->GetSymbolNameFromItemId(item_id, item_name, outbuf_size);
		};

		int GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* pItemId)
		{
			// Verify all required pointers are non-null
			ASSERT_RET(pItemId, DDI_INVALID_PARAM);
			ASSERT_RET(pItemName, DDI_INVALID_PARAM);
	
			return m_EDDEngineImpl->GetItemIdFromSymbolName(pItemName, pItemId);
		}

#ifdef _DEBUG
		int DisplayDebugInfo()
		{
			return m_EDDEngineImpl->DisplayDebugInfo();
		}
#endif
		//
		// IMethod Interface
		//

		IAsyncResult* BeginMethod( int iBlockInstance, void* pValueSpec, ACTION* pAction,
			IAsyncCallbackHandler *pCallback, void * asyncState, const wchar_t *lang_code)
		{ 
			return BeginVariableActionMethod( iBlockInstance, pValueSpec, nullptr, pAction, pCallback, asyncState, lang_code);
		}

		void CancelMethod( IAsyncResult* pIAsyncResult )
		{
			if (pIAsyncResult != nullptr)
			{								// Signal the Method Interpreter that it should abort
				((CAsyncResult*)pIAsyncResult)->CancelAsyncDelegate();
			}
		}

		// This routine waits for delegate execution done 
		// and returns true if there is no error or aborted execution.
		Mth_ErrorCode EndMethod( IAsyncResult** asyncResult, nsConsumer::EVAL_VAR_VALUE *pReturnedValue )
		{
			Mth_ErrorCode eRetVal = EndVariableActionMethod(asyncResult, pReturnedValue);

			return eRetVal;
		}

		Mth_ErrorCode EndVariableActionMethod( IAsyncResult** asyncResult, nsConsumer::EVAL_VAR_VALUE *pReturnedValue )
		{
			Mth_ErrorCode eRetVal = METH_FAILED;	//initialization
			BOOL bSuccess = true;
			if ((*asyncResult != NULL) && !(((CAsyncResult*)*asyncResult)->testNsetEndMethodCalled()))
			{
				// Retrieve the call wait handle
				if (!(*asyncResult)->get_IsCompleted())
				{
                  	if(((CAsyncResult*)*asyncResult)->m_asyncResultEvent->GetHandle() != NULL)
					{
						// Wait for the call to be completed
						bSuccess = ((CAsyncResult*)*asyncResult)->m_asyncResultEvent->Wait();// MyWaitWithMessageLoop(WaitEvent);
					}
				}

				if (bSuccess)
				{
					/* GetResult() returns the result from target method execution
					*/
					eRetVal = ((CAsyncResult*)*asyncResult)->GetResult(pReturnedValue);
				}

				PS_Sleep(1);
				//now it is time to delete the instance
				delete *asyncResult;
				*asyncResult = NULL;
			}

			return eRetVal;
		}

		// Runs a variable action method (called only by methods associated with a pre-edit, post-edit, pre-read, post-read, pre-write or post-write action)
		// Note: Some protocols (like HART and FF) limit the builtins that are used during pre/post read/write actions to
		// access only the Consumer's Parameter Cache. In this case the caller can block while waiting for the method 
		// to complete. This can be done by calling EndMethod() explicitly, waiting on the AsynWaitHandle,
		// or polling until the IsCompleted propery is true.
		//
		IAsyncResult* BeginVariableActionMethod( int iBlockInstance, void* pValueSpec, nsConsumer::EVAL_VAR_VALUE *pActionValue, ACTION* pAction,
			IAsyncCallbackHandler *pCallback, void * asyncState, const wchar_t *lang_code)
		{
			//create an instance of CMiEngine for the method execution
			CMiEngine * pLocalMiInstance;

			pLocalMiInstance = new CMiEngine(iBlockInstance, pValueSpec, pActionValue, pAction, m_pIBuiltinSupport, m_EDDEngineImpl->m_pIEDDEngineLogger, m_eProtocol, lang_code, &m_MiDataFinder);

			// create a delegate of the target method from target class
			CClassDelegate<CMiEngine> *pDelegate = new CClassDelegate<CMiEngine>(pLocalMiInstance, &CMiEngine::ExecuteMethod);
			// register the delegate with IAsyncResult pattern and pass the CMiEngine instance pointer to CClassDelegate class
			CAsyncResult *pLocalAsyncResult = new CAsyncResult((IClassDelegate *)pDelegate, pCallback, asyncState);

			// save IAsyncResult instance into CMiEngine instance
			pLocalMiInstance->pAsyncResult = (IAsyncResult *) pLocalAsyncResult;

			// execute the delegate == ExecuteMethod()
			if ((pLocalAsyncResult == NULL) || (pLocalAsyncResult->Execute() != PC_BUSY))
			{
				throw "BeginMethod function failed";
			}

			return pLocalMiInstance->pAsyncResult;
		}
		// end of IMethods implementation

	};

namespace nsEDDEngine
{

	IEDDEngine * EDDEngineFactory::CreateDeviceInstance(const wchar_t *sEDDBinaryFilename, 
		nsConsumer::IParamCache * pIParamCache, nsConsumer::IBuiltinSupport *pIBuiltinSupport, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, 
		const wchar_t* pConfigXML, EDDEngineFactoryError *pErrorCode)
	{
		*pErrorCode = nsEDDEngine::EDDE_SUCCESS;		// Initialize to a good error code

		IEDDEngine* pIEDDEngine = (IEDDEngine*) new EDDEngine(sEDDBinaryFilename, pIParamCache, pIBuiltinSupport, pIEDDEngineLogger, pConfigXML, pErrorCode);

		if (*pErrorCode != nsEDDEngine::EDDE_SUCCESS)
		{
			delete pIEDDEngine;
			pIEDDEngine = nullptr;
		}

		return pIEDDEngine;
	}

	wchar_t* EDDEngineFactory::GetEDDEngineErrorString(EDDEngineFactoryError eErrorCode)
	{
        static wchar_t str[100]{};// = _T("");

		switch(eErrorCode)
		{
		case EDDE_SUCCESS:
			PS_Wcscpy(str, 99, L"EDD Engine instance created successfully.");
			break;
		case CONFIG_FILE_ERROR:
			PS_Wcscpy(str, 99, L"Error during opening configuration xml file.");
			break;
		case DICTIONARY_FILE_ERROR:
			PS_Wcscpy(str, 99, L"Error during opening or reading dictionary file.");
			break;
		case LOAD_DD_AND_BLOCK_OPEN_ERROR:
			PS_Wcscpy(str, 99, L"Error during loading the dd or opening the block.");
			break;
		case INVALID_BINARY_FORMAT_ERROR:
			PS_Wcscpy(str, 99, L"This is not valid binary format.");
			break;
		case BAD_FILE_OPEN_ERROR:
			PS_Wcscpy(str, 99, L"Error during opening file.");
			break;
		case BAD_HEADER_FILE_ERROR:
			PS_Wcscpy(str, 99, L"Error during reading DDOD header file.");
			break;
		case CREATE_BLOCK_ERROR:
			PS_Wcscpy(str, 99, L"Error during creating blocks.");
			break;
		}

		return str;
	}
}

/***********************************************************************
 *
 * Name: panic
 *
 * ShortDesc: Printf on error and quit.
 *			  Original source app_pnic.c
 *
 * Descripton:
 *		This routine displays the error message and quits the program.
 *
 * Inputs:
 *		va_alist -- variable argument list.
 *
 * Returns:
 *		void.
 *
 **********************************************************************/
void
panic (const char *file, unsigned int line, const char *format, ...)
{
	fprintf(stdout, "%s(%d):", file, line);

	// Everything below here is like the "panic" defined below
	va_list         ap;

	va_start(ap, format);

	(void) vfprintf(stdout, format, ap);

	va_end(ap);
	throw 1;
}
