TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS +=  \
           EDD_Engine_Android \
           EDD_Engine_Common \
           ../Legacy/code/src/ServerProjects/ddshart/ddsHart.pro \
           ../Legacy/code/src/ServerProjects/server/HARTDDSHelper.pro \
           ../Legacy/code/src/ole/EDDLMethInterp/APPS/APPsupport/DevServices \
           ../Legacy/code/src/ole/EDDLMethInterp/GenBaseLib \
           ../Legacy/code/src/ole/EDDLMethInterp/MIEngineInterface \
           ../Legacy/code/src/FF/AMSDds \
           ../Legacy/code/src/FF/FFHelper \
           ../Interfaces/EDD_Engine_Interfaces \
           ../Legacy/code/src/ole/EDDLMethInterp/EDD_Engine_Mi \
           FDI/FDI_DDS_Lib \
           FDI/FDI_DDS_AdapterLib \
           FF/FFEDDEngine \
           FDI/FDIEDDEngine \
           HART/HARTEDDEngine \
          EDD_Engine/EDD_Engine.pro \

FDIEDDEngine.depends += FDI_DDS_Lib \
                        FDI_DDS_AdapterLib \
                        EDD_Engine_Interfaces \

EDD_Engine.depends += EDD_Engine_Interfaces \
                            HARTEDDEngine \
                            FDIEDDEngine \
                            MIEngineInterface \
#QMAKESPEC=linux-g++-32
#TARGET = $$(TARGET)x32
#QMAKE_CXXFLAGS += -m32

include(SuppressWarning.pri)

#QT += qml quick
#CONFIG += c++11

#SOURCES += main.cpp

#RESOURCES += qml.qrc

## Additional import path used to resolve QML modules in Qt Creator's code model
#QML_IMPORT_PATH =

## Additional import path used to resolve QML modules just for Qt Quick Designer
#QML_DESIGNER_IMPORT_PATH =

## The following define makes your compiler emit warnings if you use
## any feature of Qt which as been marked deprecated (the exact warnings
## depend on your compiler). Please consult the documentation of the
## deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

## You can also make your code fail to compile if you use deprecated APIs.
## In order to do so, uncomment the following line.
## You can also select to disable deprecated APIs only up to a certain version of Qt.
##DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

## Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
