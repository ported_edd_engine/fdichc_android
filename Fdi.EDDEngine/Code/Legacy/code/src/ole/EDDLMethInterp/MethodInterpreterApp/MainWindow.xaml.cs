﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using MethodInterpreterApp.ViewModels;
using Microsoft.Win32;
using System.Threading;             // GetAvailableThreads()
using System.ComponentModel;        // CancelEventArgs


namespace MethodInterpreterApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMainWindow _vm;
        bool winExit = false;

        private enum ProtocolType
        {
            HART = 1,
            FF = 2,
            PROFIBUS = 3,
            PROFIBUS_PN = 4,
            ISA100 = 5,
            GPE = 6,
            COMSERVER = 7
        };

        private enum MethodType
        {
            METHOD = 1,
            ACTION = 2,
        };

        public MainWindow()
        {
            InitializeComponent();

            _vm = new MainWindowVM();
            DataContext = _vm;
            _vm.FileToRun = Directory.GetCurrentDirectory() + "\\..\\Devices\\HART\\000026\\008f\\0101.fm8"; //default binaray Hart file
            _vm.sFFBlockName = UIFFBlockName.Text;// load the default value into sFFBlockName
        }

        private void ExitClick(object sender, RoutedEventArgs args)
        {
            _vm.Stop = true;

            int availableThreads;
            int dummy1, dummy2;
            int maxThreads;
            ThreadPool.GetMaxThreads(out maxThreads, out dummy2);
            do
            {
                ThreadPool.GetAvailableThreads(out availableThreads, out dummy1);
                Thread.Sleep(250);
            } while (availableThreads != maxThreads);

            winExit = true;
            Close();

        }//end of ExitClick()

        private void OnClosingWindow(object sender, CancelEventArgs e)
        {
            if (!winExit)
            {
                MessageBox.Show("Please use Exit button to close window.");
                e.Cancel = true;
            }
        }

        public void BrowseClick(object sender, RoutedEventArgs args)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;

            if (String.Compare((string)nRunMethod.Content, (string)"Begin Method", false) == 0)
            {   //the same
                switch (_vm.Protocol)
                {
                    case (short)ProtocolType.HART:
                    {
                        ofd.DefaultExt = ".fm8";
                        ofd.Filter = "DD Binary Files (*.fm8)|*.fm8|FDI DD Binary Files (*.fmA)|*.fmA|All files (*.*)|*.*";
                    }
                    break;
                    case (short)ProtocolType.FF:
                    {
                        ofd.DefaultExt = ".ff6";
                        ofd.Filter = "DD Binary Files (*.ff6)|*.ff6|All files (*.*)|*.*";
                    }
                    break;
                    case (short)ProtocolType.PROFIBUS:
                    case (short)ProtocolType.PROFIBUS_PN:
                    case (short)ProtocolType.GPE:
                    case (short)ProtocolType.COMSERVER:
                        {
                            ofd.DefaultExt = ".bin";
                        ofd.Filter = "DD Binary Files (*.bin)|*.bin|All files (*.*)|*.*";
                    }
                    break;
                    case (short)ProtocolType.ISA100:
                    {
                        ofd.DefaultExt = ".is6";
                        ofd.Filter = "DD Binary Files (*.is6)|*.is6|All files (*.*)|*.*";
                    }
                    break;
                }
                ofd.Multiselect = false;
                ofd.RestoreDirectory = true;

                if ((bool)ofd.ShowDialog())
                {
                    _vm.FileToRun = ofd.FileName;

                    // write the file name into the textbox.
                    methodFile.Text = ofd.FileName;
                }
            }
            else
            {
                MessageBox.Show("Method is running. Please wait until it is done.");
            }
        }//end of BrowseClick()

        public void ProtocolClick(object sender, RoutedEventArgs args)
        {
            RadioButton btn = sender as RadioButton;

            if (btn.Name == "HART")
            {
                _vm.Protocol = (short)ProtocolType.HART;
            }
            else if (btn.Name == "FF")
            {
                _vm.Protocol = (short)ProtocolType.FF;
            }
            else if (btn.Name == "PROFIBUS")
            {
                _vm.Protocol = (short)ProtocolType.PROFIBUS;
            }
            else if (btn.Name == "PROFIBUS_PN")
            {
                _vm.Protocol = (short)ProtocolType.PROFIBUS_PN;
            }
            else if (btn.Name == "ISA100")
            {
                _vm.Protocol = (short)ProtocolType.ISA100;
            }
            else if (btn.Name == "GPE")
            {
                _vm.Protocol = (short)ProtocolType.GPE;
            }
            else if (btn.Name == "COMSERVER")
            {
                _vm.Protocol = (short)ProtocolType.COMSERVER;
            }
        }

        public void MethodSelectionClick(object sender, RoutedEventArgs args)
        {
            RadioButton btn = sender as RadioButton;
            
            if (btn.Name == "Method")
            {
                _vm.MethodType = (short)MethodType.METHOD;
            }
            else if (btn.Name == "Action")
            {
                _vm.MethodType = (short)MethodType.ACTION;
            }
        }

        //find out all selected bit position
        public void OkBtn_Click(object sender, RoutedEventArgs args)
        {
        }

        private void UIFFBlockName_LostFocus(object sender, RoutedEventArgs e)
        {
            _vm.sFFBlockName = UIFFBlockName.Text;
        }

    }
}
