/**
 *	@(#) $Id: fch_ritm.cpp,v 1.2 2012/09/26 22:28:44 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for the ROD Item Fetch
 */

/* #includes */

#include "stdafx.h"
#include "fch_lib.h"
#include "HARTBreaker.h"
#include "evl_loc.h"
//#include "ddldefs.h"

#include "nsEDDEngine/Flats.h"


#ifdef DDSTEST
#include "tst_fail.h"
#endif

#ifndef	_HARTBREAKER_

#define HB_DUMP_BINARY(sAttrName)

#endif	// !_HARTBREAKER_


/*********************************************************************
 *
 *	Name:	get_extn_hdr
 *
 *	ShortDesc: Get non-attribute fields of object extension
 *
 *	Description:
 *		Retrieves the Item Type, Item Id, and Item Mask for an object
 *		extension and calculates the number of bytes for these fields
 *
 *	Inputs:
 *		obj_extn_ptr -	pointer to start of object extension
 *		itype -			type of item.  An error is returned if the
 *						the value obtained for item_type does not
 *						match this
 *
 *	Outputs:
 *		obj_extn_ptr -	points to first attribute in object extension
 *		extn_length -	pointer to length of object extension
 *		item_type -		pointer to value for Item Type
 *		item_subtype -	pointer to value for Item Subtype
 *		item_id -		pointer to value for Item ID
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_ITEM_TYPE_MISMATCH
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_extn_hdr(
unsigned char **obj_extn_ptr,
unsigned int   *extn_length,
unsigned char  *item_type,
unsigned char  *item_subtype,
unsigned long  *item_id,
ITEM_TYPE       itype)
{

	int             rc;	/* return code */

	unsigned char  *local_extn_ptr;	/* points to start of extension header */
	unsigned long   obj_item_id;	/* temporary variable to unpack item ID */
	int             i;


#ifdef DDSTEST
	TEST_FAIL(GET_EXTN_HDR);
#endif

	/*
	 * Extract the extension length, item type, item sub-type, and item
	 * ID fields. Compare the item type in the object extension with
	 * the requested item type and return if these do not match.
	 */

	local_extn_ptr = *obj_extn_ptr;

	if (!*extn_length) {
		return(FETCH_NO_OBJ_EXTN);
	}

	*item_type = *(local_extn_ptr++);
	if (itype != (unsigned short) *item_type) {
		return (FETCH_ITEM_TYPE_MISMATCH);
	}

	*item_subtype = *(local_extn_ptr++);

	/*
	 * Extract 4 bytes for the Item ID
	 */

	obj_item_id = 0L;
	for (i = 0; i < ITEM_ID_SIZE; i++) {
		obj_item_id = (obj_item_id << 8) |
			(unsigned long) *(local_extn_ptr++);
	}
	*item_id = obj_item_id;

	/*
	 * Extract the item mask and set the
	 * pointer to the first attribute
	 */

	int dd_item_symbol=0;

	DDL_UINT size=(*extn_length)-(ITEM_ID_SIZE-2);
	DDL_PARSE_INTEGER(&local_extn_ptr, &size, (ulong *)&dd_item_symbol);

	/*
	 * Advance the object extension pointer to the first attribute field
	 */

	*obj_extn_ptr = local_extn_ptr;

	return (SUCCESS);
}


/*********************************************************************
 *
 *	Name: parse_attribute_info
 *
 *	ShortDesc: Get info about the attribute passed in
 *
 *	Description:
 *		Parse the Attribute Identifier portion of the item attribute
 *		into the tag and length values
 *
 *	Inputs:
 *		attr_ptr -		pointer to first byte of Attribute ID
 *
 *	Outputs:
 *		attr_ptr -		pointer to first byte after Attribute ID
 *		attr_tag -		pointer to value of attribute tag
 *		attr_len -		pointer to value of attribute data length
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_ATTR_LENGTH_OVERFLOW
 *		FETCH_ATTR_ZERO_LENGTH
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
parse_attribute_info(
unsigned char **attr_ptr,
unsigned short *attr_tag,
unsigned long  *attr_len)
{
	int rc = SUCCESS;

	/*
	 * Check for valid parameters
	 */

	ASSERT_RET(attr_ptr && attr_tag && attr_len,
		FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(PARSE_ATTRIBUTE_ID);
#endif

	/*
	 * Extract the tag from the Attribute Identifier (octet #1).
	 */


	*attr_tag = *(*attr_ptr)++;

	// Extract the attribute offset field
	DDL_UINT size = 5;	// Dummy size so we have an arg below

	// Extract the attribute length field
	DDL_PARSE_INTEGER(attr_ptr, &size, attr_len);

	if (*attr_len == 0) {
		PS_TRACE(L"\n*** Error attribute %d, attribute length zero not allowed",*attr_tag);
		return (FETCH_ATTR_ZERO_LENGTH);
	}

	return (rc);

}

/*********************************************************************
 *
 *  Name:	attach_any_data
 *
 *	ShortDesc:	Attach any (attribute) to flat structure
 *
 *	Description: Posts the pointer for an item attribute to
 *				 its corresponding DEPBIN pointer in the flat
 *				 structure.
 *
 *	Inputs:
 *		attr_data_ptr - pointer to attribute data in the
 *						scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *		flat -			flat item
 *		items -			pointer to attribute map
 *
 *  Outputs:
 *      flat -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *
 *********************************************************************/
static int
attach_any_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
::nsEDDEngine::ItemBase *flat,
unsigned short  tag,
std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeItem> *items)
{
	::nsEDDEngine::DEPBIN        **depbin_ptr;

	depbin_ptr = (::nsEDDEngine::DEPBIN **) 0L;	/* Initialize the DEPBIN pointers */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	nsEDDEngine::AttributeName attrib_name=(nsEDDEngine::AttributeName)tag;

	if(items->find(attrib_name)==items->end())
	{
		PS_TRACE(_T("*** ATTRIBUTE (%d) NOT PART of this FLAT TYPE"), tag);
		return FETCH_INVALID_ATTRIBUTE;
	}

	::nsEDDEngine::AttributeItem item=(*items)[attrib_name];

	depbin_ptr=item.depbin_storage;

	if(!depbin_ptr)
		return FETCH_INVALID_ATTRIBUTE;

	if(*depbin_ptr)
	{
		PS_TRACE(_T("\n*** ATTRIBUTE (%d) Already Exists... duplicated\n"), tag);
		delete *depbin_ptr;
	}

	*depbin_ptr=new ::nsEDDEngine::DEPBIN(attr_data_ptr, data_len, true);

	/*
	 * Set the .bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	flat->masks.bin_exists |= attrib_name;
	flat->masks.bin_hooked |= attrib_name;

	return (SUCCESS);
}


/*********************************************************************
 *
 *  Name:	get_item_attr_new
 *
 *	ShortDesc:	Gets item attribute data into depbins
 *
 *	Description: Gets item attribute data into depbins
 *
 *	Inputs:
 *		scrpad					scratchpad
 *		req_mask -				requested attributes
 *		extn_attr_length -		extension length
 *		obj_ext_ptr -			extension pointer
 *
 *  Outputs:
 *      flat -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *
 *********************************************************************/
static int
get_item_attr_new(
	::nsEDDEngine::AttributeNameSet  *req_mask,
	::nsEDDEngine::ItemBase *flat,
	unsigned int   extn_attr_length,
	unsigned char  *obj_ext_ptr//,
	)
{
	unsigned char  *obj_attr_ptr = NULL;	/* pointer to attributes in object extension */

	unsigned short  curr_attr_tag = 0;		/* tag for current attribute */
	unsigned long   curr_attr_length = 0;	/* data length for current attribute */

	::nsEDDEngine::AttributeNameSet local_req_mask;	/* request mask for object *///	unsigned long   local_req_mask;	/* request mask for object */
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(obj_ext_ptr, FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(GET_ITEM_ATTR);
#endif

	/*
	 * Point to the first attribute in the object extension and begin
	 * extracting the attribute data
	 */

	local_req_mask = *req_mask;
	obj_attr_ptr = obj_ext_ptr;

	int             rc;	/* return code */
	int dd_item_attribute_count=0;
	DDL_UINT size=extn_attr_length;
	DDL_PARSE_INTEGER(&obj_attr_ptr, &size, (ulong *)&dd_item_attribute_count);

	std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeItem> items;
	flat->GetDepbinMap(items);
	
	bool invalid_attribute_found=false;
	
	for(int i=0;i<dd_item_attribute_count;i++)
	{
		/*
		 * Retrieve the Attribute Identifier information from the
		 * leading attribute bytes.  The object extension pointer will
		 * point to the first byte after the Attribute ID, which will
		 * be the local offset.
		 */

		rcode = parse_attribute_info(&obj_attr_ptr,
			&curr_attr_tag, 
			&curr_attr_length);

		if (rcode != SUCCESS) {
			return (rcode);
		}

		if (curr_attr_length > 0)
		{
			rcode = ::attach_any_data(obj_attr_ptr, curr_attr_length, flat, curr_attr_tag, &items);

			if ((rcode!=SUCCESS) && (rcode!=FETCH_INVALID_ATTRIBUTE))
				goto err_exit;

			if(rcode==FETCH_INVALID_ATTRIBUTE)
				invalid_attribute_found=true;
		}

		// move to next attribute
		obj_attr_ptr+=curr_attr_length;

		//local_req_mask &= ~curr_attr_mask;	/* clear bit */
	} /* end for */

	/*
	 * Return the local request mask which will indicate the attributes
	 * that were requested but have not been attached.
	 */

	*req_mask = local_req_mask;
	if(invalid_attribute_found)
		return FETCH_INVALID_ATTRIBUTE;

	return (SUCCESS);

err_exit:
	*req_mask = local_req_mask;
	return (rcode);
}

/*********************************************************************
 *
 *	Name:	get_rod_item
 *
 *	ShortDesc:	Get ROD object prior to processing attributes
 *
 *	Description:
 *		This routine is called by the exposed interface. It calls the ROD manager
 *		to load an object (extension) into the scratchpad.  If the load
 *		is successful, it confirms that the object extension has the
 *		correct item ID and item type, then sets a pointer to the
 *		first attribute in the extension in order to process the
 *		requested item attributes.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		attr_req_mask -	attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type)
 *		itype -			identifies the type of Item requested
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and data referenced by the object.
 *		attr_req_mask -	remaining attributes requested for the Item. As
 *                      an attribute is attached, the bit in the mask
 *                      corresponding to the attribute is reset.
 *		flats -			pointer to the flat structure containing the
 *						pointers to the Item attributes in the
 *						scratchpad memory (contents of structure
 *						depend on the Item type)
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_ITEM_TYPE_MISMATCH
 *		FETCH_INVALID_ATTRIBUTE
 *		FETCH_EMPTY_ITEM_MASK
 *		FETCH_ATTRIBUTE_NOT_FOUND
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_rod_item(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
::nsEDDEngine::AttributeNameSet *attr_req_mask,
::nsEDDEngine::ItemBase   *flats,
ITEM_TYPE       itype)
{
	OBJECT         *rod_obj;/* points to ROD object structure, returned
				 * from get_rod_obj(), containing pointers to
				 * the object extension and local data area */
	unsigned char  *obj_extn_ptr;	/* points to start of object extension */
	unsigned int    obj_extn_length;	/* length of object extension */
	unsigned char   obj_item_type;	/* item type contained in object
					 * extension */
	unsigned char	obj_item_subtype;	/* item subtype contained in
					 * object extension */
	unsigned long   obj_item_id;	/* item ID contained in object
					 * extension */
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
					 * object extension if application
					 * scratchpad is too small */
	unsigned char  *pad_buf;	/* holds object
							 * extension to allow
							 * determination of
							 * scratchpad size
							 * (copied to
							 * application
							 * scratchpad if space
							 * permits) */
	int             rcode;

#ifdef DDSTEST
	TEST_FAIL(GET_ROD_ITEM);
#endif


	// Get the size of the object that we need to access
	int size=0;
	rcode=get_rod_object_size(env_info, rhandle, obj_index, &size);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	// Allocate a buffer to read this object into
	pad_buf=(unsigned char *)malloc(size);

	if(!pad_buf) {
		return DDL_MEMORY_ERROR;
	}

	local_scrpad.pad = pad_buf;
	local_scrpad.size=size;
	local_scrpad.used=0;

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = local_scrpad.pad;

	/*
	 * Retrieve the item object.  Return with the resulting error code if
	 * not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		free (pad_buf);
		return (rcode);
	}

	/*
	 * Retrieve the extension length, item type, item ID, and item mask
	 * fields
	 */
	obj_extn_length=rod_obj->ext_length;

	rcode = get_extn_hdr(&obj_extn_ptr, &obj_extn_length,
		&obj_item_type, &obj_item_subtype, &obj_item_id,
		itype);

	if (rcode != SUCCESS) {
		free(pad_buf);
		return (rcode);
	}


	// NOTE: obj_item_mask can be zero if there are no attributes
	//		For instance all AXIS attributes are optional.

	/*
	 * Assign the pointer to the appropriate flat structure for the item
	 * type requested.  Set the bin_exists and item ID fields in the
	 * flat structure.
	 */

	if (obj_item_id) 
	{
		((::nsEDDEngine::ItemBase *) flats)->masks.bin_exists.clear();
		((::nsEDDEngine::ItemBase *) flats)->id = obj_item_id;

		switch(itype)
		{
			case COLLECTION_ITYPE:
				{
					((nsEDDEngine::FLAT_COLLECTION *) flats)->subtype = (nsEDDEngine::ITEM_TYPE) obj_item_subtype;
				}
				break;

			case ITEM_ARRAY_ITYPE:
				{
					((nsEDDEngine::FLAT_ITEM_ARRAY *) flats)->subtype = (nsEDDEngine::ITEM_TYPE) obj_item_subtype;
				}
				break;
		}
	}

	rcode = get_item_attr_new(	attr_req_mask, 
								flats,
								obj_extn_length, 
								obj_extn_ptr);

	free(pad_buf);

	return (rcode);
}


/*********************************************************************
 *
 *	Name:	fch_rod_item
 *
 *	ShortDesc: ROD item fetch interface
 *
 *	Description:
 *		This function provides the external interface for a ROD Item
 *		Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		request_mask -	attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type)
 *		itype -			identifies the type of Item requested
 *
 *	Outputs:
 *		flat_item -		pointer to the flat structure containing the
 *						pointers to the Item attributes in the
 *						scratchpad memory (contents of structure depend
 *						on the Item type)
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_item(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
::nsEDDEngine::AttributeNameSet   request_mask,
::nsEDDEngine::ItemBase *flat_item,
ITEM_TYPE       itype)
{
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET( flat_item, FETCH_INVALID_PARAM);

	/**
	 *	If this item is a variable, we must turn on the TYPE bit in the 
	 *	request mask.  This is required for var_type subattributes.
	 **/

	if (itype == VARIABLE_ITYPE) {
		request_mask |= ::nsEDDEngine::variable_type;//VAR_TYPE_SIZE;
	}

#ifdef DEBUG
	dds_item_flat_check(flat_item, itype);
#endif
	rcode = get_rod_item(env_info, rhandle, obj_index, &request_mask, flat_item, itype);
#ifdef DEBUG
	dds_item_flat_check(flat_item, itype);
#endif

	return (rcode);
}
