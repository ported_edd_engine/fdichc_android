////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "HartBuiltIn.cpp"
//
// Creation Date: 5-2-07
//
// Purpose: Handles all details for a builtin function such as post-tokenized name, 
//			number of params, and type of each param, houses InvokeFunction which
//			actually calls the end point for builtins
////////////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "HartBuiltIn.h"
#include "BuiltInMapper.h"
#include "FunctionExpression.h"  // MTD 052407: added to pass func exp * to InvokeFunction for global param issues
#include "logging.h"
#include "INTER_VARIANT.h"
#include "MEE.h"


////////////////////////////////////////////////////////////////////////////////////////////////////
CHartBuiltIn::CHartBuiltIn()
: CBuiltIn()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////
CHartBuiltIn::~CHartBuiltIn()
{
	;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
bool CHartBuiltIn::InvokeFunction(
				char *pchFunctionName
				, int iNumberOfParameters
				, INTER_VARIANT *pVarParameters
				, INTER_VARIANT *pVarReturnValue
				, int	*pBuiltinReturnCode
				, FunctionExpression* pFuncExp	// MTD 052407: added to pass func exp * to InvokeFunction for global param issues
				)
{
	bool bRetVal = true;
	CBuiltInMapper::GetInstance()->LockBIMapper(m_pMeth->GetStartedProtocol());
	CBuiltInMapper *pMapper = CBuiltInMapper::GetInstance();
	CBuiltInInfo* pInfo = (*pMapper)[pchFunctionName];
	CBuiltInMapper::GetInstance()->ReleaseBIMapper();

	if( pInfo )
	{
		switch( pInfo->GetBuiltInEn() )
		{
			case BUILTIN_UNDEFINED:
				bRetVal = false;
				break;
			default:

				bRetVal = CBuiltIn::InvokeFunction(pchFunctionName,iNumberOfParameters,pVarParameters,
												pVarReturnValue,pBuiltinReturnCode, pFuncExp);

				if (bRetVal)
				{
					CBuiltIn::DebugLogParams(pchFunctionName,iNumberOfParameters, pVarParameters, pVarReturnValue, pBuiltinReturnCode);

					if (!pBuiltinReturnCode)
					{
						*pBuiltinReturnCode = 0xFF;	//replaced by bRetVal here, indicating normal result
					}
					CBuiltIn::PostDebugLogParams(pchFunctionName,iNumberOfParameters, pVarParameters, pVarReturnValue, pBuiltinReturnCode);
				}
				break;
		}
	}
	else
	{
		bRetVal = false;
	}

	return bRetVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//This function call is protected by CRITICAL_SECTION m_IsInfo_cs
void CHartBuiltIn::BuildMapper()
{
	CBuiltInMapper *pMap = CBuiltInMapper::GetInstance();
	CBuiltInInfo tmp;

	tmp.Set(BUILTIN_delay,						"delay",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT); 	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DELAY,						"DELAY",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DELAY_TIME,					"DELAY_TIME",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_BUILD_MESSAGE,				"BUILD_MESSAGE",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_PUT_MESSAGE,				"PUT_MESSAGE",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_put_message,				"put_message",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_ACKNOWLEDGE,				"ACKNOWLEDGE",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_acknowledge,				"acknowledge",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__get_dev_var_value,			"_get_dev_var_value",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__get_local_var_value,		"_get_local_var_value",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__display_xmtr_status,		"_display_xmtr_status",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_display_response_status,	"display_response_status",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DISPLAY,					"DISPLAY",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_display,					"display",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);
	
	tmp.Set(BUILTIN_SELECT_FROM_LIST,			"SELECT_FROM_LIST",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_select_from_list,			"select_from_list",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__vassign,					"_vassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__dassign,					"_dassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__fassign,					"_fassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__lassign,					"_lassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__iassign,					"_iassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__fvar_value,				"_fvar_value",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__ivar_value,				"_ivar_value",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__ivar_value,				"int_value",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__lvar_value,				"_lvar_value",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_sassign,					"sassign",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_SIMPLE_VARIABLE, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_save_values,				"save_values",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_get_more_status,			"get_more_status",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__get_status_code_string,	"_get_status_code_string",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 4, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_get_enum_string,			"_get_enum_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__get_dictionary_string,		"_get_dictionary_string",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__dictionary_string,			"_dictionary_string",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_literal_string,				"literal_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_resolve_param_ref,			"resolve_param_ref",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_resolve_array_ref,			"resolve_array_ref",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_resolve_record_ref,			"resolve_record_ref",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_HT_resolve_local_ref,		"resolve_local_ref",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_DD_ITEM);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_rspcode_string,				"rspcode_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 4, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_rspcode_string,				"get_rspcode_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 4, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_comm_status,			"_set_comm_status",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_device_status,			"_set_device_status",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_resp_code,				"_set_resp_code",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_all_resp_code,			"_set_all_resp_code",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_no_device,				"_set_no_device",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_SET_NUMBER_OF_RETRIES,		"SET_NUMBER_OF_RETRIES",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_comm_status,		"_set_xmtr_comm_status",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_device_status,	"_set_xmtr_device_status",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_resp_code,		"_set_xmtr_resp_code",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_all_resp_code,	"_set_xmtr_all_resp_code",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_no_device,		"_set_xmtr_no_device",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_all_data,			"_set_xmtr_all_data",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__set_xmtr_data,				"_set_xmtr_data",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_abort,						"abort",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_process_abort,				"process_abort",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	//this is A
	tmp.Set(BUILTIN__add_abort_method,			"_add_abort_method",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	//Version A
	tmp.Set(BUILTIN__remove_abort_method,		"_remove_abort_method",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_remove_all_abort,			"remove_all_abort",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_push_abort_method,			"_push_abort_method",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_pop_abort_method,			"pop_abort_method",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);										
	pMap->Add(tmp);

	tmp.Set(BUILTIN_NaN_value,					"NaN_value",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_isetval,					"isetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_fsetval,					"fsetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);					
	pMap->Add(tmp);

	tmp.Set(BUILTIN_igetval,					"igetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);										
	pMap->Add(tmp);

	tmp.Set(BUILTIN_fgetval,					"fgetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);					
	pMap->Add(tmp);

	tmp.Set(BUILTIN_sgetval,					"sgetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_ssetval,					"ssetval",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_isetval,					"lsetval",						RUL_KEYWORD,	RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_igetval,					"lgetval",						RUL_KEYWORD,	RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_send,						"send",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_send_command,				"send_command",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_send_command_trans,			"send_command_trans",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_send_trans,					"send_trans",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_ext_send_command,			"ext_send_command",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 4, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_ext_send_command_trans,		"ext_send_command_trans",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 5, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_tsend_command,				"tsend_command",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_tsend_command_trans,		"tsend_command_trans",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_abs,						"abs",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_acos,						"acos",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_asin,						"asin",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_atan,						"atan",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_cbrt,						"cbrt",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_ceil,						"ceil",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_cos,						"cos",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_cosh,						"cosh",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_exp,						"exp",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_floor,						"floor",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_fmod,						"fmod",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

#ifdef XMTR								
	tmp.Set(BUILTIN_frand,						"frand",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0, RUL_NUMERIC_CONSTANT); // ??? 0 param, but defines 1...
	pMap->Add(tmp);
#endif																		

	tmp.Set(BUILTIN_log,						"log",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_log10,						"log10",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_log2,						"log2",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_pow,						"pow",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_round,						"round",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_sin,						"sin",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_sinh,						"sinh",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_sqrt,						"sqrt",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_tan,						"tan",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_tanh,						"tanh",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_trunc,						"trunc",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_atof,						"atof",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_atoi,						"atoi",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);																
	pMap->Add(tmp);

	//this is version B
	tmp.Set(BUILTIN__itoa,						"itoa",			RUL_KEYWORD, RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_STR_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

//	tmp.Set(BUILTIN_YearMonthDay_to_Date,		"YearMonthDay_to_Date",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT); // not in spec
//	pMap->Add(tmp);

	tmp.Set(BUILTIN_Date_to_Year,				"Date_to_Year",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_Date_to_Month,				"Date_to_Month",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);															
	pMap->Add(tmp);

	tmp.Set(BUILTIN_Date_to_DayOfMonth,			"Date_to_DayOfMonth",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);															
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DATE_to_days,			"DATE_to_days",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_days_to_DATE,			"days_to_DATE",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_From_DATE_AND_TIME_VALUE,	"From_DATE_AND_TIME_VALUE",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_From_TIME_VALUE,	"From_TIME_VALUE",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_GetCurrentTime,				"GetCurrentTime",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);															
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strstr,						"strstr",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 2, RUL_STR_CONSTANT,RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strupr,						"strupr",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strlwr,						"strlwr",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_STR_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strlen,						"strlen",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);																
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strcmp,						"strcmp",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_STR_CONSTANT,RUL_STR_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strtrim,					"strtrim",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 1, RUL_STR_CONSTANT);																	
	pMap->Add(tmp);

	tmp.Set(BUILTIN_strmid,						"strmid",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_STR_CONSTANT, 3, RUL_STR_CONSTANT,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_discard_on_exit,			"discard_on_exit",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__ListInsert,				"_ListInsert",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__ListDeleteElementAt,		"_ListDeleteElementAt",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__MenuDisplay,				"_MenuDisplay",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3,RUL_NUMERIC_CONSTANT,RUL_STR_CONSTANT,RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_remove_all_abort_methods,	"remove_all_abort_methods",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DiffTime,					"DiffTime",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_AddTime,					"AddTime",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 2, RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_Make_Time,					"Make_Time",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 7, RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_To_Time,					"To_Time",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 5, RUL_SIMPLE_VARIABLE,RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT,RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_Date_To_Time,				"Date_To_Time",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_To_Date,					"To_Date",						RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE,RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_Time_To_Date,				"Time_To_Date",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_seconds_to_TIME_VALUE,		"seconds_to_TIME_VALUE",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_TIME_VALUE_to_seconds,		"TIME_VALUE_to_seconds",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_TIME_VALUE_to_Hour,			"TIME_VALUE_to_Hour",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_TIME_VALUE_to_Minute,		"TIME_VALUE_to_Minute",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_TIME_VALUE_to_Second,		"TIME_VALUE_to_Second",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DATE_AND_TIME_VALUE_to_string,		"DATE_AND_TIME_VALUE_to_string",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 4, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_SIMPLE_VARIABLE, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_DATE_to_string,			"DATE_to_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_TIME_VALUE_to_string,		"TIME_VALUE_to_string",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_timet_to_string,			"timet_to_string",				RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_STR_CONSTANT, RUL_STR_CONSTANT, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_timet_to_TIME_VALUE,		"timet_to_TIME_VALUE",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);
	tmp.Set(BUILTIN_timet_to_TIME_VALUE,		"timet_To_TIME_VALUE",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_To_TIME_VALUE,		"To_TIME_VALUE",			RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 3, RUL_SIMPLE_VARIABLE, RUL_SIMPLE_VARIABLE, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_fpclassify,					"fpclassify",					RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_nanf,						"nanf",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_nan,						"nan",							RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_LOG_MESSAGE,		"LOG_MESSAGE",	RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE,	2,	RUL_SIMPLE_VARIABLE,	RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_drand,				"drand",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_dseed,				"dseed",		RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 1, RUL_SIMPLE_VARIABLE);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_isOffline,		"isOffline",	RUL_KEYWORD,	RUL_FUNCTION,	RUL_SIMPLE_VARIABLE, 0);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__ERROR, "_ERROR",	RUL_KEYWORD, RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__TRACE, "_TRACE",	RUL_KEYWORD, RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN__WARNING, "_WARNING",	RUL_KEYWORD, RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 1, RUL_STR_CONSTANT);
	pMap->Add(tmp);

	tmp.Set(BUILTIN_LINE_NUMBER, "_l", RUL_KEYWORD, RUL_FUNCTION, RUL_SIMPLE_VARIABLE, 1, RUL_NUMERIC_CONSTANT);
	pMap->Add(tmp);

}
