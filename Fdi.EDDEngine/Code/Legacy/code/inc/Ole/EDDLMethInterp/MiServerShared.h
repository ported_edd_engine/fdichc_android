#pragma once

#ifdef USE_MSXML2
// Import MSXML6 namespace
#import	 <msxml6.dll> exclude("ISequentialStream", "_FILETIME")
// Use msxml2 namespace
using namespace MSXML2;

const LPTSTR ELMTNAME_ENUM_SELECTION		=	_T("EnumSelection");
const LPTSTR ATTRNAME_NUMBER				=	_T("Number");
const LPTSTR ATTRNAME_DESCRIPTION			=	_T("Description");
const LPTSTR USEFORBLANKDESCRIPTION			=	_T("Description not available");
const LPTSTR ATTRNAME_HELP					=	_T("Help");
const LPTSTR ELMTNAME_ENUM_DATA				=	_T("EnumerationData");
const LPTSTR ELMTNAME_DD_ITEMS				=	_T("DDItem");
const LPTSTR ATTRNAME_DD_ITEM_ID			=	_T("ItemId");

#else // XXX: if not MSXML2 - use pugixml

#include "pugixml.hpp"

const wchar_t *ELMTNAME_ENUM_SELECTION = L"EnumSelection";
const wchar_t *ATTRNAME_NUMBER = L"Number";
const wchar_t *ATTRNAME_DESCRIPTION = L"Description";
const wchar_t *USEFORBLANKDESCRIPTION = L"Description not available";
const wchar_t *ATTRNAME_HELP = L"Help";
const wchar_t *ELMTNAME_ENUM_DATA = L"EnumerationData";
const wchar_t *ELMTNAME_DD_ITEMS = L"DDItem";
const wchar_t *ATTRNAME_DD_ITEM_ID = L"ItemId";

#endif