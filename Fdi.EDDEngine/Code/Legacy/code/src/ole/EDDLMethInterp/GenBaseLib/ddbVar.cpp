/*************************************************************************************************
 *
 * $Workfile: ddbVar.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		definition of the variable instance class
 * 8/30/2	sjv	 creation from ddbVariable
 */
#include "stdafx.h"
#include "ddbVar.h"
#include <CMethodInterpreterDevice.h>
#include <Inf/NtcSpecies/BssProgLog.h>
#include <ServerProjects/CommonToolkitStrings.h>
#include <Ole/EDDLMethInterp/MiServerShared.h>
#include "INTER_VARIANT.h"

#include "pugixml.hpp"
#include "varientTag.h"
#include "float.h"
#include <climits>


extern bool openIni();
extern bool closeIni();
#ifdef ISDLL
char *getSymbolValue(unsigned long id){return "getSymbol NOT implemented";};
char *getSymbolValue(char *id){return "getSymbol NOT implemented";};
#else
extern char *getSymbolValue(unsigned long id);
extern char *getSymbolValue(char *id);
#endif


#define DFLT_ISDYNAMIC  false
#define DFLT_ISLOCAL    false
#define DFLT_ISREADONLY true

#define LOG_CMD_SELECTION 1


// construct from a dllapi base construct
hCVar::hCVar(DevInfcHandle_t h, aCitemBase* paItem) : hCitemBase(h, paItem)
{
	readCommand.cmdNumber = -1;
	writeCommand.cmdNumber = -1;
	getMinMaxList(RangeList);
}

hCVar::hCVar(DevInfcHandle_t h, variableType_t vt, int vSize) : hCitemBase(h, vt, vSize)
{
	readCommand.cmdNumber = -1;
	writeCommand.cmdNumber = -1;
	getMinMaxList(RangeList);
}

                        
hCVar::hCVar(hCVar* pSrc,  itemIdentity_t newID) :  hCitemBase((hCitemBase*)pSrc,newID)
{
	readCommand.cmdNumber = -1;
	writeCommand.cmdNumber = -1;
	getMinMaxList(RangeList);
}


hCVar::~hCVar()
{

}

RETURNCODE hCVar::destroy(void)
{
	RETURNCODE rc = 0;
	try
	{
		switch( VariableType() ) 
		{
			case vT_Enumerated:		
				rc = ((hCEnum*)this)->destroy(); 
				break;
			case vT_BitEnumerated:	
				rc = ((hCBitEnum*)this)->destroy();
				break;/* I don't believe this is ever used */
			default:
				break;
		}// end switch
		hCitemBase::destroy();
		
	}
	catch(...)
	{
		cerr<<"RETURNCODE hCVar::destroy(void) inside catch(...) !!"<<endl;
	}
	return SUCCESS;
}

//
//
//
hCattrBase*   hCVar::newHCattr(DevInfcHandle_t h, aCattrBase* pACattr)  // builder 
{
	hCattrBase*    pReturnPtr = NULL;
	return pReturnPtr;
}

/****************************** ASSUMPTION *****************************/
/* The type doesn't really change...even though it's a conditional.....*/

/*static*/	//Returns null ptr on error
//            Construct a new hCVar type class from an equivellent aCvarType class
//hCVar* hCVar::newVar(DevInfcHandle_t h, aCitemBase * paItem) // from api variable base class
//{
//	void* pR = NULL;
//	return ((hCVar*) pR);
//}


//
//
//
bool hCVar::ReadParameterData(CValueVarient *pOutput, wchar_t *sValue )
{
	bool bRetVal = false;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		int iRetVal = pDevice->ReadParameterData2(pOutput, sValue, 0, 0, this->itemId, 0 );
		
		if( iRetVal == BLTIN_SUCCESS )
		{
			bRetVal = true;			
		}
	}
	return bRetVal;
}

//
//
//
double hCVar :: GetScalingFactor()
{ 
	return 1.0; 
}

void hCVar::InitializeStringToCharacter(wchar_t oneChar,int iValSize, wstring * returnedString)
{
	wchar_t *wchString = new wchar_t[iValSize + 1];
	for (int i=0; i<iValSize; i++)
	{
		wchString[i] = oneChar;
	}
	wchString[iValSize] = _T('\0');
	*returnedString = wchString;
	delete [] wchString;
}

void  hCVar :: getaAttr(varAttrType_t attrType, CValueVarient *pvtOutValue)
{

	if( varAttrDefaultValue )
	{

		if( vtDefaultVal.vTagType == ValueTagType::CVT_EMPTY)	//vtDefaultVal is in type of CValueVarient, see ddbVar.h
		{
			CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
			if( pDevice )
			{
				int iRetVal = pDevice->ReadParameterData2( pvtOutValue, _T("DefaultValue"), 0, 0, itemId, 0 );
				if (iRetVal != BLTIN_SUCCESS || pvtOutValue->vTagType == ValueTagType::CVT_EMPTY)

				{
					//DEFAULT_VALUE is not available
					variableType_t iValType = (variableType_t)VariableType();
					int iValSize = VariableSize();

					//set default DEFAULT_VALUE based on HART spec HCF_SPEC-500 section 8.1.2
					switch (iValType)
					{
					case vT_Boolean:
						CV_VT(pvtOutValue, ValueTagType::CVT_BOOL);
						pvtOutValue->vValue.bIsTrue = false;		//VT_BOOL: True=-1, False=0
						break;

					case vT_Integer:
						switch (iValSize)
						{
						case 1:
							CV_VT(pvtOutValue, ValueTagType::CVT_I1);
							CV_I1(pvtOutValue) = (char)1;
							break;
						case 2:
							CV_VT(pvtOutValue, ValueTagType::CVT_I2);
							CV_I2(pvtOutValue) = 1;
							break;
						case 3:
						case 4:
							CV_VT(pvtOutValue, ValueTagType::CVT_I4);
							CV_I4(pvtOutValue) = 1;
							break;
						default:
							CV_VT(pvtOutValue, ValueTagType::CVT_I8);
							CV_I8(pvtOutValue) = 1;
							break;
						}
						break;

					case vT_HartDate:
						CV_VT(pvtOutValue , ValueTagType::CVT_I4);
						CV_I4(pvtOutValue) = 0x010100;
						break;

					// DATE_AND_TIME default is 1-Jan-1900 00:00:00 per 61804-3, Table D.9
					case vT_DateAndTime:
						CV_VT(pvtOutValue, ValueTagType::CVT_UI8);
						CV_UI8(pvtOutValue) = 0x010100;
						break;

					case vT_Unsigned:
						switch (iValSize)
						{
						case 1:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI1);
							CV_UI1(pvtOutValue) = 1;
							break;
						case 2:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI2);
							CV_UI2(pvtOutValue) = 1;
							break;
						case 3:
						case 4:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtOutValue) = 1;
							break;
						default:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI8);
							CV_UI8(pvtOutValue) = 1;
							break;
						}
						break;

					case vT_Enumerated:
						{
							CV_VT(pvtOutValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtOutValue) = 0;

							//get enumeration list
							hCenumList aEnumList(this->devHndl());
							if ( (SUCCESS == ((hCEnum *)this)->procureList(aEnumList)) && (aEnumList.size() > 0) )
							{
	 							// find the first listed enumeration
								vector<hCenumDesc>::iterator edIT = aEnumList.begin();
								CV_UI4(pvtOutValue) = edIT->val;
							}
						}
						break;

					case vT_Index:
						{
							CV_VT(pvtOutValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtOutValue) = 0;

							//get index list
							hCitemBase*				pIB = NULL;
							int rc = (((hCindex *)this)->pIndexed)->getArrayPointer(pIB);			// find pointer to the ARRAY OF COLLECTION
							if(	(rc == SUCCESS) && (pIB != NULL) )
							{
								hCitemArray*		pIndxdArr = (hCitemArray*)pIB;
								if ( pIndxdArr->getIType() == iT_ItemArray)	//iT_Array,iT_List
								{
									UIntList_t		uintList;
									pIndxdArr->getAllindexValues(uintList);
									if(uintList.size() > 0)
									{
										UIntList_t::iterator it = uintList.begin();
										CV_UI4(pvtOutValue) = (*it);
									}
								}
							}
						}
						break;

					// The defaults for BIT_ENUMERATED, TIME, and DURATION are 0 (Table D9.2 of 61804-3)
					case vT_BitEnumerated:
					case vT_Time:
					case vT_Duration:
						switch (iValSize)
						{
						case 1:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI1);
							CV_UI1(pvtOutValue) = 0;
							break;
						case 2:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI2);
							CV_UI2(pvtOutValue) = 0;
							break;
						case 3:
						case 4:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtOutValue) = 0;
							break;
						default:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI8);
							CV_UI8(pvtOutValue) = 0;
							break;
						}
						break;

					//In namesSpace nsConsumer, the vT_TimeValue value with 4 bytes is unsigned, and value with 8 bytes is signed.
					case vT_TimeValue:
						switch (iValSize)
						{
						case 4:
							CV_VT(pvtOutValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtOutValue) = 0;
							break;
						case 8:
							CV_VT(pvtOutValue, ValueTagType::CVT_I8);
							CV_I8(pvtOutValue) = 0;
							break;
						}
						break;

					case vT_FloatgPt:	//4 byte real
						CV_VT(pvtOutValue, ValueTagType::CVT_R4);
						CV_R4(pvtOutValue) = 0;
						break;

					case vT_Double:	//8 byte real
						CV_VT(pvtOutValue, ValueTagType::CVT_R8);
						CV_R8(pvtOutValue) = 0;
						break;

					//Each character set to question mark(?)  (61804-3, table D.9 for PACKED_ASCII, Hart500 spec for ASCII)
					//Spec conflict on ASCII- 61804-3 says space for ASCII, while HART500 spec says '?'
					//Use HART version due to number of devices in the field.
					case vT_Ascii:
					case vT_PackedAscii:
						{
						    CV_VT(pvtOutValue, ValueTagType::CVT_WSTR);
							InitializeStringToCharacter(_T('?'),iValSize,&(CV_WSTR(pvtOutValue)));
						}
						break;

					//Each bit set to 0  (61804-3, table D.9 BITSTRING)
					case vT_BitString:
					case vT_OctetString:
						{
							CV_VT(pvtOutValue, ValueTagType::CVT_BITSTR);
							uchar *pusVal = new uchar[iValSize];
							memset(pusVal, 0, iValSize);
							CV_BITSTR(pvtOutValue, pusVal, iValSize);
							delete [] pusVal;		
						}
						break;
					//Each character set to space  (61804-3, table D.9 PASSWORD, EUC, VISIBLE, OCTET)
					case vT_Password:
					case vT_EUC:
					case vT_VisibleString:
						{
							CV_VT(pvtOutValue, ValueTagType::CVT_WSTR);
							InitializeStringToCharacter(_T(' '),iValSize,&(CV_WSTR(pvtOutValue)));
						}
						break;
					}
				}

				//save default value
				vtDefaultVal = *pvtOutValue;
			}
		}
		else
		{
			*pvtOutValue = vtDefaultVal;
		}
	}
	return;
}
//
//
//
int hCVar :: getValue(CValueVarient& newValue)
{
	int iRetVal = BLTIN_VAR_NOT_FOUND;
	
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		PARAM_REF opRef;
		opRef.addOperationalID(getID(), 0, iT_Variable);

		iRetVal = pDevice->devAccessTypeValue2(&opRef, &newValue, READ);		
	}

	if (iRetVal != BLTIN_SUCCESS)
	{
		newValue.clear();
	}

	return iRetVal;
}


//
//
//
unsigned int hCVar::VariableSize(void)
{
	unsigned int nRetVal = itemSize;

	if (itemSize == 0)
	{
		//if itemSize is unknown, get it from DDS
		CValueVarient vtSize{};
		if( ReadParameterData( &vtSize, DCI_PROPERTY_SIZE ) ) 
		{
			nRetVal = (unsigned int)vtSize;
			itemSize = nRetVal;
		}
	}

	return nRetVal;
}

//
//
//
RETURNCODE hCVar::getMinMaxList(hCRangeList& retList)
{
	RETURNCODE rc = SUCCESS;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	CValueVarient vtMaxValue{};
	CValueVarient vtMinValue{};

	retList.clear();
	CValueVarient vtType{};
	CValueVarient vtSize{};
	ReadParameterData( &vtSize, DCI_PROPERTY_SIZE );
	ReadParameterData( &vtType, DCI_PROPERTY_TYPE );

	CValueVarient vtMinCount{};
	ReadParameterData( &vtMinCount, DCI_PROPERTY_MIN_COUNT );

	hCRangeItem tItm;
	
	if( (int)vtMinCount > 0 )
	{
		for (int i=0; i<(int)vtMinCount; i++)
		{		
			int iRetVal_min = pDevice->ReadParameterData2( &vtMinValue, DCI_PROPERTY_MIN_VALUE, 0, 0, this->itemId, i );
			if (iRetVal_min == BLTIN_SUCCESS)
			{
				//min value
				tItm.minVal = vtMinValue;				
			}
			int iRetVal_max = pDevice->ReadParameterData2( &vtMaxValue, DCI_PROPERTY_MAX_VALUE, 0, 0, this->itemId, i );
			if (iRetVal_max == BLTIN_SUCCESS)
			{
				//max value
				tItm.maxVal = vtMaxValue;				
			}

			retList[i] = tItm;
		} //end of for loop
	}
	else
	{
		//set min/max default values
		switch( (int)vtType )
		{
		case vT_FloatgPt:
			vtMinValue = -FLT_MAX;
			vtMaxValue = FLT_MAX;
			break;

		case vT_Double:
			vtMinValue = -DBL_MAX;
			vtMaxValue = DBL_MAX;
			break;

		case vT_Unsigned:
		case vT_Enumerated:
		case vT_BitEnumerated:
		case vT_Index:
		case vT_Time:
		case vT_DateAndTime:
		case vT_Duration:
			vtMinValue = 0;
			switch ((int)vtSize)
			{
			case 1:
				vtMaxValue = UINT8_MAX;
				break;
				case 2:
				vtMaxValue = UINT16_MAX;
				break;
				case 3:
				case 4:
				vtMaxValue = UINT32_MAX;
				break;
			default:
                vtMaxValue = ULLONG_MAX;
				break;
			}
           break;

		case vT_Boolean:
			vtMinValue = FALSE;
			vtMaxValue = FALSE;
			break;

		case vT_Integer:
		case vT_HartDate:
			switch ((int)vtSize)
			{
			case 1:
				vtMinValue = INT8_MIN;
				vtMaxValue = INT8_MAX;
				break;
				case 2:
				vtMinValue = INT16_MIN;
				vtMaxValue = INT16_MAX;
				break;
				case 3:
				case 4:
				vtMinValue = INT32_MIN;
				vtMaxValue = INT32_MAX;
				break;
			default:
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
				break;
			}
			break;

		case vT_TimeValue:
			if( (int)vtSize == 4)
			{
				vtMinValue = 0;
				vtMaxValue = UINT32_MAX;
			}
			else
			{
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
			}
		
		default:
			if( (int)vtSize > 4 )
			{
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
			}
			else
			{
                //TODO:LINUX:VERIFY
                vtMinValue = INT32_MIN;//LONG_MIN;
                vtMaxValue = INT32_MAX;//LONG_MAX;
			}
			break;
		}

		//min value
		tItm.minVal = vtMinValue;

		//max value
		tItm.maxVal = vtMaxValue;

		retList[0] = tItm;
	}

	return rc;
}

//
//
//
RETURNCODE hCNumeric :: ReadForEdit(wstring &editStr)
{
	CValueVarient vtValue{};
	if( ReadParameterData( &vtValue, DCI_PROPERTY_EDIT_FORMAT ) ) 
	{
		editStr = (wstring)vtValue;
	}
	return SUCCESS;
}

//
//
// This function returns numeric variable display format string
RETURNCODE hCNumeric :: ReadForDisp(wstring &editStr)
{
	CValueVarient vtValue{};
	if( ReadParameterData( &vtValue, DCI_PROPERTY_DISPLAY_FORMAT ) ) 
	{
		editStr = (wstring)vtValue;
	}
	return SUCCESS;
}

//
//
//This function returns variable static read-only attribute
bool hCVar::IsReadOnly(void)
{
	bool bRetVal = true;

	CValueVarient vtHandling{};
	if(ReadParameterData( &vtHandling, DCI_PROPERTY_HANDLING ) ) 
	{
		if( ((int)vtHandling & handlingWrite) == handlingWrite )
		{
			bRetVal = false;
		}
	}
	if( bRetVal )//if the variable is read only, then check it to see if it is a class local variable
	{
		CValueVarient vtClass{};
		if( ReadParameterData( &vtClass, DCI_PROPERTY_CLASS ) ) 
		{
			if( ((int)vtClass & maskLocal) == maskLocal )
			{
				bRetVal = false;//dont make class local variables read-only to methods
			}
		}
	}

	return bRetVal;
}

//
//
//This function returns variable dynamic VALIDITY attribute
bool hCVar::IsValid(void)
{
	//Acording to 61804-3 spec, section 7.36.16 states
	//"The VALIDITY attribute specifies whether an element is valid or invalid. An element without a VALIDITY is always valid."
	bool bRetVal = true;

	CValueVarient vtValidity{};
	if(ReadParameterData( &vtValidity, DCI_PROPERTY_VALIDITY ) )
	{
		bRetVal = (bool)vtValidity;	//0 == false; 1 == true.
	}

	return bRetVal;
}

//
//
//
RETURNCODE hCVar::getUnitString(wstring & str)
{
	RETURNCODE  rc = SUCCESS;
	
	CValueVarient vtUnit{};
	if( ReadParameterData( &vtUnit, DCI_PROPERTY_UNITS  ) ) 
	{
		str = (wstring)vtUnit;
	}
	return rc;
}/*End getUnitString()*/

//
//
//
RETURNCODE hCVar::Help(wstring &retStr)
{
	RETURNCODE  rc = SUCCESS;

	CValueVarient vtLabel{};
	if( ReadParameterData( &vtLabel, DCI_PROPERTY_HELP  ) ) 
	{
		retStr = (wstring)vtLabel;
	}
	return rc;
}

//
//
//
RETURNCODE hCVar::Label(wstring& retStr)
{
	RETURNCODE  rc = SUCCESS;

	CValueVarient vtLabel{};
	if( ReadParameterData( &vtLabel, DCI_PROPERTY_LABEL  ) ) 
	{
		retStr = (wstring)vtLabel;			 
	}
	return rc;
}

//
// set stuff
//

// This function is only used for parser or interpreter to set value by variable name.
// This function can not be used to set value by DD item complex reference.
int hCVar::setValue(CValueVarient& newValue)
{
	int iRetVal = BLTIN_CANNOT_READ_VARIABLE;


	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
			PARAM_REF opRef;
			opRef.addOperationalID(getID(), 0, iT_Variable);

			iRetVal = pDevice->devAccessTypeValue2(&opRef, &newValue, WRITE);
	}
	else
	{
		iRetVal = BLTIN_VAR_NOT_FOUND;
	}

	return iRetVal;
}


//
//
//
bool hCVar::IsDynamic(void)//the only class the counts			
{		
	bool bRetVal = false;

	CValueVarient vtClass{};
	if( ReadParameterData( &vtClass, DCI_PROPERTY_CLASS  ) ) 
	{
		if( ((int)vtClass & maskDynamic) == maskDynamic )
		{
			bRetVal = true;
		}
	}

	return bRetVal;
}


RETURNCODE  hCVar::Add(BYTE *byteCount, BYTE **data, ulong mask, int& len)
{	
	return SUCCESS;
}



///////////////////////////////////////////////////////////////////////////////////
//
//
//  HCNUMERIC
//
//
//
///////////////////////////////////////////////////////////////////////////////////




/// CNumeric
//		wstring constant_units,		// if no units then wstring is empty
//		
//		float min_value,			// if no min value then set to -inf
//			max_value,			   	// if no min value then set to +inf
//			scaling_factor;			// if no scaling factor then set to 1.00

//*%hCNumeric::hCNumeric(aCvarType* pVarType) 
hCNumeric::hCNumeric(DevInfcHandle_t h, aCitemBase* paItemBase) : hCVar(h,paItemBase)
{
}
hCNumeric::hCNumeric(DevInfcHandle_t h, variableType_t vt, int vSize) : hCVar(h,vt,vSize)
{
}

double hCNumeric::GetScalingFactor()
{
	double fRetVal = 1.0;

	CValueVarient vtScalingFactor{};
	if( ReadParameterData( &vtScalingFactor, DCI_PROPERTY_SCALING_FACTOR  ) ) 
	{
		fRetVal = (double)vtScalingFactor;
	}

	return fRetVal;
}

/* from-typedef constructor */
hCNumeric::hCNumeric(hCNumeric* pSrc, hCNumeric* pVal,  itemIdentity_t newID) : 
           hCVar((hCVar*)pSrc,newID)
{	
}



/*************************************************************************************************
 *  the hCinteger Base Class
 *		                             ie hCunsigned:hCinteger:hCnumeric:hCvar:hCitembase:hCobject
 *************************************************************************************************
 */

hCinteger::hCinteger(DevInfcHandle_t h, aCitemBase* paItemBase, bool isS)
		 :hCNumeric(h,paItemBase)
{
}


hCinteger::hCinteger(hCinteger*  pSrc,  hCinteger*  pVal, itemIdentity_t newID) : hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID)
{
}


bool hCinteger :: isChanged(void)
{
	return false;
}


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCFLOAT
//
//
///////////////////////////////////////////////////////////////////////////////

hCFloat::hCFloat(DevInfcHandle_t h) : hCNumeric(h,vT_FloatgPt,FLT_CONST_SIZE)
{
}

hCFloat::hCFloat(DevInfcHandle_t h, aCitemBase* paItemBase) :hCNumeric(h,paItemBase)
{
}
hCFloat::hCFloat(hCFloat*  pSrc, hCFloat* pVal, itemIdentity_t newID):
         hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID)
{
}

bool hCFloat::isChanged(void)
{
	return false;
}




////////////////////////////////////////////////////////////////////////////////
//
//
//    HCDOUBLE
//
//
///////////////////////////////////////////////////////////////////////////////

hCDouble::hCDouble(DevInfcHandle_t h, aCitemBase* paItemBase) : hCNumeric(h,paItemBase)
{
}

hCDouble::hCDouble(hCDouble*   pSrc,  hCDouble*   pVal, itemIdentity_t newID) : hCNumeric((hCNumeric*)pSrc,(hCNumeric*)pVal,newID)
{
}

bool hCDouble::isChanged(void)
{
	return false;
}






hCUnsigned::hCUnsigned(DevInfcHandle_t h, aCitemBase* paItemBase) : hCinteger(h,paItemBase,false)
{
	varType = vT_undefined;
}

//
//
//
int hCUnsigned::VariableType(void)
{
	int nRetVal = varType;

	if (varType <= vT_undefined)
	{
		//if itemSize is unknown, get it from DDS
		CValueVarient vtType{};
		if( ReadParameterData( &vtType, DCI_PROPERTY_TYPE ) ) 
		{
			nRetVal = (int)vtType;
			varType = (variableType_t)nRetVal;
		}
	}

	return nRetVal;
}


hCSigned::hCSigned(DevInfcHandle_t h, aCitemBase* paItemBase) : hCinteger(h,paItemBase,true)
{
}

hCenumDesc::hCenumDesc( const hCenumDesc& hCed )
		:hCobject(hCed.devHndl()), descS(hCed.devHndl()),helpS(hCed.devHndl()),
		 func_class(hCed.devHndl()),actions(hCed.devHndl())
{
	val = hCed.val;	    descS = hCed.descS;	helpS      = hCed.helpS;
	func_class   = hCed.func_class;			actions    = hCed.actions;
	status_class = hCed.status_class;		oclasslist = hCed.oclasslist;
//	m_pComm      = hCed.m_pComm;
}

// set this from abstract
void  hCenumDesc::setEqual(void* pAdesc)
{
	if (pAdesc==NULL) 
	{
		LOGIT(CERR_LOG,"ERROR: enum descriptor set equal to NULL.\n");
		return;
	}
	aCenumDesc* paEd = (aCenumDesc*)pAdesc;

	val = paEd->val;
	status_class = paEd->status_class;
	if (paEd->oclasslist.size() > 0 )
	{
		oclasslist.clear();
		oclasslist.reserve(paEd->oclasslist.size());
		oclasslist.assign(paEd->oclasslist.begin(),paEd->oclasslist.end());
	}
	descS.setEqual(&(paEd->descS));
	helpS.setEqual(&(paEd->helpS),DEFAULT_STD_DICT_HELP);
	func_class.setEqual(&(paEd->func_class));
	actions = paEd->actions;
}; 	                   

void  hCenumDesc::setDuplicate(hCenumDesc* pED)
{
	val = pED->val;
	descS.duplicate(&(pED->descS),false);// does not use 'replicate'
	helpS.duplicate(&(pED->helpS),false);
	func_class.duplicate(&(pED->func_class),false);// does not use 'replicate'
	actions.duplicate(&(pED->actions), false);//ref
	
	status_class = pED->status_class;

	if (pED->oclasslist.size() > 0 )
	{/* no complex types, just copy it */
		oclasslist = pED->oclasslist;
	}
	else
	{
		oclasslist.clear();
	}
}; 


tchar* hCenumList::GetStringValue( const UINT32 val)   // NULL on failure
{
	szReturnValue[0]=0;
	 	// find a match
	vector<hCenumDesc>::iterator edIT;

	for (edIT = begin(); edIT < end(); edIT ++)  // simple linear search
	{		
		if ( edIT->val == val )
		{
			wcsncpy( szReturnValue, edIT->descS.procureVal().c_str(), 1024 );
			break;
		}		
	}
	return szReturnValue;
}

void  hCenumList::append(hCenumList* pList2Append)
{
	insert(end(),pList2Append->begin(),pList2Append->end());
}

bool hCenumList::BuildFromEnumXML(wstring sXml)
{
	bool bRetVal = false;
	try
	{
		pugi::xml_document xmlDoc;
		pugi::xml_parse_result result = xmlDoc.load_string(sXml.c_str(), pugi::parse_trim_pcdata);
		if (!result)
		{
			return bRetVal;
		}

		CStdString strXPathSelection;
		strXPathSelection.Format(_T("/%s/%s/%s"), ELMTNAME_ENUM_DATA, ELMTNAME_DD_ITEMS, ELMTNAME_ENUM_SELECTION);

		pugi::xpath_query enumSectionquery(strXPathSelection.GetBuffer());
		pugi::xpath_node_set enumSelections = enumSectionquery.evaluate_node_set(xmlDoc);

		for (pugi::xpath_node_set::const_iterator itr = enumSelections.begin(); itr != enumSelections.end(); ++itr)
		{
			hCenumDesc localEnumDesc(devHndl());

			pugi::xpath_node xPathNode = *itr;
			wstring sDescription = xPathNode.node().attribute(ATTRNAME_DESCRIPTION).value();
			localEnumDesc.descS.setStr(sDescription);
			
			localEnumDesc.val = xPathNode.node().attribute(ATTRNAME_NUMBER).as_int();
			push_back(localEnumDesc);
		}
		bRetVal = true;
	}
	catch(...)
	{
		bRetVal= false;
	}
	return bRetVal;
}
void  hCenumList::setEqual(void* pAclass)
{
	// assume pAclass points to a aCenumList
	if (pAclass == NULL) return;//nothing to do

	aCenumList* paL = (aCenumList*)pAclass;
	hCenumDesc wrkEnumDesc(devHndl());

	for(AenumDescList_t::iterator iT = paL->begin(); iT<paL->end(); iT++)
	{ 
//		if (iT != NULL)
		{
			wrkEnumDesc.setEqual((void *)&iT);
			push_back(wrkEnumDesc);
			wrkEnumDesc.clear();
		}
//		else
//		{
//			cerr <<"ERROR: received a aCenumDesc ptr."<<endl;
//		}
	} 
}

void  hCenumList::duplicate(hCpayload* pPayLd, bool replicate)
{
	// assume pPayLd points to a hCenumList
	if (pPayLd == NULL) return;//nothing to do

	hCenumList* paL = (hCenumList*)pPayLd;
	hCenumDesc wrkEnumDesc(devHndl());

	FOR_p_iT(enumDescList_t, paL)
	{// iT is ptr 2 a hCenumDesc 
		wrkEnumDesc.setDuplicate((hCenumDesc *)&iT); // no return status
		
		push_back(wrkEnumDesc);

		wrkEnumDesc.clear();		
	}// next
}


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCENUM
//
//
///////////////////////////////////////////////////////////////////////////////



RETURNCODE hCEnum::procureList( hCenumList& returnList) // get the resolved list
{	
	RETURNCODE rc = BLTIN_NOT_IMPLEMENTED;

	// stevev 03mar06 - get rid of huge memory leak by always resolving into an empty list
	if (returnList.size() )
	{
		returnList.clear();// (resolve cond MUST append to the list!!)
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		if( pDevice->ReadEnumerationData( this->itemId, 0, returnList ) )
		{
			rc = BLTIN_SUCCESS;
		}
	}
	return rc;
};

hCEnum::hCEnum(DevInfcHandle_t h, aCitemBase* paItemBase) : hCinteger(h, paItemBase, false), Resolved(h)
{
}


hCEnum::hCEnum( hCEnum* pSrc, hCEnum* pVal, itemIdentity_t newID) : hCinteger( (hCinteger*)pSrc, (hCinteger*)pVal, newID ), Resolved( pSrc->devHndl() )
{
}


// evaluates and then a value for a given enum wstring
RETURNCODE hCEnum::procureValue(const wstring& keyStr, UINT32& val)
{
	RETURNCODE rc = FAILURE;
	val = 0xffffffff;

	if ( SUCCESS == procureList(Resolved) && Resolved.size() > 0 )
	{//  get the match
	 	// find a match
		vector<hCenumDesc>::iterator edIT;

		for (edIT = Resolved.begin(); edIT < Resolved.end(); edIT ++)  // simple linear search
		{		
			if ( edIT->descS.procureVal(/*pComm*/) == keyStr )
			{
				val = edIT->val;
				rc = SUCCESS;
			}		
		}
	}
	return rc;
}


// evaluates and then retrieves wstring for value
RETURNCODE hCEnum::procureString( const UINT32 val, wstring& mtchStr)
{
	RETURNCODE rc = BLTIN_NOT_IMPLEMENTED;
	mtchStr.erase();// returns empty on failure

	if ( BLTIN_SUCCESS == procureList(Resolved) && Resolved.size() > 0 )
	{//  get the match		
		tchar fmt[1024+1] = {0};	
		wcsncpy( fmt, Resolved.GetStringValue( val ), 1024 );	// concatenate wstring value onto the end of the array

		if( wcslen( fmt ) == 0 )
		{
			mtchStr = L"Undefined";
			rc = BLTIN_SUCCESS;
		}
		else
		{
			mtchStr = fmt;
			rc = BLTIN_SUCCESS;
		}
	}
	//else leave it FAILURE
	return rc;
}

/* stevev 3/19/04 - added to support enums as indexes in E&H DDs */
int hCEnum::getAllValidValues(UIntList_t& returnList)// returns count in list
{
	return 0;
}
/* end 3/19/04 method */


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCBITENUM
//
//
///////////////////////////////////////////////////////////////////////////////
// get by mask - NOTE: mask is NOT verified for inRange!

// get by mask
CValueVarient hCBitEnum::getValue(ulong indexMask)
{
	CValueVarient retVal{};
	int iRetVal = getValue(retVal);// must be an int(hCinteger's getRaw..)
	if ( (iRetVal == BLTIN_SUCCESS) && retVal.vIsValid )
	{
		int y = ((int)retVal) & indexMask;
		retVal.vType = CValueVarient::isBool;
		if ( y )
		{
			retVal.vValue.bIsTrue = true;
		}
		else
		{
			retVal.vValue.bIsTrue = false;
		}
	}
	else
	{
		retVal.clear();
	}
	return retVal;	
}

//
//
//
int hCBitEnum::setValue(CValueVarient& newValue, ulong indexMask)
{
	int iRetVal = BLTIN_SUCCESS;

	unsigned int y = (unsigned int)newValue;
	unsigned int z, w;
	CValueVarient varVal{};
	iRetVal = getValue(varVal);// must be an int
	
	if (iRetVal == BLTIN_SUCCESS)
	{
		z = (unsigned int)varVal;

		w = z & ~indexMask;		// clear the intended bit(s)
		w = w | (y & indexMask);// set the passed-in intended bit(s)

		// consolodate activity into setValue stevev 22apr05
		varVal = w;
		iRetVal = hCinteger::setValue(varVal);
	}

	return iRetVal;
};	

//
// evaluates and then retrieves wstring for value
//
RETURNCODE hCBitEnum::procureString( const UINT32 val, wstring& mtchStr)
{
	RETURNCODE rc = BLTIN_NOT_IMPLEMENTED;
	mtchStr.erase();// returns empty on failure
	
	if( val == 0 )
	{
		mtchStr = L"0x0000, Undefined";
		rc = BLTIN_SUCCESS;
	}
	else if ( BLTIN_SUCCESS == procureList(Resolved) && Resolved.size() > 0 )
	{//  get the match		
		tchar fmt[1024+1] = {0};
		UINT32 nVal = val;
		size_t nIndex = 0;
		size_t nSize = Resolved.size();
		for(nIndex =0; nIndex < nSize; nIndex ++)
		{
			if ((*(std::vector<hCenumDesc,std::allocator<hCenumDesc> >*)(&Resolved))[nIndex ].val == nVal)                                                // check current vallue to see if status is active
			{
                swprintf (fmt, 1024, L"%s", Resolved.GetStringValue(nVal));
				break;
			}
		}

		if( wcslen( fmt ) == 0 )
		{
            swprintf (fmt, 1024, L"0x%04X, Undefined", val);

		}
		mtchStr = fmt;

		rc = BLTIN_SUCCESS;

	}//else leave it FAILURE
	
	return rc;
}

	
hCBitEnum::hCBitEnum(DevInfcHandle_t h, aCitemBase* paItemBase):hCEnum(h,paItemBase)
{	
}


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCINDEX
//
//
///////////////////////////////////////////////////////////////////////////////


//=================================

hCindex::hCindex(DevInfcHandle_t h, aCitemBase* paItemBase) : hCinteger( h, paItemBase, false )
{
	pIndexed = new hCattrVarIndex( h, itemId );
}

/*  NOTE: this has to point to the same array, value must be the closest valid value */
hCindex::hCindex(hCindex* pSrc, hCindex* pVal, itemIdentity_t newID) : hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID)
{
	pIndexed = NULL;
}

hCindex::~hCindex()
{
	if( pIndexed )
	{
		delete pIndexed;
		pIndexed = NULL;
	}
}

int hCindex::getAllValidValues(UIntList_t& returnList)// returns count in list
{
	return 0;
}

//This function returns indexed element descrition based on input element value
RETURNCODE hCindex::procureString( const UINT32 nCurrentValue, wstring& mtchStr)
{
	RETURNCODE				rc = BLTIN_NOT_IMPLEMENTED;
	hCitemBase*				pIB = NULL;
	hCitemArray*			pIndxdArr = NULL;
	hCgroupItemDescriptor*	pGID = NULL;
	UIntList_t				uintList;

	mtchStr.erase();								// returns empty on failure
    rc = pIndexed->getArrayPointer(pIB);			// find point to the ARRAY OF COLLECTION
	if(	(rc == SUCCESS) && (pIB != NULL) )
	{
		pIndxdArr = (hCitemArray*)pIB;
		if ( pIndxdArr->getIType() == iT_ItemArray)	//iT_Array,iT_List
		{
			pIndxdArr->getAllindexValues(uintList);
			if(uintList.size())
			{
				unsigned int	uIndxVal;
				unsigned long	elementValue = 0;
				for(UIntList_t::iterator it = uintList.begin(); it < uintList.end();it++ )
				{
					uIndxVal = (*it);
					rc = pIndxdArr->getByIndex(uIndxVal, &pGID);	//get pointer to the element of ARRAY OF COLLECTION
					if( (SUCCESS == rc) && (pGID != NULL) )
					{
						//get element value
						elementValue = pGID->getIIdx();
						if( elementValue == nCurrentValue )
						{
							hCddlString     tmpDDlStr(pGID->getDesc());
							if (tmpDDlStr.noString())
							{
								char tmp[64];
								sprintf(tmp,"%d", elementValue);
								string stmp(tmp);
								mtchStr = AStr2TStr(stmp);
							}
							else
							{
								mtchStr = tmpDDlStr.procureVal();
							}
							
							RAZE(pGID);	// alloc'd in getByIndex
							break;
						}
					}
					RAZE(pGID);			// alloc'd in getByIndex
				}
			}
		}
	}

	return rc;
}


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCBITSTRING
//
//
///////////////////////////////////////////////////////////////////////////////

hCBitString::hCBitString(DevInfcHandle_t h, aCitemBase* paItemBase):hCVar(h,paItemBase)
{
}

////////////////////////////////////////////////////////////////////////////////
//
//
//    HCASCII
//
//
///////////////////////////////////////////////////////////////////////////////

//*% hCascii::hCascii(aCvTascii* pActual)/*,Cvariable* pV)*/:hCVar((aCvarType*)pActual)//,pV)
hCascii::hCascii(DevInfcHandle_t h, aCitemBase* paItemBase):hCVar(h,paItemBase)
{
}

hCascii::hCascii(hCascii* pSrc,  hCascii* pVal, itemIdentity_t newID):
      hCVar((hCVar*)pSrc,newID)
{
}

bool hCascii::isChanged(void)
{
	return true;
};



////////////////////////////////////////////////////////////////////////////////
//
//
//    HCPASSWORD
//
//
///////////////////////////////////////////////////////////////////////////////

hCpassword::hCpassword(DevInfcHandle_t h, aCitemBase* paItemBase):hCascii(h,paItemBase)
{
}
hCpassword::hCpassword(hCpassword* pSrc, hCpassword* pVal, itemIdentity_t newID):
            hCascii((hCascii*)pSrc,(hCascii*)pVal,newID)
{
}

//*% hCpackedAscii::hCpackedAscii(aCvTpackedascii* pActual)/*,Cvariable* pV)*/:hCascii((aCvTascii*)pActual)//,pV)
hCpackedAscii::hCpackedAscii(DevInfcHandle_t h, aCitemBase* paItemBase):hCascii(h,paItemBase)
{
}
hCpackedAscii::hCpackedAscii(hCpackedAscii* pSrc, hCpackedAscii* pVal, itemIdentity_t newID):
               hCascii((hCascii*)pSrc,(hCascii*)pVal,newID)
{
}


bool hCenumList::operator== (/*const*/ hCenumList& src)
{
	bool bRetVal = false;
	if (size() != src.size())
	{
		return false;
	}
	 // compare 'em
	FOR_this_iT(enumDescList_t,this)
	{// iT is ptr 2a hCenumDesc
		vector<hCenumDesc>::iterator edIT;

		for (edIT = begin(); edIT < end(); edIT ++)  // simple linear search
		{		
			if ( edIT->val == iT->val )
			{
				bRetVal = true;
			}		
		}
		// else - check the next one
	}
	return bRetVal;
}



////////////////////////////////////////////////////////////////////////////////
//
//
//    HCASCII
//
//
///////////////////////////////////////////////////////////////////////////////

bool hCascii::IsPassword(void)
{   
	return(VariableType() == vT_Password);
};


////////////////////////////////////////////////////////////////////////////////
//
//
//    HCHARTDATE
//
//
///////////////////////////////////////////////////////////////////////////////



//
//
//
hChartDate::hChartDate(DevInfcHandle_t h, aCitemBase* paItemBase)
		/* init base class */
		:hCinteger(h,paItemBase,false)
{
	//editFormat = usFormat; // default to english format
}

//
//
//
hChartDate::hChartDate(hChartDate* pSrc, hChartDate* pVal, itemIdentity_t newID):
         hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID)
{
	//editFormat = usFormat; // default to english format
}


//////////////////////////////////////////////////////////////////////////////////////////
// Name: setValue																	//
// Description: Sets the hChartDate value
//////////////////////////////////////////////////////////////////////////////////////////

int hChartDate :: setValue(CValueVarient& value)
{
	int iRetVal = BLTIN_SUCCESS;
	CValueVarient vtValue{};


	if( value.vType == CValueVarient::isIntConst )
	{
        vtValue = (int)value;//(long)(int)value; //TODO:LINUX:FIXIT
	}

	// Set the date value in the server
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		PARAM_REF opRef;
		opRef.addOperationalID(getID(), 0, iT_Variable);

		iRetVal = pDevice->devAccessTypeValue2(&opRef, &vtValue, WRITE);
	}
	else
	{
		iRetVal = BLTIN_VAR_NOT_FOUND;
	}

	return iRetVal;
}


/*<START>Added by ANOOP 03FEB2004	For validating the date before committing for a date variable */
bool hChartDate :: ValidateDate(const INT32 LocVal )
{
	bool			bRetVal;
	INT32			nLocalVal;

	bRetVal = false;	//failed
	
	UINT16 d,m,y;
	y = LocVal         & 0xFF;
	m = (LocVal >>  8) & 0xFF;
	d = (LocVal >> 16) & 0xFF;

	nLocalVal = y + 1900;
	y = nLocalVal % 100;

	//  valid number has nothing to do with the formatting
	if ( (m <= 12 && m >= 1 ))
	{
		if( (d <=31 && d >=1 ))
		{
			bRetVal = true;		//success
		}
	}

	return bRetVal;

}
/*<END>Added by ANOOP 03FEB2004	For validating the date before committing for a date variable 	*/



/*************************************************************************************************
 *
 *   $History: ddbVar.cpp $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */
 
