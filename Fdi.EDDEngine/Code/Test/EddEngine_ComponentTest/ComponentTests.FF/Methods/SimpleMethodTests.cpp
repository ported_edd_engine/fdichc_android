#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"
#include "TestSupport/Helper.h"
#include "TestSupport/AsyncCallbackHandler.h"
#include <time.h>

#define make_time_t( a)    ( (long)   (a) )	/* returns Htime_t, a long */

using namespace std;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_running_test_method_empty_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator = new BuiltinSupportSimulator();
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            { 
                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_empty", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, pValueSpec, &methodAction, pCallback, asyncState,languageCode);

                methodResult= engine->Instance->EndMethod( & asyncRunningMethod );

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {     
                delete builtinSupportSimulator;            
                delete engine;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_display_message_simple_with_FF 
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
			static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);
                
                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_simple", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, pValueSpec, &methodAction, pCallback, asyncState,languageCode);

                methodResult= engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Hello EDDL!),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_display_message_with_label_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // set up BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                pValue->size = 1;
                pValue->type = VT_INTEGER;
                pValue->val.i = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_label", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult =  engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
                delete pValue;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_GetParamValue_at_least_once()
            {
                Assert::IsTrue( builtinSupportSimulator->GetParamValueCallCount==0, "GetParamValue Call Count: " + builtinSupportSimulator->GetParamValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Label is test variable 1),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_display_message_with_unit_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // set up BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                pValue->size = 1;
                pValue->type = VT_INTEGER;
                pValue->val.i = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_unit", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_block_item_ID;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_InfoRequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_GetParamValue_at_least_once()
            {
                Assert::IsTrue( builtinSupportSimulator->GetParamValueCallCount==0, "GetParamValue Call Count: " + builtinSupportSimulator->GetParamValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Unit is unit1),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_display_message_with_varids_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                setlocale(LC_ALL, ""); // TODO: workaround for locale settings of EDD Engine

                // set up BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                pValue->size = 1;
                pValue->type = VT_INTEGER;
                pValue->val.i = 5;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_varids", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
 				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
				nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult=engine->Instance->EndMethod(&asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_block_item_ID;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_InfoRequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_GetParamValue_at_least_once()
            {
                Assert::IsTrue( builtinSupportSimulator->GetParamValueCallCount>=1, "GetParamValue Call Count: " + builtinSupportSimulator->GetParamValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
               Assert::AreEqual("GetParamValue,GetParamValue,GetParamValue,InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // for some reason, parameter values are retrieved twice or thrice, depending on the syntax in the EDD file � see bug #569
                Assert::AreEqual(
                    "GetParamValue(1,00000000,131092),GetParamValue(1,00000000,131093),"
                    "GetParamValue(1,00000000,131075),"
                    "InfoRequest(var1: 5 � var2: 5 � var3: 5),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_acknowledge_simple_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // setup BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                Helper::SetAcknowledgeFunction(builtinSupportSimulator, BSEC_SUCCESS);

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_acknowledge_simple", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult=engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_AcknowledgementRequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->AcknowledgementRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("AcknowledgementRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("AcknowledgementRequest(Hello EDDL!\n),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_acknowledge_with_varids_with_FF 
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static FLAT_METHOD* test_method;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                setlocale(LC_ALL, ""); // TODO: workaround for locale settings of EDD Engine

                // setup BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                Helper::SetAcknowledgeFunction(builtinSupportSimulator, BSEC_SUCCESS);

                EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                pValue->size = 1;
                pValue->type = VT_INTEGER;
                pValue->val.i = 5;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_acknowledge_with_varids", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
 				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult= engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_AcknowledgementRequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->AcknowledgementRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_GetParamValue_at_least_once()
            {
                Assert::IsTrue( builtinSupportSimulator->GetParamValueCallCount>=1, "GetParamValue Call Count: " + builtinSupportSimulator->GetParamValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("GetParamValue,GetParamValue,AcknowledgementRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // for some reason, parameter values are retrieved twice or thrice, depending on the syntax in the EDD file � see bug #569
                Assert::AreEqual(
                    "GetParamValue(1,00000000,131092),GetParamValue(1,00000000,131093),"
                    "AcknowledgementRequest(var1: 5 � var2: 5),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_select_from_list_simple_with_FF
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // setup BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                Helper::SetSelectionRequestFunction(builtinSupportSimulator, BSEC_SUCCESS, 2);

                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_select_from_list_simple", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult= engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_block_item_ID;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_SelectionRequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->SelectionRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("SelectionRequest,InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("SelectionRequest(What do you want?,Option 1;Option 2;Option 3),InfoRequest(3),OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_delay_simple_with_FF 
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator = new BuiltinSupportSimulator();
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase0009\\TestCase0009.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_delay_simple", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult=engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("DelayMessageRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("DelayMessageRequest(2,delay!),"
                    "OnMethodExiting(0)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_read_value_from_TestCase000D_with_FF 
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator = new BuiltinSupportSimulator();
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase000D\\TestCase000D.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_read_value", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult=engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_readValue_once()
            {
                Assert::AreEqual(1, builtinSupportSimulator->ReadValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }
        };

        [TestClass]
        public ref class When_running_the_test_method_write_value_from_TestCase000D_with_FF 
        {
        public:
            static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator = new BuiltinSupportSimulator();
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initalize EDD Engine
                engine = gcnew EddEngineController("FF\\TestCase000D\\TestCase000D.ff6");
                engine->Initialize(nullptr, builtinSupportSimulator);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_write_value", &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult=engine->Instance->EndMethod( & asyncRunningMethod );
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_writeValue()
            {
                Assert::AreEqual(1, builtinSupportSimulator->WriteValueCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }
        };

        [TestClass]
        public ref class When_calling_a_scaling_function_that_causes_an_error
        {
        protected:
            static EddEngineController^ engine;
            static bool exceptionThrown = false;
            static nsEDDEngine::Mth_ErrorCode executionSuccess = METH_FAILED;
            static BuiltinSupportSimulator *pBuiltinSupport = nullptr;
            static EVAL_VAR_VALUE *valToScale;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                ITEM_ID *test_block_item_ID = new ITEM_ID();
                String^ eddFileName = System::IO::Path::GetFullPath("FF\\TestCase0011\\TestCase0011.ff6");
                engine = gcnew EddEngineController(eddFileName);

                try
                {
                    ParamCacheSimulator* paramCacheSimulator = new ParamCacheSimulator();
                    pBuiltinSupport = new BuiltinSupportSimulator();
                    valToScale = new EVAL_VAR_VALUE();
                    valToScale->size = 4;
                    valToScale->type = VariableType::VT_INTEGER;
                    valToScale->val.i = 42;

                    engine->Initialize(paramCacheSimulator, pBuiltinSupport);
                    paramCacheSimulator->SetEddEngine(engine->Instance);

                    // create new function Block
                    engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                    nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                    pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                    pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                    pFieldbusBlockInfo->m_usObjectIndex = 1000;
                    int iBlockInstance; // out
                    const int iCount = 1;
                    engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                
                    wchar_t* langCode = L"en";

                    FDI_PARAM_SPECIFIER test_method_specifier;
                    engine->FillParamSpecifier("aborting_method", &test_method_specifier);

					ACTION methodAction; 
					methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
					methodAction.action.meth_ref = test_method_specifier.id;
                    nsEDDEngine::IAsyncResult* callResult = engine->Instance->BeginVariableActionMethod(iBlockInstance, nullptr, valToScale, &methodAction, nullptr, nullptr, langCode);

                    executionSuccess = engine->Instance->EndVariableActionMethod(&callResult,  valToScale);
                }
                catch (...)
                {
                    exceptionThrown = true;
                }

            }

            [ClassCleanup]
            static void CleanupBase()
            {
                if(pBuiltinSupport != nullptr) delete pBuiltinSupport;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_not_crash()
            {
                Assert::IsFalse(exceptionThrown);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_inform_the_caller_of_the_abortion()
            {
                Assert::IsFalse(executionSuccess == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_OnMethodExiting()
            {
                Assert::IsTrue(pBuiltinSupport->Recorder->WasCalled(L"OnMethodExiting"));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_set_eTermActivity_to_Discard()
            {
                Assert::AreEqual((int) nsEDDEngine::ChangedParamActivity::Discard,(int) *((nsEDDEngine::ChangedParamActivity*) pBuiltinSupport->Recorder->GetParameter(L"OnMethodExiting.eTermAction")));
            }
        };


		[TestClass, Ignore]
        public ref class BaseClassCurrentDate
        {
		private:
			static ITEM_ID* test_block_item_ID;
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupport;
			static EVAL_VAR_VALUE *expectedSet, *providedValue;

        public:

            static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE expectedValue)
            { 
				expectedSet = new EVAL_VAR_VALUE(expectedValue);
                builtinSupport = new BuiltinSupportSimulator();
                builtinSupport->SetParamValueReturnValue = PC_SUCCESS_EC;

				// initalize EDD Engine
				engine = gcnew EddEngineController(eddFileName);
                engine->Initialize(nullptr, builtinSupport);

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
				methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, pValueSpec, &methodAction, pCallback, asyncState,languageCode);

                engine->Instance->EndMethod(&asyncRunningMethod);
				providedValue = (EVAL_VAR_VALUE*)builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue");

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void CleanupBase()
            {     
                if (builtinSupport) delete builtinSupport;
                if (expectedSet) delete expectedSet;           
                if (engine) delete engine;
                if(test_block_item_ID) delete test_block_item_ID;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void The_provided_value_for_SetParamValue_should_have_the_expected_type()
            {
                Assert::AreEqual((int)expectedSet->type, (int)providedValue->type);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void The_provided_value_for_SetParamValue_should_have_the_expected_size()
            {
                Assert::AreEqual(expectedSet->size, providedValue->size);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void The_provided_value_for_SetParamValue_should_have_the_expected_value()
            {
                Assert::AreEqual((int)expectedSet->val.i, (int)providedValue->val.i);
            }
        };

		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentDate_in_Testcase_0009 : BaseClassCurrentDate
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				struct tm tm_value;
				_gmtime32_s(&tm_value, &ltime);

				// current date
				long curr_date = tm_value.tm_yday * 24 * 60 * 60;

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_date;

				SetupBase("FF\\TestCase0009\\TestCase0009.ff6", "meth_current_date", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};


		[TestClass, Ignore]
        public ref class BaseClassCurrentTime
        {
        private:
			static ITEM_ID* test_block_item_ID;
            static BuiltinSupportSimulator *builtinSupport;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE *expectedSet, *providedValue;
			static String^ theMethodName;

		public:
            static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE expectedValue)
            {
 				theMethodName = methodName;

				expectedSet = new EVAL_VAR_VALUE(expectedValue);
                builtinSupport = new BuiltinSupportSimulator();
                builtinSupport->SetParamValueReturnValue = PC_SUCCESS_EC;

				// initalize EDD Engine
                engine = gcnew EddEngineController(eddFileName);
                engine->Initialize(nullptr, builtinSupport);

				// get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);

                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                engine->Instance->EndMethod(&asyncRunningMethod);
                providedValue = (EVAL_VAR_VALUE*)builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue");
            }

            static void CleanupBase()
            {
				if (builtinSupport) delete builtinSupport;
                if (expectedSet) delete expectedSet;           
                if (engine) delete engine;
                if (test_block_item_ID) delete test_block_item_ID;
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_type()
            {
                Assert::AreEqual((int)expectedSet->type, (int)providedValue->type);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_size()
            {
                Assert::AreEqual(expectedSet->size, providedValue->size);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_value()
            {
				int expectedVal = (int)expectedSet->val.i;
				int returnedVal = (int)providedValue->val.i;
				bool bCheck = ((returnedVal - expectedVal) <= 1);
				Assert::IsTrue(bCheck, ("DD test method " + theMethodName + " failed."));
            }
        };
		
		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentTime_in_Testcase_0009 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				struct tm tm_value;
				_gmtime32_s(&tm_value, &ltime);

				// current time in second
				long curr_time =  (tm_value.tm_hour * 60 + tm_value.tm_min) * 60 + tm_value.tm_sec;

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_time;

				SetupBase("FF\\TestCase0009\\TestCase0009.ff6", "meth_current_time", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};
		
		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentDateAndTime_in_Testcase_0009 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				// current date_time
				long curr_date_time = make_time_t( ltime );

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_date_time;

				SetupBase("FF\\TestCase0009\\TestCase0009.ff6", "meth_current_date_time", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

    }
}