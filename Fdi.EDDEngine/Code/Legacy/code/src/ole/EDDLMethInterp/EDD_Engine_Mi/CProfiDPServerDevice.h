#pragma once

#include "CMethodInterpreterDevice.h"
#include "Parser.h"

class CMethodThread;

class CProfibusDPServerDevice : public CMethodInterpreterDevice
{
	private:
		CMethodThread *m_pMethodThread;
		bool m_bSearchedForAutoSaveValues;
	public:
		CProfibusDPServerDevice( DevInfcHandle_t h, long lMethodID, const wchar_t* sLanguageCode, CMethodThread *pMethodThread );
		virtual ~CProfibusDPServerDevice();
		HRESULT Populate();
		INTERPRETER_STATUS ExecuteTheMethod();
		CStdString GetMethodLabel(){ return m_sMethodLabel; }
		CStdString GetAMSTag(){ return m_sAMSTag; }
		virtual bool ReadData( CStdString sValue, CValueVarient *pOutput);
		virtual bool ReadMethodData( long lItemId, CStdString sValue, CValueVarient *pOutput);
		virtual RETURNCODE ReadParameterData2( CValueVarient *pOutput, CStdString sValue, unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId );
		virtual bool ReadItemArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput);
		virtual RETURNCODE ReadParameterActionList( /*in*/ unsigned long ulBlkId, /*in*/ unsigned long ulBlkNum, /*in*/ unsigned long ulItemId, /*out*/ nsEDDEngine::ACTION_LIST *pOutput);
		virtual bool ReadValueArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput);
		virtual bool ReadCollectionData( long lItemId, CStdString sValue, CValueVarient *pOutput);
		virtual bool ReadFileData( long lItemId, CStdString sValue, CValueVarient *pOutput);
		virtual bool ReadEnumerationData( unsigned long ulItemId, unsigned long ulMemberId, hCenumList& returnList );
		virtual bool AddMapBasedOnName( CStdString sName );
		virtual bool AddMapBasedOnItemID( long lItemID );
		void		commitChangedValues(nsEDDEngine::ChangedParamActivity onExitActivity);
		virtual int ReadDictionaryString( wchar_t *sStringName, wchar_t *sValue, int iStringLen );



	public://overriden methods of hCddbDevice
		virtual RETURNCODE sendCommMethodCmd(int commandNumber, int transactionNumber, uchar* pucCmdStatus, uchar* pchMoreDataInfo, int& iMoreDataSize);
		virtual RETURNCODE getItemBySymName ( string& symName, hCitemBase** ppReturnedItempointer );

		virtual RETURNCODE AccessDynamicAttributeMethod(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType);

		virtual RETURNCODE sendListOpMethod(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount);
		virtual RETURNCODE sendUIMethod(ACTION_UI_DATA& stUIData);
		virtual RETURNCODE receiveUIMethodRsp(CValueVarient& vtRsp);
		virtual RETURNCODE get_rspcode_string( int nCommandNumber, int nResponseCode, tchar* pchResponseString, int nLength, nsEDDEngine::ProtocolType protocol);
		virtual RETURNCODE getDictionaryString(unsigned long ulIndex, wchar_t* pString, int iStringLen);
		virtual int ReadLiteralString( unsigned long ulStringID, wchar_t* sValue, long lStrLen );
		virtual bool IsDeviceCancelled();
		virtual void ClearDeviceCancel();

		virtual RETURNCODE getResponseCodeString(int iItemId, int iMemberId, int iRsponseCode, tchar *pchResponseCodeString, int iResponseCodeStringLength);
		virtual RETURNCODE getDdsError(tchar *pchErrorString, int iMaxLength);
		virtual unsigned long getCommError();
		virtual RETURNCODE sendParamValues(const PARAM_REF *pOpRef, AccessType iAccessType);
		virtual RETURNCODE getResponseCode(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId);
		virtual RETURNCODE assignValue(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef);
		virtual RETURNCODE devAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, AccessType eAccessType);
		virtual RETURNCODE devAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /* in */ AccessType eAccessType);
		virtual RETURNCODE devWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal);
		virtual ITEM_ID ResolveBlockRef(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType);
		virtual long getBlockInstanceCount(unsigned long ulBlockId, unsigned long *pulCount);
		virtual long getBlockByObjectIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex);
		virtual long getBlockByBlockTag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex);
		virtual long getListElem2 (unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data);
		virtual void deviceLogMessage(int priorityVal, wchar_t *msg);
		virtual void deviceMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml);
		virtual int deviceIsOffline();
		virtual long deviceSendCommand(unsigned long commandId, long *commandError);
		virtual RETURNCODE devAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType);
		virtual RETURNCODE deviceGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString);
		virtual int GetMemberIdByName( CStdString sMemberName, unsigned long *pMemberId );

		virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0);


	private: //member variables
        //HWND m_hParentWnd;
		CStdString m_sBlockMoniker;
		CStdString m_sAMSTag;
		long m_nMethodItemID;
		CStdString m_sMethodLabel;
};
