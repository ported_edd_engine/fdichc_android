 /**
 *	@(#) $Id: evl_enum.c,v 1.2 1996/01/04 20:12:28 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  ENUM_VALUE_LIST, ENUM_VALUE, BIT_ENUM_STATUS, OUTPUT_STATUS_LIST, and
 *	OUTPUT_STATUS.
 */

#include "stdinc.h"

#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif



#ifdef PROCESS_IMPORTS
/*********************************************************************
 *
 *  Name:  rmemcpy
 *  ShortDesc:  Reverse memcpy.
 *
 *  Description:
 *		Reverse memcpy, allows overlapping of source and destination
 *		buffers.
 *
 *  Inputs:
 *      dest - pointer to the destination buffer.
 *      src  - pointer to the source buffer.
 *      size - number of bytes to move.
 *
 *  Outputs:
 *      dest - pointer to the destination buffer.
 *
 *  Returns:
 *      void
 *
 *  Author: steve beyerl
 *
 *********************************************************************/
static void
rmemcpy(
char           *dest,
char           *src,
size_t          size)
{

	char 	*dest_ptr;		/* temp ptr to dest */
	char 	*src_ptr;		/* temp ptr to src */

	dest_ptr = &dest[size];
	src_ptr = &src[size];

	while (size-- > 0) {
		*dest_ptr-- = *src_ptr--;
	}
}


/*********************************************************************
 *
 *  Name:  ddl_insert_enum
 *  ShortDesc:  Insert an ENUM_VALUE in an ENUM_VALUE_LIST
 *
 *  Description:
 *      ddl_insert_enum inserts an ENUM_VALUE in an ENUM_VALUE_LIST. It
 *      allocates space for the entire list plus the new item, copies
 *      the list and the new item into the new space and then frees the
 *      memory pointed to by the old list.
 *
 *  Inputs:
 *      enums (ENUM_VALUE_LIST)
 *      position (position in list for insertion)
 *      *insert_data (ENUM_VALUE to insert)
 *
 *  Outputs:
 *      enums->enums points to new list and old list is freed.
 *
 *  Returns:
 *      DDL_MEMORY_ERROR -  unable to allocate memory for new list
 *      DDL_SUCCESS - insertion was successful
 *
 *  Author: steve beyerl
 *
 ****************************************************************/
int
ddl_insert_enum(
ENUM_VALUE_LIST *enums,
unsigned short  position,
ENUM_VALUE     *insert_data)
{
	ENUM_VALUE     *tmp;	/* temporary store for an enum value */

	/**
	 * Extend the list if necessary
	 */

	if (enums->count >= enums->limit) {

		/**
		 * The list needs to be longer.  It is better to malloc and
		 * copy enums than to realloc and move enums.
		 */

		tmp = (ENUM_VALUE *) malloc((size_t) ((enums->limit + ENUM_INC)
				* sizeof *enums->list));

		if (!tmp) {
			ddl_free_enum_list(enums, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}

		/**
		 * Copy the old list into the new, leaving space for the new
		 * element.
		 */

		memcpy((char *) tmp, (char *) enums->list,
			(size_t) position * sizeof *tmp);
		memcpy((char *) &tmp[position + 1], (char *) &enums->list[position],
			(size_t) (enums->limit - position - 1) * sizeof *enums->list);

		/**
		 * Initialize new list elements.
		 */

		enums->limit += ENUM_INC;
		memset((char *) &tmp[enums->count + 1], 0,
			(size_t) (enums->limit - enums->count - 1) * sizeof(*enums->list));

		/**
		 * Free the old list.
		 */

		free((void *) enums->list);
		enums->list = tmp;
	}
	else {

		/**
		 * Empty elements do exist at the end of the list. Check to see
		 * if we need to move current list elements.
		 */

		if (position < enums->count) {

			/**
			 * Move the elements above "position" to "position +1".
			 * Insert the new enum. List elements below "position"
			 * are unchanged.
			 */

			rmemcpy((char *) &enums->list[position + 1],
				(char *) &enums->list[position],
				(size_t) (enums->limit - position - 1) * sizeof *enums->list);
		}
	}

	/**
	 * add the new enum to the list.  The list element pointed at by
	 * "position" should be empty.
	 */

	memcpy((char *) &enums->list[position], (char *) insert_data,
		sizeof *insert_data);

	enums->count++;
	return DDL_SUCCESS;
}

#endif



#ifdef HART
/*********************************************************************
 *
 *	Name: ddl_shrink_output_class
 *	ShortDesc: Condense a list of OUTPUT_STATUS's
 *
 *	Description:
 *		ddl_shrink_output_class reallocates the OUTPUT_STATUS_LIST to
 *		contain space for the OUTPUT_STATUS's currently being used.
 *
 *	Inputs:
 *		list: pointer to a OUTPUT_STATUS_LIST structure
 *
 *	Outputs:
 *		list: pointer to a condensed OUTPUT_STATUS_LIST structure
 *
 *	Returns:
 *		DDL_MEMORY_ERROR
 *		DDL_SUCCESS
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
static int
ddl_shrink_output_class(
OUTPUT_STATUS_LIST *list)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_OUTPUT_CLASS);
#endif

	if (!list) {
		return DDL_SUCCESS;
	}

	/**
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (!list->list) {
		ASSERT_DBG(!list->count && !list->limit);
		return DDL_SUCCESS;
	}

	/**
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (list->count == list->limit) {
		return DDL_SUCCESS;
	}


	/*
	 *	If count = 0, free the list.
	 *	If count != 0, shrink the list
	 */

	if ( list->count == 0 ) {
		ddl_free_output_class( list);
	}
	else {
		list->limit = list->count;

		list->list = (OUTPUT_STATUS *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof *list->list));

		if (!list->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_output_class
 *	ShortDesc: Free the list of OUTPUT_STATUS's
 *
 *	Description:
 *		ddl_free_output_class will free the list of OUTPUT_STATUS's in the
 *		OUTPUT_STATUS_LIST structure.
 *
 *	Inputs:
 *		list: pointer to the OUTPUT_STATUS_LIST structure
 *
 *	Outputs:
 *		list: pointer to the OUTPUT_STATUS_LIST with an empty list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
ddl_free_output_class(
OUTPUT_STATUS_LIST *list)
{

	if (list == NULL) {
		return;
	}

	/**
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
	}
	else {

		/**
		 * Free the list.
		 */

		free((void *) list->list);
		list->list = NULL;
	}

	list->count = 0;
	list->limit = 0;

}
#endif 	/* HART */



/*********************************************************************
 *
 *	Name: ddl_free_enum_list
 *	ShortDesc: Free the list of ENUM_VALUEs
 *
 *	Description:
 *		ddl_free_enum_list will check the ENUM_VALUE_LIST pointer and
 *		the list, if the are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero
 *
 *	Inputs:
 *		enum_list: pointer to the ENUM_VALUE_LIST structure
 *		dest_flag:
 *			FREE_ATTR - free the entire list
 *			CLEAN_ATTR - cleanup (ie. destroy) any malloc'd memory associated
 *			 with each element of the list.  The original list is left intact.
 *
 *	Outputs:
 *		enum_list: pointer to the list of ENUM_VALUE_LIST with an empty list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
ddl_free_enum_list(
ENUM_VALUE_LIST *enum_list,
uchar           dest_flag)
{

	ENUM_VALUE     *temp_enum;	/* temp ptr to a list of ENUM_VALUE's */
	int             inc;


	if (enum_list == NULL) {
		return;
	}

	if (enum_list->list == NULL) {

		ASSERT_DBG(!enum_list->count && !enum_list->limit);
		enum_list->count = 0;
		enum_list->limit = 0;
	}
	else {

		/**
		 * Free all STRINGs and REFERENCE_ITEMS allocated under
		 * REFERENCE and all OUTPUT_STATUSes
		 */

		temp_enum = enum_list->list;

		for (inc = 0; inc < enum_list->limit; inc++, temp_enum++) {
			ddl_free_string(&temp_enum->desc);
			ddl_free_string(&temp_enum->help);
#ifdef HART
			ddl_free_output_class(&temp_enum->status.oclasses);
#endif
		}

		if (dest_flag == FREE_ATTR) {

			/**
			 * Free the list of ENUM_VALUEs
			 */

			free((void *) enum_list->list);
			enum_list->list = NULL;
			enum_list->limit = 0;
		}
		else {
			
			/**
			 *	Initializing the list of enum values to 0.
			 *	It is OK to initialize "evaled","val","class","desc",and "help"
			 *	"status" and "actions"
			 */

			memset( (char *)enum_list->list, 0, (size_t)enum_list->limit * 
				sizeof( *enum_list->list));
		}
		enum_list->count = 0;
	}
}




/**********************************************************************
 *
 *	Name: ddl_shrink_enum_list
 *	ShortDesc: Shrink the list of ENUM_VALUEs
 *
 *	Description:
 *		ddl_shrink_enum_list reallocs the list of ENUM_VALUEs to contain
 *		only the ENUM_VALUEs being used
 *
 *	Inputs:
 *		enum_list: pointer to the ENUM_VALUE_LIST structure
 *
 *	Outputs:
 *		enum_list: pointer to the resized ENUM_VALUE_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		return codes from:
 *			ddl_free_enum_list()
 *			ddl_shrink_output_class()
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
int
ddl_shrink_enum_list(
ENUM_VALUE_LIST *enum_list)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_ENUM_LIST);
#endif

	if (enum_list == NULL) {
		return DDL_SUCCESS;
	}

	/**
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (enum_list->list == NULL) {
		ASSERT_DBG(!enum_list->count && !enum_list->limit);
		return DDL_SUCCESS;
	}

	/**
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (enum_list->count == enum_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 *	If count = 0, free the list.
	 *	If count != 0, shrink the list
	 */

	if ( enum_list->count == 0 ) {
		ddl_free_enum_list( enum_list, FREE_ATTR );
	}
	else {
		enum_list->limit = enum_list->count;

		enum_list->list = (ENUM_VALUE *) realloc((void *) enum_list->list,
			(size_t) (enum_list->limit * sizeof(ENUM_VALUE)));

		if (enum_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

/**********************************************************************
 *
 *	Name: ddl_parse_one_enum
 *	ShortDesc: Parse the binary for one  ENUM_VALUE
 *
 *	Description:
 *      ddl_parse_one_enum will parse the binary data chunk and load
 *      an ENUM_VALUE structure and call the corresponding parse
 *		routine to fill in the respective elements of the ENUM_VALUE
 *		structure.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		len  - pointer to the size of the binary
 *		type - pointer to an TYPEINFO structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		enum_val - pointer to an ENUM_VALUE structure, where the
 *				result will be stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		type - pointer to an TYPEINFO structure containing the result
 *		enum_val - pointer to an ENUM_VALUE structure containing the
 *				result will.
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			DDL_PARSE_INTEGER()
 *			ddl_parse_item_id()
 *			ddl_parse_string()
 *			ddl_parse_bitstring()
 *			DDL_PARSE_TAG()
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
int
ddl_parse_one_enum(
unsigned char **chunkp,
DDL_UINT       *len,
TYPEINFO       *type,
ENUM_VALUE     *enum_val,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed,
EnumMap        *pEnumMap)
{
	int             rc;	/* return code */
	DDL_UINT        tag;	/* identifier of a binary chunk */
	DDL_UINT        kind_oclass;	/* temp storage for a parsed integer */
	int  			value_desired;	/* TRUE if value is desired */
	DDL_UINT		tag_len;	/* length of binary assoc. w/ parsed tag */

#ifdef HART
	OUTPUT_STATUS  *tmp2;	/* temp ptr to a list of output status's */
	DDL_UINT        which;	/* temp storage for a parsed integer */
#endif


	if (type && type->tag == ENUMERATOR_SEQLIST_TAG && type->ptr) {
		value_desired = TRUE;
	}
	else {
		value_desired = FALSE;
	}

	while ( *len > 0 ) {
		DDL_PARSE_TAG(chunkp, len, &tag, &tag_len);

		switch (tag) {

		case ENUM_VALUE_TAG:

			/**
			 * Parse the value for the enumeration
			 */

			DDL_PARSE_INTEGER(chunkp, len, 
				value_desired ? &enum_val->val : (DDL_UINT *)NULL_PTR);
			if (value_desired) {
				enum_val->evaled |= ENUM_VAL_EVALED;
			}
			break;

		case ENUM_STATUS_TAG:

			/**
			 * Parse the status class information for the enumeration.
			 */

			*len -= tag_len;	/* adjust the size of the remaining binary */

			if (value_desired) {

				/**
				 * Parse the class, and the encoding of the kind and
				 * output class (in one byte).
				 */

				DDL_PARSE_INTEGER(chunkp, &tag_len, &enum_val->status.status_class);

				while( tag_len > 0 ) {

					/**
					 *	If we are in an ISP binary,
					 *	Space has been reserved for future expansion.
					 *	The integer to be parsed must be 0.
					 */


#ifdef HART
					/**
					 *	If we are in an HART binary,
					 *	KIND and OUTPUT_CLASS are combined in the same binary byte.
					 *	KIND is stored in bits 0-2 and OUTPUT_CLASS is stored in bits
					 *	3-7 of "kind_oclass".
					 */
#endif 

					DDL_PARSE_INTEGER(chunkp, &tag_len, &kind_oclass);

					if (kind_oclass == OC_NORMAL) {
						
						/**
						 * 	There should be nothing more to parse
						 */

						ASSERT_DBG( tag_len == 0 );
						break;	/* get out of this while loop */
					}

#ifdef HART
					/**
					 * Expand the output class list if necessary, and add
					 * the information about the kind, oclass, and which.
					 */

					if (enum_val->status.oclasses.count >=
						enum_val->status.oclasses.limit) {
						enum_val->status.oclasses.limit += OUTPUT_STATUS_LIST_INCSZ;
						tmp2 = (OUTPUT_STATUS *) realloc((void *)
							enum_val->status.oclasses.list,
							(size_t) enum_val->status.oclasses.limit * sizeof *tmp2);
						if (!tmp2) {
							enum_val->status.oclasses.limit =
								enum_val->status.oclasses.count;
							ddl_free_output_class(&enum_val->status.oclasses);
							return DDL_MEMORY_ERROR;
						}

						enum_val->status.oclasses.list = tmp2;
					}
					tmp2 = enum_val->status.oclasses.list +
						enum_val->status.oclasses.count;
					enum_val->status.oclasses.count++;

					tmp2->kind = (unsigned short) (kind_oclass & 7);
					tmp2->oclass = (unsigned short) (kind_oclass >> 3);
					tmp2->which = 0;

					/**
					 *	There are 4 types of KIND.
					 *		DV = dynamic variable.
					 *		TV = transmitter variable.
					 *		AO = analog output
					 *		ALL = all outputs
					 *
					 *	DV, TV and AO are followed by the integer "which" which
					 *	indicates the variable (or output) the output_class is 
					 *	associated with.
					 */

					if (tmp2->kind == OC_DV || tmp2->kind == OC_TV ||
							tmp2->kind == OC_AO) {
						DDL_PARSE_INTEGER(chunkp, &tag_len, &which);
						tmp2->which = (unsigned short) which;
					}
#endif

				}	/* end of while() */

#ifdef HART
				/**
				 * Shrink the output class list
				 */

				rc = ddl_shrink_output_class(&enum_val->status.oclasses);

				if (rc != DDL_SUCCESS) {
					return rc;
				}
#endif

				enum_val->evaled |= ENUM_STATUS_EVALED;
			}
			else {
				DDL_PARSE_INTEGER(chunkp, &tag_len, (DDL_UINT *) NULL_PTR);

				while( tag_len > 0 ) {

					/**
					 *	If we are in an ISP binary,
					 *	Space has been reserved for future expansion.
					 *	The integer to be parsed must be 0.
					 */

#ifdef HART
					/**
					 *	If we are in an HART binary,
					 *	KIND and OUTPUT_CLASS are combined in the same binary byte.
					 *	KIND is stored in bits 0-2 and OUTPUT_CLASS is stored in bits
					 *	3-7 of "kind_oclass".
					 */
#endif

					DDL_PARSE_INTEGER(chunkp, &tag_len, &kind_oclass);

					if (kind_oclass == OC_NORMAL) {
						
						/**
						 * 	There should be nothing more to parse
						 */

						ASSERT_DBG( tag_len == 0 );
						break;	/* get out of this while loop */
					}

#ifdef HART
					kind_oclass &= 7;
					if (kind_oclass == OC_DV || kind_oclass == OC_TV || 
							kind_oclass == OC_AO) {
						DDL_PARSE_INTEGER(chunkp, &tag_len, (DDL_UINT *) NULL_PTR);
					}
#endif
				}	/* end of while() */
			}
			break;

		case ENUM_ACTIONS_TAG:

			/**
			 * Parse the actions information for the enumeration.
			 */

			rc = ddl_parse_item_id(chunkp, len, 
				value_desired ? &enum_val->actions : (ITEM_ID *) NULL, 
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (value_desired) {
				enum_val->evaled |= ENUM_ACTIONS_EVALED;
			}
			break;

		case ENUM_DESC_TAG:

			/**
			 * Parse the description for the enumeration.
			 */

			rc = ddl_parse_string(chunkp, len, 
				value_desired ? &enum_val->desc : (STRING *) NULL, 
				depinfo, env_info, var_needed, pEnumMap);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			
			if (value_desired) {
				enum_val->evaled |= ENUM_DESC_EVALED;
			}
			break;

		case ENUM_HELP_TAG:

			/**
			 * Parse the help information for the enumeration
			 */

			rc = ddl_parse_string(chunkp, len, 
				value_desired ? &enum_val->help : (STRING *) NULL, 
				depinfo, env_info, var_needed, pEnumMap);

			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (value_desired) {
				enum_val->evaled |= ENUM_HELP_EVALED;
			}
			break;

		case ENUM_CLASS_TAG:

			/**
			 * Parse the functional class of the enumeration.
			 */

			rc = ddl_parse_bitstring(chunkp, len, 
				value_desired ? &enum_val->func_class : (DDL_UINT *) NULL);

			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (value_desired) { 
				enum_val->evaled |= ENUM_CLASS_EVALED;
			}
			break;

		default:
			
			/**
			 * Ignore any new fields.
			 */

			*chunkp += tag_len;
			*len -= tag_len;
			break;

		}
	}	/* end of while (len > 0) */

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_parse_enums
 *	ShortDesc: Parse the binary for an ENUM_VALUE_LIST
 *
 *	Description:
 *      ddl_parse_enums will parse the binary data chunk and load
 *		enum info into an ENUM_VALUE_LIST structure and dependency info
 *		into an OP_ref_list structure.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size  - pointer to the size of the binary
 *		type - pointer to an TYPEINFO structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		type - pointer to an TYPEINFO structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			DDL_PARSE_TAG()
 *			ddl_parse_one_enum()
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
static int
ddl_parse_enums(
unsigned char **chunkp,
DDL_UINT       *size,
TYPEINFO       *type,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed,
EnumMap        *pEnumMap)
{
	int             rc;	/* return code */
	DDL_UINT        len;	/* length of binary to be parsed */
	DDL_UINT        tag;	/* identifier of a binary chunk */
	ENUM_VALUE     *temp_enum = NULL;	/* temp ptr to list of enum
						 * values */
	ENUM_VALUE_LIST *enums = NULL;	/* pointer to the enum list structure */

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_ENUMS);
#endif


	while (*size > 0) {

		/**
		 * Parse the tag, and make sure it is an ENUMERATOR_TAG
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != ENUMERATOR_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		if (type && type->tag == ENUMERATOR_SEQLIST_TAG && type->ptr) {

			enums = (ENUM_VALUE_LIST *) type->ptr;

			/**
			 * Parse a series of ENUM_VALUEs.  If we need more room
			 * in the array of structures, malloc more room.  Then
			 * parse the next ENUM_VALUE structure.
			 */

			if (enums->count == enums->limit) {

				enums->limit += ENUM_INC;

				temp_enum = (ENUM_VALUE *) realloc((void *) enums->list,
					(size_t) enums->limit * sizeof(ENUM_VALUE));

				if (temp_enum == NULL) {
					enums->limit = enums->count;
					ddl_free_enum_list(enums, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_enum[enums->count], 0,
					(ENUM_INC * sizeof(ENUM_VALUE)));

				enums->list = temp_enum;
			}


			/**
			 * Parse the ENUM_VALUEs
			 */

			rc = ddl_parse_one_enum(chunkp, &len, type,
				&enums->list[enums->count], depinfo, env_info,
				var_needed, pEnumMap);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			/**
			 * 	If a HELP string was not parsed, get a default HELP string from the 
			 *	standard dictionary.
			 */

			if (!(enums->list[enums->count].evaled & ENUM_HELP_EVALED)) {
				rc = app_func_get_dict_string(env_info,DEFAULT_STD_DICT_HELP,
					&enums->list[enums->count].help);


				/* 
				 *	If the default HELP string was not found, get the default error 
				 *	string from the standard dictionary.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info,DEFAULT_STD_DICT_STRING,
						&enums->list[enums->count].help);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}

			enums->count++;

		}
		else {

			/**
			 * Parse the ENUM_VALUEs
			 */

			rc = ddl_parse_one_enum(chunkp, &len, type,
				(ENUM_VALUE *) NULL, depinfo, env_info,
				var_needed, pEnumMap);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}




/*********************************************************************
 *
 *	Name: ddl_enums_choice
 *	ShortDesc: Choose the correct list of enums from a binary.
 *
 *	Description:
 *		ddl_enums_choice will parse the binary for a list of enums,
 *		according to the current conditionals (if any).  The value of
 *		the enums is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		type - pointer to an TYPEINFO structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		type - pointer to an TYPEINFO structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_cond_list()
 *			ddl_parse_enums()
 *			ddl_parse_enums()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_enums_choice(
unsigned char **chunkp,
DDL_UINT       *size,
TYPEINFO       *type,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk
					 * list */

#ifdef ISPTEST
	TEST_FAIL(DDL_ENUMS_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	EnumMap myEnumMap;

	/**
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/**
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		ENUMERATOR_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/**
	 * A list of chunks was found and a value is requested.
	 */

	if ((type && type->ptr) && (chunk_list_ptr.size > 0)) {

		/**
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || ((type == NULL) ||
				(type->ptr == NULL)));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_enums(&(chunk_ptr->chunk), &(chunk_ptr->size),
				type, depinfo, env_info, var_needed, &myEnumMap);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if ((type && type->ptr) && data_valid) {
			*data_valid = TRUE;
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	// Cleanup EnumMap here
	for (EnumMap::iterator it = myEnumMap.begin(); it != myEnumMap.end(); it++)
	{
		FLAT_VAR *pVar = it->second;

		ddl_free_enum_list(&pVar->enums, FREE_ATTR);
		free(pVar);
		pVar = nullptr;
	}

	myEnumMap.clear();

	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}
