#include "stdafx.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "MEE.h"
#include "messageUI.h"


int	CBuiltIn:: SEND_COMMAND(
			
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int  iCmdType
			, bool bMoreDataFlag
			, int& iMoreInfoSize
			)
{
	int iRetVal = BI_SUCCESS; /*Return value of this function*/
	int iSendMethCmdRetCode = BI_SUCCESS; /*Return value from the SendMethCmd for NORMAL_COMMAND*/
	int iSendMethCmdRetCode48 = BI_COMM_ERR;/*Return value from the SendMethCmd for COMMAND_48*/
	bool bRetry=false; /*Main loop controlling var*/
	bool bRetryAsked=false;/*Flag set from the retry/abort masks for the method execution*/
	int iRetryCount = 0; /*Command Retry Counter*/
	int i=0;

	varPtrList_t itemList ;
	
	hCitemBase* pIB = NULL;
	CValueVarient ValueNeeded{};
	hCVar *pLocalVar = NULL;

	//int iRc;
	/*Initialize the ResponseStatus array*/

	pchResponseStatus[STATUS_RESPONSE_CODE] = 0;	//one byte integer: command response code
	pchResponseStatus[STATUS_COMM_STATUS]   = 0;	//bit enumeration: communication error
	pchResponseStatus[STATUS_DEVICE_STATUS] = 0;	//bit enumeration: field device status

	/*Initialize the MoreDataStatus (Cmd 48) array*/
/*Vibhor 120204: Start of Code*/

/*There was copy paste problem!! */

	pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
	pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
	pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;

/*And this initialization was missing */

	for(i = 0 ; i < MAX_XMTR_STATUS_LEN ; i++)
	{
		pchMoreDataInfo[i] = 0;
	}
	iMoreInfoSize = 0;

/*Vibhor 120204: End of Code*/
	do
	{
		bRetry = false;
		bRetryAsked = false;
		iRetVal = BI_SUCCESS;			//returned value after iSendMethCmdRetCode and pchResponseStatus value analysis

		if (iCmdType == NORMAL_CMD)
		{
			iSendMethCmdRetCode = m_pDevice->sendCommMethodCmd(iCommandNumber, iTransNumber, pchResponseStatus, NULL, iMoreInfoSize);

			//convert return code to one of 4 values
			switch (iSendMethCmdRetCode)
			{
			case BLTIN_SUCCESS:
			case BLTIN_FAIL_RESPONSE_CODE:	//==BSEC_FAIL_RESPONSE
			case BLTIN_FAIL_COMM_ERROR:		//==BSEC_FAIL_COMM
				iSendMethCmdRetCode = BI_SUCCESS;
				break;
			case BI_ABORT:					//==BSEC_ABORTED
			case BI_NO_DEVICE:				//==BSEC_NO_DEVICE
				//keep the return code here
				break;
			case BLTIN_WRONG_DATA_TYPE:		//==BSEC_WRONG_DATA_TYPE
			case BLTIN_NO_LANGUAGE_STRING:	//==BSEC_NO_LANGUAGE_STRING
			case BI_ERROR:					//==BSEC_OTHER
			default:
				iSendMethCmdRetCode = BI_COMM_ERR;
				break;
			}

			if ( iSendMethCmdRetCode != BI_SUCCESS )
			{	/*Command_Not_Implemented*/
				pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
				pchResponseStatus[STATUS_COMM_STATUS]   = 0;
				pchResponseStatus[STATUS_DEVICE_STATUS] = 0;
			}/*Endif iSendMethCmdRetCode != BI_SUCCESS*/
		}/*Endif NORMAL_CMD*/
		/*
		 * else pretend we sent the command so the more status command
		 * can be issued
		 */
		else
		{
				iSendMethCmdRetCode = BI_SUCCESS;

				pchResponseStatus[STATUS_RESPONSE_CODE] = 0;
				pchResponseStatus[STATUS_COMM_STATUS]   = 0;
				pchResponseStatus[STATUS_DEVICE_STATUS] = 0;
		}/*End else*/

		/* Command 48 or abort and retry processes*/
		if((iSendMethCmdRetCode == BI_SUCCESS))
		{	
			// If command 48 and direct builtin get_more_status() are allowed, do ...
			if ( (iCmdType == MORE_STATUS_CMD)															//builtin get_more_status()
					  || (bMoreDataFlag && (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_MORESTATUSAVAIL))		//indirect command 48
					  || (bMoreDataFlag && (pchResponseStatus[STATUS_DEVICE_STATUS] & DS_DEVMALFUNCTION)) )		//indirect command 48 based on HCF_SPEC-127
			{
				/*Send Command 48*/
				iSendMethCmdRetCode48 = m_pDevice->sendCommMethodCmd(48, DEFAULT_TRANSACTION_NUMBER, pchMoreDataStatus, pchMoreDataInfo, iMoreInfoSize);

				//convert return code to one of 4 values
				switch (iSendMethCmdRetCode48)
				{
				case BLTIN_SUCCESS:
				case BLTIN_FAIL_RESPONSE_CODE:
				case BLTIN_FAIL_COMM_ERROR:
					iSendMethCmdRetCode48 = BI_SUCCESS;
					break;
				case BI_ABORT:
				case BI_NO_DEVICE:
					//keep the return code here
					break;
				case BLTIN_WRONG_DATA_TYPE:
				case BLTIN_NO_LANGUAGE_STRING:
				case BI_ERROR:
				default:
					iSendMethCmdRetCode48 = BI_COMM_ERR;
					break;
				}

				if( iSendMethCmdRetCode48 != BI_SUCCESS )
				{	/*Command_Not_Implemented*/
					pchMoreDataStatus[STATUS_RESPONSE_CODE] = 0;
					pchMoreDataStatus[STATUS_COMM_STATUS]   = 0;
					pchMoreDataStatus[STATUS_DEVICE_STATUS] = 0;
					for(i = 0 ; i < MAX_XMTR_STATUS_LEN ; i++)
					{
						pchMoreDataInfo[i] = 0;
					}
					iMoreInfoSize = 0;
				}/*Endif iSendMethCmdRetCode48 != BI_SUCCESS*/

				/*If the direct or indirect command 48 or builtin get_more_status() went fine, check the returned status*/
				if (iSendMethCmdRetCode48 == BI_SUCCESS)
				{
					if (isCOMMAND48abortORretry(pchMoreDataStatus, pchMoreDataInfo, iMoreInfoSize, ABORT))
					{
						iRetVal = BUILTIN_COMM_FAIL_RESPONSE;
					}

					else if (isCOMMAND48abortORretry(pchMoreDataStatus, pchMoreDataInfo, iMoreInfoSize, RETRY))
					{
						bRetryAsked = true;
					}
				}/*Endif iSendMethCmdRetCode48 == BI_SUCCESS */
		
				/*Abort and retry processes*/
				else if (iSendMethCmdRetCode48 == BI_NO_DEVICE)
				{
					if(m_pMeth->m_byXmtrReturnNodevAbortMask)
					{
								
						iRetVal = BUILTIN_COMM_NO_DEVICE;
					}
					else if(m_pMeth->m_byXmtrReturnNodevRetryMask)
					{

						bRetryAsked = true;
					}
					else
					{ 
						iRetVal = BI_NO_DEVICE;
					}
				}/*Endif BI_NO_DEVICE*/

				else if (iSendMethCmdRetCode48 == BI_ABORT)
				{
					iRetVal = BUILTIN_COMM_ABORTED;
				}/*Endif BI_ABORT*/

				else
				{
					iRetVal = BI_COMM_ERR;
				}
			}/*Endif Send Command 48*/
			
			/*If the direct command went fine, check the returned status first*/
			if ( (iRetVal == BUILTIN_COMM_ABORTED) 
			  || (iRetVal == BUILTIN_COMM_FAIL_RESPONSE)
			  || (iRetVal == BUILTIN_COMM_NO_DEVICE))
			{        
				// builtin aborts immediately
			}

			else if ( (m_pMeth->m_byRespAbortMask[BYTENUM(pchResponseStatus[STATUS_RESPONSE_CODE])]
					   & (1 << BITNUM(pchResponseStatus[STATUS_RESPONSE_CODE])))
					  || (pchResponseStatus[STATUS_COMM_STATUS] & m_pMeth->m_byCommAbortMask)
				      || (pchResponseStatus[STATUS_DEVICE_STATUS]	& m_pMeth->m_byStatusAbortMask)
					)
			{
				iRetVal = BUILTIN_COMM_FAIL_RESPONSE;
			}

			else if ( (!bRetryAsked) && 
					  ((m_pMeth->m_byRespRetryMask[BYTENUM(pchResponseStatus[STATUS_RESPONSE_CODE])]
					    & (1 << BITNUM(pchResponseStatus[STATUS_RESPONSE_CODE])))
					   || (pchResponseStatus[STATUS_COMM_STATUS] & m_pMeth->m_byCommRetryMask)
					   || (pchResponseStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byStatusRetryMask))
					)
			{
				bRetryAsked = true;
			}

		}/*Endif iSendMethCmdRetCode == BI_SUCCESS*/

		else if(iSendMethCmdRetCode == BI_NO_DEVICE)
		{
			if(m_pMeth->m_byReturnNodevAbortMask)
			{	
				iRetVal = BUILTIN_COMM_NO_DEVICE;
			}
			else if(m_pMeth->m_byReturnNodevRetryMask)
			{
				bRetryAsked = true;
			}
			else
			{ 
				iRetVal = BI_NO_DEVICE;
			}
		}/*Endif iSendMethCmdRetCode == BI_NO_DEVICE*/

		else if (iSendMethCmdRetCode == BI_ABORT)
		{
			iRetVal = BUILTIN_COMM_ABORTED;
		}/*Endif BI_ABORT*/

		else
		{
			iRetVal = BI_COMM_ERR;
		}

		//check retry limit
		if(bRetryAsked == true)
		{
			if(iRetryCount < m_pMeth->m_iAutoRetryLimit)
			{
				iRetryCount++;
				bRetry = true;
			}
			else
			{
				iRetVal = BUILTIN_RETRY_FAILED;
				break;
			}
		}

	} while (bRetry == true);

	switch(iRetVal)
	{
	case BUILTIN_COMM_ABORTED:
	case BUILTIN_RETRY_FAILED:
	case BUILTIN_COMM_NO_DEVICE:
	case BUILTIN_COMM_FAIL_RESPONSE:
		break;
	case BI_SUCCESS:
	case BI_COMM_ERR:
	case BI_NO_DEVICE:
	default:
		iRetVal = ConvertToBICode(iRetVal);
		break;
	}
	return iRetVal;

}/*End SEND_COMMAND*/

/* This function returns true meaning command 48 abort or returns false meaning command 48 not abort if the last argument is set to ABORT.
 * This function returns true meaning command 48 retry or returns false meaning command 48 not retry if the last argument is set to RETRY. */
bool CBuiltIn::isCOMMAND48abortORretry(uchar* pchMoreDataStatus, uchar* pchMoreDataInfo, int iMoreInfoSize, SM_ABORT_OR_RETRY eCheck)
{
	bool bRet = false;

	switch (eCheck)
	{
	case ABORT:
		{
			if ((m_pMeth->m_byXmtrRespAbortMask[BYTENUM(pchMoreDataStatus[STATUS_RESPONSE_CODE])]
				 & (1 << BITNUM(pchMoreDataStatus[STATUS_RESPONSE_CODE])))
				|| (pchMoreDataStatus[STATUS_COMM_STATUS] & m_pMeth->m_byXmtrCommAbortMask)
				|| (pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusAbortMask))
			{

				bRet = true;
			}

			if (!bRet)
			{
				for(int i = 0; i < iMoreInfoSize; i++)
				{
					if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataAbortMask[i])
					{
						bRet = true;
						break;
					}
				}
			}
		}
		break;
	case RETRY:
		{
			if ((m_pMeth->m_byXmtrRespRetryMask[BYTENUM(pchMoreDataStatus[STATUS_RESPONSE_CODE])]
				 & (1 << BITNUM(pchMoreDataStatus[STATUS_RESPONSE_CODE])))
				|| (pchMoreDataStatus[STATUS_COMM_STATUS] & m_pMeth->m_byXmtrCommRetryMask)
				|| (pchMoreDataStatus[STATUS_DEVICE_STATUS] & m_pMeth->m_byXmtrStatusRetryMask))
			{

				bRet = true;
			}

			if(!bRet)
			{
				for(int i = 0; i < iMoreInfoSize; i++)
				{
					if( pchMoreDataInfo[i] & m_pMeth->m_byXmtrDataRetryMask[i])
					{
						bRet = true;
						break;
					}
				}

			}
		}
		break;
	}

	return bRet;
}


int CBuiltIn::send(int cmd_number,uchar *cmd_status)
{
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size = 0;
	//Automatically issue command48, so bMoreDataFlag should be set to true
	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_status,more_status_data,
																				NORMAL_CMD,true,info_size);

}




int CBuiltIn::send_command(int cmd_number)
{
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_status,more_status_data,
							NORMAL_CMD,false,info_size);

}


int CBuiltIn::send_command_trans(int cmd_number,int iTransNumber)
{
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	//return SEND_COMMAND(cmd_number,iTransNumber,cmd_status,more_status,more_status_data,NORMAL_CMD,true);
	return SEND_COMMAND(cmd_number,iTransNumber,cmd_status,more_status,more_status_data,
																   NORMAL_CMD,false,info_size);
}


int CBuiltIn::send_trans(int cmd_number,int iTransNumber,uchar *pchResponseStatus)
{
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	//Automatically issue command48, so bMoreDataFlag should be set to true
	return SEND_COMMAND(cmd_number,iTransNumber,pchResponseStatus,more_status,more_status_data,
		                                                             NORMAL_CMD,true,info_size);

}

int CBuiltIn::ext_send_command(int cmd_number	, uchar *pchResponseStatus, 
								uchar *pchMoreDataStatus, uchar *pchMoreDataInfo, int& moreInfoSize)
{

	return SEND_COMMAND(cmd_number,DEFAULT_TRANSACTION_NUMBER,pchResponseStatus,pchMoreDataStatus,
		                                            pchMoreDataInfo,NORMAL_CMD,true,moreInfoSize);
	
}

int CBuiltIn::ext_send_command_trans
		(
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int&  moreInfoSize
		)
{

	return SEND_COMMAND(iCommandNumber, iTransNumber, pchResponseStatus,
							pchMoreDataStatus, pchMoreDataInfo, (int)NORMAL_CMD, true, moreInfoSize);
	
}

int CBuiltIn::tsend_command
		(
			int iCommandNumber
		)
{

	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	return SEND_COMMAND(iCommandNumber,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_status,
													more_status_data,NORMAL_CMD,false,info_size);
	

}

int CBuiltIn::tsend_command_trans
		(
			int iCommandNumber
			, int iTransNumber
		)
{
	uchar cmd_status[STATUS_SIZE];
	uchar more_status[STATUS_SIZE];
	uchar more_status_data[MAX_XMTR_STATUS_LEN];
	int  info_size;
	
	return SEND_COMMAND(iCommandNumber,iTransNumber,cmd_status,more_status,more_status_data,
																	NORMAL_CMD,false,info_size);
}


int CBuiltIn::get_more_status(uchar *more_data_status,uchar *more_data_info, int& moreInfoSize)
{
	
	uchar cmd_status[STATUS_SIZE];

	return SEND_COMMAND(0,DEFAULT_TRANSACTION_NUMBER,cmd_status,more_data_status,more_data_info,
																MORE_STATUS_CMD,true,moreInfoSize);
	
}
