/**
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_dds.h - Connection Manager DDS Structure Definitions
 *
 *	This file contains the structure definitions for cm_dds.cpp	that need to be included 
 *	by other files.
 */

#ifndef CM_DDS_H
#define CM_DDS_H
				
typedef struct {
	UINT32		magic_number;
	UINT32		header_size;
	UINT32		objects_size;
	UINT32		data_size;
	UINT32		manufacturer;
	UINT16		device_type;
	UINT8		device_revision;
	UINT8		dd_revision;
	UINT8		lit80_major_rev;
	UINT8		lit80_mminor_rev;
	UINT32		reserved1;
	UINT32		reserved2;
	UINT32		reserved3;
	UINT32		reserved4;
} DDOD_HEADER;

#endif CM_DDS_H
