
#pragma once
#include "IMiDataFinder.h"
#include "EDDEngineImpl.h"

typedef std::map<unsigned long, const wchar_t*> DEVICE_ERROR_MAP;	//used for fieldbus only

class CMiDataFinder : public IMiDataFinder
{
	private:

		CEDDEngineImpl* m_pEDDEngineImpl ;
		DEVICE_ERROR_MAP m_mDevErrCodeString;
	
	public:

		CMiDataFinder();
		~CMiDataFinder();

		void SetEDDEngineImpl( CEDDEngineImpl* pEDDEngineImpl);

		int ResolveNameToID( LPCTSTR, ITEM_ID *plItemId );//todo use ITEM_ID
		int GetValue( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, long lProperty, CValueVarient *pvtValue, const wchar_t *lang_code );
		nsEDDEngine::ITEM_TYPE GetItemType(int iBlockInstance, ITEM_ID lItemID);
		int GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType, ITEM_ID* pItemId); 
		ITEM_ID ResolveRefToID( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, long lIndex, const wchar_t *lang_code );
		int GetArrayElementLabel( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, long lIndex, CValueVarient *pvtValue, const wchar_t *lang_code );
		
		// This GetResponseCodeString takes command number and response code to get response code string  
		// This should be used for HART builtin
		int GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol, const wchar_t *lang_code );

		// This GetResponseCodeString takes item id, member id and response code to get response code string  
		// This should be used for FF builtin
		int GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, ITEM_ID lMemberId, int nResponseCode, CValueVarient *pvtValue, const wchar_t *lang_code );

		int GetEnumVarString(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString, const wchar_t *lang_code);
		int GetVarInfo( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, nsEDDEngine::FLAT_VAR** pVar, const wchar_t *lang_code);  
		int GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
		int GetDictionaryString( wchar_t* wsDictName,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
		int GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
		int ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex);
		int LookupMemberId ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, SUBINDEX ulSubIndex, const wchar_t *lang_code,
							 /* out */ITEM_ID* pulMemberId );
		
		// For FF CrossBlock BuiltIns
		int GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence);
		int GetBlockInstanceByTag(ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence);
		int GetBlockInstanceCount( ITEM_ID iItemId, int* piCount);
		int ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance);
		bool ConvertEXPRtoVARIANT(nsEDDEngine::EXPR *pSrc, CValueVarient *pvtDest);
		int GetItemsBetweenReferenceItems( /*in*/nsEDDEngine::BLOCK_INSTANCE iBlockInstance, /*in*/void* pValueSpec, /*in*/const wchar_t *lang_code,
										   /*in*/ITEM_ID ulRefItemId,
										   /*in*/const unsigned short usMaxArrayElem,
										   /*in, out*/nsEDDEngine::ITEM_TYPE peItemType[], /*in, out*/ITEM_ID pulItemId[], /*in, out*/ITEM_ID pulMemberId[],
										   /*out*/unsigned short* pArrayElemNum );

		wchar_t* GetErrorString(int iErrorNum);
		ITEM_ID resolveBlockRef( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulMemberId, ResolveType eResolveType, const wchar_t *lang_code);
		int GetActionList(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, nsEDDEngine::ACTION_LIST *pOutput, const wchar_t *lang_code);
	protected:
		int GetMethodDefinition( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetMethodParams( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetParameterType( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetParameterSize( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetKindFromItemId( int iBlockInstance, ITEM_ID lItemID, CValueVarient *pvtValue );
		int GetNameFromItemID(  ITEM_ID lItemID, CValueVarient *pvtValue );
		int GetParameterHandling( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetParameterValidity( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		long GetKindFromID( int iBlockInstance, ITEM_ID lItemID );
		int GetDDLString( CValueVarient *pvtValue );
		int GetDefaultValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetLabel( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetEnumXMLdata( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetHelp( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetClassFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetEditFormatFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetDisplayFormatFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetElementCount( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetMinMaxCount( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetMinValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetMaxValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetIndexItemArrayId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetUnits( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetNumberOfElements( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetTypeReferenceID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetItemBase(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, nsEDDEngine::AttributeNameSet *pAttrList, nsEDDEngine::FDI_GENERIC_ITEM* pGenericItem, const wchar_t *lang_code);
		int GetScalingFactor( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetAxisScaling( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetCapacity( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetIndexedValueList( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetXAxisItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetYAxisItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );
		int GetLength( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code );

		void convertOPRefTrailToParamSpec(const int iBlockInstance, const nsEDDEngine::OP_REF_TRAIL *pSrcValue, nsEDDEngine::FDI_PARAM_SPECIFIER *pDstValue);

		void loadFFDeviceErrorMap();
		int GetFFDeviceCodeString(unsigned long ulPackedCode, CValueVarient *pvtValue, const wchar_t *lang_code);
};
