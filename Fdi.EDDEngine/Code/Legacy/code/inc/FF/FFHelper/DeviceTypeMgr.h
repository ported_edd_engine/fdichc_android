#pragma once

#include "cm_lib.h"
#include "AMSDds/table.h"
#include "cm_rod.h"
#include "rmalloc.h"

#include "RodMgr.h"
#include <mutex>

//ClockDeviceTypeMgr is used to make the shared data in DeviceTypeMgr thread-safe.
//Use the function by declaring a local variable in sections of code that need to be protected:
//For example, CLockTypeDeviceTypeMgr dtLockDeviceTypeMgr;
//The code makes the functions threadsafe by locking a private member variable m_cs in the DeviceTypeMgr.
class CLockDeviceTypeMgr 
{
public:
	CLockDeviceTypeMgr();
	~CLockDeviceTypeMgr();
};

//global DeviceTypeMgr class

class CDeviceTypeMgr
{
	friend class CLockDeviceTypeMgr;

public:
	CDeviceTypeMgr();
	~CDeviceTypeMgr();

	/*Active Device Type Table Functions*/
	int valid_device_type_handle(DEVICE_TYPE_HANDLE dth);

	DD_DEVICE_ID* ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth);
	void SET_ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID* ddi);
	void SET_ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth, ROD_HANDLE rod_handle);
	void* ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth);

		/* Function Prototypes for Active Device Type Information */

	int get_adtt_count (void);
	int get_adtt_dd_device_id (DEVICE_TYPE_HANDLE, DD_DEVICE_ID *);
	int get_adtt_dd_handle (DEVICE_TYPE_HANDLE, ROD_HANDLE *);
	int get_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE, void **);
	int get_adtt_sym_info (DEVICE_TYPE_HANDLE, SYMINFO **);
	int get_adtt_usage (DEVICE_TYPE_HANDLE, int *);

	int set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE, void *);
	int set_adtt_dd_device_id (DEVICE_TYPE_HANDLE, DD_DEVICE_ID *);
	int set_adtt_dd_handle (DEVICE_TYPE_HANDLE, ROD_HANDLE);
	int set_adtt_sym_info (DEVICE_TYPE_HANDLE, SYMINFO *);
	int set_adtt_usage (DEVICE_TYPE_HANDLE, int);

	DEVICE_TYPE_HANDLE ct_device_type_search(DD_DEVICE_ID *dd_device_id);
	int ct_new_device_type(DEVICE_TYPE_HANDLE *device_type_handle);
	void ct_free_device_type( ENV_INFO* env_info, DEVICE_TYPE_HANDLE device_type_handle);

	ROD_HANDLE ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth);

	SYMINFO* ADTT_SYM_INFO (DEVICE_TYPE_HANDLE dth);
	int ADTT_USAGE(DEVICE_TYPE_HANDLE dth);

	//Rod wrapper functions
	int			valid_rod_handle( ROD_HANDLE ) ;
	unsigned long rod_get_version(ROD_HANDLE rhandle);
	int rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);	
	int ds_ddod_file_load (wchar_t *filename, ROD_HANDLE *rod_handle);
	int rod_close(ROD_HANDLE rod_handle);

private:

	std::recursive_mutex		m_cs;
	CRodMgr						m_RodMgr;
	ACTIVE_DEV_TYPE_TBL			active_dev_type_tbl;
};

//global shared device type manager
extern CDeviceTypeMgr g_DeviceTypeMgr;
