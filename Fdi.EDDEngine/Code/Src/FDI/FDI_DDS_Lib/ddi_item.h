//
//	@(#) $Id: ddi_item.h,v 1.1 2012/07/10 20:59:40 rgretta Exp $
//	Copyright 1993 Rosemount, Inc. - All rights reserved
//

#ifndef DDI_ITEM_H
#define DDI_ITEM_H

#ifndef DDLDEFS_h
#include "ddldefs.h"
#endif

#ifndef EVL_RET_H
#include "evl_ret.h"
#endif


/*
 * This is the Generic DDI Item
 */

typedef struct tag_DDI_GENERIC_ITEM {


    ITEM_TYPE   item_type;   /* the type of item */

    void        *item;       /* a pointer to the item */

    RETURN_LIST errors;      /*
                              * list of errors encountered
                              * during the last evaluation
                              */
} DDI_GENERIC_ITEM;







#endif
