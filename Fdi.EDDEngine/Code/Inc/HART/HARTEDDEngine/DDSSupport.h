
//#include "ddi_lib.h"
//#include <cm_dds.h>
//#include <map_dict.h>
//#include <langspec.h>
//#include <StandardDictionary.h>
#include "ConnectionMgr.h"
#include "nsConsumer/IEDDEngineLogger.h"

class IDDSSupport
{

public:
	virtual int DDS_GetParamValue( ENV_INFO *env_info, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value) = 0;
 	virtual CConnectionMgr* GetConnectionManager() = 0;
	virtual void DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory) = 0;

};
