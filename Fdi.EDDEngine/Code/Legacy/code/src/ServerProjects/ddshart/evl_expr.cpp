/**
 *	@(#) $Id: evl_expr.c,v 1.3 1996/01/04 20:12:30 kimwolk Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions for the expression evaluator.
 *	These functions operate on the EXPR data structure.
 */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
#include <limits>
using namespace std;
#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif

/*
 * Defines
 */

 /* Check for valid type range */
#define VALID_EXPR_TYPE(T)	((T) >= INTEGER && (T) <= INDEX)

#define	EVAL_STACK_SIZE	32	/* Size of the EVAL_STACK */


/*
 * Typedefs
 */

typedef struct {		/* Stack used for evaluating expressions */
	EXPR           *top;
	EXPR            data[EVAL_STACK_SIZE];
}               EVAL_STACK;



/*********************************************************************
 *
 *	Name: ddl_pop
 *	ShortDesc: Pop an operand off of the stack.
 *
 *	Description:
 *		ddl_pop will pop the top operand off of the stack.
 *
 *	Inputs:
 *		stack - pointer to the stack
 *
 *	Outputs:
 *		operand - pointer to the place where you wish to store
 *					the operand.
 *		stack - pointer to the modified stack
 *
 *	Returns:
 *		DDL_SUCCESS - if something is on the stack
 *		DDL_ENCODING_ERROR - if the stack is empty
 *
 *	Also:
 *		ddl_push(3)
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_pop(
EXPR           *operand,
EVAL_STACK     *stack)
{
	EXPR           *top;

#ifdef ISPTEST
	TEST_FAIL(DDL_POP);
#endif

	if (stack->top == stack->data) {
		return DDL_ENCODING_ERROR;
	}

	top = --(stack->top);

	operand->type = top->type;
	operand->size = top->size;
	operand->val = top->val;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_convert_type
 *	ShortDesc: Convert two expressions to a common type and size
 *
 *	Description:
 *		ddl_convert_type will take two expressions and convert them
 *		to the type and size of the "higher" type and/or bigger size.
 *
 *	Inputs:
 *		op1 - pointer to expression 1
 *		op2 - pointer to expression 2
 *
 *	Outputs:
 *		op1 - pointer to (possibly modified) expression 1
 *		op2 - pointer to (possibly modified) expression 2
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE - if either of the expressions is not a numeric
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_convert_type(
EXPR           *op1,
EXPR           *op2)
{
	register EXPR  *lowop;	/* points to the "lower" typed expression */
	register EXPR  *highop;	/* points to the "higher" typed expression */
	register unsigned int size1, size2;	/* size of op1 & op2 */

	ASSERT_DBG(op1 && op2);

#ifdef ISPTEST
	TEST_FAIL(DDL_CONVERT_TYPE);
#endif

	if (!VALID_EXPR_TYPE(op1->type) || !VALID_EXPR_TYPE(op2->type)) {
		return DDL_BAD_VALUE_TYPE;
	}

	size1 = op1->size;
	size2 = op2->size;

	ASSERT_DBG(size1 <= 8);
	ASSERT_DBG(size2 <= 8);

	/*
	 * If the two expressions have the same type, make sure they have the
	 * same size, and exit.
	 */

	if (op1->type == op2->type) {
		if (size1 > size2) {
			op2->size = size1;
		}
		else {
			op1->size = size2;
		}
		return DDL_SUCCESS;
	}

	/*
	 * Find which expression has the "higher" type, then switch on that
	 * type so the other expression can be promoted to the higher one.
	 */

	if (op1->type > op2->type) {
		highop = op1;
		lowop = op2;
	}
	else {
		highop = op2;
		lowop = op1;
	}

	switch (highop->type) {
    case DOUBLEG_PT:

		/*
		 * Promote lowop to a double.
		 */

		switch (lowop->type) {
        case FLOATG_PT:
			lowop->val.d = (double) lowop->val.f;
			break;

		case UNSIGNED:
			lowop->val.d = (double) lowop->val.ull;
			break;

		case INTEGER:
			lowop->val.d = (double) lowop->val.ll;
			break;

        case DOUBLEG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
        lowop->type = DOUBLEG_PT;
		lowop->size = sizeof(lowop->val.d);
		break;

    case FLOATG_PT:

		/*
		 * Promote lowop to a float.
		 */

		switch (lowop->type) {
		case UNSIGNED:
			lowop->val.f = (float) lowop->val.ull;
			break;

		case INTEGER:
			lowop->val.f = (float) lowop->val.ll;
			break;

        case DOUBLEG_PT:
        case FLOATG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
        lowop->type = FLOATG_PT;
		lowop->size = sizeof(lowop->val.f);
		break;

	case UNSIGNED:
	case INTEGER:

		/*
		 * At this point one of the expressions is an UNSIGNED and the
		 * other is INTEGER.
		 */

		/*
		 * If the expressions are the same size, promote the integer to
		 * unsigned integer and exit.
		 */

		if (size1 == size2) {
			ASSERT_DBG(lowop->type == INTEGER);
			lowop->type = UNSIGNED;
			lowop->val.ull = (ULONGLONG) lowop->val.ll;
			return DDL_SUCCESS;
		}

		/*
		 * Change the smaller sized expression to the larger sized.
		 */

		if (size1 > size2) {
			highop = op1;
			lowop = op2;
		}
		else {
			highop = op2;
			lowop = op1;
		}

		switch (highop->type) {
		case UNSIGNED:
			lowop->val.ull = (ULONGLONG) lowop->val.ll;
			lowop->type = UNSIGNED;
			break;

		case INTEGER:
			lowop->val.ll = (LONGLONG) lowop->val.ull;
			lowop->type = INTEGER;
			break;

        case DOUBLEG_PT:
        case FLOATG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
		lowop->size = highop->size;
		break;

	default:
		return DDL_BAD_VALUE_TYPE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_expr_nonzero
 *	ShortDesc: Determines if an expression is non-zero.
 *
 *	Description:
 *		ddl_expr_nonzero takes an expression and determines if its value
 *		is non-zero or not.
 *
 *	Inputs:
 *		operand - pointer to the expression
 *
 *	Outputs:
 *
 *	Returns:
 *		1 - The expression is non-zero
 *		0 - The expression is zero
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_expr_nonzero(
EXPR           *operand)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_EXPR_NONZERO);
#endif

	switch (operand->type) {
	case INTEGER:
		return (operand->val.ll ? 1 : 0);
		/* NOTREACHED */
		break;

	case UNSIGNED:
		return (operand->val.ull ? 1 : 0);
		/* NOTREACHED */
		break;

    case FLOATG_PT:
		return (operand->val.f ? 1 : 0);
		/* NOTREACHED */
		break;

    case DOUBLEG_PT:
		return (operand->val.d ? 1 : 0);
		/* NOTREACHED */
		break;

	default:
		break;
	}

	return 0;
}


/*********************************************************************
 *
 *	Name: ddl_apply_op
 *	ShortDesc: Applies the given operation to the values on the stack.
 *
 *	Description:
 *		ddl_apply_op applies the given operation to the top value(s)
 *		on the stack.  Each operand is popped from the stack, adjusted
 *		for type (if needed) and the operator is applied. The result is
 *		returned in *result_rtn.
 *
 *	Inputs:
 *		opcode - operation to apply
 *		stack - stack which contains the operands.
 *
 *	Outputs:
 *		stack - stack which contains the result.
 *		result_rtn - pointer to a structure to store the result
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is corrupted
 *		DDL_DIVIDE_BY_ZERO - Divide or Modulus denominator is zero.
 *		DDL_BAD_VALUE_TYPE - Invalid type for this operator
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

#define OP1_RSLT	0	/* Result should be same as operand 1 */
#define INT_RSLT	1	/* Result is an int */

/* Masks for need_op2 */
#define NONE		0x00	/* no second operand needed */
#define NEED_OP2	0x01	/* needs a second operand */
#define CONVERT		0x02	/* must convert both operands to like type */

static int
ddl_apply_op(
DDL_UINT        opcode,
EVAL_STACK     *stack,
EXPR           *result_rtn)
{
	int  rc = DDL_SUCCESS;
	EXPR oprd1 = {0};			/* storage for operand1 */
	EXPR oprd2 = {0};			/* storage for operand2 */
	EXPR result = {0};			/* storage for the result */
	int  need_op2 = NONE;		/* Needs two operands */
	int  result_type = INT_RSLT;/* Type of result needed */

#ifdef lint
	oprd2.type = INTEGER;
	oprd2.size = sizeof(oprd2.val.i);
	oprd2.val.i = 0;
#endif

#ifdef ISPTEST
	TEST_FAIL(DDL_APPLY_OP);
#endif

	/*
	 * Determine whether a second operand is needed and if conversion to
	 * similar types is required.  Also, determine the type of the result.
	 */

	switch (opcode) {
	case NOT_OPCODE:
		need_op2 = NONE;
		result_type = INT_RSLT;
		break;
	case NEG_OPCODE:
	case BNEG_OPCODE:
		need_op2 = NONE;
		result_type = OP1_RSLT;
		break;
	case ADD_OPCODE:
	case SUB_OPCODE:
	case MUL_OPCODE:
	case DIV_OPCODE:
	case MOD_OPCODE:
	case LSHIFT_OPCODE:
	case RSHIFT_OPCODE:
	case AND_OPCODE:
	case OR_OPCODE:
	case XOR_OPCODE:
		need_op2 = NEED_OP2 | CONVERT;
		result_type = OP1_RSLT;
		break;
	case LAND_OPCODE:
	case LOR_OPCODE:
		need_op2 = NEED_OP2;
		result_type = INT_RSLT;
		break;
	case LT_OPCODE:
	case LE_OPCODE:
	case GT_OPCODE:
	case GE_OPCODE:
	case EQ_OPCODE:
	case NEQ_OPCODE:
		need_op2 = NEED_OP2 | CONVERT;
		result_type = INT_RSLT;
		break;
	default:
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Pop the first (and possibly only) operand off of the stack.
	 */

	rc = ddl_pop(&oprd1, stack);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * If a second operand is needed, pop it from the stack.
	 */

	if (need_op2) {
		rc = ddl_pop(&oprd2, stack);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		/*
		 * If the two operands need to be of like type, convert them.
		 */

		if (need_op2 & CONVERT) {
			rc = ddl_convert_type(&oprd2, &oprd1);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	/*
	 * Set the type of the result from the flag set above.
	 */

	if (result_type == INT_RSLT) {
		result.type = INTEGER;
		result.size = sizeof(long);
	}
	else {
		result.type = oprd1.type;
		result.size = oprd1.size;
	}


	/*
	 * Do the operation itself. Put the answer in result.
	 */

	switch (opcode) {
	case NOT_OPCODE:
		result.val.ll = ddl_expr_nonzero(&oprd1) ? 0 : 1;
		break;
	case NEG_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = -oprd1.val.ll;
			break;
        case FLOATG_PT:
			result.val.f = -oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = -oprd1.val.d;
			break;
		case UNSIGNED:
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case BNEG_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = (LONGLONG) (~(ULONGLONG) oprd1.val.ll);
			break;
		case UNSIGNED:
			result.val.ull = ~oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case ADD_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll + oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull + oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f + oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d + oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case SUB_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll - oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull - oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f - oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d - oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case MUL_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll * oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull * oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f * oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d * oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case DIV_OPCODE:
		switch (result.type) {
		case INTEGER:
			{
				if (0L == oprd1.val.ll)
				{
					if (oprd2.val.ll >= 0)
					{
						result.val.ll = numeric_limits<long>::infinity();
					}
					else
					{
						result.val.ll = -numeric_limits<long>::infinity();
					}
				}
				else
				{
					result.val.ll = oprd2.val.ll / oprd1.val.ll;
				}
			}
			break;
		case UNSIGNED:
			{
				if (0L == oprd1.val.ull)
				{
					result.val.ull = numeric_limits<unsigned long>::infinity();	
				}
				else
				{
					result.val.ull = oprd2.val.ull / oprd1.val.ull;
				}
			}
			break;
        case FLOATG_PT:
			{
				if (0.0 == oprd1.val.f)
				{
					if (oprd2.val.f >= 0)
					{
						result.val.f = numeric_limits<float>::infinity();
					}
					else
					{
						result.val.f = -numeric_limits<float>::infinity();
					}
				}
				else
				{
					result.val.f = oprd2.val.f / oprd1.val.f;
				}
			}
			break;
        case DOUBLEG_PT:
			{
				if (0.0 == oprd1.val.d)
				{
					if (oprd2.val.d >= 0)
					{
						result.val.d = numeric_limits<double>::infinity();
					}
					else
					{
						result.val.d = -numeric_limits<double>::infinity();
					}
				}
				else
				{
					result.val.d = oprd2.val.d / oprd1.val.d;
				}
			}
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case MOD_OPCODE:
		switch (result.type) {
		case INTEGER:
			if (0L == oprd1.val.ll) {
				return DDL_DIVIDE_BY_ZERO;
			}
			result.val.ll = oprd2.val.ll % oprd1.val.ll;
			break;
		case UNSIGNED:
			if (0L == oprd1.val.ull) {
				return DDL_DIVIDE_BY_ZERO;
			}
			result.val.ull = oprd2.val.ull % oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case LSHIFT_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = (LONGLONG) ((ULONGLONG) oprd2.val.ll << oprd1.val.ll);
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull << oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case RSHIFT_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = (LONGLONG) ((ULONGLONG) oprd2.val.ll >> oprd1.val.ll);
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull >> oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case AND_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll & oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull & oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case OR_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll | oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull | oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case XOR_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll ^ oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ull = oprd2.val.ull ^ oprd1.val.ull;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			return DDL_BAD_VALUE_TYPE;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case LAND_OPCODE:
		if (ddl_expr_nonzero(&oprd2) && ddl_expr_nonzero(&oprd1)) {
			result.val.ll = 1;
		}
		else {
			result.val.ll = 0;
		}
		break;
	case LOR_OPCODE:
		if (ddl_expr_nonzero(&oprd2) || ddl_expr_nonzero(&oprd1)) {
			result.val.ll = 1;
		}
		else {
			result.val.ll = 0;
		}
		break;
	case LT_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll < oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull < oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f < oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d < oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case LE_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll <= oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull <= oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f <= oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d <= oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case GT_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll > oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull > oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f > oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d > oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case GE_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll >= oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull >= oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f >= oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d >= oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case EQ_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll == oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull == oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f == oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d == oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	case NEQ_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.ll = oprd2.val.ll != oprd1.val.ll;
			break;
		case UNSIGNED:
			result.val.ll = oprd2.val.ull != oprd1.val.ull;
			break;
        case FLOATG_PT:
			result.val.ll = oprd2.val.f != oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.ll = oprd2.val.d != oprd1.val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}
		break;
	default:
		return DDL_ENCODING_ERROR;
	}

	memcpy((char *) result_rtn, (char *) &result, sizeof(result));

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: find_var_min_max
 *	ShortDesc: find the min or max for the given var id
 *
 *	Description:
 *		find_var_min_max will fill an EVAL_VAR_VALUE structure
 *		with the appropriate min or max value
 *
 *	Inputs:
 *		id - the item id of the var to fetch
 *		var_value - pointer to the EVAL_VAR_VALUE structure to load
 *		choice - mask for choice (VAR_MAX_VAL or VAR_MIN_VAL)
 *		which - the subscript of the range list desired
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		var_value - the EVAL_VAR_VALUE structure loaded
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR,
 *		DDL_INVALID_PARAM,
 *		FAILURE,
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			resolve_fetch(),
 *			eval_attr_max_value(),
 *			eval_attr_min_value()
 *			append_depinfo()
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static
int
find_var_min_max(
ITEM_ID         id,
EVAL_VAR_VALUE *var_value,
unsigned long   choice,
DDL_UINT        which,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc;
	int             rc2 = DDL_SUCCESS;	/* secondary return code */
	int             free_attr = FALSE;

	SCRATCH_PAD     scratch[2];	/* scratch pad for fetch */
	FLAT_VAR        var;
	FLAT_VAR_MISC   misc;
	RANGE_DATA_LIST *range;
	DEPBIN         *cur_depbin;	/* Current dependency and binary info */

#ifdef ISPTEST
	TEST_FAIL(FIND_VAR_MIN_MAX);
#endif


	memset((char *) &var, 0, sizeof(FLAT_VAR));
	memset((char *) &misc, 0, sizeof(FLAT_VAR_MISC));

	var.misc = &misc;

	rc = resolve_fetch(env_info, id, choice,
		(void *) &var, (unsigned short) VARIABLE_ITYPE,
		env_info->block_handle, scratch);

	/**
	 *	Eval must manually check to see if min/max binary has been hooked.
  	 *  resolve_fetch() will return an error only if fetch encounters a problem.
	 *  If this particular var does not have Min or Max attributes, fetch will
 	 *	not return an error.
	 **/

	if (!(choice & var.masks.bin_hooked)) {
		rc = DDL_ENCODING_ERROR;
	}

	if (rc == DDL_SUCCESS) {

		if (!(var.masks.attr_avail & choice)) {

			if (choice == VAR_MIN_VAL) {
				cur_depbin = var.misc->depbin->db_min_val;

				rc = eval_attr_min_values(
					cur_depbin->bin_chunk,
					cur_depbin->bin_size,
					&var.misc->min_val,
					&cur_depbin->dep,
					env_info,
					var_needed);
			}
			else {	/* choice == VAR_MAX_VAL */
				cur_depbin = var.misc->depbin->db_max_val;

				rc = eval_attr_max_values(
					cur_depbin->bin_chunk,
					cur_depbin->bin_size,
					&var.misc->max_val,
					&cur_depbin->dep,
					env_info,
					var_needed);
			}

			if (rc == DDL_SUCCESS) {
				free_attr = TRUE;
			}

			if (depinfo != NULL) {
				rc2 = append_depinfo(depinfo, &cur_depbin->dep);
				if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
					rc = rc2;
				}
			}
			ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
		}
	}

	if (rc == DDL_SUCCESS) {

		range = (choice == VAR_MIN_VAL) ? &var.misc->min_val : &var.misc->max_val;

		if (which > range->count) {

			rc = DDL_ENCODING_ERROR;
		}
		else {

			which--;/* decrement which for using it as the range
				 * index */

			var_value->type = range->list[which].type;
			var_value->size = range->list[which].size;

			switch (var_value->type) {

			case INTEGER:
				var_value->val.ll = range->list[which].val.ll;
				break;

			case UNSIGNED:
				var_value->val.ull = range->list[which].val.ull;
				break;

            case FLOATG_PT:
				var_value->val.f = range->list[which].val.f;
				break;

            case DOUBLEG_PT:
				var_value->val.d = range->list[which].val.d;
				break;

			default:
				rc = VARIABLE_VALUE_NEEDED;
				break;
			}
		}
	}

	resolve_fetch_free(env_info, (void *) &var, (unsigned short) VARIABLE_ITYPE,
		scratch, env_info->block_handle);

	if (free_attr) {
		ddl_free_range_list((choice == VAR_MIN_VAL) ?
			&var.misc->min_val : &var.misc->max_val,
			FREE_ATTR);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_free_expr
 *	ShortDesc: Free EXPR structure.
 *
 *	Description:
 *		ddl_free_expr will check the type in the EXPR structure.
 *      If the type indicates that the STRING portion of the union is used,
 *		then call ddl_free_string to free it. Otherwise, just set other elements to zero.
 *
 *	Inputs:
 *		pExpr: pointer to the EXPR structure
 *
 *	Outputs:
 *		pExpr: pointer to the empty EXPR structure
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

void
ddl_free_expr (EXPR *pExpr)
{
	if (pExpr->type == ASCII)
	{
		ddl_free_string(&(pExpr->val.s));
	}

	pExpr->type = DDSUNUSED;
	pExpr->size = 0;
	pExpr->val.ull = 0;
}


/*********************************************************************
 *
 *	Name: ddl_do_eval_expr
 *	ShortDesc: Parse and evaluate the binary encoded expression.
 *
 *	Description:
 *		ddl_do_eval_expr will unpack a binary encoded expression and
 *		return the expression along with dependency information.
 *		This function uses a stack to evaluate the postfix notation
 *		used within the binary expression.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Expression was malformed
 *		error returns from other ddl_* functions
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_do_eval_expr(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc      = DDL_SUCCESS;
	EXPR            xval    = {0};	/* storage for integer or float constants */
	OP_REF          op_ref  = {STANDARD_TYPE, 0};	/* storage for operational_reference */
	DDL_UINT        which   = 0;	/* which min or max value desired */
	DDL_UINT        tag     = 0;	/* identifies the current type of expression */
	EVAL_STACK      stack   = {0};	/* stack used for evaluating the expression */
	DDL_UINT        ival    = 0;	/* integer value */
	DDL_UINT        max_val = 0;	/* Maximum value used to determine size of int */
	ITEM_ID         id      = 0;	/* store ITEM_ID in here while parsing a ref. */
	ulong           bitMask = 0;	// Stores the bitmask if VIA_BITENUM_REF
	DDL_UINT_LONG	lval = 0; //unsigned long long value

	ASSERT_DBG(chunkp && *chunkp && size);
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

#ifdef ISPTEST
	TEST_FAIL(DDL_DO_EVAL_EXPR);
#endif

	/*
	 * This code handles the very common case of processing a single
	 * integer constant much faster than the generic code
	 */

	if ((*size == 2) && ((*chunkp)[0] == INTCST_OPCODE)) {
		if (expr != NULL) {
			expr->val.ll = (*chunkp)[1];
			expr->type = INTEGER;
			expr->size = 1;
		}
		*size -= 2;
		*chunkp += 2;
		return DDL_SUCCESS;
	}

	/*
	 * Initialize the evaluation stack, and process the chunk.
	 */

	stack.top = stack.data;

	while (*size > 0) {

		/*
		 * Parse the tag, and switch on the tag type
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		switch (tag) {
		case STRCST_OPCODE: // The object in a string constant
			if (expr)
			{
				ddl_parse_string(chunkp, size, &expr->val.s, depinfo, env_info, var_needed);
				expr->type = ASCII;
			}
			return DDL_SUCCESS;
			break;

		case INTCST_OPCODE:	/* The object in an integer constant */

			rc = ddl_parse_integer_func(chunkp, size, &ival);

			if (rc == DDL_SUCCESS)
			{
				xval.type = INTEGER;

				/*
				 * Determine size of the integer by comparing it with
				 * the largest integer of the current size.  If bigger,
				 * increment the size, compute the next largest integer
				 * and try again. You may not exceed the size of
				 * long, however.
				 */

				xval.size = 1;
				max_val = 0x7F;	//maximum value in integer in one byte

				while ((ival > max_val) && (xval.size < sizeof(long))) {
					xval.size++;
					max_val = (max_val << 8) | 0xFF;
				}

				xval.val.ll = (LONGLONG) ival;
			}
			else // Try to read long unsigned integer value
			{
				rc = ddl_parse_integer_long_func(chunkp, size, &lval); 
				xval.type = INTEGER;
				xval.size = sizeof(LONGLONG);
				xval.val.ll = (LONGLONG) lval;
			}

			break;
			

		case FPCST_OPCODE:	/* This is a floating point constant */

            xval.type = FLOATG_PT;
			xval.size = sizeof(xval.val.f);

			rc = ddl_parse_float(chunkp, size, &(xval.val.f));

			break;

		case VARID_OPCODE:
		case MAXVAL_OPCODE:
		case MINVAL_OPCODE:
		case VARREF_OPCODE:
		case MAXREF_OPCODE:
		case MINREF_OPCODE:
			if ((tag != VARID_OPCODE) && (tag != VARREF_OPCODE)) {

				/*
				 * A min or max value reference.  Parse which
				 * value (0, 1, 2, ...),
				 */

				DDL_PARSE_INTEGER(chunkp, size, &which);
			}

			switch (tag) {
			case VARID_OPCODE:
			case MAXVAL_OPCODE:
			case MINVAL_OPCODE:

				/*
				 * Parse the variable ID.
				 */

				DDL_PARSE_INTEGER(chunkp, size, &ival);

				op_ref.op_info.id = (ITEM_ID) ival;
				op_ref.op_info.member = 0;
				op_ref.op_info.type = VARIABLE_ITYPE;

				break;

			case VARREF_OPCODE:
			case MAXREF_OPCODE:
			case MINREF_OPCODE:

				/*
				 * Parse the reference.  This will return an
				 * operational reference
				 */

				rc = ddl_parse_op_ref(chunkp, size, &op_ref, depinfo, env_info, var_needed, &bitMask );
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				break;

			default:	/* this cannot happen */
				CRASH_RET(DDL_SERVICE_ERROR);
				/* NOTREACHED */
				break;
			}

			/*
			 * Keep track of the fact that we have another variable
			 * dependency.
			 */

			if (depinfo) {
				rc = ddl_add_depinfo(&op_ref, depinfo);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			if (expr) {

				/*
				 * If expression is desired, do the upcall to
				 * get the value of the variable.
				 */
				EVAL_VAR_VALUE  var_value;		/* storage area for upcall values */

				switch (tag) {
				case VARID_OPCODE:
				case VARREF_OPCODE:

					rc = app_func_get_param_value(env_info, &op_ref, &var_value);

					if (rc != DDL_SUCCESS) 
					{
						if( op_ref.op_ref_type == STANDARD_TYPE )
						{
							var_needed->op_ref_type = STANDARD_TYPE;

							var_needed->op_info.id		= op_ref.op_info.id;
							var_needed->op_info.member	= op_ref.op_info.member;
							var_needed->op_info.type	= op_ref.op_info.type;
						}
						else
						{
							var_needed->op_ref_type = COMPLEX_TYPE;

							var_needed->op_info_list.count = op_ref.op_info_list.count;

							var_needed->op_info_list.list = (OP_REF_INFO*) malloc((size_t) var_needed->op_info_list.count * sizeof( OP_REF_INFO ));

							for( int i = 0; i < var_needed->op_info_list.count; i++ )
							{
								var_needed->op_info_list.list[i].id		= op_ref.op_info_list.list[i].id;
								var_needed->op_info_list.list[i].member	= op_ref.op_info_list.list[i].member;
								var_needed->op_info_list.list[i].type	= op_ref.op_info_list.list[i].type;
							}
						}
						return rc;
					}

					break;

				case MAXVAL_OPCODE:
				case MAXREF_OPCODE:
					rc = find_var_min_max(op_ref.op_info.id,
						&var_value,
						(unsigned long) VAR_MAX_VAL,
						which,
						depinfo,
						env_info,
						var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					break;

				case MINVAL_OPCODE:
				case MINREF_OPCODE:
					rc = find_var_min_max(op_ref.op_info.id,
						&var_value,
						(unsigned long) VAR_MIN_VAL,
						which,
						depinfo,
						env_info,
						var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					break;

				default:
					CRASH_RET(DDL_SERVICE_ERROR);
					/* NOTREACHED */
					break;
				}

				/*
				 * Converting EVAL_VAR_VALUE to an EXPR
				 */

				xval.size = var_value.size;
				xval.type = var_value.type;

				switch (xval.type) {
                case DOUBLEG_PT:
					xval.val.d = var_value.val.d;
					break;

                case FLOATG_PT:
					xval.val.f = var_value.val.f;
					break;

				case INTEGER:
					xval.val.ll = var_value.val.ll;
					break;

					/**
					 ** Within EVAL: INDEX, ENUMs and BIT_ENUMs will
					 ** be treated as UNSIGNED.
					 **/

				case UNSIGNED:
				case INDEX:
				case ENUMERATED:
				case BIT_ENUMERATED:
					if ( bitMask != 0 )
					{
						var_value.val.ull &= bitMask;	// AND-out the desired bit
					}

					xval.val.ull = var_value.val.ull;
					xval.type = UNSIGNED;
					break;

				case ASCII:	/* should never happen */
				default:
					return DDL_BAD_VALUE_TYPE;
					/* NOTREACHED */
					break;
				}
			}
			break;

		case BLOCK_OPCODE:

			/*
			 * Get the block's item_id from the active block table
			 */

			rc = pConnMgr->get_abt_dd_blk_id(env_info->block_handle, &id);
			if (rc != SUCCESS) {
				return (rc);
			}

			xval.val.ull = (ULONGLONG)id;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		case BLOCKID_OPCODE:

			/*
			 * Parse the block ID.
			 */

			DDL_PARSE_INTEGER(chunkp, size, &ival);
			xval.val.ull = (ULONGLONG)ival;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		case BLOCKREF_OPCODE:

			/*
			 * Parse the block reference.  This will return an
			 * ITEM_ID
			 */

			rc = ddl_parse_item_id(chunkp, size, &id, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			xval.val.ull = (ULONGLONG)id;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		default:

			/*
			 * This must be an opcode.  Apply it to the values on
			 * the stack.
			 */

			if (expr) {
				rc = ddl_apply_op(tag, &stack, &xval);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

			}
			break;
		}

		/*
		 * Take the value which was calculated above and push it on the
		 * stack.
		 */

		if (expr) {
			ASSERT_RET(stack.top < (stack.data + EVAL_STACK_SIZE), DDL_EXPR_STACK_OVERFLOW);

			stack.top->type = xval.type;
			stack.top->size = xval.size;
			stack.top->val = xval.val;

			stack.top++;
		}
	}

	if( op_ref.op_ref_type == COMPLEX_TYPE )
	{
		ddl_free_op_ref_info_list( &op_ref.op_info_list );
	}

	/*
	 * Now that the entire chunk has been parsed, the resulting value
	 * should be the only item left on the stack.  Return it.
	 */

	if (expr) 
	{
		rc = ddl_pop(expr, &stack);
		if (rc != DDL_SUCCESS) 
		{
			return rc;
		}

		if (stack.top != stack.data) 
		{
			return DDL_ENCODING_ERROR;
		}
	}
	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_eval_expr
 *	ShortDesc: Parse and evaluate the binary encoded expression.
 *
 *	Description:
 *		ddl_eval_expr will unpack a binary encoded expression and
 *		return the expression along with dependency information.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is not an expression or length
 *				is incorrect.
 *		error returns from DDL_PARSE_TAG() and ddl_do_eval_expr().
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_eval_expr(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	DDL_UINT        length, tag;	/* Length and type of this binary */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_EVAL_EXPR);
#endif

	/*
	 * Evaluate the expression, rather than extracting it. When given an
	 * expression, it evaluates the current value.
	 */

	/*
	 * Parse the tag, and make sure it is an EXPRESSION_TAG.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &length);

	if ((DDL_UINT) EXPRESSION_TAG != tag) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Make sure the length makes sense.
	 */

	if ((DDL_UINT) 0 == length || (DDL_UINT) length > *size) {
		return DDL_ENCODING_ERROR;
	}

	*size -= length;

	/*
	 * Parse and evalutate the expression
	 */

	rc = ddl_do_eval_expr(chunkp, &length, expr, depinfo, env_info, var_needed);

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_expr_choice
 *	ShortDesc: Choose the correct expression from a binary.
 *
 *	Description:
 *		ddl_expr_choice will parse the binary for an expression,
 *		according to the current conditionals (if any).  The value of
 *		the expression is returned, along with dependency information.
 *		If a value is found, the data_valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_eval_expr().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_expr_choice(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef ISPTEST
	TEST_FAIL(DDL_EXPR_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}


	/*
	 * A chunk was found and a value is requested.
	 */

	if (expr && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (expr == NULL));


		/* We have the actual value.  Parse it. */

		rc = ddl_eval_expr(&val.chunk, &val.size, expr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (expr && data_valid) {
			*data_valid = TRUE;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_promote_to
 *	ShortDesc: Promotes an EXPR to the specified type and size.
 *
 *	Description:
 *		ddl_promote_to will use ddl_convert_type() in order to
 *		promote the passed EXPR to the requested one.  If ddl_convert_type()
 *		does not succeed or if the resulting type and size were not
 *		the ones requested, an error code will be returned.
 *		NOTE: ddl_promote_to will only promote expressions, it will
 *			not succeed if the requested change requires a demotion.
 *
 *	Inputs:
 *		expr - pointer to expression to be converted
 *		new_type - desired type to convert into
 *		new_size - desired size to convert into
 *
 *	Outputs:
 *		expr - pointer to the modified expression
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE - The conversion was unsuccessful.
 *		error return from ddl_convert_type()
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

int
ddl_promote_to(
EXPR           *expr,
unsigned short  new_type,
unsigned short  new_size)
{
	EXPR            target;	/* target expression */
	int             rc;

#ifdef ISPTEST
	TEST_FAIL(DDL_PROMOTE_TO);
#endif

	target.type = new_type;
	target.size = new_size;

	switch (new_type) {
	case INTEGER:
		target.val.ll = 0;
		break;
	case UNSIGNED:
		target.val.ull = 0;
		break;
    case FLOATG_PT:
		target.val.f = (float) 0.0;
		break;
    case DOUBLEG_PT:
		target.val.d = 0.0;
		break;
	default:
		return DDL_BAD_VALUE_TYPE;
	}

	rc = ddl_convert_type(expr, &target);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * If the conversion did not produce the type and size requested,
	 * return error.
	 */

	if ((expr->type != new_type) || (expr->size != new_size)) {
		return DDL_BAD_VALUE_TYPE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_exprs_equal
 *	ShortDesc: Determines if two expressions are equal.
 *
 *	Description:
 *		ddl_exprs_equal will compare two expressions and return 1
 *		if they are equal and 0 if not.
 *
 *	Inputs:
 *		expr1 - pointer to first expression
 *		expr2 - pointer to second expression
 *
 *	Outputs:
 *
 *	Returns:
 *		1 - expressions are equal
 *		0 - expressions are not equal
 *		error return from ddl_equal()
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_exprs_equal(
EXPR           *expr1,
EXPR           *expr2)
{
	EXPR            x1, x2;	/* Temp expression structs */
	int             rc = DDL_ENCODING_ERROR;

#ifdef ISPTEST
	TEST_FAIL(DDL_EXPRS_EQUAL);
#endif

	memcpy((char *) &x1, (char *) expr1, sizeof(EXPR));
	memcpy((char *) &x2, (char *) expr2, sizeof(EXPR));

	rc = ddl_convert_type(&x1, &x2);
	if (rc != DDL_SUCCESS) 
	{
		return rc;
	}

	switch (x1.type) 
	{
		case INTEGER:
			rc = (x1.val.ll == x2.val.ll);
			break;

		case UNSIGNED:
			rc = (x1.val.ull == x2.val.ull);
			break;

        case FLOATG_PT:
			rc = (x1.val.f == x2.val.f);
			break;

        case DOUBLEG_PT:
			rc = (x1.val.d == x2.val.d);
			break;

		default:
			rc = DDL_ENCODING_ERROR;
			break;
	}

	return rc;
}





/*********************************************************************
 *
 *	Name: eval_attr_expr
 *	ShortDesc: Evaluate an expression
 *
 *	Include: libddl.h
 *
 *	Description:
 *		The eval_attr_expr function evaluates an expression.
 *		The buffer pointed to by chunk should contain an
 *		expression returned from fetch_var_attr, and size
 *		should specify its size.
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		value - pointer to an EXPR containing the result.
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			a)	ddl_expr_choice()
 *			b)	ddl_free_depinfo()
 *			c)	ddl_shrink_depinfo()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
eval_attr_expr(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc, data_valid;

#ifdef ISPTEST
	TEST_FAIL(EVAL_EXPR);
#endif
	EXPR   *expr = static_cast<EXPR*>(voidP);
	data_valid = FALSE;
	memset((char *) expr, 0, sizeof(EXPR));	/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_expr_choice(&chunk, &size, expr, depinfo, &data_valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (expr && !data_valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_attr_int_expr
 *	ShortDesc: Evaluate an integer expression
 *
 *	Description:
 *		The eval_attr_int_expr function evaluates an expression.
 *		The buffer pointed to by chunk should contain an
 *		expression returned from fetch_var_attr, and size
 *		should specify its size.
 *		If the expression is an arithmetic type (ie. int, float,
 *		double, unsigned), the expression will be cast to an
 *		unsigned long (ie. DDL_UINT).
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - pointer to the size of the binary
 *		value - pointer to a DDL_UINT where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		value - pointer to a DDL_UINT containing the result.
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE
 *		return codes from:
 *			a)	eval_attr_expr()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
int
eval_attr_int_expr(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	unsigned long*	value = static_cast<unsigned long*>(voidP);
	int             rc;
	EXPR            expr;

#ifdef ISPTEST
	TEST_FAIL(EVAL_INT_EXPR);
#endif

	*value = 0;		/** initialize output parameter **/

	if (value) {

		rc = eval_attr_expr(chunk, size, &expr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		switch (expr.type) {
		case INTEGER:
			*value = (DDL_UINT) expr.val.ll;
			break;
		case UNSIGNED:
			*value = (DDL_UINT) expr.val.ull;
			break;
        case FLOATG_PT:
			*value = (DDL_UINT) expr.val.f;
			break;
        case DOUBLEG_PT:
			*value = (DDL_UINT) expr.val.d;
			break;
		default:	/* UNUSED, ASCII */
			return DDL_BAD_VALUE_TYPE;
			/* NOTREACHED */
			break;
		}
	}
	else {
		rc = eval_attr_expr(chunk, size, (EXPR *) NULL, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	return DDL_SUCCESS;
}


int
eval_attr_ulonglong_expr(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	unsigned long long*	value = static_cast<unsigned long long*>(voidP);
	int             rc;
	EXPR            expr;

#ifdef ISPTEST
	TEST_FAIL(EVAL_INT_EXPR);
#endif

	*value = 0;		/** initialize output parameter **/

	if (value) {

		rc = eval_attr_expr(chunk, size, &expr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		switch (expr.type) {
		case INTEGER:
			*value = (unsigned long long) expr.val.ll;
			break;
		case UNSIGNED:
			*value = (unsigned long long) expr.val.ull;
			break;
        case FLOATG_PT:
			*value = (unsigned long long) expr.val.f;
			break;
        case DOUBLEG_PT:
			*value = (unsigned long long) expr.val.d;
			break;
		default:	/* UNUSED, ASCII */
			return DDL_BAD_VALUE_TYPE;
			/* NOTREACHED */
			break;
		}
	}
	else {
		rc = eval_attr_expr(chunk, size, (EXPR *) NULL, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	return DDL_SUCCESS;
}




/*********************************************************************
 *
 *	Name: do_link_expr
 *	ShortDesc: Actually do the HART linking of the binary encoded expression.
 *
 *	Author:
 *		Christian Gustafson (with a lot of help from BRUCE DAVIS, my hero)
 *
 *********************************************************************/

static int
do_link_expr(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	EXPR            xval = {0};	/* storage for integer or float constants */
	OP_REF          op_ref = {STANDARD_TYPE, 0};	/* storage for operational_reference */

	DDL_UINT        tag;	/* identifies the current type of expression */
	DDL_UINT        which;	/* is it MIN or MAX */
	DDL_UINT        ival;	/* integer value */
	DDL_UINT        max_val;/* Maximum value used to determine size of int */
	ITEM_ID         id;	/* store ITEM_ID in here while parsing a ref. */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	ASSERT_DBG(chunkp && *chunkp && size);


	which = 0;		/* initialize to not Min of Max */


	/*
	 * This code handles the very common case of processing a single
	 * integer constant much faster than the generic code
	 */

	if ((*size == 2) && ((*chunkp)[0] == INTCST_OPCODE)) {

		*size -= 2;
		*chunkp += 2;
		return DDL_SUCCESS;
	}

	/*
	 * Initialize the evaluation stack, and process the chunk.
	 */


	while (*size > 0) {

		/*
		 * Parse the tag, and switch on the tag type
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		switch (tag) {
		case INTCST_OPCODE:	/* The object in an integer constant */


			DDL_PARSE_INTEGER(chunkp, size, &ival);

			xval.type = INTEGER;

			/*
			 * Determine size of the integer by comparing it with
			 * the largest integer of the current size.  If bigger,
			 * increment the size, compute the next largest integer
			 * and try again. You may not exceed the size of
			 * long, however.
			 */

			xval.size = 1;
			max_val = 0xFF;

			while ((ival > max_val) && (xval.size < sizeof(long))) {
				xval.size++;
				max_val = (max_val << 8) | 0xFF;
			}

			xval.val.ll = (LONGLONG) ival;

			break;

		case FPCST_OPCODE:	/* This is a floating point constant */

            xval.type = FLOATG_PT;
			xval.size = sizeof(xval.val.f);

			rc = ddl_parse_float(chunkp, size, &(xval.val.f));

			break;

		case VARID_OPCODE:
		case MAXVAL_OPCODE:
		case MINVAL_OPCODE:
		case VARREF_OPCODE:
		case MAXREF_OPCODE:
		case MINREF_OPCODE:
			if ((tag != VARID_OPCODE) && (tag != VARREF_OPCODE)) {

				/*
				 * A min or max value reference.  Parse which
				 * value (0, 1, 2, ...),
				 */

				DDL_PARSE_INTEGER(chunkp, size, &which);
			}

			switch (tag) {
			case VARID_OPCODE:
			case MAXVAL_OPCODE:
			case MINVAL_OPCODE:

				/*
				 * Parse the variable ID.
				 */

				DDL_PARSE_INTEGER(chunkp, size, &ival);

				op_ref.op_info.id = (ITEM_ID) ival;

				break;

			case VARREF_OPCODE:
			case MAXREF_OPCODE:
			case MINREF_OPCODE:

				/*
				 * Parse the reference.  This will return an
				 * operational reference
				 */

				rc = link_ref(chunkp, size, depinfo, env_info, var_needed, which);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				break;

			default:	/* this cannot happen */
				CRASH_RET(DDL_SERVICE_ERROR);
				/* NOTREACHED */
				break;
			}
			break;


		case BLOCK_OPCODE:

			/*
			 * Get the block's item_id from the active block table
			 */

			rc = pConnMgr->get_abt_dd_blk_id(env_info->block_handle, &id);
			if (rc != SUCCESS) {
				return (rc);
			}

			xval.val.ull = (ULONGLONG)id;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		case BLOCKID_OPCODE:

			/*
			 * Parse the block ID.
			 */

			DDL_PARSE_INTEGER(chunkp, size, &ival);
			xval.val.ull = (ULONGLONG)ival;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		case BLOCKREF_OPCODE:

			/*
			 * Parse the block reference.  This will return an
			 * ITEM_ID
			 */

			rc = ddl_parse_item_id(chunkp, size, &id, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			xval.val.ull = (ULONGLONG)id;
			xval.type = UNSIGNED;
			xval.size = sizeof(ulong);
			break;

		default:

			/*
			 * This must be an opcode.  Apply it to the values on
			 * the stack.
			 */
			break;
		}

		/*
		 * Take the value which was calculated above and push it on the
		 * stack.
		 */

	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: link_expr
 *	ShortDesc: HART link the binary encoded expression.
 *
 *	Author:
 *		Christian Gustafson (with a lot of help from !!BRUCE DAVIS!!)
 *
 *********************************************************************/

int
link_expr(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	DDL_UINT        length, tag;	/* Length and type of this binary */

	ASSERT_DBG(chunkp && *chunkp && size);

	/*
	 * Evaluate the expression, rather than extracting it. When given an
	 * expression, it evaluates the current value.
	 */

	/*
	 * Parse the tag, and make sure it is an EXPRESSION_TAG.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &length);

	if ((DDL_UINT) EXPRESSION_TAG != tag) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Make sure the length makes sense.
	 */

	if ((DDL_UINT) 0 == length || (DDL_UINT) length > *size) {
		return DDL_ENCODING_ERROR;
	}

	*size -= length;

	/*
	 * Parse and evalutate the expression
	 */

	rc = do_link_expr(chunkp, &length, depinfo, env_info, var_needed);

	return rc;
}
