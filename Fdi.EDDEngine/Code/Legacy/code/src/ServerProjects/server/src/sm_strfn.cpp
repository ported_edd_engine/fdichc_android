

/**************************************************************************
 * "@(#)$Id: sm_strfn.cpp,v 1.36 1996/06/14 21:53:24 gabravi Exp $"
 **************************************************************************/
#include "stdinc.h"


#include <Inf/NtcSpecies/Ntcassert2.h>


//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemId. This function is passed to qsort
//////////////////////////////////////////////////////////////////////////////

int  symidtbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	////////////////////////////////////////////////////////////////////////////
	//
	// Optimized: 6/6/96 (Gabe Avila)
	//

	if ((*elem1).item_id < (*elem2).item_id)
		return -1;
	else if ((*elem1).item_id > (*elem2).item_id)
		return 1;
	else
		return 0;

	// Original code
	/*
	SYMTBL	id1, id2;

	id1 = elem1[0];
	id2 = elem2[0];
	
	if (id1.item_id < id2.item_id)
		return -1;
	else if (id1.item_id > id2.item_id)
		return 1;
	else
		return 0;
	*/
	//
	////////////////////////////////////////////////////////////////////////////
}




//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemId. This function is passes to qsort
//////////////////////////////////////////////////////////////////////////////

int  symnametbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	////////////////////////////////////////////////////////////////////////////
	//
	// Optimized: 6/6/96 (Gabe Avila)
	//

	if (strcmp((*elem1).item_name_ptr, (*elem2).item_name_ptr) < 0)
		return -1;
	else if (strcmp((*elem1).item_name_ptr, (*elem2).item_name_ptr) > 0)
		return 1;
	else
		return 0;

	// Original code
	/*
	SYMTBL	id1, id2;

	id1 = elem1[0];
	id2 = elem2[0];
	
	if (strcmp(id1.item_name_ptr, id2.item_name_ptr) < 0)
		return -1;
	else if (strcmp(id1.item_name_ptr, id2.item_name_ptr) > 0)
		return 1;
	else
		return 0;
	*/
	//
	////////////////////////////////////////////////////////////////////////////
}



#define MAX_SYM_LINE_LEN	512	// REALLY BIG SO IT NEVER IS REACHED
/*********************************************************************************
	read_sym_file
	read sym file and stores the dd-id's and corresponding names in memory
**********************************************************************************/
int 
read_sym_file(ENV_INFO* env_info, DD_DEVICE_ID *dd_device_id, SYMINFO* syminfo)
{

	wchar_t			symfile_name[MAX_PATH] = {0};
	FILE*			symfile;
	char            line[MAX_SYM_LINE_LEN + 1];
//	char           *line_ptr;
//	BOOLEAN			fourth_field;
//	BOOLEAN			bmember;
	int				tblsize;
//	int				line_size;
	int				item_name_size;
	int				recno;
	SYMTBL*			temp_symtbl;
	SYMTBL*			temp_symnametbl;
	char			szSep[] = " \t\n";		// Valid token separators
	ITEM_ID			iItemId = 0;

    PS_Wcscpy(symfile_name, wcslen(dd_device_id->dd_path) + 1, dd_device_id->dd_path);		// Copy *.fm? filename

	wchar_t* pExt = wcsrchr(symfile_name, L'.');		// Find extension position

	int iFilenameSize = (pExt - symfile_name);				// Calculate size of filename minus extension

	PS_Wcscpy(pExt, MAX_PATH - iFilenameSize , L".sym");		// Copy in the new extension

	/*
	 * Open the symbol file
	 */
    symfile = PS_wfopen(symfile_name, _T("r"));

	if (symfile == NULL)
	{
		return SM_SYMFILE_OPEN_ERR;
	}

	// get the size of the file (in lines)
	tblsize = 0;
	while (!feof (symfile))
	{
		fgets (line, sizeof(line)-1, symfile);
		tblsize ++;	
	}
	fseek(symfile, 0, SEEK_SET);

	if (tblsize == 0)
	{
		fclose (symfile);
		return SM_SYMFILE_OPEN_ERR;
	}
	
	temp_symtbl = (SYMTBL*)malloc( tblsize * sizeof(SYMTBL) );
	if (temp_symtbl == NULL) {
		CStdString strMsg;
        strMsg.TryLoad((void *)IDS_READ_SYM_MALLOC);
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", strMsg); 
	}
	memset(temp_symtbl, NULL, sizeof(temp_symtbl));

	temp_symnametbl = (SYMTBL*)malloc( tblsize * sizeof(SYMTBL) );
	if (temp_symnametbl == NULL) {
		CStdString strMsg;
        strMsg.TryLoad((void *)IDS_READ_SYM_MALLOC2);
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", strMsg); 
	}
	memset(temp_symnametbl, NULL, sizeof(temp_symnametbl));


	recno = -1;

	(void) fgets (line, sizeof(line)-1, symfile);

		for (; !feof (symfile); fgets(line, sizeof(line)-1, symfile) ) {
		char *pItemId = strtok (line, szSep);			// Position at Column 1 (but ignore)
		char *pItemName = strtok ((char *)NULL, szSep);	// Get ItemName from Column 2
		char *pCol3 = strtok ((char *)NULL, szSep);		// Position at Column 3
		char *pCol4 = strtok ((char *)NULL, szSep);		// Position at Column 4

		if (pCol4 == NULL)
		{
			pItemId = pCol3;	// If there is no Column 4, the ItemId is in Column 3
		}
		else
		{
			if(strtoul(pCol4, NULL, 10) != 0)
			{
				pItemId = pCol4;	// If there is a Column 4, the ItemId is found there
			}
			else
			{
				pItemId = pCol3;
			}
		}
		
		if(pItemId == nullptr || pItemName == nullptr)
		{
			free(temp_symtbl);
			free(temp_symnametbl);
			return SM_SYMFILE_OPEN_ERR;
		}

		// Insert into the tables
		recno++;

		// First insert the ItemName
		item_name_size = strlen(pItemName) + 1;
	
		temp_symtbl[recno].item_name_ptr = (char*) malloc(item_name_size);
		if (temp_symtbl[recno].item_name_ptr == NULL) {
		//	panic("read_sym_file: malloc for temp_symtbl.item_name_ptr failed \n");
		}
		strcpy(temp_symtbl[recno].item_name_ptr, pItemName);

		temp_symnametbl[recno].item_name_ptr = (char*) malloc(item_name_size);
		if (temp_symnametbl[recno].item_name_ptr == NULL) {
		//	panic("read_sym_file: malloc for temp_symnametbl.item_name_ptr failed \n");
		}
		strcpy(temp_symnametbl[recno].item_name_ptr, pItemName);

		// Now insert the ItemId
		int base;
        if(strncmp(pItemId, "0x", 2) == 0)
		{
			base = 16;
		}
		else
		{
			base = 10;
		}

		iItemId = strtoul(pItemId, NULL, base);

		temp_symtbl[recno].item_id = iItemId;
		temp_symnametbl[recno].item_id = iItemId;
	}

	/*
	 * Close the symbol file
	 */
	(void) fclose (symfile);

	int iSymCnt = recno+1;

	//sort by DDID
	qsort(temp_symtbl, iSymCnt, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symidtbl_compare);

	syminfo->symidtbl = temp_symtbl;

	//sort by DDstring
	qsort(temp_symnametbl, iSymCnt, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symnametbl_compare);
	
	syminfo->symnametbl = temp_symnametbl;
	
	syminfo->numelem = iSymCnt;

#ifdef DEBUG
// code for dumping copied from pc_lib.cpp.

	PS_Wcscpy(pExt, MAX_PATH - iFilenameSize , L".dbg");

	symfile = _tfopen (symfile_name, L"w");
    if (symfile != NULL)
    {
	    fprintf(symfile,  "*********************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY ID      \n");
	    fprintf(symfile,  "*********************************\n");
	    for (int i = 0; i < iSymCnt; i++) {
		    fprintf(symfile, "%10X            %s\n", temp_symtbl[i].item_id, 
				    temp_symtbl[i].item_name_ptr);
	    }
	    fprintf(symfile,  "\n");
	    fprintf(symfile,  "=================================\n\n");

	    fprintf(symfile,  "*********************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY NAME     \n");
	    fprintf(symfile,  "*********************************\n");
	    for (int j = 0; j < iSymCnt; j++) {
		    fprintf(symfile, "%10X            %s\n", temp_symnametbl[j].item_id, 
				    temp_symnametbl[j].item_name_ptr);
	    }
	    (void) fclose (symfile);
    }
#endif
	return SUCCESS;

}


void free_symtbl(SYMINFO* syminfo)
{
	SYMTBL* symtbl;
	int		num;

	num = syminfo->numelem;
	symtbl = syminfo->symidtbl;

	// free memory pointed to by item_name_ptr before freeing the table itself
	for (int i = 0; i < num; i++) {
		(void)free((void*)(symtbl[i].item_name_ptr));
	}

	free((void*)symtbl);
	syminfo->symidtbl = 0;

	symtbl = syminfo->symnametbl;

	// free memory pointed to by item_name_ptr before freeing the table itself
	for (int j = 0; j < num; j++) {
		(void)free((void*)(symtbl[j].item_name_ptr));
	}

	free((void*)symtbl);
	syminfo->symnametbl = 0;

	syminfo->numelem = 0;

}


////////////////////////////////////////////////////////////////////////////
//
// The following optimizations have passed all RTEST's but are not being 
// compiled yet (per Jon) for extra cutiousness: 6/6/96 (Gabe Avila)
//
// NOTE:	I Benchmarked these optimized functions and they performed 
//			faster than Microsoft's bsearch()
//

//
// Converts item_id to item-name. 
//

// Original code
int 
server_item_id_to_name(ENV_INFO* env_info, SYMINFO* syminfo, ITEM_ID item_id, LPTSTR item_name, int outbuf_size)
{
	int	low;
	int med;
	int	high;
	SYMTBL* symtbl;
	BOOLEAN	flag;
	int rc ;

	assert(syminfo != NULL) ;
	assert(item_id != NULL) ;
	assert(item_name != NULL) ;

	flag = FALSE;
	low = 0;
	med = 0 ;
	high = syminfo->numelem;
	symtbl = syminfo->symidtbl;
	rc = SM_ID_NOT_FOUND ;

	while (low < high) {

		med = (low + high) / 2;
		// the next check is to stop this code going into infinite loop for
		// invalid numbers. It might have to be modified
		if (med == low) {
			if (!flag)
				flag = TRUE;
			else
				break ;
		}

		if (item_id < symtbl[med].item_id)

			high = med;

		else if (item_id > symtbl[med].item_id)

			low = med;

		else {
#ifdef _WIN32
			USES_CONVERSION;

			LPCWSTR item_nameW = A2CW(symtbl[med].item_name_ptr);	// UNICODE_CONVERSION
#else
			LPCWSTR item_nameW = new wchar_t[_MAX_PATH];//Allocating address.
			PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)symtbl[med].item_name_ptr, -1, (wchar_t*)item_nameW, _MAX_PATH);

#endif
			/*
			*	Check the length of the output buffer.  If the phrase to be output
			*	is longer than the output buffer, return an error code.  Otherwise,
			*	copy the phrase in the holding buffer into the output buffer.
			*/
			if ((size_t) outbuf_size <= wcslen(item_nameW))
			{
				item_name[0] = '\0';
				return DDI_INSUFFICIENT_BUFFER;
			} 
			else
			{
				(void)PS_Wcscpy(item_name, outbuf_size, item_nameW);
			}
			
			rc = SUCCESS;
			break ;
		}

	}

	//
	// Now, check to see if we actually found
	// the id.
	//

	if (rc != SUCCESS)
	{
		//
		// Couldn't find the silly id so put
		// a really cool squawk into the NT
		// Event Log.  If you're running 9x, sorry
		// no joy.
		//

		CStdString cstrTemp ;

		cstrTemp.Format(_T("Couldn't find an item name for ID %x"), item_id) ;
		EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"LegacyHART", cstrTemp);
		
	}
	return rc ;
}


// Optimized code
/*
int 
server_item_id_to_name(SYMINFO* syminfo, ITEM_ID item_id, LPTSTR item_name)
{
	int med;
	int	low = 0;
	int	high = syminfo->numelem;
	SYMTBL* symtbl = syminfo->symidtbl;

	while(low <= high) 
	{
		med = (low + high) / 2;
		if(item_id > symtbl[med].item_id)
			low = med + 1;
		else if(item_id < symtbl[med].item_id)
			high = med - 1;
		else 
		{
			strcpy((char*)item_name, symtbl[med].item_name_ptr);
			return SUCCESS;
		}
	}

	return SM_ID_NOT_FOUND;
}
*/

//
// Converts item-name to item_id.
//

// Original code
int 
server_item_name_to_id(ENV_INFO* env_info, SYMINFO* syminfo, LPCTSTR item_name, ITEM_ID *item_id, bool bBssLog /*= true*/)
{
	int	low;
	int med;
	int	high;
	SYMTBL* symtbl;
	BOOLEAN	flag;
	int rc ;

	assert(syminfo != NULL) ;
	assert(item_name != NULL) ;
	assert(item_id != NULL) ;

	char item_nameA[_MAX_PATH + 1];
	size_t size;
	errno_t err = PS_Wcstombs(&size, item_nameA, _MAX_PATH, item_name, _MAX_PATH);
	(void)err;

	flag = FALSE;
	low = 0;
	med = 0 ;
	high = syminfo->numelem;
	symtbl = syminfo->symnametbl;
	rc = SM_NAME_NOT_FOUND ;

	while (low < high) {
	
		med = (low + high) / 2;
		/* the next check is to stop this code going into infinite loop for
		 	invalid numbers. It might have to be modified */
		if (med == low) {
			if (!flag)
				flag = TRUE;
		 	else
				break ;
		}

		if (strcmp(item_nameA, symtbl[med].item_name_ptr) < 0) 

			high = med;

		else 	if (strcmp(item_nameA, symtbl[med].item_name_ptr) > 0) 

			low = med;

		else {

			*item_id = symtbl[med].item_id;
			rc = SUCCESS;
			break ;
		}

	}

	//
	// Now, check to see if we actually found the
	// name.
	//

	if (rc != SUCCESS && bBssLog)
	{
		//
		// Couldn't find the silly name so put
		// a really cool squawk into the NT
		// Event Log.  If you're running 9x, sorry
		// no joy.
		//

		CStdString cstrTemp ;

		cstrTemp.Format(_T("Couldn't find an item id for Name %s"), item_name) ;
		EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"LegacyHART", cstrTemp);
	}

	return rc ;
}


// Optimized code
/*
int 
server_item_name_to_id(SYMINFO* syminfo, char* item_name, ITEM_ID *item_id)
{
	int comp;
	int med;
	int	low = 0;
	int	high = syminfo->numelem;
	SYMTBL* symtbl = syminfo->symnametbl;

	while(low <= high) 
	{
		med = (low + high) / 2;
		comp = strcmp(item_name, symtbl[med].item_name_ptr);
		if(comp > 0) 
			low = med + 1;
		else if(comp < 0) 
			high = med - 1;
		else 
		{
			*item_id = symtbl[med].item_id;
			return SUCCESS;
		}
	}

	return SM_NAME_NOT_FOUND;
}
*/

//
////////////////////////////////////////////////////////////////////////////

