////////////////////////////////////////////////////////////////////////
////	Simple universal definitions.
////////////////////////////////////////////////////////////////////////

#ifndef STD_H
#define STD_H

/*
 *	Allow "prototyping" for reference, but use K&R syntax for code
 */

#define P(x)	x


#define	LITTLE_ENDIAN	1	/* Uses LITTLE_ENDIAN byte order */

typedef enum { FMS_FALSE, FMS_TRUE } fms_boolean;


/*
 *	For use with qsort() and bsearch()
 */

typedef int (*CMP_FN_PTR) P((const void *, const void *));



/*
 *  One global to set the appropriate message box flags based on operating
 *  system and how process is started.
 */

extern unsigned int message_box_flags;
void set_message_box_flags_for_nt( void );


/*
 *  One global to tell us if we are the fileserver.
 *  If non-zero, it contains the database file name.
 */

extern const char* fileserver;

/*
 *  One global to tell us who the Windows login user name is.
 *  And one to tell us what that user's database key is.
 */

extern unsigned long host_address;

/*
 *	Assert macros
 */

extern void panic (const char*, unsigned, const char *,...);

#define NELEM(x) (sizeof(x) / sizeof((x)[0]))

#undef VERIFY

#define REQUIRE(cond)	extern int __require[(cond)]

#define insist(x) { if (!(x)) (*(int*) 1) = 0; }

// ASSERT and CRASH Definitions
#ifdef DEBUG

#  define ASSERT_DBG(cond) \
	 if(!(cond)) panic(__FILE__, __LINE__, "%s", #cond)
#  define ASSERT_RET(cond,param) \
     if(!(cond)) panic(__FILE__, __LINE__, "Condition not true: passed value = %d\n", (param))
#  define CRASH_RET(param) \
     panic(__FILE__, __LINE__, "Crash: passed value = %d\n", (param))

#  define CRASH_DBG() \
	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Crash")
	
#else

#  define ASSERT_DBG(cond)
#  define ASSERT_RET(cond,param)	if(!(cond)) return (param)
#  define CRASH_RET(param)		return (param)
#  define CRASH_DBG()

#endif				/* DEBUG */

// VERIFY Definitions
#ifdef DEBUG

#if defined WIN32 || defined __WIN32__	
__declspec(dllimport) void __stdcall DebugBreak( void);
#endif

#  define VERIFY(x) (x)->verify()
#  define HAS_VERIFY void verify(void)
#  define VIRTUAL_VERIFY virtual void verify(void)
#  define VIRTUAL_VERIFY_NULL virtual void verify(void) = 0
#  define assume(x) x
 
void ftracef(char* where, char* fmt, ...);

#else

#  define VERIFY(x)
#  define HAS_VERIFY
#  define VIRTUAL_VERIFY
#  define VIRTUAL_VERIFY_NULL
#  define assume(x)

__inline void ftracef(char* where, char* fmt, ...) { char *x = fmt; x = where; }

#endif				/* DEBUG */

/*
 *	Standard defines
 */

#define FALSE 0
#define TRUE 1

/*
 *	Standard typedefs
 */

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

typedef unsigned char UINT8;
typedef short INT16;
typedef unsigned short UINT16;
typedef int INT32;
typedef unsigned int UINT32;

typedef ulong DDITEM;

struct BLOCK;

#define SB_CODE_PAGE	1252	// Code Page for singlebyte narrow/wide conversion

#endif				/* STD_H */
