/**
 *	@(#) $Id: fch_rodf.cpp,v 1.3 2012/08/08 23:48:48 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This module provides an interface between the Fetch routines
 *	and the ROD access functions
 *
 */

/* #includes */

#include "stdafx.h"
#include	"fch_lib.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"
#include "DeviceTypeMgr.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif

/* #defines */

/* typedefs */

/* structure definitions */

/* global variables */

/* local procedure declarations */


/*********************************************************************
 *
 *	Name: 	get_rod_object
 *
 *	ShortDesc: Get a ROD object
 *
 *	Description:
 *		This routine retrieves a ROD object using the Connection
 *		Manager functions and stores it in the scratchpad.
 *
 *	Inputs:
 *      handle - handle of the ROD that contains the object.
 *      index - index of the object.
 *
 *	Outputs:
 *		obj - 	pointer to a structure containing information about
 *				object, including pointers to the object extension
 *				and to the local data area
 *      sp - 	pointer to a structure containing a pointer to a
 *           	scratchpad memory area where the object will be copied
 *           	to, the total size of the scratchpad memory area, and
 *           	the amount of scratchpad memory used so far (increased
 *           	by the object size).
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int 
get_rod_object(  // fd_fdi
ENV_INFO		* /* env_info */,
ROD_HANDLE      handle,
OBJECT_INDEX    index,
OBJECT        **obj,
SCRATCH_PAD    *sp)
{
	unsigned char  *local_pad;
	OBJECT         *rod_obj;
	int             rcode, length;

	ASSERT_RET(obj && sp && sp->pad,
		FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(GET_ROD_OBJECT);
#endif
	
	/*
	 * Point to the first available location in the scratchpad
	 */

	local_pad = sp->pad + sp->used;

	/*
	 * Retrieve the object information from the ROD. Return if an error
	 * occurred or the returned object extension is NULL.
	 */

	rod_obj = (OBJECT *) 0L;/* initialize to NULL */

	rcode = g_DeviceTypeMgr.rod_get(handle, index, &rod_obj);

	if (rcode != SUCCESS) {
		return (FETCH_OBJECT_NOT_FOUND);
	}
	if (!rod_obj->extension) {
		return (FETCH_OBJECT_NOT_FOUND);
	}

	/*
	 * Copy the object extension into the scratchpad.  Byte 0 gives the
	 * extension length less 1 octet.
	 */
	// fd_fdi extension_length_size change
	length = (int) (rod_obj->ext_length);

	/*
	 * Return if not enough scratchpad to load the object extension.
	 */

	if ((sp->size - sp->used) < (unsigned long) length) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	memcpy((char *) local_pad, (char *) (rod_obj->extension), length);

	/*
	 * Offset the "used" field in the scratchpad structure
	 */

	*obj = rod_obj;

	sp->used += (unsigned long) length;

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: 	get_rod_object_size
 *
 *	ShortDesc: Get a ROD object size
 *
 *	Description:
 *		This routine retrieves a size of a ROD object using the Connection
 *		Manager functions
 *
 *	Inputs:
 *      handle - handle of the ROD that contains the object.
 *      index - index of the object.
 *
 *	Outputs:
 *		length - size
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int get_rod_object_size(  
ENV_INFO		* /* env_info */,
ROD_HANDLE      handle,
OBJECT_INDEX    index,
int *length)
{
	OBJECT         *rod_obj;
	int             rcode;

#ifdef DDSTEST
	TEST_FAIL(GET_ROD_OBJECT);
#endif

	/*
	 * Retrieve the object information from the ROD. Return if an error
	 * occurred or the returned object extension is NULL.
	 */

	rod_obj = (OBJECT *) 0L;/* initialize to NULL */

	rcode = g_DeviceTypeMgr.rod_get(handle, index, &rod_obj);

	if (rcode != SUCCESS) {
		return (FETCH_OBJECT_NOT_FOUND);
	}

	if (!rod_obj->extension) {
		return (FETCH_OBJECT_NOT_FOUND);
	}

	/*
	 * Copy the object extension into the scratchpad.  Byte 0 gives the
	 * extension length less 1 octet.
	 */

	*length = (int) (rod_obj->ext_length);

	return SUCCESS;
}

