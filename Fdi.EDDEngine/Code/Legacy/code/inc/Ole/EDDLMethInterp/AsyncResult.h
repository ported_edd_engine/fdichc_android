// AsyncExecute.h
//
//  .NET Asynchronous Design Pattern for C++ (ATL/WTL) using Windows
//  I/O Completion Ports
// 
// DESCRIPTION
//
// LIMITATIONS/RESTRICTIONS/DETAILS
// (See source code comments for in-depth information)
//
//   1. IAsyncResult should NEVER be used after calling EndInvoke() as
//      EndInvoke() sets it to NULL
//   2. EndInvoke() should always be called to terminate a BeginInvoke()
//      not calling EndInvoke will lead to a memory leak.
//   3. Calling EndInvoke() within a callback is safe.
//   4. There is no support for catching exceptions
//   5. Callback calls are not included in a call completion timeframe,
//      that is, a callback may well be executed after calling EndInvoke()
//   6. Callbacks are called from the receiving thread
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "nsEDDEngine/IMethods.h"
#include <mutex>
#include <cassert>
using nsEDDEngine::IAsyncResult;
using nsEDDEngine::IAsyncCallbackHandler;

// IClassDelegate
//
// Interface provided to decouple the template definition from the delegate
// class
//
/////////////////////////////////////////////////////////////////////////////

class IClassDelegate
{
public:
	virtual int Invoke() = 0;
    virtual void Cancel() = 0;
};


// CClassDelegate
//
// Class delegate is a wrapper object to keeps an instance/member function
// pointer reference
//
/////////////////////////////////////////////////////////////////////////////

template <class T>
class CClassDelegate : public IClassDelegate
{
    //TODO:LINUX:FIXIT
	// Delegate function signature
    typedef int(/*__thiscall*/ T::*LPFNCLSDELEGATE)();

public:
	CClassDelegate(T *pInstance, LPFNCLSDELEGATE pfn) :
		m_pInstance(pInstance),
		m_pfn(pfn)
	{ }

	~CClassDelegate()
	{
	}

	int Invoke()
	{
		return (m_pInstance->*m_pfn)();
	}

	// A Cancel() function is provided here since a client using
	// an IClassDelegate would not be able to cancel its class pointer
	// unless they could cast it to CClassDelegate<T> and since they are
	// not templatized the cast is not possible
    void Cancel()
	{
		if (m_pInstance != NULL)
		{
			m_pInstance->Cancel();
		}
	}

private:
	T *m_pInstance;
	LPFNCLSDELEGATE m_pfn;
};


// CAsyncResult
/////////////////////////////////////////////////////////////////////////////

class CAsyncResult : public IAsyncResult
{
	// A delegate associated function must comply to this signature
	// Cast both the result & parameter types as required
	friend class IClassDelegate;

public:

	CAsyncResult(IClassDelegate *pDelegate, IAsyncCallbackHandler *pCallback, void* pvState) :
		m_pDelegate(pDelegate),
		m_pCallback(pCallback),
		m_pvState(pvState),
		m_bIsCompleted(false),
		m_bIsCancelled(false),
		m_bIsCompletedSynchronously(false),
		m_iResult(nsEDDEngine::METH_SUCCESS),
		m_bIsEndMethodCalled(false),
		m_asyncResultEvent(NULL)
	{
		m_asyncResultEvent = new EventClass();
		BOOL createEvent = m_asyncResultEvent->Create();
		assert((createEvent!=0));
		(void)createEvent;
	}

	virtual ~CAsyncResult()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		//delete delegate once
		if (m_pDelegate != NULL)
		{
			delete m_pDelegate;
			m_pDelegate = NULL;
		}

		assert(m_asyncResultEvent->GetHandle() != NULL);
		BOOL bRes = m_asyncResultEvent->Close();
		assert(bRes != FALSE);
		(void)bRes;

		if (m_asyncResultEvent != NULL)
		{
			delete m_asyncResultEvent;
			m_asyncResultEvent = NULL;
		}
		
		// Release ownership of the critical section.
		m_cs.unlock();

	}

	//This is called in caller thread
	int Execute()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		m_asyncResultEvent->Reset();

		// Release ownership of the critical section.
		m_cs.unlock();

		assert(m_pDelegate != NULL);
		return (m_pDelegate->Invoke());
	}

	//This is called in working thread
	//Input argument execReturnCode: return code of delegate execution;
	//Input argument *pOutputValue: pointer to output value of delegate for variable action method only
	void Complete(nsEDDEngine::Mth_ErrorCode execReturnCode, nsConsumer::EVAL_VAR_VALUE *pOutputValue)	//execReturnCode::0 = success
	{
		// Request ownership of the critical section.
		m_cs.lock();

		m_iResult = execReturnCode;
		m_evvOutputValue = *pOutputValue;

		// Flag the call as completed
		assert(m_bIsCompleted == false);
		m_bIsCompleted = TRUE;

		// Signal the call has completed
		// Signaling will delete us only if there is no callback
		// This is important because we are used as the callback
		// parameter
		//UnlockAsyncResultEvent();
		assert(m_asyncResultEvent->GetHandle() != NULL);
		BOOL bRes = m_asyncResultEvent->Set();
		assert(bRes != false);
		(void)bRes;

		// Release ownership of the critical section.
		m_cs.unlock();

		// Invoke the callback if one is provided after signaling the call is completed 
		if (m_pCallback != (IAsyncCallbackHandler *)NULL)
		{
			m_pCallback->AsyncCallback((IAsyncResult*) this);
			/*!!! Important warning: This class instance may be deleted after callback function call
			* so that this instance shall not be accessed anymore!!!*/
		}
	}

	void CancelAsyncDelegate()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		bool bIsCompleted = m_bIsCompleted;
		m_bIsCancelled = true;

		// Release ownership of the critical section.
		m_cs.unlock();

		if ((m_pDelegate != NULL) && (!bIsCompleted))
		{
			m_pDelegate->Cancel();	//class T is cancelled and eventually deleted as well
		}
		return;
	}

	//This function returns false if m_bIsEndMethodCalled was false before and m_bIsEndMethodCalled is set to true now;
	//This function returns true if m_bIsEndMethodCalled is true;
	bool testNsetEndMethodCalled()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		bool bIsEndMethodCalled = m_bIsEndMethodCalled;
		if (!m_bIsEndMethodCalled)
		{
			m_bIsEndMethodCalled = true;
		}

		// Release ownership of the critical section.
		m_cs.unlock();

		return bIsEndMethodCalled;
	}

	// IAsyncResult /////////////////////////////////////////////////////////

	void * get_AsyncState()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		void* pvState = m_pvState;

		// Release ownership of the critical section.
		m_cs.unlock();

		return pvState;
	}

	// The wait handle is of type HANDLE in implementation
	// Because the external users do not wish to import windows
	// specific headers we return the handle as as void *.
	void * get_AsyncWaitHandle()
	{
	// Request ownership of the critical section.
	m_cs.lock();

	assert(m_asyncResultEvent->GetHandle() != NULL);
	HANDLE hAsyncWaitEvent = m_asyncResultEvent->GetHandle();

	// Release ownership of the critical section.
	m_cs.unlock();

	return (void *)hAsyncWaitEvent;
	}

	bool get_IsCompleted()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		bool bIsCompleted = m_bIsCompleted;

		// Release ownership of the critical section.
		m_cs.unlock();

		return bIsCompleted;
	}

	bool get_CompletedSynchronously()
	{
		// Request ownership of the critical section.
		m_cs.lock();

		bool bIsCompletedSynchronously = m_bIsCompletedSynchronously;

		// Release ownership of the critical section.
		m_cs.unlock();

		return bIsCompletedSynchronously;
	}

	// This function can only be called once the call has been completed
	nsEDDEngine::Mth_ErrorCode GetResult(nsConsumer::EVAL_VAR_VALUE *pValue)/* const*/
	{
		// Request ownership of the critical section.
		m_cs.lock();

		//returned result
		nsEDDEngine::Mth_ErrorCode iResult = m_iResult;
		if (pValue != NULL)
		{
			//output of delegate used by variable action method only
			*pValue = m_evvOutputValue;
		}

		// Release ownership of the critical section.
		m_cs.unlock();

		return iResult;
	}

private:
	IClassDelegate *m_pDelegate;
	IAsyncCallbackHandler *m_pCallback;
	void* m_pvState;
	bool m_bIsCompleted;
	bool m_bIsCancelled;
	bool m_bIsCompletedSynchronously;
	nsEDDEngine::Mth_ErrorCode m_iResult;
	bool m_bIsEndMethodCalled;
	std::mutex m_cs;
	nsConsumer::EVAL_VAR_VALUE m_evvOutputValue;	//output value of delegate used by variable action method onl

public:
	EventClass* m_asyncResultEvent;
	
};

