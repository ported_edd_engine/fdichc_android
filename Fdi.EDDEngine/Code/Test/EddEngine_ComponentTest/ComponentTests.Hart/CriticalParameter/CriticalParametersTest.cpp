#include "TestSupport/EddEngineController.h"


using namespace nsEDDEngine;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        namespace CriticalParameter
        {          
            [TestClass, Ignore]
            public ref class BaseTestCriticalParameters
            {
            private:

                static String^ criticalParameter;
                static String^ expectedParameter;

            public:
                static void SetupBase(String^ testCaseName, String^ expected)
                {
                    expectedParameter = expected;

                    EddEngineController^ engine = gcnew EddEngineController(testCaseName);                
                    engine->Initialize();
                    int blockInstance = 0;
                    nsEDDEngine::CRIT_PARAM_TBL *pCriticalParamTbl;
                    engine->Instance->GetCriticalParams(blockInstance, &pCriticalParamTbl);

                    List<String^>^ params = gcnew List<String^>();
                    for (int i = 0; i < pCriticalParamTbl->count; i++)
                    {
                        RESOLVED_REFERENCE& ref = pCriticalParamTbl->list[i];
                        if (ref.size == 1)
                        {
                            params->Add(String::Format("{0}", ref.pResolvedRef[0]));
                        }
                        else if (ref.size == 2)
                        {
                            params->Add(String::Format("{0}.{1}", ref.pResolvedRef[0], ref.pResolvedRef[1]));
                        }
                    }
                    delete engine;


                    criticalParameter = String::Join(", ", params);
                }

                [TestMethod, TestCategory("CriticalParameterTest")]
                void It_should_return_the_expected_critical_parameters()
                {
                    Assert::AreEqual(expectedParameter, criticalParameter);
                }

            };

            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0001_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0001.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0002_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0002.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0003_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0003.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0004_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "16390, 16392, 16394, 153, 16389";
                    SetupBase("Hart\\TestCase0004.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0005_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0005.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0006_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0006.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0007_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0007.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0008_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "16388, 153";
                    SetupBase("Hart\\TestCase0008.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0009_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "16389, 153";
                    SetupBase("Hart\\TestCase0009.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000A_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000A.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000B_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000B.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000C_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000C.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000D_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000D.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000E_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000E.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase000F_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase000F.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0010_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "16391, 153";
                    SetupBase("Hart\\TestCase0010.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0011_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0011.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0012_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0012.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0013_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0013.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0014_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0014.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0015_FMA : BaseTestCriticalParameters

            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0015.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0016_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0016.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0017_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0017.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0018_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0018.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0019_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0019.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase001A_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase001A.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase001B_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase001B.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase001C_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase001C.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase001D_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase001D.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase001E_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase001E.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0020_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0020.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0021_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0021.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0022_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0022.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0023_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0023.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0024_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0024.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0025_FMA : BaseTestCriticalParameters

            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0025.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0026_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0026.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0027_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0027.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0028_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0028.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0029_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0029.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002A_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002A.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002B_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002B.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002C_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002C.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002D_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002D.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002E_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002E.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase002F_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase002F.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0030_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0030.fmA", expectedResult);
                }
            };
            [TestClass]
            public ref class When_getting_the_critical_Parameters_for_TestCase0031_FMA : BaseTestCriticalParameters
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    String^ expectedResult = "153";
                    SetupBase("Hart\\TestCase0031.fmA", expectedResult);
                }
            };
        }
    } // namespace Hart
} // namespace ComponentTests
