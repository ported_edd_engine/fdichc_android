#include "stdafx.h"
//#include <comdef.h>
#include "float.h"
#include <climits>

#include <nsEDDEngine/Common.h>
using namespace nsEDDEngine;
#include "MethodInterfaceDefs.h"
#include <Ole/EDDLMethInterp/MiEngine.h>
#include "Ole/EDDLMethInterp/AsyncResult.h"
#include "ServerProjects/rtn_code.h"
#include <Ole/EDDLMethInterp/pathtype.h>
#include "APPS/APPsupport/BuiltinLib/BI_CODES.H"
#ifdef _METHOD_INTERP_ONLY_
#include "Profibus\AMSPBDds\env_info.h"		//GETDICTIONARYSTRING
#include <Profibus/amspbserver/sm_lower.h>	//path.h and build_methd_def_from_action_methd_ref_with_args()
#endif
#include "nsEDDEngine/AttributeName.h"
#include "ddbdefs.h"
#include <Inf/NtcSpecies/Ntcassert2.h>
#include <Inf/NtcSpecies/BssProgLog.h>
#include "varientTag.h"




//If UI builtin needs more debug info, uncomment the following line
//#define UIBUILTIN_DEBUG	1

//define static members

// Initialize static variables
bool CMiEngine::m_bmethodCompleted = false;



CMiEngine::CMiEngine(BLOCK_INSTANCE iBlockInstance, void* pValueSpec, nsConsumer::EVAL_VAR_VALUE *pActionValue, ACTION* pAction, nsConsumer::IBuiltinSupport *pIBuiltinSupport, nsConsumer::IEDDEngineLogger *pEDDEngineLogger, ProtocolType protocol, const wchar_t* sLanguageCode, IMiDataFinder *pDataFinder)
#ifdef _METHOD_INTERP_ONLY_
	: 
//	 EVENT(_T(__FILE__), __LINE__),	// MHD used to schedule the CMiEngine on the Server work queue
//	 m_sinkMiEngine(this),		// MHD used to keep Server resources open

#endif
{
	if(sLanguageCode != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(engine_sLanguageCode, maxSize, sLanguageCode, _TRUNCATE);	
	}

	m_ManagedEngine = ManagedServerMiFactory::CreateManagedServerMi(this);

	// Save parameters that are passed in
	m_iBlockInstance = iBlockInstance;
	m_pValueSpec = pValueSpec;
	m_pCurrentUICallbackState = NULL;
	m_bmethodCompleted = false;
	//get protocol families as one of the three
	switch (protocol)
	{
	case ProtocolType::HART:
		m_protocol = ProtocolType::HART;
		break;
	case ProtocolType::FF:
	case ProtocolType::ISA100:
		m_protocol = ProtocolType::FF;
		break;
	case ProtocolType::PROFIBUS:
	case ProtocolType::PROFIBUS_PN:
	case ProtocolType::GPE:				//generic protocol extensions
	case ProtocolType::COMSERVER:		//communication server
		m_protocol = ProtocolType::PROFIBUS;
		break;
	}
	m_pIBuiltinSupport = pIBuiltinSupport;
	m_pIEDDEngineLogger = pEDDEngineLogger;

	// Initialize other member variables
	m_iMethodCompleteReturnCode = nsEDDEngine::METH_SUCCESS;

	m_currentState = nsMiEngine::READY;		//MI engine is created and ready to execute a method

	m_Action = *pAction;
	m_MethodID = EMBEDDED_METHOD_ID;			//regular method
	if (pActionValue)
	{
		m_evvMethodInputValue = *pActionValue;
		m_MethodID = EMBEDDED_ACTION_ID;		//action method
	}

	m_pDataFinder = pDataFinder;
	m_iDdsErrorCode = DDS_SUCCESS;
	// Initialize the critical section one time only.
	
	
	ulUICount = 0;
	m_bmethodCompleted = false;
}

//
// destructor
//
CMiEngine::~CMiEngine()
{
	(void)waitForMiEngineComplete();
	
	
}


//
// This starts the method execution.  This is essentially the state machine of the engine.  When the 
// method interpreter needs work to be done, the managed component will get triggered and put the 
// engine back into the event queue.  When the server is done doing work for the engine, the sink will
// also put the engine back into the event queue.
//
int CMiEngine::ExecuteMethod()
{
	int rc = -1;

	switch(GetState())
	{
	case nsMiEngine::READY:
		{
			std::unique_lock<std::mutex> sLock(m_CriticalSection);
		
			ResetMethodCompleteEvent();
			
			rc = m_ManagedEngine->ExecuteMethod(m_MethodID, m_protocol);

			if (rc == 0)
			{
				m_currentState = nsMiEngine::WAITING_ON_MI;
				rc = PC_BUSY;
			}
			else
			{
				CStdString strLog;
                strLog.Format(L"Failed to execute method, method ID: %d", m_MethodID);
				MILog(strLog.GetBuffer(), nsConsumer::Warning, L"MI", __FILE__, __LINE__);
			}

			sLock.unlock();
		}
		break;
	case nsMiEngine::WAITING_ON_ENGINE_MICOMPLETE:
		rc = (int)OnMethodInterpreterComplete();
		break;
	case nsMiEngine::WAITING_ON_MI:
		MILog(L"Invalid request: CMiEngine::ExecuteMethod executed in state WAITING_ON_MI", nsConsumer::Warning, L"MI", __FILE__, __LINE__);
		break;
	default:
		MILog(L"Invalid request: CMiEngine::ExecuteMethod executed in unknown state", nsConsumer::Warning, L"MI", __FILE__, __LINE__);
		break;
	}

	return rc; 
}


//Cancel MI Engine process
void CMiEngine::Cancel()
{
	// Request ownership of the critical section.
	mi_uiBltinCriticalSection.lock(); 

	m_ManagedEngine->SetCancelled();	// Send cancel signal to the next level

	if (m_pCurrentUICallbackState)
	{
		//This callback state instance has been processed. Don't do it again in UICallback()
		m_pCurrentUICallbackState->m_bProcessed = true;
		//Mark the current callback state done
		m_pCurrentUICallbackState = NULL;
	}

	//release event for UI builtin which may be waiting on
	m_ManagedEngine->SetMethodUIEvent();

	// Release ownership of the critical section.
	mi_uiBltinCriticalSection.unlock();
}


int CMiEngine::OnCommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize)
{
	int rc = BLTIN_NOT_IMPLEMENTED;
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;
	CStdString displayString;


	//ChangeState(nsMiEngine::WAITING_ON_ENGINE_EXECUTE);	//MI engine is waiting for consumer to finish action request
	// call builtin support for communication commands
	if ( (pchMoreDataInfo != NULL) && (commandNumber == 48) && (transactionNumber == 0 /*DEFAULT_TRANSACTION_NUMBER*/) )
	{
		preLogInterfaceAccess(L"GetMoreStatus", NULL);

		bsCode = m_pIBuiltinSupport->GetMoreStatus(m_pValueSpec, pucCmdStatus, pchMoreDataInfo, &iMoreDataSize);

		displayString.Format(L"GetMoreStatus with command statuses 0x%X, 0x%X, 0x%X, more data size %d and returned value %d ", pucCmdStatus[0], pucCmdStatus[1], pucCmdStatus[2], iMoreDataSize, (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}
	else
	{
		preLogInterfaceAccess(L"SendCmdTrans", NULL);

		bsCode = m_pIBuiltinSupport->SendCmdTrans(m_pValueSpec, commandNumber, transactionNumber, pucCmdStatus);
		iMoreDataSize = 0;

		displayString.Format(L"SendCmdTrans with command %d statuses 0x%X, 0x%X, 0x%X and returned value %d ", commandNumber, pucCmdStatus[0], pucCmdStatus[1], pucCmdStatus[2], (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	rc = ConvertBSCodeToErrorCode(bsCode);
	return (rc);
}


// This function read or write parameter dynamic attribute
int CMiEngine::OnAccessDynamicAttributeExecute(const PARAM_REF *pParamRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType)
{
	int rc = BLTIN_NOT_IMPLEMENTED;
	nsConsumer::PC_ErrorCode pcCode = nsConsumer::PC_OTHER_EC;

	
	if (eAccessType == AccessType::READ)
	{
		preLogInterfaceAccess(L"GetDynamicAttribute", NULL);
	}
	else
	{
		preLogInterfaceAccess(L"SetDynamicAttribute", NULL);
	}

	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function

	//convert to OP_REF operative ID
	OP_REF OpRef;
	rc = convertParamRefToOpRef(iBlockInstance, pParamRef, &OpRef);
	if (rc != BLTIN_SUCCESS)
	{
		return rc;
	}

	variableType_t eVarType = vT_undefined;
	long lVarSize = 0;
	if (eAccessType == AccessType::WRITE)
	{
		//get variable type and size
		ITEM_ID idPrimaryID = 0;	// Find out what the primary Item Id is
		ITEM_ID idPrimaryMemberID = 0;	// Find out what the primary Member Id is
		if (pParamRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			idPrimaryID = pParamRef->op_info.id;
			idPrimaryMemberID = pParamRef->op_info.member;
		}
		else
		{
			idPrimaryID = pParamRef->op_info_list.list[pParamRef->op_info_list.count - 1].id;
			idPrimaryMemberID = pParamRef->op_info_list.list[pParamRef->op_info_list.count - 1].member;
		}

		CValueVarient vtType{};
		CValueVarient vtSize{};

		//get variable type
		int ddsCode1 = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, idPrimaryID, idPrimaryMemberID, PP_Type, &vtType, engine_sLanguageCode );
			
		//get variable size
		if (ddsCode1 == BLTIN_SUCCESS)
		{
			ddsCode1 = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, idPrimaryID, idPrimaryMemberID, PP_Size, &vtSize, engine_sLanguageCode );
		}
		rc = ConvertDdsCodeToBuiltinCode(ddsCode1);

		if (rc == BLTIN_SUCCESS)
		{
			
			eVarType = (variableType_t)CV_I4(&vtType);

			
			lVarSize = CV_I4(&vtSize);
		}
		else
		{
			return rc;
		}
	}

	// call builtin support for parameter list operation
	nsConsumer::EVAL_VAR_VALUE EvalVarValue;
	switch (ulAttribute)
	{
	case varAttrFirst:
		if (eAccessType == AccessType::READ)
		{
			pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::first, &EvalVarValue);
			if (pcCode == PC_SUCCESS)
			{
				rc = convertEvalToVariant(&EvalVarValue, pParamValue);
			}
			else
			{
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		else
		{
			rc = convertVariantToEval((CValueVarient *)pParamValue, eVarType, lVarSize, &EvalVarValue);
			if (rc == BLTIN_SUCCESS)
			{
				pcCode = m_pIBuiltinSupport->SetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::first, &EvalVarValue);
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		break;
	case varAttrLast:
		if (eAccessType == AccessType::READ)
		{
			pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::last, &EvalVarValue);
			if (pcCode == PC_SUCCESS)
			{
				rc = convertEvalToVariant(&EvalVarValue, pParamValue);
			}
			else
			{
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		else
		{
			rc = convertVariantToEval((CValueVarient *)pParamValue, eVarType, lVarSize, &EvalVarValue);
			if (rc == BLTIN_SUCCESS)
			{
				pcCode = m_pIBuiltinSupport->SetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::last, &EvalVarValue);
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		break;
	case varAttrCount:
		{
			pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::count, &EvalVarValue);
			if (pcCode == PC_SUCCESS)
			{
				rc = convertEvalToVariant(&EvalVarValue, pParamValue);
			}
			else
			{
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		break;
	case varAttrViewMin:
		{
			if (eAccessType == AccessType::READ)

			{
				pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::view_min, &EvalVarValue);
				if (pcCode == PC_SUCCESS)
				{
					rc = convertEvalToVariant(&EvalVarValue, pParamValue);
				}
				else
				{
					rc = ConvertPCCodeToBuiltinCode(pcCode);
				}
			}
			else
			{
				//there is no type/size definition for VIEW_MIN in DD so that we cann't use eVarType and lVarType here
				convertCValueVarientToEval(pParamValue, &EvalVarValue);
				pcCode = m_pIBuiltinSupport->SetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::view_min, &EvalVarValue);
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
	break;
	case varAttrViewMax:
		{
			if (eAccessType == AccessType::READ)

			{
				pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::view_max, &EvalVarValue);
				if (pcCode == PC_SUCCESS)
				{
					rc = convertEvalToVariant(&EvalVarValue, pParamValue);
				}
				else
				{
					rc = ConvertPCCodeToBuiltinCode(pcCode);
				}
			}
			else
			{
				//there is no type/size definition for VIEW_MAX in DD so that we cann't use eVarType and lVarType here
				convertCValueVarientToEval(pParamValue, &EvalVarValue);
				pcCode = m_pIBuiltinSupport->SetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::view_max, &EvalVarValue);
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}

		}
		break;
	case varAttrVariableStatus:
		//This is VARIABLE_STATUS_* keyword access. For keyword values, please check enum VariableStatusValue definition in Attrs.h
		if (eAccessType == AccessType::READ)
		{
			pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::variable_status, &EvalVarValue);
			if (pcCode == PC_SUCCESS)
			{
				rc = convertEvalToVariant(&EvalVarValue, pParamValue);
			}
			else
			{
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		else
		{
			rc = convertVariantToEval((CValueVarient *)pParamValue, vT_Unsigned, 4, &EvalVarValue);
			if (rc == BLTIN_SUCCESS)
			{
				pcCode = m_pIBuiltinSupport->SetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::variable_status, &EvalVarValue);
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		break;
	case varAttrScalingFactor:
		if (eAccessType == AccessType::READ)
		{
			pcCode = m_pIBuiltinSupport->GetDynamicAttribute(iBlockInstance, m_pValueSpec, &OpRef, nsEDDEngine::AttributeName::scaling_factor, &EvalVarValue);
			if (pcCode == PC_SUCCESS)
			{
				rc = convertEvalToVariant(&EvalVarValue, pParamValue);
			}
			else
			{
				rc = ConvertPCCodeToBuiltinCode(pcCode);
			}
		}
		break;
	default:
		return BLTIN_BAD_ITEM_TYPE;
	}

	CStdString displayString;
	if (eAccessType == AccessType::READ)
	{
		displayString.Format(L"GetDynamicAttribute with returned value %d ", (int)pcCode);
		postLogInterfaceAccess((LPCTSTR)displayString, &EvalVarValue);
	}
	else
	{
		displayString.Format(L"SetDynamicAttribute with returned value %d ", (int)pcCode);
		postLogInterfaceAccess((LPCTSTR)displayString, &EvalVarValue);
	}

	return (rc);
}

/***********************************************************************
 *
 *	Name:  OnListOpExecute
 *
 *	ShortDesc:  Execute either an insert or delete on a list or embedded list.
 *
 *	Description:
 *		Based on the parameter values, instructs the BuiltinSupport to either
 *		insert or delete a member into/from a list or embedded list.
 *
 *	Reference:
 *		IEC 61804-5 Builtin ListDeleteElementAt, Builtin ListDeleteElementAt2, Builtin ListInsert, Builtin ListInsert2
 *
 *	Inputs:
 *		if delete (ListDeleteElementAt)
 *			ulListID:		ID of List on which the action is to be performed
 *			iListIndex:		Index of the element to delete
 *			ulEmbListID:	A value of zero indicating no embeded list (not ListDeleteElementAt2)
 *			iEmbIndex:		Ignored value
 *			ulItemID:		A zero Value indicating a delete operation
 *			iCount:			Value forced to 1 by this method; only one element shall be deleted.
 *		if delete embedded (ListDeleteElementAt2)
 *			ulListID:		ID of Element containing the embeded list on which the action is to be performed
 *			iListIndex:		Index to the embedded list containing the elements to delete
 *			ulEmbListID:	The ID of the embedded_list
 *			iEmbIndex:		Index of the first element to deleted
 *			ulItemID:		A zero Value indicating a delete operation
 *			iCount:			Number of elements to delete from the embedded_list
 *		if insert (ListInsert)
 *			ulListID:		ID of List on which the action is to be performed
 *			iListIndex:		Index at which to insert the new item
 *			ulEmbListID:	A value of zero indicating no embeded list (not ListInsert2)
 *			iEmbIndex:		Ignored value
 *			ulItemID:		Item ID of item to insert into list
 *			iCount:			Ignored Value
 *		if insert embeded (ListInsert2)
 *			ulListID:		ID of Element containing the embeded list on which the action is to be performed
 *			iListIndex:		Index to the embedded list in which the insert shall occur
 *			ulEmbListID:	The ID of the embedded_list
 *			iEmbIndex:		Index at which to insert the new item
 *			ulItemID:		Item ID of item to insert into list
 *			iCount:			Ignored Value
 *
 *	Outputs:
 *
 *	Returns:
 *		BLTIN_SUCCESS on success, other member of BLTIN_ErrorCode on failure
 *
 **********************************************************************/
int CMiEngine::OnListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iEmbIndex, unsigned long ulInsertItemId, int iCount)
{
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;
	CStdString displayString;

	OP_REF	listOpRef;					// Holds the OP_REF of the list being manipulated
	int		iInsertDeleteIndex = 0;		// This is the index that we want to "insert at" or "start deletion from"
	

	if(ulEmbListId == 0)		// If there is no embedded list, then we can use a STANDARD_TYPE of op_ref
	{
		listOpRef.op_ref_type = OpRefType::STANDARD_TYPE;
		listOpRef.op_info.id = ulListId;
		listOpRef.op_info.member = 0;
		listOpRef.op_info.type = m_pDataFinder->GetItemType(m_iBlockInstance, ulListId);

		iInsertDeleteIndex = iListIndex;

		iCount = 1;	// if we don't have an embedded list, this value is always 1.
	}
	else
	{
		listOpRef.op_ref_type = OpRefType::COMPLEX_TYPE;

		listOpRef.op_info_list.list = new OP_REF_INFO[2];
		listOpRef.op_info_list.count = 2;

		listOpRef.op_info_list.list[0].id = ulListId;
		listOpRef.op_info_list.list[0].member = iListIndex;
		listOpRef.op_info_list.list[0].type = m_pDataFinder->GetItemType(m_iBlockInstance, ulListId);

		listOpRef.op_info_list.list[1].id = ulEmbListId;
		listOpRef.op_info_list.list[1].member = 0;
		listOpRef.op_info_list.list[1].type = m_pDataFinder->GetItemType(m_iBlockInstance, ulEmbListId);

		iInsertDeleteIndex = iEmbIndex;
	}

	// call builtin support for parameter list operation

	if (ulInsertItemId != 0)	// if we have a ulInsertItemId, we know we need to insert it
	{
		OP_REF insertedOpRef;

		insertedOpRef.op_ref_type = OpRefType::STANDARD_TYPE;

		insertedOpRef.op_info.id = ulInsertItemId;
		insertedOpRef.op_info.member = 0;
		insertedOpRef.op_info.type = m_pDataFinder->GetItemType(m_iBlockInstance, ulInsertItemId);
		
		preLogInterfaceAccess(L"ListInsert", NULL);

		bsCode = m_pIBuiltinSupport->ListInsert(m_iBlockInstance, m_pValueSpec, &listOpRef, iInsertDeleteIndex, &insertedOpRef);

		displayString.Format(L"ListInsert with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}
	else	// delete at
	{
		preLogInterfaceAccess(L"ListDeleteElementAt", NULL);

		bsCode = m_pIBuiltinSupport->ListDeleteElementAt(m_iBlockInstance, m_pValueSpec, &listOpRef, iInsertDeleteIndex, iCount);

		displayString.Format(L"ListDeleteElementAt with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	int rc = ConvertBSCodeToErrorCode(bsCode);
	return (rc);
}


void CMiEngine::UICallback (nsConsumer::EVAL_VAR_VALUE *pvUserUIInput, nsConsumer::BS_ErrorCode eStatus, void *state)
{
	CUICallbackState *pCallbackState = (CUICallbackState *)state;

	//if the callback has not be processed, for example, by CancelMethod(), do this
	if (!pCallbackState->m_bProcessed)
	{
		CMiEngine *pThis = pCallbackState->m_pMiEngine;

		// Request ownership of the critical section.
		pThis->mi_uiBltinCriticalSection.lock(); 

		pThis->m_eUIStatus = eStatus;
		if (pvUserUIInput != nullptr)
		{
			pThis->convertEvalToVariant(pvUserUIInput, &(pThis->m_vUIInput));
		}

		//Mark the current callback state done
		pThis->m_pCurrentUICallbackState = NULL;

#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling UI builtin callback trigger function. Returned value: %d. The count: %u.\n", pThis->ulUICount, eStatus);
#endif
		pThis->m_ManagedEngine->SetMethodUIEvent();

		// Release ownership of the critical section.
		pThis->mi_uiBltinCriticalSection.unlock();
	}

	delete pCallbackState;
}

//This routine is used by FF only
int CMiEngine::OnSendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType)
{
	int rc = BLTIN_SUCCESS;
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;
	CStdString displayString;
	int ddsCode = DDS_SUCCESS;

	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function
	ITEM_ID ulBlockItemId = pOpRef->ulBlockItemId;
	unsigned long iOccurrence = pOpRef->ulOccurrence;

	if (ulBlockItemId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(ulBlockItemId, iOccurrence, &iBlockInstance);
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	}
	
	if ( rc == BLTIN_SUCCESS )
	{
		nsEDDEngine::FDI_PARAM_SPECIFIER paramSpec;
		rc = convertParamRefToParamSpec(iBlockInstance, pOpRef, &paramSpec);

		if (rc == BLTIN_SUCCESS)
		{
			if (iAccessType == AccessType::WRITE)
			{
				preLogInterfaceAccess(L"WriteValue", NULL, &paramSpec);

				bsCode = m_pIBuiltinSupport->WriteValue(iBlockInstance, m_pValueSpec, &paramSpec);
				rc = ConvertBSCodeToErrorCode(bsCode);

				displayString.Format(L"WriteValue with returned value %d ", (int)bsCode);
				postLogInterfaceAccess((LPCTSTR)displayString);
			}
			else if (iAccessType == AccessType::READ)
			{
				preLogInterfaceAccess(L"ReadValue", NULL, &paramSpec);

				bsCode = m_pIBuiltinSupport->ReadValue(iBlockInstance, m_pValueSpec, &paramSpec);
				rc = ConvertBSCodeToErrorCode(bsCode);

				displayString.Format(L"ReadValue with returned value %d ", (int)bsCode);
				postLogInterfaceAccess((LPCTSTR)displayString);
			}
			else
			{
				rc = BLTIN_WRONG_DATA_TYPE;
			}
		}
	}

	return (rc);
}

int CMiEngine::engineAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType)
{
	int rc = BLTIN_SUCCESS;
	preLogInterfaceAccess(L"ProcessChangedValues", NULL);

	nsConsumer::BS_ErrorCode bsCode = m_pIBuiltinSupport->ProcessChangedValues(m_iBlockInstance, m_pValueSpec, eAccessType);
	rc = ConvertBSCodeToErrorCode(bsCode);

	CStdString displayString;
	displayString.Format(L"ProcessChangedValues with returned value %d ", (int)bsCode);
	postLogInterfaceAccess((LPCTSTR)displayString);

	return rc;
}

nsConsumer::BS_ErrorCode CMiEngine::OnUIExecuteText(ACTION_UI_DATA& stUIData)
{
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;
	CStdString displayString;


	if (stUIData.bMethodAbortedSignalToUI)
	{
		preLogInterfaceAccess(L"AbortRequest", stUIData.textMessage.pchTextMessage);

		//ask for abort response and wait for button hit
		if (stUIData.textMessage.pchTextMessage != NULL)
		{
			//ask for abort response and wait for button hit
			nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
			m_pCurrentUICallbackState = new CUICallbackState(this);
			bsCode = m_pIBuiltinSupport->AbortRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, pFunc, m_pCurrentUICallbackState );

#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling AbortRequest(). The count: %u.\n", ulUICount);
#endif
		}
		else
		{
			//inform abort state
			bsCode = m_pIBuiltinSupport->AbortRequest( m_pValueSpec, NULL, NULL, nullptr);

#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling AbortRequest().\n");
#endif
		}

		displayString.Format(L"AbortRequest with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}
	else if (stUIData.bUserAcknowledge)	
	{
		if (stUIData.bDisplayDynamic)
		{
			//ask for info response with updated message
			bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec,  stUIData.textMessage.pchTextMessage, true);
		}
		else
		{
			preLogInterfaceAccess(L"AcknowledgementRequest", stUIData.textMessage.pchTextMessage);

			nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
			m_pCurrentUICallbackState = new CUICallbackState(this);
			bsCode = m_pIBuiltinSupport->AcknowledgementRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, pFunc, m_pCurrentUICallbackState);
		
#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling AcknowledgementRequest(). The count: %u.\n", ulUICount);
#endif

			displayString.Format(L"AcknowledgementRequest with returned value %d ", (int)bsCode);
			postLogInterfaceAccess((LPCTSTR)displayString);
		}
	}
	else if (stUIData.bDelayBuiltin)
	{
		if (stUIData.bDisplayDynamic)
		{
			//ask for info response with updated message
			bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec,  stUIData.textMessage.pchTextMessage, true);
		}
		else
		{
			preLogInterfaceAccess(L"DelayMessageRequest", stUIData.textMessage.pchTextMessage);

			//ask for delay message response
			nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
			m_pCurrentUICallbackState = new CUICallbackState(this);
			bsCode = m_pIBuiltinSupport->DelayMessageRequest(m_pValueSpec, stUIData.uDelayTime, stUIData.textMessage.pchTextMessage, pFunc, m_pCurrentUICallbackState);
		
#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling DelayMessageRequest(). The count: %u.\n", ulUICount);
#endif

			displayString.Format(L"DelayMessageRequest with returned value %d ", (int)bsCode);
			postLogInterfaceAccess((LPCTSTR)displayString);
		}
	}
	else
	{
		preLogInterfaceAccess(L"InfoRequest", stUIData.textMessage.pchTextMessage);

		//ask for info response and return immediately
		bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, false);

#ifdef UIBUILTIN_DEBUG
		PS_TRACE(L"Calling InfoRequest(). The count: %u.\n", ulUICount);
#endif

		displayString.Format(L"InfoRequest with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	return bsCode;
}

nsConsumer::BS_ErrorCode CMiEngine::OnUIExecuteEditDeviceParam(ACTION_UI_DATA& stUIData)
{
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_SUCCESS;

	// input request
	if (stUIData.bDisplayDynamic)
	{
		//ask for info response with updated message
		bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, true);
	}
	else
	{
		//ask for input message response
		BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function

		//get block instance
		if (stUIData.opRef.ulBlockItemId != 0)
		{
			int ddsCode = m_pDataFinder->ConvertToBlockInstance(stUIData.opRef.ulBlockItemId, stUIData.opRef.ulOccurrence, &iBlockInstance);
			BLTIN_ErrorCode btCode = ConvertDdsCodeToBuiltinCode(ddsCode);
			if (btCode != BLTIN_SUCCESS)
			{
				bsCode = nsConsumer::BSEC_OTHER;
			}
		}

		if (bsCode == nsConsumer::BSEC_SUCCESS)
		{

			OP_REF_TRAIL op_ref_trail;
			BLTIN_ErrorCode btCode = convertParamRefToOpRefTrail(iBlockInstance, &(stUIData.opRef), &op_ref_trail);

			if (btCode == BLTIN_SUCCESS)
			{
				preLogInterfaceAccess(L"ParameterInputRequest", stUIData.textMessage.pchTextMessage);

				nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
				m_pCurrentUICallbackState = new CUICallbackState(this);
				bsCode = m_pIBuiltinSupport->ParameterInputRequest(iBlockInstance, m_pValueSpec, stUIData.textMessage.pchTextMessage, /* in */ &op_ref_trail, pFunc, m_pCurrentUICallbackState);

#ifdef UIBUILTIN_DEBUG
				PS_TRACE(L"Calling ParameterInputRequest(). The count: %u.\n", ulUICount);
#endif

				CStdString displayString;
				displayString.Format(L"ParameterInputRequest with returned value %d ", (int)bsCode);
				postLogInterfaceAccess((LPCTSTR)displayString);
			}
			else
			{
				bsCode = nsConsumer::BSEC_OTHER;
			}
		}
	}

	return bsCode;
}

nsConsumer::BS_ErrorCode CMiEngine::OnUIExecuteEditLocal(ACTION_UI_DATA& stUIData)
{
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;


	// input request
	if (stUIData.bDisplayDynamic)
	{
		//ask for info response with updated message
		bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, true);
	}
	else
	{
		//ask for input message response
		nsConsumer::EVAL_VAR_VALUE pValue;
		nsEDDEngine::FLAT_VAR *pVar = nullptr;

		BLTIN_ErrorCode btCode = convertVariantToEval(&stUIData.EditBox.vtValue, stUIData.EditBox.varType, stUIData.EditBox.iMaxStringLength, &pValue);
		if (btCode == BLTIN_SUCCESS)
		{
			preLogInterfaceAccess(L"InputRequest", stUIData.textMessage.pchTextMessage);

			nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
			m_pCurrentUICallbackState = new CUICallbackState(this);
			bsCode = m_pIBuiltinSupport->InputRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, /* in */ &pValue, pVar, pFunc, m_pCurrentUICallbackState );
				
#ifdef UIBUILTIN_DEBUG
			PS_TRACE(L"Calling InputRequest(). The count: %u.\n", ulUICount);
#endif

			CStdString displayString;
			displayString.Format(L"InputRequest with returned value %d ", (int)bsCode);
			postLogInterfaceAccess((LPCTSTR)displayString);
		}

		if (pVar != nullptr)
		{
			delete pVar;
		}
	}

	return bsCode;
}

nsConsumer::BS_ErrorCode CMiEngine::OnUIExecuteSelection(ACTION_UI_DATA& stUIData)
{
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;

	
	if (stUIData.bDisplayDynamic)
	{
		//ask for info response with updated message
		bsCode = m_pIBuiltinSupport->InfoRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, true);
	}
	else
	{
		preLogInterfaceAccess(L"SelectionRequest", stUIData.textMessage.pchTextMessage);
		if (stUIData.ComboBox.iNumberOfComboElements > 0 )
		{
			appendLogInterfaceAccess(L"UI Builtin Selection List: \"%s\"\n", stUIData.ComboBox.pchComboElementText);
		}

		//selection request
		nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
		m_pCurrentUICallbackState = new CUICallbackState(this);
		bsCode = m_pIBuiltinSupport->SelectionRequest( m_pValueSpec, stUIData.textMessage.pchTextMessage, stUIData.ComboBox.pchComboElementText, pFunc, m_pCurrentUICallbackState);
				
#ifdef UIBUILTIN_DEBUG
		PS_TRACE(L"Calling SelectionRequest(). The count: %u.\n", ulUICount);
#endif

		CStdString displayString;
		displayString.Format(L"SelectionRequest with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	return bsCode;
}

int CMiEngine::OnUIExecute(ACTION_UI_DATA& stUIData)
{
	int rc = BLTIN_NOT_IMPLEMENTED;
	nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;

	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function

	//get block instance
	if (stUIData.opRef.ulBlockItemId != 0)
	{
		int ddsCode = m_pDataFinder->ConvertToBlockInstance(stUIData.opRef.ulBlockItemId, stUIData.opRef.ulOccurrence, &iBlockInstance);
		BLTIN_ErrorCode btCode = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (btCode != BLTIN_SUCCESS)
		{
			return BI_ERROR;
		}
	}

	//ChangeState(nsMiEngine::WAITING_ON_ENGINE_EXECUTE);
	// save UI event handle
	// Request ownership of the critical section.
	mi_uiBltinCriticalSection.lock(); 

	ulUICount++;

	// Release ownership of the critical section.
	mi_uiBltinCriticalSection.unlock();

	// call builtin support for UI
	if (stUIData.userInterfaceDataType == TEXT_MESSAGE)
	{
		bsCode = OnUIExecuteText(stUIData);
	}
	else if (stUIData.userInterfaceDataType == EDIT_DEVICE_PARAM)
	{
		bsCode = OnUIExecuteEditDeviceParam(stUIData);
	}
	else if (stUIData.userInterfaceDataType == EDIT_LOCAL)
	{
		bsCode = OnUIExecuteEditLocal(stUIData);
	}
	else if ((stUIData.userInterfaceDataType == SELECTION) && (stUIData.ComboBox.iNumberOfComboElements > 0))
	{
		bsCode = OnUIExecuteSelection(stUIData);
	}
	else if ((stUIData.userInterfaceDataType == MENU) && (stUIData.ComboBox.iNumberOfComboElements > 0))
	{
		preLogInterfaceAccess(L"UIDRequest", stUIData.textMessage.pchTextMessage);
		if (stUIData.ComboBox.iNumberOfComboElements > 0 )
		{
			appendLogInterfaceAccess(L"UI Builtin Selection List: \"%s\"\n", stUIData.ComboBox.pchComboElementText);
		}

		//UID request
		nsConsumer::IBuiltinSupport::AckCallback pFunc = &(this->UICallback);
		m_pCurrentUICallbackState = new CUICallbackState(this);
		bsCode = m_pIBuiltinSupport->UIDRequest( iBlockInstance, m_pValueSpec, stUIData.opRef.op_info.id, stUIData.ComboBox.pchComboElementText, pFunc, m_pCurrentUICallbackState);
		
#ifdef UIBUILTIN_DEBUG
		PS_TRACE(L"Calling UIDRequest(). The count: %u.\n", ulUICount);
#endif

		CStdString displayString;
		displayString.Format(L"UIDRequest with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	rc = ConvertBSCodeToErrorCode(bsCode);
	return (rc);
}

//This function delivers user input value and user response (status) to the UI builtin caller
int CMiEngine::OnUIMethRspExecute(CValueVarient& vtInputValue)
{
	BI_ErrorCode rc = BI_ERROR;

	preLogInterfaceAccess(L"OnUIMethRspExecute", NULL);

	// Request ownership of the critical section.
	mi_uiBltinCriticalSection.lock();
	
	vtInputValue = m_vUIInput;
	// Copy UI input value
	m_vUIInput.clear();

	//Convert UI input status
	switch (m_eUIStatus)
	{
	case nsConsumer::BSEC_SUCCESS:
		rc = BI_SUCCESS;
		break;
	case nsConsumer::BSEC_ABORTED:
		rc = BI_ABORT;
		break;
	default:
		rc = BI_ERROR;
		break;
	}

#ifdef UIBUILTIN_DEBUG
	PS_TRACE(L"Calling UI builtin callback response function. Returned value: %d. The count: %u.\n", ulUICount, rc);
#endif

	// Release ownership of the critical section.
	mi_uiBltinCriticalSection.unlock();

	CStdString displayString;
	displayString.Format(L"OnUIMethRspExecute with returned value %d ", (int)m_eUIStatus);
	postLogInterfaceAccess((LPCTSTR)displayString);

	return rc;
}

int CMiEngine::OnRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol)
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->GetResponseCodeString( m_iBlockInstance, m_pValueSpec, nCommandNumber, nResponseCode, pvtValue, protocol, engine_sLanguageCode );
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}


int CMiEngine::OnDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->GetDictionaryString( ulIndex, pString, iStringLen, engine_sLanguageCode );
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}


int CMiEngine::OnLitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->GetDevSpecString( ulIndex, pString, iStringLen, engine_sLanguageCode );
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}


void CMiEngine::OnCommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity)
{
	
	
	switch (onExitActivity)
	{
	case Discard:
		preLogInterfaceAccess(L"OnMethodExiting", _T("modified value being discarded"));
		break;
	case Save:
		preLogInterfaceAccess(L"OnMethodExiting", _T("modified value being saved"));
		break;
	case Send:
		preLogInterfaceAccess(L"OnMethodExiting", _T("modified value being sent"));
		break;
	}

	//ChangeState(nsMiEngine::WAITING_ON_ENGINE_EXECUTE);
	m_pIBuiltinSupport->OnMethodExiting(m_iBlockInstance, m_pValueSpec, onExitActivity);

	postLogInterfaceAccess(L"OnMethodExiting");
}

void CMiEngine::LockMethodCompleteEvent()
{
	std::unique_lock <std::mutex> condVarLock(m_methodCompleteWaitLock);
	m_methodCompleteCondVariable.wait(condVarLock, []
	{
		return m_bmethodCompleted;
	});
}

void CMiEngine::UnlockMethodCompleteEvent()
{
	m_bmethodCompleted = true;
	std::unique_lock <std::mutex> condVarLock(m_methodCompleteWaitLock);
	condVarLock.unlock();
	m_methodCompleteCondVariable.notify_one();
}

void CMiEngine::ResetMethodCompleteEvent()
{
	m_bmethodCompleted = false;	
}

void CMiEngine::OnMethodComplete(nsEDDEngine::Mth_ErrorCode iMethodCompleteReturnCode)
{
	// Method Interpreter is done.  Perform cleanup on main server thread:
	m_iMethodCompleteReturnCode = iMethodCompleteReturnCode;
	ChangeState(nsMiEngine::WAITING_ON_ENGINE_MICOMPLETE);	//MI engine is waiting for the method completion by itself
	
	ExecuteMethod();
}

// This routine is used in ~ManagedServerMi() for making sure the thread is completed
bool CMiEngine::waitForMiEngineComplete()
{
	bool bRetVal = false;	//timeout

	//wait for method complete
	LockMethodCompleteEvent();
	// reset the flag
	ResetMethodCompleteEvent();
	bRetVal = true;

	return bRetVal;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// This is a request for the Server to GetDictionaryString.
//
int CMiEngine::engineGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen )
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	try
	{
		int ddsCode = m_pDataFinder->GetDictionaryString( sDictionaryKey, sValue, iStringLen, engine_sLanguageCode );
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	}
	catch (...)
	{
		MILog(L"Unable to perform engineGetDictionaryString(): Exception Caught", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	}

	return (rc);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// This is the notification that the method has completed
//
/*LRESULT*/ int CMiEngine::OnMethodInterpreterComplete()
{
	ChangeState(nsMiEngine::WAITING_ON_SERVER_EXECUTE);	//MI engine is waiting for the caller thread to complete the method
		
	switch (m_iMethodCompleteReturnCode)
	{
	case METH_SUCCESS:
		preLogInterfaceAccess(L"is done. MI Engine OnMethodInterpreterComplete", _T("method success"));
		break;
	case METH_ABORTED:
		preLogInterfaceAccess(L"Is Done. MI Engine OnMethodInterpreterComplete", _T("method aborted"));
		break;
	case METH_CANCELLED:
		preLogInterfaceAccess(L"Is Done. MI Engine OnMethodInterpreterComplete", _T("method is cancelled"));
		break;
	case METH_FAILED:
		preLogInterfaceAccess(L"Is Done. MI Engine OnMethodInterpreterComplete", _T("method failed"));
		break;
	default:
		assert(0);
		break;
	}
	postLogInterfaceAccess(nullptr);

	if (pAsyncResult != NULL)
	{
		if (m_evvMethodReturnValue.type != VT_DDS_TYPE_UNUSED)
		{
			preLogInterfaceAccess(L"EndMethod", NULL);
			postLogInterfaceAccess(L"EndMethod", &m_evvMethodReturnValue);

			((CAsyncResult *)pAsyncResult)->Complete(m_iMethodCompleteReturnCode, &m_evvMethodReturnValue);	//callback executes inside
		}
		else
		{
			((CAsyncResult *)pAsyncResult)->Complete(m_iMethodCompleteReturnCode, &m_evvMethodInputValue);	//callback executes inside
		}
	}

	// signal MiEngine is done to the caller thread
	UnlockMethodCompleteEvent();
	

#ifdef UIBUILTIN_DEBUG
	PS_TRACE(L"CMiEngine::OnMethodInterpreterComplete[%p]-Complete[%d]\n", this, m_iMethodCompleteReturnCode);
#endif

	return 0;
}


//This function turns item name to either item ID or member ID
int CMiEngine::ResolveItemNameToID( CStdString sItemName, unsigned long *memberID )
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->ResolveNameToID( sItemName, memberID );
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}


//
//
//
int CMiEngine::GetValueByItemID2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, long lPropertyType, CValueVarient *pvtValue )
{
	BLTIN_ErrorCode nRetVal = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = DDS_SUCCESS;
	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;
	

	//initialization
	pvtValue->clear();

	//find the block instance
	if (ulBlkId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(ulBlkId, ulBlkNum, &iBlockInstance);
		nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (nRetVal != BLTIN_SUCCESS)
		{
			return nRetVal;
		}
	}

	if ((ulItemId == EMBEDDED_METHOD_ID) || (ulItemId == EMBEDDED_ACTION_ID))
	{
		//get method or action attribute
		switch(lPropertyType)
		{
		case PP_Kind:
			CV_VT(pvtValue, ValueTagType::CVT_I4);
			CV_I4(pvtValue) = PT_METHOD;
			nRetVal = BLTIN_SUCCESS;
			break;
		case PP_Name:
			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			CV_WSTR(pvtValue) = _T("EmbeddedMethod");
			nRetVal = BLTIN_SUCCESS;
			break;
		case PP_Definition:
			switch (m_Action.eType)
			{
			case ACTION::ACTION_TYPE_DEFINITION:
				{
					CStdString methodDef(m_Action.action.meth_definition.data, m_Action.action.meth_definition.size);
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					CV_WSTR(pvtValue) = methodDef;
					nRetVal = BLTIN_SUCCESS;
				}
				break;
			case ACTION::ACTION_TYPE_REFERENCE:
				{
					ddsCode = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, m_Action.action.meth_ref, 0, lPropertyType, pvtValue, engine_sLanguageCode );
					nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
				}
				break;
			case ACTION::ACTION_TYPE_REF_WITH_ARGS:
				{
					CStdString methodDef;

					//get method name from method ID
					ddsCode = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, m_Action.action.meth_ref_args.desc_id, 0, PP_Name, pvtValue, engine_sLanguageCode );
					nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
				
					//get method calling method string
					if ((nRetVal == BLTIN_SUCCESS) && (pvtValue->vTagType == ValueTagType::CVT_WSTR))
					{
						methodDef.Format(L"{\n\t%s(%s);\n}", CV_WSTR(pvtValue), m_Action.action.meth_ref_args.expr.val.s.c_str());

						pvtValue->clear();
						CV_VT(pvtValue, ValueTagType::CVT_WSTR);
						CV_WSTR(pvtValue) = methodDef;
					}
				}
				break;
			}
			break;
		case PP_Type:	//get method return type
			switch (m_Action.eType)
			{
			case ACTION::ACTION_TYPE_DEFINITION:
				{
					CV_VT(pvtValue, ValueTagType::CVT_I4);
					CV_I4(pvtValue) = 0;				//no method type defined, i.e. void type
					nRetVal = BLTIN_SUCCESS;
				}
				break;
			case ACTION::ACTION_TYPE_REFERENCE:
				{
					ddsCode = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, m_Action.action.meth_ref, 0, lPropertyType, pvtValue, engine_sLanguageCode );
					nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
				}
				break;
			case ACTION::ACTION_TYPE_REF_WITH_ARGS:
				{
					ddsCode = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, m_Action.action.meth_ref_args.desc_id, 0, lPropertyType, pvtValue, engine_sLanguageCode );
					nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);				
				}
				break;
			}
			break;
		default:
			nRetVal = BLTIN_WRONG_DATA_TYPE;
			break;
		}
	}
	else
	{
		//ChangeState(nsMiEngine::WAITING_ON_ENGINE_GET);	//MI engine is waiting for DDS to get data
		switch (lPropertyType)
		{
		case PP_Value:	//shalln't be used here. but in case...
			MILog(L"Value request is not valid in this function.", nsConsumer::Error, L"MI", __FILE__, __LINE__);
			break;
		default:
			{
				ddsCode = m_pDataFinder->GetValue( iBlockInstance, m_pValueSpec, ulItemId, ulMemberId, lPropertyType, pvtValue, engine_sLanguageCode );
				nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
			}
			break;
		}
	}

	return nRetVal;
}

int CMiEngine::engineGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput)
{
	BLTIN_ErrorCode nRetVal = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = DDS_SUCCESS;
	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;


	//find the block instance
	if (ulBlkId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(ulBlkId, ulBlkNum, &iBlockInstance);
		nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (nRetVal != BLTIN_SUCCESS)
		{
			return nRetVal;
		}
	}

	ddsCode = m_pDataFinder->GetActionList(iBlockInstance, m_pValueSpec, ulItemID, pOutput, engine_sLanguageCode);
	nRetVal = ConvertDdsCodeToBuiltinCode(ddsCode);

	return nRetVal;
}

#ifdef _METHOD_INTERP_ONLY_
//
//
//
int CMiEngine::SetValueByItemName( LPCTSTR /*sItemName*/, long /*lPropertyType*/, CValueVarient /*vtValue*/ )
{
	int nRetVal = PC_SUCCESS;

	ITEM_ID lItemId = 0;

	nRetVal = m_DataFinder.ResolveNameToID( sItemName, &lItemId );
	if( nRetVal == PC_SUCCESS )
	{
		nRetVal = m_DataFinder.SetValue( lItemId, lPropertyType, vtValue );
		if( nRetVal == PC_SUCCESS )
		{
			bool bFound = false;
            for( int nIndex = 0; nIndex < m_waSavedValues.size(); nIndex++ )
			{
				if( lItemId == m_waSavedValues[nIndex] )
				{
					bFound = true;
				}
			}
			if( !bFound )
			{
                m_waSavedValues.push_back( (WORD)lItemId );
			}
		}
	}

	return nRetVal;
}

//
//
//
int CMiEngine::SetValueByItemID( ITEM_ID lItemID, long lPropertyType, CValueVarient vtValue )
{
	int nRetVal = PC_SUCCESS;

	int nRetVal = m_DataFinder.SetValue( lItemID, lPropertyType, vtValue );
	if( nRetVal == PC_SUCCESS )
	{
		bool bFound = false;
        for( int nIndex = 0; nIndex < m_waSavedValues.size(); nIndex++ )
		{
			if( lItemID == m_waSavedValues[nIndex] )
			{
				bFound = true;
			}
		}
		if( !bFound )
		{
            m_waSavedValues.push_back((WORD)lItemID );
		}
	}
	return nRetVal;
}
#endif


void CMiEngine::ChangeState(nsMiEngine::enumMiEngineState newState)
{
	std::unique_lock<std::mutex> sLock(m_CriticalSection);

	m_currentState = newState;

}

nsMiEngine::enumMiEngineState CMiEngine::GetState()
{
	std::unique_lock<std::mutex> sLock(m_CriticalSection);

	nsMiEngine::enumMiEngineState eState= m_currentState;

	return eState;
}

int CMiEngine::OnResponseCodeStringExecute(ITEM_ID iItemId, ITEM_ID iMemberId, int iRsponseCode, CValueVarient *pvtValue)
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->GetResponseCodeString( m_iBlockInstance, m_pValueSpec, iItemId, iMemberId, iRsponseCode, pvtValue, engine_sLanguageCode );
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	
	return (rc);
}

int CMiEngine::OnDdsErrorExecute(wchar_t* pErrorString)
{
	wchar_t* pString = m_pDataFinder->GetErrorString(m_iDdsErrorCode);

	wcscpy(pErrorString, pString);

	return (m_iDdsErrorCode);
}

//This function is used by Fieldbus. The return code expects code starting with BLTIN_
unsigned long CMiEngine::OnCommErrorExecute()
{
	unsigned long ulError = (unsigned long) BLTIN_CANNOT_READ_VARIABLE;
	preLogInterfaceAccess(L"GetCommError", NULL);
	
	nsConsumer::BS_ErrorCode bsCode = m_pIBuiltinSupport->GetCommError(m_pValueSpec, &ulError);
	
	if (bsCode != nsConsumer::BSEC_SUCCESS)
	{
		ulError = (unsigned long) BLTIN_CANNOT_READ_VARIABLE;
	}

	CStdString displayString;
	displayString.Format(L"GetCommError with returned value %d ", (int)bsCode);
	postLogInterfaceAccess((LPCTSTR)displayString);

	return ulError;
}

int CMiEngine::OnResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId)
{
	int rc = BLTIN_FAIL_RESPONSE_CODE;
	preLogInterfaceAccess(L"GetResponseCode", NULL);

    INT8 Error_Class = 0;
    INT8 Error_Code = 0;
	INT16 Addl_Code = 0;
	SUBINDEX ulSubindex = 0;

    nsConsumer::BS_ErrorCode bsCode = m_pIBuiltinSupport->GetResponseCode(m_pValueSpec, &Error_Class, &Error_Code, &Addl_Code, pulErrorItemId, &ulSubindex);
	
	CStdString displayString;
	displayString.Format(L"GetResponseCode with returned value %d ", (int)bsCode);
	postLogInterfaceAccess((LPCTSTR)displayString);

	if (bsCode == nsConsumer::BSEC_SUCCESS)
	{
		*pulResponseCode = ((unsigned long)Error_Class << 24) | ((unsigned long)Error_Code << 16) | (unsigned long)Addl_Code;

		if ((m_pDataFinder->GetItemType(m_iBlockInstance, *pulErrorItemId) == ITYPE_RECORD) && (ulSubindex > 0))
		{
			//get RECORD variable member ID
			int ddsCode = m_pDataFinder->LookupMemberId(m_iBlockInstance, m_pValueSpec, *pulErrorItemId, ulSubindex, engine_sLanguageCode, pulErrorMemberId);
			rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		}
		else
		{
			*pulErrorMemberId = ulSubindex;
			rc = BLTIN_SUCCESS;
		}
	}
	else
	{
		rc = ConvertBSCodeToErrorCode(bsCode);
	}

	return rc;
}

int CMiEngine::engineAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	int ddsCode = DDS_SUCCESS;
	void* pValueSpec = m_pValueSpec;
	
	ITEM_ID uldst_BlockItemId = pdst_opRef->ulBlockItemId;
	unsigned long uldst_Occurrence = pdst_opRef->ulOccurrence;
	ITEM_ID ulsrc_BlockItemId = psrc_opRef->ulBlockItemId;
	unsigned long ulsrc_Occurrence = psrc_opRef->ulOccurrence;

	BLOCK_INSTANCE iDstBlockInstance = 0;
	BLOCK_INSTANCE iSrcBlockInstance = 0;

	if(uldst_BlockItemId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(uldst_BlockItemId, uldst_Occurrence, &iDstBlockInstance);
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (rc != BLTIN_SUCCESS)
		{
			return rc;
		}
	}
	else
	{
		iDstBlockInstance = m_iBlockInstance;
	}

	nsEDDEngine::FDI_PARAM_SPECIFIER paramSpecDst;
	rc = convertParamRefToParamSpec(iDstBlockInstance, pdst_opRef, &paramSpecDst);
	if (rc != BLTIN_SUCCESS)
	{
		return rc;
	}

	if(ulsrc_BlockItemId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(ulsrc_BlockItemId, ulsrc_Occurrence, &iSrcBlockInstance);
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (rc != BLTIN_SUCCESS)
		{
			return rc;
		}
	}
	else
	{
		iSrcBlockInstance = m_iBlockInstance;
	}

	nsEDDEngine::FDI_PARAM_SPECIFIER paramSpecSrc;
	rc = convertParamRefToParamSpec(iSrcBlockInstance, psrc_opRef, &paramSpecSrc);

	if (rc == BLTIN_SUCCESS)
	{
		preLogInterfaceAccess(L"AssignParamValue", L"source parameter", &paramSpecSrc);
		preLogInterfaceAccess(L"AssignParamValue", L"destination parameter", &paramSpecDst);

		nsConsumer::PC_ErrorCode ec = m_pIBuiltinSupport->AssignParamValue(pValueSpec, iDstBlockInstance, &paramSpecDst, iSrcBlockInstance, &paramSpecSrc);
		rc = ConvertPCCodeToBuiltinCode(ec);

		CStdString displayString;
		displayString.Format(L"AssignParamValue with returned value %d ", (int)ec);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	return rc;
}

ITEM_ID CMiEngine::OnBlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType)
{
	int iDDSCode = 0;
	ITEM_ID ulRetVal = 0;
	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function

	//get iBlockInstance based on input arguments
	if (ulBlockItemId != 0)
	{
		iDDSCode = m_pDataFinder->ConvertToBlockInstance(ulBlockItemId, iOccurrence, &iBlockInstance);
	}

	//error if block ID is invalid for the case of cross block
	if ((iDDSCode != 0) || (iBlockInstance <= 0))
	{
		iDDSCode = CM_NO_BLOCK;
	}
	
	if (ConvertDdsCodeToBuiltinCode(iDDSCode) == BLTIN_SUCCESS)	//save DDS code and check if it is success
	{
		ulRetVal  = m_pDataFinder->resolveBlockRef(iBlockInstance, m_pValueSpec, ulMemberId, eResolveType, engine_sLanguageCode);
	}
	
	return ulRetVal;
}

int CMiEngine::engineAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *vtValue, variableType_t eValType, AccessType eAccessType, long lSize)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	int ddsCode = DDS_SUCCESS;
	BLOCK_INSTANCE iBlockInstance = m_iBlockInstance;		//local to this function


	ITEM_ID ulBlkId = pOpRef->ulBlockItemId;
	unsigned long ulBlkNum = pOpRef->ulOccurrence;
	if (ulBlkId != 0)
	{
		ddsCode = m_pDataFinder->ConvertToBlockInstance(ulBlkId, ulBlkNum, &iBlockInstance);
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	}

	if (rc == BLTIN_SUCCESS)
	{
		void* pValueSpec = m_pValueSpec;
		nsConsumer::EVAL_VAR_VALUE evalValue;

		nsEDDEngine::FDI_PARAM_SPECIFIER paramSpec;
		rc = convertParamRefToParamSpec(iBlockInstance, pOpRef, &paramSpec);

		if (rc == BLTIN_SUCCESS)
		{
			nsConsumer::PC_ErrorCode ec = nsConsumer::PC_OTHER_EC;
			if (eAccessType == AccessType::READ)
			{
				//NOTE: when reading, arguments eValType and lSize are not available, i.e. they are zeros.
				preLogInterfaceAccess(L"GetParamValue", NULL, &paramSpec);

				ec = m_pIBuiltinSupport->GetParamValue(iBlockInstance, pValueSpec, &paramSpec, &evalValue);
				rc = ConvertPCCodeToBuiltinCode(ec);
				if (rc == BLTIN_SUCCESS)
				{
					rc = convertEvalToVariant(&evalValue, vtValue);
				}

				CStdString displayString;
				displayString.Format(L"GetParamValue with returned value %d ", (int)ec);
				postLogInterfaceAccess((LPCTSTR)displayString, &evalValue);
			}
			else if (eAccessType == AccessType::WRITE)
			{
				rc = convertVariantToEval((CValueVarient *)vtValue, eValType, lSize, &evalValue);
				if (rc == BLTIN_SUCCESS)
				{
					preLogInterfaceAccess(L"SetParamValue", NULL, &paramSpec);

					ec = m_pIBuiltinSupport->SetParamValue(iBlockInstance, pValueSpec, &paramSpec, &evalValue);
					rc = ConvertPCCodeToBuiltinCode(ec);

					CStdString displayString;
					displayString.Format(L"SetParamValue with returned value %d ", (int)ec);
					postLogInterfaceAccess((LPCTSTR)displayString, &evalValue);
				}
			}
			else
			{
				rc = BLTIN_WRONG_DATA_TYPE;
			}
		}
	}

	return (rc);
}

//This function reads or writes action variable value only if input parameter pAction is NULL.
//This function reads or writes action and action variable value if input parameter pAction is not NULL.
int CMiEngine::engineAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;


	if (eAccessType == AccessType::READ)
	{
		if (pAction != NULL)
		{
			//read action
			*pAction = m_Action;
		}

		if (pActionVal != NULL)
		{
			//read action value
			*pActionVal = m_evvMethodInputValue;
		}
	}
	else if (eAccessType == AccessType::WRITE)
	{
		if (pAction != NULL)
		{
			//write action
			m_Action = *pAction;
		}

		if (pActionVal != NULL)
		{
			//write action value
			m_evvMethodInputValue = *pActionVal;
		}
	}
	else
	{
		rc = BLTIN_WRONG_DATA_TYPE;
	}

	return (rc);
}

//This function writes the method returned variable value
int CMiEngine::engineWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal)
{
	if (pReturnedVal->type != VT_DDS_TYPE_UNUSED)
	{
		m_evvMethodReturnValue = *pReturnedVal;
	}
	return (BLTIN_SUCCESS);
}

BLTIN_ErrorCode CMiEngine::convertEvalToVariant(nsConsumer::EVAL_VAR_VALUE *pevalValue, CValueVarient *vtValue)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;

	switch (pevalValue->type)
	{
	case VT_ASCII:
	case VT_PASSWORD:
	case VT_PACKED_ASCII:
	case VT_EUC:
	case VT_VISIBLESTRING:
		CV_VT(vtValue, ValueTagType::CVT_WSTR);
		CV_WSTR(vtValue) = (wchar_t*)pevalValue->val.s.c_str();
		break;
	case VT_BITSTRING:	// unsigned char[]
	case VT_OCTETSTRING:
		CV_VT(vtValue, ValueTagType::CVT_BITSTR);
		CV_BITSTR(vtValue, pevalValue->val.b.ptr(), pevalValue->val.b.length());		
		break;
	case VT_TIME_VALUE:
		if (pevalValue->size <= 4)
		{
			CV_VT(vtValue, ValueTagType::CVT_UI4);
            CV_UI4(vtValue) = (ULONG)pevalValue->val.u;
		}
		else
		{
			CV_VT(vtValue, ValueTagType::CVT_I8);
			CV_I8(vtValue) = pevalValue->val.i;
		}
		break;
	case VT_UNSIGNED:
	case VT_ENUMERATED:
	case VT_BIT_ENUMERATED:
	case VT_INDEX:
	case VT_BOOLEAN:
	case VT_DATE_AND_TIME:
	case VT_TIME:
	case VT_DURATION:
	case VT_OBJECT_REFERENCE:
		if (pevalValue->size == 1)
		{
			CV_VT(vtValue, ValueTagType::CVT_UI1);
			CV_UI1(vtValue) = (unsigned char)pevalValue->val.u;
		}
		else if (pevalValue->size == 2)
		{
			CV_VT(vtValue, ValueTagType::CVT_UI2);
			CV_UI2(vtValue) = (unsigned short)pevalValue->val.u;
		}
		else if (pevalValue->size <= 4)
		{
			CV_VT(vtValue, ValueTagType::CVT_UI4);
			CV_UI4(vtValue) = (ULONG)pevalValue->val.u;
		}
		else
		{
			CV_VT(vtValue, ValueTagType::CVT_UI8);
			CV_UI8(vtValue) = pevalValue->val.u;
		}
		break;
	case VT_FLOAT:	//4 byte real
		CV_VT(vtValue, ValueTagType::CVT_R4);
		CV_R4(vtValue) = pevalValue->val.f;
		break;
	case VT_DOUBLE:	//8 byte real
		CV_VT(vtValue, ValueTagType::CVT_R8);
		CV_R8(vtValue) = pevalValue->val.d;
		break;
	case VT_EDD_DATE:
	case VT_INTEGER:
		if (pevalValue->size == 1)
		{
			CV_VT(vtValue, ValueTagType::CVT_I1);
			CV_I1(vtValue) = (char)pevalValue->val.i;
		}
		else if (pevalValue->size == 2)
		{
			CV_VT(vtValue, ValueTagType::CVT_I2);
			CV_I2(vtValue) = (short)pevalValue->val.i;
		}
		else if (pevalValue->size <= 4)
		{
			CV_VT(vtValue, ValueTagType::CVT_I4);
			CV_I4(vtValue) = (LONG)pevalValue->val.i;
		}
		else
		{
			CV_VT(vtValue, ValueTagType::CVT_I8);
			CV_I8(vtValue) = pevalValue->val.i;
		}
		break;
	default:
		vtValue->clear();
		break;
	}

	return (rc);
}

void CMiEngine::convertCValueVarientToEval(/* in */ CValueVarient *pvtSrcValue, /* out */ nsConsumer::EVAL_VAR_VALUE *evalDstValue)
{
	//assign size
	evalDstValue->size = pvtSrcValue->vSize;

	//convert type and value
	switch (pvtSrcValue->vTagType)
	{
	case ValueTagType::CVT_BOOL:
	{
		evalDstValue->type = VT_BOOLEAN;
		evalDstValue->val.u = CV_BOOL(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_I1:
	{
		evalDstValue->type = VT_INTEGER;
		evalDstValue->val.i = CV_I1(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_I2:
	{
		evalDstValue->type = VT_INTEGER;
		evalDstValue->val.i = CV_I2(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_I4:
	{
		evalDstValue->type = VT_INTEGER;
		evalDstValue->val.i = CV_I4(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_INT:
	{
		evalDstValue->type = VT_INTEGER;
		evalDstValue->val.i = CV_INT(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_I8:
	{
		evalDstValue->type = VT_INTEGER;
		evalDstValue->val.i = CV_I8(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_UI1:
	{
		evalDstValue->type = VT_UNSIGNED;
		evalDstValue->val.u = CV_UI1(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_UI2:
	{
		evalDstValue->type = VT_UNSIGNED;
		evalDstValue->val.u = CV_UI2(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_UI4:
	{
		evalDstValue->type = VT_UNSIGNED;
		evalDstValue->val.u = CV_UI4(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_UINT:
	{
		evalDstValue->type = VT_UNSIGNED;
		evalDstValue->val.u = CV_UINT(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_UI8:
	{
		evalDstValue->type = VT_UNSIGNED;
		evalDstValue->val.u = CV_UI8(pvtSrcValue);
	}
	break;
	case ValueTagType::CVT_R4:	//4 byte real
		{
		evalDstValue->type = VT_FLOAT;
		evalDstValue->val.f = static_cast<float>(CV_R4(pvtSrcValue));
		}
		break;
	case ValueTagType::CVT_R8:	//8 byte real
		{
		evalDstValue->type = VT_DOUBLE;
		evalDstValue->val.d = CV_R8(pvtSrcValue);
		}
		break;
	case ValueTagType::CVT_STR:
	{
		evalDstValue->type = VT_ASCII;
		wstring sVal = wstring(CV_STR(pvtSrcValue).begin(), CV_STR(pvtSrcValue).end());
		evalDstValue->val.s.assign(sVal.c_str(), true);
	}
	break;
	case ValueTagType::CVT_WSTR:
	{
		evalDstValue->type = VT_ASCII;
		evalDstValue->val.s.assign(CV_WSTR(pvtSrcValue).c_str(), true);
	}
	break;
	case ValueTagType::CVT_BITSTR:
	{
		evalDstValue->type = VT_BITSTRING;
		evalDstValue->val.b.assign(CV_BITSTR_BUFFER(pvtSrcValue), CV_BITSTR_LEN(pvtSrcValue));
	}
	break;
	default:	//CVT_EMPTY
		break;
	}

	return;
}

BLTIN_ErrorCode CMiEngine::convertVariantToEval(/* in */ CValueVarient *pvtSrcValue, /* in */ variableType_t eValType, /* in */ long lSize, /* out */ nsConsumer::EVAL_VAR_VALUE *evalDstValue)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;

	//convert type
	convertVariableType(eValType, &evalDstValue->type);

	//vonvert value
	switch (eValType)
	{
	case vT_Ascii:
	case vT_PackedAscii:
	case vT_Password:
	case vT_EUC:
	case vT_VisibleString:
		{
			
			if (pvtSrcValue->vTagType == ValueTagType::CVT_WSTR)
			{
				evalDstValue->val.s = CV_WSTR(pvtSrcValue).c_str();
			}
			else
			{
				evalDstValue->val.s = CV_STR(pvtSrcValue).c_str();
			}

				if(lSize >= 0) // If container size is available
				{
					if(evalDstValue->val.s.length() > (unsigned int)lSize) // If new string size is greater than the size defined in DD
					{
						wchar_t *tmpStr=new wchar_t[lSize + 1];
						PS_Wcsncpy(tmpStr, lSize + 1, evalDstValue->val.s.c_str(), _TRUNCATE); // Truncate the new string to the exact size defined in DD
						evalDstValue->val.s = tmpStr;

						delete [] tmpStr;
						tmpStr = NULL;
					}

					evalDstValue->size = lSize;
				}
				else
				{
					evalDstValue->size = evalDstValue->val.s.length();
				}
			//}
			//else
			//{
				//evalDstValue->size = lSize;
			//}
		}
		break;
	case vT_BitString:	// unsigned char[]
	case vT_OctetString:
		{
			if (pvtSrcValue->vTagType == ValueTagType::CVT_BITSTR)
			{
				ULONG uSize = CV_BITSTR_LEN(pvtSrcValue);
				evalDstValue->val.b.assign(CV_BITSTR_BUFFER(pvtSrcValue), uSize);
				evalDstValue->size = uSize;			
			}
			else
			{
				rc = (BLTIN_ErrorCode)BI_ERROR;
			}
		}
		break;
	case vT_Integer:
	case vT_HartDate:
	{
		evalDstValue->size = lSize;

		//retieve source value
		INT64 int64Value = 0;
		rc = GetINT64FromCValueVarient(pvtSrcValue, &int64Value);
		if (rc != BLTIN_SUCCESS)
		{
			break;	//source type is neither integer nor unsigned integer
		}

		switch (lSize)
		{
		case 1:
			if( (int64Value >=INT8_MIN) && (int64Value <=INT8_MAX) )
			{
				evalDstValue->val.i = int64Value;
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		case 2:
			if( (int64Value >=INT16_MIN) && (int64Value <=INT16_MAX) )
			{
				evalDstValue->val.i = int64Value;
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		case 3:
		case 4:
			if( (int64Value >=INT32_MIN) && (int64Value <=INT32_MAX) )
			{
				
				evalDstValue->val.i = int64Value;
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		default:	//signed 64-bit int
            if( (int64Value >=LLONG_MIN) && (int64Value <=LLONG_MAX) )
			{
				evalDstValue->val.i = int64Value;
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		}
		break;
	}
	case vT_Unsigned:
	case vT_Enumerated:
	case vT_BitEnumerated:
	case vT_Index:
	case vT_Time:
	case vT_DateAndTime:
	case vT_Duration:
	{
		evalDstValue->size = lSize; // Assign the size define in DD

		//retieve source value
		UINT64 uint64Value = 0;
		boolean bWasSigned = false;
		rc = GetUINT64FromCValueVarient(pvtSrcValue, &uint64Value, &bWasSigned);
		if (rc != BLTIN_SUCCESS)
		{
			break;	//source type is neither integer nor unsigned integer
		}
			
		switch (lSize)
		{
		case 1:
			if( (uint64Value >= 0) && (uint64Value <= UINT8_MAX) )
			{
				evalDstValue->val.u = uint64Value;
			}
			else if (bWasSigned && ((char)uint64Value == uint64Value))
			{
				//check for sign extension
				evalDstValue->val.u = (unsigned char)uint64Value;
			}
			else
			{
				rc = (BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		case 2:
			if ( (uint64Value >= 0) && (uint64Value <= UINT16_MAX))
			{
				evalDstValue->val.u = uint64Value;
			}
			else if (bWasSigned && ((short)uint64Value == uint64Value))
			{
				//check for sign extension
				evalDstValue->val.u = (unsigned short)uint64Value;
			}
			else
			{
				rc = (BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		case 3:
		case 4:
			if ( (uint64Value >= 0) && (uint64Value <= UINT32_MAX))
			{
				evalDstValue->val.u = uint64Value;
			}
			else if (bWasSigned && ((long)uint64Value == uint64Value))
			{
				//check for sign extension
				evalDstValue->val.u = (unsigned long)uint64Value;
			}
			else
			{
				rc = (BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		default:
			evalDstValue->val.u = uint64Value;
			break;
		}
		break;
	}
	case vT_Boolean:
		
		evalDstValue->val.u = CV_BOOL(pvtSrcValue);
		evalDstValue->size = 1;
		break;
	case vT_FloatgPt:	//4 byte real
		evalDstValue->size = 4;
		
		if( ( (CV_R4(pvtSrcValue)>=-FLT_MAX) && (CV_R4(pvtSrcValue)<=FLT_MAX) ) ||	// check for FLOAT range
			(isnan(CV_R4(pvtSrcValue)) ) ||											// check for NaN
			(_fpclass(CV_R4(pvtSrcValue)) == _FPCLASS_PINF) ||						// check for INF
			(_fpclass(CV_R4(pvtSrcValue)) == _FPCLASS_NINF) )						// check for -INF
		{
			evalDstValue->val.f = static_cast<float>(CV_R4(pvtSrcValue));
		}
		else
		{
			rc =(BLTIN_ErrorCode)BI_ERROR;
		}
		break;
	case vT_Double:	//8 byte real
		evalDstValue->size = 8;
		
		if( (CV_R8(pvtSrcValue)>=-DBL_MAX) && (CV_R8(pvtSrcValue)<=DBL_MAX) ||	// check for DOUBLE range
			(isnan(CV_R8(pvtSrcValue)) ) ||										// check for NaN
			(_fpclass(CV_R8(pvtSrcValue)) == _FPCLASS_PINF) ||					// check for INF
			(_fpclass(CV_R8(pvtSrcValue)) == _FPCLASS_NINF) )					// check for -INF
		{
			evalDstValue->val.d = CV_R8(pvtSrcValue);
		}
		else
		{
			rc =(BLTIN_ErrorCode)BI_ERROR;
		}
		break;
	case vT_TimeValue:
		switch (lSize)
		{
		case 1:
		case 2:
		case 3:
		case 4:
			evalDstValue->size = 4;
			
			if( (CV_UI4(pvtSrcValue)>=0) && (CV_UI4(pvtSrcValue)<=UINT32_MAX) )
			{
				evalDstValue->val.u = (unsigned long long)CV_UI4(pvtSrcValue);
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		default:	//signed 64-bit int
			evalDstValue->size = 8;
			
            if( (CV_I8(pvtSrcValue)>=LLONG_MIN) && (CV_I8(pvtSrcValue)<=LLONG_MAX) )
			{
				evalDstValue->val.i = CV_I8(pvtSrcValue);
			}
			else
			{
				rc =(BLTIN_ErrorCode)BI_ERROR;
			}
			break;
		}
		break;
	default:
		rc = BLTIN_WRONG_DATA_TYPE;
		MILog(L"Wrong data type", nsConsumer::Error, L"MI", __FILE__, __LINE__);
		break;
	}

	return (rc);
}

BLTIN_ErrorCode CMiEngine::CMiEngine::GetUINT64FromCValueVarient(/* in */ CValueVarient *pSrcValue, /* out */ UINT64 *pDstValue, /* out */ boolean *pIsSigned)
{
	BLTIN_ErrorCode retValue = BLTIN_SUCCESS;

	//retieve source value
	*pDstValue = 0;
	*pIsSigned = false;
	switch (pSrcValue->vTagType)
	{
	case ValueTagType::CVT_I1:
	{
		*pDstValue = (UINT64)CV_I1(pSrcValue);
		*pIsSigned = true;
		break;
	}
	case ValueTagType::CVT_I2:
	{
		*pDstValue = (UINT64)CV_I2(pSrcValue);
		*pIsSigned = true;
		break;
	}
	case ValueTagType::CVT_I4:
	{
		*pDstValue = (UINT64)CV_I4(pSrcValue);
		*pIsSigned = true;
		break;
	}
	case ValueTagType::CVT_INT:
	{
		*pDstValue = (UINT64)CV_INT(pSrcValue);
		*pIsSigned = true;
		break;
	}
	case ValueTagType::CVT_I8:
	{
		*pDstValue = (UINT64)CV_I8(pSrcValue);
		*pIsSigned = true;
		break;
	}
	case ValueTagType::CVT_UI1:
	{
		*pDstValue = (UINT64)CV_UI1(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI2:
	{
		*pDstValue = (UINT64)CV_UI2(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI4:
	{
		*pDstValue = (UINT64)CV_UI4(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UINT:
	{
		*pDstValue = (UINT64)CV_UINT(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI8:
	{
		*pDstValue = (UINT64)CV_UI8(pSrcValue);
		break;
	}
	default:
		retValue = (BLTIN_ErrorCode)BI_ERROR;
		break;
	}

	return retValue;
}

BLTIN_ErrorCode CMiEngine::GetINT64FromCValueVarient(/* in */ CValueVarient *pSrcValue, /* out */ INT64 *pDstValue)
{
	BLTIN_ErrorCode retValue = BLTIN_SUCCESS;

	//retieve source value
	*pDstValue = 0;
	switch (pSrcValue->vTagType)
	{
	case ValueTagType::CVT_I1:
	{
		*pDstValue = (INT64)CV_I1(pSrcValue);
		break;
	}
	case ValueTagType::CVT_I2:
	{
		*pDstValue = (INT64)CV_I2(pSrcValue);
		break;
	}
	case ValueTagType::CVT_I4:
	{
		*pDstValue = (INT64)CV_I4(pSrcValue);
		break;
	}
	case ValueTagType::CVT_INT:
	{
		*pDstValue = (INT64)CV_INT(pSrcValue);
		break;
	}
	case ValueTagType::CVT_I8:
	{
		*pDstValue = (INT64)CV_I8(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI1:
	{
		*pDstValue = (INT64)CV_UI1(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI2:
	{
		*pDstValue = (INT64)CV_UI2(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI4:
	{
		*pDstValue = (INT64)CV_UI4(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UINT:
	{
		*pDstValue = (INT64)CV_UINT(pSrcValue);
		break;
	}
	case ValueTagType::CVT_UI8:
	{
		*pDstValue = (INT64)CV_UI8(pSrcValue);
		break;
	}
	default:
		retValue = (BLTIN_ErrorCode)BI_ERROR;
		break;
	}

	return retValue;
}

BLTIN_ErrorCode CMiEngine::ConvertDdsCodeToBuiltinCode(int ddsCode)
{
	BLTIN_ErrorCode rtnCode = BLTIN_DDS_ERROR;

	m_iDdsErrorCode = ddsCode;
	if (m_iDdsErrorCode == DDS_SUCCESS)
	{
		rtnCode = BLTIN_SUCCESS;
	}
	else if (m_iDdsErrorCode == DDI_TAB_BAD_DDID)
	{
		rtnCode = BLTIN_BAD_ID;
	}
	else
	{
		rtnCode = BLTIN_DDS_ERROR;
	}

	return (rtnCode);
}

BLTIN_ErrorCode CMiEngine::ConvertPCCodeToBuiltinCode(nsConsumer::PC_ErrorCode pcCode)
{
	BLTIN_ErrorCode rtnCode;

	
	switch(pcCode)
	{
	case nsConsumer::PC_SUCCESS_EC:
		rtnCode = BLTIN_SUCCESS;
		break;
	case nsConsumer::PC_BUSY_EC:
		rtnCode = BLTIN_VALUE_NOT_SET;
		break;
	case nsConsumer::PC_INVALID_EC:
		rtnCode = BLTIN_CANNOT_READ_VARIABLE;
		break;
	case nsConsumer::PC_CIRC_DEPEND_EC :
		rtnCode = BLTIN_CANNOT_READ_VARIABLE;
		break;
	case nsConsumer::PC_OTHER_EC:
	default:
		rtnCode = BLTIN_VAR_NOT_FOUND;
		break;
	}

	return (rtnCode);
}

int CMiEngine::ConvertBSCodeToErrorCode(nsConsumer::BS_ErrorCode bsCode)
{
	int rtnCode;

	
	switch(bsCode)
	{
	case nsConsumer::BSEC_SUCCESS:
		rtnCode = BLTIN_SUCCESS;
		break;
	case nsConsumer::BSEC_WRONG_DATA_TYPE:
		rtnCode = BLTIN_WRONG_DATA_TYPE;
		break;
	case nsConsumer::BSEC_ABORTED:
		rtnCode = BI_ABORT;
		break;
	case nsConsumer::BSEC_NO_DEVICE:
		rtnCode = BI_NO_DEVICE;
		break;
	case nsConsumer::BSEC_FAIL_RESPONSE:
		rtnCode = BLTIN_FAIL_RESPONSE_CODE;
		break;
	case nsConsumer::BSEC_FAIL_COMM:
		rtnCode = BLTIN_FAIL_COMM_ERROR;
		break;
	case nsConsumer::BSEC_NO_LANGUAGE_STRING:
		rtnCode = BLTIN_NO_LANGUAGE_STRING;
		break;
	case nsConsumer::BSEC_OTHER:		
	default:
		rtnCode = BI_ERROR;
		break;
	}

	return (rtnCode);
}

int CMiEngine::OnBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount)
{

	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = DDS_SUCCESS;

	ddsCode = m_pDataFinder->GetBlockInstanceCount(ulBlockId, (int*)pulCount);

	rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	
	return (rc);
}

int CMiEngine::OnBlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex)
{

	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = DDS_SUCCESS;

	ddsCode = m_pDataFinder->GetBlockInstanceByObjIndex(ulObjectIndex, (int*)pulRelativeIndex);

	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}

int CMiEngine::OnBlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex)
{

	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = DDS_SUCCESS;

	ddsCode = m_pDataFinder->GetBlockInstanceByTag(ulBlockId, pchBlockTag, (int *)pulRelativeIndex);

	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}

/***********************************************************************
 *
 *	Name:  OnGetListElement2Execute
 *
 *	ShortDesc:  Get list element contents..
 *
 *	Description:
 *		Instructs the BuiltinSupport to get a value from the array element
 *		specified in the parameters. This function is used for FF.
 *
 *	Reference:
 *		IEC 61804-5 Builtin get_XXX_lelem and get_XXX_lelem2
 *
 *	Inputs:
 *			ulListId:		ID of List on which the action is to be performed
 *			iIndex:			Index of the element for which we want the value
 *			ulEmbListId		ID of an embedded list
 *			iEmbListIndex	Index of the embedded list
 *			lElementRef:	ID of the element being read (Currently being ignored since we don't understand its use.)
 *			lSubElement:	A subelement of the element being read (Currently being ignored since we don't understand its use.)
 *
 *	Outputs:
 *			data:			A pointer to the variant data where the result is to be placed
 *
 *	Returns:
 *		BLTIN_SUCCESS on success, other member of BLTIN_ErrorCode on failure
 *
 **********************************************************************/

#define MAX_LIST_ELEMENTS	10
int CMiEngine::OnGetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data)
{
	int rc = BLTIN_NOT_IMPLEMENTED;
	int ddsCode = BLTIN_DDS_ERROR;
	CValueVarient vtListItem{};

	if ((ulEmbListId == 0) && (iEmbListIndex == 0)) // if embedded list is not used
	{
		//for builtin get_*_lelem(),  find the list item type 
		ddsCode = m_pDataFinder->GetValue( m_iBlockInstance, m_pValueSpec, ulListId, iIndex, PP_ItemID, &vtListItem, engine_sLanguageCode );
	}
	else	//now using embedded list
	{
		//for builtin get_*_lelem2(), find the embedded list item type
		ddsCode = m_pDataFinder->GetValue( m_iBlockInstance, m_pValueSpec, ulEmbListId, iEmbListIndex, PP_ItemID, &vtListItem, engine_sLanguageCode );
	}
	rc = ConvertDdsCodeToBuiltinCode(ddsCode);
	if (rc != BLTIN_SUCCESS)
	{
		return rc;
	}
	


	ITEM_TYPE aeItemType[MAX_LIST_ELEMENTS] = {ITYPE_NO_VALUE};
	ITEM_ID aulItemID[MAX_LIST_ELEMENTS] = {0};
	ITEM_ID aulMemberID[MAX_LIST_ELEMENTS] = {0};
	unsigned short usItemArrayElemNum = 0;		//number of array elements used

	if (CV_UI4(&vtListItem) != ulElementId)
	{
		//fix for legacy FF because legacy FF tokenizer injects builtin get_float_lelem() to parse list complex reference
		//but missing reference between list item and element item.
		ITEM_TYPE emListType = m_pDataFinder->GetItemType(m_iBlockInstance, CV_UI4(&vtListItem));

		//pre-set the beginning item type and item ID
		aeItemType[0] = emListType;
		aulItemID[0] = CV_UI4(&vtListItem);
		//looking for beginning member ID which match the ending (reference) item ID
		ddsCode = m_pDataFinder->GetItemsBetweenReferenceItems( m_iBlockInstance, m_pValueSpec, engine_sLanguageCode,
									ulElementId, 
									MAX_LIST_ELEMENTS,
									&(aeItemType[0]), &(aulItemID[0]), &(aulMemberID[0]),
									&usItemArrayElemNum );
		rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		if (rc != BLTIN_SUCCESS)
		{
			return rc;
		}

		if (usItemArrayElemNum > 1)
		{
			//fill in item type and item ID at top level
			aeItemType[usItemArrayElemNum-1] = emListType;
			aulItemID[usItemArrayElemNum-1] = CV_UI4(&vtListItem);
		}
	}

	//generate a FDI_PARAM_SPECIFIER variable before calling interface function GetParamValue()
	//by using all references collected above
	FDI_PARAM_SPECIFIER ParamSpec;
	rc = OnGetListParamSpecifier(ulListId, iIndex, 
									ulEmbListId, iEmbListIndex,
									ulElementId, ulSubElementId, 
									aeItemType, aulItemID, aulMemberID, 
									usItemArrayElemNum,
									&ParamSpec);

	if (rc == BLTIN_SUCCESS)
	{
		preLogInterfaceAccess(L"GetParamValue", NULL, &ParamSpec);

		nsConsumer::EVAL_VAR_VALUE value;
		nsConsumer::PC_ErrorCode pcCode = m_pIBuiltinSupport->GetParamValue(m_iBlockInstance, m_pValueSpec, &ParamSpec, &value);

		CStdString displayString;
		displayString.Format(L"GetParamValue with returned value %d ", (int)pcCode);
		postLogInterfaceAccess((LPCTSTR)displayString, &value);

		rc = ConvertPCCodeToBuiltinCode(pcCode);

		if (rc == BLTIN_SUCCESS)
		{
			rc = convertEvalToVariant(&value, data);
		}
	}

	return rc;
}

/***********************************************************************
 *
 *	Name:  OnGetListParamSpecifier
 *
 *	Description:
 *	This function generates a list FDI_PARAM_SPECIFIER from a pair of given list ID/index, a pair of given embedded list ID/index, 
 *  a pair of given element ID/member ID that is ultimately referenced by the list,
 *  type and item ID arrays of middle elements between the list and the element, and the number of middle elements in the arrays.
 *
 *	Inputs:
 *			ulListId:			List item ID
 *			iIndex:				List item index
 *			ulEmbListId:		embedded list item ID
 *			iEmbListIndex		embedded list item index
 *			ulElementId:		element item ID
 *			ulSubElementId:		element member ID
 *			peItemType[]:		middle element type array
 *			pulItemID[]:		middle element item ID array
 *			pulMemberID[]:		middle element member ID array
 *			iParamCount:		number of middle elements in the above arrays
 *
 *  Outputs:
 *			pParamSpec:			FDI_PARAM_SPECIFIER variable
 *
 *	Returns:
 *			BLTIN_SUCCESS on success. BLTIN_NOT_IMPLEMENTED or BLTIN_DDS_ERROR on failure.
 *
 **********************************************************************/
 
int CMiEngine::OnGetListParamSpecifier(/*in*/unsigned long ulListId, /*in*/int iIndex,
							/*in*/unsigned long ulEmbListId, /*in*/int iEmbListIndex,
							/*in*/unsigned long ulElementId, /*in*/unsigned long ulSubElementId,
							/*in*/ITEM_TYPE peItemType[], /*in*/ITEM_ID pulItemID[], /*in*/ITEM_ID pulMemberID[],
							/*in*/unsigned short usArrElemCount,								
							/*out*/FDI_PARAM_SPECIFIER* pParamSpec)
{
	ITEM_TYPE elemType = m_pDataFinder->GetItemType(m_iBlockInstance, ulElementId);
	unsigned short usParamSpecCount = usArrElemCount;	//initialized with the number of middle elements


	if ((ulEmbListId == 0) && (iEmbListIndex == 0)) // if embedded list is not used
	{
		usParamSpecCount += 2;						//list + element
	}
	else											// now embedded list is used
	{
		usParamSpecCount += 3;						//list + embedded list + element
	}

	if (ITYPE_VARIABLE != elemType)					// if the element type is not simple variable
	{
		usParamSpecCount += 1;						//last element that is going to be resolved
	}

	pParamSpec->eType = FDI_PS_ITEM_ID_COMPLEX;
	pParamSpec->RefList.list = new OP_REF_INFO[usParamSpecCount];
	pParamSpec->RefList.count = usParamSpecCount;

	pParamSpec->RefList.list[0].id = ulListId;
	pParamSpec->RefList.list[0].member = iIndex;
	pParamSpec->RefList.list[0].type = ITYPE_LIST;

	int j = 1;
	if ((ulEmbListId != 0) || (iEmbListIndex != 0))
	{
		pParamSpec->RefList.list[1].id = ulEmbListId;
		pParamSpec->RefList.list[1].member = iEmbListIndex;
		pParamSpec->RefList.list[1].type = ITYPE_LIST;
		j++;
	}

	//set the reference IDs missed by legacy FF TOK
	for (int i = usArrElemCount - 1; i > -1; i--)
	{
		pParamSpec->RefList.list[j].id = pulItemID[i];
		pParamSpec->RefList.list[j].member = pulMemberID[i];
		pParamSpec->RefList.list[j].type = peItemType[i];
		j++;
	}

	pParamSpec->RefList.list[j].id = ulElementId;
	pParamSpec->RefList.list[j].member = ulSubElementId;
	pParamSpec->RefList.list[j].type = elemType;

	int rc = BLTIN_NOT_IMPLEMENTED;
	switch (elemType)
	{
	case ITYPE_VARIABLE:
		pParamSpec->RefList.list[j].member = 0;
		rc = BLTIN_SUCCESS;
		break;
	case ITYPE_COLLECTION:
		{
			CValueVarient vtColItemID{};
			int ddsCode = m_pDataFinder->GetValue( m_iBlockInstance, m_pValueSpec, ulElementId, ulSubElementId, PP_ItemID, &vtColItemID, engine_sLanguageCode );
			rc = ConvertDdsCodeToBuiltinCode(ddsCode);

			if (rc == BLTIN_SUCCESS)
			{
				

				pParamSpec->RefList.list[j+1].id = CV_UI4(&vtColItemID);
				pParamSpec->RefList.list[j+1].type = m_pDataFinder->GetItemType(m_iBlockInstance, CV_UI4(&vtColItemID));;
			}
		}
		break;
	case ITYPE_RECORD:
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec;

			item_spec.eType = FDI_ITEM_ID_SI;
			item_spec.item.id = ulElementId;

			//get subindex from member ID
			int tempSubindex;
			int ddsCode = m_pDataFinder->ResolveSubindex(m_iBlockInstance, ulElementId, ulSubElementId, &tempSubindex);
			if (ddsCode == SUCCESS)
			{
				pParamSpec->RefList.list[j].member = tempSubindex;	//adjust to use record subindex
				item_spec.subindex = tempSubindex;

				//find out last item ID and item type in the complex reference
				ddsCode = m_pDataFinder->GetItemTypeAndItemId(m_iBlockInstance, &item_spec, &(pParamSpec->RefList.list[j+1].type), &(pParamSpec->RefList.list[j+1].id));
			}
			rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		}
		break;
	case ITYPE_ARRAY:
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec;

			item_spec.eType = FDI_ITEM_ID_SI;
			item_spec.item.id = ulElementId;
			item_spec.subindex = ulSubElementId;

			//find out last item ID and item type in the complex reference
			int ddsCode2 = m_pDataFinder->GetItemTypeAndItemId(m_iBlockInstance, &item_spec, &(pParamSpec->RefList.list[j+1].type), &(pParamSpec->RefList.list[j+1].id));
			rc = ConvertDdsCodeToBuiltinCode(ddsCode2);
		}
		break;
	default:
		break;
	}

	return rc;
}

void CMiEngine::engineLogMessage(int priorityVal, wchar_t *msg)
{
	if (m_pIBuiltinSupport)
	{
		preLogInterfaceAccess(L"LogMessage", NULL);

		m_pIBuiltinSupport->LogMessage(m_pValueSpec, priorityVal, msg);

		postLogInterfaceAccess(L"LogMessage");
	}
}

void CMiEngine::engineMethodDebugMessage(unsigned long lineNumber, const wchar_t* currentMethodDebugXml,  const wchar_t** modifiedMethodDebugXml)
{
	if (m_pIBuiltinSupport)
	{
		preLogInterfaceAccess(L"OnMethodDebugInfo", NULL);

		m_pIBuiltinSupport->OnMethodDebugInfo(m_iBlockInstance, m_pValueSpec, lineNumber, currentMethodDebugXml, modifiedMethodDebugXml);

		postLogInterfaceAccess(L"OnMethodDebugInfo");
	}
}

int CMiEngine::engineIsOffline()
{
    int rc = BLTIN_NOT_IMPLEMENTED;

	if (m_pIBuiltinSupport)
	{
		preLogInterfaceAccess(L"isOffline", NULL);

		rc = m_pIBuiltinSupport->isOffline(m_pValueSpec);

		CStdString displayString;
		displayString.Format(L"isOffline with returned value %d ", (int)rc);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	return rc;
}

long CMiEngine::engineSendCommand(unsigned long commandId, long *commandError)
{
    nsConsumer::BS_ErrorCode bsCode = nsConsumer::BSEC_OTHER;

	if (m_pIBuiltinSupport)
	{
		preLogInterfaceAccess(L"SendCommand", NULL);

		bsCode = m_pIBuiltinSupport->SendCommand(m_pValueSpec, commandId, commandError);

		CStdString displayString;
		displayString.Format(L"SendCommand with returned value %d ", (int)bsCode);
		postLogInterfaceAccess((LPCTSTR)displayString);
	}

	long rc = ConvertBSCodeToErrorCode(bsCode);
	return rc;
}

//Convert PARAM_REF valut to OP_REF value
BLTIN_ErrorCode CMiEngine::convertParamRefToOpRef(const BLOCK_INSTANCE iBlockInstance, const PARAM_REF *pSrcValue, nsEDDEngine::OP_REF *pDstValue)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	pDstValue->block_instance = iBlockInstance;

	switch (pSrcValue->op_ref_type)
	{
	case ParamRefType::STANDARD_TYPE:
		{
			pDstValue->op_ref_type = nsEDDEngine::STANDARD_TYPE;

			rc = convertParamItemIDNType(iBlockInstance, 
									pSrcValue->op_info.id, pSrcValue->op_info.member, pSrcValue->op_info.type,
									&(pDstValue->op_info.id), &(pDstValue->op_info.member), &(pDstValue->op_info.type));
		}
		break;
	case ParamRefType::COMPLEX_TYPE:
		{
			pDstValue->op_ref_type = nsEDDEngine::COMPLEX_TYPE;

			pDstValue->op_info_list.count = pSrcValue->op_info_list.count;

			if (pDstValue->op_info_list.count > 0)
			{
				pDstValue->op_info_list.list = new OP_REF_INFO[pDstValue->op_info_list.count];

				for (int i = 0; i < pDstValue->op_info_list.count; i++)
				{
					rc = convertParamItemIDNType(iBlockInstance,
											pSrcValue->op_info_list.list[i].id, pSrcValue->op_info_list.list[i].member, pSrcValue->op_info_list.list[i].type,
											&(pDstValue->op_info_list.list[i].id), &(pDstValue->op_info_list.list[i].member), &(pDstValue->op_info_list.list[i].type));
					if (rc != BLTIN_SUCCESS)
					{
						break;
					}
				}
			}
		}
		break;
	}
	return rc;
}

void CMiEngine::convertParamRefTypeToOpRefType(const itemType_t srcValue, nsEDDEngine::ITEM_TYPE *pDstValue)
{
	switch (srcValue)
	{
	case iT_ReservedZeta:
		*pDstValue = nsEDDEngine::ITYPE_NO_VALUE;
		break;
	case iT_Variable:
		*pDstValue = nsEDDEngine::ITYPE_VARIABLE;
		break;
	case iT_Command:
		*pDstValue = nsEDDEngine::ITYPE_COMMAND;
		break;
	case iT_Menu:
		*pDstValue = nsEDDEngine::ITYPE_MENU;
		break;
	case iT_EditDisplay:
		*pDstValue = nsEDDEngine::ITYPE_EDIT_DISP;
		break;
	case iT_Method:
		*pDstValue = nsEDDEngine::ITYPE_METHOD;
		break;
	case iT_Refresh:
		*pDstValue = nsEDDEngine::ITYPE_REFRESH;
		break;
	case iT_Unit:
		*pDstValue = nsEDDEngine::ITYPE_UNIT;
		break;
	case iT_WaO:
		*pDstValue = nsEDDEngine::ITYPE_WAO;
		break;
	case iT_ItemArray:
		*pDstValue = nsEDDEngine::ITYPE_ITEM_ARRAY;
		break;
	case iT_Collection:
		*pDstValue = nsEDDEngine::ITYPE_COLLECTION;
		break;
	case iT_Block_B:
		*pDstValue = nsEDDEngine::ITYPE_BLOCK_B;	//11
		break;
	case iT_Block:
		*pDstValue = nsEDDEngine::ITYPE_BLOCK;		//12
		break;
	case iT_Record:
		*pDstValue = nsEDDEngine::ITYPE_RECORD;		//14
		break;
	case iT_Array:
		*pDstValue = nsEDDEngine::ITYPE_ARRAY;
		break;
	case iT_VariableList:
		*pDstValue = nsEDDEngine::ITYPE_VAR_LIST;
		break;
	case iT_ResponseCodes:
		*pDstValue = nsEDDEngine::ITYPE_RESP_CODES;
		break;
	case iT_Member:
		*pDstValue = nsEDDEngine::ITYPE_MEMBER;
		break;
	case iT_File:
		*pDstValue = nsEDDEngine::ITYPE_FILE;
		break;
	case iT_Chart:
		*pDstValue = nsEDDEngine::ITYPE_CHART;
		break;
	case iT_Graph:
		*pDstValue = nsEDDEngine::ITYPE_GRAPH;
		break;
	case iT_Axis:
		*pDstValue = nsEDDEngine::ITYPE_AXIS;
		break;
	case iT_Waveform:
		*pDstValue = nsEDDEngine::ITYPE_WAVEFORM;
		break;
	case iT_Source:
		*pDstValue = nsEDDEngine::ITYPE_SOURCE;
		break;
	case iT_List:
		*pDstValue = nsEDDEngine::ITYPE_LIST;
		break;
	case iT_Grid:
		*pDstValue = nsEDDEngine::ITYPE_GRID;
		break;
	case iT_Image:
		*pDstValue = nsEDDEngine::ITYPE_IMAGE;
		break;
	case iT_BLOB:
		*pDstValue = nsEDDEngine::ITYPE_BLOB;
		break;
	case iT_Plugin:
		*pDstValue = nsEDDEngine::ITYPE_PLUGIN;
		break;
	case iT_Template:
		*pDstValue = nsEDDEngine::ITYPE_TEMPLATE;
		break;
	case iT_Reserved:
		*pDstValue = nsEDDEngine::ITYPE_RESERVED;
		break;
	case iT_Component:
		*pDstValue = nsEDDEngine::ITYPE_COMPONENT;
		break;
	case iT_Component_folder:
		*pDstValue = nsEDDEngine::ITYPE_COMPONENT_FOLDER;
		break;
	case iT_Component_reference:
		*pDstValue = nsEDDEngine::ITYPE_COMPONENT_REFERENCE;
		break;
	case iT_Component_relation:
		*pDstValue = nsEDDEngine::ITYPE_COMPONENT_RELATION;
		break;

	case iT_Program:	//not used in FDI
	case iT_Domain:		//not used in FDI
	default:
		*pDstValue = nsEDDEngine::ITYPE_MAX;
		break;
	}

}

BLTIN_ErrorCode CMiEngine::convertParamRefToParamSpec(const BLOCK_INSTANCE iBlockInstance, const PARAM_REF *pSrcValue, nsEDDEngine::FDI_PARAM_SPECIFIER *pDstValue)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;

	switch (pSrcValue->op_ref_type)
	{
	case ParamRefType::STANDARD_TYPE:
		{
			//set FDI_PARAM_SPECIFIER type
			if (pSrcValue->op_info.member > 0)
			{
				pDstValue->eType = FDI_PS_ITEM_ID_SI;          //doesn't work if ulMemberId == 0
			}
			else
			{
				pDstValue->eType = FDI_PS_ITEM_ID;
			}

			//set FDI_PARAM_SPECIFIER id, param and subindex
			rc = convertParamItemIDNType(iBlockInstance, 
									pSrcValue->op_info.id, pSrcValue->op_info.member, pSrcValue->op_info.type,
									&(pDstValue->id), &(pDstValue->subindex), nullptr);
		}
		break;
	case ParamRefType::COMPLEX_TYPE:
		{
			pDstValue->eType = FDI_PS_ITEM_ID_COMPLEX;

			pDstValue->RefList.count = pSrcValue->op_info_list.count;

			if (pDstValue->RefList.count > 0)
			{
				pDstValue->RefList.list = new OP_REF_INFO[pDstValue->RefList.count];

				for (int i = 0; i < pDstValue->RefList.count; i++)
				{
					//set FDI_PARAM_SPECIFIER.OP_REF_INFO_LIST
					rc = convertParamItemIDNType(iBlockInstance,
											pSrcValue->op_info_list.list[i].id, pSrcValue->op_info_list.list[i].member, pSrcValue->op_info_list.list[i].type,
											&(pDstValue->RefList.list[i].id), &(pDstValue->RefList.list[i].member), &(pDstValue->RefList.list[i].type));
					if (rc != BLTIN_SUCCESS)
					{
						break;
					}
				}
			}
		}
		break;
	}
	return rc;
}

//Convert PARAM_REF value to OP_REF_TRAIL value
BLTIN_ErrorCode CMiEngine::convertParamRefToOpRefTrail(const BLOCK_INSTANCE iBlockInstance, PARAM_REF *pSrcValue, /*out*/nsEDDEngine::OP_REF_TRAIL *pDstValue)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	pDstValue->block_instance = iBlockInstance;

	//fill in op_ref_type, op_info or op_info_list
	switch (pSrcValue->op_ref_type)
	{
	case ParamRefType::STANDARD_TYPE:
		{
			pDstValue->op_ref_type = OpRefType::STANDARD_TYPE;

			rc = convertParamItemIDNType(iBlockInstance, 
									pSrcValue->op_info.id, pSrcValue->op_info.member, pSrcValue->op_info.type,
									&(pDstValue->op_info.id), &(pDstValue->op_info.member), &(pDstValue->op_info.type));
			if (rc != BLTIN_SUCCESS)
			{
				return rc;
			}

			//fill in desc_id and desc_type
			rc = GetDescItemTypeAndItemId(iBlockInstance, 
									 pDstValue->op_info.id, 
									 pDstValue->op_info.member,
									 pDstValue->op_info.type,
									 &(pDstValue->desc_id), &(pDstValue->desc_type));
		}
		break;
	case ParamRefType::COMPLEX_TYPE:
		{
			pDstValue->op_ref_type = OpRefType::COMPLEX_TYPE;

			pDstValue->op_info_list.count = pSrcValue->op_info_list.count;

			if (pDstValue->op_info_list.count > 0)
			{
				pDstValue->op_info_list.list = new OP_REF_INFO[pDstValue->op_info_list.count];

				for (int i = 0; i < pDstValue->op_info_list.count; i++)
				{
					rc = convertParamItemIDNType(iBlockInstance, 
											pSrcValue->op_info_list.list[i].id, pSrcValue->op_info_list.list[i].member, pSrcValue->op_info_list.list[i].type,
											&(pDstValue->op_info_list.list[i].id), &(pDstValue->op_info_list.list[i].member), &(pDstValue->op_info_list.list[i].type));
					if (rc != BLTIN_SUCCESS)
					{
						return rc;
					}
				}

				//fill in desc_id and desc_type
				rc = GetDescItemTypeAndItemId(iBlockInstance,
										 pDstValue->op_info_list.list[pDstValue->op_info_list.count - 1].id, 
										 pDstValue->op_info_list.list[pDstValue->op_info_list.count - 1].member,
										 pDstValue->op_info_list.list[pDstValue->op_info_list.count - 1].type,
										 &(pDstValue->desc_id), &(pDstValue->desc_type));
			}
		}
		break;
	}
	return rc;
}

//This function adjusts value array and list index to 0 based for HART and Profibus
//and adjusts record member ID to record index
BLTIN_ErrorCode CMiEngine::convertParamItemIDNType(const BLOCK_INSTANCE iBlockInstance,
										const ITEM_ID ulSrcItemId, const ITEM_ID ulSrcMemberId, const itemType_t SrcType,
										/*out*/ITEM_ID *pulDstItemId, /*out*/ITEM_ID *pulDstMemberId, /*out*/nsEDDEngine::ITEM_TYPE *pDstType)
{
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	ITEM_ID ulSubIndex = ulSrcMemberId;

	switch (SrcType)
	{
	case iT_Array:
	case iT_List:
			//only adjust array or list index if it is HART and Profibus protocol families
		if ( (ulSubIndex > 0) && (FF != m_protocol) )
		{
			ulSubIndex--;		//index 0 based
		}
		break;
	case iT_Record:
		//adjust record index
		{
		if (ulSubIndex != 0) // Resolve the subindex only if it is not 0. If it is zero don't retrun the error, we are processing whole record.
		{
			int tempSubindex;
			int ddsCode = m_pDataFinder->ResolveSubindex(iBlockInstance, ulSrcItemId, ulSubIndex, &tempSubindex);
			if (ddsCode == BLTIN_SUCCESS)
			{
				ulSubIndex = tempSubindex;
			}
			else
			{
				rc = ConvertDdsCodeToBuiltinCode(ddsCode);
			}
		}
		}
		break;
	}

	if (rc == BLTIN_SUCCESS)
	{
		*pulDstItemId = ulSrcItemId;
		*pulDstMemberId = ulSubIndex;
		if (pDstType)
		{
			convertParamRefTypeToOpRefType(SrcType, pDstType);
		}
	}
	return rc;
}

//This function returns descriptive item ID and item type
BLTIN_ErrorCode CMiEngine::GetDescItemTypeAndItemId(const BLOCK_INSTANCE iBlockInstance,
									 	 const ITEM_ID ulSrcItemId, const ITEM_ID ulSrcMemberId, const nsEDDEngine::ITEM_TYPE eSrcItemType,  
									 	 /*out*/ITEM_ID *pulDstItemId, /*out*/ITEM_TYPE *peDstItemType)
{
	//initialization
	BLTIN_ErrorCode rc = BLTIN_SUCCESS;
	*pulDstItemId = 0;
	*peDstItemType = ITYPE_NO_VALUE;

	switch(eSrcItemType)
	{
	case ITYPE_VARIABLE:
		*pulDstItemId = ulSrcItemId;
		*peDstItemType = eSrcItemType;
		break;
	case ITYPE_ARRAY:
	case ITYPE_LIST:
	case ITYPE_RECORD:
		{
			FDI_ITEM_SPECIFIER item_spec;

			if (ulSrcMemberId > 0)
			{
				item_spec.eType = FDI_ITEM_ID_SI;		//doesn't work if ulSrcMemberId == 0
			}
			else
			{
				item_spec.eType = FDI_ITEM_ID;
			}
			item_spec.item.id = ulSrcItemId;
			item_spec.subindex = ulSrcMemberId;			//this has to be index (not member ID) for record, value array and list

			int ddsCode = m_pDataFinder->GetItemTypeAndItemId(iBlockInstance, &item_spec, peDstItemType, pulDstItemId);
			rc = ConvertDdsCodeToBuiltinCode(ddsCode);
		}
		break;
	default:	//error out
		break;
	}
	return rc;
}


int CMiEngine::engineGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString)
{
	BLTIN_ErrorCode rc = BLTIN_NOT_IMPLEMENTED;

	int ddsCode = m_pDataFinder->GetEnumVarString(m_iBlockInstance, m_pValueSpec, ulItemId, ulEnumValue, sEnumString, engine_sLanguageCode);

	rc = ConvertDdsCodeToBuiltinCode(ddsCode);

	return (rc);
}


void CMiEngine::convertVariableType(const variableType_t srcValue, nsEDDEngine::VariableType *pDstValue)
{
	switch (srcValue)
	{
	case vT_unused:
		*pDstValue = VT_DDS_TYPE_UNUSED;
		break;
	case vT_undefined:
		*pDstValue = VT_DDS_TYPE_UNUSED;
		break;
	case vT_Integer:
		*pDstValue = VT_INTEGER;
		break;
	case vT_Unsigned:
		*pDstValue = VT_UNSIGNED;
		break;
	case vT_FloatgPt:
		*pDstValue = VT_FLOAT;
		break;
	case vT_Double:
		*pDstValue = VT_DOUBLE;
		break;
	case vT_Enumerated:
		*pDstValue = VT_ENUMERATED;
		break;
	case vT_BitEnumerated:
		*pDstValue = VT_BIT_ENUMERATED;
		break;
	case vT_Index:
		*pDstValue = VT_INDEX;
		break;
	case vT_Ascii:
		*pDstValue = VT_ASCII;
		break;
	case vT_PackedAscii:
		*pDstValue = VT_PACKED_ASCII;
		break;
	case vT_Password:
		*pDstValue = VT_PASSWORD;
		break;
	case vT_BitString:
		*pDstValue = VT_BITSTRING;
		break;
	case vT_HartDate:
		*pDstValue = VT_EDD_DATE;
		break;
	case vT_Time:
		*pDstValue = VT_TIME;
		break;
	case vT_DateAndTime:
		*pDstValue = VT_DATE_AND_TIME;
		break;
	case vT_Duration:
		*pDstValue = VT_DURATION;
		break;
	case vT_EUC:
		*pDstValue = VT_EUC;
		break;
	case vT_OctetString:
		*pDstValue = VT_OCTETSTRING;
		break;
	case vT_VisibleString:
		*pDstValue = VT_VISIBLESTRING;
		break;
	case vT_TimeValue:
		*pDstValue = VT_TIME_VALUE;
		break;
	case vT_ObjectReference:
		*pDstValue = VT_OBJECT_REFERENCE;
		break;
	case vT_Boolean:
		*pDstValue = VT_BOOLEAN;
		break;
	default:
		*pDstValue = VT_DDS_TYPE_UNUSED;
		break;
	}

}


void CMiEngine::preLogInterfaceAccess(wchar_t* funcName, wchar_t* message, nsEDDEngine::FDI_PARAM_SPECIFIER *paramSpec)
{

	wchar_t* wStrMessage = new wchar_t[DOUBLE_MAX_DD_STRING];

		if (paramSpec != nullptr)
		{
				if (paramSpec->eType == FDI_PS_ITEM_ID_COMPLEX)
				{		
					if (message != nullptr)
					{
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called with \"%.1900s\"\n", funcName, message);
					}
					else
					{
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called\n", funcName);
					}	
					MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					for (unsigned short i = 0; i < paramSpec->RefList.count; i++)
					{
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"Complex Reference ID: %u, Member: %u, Type: %u\n", paramSpec->RefList.list[i].id, paramSpec->RefList.list[i].member, paramSpec->RefList.list[i].type);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
					}				
				}
				else
				{
					if (message != nullptr)
					{
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called with \"%.1900s\" Standard Reference ID: %u, Subindex: %u, Param: %d\n", funcName, message, paramSpec->id, paramSpec->subindex, paramSpec->param);
					}
					else
					{
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called with Standard Reference ID: %u, Subindex: %u, Param: %d\n", funcName, paramSpec->id, paramSpec->subindex, paramSpec->param);
					}
					MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
				}
		}
		else
		{
			if (message != nullptr)
			{
				PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called with \"%.1900s\"\n", funcName, message);
			}
			else
			{
				PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is being called\n", funcName);
			}
			MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			//MILog(L"\n", nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
		}

		if (wStrMessage != NULL)
		{
			delete[] wStrMessage;
			wStrMessage = NULL;
		}
}

void CMiEngine::appendLogInterfaceAccess(wchar_t* message1, wchar_t* message2)
{
	wchar_t wStrMessage[MAX_DD_STRING] = { 0 };

	PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, message1, message2);
	MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
	
}

void CMiEngine::postLogInterfaceAccess(const wchar_t* funcName, nsConsumer::EVAL_VAR_VALUE *evalValue)
{

		if (funcName != nullptr)
		{	
			wchar_t* wStrMessage = new wchar_t[DOUBLE_MAX_DD_STRING];
			if (evalValue == nullptr)
			{
				PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned\n", funcName);
				MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
			}
			else
			{
				switch (evalValue->type)
				{
					case VT_ASCII:
						// We will accept max 1900 characters to print in log file.
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: ASCII,  Value: %.1900ls\n", funcName, evalValue->val.s.c_str());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_PACKED_ASCII:
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: PACKED_ASCII, Value: %.1900ls\n", funcName, evalValue->val.s.c_str());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_PASSWORD:
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: PASSWORD,  Value: %.1900ls\n", funcName, evalValue->val.s.c_str());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_BITSTRING:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: BITSTRING,  Value[0]: %hhu, length: %d\n", funcName, evalValue->val.b.ptr()[0], evalValue->val.b.length());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_EUC:
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: EUC,  Value: %.1900ls\n", funcName, evalValue->val.s.c_str());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_VISIBLESTRING:
						PS_VsnwPrintf(wStrMessage, DOUBLE_MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: VISIBLESTRING,  Value: %.1900ls\n", funcName, evalValue->val.s.c_str());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_OCTETSTRING:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: OCTETSTRING,  Value[0]: %hhu, length: %d\n", funcName, evalValue->val.b.ptr()[0], evalValue->val.b.length());
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_INTEGER:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: INTEGER, Value: %lli\n", funcName, evalValue->val.i);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_BOOLEAN:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: VT_BOOLEAN, Value: %lli\n", funcName, evalValue->val.i);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_UNSIGNED:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: UNSIGNED, Value: %llu\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_DATE_AND_TIME:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: DATE_AND_TIME, Value: %llu\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_EDD_DATE:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: EDD_DATE, Value: %llu\n", funcName, evalValue->val.i);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_TIME:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: TIME, Value: %llu\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_DURATION:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: DURATION, Value: %llu\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_TIME_VALUE:
						if (evalValue->size >= 8)
						{
							PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: TIME_VALUE, Value: %lli\n", funcName, evalValue->val.i);
						}
						else	//should be 4
						{
							PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: TIME_VALUE,  Value: %llu\n", funcName, evalValue->val.u);
						}
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_ENUMERATED:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: 64 ENUMERATED,  Value: %lli\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_BIT_ENUMERATED:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: 64 bit ENUMERATED,  Value: %lli\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_INDEX:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE:Type: INDEX,  Value: %lli\n", funcName, evalValue->val.u);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_FLOAT:	//4 byte real
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: FLOAT,  Value: %f\n", funcName, evalValue->val.f);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_DOUBLE:	//8 byte real
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: DOUBLE,  Value: %Lf\n", funcName, evalValue->val.d);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					case VT_DDS_TYPE_UNUSED:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Type: UNUSED\n", funcName);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
					default:
						PS_VsnwPrintf(wStrMessage, MAX_DD_STRING, L"IBuiltinSupport %s() is returned with EVAL_VAR_VALUE Data type is unknown.\n", funcName);
						MILog(wStrMessage, nsConsumer::LogSeverity::Information, L"MI_ExecutionLog");
						break;
				}		
			}

			if (wStrMessage != NULL)
			{
				delete[] wStrMessage;
				wStrMessage = NULL;
			}
		}

	
	
}

void CMiEngine::MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename,  UINT32 lineNumber)
{
	
	if(wcscmp(sCategory, L"MI") == 0) 
	{
    //	USES_CONVERSION;


		// Strings
		const TCHAR *cfilenameIsMissing  = L"FileName is Missing";
		const TCHAR *cnoMsgAvailable     = L"No Message is availible";
		(void)cfilenameIsMissing;

		// other constants
		static const long msgBfrDim = 3001;

		const int msgsDim = 3;        // dimension of msgAP

		// msg Array of Pointers to strings
		// to put in the event report
		// [0] = filename
		// [1] = lineNumber (decimal)
		// [2] = formatted message or raw msg if formatting fails
		const TCHAR *msgAP[msgsDim] = {0,0,0};

		// Verify a filename was passed in, and if not include this in the message
		//
		//msgAP[0] = (cFilename != NULL) ? A2CT(cFilename) : cfilenameIsMissing; 
		//TODO fixit
		PS_MultiByteToWideChar(0, 0, cFilename, (int)strlen(cFilename), (wchar_t *)msgAP, msgsDim);


		// lineNumber
		TCHAR cLineNumber[32] = { 0 }; // holds more than max UINT32 (4,ddd,ddd,ddd)
        PS_VsnwPrintf(cLineNumber, sizeof(cLineNumber), _T("%lu"), lineNumber);
		msgAP[1] = cLineNumber;

		// EventId is set equal to status


		//TCHAR cEventID[60] = { 0 }; // holds more than 2 * max INT32 (4,ddd,ddd,ddd)
		//_stprintf_s(cEventID,_T("%d 0x%X"), eventId, eventId);
		//msgAP[2] = cEventID;

		//
		// 'formated' message
		//TCHAR msgBfr[msgBfrDim] = {0};

		if (sMessage != NULL)
		{
			// the formatted message
			msgAP[2] = sMessage;
		}
		else
		{
			msgAP[2] = cnoMsgAvailable;
			//++msgsN;
		}

		// build debug string
		CStdString myString; 

		myString.Format(L"%s, %s, %s\n", msgAP[0], msgAP[1], msgAP[2]);

		// we should only break if we are not Information or Warning messages.
		if(eLogSeverity == nsConsumer::Error || eLogSeverity == nsConsumer::Critical)
		{
			if(PS_IsDebuggerPresent())
			{
				DoDebugBreak(EnableDebugBreak, myString);
			}
		}
		m_pIEDDEngineLogger->Log( myString.GetBuffer(), eLogSeverity, sCategory);
	}
	else
	{
		m_pIEDDEngineLogger->Log( sMessage, eLogSeverity, sCategory);
	}
	
}

