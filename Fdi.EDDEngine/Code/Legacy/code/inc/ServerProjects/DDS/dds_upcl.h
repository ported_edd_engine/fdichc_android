/**
 *	@(#) $Id: dds_upcl.h,v 1.5 1996/04/18 14:43:54 davcomf Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	dds_upcl.h - Connection Manager Library Definitions
 *
 *	This file contains all interface definitions for
 *	the Connection Manager Library.
 */

#ifndef DDS_UPCL_H
#define DDS_UPCL_H

#include "attrs.h"

#define DF_ISP		1	/* ISP Device Family */
#define DF_HART		2	/* Alternate Device Family */


#define DF_FF				1	/* FF Device Family type */

typedef int		DEVICE_MANUFACTURER_HANDLE;
typedef int		DEVICE_TYPE_HANDLE;
typedef int		BLOCK_HANDLE;
typedef int		DEVICE_HANDLE;
typedef int		ROD_HANDLE;
typedef int		FLAT_HANDLE;

typedef UINT8		BOOLEAN;

typedef struct {
	UINT8	password;
	UINT8	groups;
	UINT16	rights;
} ACCESS;

typedef struct	{
	BOOLEAN			ram_rom_flag;
	UINT8			name_length;
	BOOLEAN			access_protection_flag;
	UINT16			version;
	UINT16			stod_object_count;
	OBJECT_INDEX	sod_first_index;
	UINT16			sod_object_count;
	OBJECT_INDEX	dvod_first_index;
	UINT16			dvod_object_count;
	OBJECT_INDEX	dpod_first_index;
	UINT16			dpod_object_count;
} OD_DESCRIPTION_SPECIFIC;

typedef struct {
	UINT32			size;
	UINT32			local_address;
	UINT8			state;
	UINT8			upload_state;
	UINT8			usage;
} DOMAIN_SPECIFIC;

typedef struct {
	UINT8			 domain_count;
	BOOLEAN			 deleteable_flag;
	BOOLEAN			 reuseable_flag;
	UINT8			 state;	
	OBJECT_INDEX	*domain_list;
} PROGRAM_INVOCATION_SPECIFIC;

typedef struct {
	OBJECT_INDEX	data;
	UINT8			size;
	BOOLEAN			enabled_flag;
} EVENT_SPECIFIC;

typedef struct {
	UINT8			 symbol_size;
	char			*symbol;
} DATA_TYPE_SPECIFIC;

typedef struct {
	UINT8			 elem_count;
	OBJECT_INDEX	*type_list;
	UINT8			*size_list;
} DATA_TYPE_STRUCTURE_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
	UINT8			size;
} SIMPLE_VARIABLE_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
	UINT8			size;
	UINT8			elem_count;
} ARRAY_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
} RECORD_SPECIFIC;

typedef struct {
	UINT8			 elem_count;
	BOOLEAN			 deleteable_flag;
	OBJECT_INDEX	*list;
} VARIABLE_LIST_SPECIFIC;

typedef union {
	OD_DESCRIPTION_SPECIFIC			od_description;
	DOMAIN_SPECIFIC					domain;
	PROGRAM_INVOCATION_SPECIFIC		program_invocation;
	EVENT_SPECIFIC					event;
	DATA_TYPE_SPECIFIC				data_type;
	DATA_TYPE_STRUCTURE_SPECIFIC	data_type_structure;
	SIMPLE_VARIABLE_SPECIFIC		simple_variable;
	ARRAY_SPECIFIC					array;
	RECORD_SPECIFIC					record;
	VARIABLE_LIST_SPECIFIC			variable_list;
} OBJECT_SPECIFIC;

typedef struct {
	OBJECT_INDEX		 index;
	UINT8				 code;
	char				*name;
	ACCESS				 access;
	OBJECT_SPECIFIC		*specific;
	UINT8				*extension;
	UINT8				*value;
} OBJECT;

typedef struct
{
    STRING		  language_code;
	unsigned long length;
    unsigned char *data;
} BINARY_LANGUAGE_IMAGE;

#endif	/* DDS_UPCL_H */


