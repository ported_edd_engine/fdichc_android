/*************************************************************************************************
 *
 * $Workfile: MethodInterfaceDefs.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This file gives the data structures used to pass information between device object 
 *		and methods user interface.
 *	
 */

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version


#ifndef _METHODINTERFACEDEFS_H
#define _METHODINTERFACEDEFS_H

#ifdef INC_DEBUG
#pragma message("In MethodInterfaceDefs.h") 
#endif

#pragma warning (disable : 4786) 

#include "Char.h"
#include <vector>
#include "ddbdefs.h"
#include "varient.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes::MethodInterfaceDefs.h") 
#endif

/*The types of data that will need to be shown by the UI*/
enum UI_DATA_TYPE
{
	UNKNOWN_UI_DATA_TYPE = 0
	, TEXT_MESSAGE
	, EDIT_DEVICE_PARAM
	, EDIT_LOCAL
	, MENU	 // stevev 09aug05 - displayMenu() - button Names in combo list
	, SELECTION
};

/*<START>06/01/2004 Added by ANOOP for validating the list of ranges*/
typedef struct
{
    _INT64 iMinval;
    _INT64 iMaxval;
}IntMinMaxVal_t;

typedef struct
{
	float fMinval;
	float fMaxval;
}FloatMinMaxVal_t;


typedef union
{
	IntMinMaxVal_t IntMinMaxVal;
	FloatMinMaxVal_t FloatMinMaxVal;
}MinMaxVal;		


typedef vector <MinMaxVal> MinMaxVal_t;

/*<END>06/01/2004 Added by ANOOP for validating the list of ranges*/

/*This class  represents the string message to be displayed*/
class UI_DATA_TYPE_TEXT_MESSAGE
{
public:
	int		iTextMessageLength;
	tchar	*pchTextMessage;

	UI_DATA_TYPE_TEXT_MESSAGE():iTextMessageLength(0), pchTextMessage(NULL)
	{
		pchTextMessage = NULL;
	}

	~UI_DATA_TYPE_TEXT_MESSAGE()
	{
		
	}
};


/*This class  represents the value to be displayed and other related info */
class UI_DATA_EDIT_BOX
{
public:
	CValueVarient		vtValue;	//used for current variable value
	MinMaxVal_t		MinMaxVal;
	variableType_t  varType;
	int		iMaxStringLength;

	UI_DATA_EDIT_BOX()
	{
		iMaxStringLength = 0;
		varType = vT_unused;
	}

	~UI_DATA_EDIT_BOX()
	{
	}
};

/* WS:EPM 30apr07 enum added to support multiple selection of bit enums, see UI_COM SF:EPM*/
enum COMBO_BOX_TYPE
{
	UNKNOWN_COMBO_BOX_TYPE,
	COMBO_BOX_TYPE_ENUMERATION,
	COMBO_BOX_TYPE_INDEX
};

#define MAXIMUM_NUMBER_OF_BITS_IN_BITENUM 32
/*This class  represents the combo box type and the value to be displayed and other related info */
class UI_DATA_TYPE_COMBO
{
public:
	COMBO_BOX_TYPE comboBoxType;//WS:EPM 30apr07 
	int		iNumberOfComboElements;
	tchar	*pchComboElementText; //This will be a list of options separated by semi colon
	unsigned long nCurrentIndex;

	UI_DATA_TYPE_COMBO()
	{
		comboBoxType = UNKNOWN_COMBO_BOX_TYPE;//WS:EPM 30apr07 
		iNumberOfComboElements = 0;
		pchComboElementText = NULL;
		nCurrentIndex = 0;//Added By Anil October 25 2005 for fixing the PAR 5436
	}

	~UI_DATA_TYPE_COMBO()
	{
	}
};


/*This is the structure used to send the display info and values from the builtin(device object)
 to the methods UI.
*/
struct ACTION_UI_DATA
{
	UI_DATA_TYPE userInterfaceDataType;		//TEXT_MESSAGE: builtin type depends on those bool variables below
											//EDIT/HARTDATE/UI_DATE_AND_TIME/TEME_VALUE_ENUMERATION/COMBO indicate input builtins
											//SELECTION indicates selection builtins
											//MENU indicates menu(UID) builtin
	UI_DATA_TYPE_TEXT_MESSAGE	textMessage;
	UI_DATA_TYPE_COMBO			ComboBox;
	UI_DATA_EDIT_BOX			EditBox;	
	bool						bMethodAbortedSignalToUI;	//indicate abort builtins
	bool						bUserAcknowledge;	// indicate ack builtins or info builtins
  	bool						bDisplayDynamic;	// indicate dynamic update
    bool						bDelayBuiltin;		// indicate delay builtins
	unsigned					uDelayTime;			// in seconds.
	PARAM_REF					opRef;
	CValueVarient					vUserRsp;			//used for user response
	//HANDLE						m_hUIBuiltinEvent;	//used for user response set by a seperate thread

     ACTION_UI_DATA() : userInterfaceDataType(UNKNOWN_UI_DATA_TYPE), bMethodAbortedSignalToUI(false),
		textMessage(), ComboBox(), EditBox(), bUserAcknowledge(false),bDisplayDynamic(false),
		uDelayTime(0xffffffff)
	{
		//m_hUIBuiltinEvent = NULL;
		// opRef has own constructor
	};

};


#endif // _METHODINTERFACEDEFS_H
