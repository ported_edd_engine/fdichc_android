#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif

//#include <afxwin.h>     // CAY this module does NOT use pre-compiled header file
#include <stdarg.h>
#include "PlatformCommon.h"

#include <ntcassert.h>
#include <BssProgLog.h>

#include "stdstring.h"
//#include "../../EDD_Engine_Common/APIs.h"
//class NtAssert

//	Narrow verion


//void* _ams_assert2(const char* exp, const char* fileName, unsigned lineNumber)
//{
//	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"assert(%S) failed in file %S at line %u", exp, fileName, lineNumber);
//	return NULL;
//}

// UNICODE version
void* _ams_assert(const wchar_t* exp, const wchar_t* fileName, unsigned lineNumber)
{
	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"assert(%s) failed in file %s at line %u", exp, fileName, lineNumber);
	return NULL;
}

void DoDebugBreak( DebugBreakState DbgState, LPCTSTR mylpString )
{
	PS_DoDebugBreak((DbgState!=0), mylpString);
}


/*****************************************************************************
 * @func void | BssProgLog |
 *
 * Logs an event.
 *
 * Some events (see below) are logged to Microsoft's event log; The filename,
 * lineNumber, severity, status, and formatted message are all stored as part
 * of a logged event.
 *
 * See the 'example' for full details.
 *
 * @precond
 * cFilename must point to a nul terminated filename string.
 *
 * wMsgFormat should point to a valid nul terminated wchar_t format string.
 *
 * All ... parameters should be valid for the usage in wMsgFormat.
 *
 * @postcond
 * none
 *
 * @globaleffects
 * Some events (see description) are reported to the Miscrosoft Event
 * Logging system.
 *
 * @exceptions
 * The break exception is sometimes raised as described above.
 *
 * @concurrency
 * Multi-task / Multi-thread safe.
 *
 * @notes
 * Final formated wMsgFormat string is limited to 2000 characters in length.
 * BssProgLog will truncate it as necessary.
 *
 * The Microsoft Event Logging system records the date/time when an event
 * is logged.
 *
 * The "example" has further information that may help clarify how
 * BssProgLog should and should not be used.
 *
 * @ex The following writeup and mail messages may help clarify how BssProgLog
 *     should and should not be used. |
 *
 * BssProgLog
 *
 * Logs an event.
 *
 * Some events (see below) are logged to Microsoft's event log; The filename,
 * lineNumber, severity, status, and formatted message are all stored as part
 * of a logged event.
 *
 * The SETTING controls what is logged and when breaks are instantiated.
 *
 * The SETTING is a 5 character string.
 *   SETTING[0] for BssError        severity
 *   SETTING[1] for BssWarning      severity
 *   SETTING[2] for BssInformation  severity
 *   SETTING[3] for BssAuditFailure severity
 *   SETTING[4] for BssAuditSuccess severity
 *
 * Each character of the SETTING can be one of the following:
 *   '.' - no message is logged, no break instantiated
 *   'L' - message is logged, no break instantiated
 *   'B' - message is logged, break instantiated
 *
 * The SETTING defaults are:
 *   "BLLLL" - For development (i.e. NDEBUG undefined)
 *   "L..L." - For release (i.e. NDEBUG defined)
 *
  * @syntax void BssProgLog(
 *     const char *cFilename,
 *     UINT32 lineNumber,
 *     WORD severity,
 *     BssStatus status,
 *     LPCWSTR wMsgFormat=NULL,
 *     ...
 *    );
 *
 * @parm const char* | cFilename | The source code filename,
 *                                 use __FILE__ macro.
 *
 * @parm UINT32 | lineNumber | The source code line number, use __LINE__ macro.
 *
 * @parm WORD | severity | The event severity. One of BssInformation
 * (informational), BssWarning (a warning), BssError (a severe error),
 * BssAuditSuccess (security check suceeded), BssAuditFailure (security
 * check failed).
 *
 * @parm BssStatus | status | Related BssStatus value.
 *
 * @parm LPCWSTR | wMsgFormat | NULL or event message format string
 * (like wprintf)
 *
 * @parmvar Additional parameters used in wMsgFormat.
 *
 *****************************************************************************/

void
BssProgLog (
   const char *cFilename,		// source code filename
   UINT32 lineNumber,			// source code line-number
   WORD severity,				// see documentation
   long status,					// a BssStatus value
   LPCWSTR wMsgFormat,			// a 'wprintf' format
   ...							//    and it's parameters
)
{
    if (wMsgFormat != NULL)
    {
        va_list  ap1{};
        va_start (ap1, wMsgFormat);
        PS_BssProgLog(cFilename, lineNumber, severity, status, wMsgFormat, ap1);
        va_end(ap1);
    }
}

