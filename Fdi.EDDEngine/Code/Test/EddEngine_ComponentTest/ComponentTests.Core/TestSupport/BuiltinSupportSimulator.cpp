#include "BuiltinSupportSimulator.h"
#include <iostream>
#include <sstream>
#include <stdarg.h>
#include <stdarg.h>

using namespace System;
using namespace System::Threading;
using namespace nsEDDEngine;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            BuiltinSupportSimulator::BuiltinSupportSimulator()
                : Recorder(new MockRecorder())
            {
                ReadValueCallCount = 0;
                WriteValueCallCount = 0;
                GetResponseCodeCallCount = 0;
                GetCommErrorCallCount = 0;
                SendCmdTransCallCount = 0;
                GetMoreStatusCallCount = 0;
                SendCommandCallCount = 0;
                LogMessageCallCount = 0;
                ListDeleteElementAtCallCount = 0;
                ListInsertCallCount = 0;
                GetParamValueCallCount = 0;
                SetParamValueCallCount = 0;
                AssignParamValueCallCount = 0;
                OnMethodExitingCallCount = 0;
                SendAllValuesCallCount = 0;
                GetUnscaledValueCallCount = 0;
                SetUnscaledValueCallCount = 0;
                AbortRequestCallCount = 0;
                AcknowledgementRequestCallCount = 0;
                DelayMessageRequestCallCount = 0;
                InfoRequestCallCount = 0;
                InputRequestCallCount = 0;
                SelectionRequestCallCount = 0;
                UIDRequestCallCount = 0;
                ParameterInputRequestCallCount = 0;

                ReadValueReturnValue = nsConsumer::BSEC_SUCCESS;
                WriteValueReturnValue = nsConsumer::BSEC_SUCCESS;
                GetResponseCodeReturnValue = nsConsumer::BSEC_SUCCESS;
                GetCommErrorReturnValue = nsConsumer::BSEC_SUCCESS;
                SendCmdTransReturnValue = nsConsumer::BSEC_SUCCESS;
                GetMoreStatusReturnValue = nsConsumer::BSEC_SUCCESS;
                SendCommandReturnValue = nsConsumer::BSEC_SUCCESS;
                ListDeleteElementAtReturnValue = nsConsumer::BSEC_SUCCESS;
                ListInsertReturnValue = nsConsumer::BSEC_SUCCESS;
                GetParamValueReturnValue = nsConsumer::PC_SUCCESS_EC;
                SetParamValueReturnValue = nsConsumer::PC_SUCCESS_EC;
                AssignParamValueReturnValue = nsConsumer::PC_SUCCESS_EC;
                SendAllValuesReturnValue = nsConsumer::BSEC_SUCCESS;
                GetUnscaledValueReturnValue = nsConsumer::PC_SUCCESS_EC;
                SetUnscaledValueReturnValue = nsConsumer::PC_SUCCESS_EC;
                AbortRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                AcknowledgementRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                DelayMessageRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                InfoRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                InputRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                SelectionRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                UIDRequestReturnValue = nsConsumer::BSEC_SUCCESS;
                ParameterInputRequestReturnValue = nsConsumer::BSEC_SUCCESS;

                _InvokeCallbackDelay = 0;
            }

            BuiltinSupportSimulator::~BuiltinSupportSimulator()
            {
                delete Recorder;
            }

            void BuiltinSupportSimulator::AddCalledMethod(const char* method, const char* paramsFmt, ...)
            {
                // build parameters format string
                int n, bufferSize = 128;
                char* buffer = new char[bufferSize];
                va_list ap;
                while (true)
                {
                    va_start(ap, paramsFmt);
                    n = vsnprintf_s(buffer, bufferSize, _TRUNCATE, paramsFmt, ap);
                    va_end(ap);
                    // check if string fit in buffer
                    if (n > -1 && n < bufferSize)
                        break;
                    // retry with bigger buffer
                    bufferSize *= 2;
                    delete buffer;
                    buffer = new char[bufferSize];
                }

                CalledMethods.push_back(method);

                std::string methodCallString(method);
                methodCallString.append("(");
                methodCallString.append(buffer);
                methodCallString.append(")");
                CalledMethodParameters.push_back(methodCallString);

                delete buffer;
            }

            std::string BuiltinSupportSimulator::GetCalledMethodsAsString()
            {
                std::string calledMethodsAsString;
                std::vector<std::string>::const_iterator iter;
                for (iter = CalledMethods.begin(); iter != CalledMethods.end(); ++iter)
                {
                    if (iter != CalledMethods.begin())
                        calledMethodsAsString.append(",");
                    calledMethodsAsString.append(*iter);
                }
                return calledMethodsAsString;
            }

            std::string BuiltinSupportSimulator::GetCalledMethodParametersAsString()
            {
                std::string calledMethodParametersAsString;
                std::vector<std::string>::const_iterator iter;
                for (iter = CalledMethodParameters.begin(); iter != CalledMethodParameters.end(); ++iter)
                {
                    if (iter != CalledMethodParameters.begin())
                        calledMethodParametersAsString.append(",");
                    calledMethodParametersAsString.append(*iter);
                }
                return calledMethodParametersAsString;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::ReadValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec)
            {
                ReadValueCallCount++;
                AddCalledMethod("ReadValue", "%i,%lu", iBlockInstance, pParamSpec->id);
                if (ReadValueFunction) return ReadValueFunction(iBlockInstance, pValueSpec, pParamSpec);
                return ReadValueReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::WriteValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec)
            {
                WriteValueCallCount++;
                AddCalledMethod("WriteValue", "%i,%lu", iBlockInstance, pParamSpec->id);
                if (WriteValueFunction) return WriteValueFunction(iBlockInstance, pValueSpec, pParamSpec);
                return WriteValueReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::GetResponseCode(void* pValueSpec, INT8 *pError_Class, INT8 *pError_Code, INT16 *pAddl_code, unsigned long *err_id, unsigned long *err_member_id)
            {
                GetResponseCodeCallCount++;
                AddCalledMethod("GetResponseCode", "");
                if (GetResponseCodeFunction) return GetResponseCodeFunction(pValueSpec, pError_Class, pError_Code, pAddl_code, err_id, err_member_id);
                return GetResponseCodeReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::GetCommError(void* pValueSpec, unsigned long *pError)
            {
                GetCommErrorCallCount++;
                AddCalledMethod("GetCommError", "");
                if (GetCommErrorFunction) return GetCommErrorFunction(pValueSpec, pError);
                return GetCommErrorReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::SendCmdTrans(void* pValueSpec, int cmd_num, int trans, unsigned char *cmd_status)
            {
                Recorder->RegisterCall(L"SendCmdTrans");
                Recorder->RegisterParameter(L"SendCmdTrans.cmd_num", new int(cmd_num));
                Recorder->RegisterParameter(L"SendCmdTrans.trans", new int(trans));

                cmd_status[0] = 0;
                cmd_status[1] = 0;
                cmd_status[2] = 0x10; //more status available

                return nsConsumer::BSEC_SUCCESS;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::GetMoreStatus(void* pValueSpec, unsigned char *more_data_status, unsigned char *more_data_info, int* more_data_info_size)
            {
                Recorder->RegisterCall(L"GetMoreStatus");

                return nsConsumer::BSEC_SUCCESS;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::SendCommand(void* pValueSpec, ITEM_ID command_id, long *pErrorValue)
            {
                SendCommandCallCount++;
                AddCalledMethod("SendCommand", "%lu", command_id);
                if (SendCommandFunction) return SendCommandFunction(pValueSpec, command_id, pErrorValue);
                return SendCommandReturnValue;
            }            

            int BuiltinSupportSimulator::isOffline(void* pValueSpec)
            {
                throw std::exception("The method or operation is not implemented.");
            }

            void BuiltinSupportSimulator::LogMessage(void* pValueSpec, int iPriority, wchar_t *message)
            {
                LogMessageCallCount++;
                AddCalledMethod("LogMessage", "%i,%ls", iPriority, message);
                if (LogMessageFunction) LogMessageFunction(pValueSpec, iPriority, message);
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::ListDeleteElementAt(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* op_ref, int start_index, int delete_count)
            {
                ListDeleteElementAtCallCount++;
                AddCalledMethod("ListDeleteElementAt", "%lu,%i,%i", op_ref->op_info.id, start_index, delete_count);
                if (ListDeleteElementAtFunction) return ListDeleteElementAtFunction(iBlockInstance, pValueSpec, op_ref, start_index, delete_count);
                return ListDeleteElementAtReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::ListInsert(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef )
            {
                ListInsertCallCount++;
                AddCalledMethod("ListInsert", "%lu,%i,%lu", pListOpRef->op_info.id, insert_index, pInsertedOpRef->op_info.id);
                if (ListInsertFunction) return ListInsertFunction(iBlockInstance, pValueSpec, pListOpRef, insert_index, pInsertedOpRef);
                return ListInsertReturnValue;
            }

            nsConsumer::PC_ErrorCode BuiltinSupportSimulator::GetDynamicAttribute(int iBlockInstance, void* pValueSpec,  const nsEDDEngine::OP_REF* op_ref, const AttributeName attributeName, /* out */ nsConsumer::EVAL_VAR_VALUE* pParamValue)
            {
                GetDynamicAttributeCallCount++;
                AddCalledMethod("GetDynamicAttribute", "%lu,%i", op_ref->op_info.id, (int)attributeName);
                if (GetDynamicAttributeFunction) return GetDynamicAttributeFunction(iBlockInstance, pValueSpec, op_ref, attributeName, pParamValue);
                return GetDynamicAttributeReturnValue;
            }

            nsConsumer::PC_ErrorCode BuiltinSupportSimulator::GetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ nsConsumer::EVAL_VAR_VALUE *pValue)
            {
                GetParamValueCallCount++;
                AddCalledMethod("GetParamValue", "%i,%p,%lu", iBlockInstance, pValueSpec, pParamSpec->id);

                const std::wstring methodName = L"GetParamValue";

                Recorder->RegisterCall(methodName);
                Recorder->RegisterParameter(methodName + L".pParamSpec", pParamSpec);
                
                *pValue = Recorder->IsReturnValueRecorded(methodName + L".pValue") ? *((nsConsumer::EVAL_VAR_VALUE*) Recorder->ReplayReturnValue(methodName + L".pValue")) : GetParamValueCallbackValue;

                return Recorder->IsReturnValueRecorded(methodName) ? *((nsConsumer::PC_ErrorCode*) Recorder->ReplayReturnValue(methodName)) : GetParamValueReturnValue;
            }

            nsConsumer::PC_ErrorCode BuiltinSupportSimulator::SetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ nsConsumer::EVAL_VAR_VALUE *pValue)
            {
                SetParamValueCallCount++;
                AddCalledMethod("SetParamValue", "%i,%p,%lu", iBlockInstance, pValueSpec, pParamSpec->id);

                const std::wstring methodName = L"SetParamValue";

                Recorder->RegisterCall(methodName);
                Recorder->RegisterParameter(methodName + L".pParamSpec", new nsEDDEngine::FDI_PARAM_SPECIFIER(*pParamSpec));
                if ((pValue->val.s.length() > 0) && pValue->val.s.c_str())
                {
                    Recorder->RegisterParameter(methodName + L".pValue", pValue);
                }
                else
                {
                    Recorder->RegisterParameter(methodName + L".pValue", new nsConsumer::EVAL_VAR_VALUE(*pValue));
                }

                return Recorder->IsReturnValueRecorded(methodName) ? *((nsConsumer::PC_ErrorCode*) Recorder->ReplayReturnValue(methodName)) : SetParamValueReturnValue;
            }

            nsConsumer::PC_ErrorCode BuiltinSupportSimulator::AssignParamValue(void* pValueSpec, int iDestBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, int iSrcBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec)
            {
                AssignParamValueCallCount++;
                AddCalledMethod("AssignParamValue", "%p,%i,%lu,%i,%lu", pValueSpec, iDestBlockInstance, pDestParamSpec->id, iSrcBlockInstance, pSrcParamSpec->id);
                if (AssignParamValueFunction) return AssignParamValueFunction(pValueSpec, iDestBlockInstance, pDestParamSpec, iSrcBlockInstance, pSrcParamSpec);
                return AssignParamValueReturnValue;
            }       

            nsConsumer::PC_ErrorCode BuiltinSupportSimulator::SetDynamicAttribute( int /*iBlockInstance*/, void* /*pValueSpec*/, const nsEDDEngine::OP_REF *pOpRef,
                const nsEDDEngine::AttributeName /*attributeName*/, const nsConsumer::EVAL_VAR_VALUE *pParamValue)
            {
                return nsConsumer::PC_ErrorCode::PC_SUCCESS_EC;
            }

            void BuiltinSupportSimulator::OnMethodExiting(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity)
            {
                OnMethodExitingCallCount++;
                AddCalledMethod("OnMethodExiting", "%i", eActivity);

                const std::wstring methodName = L"OnMethodExiting";

                Recorder->RegisterCall(methodName);
                Recorder->RegisterParameter(methodName + L".eTermAction", new nsEDDEngine::ChangedParamActivity(eActivity));

                if (OnMethodExitingFunction)
                {
                    OnMethodExitingFunction(iBlockInstance, pValueSpec, eActivity);
                }

                if (Recorder->IsReturnValueRecorded(methodName))
                {
                    Recorder->ReplayReturnValue(methodName);
                }
            }     

			void BuiltinSupportSimulator::OnMethodDebugInfo(int iBlockInstance, void* pValueSpec, unsigned long lineNumber,
				/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml)
			{

			}
            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::ProcessChangedValues(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity)
            {
                SendAllValuesCallCount++;
                AddCalledMethod("ProcessChangedValues", "");

                if (ProcessChangedValuesFunction)
                {
                    return ProcessChangedValuesFunction(iBlockInstance, pValueSpec, eActivity);
                }

                return SendAllValuesReturnValue;  
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::AbortRequest(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)
            {
                AbortRequestCallCount++;
                AddCalledMethod("AbortRequest", "%ls", pPrompt);

                Recorder->RegisterCall(L"AbortRequest");

                if (AbortRequestFunction)
                {
                    return AbortRequestFunction(pValueSpec, pPrompt, pCallback, state);
                }
                else
                {
                    if (pCallback != nullptr)
                    {
                        AsyncUICallback::Create(pCallback, state)->Start();
                    }
                }

                return AbortRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::AcknowledgementRequest(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)
            {
                AcknowledgementRequestCallCount++;
                AddCalledMethod("AcknowledgementRequest", "%ls", pPrompt);

                if (AcknowledgementRequestFunction)
                {
                    return AcknowledgementRequestFunction(pValueSpec, pPrompt, pCallback, state);
                }
                else
                {
                    AsyncUICallback::Create(pCallback, state)->Start();
                }

                return AcknowledgementRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::DelayMessageRequest(void* pValueSpec, unsigned long lSecondsToWait, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)
            {
                DelayMessageRequestCallCount++;
                AddCalledMethod("DelayMessageRequest", "%lu,%ls", lSecondsToWait, pPrompt);

                if (DelayMessageRequestFunction)
                {
                    return DelayMessageRequestFunction(pValueSpec, lSecondsToWait, pPrompt, pCallback, state);
                }
                else
                {
                    if (pCallback != nullptr)
                    {
                        AsyncUICallback::Create(pCallback, state, _InvokeCallbackDelay)->Start();
                    }
                }

                return DelayMessageRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::InfoRequest(void* pValueSpec, wchar_t* pPrompt, bool bPromptUpdate)
            {
                InfoRequestCallCount++;
                AddCalledMethod("InfoRequest", "%ls", pPrompt);

                if (InfoRequestFunction)
                {
                    return InfoRequestFunction(pValueSpec, pPrompt);
                }

                return InfoRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::InputRequest(void* pValueSpec, wchar_t* pPrompt, const nsConsumer::EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, AckCallback pCallback, void* state)
            {
                InputRequestCallCount++;
                AddCalledMethod("InputRequest", "%ls,%lu", pPrompt, pVar->id);
                Recorder->RegisterParameter(L"InputRequest.pValue", new nsConsumer::EVAL_VAR_VALUE(*pValue));

                if (InputRequestFunction)
                {
                    return InputRequestFunction(pValueSpec, pPrompt, pValue, pVar, pCallback, state);
                }
                else
                {
                    AsyncUICallback::Create(pCallback, state)->SetCallbackValue(InputRequestCallbackValue)->Start();
                }

                return InputRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::ParameterInputRequest( int iBlockInstance, void* pValueSpec, wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state )
            {
                ParameterInputRequestCallCount++;
                AddCalledMethod("ParameterInputRequest", "%ls", pPrompt);
                Recorder->RegisterParameter(L"ParameterInputRequest.pParameter", new nsEDDEngine::OP_REF_TRAIL(*pParameter));
                if(ParameterInputRequestFunction)
                {
                    return ParameterInputRequestFunction(iBlockInstance, pValueSpec, pPrompt, pParameter, pCallback, state);
                }
                else
                {
                    nsConsumer::EVAL_VAR_VALUE callbackValue;

                    AsyncUICallback::Create(pCallback, state)->SetCallbackValue(ParameterInputRequestCallbackValue)->Start();
                }

                return ParameterInputRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::SelectionRequest(void* pValueSpec, wchar_t* pPrompt, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)
            {
                SelectionRequestCallCount++;
                AddCalledMethod("SelectionRequest", "%ls,%ls", pPrompt, pOptions);

                if (SelectionRequestFunction)
                {
                    return SelectionRequestFunction(pValueSpec, pPrompt, pOptions, pCallback, state);
                }
                else
                {
                    nsConsumer::EVAL_VAR_VALUE callbackValue;
                    callbackValue.type = VT_UNSIGNED;
                    callbackValue.size = 4;
                    callbackValue.val.u = 0; // for test purpose only: the same as original selection

                    AsyncUICallback::Create(pCallback, state)->SetCallbackValue(callbackValue)->Start();
                }

                return SelectionRequestReturnValue;
            }

            nsConsumer::BS_ErrorCode BuiltinSupportSimulator::UIDRequest(int iBlockInstance, void* pValueSpec, ITEM_ID menu, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)
            {
                UIDRequestCallCount++;
                AddCalledMethod("UIDRequest", "%lu,%ls", menu, pOptions);

                if (UIDRequestFunction)
                {
                    return UIDRequestFunction(iBlockInstance,pValueSpec, menu, pOptions, pCallback, state);
                }
                else
                {
                    nsConsumer::EVAL_VAR_VALUE callbackValue;
                    callbackValue.type = VT_UNSIGNED;
                    callbackValue.size = 4;
                    callbackValue.val.u = 0; // for test purpose only: the same as original selection

                    AsyncUICallback::Create(pCallback, state)->SetCallbackValue(callbackValue)->Start();
                }

                return UIDRequestReturnValue;
            }

            void BuiltinSupportSimulator::SetInvokeCallBackDelay(int delayInMSec)
            {
                _InvokeCallbackDelay = delayInMSec;

            }


            // ***** AsyncUICallback helper class ***** //

            AsyncUICallback* AsyncUICallback::Create(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState)
            {
                return new AsyncUICallback(uiCallbackFunction, uiCallbackState, 0);
            }

            AsyncUICallback* AsyncUICallback::Create(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState, int delay)
            {
                return new AsyncUICallback(uiCallbackFunction, uiCallbackState, delay);
            }

            AsyncUICallback::AsyncUICallback(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState, int delay)
            {
                if (uiCallbackFunction == nullptr)
                {
                    throw std::exception("AckCallback of UI builtin is nullptr!");
                }
                _UICallbackFunction = uiCallbackFunction;
                _UICallbackState = uiCallbackState;
                _UICallbackSuccess = nsConsumer::BSEC_SUCCESS;
                _UICallbackValue.type = VT_UNSIGNED;
                _UICallbackValue.size = 4;
                _UICallbackValue.val.u = 0;

                _CallbackDelay = delay;
            }

            AsyncUICallback* AsyncUICallback::SetCallbackSuccess()
            {
                _UICallbackSuccess = nsConsumer::BSEC_SUCCESS;
                return this;
            }

            AsyncUICallback* AsyncUICallback::SetCallbackFailure()
            {
                _UICallbackSuccess = nsConsumer::BSEC_ABORTED;
                return this;
            }

            AsyncUICallback* AsyncUICallback::SetCallbackValue(const nsConsumer::EVAL_VAR_VALUE& value)
            {
                _UICallbackValue = value;
                return this;
            }

            void AsyncUICallback::Start()
            {
                LPVOID threadArg = static_cast<LPVOID>(this);
                DWORD dwThreadId;
                HANDLE hThread;

                hThread = CreateThread(NULL, NULL, &AsyncUICallback::ThreadProc, threadArg, 0, &dwThreadId);
            }

            void AsyncUICallback::InvokeCallback()
            {
                Thread::Sleep(_CallbackDelay);
                (*_UICallbackFunction)(&_UICallbackValue, _UICallbackSuccess, _UICallbackState);
                delete this;
            }

            DWORD WINAPI AsyncUICallback::ThreadProc(LPVOID arg)
            {
                AsyncUICallback* asyncUICallback = static_cast<AsyncUICallback*>(arg);

                asyncUICallback->InvokeCallback();

                return 0;
            }

        } // namespace ComponentTestsSupport
    } // namespace Core
} // namespace ComponentTests
