/*************************************************************************************************
 *
 * $Workfile: ddbVar.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the variable instance class
 *		7/26/2	sjv	 created from ddbVariable.h
 *	
 *
 * #include "ddbVar.h"
 */

#ifndef _DDBVAR_H
#define _DDBVAR_H
#ifdef INC_DEBUG
#pragma message("In ddbVar.h") 
#endif

#pragma warning (disable : 4786) 

#include "varient.h"		//for CValueVariant class and Char.h
#include "ddbItemBase.h"
#include "ddbAttributes.h"
#include "../APPS/APPsupport/BuiltinLib/RTN_CODE.H"


#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbVar.h") 
#endif


/*****************************************************************************
 * Definitions (#define, typedef, enum)
 *****************************************************************************
 */

/*************************************************************************************************
 * hCVar
 *
 * this is base class for VARIABLEs, enahances the CItemBase to provide support
 * for size, class, handling, read/write command lists, pre/post actions, and
 * variable type.
 ******************************************************************************
 */

class hCattrVarDefault;


class hCVar : public hCitemBase 
{
	protected:
		variableType_t varType;

		hCcommandDescriptor	readCommand;	// 0xff == NONE 
		hCcommandDescriptor writeCommand;	// 0xff == NONE.
		wstring returnString; //
		bool ReadParameterData(CValueVarient *pOutput, wchar_t *sValue );
		CValueVarient vtDefaultVal{};
		hCRangeList RangeList;

public:

		bool IsDynamic(void);				//the only class the counts
		bool IsReadOnly(void);
		bool IsValid();     /*returns true if the item is currently valid*///return validity
		bool IsVariable(void) {return true; };
		virtual bool IsNumeric(void){return false;};
		virtual int VariableType(void) { return ((int)vT_undefined);}
		virtual unsigned int  VariableSize(void);

		virtual bool isChanged(void){ return false; }
		virtual void  getaAttr(varAttrType_t attrType, CValueVarient *pvtOutValue);/*Variables ONLY - override for others*/


	protected:
		virtual RETURNCODE getMinMaxList(hCRangeList& retList);//true @ val2Check inside min-max set

	public:
		/*******************************************/
		// usually called via hCVar::newVar(hCVar*) as a global function						
		//static hCVar* newVar(hCVar* pVariable);
		//static hCVar* newVar      (DevInfcHandle_t h, aCitemBase * paItem);
		hCattrBase*   newHCattr(DevInfcHandle_t h, aCattrBase* pACattr);  // builder 

		/******* * required methods - ALL children must have these * *******                */
		/* can't make them pure virtual methods because of the template usage of this class */
		/*																					*/
		virtual RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len);


	    hCVar(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCVar(DevInfcHandle_t h, variableType_t vt, int vSize); 
		hCVar(hCVar* pSrc,  itemIdentity_t newID);/* from-typedef constructor */
		virtual ~hCVar();
		RETURNCODE destroy(void);
		virtual double GetScalingFactor();
		virtual int getValue(CValueVarient& newValue);
		virtual int setValue(CValueVarient& newValue);

		RETURNCODE Label(wstring& retStr); 
		virtual RETURNCODE Help(wstring &h);
		wstring getUnitStr(void){ wstring s; getUnitString(s); return s; }
		RETURNCODE getUnitString(wstring & str);// stevev 30nov06 - now gets const | rel unit

		/* size test - implemented for numeric only */
		virtual bool isInRange(CValueVarient val2Check){return true;};//true @ val2Check inside min-max set
		hCRangeList& getRangeList() { return RangeList; };
	private:
		void InitializeStringToCharacter(wchar_t oneChar,int iValSize, wstring * returnedString);
};

/******************************************************************************
 * Cnumeric
 *
 * base class for numeric data types.  enhnaces the DDVariable class to support
 * min/max values, scaling, units, display and edit formats.
 ******************************************************************************
 */
class hCNumeric : public hCVar 
{
	public:
		hCNumeric(DevInfcHandle_t h, aCitemBase* paItemBase);//body in ddbvar.cpp.
		hCNumeric(DevInfcHandle_t h, variableType_t vt, int vSize);// allow temporary variables
		hCNumeric(hCNumeric* pSrc,  hCNumeric* pVal, itemIdentity_t newID);/* from-typedef */
		virtual ~hCNumeric() { };

		void clear(void){ };
		bool IsNumeric(void)  { return true; };

		/*** * pure virtual methods - ALL children must have these * ******/
		//virtual RETURNCODE Add(BYTE *byteCount, BYTE **data, ulong mask, int& len){ return SUCCESS; };
		
		//used by mee and builtins
		RETURNCODE ReadForEdit(wstring &editStr);
		RETURNCODE ReadForDisp(wstring &dispStr);

		virtual double GetScalingFactor();
};

/******************************************************************************
 * CFloat
 *
 ******************************************************************************
 */
class hCFloat : public hCNumeric 
{
	public:
		hCFloat(DevInfcHandle_t h,aCitemBase* paItemBase);//,hCVar* pV);
		hCFloat(hCFloat*   pSrc,  hCFloat*   pVal,itemIdentity_t newID);
		hCFloat(DevInfcHandle_t h); // for temporary variables

		bool isChanged(void);
		virtual int VariableType(void) { return ((int)vT_FloatgPt);}
};

/******************************************************************************
 * CDouble
 *
 ******************************************************************************
 */
class hCDouble : public hCNumeric 
{
	public:
		hCDouble(DevInfcHandle_t h,aCitemBase* paItemBase);//,hCVar* pV);
		hCDouble(hCDouble*  pSrc,  hCDouble*  pVal, itemIdentity_t newID);
		bool isChanged(void);	
		virtual int VariableType(void) { return ((int)vT_Double);}
};

/******************************************************************************
 * hCinteger
 *
 * base class for integer data types.  There are too many simularities not to
 * have a single integer class that is common
 ******************************************************************************
 */
class hCinteger : public hCNumeric 
{
	public:
		hCinteger(DevInfcHandle_t h, aCitemBase* paItemBase, bool isS = false); 
		hCinteger( hCinteger*  pSrc, hCinteger*  pVal, itemIdentity_t newID);

		virtual bool isChanged(void);

		virtual int VariableType(void) { return ((int)vT_Integer);}
};


/******************************************************************************
 * CUnsigned
 *
 ******************************************************************************
 */
class hCUnsigned : public hCinteger 
{
	public:
		hCUnsigned(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCUnsigned(hCUnsigned* pSrc,  hCUnsigned*  pVal, itemIdentity_t newID) : hCinteger((hCinteger*)pSrc,(hCinteger*)pVal,newID){};
		virtual int VariableType(void);
};

/******************************************************************************
 * CSigned
 *
 ******************************************************************************
 */
class hCSigned : public hCinteger 
{
	public:
		hCSigned(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCSigned(hCSigned* pSrc,  hCSigned* pVal, itemIdentity_t newID) : hCinteger((hCinteger*)pSrc,pVal,newID){};
		
};


/******************************************************************************
 * CTimeValue
 *
 ******************************************************************************
 */
class hCTimeValue : public hCUnsigned 
{
	public:
		hCTimeValue(DevInfcHandle_t h, aCitemBase* paItemBase):hCUnsigned(h,paItemBase){}
		hCTimeValue(hCTimeValue* pSrc,  hCTimeValue*  pVal, itemIdentity_t newID):hCUnsigned((hCUnsigned*)pSrc,(hCUnsigned*)pVal,newID){};
		virtual int VariableType(void) { return ((int)vT_TimeValue);}
};

/******************************************************************************
 * CDateAndTime
 *
 ******************************************************************************
 */
class hCDateAndTime : public hCUnsigned //public hCVar //public hCNumeric //public hCInterger
{
	public:
		hCDateAndTime(DevInfcHandle_t h, aCitemBase* paItemBase):hCUnsigned(h,paItemBase){}
		hCDateAndTime( hCDateAndTime* pSrc,  hCDateAndTime* pVal, itemIdentity_t newID):hCUnsigned((hCUnsigned*)pSrc,(hCUnsigned*)pVal,newID){};
		virtual int VariableType(void) { return ((int)vT_DateAndTime);}
};

/******************************************************************************
 * Enumeration class set
 *
 * hCEnum and supporting classes
 ******************************************************************************
 */


/*  the actual holder of the enumeration information */
class hCenumDesc : public hCobject
{
	// CcommAPI* m_pComm;
public:

	unsigned long   val;   // numeric value for this enumeration wstring
	hCddlString		descS; // this enumeration wstring
	hCddlString     helpS; // help for this enumeration wstring


	hCbitString		func_class;
	hCreference     actions; // reference to a method 
							 //   - actually a (conditional?) list of references????
	

	hCenumDesc(DevInfcHandle_t h)
		:hCobject(h), descS(h),helpS(h),func_class(h),actions(h)  { clear();};

	hCenumDesc( const hCenumDesc& hCed ) ;
	virtual ~hCenumDesc()	{ descS.destroy();helpS.destroy();actions.destroy();oclasslist.clear(); }; //VMKP  260304
	RETURNCODE destroy(void){ 
		descS.destroy();helpS.destroy();actions.destroy(); oclasslist.clear();return SUCCESS;};

	//BIT_ENUM_STATUS?
	unsigned long   status_class;
	outStatusList_t oclasslist;  // oclasses...Empty when kind_oclasses is OC_NORMAL

	void  setEqual(void* pAdesc); 
	void  setDuplicate(hCenumDesc* pED);
	void  clear(void){/*m_pComm=NULL;*/
					  val=0;descS.clear();helpS.clear();func_class.clear();
					  actions.clear();status_class=0;oclasslist.clear();};
};

typedef vector<hCenumDesc> enumDescList_t;
typedef hCenumDesc*        ptr2EnumDesc_t;

class hCenumList : public hCpayload, public hCobject, public enumDescList_t
{
private:
	tchar szReturnValue[1024+1];
public:
	hCenumList(DevInfcHandle_t h) : hCobject(h) {};
	hCenumList(const hCenumList& src):hCobject(src.devHndl()) {operator=(src);}//stevev 13jun05
	
	void duplicate(hCpayload* pPayLd, bool replicate);

	//VMKP 260304
	virtual ~hCenumList(){   
		FOR_this_iT(vector<hCenumDesc>,this)
		{// iT is ptr 2a hCenumDesc
			iT->destroy();
		}
		clear(); 
        //TODO fixit below
#if defined(_WIN32) || defined(__ANDROID__)
                vector<hCenumDesc>::~vector();
#else
                this->~vector();
#endif

    }
	//VMKP 260304
	RETURNCODE destroy(void){ RETURNCODE rc = 0;
		FOR_this_iT(vector<hCenumDesc>,this)
		{// iT is ptr 2a hCenumDesc
//			clog << "Destroying EnumList"<<endl;
			rc |= iT->destroy();
		}return rc;
	};

	// some functions to perform on this list
	tchar* GetStringValue( const UINT32 val);

	void  setEqual(void* pAclass); // set this from the aEnum	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType){ if (ldType != pltEnumList) return false; 
											  else return true;};	
	void  append(hCenumList* pList2Append);

	bool BuildFromEnumXML(wstring sXml);

	hCenumList& operator= (const hCenumList& src)
	{  *((hCpayload*)this) = src; *((vector<hCenumDesc>*)this) = src;    return *this;};
	bool operator== (/*const*/ hCenumList& src);
	RETURNCODE dumpSelf(/*int indent = 0*/){ 
		LOGIT(COUT_LOG,"%sEnumList does not dump at this time.\n","	"/*MI_LOG::Space(indent)*/);return 0;};
};


typedef struct EnumTriad_s
{
	unsigned long   val;
	wstring          descS;
	wstring          helpS;

	struct EnumTriad_s& operator=(const struct EnumTriad_s& s)
        {  val = s.val;descS=s.descS;helpS=s.helpS;return *this;}
	struct EnumTriad_s& operator=( class hCenumDesc& s)
        {
           val = s.val;
	       descS=s.descS.procureVal(/*s.theComm()*/);
           helpS=s.helpS.procureVal(/*s.theComm()*/);
           return *this;
        }
    EnumTriad_s(const struct EnumTriad_s& s)
        { operator=(s); }
    EnumTriad_s()
        {val = 0;}// default
}
/*typedef*/ EnumTriad_t;
typedef vector<EnumTriad_t> triadList_t;

/******************************************************************************
 * CEnum
 *
 * this is base class for Enumerated VARIABLEs.  enhances the hCVar type
 * by providing access to the text for the list of enumerations
 ******************************************************************************
 */
class hCEnum: public hCinteger // was: hCVar /* was hCVar:hCitemBase*/
{
	protected:
		hCenumList Resolved;

	public:
		hCEnum(DevInfcHandle_t h,aCitemBase* paItemBase);
		hCEnum(hCEnum*    pSrc,  hCEnum*    pVal, itemIdentity_t newID);
		virtual ~hCEnum(){};
		virtual RETURNCODE destroy(void){ return SUCCESS; };

		virtual RETURNCODE procureValue (const wstring& keyStr, UINT32& value);// input wstring, get a value
		virtual RETURNCODE procureString(const UINT32 value, wstring& mtchStr);// input value, get a wstring
		virtual RETURNCODE procureList( hCenumList& returnList); // get the resolved list
		
		/* stevev 3/19/04 - added to support enums as indexes in E&H DDs */
		int getAllValidValues(UIntList_t& returnList);// returns count in list
		
		virtual int VariableType(void) { return ((int)vT_Enumerated);}
};


/*****************************************************************************
 * CBitEnum
 *
 *****************************************************************************
 */
class hCBitEnum : public hCEnum 
{	
	public:
		hCBitEnum(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCBitEnum(hCBitEnum*  pSrc,  hCBitEnum*  pVal, itemIdentity_t newID) : hCEnum((hCEnum*)pSrc,pVal,newID){ };
		void copyAttrDesc(hCVar* pV);
		RETURNCODE setSize( hv_point_t& returnedSize, ulong /*qual*/ )
		{ 
			returnedSize.clear();
			return 1;
		};

	// try to use parents
		virtual RETURNCODE procureString(const UINT32 value, wstring& mtchStr);

		virtual CValueVarient getValue(ulong indexMask);
		int setValue(CValueVarient& newValue,  ulong indexMask);// mask tells which bits to change
		virtual int VariableType(void) { return ((int)vT_BitEnumerated);}
};


/*****************************************************************************
 * hCindex
 *
 *****************************************************************************
 */
class hCindex : public hCinteger
{
		wstring     resolved;
	public:
		hCindex(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCindex(hCindex*    pSrc,  hCindex*    pVal, itemIdentity_t newID);
		//VMKP 260304
		virtual ~hCindex();
		hCattrVarIndex*  pIndexed;
		int getAllValidValues(UIntList_t& returnList);
		virtual int VariableType(void) { return ((int)vT_Index);}

		virtual RETURNCODE procureString(const UINT32 value, wstring& mtchStr);
};


/*************************************************************************************************
 * hCBitString
 *
 *************************************************************************************************
 */
class hCBitString : public hCVar 
{
	public:
		hCBitString(DevInfcHandle_t h, aCitemBase* paItemBase);
		virtual int VariableType(void) { return ((int)vT_BitString);}		
};

/*************************************************************************************************
 * Cascii
 *
 *************************************************************************************************
 */
class hCascii : public hCVar 
{
	public:
		hCascii(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCascii(hCascii* pSrc, hCascii* pVal, itemIdentity_t newID);
  		
		bool IsPassword(void);
		bool isChanged(void);
		virtual int VariableType(void) { return ((int)vT_Ascii);}		
};

/******************************************************************************
 * CpackedAscii
 *
 ******************************************************************************
 */
class hCpackedAscii : public hCascii 
{
	public:
		hCpackedAscii(DevInfcHandle_t h, aCitemBase*    paItemBase);
		hCpackedAscii(hCpackedAscii* pSrc,  hCpackedAscii* pVal, itemIdentity_t newID);

		virtual int VariableType(void) { return ((int)vT_PackedAscii);}		
		
};


/******************************************************************************
 * Cpassword
 *		ascii with flag set
 ******************************************************************************
 */
class hCpassword : public hCascii 
{
	public:
		hCpassword(DevInfcHandle_t h, aCitemBase* paItemBase);
		hCpassword(hCpassword* pSrc,  hCpassword* pVal, itemIdentity_t newID);
		virtual int VariableType(void) { return ((int)vT_Password);}		
};

/******************************************************************************
 * ChartDate
 *   signed long
 ******************************************************************************
 */

#define DATE_BYTECNT  3

class hChartDate : public hCinteger //public hCVar //public hCNumeric
{
	public:
		hChartDate(DevInfcHandle_t h, aCitemBase* paItemBase);//,hCVar* pV);
		hChartDate( hChartDate* pSrc,  hChartDate* pVal, itemIdentity_t newID);

		//dtEditFmt_t editFormat;
		void clear(void){hCinteger::clear();/* editFormat = undefined;*/};
		virtual int setValue(CValueVarient& newValue);
		bool ValidateDate(const INT32);
		virtual int VariableType(void) { return ((int)vT_HartDate);}
};


#endif//_DDBVARIABLE_H

/*************************************************************************************************
 *
 *   $History: ddbVar.h $
 * 
 * *****************  Version 5  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
