#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <wtypes.h>


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_loading_and_reading_an_int_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_int_item_ID;
            static FLAT_VAR* test_int;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_int", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_int = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_int_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_int", test_int_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_int_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_int->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_int()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_int->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_int_parameter()
            {
                Assert::AreEqual(*test_int_item_ID, test_int->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_int()
            {
                Assert::AreEqual((int) 4, (int) test_int->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_float_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_float_item_ID;
            static FLAT_VAR* test_float;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_float", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_float = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_float_item_ID = new ITEM_ID();
                int returnCode =engine->Instance->GetItemIdFromSymbolName(L"test_float", test_float_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_float_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_float->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_float()
            {
                Assert::AreEqual((int) VT_FLOAT, (int) test_float->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_float_parameter()
            {
                Assert::AreEqual(*test_float_item_ID, test_float->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_float()
            {
                Assert::AreEqual((int) 4, (int) test_float->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_double_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_double_item_ID;
            static FLAT_VAR* test_double;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_double", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_double = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_double_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_double", test_double_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_double_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_double->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_double()
            {
                Assert::AreEqual((int) VT_DOUBLE, (int) test_double->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_double_parameter()
            {
                Assert::AreEqual(*test_double_item_ID, test_double->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_double()
            {
                Assert::AreEqual((int) 8, (int) test_double->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_enumerated_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_enumerated_item_ID;
            static FLAT_VAR* test_enumerated;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_enumerated", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_enumerated = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_enumerated_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_enumerated", test_enumerated_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_enumerated_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_enumerated->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_enumerated()
            {
                Assert::AreEqual((int) VT_ENUMERATED, (int) test_enumerated->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_enumerated_parameter()
            {
                Assert::AreEqual(*test_enumerated_item_ID, test_enumerated->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_enumerated()
            {
                Assert::AreEqual((int) 1, (int) test_enumerated->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_bit_enumerated_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_bit_enumerated_item_ID;
            static FLAT_VAR* test_bit_enumerated;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_bit_enumerated", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_bit_enumerated = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_bit_enumerated_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_bit_enumerated", test_bit_enumerated_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_bit_enumerated_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_bit_enumerated->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_bit_enumerated()
            {
                Assert::AreEqual((int) VT_BIT_ENUMERATED, (int) test_bit_enumerated->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_bit_enumerated_parameter()
            {
                Assert::AreEqual(*test_bit_enumerated_item_ID, test_bit_enumerated->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_bit_enumerated()
            {
                Assert::AreEqual((int) 1, (int) test_bit_enumerated->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_ascii_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_ascii_item_ID;
            static FLAT_VAR* test_ascii;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_ascii", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_ascii = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_ascii_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_ascii", test_ascii_item_ID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_ascii_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_ascii->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_ascii()
            {
                Assert::AreEqual((int) VT_ASCII, (int) test_ascii->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_ascii_parameter()
            {
                Assert::AreEqual(*test_ascii_item_ID, test_ascii->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_ascii()
            {
                Assert::AreEqual((int) 10, (int) test_ascii->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_uint_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_uint_item_ID;
            static FLAT_VAR* test_uint;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_uint", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_uint = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_uint_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_uint", test_uint_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_uint_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_uint->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_uint()
            {
                Assert::AreEqual((int) VT_UNSIGNED, (int) test_uint->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_uint_parameter()
            {
                Assert::AreEqual(*test_uint_item_ID, test_uint->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_uint()
            {
                Assert::AreEqual((int) 4, (int) test_uint->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_packed_ASCII_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_packed_ascii_item_ID;
            static FLAT_VAR* test_packed_ascii;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_packed_ascii", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_packed_ascii = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_packed_ascii_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_packed_ascii", test_packed_ascii_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_packed_ascii_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_packed_ascii->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_packed_ascii()
            {
                Assert::AreEqual((int) VT_PACKED_ASCII, (int) test_packed_ascii->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_packed_ascii_parameter()
            {
                Assert::AreEqual(*test_packed_ascii_item_ID, test_packed_ascii->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_packed_ascii()
            {
                Assert::AreEqual((int) 10, (int) test_packed_ascii->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_password_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_password_item_ID;
            static FLAT_VAR* test_password;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_password", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_password = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_password_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_password", test_password_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_password_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_password->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_password()
            {
                Assert::AreEqual((int) VT_PASSWORD, (int) test_password->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_password_parameter()
            {
                Assert::AreEqual(*test_password_item_ID, test_password->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_password()
            {
                Assert::AreEqual((int) 10, (int) test_password->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_bitstring_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_bitstring_item_ID;
            static FLAT_VAR* test_bitstring;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_bitstring", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_bitstring = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_bitstring_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_bitstring", test_bitstring_item_ID);

                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_bitstring_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_bitstring->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_bitstring()
            {
                Assert::AreEqual((int) VT_BITSTRING, (int) test_bitstring->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_bitstring_parameter()
            {
                Assert::AreEqual(*test_bitstring_item_ID, test_bitstring->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_bitstring()
            {
                Assert::AreEqual((int) 24, (int) test_bitstring->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_date_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_date_item_ID;
            static FLAT_VAR* test_date;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_date", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_date = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_date_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_date", test_date_item_ID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_date_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_date->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_date()
            {
                Assert::AreEqual((int) VT_EDD_DATE, (int) test_date->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_date_parameter()
            {
                Assert::AreEqual(*test_date_item_ID, test_date->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_date()
            {
                Assert::AreEqual((int) 3, (int) test_date->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_time_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_time_item_ID;
            static FLAT_VAR* test_time;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_time", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_time = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_time_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_time", test_time_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_time_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_time->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_time()
            {
                Assert::AreEqual((int) VT_TIME, (int) test_time->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_time_parameter()
            {
                Assert::AreEqual(*test_time_item_ID, test_time->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_time()
            {
                Assert::AreEqual((int) 6, (int) test_time->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_date_and_time_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_date_and_time_item_ID;
            static FLAT_VAR* test_date_and_time;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_date_and_time", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_date_and_time = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_date_and_time_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_date_and_time", test_date_and_time_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_date_and_time_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_date_and_time->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_date_and_time()
            {
                Assert::AreEqual((int) VT_DATE_AND_TIME, (int) test_date_and_time->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_date_and_time_parameter()
            {
                Assert::AreEqual(*test_date_and_time_item_ID, test_date_and_time->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_date_and_time()
            {
                Assert::AreEqual((int) 7, (int) test_date_and_time->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_time_value_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_time_value_item_ID;
            static FLAT_VAR* test_time_value;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_time_value", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_time_value = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_time_value_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_time_value", test_time_value_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_time_value_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_time_value->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_time_value()
            {
                Assert::AreEqual((int) VT_TIME_VALUE, (int) test_time_value->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_time_value_parameter()
            {
                Assert::AreEqual(*test_time_value_item_ID, test_time_value->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_time_value()
            {
                Assert::AreEqual((int) 8, (int) test_time_value->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_duration_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_duration_item_ID;
            static FLAT_VAR* test_duration;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_duration", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_duration = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the item ids for all parameters
                test_duration_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_duration", test_duration_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_duration_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_duration->valid == True);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_duration()
            {
                Assert::AreEqual((int) VT_DURATION, (int) test_duration->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_duration_parameter()
            {
                Assert::AreEqual(*test_duration_item_ID, test_duration->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_duration()
            {
                Assert::AreEqual((int) 6, (int) test_duration->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_array_double_parameter_in_PB
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_array_double_item_ID;
            static FLAT_ARRAY* test_array_double;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0003.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_array_double", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_array_double = dynamic_cast<FLAT_ARRAY*>(genericItem->item);

                // get the item ids for all parameters
                test_array_double_item_ID = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_array_double", test_array_double_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_array_double_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_array_double->valid == True);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_array_double_parameter()
            {
                Assert::AreEqual(*test_array_double_item_ID, test_array_double->id);
            }

        };
    }
}
