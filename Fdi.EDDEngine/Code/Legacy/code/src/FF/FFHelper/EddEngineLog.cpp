#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif

//#include <afxwin.h>     // CAY this module does NOT use pre-compiled header file

#include "stdinc.h"

#include <Inf/NtcSpecies/BssProgLog.h>

#include "../../../../../Inc/FF/FFEDDEngine/DDSSupport.h"


static void EddEngineLog_work (
   IDDSSupport	*pDDSSupport,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   UINT severity,						// see documentation
   wchar_t* wCategory,						// a BssStatus value
   LPCWSTR wMsgFormat,			// a 'printf' format
   va_list ap1							//    the rest of the arguments
)
{
	(void)ap1;
	(void)wMsgFormat;
	(void)wCategory;
	(void)severity;
	(void)lineNumber;
	(void)pDDSSupport;
	(void)cFilename;
    /*
	USES_CONVERSION;


	// Strings
	const TCHAR *cfilenameIsMissing  = L"FileName is Missing";
	const TCHAR *cnoMsgAvailable     = L"No Message is availible";

	// other constants
	static const long msgBfrDim = 3001;

	const int msgsDim = 4;        // dimension of msgAP

	// msg Array of Pointers to strings
	// to put in the event report
	// [0] = filename
	// [1] = lineNumber (decimal)
	// [2] = passed in event id
	// [3] = formatted message or raw msg if formatting fails
	const TCHAR *msgAP[msgsDim] = {0,0,0,0};

	// Verify a filename was passed in, and if not include this in the message
	//
	msgAP[0] = (cFilename != NULL) ? A2CT(cFilename) : cfilenameIsMissing;

	// lineNumber
	TCHAR cLineNumber[32] = { 0 }; // holds more than max UINT32 (4,ddd,ddd,ddd)
	_stprintf_s(cLineNumber,_T("%lu"), lineNumber);
	msgAP[1] = cLineNumber;

	// EventId is set equal to status
	

	//TCHAR cEventID[60] = { 0 }; // holds more than 2 * max INT32 (4,ddd,ddd,ddd)
	//_stprintf_s(cEventID,_T("%d 0x%X"), eventId, eventId);
	//msgAP[2] = cEventID;

	//
	// 'formated' message
	TCHAR msgBfr[msgBfrDim] = {0};

	if (wMsgFormat != NULL)
	{
		PS_VsnwPrintf(msgBfr, msgBfrDim, W2CT(wMsgFormat), ap1);	// Need to convert wMsgFormat to a TCHAR since _vsntprintf needs it

		// the formatted message
		msgAP[2] = msgBfr;
	}
	else
	{
		msgAP[2] = cnoMsgAvailable;
		//++msgsN;
	}



	// build debug string
	CStdString myString; 

	myString.Format(L"%s, %s, %s\n", msgAP[0], msgAP[1], msgAP[2]);

	// we should only break if we are not Information or Warning messages.
	if(severity == Error || severity == Critical)
	{
		if(PS_IsDebuggerPresent())
		{
			DoDebugBreak(EnableDebugBreak, myString);
		}
	}

	pDDSSupport->DDS_Log( myString.GetBuffer(), (nsConsumer::LogSeverity)severity, wCategory);
    */
}

void EddEngineLog (
   IDDSSupport *pDDSSupport,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   UINT severity,						// see documentation
   wchar_t* wCategory,						// a BssStatus value
   LPCWSTR wMsgFormat,			// a 'printf' format
   ...							//    and it's parameters
)
{
    va_list  ap1 = {};			// Get the rest of the arguments
	va_start (ap1, wMsgFormat);

	EddEngineLog_work( pDDSSupport, cFilename, lineNumber, severity, wCategory, wMsgFormat, ap1);

	va_end   (ap1);
}

void EddEngineLog (
   ENV_INFO	*env_info,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   UINT severity,						// see documentation
   wchar_t* wCategory,						// a BssStatus value
   LPCWSTR wMsgFormat,			// a 'printf' format
   ...							//    and it's parameters
)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);

    va_list  ap1 = {};			// Get the rest of the arguments
	va_start (ap1, wMsgFormat);


	EddEngineLog_work( pDDSSupport, cFilename, lineNumber, severity, wCategory, wMsgFormat, ap1);

	va_end   (ap1);
}
