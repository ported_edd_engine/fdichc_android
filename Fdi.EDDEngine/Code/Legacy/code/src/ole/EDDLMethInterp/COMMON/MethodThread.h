
#pragma once

#include <nsEDDEngine/ProtocolType.h>
using nsEDDEngine::ProtocolType;
#include "nsEDDEngine/Attrs.h"
using nsEDDEngine::ACTION_LIST;
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IEDDEngineLogger.h"
//#include <comutil.h>
#include <ddbdefs.h>
#include "CMethodInterpreterDevice.h"

/////////////////////////////////////
//	Method thread class.
//
class CMethodThread/* : public CWinThread*/
{
	public:
        CMethodThread( /*HWND hDeviceWindow,*/ long lMethodID, ProtocolType protocol );
		virtual ~CMethodThread();
		virtual BOOL InitInstance();

		//Methods called by COM interface
		ProtocolType GetProtocolType(){ return m_protocol;	};
		void SetError( long lError ){ m_lErrorCode = lError; }

		//Methods called by Method Interpreter thread.
		void SetCancel(bool isCancelled);
		bool IsThreadCancelled(){ return m_bCancel; }
		virtual nsEDDEngine::Mth_ErrorCode ExecuteMethodFunction() = 0;
		virtual int GetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen ) = 0;
		virtual int SendCommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize) = 0;
		virtual void CommitValues(nsEDDEngine::ChangedParamActivity onExitActivity) = 0;
		virtual int GetItemByItemId2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, unsigned long ulMemberID, CStdString sProperty, CValueVarient *pOutput ) = 0;
		virtual int GetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, ACTION_LIST *pOutput) = 0;
		virtual int GetItemIdByName( CStdString sItemName, unsigned long *pItemId ) = 0;
		virtual int SendListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount) = 0;
		virtual int SendUIExecute(ACTION_UI_DATA& stUIData) = 0;
		virtual int ReceiveUIMethRspExecute(CValueVarient& vtRsp) = 0;
		virtual int GetRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol) = 0;
		virtual int GetDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)=0;
		virtual int GetLiteralString( unsigned long ulIndex, wchar_t *pOutput, long lStrLen ) = 0;
		virtual int GetResponseCodeStringExecute(int iItemId, int iMemberId, int iRsponseCode, CValueVarient *pvtValue) = 0;
		virtual int getDdsErrorExecute(wchar_t* pErrorString) = 0;
		virtual unsigned long getCommErrorExecute() = 0;
		virtual int SendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType) = 0;
		virtual int getResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId) = 0;
		virtual int assignExecute(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef) = 0;

	    virtual int threadAccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType) = 0;

		virtual int threadAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eType, AccessType eAccessType, long lSize) = 0;
		virtual int threadAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType) = 0;
		virtual int threadWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal) = 0;
		virtual ITEM_ID resolveBlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType) = 0;
		virtual int getBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount)=0;
		virtual int getBlockInstanceByObjIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex) = 0;
		virtual int getBlockInstanceByBlockTag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex) = 0;
		virtual int GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data) = 0;
		virtual void threadLogMessage(int priorityVal, wchar_t *msg) = 0;
		virtual void threadMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml) = 0;
		virtual int threadIsOffline() = 0;
		virtual long threadSendCommand(unsigned long commandId, long *commandError) = 0;
		virtual int threadAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType) = 0;
		virtual int threadGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString) = 0;

		virtual void threadMILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0) = 0;
		void SetDevicePointer(CMethodInterpreterDevice* pDevice) { m_pDevice = pDevice; }
		CMethodInterpreterDevice* GetDevicePointer() { return m_pDevice; }


	protected:
		long MapStringToPPType( CStdString sPropertyType, CValueVarient& vMember );

        //HWND m_hDeviceWindow;
		long m_lMethodID;

		ProtocolType m_protocol;
		long m_lErrorCode;
		bool m_bCancel;
		CMethodInterpreterDevice* m_pDevice;

};
