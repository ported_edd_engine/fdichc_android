#pragma once

#include "nsEDDEngine/ProtocolType.h"
#include "nsConsumer/IParamCache.h"
#include "nsEDDEngine/Flats.h"
#include "nsEDDEngine/Attrs.h"
//#include "typedefs.h"


namespace nsConsumer
{

	//Return code from IBuiltinSupport requests
	enum BS_ErrorCode {
		BSEC_SUCCESS			= 0,	// Request was successful
		BSEC_WRONG_DATA_TYPE	= 1,	// Argument or variable type is wrong
		BSEC_ABORTED			= 2,	// Request has been cancelled for any reason
		BSEC_NO_DEVICE			= 3,	// Device is not dispatched or there was no response from the device
		BSEC_FAIL_RESPONSE		= 4,	// Response code caused Builtin to fail
		BSEC_FAIL_COMM			= 5,	// Communication error caused Builtin to fail
		BSEC_NO_LANGUAGE_STRING	= 6,	// No valid string for current language
		BSEC_OTHER				= 7		// Any other error
	};

	// IBuiltinSupport Provides functionality required to complete Builtin calls 
	// when they are encountered by the Method Execution Engine.
	class IBuiltinSupport
	{
	public:
		virtual ~IBuiltinSupport(void) {};

		// TODO MHD Determine whether the IBuiltinSupport should be passed in at a device or block level
		// If at a device level, iBlockInstance needs to be specified whenever a PARAM Specifier is used.
		// If at a block level, iBlockInstance is only specified when a param could be in a different block (read_value, read_value2)

	//
	// Comm Builtins Support
	//		Route to Communications component

		// FF
		virtual BS_ErrorCode ReadValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec ) = 0;
		virtual BS_ErrorCode WriteValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec ) = 0;

		virtual BS_ErrorCode GetResponseCode( void* pValueSpec,
							// from FF-870 (Fieldbus Message Spec) Section 6.1 "Parameter Error Type"
							// See also Section 10.1.4 "ServiceError" to Section 10.1.4.4 "Error Class" for values
                            /* [out] */ INT8 *pError_Class,	// Error Class
                            /* [out] */ INT8 *pError_Code,	// Error Code
                            /* [out] */ INT16 *pAddl_code,	// Additional Code
							/* [out] */ unsigned long *err_id, /* [out] */ unsigned long *err_subindex ) = 0;

		virtual BS_ErrorCode GetCommError( void* pValueSpec, unsigned long *pError ) = 0;

		// HART
		// cmd_status[3] has the following possible values. also see HARTsupport.h

		// cmd_status[0] - RESPONSE CODE - Hi Bit is CLEAR in first byte, use them as an integer; There could be more integers.
		// CR_NO_SPECIFIC_ERROR			0
		// CR_INVALID_SELECTION			2
		// CR_PARAM_TOO_LARGE			3
		// CR_PARAM_TOO_SMALL			4
		// CR_DATA_TOO_FEW				5
		// CR_DEV_COMMAND_ERROR			6
		// CR_WRITE_PROTECT_MODE		7
		// CR_ACCESS_RESTRICTED			16
		// CR_DEVICE_BUSY				32
		// CR_COMMAND_NOT_IMPL			64

		// cmd_status[1] - COMMUNICATION STATUS - Hi Bit is SET in first byte; second byte is zero
		// DEV_PARITYERR		0xC0	// hi bit and bit 6
		// DEV_OVERUNERR		0xA0	// hi bit and bit 5
		// DEV_FRAMINGERR		0x90	// hi bit and bit 4
		// DEV_CHKSUMERR		0x88	// hi bit and bit 3
		// DEV_BIT2RSVD			0x84	// hi bit and bit 2 - reserved
		// DEV_RXBUFFOVER		0x82	// hi bit and bit 1
		// DEV_BIT0RSVD			0x81	// hi bit and bit 0 - reserved

		// cmd_status[2] - DEVICE STATUS - Second byte after a command response code <non-communication error>
		// DS_DEVMALFUNCTION	0x80	// bit 7
		// DS_CONFIGCHANGED		0x40	// bit 6
		// DS_COLDSTART			0x20	// bit 5
		// DS_MORESTATUSAVAIL	0x10	// bit 4
		// DS_OUTCURRENTFIXED	0x08	// bit 3
		// DS_ANALGOUTSATURATE	0x04	// bit 2
		// DS_PRIMVAROUTLIMIT	0x02	// bit 1
		// DS_NONPRIMOUTLIMIT	0x01	// bit 0

					//                                         [in]         [in]           [out, size(3)]
		virtual BS_ErrorCode SendCmdTrans( void* pValueSpec, int cmd_num, int trans, unsigned char *cmd_status ) = 0;

					//                                              [out, size(3)]               [out, size(64)]  [out] size of more_data_info buffer
		virtual BS_ErrorCode GetMoreStatus( void* pValueSpec, unsigned char *cmd_status, unsigned char *more_data_info, int* more_data_info_size ) = 0;	// Sends Cmd 48
		
		// PB
		virtual BS_ErrorCode SendCommand( void* pValueSpec, ITEM_ID command_id, long *pErrorValue) = 0;

	//
	// Offline Support
	//		isOffline  returns a non-zero value when the Host Application state is currently "Offline". Otherwise, it returns zero.
		virtual int isOffline( void* pValueSpec) = 0;

	//
	// Debug Builtins Support
		virtual void LogMessage( void* pValueSpec, int iPriority, wchar_t *message) = 0; // used for _ERROR, _WARNING, _TRACE and LOG_MESSAGE

		virtual void OnMethodDebugInfo(int iBlockInstance, void* pValueSpec, unsigned long lineNumber,
			/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml) = 0; // used for method debug info

	//
	// List Cache Builtins Support
	//		Route to List Cache

		virtual BS_ErrorCode ListDeleteElementAt( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int start_index, int delete_count ) = 0;

		virtual BS_ErrorCode ListInsert( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef ) = 0;


	//
	// Dynamic Attribute Support
	//
        // These methods will Get or Set the Dynamic Attribute that is requested.
		//		Dynamic Attributes are those whose values are not stored in the EDD,
		//		like VARIABLE_STATUS, VIEW_MIN, VIEW_MAX, etc.
		// pOpRef        specifies a reference to the item
		// attributeName specifies the attribute that is to be gotten or set
        // pParamValue   contains the value to be gotten or set.

		virtual PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ EVAL_VAR_VALUE * pParamValue) = 0;

		virtual PC_ErrorCode SetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName,  /* in */ const EVAL_VAR_VALUE * pParamValue) = 0;


	//
	// Param Cache Builtins Support
	//		Route to Param Cache
		virtual PC_ErrorCode GetParamValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ EVAL_VAR_VALUE *pValue ) = 0;
		virtual PC_ErrorCode SetParamValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ EVAL_VAR_VALUE *pValue ) = 0;
		virtual PC_ErrorCode AssignParamValue( void* pValueSpec, int iDestBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, 
													int iSrcBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec ) = 0;

		virtual void OnMethodExiting ( int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity ) = 0;
	
		// If eActivity = Save, all changed values are saved to param cache. This function is used for Hart buitin save_values().
		// If eActivity = Send, all changed values are sent to device. This function is used for FF builtin send_all_values(). 
		virtual BS_ErrorCode ProcessChangedValues(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity) = 0;


	//
	//  UI Builtins Support
	//		Route to UI
		typedef void (*AckCallback)(EVAL_VAR_VALUE *pvUserInput, BS_ErrorCode eStatus, void *state);
	
		// Async - This call should return immediatly.
		// If the pPrompt is not NULL, it should be displayed to the user. When the user presses the Ok button, the pCallback should be called.
		// If pPrompt and pCallback are NULL, this is simply a signal to the host that we have started abort proccesing. The callback is not used.
		virtual BS_ErrorCode AbortRequest( void* pValueSpec, wchar_t* pPrompt, AckCallback pCallback, void* state ) = 0;

		// Async; after receiving the AcknowledgementResponse.
		// the callback function is called with a scope (VT_ERROR: E_ABORT, S_OK) to indicate whether it was cancelled or not.
		virtual BS_ErrorCode AcknowledgementRequest(void* pValueSpec,  wchar_t* pPrompt, 
						AckCallback pCallback, void* state ) = 0;

		// Async - This call should return immediatly.
		// This request will be called twice per delay builtin.
		//   - The first call is when the builtin is started. Here the lSecondsToWait will be non-zero.
		//   - The second call is when the delayed time is expired. Now lSecondsToWait will be zero.
		// If the pPrompt is non-zero, it should be displayed to the user. If it is null, it is simply a signal to the host that a delay is occurring.
		// If pCallback is non-zero, then the user is provided with an ABORT button, which he can use to abort the method via the pCallback.
		virtual BS_ErrorCode DelayMessageRequest( void* pValueSpec, unsigned long lSecondsToWait, wchar_t* pPrompt,
						AckCallback pCallback, void* state ) = 0;

		// Sync, Must return immediately, so dynamic update is possible
		// bPromptUpdate='true'  - this InfoRequest() is dynamically updating a previous UI request prompt.
		// bPromptUpdate='false' - this InfoRequest() is a new UI request.
		virtual BS_ErrorCode InfoRequest( void* pValueSpec, wchar_t* pPrompt, bool bPromptUpdate ) = 0;

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest.
										// pValue is InitialValue, which is set to final value during call
		// the callback function is called with a scope (VT_ERROR: E_ABORT) or other variable types when user hits okay button.
		virtual BS_ErrorCode InputRequest( void* pValueSpec, wchar_t* pPrompt, /* in */ const EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, 
						AckCallback pCallback, void* state ) = 0;

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest.
										// pValue is InitialValue, which is set to final value during call
		// the callback function is called with a scope (VT_ERROR: E_ABORT) or other variable types when user hits okay button.
		virtual BS_ErrorCode ParameterInputRequest( int iBlockInstance, void* pValueSpec, wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, 
						AckCallback pCallback, void* state ) = 0;

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest.
		// the callback function is called with a scope (VT_ERROR: E_ABORT) or CVT_UI4 type when user hits okay button.
		// option list in *pOptions is 0-based index
		virtual BS_ErrorCode SelectionRequest( void* pValueSpec, wchar_t* pPrompt, wchar_t* pOptions,
						AckCallback pCallback, void* state) = 0;

		//block until user presses okay button
		// option list in *pOptions is 0-based index
		virtual BS_ErrorCode UIDRequest ( int iBlockInstance, void* pValueSpec, ITEM_ID menu, wchar_t* pOptions, 
						AckCallback pCallback, void* state) = 0;

	};

}
