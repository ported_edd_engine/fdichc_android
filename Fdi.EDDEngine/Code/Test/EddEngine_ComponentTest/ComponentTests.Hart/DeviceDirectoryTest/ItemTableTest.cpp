#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass]
        public ref class When_getting_the_items_of_a_hart_dd
        {
        private:
            TestContext^ testContextInstance;

        public: 
            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            property Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ TestContext
            {
                Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ get()
                {
                    return testContextInstance;
                }
                System::Void set(Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ value)
                {
                    testContextInstance = value;
                }
            };

            /// <summary>
            /// Tests if the EDD Engine reports the correct number of variables for the
            /// Test-DD (Deltabar S).
            ///</summary>
            [TestMethod, TestCategory("ItemTableTest")]
            void It_should_contain_the_expected_number_of_variables()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\DeltabarS.fm8");                
                engine->Initialize();                

                FLAT_DEVICE_DIR *flat_device_dir;
                int returnCode = engine->Instance->GetDeviceDir(&flat_device_dir);
                Assert::AreEqual(0, returnCode);

                int varCount = 0;
                for (int nIndex = 0; nIndex < flat_device_dir->item_tbl.count; nIndex++)
                {
                    ITEM_TBL_ELEM item = flat_device_dir->item_tbl.list[nIndex];

                    if (item.item_type == ITYPE_VARIABLE)
                    {
                        varCount++;
                    }
                }

                Assert::AreEqual(204, varCount);
            }

            /// <summary>
            /// Tests if the EDD Engine reports the correct variable symbol names.
            ///</summary>
            [TestMethod, TestCategory("ItemTableTest")]
            void It_should_have_common_hart_variables_with_their_expected_item_ids()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\DeltabarS.fm8");
                engine->Initialize();                

                int returnCode;
                FLAT_DEVICE_DIR *flat_device_dir;
                returnCode = engine->Instance->GetDeviceDir(&flat_device_dir);
                Assert::AreEqual(0, returnCode);

                System::Collections::Generic::Dictionary<int, System::String^>^ variables =
                    gcnew System::Collections::Generic::Dictionary<int, System::String^>();

                for (int nIndex = 0; nIndex < flat_device_dir->item_tbl.count; nIndex++)
                {
                    ITEM_TBL_ELEM item = flat_device_dir->item_tbl.list[nIndex];

                    if (item.item_type == ITYPE_VARIABLE)
                    {
                        FDI_GENERIC_ITEM* genericItem = engine->GetVariableItem(item.item_id);
                        Assert::AreEqual((unsigned short)0, genericItem->errors.count);

                        FLAT_VAR *flat_var = dynamic_cast<FLAT_VAR*>(genericItem->item);
                        Assert::IsTrue(flat_var != nullptr);

                        variables->Add(item.item_id, gcnew System::String(flat_var->symbol_name.c_str()));

                        delete genericItem;
                    }
                }

                Assert::AreEqual(204, variables->Count);
                Assert::AreEqual("response_code", variables[150]);
                Assert::AreEqual("device_status", variables[151]);
                Assert::AreEqual("comm_status", variables[152]);
                Assert::AreEqual("manufacturer_id", variables[153]);
                Assert::AreEqual("device_type", variables[154]);
                Assert::AreEqual("request_preambles", variables[155]);
                Assert::AreEqual("universal_revision", variables[156]);
                Assert::AreEqual("transmitter_revision", variables[157]);
                Assert::AreEqual("software_revision", variables[158]);
                Assert::AreEqual("hardware_revision", variables[159]);
                Assert::AreEqual("device_flags", variables[160]);
                Assert::AreEqual("device_id", variables[161]);
                Assert::AreEqual("polling_address", variables[162]);
                Assert::AreEqual("tag", variables[163]);
                Assert::AreEqual("message", variables[164]);
                Assert::AreEqual("descriptor", variables[165]);
                Assert::AreEqual("date", variables[166]);
                Assert::AreEqual("write_protect", variables[167]);
            }

            [TestMethod, TestCategory("ItemTableTest")]
            void It_should_not_crash_on_excessive_calls_to_get_its_variables()
            {
                for (int i = 0; i < 10; i++)
                {
                    It_should_have_common_hart_variables_with_their_expected_item_ids();
                }
            }

            /// <summary>
            /// Tests if the EDD Engine correctly handle parameters with unavailable dependencies.
            ///</summary>
            [TestMethod, TestCategory("ItemTableTest")]
            void It_should_return_the_correct_item_id_of_not_available_dependent_variable()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\DeltabarS.fm8");
                ParamCacheSimulator *paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                
                ITEM_ID itemId_lin_units_edit = 16426;
                ITEM_ID itemId_linearisation_mode = 16392;

                // make linearisation_mode unavailable
                nsEDDEngine::FDI_PARAM_SPECIFIER paramSpecifier;
                paramSpecifier.eType = nsEDDEngine::FDI_PS_ITEM_ID;
                paramSpecifier.id = itemId_linearisation_mode;
                paramCacheSimulator->SetParamValue(&paramSpecifier, 0);

                FDI_GENERIC_ITEM* genericItem = engine->GetVariableItem(itemId_lin_units_edit);
                Assert::AreEqual((unsigned short)1, genericItem->errors.count);

                Assert::AreEqual(itemId_linearisation_mode, genericItem->errors.list[0].var_needed.op_info.id);

                Assert::AreEqual(-2006, genericItem->errors.list[0].rc);
                delete engine;
                delete genericItem;
            }
        };

    } // namespace Hart
} // namespace ComponentTests
