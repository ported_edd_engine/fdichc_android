#include "stdafx.h"
#include <afx.h>
#include "BuildDDSXML.h"
#include "BuildDDSXMLElemtsAndAttr.h"
#include "ParamCache.h"
#include "EDDEngineLogger.h"
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <afxmt.h>


#include "Shlobj.h"

#define TEST_LANGUAGE L"|en|"

// Do a detailed flat copy/assignment test for each flat type every FLAT_TEST_INTERVAL time
#define FLAT_TEST_INTERVAL 600

CLASSSTRUCT attribute_names_list[] = 
{
		{	item_information,					"item_information"			},
		{	label,								"label"						},
		{	help,								"help"						},
		{	validity,							"validity"					},
		{	members,							"members"					},
		{	handling,							"handling"					},
		{	response_codes,						"response_codes"			},
		{	height,								"height"					},
		{	post_edit_actions,					"post_edit_actions"			},
		{	pre_edit_actions,					"pre_edit_actions"			},
		{	refresh_actions,					"refresh_actions"			},
		{	relation_update_list,				"relation_update_list"		},
		{	relation_watch_list,				"relation_watch_list"		},
		{	relation_watch_variable,			"relation_watch_variable"	},
		{	width,								"width"						},
		{	nsEDDEngine::AttributeName::access,	"access"					}, /* conflicting this symbol ("access") with function in io.h hence providing full name */
		{	class_attr,							"class_attr"				},
		{	constant_unit,						"constant_unit"				},
		{	cycle_time,							"cycle_time"				},
		{	emphasis,							"emphasis"					},
		{	exit_actions,						"exit_actions"				},
		{	identity,							"identity"					},
		{	init_actions,						"init_actions"				},
		{	line_color,							"line_color"				},
		{	line_type,							"line_type"					},
		{	max_value,							"max_value"					},
		{	min_value,							"min_value"					},
		{	command_number,						"command_number"			},
		{	post_read_actions,					"post_read_actions"			},
		{	post_write_actions,					"post_write_actions"		},
		{	pre_read_actions,					"pre_read_actions"			},		
		{	pre_write_actions,					"pre_write_actions"			},
		{	type_definition,					"type_definition"			},
		{	y_axis,								"y_axis"					},
		{	appinstance,						"appinstance"				},
		{	axis_items,							"axis_items"				},
		{	block_b,							"block_b"					},
		{	block_b_type,						"block_b_type"				},
		{	capacity,							"capacity"					},
		{	characteristics,					"characteristics"			},
		{	chart_items,						"chart_items"				},
		{	chart_type,							"chart_type"				},
		{	charts,								"charts"					},
		{	collection_items,					"collection_items"			},
		{	block_number,						"block_number"				},
		{	count,								"count"						},
		{	default_value,						"default_value"				},
		{	default_values,						"default_values"			},
		{	definition,							"definition"				},
		{	display_format,						"display_format"			},
		{	display_items,						"display_items"				},
		{	edit_display_items,					"edit_display_items"		},
		{	edit_format,						"edit_format"				},
		{	edit_items,							"edit_items"				},
		{	elements,							"elements"					},
		{	enumerations,						"enumerations"				},
		{	file_items,							"file_items"				},
		{	first,								"first"						},
		{	graph_items,						"graph_items"				},
		{	graphs,								"graphs"					},
		{	grid_items,							"grid_items"				},
		{	grids,								"grids"						},
		{	image_items,						"image_items"				},
		{	image_table_index,					"image_table_index"			},
		{	index,								"index"						},
		{	indexed,							"indexed"					},
		{	initial_value,						"initial_value"				},
		{	items,								"items"						},
		{	keypoints_x_values,					"keypoints_x_values"		},
		{	keypoints_y_values,					"keypoints_y_values"		},
		{	last,								"last"						},
		{	length,								"length"					},
		{	link,								"link"						},
		{	list_items,							"list_items"				},
		{	lists,								"lists"						},
		{	local_parameters,					"local_parameters"			},
		{	menu_items,							"menu_items"				},
		{	menus,								"menus"						},
		{	method_items,						"method_items"				},
		{	method_parameters,					"method_parameters"			},
		{	method_type,						"method_type"				},
		{	methods,							"methods"					},
		{	number_of_elements,					"number_of_elements"		},
		{	number_of_points,					"number_of_points"			},	
		{	on_update_actions,					"on_update_actions"			},
		{	operation,							"operation"					},
		{	orientation,						"orientation"				},
		{	parameter_lists,					"parameter_lists"			},
		{	parameters,							"parameters"				},
		{	post_user_actions,					"post_user_actions"			},
		{	post_rqst_actions,					"post_rqst_actions"			},
		{	uuid,								"uuid"						},
		{	reference_array_items,				"reference_array_items"		},
		{	refresh_items,						"refresh_items"				},
		{	scaling,							"scaling"					},
		{	scaling_factor,						"scaling_factor"			},
		{	slot,								"slot"						},
		{	source_items,						"source_items"				},
		{	style,								"style"						},
		{	sub_slot,							"sub_slot"					},
		{	time_format,						"time_format"				},
		{	time_scale,							"time_scale"				},
		{	transaction,						"transaction"				},
		{	unit_items,							"unit_items"				},
		{	variable_type,						"variable_type"				},
		{	vectors,							"vectors"					},
		{	waveform_items,						"waveform_items"			},
		{	waveform_type,						"waveform_type"				},
		{	write_as_one_items,					"write_as_one_items"		},
		{	component_byte_order,				"component_byte_order"		},
		{	x_axis,								"x_axis"					},
		{	x_increment,						"x_increment"				},
		{	x_initial,							"x_initial"					},
		{	x_values,							"x_values"					},
		{	y_values,							"y_values"					},
		{	view_min,							"view_min"					},
		{	view_max,							"view_max"					},
		{	reserved117,						"reserved117"				},
		{	reserved118,						"reserved118"				},
		{	reserved119,						"reserved119"				},
		{	addressing,							"addressing"				},
		{	can_delete,							"can_delete"				},
		{	check_configuration,				"check_configuration"		},
		{	classification,						"classification"			},
		{	component_parent,					"component_parent"			},
		{	component_path,						"component_path"			},
		{	component_relations,				"component_relations"		},
		{	components,							"components"				},
		{	declaration,						"declaration"				},
		{	detect,								"detect"					},
		{	device_revision,					"device_revision"			},
		{	device_type,						"device_type"				},
		{	edd,								"edd"						},
		{	manufacturer,						"manufacturer"				},
		{	maximum_number,						"maximum_number"			},
		{	minimum_number,						"minimum_number"			},
		{	protocol,							"protocol"					},
		{	redundancy,							"redundancy"				},
		{	relation_type,						"relation_type"				},
		{	component_connect_point,			"component_connect_point"	},
		{	scan,								"scan"						},
		{	scan_list,							"scan_list"					},
		{	private_attr,						"private_attr"				},
		{	visibility,							"visibility"				},
		{	product_uri,						"product_uri"				},
		{	api,								"api"						},
		{	header,								"header"					},
		{	write_mode,							"write_mode"				},
		{	plugin_items,						"plugin_items"				},
		{	files,								"files"						},
		{	plugins,							"plugins"					},
		{	initial_values,						"initial_values"			},
		{	post_rqstreceive_actions,			"post_rqstreceive_actions"	},
		{	shared,								"shared"					},
		{	variable_status,					"variable_status"			},
		{	count_ref,							"count_ref"					},
		{	min_value_ref,						"min_value_ref"				},
		{	max_value_ref,						"max_value_ref"				},
		{	image_id,							"image_id"					},
		{	_end_,								"_end_"						}
};	

CLASSSTRUCT class_names_list[] = 
{
	{	CT_NONE,				"Unassigned"		},
	{	CT_DIAGNOSTIC, 			"Diagnostic"		},
	{	CT_DYNAMIC,				"Dynamic"			},
	{	CT_SERVICE,				"Service"			},
	{	CT_CORRECTION,			"Correction"		},
	{	CT_COMPUTATION,			"Computation"		},
	{	CT_ANALOG_INPUT,		"Input Block"		},
	{	CT_ANALOG_OUTPUT,		"Analog"			},
	{	CT_HART,				"Hart"				},
	{	CT_LOCAL_DISPLAY,		"Local Display"		},
	{	CT_FREQUENCY_INPUT,		"Frequency"			}, 
	{	CT_DISCRETE_INPUT,		"Discrete"			},
	{	CT_DEVICE,				"Device"			},
	{	CT_LOCAL_deprecated,	"Local_depricated"	},
	{	CT_INPUT,				"Input"				},
	{	CT_OPERATE,				"Operate"			},
	{	CT_ALARM,				"Alarm"				},
	{	CT_ANALOG_CHANNEL,		"Anolog Channel"	},
	{	CT_CONTAINED, 			"Contained"			},
	{	CT_DIGITAL_INPUT,		"Digital Input"		},
	{	CT_DIGITAL_OUTPUT,		"Digital Output"	},
	{	CT_DISCRETE_OUTPUT,		"Discrete Output"	},
	{	CT_FREQUENCY_OUTPUT,	"Frequency Input"	},
	{	CT_TUNE,				"Tune"				},
	{	CT_OUTPUT,				"Output"			},
	{	CT_DISCRETE,			"Discrete"			},
	{	CT_BACKGROUND,			"Background"		},
	{	CT_FREQUENCY,			"Frequency"			},
	{	CT_MODE,				"Mode"				},
	{	CT_RANGE,				"Range"				},
	{	CT_OPTIONAL,			"Optional"			},
	{	CT_TEMPORARY,			"Temporary"			},
	{	CT_FACTORY,				"Factory"			},
	{   CT_LOCAL_A,				"Local_a"			},
	{   CT_LOCAL_B,				"Local_b"			},
	{	CT_CONSTANT,			"Constant"			},
	{	CT_SPECIALIST,			"Specialist"		},
	{	CT_IS_CONFIG,			"Is Config"		    },
};

CLASSSTRUCT variable_list[] = 
{
		
	{	VT_DDS_TYPE_UNUSED,		"unused"			},
	{	VT_INTEGER,				"integer"			},
	{	VT_UNSIGNED,			"unsigned"			},
	{	VT_FLOAT,				"float"				},
	{	VT_DOUBLE,				"double"			},
	{	VT_ENUMERATED,			"enumerated"		},
	{	VT_BIT_ENUMERATED,		"bit-enumerated"	},
	{	VT_INDEX,				"index"				},
	{	VT_ASCII,				"ascii"				},
	{	VT_PACKED_ASCII,		"packed-ascii"		},
	{	VT_PASSWORD,			"password"			},
	{	VT_BITSTRING,			"bitstring"			},
	{	VT_EDD_DATE,			"date"				},
	{	VT_TIME,				"HART-time"			},
	{	VT_DATE_AND_TIME,		"date-and-time"		},
	{	VT_DURATION,			"duration"			},
	{	VT_EUC,					"euc"				},
	{	VT_OCTETSTRING,			"octet-string"		},
	{	VT_VISIBLESTRING,		"visible-string"	},
	{	VT_TIME_VALUE,			"time_value"		},
	{	VT_OBJECT_REFERENCE,	"object-reference"	},
	{	VT_BOOLEAN,				"boolean"			},

};

CLASSSTRUCT chart_type_list[] = 
{
	{	CTYPE_GAUGE,		"GAUGE" 			},
	{	CTYPE_HORIZ_BAR,	"HORIZONTAL_BAR"	},
	{	CTYPE_SCOPE,		"SCOPE" 			},
	{	CTYPE_STRIP,		"STRIP"				},
	{	CTYPE_SWEEP,		"SWEEP" 			},
	{	CTYPE_VERT_BAR,		"VERTICAL_BAR"		},
};

CLASSSTRUCT command_type_list[] =
{
	{	FDI_DATA_CONSTANT,			"DATA_CONSTANT"			},
	{	FDI_DATA_REFERENCE,			"DATA_REFERENCE"		},
	{	FDI_DATA_REF_FLAGS,			"DATA_REF_FLAGS"		},
	{	FDI_DATA_REF_WIDTH,			"DATA_REF_WIDTH"		},
	{	FDI_DATA_REF_FLAGS_WIDTH,	"DATA_REF_FLAGS_WIDTH"	},
	{	FDI_DATA_FLOATING,			"DATA_FLOATING"			},
	{	FDI_DATA_STRING,			"DATA_STRING"			},
	{	FDI_DATA_UNSIGNED,			"DATA_UNSIGNED"			},
	{	FDI_DATA_DOUBLE,			"DATA_DOUBLE"			},
};

CLASSSTRUCT waveform_type_list[] = 
{
	{	FDI_XY_WAVEFORM_TYPE,			"XY" 			},
	{	FDI_YT_WAVEFORM_TYPE,			"YT"			},
	{	FDI_HORZ_WAVEFORM_TYPE,			"HORIZONTAL" 	},
	{	FDI_VERT_WAVEFORM_TYPE,			"VERTICAL"		},
};

CLASSSTRUCT method_type_list[] = 
{
	{	MT_VOID,				"Void" 					},
	{	MT_INT_8,				"Signed Char"			},
	{	MT_INT_16,				"Short"					},
	{	MT_INT_32,				"Int & Long"			},
	{	MT_FLOAT,				"Floating Point"		},
	{	MT_DOUBLE,				"Double"				},
	{	MT_UINT_8,				"Unsigned Char"			},
	{	MT_UINT_16,				"Unsigned Short"		},
	{	MT_UINT_32,				"Unsigned Int & Long"	},
	{	MT_INT_64,				"Unsupported"			},
	{	MT_UINT_64,				"Unsupported"			},
	{	MT_DDSTRING,			"DD String"				},
	{	MT_DD_ITEM,				"DD Item"				},
};

//these strings match what is in the sym file.
CLASSSTRUCT item_type_list[] = 
{
	{	ITYPE_NO_VALUE,					""				},
	{	ITYPE_VARIABLE,					"variable"		},
	{	ITYPE_COMMAND,					"command"		},
	{	ITYPE_MENU,						"menu"			},
	{	ITYPE_EDIT_DISP,				"edit-display"	},
	{	ITYPE_METHOD,					"method"		},
	{	ITYPE_REFRESH,					"refresh"		},
	{	ITYPE_UNIT,						"unit"			},
	{	ITYPE_WAO,						"wao"			},
	{	ITYPE_ITEM_ARRAY,				"reference-array"	},
	{	ITYPE_COLLECTION,				"collection"	},
	{	ITYPE_BLOCK_B,					"block-b"		},
	{	ITYPE_BLOCK,					"block"			},
	{	ITYPE_RECORD,					"record"		},
	{	ITYPE_ARRAY,					"value-array"	},
	{	ITYPE_VAR_LIST,					"var-list"		},
	{	ITYPE_RESP_CODES,				"response-code"	},
	{	ITYPE_MEMBER,					"member"		},
	{   ITYPE_FILE,						"file"			},
	{	ITYPE_CHART,					"chart"			},
	{	ITYPE_GRAPH,					"graph"			},
	{	ITYPE_AXIS,						"axis"			},
	{	ITYPE_WAVEFORM,					"waveform"		},
	{	ITYPE_SOURCE,					"source"		},
	{	ITYPE_LIST,						"list"			},
	{	ITYPE_GRID,						"grid"			},
	{	ITYPE_IMAGE,					"image"			},
	{	ITYPE_BLOB,						"blob"			},
	{	ITYPE_PLUGIN,					"plugin"		},
	{	ITYPE_TEMPLATE,					"template"		},
	{	ITYPE_RESERVED,					"reserved"		},
	{	ITYPE_COMPONENT,				"component"		},
	{	ITYPE_COMPONENT_FOLDER,			"component-folder"		},
	{	ITYPE_COMPONENT_REFERENCE,		"component-reference" },
	{	ITYPE_COMPONENT_RELATION,		"component-relation"	},
	{	ITYPE_SEPARATOR,				"seperator"		},
	{	ITYPE_ROWBREAK,					"rowbreak"		},
	{	ITYPE_COLUMNBREAK,				"columnbreak"	},
	{	ITYPE_ENUM_BIT,					"enum-bit"		},
	{	ITYPE_STRING_LITERAL,			"stringliteral" },

	{	ITYPE_CONST_INTEGER,			"const-interger"},
	{	ITYPE_CONST_FLOAT,				"const-float"	},
	{	ITYPE_CONST_UNSIGNED,			"const-unsigned"},
	{	ITYPE_CONST_DOUBLE,			    "const-double"	},
	{	ITYPE_SELECTOR,					"selector"		},
	{	ITYPE_LOCAL_PARAM,				"local-param"	},
	{	ITYPE_METH_ARGS,				"meth-args"		},
	{	ITYPE_ATTR,						"attr"			},
    {   ITYPE_BLOCK_XREF,               "block-xref"    },
	
};

CLASSSTRUCT handling_list[] = 
{
	{ 	FDI_READ_HANDLING, 			"Read"			},
	{ 	FDI_WRITE_HANDLING, 		"Write"			},
};

CLASSSTRUCT menu_style_list[] = 
{
	{	FDI_DIALOG_STYLE_TYPE,		"DIALOG" 		},
	{	FDI_WINDOW_STYLE_TYPE,		"WINDOW"		},
	{	FDI_PAGE_STYLE_TYPE,		"PAGE" 			},
	{	FDI_GROUP_STYLE_TYPE,		"GROUP"			},
	{	FDI_MENU_STYLE_TYPE,		"MENU" 			},
	{	FDI_TABLE_STYLE_TYPE,		"TABLE"			},
	{	FDI_NO_STYLE_TYPE,			"N/A"			},
};

CLASSSTRUCT LineType_list[] = 
{
	{	FDI_LOWLOW_LINETYPE,			"LOW LOW"		},
	{	FDI_LOW_LINETYPE,				"LOW" 			},
	{	FDI_HIGH_LINETYPE,				"HIGH"			},
	{	FDI_HIGHHIGH_LINETYPE,			"HIGH HIGH" 	},
	{	FDI_TRANSPARENT_LINETYPE,		"TRANSPARENT"	},
	{	FDI_DATA0_LINETYPE,				"DATA" 			},
	{	FDI_DATA1_LINETYPE,				"DATA1"			},
	{	FDI_DATA2_LINETYPE,				"DATA2"			},
	{	FDI_DATA3_LINETYPE,				"DATA3"			},
	{	FDI_DATA4_LINETYPE,				"DATA4"			},
	{	FDI_DATA5_LINETYPE,				"DATA5"			},
	{	FDI_DATA6_LINETYPE,				"DATA6"			},
	{	FDI_DATA7_LINETYPE,				"DATA7"			},
	{	FDI_DATA8_LINETYPE,				"DATA8"			},
	{	FDI_DATA9_LINETYPE,				"DATA9"			},
};

CLASSSTRUCT display_size_list[] = 
{
	{	DISPSIZE_XX_SMALL,		"XX SMALL" 			},
	{	DISPSIZE_X_SMALL,		"X SMALL"			},
	{	DISPSIZE_SMALL,			"SMALL" 			},
	{	DISPSIZE_MEDIUM,		"MEDIUM"			},
	{	DISPSIZE_LARGE,			"LARGE" 			},
	{	DISPSIZE_X_LARGE,		"X LARGE"			},
	{	DISPSIZE_XX_LARGE,		"XX LARGE" 			},
	{	DISPSIZE_XXX_SMALL,		"XXX SMALL"			},
};

CLASSSTRUCT Scaling_list[] = 
{
	{	FDI_LINEAR_SCALE,				"Linear"	},
	{	FDI_LOG_SCALE,					"Log"		},
};

CLASSSTRUCT WriteMode_list[] = 
{
	{	remote_output,				"ROUT"		},
	{	remote_cascade,				"RCAS"		},
	{	cascade,					"CAS"		},
	{	automatic,					"AUTO"		},
	{	manual,						"MAN"		},
	{	locked,						"LO"		},
	{	initialization,				"IMAN"		},
	{	out_of_service,				"OOS"		},
};

CLASSSTRUCT TimeScale_list[] = 
{
	{	FDI_TIME_SCALE_NONE,			"NONE"			},
	{	FDI_TIME_SCALE_SECONDS,			"SECONDS"		},
	{	FDI_TIME_SCALE_MINUTES,			"MINUTES"		},
	{	FDI_TIME_SCALE_HOURS,			"HOURS"			},
};

CLASSSTRUCT blockType_list[] = 
{
	{	TRANSDUCER_BLOCK_TYPE,		"TRANSDUCER BLOCK"	},
	{	PHYSICAL_BLOCK_TYPE,		"PHYSICAL BLOCK"	},
	{	FUNCTION_BLOCK_TYPE,		"FUNCTION BLOCK"	},
	{	UNKNOWN_BLOCK_TYPE,			"UNKNOWN BLOCK"	},
};


CLASSSTRUCT base_class_list[] = 
{
	{	FDI_UNDEFINED_STATUS,               "UNDEFINED STATUS"      },
	{	FDI_HARDWARE_STATUS,			    "HARDWARE STATUS"	    },
	{	FDI_SOFTWARE_STATUS,				"SOFTWARE STATUS"       },
	{	FDI_PROCESS_STATUS,				    "PROCESS STATUS"        },
	{	FDI_MODE_STATUS,					"MODE STATUS"           },
	{	FDI_DATA_STATUS,					"DATA STATUS"           },
	{	FDI_MISC_STATUS,					"MISC STATUS"           },
	{	FDI_EVENT_STATUS,				    "EVENT STATUS"          },
	{	FDI_STATE_STATUS,				    "STATE STATUS"          },
	{	FDI_SELF_CORRECTING_STATUS,		    "SELF CORRECTING STATUS"},
	{	FDI_CORRECTABLE_STATUS,			    "CORRECTABLE STATUS"    },
	{	FDI_UNCORRECTABLE_STATUS,		    "UNCORRECTABLE STATUS"  },
	{	FDI_SUMMARY_STATUS,				    "SUMMARY STATUS"        },
	{	FDI_DETAIL_STATUS,				    "DETAIL STATUS"         },
	{	FDI_MORE_STATUS,					"MORE STATUS"           },
	{	FDI_COMM_ERROR_STATUS,			    "COMM ERROR STATUS"     },
	{	FDI_IGNORE_IN_HANDHELD,	            "IGNORE IN HANDHELD"    },
	{	FDI_BAD_OUTPUT_STATUS,			    "BAD OUTPUT STATUS"     },
	{	FDI_IGNORE_IN_HOST,		            "IGNORE IN HOST"        },
	{	FDI_ERROR_STATUS,				    "ERROR STATUS"          },
	{	FDI_WARNING_STATUS,				    "WARNING STATUS"        },
	{	FDI_INFO_STATUS,					"INFO STATUS"           },
	{	FDI_DV_STATUS,					    "DV STATUS"             },
	{	FDI_TV_STATUS,					    "TV STATUS"             },
	{	FDI_AO_STATUS,					    "AO STATUS"             },
	{	FDI_ALL_STATUS,					    "ALL STATUS"            },
};

CLASSSTRUCT mode_and_reliability_list[] = 
{
	{	FDI_MR_UNUSED,			            "UNUSED" 		        },
	{	FDI_MR_AUTO_GOOD,			        "AUTO GOOD" 		    },
	{	FDI_MR_AUTO_BAD,	                "AUTO BAD"		        },
	{	FDI_MR_MANUAL_GOOD,                 "MANUAL GOOD"		    },
	{	FDI_MR_MANUAL_BAD,	                "MANUAL BAD"            },
};

CLASSSTRUCT edd_profile_list[] =
{
	{ PROFILE_NONE,			        "NONE" },
	{ PROFILE_HART,			        "HART" },
	{ PROFILE_FF,					  "FF" },
	{ PROFILE_PB,	            "PROFIBUS" },
	{ PROFILE_PN,	            "PROFINET" },
	{ PROFILE_ISA100,	          "ISA100" },
	{ PROFILE_GPE,	             "GENERIC" },
	{ PROFILE_COMMSERVER,	 "COMM SERVER" },
};

CLASSSTRUCT layout_type_list[] =
{
	{ COLUMNWIDTH_NONE,			            "COLUMNWIDTH NONE" },
	{ COLUMNWIDTH_EQUAL,			        "COLUMNWIDTH EQUAL" },
	{ COLUMNWIDTH_OPTIMIZED,	            "COLUMNWIDTH OPTIMIZED" },
};

CLASSSTRUCT classification_list[] =
{
	{ actuator,									"ACTUATOR" },
	{ actuator_electro_pneumatic,				"ACTUATOR ELECTRO PNEUMATIC" },
	{ actuator_electric,						"ACTUATOR ELECTRIC" },
	{ actuator_hydraulic,						"ACTUATOR HYDRAULIC" },
	{ converter,								"CONVERTER" },
	{ controller,								"CONTROLLER" },
	{ discrete_in,								"DISCRETE IN" },
	{ discrete_out,								"DISCRETE OUT" },
	{ electrical_distribution_power_monitoring,	"ELECTRICAL_DISTRIBUTION_POWER_MONITORING" },
	{ frequency_converter,						"FREQUENCY CONVERTER" },
	{ indicator,								"INDICATOR" },
	{ network,									"NETWORK" },
	{ network_communication_service_provider,	"NETWORK COMMUNICATION SERVICE PROVIDER" },
	{ network_component,						"NETWORK COMPONENT" },
	{ network_component_wirelessadapter,		"NETWORK COMPONENT WIRELESSADAPTER" },
	{ network_connection_point,					"NETWORK CONNECTION POINT" },
	{ remoteio,									"REMOTE IO" },
	{ recorder,									"RECORDER" },
	{ sensor,									"SENSOR" },
	{ sensor_acoustic,							"SENSOR ACOUSTIC" },
	{ sensor_analytic,							"SENSOR ANALYTIC" },
	{ sensor_analytic_gas,						"SENSOR ANALYTIC GAS" },
	{ sensor_analytic_liquid,					"SENSOR ANALYTIC LIQUID" },
	{ sensor_concentration,						"SENSOR CONCENTRATION" },
	{ sensor_density,							"SENSOR DESITY" },
	{ sensor_density_radiometric,				"SENSOR DENSITY RADIOMETRIC" },
	{ sensor_flow,								"SENSOR FLOW" },
	{ sensor_flow_coriolis,						"SENSOR FLOW CORIOLIS" },
	{ sensor_flow_electro_magnetic,				"SENSOR FLOW ELCTRO MAGNETIC" },
	{ sensor_flow_mechanical,					"SENSOR FLOW MECHANICAL" },
	{ sensor_flow_radiometric,					"SENSOR FLOW RADIOMETRIC" },
	{ sensor_flow_thermal,						"SENSOR FLOW THERMAL" },
	{ sensor_flow_ultrasonic,					"SENSOR FLOW ALTRSONIC" },
	{ sensor_flow_vortex_counter,				"SENSOR FLOW VORTEX COUNTER" },
	{ sensor_level,								"SENSOR LEVEL" },
	{ sensor_level_buoyancy,					"SENSOR LEVEL BUOYANCY" },
	{ sensor_level_capacitive,					"SENSOR LEVEL CAPACITIVE" },
	{ sensor_level_hydrostatic,					"SENSOR LEVEL HYDROSTATIC" },
	{ sensor_level_radiometric,					"SENSOR LEVEL RADIOMETRIC" },
	{ sensor_multivariable,						"SENSOR MULTIVARIABLE" },
	{ sensor_position,							"SENSOR POSITION" },
	{ sensor_pressure,							"SENSOR PRESSURE" },
	{ sensor_temperature,						"SENSOR TEMPRATURE" },
	{ semiconductor,							"SEMICONDUCTOR" },
	{ switchgear,								"SWITCHGEAR" },
	{ universal,								"UNIVERSAL" }
};


CLASSSTRUCT ByteOrder_list[] =
{
	{ No_Order,					  "NO ORDER" },
	{ Big_Endian,				"BIG ENDIAN" },
	{ Little_Endian,		 "LITTLE ENDIAN" }
};


CLASSSTRUCT Protocol_list[] =
{
	{ ff,				"FF" },
	{ hart,				"HART" },
	{ profibus_dp,		"PROFIBUS_DP" },
	{ profibus_pa,		"PROFIBUS_PA" },
	{ profinet,			"PROFINET" },
	{ isa100,			"ISA100" }
};

CLASSSTRUCT RelationType_list[] =
{
	{ CHILD_COMPONENT,		"CHILD COMPONENT" },
	{ PARENT_COMPONENT,		"PARENT_COMPONENT" },
	{ SIBLING_COMPONENT,	"SIBLING_COMPONENT" },
	{ NEXT_COMPONENT,		"NEXT_COMPONENT" },
	{ PREV_COMPONENT,		"PREV_COMPONENT" }
};
//
//Constructor
//
CDDSXMLBuilder::CDDSXMLBuilder()
	: m_bLoadXML(true)
{
	// m_szFilename - has own constructor
	// m_pDOMDoc - has own constructor
	// bstr_DataAsXml - has own constructor
	m_EDDEngine = nullptr;

	m_iBlockCount = 0;
	m_iBlockInstances = nullptr;
	m_pFieldbusBlockInfo = nullptr;
	m_iDeviceInstanceMemoryUsage = 0;

	m_Protocol = HART;
}

//
//Destructor
//
CDDSXMLBuilder::~CDDSXMLBuilder()
{
	m_pDOMDoc = NULL;
	// m_szFilename - has own destructor
	// bstr_DataAsXml - has own destructor
	m_EDDEngine = nullptr;

	m_iBlockCount = 0;
	delete [] m_iBlockInstances;
	delete [] m_pFieldbusBlockInfo;
}

//******************************************//
//Convert attribute name to string
//******************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_attribute_name(nsEDDEngine::AttributeName attribute_name, CString &attribute_name_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*attr_struct=NULL;
	unsigned long	id = attribute_name;

	for(int i = 0; i < NUMELEM(attribute_names_list); i++)
	{
		attr_struct = &attribute_names_list[i];
		if(attr_struct->num == id)
		{
			attribute_name_as_string = attr_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		attribute_name_as_string.Format(L"%lu - NOT FOUND", id);
	}
}
 
//***********************************//
//Convert class attribute to string
//***********************************//

void CDDSXMLBuilder::AmsHartDDS_convert_class_name(unsigned long long class_type, CString &class_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*class_struct=NULL;
	unsigned long long	id = class_type;

	class_type_as_string.Empty();		// Make sure we are starting with an empty string.

	for(int i = (NUMELEM(class_names_list) - 1); i >= 0; i--)
	{
		class_struct = &class_names_list[i];
		if( class_struct->num > id )
			continue;
		else if( class_struct->num < id )
		{
			class_type_as_string += class_struct->str;
			class_type_as_string += _T(" & ");
			id -= class_struct->num;
			bFound = true;
		}
		else if( class_struct->num == id)
		{
			class_type_as_string += class_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		class_type_as_string.Format(L"%llu - NOT FOUND", id);
	}
}

//***********************************//
//Convert handling attribute to string
//***********************************//

void CDDSXMLBuilder::AmsHartDDS_convert_handling(long handling, CString &handling_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*handling_struct = NULL;
	unsigned long long	id = handling;

	handling_as_string.Empty();		// Make sure we are starting with an empty string.

	for(int i = (NUMELEM(handling_list) - 1); i >= 0; i--)
	{
		handling_struct = &handling_list[i];
		if( handling_struct->num > id )
			continue;
		else if( handling_struct->num < id )
		{
			handling_as_string += handling_struct->str;
			handling_as_string += _T(" & ");
			id -= handling_struct->num;
			bFound = true;
		}
		else if( handling_struct->num == id)
		{
			handling_as_string += handling_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		handling_as_string.Format(L"%llu - NOT FOUND", id);
	}
}

//******************************************//
//Convert variable type attribute to string
//******************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_variable_type(VariableType variable_type, CString &var_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*var_struct=NULL;
	unsigned long	id = variable_type;

	for(int i = 0; i < NUMELEM(variable_list); i++)
	{
		var_struct = &variable_list[i];
		if(var_struct->num == id/*variable_type*/)
		{
			var_type_as_string = var_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		var_type_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert menu style attribute to string
//***************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_menu_style(unsigned long menu_style, CString &style_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*style_struct=NULL;

	for(int i = 0; i < NUMELEM(menu_style_list); i++)
	{
		style_struct = &menu_style_list[i];
		if(style_struct->num == menu_style)
		{
			style_as_string = style_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		style_as_string.Format(L"%lu - NOT FOUND", menu_style);
	}
}

//***************************************//
//Convert chart type attribute to string
//***************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_chart_type(unsigned long chart_type, CString &chart_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*chart_struct = NULL;

	for(int i = 0; i < NUMELEM(chart_type_list); i++)
	{
		chart_struct = &chart_type_list[i];
		if(chart_struct->num == chart_type)
		{
			chart_type_as_string = chart_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		chart_type_as_string.Format(L"%lu - NOT FOUND", chart_type);
	}
}

//*******************************************//
//Convert waveform type attribute to string
//*******************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_waveform_type(unsigned long waveform_type, CString &waveform_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*waveform_struct=NULL;

	for(int i = 0; i < NUMELEM(waveform_type_list); i++)
	{
		waveform_struct = &waveform_type_list[i];
		if(waveform_struct->num == waveform_type)
		{
			waveform_type_as_string = waveform_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		waveform_type_as_string.Format(L"%lu - NOT FOUND", waveform_type);
	}
}

//****************************************//
//Convert method type attribute to string
//****************************************//

void CDDSXMLBuilder::AmsHartDDS_convert_method_type(unsigned long method_type, CString &method_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*method_struct=NULL;

	for(int i = 0; i < NUMELEM(method_type_list); i++)
	{
		method_struct = &method_type_list[i];
		if(method_struct->num == method_type)
		{
			method_type_as_string = method_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		method_type_as_string.Format(L"%lu - NOT FOUND", method_type);
	}
}
//******************************//
//Convert item type to string
//******************************//

void CDDSXMLBuilder::AmsHartDDS_convert_item_type(ITEM_TYPE item_type, CString &item_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*item_struct=NULL;
	unsigned long	id = item_type;

	for(int i = 0; i < NUMELEM(item_type_list); i++)
	{
		item_struct = &item_type_list[i];
		if(item_struct->num == id/*item_type*/)
		{
			item_type_as_string = item_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		item_type_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//*******************************************//
//Convert live type type attribute to string
//*******************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_line_type(unsigned long line_type, CString &line_type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*lineType_struct=NULL;

	for(int i = 0; i < NUMELEM(LineType_list); i++)
	{
		lineType_struct = &LineType_list[i];
		if(lineType_struct->num == line_type)
		{
			line_type_as_string = lineType_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		line_type_as_string.Format(L"%lu - NOT FOUND", line_type);
	}
}

//******************************//
//Convert item size to string
//******************************//

void CDDSXMLBuilder::FDIHartDDS_convert_size_to_name(DisplaySize item_size, CString &item_size_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*item_struct=NULL;
	unsigned long	id = item_size;

	for(int i = 0; i < NUMELEM(display_size_list); i++)
	{
		item_struct = &display_size_list[i];
		if(item_struct->num == id/*item_size*/)
		{
			item_size_as_string = item_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		item_size_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//******************************//
//Convert scaling to string
//******************************//

void CDDSXMLBuilder::FDIHartDDS_convert_scaling_to_name(Scaling scaling_value, CString &scaling_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*scaling_struct=NULL;
	unsigned long	id = scaling_value;

	for(int i = 0; i < NUMELEM(Scaling_list); i++)
	{
		scaling_struct = &Scaling_list[i];
		if(scaling_struct->num == id/*scaling_value*/)
		{
			scaling_as_string = scaling_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		scaling_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//******************************//
//Convert data item type to string
//******************************//

void CDDSXMLBuilder::FDIHartDDS_convert_DataItemType_to_name(DataItemType dataType, CString &type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*dataItemType_struct=NULL;
	unsigned long	id = dataType;

	for(int i = 0; i < NUMELEM(command_type_list); i++)
	{
		dataItemType_struct = &command_type_list[i];
		if(dataItemType_struct->num == id)
		{
			type_as_string = dataItemType_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		type_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert write mode (bit enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_writeMode_to_name(WriteMode wMode, CString &wMode_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*writeMode_struct=NULL;
	unsigned long	id = wMode;

	for(int i = 0; i < NUMELEM(WriteMode_list); i++)
	{
		writeMode_struct = &WriteMode_list[i];
		if(writeMode_struct->num & id)
		{
			wMode_as_string = writeMode_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		wMode_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert TimeScale to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_TimeScale_to_name(TimeScale tScale, CString &tScale_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*timeScale_struct=NULL;
	unsigned long	id = tScale;

	for(int i = 0; i < NUMELEM(TimeScale_list); i++)
	{
		timeScale_struct = &TimeScale_list[i];
		if(timeScale_struct->num == id)
		{
			tScale_as_string = timeScale_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		tScale_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert block type (enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_BlockType_to_name(BlockBType type, CString &type_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*blockType_struct=NULL;
	unsigned long	id = type;

	for(int i = 0; i < NUMELEM(blockType_list); i++)
	{
		blockType_struct = &blockType_list[i];
		if(blockType_struct->num == id/*type*/)
		{
			type_as_string = blockType_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		type_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert classification (enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_classification_to_name(Classification classification, CString &classification_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*classification_struct = NULL;
	unsigned long	id = classification;

	for (int i = 0; i < NUMELEM(classification_list); i++)
	{
		classification_struct = &classification_list[i];
		if (classification_struct->num == id/*type*/)
		{
			classification_as_string = classification_struct->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		classification_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert byte order (enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_ByteOrder_to_name(ByteOrder byteorder, CString &byteorder_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*ByteOrder_struct = NULL;
	unsigned long	id = byteorder;

	for (int i = 0; i < NUMELEM(ByteOrder_list); i++)
	{
		ByteOrder_struct = &ByteOrder_list[i];
		if (ByteOrder_struct->num == id/*type*/)
		{
			byteorder_as_string = ByteOrder_struct->str;
			bFound = true;
			break;
		}
	}
	if (bFound == false)
	{
		byteorder_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert protocol list (enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_Protocol_to_name(Protocol protocol, CString &protocol_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*protocol_struct = NULL;
	unsigned long	id = protocol;

	for (int i = 0; i < NUMELEM(Protocol_list); i++)
	{
		protocol_struct = &Protocol_list[i];
		if (protocol_struct->num == id/*type*/)
		{
			protocol_as_string = protocol_struct->str;
			bFound = true;
			break;
		}
	}
	if (bFound == false)
	{
		protocol_as_string.Format(L"%lu - NOT FOUND", id);
	}
}

//***************************************//
//Convert relation type (enum) to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_RelationType_to_name(RelationType relation, CString &relation_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*relation_struct = NULL;
	unsigned long	id = relation;

	for (int i = 0; i < NUMELEM(RelationType_list); i++)
	{
		relation_struct = &RelationType_list[i];
		if (relation_struct->num == id/*type*/)
		{
			relation_as_string = relation_struct->str;
			bFound = true;
			break;
		}
	}
	if (bFound == false)
	{
		relation_as_string.Format(L"%lu - NOT FOUND", id);
	}
}
//*****************************************************************************************//
//
//	Name: ReturnEXPRAsString
//
//	Description: Take the expression and return a CString representation of the value.
//
//
//*****************************************************************************************//
CString ReturnEXPRAsString( EXPR *expr )
{
	CString sValue;
	switch( expr->eType )
	{
			case EXPR::EXPR_TYPE_INTEGER:
				sValue.Format(_T("%I64d"), expr->val.i );
				break;
			case EXPR::EXPR_TYPE_UNSIGNED:
				sValue.Format(_T("%I64u"), expr->val.u);
				break;
			case EXPR::EXPR_TYPE_FLOAT:
				sValue.Format(_T("%f"), expr->val.f);
				break;
			case EXPR::EXPR_TYPE_DOUBLE:
				sValue.Format(_T("%f"), expr->val.d);
				break;
			case EXPR::EXPR_TYPE_STRING:
				//Using the .Format function with "%s" does not work for 64-bit
				sValue = expr->val.s.c_str();
				break;
			case EXPR::EXPR_TYPE_BINARY:
			{
				if (expr->val.b.ptr())
				{
					//Need to convert BINARY buffer to hex string format.
					std::stringstream ss;

					unsigned char* szBuf = { 0 };

					szBuf = (unsigned char*)expr->val.b.ptr();

					for (unsigned int i = 0; i < expr->val.b.length(); i++)
					{
						//convert unsigned char to hexadecimal base format and fill '0' for the 0 value. 
						ss << std::setfill('0') << std::setw(2) << std::hex << unsigned(szBuf[i]);
					}

					std::string result(ss.str());
					sValue = result.c_str();
				}
			}
				break;
			default:
				break;
	}
	return sValue;
}

//***************************************//
//Convert OutputKind attribute to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_base_class(BaseClass base_class, CString &base_class_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*base_class_entry=NULL;

	for(int i = 0; i < NUMELEM(base_class_list); i++)
	{
		base_class_entry = &base_class_list[i];
		if(base_class_entry->num == (unsigned long)base_class)
		{
			base_class_as_string = base_class_entry->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		base_class_as_string.Format(L"%lu - NOT FOUND", (unsigned long)base_class);
	}
}

//***************************************//
//Convert ModeAndReliability attribute to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_mode_and_reliability(ModeAndReliability mode_and_reliability, CString &output_class_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*mode_and_reliability_entry=NULL;

	for(int i = 0; i < NUMELEM(mode_and_reliability_list); i++)
	{
		mode_and_reliability_entry = &mode_and_reliability_list[i];
		if(mode_and_reliability_entry->num == (unsigned long)mode_and_reliability)
		{
			output_class_as_string = mode_and_reliability_entry->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		output_class_as_string.Format(L"%lu - NOT FOUND", (unsigned long)mode_and_reliability);
	}
}

//***************************************//
//Convert EDD Profile attribute to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_Edd_Profile(EDD_Profile edd_profile, CString &output_edd_profile_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*class_edd_profile_entry = NULL;

	for (int i = 0; i < NUMELEM(edd_profile_list); i++)
	{
		class_edd_profile_entry = &edd_profile_list[i];
		if (class_edd_profile_entry->num == (unsigned long)edd_profile)
		{
			output_edd_profile_as_string = class_edd_profile_entry->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		output_edd_profile_as_string.Format(L"%lu - NOT FOUND", (unsigned long)edd_profile);
	}
}

//***************************************//
//Convert LayoutType attribute to string
//***************************************//

void CDDSXMLBuilder::FDIHartDDS_convert_layout_type(LayoutType layout_type, CString &output_layout_as_string)
{
	bool bFound = false;
	CLASSSTRUCT		*class_type_entry = NULL;

	for (int i = 0; i < NUMELEM(layout_type_list); i++)
	{
		class_type_entry = &layout_type_list[i];
		if (class_type_entry->num == (unsigned long)layout_type)
		{
			output_layout_as_string = class_type_entry->str;
			bFound = true;
			break;
		}
	}

	if (bFound == false)
	{
		output_layout_as_string.Format(L"%lu - NOT FOUND", (unsigned long)layout_type);
	}
}

//////////////////////////////////////////
// FormatItemIdAndName
//////////////////////////////////////////
void CDDSXMLBuilder::FormatItemIdAndName(ITEM_ID id, CString &strIdAndName)
{
	wchar_t szItemName[128];

	int rc = m_EDDEngine->GetSymbolNameFromItemId(id, szItemName, sizeof(szItemName)/sizeof(szItemName[0]) );
	if (rc == 0)	// Success
	{
		if (m_Protocol == HART || m_Protocol == FF || m_Protocol == ISA100)
		{
			strIdAndName.Format(L"%lu(%s)", id, szItemName);	// If name lookup succeeds, add it
		}
		else
		{
			strIdAndName.Format(L"%s", szItemName);	// If its .bin file don't display item id.
		}
	}
	else
	{
		strIdAndName.Format(L"%lu", id);	// If no name found, just return the id
	}
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_dd_list
//
//	Description: 
//			Goes through the the device's item list and calls the specific function for each  
//			of the following types:  VARIABLE_ITYPE
//					   			     ARRAY_ITYPE
//							         COMMAND_ITYPE
//									 EDIT_DISP_ITYPE
//									 MENU_ITYPE
//								     METHOD_ITYPE
//									 ITEM_ARRAY_ITYPE
//									 CHART_ITYPE	
//									 GRAPH_ITYPE
//									 AXIS_ITYPE				
//									 WAVEFORM_ITYPE
//									 GRID_ITYPE			
//									 IMAGE_ITYPE
//									 RECORD_ITYPE
//									 COLLECTION_ITYPE
//			Inside those specific functions the parameters are retrieved from the specific type 
//			flat.
//
//	Inputs:
//		flat_device_dir - pointer to the FLAT buffer that is filled by ddi_get_item()
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDODSODElmt - Element of the xml that combines all the item
//			types along with the parameters of each sorted by item id.
//
//	Returns:
//		SUCCESS when function completes
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_dd_list( FLAT_DEVICE_DIR *flat_device_dir,
													MSXML2::IXMLDOMElementPtr pDDODSODElmt )
{

	ITEM_TBL					*item_table;
	ITEM_TBL_ELEM				*item_element;
	ITEM_ID						item_id = 0;
	ITEM_TYPE					item_type = ITYPE_NO_VALUE;
	
	if(!flat_device_dir)
		return E_FAIL;

	item_table = &flat_device_dir->item_tbl;

	for( int nIndex = 0; nIndex < item_table->count ; nIndex++ )
	{
		item_element = &item_table->list[nIndex];
		item_id = item_element->item_id;
		item_type = item_element->item_type;

		
		MSXML2::IXMLDOMElementPtr pDDItem = AddItem( pDDODSODElmt, item_id , item_type ); // Adds Item to the list of DD Items in the XML

		switch( item_type )
		{
		case ITYPE_VARIABLE:
			{
				AmsHartDDS_get_variable_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_ARRAY:
			{
				AmsHartDDS_get_array_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_COMMAND:
			{
				AmsHartDDS_get_command_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_EDIT_DISP:
			{
				AmsHartDDS_get_editDisplay_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_MENU:
			{
				AmsHartDDS_get_menu_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_METHOD:
			{
				AmsHartDDS_get_method_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_ITEM_ARRAY:
			{
				AmsHartDDS_get_itemArray_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_CHART:	
			{
				AmsHartDDS_get_chart_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_GRAPH:
			{
				AmsHartDDS_get_graph_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_AXIS:
			{
				AmsHartDDS_get_axis_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_WAVEFORM:
			{
				AmsHartDDS_get_waveform_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_GRID:
			{
				AmsHartDDS_get_grid_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_IMAGE:
			{
				AmsHartDDS_get_image_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_RECORD:
			{
				AmsHartDDS_get_record_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_COLLECTION:
			{
				AmsHartDDS_get_collection_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_SOURCE:
			{
				AmsHartDDS_get_source_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_FILE:
			{
				AmsHartDDS_get_file_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_VAR_LIST:
			{
				AmsHartDDS_get_varList_item(0, item_id, pDDItem );
				break;
			}
		case ITYPE_WAO:
			{
				AmsHartDDS_get_wao_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_UNIT:
			{
				AmsHartDDS_get_unit_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_RESP_CODES:
			{
				AmsHartDDS_get_resp_codes_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_REFRESH:
			{
				AmsHartDDS_get_refresh_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_BLOCK:
			{
				AmsHartDDS_get_block_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_BLOCK_B:
			{
				AmsHartDDS_get_block_b_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_LIST:
			{
				AmsHartDDS_get_list_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_PLUGIN:
			{
				AmsHartDDS_get_plugin_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_TEMPLATE:
			{
				AmsHartDDS_get_template_item( 0, item_id, pDDItem );
				break;
			}
		case ITYPE_COMPONENT:
			{
				AmsHartDDS_get_component_item(0, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_FOLDER:
			{
				AmsHartDDS_get_component_folder_item(0, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_REFERENCE:
			{
				AmsHartDDS_get_component_reference_item(0, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_RELATION:
			{
				AmsHartDDS_get_component_relation_item(0, item_id, pDDItem);
				break;
			}
		case ITYPE_NO_VALUE:
			break;

		default:
			{
				AmsHartDDS_get_any_item( 0, item_id, pDDItem );
				break;
			}
		}
	}
	
	AmsHartDDS_displayDebugInfo();
	return 0;
}

int CDDSXMLBuilder::CreateBlockIndexes( nsEDDEngine::FLAT_DEVICE_DIR * flat_device_dir )
{
	const int s_num_of_instances = 2;	// This indicates how many instances of the same
										// type of block you want to simulate. This is important
										// to test since in real devices you may have
										// multiple instances of the same type of block, such 
										// as several AI or PID blocks.

	m_iBlockCount = flat_device_dir->blk_tbl.count * s_num_of_instances;

	m_pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo[m_iBlockCount];
	memset(m_pFieldbusBlockInfo, 0, sizeof(nsEDDEngine::CFieldbusBlockInfo) * m_iBlockCount);

	m_iBlockInstances = new nsEDDEngine::BLOCK_INSTANCE[m_iBlockCount];
	memset(m_iBlockInstances, 0, sizeof(nsEDDEngine::BLOCK_INSTANCE) * m_iBlockCount);

	unsigned short object_index = 100;
	
	for( int i = 0; i < m_iBlockCount; i++ )
	{
		CFieldbusBlockInfo* pCurBlock = &m_pFieldbusBlockInfo[i];

		pCurBlock->m_BlockType = UNKNOWN_BLOCK;
		pCurBlock->m_CharRecord.m_ulDDItemId = flat_device_dir->blk_tbl.list[i/s_num_of_instances].blk_id;
		pCurBlock->m_usObjectIndex = object_index;

		object_index += 1000;
	}

	int rc = m_EDDEngine->AddFFBlocks( m_pFieldbusBlockInfo, m_iBlockInstances, m_iBlockCount );
	return rc;
}


int CDDSXMLBuilder::Dump_ITEM_ID( ITEM_ID itemID, std::unordered_set<ITEM_ID> *pIdSet, int iBlockInstance, MSXML2::IXMLDOMElementPtr pDDODSODElmt )
{
	if (pIdSet != nullptr)
	{
		ITEM_ID key = itemID /*| (iBlockInstance << 24)*/;	// This itemID in this block

		if (pIdSet->find(key) != pIdSet->cend())		// Don't process if we already have
		{
			return 0;
		}
		pIdSet->insert(key);
	}

	// Create ItemSpec to get itemType
	nsEDDEngine::FDI_ITEM_SPECIFIER ItemSpec = {FDI_ITEM_ID, itemID, 0};

	// Get Item Type
	nsEDDEngine::ITEM_TYPE itemType = ITYPE_NO_VALUE;
	int rs = m_EDDEngine->GetItemType(iBlockInstance, &ItemSpec, &itemType);

	// Create the DDItem XML element
	MSXML2::IXMLDOMElementPtr pDDItem = AddItem( pDDODSODElmt, itemID, itemType ); // Adds Item to the list of DD Items in the XML

	// Get the rest of the info from the EDD Engine and generate the XML for this itemID
	GetAnyItem(iBlockInstance, itemType, itemID, pDDItem);

	return rs;
}


void CDDSXMLBuilder::Add_ITEM_ID_LIST(const ITEM_ID_LIST *ItemIdList)
{
	for (int i=0; i < ItemIdList->count; i++)
	{
		Add_ITEM_ID(ItemIdList->list[i]);
	}
}


void CDDSXMLBuilder::Add_MEMBER_LIST(const MEMBER_LIST *MemberList)
{
	for (int iCount=0; iCount < MemberList->count; iCount++)
	{
		Add_ITEM_ID(MemberList->list[iCount].ref.id);
	}
}

void CDDSXMLBuilder::Add_ITEM_ID( ITEM_ID itemID)
{
	if (itemID > 0)
	{
		item_id_work_list.AddTail(itemID);
	}
}

void CDDSXMLBuilder::Add_OP_REF(const OP_REF *opRef)
{
	if (opRef->op_ref_type == STANDARD_TYPE)
	{
		Add_ITEM_ID(opRef->op_info.id);
		//Attn- The .member value appears to be a subindex, rather than an ITEM_ID, so don't do ADD_ITEM_ID
		//Add_ITEM_ID(opRef->op_info.member);
	}
	else if (opRef->op_ref_type == COMPLEX_TYPE)
	{
		for (int iCount = 0; iCount < opRef->op_info_list.count; iCount++)
		{
			Add_ITEM_ID(opRef->op_info_list.list[iCount].id);
			//Attn- The .member value appears to be a subindex, rather than an ITEM_ID, so don't do ADD_ITEM_ID
			//Add_ITEM_ID(opRef->op_info_list.list[iCount].member);
		}
	}
}

void CDDSXMLBuilder::Add_OP_REF_TRAIL(const OP_REF_TRAIL *opRefTrail)
{
	if (opRefTrail->op_ref_type == STANDARD_TYPE)
	{
		Add_ITEM_ID(opRefTrail->op_info.id);
		//Attn- The .member value appears to be a subindex, rather than an ITEM_ID, so don't do ADD_ITEM_ID
		//Add_ITEM_ID(opRefTrail->op_info.member);
	}
	else if (opRefTrail->op_ref_type == COMPLEX_TYPE)
	{
		for (int iCount = 0; iCount < opRefTrail->op_info_list.count; iCount++)
		{
			Add_ITEM_ID(opRefTrail->op_info_list.list[iCount].id);
			//Attn- The .member value appears to be a subindex, rather than an ITEM_ID, so don't do ADD_ITEM_ID
			//Add_ITEM_ID(opRefTrail->op_info_list.list[iCount].member);
		}
		
	}

	Add_ITEM_ID(opRefTrail->desc_id);

	for (int iCount = 0; iCount < opRefTrail->trail_count; iCount++)
	{
		Add_ITEM_ID(opRefTrail->trail[iCount].id);
	}
}

void CDDSXMLBuilder::Add_OP_REF_TRAIL_LIST(const OP_REF_TRAIL_LIST *opRefTrailList)
{
	for (int iCount = 0; iCount < opRefTrailList->count; iCount++)
	{
		Add_OP_REF_TRAIL(&opRefTrailList->list[iCount]);
	}
}

void CDDSXMLBuilder::Add_ACTION(const ACTION *action)
{
	if (action->eType == ACTION::ACTION_TYPE_REFERENCE)
	{
		Add_ITEM_ID(action->action.meth_ref);
	}
	else if (action->eType == ACTION::ACTION_TYPE_REF_WITH_ARGS)
	{
		Add_OP_REF_TRAIL(&action->action.meth_ref_args);
	}
}

	
void CDDSXMLBuilder::Add_ACTION_LIST(const ACTION_LIST *actionList)
{
	for ( int childIndex = 0; childIndex < actionList->count; childIndex ++ )
	{
		Add_ACTION(&actionList->list[childIndex]);
	}
}

void CDDSXMLBuilder::Add_ITEM_ARRAY_ELEMENT_LIST(const ITEM_ARRAY_ELEMENT_LIST *elements)
{
	for (int iCount = 0; iCount < elements->count; iCount++)
	{
		Add_ITEM_ID(elements->list[iCount].ref.id);
	}
}

void CDDSXMLBuilder::Add_DATA_ITEM_LIST(const DATA_ITEM_LIST *dataItemList)
{
	for (int iCount = 0; iCount < dataItemList->count; iCount++)
	{	
		if ((dataItemList->list[iCount].type == DataItemType::FDI_DATA_REFERENCE) ||
			(dataItemList->list[iCount].type == DataItemType::FDI_DATA_REF_FLAGS) ||
			(dataItemList->list[iCount].type == DataItemType::FDI_DATA_REF_WIDTH) ||
			(dataItemList->list[iCount].type == DataItemType::FDI_DATA_REF_FLAGS_WIDTH))
		{
			Add_OP_REF_TRAIL(&dataItemList->list[iCount].data.ref);
		}
	}
}


void CDDSXMLBuilder::Add_VECTOR_LIST(const VECTOR_LIST *vectorList)
{
	for (int iCount = 0; iCount < vectorList->count; iCount++)
	{
		for (int iSubCount = 0; iSubCount < vectorList->vectors[iCount].count; iSubCount++)
		{	
			if ((vectorList->vectors[iCount].list[iSubCount].type == DataItemType::FDI_DATA_REFERENCE) ||
				(vectorList->vectors[iCount].list[iSubCount].type == DataItemType::FDI_DATA_REF_FLAGS) ||
				(vectorList->vectors[iCount].list[iSubCount].type == DataItemType::FDI_DATA_REF_WIDTH) ||
				(vectorList->vectors[iCount].list[iSubCount].type == DataItemType::FDI_DATA_REF_FLAGS_WIDTH))
			{
				Add_OP_REF_TRAIL(&vectorList->vectors[iCount].list[iSubCount].vector.ref);
			}
		}
	}
}

void CDDSXMLBuilder::Add_DESC_REF(const DESC_REF *descRef)
{
	Add_ITEM_ID(descRef->id);
}

void CDDSXMLBuilder::Add_OP_MEMBER_LIST(const OP_MEMBER_LIST *opMemberList)
{
	for (int iCount = 0; iCount < opMemberList->count; iCount++)
	{
		Add_OP_REF_TRAIL(&opMemberList->list[iCount].ref);
	}

}

void CDDSXMLBuilder::Add_DEFAULT_VALUES_LIST(const DEFAULT_VALUES_LIST * defaultValuesList)
{
	for (int iCount = 0; iCount < defaultValuesList->count; iCount++)
	{
		Add_OP_REF_TRAIL(&defaultValuesList->list[iCount].ref);
	}
}

void CDDSXMLBuilder::Add_RESPONSE_CODES(const RESPONSE_CODES *responseCodes)
{
	if (responseCodes->eType == RESPONSE_CODES::RESP_CODE_TYPE_REF)
	{
		Add_ITEM_ID(responseCodes->response_codes.resp_code_ref);
	}
}

void CDDSXMLBuilder::Add_TRANSACTION_LIST(const TRANSACTION_LIST * transactionList)
{
	for (int iCount = 0; iCount < transactionList->count; iCount++)
	{
		Add_ACTION_LIST(&transactionList->list[iCount].post_actions);
		Add_DATA_ITEM_LIST(&transactionList->list[iCount].request);
		Add_DATA_ITEM_LIST(&transactionList->list[iCount].reply);
		Add_RESPONSE_CODES(&transactionList->list[iCount].resp_codes);
	}
}



int CDDSXMLBuilder::GetDDListByBlocks( nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir, MSXML2::IXMLDOMElementPtr pDDODSODElmt )
{
	std::unordered_set<ITEM_ID> itemIDset;		// Store ItemIds that have been dumped already
	ITEM_ID itemID = 0;

	for(int i=0; i < m_iBlockCount; i++)
	{
		int iBlockInstance = m_iBlockInstances[i];

		Add_ITEM_ID( m_pFieldbusBlockInfo[i].m_CharRecord.m_ulDDItemId );

        while (item_id_work_list.GetHeadPosition() != NULL)
        {
                itemID = item_id_work_list.RemoveHead();     // And pop it of the list
                Dump_ITEM_ID(itemID, &itemIDset, iBlockInstance, pDDODSODElmt);
        }
	}

	// Rest of the items - same as we do for Hart xml
	OutputDebugString(L"----------------\nDumping Non-block info\n--------------\n");
	MSXML2::IXMLDOMCommentPtr pComment = m_pDOMDoc->createComment( L" **** Non-Block Items **** " );

	pDDODSODElmt->appendChild(pComment);

	::nsEDDEngine::ITEM_TBL *it = &flat_device_dir->item_tbl;

	for( int nIndex = 0; nIndex < it->count; nIndex++ )
	{
		itemID = it->list[nIndex].item_id;
		
		// dump items that have not been decoded as a part of blocks
		// Dump_ITEM_ID() will check for uniqueness before dumping.
		Dump_ITEM_ID( itemID, &itemIDset, nsEDDEngine::DeviceLevel_BI, pDDODSODElmt );
		
	} //end of for loop for item table
	
	AmsHartDDS_displayDebugInfo();

	return 0;
}


void CDDSXMLBuilder::GetAnyItem( int iBlockIndex, nsEDDEngine::ITEM_TYPE item_type, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{

	switch( item_type )
	{
		case ITYPE_VARIABLE:
			{
				AmsHartDDS_get_variable_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_ARRAY:
			{
				AmsHartDDS_get_array_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_COMMAND:
			{
				AmsHartDDS_get_command_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_EDIT_DISP:
			{
				AmsHartDDS_get_editDisplay_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_MENU:
			{
				AmsHartDDS_get_menu_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_METHOD:
			{
				AmsHartDDS_get_method_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_ITEM_ARRAY:
			{
				AmsHartDDS_get_itemArray_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_CHART:	
			{
				AmsHartDDS_get_chart_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_GRAPH:
			{
				AmsHartDDS_get_graph_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_AXIS:
			{
				AmsHartDDS_get_axis_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_WAVEFORM:
			{
				AmsHartDDS_get_waveform_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_GRID:
			{
				AmsHartDDS_get_grid_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_IMAGE:
			{
				AmsHartDDS_get_image_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_RECORD:
			{
				AmsHartDDS_get_record_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_COLLECTION:
			{
				AmsHartDDS_get_collection_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_SOURCE:
			{
				AmsHartDDS_get_source_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_FILE:
			{
				AmsHartDDS_get_file_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_VAR_LIST:
			{
				AmsHartDDS_get_varList_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_WAO:
			{
				AmsHartDDS_get_wao_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_UNIT:
			{
				AmsHartDDS_get_unit_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_RESP_CODES:
			{
				AmsHartDDS_get_resp_codes_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_REFRESH:
			{
				AmsHartDDS_get_refresh_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_BLOCK:
			{
				AmsHartDDS_get_block_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_BLOCK_B:
			{
				AmsHartDDS_get_block_b_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_LIST:
			{
				AmsHartDDS_get_list_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_PLUGIN:
			{
				AmsHartDDS_get_plugin_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_TEMPLATE:
			{
				AmsHartDDS_get_template_item( iBlockIndex, item_id, pDDItem );
				break;
			}
		case ITYPE_COMPONENT:
			{
				AmsHartDDS_get_component_item(iBlockIndex, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_FOLDER:
			{
				AmsHartDDS_get_component_folder_item(iBlockIndex, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_REFERENCE:
			{
				AmsHartDDS_get_component_reference_item(iBlockIndex, item_id, pDDItem);
				break;
			}
		case ITYPE_COMPONENT_RELATION:
			{
				AmsHartDDS_get_component_relation_item(iBlockIndex, item_id, pDDItem);
				break;
			}
		case ITYPE_NO_VALUE:
			break;
		default:
			{
				AmsHartDDS_get_any_item( iBlockIndex, item_id, pDDItem );
				break;
			}
	}
}


int CDDSXMLBuilder::AmsHartDDS_displayDebugInfo()
{
	int r_code = 0;
#ifdef DEBUG	
	r_code = m_EDDEngine->DisplayDebugInfo();
#endif
	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_any_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr /*pDDItem*/  )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_BLOCK::Every_Attrs;

	CString temp_class;
	CString temp_var;
	CString temp_handle;

	
	int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	if(!generic_item.item)
	{
		TRACE(L"*** error : no item returned by GetItem\n");
		return r_code;
	}

	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_variable_item
//
//	Description: This function calls ddi_get_item() for the type: VARIABLE. It retrieves the 
//				 id, label, kind, name and other parameters of that particular variable,   
//				 from the flat: FLAT_VAR. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done 
//				 with the flat information of that item we call ddi_clean_item() to free 
//				 the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the variable itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_variable_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem  )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_VAR::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.


#if 0	// Time the GetItem() call
	LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
	QueryPerformanceCounter(&StartingTime);		// Capture Start time
#endif

	int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

#if 0
	QueryPerformanceCounter(&EndingTime);		// Capture End time

	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);		// Get the timer frequency

	// Calculate elapsed time
	ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;

	ElapsedMicroseconds.QuadPart *= 1000000;
	ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;

	// If elapsed time is large enough to care about, add to the XML
	if (ElapsedMicroseconds.QuadPart > 10000)
	{
		pDDItem->setAttribute( L"QPCTicks", ElapsedMicroseconds.QuadPart );
	}
#endif

	ASSERT( (r_code == 0) || (r_code == -1517) );


//	if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
//	{
//		r_code = DDS_SUCCESS;

	FLAT_VAR *flat_var = dynamic_cast<FLAT_VAR*>(generic_item.item);

	//Add item_ids to the block list here.

	//POST_EDIT_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->post_edit_act);

	//POST_READ_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->post_read_act);

	//POST_WRITE_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->post_write_act);

	//PRE_EDIT_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->pre_edit_act);

	//PRE_READ_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->pre_read_act);

	//PRE_WRITE_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->pre_write_act);

	//REFRESH_ACTIONS attribute
	Add_ACTION_LIST(&flat_var->refresh_act);

	//RESPONSE_CODES attribute
	Add_RESPONSE_CODES(&flat_var->resp_codes);
	//KJ- ATTN- items below are in flats.h, but not referenced in other parts of the code
	//***************************************************************************************
	//INDEX
	Add_ITEM_ID(flat_var->indexed);

	//POST_USER_ACTION
	Add_ACTION_LIST(&flat_var->post_user_action);

	//POST_RQST_ACTION
	Add_ACTION_LIST(&flat_var->post_rqst_actions);
	//******************************************************************************************
	if (!m_bLoadXML)
	{
		return r_code;
	}

	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_VAR Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_VAR *flat_pointer1 = new FLAT_VAR(*flat_var);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_VAR flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_VAR *flat_pointer2 = new FLAT_VAR(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;

	}

	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}




	//The following is for adding the properties of the particular item.
	//Each Item Kind has different properties to display in the Xml
	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	// Dump Item_information if available 
	if (flat_var->isAvailable(item_information))
	{
		if (flat_var->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_var->item_info);
		}
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_var->symbol_name.c_str());

	if (flat_var->isAvailable(label))
	{
		if (flat_var->label.length())
		{
			CString temp_label = StripOffNPChars((TCHAR*)flat_var->label.c_str());
			pDDItemAttributes->setAttribute(ATTRNAME_LABEL, (LPCTSTR)temp_label);
		}
	}

	CString temp_class;
	if (flat_var->isAvailable(class_attr))
	{
		AmsHartDDS_convert_class_name(flat_var->class_attr, temp_class);
		pDDItemAttributes->setAttribute(ATTRNAME_CLASS_AS_STRING, (LPCTSTR)temp_class);
	}

	
	
	if (flat_var->isAvailable(variable_type))
	{
		CString temp_var;
		AmsHartDDS_convert_variable_type(flat_var->type_size.type, temp_var);
		pDDItemAttributes->setAttribute(ATTRNAME_TYPE, flat_var->type_size.type);

		pDDItemAttributes->setAttribute(ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_var);



		if (flat_var->type_size.type == VT_INDEX)
		{
			if (flat_var->isAvailable(indexed))
			{
				CString strIndexed;

				FormatItemIdAndName(flat_var->indexed, strIndexed);
				pDDItemAttributes->setAttribute("Indexed", (LPCTSTR)strIndexed);
			}
		}

		pDDItemAttributes->setAttribute(ATTRNAME_SIZE, flat_var->type_size.size);
	}

	if (flat_var->isAvailable(handling))
	{
		pDDItemAttributes->setAttribute(ATTRNAME_HANDLING, flat_var->handling);

		CString temp_handle;
		AmsHartDDS_convert_handling(flat_var->handling, temp_handle);
		pDDItemAttributes->setAttribute(ATTRNAME_HANDLING_AS_STR, (LPCTSTR)temp_handle);
	}

	if (flat_var->isAvailable(help))
	{

		if (flat_var->help.length())
		{
			CString tempHelp = StripOffNPChars((TCHAR*)flat_var->help.c_str());
			pDDItemAttributes->setAttribute(ATTRNAME_HELP, (LPCTSTR)tempHelp);
		}
	}

	if (flat_var->isAvailable(display_format))
	{
		if (flat_var->display.length())
		{
			pDDItemAttributes->setAttribute(ATTRNAME_DISPLAY_FORMAT, flat_var->display.c_str());
		}
	}
	if (flat_var->isAvailable(edit_format))
	{
		if (flat_var->edit.length())
		{
			pDDItemAttributes->setAttribute(ATTRNAME_EDIT_FORMAT, flat_var->edit.c_str());
		}
	}

	if (flat_var->isAvailable(scaling_factor))
	{
		if (flat_var->scaling_factor.eType != 0)
		{
			switch (flat_var->scaling_factor.eType)
			{
			case EXPR::EXPR_TYPE_UNSIGNED:
				pDDItemAttributes->setAttribute(ATTRNAME_SCALING_FACTOR, (long)flat_var->scaling_factor.val.u);
				break;
			case EXPR::EXPR_TYPE_INTEGER:
				pDDItemAttributes->setAttribute(ATTRNAME_SCALING_FACTOR, (long)flat_var->scaling_factor.val.i);
				break;
			case EXPR::EXPR_TYPE_DOUBLE:
				pDDItemAttributes->setAttribute(ATTRNAME_SCALING_FACTOR, flat_var->scaling_factor.val.d);
				break;
			case EXPR::EXPR_TYPE_FLOAT:
				pDDItemAttributes->setAttribute(ATTRNAME_SCALING_FACTOR, flat_var->scaling_factor.val.f);
				break;
			case EXPR::EXPR_TYPE_STRING:
				pDDItemAttributes->setAttribute(ATTRNAME_SCALING_FACTOR, flat_var->scaling_factor.val.s.c_str());
				break;
			default:
				break;
			}
		}
	}

	//CONSTANT_UNIT
	if (flat_var->isAvailable(constant_unit))
	{
		if (flat_var->constant_unit.length() > 0)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_CONST_UNIT, flat_var->constant_unit.c_str());
		}
	}

	//DEFAULT_VALUE
	if (flat_var->isAvailable(default_value))
	{
		if (flat_var->default_value.eType != EXPR::EXPR_TYPE_NONE)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_DEFAULT_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_var->default_value));
		}
	}

	//INITIAL_VALUE
	if (flat_var->isAvailable(initial_value))
	{
		if (flat_var->initial_value.eType != EXPR::EXPR_TYPE_NONE)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_INITIAL_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_var->initial_value));
		}
	}

	//TIME_SCALE
	if (flat_var->isAvailable(time_scale))
	{
		if (flat_var->time_scale != FDI_TIME_SCALE_NONE)	
		{
			CString temp_time_scale;
			FDIHartDDS_convert_TimeScale_to_name(flat_var->time_scale, temp_time_scale);
			pDDItemAttributes->setAttribute(ATTRNAME_TIME_SCALE, (LPCTSTR)temp_time_scale);
		}
	}
		
	CString size_name;
	//HEIGHT attribute
	if (flat_var->isAvailable(height))
	{
		FDIHartDDS_convert_size_to_name(flat_var->height, size_name);
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute(ATTRNAME_HEIGHT, (LPCTSTR)size_name);
		}
	}

	//LENGHT attribute
	if (flat_var->isAvailable(width))
	{
		FDIHartDDS_convert_size_to_name(flat_var->width, size_name);
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute(ATTRNAME_WIDTH, (LPCTSTR)size_name);
		}
	}

	//VALIDITY attribute
	if (flat_var->isAvailable(validity))
	{
		if (flat_var->valid)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_VALIDITY, (LPCTSTR)_T("True"));
		}
		else
		{
			pDDItemAttributes->setAttribute(ATTRNAME_VALIDITY, (LPCTSTR)_T("False"));
		}
	}

	//VISIBILITY attribute
	if (flat_var->isAvailable(visibility))
	{
		if (flat_var->visibility)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_VISIBILITY, (LPCTSTR)_T("True"));
		}
		else
		{
			pDDItemAttributes->setAttribute(ATTRNAME_VISIBILITY, (LPCTSTR)_T("False"));
		}
	}

	//PRIVATE attribute
	if (flat_var->isAvailable(private_attr))
	{
		if (flat_var->private_attr)
		{
			pDDItemAttributes->setAttribute(ATTRNAME_PRIVATE, (LPCTSTR)_T("True"));
		}
		else
		{
			pDDItemAttributes->setAttribute(ATTRNAME_PRIVATE, (LPCTSTR)_T("False"));
		}
	}

	//WRITE_MODE
	if (flat_var->isAvailable(write_mode))
	{
		if (flat_var->write_mode != 0)
		{
			CString temp_writemode;
			FDIHartDDS_convert_writeMode_to_name(flat_var->write_mode, temp_writemode);
			pDDItemAttributes->setAttribute(ATTRNAME_WRITE_MODE, (LPCTSTR)temp_writemode);
		}
	}
	pDDItem->appendChild( pDDItemAttributes );

	// Add Min-Max if it exists
	if (flat_var->isAvailable(max_value) || flat_var->isAvailable(min_value))
	{
		if (flat_var->max_val.count || flat_var->min_val.count)
		{
			for (int m = 0; ((m < flat_var->max_val.count) || (m < flat_var->min_val.count)); m++)
			{
				MSXML2::IXMLDOMElementPtr pMinMaxValues;
				pMinMaxValues = m_pDOMDoc->createElement(ELMTNAME_MIN_MAX_VALS);
				if (flat_var->min_val.count > m)
				{
					pMinMaxValues->setAttribute(ATTRNAME_MIN_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_var->min_val.list[m]));
				}
				if (flat_var->max_val.count > m)
				{
					pMinMaxValues->setAttribute(ATTRNAME_MAX_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_var->max_val.list[m]));
				}
				pDDItem->appendChild(pMinMaxValues);
			}

		}
	}
	// Add enumeration values if they exist
	if (flat_var->isAvailable(enumerations))
	{
		if (flat_var->enums.count != 0)
		{
			for (int i = 0; i < flat_var->enums.count; i++)
			{
				MSXML2::IXMLDOMElementPtr pEnumSelection;
				pEnumSelection = m_pDOMDoc->createElement(ELMTNAME_ENUM_SELECTION);
				CString sMessage;
				sMessage.Format(_T("%I64u"), flat_var->enums.list[i].val.val.u);
				pEnumSelection->setAttribute(ATTRNAME_NUMBER, (LPCTSTR)sMessage);
				if (flat_var->enums.list[i].desc.length())
				{
					CString temp_desc = StripOffNPChars((TCHAR*)flat_var->enums.list[i].desc.c_str());
					if (temp_desc.CompareNoCase(_T(" ")) == 0)
					{
						pEnumSelection->setAttribute(ATTRNAME_LABEL, (LPCTSTR)USEFORBLANKDESCRIPTION);
					}
					else
					{
						pEnumSelection->setAttribute(ATTRNAME_LABEL, (LPCTSTR)temp_desc);
					}
				}
				else
				{
					pEnumSelection->setAttribute(ATTRNAME_LABEL, (LPCTSTR)USEFORBLANKDESCRIPTION);
				}
				if (flat_var->enums.list[i].help.length())
				{
					CString tempHelp = StripOffNPChars((TCHAR*)flat_var->enums.list[i].help.c_str());
					pEnumSelection->setAttribute(ATTRNAME_HELP, (LPCTSTR)tempHelp);
				}
				if (flat_var->enums.list[i].evaled & ENUM_VALUE::EnumValueEvaled::FDI_ENUM_ACTIONS_EVALED)
				{
					CString strAction;
					FormatItemIdAndName(flat_var->enums.list[i].actions, strAction);
					pEnumSelection->setAttribute(ATTRNAME_ACTION, (LPCTSTR)strAction);
				}

				for (int j = 0; j < flat_var->enums.list[i].status.count; j++)
				{
					CString temp_output_base_class;

					MSXML2::IXMLDOMElementPtr pEnumClass;
					pEnumClass = m_pDOMDoc->createElement(ATTRNAME_OCLASS);

					CString sMessage2;

					STATUS_CLASS_ELEM temp_class1 = flat_var->enums.list[i].status.list[j];

					FDIHartDDS_convert_base_class(temp_class1.base_class, temp_output_base_class);
					pEnumClass->setAttribute(ATTRNAME_OCLASSES_BASE_CLASS, (LPCTSTR)temp_output_base_class);

					// which only applies when kind is DV, TV, or AO.
					// Basically everything but not specified.
					if (temp_class1.base_class == FDI_DV_STATUS || temp_class1.base_class == FDI_TV_STATUS ||
						temp_class1.base_class == FDI_AO_STATUS || temp_class1.base_class == FDI_ALL_STATUS)
					{
						CString temp_output_mode_and_reliability_class;

						FDIHartDDS_convert_mode_and_reliability(temp_class1.output_class.mode_and_reliability, temp_output_mode_and_reliability_class);
						pEnumClass->setAttribute(ATTRNAME_OCLASSES_MODE_AND_RELIABILITY, (LPCTSTR)temp_output_mode_and_reliability_class);

						sMessage2.Format(_T("%d"), temp_class1.output_class.which_output);
						pEnumClass->setAttribute(ATTRNAME_OCLASSES_WHICH, (LPCTSTR)sMessage2);
					}
					pEnumSelection->appendChild(pEnumClass);
				}

				if (flat_var->enums.list[i].evaled & ENUM_VALUE::EnumValueEvaled::FDI_ENUM_CLASS_EVALED)
				{
					AmsHartDDS_convert_class_name(flat_var->enums.list[i].func_class, temp_class);

					pEnumSelection->setAttribute(ATTRNAME_CLASS_AS_STRING, (LPCTSTR)temp_class);
				}

				pDDItem->appendChild(pEnumSelection);
			}
		}
	}

	//POST_EDIT_ACTIONS attribute
	if (flat_var->isAvailable(post_edit_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->post_edit_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTEDITACT_ITEM, &flat_var->post_edit_act.list[childIndex]);
		}
	}

	//POST_READ_ACTIONS attribute
	if (flat_var->isAvailable(post_read_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->post_read_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTREADACT_ITEM, &flat_var->post_read_act.list[childIndex]);
		}
	}

	//POST_WRITE_ACTIONS attribute
	if (flat_var->isAvailable(post_write_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->post_write_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTWRITEACT_ITEM, &flat_var->post_write_act.list[childIndex]);
		}
	}

	//PRE_EDIT_ACTIONS attribute
	if (flat_var->isAvailable(pre_edit_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->pre_edit_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREEDITACT_ITEM, &flat_var->pre_edit_act.list[childIndex]);
		}
	}

	//PRE_READ_ACTIONS attribute
	if (flat_var->isAvailable(pre_read_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->pre_read_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREREADACT_ITEM, &flat_var->pre_read_act.list[childIndex]);
		}
	}

	//PRE_WRITE_ACTIONS attribute
	if (flat_var->isAvailable(pre_write_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->pre_write_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREWRITEACT_ITEM, &flat_var->pre_write_act.list[childIndex]);
		}
	}

	//REFRESH_ACTIONS
	if (flat_var->isAvailable(refresh_actions))
	{
		for (int childIndex = 0; childIndex < flat_var->refresh_act.count; childIndex++)
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_REFRESHACT_ITEM, &flat_var->refresh_act.list[childIndex]);
		}
	}

	//CommandLists
	AddCommandListForItem(pDDItem, item_id, ITYPE_VARIABLE);

	//RESPONSE_CODES
	if (flat_var->isAvailable(response_codes))
	{
		FDIHartDDS_AddChildResponseCodes(pDDItem, &flat_var->resp_codes);
	}

	FDI_PARAM_SPECIFIER ParamSpec;

	ParamSpec.eType = FDI_PS_ITEM_ID;
	ParamSpec.id = item_id;
	ParamSpec.subindex = 0;

	AmsHartDDS_AddRelationsAndUpdateInfo( pDDItem, iBlockIndex, &ParamSpec);

	ITEM_ID myUnit = 0;
	r_code = m_EDDEngine->GetParamUnitRelItemId(iBlockIndex, nullptr, &ParamSpec, &myUnit);

	if (r_code != 0)
	{
		r_code = -5;
	}

	/*}
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Variable Flat information not complete...returned with error code %d", r_code );
	}*/

	return r_code;
}

CString CDDSXMLBuilder::StripOffNPChars(const TCHAR* EnumDescription )
{
	CString sRetVal;
	int nStringLength = _tcslen(EnumDescription);

	for(int i = 0; i < nStringLength; i++ )
	{
		if( ((EnumDescription[i] >= 0x00)&&(EnumDescription[i] <= 0x08)) || 
			(EnumDescription[i] == 0x0B) ||
			(EnumDescription[i] == 0x0C) ||
			((EnumDescription[i] >= 0x0e)&&(EnumDescription[i] <= 0x1f)) ) 
		{
			//thow this away
		}
		else
		{
			sRetVal += EnumDescription[i];
		}
	}
	
	return sRetVal;
}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_array_item
//
//	Description: This function calls ddi_get_item() for the type: ARRAY. It retrieves the 
//				 id, label, kind, type, and name of that particular array, from the   
//				 flat: FLAT_ARRAY. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the array itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_array_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);

	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_ARRAY::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//if(r_code == 0)
	{
		r_code = 0;

		FLAT_ARRAY *flat_array= dynamic_cast<FLAT_ARRAY*>(generic_item.item);	

		Add_ITEM_ID(flat_array->type);

		Add_RESPONSE_CODES(&flat_array->resp_codes);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_ARRAY Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_ARRAY *flat_pointer1 = new FLAT_ARRAY(*flat_array);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_ARRAY flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_ARRAY *flat_pointer2 = new FLAT_ARRAY(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_array->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_array->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_array->symbol_name.c_str() );

		if( flat_array->label.length() )
		{
			CString tempLabel =  StripOffNPChars(  (TCHAR*)flat_array->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}

		CString strTemp;
		FormatItemIdAndName(flat_array->type, strTemp);
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE, (LPCTSTR)strTemp );

		if( flat_array->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_array->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}
		pDDItemAttributes->setAttribute( ATTRNAME_NUMOFELEMENTS, (unsigned long)flat_array->num_of_elements );

		//VALIDITY attribute
		if (flat_array->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_array->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//PRIVATE attribute
		if (flat_array->private_attr)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
		}

		//WRITE_MODE
		if (flat_array->write_mode != 0)
		{
			CString temp_writemode;
			FDIHartDDS_convert_writeMode_to_name(flat_array->write_mode, temp_writemode);
			pDDItemAttributes->setAttribute( ATTRNAME_WRITE_MODE, (LPCTSTR)temp_writemode );
		}

		pDDItem->appendChild( pDDItemAttributes );

		//Command List
		AddCommandListForItem(pDDItem, item_id, ITYPE_ARRAY);

		//RESPONSE_CODES
		FDIHartDDS_AddChildResponseCodes (pDDItem, &flat_array->resp_codes);

		//Update Table
		FDI_PARAM_SPECIFIER ParamSpec;
		ParamSpec.eType = FDI_PS_ITEM_ID;	// First get RelationsAndUpdateInfo for the entire array
		ParamSpec.id = item_id;

		DESC_REF DescRef;
		DescRef.id = item_id;
		DescRef.type = ITYPE_ARRAY;

		GetRecursiveRelations( pDDItem, iBlockIndex, &ParamSpec, &DescRef);
	}
	/*else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Value Array Flat information not complete...returned with error code %d", r_code );
	}*/

	return r_code;
	
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_command_item
//
//	Description: This function calls ddi_get_item() for the type: COMMAND. It retrieves the 
//				 id, number, kind, and name of that particular command, from the   
//				 flat: FLAT_COMMAND. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected evry time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the command itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_command_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem  )
{
	
	FDI_GENERIC_ITEM generic_item(ITYPE_COMMAND);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};

	AttributeNameSet attribute_list = FLAT_COMMAND::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	
	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		//r_code = DDS_SUCCESS;
		r_code =0;

		FLAT_COMMAND *flat_command = dynamic_cast< FLAT_COMMAND *>(generic_item.item);
		
		Add_ITEM_ID(flat_command->block_b);
		Add_ITEM_ID(flat_command->connection);

		Add_TRANSACTION_LIST(&flat_command->trans);

		Add_RESPONSE_CODES(&flat_command->resp_codes);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_COMMAND Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_COMMAND *flat_pointer1 = new FLAT_COMMAND(*flat_command);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_COMMAND flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_COMMAND *flat_pointer2 = new FLAT_COMMAND(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_command->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_command->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_command->symbol_name.c_str() );

		pDDItemAttributes->setAttribute( ATTRNAME_COMMAND_NUM, flat_command->number );

		//OPERATION

		switch (flat_command->oper)
		{
		case FDI_COMMAND_OPERATION:
			pDDItemAttributes->setAttribute( ATTRNAME_OPERATION, (LPCTSTR)_T("Command") );
			break;
		case FDI_READ_OPERATION:
			pDDItemAttributes->setAttribute( ATTRNAME_OPERATION, (LPCTSTR)_T("Read") );
			break;
		case FDI_WRITE_OPERATION:
			pDDItemAttributes->setAttribute( ATTRNAME_OPERATION, (LPCTSTR)_T("Write") );
			break;
		case FDI_DATA_EX_OPERATION:
			pDDItemAttributes->setAttribute( ATTRNAME_OPERATION, (LPCTSTR)_T("Data Exchange") );
			break;
		}

		//INDEX
		pDDItemAttributes->setAttribute( ATTRNAME_INDEX, (unsigned long)flat_command->index );

		//BLOCK_B
		if (flat_command->isAvailable(block_b))
		{
			CString strTemp;
			FormatItemIdAndName(flat_command->block_b, strTemp);
			pDDItemAttributes->setAttribute( ATTRNAME_BLOCK_B, (LPCTSTR)strTemp );
		}

		//SLOT
		if (flat_command->isAvailable(slot))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_SLOT_NUM, (unsigned long)flat_command->slot );
		}

		//SUB_SLOT
		if (flat_command->isAvailable(sub_slot))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_SUB_SLOT, (unsigned long)flat_command->sub_slot );
		}

		//API
		if (flat_command->isAvailable(api))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_API, (unsigned long)flat_command->api );
		}

		//HEADER
		if( flat_command->header.length() )
		{
			CString tempHeader =  StripOffNPChars( (TCHAR*)flat_command->header.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HEADER, (LPCTSTR)tempHeader );
		}

		pDDItem->appendChild( pDDItemAttributes );

		//TRANSACTION
		if( flat_command->trans.count != 0 )
		{
			for(int i = 0; i < flat_command->trans.count; i++ )
			{
				r_code = FDIHartDDS_AddChildTransaction (pDDItem, &(flat_command->trans.list[i]));
			}
		}

		//RESPONSE_CODES
		r_code = FDIHartDDS_AddChildResponseCodes (pDDItem, &flat_command->resp_codes);
	}

	/*else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Command Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

CString CDDSXMLBuilder::GetCommandIdType( int iType )
{
	CString tempType;

//Check type to see if it is of a constant type, because if it is constant we are 
//wrongly putting in "0" for the item id and the alm file generator is skipping over
//these bytes of information. We still need to count the byte for the alm file.
//We now put in an identifier for the item id if the reply type is a constant.

	switch( iType )
	{
	case FDI_DATA_CONSTANT: 
		{
			//Constant integer
			tempType = COMMANDITEMIDCONSTANT;
			break;
		}
	case FDI_DATA_FLOATING:
		{
			//Constant Float
			tempType = COMMANDITEMIDFLOATING;
			break;
		}
	case FDI_DATA_STRING:
		{
			//Constant String
			tempType = COMMANDITEMIDSTRING;
			break;
		}
	default:
		{
			//Reference
			tempType = _T("Reference");
			break;
		}
	}
	return tempType;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_editDisplay_item
//
//	Description: This function calls ddi_get_item() for the type: EDIT DISPLAY. It retrieves
//				 the id, label, kind, and name of that particular array, from the   
//				 flat: FLAT_EDIT_DISPLAY. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are 
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the edit display itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_editDisplay_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem  )
{
	
	FDI_GENERIC_ITEM generic_item(ITYPE_EDIT_DISP);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_EDIT_DISPLAY::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


	/*if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{*/
		r_code = 0;

		FLAT_EDIT_DISPLAY *flat_edit_disp = dynamic_cast< FLAT_EDIT_DISPLAY *>(generic_item.item);
		
		Add_OP_REF_TRAIL_LIST(&flat_edit_disp->disp_items);
		Add_OP_REF_TRAIL_LIST(&flat_edit_disp->edit_items);
		
		Add_ACTION_LIST(&flat_edit_disp->pre_edit_act);
		Add_ACTION_LIST(&flat_edit_disp->post_edit_act);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_EDIT_DISPLAY Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_EDIT_DISPLAY *flat_pointer1 = new FLAT_EDIT_DISPLAY(*flat_edit_disp);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_EDIT_DISPLAY flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_EDIT_DISPLAY *flat_pointer2 = new FLAT_EDIT_DISPLAY(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_edit_disp->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_edit_disp->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_edit_disp->symbol_name.c_str() );

		if( flat_edit_disp->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_edit_disp->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}

		pDDItem->appendChild( pDDItemAttributes );

		//EDIT_ITEMS
		for ( int childIndex = 0; childIndex < flat_edit_disp->edit_items.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_EDIT_ITEM, &(flat_edit_disp->edit_items.list[childIndex]));
		}

		//DISPLAY_ITEMS
		for ( int childIndex = 0; childIndex < flat_edit_disp->disp_items.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_DISPLAY_ITEM, &(flat_edit_disp->disp_items.list[childIndex]));
		}

		//POST_EDIT_ACTIONS attribute
		for ( int childIndex = 0; childIndex < flat_edit_disp->post_edit_act.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTEDITACT_ITEM, &flat_edit_disp->post_edit_act.list[childIndex]);
		}

		//PRE_EDIT_ACTIONS attribute
		for ( int childIndex = 0; childIndex < flat_edit_disp->pre_edit_act.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREEDITACT_ITEM, &flat_edit_disp->pre_edit_act.list[childIndex]);
		}
	/*}
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Edit Display Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_menu_item
//
//	Description: This function calls ddi_get_item() for the type: MENU. It retrieves the 
//				 id, label, kind, and name of that particular menu, from the   
//				 flat: FLAT_MENU. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the menu itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_menu_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	int r_code = 0;

	FDI_GENERIC_ITEM generic_item(ITYPE_MENU);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_MENU::All_Attrs;
	
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	
	r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		//r_code = DDS_SUCCESS;

		FLAT_MENU *flat_menu = dynamic_cast< FLAT_MENU *>(generic_item.item);

		for ( int childIndex = 0; childIndex < flat_menu->items.count; childIndex ++ )
		{
			Add_OP_REF_TRAIL(&(flat_menu->items.list[childIndex].ref));
		}

		//EXIT_ACTIONS
		Add_ACTION_LIST(&flat_menu->exit_actions);

		//INIT_ACTIONS
		Add_ACTION_LIST(&flat_menu->init_actions);

		//POST_EDIT_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->post_edit_act);

		//POST_READ_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->post_read_act);

		//POST_WRITE_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->post_write_act);
	
		//PRE_EDIT_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->pre_edit_act);
	
		//PRE_READ_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->pre_read_act);

		//PRE_WRITE_ACTIONS attribute
		Add_ACTION_LIST(&flat_menu->pre_write_act);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_MENU Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_MENU *flat_pointer1 = new FLAT_MENU(*flat_menu);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_MENU flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_MENU *flat_pointer2 = new FLAT_MENU(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}


	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}


	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	// Dump Item_information if available 
	if (flat_menu->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_menu->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_menu->symbol_name.c_str());

	if( flat_menu->label.length())
	{
		CString tempLabel =  StripOffNPChars( (TCHAR*)flat_menu->label.c_str() );
		pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
	}

	CString temp_menu;
	AmsHartDDS_convert_menu_style( flat_menu->style, temp_menu );
	pDDItemAttributes->setAttribute( ATTRNAME_STYLE, flat_menu->style );
	pDDItemAttributes->setAttribute( ATTRNAME_STYLE_AS_STRING, (LPCTSTR)temp_menu );
#if 0
	if( temp_menu == "N/A" )	//// FIX THIS too!
	{
		FDI_GENERIC_ITEM generic_item2(ITYPE_MENU);
		// MHD Fix the code that requires this kludge
			attribute_list.clear();

		//This GetItem  might be an extra, but it does get executed.  
		// This will be blocked by the m_bLoadXML above for the memory test.
		r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item2);

	//	if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
		{
			FLAT_MENU *flat_menu2 = dynamic_cast<FLAT_MENU *>(generic_item2.item);
				
			pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_menu2->symbol_name.c_str());
		}

	}
#endif
	if( flat_menu->help.length() )
	{
		CString tempHelp =  StripOffNPChars( (TCHAR*)flat_menu->help.c_str() );
		pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
	}

	//ACCESS attribute
	if (flat_menu->access == ONLINE_ACCESS)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("OnLine") );
	}
	else if (flat_menu->access == OFFLINE_ACCESS)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("OffLine") );
	}
	else if (flat_menu->access == UNKNOWN_ACCESS)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("Unknown") );
	}

	//VALIDITY attribute
	if (flat_menu->valid)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
	}

	//VISIBILITY attribute
	if (flat_menu->visibility)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
	}

	pDDItem->appendChild ( pDDItemAttributes );

	//ITEMS attribute
	for ( int childIndex = 0; childIndex < flat_menu->items.count; childIndex ++ )
	{
		FDIHartDDS_AddChildMenuItem(pDDItem, &flat_menu->items.list[childIndex]);
	}

	//EXIT_ACTIONS
	for ( int childIndex = 0; childIndex < flat_menu->exit_actions.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_EXITACT_ITEM, &flat_menu->exit_actions.list[childIndex]);
	}

	//INIT_ACTIONS
	for ( int childIndex = 0; childIndex < flat_menu->init_actions.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_INITACT_ITEM, &flat_menu->init_actions.list[childIndex]);
	}

	//POST_EDIT_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->post_edit_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTEDITACT_ITEM, &flat_menu->post_edit_act.list[childIndex]);
	}

	//POST_READ_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->post_read_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTREADACT_ITEM, &flat_menu->post_read_act.list[childIndex]);
	}

	//POST_WRITE_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->post_write_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_POSTWRITEACT_ITEM, &flat_menu->post_write_act.list[childIndex]);
	}

	//PRE_EDIT_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->pre_edit_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREEDITACT_ITEM, &flat_menu->pre_edit_act.list[childIndex]);
	}

	//PRE_READ_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->pre_read_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREREADACT_ITEM, &flat_menu->pre_read_act.list[childIndex]);
	}

	//PRE_WRITE_ACTIONS attribute
	for ( int childIndex = 0; childIndex < flat_menu->pre_write_act.count; childIndex ++ )
	{
		FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_PREWRITEACT_ITEM, &flat_menu->pre_write_act.list[childIndex]);
	}

//}


	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Menu Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;

}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_string_table
//
//	Description: This function calls ddi_get_item() for the type: STRING_TABLE. It retrieves the 
//				 id, mfg, dev_type, rev, ddrev of a particular device. 
//               Once it retrieves the information, we create a new element for the xml output 
//               and assign those parameters to their respective  attributes of that item type. 
//               They then are attached to the xml output that is being collected every time it 
//               enters an item type. Once we are done with the deviceinformation of that item we call ddi_clean_item() to free the memory.
//
//				For reference only
//		
//				typedef struct {
//					unsigned long   id;
//					unsigned long	mfg;
//					unsigned short  dev_type;
//					unsigned char   rev;
//					unsigned char   ddrev;
//				}               DEV_STRING_INFO;
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains new attached information for
//											the string table under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Michael Francisco
//
//*****************************************************************************************//

void CDDSXMLBuilder::AmsHartDDS_get_string_table( MSXML2::IXMLDOMElementPtr pParentElement )
{
	FLAT_DEVICE_DIR		*flat_device_dir;

	// This is the main string table
	MSXML2::IXMLDOMElementPtr pDDStringTableList;
	pDDStringTableList = m_pDOMDoc->createElement( ELMTNAME_STRING_TABLE_LIST );

	int r_code = m_EDDEngine->GetDeviceDir(&flat_device_dir);
	
	if(r_code)
		return; //r_code;
	
	if(r_code == 0)
	{
		for (int nIndex = 0; nIndex < flat_device_dir->string_tbl.count;
			/* nIndex is incremented in ATTRNAME_INDEX, below */)
		{
			LPTSTR table_str = (TCHAR*)flat_device_dir->string_tbl.list[nIndex].c_str();

			int buf_len = _tcslen(table_str);
			LPTSTR buf = (TCHAR*)malloc((buf_len+1)*sizeof(TCHAR));

			r_code = m_EDDEngine->GetStringTranslation(table_str,
				TEST_LANGUAGE,
				buf,
				buf_len );

			if ((r_code == 0) && (m_bLoadXML))
			{
				// This is the String Element (we'll make one string element for each device attribute found in DEV_STRING_INFO)
				MSXML2::IXMLDOMElementPtr pDDStringItem;	

				pDDStringItem = m_pDOMDoc->createElement( ELMTNAME_STRING_ITEM );
				pDDStringItem->setAttribute( ATTRNAME_INDEX, ++nIndex );
				CString temp_string = StripOffNPChars( buf );
				pDDStringItem->setAttribute( _T("Value"), (LPCTSTR)temp_string );
				pDDStringTableList->appendChild ( pDDStringItem );
			}
			else
			{
				// Need to increment nIndex when XML is not loaded, otherwise this function will hang.
				++nIndex;
			}

			free( buf );
		}


		pParentElement->appendChild(pDDStringTableList);
	}
}


int CDDSXMLBuilder::FDIHartDDS_AddChildMenuItem(MSXML2::IXMLDOMElementPtr pDDItem, MENU_ITEM *pMenuItem )
{
	CString temp_childmenutype;

	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( ELMTNAME_CHILD_ITEM );

	//MenuItemQualifier
	temp_childmenutype = _T("");
	if (pMenuItem->qual & FDI_READ_ONLY_ITEM)
	{
		temp_childmenutype += _T("Read only, ");
	}
	if (pMenuItem->qual & FDI_DISPLAY_VALUE_ITEM)
	{
		temp_childmenutype += _T("Display value, ");
	}
	if (pMenuItem->qual & FDI_REVIEW_ITEM)
	{
		temp_childmenutype += _T("Review, ");
	}
	if (pMenuItem->qual & FDI_NO_LABEL_ITEM)
	{
		temp_childmenutype += _T("No label, ");
	}
	if (pMenuItem->qual & FDI_NO_UNIT_ITEM)
	{
		temp_childmenutype += _T("No unit, ");
	}
	if (pMenuItem->qual & FDI_INLINE_ITEM)
	{
		temp_childmenutype += _T("Inline, ");
	}
	if (pMenuItem->qual & FDI_HIDDEN_ITEM)
	{
		temp_childmenutype += _T("Hidden.");
	}

	if ( temp_childmenutype.Compare(_T("")) )
	{
		pChildItem->setAttribute( _T("MenuItemQualifier"), (LPCTSTR) temp_childmenutype);
	}
	pDDItem->appendChild(pChildItem);

	//OP_REF_TRAIL
	int rc = FDIHartDDS_AddChildOpRefTrail(pChildItem, ELMTNAME_REFERENCE_ITEM, &(pMenuItem->ref));

	return rc;
}

int CDDSXMLBuilder::FDIHartDDS_AddConstantChild (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, nsEDDEngine::VECTOR_ITEM* pVectorItem )
{
	CString temp_childtype;

	MSXML2::IXMLDOMElementPtr pChildItem;
	pChildItem = m_pDOMDoc->createElement( wName );

	pChildItem->setAttribute(ATTRNAME_TYPE, pVectorItem->type);
	switch(pVectorItem->type)
	{
		case FDI_DATA_CONSTANT :
			pChildItem->setAttribute(ATTRNAME_VALUE, pVectorItem->vector.iconst);
			break;
		case FDI_DATA_UNSIGNED :
			pChildItem->setAttribute(ATTRNAME_VALUE, pVectorItem->vector.uconst);
			break;
		case FDI_DATA_DOUBLE :
			pChildItem->setAttribute(ATTRNAME_VALUE, pVectorItem->vector.dconst);
			break;
		case FDI_DATA_FLOATING :
			pChildItem->setAttribute(ATTRNAME_VALUE, pVectorItem->vector.fconst);
			break;
		case FDI_DATA_STRING :
			pChildItem->setAttribute(ATTRNAME_VALUE, pVectorItem->vector.str.c_str());
			break;
	}
	pDDItem->appendChild(pChildItem);
	return 0;
}

//
// Add DESC_REF info to XML
//
int CDDSXMLBuilder::FDIHartDDS_AddChildDescRef (MSXML2::IXMLDOMElementPtr & pElement, DESC_REF *pRefItem )
{
	CString strTemp;

	//desc_id
	if (pRefItem->id != 0)
	{
		FormatItemIdAndName(pRefItem->id, strTemp);
		pElement->setAttribute( ATTRNAME_DD_DESC_ID, (LPCTSTR)strTemp);
	}

	//desc_type
	if (pRefItem->type != ITYPE_NO_VALUE)
	{
		AmsHartDDS_convert_item_type( pRefItem->type, strTemp); 
		pElement->setAttribute( ATTRNAME_DD_DESC_TYPE, (LPCTSTR) strTemp);
	}

	return 0;
}
	
//
// Add DESC_REF info to XML
//
int CDDSXMLBuilder::FDIHartDDS_AddChildDescRef(MSXML2::IXMLDOMElementPtr & pElement, wchar_t *wName, DESC_REF *pRefItem)
{
	CString strTemp;

	MSXML2::IXMLDOMElementPtr pChildItem;
	pChildItem = m_pDOMDoc->createElement(wName);

	//desc_id
	if (pRefItem->id != 0)
	{
		FormatItemIdAndName(pRefItem->id, strTemp);
		pChildItem->setAttribute(ATTRNAME_DD_DESC_ID, (LPCTSTR)strTemp);
	}

	//desc_type
	if (pRefItem->type != ITYPE_NO_VALUE)
	{
		AmsHartDDS_convert_item_type(pRefItem->type, strTemp);
		pChildItem->setAttribute(ATTRNAME_DD_DESC_TYPE, (LPCTSTR)strTemp);
	}
	pElement->appendChild(pChildItem);
	return 0;
}

//
// Add OP_REF_TRAIL info to XML
//
int CDDSXMLBuilder::FDIHartDDS_AddChildOpRefTrail (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, OP_REF_TRAIL *pRefItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem = m_pDOMDoc->createElement( wName );
	 
	CString strTemp;

	//desc_id
	if (pRefItem->desc_id != 0)
	{
		FormatItemIdAndName(pRefItem->desc_id, strTemp);
		pChildItem->setAttribute( ATTRNAME_DD_DESC_ID, (LPCTSTR)strTemp);
	}

	//desc_type
	if (pRefItem->desc_type != ITYPE_NO_VALUE)
	{
		AmsHartDDS_convert_item_type( pRefItem->desc_type, strTemp); 
		pChildItem->setAttribute( ATTRNAME_DD_DESC_TYPE, (LPCTSTR) strTemp);
	}
	
	//bit_mask
	if (pRefItem->desc_bit_mask != 0)
	{
		// This is a bit mask so logically it should be displayed in hexadecimal
		WCHAR hexValue[20];
		wsprintf (hexValue, L"0x%x",pRefItem->desc_bit_mask);
		pChildItem->setAttribute( ATTRNAME_BITMASK, hexValue);
	}

	// Process the OP_REF part, that is, the STANDARD or COMPLEX reference
	FDIHartDDS_AddOpRef( pChildItem, pRefItem->op_ref_type, &pRefItem->op_info, &pRefItem->op_info_list,
		pRefItem->block_instance, pRefItem->desc_id, pRefItem->desc_type); 


	// value
	if ( pRefItem->expr.eType != EXPR::EXPR_TYPE_NONE )
	{
		pChildItem->setAttribute( ATTRNAME_VALUE, (LPCTSTR)ReturnEXPRAsString(&pRefItem->expr) );
	}

	//trail
	if (pRefItem->trail_count > 0)
	{
		MSXML2::IXMLDOMElementPtr pTrail = m_pDOMDoc->createElement(L"Trail");

		for(int i = 0; i < pRefItem->trail_count; i++)
		{
			RESOLVE_INFO *pTr = &(pRefItem->trail[i]);
			MSXML2::IXMLDOMElementPtr pTrailItem = m_pDOMDoc->createElement(L"TrailItem");

			//id
			FormatItemIdAndName(pTr->id, strTemp);
			pTrailItem->setAttribute( L"TrailId", (LPCTSTR)strTemp );

			//type
			AmsHartDDS_convert_item_type( pTr->type, strTemp); 
			pTrailItem->setAttribute( L"TrailType", (LPCTSTR) strTemp);

			//element
			switch (pTr->type)		// Determine the format for the "TrailMember" attribute
			{
			case ITYPE_NO_VALUE:		// In a trail, a zero type is used for PARAM.xxxx notation
			case ITYPE_CHART:
			case ITYPE_COLLECTION:
			case ITYPE_FILE:
			case ITYPE_GRAPH:
			case ITYPE_RECORD:
			case ITYPE_SOURCE:
			case ITYPE_VAR_LIST:
				FormatItemIdAndName(pTr->element, strTemp);	// Try to format as a symbol name
				pTrailItem->setAttribute( L"TrailMember", (LPCTSTR)strTemp );
				break;
			default:
				pTrailItem->setAttribute( L"TrailMember", pTr->element );	// Just display the number
				break;
			}

			pTrail->appendChild(pTrailItem);
		}
		pChildItem->appendChild(pTrail);
	}

	pDDItem->appendChild(pChildItem);
	return 0;
}


//
// Add OP_REF info to XML
//
int CDDSXMLBuilder::FDIHartDDS_AddChildOpRef (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, OP_REF *pRefItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem = m_pDOMDoc->createElement( wName );

	FDIHartDDS_AddOpRef( pChildItem, pRefItem->op_ref_type, &pRefItem->op_info, &pRefItem->op_info_list, pRefItem->block_instance); 

	pDDItem->appendChild(pChildItem);

	return 0;
}


//
// Add the common OP_REF info to XML from either the OP_REF or OP_REF_TRAIL
//
int CDDSXMLBuilder::FDIHartDDS_AddOpRef (MSXML2::IXMLDOMElementPtr & pDDItem,
	OpRefType  OpType,
	const OP_REF_INFO *pOpRefInfo,
	const OP_REF_INFO_LIST *pOpRefInfoList,
	BLOCK_INSTANCE BlockInstance,
	ITEM_ID DescId /* =0 */,					// Pass DESC_REF part in for OP_REF_TRAIL only
	ITEM_TYPE DescType /* =ITYPE_NO_VALUE */)
{
	CString strTemp;

	if( OpType == STANDARD_TYPE )
	{
		if ( (pOpRefInfo->id == DescId) && (pOpRefInfo->type == DescType) )
		{
			pDDItem->setAttribute( ATTRNAME_DD_OP_REF, L"-same-");	// If desc and op_info are the same, just note it.
		}
		else
		{
			if (pOpRefInfo->id != 0)
			{
				//op_id
				FormatItemIdAndName(pOpRefInfo->id, strTemp);
				pDDItem->setAttribute( ATTRNAME_DD_OP_ID, (LPCTSTR)strTemp );
			}

			//op_type
			if ( pOpRefInfo->type != ITYPE_NO_VALUE )
			{
				AmsHartDDS_convert_item_type( pOpRefInfo->type, strTemp); 
				pDDItem->setAttribute( ATTRNAME_DD_OP_TYPE, (LPCTSTR) strTemp);
			}
		}

		bool bShowMember = false;	// Determine whether to show the op_info.member value

		if (pOpRefInfo->member != 0)		// If it is non-zero, show it.
		{
			bShowMember = true;
		}
		else
		{
			switch ( pOpRefInfo->type)	// If the op_info.type uses the op_info.member, show it.
			{
			case ITYPE_ARRAY:
			case ITYPE_RECORD:
			case ITYPE_COLLECTION:
			case ITYPE_ITEM_ARRAY:
			case ITYPE_VAR_LIST:
			case ITYPE_LIST:
				bShowMember = true;
				break;
			}
		}

		if (bShowMember)
		{
			// op_info.member
			pDDItem->setAttribute( ATTRNAME_DD_OP_MEMBER, pOpRefInfo->member );
		}
	}
	else		// COMPLEX_TYPE
	{
		MSXML2::IXMLDOMElementPtr pComplexList = m_pDOMDoc->createElement(ELMTNAME_COMPLEX_REFERENCE);

		for( int i = 0; i < pOpRefInfoList->count; i++ )
		{
			MSXML2::IXMLDOMElementPtr pComplexItem = m_pDOMDoc->createElement(ELMTNAME_COMPLEX_ITEM);
			OP_REF_INFO *pOpRefInfo1 = &(pOpRefInfoList->list[i]);

			// list[i].id
			FormatItemIdAndName(pOpRefInfo1->id, strTemp);
			pComplexItem->setAttribute( ATTRNAME_DD_OP_ID, (LPCTSTR)strTemp );

			// list[i].type
			AmsHartDDS_convert_item_type( pOpRefInfo1->type, strTemp); 
			pComplexItem->setAttribute( ATTRNAME_DD_OP_TYPE, (LPCTSTR) strTemp);

			// list[i].member
			pComplexItem->setAttribute( ATTRNAME_DD_OP_MEMBER, pOpRefInfo1->member );

			pComplexList->appendChild( pComplexItem );
		}

		pDDItem->appendChild( pComplexList );
	}

	// block_instance
	if (BlockInstance != 0)
	{
		pDDItem->setAttribute( ATTRNAME_DD_OP_BLK_INST, BlockInstance);
	}

	return 0;
}

int CDDSXMLBuilder::FDIHartDDS_AddChildCommandItems( MSXML2::IXMLDOMElementPtr & pCommandList, wchar_t *wName, COMMAND_ELEM* pCommandElem )
{
	MSXML2::IXMLDOMElementPtr pChildItem;
	pChildItem = m_pDOMDoc->createElement( wName );
	
	pChildItem->setAttribute(ATTRNAME_COMMAND_ID, pCommandElem->command_id);

	if( m_Protocol == HART )
	{
		pChildItem->setAttribute(ATTRNAME_COMMAND_NUM, pCommandElem->command_number);
	}

	pChildItem->setAttribute(L"Transaction", pCommandElem->transaction);

	pChildItem->setAttribute(L"Weight", pCommandElem->weight);

	for (int i = 0; i < pCommandElem->count; i++)
	{
		MSXML2::IXMLDOMElementPtr pCmdIdx = m_pDOMDoc->createElement( L"COMMAND_INDEX" );

		pCmdIdx->setAttribute(L"id", pCommandElem->index_list[i].id);
		pCmdIdx->setAttribute(L"value", pCommandElem->index_list[i].value);

		pChildItem->appendChild(pCmdIdx);
	}

	pCommandList->appendChild(pChildItem);
	return 0;
}

int CDDSXMLBuilder::FDIHartDDS_AddChildPostPreAction (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, ACTION *pActionItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( wName );

	switch (pActionItem->eType)
	{
	case ACTION::ACTION_TYPE_REFERENCE:
		if (pActionItem->action.meth_ref != 0)
		{
			CString strTemp;
			FormatItemIdAndName( pActionItem->action.meth_ref, strTemp);
			pChildItem->setAttribute( ATTRNAME_DD_ITEM_ID, (LPCTSTR)strTemp);
		}
		break;
	case ACTION::ACTION_TYPE_REF_WITH_ARGS:
		FDIHartDDS_AddChildOpRefTrail(pChildItem, wName, &(pActionItem->action.meth_ref_args));
		break;
	case ACTION::ACTION_TYPE_DEFINITION:
		pChildItem->setAttribute( ATTRNAME_DEFINITION, pActionItem->action.meth_definition.data );
		break;
	}

	pDDItem->appendChild(pChildItem);
	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildVectorAttr(MSXML2::IXMLDOMElementPtr pDDItem, VECTOR *pVector)
{
	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( ELMTNAME_VECTOR_ITEM );

	//decription attribute
	if (pVector->description.length() > 0)
	{
		pChildItem->setAttribute(ATTRNAME_DESCRIPTION, pVector->description.c_str());
	}

	//reference attribute
	for ( int childIndex = 0; (childIndex < pVector->count); childIndex ++ )
	{
		//vector item value
		switch (pVector->list[childIndex].type)
		{
			case FDI_DATA_CONSTANT :
			case FDI_DATA_UNSIGNED :
			case FDI_DATA_DOUBLE :
			case FDI_DATA_FLOATING :
				FDIHartDDS_AddConstantChild(pChildItem, ELMTNAME_VECTOR_CONST_ITEM, &pVector->list[childIndex]);
				break;

			case FDI_DATA_REFERENCE :
			case FDI_DATA_REF_FLAGS :
			case FDI_DATA_REF_WIDTH :
			case FDI_DATA_REF_FLAGS_WIDTH:
				FDIHartDDS_AddChildOpRefTrail(pChildItem, ELMTNAME_REFERENCE_ITEM, &(pVector->list[childIndex].vector.ref));
				break;

			case FDI_DATA_STRING :
				FDIHartDDS_AddConstantChild(pChildItem, ELMTNAME_STRING_ITEM, &pVector->list[childIndex]);
				break;
		}
	}

	pDDItem->appendChild(pChildItem);

	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildDataItem (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, DATA_ITEM *pPointItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( wName );

	//KEY_POINT type
	CString tempTypeString;
	FDIHartDDS_convert_DataItemType_to_name(pPointItem->type, tempTypeString);
	pChildItem->setAttribute( ATTRNAME_TYPE, pPointItem->type );
	pChildItem->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)tempTypeString );

	switch (pPointItem->type)
	{
	case FDI_DATA_CONSTANT:
		pChildItem->setAttribute( ATTRNAME_VALUE, pPointItem->data.iconst );
		break;
	case FDI_DATA_REFERENCE:
	case FDI_DATA_REF_FLAGS:
	case FDI_DATA_REF_WIDTH:
	case FDI_DATA_REF_FLAGS_WIDTH:
		if (pPointItem->data_item_mask)
		{
			CString strMask;
			strMask.Format(L"%#llx", pPointItem->data_item_mask);
			pChildItem->setAttribute( L"ItemMask", (LPCTSTR)strMask );
		}

		if (pPointItem->flags & INFO_DATA_ITEM)
		{
			pChildItem->setAttribute( ATTRNAME_FLAGS, pPointItem->flags );
			pChildItem->setAttribute( "INFO", L"T" );
		}
		if (pPointItem->flags & INDEX_DATA_ITEM)
		{
			pChildItem->setAttribute( ATTRNAME_FLAGS, pPointItem->flags );
			pChildItem->setAttribute( "INDEX", L"T" );
		}

		if (pPointItem->width != 0)
		{
			pChildItem->setAttribute( ATTRNAME_WIDTH, pPointItem->width );
		}

		FDIHartDDS_AddChildOpRefTrail(pChildItem, ELMTNAME_REFERENCE_ITEM, &(pPointItem->data.ref));
		break;
	case FDI_DATA_FLOATING:
		pChildItem->setAttribute( ATTRNAME_VALUE, pPointItem->data.fconst );
		break;
	case FDI_DATA_UNSIGNED:
		pChildItem->setAttribute( ATTRNAME_VALUE, pPointItem->data.uconst );
		break;
	case FDI_DATA_DOUBLE:
		pChildItem->setAttribute( ATTRNAME_VALUE, pPointItem->data.dconst );
		break;
	case FDI_DATA_STRING:
		break;
	}

	pDDItem->appendChild(pChildItem);
	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildResponseCodes (MSXML2::IXMLDOMElementPtr pDDItem, RESPONSE_CODES *pRespCodesItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( ELMTNAME_RESP_CODES_ITEM );

	switch (pRespCodesItem->eType)
	{
	case RESPONSE_CODES::RESP_CODE_TYPE_REF:
		{
			CString strTemp;
			FormatItemIdAndName( pRespCodesItem->response_codes.resp_code_ref, strTemp);
			pChildItem->setAttribute( ATTRNAME_DD_ITEM_ID, (LPCTSTR)strTemp);	

	 		pDDItem->appendChild(pChildItem);
		}
		break;
	case RESPONSE_CODES::RESP_CODE_TYPE_LIST:
		{
			for ( int childIndex = 0; (childIndex < pRespCodesItem->response_codes.resp_code_list.count); childIndex ++ )
			{
				FDIHartDDS_AddChildResponseCodeList(pChildItem, &pRespCodesItem->response_codes.resp_code_list.list[childIndex]);
			}
			if(pRespCodesItem->response_codes.resp_code_list.count > 0)
			{
				pDDItem->appendChild(pChildItem);
			}
		}
		break;
	case RESPONSE_CODES::RESP_CODE_TYPE_NONE:
		break;
	}

	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildResponseCodeList (MSXML2::IXMLDOMElementPtr pDDItem, RESPONSE_CODE *pRespCodeItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem;

	pChildItem = m_pDOMDoc->createElement( _T("ResponseCode") );

	if (pRespCodeItem->evaled & RESPONSE_CODE::FDI_RS_VAL_EVALED)
	{
		pChildItem->setAttribute(ATTRNAME_INTEGER, (unsigned short)pRespCodeItem->val);
	}
	if (pRespCodeItem->evaled & RESPONSE_CODE::FDI_RS_TYPE_EVALED)
	{
		switch (pRespCodeItem->type)
		{
		case FDI_SUCCESS_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Success"));
			break;
		case FDI_MISC_WARNING_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Misc. Warning"));
			break;
		case FDI_DATA_ENTRY_WARNING_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Data Entry Warning"));
			break;
		case FDI_DATA_ENTRY_ERROR_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Data Entry Error"));
			break;
		case FDI_MODE_ERROR_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Mode Error"));
			break;
		case FDI_PROCESS_ERROR_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Process Error"));
			break;
		case FDI_MISC_ERROR_RSPCODE:
			pChildItem->setAttribute(ATTRNAME_RESPCODETYPE, _T("Misc. Error"));
			break;
		}
	}
	if (pRespCodeItem->evaled & RESPONSE_CODE::FDI_RS_DESC_EVALED)
	{
		pChildItem->setAttribute(ATTRNAME_DESCRIPTION, pRespCodeItem->desc.c_str());
	}
	if (pRespCodeItem->evaled & RESPONSE_CODE::FDI_RS_HELP_EVALED)
	{
		pChildItem->setAttribute(ATTRNAME_HELP, pRespCodeItem->help.c_str());
	}
	
	pDDItem->appendChild(pChildItem);
	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildItemIdList (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, ITEM_ID_LIST *pItemIdList )
{
	int r_code = 0;

	if (pItemIdList->count > 0)
	{
		CString strTemp;

		MSXML2::IXMLDOMElementPtr pElementList = m_pDOMDoc->createElement( wName );

		for( int i = 0; i < pItemIdList->count; i++ )
		{
			MSXML2::IXMLDOMElementPtr pID = m_pDOMDoc->createElement( L"ItemId" );

			FormatItemIdAndName(pItemIdList->list[i], strTemp);
			pID->setAttribute(L"Id", (LPCTSTR) strTemp );

			pElementList->appendChild(pID);
		}

		pDDItem->appendChild( pElementList );
	}

	return r_code;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildMemberList (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, MEMBER_LIST *pMemberList )
{
	int r_code = 0;

	if (pMemberList->count > 0)
	{
		MSXML2::IXMLDOMElementPtr pElementList = m_pDOMDoc->createElement( wName );

		for( int i = 0; i < pMemberList->count; i++ )
		{
			r_code = FDIHartDDS_AddChildMember(pElementList, &pMemberList->list[i]);
		}

		pDDItem->appendChild( pElementList );
	}

	return r_code;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildMember (MSXML2::IXMLDOMElementPtr pDDItem, MEMBER *pMemberItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem = m_pDOMDoc->createElement( ELMTNAME_MEMBERS );

	if (pMemberItem->evaled & MEMBER::FDI_MEM_NAME_EVALED)
	{
		// ITEM_ID name
		CString strMemberName;
		FormatItemIdAndName( pMemberItem->name, strMemberName);
		pChildItem->setAttribute( ATTRNAME_DD_MEMBER_ID, (LPCTSTR)strMemberName );
	}

	if (pMemberItem->evaled & MEMBER::FDI_MEM_REF_EVALED)
	{
		// DESC_REF ref
		FDIHartDDS_AddChildDescRef (pChildItem, &pMemberItem->ref );
	}

	if( pMemberItem->desc.length() )
	{
		CString tempDesc =  StripOffNPChars( (TCHAR*)pMemberItem->desc.c_str() );
		pChildItem->setAttribute( ATTRNAME_DESCRIPTION, (LPCTSTR)tempDesc );
	}

	if( pMemberItem->help.length() )
	{
		CString tempHelp =  StripOffNPChars( (TCHAR*)pMemberItem->help.c_str() );
		pChildItem->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
	}

	pDDItem->appendChild( pChildItem );

	return 0;
}

int CDDSXMLBuilder::FDIHartDDS_AddChildOPMembers (MSXML2::IXMLDOMElementPtr pDDItem, OP_MEMBER *pOpMemberItem )
{
	MSXML2::IXMLDOMElementPtr pChildItem = m_pDOMDoc->createElement( ELMTNAME_MEMBERS );

	if (pOpMemberItem->evaled & OP_MEMBER::FDI_OP_MEM_NAME_EVALED)
	{
		// ITEM_ID name
		CString strMemberName;
		FormatItemIdAndName( pOpMemberItem->name, strMemberName);
		pChildItem->setAttribute( ATTRNAME_DD_MEMBER_ID, (LPCTSTR)strMemberName );

	}

	if (pOpMemberItem->evaled & OP_MEMBER::FDI_OP_MEM_REF_EVALED)
	{
		FDIHartDDS_AddChildOpRefTrail(pChildItem, ELMTNAME_REFERENCE_ITEM, &(pOpMemberItem->ref));
	}

	if( pOpMemberItem->desc.length() )
	{
		CString tempDesc =  StripOffNPChars( pOpMemberItem->desc.c_str() );
		pChildItem->setAttribute( ATTRNAME_DESCRIPTION, (LPCTSTR)tempDesc );
	}
	if( pOpMemberItem->help.length() )
	{
		CString tempHelp =  StripOffNPChars( pOpMemberItem->help.c_str() );
		pChildItem->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
	}

	pDDItem->appendChild( pChildItem );

	return 0;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildTransaction (MSXML2::IXMLDOMElementPtr pDDItem, TRANSACTION *pTransItem )
{
	MSXML2::IXMLDOMElementPtr pDDItemTransactions;
	pDDItemTransactions = m_pDOMDoc->createElement( ELMTNAME_COMMAND_TRANS );
	int rc = 0;

	//NUMBER
	pDDItemTransactions->setAttribute( ATTRNAME_TRANS_NUM, pTransItem->number );
	//DATA_ITEM_LIST
	if( pTransItem->request.count > 0)
	{
		for( int j = 0; j < pTransItem->request.count; j++)
		{
			rc = FDIHartDDS_AddChildDataItem(pDDItemTransactions, ELMTNAME_COMMAND_REQUEST, &pTransItem->request.list[j]);
		}
	}
	if( pTransItem->reply.count > 0 )
	{
		for( int k = 0; k < pTransItem->reply.count; k++ )
		{
			rc = FDIHartDDS_AddChildDataItem(pDDItemTransactions, ELMTNAME_COMMAND_REPLY, &pTransItem->reply.list[k]);
		}
	}
	//RESPONSE_CODES
	rc = FDIHartDDS_AddChildResponseCodes (pDDItemTransactions, &pTransItem->resp_codes);

	pDDItem->appendChild( pDDItemTransactions );
	return rc;
}


int CDDSXMLBuilder::FDIHartDDS_AddChildDefaultValues (MSXML2::IXMLDOMElementPtr pDDItem, DEFAULT_VALUE_ITEM *pDefaultVals )
{
	MSXML2::IXMLDOMElementPtr pDDItemDefaultValues;
	pDDItemDefaultValues = m_pDOMDoc->createElement( ELMTNAME_DEFAULT_VALUES_ITEM );

	//OP_REF_TRAIL ref
	int rc = FDIHartDDS_AddChildOpRefTrail(pDDItemDefaultValues, ELMTNAME_REFERENCE_ITEM, &(pDefaultVals->ref));

	//EXPR value
	if ( pDefaultVals->value.eType != EXPR::EXPR_TYPE_NONE )
	{
		pDDItemDefaultValues->setAttribute( ATTRNAME_VALUE, (LPCTSTR)ReturnEXPRAsString(&pDefaultVals->value) );
	}

	pDDItem->appendChild( pDDItemDefaultValues );
	return rc;
}

int CDDSXMLBuilder::FDIHartDDS_AddChildComponentInitialValues(MSXML2::IXMLDOMElementPtr pDDItem, DEFAULT_VALUE_ITEM *pInitialVals)
{
	MSXML2::IXMLDOMElementPtr pDDItemDefaultValues;
	pDDItemDefaultValues = m_pDOMDoc->createElement(ATTRNAME_INITIAL_VALUE);

	//OP_REF_TRAIL ref
	int rc = FDIHartDDS_AddChildOpRefTrail(pDDItemDefaultValues, ELMTNAME_REFERENCE_ITEM, &(pInitialVals->ref));

	//EXPR value
	if (pInitialVals->value.eType != EXPR::EXPR_TYPE_NONE)
	{
		pDDItemDefaultValues->setAttribute(ATTRNAME_VALUE, (LPCTSTR)ReturnEXPRAsString(&pInitialVals->value));
	}

	pDDItem->appendChild(pDDItemDefaultValues);
	return rc;
}

void CDDSXMLBuilder::FDIHartDDS_AddRangeList(MSXML2::IXMLDOMElementPtr pDDItem, RANGE_SET *pRangeSet)
{
	MSXML2::IXMLDOMElementPtr pDDItemRangeSet;
	pDDItemRangeSet = m_pDOMDoc->createElement(ATTRNAME_RANGE_SET);
	
	//RANGE_SET MAX_VALUE
	pDDItemRangeSet->setAttribute(ATTRNAME_MAX_WHICH, pRangeSet->max_num.which);
	
	//EXPR value
	if (pRangeSet->max_num.value.eType != EXPR::EXPR_TYPE_NONE)
	{
		pDDItemRangeSet->setAttribute(ATTRNAME_MAXIMUM_NUMBER, (LPCTSTR)ReturnEXPRAsString(&pRangeSet->max_num.value));
	}

	//RANGE_SET MIN_VALUE
	pDDItemRangeSet->setAttribute(ATTRNAME_MIN_WHICH, pRangeSet->min_num.which);

	//EXPR value
	if (pRangeSet->min_num.value.eType != EXPR::EXPR_TYPE_NONE)
	{
		pDDItemRangeSet->setAttribute(ATTRNAME_MINIMUM_NUMBER, (LPCTSTR)ReturnEXPRAsString(&pRangeSet->min_num.value));
	}

	pDDItem->appendChild(pDDItemRangeSet);
	
}

int CDDSXMLBuilder::FDIHartDDS_AddChildComponents(MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, COMPONENT_SPECIFIER *pComponentSpecifierItem)
{
	(void)wName;
	MSXML2::IXMLDOMElementPtr pChildItem = m_pDOMDoc->createElement(ELMTNAME_COMPONENTS);

	//OP_REF_TRAIL ref
	int rc = FDIHartDDS_AddChildOpRefTrail(pChildItem, ELMTNAME_REFERENCE_ITEM, &(pComponentSpecifierItem->cr_ref));

	// maximum_number
	if (pComponentSpecifierItem->maximum_number.eType != EXPR::EXPR_TYPE_NONE)
	{
		pChildItem->setAttribute(ATTRNAME_MAXIMUM_NUMBER, (LPCTSTR)ReturnEXPRAsString(&pComponentSpecifierItem->maximum_number));
	}

	// minimum_number
	if (pComponentSpecifierItem->minimum_number.eType != EXPR::EXPR_TYPE_NONE)
	{
		pChildItem->setAttribute(ATTRNAME_MINIMUM_NUMBER, (LPCTSTR)ReturnEXPRAsString(&pComponentSpecifierItem->minimum_number));
	}

	// auto_create
	if (pComponentSpecifierItem->auto_create.eType != EXPR::EXPR_TYPE_NONE)
	{
		pChildItem->setAttribute(ATTRNAME_AUTO_CREATE, (LPCTSTR)ReturnEXPRAsString(&pComponentSpecifierItem->auto_create));
	}

	//filter 
	if (pComponentSpecifierItem->filter)
	{
		pChildItem->setAttribute(ATTRNAME_FILTER, (LPCTSTR)_T("True"));
	}
	else
	{
		pChildItem->setAttribute(ATTRNAME_FILTER, (LPCTSTR)_T("False"));
	}

	//pDDItem->appendChild(pChildItem);

	if (pComponentSpecifierItem->range_list.count != 0)
	{
		MSXML2::IXMLDOMElementPtr pDDItemRangeList;
		pDDItemRangeList = m_pDOMDoc->createElement(ATTRNAME_RANGE_LIST);

		FDIHartDDS_AddChildDescRef(pDDItemRangeList, &pComponentSpecifierItem->range_list.var_refrences);

		for (int i = 0; i < pComponentSpecifierItem->range_list.count; i++)
		{
			FDIHartDDS_AddRangeList(pDDItemRangeList, &pComponentSpecifierItem->range_list.list[i]);
		}

		pChildItem->appendChild(pDDItemRangeList);
	}
	pDDItem->appendChild(pChildItem);

	return rc;
}

//*****************************************************************************************//
//	Name: AmsHartDDS_get_method_item
//
//	Description: This function calls ddi_get_item() for the type: METHOD. It retrieves the 
//				 id, label, kind, type, and name of that particular method, from the   
//				 flat: FLAT_METHOD. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected evry time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the method itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_method_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	
	FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_METHOD::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	CString					temp_method;

	
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


	if (!m_bLoadXML)
	{
		return r_code;
	}

	/*if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )*/

	{
		r_code = 0;

		FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_METHOD Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_METHOD *flat_pointer1 = new FLAT_METHOD(*flat_method);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_METHOD flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_METHOD *flat_pointer2 = new FLAT_METHOD(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}
		
		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		AmsHartDDS_convert_method_type( flat_method->type, temp_method );
		
		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_method->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_method->item_info);
		}
	
		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_method->symbol_name.c_str() );

		if( flat_method->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_method->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE, (unsigned long)flat_method->type );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_method );

		if( flat_method->params.length() )
		{			
			CString tempParams =  StripOffNPChars( (TCHAR*)flat_method->params.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_PARAMS, (LPCTSTR)tempParams );
		}
		if( flat_method->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_method->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		//CLASS
		AmsHartDDS_convert_class_name( flat_method->class_attr, temp_method );
		pDDItemAttributes->setAttribute( ATTRNAME_CLASS_AS_STRING, (LPCTSTR)temp_method );

		//ACCESS attribute
		if (flat_method->access == ONLINE_ACCESS)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("OnLine") );
		}
		else if (flat_method->access == OFFLINE_ACCESS)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("OffLine") );
		}
		else if (flat_method->access == UNKNOWN_ACCESS)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_ACCESS, (LPCTSTR)_T("Unknown") );
		}

		//VALIDITY attribute
		if (flat_method->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_method->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//PRIVATE attribute
		if (flat_method->private_attr)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
		}

		// Append DDItemProperties to the DDItem element
		pDDItem->appendChild( pDDItemAttributes );

		// DEFINITION attribute as its own element
		MSXML2::IXMLDOMElementPtr pDefinitionAttr = m_pDOMDoc->createElement( ATTRNAME_DEFINITION );
		MSXML2::IXMLDOMTextPtr pText = m_pDOMDoc->createTextNode( flat_method->def.data );
		pDefinitionAttr->appendChild( pText );

		// Append Definition to the DDItem element
		pDDItem->appendChild( pDefinitionAttr );

	}
	/*}
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Method Flat information not complete...returned with error code %d", r_code );
	}
	*/



	return r_code;
}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_itemArray_item
//
//	Description: This function calls ddi_get_item() for the type: ITEM ARRAY. It retrieves 
//				 the id, label, kind, and name of that particular item array, from the   
//				 flat: FLAT_ITEM_ARRAY. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done
//				 with the flat information of that item we call ddi_clean_item() to free
//				 the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the item array itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_itemArray_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{

	FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_ITEM_ARRAY::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.


	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );


	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{

		r_code = 0;

		FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);

		Add_ITEM_ARRAY_ELEMENT_LIST(&flat_item_arry->elements);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_ITEM_ARRAY Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_ITEM_ARRAY *flat_pointer1 = new FLAT_ITEM_ARRAY(*flat_item_arry);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_ITEM_ARRAY flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_ITEM_ARRAY *flat_pointer2 = new FLAT_ITEM_ARRAY(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}
		

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		AmsHartDDS_convert_item_type( flat_item_arry->subtype , temp_item );
		
		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_item_arry->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_item_arry->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_item_arry->symbol_name.c_str() );

		if( flat_item_arry->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*) flat_item_arry->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_item_arry->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_item_arry->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_item );

		//VALIDITY attribute
		if (flat_item_arry->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_item_arry->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//PRIVATE attribute
		if (flat_item_arry->private_attr)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
		}

		pDDItem->appendChild( pDDItemAttributes );

		//ELEMENTS
		if( flat_item_arry->elements.count )
		{
			for( int i = 0; i < flat_item_arry->elements.count; i++ )
			{
				MSXML2::IXMLDOMElementPtr pMembers;
				pMembers = m_pDOMDoc->createElement( ATTRNAME_ELEMENTS );

				if (flat_item_arry->elements.list[i].evaled & FDI_IA_INDEX_EVALED)
				{
					pMembers->setAttribute( ATTRNAME_INDEX, flat_item_arry->elements.list[i].index );
				}
				if (flat_item_arry->elements.list[i].evaled & FDI_IA_REF_EVALED)
				{
					// DESC_REF ref
					FDIHartDDS_AddChildDescRef( pMembers, &(flat_item_arry->elements.list[i].ref));
				}

				if( flat_item_arry->elements.list[i].desc.length() )
				{
					CString tempDesc =  StripOffNPChars( (TCHAR*)flat_item_arry->elements.list[i].desc.c_str() );
					pMembers->setAttribute( ATTRNAME_DESCRIPTION, (LPCTSTR)tempDesc );
				}
				if( flat_item_arry->elements.list[i].help.length() )
				{
					CString tempHelp =  StripOffNPChars( (TCHAR*)flat_item_arry->elements.list[i].help.c_str() );
					pMembers->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
				}
				pDDItem->appendChild( pMembers );

			}
		}
	/*}
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Item Array Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;

}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_chart_item
//
//	Description: This function calls ddi_get_item() for the type: CHART. It retrieves the 
//				 id, label, kind, type, and name of that particular chart, from the   
//				 flat: FLAT_CHART. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the chart itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_chart_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	
	FDI_GENERIC_ITEM generic_item(ITYPE_CHART);
	CString temp_chart;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_CHART::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

//	if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_CHART *flat_chart  = dynamic_cast<FLAT_CHART *>(generic_item.item);
		
		Add_MEMBER_LIST(&flat_chart->members);

		if (!m_bLoadXML)
		{
			return r_code;
		}
		
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_CHART Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_CHART *flat_pointer1 = new FLAT_CHART(*flat_chart);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_CHART flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_CHART *flat_pointer2 = new FLAT_CHART(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		AmsHartDDS_convert_chart_type( flat_chart->type, temp_chart );

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_chart->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_chart->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_chart->symbol_name.c_str() );

		if ( flat_chart->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_chart->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE, flat_chart->type );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_chart );
		if( flat_chart->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_chart->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}
		
		//HEIGHT attribute
		CString size_name;
		FDIHartDDS_convert_size_to_name( flat_chart->height, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_HEIGHT, (LPCTSTR)size_name );
		}

		//WIDTH attribute
		FDIHartDDS_convert_size_to_name( flat_chart->width, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_WIDTH, (LPCTSTR)size_name );
		}

		//LENGHT attribute
		pDDItemAttributes->setAttribute( ATTRNAME_LENGTH, flat_chart->length );

		//VALIDITY attribute
		if (flat_chart->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_chart->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//CYCLE_TIME attribute
		pDDItemAttributes->setAttribute( ATTRNAME_CYCLE_TIME, flat_chart->cycle_time );

		pDDItem->appendChild( pDDItemAttributes );
		
		for( int i = 0; i < flat_chart->members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildMember(pDDItem, &flat_chart->members.list[i]);
		}
	}

	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Chart Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;

}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_graph_item
//
//	Description: This function calls ddi_get_item() for the type: GRAPH. It retrieves the 
//				 id, label, kind, and name of that particular graph, from the   
//				 flat: FLAT_GRAPH. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the graph itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_graph_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{

	FDI_GENERIC_ITEM generic_item(ITYPE_GRAPH);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_GRAPH::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		r_code = 0;;

		FLAT_GRAPH *flat_graph  = dynamic_cast<FLAT_GRAPH *>(generic_item.item);

		Add_MEMBER_LIST(&flat_graph->members);

		Add_ITEM_ID(flat_graph->x_axis);
			
		if (!m_bLoadXML)
		{
			return r_code;
		}

				
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_GRAPH Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_GRAPH *flat_pointer1 = new FLAT_GRAPH(*flat_graph);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_GRAPH flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_GRAPH *flat_pointer2 = new FLAT_GRAPH(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		// Dump Item_information if available 
		if (flat_graph->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_graph->item_info);
		}
		
		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_graph->symbol_name.c_str() );

		if( flat_graph->label.length())
		{
			CString tempLabel =  StripOffNPChars((TCHAR*) flat_graph->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_graph->help.length() )
		{
			CString tempHelp =  StripOffNPChars((TCHAR*) flat_graph->help.c_str());
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}
		
		//HEIGHT attribute
		CString size_name;
		FDIHartDDS_convert_size_to_name( flat_graph->height, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_HEIGHT, (LPCTSTR)size_name );
		}

		//WIDTH attribute
		FDIHartDDS_convert_size_to_name( flat_graph->width, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_WIDTH, (LPCTSTR)size_name );
		}

		//VALIDITY attribute
		if (flat_graph->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_graph->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//CYCLE_TIME attribute
		pDDItemAttributes->setAttribute( ATTRNAME_CYCLE_TIME, flat_graph->cycle_time );

		//X_AXIS attribute
		if (flat_graph->x_axis != 0)
		{
			CString strTemp;
			FormatItemIdAndName(flat_graph->x_axis, strTemp);
			pDDItemAttributes->setAttribute(ATTRNAME_X_AXIS, (LPCTSTR)strTemp);
		}

		pDDItem->appendChild(pDDItemAttributes);

		for( int i = 0; i < flat_graph->members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildMember(pDDItem, &flat_graph->members.list[i]);
		}
	//}

	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Graph Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_grid_item
//
//	Description: This function calls ddi_get_item() for the type: GRID. It retrieves the 
//				 id, label, kind, and name of that particular grid, from the   
//				 flat: FLAT_GRID. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the grid itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_grid_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_GRID);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_GRID::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	CString temp_handle;

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		r_code = 0;

		FLAT_GRID *flat_grid = dynamic_cast< FLAT_GRID *>(generic_item.item);

		Add_VECTOR_LIST(&flat_grid->vectors);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_GRID Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_GRID *flat_pointer1 = new FLAT_GRID(*flat_grid);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_GRID flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_GRID *flat_pointer2 = new FLAT_GRID(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		AmsHartDDS_convert_handling( flat_grid->handling, temp_handle );

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_grid->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_grid->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_grid->symbol_name.c_str() );

		if( flat_grid->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_grid->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		pDDItemAttributes->setAttribute( ATTRNAME_HANDLING, flat_grid->handling );
		pDDItemAttributes->setAttribute( ATTRNAME_HANDLING_AS_STR, (LPCTSTR)temp_handle );
		if( flat_grid->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_grid->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		//HEIGHT attribute
		CString size_name;
		FDIHartDDS_convert_size_to_name( flat_grid->height, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_HEIGHT, (LPCTSTR)size_name );
		}

		//WIDTH attribute
		FDIHartDDS_convert_size_to_name( flat_grid->width, size_name );
		if (!(size_name.IsEmpty()))
		{
			pDDItemAttributes->setAttribute( ATTRNAME_WIDTH, (LPCTSTR)size_name );
		}

		//ORIENTATION attribute
		if (flat_grid->orientation == FDI_ORIENT_HORIZ)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_ORIENTATION, (LPCTSTR)_T("Horizontal") );
		}
		else if (flat_grid->orientation == FDI_ORIENT_VERT)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_ORIENTATION, (LPCTSTR)_T("Vertical") );
		}

		//VALIDITY attribute
		if (flat_grid->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_grid->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		pDDItem->appendChild( pDDItemAttributes );

		MSXML2::IXMLDOMElementPtr pVectorList;
		pVectorList = m_pDOMDoc->createElement( ELMTNAME_VECTOR_LIST );
		//VECTORS attribute
		for ( int childIndex = 0; childIndex < flat_grid->vectors.count; childIndex ++ )
		{
			FDIHartDDS_AddChildVectorAttr(pVectorList, &flat_grid->vectors.vectors[childIndex]);
		}
		pDDItem->appendChild(pVectorList);
	/*}

	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Grid Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;

}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_axis_item
//
//	Description: This function calls ddi_get_item() for the type: AXIS. It retrieves the 
//				 id, label, kind, and name of that particular axis, from the   
//				 flat: FLAT_AXIS. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the axis itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_axis_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_AXIS::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


	/*if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{*/
		r_code = 0;

		FLAT_AXIS *flat_axis  = dynamic_cast<FLAT_AXIS *>(generic_item.item);

		Add_OP_REF(&flat_axis->min_axis_ref);
		Add_OP_REF(&flat_axis->max_axis_ref);
				
		if (!m_bLoadXML)
		{
			return r_code;
		}
	//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_AXIS Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_AXIS *flat_pointer1 = new FLAT_AXIS(*flat_axis);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_AXIS flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_AXIS *flat_pointer2 = new FLAT_AXIS(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		//if (flat_axis->item_info.attrs.count > 0)
		if (flat_axis->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_axis->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_axis->symbol_name.c_str() );

		if( flat_axis->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_axis->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_axis->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_axis->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp  );
		}

		//CONSTANT_UNIT
		if (flat_axis->constant_unit.length() > 0)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_CONST_UNIT, flat_axis->constant_unit.c_str() );
		}

		//MIN_VALUE
		if ( flat_axis->min_axis.eType != EXPR::EXPR_TYPE_NONE )
		{
			pDDItemAttributes->setAttribute( ATTRNAME_MIN_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_axis->min_axis) );
		}

		//MIN_VALUE_REF
		if ((flat_axis->min_axis_ref.op_ref_type == COMPLEX_TYPE) || (flat_axis->min_axis_ref.op_info.id != 0))
		{
			FDIHartDDS_AddChildOpRef(pDDItemAttributes, ATTRNAME_MIN_VALUE_REFERENCE, &(flat_axis->min_axis_ref));
		}

		
		//MAX_VALUE
		if ( flat_axis->max_axis.eType != EXPR::EXPR_TYPE_NONE )
		{
			pDDItemAttributes->setAttribute( ATTRNAME_MAX_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_axis->max_axis) );
		}

		//MAX_VALUE_REF
		if ((flat_axis->max_axis_ref.op_ref_type == COMPLEX_TYPE) || (flat_axis->max_axis_ref.op_info.id != 0))
		{
			FDIHartDDS_AddChildOpRef(pDDItemAttributes, ATTRNAME_MAX_VALUE_REFERENCE, &(flat_axis->max_axis_ref));
		}

		//SCALING
		CString scaling_name;
		FDIHartDDS_convert_scaling_to_name(flat_axis->scaling, scaling_name);
		pDDItemAttributes->setAttribute( ATTRNAME_SCALING, flat_axis->scaling );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)scaling_name );

		pDDItem->appendChild( pDDItemAttributes );


		// See if this AXIS has a Unit
		ITEM_ID unitItem = 0;
		r_code = m_EDDEngine->GetAxisUnitRelItemId(iBlockIndex, 0, item_id, &unitItem);
		if (r_code == 0)
		{
			AddItemWithName( pDDItem, L"UnitItem", unitItem ); // Adds Item to the list of DD Items in the XML
		}

	/*}

	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Axis Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_waveform_item
//
//	Description: This function calls ddi_get_item() for the type: WAVEFORM. It retrieves the 
//				 id, label, kind, type, and name of that particular waveform, from the   
//				 flat: FLAT_WAVEFORM. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the waveform itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_waveform_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_WAVEFORM);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_WAVEFORM::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	CString 				temp_waveform;
	CString					temp_handle;

	
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	/*if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{*/
		r_code = 0;

		FLAT_WAVEFORM *flat_waveform= dynamic_cast<FLAT_WAVEFORM *>(generic_item.item);

		Add_ACTION_LIST(&flat_waveform->init_actions);
		Add_ACTION_LIST(&flat_waveform->refresh_actions);
		Add_ACTION_LIST(&flat_waveform->exit_actions);

		Add_DATA_ITEM_LIST(&flat_waveform->x_values);
		Add_DATA_ITEM_LIST(&flat_waveform->y_values);
		Add_DATA_ITEM_LIST(&flat_waveform->key_x_values);
		Add_DATA_ITEM_LIST(&flat_waveform->key_y_values);

		Add_ITEM_ID(flat_waveform->y_axis);

		if (!m_bLoadXML)
		{
			return r_code;
		}
	
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_WAVEFORM Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_WAVEFORM *flat_pointer1 = new FLAT_WAVEFORM(*flat_waveform);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_WAVEFORM flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_WAVEFORM *flat_pointer2 = new FLAT_WAVEFORM(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}
		
		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		AmsHartDDS_convert_waveform_type( flat_waveform->waveform_type, temp_waveform );
		AmsHartDDS_convert_handling( flat_waveform->handling, temp_handle );

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_waveform->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_waveform->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME,  flat_waveform->symbol_name.c_str() );

		if( flat_waveform->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_waveform->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE, flat_waveform->waveform_type );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_waveform );
		pDDItemAttributes->setAttribute( ATTRNAME_HANDLING, flat_waveform->handling );
		pDDItemAttributes->setAttribute( ATTRNAME_HANDLING_AS_STR, (LPCTSTR)temp_handle );
		if( flat_waveform->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_waveform->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		//EMPHASIS
		if (flat_waveform->emphasis)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_EMPHASIS, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_EMPHASIS, (LPCTSTR)_T("False") );
		}

		//LINE_COLOR
		pDDItemAttributes->setAttribute(ATTRNAME_LINE_COLOR, (unsigned long)flat_waveform->line_color);

		//LINE_TYPE
		CString line_type_name;
		FDIHartDDS_convert_line_type( flat_waveform->line_type, line_type_name );
		pDDItemAttributes->setAttribute( ATTRNAME_LINE_TYPE, flat_waveform->line_type );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)line_type_name );

		//VALIDITY attribute
		if (flat_waveform->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_waveform->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//Y_AXIS attribute
		if (flat_waveform->y_axis)
		{
			CString strTemp;
			FormatItemIdAndName(flat_waveform->y_axis, strTemp);
			pDDItemAttributes->setAttribute(ATTRNAME_Y_AXIS, (LPCTSTR)strTemp);
		}
		pDDItem->appendChild ( pDDItemAttributes );

		//EXIT_ACTIONS
		for ( int childIndex = 0; childIndex < flat_waveform->exit_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_EXITACT_ITEM, &flat_waveform->exit_actions.list[childIndex]);
		}

		//INIT_ACTIONS
		for ( int childIndex = 0; childIndex < flat_waveform->init_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_INITACT_ITEM, &flat_waveform->init_actions.list[childIndex]);
		}

		//REFRESH_ACTIONS
		for ( int childIndex = 0; childIndex < flat_waveform->refresh_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_REFRESHACT_ITEM, &flat_waveform->refresh_actions.list[childIndex]);
		}

		//X_VALUES
		for ( int childIndex = 0; childIndex < flat_waveform->x_values.count; childIndex ++ )
		{
			FDIHartDDS_AddChildDataItem(pDDItem, ELMTNAME_X_ITEM, &flat_waveform->x_values.list[childIndex]);
		}

		//Y_VALUES
		for ( int childIndex = 0; childIndex < flat_waveform->y_values.count; childIndex ++ )
		{
			FDIHartDDS_AddChildDataItem(pDDItem, ELMTNAME_Y_ITEM, &flat_waveform->y_values.list[childIndex]);
		}

		//KEY_POINTS - X_VALUES
		for ( int childIndex = 0; childIndex < flat_waveform->key_x_values.count; childIndex ++ )
		{
			FDIHartDDS_AddChildDataItem(pDDItem, ELMTNAME_KEY_POINT_X_ITEM, &flat_waveform->key_x_values.list[childIndex]);
		}

		//KEY_POINTS - Y_VALUES
		for ( int childIndex = 0; childIndex < flat_waveform->key_y_values.count; childIndex ++ )
		{
			FDIHartDDS_AddChildDataItem(pDDItem, ELMTNAME_KEY_POINT_Y_ITEM, &flat_waveform->key_y_values.list[childIndex]);
		}

	/*}

	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Waveform Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_image_item
//
//	Description: This function calls ddi_get_item() for the type: IMAGE. It retrieves the 
//				 id, label, kind, and name of that particular image, from the   
//				 flat: FLAT_IMAGE. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item 
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the image itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_image_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_IMAGE);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_IMAGE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		r_code = 0;

		FLAT_IMAGE *flat_image = dynamic_cast<FLAT_IMAGE *>(generic_item.item);

		Add_DESC_REF(&flat_image->link);

		if (!m_bLoadXML)
		{
			return r_code;
		}
		
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_IMAGE Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_IMAGE *flat_pointer1 = new FLAT_IMAGE(*flat_image);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_IMAGE flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_IMAGE *flat_pointer2 = new FLAT_IMAGE(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		// Dump Item_information if available 
		if (flat_image->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_image->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_image->symbol_name.c_str() );

		if( flat_image->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_image->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_image->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_image->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		// Image Data - Extract first 8 bytes of Image data.

		if(flat_image->entry.data)
		{
			static wchar_t hex_chars[] = L"0123456789abcdef";
			unsigned long Size = 8;
			wchar_t buffer[17];		// Create a new buffer to hold the bin hex string
			
			for (unsigned i = 0; i < 8; i++)					// For each byte, render it as two hex digits
			{
				unsigned char ch = (flat_image->entry.data[i] & 0xF0) >> 4;
				buffer[i*2] = hex_chars[ch];

				ch = flat_image->entry.data[i] & 0x0F;
				buffer[(i*2)+1] = hex_chars[ch];
			}
			buffer[ Size*2 ] = NULL;	// NULL terminate

			pDDItemAttributes->setAttribute( ATTRNAME_IMAGEBYTES, buffer );
		}

		if( flat_image->image_id.length() )
		{
			CString tempImageId =  StripOffNPChars( (TCHAR*)flat_image->image_id.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_IMAGEID, (LPCTSTR)tempImageId );
		}

		//LINK attribute
		if (flat_image->link.id != 0)
		{
			FDIHartDDS_AddChildDescRef(pDDItemAttributes, &(flat_image->link));
		}

		//VALIDITY attribute
		if (flat_image->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_image->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		pDDItem->appendChild( pDDItemAttributes );
	//}

	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Image Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;
}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_record_item
//
//	Description: This function calls ddi_get_item() for the type: RECORD. It retrieves the 
//				 id, label, kind, and name of that particular record, from the   
//				 flat: FLAT_RECORD. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are done with the 
//				 flat information of that item we call ddi_clean_item() to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the record itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_record_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
	CString					temp_item;

	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_RECORD::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );
	
	r_code = 0;

	FLAT_RECORD *flat_record = dynamic_cast<FLAT_RECORD *>(generic_item.item);
	
	Add_MEMBER_LIST(&flat_record->members);

	Add_RESPONSE_CODES(&flat_record->resp_codes);

	if (!m_bLoadXML)
	{
		return r_code;
	}
				
	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_RECORD Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_RECORD *flat_pointer1 = new FLAT_RECORD(*flat_record);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_RECORD flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_RECORD *flat_pointer2 = new FLAT_RECORD(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	// Dump Item_information if available 
	if (flat_record->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_record->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_record->symbol_name.c_str() );

	if( flat_record->label.length() )
	{
		CString tempLabel =  StripOffNPChars( (TCHAR*)flat_record->label.c_str() );
		pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
	}
	if( flat_record->help.length() )
	{
		CString tempHelp =  StripOffNPChars( (TCHAR*)flat_record->help.c_str() );
		pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );		
	}

	//VALIDITY attribute
	if (flat_record->valid)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
	}

	//VISIBILITY attribute
	if (flat_record->visibility)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
	}

	//PRIVATE attribute
	if (flat_record->private_attr)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
	}

	//WRITE_MODE
	if (flat_record->write_mode != 0)
	{
		CString temp_writemode;
		FDIHartDDS_convert_writeMode_to_name(flat_record->write_mode, temp_writemode);
		pDDItemAttributes->setAttribute( ATTRNAME_WRITE_MODE, (LPCTSTR)temp_writemode );
	}

	pDDItem->appendChild( pDDItemAttributes );

	//RESPONSE_CODES
	r_code = FDIHartDDS_AddChildResponseCodes (pDDItem, &flat_record->resp_codes);

	//MEMBERS
	for( int i = 0; i < flat_record->members.count; i++ )
	{
		r_code = FDIHartDDS_AddChildMember(pDDItem, &flat_record->members.list[i]);
	}

	//Update Table
	FDI_PARAM_SPECIFIER ParamSpec;
	ParamSpec.eType = FDI_PS_ITEM_ID;	// First get RelationsAndUpdateInfo for the entire record
	ParamSpec.id = item_id;

	DESC_REF DescRef;
	DescRef.id = item_id;
	DescRef.type = ITYPE_RECORD;

	GetRecursiveRelations( pDDItem, iBlockIndex, &ParamSpec, &DescRef);

	return r_code;
}

bool CDDSXMLBuilder::ParamSpecGetSI(FDI_PARAM_SPECIFIER *pParamSpec, CString &strSubIndex)
{
	bool bRet = false;

	switch (pParamSpec->eType)
	{
	case FDI_PS_ITEM_ID:		// No subindex
		break;

	case FDI_PS_ITEM_ID_SI:		// Only one SubIndex
		{
			strSubIndex.Format(L"%d", pParamSpec->subindex);
			bRet = true;
		}
		break;

	case FDI_PS_ITEM_ID_COMPLEX:	// Multiple SubIndexes
		{
			OP_REF_INFO_LIST *pRefList = &pParamSpec->RefList;

			strSubIndex.Format( L"%d", pRefList->list[0].member );

			for (int i=1; i < pRefList->count-1; i++)		// Append all but the last entry
			{
				strSubIndex.AppendFormat(L", %u", pRefList->list[i].member);
			}

			bRet = true;
		}
		break;

	default:
		break;
	}

	if (bRet == false)	// If a subindex was not found, set the output string to ""
	{
		strSubIndex.Empty();
	}

	return bRet;
}


void CDDSXMLBuilder::ParamSpecAddSI(int iBlockIndex,
	FDI_PARAM_SPECIFIER *pNewParamSpec,			// New Param Spec to fill
	const FDI_PARAM_SPECIFIER *pOrigParamSpec,	// Current Param Spec
	const DESC_REF *pOrigDescRef,				// Desc Ref of the Current Param Spec
	const DESC_REF *pNewDescRef,				// Desc Ref of the item to add
	ulong index)								// Index or Member of the item to add
{
	int rc = 0;

	switch (pOrigParamSpec->eType)
	{
	case FDI_PS_ITEM_ID:		// Simply add the index to make an _SI
		{
			pNewParamSpec->eType	= FDI_PS_ITEM_ID_SI;
			pNewParamSpec->id		= pOrigParamSpec->id;
			pNewParamSpec->subindex	= index;
		}
		break;

	case FDI_PS_ITEM_ID_SI:		// Make a Complex with 3 levels
		{
			// Find the type of the Original Item Id
			FDI_ITEM_SPECIFIER OrigItemSpec = { FDI_ITEM_ID, 0};
			OrigItemSpec.item.id = pOrigParamSpec->id;

			ITEM_TYPE OrigParamSpecIType = ITYPE_NO_VALUE;

			m_EDDEngine->GetItemType(iBlockIndex, &OrigItemSpec, &OrigParamSpecIType);

			// Now fill in the RefList
			pNewParamSpec->eType	= FDI_PS_ITEM_ID_COMPLEX;
			pNewParamSpec->RefList.count = 3;
			pNewParamSpec->RefList.list = new OP_REF_INFO[pNewParamSpec->RefList.count];

			pNewParamSpec->RefList.list[0].id		= pOrigParamSpec->id;
			pNewParamSpec->RefList.list[0].type		= OrigParamSpecIType;
			pNewParamSpec->RefList.list[0].member	= pOrigParamSpec->subindex;

			pNewParamSpec->RefList.list[1].id		= pOrigDescRef->id;
			pNewParamSpec->RefList.list[1].type		= pOrigDescRef->type;
			pNewParamSpec->RefList.list[1].member	= index;

			pNewParamSpec->RefList.list[2].id		= pNewDescRef->id;
			pNewParamSpec->RefList.list[2].type		= pNewDescRef->type;
			pNewParamSpec->RefList.list[2].member	= 0;
		}
		break;

	case FDI_PS_ITEM_ID_COMPLEX:	// Make a Complex with "count+1" levels
		{
			pNewParamSpec->eType	= FDI_PS_ITEM_ID_COMPLEX;

			OP_REF_INFO_LIST *pNewRefList			= &pNewParamSpec->RefList;
			OP_REF_INFO_LIST const *pOrigRefList	= &pOrigParamSpec->RefList;

			pNewRefList->count = pOrigRefList->count + 1;	// Make the new one bigger
			pNewRefList->list = new OP_REF_INFO[pNewRefList->count];

			for (int i=0; i < pOrigRefList->count; i++)		// Copy in all the original RefList contents
			{
				pNewRefList->list[i] = pOrigRefList->list[i];
			}

			pNewRefList->list[pOrigRefList->count-1].member	= index;	// Overwrite the member entry in second to last

			pNewRefList->list[pNewRefList->count-1].id		= pNewDescRef->id;		// Add the new entry
			pNewRefList->list[pNewRefList->count-1].type	= pNewDescRef->type;
			pNewRefList->list[pNewRefList->count-1].member	= 0;
		}
		break;

	default:
		rc = 1;
	}
}


void CDDSXMLBuilder::GetRecursiveRelations( MSXML2::IXMLDOMElementPtr& pDDItem, int iBlockIndex,
	FDI_PARAM_SPECIFIER *pParamSpec, DESC_REF *curDescRef)
{
	int rc = 0;

	// Add the Relation and Update Info for this Param Spec
	AmsHartDDS_AddRelationsAndUpdateInfo( pDDItem, iBlockIndex, pParamSpec);

	// Depending upon the type of this Item, we want to recurse on the sub-elements
	switch (curDescRef->type)
	{
	case ITYPE_VARIABLE:
		// No need to recurse
		break;

	case ITYPE_ARRAY:
		{
			// get FLAT_ARRAY definition
			// For each element
			//		Update the Param Spec
			//		Call GetRecursiveRelations()
			FDI_ITEM_SPECIFIER ItemSpec = { FDI_ITEM_ID, curDescRef->id, 0 };
			FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
			AttributeNameSet attribute_list = nsEDDEngine::type_definition | nsEDDEngine::number_of_elements;

			int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &ItemSpec, &attribute_list, TEST_LANGUAGE, &generic_item);
			if( r_code == 0 )
			{
				FLAT_ARRAY* arrayItem = dynamic_cast<FLAT_ARRAY*>(generic_item.item);
		
				// Find the IType of the child elements
				FDI_ITEM_SPECIFIER childItemSpec = { FDI_ITEM_ID, arrayItem->type, 0};

				ITEM_TYPE ChildElemIType = ITYPE_NO_VALUE;

				m_EDDEngine->GetItemType(iBlockIndex, &childItemSpec, &ChildElemIType);

				// Gather the Desc Ref of the array elements
				DESC_REF childDescRef;
				childDescRef.id = arrayItem->type;
				childDescRef.type = ChildElemIType;

				for( ulong index = 0; index < arrayItem->num_of_elements; index++ )
				{
					// Make new param spec
					FDI_PARAM_SPECIFIER childParamSpec;

					ulong subindex = ((m_Protocol == FF) || (m_Protocol == ISA100)) ? index+1 : index;	// Adjust the subindex for protocol

					ParamSpecAddSI(iBlockIndex, &childParamSpec, pParamSpec, curDescRef, &childDescRef, subindex);
					
					// Recursively get the relations for each element
					GetRecursiveRelations( pDDItem, iBlockIndex, &childParamSpec, &childDescRef);
				}
			}
		}
		break;

	case ITYPE_RECORD:
		{
			// get FLAT_RECORD definition
			// For each element
			//		Update the Param Spec
			//		Call GetRecursiveRelations()
			FDI_ITEM_SPECIFIER ItemSpec = { FDI_ITEM_ID, curDescRef->id, 0 };
			FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
			AttributeNameSet attribute_list = nsEDDEngine::members;

			int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &ItemSpec, &attribute_list, TEST_LANGUAGE, &generic_item);
			if( r_code == 0 )
			{
				FLAT_RECORD* recItem = dynamic_cast<FLAT_RECORD*>(generic_item.item);
		
				MEMBER_LIST *pMemberList = &recItem->members;

				for( ushort index = 0; index < pMemberList->count; index++ )
				{
					// Gather the Desc Ref of the Collection member
					DESC_REF childDescRef;
					childDescRef.id = pMemberList->list[index].ref.id;
					childDescRef.type = pMemberList->list[index].ref.type;

					// Make new param spec
					FDI_PARAM_SPECIFIER childParamSpec;

					ulong subindex = ((m_Protocol == FF) || (m_Protocol == ISA100)) ? index+1 : index;	// Adjust the subindex for protocol

					ParamSpecAddSI(iBlockIndex, &childParamSpec, pParamSpec, curDescRef, &childDescRef, subindex);
					
					// Recursively get the relations for each element
					GetRecursiveRelations( pDDItem, iBlockIndex, &childParamSpec, &childDescRef);
				}
			}
		}
		break;

	case ITYPE_COLLECTION:
		{
			// get FLAT_COLLECTION definition
			// For each element
			//		Update the Param Spec
			//		Call GetRecursiveRelations()
			FDI_ITEM_SPECIFIER ItemSpec = { FDI_ITEM_ID, curDescRef->id, 0 };
			FDI_GENERIC_ITEM generic_item(ITYPE_COLLECTION);
			AttributeNameSet attribute_list = nsEDDEngine::members;

			int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &ItemSpec, &attribute_list, TEST_LANGUAGE, &generic_item);
			if( r_code == 0 )
			{
				FLAT_COLLECTION* colItem = dynamic_cast<FLAT_COLLECTION*>(generic_item.item);
		
				OP_MEMBER_LIST *pOpMemberList = &colItem->op_members;

				for( ushort index = 0; index < pOpMemberList->count; index++ )
				{
					// Gather the Desc Ref of the Collection member
					DESC_REF childDescRef;
					childDescRef.id = pOpMemberList->list[index].ref.desc_id;
					childDescRef.type = pOpMemberList->list[index].ref.desc_type;

					// Make new param spec
					FDI_PARAM_SPECIFIER childParamSpec;

					ParamSpecAddSI(iBlockIndex, &childParamSpec, pParamSpec, curDescRef, &childDescRef, pOpMemberList->list[index].name);
					
					// Recursively get the relations for each element
					GetRecursiveRelations( pDDItem, iBlockIndex, &childParamSpec, &childDescRef);
				}
			}
		}
		break;

	case ITYPE_LIST:
		{
			// get FLAT_LIST definition
			// For each element
			//		Update the Param Spec
			//		Call GetRecursiveRelations()
			FDI_ITEM_SPECIFIER ItemSpec = { FDI_ITEM_ID, curDescRef->id, 0 };
			FDI_GENERIC_ITEM generic_item(ITYPE_LIST);
			AttributeNameSet attribute_list = nsEDDEngine::type_definition | nsEDDEngine::capacity | nsEDDEngine::count;

			int r_code = m_EDDEngine->GetItem( iBlockIndex, 0, &ItemSpec, &attribute_list, TEST_LANGUAGE, &generic_item);
			if( r_code == 0 )
			{
				FLAT_LIST* listItem = dynamic_cast<FLAT_LIST*>(generic_item.item);
		
				// Find the IType of the child elements
				FDI_ITEM_SPECIFIER childItemSpec = { FDI_ITEM_ID, listItem->type, 0};

				ITEM_TYPE ChildElemIType = ITYPE_NO_VALUE;

				m_EDDEngine->GetItemType(iBlockIndex, &childItemSpec, &ChildElemIType);

				// Gather the Desc Ref of the list elements
				DESC_REF childDescRef;
				childDescRef.id = listItem->type;
				childDescRef.type = ChildElemIType;

				ulong count = max( max( listItem->capacity, listItem->count), 2);
				for( ulong index = 0; index < count; index++ )
				{
					// Make new param spec
					FDI_PARAM_SPECIFIER childParamSpec;

					ulong subindex = ((m_Protocol == FF) || (m_Protocol == ISA100)) ? index+1 : index;	// Adjust the subindex for protocol

					ParamSpecAddSI(iBlockIndex, &childParamSpec, pParamSpec, curDescRef, &childDescRef, subindex);
					
					// Recursively get the relations for each element
					GetRecursiveRelations( pDDItem, iBlockIndex, &childParamSpec, &childDescRef);
				}
			}
		}
		break;

	default:
		rc = 1;
		break;
	}

	rc = 3;
	return;
}

void CDDSXMLBuilder::AmsHartDDS_AddRelationsAndUpdateInfo(MSXML2::IXMLDOMElementPtr& pDDItem, int iBlockIndex, FDI_PARAM_SPECIFIER *ParamSpec)
{
	ITEM_ID wao_item = 0;
	ITEM_ID unit_item = 0;
	OP_REF_LIST update_list;
	OP_REF_LIST dominant_list;

	m_EDDEngine->GetRelationsAndUpdateInfo(iBlockIndex, nullptr, ParamSpec, &wao_item, &unit_item, &update_list, &dominant_list);

	if (wao_item != 0 || unit_item != 0 || update_list.count != 0 || dominant_list.count != 0)
	{
		MSXML2::IXMLDOMElementPtr  pRelationsAndUpdate = m_pDOMDoc->createElement( ATTRNAME_RELATIONS_AND_UPDATE_INFO );

		CString strTemp;
		if (ParamSpecGetSI(ParamSpec, strTemp))	// Find the Subindex(es), if any
		{
			pRelationsAndUpdate->setAttribute(ATTRNAME_SUBINDEX, (LPCTSTR)strTemp);
		}

		if (wao_item != 0)
		{
			AddItemWithName( pRelationsAndUpdate, L"WAOItem", wao_item ); // Adds Item to the list of DD Items in the XML
		}

		if (unit_item != 0)
		{
			AddItemWithName( pRelationsAndUpdate, L"UnitItem", unit_item ); // Adds Item to the list of DD Items in the XML
		}

		if (update_list.count != 0)
		{
			MSXML2::IXMLDOMElementPtr  pUpdateItems = m_pDOMDoc->createElement( L"UpdateList" );
			for (int i = 0 ; i < update_list.count; i++)
			{
				FDIHartDDS_AddChildOpRef (pUpdateItems,  L"UpdateItem" , &update_list.list[i] );
			}
			pRelationsAndUpdate->appendChild( pUpdateItems );
		}

		if (dominant_list.count != 0)
		{
			MSXML2::IXMLDOMElementPtr  pDominantItems = m_pDOMDoc->createElement( L"DominantList" );
			for (int i = 0 ; i < dominant_list.count; i++)
			{
					
				FDIHartDDS_AddChildOpRef (pDominantItems,  L"DominantItem" , &dominant_list.list[i] );
			}
			pRelationsAndUpdate->appendChild( pDominantItems );
		}

		pDDItem->appendChild( pRelationsAndUpdate );
	}
}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_collection_item
//
//	Description: This function calls ddi_get_item() for the type: COLLECTION. It retrieves 
//				 the id, label, kind, and name of that particular collection, from the   
//				 flat: FLAT_COLLECTION. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_collection_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_COLLECTION);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_COLLECTION::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_COLLECTION *flat_collection = dynamic_cast< FLAT_COLLECTION * >(generic_item.item);

		Add_OP_MEMBER_LIST(&flat_collection->op_members);

		if (!m_bLoadXML)
		{
			return r_code;
		}
						
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_COLLECTION Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_COLLECTION *flat_pointer1 = new FLAT_COLLECTION(*flat_collection);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_COLLECTION flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_COLLECTION *flat_pointer2 = new FLAT_COLLECTION(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}


		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		// Dump Item_information if available 
		if (flat_collection->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_collection->item_info);
		}
		
		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_collection->symbol_name.c_str() );

		if( flat_collection->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_collection->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_collection->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_collection->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		if (flat_collection->subtype != ITYPE_NO_VALUE)
		{
			AmsHartDDS_convert_item_type(flat_collection->subtype, temp_item);
			pDDItemAttributes->setAttribute(ATTRNAME_TYPE_AS_STRING, (LPCTSTR)temp_item);
		}
		else
		{
			pDDItemAttributes->setAttribute(ATTRNAME_TYPE_AS_STRING, "");	// There is no sub-type specified
		}

		//VALIDITY attribute
		if (flat_collection->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_collection->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//PRIVATE attribute
		if (flat_collection->private_attr)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
		}

		pDDItem->appendChild( pDDItemAttributes );

		//MEMBERS
		for( int i = 0; i < flat_collection->op_members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildOPMembers(pDDItem, &flat_collection->op_members.list[i]);
		}

	}

	/*else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Collection Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_source_item
//
//	Description: This function calls ddi_get_item() for the type: SOURCE. It retrieves 
//				 the id, label, kind, and name of that particular source, from the   
//				 flat: FLAT_SOURCE. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_source_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_SOURCE);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_SOURCE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		r_code = 0;

		FLAT_SOURCE *flat_source = dynamic_cast< FLAT_SOURCE * >(generic_item.item);

		Add_OP_MEMBER_LIST(&flat_source->op_members);

		Add_ITEM_ID(flat_source->y_axis);

		Add_ACTION_LIST(&flat_source->init_actions);
		Add_ACTION_LIST(&flat_source->refresh_actions);
		Add_ACTION_LIST(&flat_source->exit_actions);

		if (!m_bLoadXML)
		{
			return r_code;
		}
									
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_SOURCE Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_SOURCE *flat_pointer1 = new FLAT_SOURCE(*flat_source);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_SOURCE flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_SOURCE *flat_pointer2 = new FLAT_SOURCE(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		// Dump Item_information if available 
		if (flat_source->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_source->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_source->symbol_name.c_str() );

		if( flat_source->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_source->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_source->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_source->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		//EMPHASIS
		if (flat_source->emphasis)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_EMPHASIS, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_EMPHASIS, (LPCTSTR)_T("False") );
		}

		//LINE_COLOR
		pDDItemAttributes->setAttribute(ATTRNAME_LINE_COLOR, (unsigned long)flat_source->line_color);

		//LINE_TYPE
		CString line_type_name;
		FDIHartDDS_convert_line_type( flat_source->line_type, line_type_name );
		pDDItemAttributes->setAttribute( ATTRNAME_LINE_TYPE, flat_source->line_type );
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)line_type_name );

		//VALIDITY attribute
		if (flat_source->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_source->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//Y_AXIS attribute
		if (flat_source->y_axis != 0)
		{
			CString strTemp;
			FormatItemIdAndName(flat_source->y_axis, strTemp);
			pDDItemAttributes->setAttribute(ATTRNAME_Y_AXIS, (LPCTSTR)strTemp);
		}
		pDDItem->appendChild( pDDItemAttributes );

		//MEMBERS

		for( int i = 0; i < flat_source->op_members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildOPMembers(pDDItem, &flat_source->op_members.list[i]);
		}

		//EXIT_ACTIONS
		for ( int childIndex = 0; childIndex < flat_source->exit_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_EXITACT_ITEM, &flat_source->exit_actions.list[childIndex]);
		}

		//INIT_ACTIONS
		for ( int childIndex = 0; childIndex < flat_source->init_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_INITACT_ITEM, &flat_source->init_actions.list[childIndex]);
		}

		//REFRESH_ACTIONS
		for ( int childIndex = 0; childIndex < flat_source->refresh_actions.count; childIndex ++ )
		{
			FDIHartDDS_AddChildPostPreAction(pDDItem, ELMTNAME_REFRESHACT_ITEM, &flat_source->refresh_actions.list[childIndex]);
		}

	//}
	//
	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Source Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_file_item
//
//	Description: This function calls ddi_get_item() for the type: FILE. It retrieves 
//				 the id, label, kind, and name of that particular file, from the   
//				 flat: FLAT_FILE. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_file_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item (ITYPE_FILE);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_FILE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_FILE *flat_file = dynamic_cast< FLAT_FILE * >(generic_item.item);

		Add_MEMBER_LIST(&flat_file->members);

		Add_ACTION_LIST(&flat_file->on_update_actions);

		if (!m_bLoadXML)
		{
			return r_code;
		}
										
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_FILE Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_FILE *flat_pointer1 = new FLAT_FILE(*flat_file);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_FILE flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_FILE *flat_pointer2 = new FLAT_FILE(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}


		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		//Dump Item_information if available 
		if (flat_file->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_file->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_file->symbol_name.c_str() );

		if( flat_file->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_file->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_file->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_file->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}
		pDDItem->appendChild( pDDItemAttributes );

		//MEMBERS
		for( int i = 0; i < flat_file->members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildMember(pDDItem, &flat_file->members.list[i]);
		}
	}
	
	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"File Flat information not complete...returned with error code %d", r_code );
	//}
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_varList_item
//
//	Description: This function calls ddi_get_item() for the type: VARIABLE_LIST. It retrieves 
//				 the id, label, kind, and name of that particular variable list, from the   
//				 flat: FLAT_VAR_LIST. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_varList_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item (ITYPE_VAR_LIST);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_VAR_LIST::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_VAR_LIST *flat_varList = dynamic_cast< FLAT_VAR_LIST * >(generic_item.item);

		Add_MEMBER_LIST(&flat_varList->members);

		Add_ITEM_ID(flat_varList->resp_codes);

		if (!m_bLoadXML)
		{
			return r_code;
		}

		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_VAR_LIST Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_VAR_LIST *flat_pointer1 = new FLAT_VAR_LIST(*flat_varList);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_VAR_LIST flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_VAR_LIST *flat_pointer2 = new FLAT_VAR_LIST(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		//Dump Item_information if available 
		if (flat_varList->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_varList->item_info);
		}
		
		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_varList->symbol_name.c_str() );

		if( flat_varList->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_varList->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		if( flat_varList->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_varList->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		if (flat_varList->isAvailable(response_codes))
		{
			CString strTemp;
			FormatItemIdAndName( flat_varList->resp_codes, strTemp);
			pDDItemAttributes->setAttribute( ELMTNAME_RESP_CODES_ITEM, (LPCTSTR)strTemp );
		}

		pDDItem->appendChild( pDDItemAttributes );

		

		//MEMBERS
		for( int i = 0; i < flat_varList->members.count; i++ )
		{
			r_code = FDIHartDDS_AddChildMember(pDDItem, &flat_varList->members.list[i]);
		}
	}

	/*else
	{
	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Var-List Flat information not complete...returned with error code %d", r_code );
	}*/
	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_wao_item
//
//	Description: This function calls ddi_get_item() for the type: WRITE-AS-ONE. It retrieves 
//				 the id, kind, and name of that particular write-as-one, from the   
//				 flat: FLAT_WAO. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_wao_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_WAO);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_WAO::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT( (r_code == 0) || (r_code == -1517) );

	/*if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{*/
		r_code = 0;

		FLAT_WAO *flat_wao = dynamic_cast<FLAT_WAO *>(generic_item.item);

		Add_OP_REF_TRAIL_LIST(&flat_wao->items);

		if (!m_bLoadXML)
		{
			return r_code;
		}
												
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_WAO Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_WAO *flat_pointer1 = new FLAT_WAO(*flat_wao);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_WAO flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_WAO *flat_pointer2 = new FLAT_WAO(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		//Dump Item_information if available 
		if (flat_wao->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_wao->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_wao->symbol_name.c_str() );

		pDDItem->appendChild( pDDItemAttributes );

		//REFERENCE ITEMS
		for ( int childIndex = 0; childIndex < flat_wao->items.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_REFERENCE_ITEM, &(flat_wao->items.list[childIndex]));
		}
	/*}
			
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"WAO Flat information not complete...returned with error code %d", r_code );
	}*/


	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_unit_item
//
//	Description: This function calls ddi_get_item() for the type: UNIT. It retrieves 
//				 the id, kind, and name of that particular unit, from the   
//				 flat: FLAT_UNIT. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_unit_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_UNIT);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_UNIT::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );


/*	if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{*/
		r_code = 0;

		FLAT_UNIT *flat_unit = dynamic_cast< FLAT_UNIT * >(generic_item.item);

		Add_OP_REF_TRAIL(&flat_unit->var);

		Add_OP_REF_TRAIL_LIST(&flat_unit->var_units);

		if (!m_bLoadXML)
		{
			return r_code;
		}													
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_UNIT Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_UNIT *flat_pointer1 = new FLAT_UNIT(*flat_unit);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_UNIT flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_UNIT *flat_pointer2 = new FLAT_UNIT(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		
		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		//Dump Item_information if available 
		if (flat_unit->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_unit->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_unit->symbol_name.c_str() );

		pDDItem->appendChild( pDDItemAttributes );

		//UPDATE_ITEMS: CAUSE-PARAMETER
		FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_CAUSE_PARAM_ITEM, &(flat_unit->var));

		//DEPEND_ITEMS: EFFECTED-PARAMETER
		for ( int childIndex = 0; childIndex < flat_unit->var_units.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_EFFECTED_PARAM_ITEM, &(flat_unit->var_units.list[childIndex]));
		}

	//}
	//	
	//else
	//{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Unit Flat information not complete...returned with error code %d", r_code );
	//}


	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_resp_codes_item
//
//	Description: This function calls ddi_get_item() for the type: ITYPE_RESP_CODES. It retrieves 
//				 the id, kind, and name of that particular unit, from the   
//				 flat: FLAT_RESP_CODE. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_resp_codes_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_RESP_CODES);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_RESP_CODE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );

	if (!m_bLoadXML)
	{
		return r_code;
	}


	r_code = 0;

	FLAT_RESP_CODE *flat_respCode = dynamic_cast< FLAT_RESP_CODE * >(generic_item.item);
																
	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_RESP_CODE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_RESP_CODE *flat_pointer1 = new FLAT_RESP_CODE(*flat_respCode);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_RESP_CODE flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_RESP_CODE *flat_pointer2 = new FLAT_RESP_CODE(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}
	
		
	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	//Dump Item_information if available 
	if (flat_respCode->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_respCode->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_respCode->symbol_name.c_str() );

	pDDItem->appendChild( pDDItemAttributes );

	//MEMBERS
	for ( int childIndex = 0; (childIndex < flat_respCode->member.count); childIndex ++ )
	{
		r_code = FDIHartDDS_AddChildResponseCodeList(pDDItem, &flat_respCode->member.list[childIndex]);
	}
	
	return r_code;
}


//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_refresh_item
//
//	Description: This function calls ddi_get_item() for the type: REFRESH. It retrieves 
//				 the id, kind, and name of that particular refresh, from the   
//				 flat: FLAT_REFRESH. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_refresh_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	CString temp_handle;
	FDI_GENERIC_ITEM generic_item(ITYPE_REFRESH);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_REFRESH::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	//{
		r_code = 0;

		FLAT_REFRESH *flat_refresh = dynamic_cast< FLAT_REFRESH * >(generic_item.item);

		Add_OP_REF_TRAIL_LIST(&flat_refresh->depend_items);
		Add_OP_REF_TRAIL_LIST(&flat_refresh->update_items);

		if (!m_bLoadXML)
		{
			return r_code;
		}
																	
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_REFRESH Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_REFRESH *flat_pointer1 = new FLAT_REFRESH(*flat_refresh);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_REFRESH flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_REFRESH *flat_pointer2 = new FLAT_REFRESH(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		//Dump Item_information if available 
		if (flat_refresh->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_refresh->item_info);
		}
		
		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_refresh->symbol_name.c_str() );

		pDDItem->appendChild( pDDItemAttributes );

		//UPDATE_ITEMS: CAUSE-PARAMETER
		for ( int childIndex = 0; childIndex < flat_refresh->depend_items.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_CAUSE_PARAM_ITEM, &(flat_refresh->depend_items.list[childIndex]));
		}

		//DEPEND_ITEMS: EFFECTED-PARAMETER
		for ( int childIndex = 0; childIndex < flat_refresh->update_items.count; childIndex ++ )
		{
			FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_EFFECTED_PARAM_ITEM, &(flat_refresh->update_items.list[childIndex]));
		}
	/*}
		
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Refresh Flat information not complete...returned with error code %d", r_code );
	}*/


	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_block_item
//
//	Description: This function calls ddi_get_item() for the type: BLOCK. It retrieves 
//				 the id, kind, and name of that particular block, from the   
//				 flat: FLAT_BLOCK. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_block_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	/////////////////////////////////////////////////
	if (item_id == 0)		// Early return for PB FDI format that has an item_id of 0 for the fake block
	{						// Remove this check after PB Tokenizer is fixed.
		return 0;
	}
	//////////////////////////////////////////////////

	CString temp_handle;
	FDI_GENERIC_ITEM generic_item(ITYPE_BLOCK);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_BLOCK::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );

	// if  (r_code == DDL_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_BLOCK *flat_block = dynamic_cast< FLAT_BLOCK * >(generic_item.item);

		// Add any of the found ItemIds to the 
		// Dump the Characteristics Record
		Add_ITEM_ID( flat_block->characteristic );

		// Dump the PARAMETERS
		Add_MEMBER_LIST(&flat_block->param);

		// Dump the LOCAL_PARAMETERS
		Add_MEMBER_LIST(&flat_block->local_param);

		// Dump the PARAMETER_LISTS
		Add_MEMBER_LIST(&flat_block->param_lists);

		// Dump the "Exported items"
		Add_MEMBER_LIST(&flat_block->menus);
		Add_MEMBER_LIST(&flat_block->methods);

		Add_MEMBER_LIST(&flat_block->charts);
		Add_MEMBER_LIST(&flat_block->graphs);
		Add_MEMBER_LIST(&flat_block->grids);

		Add_MEMBER_LIST(&flat_block->files);
		Add_MEMBER_LIST(&flat_block->lists);
		Add_MEMBER_LIST(&flat_block->plugins);

		// Dump the Item Lists
		Add_ITEM_ID_LIST(&flat_block->item_array);
		Add_ITEM_ID_LIST(&flat_block->collect);

		Add_ITEM_ID_LIST(&flat_block->menu);
		Add_ITEM_ID_LIST(&flat_block->method);
		Add_ITEM_ID_LIST(&flat_block->edit_disp);

		Add_ITEM_ID_LIST(&flat_block->unit);
		Add_ITEM_ID_LIST(&flat_block->refresh);
		Add_ITEM_ID_LIST(&flat_block->wao);

		Add_ITEM_ID_LIST(&flat_block->chart);
		Add_ITEM_ID_LIST(&flat_block->graph);
		Add_ITEM_ID_LIST(&flat_block->grid);
		Add_ITEM_ID_LIST(&flat_block->image);

		Add_ITEM_ID_LIST(&flat_block->axis);
		Add_ITEM_ID_LIST(&flat_block->source);
		Add_ITEM_ID_LIST(&flat_block->waveform);

		Add_ITEM_ID_LIST(&flat_block->file);
		Add_ITEM_ID_LIST(&flat_block->list);
		Add_ITEM_ID_LIST(&flat_block->plugin_items);

		if (!m_bLoadXML)
		{
			return r_code;
		}
																				
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_BLOCK Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_BLOCK *flat_pointer1 = new FLAT_BLOCK(*flat_block);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_BLOCK flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_BLOCK *flat_pointer2 = new FLAT_BLOCK(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );
		
		//Dump Item_information if available 
		if (flat_block->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_block->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_block->symbol_name.c_str() );

		// LABEL
		if( flat_block->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_block->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}

		// HELP
		if( flat_block->help.length() )
		{
			CString tempHelp =  StripOffNPChars((TCHAR*) flat_block->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		CString strTemp;
		// CHARACTERISTICS
		FormatItemIdAndName(flat_block->characteristic, strTemp);
		pDDItemAttributes->setAttribute( L"Characteristics", (LPCTSTR)strTemp);

		pDDItem->appendChild( pDDItemAttributes );		// Done with DDItemAttributes, so append it

		// PARAMETERS
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Parameters", &flat_block->param);
		// LOCAL_PARAMETERS
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"LocalParameters", &flat_block->local_param);
		// PARAMETER_LISTS
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"ParameterLists", &flat_block->param_lists);

		// Dump the "Exported items"
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Menus", &flat_block->menus);
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Methods", &flat_block->methods);

		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Charts", &flat_block->charts);
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Graphs", &flat_block->graphs);
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Grids", &flat_block->grids);

		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Lists", &flat_block->lists);
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Files", &flat_block->files);
		r_code = FDIHartDDS_AddChildMemberList(pDDItem, L"Plugins", &flat_block->plugins);

		// Dump the Item Lists
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"ReferenceArrayItems", &flat_block->item_array);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"CollectionItems", &flat_block->collect);

		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"UnitItems", &flat_block->unit);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"RefreshItems", &flat_block->refresh);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"WAOItems", &flat_block->wao);

		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"MenuItems", &flat_block->menu);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"MethodItems", &flat_block->method);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"EditDisplayItems", &flat_block->edit_disp);

		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"ChartItems", &flat_block->chart);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"GraphItems", &flat_block->graph);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"GridItems", &flat_block->grid);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"ImageItems", &flat_block->image);

		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"AxisItems", &flat_block->axis);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"SourceItems", &flat_block->source);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"WaveformItems", &flat_block->waveform);

		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"ListItems", &flat_block->list);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"FileItems", &flat_block->file);
		r_code = FDIHartDDS_AddChildItemIdList(pDDItem, L"PluginItems", &flat_block->plugin_items);

	}
	/*	
	else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"Block Flat information not complete...returned with error code %d", r_code );
	}*/


	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_block_b_item
//
//	Description: This function calls GetItem() for the type: BLOCK_B. It retrieves 
//				 the number and type of that particular block, from the   
//				 flat: FLAT_BLOCK_B. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//*****************************************************************************************//

int CDDSXMLBuilder::AmsHartDDS_get_block_b_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	/////////////////////////////////////////////////
	if (item_id == 0)		// Early return for PB FDI format that has an item_id of 0 for the fake block
	{						// Remove this check after PB Tokenizer is fixed.
		return 0;
	}
	//////////////////////////////////////////////////

	CString temp_handle;
	FDI_GENERIC_ITEM generic_item(ITYPE_BLOCK_B);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_BLOCK_B::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
	
	ASSERT( (r_code == 0) || (r_code == -1517) );



	if (!m_bLoadXML)
	{
		return r_code;
	}

	r_code = 0;

	FLAT_BLOCK_B *flat_block_b = dynamic_cast< FLAT_BLOCK_B * >(generic_item.item);
																					
	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_BLOCK_B Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_BLOCK_B *flat_pointer1 = new FLAT_BLOCK_B(*flat_block_b);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_BLOCK_B flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_BLOCK_B *flat_pointer2 = new FLAT_BLOCK_B(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	//Dump Item_information if available 
	if (flat_block_b->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_block_b->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_block_b->symbol_name.c_str() );

	//NUMBER
	pDDItemAttributes->setAttribute( ATTRNAME_NUMBER, (unsigned long)flat_block_b->number );
	//TYPE
	CString type_name;
	FDIHartDDS_convert_BlockType_to_name(flat_block_b->type, type_name);
	pDDItemAttributes->setAttribute( ATTRNAME_TYPE_AS_STRING, (LPCTSTR)type_name );

	pDDItem->appendChild( pDDItemAttributes );

	return r_code;
}

//*****************************************************************************************//
//
//	Name: AmsHartDDS_get_list_item
//
//	Description: This function calls ddi_get_item() for the type: LIST. It retrieves 
//				 the id, kind, and name of that particular list, from the   
//				 flat: FLAT_LIST. Once it retrieves the information, we create a new 
//				 element for the xml output and assign those parameters to there respective 
//				 attributes of that item type. They then are attached to the xml output that 
//				 is being collected every time it enters an item type. Once we are  
//				 done with the flat information of that item we call ddi_clean_item() 
//				 to free the memory.
//
//	Inputs:
//		ITEM_ID item_id - the id number for the particular item
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItem - Now contains a new attached information for
//											the collection itype under the specific item ID.
//
//	Returns:
//		SUCCESS
//
//	Author:
//		Katie Frost
//
//*****************************************************************************************//
int CDDSXMLBuilder::AmsHartDDS_get_list_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_LIST);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_LIST::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
		
	ASSERT( (r_code == 0) || (r_code == -1517) );

	//if( (r_code == DDS_SUCCESS) || (r_code == DDL_DEFAULT_ATTR) || (r_code == DDL_CHECK_RETURN_LIST) )
	{
		r_code = 0;

		FLAT_LIST * flat_list = dynamic_cast< FLAT_LIST * >(generic_item.item);

		Add_ITEM_ID(flat_list->type);

		Add_OP_REF(&flat_list->count_ref);

		if (!m_bLoadXML)
		{
			return r_code;
		}
																						
		//Flat test Suite
		static int flatTestCounter = 0;
		if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
		{
#ifdef _OUTPUT_TEST_SUITE_
			fprintf(stdout, "FLAT_LIST Test Suite Item#%d", item_spec.item.id);
			fflush(stdout);
#endif
			// Testing Copy Constructor from a heap variable
			FLAT_LIST *flat_pointer1 = new FLAT_LIST(*flat_list);
			//Testing assignment operator for FLAT_VAR into a local
			FLAT_LIST flat_local;
			flat_local = *flat_pointer1;
			//Testing copy constructor from a local variable
			FLAT_LIST *flat_pointer2 = new FLAT_LIST(flat_local);
			//Testing assignment operator into pointer
			*flat_pointer2 = *flat_pointer1;
			// Testing assignment operator for same (no action taken nothing gets messed up)
			flat_local = flat_local;

			// Testing deletes (and do cleanup)
			delete flat_pointer1;
			delete flat_pointer2;
		}

		if(generic_item.errors.count > 0){
			AddReturnList(pDDItem, &generic_item);
		}

		MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

		//Dump Item_information if available 
		if (flat_list->item_info.line_number != 0)
		{
			AddItemInformation(pDDItem, &flat_list->item_info);
		}

		pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_list->symbol_name.c_str() );

		if( flat_list->label.length() )
		{
			CString tempLabel =  StripOffNPChars( (TCHAR*)flat_list->label.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_LABEL, (LPCTSTR)tempLabel );
		}
		
		CString strTemp;
		FormatItemIdAndName( flat_list->type, strTemp);
		pDDItemAttributes->setAttribute( ATTRNAME_TYPE, (LPCTSTR)strTemp );

		if( flat_list->help.length() )
		{
			CString tempHelp =  StripOffNPChars( (TCHAR*)flat_list->help.c_str() );
			pDDItemAttributes->setAttribute( ATTRNAME_HELP, (LPCTSTR)tempHelp );
		}

		//CAPACITY
		pDDItemAttributes->setAttribute( ATTRNAME_CAPACITY, (unsigned long)flat_list->capacity );
		
		//COUNT
		pDDItemAttributes->setAttribute( ATTRNAME_COUNT, (unsigned long)flat_list->count );

		if ((flat_list->count_ref.op_ref_type == COMPLEX_TYPE) || (flat_list->count_ref.op_info.id != 0))
		{
				FDIHartDDS_AddChildOpRef(pDDItemAttributes, ATTRNAME_COUNT_REFERENCE, &(flat_list->count_ref));
		}

		//VALIDITY attribute
		if (flat_list->valid)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
		}

		//VISIBILITY attribute
		if (flat_list->visibility)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
		}

		//PRIVATE attribute
		if (flat_list->private_attr)
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("True") );
		}
		else
		{
			pDDItemAttributes->setAttribute( ATTRNAME_PRIVATE, (LPCTSTR)_T("False") );
		}

		pDDItem->appendChild( pDDItemAttributes );
	}
	
	//Command List
	AddCommandListForItem(pDDItem, item_id, ITYPE_LIST);
	
	//Update Table
	FDI_PARAM_SPECIFIER ParamSpec;
	ParamSpec.eType = FDI_PS_ITEM_ID;	// First get RelationsAndUpdateInfo for the entire list
	ParamSpec.id = item_id;

	DESC_REF DescRef;
	DescRef.id = item_id;
	DescRef.type = ITYPE_LIST;

	GetRecursiveRelations( pDDItem, iBlockIndex, &ParamSpec, &DescRef);

	/*else
	{
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"List Flat information not complete...returned with error code %d", r_code );
	}*/
	

	return r_code;
} 

int CDDSXMLBuilder::AmsHartDDS_get_plugin_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_PLUGIN);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_PLUGIN::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
		
	ASSERT( (r_code == 0) || (r_code == -1517) );

	
	if (!m_bLoadXML)
	{
		return r_code;
	}

	r_code = 0;

	FLAT_PLUGIN * flat_plugin = dynamic_cast< FLAT_PLUGIN * >(generic_item.item);
																									
	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_PLUGIN Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_PLUGIN *flat_pointer1 = new FLAT_PLUGIN(*flat_plugin);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_PLUGIN flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_PLUGIN *flat_pointer2 = new FLAT_PLUGIN(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	//Dump Item_information if available 
	if (flat_plugin->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_plugin->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_plugin->symbol_name.c_str() );

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;			// MHD ??? Just do it like we used to.
	if( flat_plugin->label.length() )
	{
		pAttribute = m_pDOMDoc->createElement( ATTRNAME_LABEL );
		CString tempLabel =  StripOffNPChars( (TCHAR*)flat_plugin->label.c_str() );
		pText = m_pDOMDoc->createTextNode( (LPCTSTR)tempLabel );
		pAttribute->appendChild( pText );
		pDDItemAttributes->appendChild( pAttribute );
	}
				
	if( flat_plugin->help.length() )
	{
		CString tempHelp =  StripOffNPChars( (TCHAR*)flat_plugin->help.c_str() );
		pAttribute = m_pDOMDoc->createElement( ATTRNAME_HELP );
		pText = m_pDOMDoc->createTextNode( (LPCTSTR)tempHelp );
		pAttribute->appendChild( pText );
		pDDItemAttributes->appendChild( pAttribute );
	}
		
	pAttribute = m_pDOMDoc->createElement( "UUID" );
	wchar_t szGuid[40] = {0};
	::StringFromGUID2(flat_plugin->uuid, szGuid, 40);
	_bstr_t tempUUID = szGuid;
	pText = m_pDOMDoc->createTextNode( tempUUID );
	pAttribute->appendChild( pText );
	pDDItemAttributes->appendChild( pAttribute );

	//VALIDITY attribute
	if (flat_plugin->valid)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
	}

	//VISIBILITY attribute
	if (flat_plugin->visibility)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VISIBILITY, (LPCTSTR)_T("False") );
	}

	pDDItem->appendChild( pDDItemAttributes );


	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_template_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_TEMPLATE);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, item_id, 0};
	AttributeNameSet attribute_list = FLAT_TEMPLATE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
		
	ASSERT( (r_code == 0) || (r_code == -1517) );

	

	r_code = 0;

	FLAT_TEMPLATE * flat_template = dynamic_cast< FLAT_TEMPLATE * >(generic_item.item);

	Add_DEFAULT_VALUES_LIST(&flat_template->default_values);

	if (!m_bLoadXML)
	{
		return r_code;
	}
																										
	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_TEMPLATE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_TEMPLATE *flat_pointer1 = new FLAT_TEMPLATE(*flat_template);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_TEMPLATE flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_TEMPLATE *flat_pointer2 = new FLAT_TEMPLATE(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if(generic_item.errors.count > 0){
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement( ELMTNAME_DD_ITEM_ATTR_LIST );

	//Dump Item_information if available 
	if (flat_template->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_template->item_info);
	}

	pDDItemAttributes->setAttribute( ATTRNAME_NAME, flat_template->symbol_name.c_str() );

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;	// MHD ??? Just do it like we used to.
	if( flat_template->label.length() )
	{
		pAttribute = m_pDOMDoc->createElement( ATTRNAME_LABEL );
		CString tempLabel =  StripOffNPChars( (TCHAR*)flat_template->label.c_str() );
		pText = m_pDOMDoc->createTextNode( (LPCTSTR)tempLabel );
		pAttribute->appendChild( pText );
		pDDItemAttributes->appendChild( pAttribute );
	}
				
	if( flat_template->help.length() )
	{
		CString tempHelp =  StripOffNPChars( (TCHAR*)flat_template->help.c_str() );
		pAttribute = m_pDOMDoc->createElement( ATTRNAME_HELP );
		pText = m_pDOMDoc->createTextNode( (LPCTSTR)tempHelp );
		pAttribute->appendChild( pText );
		pDDItemAttributes->appendChild( pAttribute );
	}

	//VALIDITY attribute
	if (flat_template->valid)
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("True") );
	}
	else
	{
		pDDItemAttributes->setAttribute( ATTRNAME_VALIDITY, (LPCTSTR)_T("False") );
	}

	pDDItem->appendChild( pDDItemAttributes );
		
	//default_values
	for ( int childIndex = 0; childIndex < flat_template->default_values.count; childIndex ++ )
	{
		r_code = FDIHartDDS_AddChildDefaultValues(pDDItem, &flat_template->default_values.list[childIndex]);
	}
	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_component_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem)
{
	FDI_GENERIC_ITEM generic_item(ITYPE_COMPONENT);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = { FDI_ITEM_ID, item_id, 0 };
	AttributeNameSet attribute_list = FLAT_COMPONENT::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.

	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT((r_code == 0) || (r_code == -1517));

	r_code = 0;

	FLAT_COMPONENT * flat_component = dynamic_cast< FLAT_COMPONENT * >(generic_item.item);

	Add_DESC_REF(&flat_component->check_configuration);
	Add_DESC_REF(&flat_component->component_connect_point);
	Add_DESC_REF(&flat_component->component_parent);
	Add_DESC_REF(&flat_component->detect);
	Add_DESC_REF(&flat_component->scan);
	Add_DESC_REF(&flat_component->scan_list);

	Add_OP_REF_TRAIL_LIST(&flat_component->component_relations);
	Add_DEFAULT_VALUES_LIST(&flat_component->initial_values);

	if (!m_bLoadXML)
	{
		return r_code;
	}

	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_TEMPLATE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_COMPONENT *flat_pointer1 = new FLAT_COMPONENT(*flat_component);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_COMPONENT flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_COMPONENT *flat_pointer2 = new FLAT_COMPONENT(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if (generic_item.errors.count > 0) {
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement(ELMTNAME_DD_ITEM_ATTR_LIST);

	//Dump Item_information if available 
	if (flat_component->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_component->item_info);
	}

	pDDItemAttributes->setAttribute(ATTRNAME_NAME, flat_component->symbol_name.c_str());

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;	// MHD ??? Just do it like we used to.
	if (flat_component->label.length())
	{
		CString temp_label = StripOffNPChars((TCHAR*)flat_component->label.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_LABEL, (LPCTSTR)temp_label);
	}

	if (flat_component->help.length())
	{
		CString tempHelp = StripOffNPChars((TCHAR*)flat_component->help.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_HELP, (LPCTSTR)tempHelp);
	}

	pDDItem->appendChild(pDDItemAttributes);


	MSXML2::IXMLDOMElementPtr pChildItem;
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_CAN_DELETE);
	//CAN_DELETE attribute
	if (flat_component->can_delete)
	{
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)_T("True"));
	}
	else
	{
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)_T("False"));
	}
	pDDItem->appendChild(pChildItem);


	//CHECK_CONFIGURATION attribute
	if (flat_component->check_configuration.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"CheckConfiguration", &(flat_component->check_configuration));
	}
	//CLASSIFICATION attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_CLASSIFICATION);
	CString temp_classification;
	FDIHartDDS_convert_classification_to_name(flat_component->classification, temp_classification);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_classification);
	pDDItem->appendChild(pChildItem);

	//BYTE_ORDER attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_BYTE_ORDER);
	CString temp_ByteOrder;
	FDIHartDDS_convert_ByteOrder_to_name(flat_component->component_byte_order, temp_ByteOrder);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_ByteOrder);
	pDDItem->appendChild(pChildItem);

	//CONNECTION_POINT attribute
	if (flat_component->component_connect_point.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"ConnectionPoint", &(flat_component->component_connect_point));
	}

	//COMPONENT_PARENT attribute
	if (flat_component->component_parent.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"ComponentParent", &(flat_component->component_parent));
	}

	// COMPONENT_PATH attribute
	if (flat_component->component_path.length())
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_COMPONENT_PATH);
		CString temp_path = StripOffNPChars((TCHAR*)flat_component->component_path.c_str());
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_path);
		pDDItem->appendChild(pChildItem);
	}

	//COMPONENT_RELATIONS attribute
	for (int childIndex = 0; childIndex < flat_component->component_relations.count; childIndex++)
	{
		FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_COMPONENT_RELATION_ITEM, &(flat_component->component_relations.list[childIndex]));
	}

	//DECLARATION attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_DECLARATION);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component->declaration);
	pDDItem->appendChild(pChildItem);

	//DETECT attribute
	if (flat_component->detect.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"Detect", &(flat_component->detect));
	}

	// EDD attribute
	if (flat_component->edd.length())
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_EDD);
		CString temp_edd = StripOffNPChars((TCHAR*)flat_component->edd.c_str());
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_edd);
		pDDItem->appendChild(pChildItem);
	}


	// PRODUCT_URI attribute
	if (flat_component->product_uri.length())
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_PRODUCT_URI);
		CString temp_product_uri = StripOffNPChars((TCHAR*)flat_component->product_uri.c_str());
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_product_uri);
		pDDItem->appendChild(pChildItem);
	}

	//INITIAL_VALUES attribute
	if (flat_component->initial_values.count > 0)
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_INITIAL_VALUES);
		for (int childIndex = 0; childIndex < flat_component->initial_values.count; childIndex++)
		{
			r_code = FDIHartDDS_AddChildComponentInitialValues(pChildItem, &flat_component->initial_values.list[childIndex]);
		}
		pDDItem->appendChild(pChildItem);
	}

	//PROTOCOL attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_PROTOCOL);
	CString temp_protocol;
	FDIHartDDS_convert_Protocol_to_name(flat_component->protocol, temp_protocol);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_protocol);
	pDDItem->appendChild(pChildItem);


	//REDUNDANCY attribute
	if (flat_component->redundancy.eType != EXPR::EXPR_TYPE_NONE)
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_REDUNDANCY);
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)ReturnEXPRAsString(&flat_component->redundancy));
		pDDItem->appendChild(pChildItem);
	}

	//SCAN attribute
	if (flat_component->scan.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"Scan", &(flat_component->scan));
	}

	//SCAN_LIST attribute
	if (flat_component->scan_list.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"ScanList", &(flat_component->scan_list));
	}

	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_component_folder_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem)
{
	FDI_GENERIC_ITEM generic_item(ITYPE_COMPONENT_FOLDER);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = { FDI_ITEM_ID, item_id, 0 };
	AttributeNameSet attribute_list = FLAT_COMPONENT_FOLDER::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT((r_code == 0) || (r_code == -1517));

	r_code = 0;

	FLAT_COMPONENT_FOLDER * flat_component_folder = dynamic_cast< FLAT_COMPONENT_FOLDER * >(generic_item.item);


	Add_DESC_REF(&flat_component_folder->component_parent);


	if (!m_bLoadXML)
	{
		return r_code;
	}

	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_TEMPLATE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_COMPONENT_FOLDER *flat_pointer1 = new FLAT_COMPONENT_FOLDER(*flat_component_folder);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_COMPONENT_FOLDER flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_COMPONENT_FOLDER *flat_pointer2 = new FLAT_COMPONENT_FOLDER(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if (generic_item.errors.count > 0) {
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement(ELMTNAME_DD_ITEM_ATTR_LIST);

	//Dump Item_information if available 
	if (flat_component_folder->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_component_folder->item_info);
	}

	pDDItemAttributes->setAttribute(ATTRNAME_NAME, flat_component_folder->symbol_name.c_str());

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;	// MHD ??? Just do it like we used to.
	if (flat_component_folder->label.length())
	{
		CString temp_label = StripOffNPChars((TCHAR*)flat_component_folder->label.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_LABEL, (LPCTSTR)temp_label);
	}

	if (flat_component_folder->help.length())
	{
		CString tempHelp = StripOffNPChars((TCHAR*)flat_component_folder->help.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_HELP, (LPCTSTR)tempHelp);
	}
	
	pDDItem->appendChild(pDDItemAttributes);

	MSXML2::IXMLDOMElementPtr pChildItem;
	//CLASSIFICATION attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_CLASSIFICATION);
	CString temp_classification;
	FDIHartDDS_convert_classification_to_name(flat_component_folder->classification, temp_classification);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_classification);
	pDDItem->appendChild(pChildItem);

	//COMPONENT_PARENT attribute
	if (flat_component_folder->component_parent.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"ComponentParent", &(flat_component_folder->component_parent));
	}

	// COMPONENT_PATH attribute
	if (flat_component_folder->component_path.length())
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_COMPONENT_PATH);
		CString temp_path = StripOffNPChars((TCHAR*)flat_component_folder->component_path.c_str());
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_path);
		pDDItem->appendChild(pChildItem);
	}
	//PROTOCOL attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_PROTOCOL);
	CString temp_protocol;
	FDIHartDDS_convert_Protocol_to_name(flat_component_folder->protocol, temp_protocol);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_protocol);
	pDDItem->appendChild(pChildItem);

	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_component_reference_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem)
{
	FDI_GENERIC_ITEM generic_item(ITYPE_COMPONENT_REFERENCE);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = { FDI_ITEM_ID, item_id, 0 };
	AttributeNameSet attribute_list = FLAT_COMPONENT_REFERENCE::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT((r_code == 0) || (r_code == -1517));

	r_code = 0;

	FLAT_COMPONENT_REFERENCE * flat_component_reference = dynamic_cast< FLAT_COMPONENT_REFERENCE * >(generic_item.item);


	Add_DESC_REF(&flat_component_reference->component_parent);


	if (!m_bLoadXML)
	{
		return r_code;
	}

	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_TEMPLATE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_COMPONENT_REFERENCE *flat_pointer1 = new FLAT_COMPONENT_REFERENCE(*flat_component_reference);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_COMPONENT_REFERENCE flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_COMPONENT_REFERENCE *flat_pointer2 = new FLAT_COMPONENT_REFERENCE(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if (generic_item.errors.count > 0) {
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement(ELMTNAME_DD_ITEM_ATTR_LIST);

	//Dump Item_information if available 
	if (flat_component_reference->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_component_reference->item_info);
	}

	pDDItemAttributes->setAttribute(ATTRNAME_NAME, flat_component_reference->symbol_name.c_str());

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;	// MHD ??? Just do it like we used to.

	MSXML2::IXMLDOMElementPtr pChildItem;
	//CLASSIFICATION attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_CLASSIFICATION);
	CString temp_classification;
	FDIHartDDS_convert_classification_to_name(flat_component_reference->classification, temp_classification);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_classification);
	pDDItem->appendChild(pChildItem);

	//COMPONENT_PARENT attribute
	if (flat_component_reference->component_parent.id != 0)
	{
		FDIHartDDS_AddChildDescRef(pDDItem, L"ComponentParent", &(flat_component_reference->component_parent));
	}

	// COMPONENT_PATH attribute
	if (flat_component_reference->component_path.length())
	{
		pChildItem = m_pDOMDoc->createElement(ATTRNAME_COMPONENT_PATH);
		CString temp_path = StripOffNPChars((TCHAR*)flat_component_reference->component_path.c_str());
		pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_path);
		pDDItem->appendChild(pChildItem);
	}
	//PROTOCOL attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_PROTOCOL);
	CString temp_protocol;
	FDIHartDDS_convert_Protocol_to_name(flat_component_reference->protocol, temp_protocol);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_protocol);
	pDDItem->appendChild(pChildItem);

	// MANUFACTURER attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_MANUFACTURER);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component_reference->manufacturer);
	pDDItem->appendChild(pChildItem);

	// DEVICE_TYPE attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_DEVICE_TYP);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component_reference->device_type);
	pDDItem->appendChild(pChildItem);

	// DEVICE_REVISION attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_DEVICE_REV);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component_reference->device_revision);
	pDDItem->appendChild(pChildItem);

	return r_code;
}

int CDDSXMLBuilder::AmsHartDDS_get_component_relation_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem)
{
	FDI_GENERIC_ITEM generic_item(ITYPE_COMPONENT_RELATION);
	CString					temp_item;
	FDI_ITEM_SPECIFIER item_spec = { FDI_ITEM_ID, item_id, 0 };
	AttributeNameSet attribute_list = FLAT_COMPONENT_RELATION::All_Attrs;
	attribute_list = attribute_list | item_information; // Add item_information attribute to get the debug information.
	int r_code = m_EDDEngine->GetItem(iBlockIndex, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

	ASSERT((r_code == 0) || (r_code == -1517));

	r_code = 0;

	FLAT_COMPONENT_RELATION * flat_component_relation = dynamic_cast< FLAT_COMPONENT_RELATION * >(generic_item.item);

	if (!m_bLoadXML)
	{
		return r_code;
	}

	//Flat test Suite
	static int flatTestCounter = 0;
	if (flatTestCounter++ % FLAT_TEST_INTERVAL == 0)	// post increment so the first instance gets tested.
	{
#ifdef _OUTPUT_TEST_SUITE_
		fprintf(stdout, "FLAT_TEMPLATE Test Suite Item#%d", item_spec.item.id);
		fflush(stdout);
#endif
		// Testing Copy Constructor from a heap variable
		FLAT_COMPONENT_RELATION *flat_pointer1 = new FLAT_COMPONENT_RELATION(*flat_component_relation);
		//Testing assignment operator for FLAT_VAR into a local
		FLAT_COMPONENT_RELATION flat_local;
		flat_local = *flat_pointer1;
		//Testing copy constructor from a local variable
		FLAT_COMPONENT_RELATION *flat_pointer2 = new FLAT_COMPONENT_RELATION(flat_local);
		//Testing assignment operator into pointer
		*flat_pointer2 = *flat_pointer1;
		// Testing assignment operator for same (no action taken nothing gets messed up)
		flat_local = flat_local;

		// Testing deletes (and do cleanup)
		delete flat_pointer1;
		delete flat_pointer2;
	}

	if (generic_item.errors.count > 0) {
		AddReturnList(pDDItem, &generic_item);
	}

	MSXML2::IXMLDOMElementPtr pDDItemAttributes = m_pDOMDoc->createElement(ELMTNAME_DD_ITEM_ATTR_LIST);

	//Dump Item_information if available 
	if (flat_component_relation->item_info.line_number != 0)
	{
		AddItemInformation(pDDItem, &flat_component_relation->item_info);
	}

	pDDItemAttributes->setAttribute(ATTRNAME_NAME, flat_component_relation->symbol_name.c_str());

	MSXML2::IXMLDOMElementPtr pAttribute;	// MHD ??? Just do it like we used to.
	MSXML2::IXMLDOMTextPtr pText;	// MHD ??? Just do it like we used to.
	if (flat_component_relation->label.length())
	{
		CString temp_label = StripOffNPChars((TCHAR*)flat_component_relation->label.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_LABEL, (LPCTSTR)temp_label);
	}

	if (flat_component_relation->help.length())
	{
		CString tempHelp = StripOffNPChars((TCHAR*)flat_component_relation->help.c_str());
		pDDItemAttributes->setAttribute(ATTRNAME_HELP, (LPCTSTR)tempHelp);
	}
	
	pDDItem->appendChild(pDDItemAttributes);

	MSXML2::IXMLDOMElementPtr pChildItem;
	//MAXIMUM_NUMBER attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_MAXIMUM_NUMBER);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component_relation->maximum_number);
	pDDItem->appendChild(pChildItem);

	//MINIMUM_NUMBER attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_MINIMUM_NUMBER);
	pChildItem->setAttribute(ATTRNAME_VALUE, flat_component_relation->minimum_number);
	pDDItem->appendChild(pChildItem);

	//RELATION_TYPE attribute
	pChildItem = m_pDOMDoc->createElement(ATTRNAME_RELATION_TYPE);
	CString temp_relation_type;
	FDIHartDDS_convert_RelationType_to_name(flat_component_relation->relation_type, temp_relation_type);
	pChildItem->setAttribute(ATTRNAME_VALUE, (LPCTSTR)temp_relation_type);
	pDDItem->appendChild(pChildItem);

	
	for (int childIndex = 0; childIndex < flat_component_relation->addressing.count; childIndex++)
	{
		FDIHartDDS_AddChildOpRefTrail(pDDItem, ELMTNAME_ADDRESSING_ITEM, &(flat_component_relation->addressing.list[childIndex]));
	}

	//COMPONENTS attribute
	for (int childIndex = 0; childIndex < flat_component_relation->components.count; childIndex++)
	{
		FDIHartDDS_AddChildComponents(pDDItem, ELMTNAME_ADDRESSING_ITEM, &(flat_component_relation->components.list[childIndex]));
	}



	return r_code;
}

//*****************************************************************************************//
//
//   Name: CreateXMLFrom
//	 
//	 Description: 
//		This function creates the XML data of the specified
//		DD using an IDOM document. The information in the XML 
//		data is from the header, the DDOD object data and the
//		DDOD static objects elements of the DDOD Binary file.
//		This XML data will later be output to a file to be parsed
//		to get certain parameters for DDInstall.
//		
//	 Inputs:
//		sFilename - The FM6 file that we want to create our symbol file
//					from.
//
//	 Outputs:
//		DataAsXml - All of the block and device info in an xml string.
//
//	 Returns:
//		bSuccessful: TRUE
//
//	 Author:
//		Katie Frost
//*****************************************************************************************//
BOOL CDDSXMLBuilder::CreateXMLFrom( CString sFilename, BSTR* DataAsXml )
{

	FLAT_DEVICE_DIR		*flat_device_dir = NULL;
	int					r_code = 0;
    wchar_t             configXML[ CONFIG_XML_MAX ]; // arbitrary max string length
	_CrtMemState s1, s2, s3;
	(void)s1;
	(void)s2;

    // Initialize to null so we can handle error conditions correctly
    *DataAsXml = NULL;

	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	m_szFilename = sFilename;
	
	USES_CONVERSION;
	
    if (!ReadConfigXML(configXML, CONFIG_XML_MAX))
    {
        ASSERT(0);
    }

	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	CParamCache* pParamCache = new CParamCache();

	CEDDEngineLogger* pEDDEngineLogger = new CEDDEngineLogger();
	//nsConsumer::IEDDEngineLogger* m_IEDDEngineLogger = (nsConsumer::IEDDEngineLogger*)pEDDEngineLogger;

	nsConsumer::IParamCache* m_IParamCache = (nsConsumer::IParamCache*)pParamCache;
	nsConsumer::IEDDEngineLogger* m_IEDDEngineLogger = (nsConsumer::IEDDEngineLogger*)pEDDEngineLogger;

	nsEDDEngine::EDDEngineFactoryError eError = nsEDDEngine::EDDE_SUCCESS;
	pEDDEngineLogger->OpenLogFile();
	m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, m_IParamCache, NULL, m_IEDDEngineLogger, configXML, &eError);

	
	wchar_t *wstr_error = L""; 

	if (m_EDDEngine)
    {
	    pParamCache->AssignIConvenience( m_EDDEngine );
		pParamCache->AssignItemInfo( m_EDDEngine );
	    r_code = m_EDDEngine->GetDeviceDir(&flat_device_dir);

    }
	else
	{
		wstr_error = EDDEngineFactory::GetEDDEngineErrorString(eError);
	}


	//if( r_code == DDS_SUCCESS)
	if( (r_code == 0) && (flat_device_dir!=NULL))
	{	
		// Create XML Document
		try
		{
			HRESULT hr = m_pDOMDoc.CreateInstance(__uuidof(MSXML2::DOMDocument60));
			if(FAILED(hr))
			{
			//	r_code = DDS_ERROR_END;
			}
			else
			{
				m_pDOMDoc->async = false;
				m_pDOMDoc->setProperty( "MaxXMLSize", 0 );

				//Processing Instruction
				MSXML2::IXMLDOMProcessingInstructionPtr pProcInstr;
				pProcInstr = m_pDOMDoc->createProcessingInstruction(_T("xml"),_T("version='1.0'"));
				m_pDOMDoc->appendChild(pProcInstr);

				//Top most Element of AMSHARTDDS xml schema - DDOD_BINARY	
				MSXML2::IXMLDOMElementPtr pDDODBinElmt;
				pDDODBinElmt = m_pDOMDoc->createElement(ELMTNAME_DDOD_BINARY);
				

				//DDODHeader Element
				BuildHeader( pDDODBinElmt );
				
				//Add the DDItemList
				MSXML2::IXMLDOMElementPtr pDDODSODElmt;
				pDDODSODElmt = m_pDOMDoc->createElement( ELMTNAME_DDOD_STATIC_OBJ );

				CString strFilename;
				strFilename.Format(L"--- Processing %s ---\n", wstr_name);

				::OutputDebugString(strFilename);

				wchar_t ext[_MAX_EXT] =  {0};

				_wsplitpath_s(wstr_name, NULL, 0, NULL, 0, NULL, 0, ext, _MAX_EXT);

				// force extension to lower case in case an upper case character
				// gets into the file extension
				for (int i = 0 ; i < (int)wcslen(ext) ; i ++)
				{
					ext[i] = towlower(ext[i]);
				}
				
				if (	(wcscmp(ext, L".ff6") == 0)			// FF FDI format 
					||	(wcscmp(ext, L".ffo") == 0)			// FF Legacy
					||	(wcscmp(ext, L".ff5") == 0)			// FF Legacy
					||  (wcscmp(ext, L".is6") == 0)	)		// ISA 100
				{
					m_Protocol = FF;
					r_code = CreateBlockIndexes( flat_device_dir );
					if( r_code == 0 )
					{
						r_code = GetDDListByBlocks( flat_device_dir, pDDODSODElmt );
					}
				}
				else												// All others
				{
					if( wcscmp(ext, L".bin")== 0 )
					{
						m_Protocol = PROFIBUS;
					}
					r_code = AmsHartDDS_get_dd_list(flat_device_dir, pDDODSODElmt);
				}

				//if( r_code == DDS_SUCCESS)
				if( r_code == 0)
				{
					pDDODBinElmt->appendChild( pDDODSODElmt );

					//Add the string table
					AmsHartDDS_get_string_table( pDDODBinElmt );

					m_pDOMDoc->appendChild( pDDODBinElmt );

					//Turn data into XML format
					bstr_DataAsXml = (BSTR)m_pDOMDoc->Getxml();
					*DataAsXml = ::SysAllocString(bstr_DataAsXml);
				}
				else
				{
				//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"DD failed to get flat information for items with an error code of %d", r_code );
				}	
			}
		}//End try
		catch( ... )
		{
		//	r_code = DDS_ERROR_END;
		}
	}
	else
	{
	//	BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE, L"File could not be loaded...returned from AmsHartDDS_get_loaded_dd() with an error code of %d", r_code );
	}

    if(*DataAsXml == NULL)
	{
		BSTR st1 = L"<?xml version=\"1.0\"?><Error>";
		BSTR st2 = L" No Edd Engine Created </Error>";

		BSTR st3 = ::SysAllocStringLen(L"",100);
		
		wcscpy(st3, st1);
		wcscat(st3, wstr_error);	
		wcscat(st3, st2);	

		*DataAsXml = ::SysAllocString(st3);
	}
	//clean up the device information regardless of the scenario

	//KJ measure memory usage around the destructor of m_EDDEngine

	_CrtMemCheckpoint( &s1 );

	delete m_EDDEngine;

	_CrtMemCheckpoint(&s2);

	_CrtMemDifference( &s3, &s2, &s1 );
	m_iDeviceInstanceMemoryUsage = s3.lSizes[_NORMAL_BLOCK];	
	pEDDEngineLogger->CloseLogFile();
	delete pParamCache;
	delete pEDDEngineLogger;
	return r_code;
}

 



//*****************************************************************************************//
//
//   Name: ReadConfigXML
//	 
//	 Description: 
//		This function creates the configuration XML string for passing
//      on to the edd engine.  It gets it from the EddEngineConfig.xml
//      file in c:\ProgramData\FDI  or C:\Documents and Settings\All Users\Application Data\FDI
//      depending on the OS.
//		
//	 Inputs:
//		ConfigXML - The configuration as an xml string.
//                - empty string on failure
//      ConfigXMLLen - max length of the configuration string.
//
//
//	 Returns:
//      true if it worked or false on an error opening the file.
//      This function in no way warrants that the XML in the file
//      is properly formed.  That is handled in the EDD Engine Code.
//
//	 Author:
//		Mark Sandmann
//*****************************************************************************************//
 
int CDDSXMLBuilder::ReadConfigXML(wchar_t* configXML, int configXMLLen)
{
	static CCriticalSection st_CritSec;

	CSingleLock singleLock(&st_CritSec, TRUE);

    int returnValue = false;    // default to failed

    // String buffer for holding the path.
    static TCHAR strPath[ MAX_PATH ];

    int configXMLIndex = 0;
    configXML[0] = 0;   // terminate configuration

    // Get the special folder path from the OS Environment Variable
    // Windows 7 = c:\Program Data
    // Windows XP = C:\Documents and Settings\user\Application Data
    SHGetSpecialFolderPath( 0,       // Hwnd
                            strPath, // String buffer.
                            CSIDL_COMMON_APPDATA, // CSLID of folder
                            FALSE ); // Create if doesn't exists?

    // Add in the \FDI\EddEngineConfig.xml part of the path.
    wcscat(strPath, L"\\FDI\\EddEngineConfig.xml");

    char tempChar;
    try
    {
		std::ifstream myfile (strPath, std::ios_base::in, _SH_DENYNO);

        if (myfile.is_open())
        {
            returnValue = true;
            while ( myfile.good() && configXMLIndex < configXMLLen)
            {
                // Get the next character
                tempChar = (char)myfile.get();
                // ignore CR/LF/EOF characters
                if(tempChar != '\n' && tempChar != '\r' && tempChar != '\377')
                    configXML[configXMLIndex++] = tempChar;
            }
        }
		else
		{
			TRACE(L"CDDSXMLBuilder::ReadConfigXML std::ifstream failed with error\n");
		}
    }
	catch (std::ios_base::failure& e)
	{
		(void)e;
		TRACE(L"CDDSXMLBuilder::ReadConfigXML std::ifstream failed with error %s\n",e.what());
	}

    // terminate config string.
    configXML[configXMLIndex++] = 0;
 
    return returnValue;
}


//*****************************************************************************************//
//
//   Name: BuildHeader
//	 
//	 Description: 
//		This function gets the header info from the 
//		specified DD. The parameters that are wanted 
//      from the DD are attaches the XML formatted params
//      to our document using IDOM.
//
//	 Inputs:
//		IXMLDOMDocument2Ptr pDOMDoc
//		IXMLDOMElementPtr pDOMDDODBinElmt
//
//	 Returns:
//		FALSE
//
//	 Author:
//		Katie Frost
//*****************************************************************************************//


BOOL CDDSXMLBuilder::BuildHeader( MSXML2::IXMLDOMElementPtr pDDODBinElmt )
{
	BOOL			 bRetVal = FALSE;
	EDDFileHeader header = {0};
	
	m_EDDEngine->GetEDDFileHeader(&header);

	//Header Element
	MSXML2::IXMLDOMElementPtr pHeaderElmt;
	pHeaderElmt = m_pDOMDoc->createElement( ELMTNAME_HEADER_INFO );
	pHeaderElmt->setAttribute( ATTRNAME_MJR_REV, header.major_rev);
	pHeaderElmt->setAttribute( ATTRNAME_MNR_REV, header.minor_rev);
	
	CString temp_edd_profile;

	FDIHartDDS_convert_Edd_Profile(header.edd_profile, temp_edd_profile);
	pHeaderElmt->setAttribute(ATTRNAME_EDD_PROFILE, (LPCTSTR)temp_edd_profile);

	pHeaderElmt->setAttribute(ATTRNAME_TOOL_RELEASE, header.tool_release);

	CString temp_layout_type;

	FDIHartDDS_convert_layout_type(header.layout, temp_layout_type);
	pHeaderElmt->setAttribute(ATTRNAME_LAYOUT_TYPE, (LPCTSTR)temp_layout_type);

	if (wcslen(header.manufacturer_ext))
	{
		pHeaderElmt->setAttribute(ATTRNAME_MANUFACTURER_EXT, header.manufacturer_ext);
	}

	if (wcslen(header.device_type_ext))
	{
		pHeaderElmt->setAttribute(ATTRNAME_DEVICE_TYP_EXT, header.device_type_ext);
	}

	//Header Element's Child Element - DeviceIdInfo
	MSXML2::IXMLDOMElementPtr pDeviceIdInfo;
	pDeviceIdInfo = m_pDOMDoc->createElement( ELMTNAME_DEVICE_ID );
	
	pDeviceIdInfo->setAttribute( ATTRNAME_MANUFACTURER, header.device_id.manufacturer);
	pDeviceIdInfo->setAttribute( ATTRNAME_DEVICE_TYP, header.device_id.device_type );
	pDeviceIdInfo->setAttribute( ATTRNAME_DEVICE_REV, header.device_id.device_revision );
	pDeviceIdInfo->setAttribute( ATTRNAME_DD_REV, header.device_id.dd_revision );

	//Append child element of header to the header element
	pHeaderElmt->appendChild( pDeviceIdInfo );
	//Append child element of DDOD binary to the DDODBinary element
	pDDODBinElmt->appendChild( pHeaderElmt );
	
	return bRetVal;
}



//************************************************************************************************//
//
//	Name: AddItem()
//
//	Description: This fuction is called in every get_""_item function and its prupose is to
//				 add the item, by item id, to the list of dd items in the xml. It writes the
//				 item id, item kind, and item kind as string to the item element. The other 
//				 attributes of the item are contained in another element that is added to this one
//				 in each of their given functions.
//
//	Inputs:
//		ItemID - the item id of the item type being manipulated
//		ItemKind - the actual item type ( in numerical format )
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItemList - the element created with the information and appended
//												to the list of item elements.
//
//	Returns:
//		pDDItem - to be used in added the element's many other attributes to the item element
//
//	Author:
//		Katie Frost
//
//***********************************************************************************************//

MSXML2::IXMLDOMElementPtr CDDSXMLBuilder::AddItem( MSXML2::IXMLDOMElementPtr pDDItemList, 
																	unsigned long lItemID, ITEM_TYPE item_type )
{
	MSXML2::IXMLDOMElementPtr pDDItem;

	if (m_bLoadXML)
	{
		pDDItem = m_pDOMDoc->createElement( ELMTNAME_DD_ITEMS );

		CString strTemp;
		FormatItemIdAndName( lItemID, strTemp);
		pDDItem->setAttribute( ATTRNAME_DD_ITEM_ID, (LPCTSTR)strTemp );

		AmsHartDDS_convert_item_type( item_type, strTemp );
		pDDItem->setAttribute( ATTRNAME_TYPE, (LPCTSTR)strTemp  );

		pDDItemList->appendChild( pDDItem );
	}
	return pDDItem;
}

//************************************************************************************************//
//
//	Name: AddItemWithName()
//
//	Description: This function is called instead of AddItem when in addition to the id, and type
//  name is desired.
//
//	Inputs:
//		wName - the name of the Element to create
//		ItemID - the item id of the item type being manipulated
//
//	Outputs:
//		MSXML2::IXMLDOMElementPtr pDDItemList - the element created with the information and appended
//												to the list of item elements.
//
//	Returns:
//
//	Author:
//		Mark Sandmann from a function by Katie Frost
//
//***********************************************************************************************//

int CDDSXMLBuilder::AddItemWithName( MSXML2::IXMLDOMElementPtr pDDItemList, wchar_t *wName, 
																	ITEM_ID ItemId )
{
	MSXML2::IXMLDOMElementPtr pDDItem = m_pDOMDoc->createElement( wName );

	CString temp_item;
	FormatItemIdAndName( ItemId, temp_item);
	pDDItem->setAttribute( ATTRNAME_DD_ITEM_ID, (LPCTSTR)temp_item );

	pDDItemList->appendChild( pDDItem );
	return 0;
}

void CDDSXMLBuilder::AddCommandListForItem(MSXML2::IXMLDOMElementPtr pDDItem, ITEM_ID itemId, ITEM_TYPE type)
{
	if ((m_Protocol != FF) && (m_Protocol != ISA100))
	{
		FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, itemId, 0};
		MSXML2::IXMLDOMElementPtr pCommandList;
		pCommandList = m_pDOMDoc->createElement(ELMTNAME_COMMAND_LIST);	
	
		if( type == ITYPE_ARRAY )
		{
			// First get command list for whole Array
			FDI_PARAM_SPECIFIER paramSpec;
					
			paramSpec.eType = FDI_PS_ITEM_ID;
			paramSpec.id = itemId;
			paramSpec.subindex = 0;

			DoCommandExecution(pCommandList, &paramSpec);

			FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
			AttributeNameSet attribute_list = FLAT_ARRAY::All_Attrs;
			ITEM_TYPE array_type;
			int r_code = m_EDDEngine->GetItem( 0, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);
			if( r_code == 0 )
			{
				FLAT_ARRAY* arrayItem = dynamic_cast<FLAT_ARRAY*>(generic_item.item);
		
				item_spec.item.id = arrayItem->type;

				m_EDDEngine->GetItemType(0, &item_spec, &array_type);
				if( array_type == ITYPE_VARIABLE )
				{
					for( ulong index = 0; index < arrayItem->num_of_elements; index++ )
					{
						paramSpec.eType = FDI_PS_ITEM_ID_SI;
						paramSpec.id = itemId;
						paramSpec.subindex = index;

						DoCommandExecution(pCommandList, &paramSpec);
					}
				}
				else if ( array_type == ITYPE_COLLECTION )
				{
					FDI_GENERIC_ITEM generic_item1(ITYPE_COLLECTION);
					AttributeNameSet attribute_list1 = FLAT_COLLECTION::All_Attrs;
					FDI_PARAM_SPECIFIER paramSpec1;
					 //array, subindex, collection member
				
					int r_code1 = m_EDDEngine->GetItem( 0, 0, &item_spec, &attribute_list1, TEST_LANGUAGE, &generic_item1);
					if( r_code1 == 0 )
					{
						FLAT_COLLECTION* collectionItem = dynamic_cast<FLAT_COLLECTION*>(generic_item1.item);
						for( ulong index = 0; index < arrayItem->num_of_elements; index++ )
						{
							for( int i = 0; i < collectionItem->op_members.count; i++ )
							{
								if( collectionItem->op_members.list[i].ref.op_ref_type == STANDARD_TYPE )//variable, record, simple list or array
								{
									if( collectionItem->op_members.list[i].ref.op_info.type == ITYPE_VARIABLE)
									{
										paramSpec1.RefList.count = 2;
										paramSpec1.RefList.list = new nsEDDEngine::OP_REF_INFO[paramSpec1.RefList.count];
										paramSpec1.eType = FDI_PS_ITEM_ID_COMPLEX;
										paramSpec1.RefList.list[0].id = itemId;
										paramSpec1.RefList.list[0].member = index;
										paramSpec1.RefList.list[0].type = type;
										paramSpec1.RefList.list[1].id = 0;
										paramSpec1.RefList.list[1].member = collectionItem->op_members.list[i].name;;
										paramSpec1.RefList.list[1].type = array_type;
									
										DoCommandExecution(pCommandList, &paramSpec1);
										delete [] paramSpec1.RefList.list;
										paramSpec1.RefList.list = nullptr;
									}
									else
									{
										int arrayListCount = 0;
										if( collectionItem->op_members.list[i].ref.op_info.type == ITYPE_ARRAY )
										{
											FDI_GENERIC_ITEM generic_item2(ITYPE_ARRAY);
											AttributeNameSet attribute_list2 = FLAT_ARRAY::All_Attrs;
											item_spec.item.id = collectionItem->op_members.list[i].ref.op_info.id;

											int r_code2 = m_EDDEngine->GetItem( 0, 0, &item_spec, &attribute_list2, TEST_LANGUAGE, &generic_item2);
											if( r_code2 == 0 )
											{
												FLAT_ARRAY* arrayItem2 = dynamic_cast<FLAT_ARRAY*>(generic_item2.item);
												arrayListCount = arrayItem2->num_of_elements;
											}
										}
										else if(collectionItem->op_members.list[i].ref.op_info.type == ITYPE_LIST )
										{
											FDI_GENERIC_ITEM generic_item3(ITYPE_LIST);
											AttributeNameSet attribute_list3 = FLAT_LIST::All_Attrs;
											item_spec.item.id = collectionItem->op_members.list[i].ref.op_info.id;

											int r_code3 = m_EDDEngine->GetItem( 0, 0, &item_spec, &attribute_list3, TEST_LANGUAGE, &generic_item3);
											if( r_code3 == 0 )
											{
												FLAT_LIST* arrayItem3 = dynamic_cast<FLAT_LIST*>(generic_item3.item);
												arrayListCount = arrayItem3->count;
											}
										}
									
										for( int innerIndex = 0; innerIndex < arrayListCount; innerIndex++ )
										{
											paramSpec1.RefList.count = 3;
											paramSpec1.RefList.list = new nsEDDEngine::OP_REF_INFO[paramSpec1.RefList.count];
											paramSpec1.eType = FDI_PS_ITEM_ID_COMPLEX;
											paramSpec1.RefList.list[0].id = itemId;
											paramSpec1.RefList.list[0].member = index;
											paramSpec1.RefList.list[0].type = type;
											paramSpec1.RefList.list[1].id = 0;
											paramSpec1.RefList.list[1].member = collectionItem->op_members.list[i].name;
											paramSpec1.RefList.list[1].type = array_type;
											paramSpec1.RefList.list[2].id = collectionItem->op_members.list[i].ref.op_info.id;
											paramSpec1.RefList.list[2].member = innerIndex;
											paramSpec1.RefList.list[2].type = collectionItem->op_members.list[i].ref.op_info.type;
										
											DoCommandExecution(pCommandList, &paramSpec1);
											delete [] paramSpec1.RefList.list;
											paramSpec1.RefList.list = nullptr;
										}
									}
								}
							}					
						}
					}
				}
			}			
		}
		else if( type == ITYPE_LIST )
		{
			// First get command list for whole list
			FDI_PARAM_SPECIFIER paramSpec;
					
			paramSpec.eType = FDI_PS_ITEM_ID;
			paramSpec.id = itemId;
			paramSpec.subindex = 0;

			DoCommandExecution(pCommandList, &paramSpec);

			// Next get command list for individual elements
			FDI_GENERIC_ITEM generic_item(ITYPE_LIST);

			AttributeNameSet attribute_list = FLAT_LIST::All_Attrs;
			int r_code = m_EDDEngine->GetItem( 0, 0, &item_spec, &attribute_list, TEST_LANGUAGE, &generic_item);

			if( r_code == 0 )
			{
				FLAT_LIST* listItem = dynamic_cast<FLAT_LIST*>(generic_item.item);
		
				item_spec.item.id = listItem->type;

				ITEM_TYPE list_IType = ITYPE_NO_VALUE;
				m_EDDEngine->GetItemType(0, &item_spec, &list_IType);

				if( list_IType == ITYPE_VARIABLE )
				{
					// Search for the first one
					paramSpec.eType = FDI_PS_ITEM_ID_SI;
					paramSpec.id = itemId;
					paramSpec.subindex = 0;

					DoCommandExecution(pCommandList, &paramSpec);

					// If more are specified in the EDD, get them too.
					// Note this index starts at 1!
					for( ulong index = 1; index < listItem->count; index++ )
					{
						paramSpec.subindex = index;

						DoCommandExecution(pCommandList, &paramSpec);
					}

					// Search for one that is bigger than expected
					paramSpec.subindex = listItem->count+1;

					DoCommandExecution(pCommandList, &paramSpec);
				}
				else if (list_IType == ITYPE_ARRAY)
				{
					FDI_GENERIC_ITEM arr_generic_item(ITYPE_ARRAY);

					FDI_ITEM_SPECIFIER arr_item_spec = {FDI_ITEM_ID, listItem->type, 0};
					AttributeNameSet arr_attribute_list = FLAT_ARRAY::All_Attrs;

					r_code = m_EDDEngine->GetItem( 0, 0, &arr_item_spec, &arr_attribute_list, TEST_LANGUAGE, &arr_generic_item);

					if (r_code == 0)
					{
						FLAT_ARRAY* arrayItem = dynamic_cast<FLAT_ARRAY*>(arr_generic_item.item);

						ulong arr_count = arrayItem->num_of_elements;

						//item_spec.item.id = arrayItem->type;

						//ITEM_TYPE arr_type = ITYPE_NO_VALUE;
						//m_EDDEngine->GetItemType(0, &item_spec, &arr_type);


						// Create the ParamSpec
						paramSpec.eType = FDI_PS_ITEM_ID_COMPLEX;
						paramSpec.RefList.count = 2;
						paramSpec.RefList.list = new OP_REF_INFO[paramSpec.RefList.count];

						paramSpec.RefList.list[0].id = itemId;			// of the LIST
						paramSpec.RefList.list[0].type = ITYPE_LIST;

						//
						// Search for the first element of the list
						//
						paramSpec.RefList.list[0].member = 0;	// of the LIST

						paramSpec.RefList.count = 1;
						DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire array

						paramSpec.RefList.count = 2;		// Get elements

						// foreach element of the array
						for (ulong i=0; i<arr_count; i++)
						{
							paramSpec.RefList.list[1].id = listItem->type;
							paramSpec.RefList.list[1].member = i;
							paramSpec.RefList.list[1].type = ITYPE_ARRAY;

							DoCommandExecution(pCommandList, &paramSpec);
						}

						//
						// If more are specified in the EDD, get them too.
						//
						// Note this index starts at 1!
						for (ulong index=1; index < listItem->count; index++)
						{
							paramSpec.RefList.list[0].member = index;	// of the LIST

							paramSpec.RefList.count = 1;
							DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire array

							paramSpec.RefList.count = 2;		// Get elements

							// foreach element of the array
							for (ulong i=0; i<arr_count; i++)
							{
								paramSpec.RefList.list[1].id = listItem->type;
								paramSpec.RefList.list[1].member = i;
								paramSpec.RefList.list[1].type = ITYPE_ARRAY;

								DoCommandExecution(pCommandList, &paramSpec);
							}
						}

						//
						// Search for one that is bigger than expected
						//
						paramSpec.RefList.list[0].member = listItem->count+1;	// of the LIST

						paramSpec.RefList.count = 1;
						DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire array

						paramSpec.RefList.count = 2;		// Get elements

						// foreach element of the array
						for (ulong i=0; i<arr_count; i++)
						{
							paramSpec.RefList.list[1].id = listItem->type;
							paramSpec.RefList.list[1].member = i;
							paramSpec.RefList.list[1].type = ITYPE_ARRAY;

							DoCommandExecution(pCommandList, &paramSpec);
						}


						delete [] paramSpec.RefList.list;
						paramSpec.RefList.list = nullptr;
					}

				}
				else if (list_IType == ITYPE_COLLECTION)
				{
					FDI_GENERIC_ITEM col_generic_item(ITYPE_COLLECTION);

					FDI_ITEM_SPECIFIER col_item_spec = {FDI_ITEM_ID, listItem->type, 0};
					AttributeNameSet col_attribute_list = FLAT_COLLECTION::All_Attrs;

					r_code = m_EDDEngine->GetItem( 0, 0, &col_item_spec, &col_attribute_list, TEST_LANGUAGE, &col_generic_item);

					if (r_code == 0)
					{
						FLAT_COLLECTION* colItem = dynamic_cast<FLAT_COLLECTION*>(col_generic_item.item);

						//ulong arr_count = arrayItem->num_of_elements;

						//item_spec.item.id = arrayItem->type;

						//ITEM_TYPE arr_type = ITYPE_NO_VALUE;
						//m_EDDEngine->GetItemType(0, &item_spec, &arr_type);


						// Create the ParamSpec
						paramSpec.eType = FDI_PS_ITEM_ID_COMPLEX;
						paramSpec.RefList.count = 2;
						paramSpec.RefList.list = new OP_REF_INFO[paramSpec.RefList.count];

						paramSpec.RefList.list[0].id = itemId;			// of the LIST
						paramSpec.RefList.list[0].type = ITYPE_LIST;

						//
						// Search for the first element of the list
						//
						paramSpec.RefList.list[0].member = 0;	// of the LIST

						paramSpec.RefList.count = 1;
						DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire collection

						paramSpec.RefList.count = 2;		// Now try getting the members

						// foreach member of the collection
						for (ulong i=0; i<colItem->op_members.count; i++)
						{
							paramSpec.RefList.list[1].id = listItem->type;
							paramSpec.RefList.list[1].member = colItem->op_members.list[i].name;
							paramSpec.RefList.list[1].type = ITYPE_COLLECTION;

							DoCommandExecution(pCommandList, &paramSpec);
						}

						//
						// If more are specified in the EDD, get them too.
						//
						// Note this index starts at 1!
						for (ulong index=1; index < listItem->count; index++)
						{
							paramSpec.RefList.list[0].member = index;	// of the LIST

							paramSpec.RefList.count = 1;
							DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire array

							paramSpec.RefList.count = 2;		// Get elements

							// foreach member of the collection
							for (ulong i=0; i<colItem->op_members.count; i++)
							{
								paramSpec.RefList.list[1].id = listItem->type;
								paramSpec.RefList.list[1].member = colItem->op_members.list[i].name;
								paramSpec.RefList.list[1].type = ITYPE_COLLECTION;

								DoCommandExecution(pCommandList, &paramSpec);
							}
						}

						//
						// Search for one that is bigger than expected
						//
						paramSpec.RefList.list[0].member = listItem->count+1;	// of the LIST

						paramSpec.RefList.count = 1;
						DoCommandExecution(pCommandList, &paramSpec); // Try getting the entire array

						paramSpec.RefList.count = 2;		// Get elements

						// foreach member of the collection
						for (ulong i=0; i<colItem->op_members.count; i++)
						{
							paramSpec.RefList.list[1].id = listItem->type;
							paramSpec.RefList.list[1].member = colItem->op_members.list[i].name;
							paramSpec.RefList.list[1].type = ITYPE_COLLECTION;

							DoCommandExecution(pCommandList, &paramSpec);
						}


						delete [] paramSpec.RefList.list;
						paramSpec.RefList.list = nullptr;
					}
				}
			}
		}
		else if( type == ITYPE_VARIABLE )
		{
			FDI_PARAM_SPECIFIER varParamSpec;
			varParamSpec.eType = FDI_PS_ITEM_ID;
			varParamSpec.id = itemId;
		
			DoCommandExecution(pCommandList, &varParamSpec);		
		}
	
		pDDItem->appendChild(pCommandList);
	}
}

void CDDSXMLBuilder::DoCommandExecution(MSXML2::IXMLDOMElementPtr & pCommandList, FDI_PARAM_SPECIFIER* paramSpec )
{
	int r_code = 0; 
	FDI_COMMAND_LIST writeCommandList;
	FDI_COMMAND_LIST readCommandList;
	MSXML2::IXMLDOMElementPtr pReferenceItem;
	pReferenceItem = m_pDOMDoc->createElement(ELMTNAME_REF_ITEM);

	CString sResolvedRef = _T("");

	if( paramSpec->eType == FDI_PS_ITEM_ID_COMPLEX )
	{
		for( int i = 0; i < paramSpec->RefList.count; i++ )
		{
			
			if( i == 0 )
			{
				CString strMember = _T("");
				FormatItemIdAndName(paramSpec->RefList.list[i].id, sResolvedRef);
				strMember.Format(_T(", %u"), paramSpec->RefList.list[i].member);
				sResolvedRef += strMember;
				
			}
			else
			{
				CString temp = _T("");
				FormatItemIdAndName(paramSpec->RefList.list[i].member, temp);
				sResolvedRef += (CString)", " + temp;
			}
		}
	}
	else if( paramSpec->eType == FDI_PS_ITEM_ID_SI )
	{
		CString strsubindex = _T("");
		FormatItemIdAndName(paramSpec->id, sResolvedRef);
		strsubindex.Format(_T(", %u"), paramSpec->subindex);
		sResolvedRef += strsubindex;
	}
	else
	{
		FormatItemIdAndName(paramSpec->id, sResolvedRef);
	}

	pReferenceItem->setAttribute(ATTRNAME_RESOLVED_REF, (LPCTSTR)sResolvedRef);
	// Get all read commands for this param
	r_code = m_EDDEngine->GetCommandList(0, paramSpec, FDI_READ_COMMAND, &readCommandList );
	if( r_code == 0 )
	{
		int nCount = readCommandList.count;
		for( int nIndex = 0; nIndex < nCount; nIndex++ )
		{
			FDIHartDDS_AddChildCommandItems( pReferenceItem, ELMTNAME_READ_COMMAND, &readCommandList.list[nIndex] );
		}
	}
	
	r_code = m_EDDEngine->GetCommandList(0, paramSpec, FDI_WRITE_COMMAND, &writeCommandList);
	if( r_code == 0 )
	{
		int nCount = writeCommandList.count;
		for( int nIndex = 0; nIndex < nCount; nIndex++ )
		{
			FDIHartDDS_AddChildCommandItems( pReferenceItem, ELMTNAME_WRITE_COMMAND, &writeCommandList.list[nIndex] );
		}
	}

	if( writeCommandList.count > 0 || readCommandList.count > 0 )
	{
		pCommandList->appendChild(pReferenceItem);
	}
}

void CDDSXMLBuilder::GetErrorString( BSTR sFileName, int ErrorCode, BSTR* sErrorString )
{
    m_szFilename = sFileName;

    USES_CONVERSION;

    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
    wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());

	nsEDDEngine::EDDEngineFactoryError eError;
    m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr, NULL, NULL, configXML, &eError);
    wchar_t *pszErrorstr;
    pszErrorstr = m_EDDEngine->GetErrorString(ErrorCode);
    *sErrorString = ::SysAllocString(pszErrorstr);

    //clean up the device information regardless of the scenario
    delete m_EDDEngine;
}


int CDDSXMLBuilder::GetCmdIdFromNumber(BSTR sFileName, int iBlockInstance, unsigned long ulCmdNumber, unsigned long * pCmdItemId)
{
    m_szFilename = sFileName;

	USES_CONVERSION;
	
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	nsEDDEngine::EDDEngineFactoryError eError;
    m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr,NULL, NULL, configXML, &eError);

	//m_EDDEngine->Init(0, wstr_name);
    int rc = m_EDDEngine->GetCmdIdFromNumber(iBlockInstance, ulCmdNumber, pCmdItemId);
	//clean up the device information regardless of the scenario
	delete m_EDDEngine;
    return rc;
}

int CDDSXMLBuilder::GetAxisUnitRelItemId(BSTR sFileName, int iBlockInstance, unsigned long AxisItemId, unsigned long * pUnitItemId)
{
    m_szFilename = sFileName;
    
	USES_CONVERSION;
	
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	nsEDDEngine::EDDEngineFactoryError eError;
    m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr, NULL, NULL, configXML, &eError);

	//m_EDDEngine->Init(0, wstr_name);
	void *pValueSpec = 0;
    int rc = m_EDDEngine->GetAxisUnitRelItemId(iBlockInstance, pValueSpec, AxisItemId, pUnitItemId);
	//clean up the device information regardless of the scenario
	delete m_EDDEngine;
    return rc;
}

int CDDSXMLBuilder::GetParamUnitRelItemId(BSTR bstrFilename, int iBlockInstance, int /*eType*/, unsigned long id, unsigned long subindex, unsigned long* pUnitItemId)
{
    m_szFilename = bstrFilename;

	USES_CONVERSION;
	
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	nsEDDEngine::EDDEngineFactoryError eError;
    m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr, NULL, NULL, configXML, &eError);
    FDI_PARAM_SPECIFIER ParamSpec;
    ITEM_ID  ItemId;

	if( subindex != 0 )
	{
		ParamSpec.eType = FDI_PS_ITEM_ID_SI;
	}
	else
	{
		ParamSpec.eType = FDI_PS_ITEM_ID;
	}
    ParamSpec.id = (ITEM_ID) id;
    ParamSpec.subindex = subindex;

	//m_EDDEngine->Init(0, wstr_name);
    int rc = m_EDDEngine->GetParamUnitRelItemId(iBlockInstance, nullptr, &ParamSpec, &ItemId);
    *pUnitItemId = ItemId;
	//clean up the device information regardless of the scenario
	delete m_EDDEngine;
    return rc;
}

int CDDSXMLBuilder::GetItemType(BSTR sFileName, int iBlockInstance, int /*eType*/, unsigned long id, unsigned long /*subindex*/, int *pItemType)
{
    int rc = 0;

    FDI_ITEM_SPECIFIER ItemSpec;
    ITEM_TYPE  ItemType;

    ItemSpec.eType = FDI_ITEM_ID;
    ItemSpec.item.id = (ITEM_ID) id;
    ItemSpec.subindex = 0;
    
	m_szFilename = sFileName;
    USES_CONVERSION;
	
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	nsEDDEngine::EDDEngineFactoryError eError;
	m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr, NULL, NULL, configXML, &eError);

    rc = m_EDDEngine->GetItemType(iBlockInstance, &ItemSpec, &ItemType);
    *pItemType = ItemType;
    delete m_EDDEngine;
    return rc;
}


int CDDSXMLBuilder::GetItemTypeAndItemId(BSTR sFileName, int iBlockInstance, int /*eType*/, unsigned long id, unsigned long /*subindex*/, int *pItem_Type, unsigned long *pItemId)
{
	m_szFilename = sFileName;

	USES_CONVERSION;
	
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }
	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
	nsEDDEngine::EDDEngineFactoryError eError;
	m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(wstr_name, nullptr, NULL, NULL, configXML, &eError);
    FDI_ITEM_SPECIFIER ItemSpec;
    ITEM_TYPE  ItemType;

    ItemSpec.eType = FDI_ITEM_ID;
    ItemSpec.item.id = (ITEM_ID) id;
    ItemSpec.subindex = 0;
    int rc = 0;
    rc = m_EDDEngine->GetItemTypeAndItemId(iBlockInstance, &ItemSpec, &ItemType, pItemId);
    *pItem_Type = ItemType;
    delete m_EDDEngine;
    return rc;
}

///* For Automated testing of GetErrorString
//*   This function will generate a file which can be used to verify the output
//*/
//void CDDSXMLBuilder::GetErrorString( BSTR sFileName, BSTR* sErrorString )
//{
//    int r_code = 0;
//    m_szFilename = sFileName;
//
//    USES_CONVERSION;
//
//    wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
//    m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(nsEDDEngine::HART, wstr_name, NULL, NULL, L"en", &r_code);
//
//    wchar_t *pszErrorstr;
//
//    int l_arrErrorCode[] = {-1,0,1,2,10,903,-10};
//
//    FILE * pFile;
//    char name [100];
//
//    pFile = fopen ("E:\\Sai\\UnitTestFile\\GetErrorStringUTGenerated.txt","w");
//    for (int j = 0; j < 7; j++)
//    {
//        pszErrorstr = m_EDDEngine->GetErrorString(l_arrErrorCode[j]);
//        wcstombs(name, pszErrorstr, wcslen(pszErrorstr)+1);
//        fprintf (pFile, "Error Code: %d  Error String: [%s]\n",l_arrErrorCode[j],name);
//    }
//    fclose (pFile);
//    //clean up the device information regardless of the scenario
//    delete m_EDDEngine;
//}
//
//
//// For Automated testing of GetItemType
//int CDDSXMLBuilder::GetItemType(BSTR sFileName, int *pItemType)
//{
//	int r_code = 0;
//
//	m_szFilename = sFileName;
//    FLAT_DEVICE_DIR *flat_device_dir;
//	USES_CONVERSION;
//	
//	wchar_t *wstr_name = T2W(m_szFilename.GetBuffer());
//	m_EDDEngine = EDDEngineFactory::CreateDeviceInstance(nsEDDEngine::HART, wstr_name, NULL, NULL, L"en", &r_code);
//
//  //////////////////////////////////////////////////////////
//    r_code = m_EDDEngine->GetDeviceDir(&flat_device_dir);
//
//    ITEM_TBL					*item_table;
//	ITEM_TBL_ELEM				*item_element;
//	ITEM_ID						item_id = 0;
//	ITEM_TYPE					item_type = ITYPE_NO_VALUE;
//	
//	item_table = &flat_device_dir->item_tbl;
//
//    int iBlockInstance = 0;
//    FILE * pFile;
//    char name [100];
//
//    pFile = fopen ("E:\\Sai\\UnitTestFile\\GetItemTypeUTGenerated.txt","w");
//    for( int nIndex = 0; nIndex < item_table->count ; nIndex++ )
//    {
//        FDI_ITEM_SPECIFIER ItemSpec;
//        item_element = &item_table->list[nIndex];
//        ItemSpec.eType = FDI_ITEM_ID;
//        ItemSpec.item.id = item_element->item_id;
//        ItemSpec.subindex = 0;
//        //item_type = item_element->item_type;
//        r_code  = m_EDDEngine->GetItemType(iBlockInstance, &ItemSpec, &item_type);
//
//        *pItemType = item_type;
//
//        itoa(*pItemType,name,10);
//        fprintf (pFile, "Item Id: %d  Item Type: [%s]\n",ItemSpec.item.id,name);
//    }
//    fclose (pFile);
//    return r_code;
//}


void CDDSXMLBuilder::SetLoadXML(bool bLoadXML)
{
	m_bLoadXML = bLoadXML;
}

void CDDSXMLBuilder::AddReturnList(MSXML2::IXMLDOMElementPtr pDDItem, FDI_GENERIC_ITEM *generic_item){
	
	// if(generic_item->errors.count > 0){
		MSXML2::IXMLDOMElementPtr pReturnList;
		MSXML2::IXMLDOMElementPtr pReturnInfo;
		pReturnList = m_pDOMDoc->createElement( ELMTNAME_RETURN_LIST );
		RETURN_LIST return_list = generic_item->errors;
		CString attribute_name;
		
		for (int i = 0; i< generic_item->errors.count; i++ ){
			pReturnInfo = m_pDOMDoc->createElement( ELMTNAME_RETURN_INFO );
			RETURN_INFO return_info = return_list.list[i];
			pReturnInfo->setAttribute( ATTRNAME_RESPONSE_CODE, (int)return_info.rc );
			// pReturnInfo->setAttribute( ATTRNAME_BAD_ATTR, EnumAttributeStrings[(int)return_info.bad_attr]);									
			AmsHartDDS_convert_attribute_name(return_info.bad_attr,attribute_name);
			pReturnInfo->setAttribute( ATTRNAME_BAD_ATTR, (LPCTSTR)attribute_name);									
			pReturnList->appendChild( pReturnInfo );		
		}	

		pDDItem->appendChild( pReturnList );
	//}
}

void CDDSXMLBuilder::AddItemInformation(MSXML2::IXMLDOMElementPtr pDDItem, ITEM_INFORMATION *item_info) {

	MSXML2::IXMLDOMElementPtr pItemInfoAttr = m_pDOMDoc->createElement(ATTRNAME_ITEM_INFORMATION);
	pItemInfoAttr->setAttribute(ELMTNAME_ITEM_FILE_NAME, item_info->file_name.c_str());
	pItemInfoAttr->setAttribute(ELMTNAME_ITEM_LINE_NO, item_info->line_number);

	MSXML2::IXMLDOMElementPtr pAttrInfo;

	for (int i = 0; i < item_info->attrs.count; i++)
	{
		pAttrInfo = m_pDOMDoc->createElement(ELMTNAME_ATTR_INFO);
		CString attribute_name;
		AmsHartDDS_convert_attribute_name(item_info->attrs.list[i].attr_name, attribute_name);
		pAttrInfo->setAttribute(ELMTNAME_ATTR_NAME, (LPCTSTR)attribute_name);
		pAttrInfo->setAttribute(ELMTNAME_ATTR_LINE_NO, item_info->attrs.list[i].attr_lineno);
		pAttrInfo->setAttribute(ELMTNAME_ATTR_FILE_NAME, item_info->attrs.list[i].attr_file_name.c_str());
		pItemInfoAttr->appendChild(pAttrInfo);
	}
	
	pDDItem->appendChild(pItemInfoAttr);
}


