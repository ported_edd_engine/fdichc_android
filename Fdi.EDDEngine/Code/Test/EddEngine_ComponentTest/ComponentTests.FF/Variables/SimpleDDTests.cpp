#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_a_simple_test_DD_for_FF
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;
            static  FDI_GENERIC_ITEM* genericItem;
            static FLAT_VAR* test_var;
            static ITEM_ID* test_var_item_ID;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0001\\TestCase0001.ff6");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);

                test_var_item_ID = new ITEM_ID();

                engine->Instance->GetItemIdFromSymbolName(L"test_var_2", test_var_item_ID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
                delete genericItem;
                delete test_var_item_ID;
                delete test_var;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_the_correct_item_id_for_a_variable()
            {
                Assert::AreEqual(0x20007u, *test_var_item_ID);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_device_type_one()
            {
                Assert::AreEqual(1u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_device_revision_three()
            {
                Assert::AreEqual(3u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_dd_revision_four()
            {
                Assert::AreEqual(4u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(253u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_parameters_from_one_block_with_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_VAR* test_var;
            static int currentBlockinstance;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0001\\TestCase0001.ff6");                
                engine->Initialize();                

                // get the ItemId for the test_block
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                int i = engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                for (int i = 1; i <= 100; i++)
                {
                    pFieldbusBlockInfo->m_usObjectIndex =1000+i*10;
                    engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                    currentBlockinstance = iBlockInstance;
                }

                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_var_8", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_var = dynamic_cast<FLAT_VAR*>(genericItem->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_class_type_for_the_test_variable_8()
            {
                Assert::AreEqual((int) CT_LOCAL_A, (int)test_var->class_attr);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_current_blockInstance_65()
            {
                Assert::AreEqual(0x65, currentBlockinstance);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_id_for_the_test_variable()
            {
                Assert::AreEqual(0x2000Au, test_var->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_block()
            {
                Assert::AreEqual(0x20006u, *test_block_item_ID);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_label_for_the_test_variable()
            {
                Assert::AreEqual("Test Variable 8", gcnew String(test_var->label.c_str()));
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(test_var->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_contain_the_label_as_mask()
            {
                Assert::IsTrue(test_var->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(test_var->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_not_contain_the_default_value_as_mask()
            {
                Assert::IsFalse(test_var->isAvailable(nsEDDEngine::default_value), "default_value");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_contain_the_default_handling_as_mask()
            {
                Assert::IsTrue(test_var->isAvailable(nsEDDEngine::handling), "handling");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_not_contain_the_help_as_mask()
            {
                Assert::IsFalse(test_var->isAvailable(nsEDDEngine::help), "help");
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_block_with_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_BLOCK* test_block;
            static ITEM_ID* test_var_item_ID_4;
            static ITEM_ID* test_var_item_ID_6;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0001\\TestCase0001.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem->item);

                test_var_item_ID_4 = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_4", test_var_item_ID_4);

                test_var_item_ID_6 = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_6", test_var_item_ID_6);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
                delete test_var_item_ID_4;
                delete test_var_item_ID_6;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_label()
            {
                Assert::AreEqual("Test Block", gcnew String( test_block->label.c_str()));
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_help_text()
            {
                Assert::AreEqual("Help Test Block", gcnew String( test_block->help.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_one_local_Parameter_in_list()
            {
                Assert::AreEqual(1u, (UINT32) test_block->local_param.count);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_two_parameter_in_list()
            {
                Assert::AreEqual(2u, (UINT32) test_block->param.count);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_as_first_parameter_in_list_test_var_4()
            {
                Assert::AreEqual(*test_var_item_ID_4, test_block->param.list[0].ref.id);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_as_second_parameter_in_list_test_var_6()
            {
                Assert::AreEqual(*test_var_item_ID_6, test_block->param.list[1].ref.id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_item_id_for_the_characteristics_for_test_block()
            {
                Assert::AreEqual(0x2000Bu, test_block->characteristic);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_not_contain_the_help_as_mask_in_test_variable_four()
            {
                Assert::IsTrue(test_block->isAvailable(nsEDDEngine::help), "help");
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_contain_the_label_as_mask_in_test_variable_four()
            {
                Assert::IsTrue(test_block->isAvailable(nsEDDEngine::label), "label");
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassVariableTypes
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_BLOCK* test_block;
            static TYPE_SIZE *_expectedTypeSize, *_receivedTypeSize; 

            static void SetupBase(String ^eddFilename,String ^itemSymbolName, TYPE_SIZE expected)
            {
                _expectedTypeSize = new TYPE_SIZE(expected);
                _receivedTypeSize = nullptr;
                engine = gcnew EddEngineController(eddFilename);                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier(itemSymbolName, &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                FLAT_VAR* elementToCheck = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's TYPE_SIZE
                _receivedTypeSize =new TYPE_SIZE(elementToCheck->type_size);
                Assert::IsFalse(_receivedTypeSize == nullptr, "Could not get TYPE_SIZE_info from elementToCheck");
                delete pFieldbusBlockInfo;
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
                delete _expectedTypeSize;
                delete _receivedTypeSize;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_type()
            {
                Assert::AreEqual((int) _expectedTypeSize->type, (int)_receivedTypeSize->type);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_size()
            {
                Assert::AreEqual(_expectedTypeSize->size, _receivedTypeSize->size);
            }

        };

        [TestClass]
        public ref class When_loading_a_DATE_parameter_from_TestCase0002 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typesizeToCheck;
                typesizeToCheck.type = VT_EDD_DATE;
                typesizeToCheck.size = 3;
                SetupBase("FF\\TestCase0002\\TestCase0002.ff6", "test_date", typesizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_TIME_parameter_from_TestCase0002 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typesizeToCheck;
                typesizeToCheck.type = VT_TIME;
                typesizeToCheck.size = 6;
                SetupBase("FF\\TestCase0002\\TestCase0002.ff6", "test_time", typesizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_DATE_AND_TIME_parameter_from_TestCase0002 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typesizeToCheck;
                typesizeToCheck.type = VT_DATE_AND_TIME;
                typesizeToCheck.size = 7;
                SetupBase("FF\\TestCase0002\\TestCase0002.ff6", "test_date_and_time", typesizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_TIME_VALUE_parameter_from_TestCase0002 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typesizeToCheck;
                typesizeToCheck.type = VT_TIME_VALUE;
                typesizeToCheck.size = 8;
                SetupBase("FF\\TestCase0002\\TestCase0002.ff6", "test_time_value", typesizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_DURATION_parameter_from_TestCase0002 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typesizeToCheck;
                typesizeToCheck.type = VT_DURATION;
                typesizeToCheck.size = 6;
                SetupBase("FF\\TestCase0002\\TestCase0002.ff6", "test_duration", typesizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };
      
    } // namespace FF
} // namespace ComponentTests
