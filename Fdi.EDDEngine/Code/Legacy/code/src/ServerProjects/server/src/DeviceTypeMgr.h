#pragma once


#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"
#include "RodMgr.h"
#include <mutex>


//ClockDeviceTypeMgr is used to make the shared data in DeviceTypeMgr thread-safe.
//Use the function by declaring a local variable in sections of code that need to be protected:
//For example, CLockTypeDeviceTypeMgr dtLockDeviceTypeMgr;
//The code makes the functions threadsafe by locking a private member variable m_cs in the DeviceTypeMgr.
class CLockDeviceTypeMgr 
{
public:
	CLockDeviceTypeMgr();
	~CLockDeviceTypeMgr();
};

//global DeviceTypeMgr class

class CDeviceTypeMgr
{
	friend class CLockDeviceTypeMgr;
public:
	CDeviceTypeMgr();
	~CDeviceTypeMgr();

	//Device type table functions.
	int get_adtt_dd_handle( DEVICE_TYPE_HANDLE dth, ROD_HANDLE *ddh ) const;
	void set_adtt_dd_handle( DEVICE_TYPE_HANDLE dth, ROD_HANDLE *dh );
	int get_adtt_dd_dev_tbls( DEVICE_TYPE_HANDLE dth, void **ddt) const;
	int set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE,void *);
	int valid_device_type_handle( DEVICE_TYPE_HANDLE ) const;
	DEVICE_TYPE_HANDLE ct_device_type_search( DD_DEVICE_ID *);	
	int get_adtt_usage( DEVICE_TYPE_HANDLE dth ) const;
	void set_adtt_usage( DEVICE_TYPE_HANDLE dth, int u );
	int ct_new_device_type(DEVICE_TYPE_HANDLE *);	
	DD_DEVICE_ID* get_adtt_dd_device_id( DEVICE_TYPE_HANDLE dth ) const;
	void set_adtt_dd_device_id( DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID* id );		
	void set_adtt_compatibility( DEVICE_TYPE_HANDLE dth, CM_COMPAT c );	
	int get_adtt_syminfo( DEVICE_TYPE_HANDLE dth, SYMINFO **si ) const;
	int set_adtt_syminfo (DEVICE_TYPE_HANDLE,SYMINFO*);	
	void ct_remove_device_type(DEVICE_TYPE_HANDLE device_type_handle);
	
	//Device manufacturer table functions.
	void SetDevManufacturerTblOffsetInDevTypeTbl(DEVICE_TYPE_HANDLE dth, DEVICE_MANUFACTURER_HANDLE admto);	
	DEVICE_MANUFACTURER_HANDLE GetDevManufacturerTblOffsetFromDevTypeTbl(DEVICE_TYPE_HANDLE dth) const;
	ActiveDevManufacturerTblElem* GetActiveDevManufacturerTblElem(DEVICE_MANUFACTURER_HANDLE dmh) ;
	
	int ConnectDeviceManufacturerTableEntry(DD_DEVICE_ID dd_device_id, DEVICE_TYPE_HANDLE dth);
	void DecrementOrDeallocateManufacturer(DEVICE_MANUFACTURER_HANDLE device_manufacturer_handle);
	//Remote Object Dictionary(ROD) wrapper functions
	int			rod_get_tok_major_rev(ROD_HANDLE rhandle) ;
	int			valid_rod_handle( ROD_HANDLE ) ;
	int			ds_ddod_file_load( ENV_INFO *env_info, LPTSTR filename,	ROD_HANDLE *rod_handle);
	int			rod_close(ROD_HANDLE rod_handle);
	bool		is_tok_major_rev_6_or_8(ROD_HANDLE rhandle);
	bool		is_string_encoded_in_utf8(ROD_HANDLE rhandle);
	int			rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object) ;
	int			rod_get_image(ROD_HANDLE rod_handle, DDL_UINT offset, DDL_UINT size, BINARY_LANGUAGE_IMAGE* image) ;
	DDOD_HEADER * get_header(ROD_HANDLE rhandle);
	
private:
	//variable declarations
	std::recursive_mutex		m_cs;
	ACTIVE_DEV_TYPE_TBL			active_dev_type_tbl;
	CRodMgr						m_RodMgr;
	ActiveDevManufacturerTbl	active_dev_manufacturer_tbl;

	//method declarations
	DEVICE_MANUFACTURER_HANDLE get_adt_man_tbl_offset( DEVICE_HANDLE dh );
	CM_COMPAT get_adtt_compatibility( DEVICE_TYPE_HANDLE dth );		
};

//global shared device type manager
extern CDeviceTypeMgr g_DeviceTypeMgr;