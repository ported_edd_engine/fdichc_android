/*****************************************************************************
 *
 * File: Methods.h
 * Version: 4.1.0
 * Date: 06/11/10
 *
 *****************************************************************************
 * Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: This is the include file required when writing a method.  It  
 *              contains the return codes for the standard libray functions 
 *              and a variety of macros often needed when writing methods.
 *
 *****************************************************************************
 */
 
#ifndef _METHODS_H
#define _METHODS_H

#define BI_SUCCESS       0              /* task succeeded in intended task    */
#define BI_ERROR        -1              /* error occured in task              */
#define BI_ABORT        -2              /* user aborted task                  */
#define BI_NO_DEVICE    -3              /* no device found on comm request    */
#define BI_COMM_ERR     -4              /* communications error               */
#define BI_CONTINUE     -5              /* continue */
#define BI_RETRY        -6              /* retry */
#define BI_PORT_IN_USE  -7              /* block transfer port */
#define N_BI_CODES       8

#define ALLBITS                 0xFF    /* sets all bits in the byte */

#define STATUS_SIZE             3       /* size of status array               */
#define STATUS_RESPONSE_CODE    0                                  
#define STATUS_COMM_STATUS      1
#define STATUS_DEVICE_STATUS    2

#define RESP_MASK_LEN           16      /* size of response code masks      */
#define DATA_MASK_LEN           25      /* size of data masks               */
#define MAX_XMTR_STATUS_LEN     DATA_MASK_LEN

#define RESPONSE_BUFFER_LENGTH  40      /* size of buffer string to place resp*/
#define BI_DISP_STR_LEN         126     /* size of # lines X # char/line in a response code display */


/*
 * Device status codes used in defaults
 */
#define DEVICE_MALFUNCTION      0x80
#define CMD_NOT_IMPLIMENTED     0x40
#define ACCESS_RESTRICTED       0x10


/*
 * Name to ID Translation Functions
 */
#define get_dictionary_string(a,b,c)    _get_dictionary_string(DICT_ID(a),(b),(c))
#define VARID(a)                        METHODID(a)
#define array_reference(a,b)            resolve_array_ref(METHODID(a),(b))
#define collection_reference(a,b)       resolve_record_ref(METHODID(a),(b))
#define NaN                             NaN_value()
#define dictionary_string(a)            _dictionary_string(DICT_ID(a))


/*
 * Display Message, Value, and Menu Functions
 */
#define get_dev_var_value(a,b,c)            _get_dev_var_value((a),(b),METHODID(c))
#define GET_DEV_VAR_VALUE(a,b)              _get_dev_var_value((a),0,METHODID(b))
#define get_local_var_value(a,b,c)          _get_local_var_value((a),(b),LOCALVAR(c))
#define GET_LOCAL_VAR_VALUE(a,b)            _get_local_var_value((a),0,LOCALVAR(b))
#define display_comm_status(a)              _display_xmtr_status(METHODID(comm_status),a)
#define display_device_status(a)            _display_xmtr_status(METHODID(device_status),a)
#define display_xmtr_status(a,b)            _display_xmtr_status(METHODID(a),b)
#define get_status_code_string(a,b,c,d)     _get_status_code_string(METHODID(a),(b),(c),(d))
#define get_enum_string(a,b,c)              _get_enum_string(METHODID(a),(b),(c)) 
#define MenuDisplay(a,b,c)                  _MenuDisplay (METHODID(a),(b),LOCALVAR(c))
#define Menu(a)                             _MenuDisplay (METHODID(a),"OK",0)
#define DISPLAY(a)                          display( a,0)
#define itoa( a,b,c )                       itoa( ( a), LOCALVAR( b), ( c) )
#define get_rspcode_string( a, b, c, d )    rspcode_string( ( a), ( b), LOCALVAR( c), ( d) ) 


/*
 * List Support Functions
 */
#define ListInsert(a,b,c)           _ListInsert(METHODID(a),(b),METHODID(c))
#define ListDeleteElementAt(a,b)    _ListDeleteElementAt(METHODID(a),(b))


/*
 * Variable Access Functions
 */
#define assign_var(a,b)             _vassign(METHODID(a),METHODID(b))
#define vassign(a,b)                _vassign(METHODID(a),METHODID(b))

#define assign_double(a,b)          _dassign(METHODID(a),(b))
#define dassign(a,b)                _dassign(METHODID(a),(b))

#define assign_float(a,b)           _fassign(METHODID(a),(b))
#define fassign(a,b)                _fassign(METHODID(a),(b))

#define assign_int(a,b)             _iassign(METHODID(a),(b))
#define iassign(a,b)                _iassign(METHODID(a),(b))

#define lassign(a,b)                _lassign(METHODID(a),(b))

#define float_value(a)              _fvar_value(METHODID(a))
#define fvar_value(a)               _fvar_value(METHODID(a))

#define int_value(a)                _ivar_value(METHODID(a))
#define ivar_value(a)               _ivar_value(METHODID(a))

#define long_value(a)               _lvar_value(METHODID(a))
#define lvar_value(a)               _lvar_value(METHODID(a))


/*
 * Abort Methods Functions
 */
#define add_abort_method(a)             _add_abort_method(METHODID(a))
#define remove_abort_method(a)          _remove_abort_method(METHODID(a))
#define remove_all_abort_methods()      remove_all_abort()
#define push_abort_method(a)            _push_abort_method(METHODID(a))


/*
 * ABORT, IGNORE, RETRY Functions
 */
#define ABORT_ON_COMM_ERROR()           _set_comm_status(0xFF,__ABORT__)
#define IGNORE_COMM_ERROR()             _set_comm_status(0xFF,__IGNORE__)
#define RETRY_ON_COMM_ERROR()           _set_comm_status(0xFF,__RETRY__)

#define ABORT_ON_ALL_COMM_STATUS()      _set_comm_status(0x7F,__ABORT__)
#define IGNORE_ALL_COMM_STATUS()        _set_comm_status(0x7F,__IGNORE__)
#define RETRY_ON_ALL_COMM_STATUS()      _set_comm_status(0x7F,__RETRY__)

#define ABORT_ON_COMM_STATUS(comm_stat) _set_comm_status((comm_stat),__ABORT__)
#define IGNORE_COMM_STATUS(comm_stat)   _set_comm_status((comm_stat),__IGNORE__)
#define RETRY_ON_COMM_STATUS(comm_stat) _set_comm_status((comm_stat),__RETRY__)


#define ABORT_ON_ALL_DEVICE_STATUS()    _set_device_status(0xFF,__ABORT__)
#define IGNORE_ALL_DEVICE_STATUS()      _set_device_status(0xFF,__IGNORE__)
#define RETRY_ON_ALL_DEVICE_STATUS()    _set_device_status(0xFF,__RETRY__)

#define ABORT_ON_DEVICE_STATUS(dev_st)  _set_device_status((dev_st),__ABORT__)
#define IGNORE_DEVICE_STATUS(dev_st)    _set_device_status((dev_st),__IGNORE__)
#define RETRY_ON_DEVICE_STATUS(dev_st)  _set_device_status((dev_st),__RETRY__)


#define ABORT_ON_ALL_RESPONSE_CODES()   _set_all_resp_code(__ABORT__)
#define IGNORE_ALL_RESPONSE_CODES()     _set_all_resp_code(__IGNORE__)
#define RETRY_ON_ALL_RESPONSE_CODES()   _set_all_resp_code(__RETRY__)

#define ABORT_ON_RESPONSE_CODE(rsp_cod) _set_resp_code((rsp_cod),__ABORT__)
#define IGNORE_RESPONSE_CODE(rsp_cod)   _set_resp_code((rsp_cod),__IGNORE__)
#define RETRY_ON_RESPONSE_CODE(rsp_cod) _set_resp_code((rsp_cod),__RETRY__)


#define ABORT_ON_NO_DEVICE()            _set_no_device(__ABORT__)
#define IGNORE_NO_DEVICE()              _set_no_device(__IGNORE__)
#define RETRY_ON_NO_DEVICE()            _set_no_device(__RETRY__)


/*
 *  XMTR - Abort, Ignore, Retry functions
 */
#define __IGNORE__  0
#define __ABORT__   1
#define __RETRY__   2

#define XMTR_ABORT_ON_COMM_ERROR()      _set_xmtr_comm_status(0xFF,__ABORT__)
#define XMTR_IGNORE_COMM_ERROR()        _set_xmtr_comm_status(0xFF,__IGNORE__)
#define XMTR_RETRY_ON_COMM_ERROR()      _set_xmtr_comm_status(0xFF,__RETRY__)


#define XMTR_ABORT_ON_ALL_COMM_STATUS() _set_xmtr_comm_status(0x7F,__ABORT__)
#define XMTR_IGNORE_ALL_COMM_STATUS()   _set_xmtr_comm_status(0x7F,__IGNORE__)
#define XMTR_RETRY_ON_ALL_COMM_STATUS() _set_xmtr_comm_status(0x7F,__RETRY__)

#define XMTR_ABORT_ON_COMM_STATUS(comm_st)  _set_xmtr_comm_status((comm_st),__ABORT__)
#define XMTR_IGNORE_COMM_STATUS(comm_st)    _set_xmtr_comm_status((comm_st),__IGNORE__)
#define XMTR_RETRY_ON_COMM_STATUS(comm_st)  _set_xmtr_comm_status((comm_st),__RETRY__)


#define XMTR_ABORT_ON_ALL_DEVICE_STATUS()   _set_xmtr_device_status(0xFF,__ABORT__)
#define XMTR_IGNORE_ALL_DEVICE_STATUS()     _set_xmtr_device_status(0xFF,__IGNORE__)
#define XMTR_RETRY_ON_ALL_DEVICE_STATUS()   _set_xmtr_device_status(0xFF,__RETRY__)

#define XMTR_ABORT_ON_DEVICE_STATUS(dev_st) _set_xmtr_device_status((dev_st),__ABORT__)
#define XMTR_IGNORE_DEVICE_STATUS(dev_st)   _set_xmtr_device_status((dev_st),__IGNORE__)
#define XMTR_RETRY_ON_DEVICE_STATUS(dev_st) _set_xmtr_device_status((dev_st),__RETRY__)


#define XMTR_ABORT_ON_ALL_RESPONSE_CODES()  _set_xmtr_all_resp_code(__ABORT__)
#define XMTR_IGNORE_ALL_RESPONSE_CODES()    _set_xmtr_all_resp_code(__IGNORE__)
#define XMTR_RETRY_ON_ALL_RESPONSE_CODES()  _set_xmtr_all_resp_code(__RETRY__)

#define XMTR_ABORT_ON_RESPONSE_CODE(rsp_cd) _set_xmtr_resp_code((rsp_cd),__ABORT__)
#define XMTR_IGNORE_RESPONSE_CODE(rsp_cd)   _set_xmtr_resp_code((rsp_cd),__IGNORE__)
#define XMTR_RETRY_ON_RESPONSE_CODE(rsp_cd) _set_xmtr_resp_code((rsp_cd),__RETRY__)


#define XMTR_ABORT_ON_NO_DEVICE()       _set_xmtr_no_device(__ABORT__)
#define XMTR_IGNORE_NO_DEVICE()         _set_xmtr_no_device(__IGNORE__)
#define XMTR_RETRY_ON_NO_DEVICE()       _set_xmtr_no_device(__RETRY__)


#define XMTR_ABORT_ON_ALL_DATA()        _set_xmtr_all_data(__ABORT__)
#define XMTR_IGNORE_ALL_DATA()          _set_xmtr_all_data(__IGNORE__)
#define XMTR_RETRY_ON_ALL_DATA()        _set_xmtr_all_data(__RETRY__)

#define XMTR_ABORT_ON_DATA(byte,bit)    _set_xmtr_data((byte),(bit),__ABORT__)
#define XMTR_IGNORE_DATA(byte,bit)      _set_xmtr_data((byte),(bit),__IGNORE__)
#define XMTR_RETRY_ON_DATA(byte,bit)    _set_xmtr_data((byte),(bit),__RETRY__)


/*
 * Math Constants.
 */
#ifndef PI
#define PI      M_PI
#endif


/*
 * These macros have been historically included with Berkley UNIX (BSD)
 */
#define M_E        2.7182818284590452354        /* e The base of natural logarithms */
#define M_LOG2E    1.4426950408889634074        /* log 2(e) The logarithm to base 2 of e */
#define M_LOG10E   0.43429448190325182765       /* log 10(e) The logarithm to base 10 of e */
#define M_LN2      0.69314718055994530942       /* ln(2) The natural logarithm of 2 */
#define M_LN10     2.30258509299404568402       /* ln(10) The natural logarithm of 10 */
#define M_PI       3.14159265358979323846       /* pi The ratio of a circle's circumference to its diameter */
#define M_PI_2     1.57079632679489661923       /* pi/2 Pi divided by two */
#define M_PI_4     0.78539816339744830962       /* pi/4 Pi divided by four */
#define M_1_PI     0.31830988618379067154       /* 1/pi The reciprocal of pi */
#define M_2_PI     0.63661977236758134308       /* 2/pi Two times the reciprocal of pi */
#define M_2_SQRTPI 1.12837916709551257390       /* 2/sqrt(pi) Two times the reciprocal of the square root of pi */
#define M_SQRT2    1.41421356237309504880       /* sqrt(2) The square root of two */
#define M_SQRT1_2  0.70710678118654752440       /* 1/sqrt(2) The reciprocal of the square root of two (also the square root of 1/2) */

#endif

