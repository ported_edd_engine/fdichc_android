
/******************************************************************************
 * $Header: sm_strfn.cpp: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:54 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *													   
 * $Workfile: sm_strfn.cpp$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#include "STDINC.H"
#include "DeviceTypeMgr.h"


#include "cm_loc.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemId. This function is passed to qsort
//////////////////////////////////////////////////////////////////////////////

int  symidtbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	// Implementation note: item_id is an unsigned long, so simple subtraction
	// yields strange results. Just do the compares and return -1, 0, or 1

	if (elem1->SymbolId < elem2->SymbolId)
		return -1;
	else if (elem1->SymbolId > elem2->SymbolId)
		return 1;
	else
		return 0;
}




//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemName. This function is passed to qsort
//////////////////////////////////////////////////////////////////////////////

int  symnametbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	return wcscmp(elem1->SymbolName, elem2->SymbolName);
}



/*********************************************************************************
	read_sym_file
	read sym file and stores the dd-id's and corresponding names in memory
**********************************************************************************/
int CConnectionMgr::Fill_Syminfo(DEVICE_TYPE_HANDLE dth, SYMINFO* syminfo)
{
	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir = NULL;

	int rc = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(dth, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	
	// Get a pointer to the Symbol Table which is now unpacked from the DD
	nsEDDEngine::SYMBOL_TBL *pSymbolTable = &(flat_device_dir->symbol_tbl);

	int cntTotalSyms = pSymbolTable->count;	// Find out how many symbols we have

	nsEDDEngine::SYMBOL_TBL_ELEM* pSymElem	= pSymbolTable->list;	// Set up table pointer

	int cntMemberName = 0;		// Number of MemberName symbols
	int cntItemName = 0;		// Number of ItemName symbols
	for (int i = 0; i < cntTotalSyms; i++, pSymElem++)	// Loop through and find symbol types
	{
		if (pSymElem->type == MEMBER_ITYPE)
		{
			cntMemberName++;
		}
		else
		{
			cntItemName++;
		}
	}

	ASSERT(cntMemberName + cntItemName == cntTotalSyms);


	syminfo->symIdTbl			= new SYMTBL[ cntTotalSyms ];	// Allocate SYMTBL arrays
	syminfo->symItemNameTbl		= new SYMTBL[ cntItemName ];
	syminfo->symMemberNameTbl	= new SYMTBL[ cntMemberName ];

	pSymElem				= pSymbolTable->list;		// Set up table pointers
	SYMTBL* pIdTblElem		= syminfo->symIdTbl;
	SYMTBL* pItemTblElem	= syminfo->symItemNameTbl;
	SYMTBL* pMemberTblElem	= syminfo->symMemberNameTbl;

	for (int i = 0; i < cntTotalSyms; i++, pSymElem++, pIdTblElem++ )
	{
		// Add to the SymbolId Table
		pIdTblElem->SymbolName	= pSymElem->name;
		pIdTblElem->SymbolId	= pSymElem->symbol_id;

		// Add to the correct SymbolName table
		if (pSymElem->type == MEMBER_ITYPE)
		{
			pMemberTblElem->SymbolName	= pSymElem->name;
			pMemberTblElem->SymbolId	= pSymElem->symbol_id;
			pMemberTblElem++;
		}
		else
		{
			pItemTblElem->SymbolName	= pSymElem->name;
			pItemTblElem->SymbolId		= pSymElem->symbol_id;
			pItemTblElem++;
		}
	}

	//sort by Symbol ID
	qsort(syminfo->symIdTbl, cntTotalSyms, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symidtbl_compare);
	syminfo->numIdElem = cntTotalSyms;

	//sort by Item Name
	qsort(syminfo->symItemNameTbl, cntItemName, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symnametbl_compare);
	syminfo->numItemNameElem = cntItemName;
	
	//sort by Symbol Name
	qsort(syminfo->symMemberNameTbl, cntMemberName, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symnametbl_compare);
	syminfo->numMemberNameElem = cntMemberName;
	
// FD Comment Out
#ifdef DEBUG
	USES_CONVERSION;

	DD_DEVICE_ID dd_device_id = {0};
	g_DeviceTypeMgr.get_adtt_dd_device_id(dth, &dd_device_id);

	wchar_t symfile_name[MAX_PATH];
	PS_Wcscpy(symfile_name, MAX_PATH, dd_device_id.dd_file_path);

	wchar_t* pExt = wcsrchr(symfile_name, L'.');		// Find extension position

	int iFilenameSize = (pExt - symfile_name);				// Calculate size of filename minus extension

	PS_Wcscpy(pExt, MAX_PATH - iFilenameSize , L".dbg");		// Copy in the new extension

	FILE *symfile = _wfopen (symfile_name, L"w");
    if (symfile != NULL)
    {
	    fprintf(symfile,  "*********************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY ID      \n");
	    fprintf(symfile,  "*********************************\n");
	    for (int i = 0; i < cntTotalSyms; i++) {
		    fprintf(symfile, "%10X            %ws\n", syminfo->symIdTbl[i].SymbolId, 
				    (LPCWSTR)syminfo->symIdTbl[i].SymbolName);
	    }
	    fprintf(symfile,  "\n");
	    fprintf(symfile,  "=================================\n\n");

	    fprintf(symfile,  "**************************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY ITEM NAME    \n");
	    fprintf(symfile,  "**************************************\n");
	    for (int j = 0; j < cntItemName; j++) {
		    fprintf(symfile, "%10X            %ws\n", syminfo->symItemNameTbl[j].SymbolId, 
				    (LPCWSTR)syminfo->symItemNameTbl[j].SymbolName);
	    }
	    fprintf(symfile,  "\n");
	    fprintf(symfile,  "=================================\n\n");

	    fprintf(symfile,  "*****************************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY MEMBER NAME     \n");
	    fprintf(symfile,  "*****************************************\n");
	    for (int j = 0; j < cntMemberName; j++) {
		    fprintf(symfile, "%10X            %ws\n", syminfo->symMemberNameTbl[j].SymbolId, 
				    (LPCWSTR)syminfo->symMemberNameTbl[j].SymbolName);
	    }

	    (void) fclose (symfile);
    }

#endif

	return SUCCESS;
}


void Free_Syminfo(SYMINFO* syminfo)
{
	delete [] syminfo->symIdTbl;
	syminfo->symIdTbl = NULL;
	syminfo->numIdElem = 0;

	delete [] syminfo->symItemNameTbl;
	syminfo->symItemNameTbl = NULL;
	syminfo->numItemNameElem = 0;

	delete [] syminfo->symMemberNameTbl;
	syminfo->symMemberNameTbl = NULL;
	syminfo->numMemberNameElem = 0;
}


//
// Converts item_id to item-name. 
//

int 
server_item_id_to_name(SYMINFO* syminfo, ITEM_ID item_id, LPTSTR item_name, int outbuf_size)
{
	if (syminfo == NULL)
	{
		return CM_BAD_POINTER;
	}

	int	low = 0;
	int med = 0;
	int	high = syminfo->numIdElem - 1;

	SYMTBL* symtbl = syminfo->symIdTbl;

	while(low <= high) 
	{
		med = (low + high) / 2;

		if(item_id > symtbl[med].SymbolId)
		{
			low = med + 1;
		}
		else if(item_id < symtbl[med].SymbolId)
		{
			high = med - 1;
		}
		else 
		{
			
			if( ( size_t) outbuf_size <= wcslen( symtbl[med].SymbolName ) )
			{
				item_name[0] = '\0';
				return DDI_INSUFFICIENT_BUFFER;
			}
			else
			{
				PS_Wcscpy( item_name, outbuf_size, symtbl[med].SymbolName );
			}
			return SUCCESS;
		}
	}

	return SM_ID_NOT_FOUND;
}


int
item_id_to_name(BLOCK_HANDLE bh, ENV_INFO *env_info, ITEM_ID item_id, LPTSTR item_name, int outbuf_size )
{
	int rc = PC_SUCCESS;
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	if (!pConnectionMgr->valid_block_handle(bh))
	{
		return CM_BAD_BLOCK_HANDLE;
	}

	DEVICE_TYPE_HANDLE dth = pConnectionMgr->ABT_ADTT_OFFSET(bh);

	if (!g_DeviceTypeMgr.valid_device_type_handle(dth))
	{
		return CM_BAD_DEVICE_TYPE_HANDLE;
	}

	rc = server_item_id_to_name(g_DeviceTypeMgr.ADTT_SYMBOL_INFO(dth), item_id, item_name, outbuf_size);

	return rc;
}

//
// Finds the id in the SYMTBL, by looking up the symbol name
//

static int 
find_id_from_name(SYMTBL* symtbl, int numElem, LPCTSTR symbol_name, ITEM_ID *item_id)
{
	int iCmp = 0;

	int	low = 0;
	int med = 0;
	int	high = numElem - 1;

	while (low <= high)
	{
		med = (low + high) / 2;

		iCmp = wcscmp(symbol_name, symtbl[med].SymbolName);

		if (iCmp > 0) 
		{
			low = med + 1;
		}
		else if (iCmp < 0) 
		{
			high = med - 1;
		}
		else
		{
			*item_id = symtbl[med].SymbolId;
			return SUCCESS;
		}
	}

	return SM_NAME_NOT_FOUND;
}

//
// Converts item-name to item_id.
//

int 
server_item_name_to_id(SYMINFO* syminfo, LPCTSTR item_name, ITEM_ID *item_id)
{
	int rc = SM_NAME_NOT_FOUND;
	if( syminfo )
	{
		rc = find_id_from_name(syminfo->symItemNameTbl, syminfo->numItemNameElem, item_name, item_id);

		if (rc == SM_NAME_NOT_FOUND)	// If not found, try getting this from the MemberNameTbl.
		{
			rc = find_id_from_name(syminfo->symMemberNameTbl, syminfo->numMemberNameElem, item_name, item_id);
		}
	}
	return rc;
}

//
// Converts member-name to item_id.
//

int 
server_member_name_to_id(SYMINFO* syminfo, LPCTSTR member_name, ITEM_ID *item_id)
{
	int rc = find_id_from_name(syminfo->symMemberNameTbl, syminfo->numMemberNameElem, member_name, item_id);
	return rc;
}

//To-Do: this method needs to be fixed... just commenting out to get by for now.
//int build_methd_def_from_action_methd_ref_with_args(BLOCK_HANDLE bh, ACTION* action_ptr, CStdString *psMethdDef)
//{
//	int rc = SUCCESS;
//
//	if (action_ptr->type != nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REF_WITH_ARGS)
//	{
//		ntcassert2(0);
//		rc = SM_UNSUPPORTED_FEATURE_IN_DD;
//	}
//	else
//	{
//
//		wchar_t name_buf[64] = {0};	// Buffer for the name
//		rc = item_id_to_name(bh, action_ptr->action.meth_ref_args.op_id, name_buf, (sizeof(name_buf)/sizeof(name_buf[0])));
//
//		if (rc == SUCCESS)
//		{
//			CStdString sName(name_buf);
//			CStdString sArgument(action_ptr->action.meth_ref_args.expr.val.s.str, action_ptr->action.meth_ref_args.expr.val.s.len);
//			*psMethdDef = _T("{") + sName + _T("(") + sArgument + _T(");}");
//		}
//	}
//
//	return rc;
//}

//int build_methd_def_from_menu_methd_ref_with_args(BLOCK_HANDLE bh, MENU_ITEM* menu_ptr, CStdString *psMethdDef)
//{
//	int rc = SUCCESS;
//
//	wchar_t name_buf[64] = {0};	// Buffer for the name
//	rc = item_id_to_name(bh, menu_ptr->ref.desc_id, name_buf);
//
//	if (rc == SUCCESS)
//	{
//		CStdString sName(name_buf);
//		CStdString sArgument(menu_ptr->ref.expr.val.s.str, menu_ptr->ref.expr.val.s.len);
//		*psMethdDef = _T("{") + sName + _T("(") + sArgument + _T(");}");
//	}
//
//	return rc;
//}
//
////////////////////////////////////////////////////////////////////////////

