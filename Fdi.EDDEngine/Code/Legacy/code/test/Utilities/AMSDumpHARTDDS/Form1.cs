
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Diagnostics;
using System.Text;
using HARTDDSCOM = Emerson.AMS.Public.HARTDDSCOM.Interop;



namespace AMSDumpHARTDDS
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
    {
        private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox fm8File;
		private System.Windows.Forms.TextBox OutPutFile;
		private System.Windows.Forms.Button btnGenXML;
		private System.Windows.Forms.Button btnCancel;
        private GroupBox groupBox4;
        private TextBox Graphs;
        private TextBox Commands;
        private TextBox Menus;
        private TextBox Methods;
        private TextBox Variables;
        private TextBox Members;
        private TextBox Grids;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Button DDBrowseButton;
		private WebBrowser webBrowser1;
        private Button button1;
        private TextBox textBox2;
        private Label label9;
        private ComboBox comboBox1;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn parameter1;
        private DataGridViewTextBoxColumn Value;
        private GroupBox groupBox5;
        private TextBox textBox1;
        private Button button2;
        private TextBox txtCompareStatus;
        private ListBox lbFileAndMemoryFootprint;
        private Label label8;
        private Label label10;
        private Label label11;
        private HARTDDSCOM.IGetHARTDDSXML m_loader;
        private Button btnOnlyEDDInstance;
        private Button btnThreadSafetyFiles;
        private Button btnDeleteOneElement;
        private Label label12;
        private Button btnTestTimeMemory;
        private Button btnTimingMemoryFixedFiles;
        private Button btnCopyTestDataToClipboard;
        private Button btnLoad10000Instances;
        private CheckBox chkLegacy;
        private Button btn10xTiming;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.fm8File = new System.Windows.Forms.TextBox();
            this.OutPutFile = new System.Windows.Forms.TextBox();
            this.DDBrowseButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btnGenXML = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Commands = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Graphs = new System.Windows.Forms.TextBox();
            this.Menus = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Grids = new System.Windows.Forms.TextBox();
            this.Variables = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Members = new System.Windows.Forms.TextBox();
            this.Methods = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.parameter1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbFileAndMemoryFootprint = new System.Windows.Forms.ListBox();
            this.txtCompareStatus = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnOnlyEDDInstance = new System.Windows.Forms.Button();
            this.btnThreadSafetyFiles = new System.Windows.Forms.Button();
            this.btnDeleteOneElement = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnTestTimeMemory = new System.Windows.Forms.Button();
            this.btnTimingMemoryFixedFiles = new System.Windows.Forms.Button();
            this.btnCopyTestDataToClipboard = new System.Windows.Forms.Button();
            this.btnLoad10000Instances = new System.Windows.Forms.Button();
            this.chkLegacy = new System.Windows.Forms.CheckBox();
            this.btn10xTiming = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // fm8File
            // 
            this.fm8File.Location = new System.Drawing.Point(20, 30);
            this.fm8File.Name = "fm8File";
            this.fm8File.Size = new System.Drawing.Size(639, 20);
            this.fm8File.TabIndex = 1;
            this.fm8File.TextChanged += new System.EventHandler(this.fm8File_TextChanged);
            // 
            // OutPutFile
            // 
            this.OutPutFile.Location = new System.Drawing.Point(20, 84);
            this.OutPutFile.Name = "OutPutFile";
            this.OutPutFile.Size = new System.Drawing.Size(639, 20);
            this.OutPutFile.TabIndex = 2;
            // 
            // DDBrowseButton
            // 
            this.DDBrowseButton.Location = new System.Drawing.Point(584, 55);
            this.DDBrowseButton.Name = "DDBrowseButton";
            this.DDBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.DDBrowseButton.TabIndex = 7;
            this.DDBrowseButton.Text = "Browse";
            this.DDBrowseButton.Click += new System.EventHandler(this.DDBrowseButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.webBrowser1);
            this.groupBox3.Location = new System.Drawing.Point(20, 327);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(723, 160);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(5, 18);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(712, 126);
            this.webBrowser1.TabIndex = 0;
            // 
            // btnGenXML
            // 
            this.btnGenXML.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenXML.Location = new System.Drawing.Point(741, 638);
            this.btnGenXML.Name = "btnGenXML";
            this.btnGenXML.Size = new System.Drawing.Size(91, 24);
            this.btnGenXML.TabIndex = 6;
            this.btnGenXML.Text = "Generate XML";
            this.btnGenXML.Click += new System.EventHandler(this.Ok_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(836, 639);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.Commands);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.Graphs);
            this.groupBox4.Controls.Add(this.Menus);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.Grids);
            this.groupBox4.Controls.Add(this.Variables);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.Members);
            this.groupBox4.Controls.Add(this.Methods);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(762, 30);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(146, 302);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Item Counts";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "# of Grids";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "# of Menus";
            // 
            // Commands
            // 
            this.Commands.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Commands.Location = new System.Drawing.Point(37, 151);
            this.Commands.Name = "Commands";
            this.Commands.Size = new System.Drawing.Size(100, 20);
            this.Commands.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "# of Graphs";
            // 
            // Graphs
            // 
            this.Graphs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Graphs.Location = new System.Drawing.Point(37, 190);
            this.Graphs.Name = "Graphs";
            this.Graphs.Size = new System.Drawing.Size(100, 20);
            this.Graphs.TabIndex = 4;
            // 
            // Menus
            // 
            this.Menus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Menus.Location = new System.Drawing.Point(37, 112);
            this.Menus.Name = "Menus";
            this.Menus.Size = new System.Drawing.Size(100, 20);
            this.Menus.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "# of Members";
            // 
            // Grids
            // 
            this.Grids.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Grids.Location = new System.Drawing.Point(37, 230);
            this.Grids.Name = "Grids";
            this.Grids.Size = new System.Drawing.Size(100, 20);
            this.Grids.TabIndex = 5;
            // 
            // Variables
            // 
            this.Variables.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Variables.Location = new System.Drawing.Point(37, 32);
            this.Variables.Name = "Variables";
            this.Variables.Size = new System.Drawing.Size(100, 20);
            this.Variables.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "# of Commands";
            // 
            // Members
            // 
            this.Members.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Members.Location = new System.Drawing.Point(37, 270);
            this.Members.Name = "Members";
            this.Members.Size = new System.Drawing.Size(100, 20);
            this.Members.TabIndex = 6;
            // 
            // Methods
            // 
            this.Methods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Methods.Location = new System.Drawing.Point(37, 72);
            this.Methods.Name = "Methods";
            this.Methods.Size = new System.Drawing.Size(100, 20);
            this.Methods.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "# of Variables";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "# of Methods";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(142, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Find";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox2.Location = new System.Drawing.Point(491, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(219, 20);
            this.textBox2.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(488, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Output:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "GetErrorString",
            "GetCmdIdFromNumber",
            "GetItemType",
            "GetItemTypeAndItemId",
            "GetAxisUnitRelItemId",
            "GetParamUnitRelItemId"});
            this.comboBox1.Location = new System.Drawing.Point(15, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.parameter1,
            this.Value});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(231, 10);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(245, 124);
            this.dataGridView1.TabIndex = 15;
            // 
            // parameter1
            // 
            this.parameter1.HeaderText = "ParameterName";
            this.parameter1.Name = "parameter1";
            this.parameter1.ReadOnly = true;
            // 
            // Value
            // 
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.comboBox1);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.dataGridView1);
            this.groupBox5.Location = new System.Drawing.Point(20, 486);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(723, 140);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "UnitTest";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(142, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 20);
            this.button2.TabIndex = 17;
            this.button2.Text = "Auto Run";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.Location = new System.Drawing.Point(491, 64);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(219, 20);
            this.textBox1.TabIndex = 16;
            this.textBox1.Visible = false;
            // 
            // lbFileAndMemoryFootprint
            // 
            this.lbFileAndMemoryFootprint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFileAndMemoryFootprint.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFileAndMemoryFootprint.FormattingEnabled = true;
            this.lbFileAndMemoryFootprint.HorizontalScrollbar = true;
            this.lbFileAndMemoryFootprint.ItemHeight = 11;
            this.lbFileAndMemoryFootprint.Location = new System.Drawing.Point(20, 132);
            this.lbFileAndMemoryFootprint.Name = "lbFileAndMemoryFootprint";
            this.lbFileAndMemoryFootprint.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbFileAndMemoryFootprint.Size = new System.Drawing.Size(723, 180);
            this.lbFileAndMemoryFootprint.TabIndex = 17;
            // 
            // txtCompareStatus
            // 
            this.txtCompareStatus.Location = new System.Drawing.Point(439, 107);
            this.txtCompareStatus.Name = "txtCompareStatus";
            this.txtCompareStatus.ReadOnly = true;
            this.txtCompareStatus.Size = new System.Drawing.Size(216, 20);
            this.txtCompareStatus.TabIndex = 3;
            this.txtCompareStatus.TabStop = false;
            this.txtCompareStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCompareStatus.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "DD Binary to parse";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Output File Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Test Data";
            // 
            // btnOnlyEDDInstance
            // 
            this.btnOnlyEDDInstance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOnlyEDDInstance.Location = new System.Drawing.Point(749, 364);
            this.btnOnlyEDDInstance.Name = "btnOnlyEDDInstance";
            this.btnOnlyEDDInstance.Size = new System.Drawing.Size(158, 26);
            this.btnOnlyEDDInstance.TabIndex = 21;
            this.btnOnlyEDDInstance.Text = "Load one EDD Instance";
            this.btnOnlyEDDInstance.UseVisualStyleBackColor = true;
            this.btnOnlyEDDInstance.Click += new System.EventHandler(this.btnOnlyEDDInstance_Click);
            // 
            // btnThreadSafetyFiles
            // 
            this.btnThreadSafetyFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThreadSafetyFiles.Location = new System.Drawing.Point(749, 478);
            this.btnThreadSafetyFiles.Name = "btnThreadSafetyFiles";
            this.btnThreadSafetyFiles.Size = new System.Drawing.Size(158, 26);
            this.btnThreadSafetyFiles.TabIndex = 23;
            this.btnThreadSafetyFiles.Text = "Thread Safety Files Test";
            this.btnThreadSafetyFiles.UseVisualStyleBackColor = true;
            this.btnThreadSafetyFiles.Click += new System.EventHandler(this.btnThreadSafetyFiles_Click);
            // 
            // btnDeleteOneElement
            // 
            this.btnDeleteOneElement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteOneElement.Location = new System.Drawing.Point(749, 393);
            this.btnDeleteOneElement.Name = "btnDeleteOneElement";
            this.btnDeleteOneElement.Size = new System.Drawing.Size(158, 26);
            this.btnDeleteOneElement.TabIndex = 24;
            this.btnDeleteOneElement.Text = "Delete one EDD instance";
            this.btnDeleteOneElement.UseVisualStyleBackColor = true;
            this.btnDeleteOneElement.Click += new System.EventHandler(this.btnDeleteOneElement_Click);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(749, 345);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "File Sharing Tests";
            // 
            // btnTestTimeMemory
            // 
            this.btnTestTimeMemory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestTimeMemory.Location = new System.Drawing.Point(749, 507);
            this.btnTestTimeMemory.Name = "btnTestTimeMemory";
            this.btnTestTimeMemory.Size = new System.Drawing.Size(158, 26);
            this.btnTestTimeMemory.TabIndex = 26;
            this.btnTestTimeMemory.Text = "Timing and memory test";
            this.btnTestTimeMemory.UseVisualStyleBackColor = true;
            this.btnTestTimeMemory.Click += new System.EventHandler(this.btnTestTimeMemory_Click);
            // 
            // btnTimingMemoryFixedFiles
            // 
            this.btnTimingMemoryFixedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimingMemoryFixedFiles.Location = new System.Drawing.Point(749, 536);
            this.btnTimingMemoryFixedFiles.Name = "btnTimingMemoryFixedFiles";
            this.btnTimingMemoryFixedFiles.Size = new System.Drawing.Size(158, 40);
            this.btnTimingMemoryFixedFiles.TabIndex = 27;
            this.btnTimingMemoryFixedFiles.Text = "Timing and memory test of fixed file list";
            this.btnTimingMemoryFixedFiles.UseVisualStyleBackColor = true;
            this.btnTimingMemoryFixedFiles.Click += new System.EventHandler(this.btnTimingMemoryFixedFiles_Click);
            // 
            // btnCopyTestDataToClipboard
            // 
            this.btnCopyTestDataToClipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyTestDataToClipboard.Location = new System.Drawing.Point(749, 579);
            this.btnCopyTestDataToClipboard.Name = "btnCopyTestDataToClipboard";
            this.btnCopyTestDataToClipboard.Size = new System.Drawing.Size(162, 23);
            this.btnCopyTestDataToClipboard.TabIndex = 28;
            this.btnCopyTestDataToClipboard.Text = "Copy Test Data to Clipboard";
            this.btnCopyTestDataToClipboard.UseVisualStyleBackColor = true;
            this.btnCopyTestDataToClipboard.Click += new System.EventHandler(this.btnCopyTestDataToClipboard_Click);
            // 
            // btnLoad10000Instances
            // 
            this.btnLoad10000Instances.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad10000Instances.Location = new System.Drawing.Point(749, 423);
            this.btnLoad10000Instances.Name = "btnLoad10000Instances";
            this.btnLoad10000Instances.Size = new System.Drawing.Size(158, 25);
            this.btnLoad10000Instances.TabIndex = 29;
            this.btnLoad10000Instances.Text = "Load 10000 EDD Instances";
            this.btnLoad10000Instances.UseVisualStyleBackColor = true;
            this.btnLoad10000Instances.Click += new System.EventHandler(this.btnLoad10000Instances_Click);
            // 
            // chkLegacy
            // 
            this.chkLegacy.AutoSize = true;
            this.chkLegacy.Location = new System.Drawing.Point(666, 58);
            this.chkLegacy.Name = "chkLegacy";
            this.chkLegacy.Size = new System.Drawing.Size(61, 17);
            this.chkLegacy.TabIndex = 30;
            this.chkLegacy.Text = "Legacy";
            this.chkLegacy.UseVisualStyleBackColor = true;
            // 
            // btn10xTiming
            // 
            this.btn10xTiming.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn10xTiming.Location = new System.Drawing.Point(497, 639);
            this.btn10xTiming.Name = "btn10xTiming";
            this.btn10xTiming.Size = new System.Drawing.Size(158, 26);
            this.btn10xTiming.TabIndex = 31;
            this.btn10xTiming.Text = "10x Timing and memory test";
            this.btn10xTiming.UseVisualStyleBackColor = true;
            this.btn10xTiming.Click += new System.EventHandler(this.btn10xTiming_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(914, 668);
            this.Controls.Add(this.btn10xTiming);
            this.Controls.Add(this.chkLegacy);
            this.Controls.Add(this.btnLoad10000Instances);
            this.Controls.Add(this.btnCopyTestDataToClipboard);
            this.Controls.Add(this.btnTimingMemoryFixedFiles);
            this.Controls.Add(this.btnTestTimeMemory);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnDeleteOneElement);
            this.Controls.Add(this.btnThreadSafetyFiles);
            this.Controls.Add(this.btnOnlyEDDInstance);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbFileAndMemoryFootprint);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnGenXML);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.DDBrowseButton);
            this.Controls.Add(this.fm8File);
            this.Controls.Add(this.txtCompareStatus);
            this.Controls.Add(this.OutPutFile);
            this.Name = "Form1";
            this.Text = "DDS - Dump Binary";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
 
        	/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
        static void Main(string[] args)
        {
            string sInputFileName = "";
            string sOutputFileName = "";

            if (args.Length > 0)
            {
                sInputFileName = args[0];
            }
            if (args.Length > 1)
            {
                sOutputFileName = args[1];
            }
            if (sInputFileName.Length == 0)
            {
                Application.Run(new Form1());
            }
            else if (sInputFileName.Length > 0 && sOutputFileName.Length > 0)
            {
                if (sInputFileName.ToLower().EndsWith(".pbo"))
                {
                    //string soutput;
                    //int error;

                    //Call COM object
// MHD                    ProfibusDDSCOM.AMSProfibusDDSCOM loader = new ProfibusDDSCOM.AMSProfibusDDSCOMClass();
					//soutput = loader.GetDDSInfo(sInputFileName, out error);

					////Send XML output from DD to ".xml" file of specified path and filename 
					//FileStream pFileName = new FileStream(sOutputFileName, FileMode.Create, FileAccess.ReadWrite);
					//StreamWriter sw = new StreamWriter(pFileName);
					//sw.Write(soutput);
					//sw.Close();
                }
                else
                {
                    string soutput;
 
                    //Call COM object
                    HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();
                    loader.GetDDSInfo(sInputFileName, out soutput);

                    //Send XML output from DD to ".xml" file of specified path and filename 
                    FileStream pFileName = new FileStream(sOutputFileName, FileMode.Create, FileAccess.ReadWrite);
                    StreamWriter sw = new StreamWriter(pFileName);
                    sw.Write(soutput);
                    sw.Close();
                }
            }
            else
            {
                MessageBox.Show("If No arguments are passed this application will display a UI that allows the user to select an input and output file.\n Otherwise this application takes two arguments\n The first argument is the input file (this should be a path to an fms or fm6 file).  \nThe second argument should be a path to an output file, usually an xml file.");
            }
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			fm8File.Text = "..\\Devices\\HART\\000026\\0006\\0709.fm8";

            m_loader = new HARTDDSCOM.CGetHARTDDSXML();
            //dataGridView1.Rows.Add();
		}

		private void Ok_Click(object sender, System.EventArgs e)
		{
			try
			{
				string soutput = "";
				string sExplicitAddress = "";
				string sfilename = fm8File.Text;

				// Initialize UI to initial defaults
				txtCompareStatus.Visible = false;
				webBrowser1.Visible = false;
				this.Update();

                Stopwatch stopWatch = new Stopwatch();
              //LIST OF FILES  for (i = 0; i < 20; i++)
                {

                    m_loader.SetLoadXML(true);
                    stopWatch.Start();


                    // Call the EDD Engine
                    m_loader.GetDDSInfo(sfilename, out soutput);// Set a BOOL to shut off XML generation.


                    stopWatch.Stop();  
                    // Get the elapsed time as a TimeSpan value.
                    TimeSpan ts = stopWatch.Elapsed;  
                    // Format and display the TimeSpan value. 
                    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds);

                    string sShortFilename;      // Trim down the filename to 65 characters
                    int iFileLen = sfilename.Length;
                    if (iFileLen > 65)
                    {
                        sShortFilename = "..." + sfilename.Substring(iFileLen - 65);
                    }
                    else
                    {
                        sShortFilename = sfilename;
                    }

                    lbFileAndMemoryFootprint.Items.Add("GetDDSInfo: " + sShortFilename + " RunTime: " + elapsedTime);
                    lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;
                }

              

 

				// Save the XML output to a file
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(soutput);

				XmlTextWriter writer = new XmlTextWriter(OutPutFile.Text, null);
				writer.Formatting = Formatting.Indented;
				doc.Save(writer);
				writer.Close();

				// Get FullName of output file for use with webBrowser below
				FileInfo fi = new FileInfo(OutPutFile.Text);
				sExplicitAddress = fi.FullName;

				// Point the webBrowser at the output file
				webBrowser1.Visible = true;
				webBrowser1.Url = new Uri(sExplicitAddress);

				// Get some statistics from this XML
				Variables.Text = GetvariableCount(soutput).ToString();
				Methods.Text = GetmethodCount(soutput).ToString();
				Menus.Text = GetmenuCount(soutput).ToString();
				Commands.Text = GetcommandCount(soutput).ToString();
				Graphs.Text = GetgraphCount(soutput).ToString();
				Grids.Text = GetgridCount(soutput).ToString();
				Members.Text = GetmemberCount(soutput).ToString();

				// If the Expected file exists, do the comparison and display result
				string expectedFile = sfilename + ".Expected.xml";
				if (File.Exists(expectedFile))
				{
					string fileXml = File.ReadAllText(expectedFile);
					string expectedXml = Regex.Replace(fileXml, @"\s", "");

					string compareXml = Regex.Replace(soutput, @"\s", "");

					if (String.Compare(compareXml, expectedXml, false) == 0)
					{
						txtCompareStatus.Text = "Good Comparison with Expected XML!";
						txtCompareStatus.BackColor = Color.PaleGreen;
					}
					else
					{
						txtCompareStatus.Text = "BAD Comparison with Expected XML!";
						txtCompareStatus.BackColor = Color.Red;
					}

					txtCompareStatus.Visible = true;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());

			}
		}

		private void Cancel_Click(object sender, System.EventArgs e)
		{
            //Form1.ActiveForm.Close();
            this.Close();
        }

        private int GetvariableCount( string sInputString )
        {
            int ivariableCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("variable"))
                            {
                                ivariableCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return ivariableCount;
        }

        private int GetmethodCount( string sInputString )
        {
            int imethodCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("method"))
                            {
                                imethodCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return imethodCount;
        }

        private int GetmenuCount( string sInputString )
        {
            int imenuCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("menu"))
                            {
                                imenuCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return imenuCount;
        }

        private int GetcommandCount( string sInputString )
        {
            int icommandCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("command"))
                            {
                                icommandCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return icommandCount;
        }

        private int GetgraphCount( string sInputString )
        {
            int igraphCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("graph"))
                            {
                                igraphCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return igraphCount;
        }

        private int GetgridCount( string sInputString )
        {
            int igridCount = 0;
            XmlDocument sampleXml = new XmlDocument();
            sampleXml.LoadXml( sInputString );
            try
            {
                XmlNodeList topLevel = sampleXml.SelectNodes( "DDODBinary" );
                foreach ( XmlNode secondLevel in topLevel )
                {
                    XmlNodeList thirdLevel = secondLevel.SelectNodes( "DDItemList" );
                    foreach ( XmlNode fourthLevel in thirdLevel )
                    {
                        XmlNodeList fifthLevel = fourthLevel.SelectNodes( "DDItem" );
                        foreach ( XmlNode XmlFinder in fifthLevel )
                        {
                            if (XmlFinder.Attributes.GetNamedItem("Type").Value.Equals("grid"))
                            {
                                igridCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return igridCount;
        }

        private int GetmemberCount( string sInputString )
        {
            int MemberCount = 0;

            try
            {
                XmlDocument sampleXml = new XmlDocument();
                sampleXml.LoadXml( sInputString );
                XmlNodeList xmlDDODBinary = sampleXml.SelectNodes( "DDODBinary" );

                foreach ( XmlNode xmlDDODBin in xmlDDODBinary )
                {
                    XmlNodeList xmlDDItemList = xmlDDODBin.SelectNodes( "DDItemList" );
                    foreach ( XmlNode xmlItemList in xmlDDItemList )
                    {
                        XmlNodeList xmlDDItems = xmlItemList.SelectNodes( "DDItem" );
                        foreach ( XmlNode xmlItem in xmlDDItems )
                        {
                            XmlNodeList xmlMemberItems = xmlItem.SelectNodes( "MemberItem" );
                            foreach ( XmlNode xmlMember in xmlMemberItems )
                            {
                                MemberCount++;
                            }
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            return MemberCount;
        }

        private void DDBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();

            if (chkLegacy.Checked)  // If Legacy is checked, start with the Legacy extension filter
            {
                op.Filter = "All HART or FF Files (*.fm?;*.ff?)|*.fm?;*.ff?" +
                            "|FDI Binary Files (*.fmA;*.ff6;*.bin)|*.fmA;*.ff6;*.bin" +
                            "|All files (*.*)|*.*";
            }
            else                    // If Legacy is not checked, start with the FDI extension filters
            {
                op.Filter = "FDI Binary Files (*.fmA;*.ff6;*.bin;*.is6)|*.fmA;*.ff6;*.bin;*.is6" +
                            "|All HART or FF Files (*.fm?;*.ff?)|*.fm?;*.ff?" +
                            "|All files (*.*)|*.*";
            }
			op.ShowDialog();
            fm8File.Text = op.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sfilename = fm8File.Text;
            //Call COM object
            if (comboBox1.SelectedItem.ToString() == "GetErrorString")
            {
                int nErrorCode = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                string sErrorString = "";
                m_loader.GetErrorString(sfilename, nErrorCode, out sErrorString);          
                label9.Text = "Error String:";
                textBox2.Text = sErrorString;
            }
            else if(comboBox1.SelectedItem.ToString() == "GetCmdIdFromNumber")
            {
                string sCmdId = "";
                uint ulCmdNumber = 0;
                uint pCmdItemId;
                int iBlockInstance = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                ulCmdNumber = Convert.ToUInt32(dataGridView1.Rows[1].Cells[1].FormattedValue.ToString());
                m_loader.GetCmdIdFromNumber(sfilename, iBlockInstance, ulCmdNumber, out pCmdItemId);
                sCmdId = pCmdItemId.ToString();
                label9.Text = "ItemId:";
                textBox2.Text = sCmdId;
            }
            else if (comboBox1.SelectedItem.ToString() == "GetItemType")
            {
                string sItemType = "";
                int eType = 0;
                uint ulid = 0;
                uint sunbindex;
                int paramtype;
                int iBlockInstance = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                eType = Convert.ToInt32(dataGridView1.Rows[1].Cells[1].FormattedValue.ToString());
                ulid = Convert.ToUInt32(dataGridView1.Rows[2].Cells[1].FormattedValue.ToString());
                sunbindex = Convert.ToUInt32(dataGridView1.Rows[3].Cells[1].FormattedValue.ToString());
                m_loader.GetItemType(sfilename, iBlockInstance, eType, ulid, sunbindex, out paramtype);
                sItemType = paramtype.ToString();
                label9.Text = "ItemType:";
                textBox2.Text = sItemType;
            }
            else if (comboBox1.SelectedItem.ToString() == "GetItemTypeAndItemId")
            {
                string sItemId = "";
                string sItemType = "";
                int eType = 0;
                uint ulid = 0;
                uint sunbindex;
                int paramtype = 0;
                uint itemid;
                int iBlockInstance = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                eType = Convert.ToInt32(dataGridView1.Rows[1].Cells[1].FormattedValue.ToString());
                ulid = Convert.ToUInt32(dataGridView1.Rows[2].Cells[1].FormattedValue.ToString());
                sunbindex = Convert.ToUInt32(dataGridView1.Rows[3].Cells[1].FormattedValue.ToString());
                m_loader.GetItemTypeAndItemId(sfilename, iBlockInstance, eType, ulid, sunbindex, out paramtype, out itemid);
                sItemId = itemid.ToString();
                sItemType = paramtype.ToString();
                textBox1.Visible = true;
                label9.Text = "ItemId:";
                textBox2.Text = sItemId;

                textBox1.Text = sItemType;
            }
            else if (comboBox1.SelectedItem.ToString() == "GetAxisUnitRelItemId")
            {
                string sItemId = "";
                uint sunbindex;
                uint itemid;
                int iBlockInstance = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                sunbindex = Convert.ToUInt32(dataGridView1.Rows[1].Cells[1].FormattedValue.ToString());
                m_loader.GetAxisUnitRelItemId(sfilename, iBlockInstance, sunbindex, out itemid);
                sItemId = itemid.ToString();
                label9.Text = "ItemId:";
                textBox2.Text = sItemId;
            }
            else if (comboBox1.SelectedItem.ToString() == "GetParamUnitRelItemId")
            {
                string sItemId = "";
                int eType = 0;
                uint itemid = 0;
                int iBlockInstance = Convert.ToInt32(dataGridView1.Rows[0].Cells[1].FormattedValue.ToString());
                uint ulid = Convert.ToUInt32(dataGridView1.Rows[1].Cells[1].FormattedValue.ToString());
                uint subindex = Convert.ToUInt32(dataGridView1.Rows[2].Cells[1].FormattedValue.ToString());
                m_loader.GetParamUnitRelItemId(sfilename, iBlockInstance, eType, ulid, subindex, out itemid);
                sItemId = itemid.ToString();
                label9.Text = "ItemId:";
                textBox2.Text = sItemId;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            textBox1.Visible = false;
            if (comboBox1.SelectedItem.ToString() == "GetErrorString")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("ErrorID");
            }
            else if (comboBox1.SelectedItem.ToString() == "GetCmdIdFromNumber")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("iBlockInstance");
                dataGridView1.Rows.Add("ulCmdNumber");
            }
            else if (comboBox1.SelectedItem.ToString() == "GetItemType")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("iBlockInstance");
                dataGridView1.Rows.Add("eType");
                dataGridView1.Rows.Add("ulid");
                dataGridView1.Rows.Add("sunbindex");
            }
            else if (comboBox1.SelectedItem.ToString() == "GetItemTypeAndItemId")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("iBlockInstance");
                dataGridView1.Rows.Add("eType");
                dataGridView1.Rows.Add("ulid");
                dataGridView1.Rows.Add("sunbindex");
            }
            else if (comboBox1.SelectedItem.ToString() == "GetAxisUnitRelItemId")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("iBlockInstance");
                dataGridView1.Rows.Add("ulid");
                dataGridView1.Rows.Add("sunbindex");
            }
            else if (comboBox1.SelectedItem.ToString() == "GetParamUnitRelItemId")
            {
                dataGridView1.Focus();
                dataGridView1.Rows.Add("iBlockInstance");
                dataGridView1.Rows.Add("ulid");
                dataGridView1.Rows.Add("sunbindex");
            }
        }

		private void fm8File_TextChanged(object sender, EventArgs e)
		{
			OutPutFile.Text = fm8File.Text + ".xml";
		}

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Marshal.ReleaseComObject(m_loader);
        }

        private void LoadEDDInstance(int iNumberOfLoads)
        {
            string sfilename = fm8File.Text;
            int iTruncate = sfilename.Length - 25;

            if (iTruncate < 0)
            {
                iTruncate = 0;
            }

            string strTruncatedFile = "..." + sfilename.Substring(iTruncate);

            int iMemoryFootprint;
            Stopwatch stopWatch = new Stopwatch();
#if DEBUG
            // Display the timer frequency and resolution. 
            if (Stopwatch.IsHighResolution)
            {
                Console.WriteLine("Operations timed using the system's high-resolution performance counter.");
            }
            else
            {
                Console.WriteLine("Operations timed using the DateTime class.");
            }
            long frequency = Stopwatch.Frequency;
            Console.WriteLine("  Timer frequency in ticks per second = {0}",
                frequency);
            long nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            Console.WriteLine("  Timer is accurate within {0} nanoseconds",
                nanosecPerTick);
#endif
            for (int i = 0; i < iNumberOfLoads; i++)
            {
                    stopWatch.Start();
                    iMemoryFootprint = 0;
                    // Call the EDD Engine
                    m_loader.OnlyCreateEDDEngine(sfilename, out iMemoryFootprint);
 
                    stopWatch.Stop();

                    // Get the elapsed time as a TimeSpan value.
                    TimeSpan ts = stopWatch.Elapsed;

                    // Format and display the TimeSpan value. 
                    string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                        ts.Hours, ts.Minutes, ts.Seconds,
                        ts.Milliseconds);
                    int iInstanceCount = i + 1;
                    string strLabel;
                    strLabel = strTruncatedFile + " Memory: " + iMemoryFootprint.ToString() + " bytes" + " Time: " + elapsedTime + " Count: " + iInstanceCount.ToString();

#if DEBUG
                    lbFileAndMemoryFootprint.Items.Add(strLabel);
        #else
                    lbFileAndMemoryFootprint.Items.Add("File Name: " + sfilename + " Run Time: " + elapsedTime + " Instance Count: "  + (i + 1).ToString());
#endif
                    lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;
                    stopWatch.Reset();
            }
 }

       
        
        private void btnOnlyEDDInstance_Click(object sender, EventArgs e)
        {
            LoadEDDInstance(1);
        }

        class DataSharingResult
        {
            public string m_strXML;
        }

        private void btnDeleteOneElement_Click(object sender, EventArgs e)
        {
            bool bDone = false;
            int iMemoryFootprint = 0;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();  

            m_loader.DeleteOneEDDEngineElement(out bDone, out iMemoryFootprint);

            stopWatch.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);

            if (!bDone)
            {
                lbFileAndMemoryFootprint.Items.Add(" Memory Released: " + iMemoryFootprint.ToString() + " bytes" + " Run Time: " + elapsedTime);
                lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;
            }
            else
            {
                MessageBox.Show("Done deleting elements");
            }
        }

        private void ThreadProc_GetDDSInfo(object my_out)
        {
            try
            {
                DataSharingResult param = (DataSharingResult)my_out;

                 string strXML;
                // Call the EDD Engine
                m_loader.SetLoadXML(true);
                m_loader.GetDDSInfo(fm8File.Text, out strXML);
                param.m_strXML = strXML;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private bool CheckFileResults(string strXML)
        {
            			// If the Expected file exists, do the comparison and display result
			string expectedFile = fm8File.Text + ".Expected.xml";

            if (File.Exists(expectedFile))
            {
                string fileXml = File.ReadAllText(expectedFile);
                string expectedXml = Regex.Replace(fileXml, @"\s", "");
                string compareXml = Regex.Replace(strXML, @"\s", "");

                if (String.Compare(compareXml, expectedXml, false) == 0)
                {
                    return true;
                }
                else
                {
                    string compareFile = fm8File.Text + ".BadCompare.xml";
  
                    // Save the XML output to a file
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(strXML);

                    XmlTextWriter writer = new XmlTextWriter(compareFile, null);
                    writer.Formatting = Formatting.Indented;
                    doc.Save(writer);
                    writer.Close();
                      
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void btnThreadSafetyFiles_Click(object sender, EventArgs e)
        {
            DataSharingResult x1 = new DataSharingResult();
            DataSharingResult x2 = new DataSharingResult();
            DataSharingResult x3 = new DataSharingResult();
            DataSharingResult x4 = new DataSharingResult();
            DataSharingResult x5 = new DataSharingResult();

            Thread thread1 = new Thread(ThreadProc_GetDDSInfo);
            Thread thread2 = new Thread(ThreadProc_GetDDSInfo);
            Thread thread3 = new Thread(ThreadProc_GetDDSInfo);
            Thread thread4 = new Thread(ThreadProc_GetDDSInfo);
            Thread thread5 = new Thread(ThreadProc_GetDDSInfo);

            thread1.Name = "x1";
            thread2.Name = "x2";
            thread3.Name = "x3";
            thread4.Name = "x4";
            thread5.Name = "x5";

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();   

            thread1.Start(x1);
            thread2.Start(x2);
            thread3.Start(x3);
            thread4.Start(x4);
            thread5.Start(x5);

            thread1.Join();
            thread2.Join();
            thread3.Join();
            thread4.Join();
            thread5.Join();
            stopWatch.Stop();


            bool bThread1FileResult = CheckFileResults(x1.m_strXML);
            bool bThread2FileResult = CheckFileResults(x2.m_strXML);
            bool bThread3FileResult = CheckFileResults(x3.m_strXML);
            bool bThread4FileResult = CheckFileResults(x4.m_strXML);
            bool bThread5FileResult = CheckFileResults(x5.m_strXML);

            

            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);

            string sShortFilename;      // Trim down the filename to 65 characters
            int ifm8FileLen = fm8File.Text.Length;
            if (ifm8FileLen > 65)
            {
                sShortFilename = "..." + fm8File.Text.Substring(ifm8FileLen - 65);
            }
            else
            {
                sShortFilename = fm8File.Text;
            }

            lbFileAndMemoryFootprint.Items.Add("Thread 1: " + sShortFilename + " Result: " + bThread1FileResult.ToString());
            lbFileAndMemoryFootprint.Items.Add("Thread 2: " + sShortFilename + " Result: " + bThread2FileResult.ToString());
            lbFileAndMemoryFootprint.Items.Add("Thread 3: " + sShortFilename + " Result: " + bThread3FileResult.ToString());
            lbFileAndMemoryFootprint.Items.Add("Thread 4: " + sShortFilename + " Result: " + bThread4FileResult.ToString());
            lbFileAndMemoryFootprint.Items.Add("Thread 5: " + sShortFilename + " Result: " + bThread5FileResult.ToString());

            lbFileAndMemoryFootprint.Items.Add("Run Time: " + elapsedTime);

            lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;
 
        }

        private void TestMemory(string sfilename)
        {
            try
            {
                int iUnused = 0;
                int iMemoryUsage = 0;
                int iMemoryUsage2nd = 0;

                string soutput1 = "";
                string soutput2 = "";
  

                FileInfo fi = new FileInfo(sfilename);
                string strDirectory = fi.DirectoryName;
                int iTruncate = strDirectory.Length - 25;
                if (iTruncate < 0)
                {
                    iTruncate = 0;
                }
                string strTruncatedDirectory = "..." + strDirectory.Substring(iTruncate);
      
                bool bDone = false;

                // Initialize UI to initial defaults
                txtCompareStatus.Visible = false;
                webBrowser1.Visible = false;
                this.Update();

                Stopwatch stopWatch = new Stopwatch();
                m_loader.SetLoadXML(false);
                stopWatch.Start();

                // Call the EDD Engine
                m_loader.GetDDSInfo(sfilename, out soutput1);// Set a BOOL to shut off XML generation.
                stopWatch.Stop();

                m_loader.GetDeviceInstanceMemoryUsage(out iMemoryUsage);
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;
                // Format and display the TimeSpan value. 
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);

                lbFileAndMemoryFootprint.Items.Add(strTruncatedDirectory + "\\" + fi.Name + " Run Time: " + elapsedTime + " Memory used: " + iMemoryUsage.ToString() + " bytes");
                lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;

                //ignore stats on this item.
                m_loader.OnlyCreateEDDEngine(sfilename,out iUnused);

                m_loader.SetLoadXML(false);

                Stopwatch stopWatch2 = new Stopwatch();
                stopWatch2.Start();
                // Call the EDD Engine
                m_loader.GetDDSInfo(sfilename, out soutput2);// Set a BOOL to shut off XML generation.
                stopWatch2.Stop();
                m_loader.GetDeviceInstanceMemoryUsage(out iMemoryUsage2nd);
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts2 = stopWatch2.Elapsed;

                // Format and display the TimeSpan value. 
                elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", ts2.Hours, ts2.Minutes, ts2.Seconds,ts2.Milliseconds);
                float fMemoryPercentage = 0;
#if DEBUG
                fMemoryPercentage = ((float)iMemoryUsage2nd / (float)iMemoryUsage) * (float)100.0;
#endif
                float fTimePercentage = ((float)ts2.Ticks / (float)ts.Ticks) * (float)100.0;

                string strRunTime = strTruncatedDirectory + "\\" + fi.Name + " Run Time: " + elapsedTime;
                string strMemory = " Memory used: " + iMemoryUsage2nd.ToString().PadLeft(iMemoryUsage.ToString().Length) + " bytes";

                string strFirstMemoryLabel = strRunTime + strMemory;
                
                lbFileAndMemoryFootprint.Items.Add(strFirstMemoryLabel);

                string strRunTimePercentage = "Second Time Used Run Time: " + fTimePercentage.ToString("F2") + "%";
                string strMemoryPercentage = "Memory Used: " + fMemoryPercentage.ToString("F2") + "%";

                lbFileAndMemoryFootprint.Items.Add(strRunTimePercentage.PadLeft(strRunTime.Length) + strMemoryPercentage.PadLeft(strMemory.Length));
                lbFileAndMemoryFootprint.Items.Add("");
                    
                lbFileAndMemoryFootprint.TopIndex = lbFileAndMemoryFootprint.Items.Count - 1;

                //Ignore stats on this item.
                m_loader.DeleteOneEDDEngineElement(out bDone, out iUnused);
               
            }

            catch (Exception ex)
            {
               MessageBox.Show(ex.ToString());
            }
        }

        private void btnTestTimeMemory_Click(object sender, EventArgs e)
        {
            TestMemory(fm8File.Text);   
        }

        private void btnTimingMemoryFixedFiles_Click(object sender, EventArgs e)
        {
            TestMemory("..\\Devices\\HART\\00001F\\002a\\0602.fm8");
            TestMemory("..\\Devices\\FF\\005100\\5900\\0803.ffo");
            TestMemory("..\\Devices\\PROFIBUS-DP\\ProfibusFDI\\0000fd\\0000\\FDI_PB_Test_000C.DDL.bin");
            TestMemory("..\\Devices\\PROFIBUS-DP\\FoundationFDI\\000000\\ffff\\ffff.ff6");
        }

        private void btnCopyTestDataToClipboard_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (object row in lbFileAndMemoryFootprint.Items)
                {
                    sb.Append(row.ToString());
                    sb.AppendLine();
                }
                sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoad10000Instances_Click(object sender, EventArgs e)
        {
            LoadEDDInstance(10000);
        }

        private void btn10xTiming_Click(object sender, EventArgs e)
        {
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);
            TestMemory(fm8File.Text);

            this.Close();
        }



          //private void button2_Click(object sender, EventArgs e)
        //{
        //    string sfilename = fm8File.Text.ToString();
        //    //Call COM object
        //    HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();
        //    if (comboBox1.SelectedItem.ToString() == "GetItemType")
        //    {
        //        string sItemType = "";
        //        int paramtype;
        //        loader.GetItemType_Auto(sfilename, out paramtype);
        //        sItemType = "GetItemTypeUTGenerated.txt file generated";
        //        Marshal.ReleaseComObject(loader);
        //        label9.Text = "ItemType:";
        //        textBox2.Text = sItemType;
        //    }
        //    if (comboBox1.SelectedItem.ToString() == "GetErrorString")
        //    {
        //        string ErrorString = "";
        //        loader.GetErrorString_Auto(sfilename, out ErrorString);
        //        ErrorString = "GetErrorStringUTGenerated.txt file generated";
        //        Marshal.ReleaseComObject(loader);
        //        label9.Text = "Error String:";
        //        textBox2.Text = ErrorString;
        //    }
        //}
	}
}
 
