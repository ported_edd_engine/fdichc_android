////////////////////////////////////////////////////////////////////////
//        Header file that describes the path and property constants used.
//
//   ALL CONSTANTS HAVING TO DO WITH PATHS OR PROPERTIES MUST BE IN THIS
//   INCLUDE FILE SO THAT THE APPLICATIONS CAN USE THEM!!!!!
//
//        @(#) $Id: pathtype_server.h,v 1.1 2012/07/10 20:59:53 rgretta Exp $
////////////////////////////////////////////////////////////////////////

#ifndef __FF_PATHTYPE_SERVER_INC__
#define __FF_PATHTYPE_SERVER_INC__

#include <pathtype_global.h>	// Start with the contents of ServerProjects\pathtype.h
										// Then add our additional defines.
										// This ensures that there are no duplicates


////////////////////////////////////////////////////////////////////////
////    FF Path Property List.	(Extends HART list)
////////////////////////////////////////////////////////////////////////

#define PP_FullLabel				(PP_HART_LAST+2) // Fully qualified label
#define PP_IndexItemArray			(PP_HART_LAST+3) // Item Array that this is an index for
#define PP_IsModified				(PP_HART_LAST+4) // Whether or not this is a modified parameter
#define PP_ServerCount				(PP_HART_LAST+5) // Count of enums on the server
#define PP_CompLabel				(PP_HART_LAST+6) // Composite Label
#define PP_EnumValueList			(PP_HART_LAST+7) // Enumeration Value List (composite)
#define PP_ObjectIndex				(PP_HART_LAST+8) // Object Index for this block
#define PP_OutOfDateDDL				(PP_HART_LAST+10) // Are we using an out of date DLL for this block
#define PP_DDL						(PP_HART_LAST+11) // What is the Mfg, DevRev, DevType, DDRev that we are using
#define PP_ProtocolType				(PP_HART_LAST+12) // What protocol are we using?
#define PP_BlockType				(PP_HART_LAST+13) // What type of block is this?
#define PP_MaxCount					(PP_HART_LAST+16) // Max count
#define PP_MinCount					(PP_HART_LAST+17) // Min count
#define PP_BlockName				(PP_HART_LAST+18) // BlockName (ie. RESOURCE, TRANSDUCER300, PID1200, AI520...)
#define PP_SubIndex					(PP_HART_LAST+19) // SubIndex for a member of a record or array
#define PP_ParamOffset				(PP_HART_LAST+20) // Param Offset of this parameter in the block
#define PP_OperationalId			(PP_HART_LAST+21) // Operational Id for a parameter
#define PP_OperationalSubIndex		(PP_HART_LAST+22) // Operational Subindex for a parameter
#define PP_DefaultValue				(PP_HART_LAST+23) // Operational Subindex for a parameter

////////////////////////////////////////////////////////////////////////
////    Data Views.
////////////////////////////////////////////////////////////////////////

#undef VIEW_LIVE			// Need to get rid of HART definitions
#undef VIEW_HISTORICAL		// Need to get rid of HART definitions
#undef VIEW_OFFLINE			// Need to get rid of HART definitions

typedef
enum {
		VIEW_LIVE       = 'l', // (L)ive data    ASCII     108
		VIEW_HISTORICAL = 'h', // (H)istorical data ASCII     104
		VIEW_OFFLINE    = 'o', // (O)ffline data  ASCII     111
} VIEWTYPE;


////////////////////////////////////////////////////////////////////////
////    FF Root property list.	(Extends HART list)
////////////////////////////////////////////////////////////////////////

#define RP_ApplicationName		(RP_HART_LAST+1)	// Client Application Name
#define RP_ComputerName			(RP_HART_LAST+2)	// Client Computer Name
#define RP_DATCategory			(RP_HART_LAST+3)	// DAT Category to use when writing an event (guid)


////////////////////////////////////////////////////////////////////////
////    FF Path object types.	(Extends HART list)
////////////////////////////////////////////////////////////////////////

#define PT_PHYSICAL_DEVICE_COL			(PT_HART_LAST+1) // Physical Device collection 
#define PT_LOGICAL_HSI_COL				(PT_HART_LAST+2) // Logical HSI Collection 
#define PT_LOGICAL_DEVICE_COL			(PT_HART_LAST+3) // Logical Device Collection 
#define PT_LOGICAL_DEVICE				(PT_HART_LAST+4) // Logical Device
#define PT_BLOCK_ID_COL					(PT_HART_LAST+5) // Block Id Collection
#define PT_DEVICE_PARAM_COL				(PT_HART_LAST+7) // Device Parameter collection
#define PT_LOCAL_PARAM_COL				(PT_HART_LAST+8) // Local Parameter collection
#define PT_CROSS_BLOCK_ITEM				(PT_HART_LAST+9) // Cross Block Item
#define PT_NAMED_MENU_COL				(PT_HART_LAST+10) // Named Menu collection
#define PT_NAMED_METHOD_COL				(PT_HART_LAST+11) // Named Method collection
#define PT_NAMED_CHART_COL				(PT_HART_LAST+12) // Named Chart collection
#define PT_NAMED_GRAPH_COL				(PT_HART_LAST+13) // Named Graph collection
#define PT_NAMED_GRID_COL				(PT_HART_LAST+14) // Named Grid collection

#define PT_FF_LAST						(PT_HART_LAST+15) // THIS MUST BE THE LAST IN THE LIST!
														  // It is used by PB projects!

#endif	//__FF_PATHTYPE_INC__
