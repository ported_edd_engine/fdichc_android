// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif
#define _ATL_FREE_THREADED
#ifdef _WIN32
#include <afxwin.h>
#include <afxdisp.h>
#include <afxext.h>         // MFC extensions
#include <afxcmn.h>			// MFC support for Windows Common Controls

#include <afxtempl.h>       // MFC Templates 
#include <atlbase.h>
#endif
#include "stdstring.h"
#include "varient.h"
#include "varientTag.h"
