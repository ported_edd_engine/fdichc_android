#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestItemIDStandardDeviceRootMenu
        {
        public:
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"device_root_menu", itemID);
            }

            static void CleanupBase()
            {
                delete itemID;
                delete engine;

            }
            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(ITEM_ID(1020), *itemID);
            }

        };

        [TestClass, Ignore]
        public ref class BaseTestItemIdNotExistingSymbolName
        {
        public:
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"invalid", itemID);
            }

            static void CleanupBase()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_SM_NAME_NOT_FOUND()
            {
                //SM_NAME_NOT_FOUND = -932
                Assert::AreEqual(-932, returnCode);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestItemIdCustomMenu
        {
        public: 
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", itemID);
            }
            static void CleanupBase()
            {
                delete itemID;
                delete engine;

            }
            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(ITEM_ID(16385), *itemID);
            }

        };

        [TestClass, Ignore]
        public ref class BaseTestItemIdCustomVariable
        {
        public: 
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_integer", itemID);
            }

            static void CleanupBase()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(ITEM_ID(16388), *itemID);
            }
        };

        // WORKAROUND: use TestInitalize instead ClassInitalize
        // Impediment
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with items 7, but every test use only 1)

        [TestClass]
        public ref class When_request_the_Item_ID_for_Standard_Device_Root_Menu_with_FM8 : BaseTestItemIDStandardDeviceRootMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_Standard_Device_Root_Menu_with_FMA : BaseTestItemIDStandardDeviceRootMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_not_existing_SymbolName_with_FM8 : BaseTestItemIdNotExistingSymbolName
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_not_existing_SymbolName_with_FMA : BaseTestItemIdNotExistingSymbolName
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_menu_with_FM8 : BaseTestItemIdCustomMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_menu_with_FMA : BaseTestItemIdCustomMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_variable_with_FM8 : BaseTestItemIdCustomVariable
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_variable_with_FMA : BaseTestItemIdCustomVariable
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0001.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
