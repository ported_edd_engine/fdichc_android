#include "StdAfx.h"
#include "logging.h"
#include "stdarg.h"



MI_LOG::MI_LOG()
{
	
}

MI_LOG::~MI_LOG()
{
}

MI_LOG* MI_LOG::getMILOGInstance()
{
	static MI_LOG m_pMILOGInst;

	return &m_pMILOGInst;
}

char * MI_LOG::Space (int m)			// Return a static buffer with "m" spaces in it.
{
	static const char sStr[] = "                                                                               ";
	static char spBuf[128] = {0};

	if (m >= 128)
	{
		m = 127;
	}
    PS_Strncpy(spBuf, 127, sStr, m );	// The "safe" (*_s) version always appends a NULL

	return spBuf;
}


//
//
//
bool MI_LOG::IsMiLoggingEnabled()
{
#ifdef _DEBUG
	//static bool g_SearchForLogFile = true;	// Initializes to true, but retains its value between calls to this function
	bool bRetVal = false;

	//EnterCriticalSection( &m_MILOG_cs );
	//PS_VsnPrintf( g_szLogFileName, LOGFILE_SIZE-1, "%s", "methodLog_2.txt" );	//A null character is appended after the last character written
	//LeaveCriticalSection( &m_MILOG_cs );

	//if( g_SearchForLogFile )//search the fms.ini file once and only once.
	//{
	//	GetPrivateProfileStringA( "INSTALLATION", "MethodInterpreterLogging", "", g_szLogFileName, MAX_PATH, "fms.ini" );
	//	g_SearchForLogFile = false;
	//}
	//if( g_szLogFileName[0] != 0 )//if the log filename exists, return true.
	//{
	//	bRetVal = true;
	//}
	return bRetVal;
#endif //_DEBUG
	return false;
}

//
// This is the HCF error logging mechanism
//
int MI_LOG::logout(int channel, char* format, ...)
{
	va_list vMarker;

	if( IsMiLoggingEnabled() )//if the log filename exists, output to it.
	{
		va_start(vMarker, format);

		m_MILOG_cs.lock();
		FILE *fp = NULL;

        fp = fopen(g_szLogFileName, "a" );//append to the end.
        errno_t err = PS_GetLastError();
		if( (err == 0) && fp )
		{
			//char strActual[LOG_BUF_SIZE]={0};
			char *strActual = new char[strlen(format) + LOG_BUF_SIZE];

			__time64_t TimeNow;		//seconds elapsed since midnight (00:00:00), January 1, 1970
			// Get time as 64-bit integer.
            time( &TimeNow );

            struct tm _tm = *localtime(&TimeNow);

			// Convert to local time.
            //errno_t err = localtime(&_tm, &TimeNow);

			// Build timestamp
            //if (!err)
			{
                PS_VsnPrintf(strActual, LOG_BUF_SIZE-1, "%02d%02d%02d %02d:%02d:%02d ",
                    (_tm.tm_year+1900), (_tm.tm_mon+1), _tm.tm_mday, _tm.tm_hour, _tm.tm_min, _tm.tm_sec);
				fputs(strActual, fp );

				//int len = _vscprintf( format, vMarker ) + 1;	// _vscprintf doesn't count terminating '\0'
				//vsprintf_s(strActual, len, format, vMarker);	//use to get rid of warning C4996 but doesn't work
				PS_VsnPrintf(strActual, strlen(format) + LOG_BUF_SIZE, format, vMarker);
				fputs(strActual, fp );
			}
			fclose(fp);//close handle

			if (strActual)
			{
				delete strActual;
			}
		}
		m_MILOG_cs.unlock();

		va_end(vMarker);
	}
	return 0; 
}


	


