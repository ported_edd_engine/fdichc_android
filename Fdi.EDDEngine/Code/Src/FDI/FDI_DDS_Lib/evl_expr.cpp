/**
 *	@(#) $Id: evl_expr.cpp,v 1.1 2012/09/20 21:06:05 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions for the expression evaluator.
 *	These functions operate on the EXPR data structure.
 */


#include "stdafx.h"
#include "fch_lib.h"
#include "dds_upcl.h"
#include "evl_loc.h"
#include "nsConsumer/IParamCache.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"
#include <limits>
using namespace std;

#include "conversions.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif

extern void memdump(unsigned char *chunk);
/*
 * Defines
 */

 /* Check for valid type range */
#define VALID_EXPR_TYPE(T)	((T) >= INTEGER && (T) <= INDEX)

#define	EVAL_STACK_SIZE	32	/* Size of the EVAL_STACK */

/*
 * Typedefs
 */

typedef struct tag_EVAL_STACK {		/* Stack used for evaluating expressions */
	EXPR           *top;
	EXPR            data[EVAL_STACK_SIZE];
}               EVAL_STACK;

int get_attribute_value(::nsEDDEngine::AttributeName attrib_name, DDL_UINT which, DESC_REF *desc_ref,
					EXPR	*expr,	nsEDDEngine::OP_REF_LIST *depinfo,	ENV_INFO *env_info,	nsEDDEngine::OP_REF	*var_needed);

extern void decode_BYTE_STRING(unsigned char **ptr, unsigned long *bin_size, char *str, int size, int ddtype);
extern int get_ddtype(ENV_INFO *env_info);

/*********************************************************************
 *
 *	Name: ddl_pop
 *	ShortDesc: Pop an operand off of the stack.
 *
 *	Description:
 *		ddl_pop will pop the top operand off of the stack.
 *
 *	Inputs:
 *		stack - pointer to the stack
 *
 *	Outputs:
 *		operand - pointer to the place where you wish to store
 *					the operand.
 *		stack - pointer to the modified stack
 *
 *	Returns:
 *		DDL_SUCCESS - if something is on the stack
 *		DDL_ENCODING_ERROR - if the stack is empty
 *
 *	Also:
 *		ddl_push(3)
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_pop(
EXPR           *operand,
EVAL_STACK     *stack)
{
	EXPR           *top;

#ifdef DDSTEST
	TEST_FAIL(DDL_POP);
#endif

	if (stack->top == stack->data) {
		PS_TRACE(L"\n*** error (ddl_pop) : nothing more to pop off stack");
		return DDL_ENCODING_ERROR;
	}

	top = --(stack->top);

	operand->type = top->type;
	operand->size = top->size;
	operand->val = top->val;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_EvalVarToExpr
 *	ShortDesc: Convert an nsConsumer::EVAL_VAR_VALUE to an EXPR
 *
 *	Description:
 *		ddl_convert_type will take an EVAL_VAR_VALUE value and convert
 *      it into an EXPR value.
 *
 *	Inputs:
 *		var_value - pointer to EVAL_VAR_VALUE
 *
 *	Outputs:
 *		expr - pointer to EXPR
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE - if the value is not a numeric
 *
 *	Author:
 *		Mark Sandmann
 *
 *********************************************************************/

static int ddl_EvalVarToExpr(nsConsumer::EVAL_VAR_VALUE* var_value, EXPR* expr,  unsigned long bitMask)
{
	// Convert EVAL_VAR_VALUE to an EXPR
	expr->size = var_value->size;

	switch (var_value->type)
	{
	case nsEDDEngine::VT_DOUBLE:
		expr->val.d = var_value->val.d;
        expr->type = DOUBLEG_PT;
		break;

	case nsEDDEngine::VT_FLOAT:
		expr->val.f = var_value->val.f;
        expr->type = FLOATG_PT;
		break;

	case nsEDDEngine::VT_EDD_DATE:
	case nsEDDEngine::VT_INTEGER:
		expr->val.i = (int)var_value->val.i;
		expr->type = INTEGER;
		break;

		/**
			** Within EVAL: INDEX, ENUMs and BIT_ENUMs will
			** be treated as UNSIGNED.
			**/

	case nsEDDEngine::VT_TIME:
	case nsEDDEngine::VT_DATE_AND_TIME:
	case nsEDDEngine::VT_DURATION:
	case nsEDDEngine::VT_UNSIGNED:
	case nsEDDEngine::VT_INDEX:
	case nsEDDEngine::VT_ENUMERATED:
	case nsEDDEngine::VT_BIT_ENUMERATED:

		
		if(bitMask != 0) 
		{
			var_value->val.u = var_value->val.u & bitMask;
		}

		expr->val.u = (unsigned long)var_value->val.u;
		expr->type = UNSIGNED;
		break;

	case nsEDDEngine::VT_TIME_VALUE:
		if (var_value->size == 4)
		{
			expr->val.u = (unsigned long)var_value->val.u;
			expr->type = UNSIGNED;
		}
		else // size will be 8
		{
			expr->val.i = (int)var_value->val.i;
			expr->type = INTEGER;
		}
		break;

	case nsEDDEngine::VT_ASCII:
	case nsEDDEngine::VT_PACKED_ASCII:
	case nsEDDEngine::VT_PASSWORD:
	case nsEDDEngine::VT_EUC:  
	case nsEDDEngine::VT_VISIBLESTRING:
		FDI_ConvSTRING::CopyTo(&var_value->val.s,&expr->val.s);
		expr->type = ASCII;
		break;

	case nsEDDEngine::VT_BITSTRING: 
	case nsEDDEngine::VT_OCTETSTRING:
		expr->type = BITSTRING;
		expr->val.b.len = var_value->val.b.length();
		expr->val.b.ptr = (uchar *) malloc( expr->val.b.len );
		memcpy(expr->val.b.ptr, var_value->val.b.ptr(), var_value->val.b.length());
		break;

	default:
		ASSERT(0);
		return DDL_BAD_VALUE_TYPE;
		/* NOTREACHED */
		break;
	}
    return SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_convert_type
 *	ShortDesc: Convert two expressions to a common type and size
 *
 *	Description:
 *		ddl_convert_type will take two expressions and convert them
 *		to the type and size of the "higher" type and/or bigger size.
 *
 *	Inputs:
 *		op1 - pointer to expression 1
 *		op2 - pointer to expression 2
 *
 *	Outputs:
 *		op1 - pointer to (possibly modified) expression 1
 *		op2 - pointer to (possibly modified) expression 2
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE - if either of the expressions is not a numeric
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_convert_type(
EXPR           *op1,
EXPR           *op2)
{
	register EXPR  *lowop;	/* points to the "lower" typed expression */
	register EXPR  *highop;	/* points to the "higher" typed expression */
	register unsigned int size1, size2;	/* size of op1 & op2 */

	ASSERT_DBG(op1 && op2);

#ifdef DDSTEST
	TEST_FAIL(DDL_CONVERT_TYPE);
#endif

	if (!VALID_EXPR_TYPE(op1->type) || !VALID_EXPR_TYPE(op2->type)) {
		return DDL_BAD_VALUE_TYPE;
	}

	size1 = op1->size;
	size2 = op2->size;

	/*
	 * If the two expressions have the same type, make sure they have the
	 * same size, and exit.
	 */

	if (op1->type == op2->type) {
		if (size1 > size2) {
			op2->size = size1;
		}
		else {
			op1->size = size2;
		}
		return DDL_SUCCESS;
	}

	/*
	 * Find which expression has the "higher" type, then switch on that
	 * type so the other expression can be promoted to the higher one.
	 */

	if (op1->type > op2->type) {
		highop = op1;
		lowop = op2;
	}
	else {
		highop = op2;
		lowop = op1;
	}

	switch (highop->type) {
    case DOUBLEG_PT:

		/*
		 * Promote lowop to a double.
		 */

		switch (lowop->type) {
        case FLOATG_PT:
			lowop->val.d = (double) lowop->val.f;
			break;

		case UNSIGNED:
			lowop->val.d = (double) lowop->val.u;
			break;

		case INTEGER:
			lowop->val.d = (double) lowop->val.i;
			break;

        case DOUBLEG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
        lowop->type = DOUBLEG_PT;
		lowop->size = sizeof(lowop->val.d);
		break;

    case FLOATG_PT:

		/*
		 * Promote lowop to a float.
		 */

		switch (lowop->type) {
		case UNSIGNED:
			lowop->val.f = (float) lowop->val.u;
			break;

		case INTEGER:
			lowop->val.f = (float) lowop->val.i;
			break;

        case DOUBLEG_PT:
        case FLOATG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
        lowop->type = FLOATG_PT;
		lowop->size = sizeof(lowop->val.f);
		break;

	case UNSIGNED:
	case INTEGER:

		/*
		 * At this point one of the expressions is an UNSIGNED and the
		 * other is INTEGER.
		 */

		/*
		 * If the expressions are the same size, promote the integer to
		 * unsigned integer and exit.
		 */

		if (size1 == size2) {
			ASSERT_DBG(lowop->type == INTEGER);
			lowop->type = UNSIGNED;
			lowop->val.u = (ulong) lowop->val.i;
			return DDL_SUCCESS;
		}

		/*
		 * Change the smaller sized expression to the larger sized.
		 */

		if (size1 > size2) {
			highop = op1;
			lowop = op2;
		}
		else {
			highop = op2;
			lowop = op1;
		}

		switch (highop->type) {
		case UNSIGNED:
			lowop->val.u = (ulong) lowop->val.i;
			lowop->type = UNSIGNED;
			break;

		case INTEGER:
			lowop->val.i = (long) lowop->val.u;
			lowop->type = INTEGER;
			break;

        case DOUBLEG_PT:
        case FLOATG_PT:
		default:
			return DDL_BAD_VALUE_TYPE;
		}
		lowop->size = highop->size;
		break;

	default:
		return DDL_BAD_VALUE_TYPE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_expr_nonzero
 *	ShortDesc: Determines if an expression is non-zero.
 *
 *	Description:
 *		ddl_expr_nonzero takes an expression and determines if its value
 *		is non-zero or not.
 *
 *	Inputs:
 *		operand - pointer to the expression
 *
 *	Outputs:
 *
 *	Returns:
 *		1 - The expression is non-zero
 *		0 - The expression is zero
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_expr_nonzero(
EXPR           *operand)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_EXPR_NONZERO);
#endif

	switch (operand->type) {
	case INTEGER:
		return (operand->val.i ? 1 : 0);
		/* NOTREACHED */
		break;

	case UNSIGNED:
		return (operand->val.u ? 1 : 0);
		/* NOTREACHED */
		break;

    case FLOATG_PT:
		return (operand->val.f ? 1 : 0);
		/* NOTREACHED */
		break;

    case DOUBLEG_PT:
		return (operand->val.d ? 1 : 0);
		/* NOTREACHED */
		break;

	default:
		break;
	}

	return 0;
}


/*********************************************************************
 *
 *	Name: ddl_apply_op
 *	ShortDesc: Applies the given operation to the values on the stack.
 *
 *	Description:
 *		ddl_apply_op applies the given operation to the top value(s)
 *		on the stack.  Each operand is popped from the stack, adjusted
 *		for type (if needed) and the operator is applied. The result is
 *		returned in *result_rtn.
 *
 *	Inputs:
 *		opcode - operation to apply
 *		stack - stack which contains the operands.
 *
 *	Outputs:
 *		stack - stack which contains the result.
 *		result_rtn - pointer to a structure to store the result
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is corrupted
 *		DDL_DIVIDE_BY_ZERO - Divide or Modulus denominator is zero.
 *		DDL_BAD_VALUE_TYPE - Invalid type for this operator
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/
/* result_type values */
#define OP1_RSLT	0	/* Result should be same as operand 1 */
#define INT_RSLT	1	/* Result is an int */

/* Masks for OpFlags */
#define NONE		0x00	/* no second operand needed */
#define NEED_OP2	0x01	/* needs a second operand */
#define NEED_OP3	0x02	/* needs a third operand */
#define CONVERT		0x04	/* must convert first and second operands to like type */

static int
ddl_apply_op(
DDL_UINT        opcode,
EVAL_STACK     *stack,
EXPR           *result_rtn)
{
	int  rc = DDL_SUCCESS;
	EXPR oprd1 = {0};			/* storage for operand1 */
	EXPR oprd2 = {0};			/* storage for operand2 */
	EXPR oprd3 = {0};			/* storage for operand3 */
	EXPR result = {0};			/* storage for the result */
	int  OpFlags = NONE;		/* Operand flags */
	int  result_type = INT_RSLT;/* Type of result needed */

#ifdef DDSTEST
	TEST_FAIL(DDL_APPLY_OP);
#endif

	/*
	 * Determine whether a second operand is needed and if conversion to
	 * similar types is required.  Also, determine the type of the result.
	 */

	switch (opcode) {
	case NOT_OPCODE:
		OpFlags = NONE;
		result_type = INT_RSLT;
		break;
	case NEG_OPCODE:
	case BNEG_OPCODE:
		OpFlags = NONE;
		result_type = OP1_RSLT;
		break;
	case ADD_OPCODE:
	case SUB_OPCODE:
	case MUL_OPCODE:
	case DIV_OPCODE:
	case MOD_OPCODE:
	case LSHIFT_OPCODE:
	case RSHIFT_OPCODE:
	case AND_OPCODE:
	case OR_OPCODE:
	case XOR_OPCODE:
		OpFlags = NEED_OP2 | CONVERT;
		result_type = OP1_RSLT;
		break;
	case LAND_OPCODE:
	case LOR_OPCODE:
		OpFlags = NEED_OP2;
		result_type = INT_RSLT;
		break;
	case LT_OPCODE:
	case LE_OPCODE:
	case GT_OPCODE:
	case GE_OPCODE:
	case EQ_OPCODE:
	case NEQ_OPCODE:
		OpFlags = NEED_OP2 | CONVERT;
		result_type = INT_RSLT;
		break;

	case CONDITIONAL_OPCODE:
		OpFlags = NEED_OP2 | NEED_OP3 | CONVERT;
		result_type = OP1_RSLT;
		break;

	default:
		PS_TRACE(L"\n*** error (ddl_apply_op) : no case for unknown opcode = %d",opcode);

		return DDL_ENCODING_ERROR;
	}

	/*
	 * Pop the first (and possibly only) operand off of the stack.
	 */

	rc = ddl_pop(&oprd1, stack);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * If a second operand is needed, pop it from the stack.
	 */

	if (OpFlags & NEED_OP2) {
		rc = ddl_pop(&oprd2, stack);
		if (rc != DDL_SUCCESS) {
			PS_TRACE(L"\n*** error (ddl_apply_op) : operation flags requires antother argument, however there are no additional arguments");
			return rc;
		}

		/*
		 * If the two operands need to be of like type, convert them.
		 */

		if (OpFlags & CONVERT) {
			// If either of the operands are ASCII, ensure that they are both ASCII
			if ((oprd1.type == ASCII) || (oprd2.type == ASCII) || (oprd1.type == BITSTRING) || (oprd2.type == BITSTRING))
			{
				if (oprd1.type != oprd2.type) {
					rc =  DDL_BAD_VALUE_TYPE;
				}
			}
			else
			{	// Convert the numeric operands to the same type
				rc = ddl_convert_type(&oprd2, &oprd1);
			}

			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	/*
	 * If a third operand is needed, pop it from the stack.
	 */

	if (OpFlags & NEED_OP3) {
		rc = ddl_pop(&oprd3, stack);
		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	/*
	 * Set the type of the result from the flag set above.
	 */

	if (result_type == INT_RSLT) {
		result.type = INTEGER;
		result.size = sizeof(result.val.i);
	}
	else {
		result.type = oprd1.type;
		result.size = oprd1.size;
	}


	/*
	 * Do the operation itself. Put the answer in result.
	 */

	switch (opcode) {
	case NOT_OPCODE:
		result.val.i = ddl_expr_nonzero(&oprd1) ? 0 : 1;
		break;
	case NEG_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = -oprd1.val.i;
			break;
        case FLOATG_PT:
			result.val.f = -oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = -oprd1.val.d;
			break;
		case UNSIGNED:
			result.type = INTEGER;
			result.val.i = -((int)oprd1.val.u);
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case BNEG_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = (long) (~(ulong) oprd1.val.i);
			break;
		case UNSIGNED:
			result.val.u = ~oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case ADD_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i + oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u + oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f + oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d + oprd1.val.d;
			break;
		case ASCII:
		case PACKED_ASCII:
		case PASSWORD:
		case EUC:  
		case VISIBLESTRING:
			{	// Concatenate the strings
				int len = oprd1.val.s.len + oprd2.val.s.len;

				wchar_t* newstring = (wchar_t*)malloc((len+1) * sizeof(wchar_t));
				memset(newstring,0,(len+1) * sizeof(wchar_t));

#ifdef __ANDROID__
				if (oprd2.val.s.str != NULL)
					wcscpy(newstring, oprd2.val.s.str);		// oprd2 comes first when unpacking RPN

				if (oprd1.val.s.str != NULL)
					wcscat(newstring, oprd1.val.s.str);
#else
				if (oprd2.val.s.str>0)
					wcscpy(newstring, oprd2.val.s.str);		// oprd2 comes first when unpacking RPN

				if (oprd1.val.s.str>0)
					wcscat(newstring, oprd1.val.s.str);		// oprd1 comes next (see SUB_OPCODE example)
#endif

				result.val.s.str = newstring;
				result.val.s.len = (unsigned short)wcslen(newstring);
				result.val.s.flags = STRING::FREE_STRING;

				ddl_free_string(&oprd1.val.s);
				ddl_free_string(&oprd2.val.s);
			}
			break;
		case OCTETSTRING:
		case BITSTRING:
			{	// Concatenate the Octet strings
				int len = oprd1.val.b.len + oprd2.val.b.len;

				
				uchar* tmp = (uchar *)malloc(sizeof(uchar)*len);

#ifdef __ANDROID__
				if (oprd2.val.b.ptr != NULL)
					memcpy(tmp, oprd2.val.b.ptr, sizeof(uchar)*oprd2.val.b.len); // oprd2 comes first when unpacking RPN

				if (oprd1.val.b.ptr != NULL)
					memcpy(tmp + oprd2.val.b.len, oprd1.val.b.ptr, sizeof(uchar)*oprd1.val.b.len); // oprd1 comes next (see SUB_OPCODE example)
#else
				if (oprd2.val.b.ptr>0)
					memcpy(tmp, oprd2.val.b.ptr, sizeof(uchar)*oprd2.val.b.len); // oprd2 comes first when unpacking RPN

				if (oprd1.val.b.ptr>0)
					memcpy(tmp + oprd2.val.b.len, oprd1.val.b.ptr, sizeof(uchar)*oprd1.val.b.len); // oprd1 comes next (see SUB_OPCODE example)
#endif


				result.val.b.ptr = tmp;
				
				result.val.b.len = len;
				
				free((void *)oprd1.val.b.ptr);
				free((void *)oprd2.val.b.ptr);
			}
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case SUB_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i - oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u - oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f - oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d - oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case MUL_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i * oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u * oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.f = oprd2.val.f * oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.d = oprd2.val.d * oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case DIV_OPCODE:
		switch (result.type) {
		case INTEGER:
			{
				if (0L == oprd1.val.i)
				{
					if (oprd2.val.i >= 0)
					{
						result.val.i = numeric_limits<long>::infinity();
					}
					else
					{
						result.val.i = -numeric_limits<long>::infinity();
					}
				}
				else
				{
					result.val.i = oprd2.val.i / oprd1.val.i;
				}
			}
			break;
		case UNSIGNED:
			{
				if (0L == oprd1.val.u)
				{
					result.val.u = numeric_limits<unsigned long>::infinity();
				}
				else
				{
					result.val.u = oprd2.val.u / oprd1.val.u;
				}
			}
			break;
        case FLOATG_PT:
			{
				if (0.0 == oprd1.val.f)
				{
					if (oprd2.val.f >= 0)
					{
						result.val.f = numeric_limits<float>::infinity();
					}
					else
					{
						result.val.f = -numeric_limits<float>::infinity();
					}
				}
				else
				{
					result.val.f = oprd2.val.f / oprd1.val.f;
				}
			}
			break;
        case DOUBLEG_PT:
			{
				if (0.0 == oprd1.val.d)
				{
					if (oprd2.val.d >= 0)
					{
						result.val.d = numeric_limits<double>::infinity();
					}
					else
					{
						result.val.d = -numeric_limits<double>::infinity();
					}
				}
				else
				{
					result.val.d = oprd2.val.d / oprd1.val.d;
				}
			}
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case MOD_OPCODE:
		switch (result.type) {
		case INTEGER:
			if (0L == oprd1.val.i) {
				return DDL_DIVIDE_BY_ZERO;
			}
			result.val.i = oprd2.val.i % oprd1.val.i;
			break;
		case UNSIGNED:
			if (0L == oprd1.val.u) {
				return DDL_DIVIDE_BY_ZERO;
			}
			result.val.u = oprd2.val.u % oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case LSHIFT_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = (long) ((ulong) oprd2.val.i << oprd1.val.i);
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u << oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case RSHIFT_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = (long) ((ulong) oprd2.val.i >> oprd1.val.i);
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u >> oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case AND_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i & oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u & oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case OR_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i | oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u | oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case XOR_OPCODE:
		switch (result.type) {
		case INTEGER:
			result.val.i = oprd2.val.i ^ oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.u = oprd2.val.u ^ oprd1.val.u;
			break;
        case FLOATG_PT:
        case DOUBLEG_PT:
			PS_TRACE(L"\n*** error (ddl_apply_op) : unsupported data type for opcode = %d, data type = %d",opcode,result.type);
			return DDL_BAD_VALUE_TYPE;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,result.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case LAND_OPCODE:
		if (ddl_expr_nonzero(&oprd2) && ddl_expr_nonzero(&oprd1)) {
			result.val.i = 1;
		}
		else {
			result.val.i = 0;
		}
		break;
	case LOR_OPCODE:
		if (ddl_expr_nonzero(&oprd2) || ddl_expr_nonzero(&oprd1)) {
			result.val.i = 1;
		}
		else {
			result.val.i = 0;
		}
		break;
	case LT_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i < oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u < oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f < oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d < oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case LE_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i <= oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u <= oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f <= oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d <= oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case GT_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i > oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u > oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f > oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d > oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case GE_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i >= oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u >= oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f >= oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d >= oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case EQ_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i == oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u == oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f == oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d == oprd1.val.d;
			break;
		case ASCII:
		case PACKED_ASCII:
		case PASSWORD:
		case EUC:
		case VISIBLESTRING:
		{	
			if (!wcscmp(oprd1.val.s.str, oprd2.val.s.str))
			{
				// Both strings are equal
				result.val.i = 1;
			}
			else
			{
				result.val.i = 0;
			}
			ddl_free_string(&oprd1.val.s);
			ddl_free_string(&oprd2.val.s);
		}
		break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case NEQ_OPCODE:
		switch (oprd1.type) {
		case INTEGER:
			result.val.i = oprd2.val.i != oprd1.val.i;
			break;
		case UNSIGNED:
			result.val.i = oprd2.val.u != oprd1.val.u;
			break;
        case FLOATG_PT:
			result.val.i = oprd2.val.f != oprd1.val.f;
			break;
        case DOUBLEG_PT:
			result.val.i = oprd2.val.d != oprd1.val.d;
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d, data type = %d",opcode,oprd1.type);
			return DDL_ENCODING_ERROR;
		}
		break;
	case CONDITIONAL_OPCODE:
		result.val = ddl_expr_nonzero(&oprd3)? oprd2.val : oprd1.val;
		break;
	default:
		ASSERT(0);
		PS_TRACE(L"\n*** error (ddl_apply_op) : no case for opcode = %d",opcode);
		return DDL_ENCODING_ERROR;
	}

	memcpy((char *) result_rtn, (char *) &result, sizeof(result));

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_do_eval_expr
 *	ShortDesc: Parse and evaluate the binary encoded expression.
 *
 *	Description:
 *		ddl_do_eval_expr will unpack a binary encoded expression and
 *		return the expression along with dependency information.
 *		This function uses a stack to evaluate the postfix notation
 *		used within the binary expression.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Expression was malformed
 *		error returns from other ddl_* functions
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

static int
ddl_do_eval_expr(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;
    nsEDDEngine::OP_REF  op_ref;	/* storage for operational_reference */
	DDL_UINT        which;	/* which min or max value desired */
	DDL_UINT        tag;	/* identifies the current type of expression */
	EVAL_STACK      stack;	/* stack used for evaluating the expression */
	DDL_UINT        ival;	/* integer value */
	DDL_UINT        max_val;/* Maximum value used to determine size of int */
	DDL_UINT_LONG	lval = 0; //unsigned long long value

	ulong bitMask = 0;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_DO_EVAL_EXPR);
#endif

	which = 0;

	/*
	 * This code handles the very common case of processing a single
	 * integer constant much faster than the generic code
	 */

	if ((*size == 2) && ((*chunkp)[0] == INTCST_OPCODE)) {
		if (expr != NULL) {
			expr->val.i = (*chunkp)[1];
			expr->type = INTEGER;
			expr->size = 1;
		}
		*size -= 2;
		*chunkp += 2;
		return DDL_SUCCESS;
	}

	/*
	 * Initialize the evaluation stack, and process the chunk.
	 */

	stack.top = stack.data;

	while (*size > 0) {

		EXPR xval = {0};	/* storage for operands */

		/*
		 * Parse the tag, and switch on the tag type
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		switch (tag) {
		case STRCST_OPCODE: // The expression is a string
			rc = ddl_parse_string(chunkp, size, &xval.val.s, depinfo, env_info, var_needed);
			xval.type = ASCII;
			break;

		case OCTET_STRING_CONST_OPCODE: 
			{
				DDL_PARSE_INTEGER(chunkp, size, &ival);

				uchar* newstring = (uchar*)malloc(ival * sizeof(uchar));
				xval.val.b.ptr = newstring;
				::decode_BYTE_STRING(chunkp, size, (char *)xval.val.b.ptr, ival, get_ddtype(env_info)); // need to chk for uchar
				xval.val.b.len = ival;
				xval.type = BITSTRING;
				xval.size = ival;

			}
			break;

		case UNSIGNEDCST_OPCODE:

			rc = ddl_parse_integer_func(chunkp, size, &ival);
			//DDL_PARSE_INTEGER(chunkp, size, &ival);

			if (rc == DDL_SUCCESS)
			{
				xval.type = UNSIGNED;

				/*
				 * Determine size of the integer by comparing it with
				 * the largest integer of the current size.  If bigger,
				* increment the size, compute the next largest integer
				* and try again. You may not exceed the size of
				 * "val.i", however.
				 */

				xval.size = 1;
				max_val = 0xFF;

				while ((ival > max_val) && (xval.size < sizeof(ulong))) {
					xval.size++;
					max_val = (max_val << 8) | 0xFF;
				}
				xval.val.u = (unsigned long) ival;
			}
			else // Try to read long unsigned integer value
			{
				rc = ddl_parse_integer_long_func(chunkp, size, &lval); 
				//DDL_PARSE_INTEGER_LONG(chunkp, size, &lval);
				if(rc == DDL_SUCCESS)
				{
					xval.type = UNSIGNED;
					xval.size = sizeof(ULONGLONG);
					xval.val.u = (ULONGLONG) lval;
				}
				else
				{
					return rc;
				}
			}
			break;

		case SCALING_TYPE_OPCODE:	/* The object in an integer constant */
		case BOOLCST_OPCODE:	/* The object in an integer constant */

			::ddl_parse_byte(chunkp,size,&ival);
			xval.type = INTEGER;

			/*
			 * Determine size of the integer by comparing it with
			 * the largest integer of the current size.  If bigger,
			 * increment the size, compute the next largest integer
			 * and try again. You may not exceed the size of
			 * "val.i", however.
			 */

			xval.size = 1;
			xval.val.i = ival;

			break;

		case INTCST_OPCODE:	/* The object in an integer constant */

			rc = ddl_parse_integer_func(chunkp, size, &ival);
			//DDL_PARSE_INTEGER(chunkp, size, &ival);

			if (rc == DDL_SUCCESS)
			{
				xval.type = INTEGER;

				/*
				 * Determine size of the integer by comparing it with
				 * the largest integer of the current size.  If bigger,
				* increment the size, compute the next largest integer
				* and try again. You may not exceed the size of
				 * "val.i", however.
				 */

				xval.size = 1;
				max_val = 0x7F;	//maximum value in integer in one byte

				while ((ival > max_val) && (xval.size < sizeof(long))) {
					xval.size++;
					max_val = (max_val << 8) | 0xFF;
				}
				xval.val.i = (ulong) ival;
			}

			else // Try to read long unsigned integer value
			{
				rc = ddl_parse_integer_long_func(chunkp, size, &lval); 
				//DDL_PARSE_INTEGER_LONG(chunkp, size, &lval);
				if(rc == DDL_SUCCESS)
				{
					xval.type = INTEGER;
					xval.size = sizeof(ULONGLONG);
					xval.val.i = (ULONGLONG) lval;
				}
				else
				{
					return rc;
				}
			}
			break;

		case FPCST_OPCODE:	/* This is a floating point constant */

            xval.type = DOUBLEG_PT;
			xval.size = sizeof(xval.val.d);

			rc = ddl_parse_floating(chunkp, size, &(xval.val.d));

			break;

		case ATTRIBUTE_VALUE_OPCODE:
			{
				::ddl_parse_byte(chunkp,size,&ival);

				::nsEDDEngine::AttributeName attrib_name =(::nsEDDEngine::AttributeName)ival; 

				rc = ddl_parse_op_ref(chunkp, size, &op_ref, depinfo, env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					return rc;
				}

				DESC_REF desc_ref;
				desc_ref.id = op_ref.op_info.id;
				desc_ref.type = (ITEM_TYPE)op_ref.op_info.type;
				which = op_ref.op_info.member;

				rc = get_attribute_value(attrib_name,
					which,
					&desc_ref,
					&xval,
					depinfo, 
					env_info, 
					var_needed);

				if (rc != DDL_SUCCESS) {
					return rc;
				}

			}
			break;

		case MINIMAX_REF_OPCODE:
			{
				DDL_PARSE_INTEGER(chunkp, size, &ival);
				which = (long) ival;

				::ddl_parse_byte(chunkp,size,&ival);
				xval.type = INTEGER;
				xval.size = 1;
				xval.val.i = ival;

				rc = ddl_parse_op_ref(chunkp, size, &op_ref, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
			}
			}
			break;

		case ARRAY_INDEX_OPCODE:
			return SM_UNSUPPORTED_FEATURE_IN_DD;	// Return an error until this gets supported.
			break;

		case REF_OPCODE:
		case METHOD_VALUE_REF_OPCODE:

			switch (tag) {
			case METHOD_VALUE_REF_OPCODE:
			case REF_OPCODE:
				/*
				 * Parse the reference.  This will return an
				 * operational reference
				 */

				rc = ddl_parse_op_ref(chunkp, size, &op_ref, depinfo, env_info, var_needed, &bitMask);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
					
				break;

			default:	/* this cannot happen */
				CRASH_RET(DDL_SERVICE_ERROR);
				/* NOTREACHED */
				break;
			}

			/*
			 * Keep track of the fact that we have another variable
			 * dependency.
			 */

			if (depinfo) {
				rc = ddl_add_depinfo(&op_ref, depinfo);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			if (expr) {

				/*
				 * If expression is desired, do the upcall to
				 * get the value of the variable.
				 */

				switch (tag) 
				{
				case METHOD_VALUE_REF_OPCODE:
					PS_TRACE(L"\n*** error not implemented METHOD_VALUE_REF_OPCODE\n");
					rc=E_FAIL;
					break;

				case REF_OPCODE:
					{
						nsConsumer::EVAL_VAR_VALUE  var_value;// = {0};	/* storage area for upcall values */
						rc = app_func_get_param_value(env_info, &op_ref, &var_value);

						if (rc != DDL_SUCCESS) 
						{
							*var_needed = op_ref;
							return rc;
						}

						// Convert EVAL_VAR_VALUE to an EXPR
						rc = ddl_EvalVarToExpr(&var_value, &xval, bitMask);
						if (rc != 0)
						{
							return rc;
						}
						
					}
					break;

				default:
					CRASH_RET(DDL_SERVICE_ERROR);
					/* NOTREACHED */
					break;
				}
			}
			break;

		default:

			/*
			 * This must be an opcode.  Apply it to the values on
			 * the stack.
			 */

			if (expr) {
				rc = ddl_apply_op(tag, &stack, &xval);
				if (rc != DDL_SUCCESS) {
					memdump(*chunkp);
					return rc;
				}

			}
			break;
		}

		/*
		 * Take the value which was calculated above and push it on the
		 * stack.
		 */

		if (expr) {
			ASSERT_RET(stack.top < (stack.data + EVAL_STACK_SIZE), DDL_EXPR_STACK_OVERFLOW);

			stack.top->type = xval.type;
			stack.top->size = xval.size;
			stack.top->val = xval.val;

			stack.top++;
		}
	}

	/*
	 * Now that the entire chunk has been parsed, the resulting value
	 * should be the only item left on the stack.  Return it.
	 */

	if (expr) {
		rc = ddl_pop(expr, &stack);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (stack.top != stack.data) {
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_eval_expr
 *	ShortDesc: Parse and evaluate the binary encoded expression.
 *
 *	Description:
 *		ddl_eval_expr will unpack a binary encoded expression and
 *		return the expression along with dependency information.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is not an expression or length
 *				is incorrect.
 *		error returns from DDL_PARSE_TAG() and ddl_do_eval_expr().
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_eval_expr(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;
	DDL_UINT        length, tag;	/* Length and type of this binary */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_EVAL_EXPR);
#endif

	/*
	 * Evaluate the expression, rather than extracting it. When given an
	 * expression, it evaluates the current value.
	 */

	/*
	 * Parse the tag, and make sure it is an EXPRESSION_TAG.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &length);

	if ((DDL_UINT) EXPRESSION_TAG != tag) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Make sure the length makes sense.
	 */

	if ((DDL_UINT) 0 == length || (DDL_UINT) length > *size) {
		return DDL_ENCODING_ERROR;
	}

	*size -= length;

	/*
	 * Parse and evalutate the expression
	 */

	rc = ddl_do_eval_expr(chunkp, &length, expr, depinfo, env_info, var_needed);

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_expr_choice
 *	ShortDesc: Choose the correct expression from a binary.
 *
 *	Description:
 *		ddl_expr_choice will parse the binary for an expression,
 *		according to the current conditionals (if any).  The value of
 *		the expression is returned, along with dependency information.
 *		If a value is found, the data_valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		expr - pointer to an EXPR structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_eval_expr().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_expr_choice(
unsigned char **chunkp,
DDL_UINT       *size,
EXPR           *expr,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_EXPR_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

here:
	DDL_UINT tag=DDL_PEEK_TAG(**chunkp);

	if(tag==EXPRESSION_TAG)
	{
		ASSERT_DBG((data_valid != NULL) || (expr == NULL));

		rc = ddl_eval_expr(chunkp, size, expr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (expr && data_valid) {
			*data_valid = TRUE;
		}
	}
	else if(tag==EXPRESSION_LIST_TAG)
	{
		DDL_UINT        len;	/* # of DATA bytes associated with TAG */
		tag = 0;	/* "label" associated with current chunk */
		DDL_PARSE_TAG(chunkp, size, &tag, &len);
		goto here;
	}
	else
	{
		rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}


		/*
		 * A chunk was found and a value is requested.
		 */

		if (expr && val.chunk) {

			/*
			 * If the calling routine is expecting a value, data_valid
			 * cannot be NULL
			 */

			ASSERT_DBG((data_valid != NULL) || (expr == NULL));


			/* We have the actual value.  Parse it. */

			rc = ddl_eval_expr(&val.chunk, &val.size, expr, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (expr && data_valid) {
				*data_valid = TRUE;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_promote_to
 *	ShortDesc: Promotes an EXPR to the specified type and size.
 *
 *	Description:
 *		ddl_promote_to will use ddl_convert_type() in order to
 *		promote the passed EXPR to the requested one.  If ddl_convert_type()
 *		does not succeed or if the resulting type and size were not
 *		the ones requested, an error code will be returned.
 *		NOTE: ddl_promote_to will only promote expressions, it will
 *			not succeed if the requested change requires a demotion.
 *
 *	Inputs:
 *		expr - pointer to expression to be converted
 *		new_type - desired type to convert into
 *		new_size - desired size to convert into
 *
 *	Outputs:
 *		expr - pointer to the modified expression
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE - The conversion was unsuccessful.
 *		error return from ddl_convert_type()
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

int
ddl_promote_to(
EXPR           *expr,
unsigned short  new_type,
unsigned int  new_size)
{
	EXPR            target;	/* target expression */
	int             rc;

#ifdef DDSTEST
	TEST_FAIL(DDL_PROMOTE_TO);
#endif

	target.type = new_type;
	target.size = new_size;

	switch (new_type) {
	case INTEGER:
		target.val.i = 0;
		break;
	case UNSIGNED:
		target.val.u = 0;
		break;
    case FLOATG_PT:
		target.val.f = (float) 0.0;
		break;
    case DOUBLEG_PT:
		target.val.d = 0.0;
		break;
	default:
		return DDL_BAD_VALUE_TYPE;
	}

	rc = ddl_convert_type(expr, &target);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * If the conversion did not produce the type and size requested,
	 * return error.
	 */

	if ((expr->type != new_type) || (expr->size != new_size)) {
		return DDL_BAD_VALUE_TYPE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_exprs_equal
 *	ShortDesc: Determines if two expressions are equal.
 *
 *	Description:
 *		ddl_exprs_equal will compare two expressions and return 1
 *		if they are equal and 0 if not.
 *
 *	Inputs:
 *		expr1 - pointer to first expression
 *		expr2 - pointer to second expression
 *
 *	Outputs:
 *
 *	Returns:
 *		1 - expressions are equal
 *		0 - expressions are not equal
 *		error return from ddl_equal()
 *
 *	Author:
 *		Bruce Davis
 *
 *********************************************************************/

int
ddl_exprs_equal(
EXPR           *expr1,
EXPR           *expr2)
{
	EXPR            x1, x2;	/* Temp expression structs */
	int             rc = DDL_ENCODING_ERROR;

#ifdef DDSTEST
	TEST_FAIL(DDL_EXPRS_EQUAL);
#endif

	memcpy((char *) &x1, (char *) expr1, sizeof(EXPR));
	memcpy((char *) &x2, (char *) expr2, sizeof(EXPR));

	rc = ddl_convert_type(&x1, &x2);
	if (rc != DDL_SUCCESS) 
	{
		return rc;
	}

	switch (x1.type) 
	{
		case INTEGER:
			rc = (x1.val.i == x2.val.i);
			break;

		case UNSIGNED:
			rc = (x1.val.u == x2.val.u);
			break;

        case FLOATG_PT:
			rc = (x1.val.f == x2.val.f);
			break;

        case DOUBLEG_PT:
			rc = (x1.val.d == x2.val.d);
			break;

		default:
			rc = DDL_ENCODING_ERROR;
			break;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_attr_expr
 *	ShortDesc: Evaluate an expression
 *
 *	Include: libddl.h
 *
 *	Description:
 *		The eval_attr_expr function evaluates an expression.
 *		The buffer pointed to by chunk should contain an
 *		expression returned from fetch_var_attr, and size
 *		should specify its size.
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - pointer to the size of the binary
 *		expr - pointer to an EXPR where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		value - pointer to an EXPR containing the result.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			a)	ddl_expr_choice()
 *			b)	ddl_free_depinfo()
 *			c)	ddl_shrink_depinfo()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
eval_attr_expr(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	int             rc, data_valid;

#ifdef DDSTEST
	TEST_FAIL(EVAL_EXPR);
#endif
	EXPR   *expr = static_cast<EXPR*>(voidP);
	data_valid = FALSE;
	memset((char *) expr, 0, sizeof(EXPR));	/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_expr_choice(&chunk, &size, expr, depinfo, &data_valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (expr && !data_valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_int_expr
 *	ShortDesc: Evaluate an integer expression
 *
 *	Description:
 *		The eval_attr_int_expr function evaluates an expression.
 *		The buffer pointed to by chunk should contain an
 *		expression returned from fetch_var_attr, and size
 *		should specify its size.
 *		If the expression is an arithmetic type (ie. int, float,
 *		double, unsigned), the expression will be cast to an
 *		unsigned long (ie. DDL_UINT).
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - pointer to the size of the binary
 *		value - pointer to a DDL_UINT where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		value - pointer to a DDL_UINT containing the result.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_BAD_VALUE_TYPE
 *		return codes from:
 *			a)	eval_attr_expr()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
eval_attr_int_expr(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	unsigned long*	value = static_cast<unsigned long*>(voidP);
	int             rc;
	EXPR            expr;

#ifdef DDSTEST
	TEST_FAIL(EVAL_INT_EXPR);
#endif

	*value = 0;		/** initialize output parameter **/

	if (value) {

		rc = eval_attr_expr(chunk, size, &expr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		switch (expr.type) {
		case INTEGER:
			*value = (DDL_UINT) expr.val.i;
			break;
		case UNSIGNED:
			*value = (DDL_UINT) expr.val.u;
			break;
        case FLOATG_PT:
			*value = (DDL_UINT) expr.val.f;
			break;
        case DOUBLEG_PT:
			*value = (DDL_UINT) expr.val.d;
			break;
		default:	/* UNUSED, ASCII */
			return DDL_BAD_VALUE_TYPE;
			/* NOTREACHED */
			break;
		}
	}
	else {
		rc = eval_attr_expr(chunk, size, (EXPR *) NULL, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	find_any_attr
 *
 *	ShortDesc:	finds and decodes requested attribute and puts results 
 *				int expr
 *
 *	Inputs:
 *		id	- item id
 *		expr - value output
 *		which - subindex
 *		attr_name - name of attribute requested
 *		env_inf	- envirion info
 *
 *	Outputs:
 *		expr - value output
 *
 *	Returns:
 *		error
 *
 *
 *********************************************************************/

static
int
find_any_attr(
	DESC_REF		                    *desc_ref,			// Reference
	EXPR								*pValue,
	int									which,
	::nsEDDEngine::AttributeName		attr_name,
	nsEDDEngine::OP_REF_LIST			* /* depinfo */,
	ENV_INFO							*env_info,
	nsEDDEngine::OP_REF					* /* var_needed */)
{
    int rc = 0;
	DDI_GENERIC_ITEM gi = {0};
	DDI_BLOCK_SPECIFIER bs = {0};

    if (  (desc_ref->type == LIST_ITYPE && 
            (attr_name == ::nsEDDEngine::AttributeName::count ||
             attr_name == ::nsEDDEngine::AttributeName::first ||
             attr_name == ::nsEDDEngine::AttributeName::last))
          ||
          (desc_ref->type == AXIS_ITYPE && 
            (attr_name == ::nsEDDEngine::AttributeName::view_max ||
             attr_name == ::nsEDDEngine::AttributeName::view_min)))
    {
        // don't do anything
    }
    else    // If none of the preceeding conditions are true get the needed info from the dd
    {
	    nsEDDEngine::FDI_ITEM_SPECIFIER is= {nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID, 0, 0};

	    bs.type = DDI_BLOCK_HANDLE;
	    bs.block.handle = env_info->block_handle;

	    is.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
	    is.item.id=desc_ref->id;

	    //fix masks throughout , indent level when evaling indentLevel++ inside of eval
	    nsEDDEngine::AttributeNameSet masks;
	    masks+=attr_name;

	    rc = ddi_get_item(&bs, &is, env_info, masks, &gi); 

	    if(rc)
		    return rc;
    }

	switch(desc_ref->type)
	{
	case VARIABLE_ITYPE:
		{
			::nsEDDEngine::FLAT_VAR *var=(::nsEDDEngine::FLAT_VAR *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::label:
				FDI_ConvSTRING::CopyTo(&var->label,&pValue->val.s);
				pValue->type=ASCII;
				pValue->size=0;
				break;

			case ::nsEDDEngine::AttributeName::constant_unit:
				FDI_ConvSTRING::CopyTo(&var->constant_unit,&pValue->val.s);
				pValue->type=ASCII;
				pValue->size=0;
				break;

			case ::nsEDDEngine::AttributeName::help:
				FDI_ConvSTRING::CopyTo(&var->help,&pValue->val.s);
				pValue->type=ASCII;
				pValue->size=0;
				break;

		//	case ::nsEDDEngine::AttributeName::variable_status: ???
			case ::nsEDDEngine::AttributeName::x_axis: 
			case ::nsEDDEngine::AttributeName::y_axis: 
			case ::nsEDDEngine::AttributeName::scaling:
			case ::nsEDDEngine::AttributeName::capacity:
			case ::nsEDDEngine::AttributeName::count:
			case ::nsEDDEngine::AttributeName::first:
			case ::nsEDDEngine::AttributeName::number_of_elements:
				return E_FAIL;
				break;

			case ::nsEDDEngine::AttributeName::initial_value:
				::FDI_ConvEXPR::CopyTo(&var->initial_value, pValue);
				break;

			case ::nsEDDEngine::AttributeName::scaling_factor:
				::FDI_ConvEXPR::CopyTo(&var->scaling_factor, pValue);
				break;

			case ::nsEDDEngine::AttributeName::default_value:
				::FDI_ConvEXPR::CopyTo(&var->default_value, pValue);
				break;

			case ::nsEDDEngine::AttributeName::min_value:
				if(!var->min_val.list)
					return E_FAIL;

				if(var->min_val.count <= which)
					return E_FAIL;

				::FDI_ConvEXPR::CopyTo(&var->min_val.list[which], pValue);
				break;

			case ::nsEDDEngine::AttributeName::max_value:

				if(!var->max_val.list)
					return E_FAIL;

				if(var->max_val.count <= which)
					return E_FAIL;

				::FDI_ConvEXPR::CopyTo(&var->max_val.list[which], pValue);
				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete var;
		}
		break;

	case ARRAY_ITYPE:	
		{
			::nsEDDEngine::FLAT_ARRAY *arr=(::nsEDDEngine::FLAT_ARRAY *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::number_of_elements:
				pValue->val.u=arr->num_of_elements;
				pValue->type=UNSIGNED;
				pValue->size=0;
				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete arr;
		}
		break;

	case COLLECTION_ITYPE:
		{
			::nsEDDEngine::FLAT_COLLECTION *coll=(::nsEDDEngine::FLAT_COLLECTION *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::label:
				FDI_ConvSTRING::CopyTo(&coll->label,&pValue->val.s);
				pValue->type=ASCII;
				pValue->size=0;
				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete coll;
		}
		break;
	case BLOB_ITYPE:
		{
			::nsEDDEngine::FLAT_BLOB *blob=(::nsEDDEngine::FLAT_BLOB *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::count:
				pValue->val.u=blob->count;
				pValue->type=UNSIGNED;
				pValue->size=0;
				break;

			case ::nsEDDEngine::AttributeName::identity:
				FDI_ConvSTRING::CopyTo(&blob->identity,&pValue->val.s);
				pValue->type=ASCII;
				pValue->size=0;
				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete blob;
		}
		break;

	case LIST_ITYPE:
		{
			::nsEDDEngine::FLAT_LIST *list=(::nsEDDEngine::FLAT_LIST *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::capacity:
				pValue->val.u=list->capacity;//->cap;
				pValue->type=UNSIGNED;
				pValue->size=0;
				break;

			case ::nsEDDEngine::AttributeName::count:
			case ::nsEDDEngine::AttributeName::first:
			case ::nsEDDEngine::AttributeName::last:
                {
					if (env_info->handle_type == ENV_INFO::BlockHandle)
					{
						nsConsumer::EVAL_VAR_VALUE  list_info;	/* storage area for upcall values */
						nsConsumer::PC_ErrorCode pc_error_code = nsConsumer::PC_OTHER_EC;
						IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);

						pc_error_code =  pDDSSupport->DDS_GetDynamicAttribute( env_info->block_handle, env_info->value_spec, desc_ref, attr_name, &list_info );
						if (pc_error_code == nsConsumer::PC_SUCCESS_EC)
						{
							rc = ddl_EvalVarToExpr(&list_info, pValue,0);
						}
						else
						{
							rc = VARIABLE_VALUE_NEEDED;
						}
					}
					else
					{
						rc = DDS_INTERNAL_ERROR;
					}
                }

				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete list;
		}
		break;

	case AXIS_ITYPE:
		{
			::nsEDDEngine::FLAT_AXIS *axis=(::nsEDDEngine::FLAT_AXIS *)gi.item;

			switch(attr_name)
			{
			case ::nsEDDEngine::AttributeName::max_value:
				::FDI_ConvEXPR::CopyTo(&axis->max_axis, pValue);
				break;

			case ::nsEDDEngine::AttributeName::min_value:
				::FDI_ConvEXPR::CopyTo(&axis->min_axis, pValue);
				break;

			case ::nsEDDEngine::AttributeName::scaling:
				pValue->val.u=axis->scaling;
				pValue->type=UNSIGNED;
				pValue->size=0;
				break;

			case ::nsEDDEngine::AttributeName::view_max:
			case ::nsEDDEngine::AttributeName::view_min:
                {
					if (env_info->handle_type == ENV_INFO::BlockHandle)
					{
	                    nsConsumer::EVAL_VAR_VALUE  view_info;	/* storage area for upcall values */
						nsConsumer::PC_ErrorCode pc_error_code = nsConsumer::PC_OTHER_EC;
						IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);

						pc_error_code =  pDDSSupport->DDS_GetDynamicAttribute( env_info->block_handle, env_info->value_spec, desc_ref, attr_name, &view_info );
						if (pc_error_code == nsConsumer::PC_SUCCESS_EC)
						{
							rc = ddl_EvalVarToExpr(&view_info, pValue,0);
						}
						else
						{
							rc = VARIABLE_VALUE_NEEDED;
						}
					}
					else
					{
						rc = DDS_INTERNAL_ERROR;
					}

				    break;
                }

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}

			delete axis;
		}
		break;
	case MENU_ITYPE:
		{
			::nsEDDEngine::FLAT_MENU *menu = (::nsEDDEngine::FLAT_MENU *)gi.item;

			switch (attr_name)
			{
			case ::nsEDDEngine::AttributeName::label:
				FDI_ConvSTRING::CopyTo(&menu->label, &pValue->val.s);
				pValue->type = ASCII;
				pValue->size = 0;
				break;
			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
				break;
			}
			delete menu;
		}
		break;
	
	default:
		ASSERT(0);
		return DDL_ENCODING_ERROR;
		break;
	}

	return rc;
}

/*********************************************************************
 *
 *	Name:	get_attribute_value
 *
 *	ShortDesc:	finds and decodes requested attribute and puts results 
 *				int expr
 *
 *	Inputs:
 *		id	- item id
 *		expr - value output
 *		which - subindex
 *		attr_name - name of attribute requested
 *		env_inf	- envirion info
 *
 *	Outputs:
 *		expr - value output
 *
 *	Returns:
 *		error
 *
 *
 *********************************************************************/
int
get_attribute_value(
::nsEDDEngine::AttributeName attrib_name,			// Selector tag
DDL_UINT		which,				//		Selector which (if min/max)
DESC_REF		*desc_ref,			// Reference
EXPR			*expr,				// out value
nsEDDEngine::OP_REF_LIST		*depinfo,			// normal eval args...
ENV_INFO		*env_info,
nsEDDEngine::OP_REF			*var_needed)
{
	int rc = DDL_SUCCESS;

	switch(desc_ref->type)
	{
	case AXIS_ITYPE:
	case ARRAY_ITYPE:
	case VARIABLE_ITYPE:
	case LIST_ITYPE:
	case BLOB_ITYPE:
	case COLLECTION_ITYPE:
	case MENU_ITYPE:
		rc = find_any_attr(desc_ref, expr, which, attrib_name, depinfo, env_info, var_needed);
		break;

	default:
		PS_TRACE(L"\n*** error : get_attribute_value - unsupported itype case %d\n", desc_ref->type);
		ASSERT(0);
		return DDL_ENCODING_ERROR;
		break;
	}

	return rc;
} 

/*********************************************************************
 *
 *	Name:	ddl_parse_byte
 *
 *	ShortDesc:	parses a single byte
 *
 *	Description:
 *		parses a single byte
 *
 *	Inputs:
 *		chunkp -			chunk
 *		size -				chunk size
 *
 *	Outputs:
 *		val -				byte
 *
 *	Returns:
 *		error
 *
 *********************************************************************/

int ddl_parse_byte(unsigned char** chunkp,
					   DDL_UINT* size,
					   DDL_UINT* val)
{
	if(*size<=0)
		return DDL_ENCODING_ERROR;

	*val=(*chunkp)[0];

	(*chunkp)++;
	*size=*size-1;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	ddl_free_expr
 *
 *	ShortDesc:	free expr value
 *
 *	Description:
 *		free expr value
 *
 *	Inputs:
 *		expr -			expression to clean
 *
 *	Returns:
 *		void
 *
 *********************************************************************/

void
ddl_free_expr(EXPR* expr)
{
	if (expr != NULL)
	{
		switch (expr->type)
		{
		case ASCII:
		case PACKED_ASCII:
		case PASSWORD:
		case EUC:  
		case VISIBLESTRING:
			ddl_free_string(&expr->val.s);
			break;
		case BITSTRING:
		case OCTETSTRING:
			free(expr->val.b.ptr);
			expr->val.b.ptr = nullptr;
			expr->val.b.len = 0;
			break;
		default:	// other types don't need to be freed.
			break;
		}

		expr->size = 0;
		expr->type = 0;
		expr->val.d = 0.0;
	}
}

