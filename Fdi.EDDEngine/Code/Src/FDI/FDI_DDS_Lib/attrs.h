/*
 * @(#) $Id: attrs.h,v 1.7 2012/09/12 22:40:55 rgretta Exp $
 */

#pragma once

#include "ddldefs.h"
//#include "AttributeName.h"

#include "nsEDDEngine/Attrs.h"

#ifndef OD_DEFS_H
#include "od_defs.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif

typedef struct tag_STRING {
	wchar_t        *str;	/* the pointer to the UNICODE string */
	unsigned short  len;	/* the length in characters of the string */
	enum Flags {
		NOT_SET, DONT_FREE_STRING, FREE_STRING
	} flags;				/* memory allocation flags */
}	STRING;


typedef struct tag_BINARY {
	uchar *	ptr;			/* Start of bytestream */
	int		len;			/* Length of bytestream */
} BINARY;

/*
 * Expressions
 */

/*
 * Types of expressions.
 * The permitted type fields are the same as variables types (ddldefs.h)
 */


typedef struct tag_EXPR {
	unsigned short  type;	/* valid types are defined in ddldefs.h (ie. INTEGER) */
	unsigned int  size;
	union tag_EXPR_Union {
		float				 f;		// FLOAT
		double				 d;		// DOUBLE
		unsigned long long   u;		// UNSIGNED
		long long			 i;		// INTEGER
		STRING				 s;		// ASCII
		BINARY				 b;		// BITSTRING   
	} val;
}               EXPR;

/*
 *  RANGE_DATA_LIST
 */

typedef struct tag_RANGE_DATA_LIST {
	unsigned short  count;
	unsigned short  limit;
	EXPR           *list;
}               RANGE_DATA_LIST;


/*
 * Defines for REFERENCE_ITEM types
 */


typedef struct tag_ITEM_ID_LIST {
	unsigned short  count;		/* Number of used item IDs */
	unsigned short  limit;		/* Total number of item IDs */
	ITEM_ID        *list;		/* Pointer to list of IDs */
}               ITEM_ID_LIST;


typedef struct tag_DESC_REF {

	ITEM_ID         id;
	ITEM_TYPE       type;
}               DESC_REF;

typedef struct tag_RESOLVE_INFO {

	ITEM_TYPE       type;
	ITEM_ID         id;
	unsigned long   element;
}               RESOLVE_INFO;

typedef struct tag_OP_REF_INFO {
	ITEM_ID			id;
	ITEM_ID			member;
	ITEM_TYPE		type;
}				OP_REF_INFO;

typedef struct tag_OP_REF_INFO_LIST {
	unsigned short  count;
	unsigned short  limit;
	OP_REF_INFO     *list;
}				OP_REF_INFO_LIST;

enum OpType
{
	STANDARD_TYPE	= 0,
	COMPLEX_TYPE	= 1
};

typedef struct tag_OP_REF_TRAIL {

	OpType				op_ref_type;
	OP_REF_INFO			op_info;
	OP_REF_INFO_LIST	op_info_list;

	int				block_instance;

	ITEM_ID         desc_id;
	ITEM_TYPE       desc_type;
	unsigned long	bit_mask;  /* Used for BIT_ENUM variables on menus, set to 0 if unused */

	unsigned short  trail_count;
	unsigned short  trail_limit;
	RESOLVE_INFO   *trail;
	EXPR			expr;

}               OP_REF_TRAIL;


typedef struct tag_OP_REF_TRAIL_LIST {
	unsigned short  count;
	unsigned short  limit;
	OP_REF_TRAIL   *list;
}               OP_REF_TRAIL_LIST;


typedef struct tag_BINARY_IMAGE {
    unsigned long length;
    unsigned char *data;
} BINARY_IMAGE;


/*
 * Binary and Dependency Info
 */


typedef struct tag_BININFO {
	unsigned long   size;
	unsigned char  *chunk;
}               BININFO;



typedef struct tag_DEPBIN {
	nsEDDEngine::OP_REF_LIST     dep;
	unsigned long   bin_size;
	unsigned char  *bin_chunk;
}               DEPBIN;




/* The masks for ITEM_ARRAY_ELEMENT */

#define IA_DESC_EVALED		0X01
#define IA_HELP_EVALED		0X02
#define IA_INDEX_EVALED		0X04
#define IA_REF_EVALED		0X08


typedef struct tag_ITEM_ARRAY_ELEMENT {
	unsigned short  evaled;
	unsigned long   index;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
}               ITEM_ARRAY_ELEMENT;

typedef struct tag_ITEM_ARRAY_ELEMENT_LIST {
	unsigned short  count;
	unsigned short  limit;
	ITEM_ARRAY_ELEMENT *list;
}               ITEM_ARRAY_ELEMENT_LIST;


/* The masks for MEMBER */

#define MEM_DESC_EVALED		0X01
#define MEM_HELP_EVALED		0X02
#define MEM_NAME_EVALED		0X04
#define MEM_REF_EVALED		0X08
#define MEM_NAME_STR_EVALED	0X10

typedef struct tag_MEMBER {
	unsigned short  evaled;
	unsigned long   name;
	unsigned long   symbol_index;
	DESC_REF        ref;
	STRING          desc;
	STRING          help;
}               MEMBER;

typedef struct tag_MEMBER_LIST {
	unsigned short  count;
	unsigned short  limit;
	MEMBER         *list;
}               MEMBER_LIST;


typedef struct tag_OP_MEMBER {
	unsigned short  evaled;
	unsigned long   name;
	unsigned long   symbol_index;
	OP_REF_TRAIL    ref;
	STRING          desc;
	STRING          help;
}               OP_MEMBER;

typedef struct tag_OP_MEMBER_LIST {
	unsigned short  count;
	unsigned short  limit;
	OP_MEMBER       *list;
}               OP_MEMBER_LIST;



/*
 * Menu
 */

typedef struct tag_MENU_ITEM {
	OP_REF_TRAIL    ref;
	unsigned short  qual;
}               MENU_ITEM;

typedef struct tag_MENU_ITEM_LIST {
	unsigned short  count;
	unsigned short  limit;
	MENU_ITEM      *list;
}               MENU_ITEM_LIST;

typedef struct tag_PURPOSE {
	unsigned long value;
	STRING user_defined;
}				PURPOSE;


/*
* Component
*/

typedef struct tag_NUMBER_EXPR {
	ulong which;
	EXPR value;
}               NUMBER_EXPR;

typedef struct tag_RANGE_SET {
	//EXPR max_val;
	//EXPR min_val;
	NUMBER_EXPR max_num;
	NUMBER_EXPR min_num;
}               RANGE_SET;


typedef struct tag_RANGE_LIST {
	DESC_REF	var_refrences;

	unsigned short  count;
	unsigned short  limit;
	RANGE_SET       *list;
}               RANGE_LIST;

typedef struct tag_COMPONENT_SPECIFIER {
	OP_REF_TRAIL  cr_ref;
	//OP_REF_TRAIL    ref;
	EXPR maximum_number;
	EXPR minimum_number;
	EXPR auto_create;
	bool filter;
	RANGE_LIST range_list;
	
}               COMPONENT_SPECIFIER;

typedef struct tag_COMPONENT_SPECIFIER_LIST {
	unsigned short  count;
	unsigned short  limit;
	COMPONENT_SPECIFIER     *list;
}               COMPONENT_SPECIFIER_LIST;

/*
 * Response Codes
 */

/* The masks for RESPONSE_CODE */

#define RS_DESC_EVALED		0X01
#define RS_HELP_EVALED		0X02
#define RS_TYPE_EVALED		0X04
#define RS_VAL_EVALED		0X08


typedef struct tag_RESPONSE_CODE {
	unsigned short  evaled;
	unsigned long   val;
	unsigned short  type;
	STRING          desc;
	STRING          help;
}               RESPONSE_CODE;

typedef struct tag_RESPONSE_CODE_LIST {
	ITEM_ID			ref_id;
	unsigned short  count;
	unsigned short  limit;
	RESPONSE_CODE  *list;
}               RESPONSE_CODE_LIST;

typedef struct tag_RESPONSE_CODES {
	unsigned short type;

	union tag_RESPONSE_CODES_Union {
		ITEM_ID				resp_code_ref;
		RESPONSE_CODE_LIST	resp_code_list;
	} response_codes;

}	RESPONSE_CODES;

/*
 * Relation Types
 */

typedef struct tag_REFRESH_RELATION {

	OP_REF_TRAIL_LIST depend_items;
	OP_REF_TRAIL_LIST update_items;

}               REFRESH_RELATION;


typedef struct tag_UNIT_RELATION {

	OP_REF_TRAIL    var;
	OP_REF_TRAIL_LIST var_units;

}               UNIT_RELATION;


/*
 * Definitions
 */

typedef struct tag_DEFINITION {
	unsigned long   size;
	wchar_t        *data;
}               DEFINITION;


/*
 * Actions
 */


typedef struct tag_ACTION {

	unsigned short type;

	union tag_ACTION_Union {
		ITEM_ID			meth_ref;		// ACTION_TYPE_REFERENCE
		OP_REF_TRAIL	meth_ref_args;	// ACTION_TYPE_REF_WITH_ARGS
		DEFINITION		meth_definition;// ACTION_TYPE_DEFINITION
	}	action;

}	ACTION;

typedef struct tag_ACTION_LIST {
	unsigned short	count;
	unsigned short	limit;
	ACTION			*list;
}	ACTION_LIST;


/*
 * Variable Types
 */

typedef struct tag_TYPE_SIZE {
	unsigned short  type;
	unsigned int  size;
}               TYPE_SIZE;



/*
 * Enumerations
 */

/* The masks for ENUM_VALUE */

#define ENUM_ACTIONS_EVALED		0X01
#define ENUM_CLASS_EVALED		0X02
#define ENUM_DESC_EVALED		0X04
#define ENUM_HELP_EVALED		0X08
#define ENUM_STATUS_EVALED		0X10
#define ENUM_VAL_EVALED			0X20
#define ENUM_ENUMVAL_EVALED		0X40

typedef struct tag_STATUS_OUTPUT_CLASS
{
	int mode_and_reliability;
	int which_output;
} STATUS_OUTPUT_CLASS;

typedef struct tag_STATUS_CLASS_ELEM
{
	int base_class;
	STATUS_OUTPUT_CLASS output_class;
} STATUS_CLASS_ELEM;

typedef struct tag_STATUS_CLASS
{
	unsigned short count;
	unsigned short limit;
	STATUS_CLASS_ELEM *list;
} STATUS_CLASS;

typedef struct tag_ENUM_VALUE {
	unsigned short  evaled;
	EXPR            val;
	STRING          desc;
	STRING          help;
	unsigned long   func_class;	/* functional class */
	STATUS_CLASS	status_class;
	ITEM_ID         actions;
}               ENUM_VALUE;

typedef struct tag_ENUM_VALUE_LIST {
	unsigned short  count;
	unsigned short  limit;
	ENUM_VALUE     *list;
}               ENUM_VALUE_LIST;

/*
 * Data Fields
 */

typedef struct tag_DATA_ITEM {
	union tag_DATA_ITEM_Union {
		long			iconst;
		OP_REF_TRAIL    ref;
		float			fconst;
		double			dconst;
		unsigned long	uconst;
	}			data;
	unsigned short  type;
	unsigned short  flags;
	unsigned short  width;
	unsigned long long   data_item_mask;
}               DATA_ITEM;

typedef struct tag_DATA_ITEM_LIST {
	unsigned short  count;
	unsigned short  limit;
	DATA_ITEM      *list;
}               DATA_ITEM_LIST;

/*
 * Vector Fields
 */

typedef struct tag_VECTOR {
	STRING			description;
	OP_REF_TRAIL_LIST values;
}               VECTOR;


typedef struct tag_VECTOR_LIST {
	unsigned short  count;
	unsigned short  limit;
	VECTOR      *vectors;
}               VECTOR_LIST;

/*
 * Transactions
 */

typedef struct tag_TRANSACTION {
	unsigned long   number;
	DATA_ITEM_LIST  request;
	DATA_ITEM_LIST  reply;
	RESPONSE_CODES	resp_codes;
	ACTION_LIST		post_actions;
}               TRANSACTION;


typedef struct tag_TRANSACTION_LIST {
	unsigned short  count;
	unsigned short  limit;
	TRANSACTION    *list;
}               TRANSACTION_LIST;


#ifdef __cplusplus
    }
#endif

