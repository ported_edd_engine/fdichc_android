#-------------------------------------------------
#
# Project created by QtCreator 2017-12-05T17:14:24
#
#-------------------------------------------------

QT       -= gui

TARGET = DevServices
TEMPLATE = lib
#CONFIG +=plugin
CONFIG += staticlib
DEFINES += DEVSERVICES_LIBRARY \
           UNICODE \
           _FULL_RULE_ENGINE \
QMAKESPEC=linux-g++-32
QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CC = $$QMAKE_CXX
    QMAKE_CFLAGS  = $$QMAKE_CXXFLAGS

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += ../BuiltinLib/FFBuiltIn.cpp \
    ddbAttributes.cpp \
    ddbCommand.cpp \
    ../../../COMMON/ddbdefs.cpp \
    ddbDevice.cpp \
    ddbItemBase.cpp \
    ddbItemLists.cpp \
    ddbList.cpp \
    ddbPrimatives.cpp \
    ddbReference.cpp \
    foundation.cpp \
    ../Interpreter/ArrayExpression.cpp \
    ../Interpreter/AssignmentStatement.cpp \
    ../Interpreter/BreakStatement.cpp \
    ../Interpreter/CaseStatement.cpp \
    ../Interpreter/ComplexDDExpression.cpp \
    ../Interpreter/CompoundExpression.cpp \
    ../Interpreter/CompoundStatement.cpp \
    ../Interpreter/ContinueStatement.cpp \
    ../Interpreter/Declaration.cpp \
    ../Interpreter/Declarations.cpp \
    ../Interpreter/ELSEStatement.cpp \
    ../Interpreter/EmptyStatement.cpp \
    ../Interpreter/ExpParser.cpp \
    ../Interpreter/Expression.cpp \
    ../Interpreter/FunctionExpression.cpp \
    ../Interpreter/GrammarNode.cpp \
    ../Interpreter/GrammarNodeVisitor.cpp \
    ../Interpreter/IFExpression.cpp \
    ../Interpreter/INTER_SAFEARRAY.cpp \
    ../Interpreter/INTER_VARIANT.cpp \
    ../Interpreter/InterpretedVisitor.cpp \
    ../Interpreter/Interpreter.cpp \
    ../Interpreter/IterationDoWhile.cpp \
    ../Interpreter/IterationFor.cpp \
    ../Interpreter/IterationStatement.cpp \
    ../Interpreter/LexicalAnalyzer.cpp \
    ../Interpreter/OMServiceExpression.cpp \
    ../Interpreter/OptionalStatements.cpp \
    ../Interpreter/ParseEngine.cpp \
    ../Interpreter/Parser.cpp \
    ../Interpreter/ParserBuilder.cpp \
    ../Interpreter/PrimaryExpression.cpp \
    ../Interpreter/Program.cpp \
    ../Interpreter/ReturnStatement.CPP \
    ../Interpreter/RuleServiceStatement.cpp \
    ../Interpreter/SelectionStatement.cpp \
    ../Interpreter/ServiceStatement.cpp \
    ../Interpreter/Statement.cpp \
    ../Interpreter/StatementList.cpp \
    ../Interpreter/SwitchStatement.cpp \
    ../Interpreter/SymbolTable.cpp \
    ../Interpreter/SynchronisationSet.cpp \
    ../Interpreter/Token.cpp \
    ../Interpreter/TypeCheckVisitor.cpp \
    ../Interpreter/Variable.cpp \
    ../BuiltinLib/AbortBuiltins.cpp \
    ../BuiltinLib/BuiltIn.cpp \
    ../BuiltinLib/BuiltInInfo.cpp \
    ../BuiltinLib/BuiltinInvoke.cpp \
    ../BuiltinLib/BuiltInMapper.cpp \
    ../BuiltinLib/BuiltinUtil.cpp \
    ../BuiltinLib/DateTimeBuiltins.cpp \
    ../BuiltinLib/Delay_Builtin.cpp \
    ../BuiltinLib/HartBuiltIn.cpp \
    ../BuiltinLib/InvokeReparamHelper.cpp \
    ../BuiltinLib/MathBuiltins.cpp \
    ../BuiltinLib/PrePostActionsBuiltins.cpp \
    ../BuiltinLib/ProfiBuiltIn.cpp \
    ../BuiltinLib/ResolveBuiltins.cpp \
    ../BuiltinLib/SendCmdBuiltins.cpp \
    ../BuiltinLib/SetBuiltIns.cpp \
    ../BuiltinLib/StringBuiltins.cpp \
    ../BuiltinLib/UIBuiltins.cpp \
    ../MEE/MEE.cpp \
    stdafx.cpp \

HEADERS += ../../../../../../inc/3rdParty/panic.h\
    ../../../COMMON/Char.h \
    ../../../COMMON/ddbdefs.h \
    ../../../COMMON/DDLDEFS.h \
    ../../../COMMON/dllapi.h \
    ../../../COMMON/foundation.h \
    ../../../COMMON/messageUI.h \
    ../../../COMMON/MethodInterfaceDefs.h \
    ../../../COMMON/varientTag.h \
    ../../../APPS/APPsupport/BuiltinLib/BuiltIn.h \
    ../../../APPS/APPsupport/BuiltinLib/BuiltInInfo.h \
    ../../../APPS/APPsupport/BuiltinLib/BuiltInMapper.h \
    ../../../APPS/APPsupport/BuiltinLib/FFBuiltIn.h \
    ../../../APPS/APPsupport/BuiltinLib/HartBuiltIn.h \
    ../../../APPS/APPsupport/BuiltinLib/InvokeReparamHelper.h \
    ../../../APPS/APPsupport/BuiltinLib/NAN_DEF.h \
    ../../../APPS/APPsupport/BuiltinLib/RTN_CODE.H \
    ../../../APPS/APPsupport/DDParser/std.h \
    ../../../APPS/APPsupport/Interpreter/VMConstants.h \
    ddbArray.h \
    ddbAttributes.h \
    ddbAxis.h \
    ddbCmdList.h \
    ddbCollAndArr.h \
    ddbCommand.h \
    ../../../COMMON/ddbDevice.h \
    ddbDeviceService.h \
    ../../../COMMON/DDBgeneral.h \
    ddbGlblSrvInfc.h \
    ddbGrpItmDesc.h \
    DDBitemBase.h \
    ddbItemListBase.h \
    ddbList.h \
    ddbMethod.h \
    DDBpayload.h \
    ddbPrimatives.h \
    DDBreferenceNexpression.h \
    ddbResponseCode.h \
    ddbTracking.h \
    ddbVar.h \
    ../../../COMMON/HARTsupport.h \
    ../../../COMMON/varient.h \
    ../Interpreter/ArrayExpression.h \
    ../Interpreter/AssignmentStatement.h \
    ../Interpreter/BreakStatement.h \
    ../Interpreter/CaseStatement.h \
    ../Interpreter/ComplexDDExpression.h \
    ../Interpreter/CompoundExpression.h \
    ../Interpreter/CompoundStatement.h \
    ../Interpreter/ContinueStatement.h \
    ../Interpreter/Declaration.h \
    ../Interpreter/Declarations.h \
    ../Interpreter/ELSEStatement.h \
    ../Interpreter/EmptyStatement.h \
    ../Interpreter/ExpParser.h \
    ../Interpreter/Expression.h \
    ../Interpreter/FunctionExpression.h \
    ../Interpreter/GrammarNode.h \
    ../Interpreter/GrammarNodeVisitor.h \
    ../Interpreter/IFExpression.h \
    ../Interpreter/INTER_SAFEARRAY.h \
    ../Interpreter/INTER_VARIANT.h \
    ../Interpreter/InterpretedVisitor.h \
    ../Interpreter/Interpreter.h \
    ../Interpreter/IterationDoWhile.h \
    ../Interpreter/IterationFor.h \
    ../Interpreter/IterationStatement.h \
    ../Interpreter/LexicalAnalyzer.h \
    ../Interpreter/OMServiceExpression.h \
    ../Interpreter/OptionalStatements.h \
    ../Interpreter/Parser.h \
    ../Interpreter/ParserBuilder.h \
    ../Interpreter/PrimaryExpression.h \
    ../Interpreter/Program.h \
    ../Interpreter/ReturnStatement.h \
    ../Interpreter/RuleServiceStatement.h \
    ../Interpreter/SelectionStatement.h \
    ../Interpreter/ServiceStatement.h \
    ../Interpreter/Statement.h \
    ../Interpreter/StatementList.h \
    ../Interpreter/SwitchStatement.h \
    ../Interpreter/SymbolTable.h \
    ../Interpreter/SynchronisationSet.h \
    ../Interpreter/Token.h \
    ../Interpreter/TypeCheckVisitor.h \
    ../Interpreter/Variable.h \
    ../../../APPS/APPsupport/BuiltinLib/BI_CODES.H \
    ../MEE/MEE.h \

unix {
    target.path = /usr/lib/
    INSTALLS += target
}
DESTDIR = ../../../../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u
include($$DESTDIR/../../../Src/SuppressWarning.pri)


INCLUDEPATH += $$PWD/../../../COMMON/
INCLUDEPATH += $$PWD/../Interpreter/
INCLUDEPATH += $$PWD/../BuiltinLib/
INCLUDEPATH += $$PWD/../MEE/
INCLUDEPATH += $$PWD/../DDParser/
INCLUDEPATH += $$PWD/../ParserInfc/
INCLUDEPATH += $$PWD/../../../../../../inc/
INCLUDEPATH += $$PWD/../../../../../../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../../../../../../Src/EDD_Engine_Common/

android {
    INCLUDEPATH += $$PWD/../../../../../../../../Src/EDD_Engine_Android/
}
else {
    INCLUDEPATH += $$PWD/../../../../../../../../Src/EDD_Engine_Linux/
}

unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common

DEPENDPATH += $$DESTDIR/EDD_Engine_Common

unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a
