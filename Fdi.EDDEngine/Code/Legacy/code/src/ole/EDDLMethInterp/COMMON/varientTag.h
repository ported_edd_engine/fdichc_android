#pragma once

#include "varient.h"
#include "nsConsumer/IParamCache.h"

#define CV_BOOL(X)  ((X)->vValue.bIsTrue)
    	
#define CV_UI1(X)  ((X)->vValue.iIntConst)
#define CV_UI2(X)  ((X)->vValue.iIntConst)
#define CV_UINT(X) ((X)->vValue.iIntConst)
#define CV_UI4(X)  ((X)->vValue.varSymbolID)
#define CV_UI8(X)  ((X)->vValue.longlongVal)


#define CV_I1(X)  ((X)->vValue.iIntConst)
#define CV_I2(X)  ((X)->vValue.iIntConst)
#define CV_INT(X) ((X)->vValue.iIntConst)
#define CV_I4(X)  ((X)->vValue.iIntConst)
#define CV_I8(X)  ((X)->vValue.longlongVal)

#define CV_R4(X)	 ((X)->vValue.fFloatConst)
#define CV_R8(X)	 ((X)->vValue.fFloatConst)

#define CV_WSTR(X)   ((X)->sWideStringVal)
#define CV_STR(X)    ((X)->sStringVal)

#define CV_BITSTR(X, Y, Z)  ((X)->bitStringAssign(Y, Z))
#define CV_BITSTR_LEN(X)    ((X)->bsVal.bsLen)
#define CV_BITSTR_BUFFER(X) ((X)->bsVal.bs)


#define ValueTagType CValueVarient::valueTagType_t 

inline void CV_VT(CValueVarient *X, ValueTagType type)
{
	X->vTagType = type;
	switch (type)
	{
	case ValueTagType::CVT_I1:
		*X = (char)0;
		break;

	case ValueTagType::CVT_I2:
		*X = (short)0;
		break;

	case ValueTagType::CVT_INT:
		*X = (int)0;
		break;

	case ValueTagType::CVT_I4:
		*X = (int)0;
		break;

	case ValueTagType::CVT_I8:
		*X = (__int64)0;
		break;

	case ValueTagType::CVT_UI1:
		*X = (unsigned char)0;
		break;

	case ValueTagType::CVT_UI2:
		*X = (unsigned short)0;
		break;

	case ValueTagType::CVT_UI4:
		*X = (unsigned long)0;
		break;

	case ValueTagType::CVT_UINT:
		*X = (unsigned int)0;
		break;

	case ValueTagType::CVT_R4:
		*X = (float)0;
		break;

	case ValueTagType::CVT_R8:
		*X = (double)0;
		break;
	case ValueTagType::CVT_WSTR:
		*X = (wstring)L"";
		break;
	case ValueTagType::CVT_STR:
		*X = (string)"";
		break;
	default:
		break;
	}	
}
