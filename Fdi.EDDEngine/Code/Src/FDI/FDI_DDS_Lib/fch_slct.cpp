/**
 *	@(#) $Id: fch_slct.cpp,v 1.3 2012/08/30 22:33:29 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all of the functions for fetch select.  This
 *	is the "front end" of fetch and basically serves to obtain the
 *	proper DD handles and DD references (object indices or file
 *	offsets) and routing the calls to either ROD fetch or flat fetch.
 *
 *	A call to ROD fetch means that the requested data is contained
 *	in a Remote Object Dictionary (ROD) in binary form and will need
 *	to be evaluated.  A call to flat fetch means that the requested
 *	data is contained in a flat file and is already evaluated (except
 *	for any data that is dynamic).
 * 
 *	Currently, the flat fetch interface is only implemented with stub
 *	functions.  These stub functions are included in this file for
 *	the time being.
 */


#include "stdafx.h"
#include "fch_lib.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif

#define	LESS_THAN					-1
#define	GREATER_THAN				 1
#define	EQUAL						 0


/**********************************************************************
 *                                                                    *
 *                    Fetch Select Functions                          *
 *                                                                    *
 **********************************************************************/

/***********************************************************************
 *
 *	Name:  compare_item_id
 *	ShortDesc:  Compare item ID's of two items in the Item Table.
 *
 *	Description:
 *		The compare_item_id function takes in two pointers to Item
 *		Table element structures in an Item Table structure and
 *		compares their item ID's.  The value returned is reflective
 *		of the comparison of the two item ID's.  This function is
 *		used in conjunction with the bsearch function (for searching
 *		the Item Table).
 *
 *	Inputs:
 *		item_1 - pointer to an Item Table element structure in an
 *				 Item Table structure.
 *		item_2 - pointer to another Item Table element structure in
 *				 an Item Table structure.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		LESS_THAN.
 *		GREATER_THAN.
 *		EQUAL.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
compare_item_id(
nsEDDEngine::ITEM_TBL_ELEM	*item_1,
nsEDDEngine::ITEM_TBL_ELEM	*item_2)
{
	/*
	 *	Compare the two item ID's and return the reflective value.
	 */

	if (item_1->item_id < item_2->item_id) {
		return(LESS_THAN);
	}
	else if (item_1->item_id > item_2->item_id) {
		return(GREATER_THAN);
	}
	else {
		return(EQUAL);
	}
}


/***********************************************************************
 *
 *	Name:  get_item_dd_ref
 *	ShortDesc:  Get the DD reference for an item.
 *
 *	Description:
 *		The get_item_dd_ref function takes a device handle, and
 *		uses the Connection Tables to obtain the corresponding Device
 *		Directory Tables.  It then searches the Item Table to find
 *		the particular item ID, and assigns its DD reference.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the item.
 *		item_id - ID of the item.
 *
 *	Outputs:
 *		dd_reference - DD reference of the item.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_TABLES_NOT_FOUND.
 *		FETCH_ITEM_NOT_FOUND.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_item_dd_ref(
ENV_INFO			*env_info,
DEVICE_HANDLE		 device_handle,
ITEM_ID				 item_id,
nsEDDEngine::DD_REFERENCE		*dd_reference)
{
	nsEDDEngine::FLAT_DEVICE_DIR		*flat_device_dir;
	nsEDDEngine::ITEM_TBL_ELEM		*item_tbl_elem;
	int					r_code ;

#ifdef DDSTEST
	TEST_FAIL(GET_ITEM_DD_REF);
#endif

	/*
	 *	Get the pointer to the Device Directory Tables.
	 */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	r_code = pConnectionMgr->get_adt_dd_dev_tbls(device_handle, 
			(void **)&flat_device_dir);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_dir == 0) {
		return(FETCH_TABLES_NOT_FOUND);
	}

	/*
	 *	Search for the particular item ID in the Item Table.
	 */

	item_tbl_elem = (nsEDDEngine::ITEM_TBL_ELEM *) bsearch(
		(char *)&item_id,
		(char *)flat_device_dir->item_tbl.list,
		(unsigned int)flat_device_dir->item_tbl.count,
		sizeof(nsEDDEngine::ITEM_TBL_ELEM),
		(CMP_FN_PTR)compare_item_id);

	if (item_tbl_elem == 0) {
		PS_TRACE(L"\n*** error (get_item_dd_ref) : bsearch could not find item in flat_device_dir->item_tbl.list itemID=0x%x",item_id);
		return(FETCH_ITEM_NOT_FOUND);
	}


	/*
	 *	Assign the DD reference of the item.
	 */

	*dd_reference = item_tbl_elem->dd_ref;

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_item
 *	ShortDesc:  Fetch attribute data for an item.
 *
 *	Description:
 *		The fch_item function takes a device handle and obtains
 *		the corresponding DD handle.  It then converts an item ID
 *		into a DD reference (object index or file offset).  It then
 *		uses either ROD fetch or flat fetch (depending upon the type
 *		of DD handle) to fetch the attribute data for the specified
 *		item.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the item.
 *		item_id - ID of the item.
 *		request_mask - mask that identifies the specific attributes
 *					   to be fetched.
 *		flat - item flat structure.
 *		item_type - type of item to be fetched.
 *
 *	Outputs:
 *		flat - item flat structure containing attribute data.
 *
 *	Returns: 
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from get_item_dd_ref function.
 *		Return values from fch_rod_item function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_item(
ENV_INFO		*env_info,
DEVICE_HANDLE	 device_handle,
ITEM_ID			 item_id,
::nsEDDEngine::AttributeNameSet	 request_mask,
::nsEDDEngine::ItemBase *flat,
ITEM_TYPE		 item_type)
{
	ROD_HANDLE			rod_handle = 0;
	nsEDDEngine::DD_REFERENCE		dd_reference;
	int					r_code = 0;

#ifdef DDSTEST
	TEST_FAIL(FETCH_ITEM);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Validate the device handle.
	 */

	if (!pConnectionMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Get the DD reference from the device handle and item ID.
	 */

	r_code = get_item_dd_ref(
		env_info,
		device_handle,
		item_id,
		&dd_reference);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnectionMgr->get_adt_dd_handle(device_handle, &rod_handle);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!pConnectionMgr->valid_dd_handle(rod_handle)) {
		r_code = pConnectionMgr->ds_dd_device_load( env_info,
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	/* 
	 *	Check the type of the DD handle and route the call
	 *	accordingly.
	 */

	r_code = fch_rod_item(
		env_info,
		rod_handle,
		dd_reference.object_index,
		request_mask,
		flat,
		item_type);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}

