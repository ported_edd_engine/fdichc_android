#include "TestSupport/EddEngineController.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestRangeSingleMinValue
        {
        public: 
            static EddEngineController^ engine;
            static FLAT_VAR* range_var;
            static FDI_GENERIC_ITEM* genericItem ;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER range_var_specifier;
                engine->FillParamSpecifier("range_var_one_min_range_only", &range_var_specifier);
                genericItem = engine->GetGenericItem(range_var_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                range_var = dynamic_cast<FLAT_VAR*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_1_min_value()
            {
                Assert::AreEqual((UInt16)1, range_var->min_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_0_max_values()
            {
                Assert::AreEqual((UInt16)0, range_var->max_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_min_value_should_be_5()
            {
                Assert::AreEqual((Int64)5, range_var->min_val.list[0].val.i);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestRangeSinglePairSet
        {
        public: 
            static EddEngineController^ engine;
            static FLAT_VAR* range_var;
            static FDI_GENERIC_ITEM* genericItem;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER range_var_specifier;
                engine->FillParamSpecifier("range_var_single_range", &range_var_specifier);
                genericItem = engine->GetGenericItem(range_var_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                range_var = dynamic_cast<FLAT_VAR*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_1_min_value()
            {
                Assert::AreEqual((UInt16)1, range_var->min_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_1_max_value()
            {
                Assert::AreEqual((UInt16)1, range_var->max_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_min_value_should_be_5()
            {
                Assert::AreEqual((Int64)5, range_var->min_val.list[0].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_max_value_should_be_10()
            {
                Assert::AreEqual((Int64)10, range_var->max_val.list[0].val.i);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestRangeTwoPairs 
        {
        public: 
            static EddEngineController^ engine;
            static FLAT_VAR* range_var;
            static FDI_GENERIC_ITEM* genericItem;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER range_var_specifier;
                engine->FillParamSpecifier("range_var_mult_range_paired", &range_var_specifier);
                genericItem = engine->GetGenericItem(range_var_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                range_var = dynamic_cast<FLAT_VAR*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_2_min_values()
            {
                Assert::AreEqual((UInt16)2, range_var->min_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_2_max_values()
            {
                Assert::AreEqual((UInt16)2, range_var->max_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_min_value_should_be_5()
            {
                Assert::AreEqual((Int64)5, range_var->min_val.list[0].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_max_value_should_be_10()
            {
                Assert::AreEqual((Int64)10, range_var->max_val.list[0].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_min_value_should_be_15()
            {
                Assert::AreEqual((Int64)15, range_var->min_val.list[1].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_max_value_should_be_20()
            {
                Assert::AreEqual((Int64)20, range_var->max_val.list[1].val.i);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestRangeWithOpenRange
        {
        public: 
            static EddEngineController^ engine;
            static FLAT_VAR* range_var;
            static FDI_GENERIC_ITEM* genericItem;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER range_var_specifier;
                engine->FillParamSpecifier("range_var_mult_range_open", &range_var_specifier);
                genericItem = engine->GetGenericItem(range_var_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                range_var = dynamic_cast<FLAT_VAR*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_3_min_values()
            {
                Assert::AreEqual((UInt16)3, range_var->min_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_2_max_values()
            {
                Assert::AreEqual((UInt16)2, range_var->max_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_min_value_should_be_uninitialized()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_NONE, (UInt64)range_var->min_val.list[0].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_max_value_should_be_5()
            {
                Assert::AreEqual((Int64)5, range_var->max_val.list[0].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_min_value_should_be_10()
            {
                Assert::AreEqual((Int64)10, range_var->min_val.list[1].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_max_value_should_be_15()
            {
                Assert::AreEqual((Int64)15, range_var->max_val.list[1].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_min_value_should_be_20()
            {
                Assert::AreEqual((Int64)20, range_var->min_val.list[2].val.i);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_max_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->max_val.list[0].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_min_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->min_val.list[1].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_max_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->max_val.list[1].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_min_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->min_val.list[2].eType);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestRangeWithOverlappingRanges
        {
        public: 
            static EddEngineController^ engine;
            static FLAT_VAR* range_var;
            static FDI_GENERIC_ITEM* genericItem;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER range_var_specifier;
                engine->FillParamSpecifier("range_var_mult_range_overlapping", &range_var_specifier);
                genericItem = engine->GetGenericItem(range_var_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                range_var = dynamic_cast<FLAT_VAR*>(genericItem->item);

            }
            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_3_min_values()
            {
                Assert::AreEqual((UInt16)3, range_var->min_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variable_should_have_3_max_values()
            {
                Assert::AreEqual((UInt16)3, range_var->max_val.count);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_min_value_should_be_uninitialized()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_NONE, (UInt64)range_var->min_val.list[0].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_first_max_value_should_be_uninitialized()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_NONE, (UInt64)range_var->max_val.list[0].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_min_value_should_be_uninitialized()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_NONE, (UInt64)range_var->min_val.list[1].eType);
            }

            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_max_value_should_be_20()
            {
                Assert::AreEqual((Int64)20, range_var->max_val.list[1].val.i);
            }
            [TestMethod, TestCategory("RangeTest")]
            void The_variables_second_max_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->max_val.list[1].eType);
            }
            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_min_value_should_be_15()
            {
                Assert::AreEqual((Int64)15, range_var->min_val.list[2].val.i);
            }
            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_min_type_should_be_integer5()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->min_val.list[2].eType);
            }
            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_max_value_should_be_25()
            {
                Assert::AreEqual((Int64)25, range_var->max_val.list[2].val.i);
            }
            [TestMethod, TestCategory("RangeTest")]
            void The_variables_third_max_type_should_be_integer()
            {
                Assert::AreEqual((UInt64)EXPR::EXPR_TYPE_INTEGER, (UInt64)range_var->max_val.list[2].eType);
            }
        };


        // WORKAROUND: use TestInitalize instead ClassInitalize
        // Impediment
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with items 7, but every test use only 1)

        [TestClass]
        public ref class When_requesting_the_variable_with_a_single_min_value_set_with_FM8 : BaseTestRangeSingleMinValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_a_single_min_value_set_with_FMA : BaseTestRangeSingleMinValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_a_single_range_pair_set_with_FM8 : BaseTestRangeSinglePairSet
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_a_single_range_pair_set_with_FMA : BaseTestRangeSinglePairSet
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_two_range_pairs_with_FM8 : BaseTestRangeTwoPairs
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_two_range_pairs_with_FMA : BaseTestRangeTwoPairs
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_open_ranges_with_FM8 : BaseTestRangeWithOpenRange
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_open_ranges_with_FMA : BaseTestRangeWithOpenRange
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_overlapping_ranges_with_FM8 : BaseTestRangeWithOverlappingRanges
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_the_variable_with_overlapping_ranges_with_FMA : BaseTestRangeWithOverlappingRanges
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0012.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
