/*** FDI DeviceModelServices EDD TestCase ***/
#include "Macros.h"
#include "Methods.h"

// Manufacturer 0xFD = "Special"
MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0025, DEVICE_REVISION 1, DD_REVISION 1

/****************************************************************************** */
/*    Common Tables and Univeral Commands (mandatory)                           */
/* **************************************************************************** */
/* */                                                                           
/* */  IMPORT STANDARD _TABLES, DEVICE_REVISION 6, DD_REVISION 1                
/* */  {                                                                        
/* */      EVERYTHING;                                                          
/* */  }                                                                        
/* */                                                                           
/* */  IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 5, DD_REVISION 1             
/* */  {                                                                        
/* */      EVERYTHING;
/* */  }                                                                        
/* */

/****************************************************************************** */
/* Dynamic variables (mandatory)                                                */
/* **************************************************************************** */
/* */  
/* */  
/* */  ARRAY OF VARIABLE dynamic_variables
/* */  {
/* */      ELEMENTS
/* */      {
/* */          PRIMARY, primary_value;
/* */      }
/* */  }
/* */
/* */  VARIABLE primary_value
/* */  {
/* */      LABEL "Primary value as float";
/* */      TYPE FLOAT;
/* */      CLASS DEVICE;
/* */  }
/* */ 
/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    STYLE MENU;
    ITEMS
    {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */

MENU main_test_menu
{
    LABEL "Main test menu";
    ITEMS
    {
        var_with_multiple_actions
    }
}

VARIABLE var_with_multiple_actions
{
    CLASS DEVICE;
    TYPE INTEGER(4);
    PRE_READ_ACTIONS
    {
        add_100,
        add_100
    }
    POST_READ_ACTIONS
    {
        remove_100,
        remove_100
    }
    PRE_WRITE_ACTIONS
    {
        add_1000,
        add_1000
    }
    
    POST_WRITE_ACTIONS
    {
        remove_1000,
        simple_scaling_method
    }
}

VARIABLE var_with_access_counter
{
    LABEL "var_with_access_counter";
    CLASS DEVICE;
    TYPE INTEGER(4);
    
    PRE_READ_ACTIONS
    {
        increase_callcount
    }
    
    POST_WRITE_ACTIONS
    {
        reset_callcount
    }
}


VARIABLE callcount
{
    LABEL "CallCounter";
    CLASS LOCAL;
    TYPE UNSIGNED_INTEGER(2);
    HANDLING READ & WRITE;
    DEFAULT_VALUE 0;
}

METHOD add_100
{
    DEFINITION
    {
        int i;

        i = igetval();
        i = i + 100;

        isetval(i);
    }
}

METHOD add_1000
{
    DEFINITION
    {
        int i;

        i = igetval();
        i = i + 1000;

        isetval(i);
    }
}

METHOD remove_100
{
    DEFINITION
    {
        int i;

        i = igetval();
        i = i - 100;

        isetval(i);
    }
}

METHOD remove_1000
{
    DEFINITION
    {
        int i;

        i = igetval();
        i = i - 1000;

        isetval(i);
    }
}


METHOD simple_scaling_method
{
    DEFINITION
    {
        int i;
        i = igetval();
        i = i * 50;
    
        isetval(i);
    }
}

METHOD increase_callcount
{
    DEFINITION
    {
        // Save values is not allowed but it is also not required in case of scaling functions
        int cc;
        cc = callcount;
        cc = cc + 1;
        callcount = cc;
    }
}

METHOD reset_callcount
{
    DEFINITION
    {
        // Save values is not allowed but it is also not required in case of scaling functions
        callcount = 0;
    }
}

METHOD divide_by_zero
{
    DEFINITION
    {
        int i;
        i = igetval();
        i = i / 0;
        isetval(i);
    }
}

COMMAND read_test_int
{
    NUMBER 202;
    OPERATION READ;
    TRANSACTION
    {
        REQUEST
        {
            var_with_multiple_actions (INFO)
        }
        REPLY
        {
            response_code,
            device_status,
            var_with_multiple_actions
        }
    }
    RESPONSE_CODES
    {
        0,  SUCCESS,    [no_command_specific_errors];
    }
}

COMMAND write_test_int
{
    NUMBER 203;
    OPERATION WRITE;
    TRANSACTION
    {
        REQUEST 
        {
            var_with_multiple_actions
        }
        REPLY
        {
            response_code,
            device_status,
            var_with_multiple_actions
        }
    }
    RESPONSE_CODES
    {
        0,  SUCCESS,    [no_command_specific_errors];
    }
}

COMMAND read_var_with_access_counter
{
    NUMBER 205;
    OPERATION READ;
    TRANSACTION
    {
        REQUEST{}
        REPLY
        {
            response_code,
            device_status,
            var_with_access_counter
        }
    }
    RESPONSE_CODES
    {
        0, SUCCESS,  [no_command_specific_errors];
    }
}

COMMAND write_var_with_access_counter
{
    NUMBER 206;
    OPERATION WRITE;
    TRANSACTION
    {
        REQUEST
        {
            var_with_access_counter
        }
        REPLY
        {
            response_code,
            device_status,
            var_with_access_counter
        }
    }
    RESPONSE_CODES
    {
        0,  SUCCESS,  [no_command_specific_errors];
    }
}
