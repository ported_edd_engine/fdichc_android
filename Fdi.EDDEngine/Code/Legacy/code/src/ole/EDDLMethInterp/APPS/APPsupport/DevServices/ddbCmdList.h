/*************************************************************************************************
 *
 * ddbCmdList.h
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		generated from from a portion of previous version of ddbItemLists.h
 *
 * #include "ddbCmdList.h"
 */

#ifndef _DDBCMDLIST_H
#define _DDBCMDLIST_H
#ifdef INC_DEBUG
#pragma message("In ddbCmdList.h") 
#endif

#include "ddbCommand.h"
#include "ddbItemListBase.h"


#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbCmdList.h") 
#endif



class CCmdList : public CitemListBase<iT_Command, hCcommand>
{
public:
	CCmdList(DevInfcHandle_t h) : CitemListBase<iT_Command, hCcommand>(h){};

	hCcommand* getCmdByNumber(int cmdNumber);
};

#endif	//_DDBCMDLIST_H
/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
