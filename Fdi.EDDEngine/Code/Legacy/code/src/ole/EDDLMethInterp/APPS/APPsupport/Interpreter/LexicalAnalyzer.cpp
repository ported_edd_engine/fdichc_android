
#pragma warning (disable : 4786)
#include "stdafx.h"
#include "LexicalAnalyzer.h"
#include "Variable.h"
//#include "RIDEError.h"
#include <ctype.h>

#include "ErrorDefinitions.h"
#include "SynchronisationSet.h"

#include "MEE.h"
#include "BuiltInMapper.h"//WHS:Change done for StingrayProject

CLexicalAnalyzer::CLexicalAnalyzer()
{
    m_i32CurrentPos		=	0;
    m_i32PrevPos		=	0;
    m_i32LAPosition		=	0;
    m_i32LineNo			=	1;
    m_pvecErr			=	0;
    m_i32CurLineNo		=	1;
    m_i32PrevLineNo		=	1;
    m_pMEE				=	NULL;
    m_szRuleName[0]		=	'\0';

    m_i32CapturedCurrentPos = 0;
    m_i32CapturedPrevPos = 0;
    m_i32CapturedLAPosition = 0;
    m_i32CapturedLineNo = 0;
    m_i32CapturedCurLineNo = 0;
    m_i32CapturedPrevLineNo = 0;

    m_nIndentIndex=0;
    //m_sDeclarationStack has its own constructor
}

CLexicalAnalyzer::~CLexicalAnalyzer()
{
    m_pvecErr			=	0;
}

_INT32 CLexicalAnalyzer::nextToken(
            CToken**	ppToken,
            _INT32&		i32NewPos,
            _CHAR*		pszBuffer)
{
    const _CHAR*		pszSource = (const _CHAR*)m_pszSource;
    RUL_TOKEN_TYPE		Type;
    RUL_TOKEN_SUBTYPE	SubType;
    bool				bIsFound = false;
    bool				bIsObject_or_Service = false;
    _INT32				i=0;
    COMPOUND_DATA		CmpData; memset(&CmpData,0,sizeof(COMPOUND_DATA));

    try
    {
        for(i = m_i32CurrentPos; (pszSource[i]) && !bIsFound; )
        {
    //munch all prepended white space of a token...
            if(isSpace(pszSource,i,Type,SubType,pszBuffer))
            {
                ;
            }
            else if(isTerminal(pszSource,i,Type,SubType,pszBuffer))
            {
                if(Type == RUL_COMMENT)
                {
                    while(pszSource[i] && pszSource[i++] != '\n')
                        ;
                    continue;
                }

                char pchBuffer1[100];
                strcpy(pchBuffer1, pszBuffer);

                if ((Type == RUL_SYMBOL) && (SubType == RUL_DOT))
                {
                    if(isNumber(pszSource,i,Type,SubType,pszBuffer))
                    {
                        //Get the numeric constant...
                        strcat (pchBuffer1, pszBuffer);
                        strcpy(pszBuffer,pchBuffer1);
                        bIsFound = true;
                        break;
                    }
                }

                bIsFound = true;
    //The next operator is Unary + or - operator if the current Token is
    //a.	Operator
    //b.	Symbol
    //c.	None
                if( (m_CurToken.GetType() == RUL_TYPE_NONE)
                    || (m_CurToken.IsOperator())
                    || (m_CurToken.IsSymbol() && m_CurToken.GetSubType() != RUL_RPAREN
                             && m_CurToken.GetSubType() != RUL_RBOX)
                    || (m_CurToken.GetType() == RUL_KEYWORD)
                    )
                {
                    switch(SubType)
                    {
                    case RUL_PLUS:
                        SubType = RUL_UPLUS;
                        bIsFound = true;
                        break;
                    case RUL_MINUS:
                        SubType = RUL_UMINUS;
                        bIsFound = true;
                        break;
                    case RUL_PLUS_PLUS:
                        SubType = RUL_PRE_PLUS_PLUS;
                        bIsFound = true;
                        break;
                    case RUL_MINUS_MINUS:
                        SubType = RUL_PRE_MINUS_MINUS;
                        bIsFound = true;
                        break;
                    case RUL_BIT_AND:		//is & reference following ',' or '(' or ';'
                        if( m_CurToken.IsSymbol() &&
                            ((m_CurToken.GetSubType() == RUL_COMMA) || (m_CurToken.GetSubType() == RUL_LPAREN)
                             || (m_CurToken.GetSubType() == RUL_SEMICOLON)) )
                        {
                            //'&' represents the address_of operator inside of argument list
                            bIsFound = false;	//set to false so that '&' is ignored
                        }
                        break;
                    }
                }
            }
            else if(isNumber(pszSource,i,Type,SubType,pszBuffer))
            {
                //Get the numeric constant...
                bIsFound = true;
            }
            else if(isString(pszSource,i,Type,SubType,pszBuffer))
            {
                //Get the string constant...
                bIsFound = true;
            }
            else if(isChar(pszSource,i,Type,SubType,pszBuffer))
            {
                //Get the char constant...
                bIsFound = true;
            }
            else if(isService(pszSource,i,Type,SubType,pszBuffer,CmpData))
            {
                bIsFound = true;
                bIsObject_or_Service = true;
            }
            else if(isIdentifier(pszSource,i,Type,SubType,pszBuffer))
            {
                if(pszSource[i] == LBOX)
                {
                    Type = RUL_ARRAY_VARIABLE;
                }
                else
                {
                    int iSavedPos = i;
                    while (pszSource[iSavedPos] == ' ')
                    {
                        iSavedPos++;
                    }
                    if(pszSource[iSavedPos] == LBOX)
                    {
                        Type = RUL_ARRAY_VARIABLE;
                        i = iSavedPos;
                    }
                }
                bIsFound = true;
            }
            else
            {
                i++;
                bIsFound = true;
                Type = RUL_TYPE_ERROR;
                SubType = RUL_SUBTYPE_ERROR;

                throw(C_LEX_ERROR_ILLEGALCHAR);
            }
        }
        if(bIsFound)
        {
            i32NewPos = i;
            if(bIsObject_or_Service)
                return Tokenize(i,Type,SubType,ppToken,pszBuffer,CmpData);
            return Tokenize(i,Type,SubType,ppToken,pszBuffer);
        }
    }
    catch(_INT32 error)
    {
        i32NewPos = i;
        *ppToken = 0;
        throw(error);
    }

    return LEX_FAIL;
}

_INT32 CLexicalAnalyzer::nextAnyToken(
            CToken**	ppToken,
            _INT32&		i32NewPos,
            _CHAR*		pszBuffer)
{
    const _CHAR*		pszSource = (const _CHAR*)m_pszSource;
    RUL_TOKEN_TYPE		Type;
    RUL_TOKEN_SUBTYPE	SubType;
    bool				bIsFound = false;
    bool				bIsObject_or_Service = false;
    _INT32				i=0;
    COMPOUND_DATA		CmpData; memset(&CmpData,0,sizeof(COMPOUND_DATA));

    try
    {
        for(i = m_i32CurrentPos; (pszSource[i]) && !bIsFound; )
        {
    //munch all prepended white space of a token...
            if(isSpace(pszSource,i,Type,SubType,pszBuffer))
            {
                ;
            }
            else if(isTerminal(pszSource,i,Type,SubType,pszBuffer))
            {
                bIsFound = true;
            }
            else
            {
                i++;
                bIsFound = true;
                Type = RUL_TYPE_ERROR;
                SubType = RUL_SUBTYPE_ERROR;
            }
        }
        if(bIsFound)
        {
            i32NewPos = i;
            if(bIsObject_or_Service)
                return Tokenize(i,Type,SubType,ppToken,pszBuffer,CmpData);
            return Tokenize(i,Type,SubType,ppToken,pszBuffer);
        }
    }
    catch(_INT32 error)
    {
        i32NewPos = i;
        *ppToken = 0;
        throw(error);
    }

    return LEX_FAIL;
}

_INT32 CLexicalAnalyzer::LookAheadToken(
            CToken**		ppToken)
{
    try
    {
        _INT32	i32NewPos=0;
        _CHAR	szBuffer[BUFFER_SIZE];

        _INT32 i32Ret = nextToken(ppToken, i32NewPos, szBuffer);
        m_i32LineNo = m_i32CurLineNo;

        return i32Ret;
    }
    catch(_INT32 error)
    {
        throw(error);
    }

    return LEX_FAIL;
}

_BOOL CLexicalAnalyzer::ScanLineForToken(
                                RUL_TOKEN_TYPE tokenType
                                , RUL_TOKEN_SUBTYPE tokenSubType
                                , CToken** ppToken
                                )
{
    _INT32	i32NewPos=0, i32CurrentPos;
    _INT32	iPreviousCurrentPos = m_i32CurrentPos;

    _CHAR	pszRulString[BUFFER_SIZE];

    /* Get the rule string */
    if (!GetRulString(tokenType, tokenSubType, pszRulString))
    {
        return false;
    }

    i32CurrentPos = i32NewPos;
    char *pSubStringStartAddress =                      // WS - 9apr07 - VS2005 checkin
                (char*)strstr((const char*)&m_pszSource[m_i32CurrentPos], pszRulString);

    /* Check if it is a == operator */
    if ((tokenSubType == RUL_ASSIGN) && (pSubStringStartAddress != NULL))
    {
        if ((*pSubStringStartAddress == '=') && (*(pSubStringStartAddress + 1) == '='))
        {
            pSubStringStartAddress = NULL;	//"=="
        }
        else
        if ((*pSubStringStartAddress == '=') && (*(pSubStringStartAddress - 1) == '>') && (*(pSubStringStartAddress - 2) != '>'))
        {
            pSubStringStartAddress = NULL;	//">="
        }
        else
        if ((*pSubStringStartAddress == '=') && (*(pSubStringStartAddress - 1) == '<') && (*(pSubStringStartAddress - 2) != '<'))
        {
            pSubStringStartAddress = NULL;	//"<="
        }
        else
        if ((*pSubStringStartAddress == '=') && (*(pSubStringStartAddress - 1) == '!'))
        {
            pSubStringStartAddress = NULL;	//"!="
        }
    }

    if (pSubStringStartAddress == NULL)
    {
        return false;
    }


    char *pSubSemiColonStartAddress = (char*)strstr((const char*)&m_pszSource[m_i32CurrentPos], ";");

    char *pSubEOSStartAddress = NULL;

    char *pSubEOSStartAddressSearchPoint = (char*)&m_pszSource[m_i32CurrentPos];
    char *pSubEOSStartAddressOpenBracketSearchPoint = (char*)&m_pszSource[m_i32CurrentPos];
    while (1)
    {
        pSubEOSStartAddress =    // WS - 9apr07 - VS2005 checkin
            (char*)strstr(pSubEOSStartAddressSearchPoint, ")");
        if ((pSubEOSStartAddress) && (pSubEOSStartAddress < pSubSemiColonStartAddress))
        {
            char *pSubEOSStartAddressOpenBracket =  // WS - 9apr07 - VS2005 checkin
                (char*)strstr(pSubEOSStartAddressOpenBracketSearchPoint, "(");
            if ((pSubEOSStartAddressOpenBracket != NULL) && (pSubEOSStartAddressOpenBracket < pSubSemiColonStartAddress))
            {
                if (pSubEOSStartAddressOpenBracket < pSubEOSStartAddress)
                {
                    pSubEOSStartAddressSearchPoint = pSubEOSStartAddress + 1;
                    pSubEOSStartAddressOpenBracketSearchPoint = pSubEOSStartAddressOpenBracket + 1;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    if (pSubSemiColonStartAddress != NULL)
    {
        if (pSubEOSStartAddress != NULL)
        {
            if (pSubSemiColonStartAddress < pSubEOSStartAddress)
            {
                pSubEOSStartAddress = pSubSemiColonStartAddress;
            }
        }
        else
        {
            pSubEOSStartAddress = pSubSemiColonStartAddress;
        }
    }

    if (pSubEOSStartAddress != NULL)
    {
        if (pSubStringStartAddress > pSubEOSStartAddress)
        {
            return false;
        }
    }

    char *pSubDoubleQuoteStartAddress = NULL;
    pSubDoubleQuoteStartAddress =               // WS - 9apr07 - VS2005 checkin
                (char*)strstr((const char*)&m_pszSource[m_i32CurrentPos], "\"");
    if ( (pSubDoubleQuoteStartAddress != NULL)
        && (pSubDoubleQuoteStartAddress < pSubStringStartAddress)
        )
    {
        char *pSubDoubleQuoteEndAddress = NULL;
        pSubDoubleQuoteEndAddress =               // WS - 9apr07 - VS2005 checkin
                (char*)strstr((const char*)pSubDoubleQuoteStartAddress + 1, "\"");
        if (pSubDoubleQuoteEndAddress > pSubStringStartAddress)
        {
            /* The symbol is within a string */
            return false;
        }
    }

    if (pSubStringStartAddress != NULL)
    {
        int iReturnValue = TokenizeWithoutSave
        (
            0
            , tokenType
            , tokenSubType
            , ppToken
            , (char *)pszRulString
        );

        if (iReturnValue)
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

_INT32 CLexicalAnalyzer::GetNextToken(
            CToken**		ppToken,
            CSymbolTable*	pSymbolTable)
{
    long	i32NewPos=0;
    try
    {
        _CHAR	szBuffer[BUFFER_SIZE];
        if(LEX_FAIL != nextToken(ppToken, i32NewPos, szBuffer))
        {
            SaveState(i32NewPos);

#ifdef _FULL_RULE_ENGINE
        //Before u exit, make an entry in the Symbol Table
            if((*ppToken)->IsVariable()
                || (*ppToken)->IsArrayVar() && (*ppToken)->GetSubType() != RUL_DICT_STRING)
            {
/*Vibhor 010705: Start of Code*/
/*Following change will prevent an undeclared variable to be used in a method
& consequently the Interpreter will throw a parsing error.

If we come here with a Variable token, and the same is not available in symbol Table
we possibly have a Global (DD) Variable. In that case, search it in the device and add
it to the Global symbol table. If its not found there too send a LEX_FAIL to the caller.

*/
                _INT32 i32Idx = pSymbolTable->GetIndexInScope((*ppToken)->GetLexeme());
                if(-1 != i32Idx)
                {
                    //get the token in local variable table
                    CVariable* pTheVariable = pSymbolTable->GetAt(i32Idx);

                    //fill in token members
                    (*ppToken)->SetSymbolTableIndex(i32Idx);
                    (*ppToken)->SetSymbolTableScopeIndex(pTheVariable->GetSymbolTableScopeIndex());
                    (*ppToken)->SetSubType(pTheVariable->GetSubType());
                    (*ppToken)->m_bIsRoutineToken = pTheVariable->m_bIsRoutineToken;
                    /* Even though a variable is declared as an local array in DD and saved as an array in local symbol table,
                     * the variable type may be RUL_SIMPLE_VARIABLE instead of RUL_ARRAY_VARIABLE
                     * which is decided in function nextToken(), if the reference is array name without square bracket.
                     */

                    //get token operative ID
                    PARAM_REF* pOpRef = pTheVariable->GetParamRef();
                    if (pOpRef && pOpRef->isOperationalIDValid() && pTheVariable->m_bIsRoutineToken)
                    {
                        //this must be a method parameter and a DD_ITEM
                        (*ppToken)->SetType(RUL_DD_ITEM);
                        (*ppToken)->SetParamRef(pTheVariable->GetParamRef());
                        (*ppToken)->SetDDItemName(pTheVariable->GetLexeme());				//DD item name without extended name

                        //find extended complex DD item name
                        char* szDotExpression = NULL;
                        bool bExtName = GetComplexDotExp(i32NewPos, &szDotExpression);

                        if ( (bExtName && szDotExpression)
                             && !strchr(szDotExpression, '[') && !strchr(szDotExpression, '(') )
                        {
                            //form a full token name
                            size_t iExpLength = strlen((*ppToken)->GetLexeme()) + 1;
                            iExpLength += strlen(szDotExpression);
                            char* szFullLexeme = new char[iExpLength];
                            PS_Strcpy(szFullLexeme, iExpLength, (*ppToken)->GetLexeme());
                            strcat(szFullLexeme, szDotExpression);

                            (*ppToken)->SetLexeme((const char*)szFullLexeme);			//update DD item name with extended name
                            DELETE_ARR (szFullLexeme);

                            i32Idx = pSymbolTable->InsertInScope((**ppToken), false);	//symbolTable::Insert makes a copy of this token.
                            if (i32Idx >= 0)
                            {
                                (*ppToken)->SetSymbolTableIndex(i32Idx);
                                (*ppToken)->SetSymbolTableScopeIndex(pSymbolTable->m_sScope.top());

                                //UnGetToken();							//undo m_CurToken, m_i32CurrentPos, etc
                                i32NewPos += (long)strlen(szDotExpression);
                                SaveState(i32NewPos);					//update m_i32CurrentPos etc

                                //creat a new token with full item name
                                CToken* pTokenTemp = 0;					//update m_CurToken etc
                                Tokenize(i32NewPos, RUL_DD_ITEM, RUL_DD_COMPLEX, &pTokenTemp, (char*)(*ppToken)->GetLexeme());
                                DELETE_PTR(pTokenTemp);					//clean up memory
                            }
                        }

                        DELETE_ARR (szDotExpression);
                    }
                }
                //The token maybe a global token
                else //if(-1 == i32Idx)
                {
                    char dictBuffer[BUFFER_SIZE] = {0};

                    //find extended complex DD item name
                    char* szDotExpression = NULL;
                    bool bExtName = GetComplexDotExp(i32NewPos, &szDotExpression);

                    //form a full token name
                    size_t iExpLength = strlen((*ppToken)->GetLexeme()) + 1;
                    if (bExtName && szDotExpression)
                    {
                        iExpLength += strlen(szDotExpression);
                    }
                    char* szFullLexeme = new char[iExpLength];
                    PS_Strcpy(szFullLexeme, iExpLength, (*ppToken)->GetLexeme());
                    if (bExtName && szDotExpression)
                    {
                        strcat(szFullLexeme, szDotExpression);
                    }

                    //Anil August 26 2005 For handling DD variable and Expression
                    //Anil Octobet 5 2005 for handling Method Calling Method
                    DD_ITEM_TYPE DDitemType;
                    i32Idx = m_pMEE->FindGlobalToken((*ppToken)->GetLexeme(), szFullLexeme, GetLineNumber(), DDitemType);
                    if(-1 != i32Idx)
                    {
                        //found a global token
                        (*ppToken)->m_bIsGlobal = true;
                        (*ppToken)->SetSymbolTableIndex(i32Idx);
                        (*ppToken)->SetType   (m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->GetType());
                        (*ppToken)->SetSubType(m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->GetSubType());
                        (*ppToken)->SetEnumToken(m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->IsEnumDDItem());
                        (*ppToken)->SetParamRef(m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->GetParamRef());

                        // stevev 16mar09 - method calling item other than method....  eg    menuItem();// makes a mess
                        if (m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->GetSubType() != RUL_DD_METHOD &&
                            m_pszSource[i32NewPos] == '(' &&
                            !m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->IsEnumDDItem()
                           )
                        {
                            (*ppToken)->SetType   (RUL_TYPE_ERROR);
                            (*ppToken)->SetSubType(RUL_SUBTYPE_ERROR);
                            LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"ERROR: calling a non-method item.\n");

							DELETE_ARR(szDotExpression);
							DELETE_ARR(szFullLexeme);
							throw(C_LEX_ERROR_ILLEGALITEM);
						}
						else
						{
							(*ppToken)->SetDDItemName((*ppToken)->GetLexeme());		//DD item name without extended name

                            bool bResetGlobalSymbolTable = true;
                            if(szDotExpression)
                            {
                                (*ppToken)->SetLexeme((const char*)szFullLexeme);	//update DD item name with extended name

                                i32NewPos += (long)strlen(szDotExpression);

                                if ( DDitemType == DD_ITEM_VAR )
                                {
                                    bResetGlobalSymbolTable = false; // do reset the global symbols table for this
                                }
                            }

                            if(((*ppToken)->GetDDItemName() != NULL) && bResetGlobalSymbolTable && (m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->GetDDItemName() == NULL))
                            {
                                m_pMEE->m_GlobalSymTable.GetAt(i32Idx)->SetDDItemName((*ppToken)->GetDDItemName());
                            }

                            UnGetToken();							//undo m_CurToken, m_i32CurrentPos, etc
                            SaveState(i32NewPos);					//update m_i32CurrentPos etc
                            CToken* pTokenTemp=0;
                            //Anil Octobet 5 2005 for handling Method Calling Method
                            //Changed the Condition for DD var and DD method
                            if(DDitemType == DD_ITEM_VAR)
                            {
                                Tokenize(i32NewPos,RUL_DD_ITEM,RUL_DD_SIMPLE,&pTokenTemp,(char*)(*ppToken)->GetLexeme());
                            }
                            //Anil Octobet 5 2005 for handling Method Calling Method
                            else if(DDitemType == DD_ITEM_NONVAR)
                            {
                                Tokenize(i32NewPos,RUL_DD_ITEM,RUL_DD_COMPLEX,&pTokenTemp,(char*)(*ppToken)->GetLexeme());
                            }
                            else if(DDitemType == DD_ITEM_METHOD)
                            {
                                Tokenize(i32NewPos,RUL_DD_ITEM,RUL_DD_METHOD,&pTokenTemp,(char*)(*ppToken)->GetLexeme());
                            }
                            DELETE_PTR(pTokenTemp);//clean up memory
                        }
                    }
                    else if(m_pMEE && m_pMEE->GetDictionaryString((char*)(*ppToken)->GetLexeme(), dictBuffer, BUFFER_SIZE) == SUCCESS)
                    {
                        //found a dictionary string token
                        (*ppToken)->SetSubType(RUL_DICT_STRING);
                    }
                    else
                    {
                        i32Idx = -1;

                        //check if token name matches VARIABLE_STATUS_* key word if PROFIBUS is used.
                        if (m_pMEE->GetStartedProtocol() == nsEDDEngine::PROFIBUS)
                        {
                            string sName = (*ppToken)->GetLexeme();
                            VARIABLE_STATUS_MAP::iterator varStatusIter = (m_pMEE->m_MapVarStatusNameToValue).find(sName);
                            if(varStatusIter != (m_pMEE->m_MapVarStatusNameToValue).end())
                            {
                                i32Idx = 0;

                                //found a token with VARIABLE_STATUS_* key word. Replace the key word with a value string
                                long lVariableStatusVal = varStatusIter->second;
                                char sVarStatusBuffer[BUFFER_SIZE] = {0};
                                itoa(lVariableStatusVal, sVarStatusBuffer, 10);

                                //change or fill in token members
                                (*ppToken)->SetLexeme(sVarStatusBuffer);
                                (*ppToken)->SetType(RUL_NUMERIC_CONSTANT);
                                (*ppToken)->SetSubType(RUL_UNSIGNED_INTEGER_DECL);
                            }
                        }

                        if (i32Idx == -1)
                        {
                            LOGIT(CERR_LOG|CLOG_LOG|UI_LOG,"Method Parsing Error: Identifier '%s' "
                            " was not found.  (%s:Line %d)\n",
                            (*ppToken)->GetLexeme(),m_pMEE->methodNameString.c_str(),GetLineNumber());
							DELETE_ARR(szDotExpression);
							DELETE_ARR(szFullLexeme);
                            return LEX_FAIL;
                        }
                    }

	                DELETE_ARR (szDotExpression);
					DELETE_ARR(szFullLexeme);
				}
/*Vibhor 010705: End of Code*/
            }
            else if(RUL_STRING_CONSTANT == (*ppToken)->GetSubType())
            {
                _INT32 i32Idx = pSymbolTable->InsertConstant(**ppToken);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetConstantIndex(i32Idx);
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert string constant into symbol table\n");
                }
            }
            else if(RUL_CHAR_CONSTANT == (*ppToken)->GetSubType())
            {
                _INT32 i32Idx = pSymbolTable->InsertConstant(**ppToken);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetConstantIndex(i32Idx);
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert char constant into symbol table\n");
                }
            }
#endif
            return LEX_SUCCESS;
        }
    }
    catch(_INT32 error)
    {
        SaveState(i32NewPos);
        throw(error);
    }
    return LEX_FAIL;
}

_INT32 CLexicalAnalyzer::GetNextVarToken(
            CToken**		ppToken,
            CSymbolTable*	pSymbolTable,
            RUL_TOKEN_SUBTYPE SubType)
{
    _INT32	i32NewPos=0;
    try
    {
        _CHAR	szBuffer[BUFFER_SIZE];
        if(LEX_FAIL != nextToken(ppToken, i32NewPos, szBuffer))
        {
            SaveState(i32NewPos);

#ifdef _FULL_RULE_ENGINE
            //m_nIndentIndex indicates the level of compound statements starting from 1
            //updating before this function is executed.
            //m_sDeclarationStack is a stack of m_nIndentIndex values
            //updating after this function is executed.
            //pSymbolTable->m_sScope is a stack of variable scope indexes starting from 1
            //updating after this function is executed.
            //
            //for variable _bi_rc, the scope index will be set to 1.
            //At this time, both m_sDeclarationStack and pSymbolTable->m_sScope are empty;
            //m_nIndentIndex is 1.
            //for the rest of really declared variables, the scope indexes will be set
            //to a number starting from 2.
            //At first time, m_sDeclarationStack is empty;
            //pSymbolTable->m_sScope top is 1 and m_nIndentIndex is equal to or greater than 1.
            //
            //Before u exit, make an entry in the Symbol Table
            bool bCreateNewScope = false;
            if ( ((m_sDeclarationStack.size() == 0) && (m_nIndentIndex > 0))
                 || ((m_sDeclarationStack.size() > 0) && (m_nIndentIndex > m_sDeclarationStack.top())) )
            {
                bCreateNewScope = true;
            }

            if((*ppToken)->IsVariable())
            {
                (*ppToken)->SetSubType(SubType);
                _INT32 i32Idx = pSymbolTable->InsertInScope(**ppToken, bCreateNewScope);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetSymbolTableIndex(i32Idx);
                    (*ppToken)->SetSymbolTableScopeIndex(pSymbolTable->m_sScope.top());
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert variable into symbol table\n");
                }
            }
            else
            if((*ppToken)->IsArrayVar())
            {
                (*ppToken)->SetSubType(RUL_ARRAY_DECL);
                _INT32 i32Idx = pSymbolTable->InsertInScope(**ppToken, bCreateNewScope);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetSymbolTableIndex(i32Idx);
                    (*ppToken)->SetSymbolTableScopeIndex(pSymbolTable->m_sScope.top());
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert array into symbol table\n");
                }
            }
            else if(RUL_STRING_CONSTANT == (*ppToken)->GetSubType())
            {
                _INT32 i32Idx = pSymbolTable->InsertConstant(**ppToken);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetConstantIndex(i32Idx);
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert string constant into symbol table\n");
                }
            }
            else if(RUL_CHAR_CONSTANT == (*ppToken)->GetSubType())
            {
                _INT32 i32Idx = pSymbolTable->InsertConstant(**ppToken);
                if( i32Idx >= 0 )
                {
                    (*ppToken)->SetConstantIndex(i32Idx);
                }
                else
                {
                    LOGIT(CERR_LOG,"Unable to insert character constant into symbol table\n");
                }
            }
#endif
            return LEX_SUCCESS;
        }
    }
    catch(_INT32 error)
    {
        SaveState(i32NewPos);
        throw(error);
    }
    return LEX_FAIL;
}

_INT32 CLexicalAnalyzer::UnGetToken()
{
    m_CurToken		= m_PrevToken;
    m_i32CurrentPos = m_i32PrevPos;

    m_i32LineNo	= m_i32PrevLineNo;
    m_i32CurLineNo	= m_i32PrevLineNo;
    return LEX_SUCCESS;
}

_INT32 CLexicalAnalyzer::Load(
        _UCHAR*		pszRule,
        _UCHAR*		pszRuleName,
        ERROR_VEC*	pvecErrors)
{
    m_pszSource = pszRule;
    m_pvecErr = pvecErrors;
    //strcpy((_CHAR*)m_szRuleName,(const _CHAR*)pszRuleName);

    return LEX_SUCCESS;
}

_INT32  CLexicalAnalyzer::SaveState(
            _INT32 i32CurState)
{
    m_i32PrevPos	= m_i32CurrentPos;
    m_i32CurrentPos = i32CurState;
    m_i32PrevLineNo = m_i32CurLineNo;
    m_i32CurLineNo = m_i32LineNo;
    return LEX_SUCCESS;
}


bool CLexicalAnalyzer::isSpace(
            const _CHAR*		pszSource,
            _INT32&				i,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    if(pszSource[i] == ' ' || pszSource[i] == '\t' || pszSource[i] == '\r')
    {
        //munch all prepended white space of a token...
        i++;
    }
    else if(pszSource[i] == '\n')
    {
        m_i32LineNo++;
        i++;
    }
    else
    {
        return false;
    }
    return true;
}

//match for the regular expression
//	digit	-->		[0-9]
//	Number	-->		digit+ (.digit+)
bool	CLexicalAnalyzer::isNumber(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    memset(pszBuffer,0,BUFFER_SIZE);
    if(isdigit(pszSource[i32CurPos]) || (pszSource[i32CurPos] == '.'))
    {
        _INT32		i=0;
        bool	bIsFloat = false;
        bool	bIsHex = false;
        for( ;pszSource[i32CurPos+i]; i++ )
        {
            if (i==0)
            {
                if(isdigit(pszSource[i32CurPos+i]))
                {
                    pszBuffer[i] = pszSource[i32CurPos+i];
                    pszBuffer[i + 1] = 0;
                    if ((atoi(pszBuffer) ==  0) &&
                        ((pszSource[i32CurPos+i+1] == 'x') || (pszSource[i32CurPos+i+1] == 'X')))
                    {
                        pszBuffer[i + 1] = pszSource[i32CurPos+i+1];
                        bIsHex = true;
                        i++;
                    }
                    continue;
                }
            }
            if(isdigit(pszSource[i32CurPos+i]))
            {
                pszBuffer[i] = pszSource[i32CurPos+i];
                continue;
            }
            else if(('.' == pszSource[i32CurPos+i]) && !bIsFloat)
            {
                pszBuffer[i] = pszSource[i32CurPos+i];
                bIsFloat = true;
                continue;
            }
            else
            if ( bIsFloat &&
                 ( ('e' == pszSource[i32CurPos+i]) ||
                   ('E' == pszSource[i32CurPos+i])
                 )  )
            {
                if ( ('+' == pszSource[i32CurPos+i+1]) || ('-' == pszSource[i32CurPos+i+1]) ||
                     isdigit(pszSource[i32CurPos+i+1]))//WHS EP June17-2008 support implicit + sign for scientific notation
                {
                    pszBuffer[i] = pszSource[i32CurPos+i];
                    pszBuffer[i+1] = pszSource[i32CurPos+i+1];
                    i++;
                    continue;
                }
            }
            else if(bIsHex)
            {
                bool bUnknownSymbolFound = false;
                switch(pszSource[i32CurPos+i])
                {
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    case 'F':
                        {
                            pszBuffer[i] = pszSource[i32CurPos+i];
                            continue;
                        }
                    default:
                        {
                            bUnknownSymbolFound = true;
                            break;
                        }
                }
                if (bUnknownSymbolFound)
                    break;
            }
            else
            {
                if (!bIsHex && !bIsFloat)
                {
                    if(
                    ('e' == pszSource[i32CurPos+i])
                    || ('E' == pszSource[i32CurPos+i])
                    )
                    {
                        bIsFloat = true;
                        pszBuffer[i] = pszSource[i32CurPos+i];
                        if ( ('+' == pszSource[i32CurPos+i+1]) || ('-' == pszSource[i32CurPos+i+1]) ||
                             isdigit(pszSource[i32CurPos+i+1]))//WHS EP June17-2008 support implicit + sign for scientific notation
                        {
                            pszBuffer[i+1] = pszSource[i32CurPos+i+1];
                            i++;
                        }
                        continue;
                    }

                }
                break;
            }
        }
        if(i >= 0)	//if found a number
        {
            i32CurPos	+=	i;
            Type		=	RUL_NUMERIC_CONSTANT;
            SubType		=	bIsFloat? RUL_REAL_CONSTANT:RUL_INT_CONSTANT;
            return true;
        }

    }
    return false;
}

bool CLexicalAnalyzer::isString(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    memset(pszBuffer,0,BUFFER_SIZE);
    if('"' == pszSource[i32CurPos])
    {
        _INT32		i=1;
        for(;'"' != pszSource[i32CurPos+i];i++)
        {
            pszBuffer[i-1] = pszSource[i32CurPos+i];
            if ((pszSource[i32CurPos+i] == '\\') && (pszSource[i32CurPos+i+1] == '"')
                && (pszSource[i32CurPos+i-1] != '\\'))
            {
                pszBuffer[i-1 + 1] = pszSource[i32CurPos+i + 1];
                i++;
            }
        }
        if('"' == pszSource[i32CurPos+i])
        {
            i++;
            i32CurPos	+= i;
            Type		= RUL_STR_CONSTANT;
            SubType		= RUL_STRING_CONSTANT;
            return true;
        }
    }
    return false;
}

bool CLexicalAnalyzer::isChar(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    memset(pszBuffer,0,BUFFER_SIZE);
    if('\'' == pszSource[i32CurPos])
    {
        _INT32		i=1;
        for(;'\'' != pszSource[i32CurPos+i];i++)
        {
            if ('\\' == pszSource[i32CurPos+i])
            {
                //this must be one of the escape sequences or '\0' NULL terminator
                i++;
                switch (pszSource[i32CurPos+i])
                {
                case '0':
                    pszBuffer[i-2] = '\0';
                    break;
                case 'a':
                    pszBuffer[i-2] = '\a';
                    break;
                case 'f':
                    pszBuffer[i-2] = '\f';
                    break;
                case 'n':
                    pszBuffer[i-2] = '\n';
                    break;
                case 'r':
                    pszBuffer[i-2] = '\r';
                    break;
                case 't':
                    pszBuffer[i-2] = '\t';
                    break;
                case 'v':
                    pszBuffer[i-2] = '\v';
                    break;
                case '\'':
                case '"':
                case '|':
                case '?':
                case '\\':
                    pszBuffer[i-2] = pszSource[i32CurPos+i];	//charactor after '\'
                    break;
                default:
                    pszBuffer[i-2] = pszSource[i32CurPos+i-1];	//'\'
                    pszBuffer[i-1] = pszSource[i32CurPos+i];	//charactor after '\'
                    break;
                }
            }
            else
            {
                pszBuffer[i-1] = pszSource[i32CurPos+i];		//charactor after '''
            }
        }
        if('\'' == pszSource[i32CurPos+i])
        {
            i++;
            i32CurPos	+= i;
            Type		= RUL_CHR_CONSTANT;
            SubType		= RUL_CHAR_CONSTANT;
            return true;
        }
    }
    return false;
}

//Store the previous token for identifying the
//unary- operator

_INT32	CLexicalAnalyzer::Tokenize(
            _INT32				i32CurState,
            RUL_TOKEN_TYPE		Type,
            RUL_TOKEN_SUBTYPE	SubType,
            CToken**			ppToken,
            _CHAR*				pszBuffer)
{
    try
    {
        *ppToken	= new CToken(pszBuffer,Type,SubType,GetLineNumber());
        if(0 == *ppToken)
        {
            throw(C_UM_ERROR_LOWMEMORY);
        }
        m_PrevToken = m_CurToken;
        if (m_PrevToken.GetLexeme())
        {
            PS_Strcpy((_CHAR*)m_szRuleName, RULENAMELEN, (const _CHAR*)m_PrevToken.GetLexeme());
        }
        m_CurToken	= **ppToken;

        if(Type == RUL_TYPE_ERROR)
            strcpy(pszBuffer,"Error in Lexical Analysis");

        return LEX_SUCCESS;
    }
    catch(_INT32 error)
    {
        DELETE_PTR(*ppToken);
        throw(error);
    }
    return LEX_FAIL;
}

//Store the previous token for identifying the
//unary- operator

_INT32	CLexicalAnalyzer::Tokenize(
            _INT32				i32CurState,
            RUL_TOKEN_TYPE		Type,
            RUL_TOKEN_SUBTYPE	SubType,
            CToken**			ppToken,
            _CHAR*				pszBuffer,
            COMPOUND_DATA&		cmpData)
{
    try
    {
        memset(pszBuffer,0,BUFFER_SIZE);
        strcat(pszBuffer,cmpData.m_szName);

        *ppToken	= new CToken(pszBuffer,Type,SubType,cmpData,GetLineNumber());
        if(0 == *ppToken)
        {
            throw(C_UM_ERROR_LOWMEMORY);
        }
        m_PrevToken = m_CurToken;
        PS_Strcpy((_CHAR*)m_szRuleName, RULENAMELEN, (const _CHAR*)m_PrevToken.GetLexeme());
        m_CurToken	= **ppToken;

        if(Type == RUL_TYPE_ERROR)
            strcpy(pszBuffer,"Error in Lexical Analysis");

        return LEX_SUCCESS;
    }
    catch(_INT32 error)
    {
        DELETE_PTR(*ppToken);
        throw(error);
    }

    return LEX_FAIL;
}

_INT32	CLexicalAnalyzer::TokenizeWithoutSave(
            _INT32				i32CurState,
            RUL_TOKEN_TYPE		Type,
            RUL_TOKEN_SUBTYPE	SubType,
            CToken**			ppToken,
            _CHAR*				pszBuffer
            )
{
    try
    {
        *ppToken	= new CToken(pszBuffer,Type,SubType,GetLineNumber());
        if(0 == *ppToken)
        {
            throw(C_UM_ERROR_LOWMEMORY);
        }

        if(Type == RUL_TYPE_ERROR)
            strcpy(pszBuffer,"Error in Lexical Analysis");

        return LEX_SUCCESS;
    }
    catch(_INT32 error)
    {
        DELETE_PTR(*ppToken);
        throw(error);
    }
    return LEX_FAIL;
}

bool	CLexicalAnalyzer::MatchGrammarTerminals(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    bool bRetVal = false;

    CBuiltInMapper::GetInstance()->LockBIMapper(GetMEEInterface()->GetStartedProtocol());
    CBuiltInMapper* pMapper = CBuiltInMapper::GetInstance(); //WHS May 24 2007 BUILTIN SUBCLASSING
    unsigned int i32Size = pMapper->GetSize(); //WHS May 24 2007 BUILTIN SUBCLASSING
    unsigned int i = 0, j = 0;

    for(i=0; i<i32Size;i++)
    {
        unsigned int i32Len = (unsigned int)strlen((*pMapper)[i].GetName().c_str()); //WHS May 24 2007 BUILTIN SUBCLASSING
        memset(pszBuffer,0,BUFFER_SIZE);
        for(j=0;(pszSource[i32CurPos+j]) && (j<i32Len); j++)
        {
            pszBuffer[j] = pszSource[i32CurPos+j];

            if( ((*pMapper)[i].GetName().c_str())[j] != pszSource[i32CurPos+j] ) //WHS May 24 2007 BUILTIN SUBCLASSING
                break;
        }

        if ((j == i32Len) && (pszSource[i32CurPos+j] == '_')
            && ( (*pMapper)[i].GetTokenType() == RUL_KEYWORD)) //WHS May 24 2007 BUILTIN SUBCLASSING
        {
            continue;
        }

        if(
            (j == i32Len)
            && ( ((*pMapper)[i].GetTokenType() != RUL_KEYWORD) ||  //WHS May 24 2007 BUILTIN SUBCLASSING
               ( ((*pMapper)[i].GetTokenType() == RUL_KEYWORD) && !isalnum(pszSource[i32CurPos+j])) ))	//WHS May 24 2007 BUILTIN SUBCLASSING
        {
            i32CurPos	+=	(long)j;
            Type		=	(*pMapper)[i].GetTokenType();	  //WHS May 24 2007 BUILTIN SUBCLASSING
            SubType		=	(*pMapper)[i].GetTokenSubtype();  //WHS May 24 2007 BUILTIN SUBCLASSING
            bRetVal = true;
            break;
        }
    }
    CBuiltInMapper::GetInstance()->ReleaseBIMapper();

    //make sure the buffer containing function name following '('
    if (bRetVal && (Type == RUL_KEYWORD) && (SubType == RUL_FUNCTION))
    {
        i = i32CurPos;
		while ((pszSource[i] == ' ') || (pszSource[i] == '\n') || (pszSource[i] == '\t') || (pszSource[i] == '\r'))
        {
            i++;	//get rid of empty space before next charaacter
			if (pszSource[i] == '\n')
			{
				m_i32LineNo++;	//get rid of new line before next charaacter
        }
		}
		
		if (pszSource[i] != '(')
		{
			//this is not function name. recovering parameters
			i32CurPos	-=	j;
			bRetVal = false;
		}
	}

    return bRetVal;
}

bool	CLexicalAnalyzer::MatchOMService(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    size_t nSize = sizeof(OM_Service)/sizeof(DFA_State);
    size_t i = 0, j = 0;

    for(i=0; i<nSize;i++)
    {
        size_t nLen = strlen(OM_Service[i].szWord);
        memset(pszBuffer,0,BUFFER_SIZE);
        for(j=0;(pszSource[i32CurPos+j]) && (j<nLen); j++)
        {
            pszBuffer[j] = pszSource[i32CurPos+j];

            if(OM_Service[i].szWord[j] != pszSource[i32CurPos+j])
                break;
        }
        if((j == nLen)
            && ((OM_Service[i].Type != RUL_KEYWORD)
                    || ((OM_Service[i].Type == RUL_KEYWORD) && !isalnum(pszSource[i32CurPos+j]))))	//match found
        {
            i32CurPos	+= (_INT32)j;
            Type		= OM_Service[i].Type;
            SubType		= OM_Service[i].SubType;
            return true;
        }
    }

    return false;
}


bool	CLexicalAnalyzer::isTerminal(
            const _CHAR*		szSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    return MatchGrammarTerminals(szSource,i32CurPos,Type,SubType,pszBuffer);
}

bool isPointChar(
        unsigned char ch)
{
    return (isalnum(ch) || ch == '.');
}

//	match for the regular expression
//	letter		--> [a-zA-Z]
//	digit		--> [0-9]
//	identifier	-->	letter(letter|digit)*
bool CLexicalAnalyzer::isIdentifier(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer)
{
    try
    {
#ifdef _FULL_RULE_ENGINE
        if(isalpha(pszSource[i32CurPos]) || (pszSource[i32CurPos] == '_'))
        {
    //Get the Identifier
            _INT32 i=0;
            memset(pszBuffer,0,BUFFER_SIZE);
            for(i=0;
                    (pszSource[i32CurPos+i])
                    && ( isalnum(pszSource[i32CurPos+i])
                            || pszSource[i32CurPos+i] == '_')
                    && (i<BUFFER_SIZE);
                i++)
            {
                pszBuffer[i] = pszSource[i32CurPos+i];
            }
    //Variable -- pszBuffer -- It's got to be a variable now.
            i32CurPos	+= i;
            Type		= RUL_SIMPLE_VARIABLE;//Keywords[i].Type;
            SubType		= RUL_SUBTYPE_NONE;//Keywords[i].SubType;

            if(i>=BUFFER_SIZE)
            {
                throw(C_LEX_ERROR_IDLONG);
            }
            return LEX_SUCCESS;
        }

#else
        if(pszSource[i32CurPos] == '#')
            ++nCurPos;
        else
            return LEX_FAIL;
        if(isalnum(pszSource[i32CurPos]))
        {
    //Get the Identifier
            _INT32 i=0;
            memset(pszBuffer,0,BUFFER_SIZE);
            for(i=0; (pszSource[i32CurPos+i]) && isPointChar(pszSource[i32CurPos+i]);i++)
            {
                pszBuffer[i] = pszSource[i32CurPos+i];
            }

    //Variable -- pszBuffer -- It's got to be a variable now.
            nCurPos += i;
            Type	= RUL_SIMPLE_VARIABLE;//Keywords[i].Type;
            SubType = RUL_SUBTYPE_NONE;//Keywords[i].SubType;

            _WORD	wPointDataType;
            _UINT32 wErrCode = g_PointDatabase->GetPointDataType(pszBuffer, wPointDataType);
            if(wErrCode != DCAP_SUCCESS)
                return LEX_FAIL;
            else
                SubType = (enum RUL_TOKEN_SUBTYPE)wPointDataType ; //RUL_INTEGER_DECL;
            return LEX_SUCCESS;
        }

#endif
    }
    catch(_INT32 error)
    {
        throw(error);
    }
    return LEX_FAIL;
}

_INT32 CLexicalAnalyzer::GetLineNumber()
{
    return m_i32CurLineNo;
}

//The Object access is of the form
// <ObjectManager::><Id(.Id)*>
bool CLexicalAnalyzer::isObject(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer,
            COMPOUND_DATA&		cmpData)
{
    _INT32 i32Temp = i32CurPos;
    return LEX_FAIL;
}

bool CLexicalAnalyzer::isService(
            const _CHAR*		pszSource,
            _INT32&				i32CurPos,
            RUL_TOKEN_TYPE&		Type,
            RUL_TOKEN_SUBTYPE&	SubType,
            _CHAR*				pszBuffer,
            COMPOUND_DATA&		cmpData)
{
    _INT32 i32Temp = i32CurPos;
    if(MatchOMService(pszSource,i32Temp,Type,SubType,pszBuffer)  && (Type == RUL_SERVICE))
    {
        i32CurPos = i32Temp;	//if the token is a desired one, then update the cur pointer.
        RUL_TOKEN_TYPE		newType;
        RUL_TOKEN_SUBTYPE	newSubType;
        memset(&cmpData,0,sizeof(COMPOUND_DATA));
        _INT32				i=i32CurPos;

        EAT_SPACE;
        if(isTerminal(pszBuffer,i,newType,newSubType,pszBuffer) && (newSubType==RUL_SCOPE))
        {
            EAT_SPACE;
            if(isIdentifier(pszSource,i,newType,newSubType,pszBuffer))
            {
        //Now go on looking for (.Id)*
                //1.	Copy the buffer into the Compound Data.
                //2.	Go on looking for <alphanum> and <.>
                i32CurPos = i;
                size_t nLen = strlen(pszBuffer);
                memset(cmpData.m_szName,0,nLen+1);
                memcpy(cmpData.m_szName,pszBuffer,nLen);

                if(isTerminal(pszSource,i,newType,newSubType,pszBuffer) && (newSubType==RUL_DOT))
                {
                    if(isIdentifier(pszSource,i,newType,newSubType,pszBuffer))
                    {
                        strcat(cmpData.m_szAttribute, ".");
                        strcat(cmpData.m_szAttribute, pszBuffer);
                        i32CurPos = i;
                        if(isTerminal(pszSource,i,newType,newSubType,pszBuffer) && (newSubType==RUL_LPAREN))
                        {
                            if(isTerminal(pszSource,i,newType,newSubType,pszBuffer) && (newSubType==RUL_RPAREN))
                            {
                                SubType = RUL_SERVICE_INVOKE;
                                i32CurPos = i;
                            }
                            else
                            {
                                //error -- mismatched parenthesis
                                return LEX_FAIL;
                            }
                        }
                        return LEX_SUCCESS;
                    }
                    else
                    {
                        //error -- no identifier after <.>
                        return LEX_FAIL;
                    }
                }
                else
                {
                    return LEX_SUCCESS;
                }
            }
            else
            {
                //error --  no identifier after <::>
                return 0;
            }
        }
    }
    return LEX_FAIL;
}

_CHAR* CLexicalAnalyzer::GetRuleName()
{
    return (_CHAR*)m_szRuleName;		//token name
}

_INT32	CLexicalAnalyzer::MoveTo(
            RUL_TOKEN_TYPE		Type,
            RUL_TOKEN_SUBTYPE	SubType,
            CSymbolTable*		pSymbolTable)
{
    CToken* pToken=0;
    _INT32	i32Ret = LEX_FAIL;
    bool	bIsFound = false;
    while(LEX_SUCCESS == (i32Ret = GetNextToken(&pToken,pSymbolTable))
        && pToken)
    {
        RUL_TOKEN_TYPE newType			= pToken->GetType();
        RUL_TOKEN_SUBTYPE newSubType	= pToken->GetSubType();
        DELETE_PTR(pToken);

        if(newType == Type	&& newSubType == SubType)
        {
            bIsFound = true;
            break;
        }
    }
    DELETE_PTR(pToken);
    if(bIsFound)
    {
        UnGetToken();
    }
    return i32Ret;
}

_INT32	CLexicalAnalyzer::MovePast(
            RUL_TOKEN_TYPE		Type,
            RUL_TOKEN_SUBTYPE	SubType,
            CSymbolTable*		pSymbolTable)
{
    CToken* pToken=0;
    _INT32 i32Ret = LEX_FAIL;
    while(LEX_SUCCESS == (i32Ret = GetNextToken(&pToken,pSymbolTable))
        && pToken)
    {
        RUL_TOKEN_TYPE newType			= pToken->GetType();
        RUL_TOKEN_SUBTYPE newSubType	= pToken->GetSubType();
        DELETE_PTR(pToken);

        if(newType == Type	&& newSubType == SubType)
        {
            break;
        }
    }
    DELETE_PTR(pToken);
    return i32Ret;
}

_INT32	CLexicalAnalyzer::SynchronizeTo(
        PRODUCTION				production,
        CSymbolTable*		pSymbolTable)
{
    CToken* pToken=0;
    _INT32 i32Ret = LEX_FAIL;
    bool	bIsFound = false;

	try
	{
		while(LEX_SUCCESS == (i32Ret = GetNextToken(&pToken,pSymbolTable))
			&& pToken)
		{
			RUL_TOKEN_TYPE newType			= pToken->GetType();
			RUL_TOKEN_SUBTYPE newSubType	= pToken->GetSubType();
			DELETE_PTR(pToken);

			if(FOLLOW_SET::IsPresent(production,newType,newSubType))
			{
				bIsFound = true;
				break;
			}
		}
		DELETE_PTR(pToken);		//to avoid memory leak
		if(bIsFound)
		{
			UnGetToken();
		}
	}
	catch (_INT32 error)
	{
		DELETE_PTR(pToken);
		throw(error);
	}

    //keep rule name for error message display
    if (strlen(m_szRuleName) == 0)
    {
        PS_Strcpy((_CHAR*)m_szRuleName, RULENAMELEN, m_CurToken.GetLexeme());
    }

    return i32Ret;
}

_BOOL CLexicalAnalyzer::GetRulString(
                            RUL_TOKEN_TYPE tokenType
                            , RUL_TOKEN_SUBTYPE tokenSubType
                            , _CHAR*		pszRulString
                            )
{
    _BOOL bRetVal = false;

    CBuiltInMapper::GetInstance()->LockBIMapper(GetMEEInterface()->GetStartedProtocol());
    CBuiltInMapper* pMapper = CBuiltInMapper::GetInstance(); //WHS May 24 2007 BUILTIN SUBCLASSING
    _INT32 i32Size = (_INT32)pMapper->GetSize(); //WHS May 24 2007 BUILTIN SUBCLASSING

    for (int iLoopVar = 0;iLoopVar < i32Size;iLoopVar++)
    {
        if ( ((*pMapper)[iLoopVar].GetTokenType() == tokenType) && //WHS May 24 2007 BUILTIN SUBCLASSING
             ((*pMapper)[iLoopVar].GetTokenSubtype() == tokenSubType) ) //WHS May 24 2007 BUILTIN SUBCLASSING
        {
            strcpy(pszRulString, (*pMapper)[iLoopVar].GetName().c_str() ); //WHS May 24 2007 BUILTIN SUBCLASSING
            bRetVal = true;
            break;
        }
    }
    CBuiltInMapper::GetInstance()->ReleaseBIMapper();

    return bRetVal;
}


//This fuction gets the Expression followed by the First dot operator or ( or [ operator
//which we need to resolve while parsing the statement.
//For example, if expression mixes with dot operator and '[' operator like processVariables[index][index].DEVICE_VARIABLE.DIGITAL_VALUE,
//this function shall return the expression extention name as "[index][index].DEVICE_VARIABLE.DIGITAL_VALUE".
bool CLexicalAnalyzer::GetComplexDotExp(const int iPosOfDot, /* out */char** szDotExpression)
{
    if (!szDotExpression)
    {
        return false;
    }

    const _CHAR*		pszSource = (const _CHAR*)m_pszSource;
    RUL_TOKEN_TYPE		Type = RUL_TYPE_NONE;
    RUL_TOKEN_SUBTYPE	SubType = RUL_SUBTYPE_NONE;
    char pszBuffer[100];
    int iLeftBrackCount = 0;
    int iLeftBrace = 0;
    bool bFound = false;
    long int lCount = 0;							//number of characters between first non-space character and end of reference expression
    long int lLastCount = 0;						//number of white spaces between last non-space character and end of reference expression
    long int lStart = iPosOfDot;					//first non-space character

    //Loop through and get the total count of the Expression
    for(long int i = iPosOfDot; (pszSource[i] != '\0');)
    {
        if(isSpace(pszSource,i,Type,SubType,pszBuffer))	//variable i is incremented here
        {
            //skip white space
            if (lCount > 0)
            {
                lCount++;
            }
            lLastCount++;
            continue;
        }

        if ( (iLeftBrace == 0) && (iLeftBrackCount == 0) && IsEndofComDotOp(pszSource[i], i) )
        {
            //end of DD_ITEM name expression
            break;
        }

        if (0 == lCount)
        {
            lStart = i;
        }
        lLastCount = 0;

        if(pszSource[i] == '(')
        {
            iLeftBrace++;
            bFound = true;
        }
        else if(pszSource[i] == ')')
        {
            iLeftBrace--;
        }
        else if(pszSource[i] == '[')
        {
            iLeftBrackCount++;
            bFound = true;
        }
        else if(pszSource[i] == ']')
        {
            iLeftBrackCount--;
        }
        else if(pszSource[i] == '.')
        {
            bFound = true;
        }

        lCount++;
        i++;
    }

    //check DD expression correction
    if ( (iLeftBrace != 0) || (iLeftBrackCount != 0) || (lCount == 0) || !bFound )
    {
        return false;	//something wrong
    }

    //Get the Expression after dot (.) or ( or [
    lCount -= lLastCount;
    *szDotExpression = new char[lCount + 1];
    PS_Strncpy(*szDotExpression, lCount + 1, (const char*)&m_pszSource[lStart], _TRUNCATE);

    return true;
}

//Helper Function to know whether DD Express is terminated
bool CLexicalAnalyzer::IsEndofComDotOp(char ch, long int i)
{
    if( (m_pszSource[i] == '+')||
        (m_pszSource[i] == '-')||
        (m_pszSource[i] == '/')||
        (m_pszSource[i] == '*')||
        (m_pszSource[i] == '%')||
        (m_pszSource[i] == ';')||
        (m_pszSource[i] == '=')||
        (m_pszSource[i] == '>')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == '<')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == '!')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == '&')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == '|')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == '^')||	/* added as per Anil 24oct05 */
        (m_pszSource[i] == ',')||
        //Anil 040806 Bug fix 562
        //If the DD expression follwed by ")", which is the case in if statement
        (m_pszSource[i] == ')')

        )
    {

        return true;
    }
    return false;
}

// add the ability to say goto here. This captures a position
void  CLexicalAnalyzer::CaptureState()
{

    m_i32CapturedCurrentPos = m_i32CurrentPos;
    m_i32CapturedPrevPos = m_i32PrevPos ;
    m_i32CapturedLAPosition = m_i32LAPosition;
    m_i32CapturedLineNo = m_i32LineNo;
    m_i32CapturedCurLineNo = m_i32CurLineNo;
    m_i32CapturedPrevLineNo = m_i32PrevLineNo;

}
// add the ability to say goto here. This returns us to a position
void  CLexicalAnalyzer::ReturnState()
{

    m_i32CurrentPos = m_i32CapturedCurrentPos;
    m_i32PrevPos = m_i32CapturedPrevPos;
    m_i32LAPosition = m_i32CapturedLAPosition;
    m_i32LineNo = m_i32CapturedLineNo;
    m_i32CurLineNo = m_i32CapturedCurLineNo;
    m_i32PrevLineNo = m_i32CapturedPrevLineNo;

}


bool CLexicalAnalyzer::isErrorIn(_INT32 ErrNumber, ERROR_VEC* pvecErrors)
{
    bool bFound = false;
    for (ERROR_VEC::iterator iT = pvecErrors->begin(); iT != pvecErrors->end(); iT++)
    {
        if ((*iT)->getErrorNumber() == (_INT32)ErrNumber)
        {
            bFound = true;
            break;
        }
    }
    return bFound;
}
