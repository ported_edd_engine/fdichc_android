/**
 *	@(#) $Id: cm_struc.h,v 1.33 1996/09/13 19:52:05 kimwolk Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_struct.h - Connection Manager Structure Definitions
 *
 *	This file contains all structure definitions for
 *	the Connection Manager Library.
 */

#ifndef CM_STRUCT_H
#define CM_STRUCT_H

#ifndef RTN_CODE_H
#include "rtn_code.h"
#endif

#ifndef DDLDEFS_H
#include "ddldefs.h"
#endif

#ifndef OD_DEFS_H
#include "od_defs.h"
#endif

#include "ddi_lib.h"
#include "tx_hart.h"

#include <ServerProjects/DeviceIdent.h>

/*
 *	Connection Manager Modes
 */

#define	CMM_ON_LINE		1
#define	CMM_OFF_LINE	2

/*
 * Table Display Modes
 */

#define	CM_TBL_VALUE	1
#define	CM_TBL_HEADER	2

/*
 * CM_COMPAT types
 */

#define OLDER_DD_LOADED		0
#define CURRENT_DD_LOADED	1
#define	NEWER_DD_LOADED		2
#define DD_NOT_LOADED		3

/*
 * Connection manager fetch priorities.
 */
#define CM_FETCH_PRIORITY	10


/*
 *	Connection Manager Typedefs
 */

typedef int		DEVICE_HANDLE;
typedef int		DEVICE_TYPE_HANDLE;
typedef int		ROD_HANDLE;
typedef int		FLAT_HANDLE;

typedef	double	CM_TIME ;

typedef unsigned CM_VIEW;
typedef	unsigned int	CM_DB_HANDLE ;

typedef	int	CM_OP ;
typedef long CM_STATUS ;
typedef	int	CM_COMPAT ;

#define	CM_OPEN_BLOCK_OP 			1
#define	CM_OPEN_ADDR_OP				2
#define CM_IDENTIFY_OP				3
#define CM_IDLE_OP					4
#define CM_COMM_GET_OP				5
#define	CM_COMM_PUT_OP				6
#define	CM_COMM_READ_OP				7
#define	CM_COMM_WRITE_OP			8
#define	CM_COMM_UPLOAD_OP			9
#define	CM_PARAM_LOAD_OP			10
#define	CM_DELETE_BLOCK_OP			11
#define	CM_READ_TAG_OP				12
#define	CM_ASSIGN_TAG_OP			13
#define	CM_SEND_CMD_OP				14
#define	CM_INITIATE_BURST_OP		15
#define	CM_TERMINATE_BURST_OP		16
#define	CM_ACTIVE_BLOCK_LIST_OP		17
#define	CM_DEVICE_TYPES_OP			18
#define	CM_POLL_ADDR_OP				19
#define	CM_BI_SEND_CMD_OP			20
#define	CM_INITIATE_COMM_OP			21
#define	CM_TERMINATE_COMM_OP		22
#define	CM_NON_DD_PARAM_LIST_OP		23
#define	CM_NON_DD_WRITE_PARAM_OP	24
#define	CM_NON_DD_PARAM_READ_OP		25
#define CM_BURN_EEPROM_OP			26
#define	CM_MORE_DATA_OP				27
#define CM_HAVE_VALUE_OP			28
#define CM_ASSIGN_ASSEM_NUM_OP		29
#define CM_OPENING_AND_HAVE_VALUE_OP	30


typedef UINT8		BOOLEAN;

typedef struct {
	UINT32	mfrid ;
	UINT16	mfr_dev_type ;
	UINT8	xmtr_spec_rev ;
	LPTSTR	tag ;
	UINT8	device_id[3] ;
	UINT8	hart_rev;
	bool	bUseBasePathUnmodified;
} SIMPLE_OPEN ;

typedef struct {
	BLOCK_HANDLE	block_handle ;
	int				status ;
} BLK_LIST_ELEM ;

typedef struct {
	int				count ;
	BLK_LIST_ELEM	list[16] ;
} BLK_LIST ;


typedef struct {
	ITEM_ID					block_id ;
	BLOCK_HANDLE			block_handle ;
} CM_BLOCK_LIST_ELEM;

typedef struct {
	int						count ;
	CM_BLOCK_LIST_ELEM		*list ;
} CM_BLOCK_LIST ;

typedef struct {
	ITEM_ID					block_type_id ;
} CM_BLOCK_TYPE_LIST_ELEM ;

typedef struct {
	int						count ;
	CM_BLOCK_TYPE_LIST_ELEM	*list ;
} CM_BLOCK_TYPE_LIST ;

typedef struct {
	unsigned short		cm_type;
	unsigned short		cm_size;
	union {
		float			cm_float;
		double			cm_double;
		unsigned long	cm_ulong;
		long			cm_long;
		char			*cm_stream;
		char			cm_ca[8];
	} cm_val;
} CM_VALUE;


typedef struct {
	int			cmvl_count;
	CM_VALUE	*cmvl_list;
} CM_VALUE_LIST;

typedef int		CREF;

typedef struct {
	UINT32          ddid_manufacturer;
	TCHAR			base_dd_path[_MAX_PATH] ;
} DD_MANUFACTURER_ID;

typedef struct DD_DEVICE_ID {
	UINT32          ddid_manufacturer;
	UINT16          ddid_device_type;
	UINT8           ddid_device_rev;
	int				ddid_dd_rev;  // Changed to int so we can know when this has been initialized. DDC
	LPTSTR			base_dd_path;
	LPTSTR			dd_path;
	bool			bUseBasePathUnmodified;
} DD_DEVICE_ID;





/* Active Block Table */

typedef struct {
	int				sub_station ;
} ABT_HART_SPEC ;

typedef struct {
	OBJECT_INDEX	op_index ;
} ABT_ISP_SPEC ;

typedef struct {
	ABT_HART_SPEC	abt_hart ;
	ABT_ISP_SPEC	abt_isp ;
} ABT_NET_SPEC ;


/* Active Device Table */

typedef	int BAUD_RATE ;

#define	HART_PRIMARY_MASTER		0
#define HART_SECONDARY_MASTER	1
typedef	int MASTER_TYPE ;


#define	DEVICE_NOT_IDENTIFIED	0
#define DEVICE_IDENTIFIED		1
#define	DEVICE_DISAPPEARED		2

typedef int CM_IDENTIFIED_TYPE ;



const int	NST_OFF_LINE	= 0;	/* Off-Line Mode */
const int	NST_FB			= 1;	/* ISP Network */
const int	NST_FB_SIM		= 2;	/* Simulated ISP Network */
const int	NST_HART		= 3;	/* HART Network */
const int	NST_HART_SIM	= 4;	/* Simulated HART Network */
const int	NST_ETHER		= 5;	/* Ethernet Network (connected to ISP) */
const int	NST_CONTROL		= 6;	/* Control Network */
const int	NST_DATABASE	= 7;	/* Database network for File Server */
const int	NST_FB_DELTAV	= 8;	/* Fieldbus Using the DeltaV Pass-Through */
const int	NST_HART_PT		= 9;	/* HART Pass-through Network */


#define DF_ISP		1	/* ISP Device Family */

#define DF_HART		2	/* Alternate Device Family */



#define CT_OPOD		1
#define CT_DDOD		2
#define CT_MIB		3

typedef int COMM_TYPE;

#endif	/* CM_STRUCT_H */
