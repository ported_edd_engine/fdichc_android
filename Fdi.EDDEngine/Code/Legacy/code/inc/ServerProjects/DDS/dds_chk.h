/**
 * @(#) $Id: dds_chk.h,v 1.2 1995/05/22 11:44:28 donmarq Exp $
 * Copyright 1994 Rosemount, Inc. - All rights reserved 
 *
 */

#ifndef DDS_CHK_H
#define DDS_CHK_H

#include "std.h"
#include "table.h"
#include "tags_sa.h"
#include "ddldefs.h"
#include "flats.h"

#ifdef __cplusplus
    extern "C" {
#endif

/*
 * Flat item list structure checking macros
 */

#define	CHK_FLAT_LIST(A,M,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_VAR_FLAT_LIST(A,M,F,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else if (F) {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_STRING(A,M,S)	\
{	\
	if ((A) & (M)) {	\
		if ((S).len) {	\
			ASSERT_DBG((S).str);	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(S).str && !(S).len);	\
	}	\
}

/*
 * Directory table structure checking macros
 */

#define CHK_BIN_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG((T).chunk && (T).size);	\
	}	\
	else {	\
		ASSERT_DBG(!(T).chunk && !(T).size);	\
	}	\
}

#define CHK_FLAT_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG(((T).list && (T).count) ||	\
			(!(T).list && !(T).count));	\
	}	\
	else {	\
		ASSERT_DBG(!(T).list && !(T).count);	\
	}	\
}

/*
 * Function prototypes
 */

#ifdef DEBUG
extern void dds_item_flat_check P(( void *, ITEM_TYPE ));
extern void dds_dev_dir_flat_chk P(( BIN_DEVICE_DIR *, FLAT_DEVICE_DIR * ));
extern void dds_blk_dir_flat_chk P(( BIN_BLOCK_DIR *, FLAT_BLOCK_DIR * ));
#endif


#ifdef __cplusplus
    }
#endif

#endif
