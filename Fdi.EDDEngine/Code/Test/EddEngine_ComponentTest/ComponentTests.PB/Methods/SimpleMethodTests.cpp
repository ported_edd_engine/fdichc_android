#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"
#include <nsEDDEngine/ProtocolType.h>
#include <time.h>

#define make_time_t( a)    ( (long)   (a) )	/* returns Htime_t, a long */



using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_running_test_method_empty_with_PB
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static  FLAT_METHOD* test_method;
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_empty", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_put_message_simple_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_simple", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Hello EDDL!),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_put_message_with_label_with_PB
        {
            static nsConsumer::EVAL_VAR_VALUE* returnValue;		//Fei test code

        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static ITEM_ID* test_var_item_id_1;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                //Fei test code
                //register final returned string variable
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_ASCII;
				returnValue->val.s= L"teststring";
                returnValue->size = wcslen(returnValue->val.s.c_str());

                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", returnValue);
                int errorCode = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", &errorCode);

                bool returnCode = true;
                builtinSupportSimulator->Recorder->Record(L"OnMethodExiting", new bool(returnCode));
                //end of test                

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_label", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
                // get the item ids for all parameters
                test_var_item_id_1 = new ITEM_ID();
                int rc =   engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_item_id_1);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_var_item_id_1;

                //Fei test code
                delete returnValue;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // method execution engine also obtains parameter value when evaluating its label
                Assert::AreEqual("InfoRequest(Label is test variable 1),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_put_message_with_unit_with_PB
        {
            static nsConsumer::EVAL_VAR_VALUE* returnValue;		//Fei test code

        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static ITEM_ID* test_var_item_id_1;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);                

                //Fei test code
                //register final returned string variable
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_ASCII;
				returnValue->val.s = L"GetParamValue(0,0,16399),InfoRequest(Unit is unit1),OnMethodExiting(0)";
                returnValue->size = wcslen(returnValue->val.s.c_str());

                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", returnValue);
                int errorCode = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", &errorCode);

                bool returnCode = true;
                builtinSupportSimulator->Recorder->Record(L"OnMethodExiting", new bool(returnCode));
                //end of test

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_unit", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
                // get the item ids for all parameters
                test_var_item_id_1 = new ITEM_ID();
                int rc = engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_item_id_1);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_var_item_id_1;

                //Fei test code
                delete returnValue;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // method execution engine also obtains parameter value when evaluating its unit
                Assert::AreEqual("InfoRequest(Unit is unit1),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_put_message_with_varids_with_PB
        {
            static nsConsumer::EVAL_VAR_VALUE* returnValue;		//Fei test code

        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static ITEM_ID* test_var_item_id_1;
            static ITEM_ID* test_var_item_id_2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                //Fei test code
                //register final returned integer variable
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_INTEGER;
                returnValue->size = 1;
                returnValue->val.i = 16;

                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", returnValue);
                int errorCode = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", &errorCode);

                bool returnCode = true;
                builtinSupportSimulator->Recorder->Record(L"OnMethodExiting", new bool(returnCode));
                //end of test

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_put_message_with_varids", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);

                // get the item ids for all parameters
                test_var_item_id_1 = new ITEM_ID();
                int rc =  engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_item_id_1);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
                test_var_item_id_2 = new ITEM_ID();
                rc =  engine->Instance->GetItemIdFromSymbolName(L"test_var_2", test_var_item_id_2);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_var_item_id_1;
                delete test_var_item_id_2;

                //Fei test code
                delete returnValue;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("GetParamValue,GetParamValue,InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // method execution engine obtains parameter value twice
                Assert::AreEqual(
                    "GetParamValue(0,00000000,"+*test_var_item_id_1+"),"
                    "GetParamValue(0,00000000,"+*test_var_item_id_2+"),"
                    "InfoRequest(var1: 16\nvar2: 16),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()) );
            }
        };

        [TestClass]
        public ref class When_running_test_method_acknowledge_simple_with_PB
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_acknowledge_simple", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_acknowledgementrequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->AcknowledgementRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_not_call_inforequest()
            {
                Assert::AreEqual(0, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("AcknowledgementRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("AcknowledgementRequest(Hello EDDL!),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_acknowledge_with_varids_with_PB 
        {
            static nsConsumer::EVAL_VAR_VALUE* returnValue;		//Fei test code

        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static ITEM_ID* test_var_item_id_1;
            static ITEM_ID* test_var_item_id_2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                //Fei test code
                //register method used variable value and funciton call OnMethodExiting()
                returnValue = new nsConsumer::EVAL_VAR_VALUE();
                returnValue->type = VT_INTEGER;
                returnValue->size = 4;
                returnValue->val.i = 0;

                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", returnValue);
                int errorCode = 0;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", &errorCode);

                bool returnCode = true;
                builtinSupportSimulator->Recorder->Record(L"OnMethodExiting", new bool(returnCode));
                //end of test

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_acknowledge_with_varids", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);

                // get the item ids for all parameters
                test_var_item_id_1 = new ITEM_ID();
                int rc =  engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_item_id_1);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
                test_var_item_id_2 = new ITEM_ID();
                rc =  engine->Instance->GetItemIdFromSymbolName(L"test_var_2", test_var_item_id_2);
                Assert::IsTrue(rc == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + rc);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
                delete test_var_item_id_1;
                delete test_var_item_id_2;

                //Fei test code
                delete returnValue;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_acknowledgementrequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->AcknowledgementRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_not_call_inforequest()
            {
                Assert::AreEqual(0, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual(
                    "GetParamValue,GetParamValue,AcknowledgementRequest,"
                    "OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                // method execution engine obtains parameter value twice
                String^ actual = gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str());
                Assert::AreEqual(
                    "GetParamValue(0,00000000,"+*test_var_item_id_1+"),"
                    "GetParamValue(0,00000000,"+*test_var_item_id_2+"),"
                    "AcknowledgementRequest(var1: 0\nvar2: 0),"
                    "OnMethodExiting(1)",
                    actual);

            }
        };

        [TestClass]
        public ref class When_running_test_method_select_from_list_simple_with_PB
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_select_from_list_simple", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_selectionrequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->SelectionRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("SelectionRequest,InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("SelectionRequest(What do you want?,Option 1;Option 2;Option 3),InfoRequest(0),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_delay_simple_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_delay_simple", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_delaymessagerequest()
            {
                Assert::IsTrue(1 <= builtinSupportSimulator->DelayMessageRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_not_call_inforequest()
            {
                Assert::AreEqual(0, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("DelayMessageRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("DelayMessageRequest(2,delay!),"
                    "OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_a_method_with_sendCommand_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("send_simple_read_command", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }
            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_a_positiv_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_sendCommand_once()
            {
                Assert::AreEqual(1, builtinSupportSimulator->SendCommandCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

        };

        [TestClass]
        public ref class When_reading_a_method_with_visibility_from_the_edd_engine_with_PB
        { 
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_METHOD* result;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_delay_simple", &test_method_specifier);
                genericItem = engine->GetGenericItem(
                    test_method_specifier.id,
                    nsEDDEngine::ITYPE_METHOD);

                result = dynamic_cast<FLAT_METHOD*>(genericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_provide_the_visibility_attribute()
            {
                Assert::IsTrue( result->isAvailable(visibility) );
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_provide_the_visibility_true()
            {
                Assert::IsTrue(result->visibility == nsEDDEngine::True);
            }
        };

        [TestClass]
        public ref class When_loading_an_EDD_containing_a_method_with_a_long_name_with_PB
        {
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_METHOD* result;
            static ITEM_ID* test_var_item_id;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000C.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("Method_with_very_long_name_which_circumstance_should_not_have_any_negative_effects_whatsoever_and_everything_should_run_smoothly", &test_method_specifier);
                genericItem = engine->GetGenericItem(
                    test_method_specifier.id,
                    nsEDDEngine::ITYPE_METHOD);

                result = dynamic_cast<FLAT_METHOD*>(genericItem->item);

                // get the item ids for all parameters
                test_var_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"Method_with_very_long_name_which_circumstance_should_not_have_any_negative_effects_whatsoever_and_everything_should_run_smoothly", test_var_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete builtinSupportSimulator;
                delete test_var_item_id;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_be_possible_to_get_the_method_item_with_correct_item_ID_from_EDD_Engine()
            {
                Assert::AreEqual(*test_var_item_id, result->id);
            }
        };

        [TestClass]
        public ref class When_running_test_method_that_calls_other_method_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_that_calls_other_empty_method", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }

        };

        [TestClass]
        public ref class When_running_test_method_that_calls_two_other_methods_with_parameter_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static  FDI_GENERIC_ITEM* genericItem;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {


                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_that_calls_other_method", &test_method_specifier);
                genericItem = engine->GetGenericItem(
                    test_method_specifier.id,
                    nsEDDEngine::ITYPE_METHOD);

                FLAT_METHOD* test_method = dynamic_cast<FLAT_METHOD*>(genericItem->item);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;

                wchar_t* languageCode = L"en";
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem;
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_with_parameter_by_value_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static  FDI_GENERIC_ITEM* genericItem;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {


                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_with_number_by_value", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REF_WITH_ARGS;
                methodAction.action.meth_ref_args.desc_id = test_method_specifier.id;
                methodAction.action.meth_ref_args.expr.eType = EXPR::EXPR_TYPE_STRING;
                methodAction.action.meth_ref_args.expr.size = (unsigned short)1;
                if (methodAction.action.meth_ref_args.expr.size > 0)
                {
					methodAction.action.meth_ref_args.expr.val.s.assign(L"5",false);
                }

                wchar_t* languageCode = L"en";
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem;
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {

                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {

                Assert::AreEqual("OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {

                Assert::AreEqual("OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }

        };
        
        [TestClass]
        public ref class When_reading_a_menu_that_contains_method_calls_with_arguments
        {
        private:
            static FLAT_MENU* menu_read_from_dd;
            static EddEngineController^ engine;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                BuiltinSupportSimulator* builtinSimulator = new BuiltinSupportSimulator();
                ParamCacheSimulator* paramCacheSimulator= new ParamCacheSimulator();
                
                engine = gcnew EddEngineController("Profibus\\FDI_PB_Test_0018.DDL.bin");
                engine->Initialize(paramCacheSimulator, builtinSimulator);
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                menu_read_from_dd = dynamic_cast<FLAT_MENU*>(genericItem->item);
                Assert::IsTrue(menu_read_from_dd != nullptr);
                genericItem->item = nullptr; // to prevent cleanup at end of scope

                delete builtinSimulator;
                //delete paramCacheSimulator; -- the Param Cache is deleted by the EddEngineController
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete menu_read_from_dd;
            }

            [TestMethod]
            void It_should_have_7_menu_items()
            {
                Assert::AreEqual((unsigned short)7, menu_read_from_dd->items.count);
            }

            [TestMethod]
            void The_third_menu_item_should_reference_a_method()
            {
                MENU_ITEM elementAtPosition2 = menu_read_from_dd->items.list[2];
                FDI_GENERIC_ITEM* elementreferencedByMenuItem2 = engine->GetGenericItem(elementAtPosition2.ref.desc_id);

                Assert::AreEqual((int)ITYPE_METHOD, (int)elementreferencedByMenuItem2->item->item_type);
            }

            [TestMethod]
            void The_third_menu_item_should_have_no_arguments()
            {
                MENU_ITEM elementAtPosition2 = menu_read_from_dd->items.list[2];

                Assert::AreEqual((unsigned)0, elementAtPosition2.ref.expr.val.s.length());
            }

            [TestMethod]
            void The_fourth_menu_item_should_reference_a_method()
            {
                MENU_ITEM elementAtPosition3 = menu_read_from_dd->items.list[3];
                FDI_GENERIC_ITEM* elementreferencedByMenuItem3 = engine->GetGenericItem(elementAtPosition3.ref.desc_id);

                Assert::AreEqual((int)ITYPE_METHOD, (int)elementreferencedByMenuItem3->item->item_type);
            }

            [TestMethod]
            void The_third_menu_item_should_have_argument_33()
            {
                MENU_ITEM elementAtPosition3 = menu_read_from_dd->items.list[3];

                Assert::AreEqual(0, wcscmp(elementAtPosition3.ref.expr.val.s.c_str(), L"33"));
            }

            [TestMethod]
            void The_fifth_menu_item_should_reference_a_method()
            {
                MENU_ITEM elementAtPosition4 = menu_read_from_dd->items.list[4];
                FDI_GENERIC_ITEM* elementreferencedByMenuItem4 = engine->GetGenericItem(elementAtPosition4.ref.desc_id);

                Assert::AreEqual((int)ITYPE_METHOD, (int)elementreferencedByMenuItem4->item->item_type);
            }

            [TestMethod]
            void The_fifth_menu_item_should_have_argument_Hello_in_quote_signs()
            {
                MENU_ITEM elementAtPosition4 = menu_read_from_dd->items.list[4];

                Assert::AreEqual(0, wcscmp(elementAtPosition4.ref.expr.val.s.c_str(), L"\"Hello!\""));
            }

            [TestMethod]
            void The_sixth_menu_item_should_reference_a_method()
            {
                MENU_ITEM elementAtPosition5 = menu_read_from_dd->items.list[5];
                FDI_GENERIC_ITEM* elementreferencedByMenuItem5 = engine->GetGenericItem(elementAtPosition5.ref.desc_id);

                Assert::AreEqual((int)ITYPE_METHOD, (int)elementreferencedByMenuItem5->item->item_type);
            }

            [TestMethod]
            void The_sixth_menu_item_should_have_arguments_2_and_5_point_331()
            {
                MENU_ITEM elementAtPosition5 = menu_read_from_dd->items.list[5];

                Assert::AreEqual(0, wcsncmp(elementAtPosition5.ref.expr.val.s.c_str(), L"2,5.331", 7)); // EddEngine pads the variable to 6 floating points, therefore only using 7 chars for comparing
            }

            [TestMethod]
            void The_seventh_menu_item_should_reference_a_method()
            {
                MENU_ITEM elementAtPosition6 = menu_read_from_dd->items.list[6];
                FDI_GENERIC_ITEM* elementreferencedByMenuItem6 = engine->GetGenericItem(elementAtPosition6.ref.desc_id);

                Assert::AreEqual((int)ITYPE_METHOD, (int)elementreferencedByMenuItem6->item->item_type);
            }

            [TestMethod]
            void The_seventh_menu_item_should_have_the_variable_name_as_argument()
            {
                MENU_ITEM elementAtPosition6 = menu_read_from_dd->items.list[6];

                Assert::AreEqual(0, wcsncmp(elementAtPosition6.ref.expr.val.s.c_str(), L"test_var_2", 7)); 
            }
        };

        [TestClass]
        public ref class When_reading_the_action_list_from_a_parameter_which_contains_a_method_with_arguments
        {
        private:
            static EddEngineController^ engine;
            static FLAT_VAR* parameterFromDD;
        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                BuiltinSupportSimulator* builtinSimulator = new BuiltinSupportSimulator();
                ParamCacheSimulator* paramCacheSimulator= new ParamCacheSimulator();

                engine = gcnew EddEngineController("Profibus\\FDI_PB_Test_0018.DDL.bin");
                engine->Initialize(paramCacheSimulator, builtinSimulator);
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                parameterFromDD = dynamic_cast<FLAT_VAR*>(genericItem->item);
                Assert::IsTrue(parameterFromDD != nullptr);
                genericItem->item = nullptr;
                delete builtinSimulator;
                //delete paramCacheSimulator; -- the Param Cache is deleted by the EddEngineController
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete parameterFromDD;
            }

            [TestMethod]
            void It_should_contain_a_post_read_action_list_with_1_member()
            {
                Assert::AreEqual((unsigned short)1, parameterFromDD->post_read_act.count);
            }

            [TestMethod]
            void The_post_read_action_should_be_of_type_REF_WITH_ARGUMENTS()
            {
                Assert::AreEqual((unsigned int)ACTION::ACTION_TYPE_REF_WITH_ARGS, (unsigned int)parameterFromDD->post_read_act.list[0].eType);
            }

            [TestMethod]
            void The_post_read_action_should_reference_method_with_id_8()
            {
                ACTION action_from_actionList = parameterFromDD->post_read_act.list[0];
                Assert::AreEqual((unsigned int)8, action_from_actionList.action.meth_ref_args.desc_id);
            }

            [TestMethod]
            void The_post_read_action_should_provide_the_argument_20()
            {
                ACTION action_from_actionList = parameterFromDD->post_read_act.list[0];
                Assert::AreEqual(0, wcscmp(L"20", action_from_actionList.action.meth_ref_args.expr.val.s.c_str()));
            }
        };


        [TestClass]
        public ref class When_running_test_method_with_parameter_by_reference_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;
            static  FDI_GENERIC_ITEM* genericItem;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {


                // setup BuiltinSupportSimulator
                builtinSupportSimulator = new BuiltinSupportSimulator();
                EVAL_VAR_VALUE* pValue = new EVAL_VAR_VALUE();
                pValue->size = 1;
                pValue->type = VT_INTEGER;
                pValue->val.i = 4;
                builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", pValue);
                builtinSupportSimulator->Recorder->Record(L"GetParamValue", new PC_ErrorCode(PC_SUCCESS_EC));

                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_with_number_by_ref", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REF_WITH_ARGS;
                methodAction.action.meth_ref_args.desc_id = test_method_specifier.id;
                methodAction.action.meth_ref_args.expr.eType = EXPR::EXPR_TYPE_STRING;
                methodAction.action.meth_ref_args.expr.size = (unsigned short)10;
                if (methodAction.action.meth_ref_args.expr.size > 0)
                {
					methodAction.action.meth_ref_args.expr.val.s.assign(L"test_var_1",false);	
                }

                wchar_t* languageCode = L"en";
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem;
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {

                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {

                Assert::AreEqual("GetParamValue,GetParamValue,SetParamValue,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {

                Assert::AreEqual("GetParamValue(0,00000000,13),GetParamValue(0,00000000,13),SetParamValue(0,00000000,13),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_that_calls_other_method_with_put_message_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_that_calls_put_message_method", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Hello EDDL!),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_running_test_method_that_put_message_the_result_of_an_other_method_with_PB 
        {
        public:
            static EddEngineController^ engine;
            static BuiltinSupportSimulator* builtinSupportSimulator;
            static nsEDDEngine::Mth_ErrorCode methodResult;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000F.DDL.bin");
                builtinSupportSimulator = new BuiltinSupportSimulator();
                engine->Initialize(nullptr, builtinSupportSimulator);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier("test_method_that_calls_other_method_and_put_message_the_result", &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                methodResult = engine->Instance->EndMethod(&asyncRunningMethod);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete builtinSupportSimulator;
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_return_true_as_method_result()
            {
                Assert::IsTrue(methodResult == METH_SUCCESS);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_inforequest()
            {
                Assert::AreEqual(1, builtinSupportSimulator->InfoRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_onmethodexiting()
            {
                Assert::AreEqual(1, builtinSupportSimulator->OnMethodExitingCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual("InfoRequest,OnMethodExiting",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual("InfoRequest(Result: 10),OnMethodExiting(1)",
                    gcnew System::String(builtinSupportSimulator->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassParameterInputRequests
        {
        private:
            static BuiltinSupportSimulator *builtinSupport;
            static EddEngineController^ engine;
            static ITEM_ID expectedItemIdParameter;
            static String^ _expectedOrder;
            static String^ _expectedParameter;

        public:

            static void SetupBase(String^ eddFileName, 
                String^ methodName, 
                String^ parameterName,
                String^ expectedOrder,
                String^ expectedParameter)
            { 

                _expectedOrder = expectedOrder;
                _expectedParameter = expectedParameter;

                builtinSupport = new BuiltinSupportSimulator();
                engine = gcnew EddEngineController(eddFileName);
                engine->Initialize(nullptr, builtinSupport);

                FDI_PARAM_SPECIFIER test_parameter_specifier;
                engine->FillParamSpecifier(parameterName, &test_parameter_specifier);
                expectedItemIdParameter = test_parameter_specifier.id;

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;

                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                engine->Instance->EndMethod(&asyncRunningMethod );

                //this is not methodAction's data, please don't delete it
                methodAction.action.meth_definition.data = nullptr;

            }

            static void CleanupBase()
            {
                if (builtinSupport) delete builtinSupport;

            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_ParameterInputRequest_once()
            {
                Assert::AreEqual(1, builtinSupport->ParameterInputRequestCallCount);
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_have_the_expected_itemid_for_the_requested_parameter()
            {
                nsEDDEngine::OP_REF_TRAIL *providedValue = nullptr;
                providedValue = (nsEDDEngine::OP_REF_TRAIL*)builtinSupport->Recorder->GetParameter(L"ParameterInputRequest.pParameter");

                Assert::AreEqual( expectedItemIdParameter, providedValue->desc_id);
            }   

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_in_correct_order()
            {
                Assert::AreEqual(_expectedOrder, gcnew System::String(builtinSupport->GetCalledMethodsAsString().c_str()));
            }

            [TestMethod, TestCategory("SimpleMethodTests")]
            void It_should_call_the_builtin_callbacks_with_correct_parameters()
            {
                Assert::AreEqual(_expectedParameter, gcnew System::String(builtinSupport->GetCalledMethodParametersAsString().c_str()));
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_starts_an_InputRequest_for_a_TIME_VALUE_in_PB_TestCase0014 :BaseClassParameterInputRequests
        {
        public:
            [TestInitialize]
            void Setup()
            {
                String^ fileName = "ProfiBus\\FDI_PB_Test_0014.DDL.bin";
                String^ methodName = "test_method_input_request_for_time_value";
                String^ parameterName = "test_variable_time_value";
                String^ expectedOrder = "GetParamValue,ParameterInputRequest,OnMethodExiting";
                String^ expectedParameter = "GetParamValue(0,00000000,21),ParameterInputRequest(Please enter time information:),OnMethodExiting(1)";

                SetupBase(
                    fileName, 
                    methodName, 
                    parameterName,
                    expectedOrder,
                    expectedParameter);                
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

        };

        [TestClass]
        public ref class When_calling_a_method_that_starts_an_InputRequest_for_a_DATE_in_PB_TestCase0014 :BaseClassParameterInputRequests
        {
        public:
            [TestInitialize]
            void Setup()
            {
                String^ fileName = "ProfiBus\\FDI_PB_Test_0014.DDL.bin";
                String^ methodName = "test_method_input_request_for_date";
                String^ parameterName = "test_variable_date";
                String^ expectedOrder = "GetParamValue,ParameterInputRequest,OnMethodExiting";
                String^ expectedParameter = "GetParamValue(0,00000000,22),ParameterInputRequest(Please enter date information:),OnMethodExiting(1)";

                SetupBase(
                    fileName, 
                    methodName, 
                    parameterName,
                    expectedOrder,
                    expectedParameter);


            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

        };

        [TestClass]
        public ref class When_calling_a_method_that_starts_an_InputRequest_for_a_DATE_AND_TIME_in_PB_TestCase0014 :BaseClassParameterInputRequests
        {
        public:
            [TestInitialize]
            void Setup()
            {
                String^ fileName = "ProfiBus\\FDI_PB_Test_0014.DDL.bin";
                String^ methodName = "test_method_input_request_for_date_and_time";
                String^ parameterName = "test_variable_date_and_time";
                String^ expectedOrder = "GetParamValue,ParameterInputRequest,OnMethodExiting";
                String^ expectedParameter = "GetParamValue(0,00000000,23),ParameterInputRequest(Please enter date and time information:),OnMethodExiting(1)";

                SetupBase(
                    fileName, 
                    methodName, 
                    parameterName,
                    expectedOrder,
                    expectedParameter);


            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_starts_an_InputRequest_for_a_TIME_in_PB_TestCase0014 :BaseClassParameterInputRequests
        {
        public:
            [TestInitialize]
            void Setup()
            {
                String^ fileName = "ProfiBus\\FDI_PB_Test_0014.DDL.bin";
                String^ methodName = "test_method_input_request_for_time";
                String^ parameterName = "test_variable_time";
                String^ expectedOrder = "GetParamValue,ParameterInputRequest,OnMethodExiting";
                String^ expectedParameter = "GetParamValue(0,00000000,24),ParameterInputRequest(Please enter time information:),OnMethodExiting(1)";

                SetupBase(
                    fileName, 
                    methodName, 
                    parameterName,
                    expectedOrder,
                    expectedParameter);

            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_starts_an_InputRequest_for_a_DURATION_in_PB_TestCase0014 :BaseClassParameterInputRequests
        {
        public:
            [TestInitialize]
            void Setup()
            {
                String^ fileName = "ProfiBus\\FDI_PB_Test_0014.DDL.bin";
                String^ methodName = "test_method_input_request_for_duration";
                String^ parameterName = "test_variable_duration";
                String^ expectedOrder = "GetParamValue,ParameterInputRequest,OnMethodExiting";
                String^ expectedParameter = "GetParamValue(0,00000000,25),ParameterInputRequest(Please enter duration information:),OnMethodExiting(1)";

                SetupBase(
                    fileName, 
                    methodName, 
                    parameterName,
                    expectedOrder,
                    expectedParameter);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass, Ignore]
        public ref class BaseClassAssignments
        {
        private:
            static BuiltinSupportSimulator *builtinSupport;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE *expectedSet, *providedValue;
        public:

            static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE expectedValue, EVAL_VAR_VALUE previousValue)
            {
                expectedSet = new EVAL_VAR_VALUE(expectedValue);
                builtinSupport = new BuiltinSupportSimulator();
                builtinSupport->GetParamValueReturnValue = PC_SUCCESS_EC;
                builtinSupport->GetParamValueCallbackValue = previousValue;
                builtinSupport->SetParamValueReturnValue = PC_SUCCESS_EC;

                engine = gcnew EddEngineController(eddFileName);
                engine->Initialize(nullptr, builtinSupport);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                engine->Instance->EndMethod(&asyncRunningMethod);
                providedValue = (EVAL_VAR_VALUE*)builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue");
            }

            static void CleanupBase()
            {
                if (builtinSupport) delete builtinSupport;
                if (expectedSet) delete expectedSet;
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_type()
            {
                Assert::AreEqual((int)expectedSet->type, (int)providedValue->type);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_size()
            {
                Assert::AreEqual(expectedSet->size, providedValue->size);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_value()
            {
                Assert::AreEqual((int)expectedSet->val.i, (int)providedValue->val.i); // ask for u because this test is meant for DATE/TIME etc.
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_assigns_a_DATE_variable_in_Profibus_Testcase_0014 : BaseClassAssignments
        {
        public:
            [TestInitialize]
            void Setup()
            {
                EVAL_VAR_VALUE expectedValue;
                expectedValue.type = VT_EDD_DATE;
                expectedValue.size = 3;
                expectedValue.val.i = 721522; //2014/2/11

                EVAL_VAR_VALUE initialValue;
                initialValue.type = VT_EDD_DATE;
                initialValue.size = 3;
                initialValue.val.i = 0x010101; //1901/1/1

                SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "simple_assign_of_date_values", expectedValue, initialValue);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_assigns_a_DATE_AND_TIME_variable_in_Profibus_Testcase_0014 : BaseClassAssignments
        {
        public:
            [TestInitialize]
            void Setup()
            {
                EVAL_VAR_VALUE expectedValue;
                expectedValue.type = VT_DATE_AND_TIME;
                expectedValue.size = 7;
                expectedValue.val.u = 49478152300986994; //2014/2/11 12:30:45.0

                EVAL_VAR_VALUE initialValue;
                initialValue.type = VT_DATE_AND_TIME;
                initialValue.size = 7;
                initialValue.val.u = 0x010101; // 1901/1/1 00:00:00.0

                SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "simple_assign_of_date_and_time_values", expectedValue, initialValue);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_assigns_a_TIME_VALUE_variable_in_Profibus_Testcase_0014 : BaseClassAssignments
        {
        public:
            [TestInitialize]
            void Setup()
            {
                EVAL_VAR_VALUE expectedValue;
                expectedValue.type = VT_TIME_VALUE;
                expectedValue.size = 8;
                expectedValue.val.i = 1441440000; // 1972/1/1 12:30:45.0

                EVAL_VAR_VALUE initialValue;
                initialValue.type = VT_TIME_VALUE;
                initialValue.size = 8;
                initialValue.val.i = 0; // 1972/1/1 00:00:00

                SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "simple_assign_of_time_value_values", expectedValue, initialValue);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }


        };

        [TestClass]
        public ref class When_calling_a_method_that_assigns_a_TIME_variable_in_Profibus_Testcase_0014 : BaseClassAssignments
        {
        public:
            [TestInitialize]
            void Setup()
            {

                EVAL_VAR_VALUE expectedValue;
                expectedValue.type = VT_TIME;
                expectedValue.size = 6;
                expectedValue.val.u = 2952069130999; // 2014/2/11 12:30:45.0

                EVAL_VAR_VALUE initialValue;
                initialValue.type = VT_TIME;
                initialValue.size = 6;
                initialValue.val.u = 0; // 1984/1/1 00:00:00.0
                SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "simple_assign_of_time_values", expectedValue, initialValue);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_calling_a_method_that_assigns_a_DURATION_variable_in_Profibus_Testcase_0014 : BaseClassAssignments
        {
        public:
            [TestInitialize]
            void Setup()
            {

                EVAL_VAR_VALUE expectedValue;
                expectedValue.type = VT_DURATION;
                expectedValue.size = 6;
                expectedValue.val.u = 2952069120005; // 5d 12h 30m 45sec 0msec

                EVAL_VAR_VALUE initialValue;
                initialValue.type = VT_DURATION;
                initialValue.size = 6;
                initialValue.val.u = 0; // 0d 0h 0m 0sec 0msec
                SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "simple_assign_of_duration_values", expectedValue, initialValue);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentDate_in_Testcase_0014 : BaseClassAssignments
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				struct tm tm_value;
				_gmtime32_s(&tm_value, &ltime);

				// current date
				long curr_date = tm_value.tm_yday * 24 * 60 * 60;

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_date;

				EVAL_VAR_VALUE initialValue;

				SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "meth_current_date", expectedValue, initialValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

		[TestClass, Ignore]
        public ref class BaseClassCurrentTime
        {
        private:
            static BuiltinSupportSimulator *builtinSupport;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE *expectedSet, *providedValue;
			static String^ theMethodName;

		public:
            static void SetupBase(String^ eddFileName, String^ methodName, EVAL_VAR_VALUE expectedValue)
            {
 				theMethodName = methodName;

				expectedSet = new EVAL_VAR_VALUE(expectedValue);
                builtinSupport = new BuiltinSupportSimulator();
                builtinSupport->SetParamValueReturnValue = PC_SUCCESS_EC;

                engine = gcnew EddEngineController(eddFileName);
                engine->Initialize(nullptr, builtinSupport);

                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;

                wchar_t* languageCode = L"en";
                ACTION methodAction; 
                methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
                methodAction.action.meth_ref = test_method_specifier.id;
                nsEDDEngine::IAsyncResult* asyncRunningMethod = engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                engine->Instance->EndMethod(&asyncRunningMethod);
                providedValue = (EVAL_VAR_VALUE*)builtinSupport->Recorder->GetParameter(L"SetParamValue.pValue");
            }

            static void CleanupBase()
            {
                if (builtinSupport) delete builtinSupport;
                if (engine) delete engine;
                if (expectedSet) delete expectedSet;
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_type()
            {
                Assert::AreEqual((int)expectedSet->type, (int)providedValue->type);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_size()
            {
                Assert::AreEqual(expectedSet->size, providedValue->size);
            }

            [TestMethod]
            void The_provided_value_for_SetParamValue_should_have_the_expected_value()
            {
				int expectedVal = (int)expectedSet->val.i;
				int returnedVal = (int)providedValue->val.i;
				bool bCheck = false;
				if (theMethodName == "meth_get_tick_acount")
				{
					bCheck = ((returnedVal - expectedVal) < 100);
				}
				else
				{
					bCheck = ((returnedVal - expectedVal) <= 1);
				}
				Assert::IsTrue(bCheck, ("DD test method " + theMethodName + " failed."));
            }
        };
		
		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentTime_in_Testcase_0014 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				struct tm tm_value;
				_gmtime32_s(&tm_value, &ltime);

				// current time in second
				long curr_time =  (tm_value.tm_hour * 60 + tm_value.tm_min) * 60 + tm_value.tm_sec;

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_time;

				SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "meth_current_time", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};
		
		[TestClass]
		public ref class When_calling_a_method_that_tests_GetCurrentDateAndTime_in_Testcase_0014 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				// current date_time
				long curr_date_time = make_time_t( ltime );

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_date_time;

				SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "meth_current_date_time", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

		[TestClass]
		public ref class When_calling_a_method_that_tests_From_TIME_VALUE_in_Testcase_0014 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
 				__time32_t	ltime;
				_time32( &ltime );

				/* calculation for result of From_TIME_VALUE builtin */
				unsigned long time_value = 0x290390;
				ltime += (__time32_t)((double)time_value / 32000);

				// current time_value
				long curr_time_val = make_time_t( ltime );

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = curr_time_val;

				SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "meth_from_time_value", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

		[TestClass]
		public ref class When_calling_a_method_that_tests_GET_TICK_COUNT_in_Testcase_0014 : BaseClassCurrentTime
		{
		public:
			[TestInitialize]
			void Setup()
			{
				// current tick_count in millisecond
				long tick_count = GetTickCount();

				EVAL_VAR_VALUE expectedValue;
				expectedValue.type = VT_INTEGER;
				expectedValue.size = 4;
				expectedValue.val.i = tick_count;

				SetupBase("ProfiBus\\FDI_PB_Test_0014.DDL.bin", "meth_get_tick_acount", expectedValue);
			}

			[TestCleanup]
			void Cleanup()
			{
				CleanupBase();
			}
		};

    }
}
