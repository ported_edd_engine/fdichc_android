/*
 * @(#) $Id: ddi_tab.c,v 1.2 1996/01/27 03:29:16 miked Exp $
 */

/************************************************************
 * Includes
 ************************************************************/


#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
#pragma warning (disable : 4131)

/***********************************************************************
 *
 *  Name:  compare_bt_elem
 *  ShortDesc:  Compare block names of two blocks in the Block Table.
 *
 *  Description:
 *      The compare_bt_elem function takes in two pointers to Block
 *      Table element structures in a Block Table structure
 *      and compares their item ID's.  The value returned is
 *		reflective of the difference of the two item ID's.  This
 *      function is used in conjunction with the bsearch function
 *      (for searching the Block Table).
 *
 *  Inputs:
 *      bt_elem_1 - pointer to a Block Table element structure
 *					in a Block Table structure.
 *      bt_elem_2 - pointer to another Block Table element
 *					structure in a Block Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		 1
 *		 0
 *		-1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
compare_bt_elem(
BLK_TBL_ELEM    *bt_elem_1,
BLK_TBL_ELEM    *bt_elem_2)
{
	/*
	 *	Compare the two item ID's of the blocks and return the
	 *	reflective difference.
	 */
	
	if (BTE_BLK_ID(bt_elem_1) < BTE_BLK_ID(bt_elem_2)) {
		return(-1);
	}
	else if (BTE_BLK_ID(bt_elem_1) > BTE_BLK_ID(bt_elem_2)) {
		return(1);
	}
	else {
		return(0);
	}
}

/***********************************************************************
 *
 *  Name:  compare_ptoc_t8_elem
 *  ShortDesc:  Compare item ids of two ptoc table 8 elements in the Command Table 8.
 *
 *  Description:
 *      The compare_ptoc_t8_elem function takes in two pointers to PTOC
 *      Table 8 element structures in a Command Table 8 structure
 *      and compares their item ID's.  The value returned is
 *		reflective of the difference of the two item ID's.  This
 *      function is used in conjunction with the bsearch function
 *      (for searching the Command Table 8).
 *
 *  Inputs:
 *      ptoc_t8_elem_1 - pointer to a PTOC Table 8 element structure
 *					in a COMMAND Table 8 structure.
 *      ptoc_t8_elem_2 - pointer to another PTOC Table 8 element
 *					structure in a  COMMAND Table 8 structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		 1
 *		 0
 *		-1
 *
 *  Author:
 *      Ying Xu
 *
 **********************************************************************/

int
compare_ptoc_t8_elem(
PTOC_TBL_8_ELEM    *ptoc_t8_elem_1,
PTOC_TBL_8_ELEM    *ptoc_t8_elem_2)
{
	/*
	 *	Compare the two item ID's of the PTOC_TBL_8_ELEM and return the
	 *	reflective difference.
	 */
	
	if (ptoc_t8_elem_1->item_id < ptoc_t8_elem_2->item_id)
	{
		return(-1);
	}
	else if (ptoc_t8_elem_1->item_id > ptoc_t8_elem_2->item_id)
	{
		return(1);
	}
	else 
	{
		return(0);
	}
}	


/***********************************************************************
 *
 *  Name:  compare_it_elem
 *  ShortDesc:  Compare item ids of two items in the Item Table.
 *
 *  Description:
 *      The compare_it_elem function takes in two pointers to Item
 *      Table element structures in an Item Table structure
 *      and compares their Item ID's.  The value returned is
 *		reflective of the comparison of the two Item ID's.
 *      This function is used in conjunction with the bsearch
 *		function (for searching the Item Table).
 *
 *  Inputs:
 *      it_elem_1 - pointer to an Item Table element structure in an
 *					Item Table structure.
 *      it_elem_2 - pointer to another Item Table element structure
 *					in an Item Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_it_elem(
ITEM_TBL_ELEM		*it_elem_1,
ITEM_TBL_ELEM		*it_elem_2)
{
	/*
	 *	Compare the two Item ID's and return the reflective value.
	 */

	if (ITE_ITEM_ID(it_elem_1) <
			ITE_ITEM_ID(it_elem_2)) {
		return(-1);
	}

	else if (ITE_ITEM_ID(it_elem_1) >
			ITE_ITEM_ID(it_elem_2)) {
		return(1);
	}
	else {
		return(0);
	}
}

/***********************************************************************
 *
 *  Name:  find_it_elem
 *  ShortDesc:  Find an element in the Item Table using an Item ID.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the Item Table.  The function compare_it_elem
 *				is used to compare elements of the Item Table.
 *				If an element (containing the desired Item ID) is found,
 *				it is returned to the caller.
 *
 *  Inputs:
 *      it - pointer to a Item Table structure to search for the
 *			 Item ID.
 *      item_id - the Item ID to find in the Item Table.
 *
 *  Outputs:
 *      it_elem - a pointer to the Item Table element that contains
 *				  the requested Item ID.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_DDID.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
find_it_elem( ENV_INFO* env_info,
ITEM_TBL		*it,
ITEM_ID			item_id,
ITEM_TBL_ELEM	**it_elem)
{
	int			assert_conds ;
	assert_conds = it && item_id && it_elem && IT_LIST(it) &&
				IT_COUNT(it) ;
	//ASSERT_RET(assert_conds,DDI_INVALID_PARAM) ;
	if (!assert_conds) // See COMqa14860
	{
		//COMqa23055  DoDebugBreak() could terminate file server
		//DoDebugBreak();
		EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"LegacyHART", L"Invalid parameter");
		return DDI_INVALID_PARAM;
	}

	/*
	 *	Find the Item Table element containing the Item ID.
	 */


	*it_elem = (ITEM_TBL_ELEM *) bsearch(
		(char *)&item_id,
		(char *)IT_LIST(it),
		(unsigned int)IT_COUNT(it),
		sizeof(ITEM_TBL_ELEM),
		(CMP_FN_PTR)compare_it_elem);

	/*
	 *	Determine if the Item Id Was Found.
	 */

	if (*it_elem == 0) {
		return(DDI_TAB_BAD_DDID);
	}

	return(DDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name:  compare_pet_elem
 *  ShortDesc:  Compare Subindexes of two items in the Parameter Element
 *              Table.
 *
 *  Description:
 *      The compare_pet_elem function takes in two pointers to 
 *		Parameter Element Table element structures in a Parameter
 *		Element Table structure  and compares their subindexes.
 *      The value returned is reflective of the comparison of the 
 *		two subindexes.  This function is used in conjunction with
 *		the bsearch function (for searching the Parameter Element 
 *		Table).
 *
 *  Inputs:
 *      pet_elem_1 - pointer to a Parameter Element Table element 
 *					structure in a Parameter Element Table structure.
 *      pet_elem_2 - pointer to a Parameter Element Table element 
 *					structure in a Parameter Element Table structure.
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
compare_pet_elem(
PARAM_ELEM_TBL_ELEM	*pet_elem_1,
PARAM_ELEM_TBL_ELEM	*pet_elem_2)
{
	/*
	 *	Compare the two Subindexes and return the reflective value.
	 */

	if (PETE_PE_SUBINDEX(pet_elem_1) <
			PETE_PE_SUBINDEX(pet_elem_2)) {
		return(-1);
	}

	else if (PETE_PE_SUBINDEX(pet_elem_1) >
			PETE_PE_SUBINDEX(pet_elem_2)) {
		return(1);
	}

	else {
		return(0);
	}
}


/***********************************************************************
 *
 *  Name:  find_pet_elem
 *  ShortDesc:  Find an element in the Parameter Element Table using a
 *				(array) parameter element subindex.
 *
 *  Description:
 *				The function uses a binary search to find an element in
 *				the parameter element Table.  The function 
 *				compare_pet_elem is used to compare elements of the 
 *				parameter element table.  If an element 
 *				(containing the desired subindex) is found, it is 
 * 				returned to the caller.
 *
 *  Inputs:
 *      pet 	- pointer to a Parameter Element Table structure to 
 *				  search for the parameter (array) element subindex.
 *     	pt_elem - a pointer to the Parameter Table element
 *				    that contains the parameter information.
 *      subindex - the subindex parameter element
 *
 *  Outputs:
 *     	pet_elem - a pointer to the Parameter Element Table element
 *				    that contains the parameter element subindex.
 *
 *  Returns:
 *      DDS_SUCCESS
 *		DDI_TAB_BAD_NAME.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
find_pet_elem(
PARAM_ELEM_TBL				*pet,
PARAM_TBL_ELEM				*pt_elem,
SUBINDEX					subindex,
PARAM_ELEM_TBL_ELEM			**pet_elem)
{
	char					*list_start ;

	//Unit relationships are optional, so if it is not found, simply return DDI_TAB_BAD_SUBINDEX 
	if ((pet && pet_elem && PET_LIST(pet) && PET_COUNT(pet)) == false)
	{
		return (DDI_TAB_BAD_SUBINDEX);
	}

	/*
	 * Check the Number of Members in the Parameter Record
	 */

	if (PTE_PE_COUNT(pt_elem) <= 0) {
		return(DDI_TAB_BAD_SUBINDEX) ;
	}	

	/*
	 * Calculate the beginning of the member name of the requested
	 * parameter
	 */

	list_start = (char *)&(PET_LIST(pet)[PTE_PET_OFFSET(pt_elem)]) ; 

	/*
	 *	Find the Parameter Element Table element Containing the 
	 *	requested parameter array element subindex
	 */

	*pet_elem = (PARAM_ELEM_TBL_ELEM *) bsearch(
		(char *)&subindex,
		list_start,
		(unsigned int)PTE_PE_COUNT(pt_elem),
		sizeof(PARAM_ELEM_TBL_ELEM),
		(CMP_FN_PTR)compare_pet_elem);

	/*
	 *	Determine if the parameter array element subindex was
	 *	found.
	 */

	if (*pet_elem == 0) {
		return(DDI_TAB_BAD_SUBINDEX);
	}
	return(DDS_SUCCESS);
}


/*********************************************************************
 *
 *	Name: tr_id_si_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_id_si_to_bint_offset finds the Block Item Name Table offset
 *		for the given bt_elem item_id and subindex
 *
 *	Inputs:
 *		bt_elem:	The Block Table element
 *		subindex:	The subindex of the item_id
 *		item_id:	The item_id to obtain the bint_offset for
 *
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_id_si_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
SUBINDEX		subindex,
ITEM_ID			item_id,
int				*bint_offset)
{
	BLK_ITEM_TBL_ELEM			*bit_elem;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							rc;
	int							param_offset;

	 /*	
	  *	Check the parameter offset to see if it is in range.
	  *	Find the Block Item Table element that corresponds to the
	  *	item id.
	  */

	rc = find_bit_elem(BTE_BIT(bt_elem), item_id, &bit_elem);

	if (rc != SUCCESS) {
		return(rc);
	}

	/*
	 *	Calculate the parameter offset.
	 */

	bint_elem = BINTE(BTE_BINT(bt_elem),
			BITE_BINT_OFFSET(bit_elem));
	param_offset = BINTE_PT_OFFSET(bint_elem);

	/*
	 * Convert the parameter offset and subindex in a block
	 * item name table offset
	 */

	rc = tr_param_si_to_bint_offset(bt_elem,subindex,
		param_offset,bint_offset);
	return rc;
}

/*********************************************************************
 *
 *	Name: tr_param_si_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_param_si_to_bint_offset finds the Block Item Name Table
 *		offset for the given bt_elem param and subindex
 *
 *	Inputs:
 *		bt_elem:	The Block Table element
 *		subindex:	The subindex of the param
 *		param_offset:		The param to obtain the bint_offset for
 *
 *
 *	Outputs:
 *		bint_offset:		a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_param_si_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
SUBINDEX		subindex,
int				param_offset,
int				*bint_offset)
{
	PARAM_TBL_ELEM				*pt_elem;
	int							rc;
	int							param_mem_offset;

	/*
	 *	Check the parameter offset to see if it is in range.
	 */

	rc = CHECK_PARAM_OFFSET(BTE_PT(bt_elem),param_offset);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	/*
	 *	Convert the subindex to a parameter table offset
	 */

	param_mem_offset = PARAM_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex);

	/*
	 *	Check the subindex to see if it is in range.
	 */


	pt_elem = PTE(BTE_PT(bt_elem),param_offset);

	rc = CHECK_PARAM_MEMBER_OFFSET(pt_elem, param_mem_offset);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	*bint_offset = PTE_BINT_OFFSET(pt_elem);

	return(DDS_SUCCESS);
}



/***********************************************************************
 *
 *  Name: tr_gdr_block_handle
 *  ShortDesc:  Convert a block_handle into a block directory dd
 *				reference.
 *
 *  Description:
 *		The function first converts the block handle into a device type
 *		handle.  Second, the function gets the blockname that is
 *		associated with the block_handle.  Finally, the function
 *		tr_gdr_device_type_handle_blockname is called to
 *		convert device_type_handle and blockname into the dd reference
 *		of the requested block directory.
 *
 *  Inputs:
 *      block_handle - handle of the block that contains the requested
 *                     block directory.
 *  Outputs:
 *      dd_ref - the dd reference of the requested block directory.
 *
 *  Returns:
 *      DDI_TAB_NO_BLOCK.
 *      Return values from
 *			tr_gdr_device_type_handle_blockname function.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_gdr_block_handle(
ENV_INFO *env_info,
BLOCK_HANDLE	block_handle,
DD_REFERENCE	*dd_ref)
{
	DEVICE_TYPE_HANDLE		device_type_handle;
	int						rs;
	ITEM_ID					block_name;

	/*
	 *	Get the device type handle.
	 */
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rs = pConnMgr->get_abt_adtt_offset(block_handle, &device_type_handle);
	if (rs != SUCCESS) {
		return(rs) ;
	}

	/*
	 *	Get the block name of the selected block.
	 */

	rs = pConnMgr->get_abt_dd_blk_id(block_handle, &block_name);
	if (rs != SUCCESS) {
		return(rs) ;
	}

	if (block_name == 0) {
		return(DDI_TAB_NO_BLOCK);
	}

	/*
	 *	Get the dd reference of the block directory of the selected
	 *	block.
	 */

	rs = tr_gdr_device_type_handle_blockname( env_info,
			device_type_handle,block_name,dd_ref);
	return(rs);
}


/***********************************************************************
 *
 *  Name: tr_gdr_device_type_handle_blockname
 *  ShortDesc: Convert a device_type_handle and block name into a block 
 *				directory dd reference.
 *
 *  Description:
 *		First, get the DDS Tables from the Device Type Table.  Second,
 *		get the block_name_table from the DDS Tables (FLAT_DEVICE_DIR).
 *		Third, search the Block Table for the requested blockname.
 *		Finally, if the block name is found return the corresponding
 *		dd reference for the block directory (and DDS_SUCCESS).
 *		If the block name is not found, return DDI_TAB_NO_BLOCK.
 *
 *  Inputs:
 *      device_type_handle - handle of the device type that contains
 *						 	 the block directory.
 *      block_name - The reference to dd by a particular block.  The
 *					 blockname specifies the which block directory to
 *					 get from the requested device type.
 *  Outputs:
 *      dd_ref - dd reference of the requested block directory.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *	Includes:
 *		search.h
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_gdr_device_type_handle_blockname(
ENV_INFO				* /* env_info */,
DEVICE_TYPE_HANDLE		device_type_handle,
ITEM_ID					block_name,
DD_REFERENCE			*dd_ref)
{
    BLK_TBL			*bt;
	BLK_TBL_ELEM    *bt_elem;
	FLAT_DEVICE_DIR	*flat_device_dir ;
	int				rs ;

	/*
	 *	Get the Block Table From the DDS Tables.
	 */

	rs = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(device_type_handle, 
			(void **)&flat_device_dir) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}
	bt = &flat_device_dir->blk_tbl;

	/*
	 *	Find the Block Table element containing the block name.
	 */

	bt_elem = (BLK_TBL_ELEM *) bsearch(
		(char *)&block_name,
		(char *)BT_LIST(bt),
		(unsigned int)BT_COUNT(bt),
		sizeof(BLK_TBL_ELEM),
		(CMP_FN_PTR)compare_bt_elem);

	/*
	 *	Determine if the Block Name Was Found.
	 */

	if (bt_elem == 0) {
		return(DDI_TAB_NO_BLOCK);
	}
	*dd_ref = BTE_BD_DD_REF(bt_elem);
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_block_to_ite
 *  ShortDesc:  Convert a block_handle into the corresponding item
 *				table element pointer 
 *
 *  Description:
 *		1) Get the item offset of the block table element.
 *		2) Calculate the item table element pointer.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *		bt_elem - block table element containing the requested block
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested block.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_block_to_ite(
ITEM_TBL		*it,
BLK_TBL_ELEM	*bt_elem,
ITEM_TBL_ELEM	**it_elem)
{

	/*
	 *	Get the Item Table location from the bt_elem.
	 */

	*it_elem = ITE(it,BTE_IT_OFFSET(bt_elem));

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_id_to_ite
 *  ShortDesc:  Convert a block_handle and Item ID into the
 *				corresponding item table element pointer.
 *
 *  Description:
 *				1.	Find the Item Table element (it_elem) of the
 *					specified Item ID.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *      item_id - The item id to that specifies thedd reference. 
 *
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested item.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_id_to_ite(ENV_INFO* env_info,
ITEM_TBL			*it,
ITEM_ID				item_id,
ITEM_TBL_ELEM		**it_elem)
{
	int					rs;

	/*
	 *	Find the Item Table element that corresponds to the block
	 *	handle.
	 */

	rs = find_it_elem(env_info, it,item_id,it_elem);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	return(DDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: tr_name_to_ite
 *  ShortDesc: 	Convert a block_handle and parameter name into a  
 *				item table element pointer.
 *
 *  Description:
 *				1.	Get the Block Item Name Table (bint) from the
 *					bt_elem and search the Block Item Name Table for the
 *					specified name.
 *				2.	Use the Block Item Name Table element pointer (from
 *					2) to get the Item Table offset of the item with the
 *					specified name.
 *				3.	Form an Item Table element pointer using the Item
 *					Table pointer and the Item Table offset.
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *      name - The name of a name to convert to the dd reference of the
 *			   corresponding variable or record.
 *		bt_elem - block table element.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested name.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_name_to_ite(
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
ITEM_ID				name,
ITEM_TBL_ELEM		**it_elem)
{
	BLK_ITEM_NAME_TBL			*bint;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							rs;

	/*
	 *	Find the bint element that corresponds to the parameter name.
	 */

	bint = BTE_BINT(bt_elem);

	rs = find_bint_elem(bint, name,	&bint_elem);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Get the Item Table location from the bint element.
	 */

	*it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  tr_param_offset_to_op_index
 *
 *	ShortDesc:  Convert a parameter offset to an operational index.
 *
 *	Description:
 *		The tr_param_offset_to_op_index function takes a block handle
 *		and offset of a parameter and calculates the corresponding
 *		operational index.
 *
 *	Inputs:
 *		block_handle - handle of the block that contains the parameter.
 *		parameter_offset - offset of the parameter.
 *
 *	Outputs:
 *		operational_index - operational index of the parameter.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_BLOCK_HANDLE.
 *		DDI_TAB_BAD_PARAM_OFFSET.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
tr_param_offset_to_op_index(
ENV_INFO		*env_info,
BLOCK_HANDLE	 block_handle,
int				 parameter_offset,
OBJECT_INDEX	*operational_index)
{
	unsigned int	param_count ;
	OBJECT_INDEX	oi ;
	int				rs ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!pConnMgr->valid_block_handle(block_handle)) {
		return(DDI_INVALID_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that the parameter offset is valid.
	 */

	rs = pConnMgr->get_abt_param_count(block_handle, &param_count) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}
	if ((int)param_count < 0) {
		return(DDI_TAB_BAD_PARAM_COUNT);
	}
	if ((parameter_offset + 1 > (int)param_count) ||
			(parameter_offset + 1 <= 0)) {
		return(DDI_TAB_BAD_PARAM_OFFSET);
	}

	/*
	 *	Calculate the operational index.
	 */

	rs = pConnMgr->get_abt_op_index(block_handle, &oi);
	if (rs != SUCCESS) {
		return(rs) ;
	}
	*operational_index = (OBJECT_INDEX) (oi + 1 + parameter_offset);

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  tr_op_index_to_param_offset
 *
 *	ShortDesc:  Convert an operational index into a parameter offset.
 *
 *	Description:
 *		The tr_op_index_to_param_offset function takes a block handle
 *		and operational index of a parameter and calculates the
 *		corresponding parameter offset.
 *
 *	Inputs:
 *		block_handle - handle of the block that contains the parameter.
 *		operational_index - operational index of the parameter.
 *
 *	Outputs:
 *		parameter_offset - offset of the parameter.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_BLOCK_HANDLE.
 *		DDI_TAB_BAD_OP_INDEX.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
tr_op_index_to_param_offset(
ENV_INFO		*env_info,
BLOCK_HANDLE	 block_handle,
OBJECT_INDEX	 operational_index,
int				*parameter_offset)
{
	int				temp_offset;
	OBJECT_INDEX	oi ;
	int				rs ;
	unsigned int	param_count ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!pConnMgr->valid_block_handle(block_handle)) {
		return(DDI_INVALID_BLOCK_HANDLE);
	}

	/*
	 *	Calculate the parameter offset.
	 */

	rs = pConnMgr->get_abt_op_index(block_handle, &oi) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}
	temp_offset = (int) (operational_index - oi - 1);

	/*
	 *	Check to make sure that the parameter offset is valid.
	 */

	rs = pConnMgr->get_abt_param_count(block_handle, &param_count) ; 
	if (rs != SUCCESS) {
		return(rs) ;
	}
	if ((int)param_count < 0) {
		return(DDI_TAB_BAD_PARAM_COUNT);
	}
	if ((temp_offset + 1 > (int)param_count) || 
			(temp_offset + 1 <= 0)) {
		return(DDI_TAB_BAD_OP_INDEX);
	}

	*parameter_offset = temp_offset;

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_op_index_to_ite
 *  ShortDesc: 	Convert a block_handle and operational index into a 
 *		item table element ponter.
 *
 *  Description:
 *		1) convert op_index to param offset.
 *		2) Get an item table element pointer for the parameter.
 *
 *  Inputs:
 *		block_handle - the block handle
 *      it - 	item table pointer that contains the requested item
 *      op_index - The object index to convert to the dd reference of
 *				   the corresponding variable or record.
 *		bt_elem - block table element.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_op_index_to_ite(
ENV_INFO			*env_info,
BLOCK_HANDLE		block_handle,
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
OBJECT_INDEX		op_index,
ITEM_TBL_ELEM		**it_elem)
{
	int							param_offset;			/* temp param */
	int							rc;				/* return code */

	/*
	 *	Convert the op index to a parameter offset.
	 */

	rc = tr_op_index_to_param_offset(env_info, block_handle, op_index, &param_offset);
	if (rc != DDS_SUCCESS) {

		return(rc);
	}

	/*
	 *	Get the Item Table location using the param to item 
	 *	table element function
	 */

	rc = tr_param_to_ite(it,bt_elem,param_offset,it_elem) ;

	return(rc);
}


/***********************************************************************
 *
 *  Name: tr_param_to_ite
 *  ShortDesc: Convert a block_handle and offset into the list of
 *				parameters into an item table element ponter 
 *
 *  Description:
 *				1.	Check the parameter offset passed in using the 
 *					Parameter Table pointer (from bt_elem).
 *				2.	Form a Parameter Table element using Parameter Table
 *					pointer and the parameter offset.
 *				3.	Form a Block Item Name Table pointer using the Block
 *					Item Name Table pointer (from bt_elem) and the
 *					Block Item Name Table offset (from pt_element).
 *				4.	Form an Item Table element using the Item Table
 *					pointer	and the Item Table offset (from bint_elem).
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *		bt_elem - block table element.
 *      param_offset - The offset into the list of parameters of the
 *					   requested block. 
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_param_to_ite(
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
int					param_offset,
ITEM_TBL_ELEM		**it_elem)
{
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	PARAM_TBL_ELEM				*pt_elem;
	int							rs;

	/*
	 *	Check the parameter offset to see if it is in range.
	 */

	rs = CHECK_PARAM_OFFSET(BTE_PT(bt_elem),param_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Get the Item Table location from the param offset and bt_elem.
	 */

	pt_elem = PTE(BTE_PT(bt_elem),param_offset);
	bint_elem = BINTE(BTE_BINT(bt_elem),
			PTE_BINT_OFFSET(pt_elem));
	*it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_param_list_to_ite
 *  ShortDesc:  Convert a block_handle and offset into the list of
 *				parameter lists (of the block) into an item table
 *				element pointer.
 *
 *  Description:
 *				1.	Check the parameter list offset passed in using the 
 *					Parameter Table pointer (from bt_elem).
 *				2.	Form a Parameter List Table element using Parameter
 *					Table pointer and the parameter offset.
 *				3.	Form a Block Item Name Table pointer using the Block
 *					Item Name Table pointer (from bt_elem) and the
 *					Block Item Name Table offset (from plt_elem).
 *				4.	Form an Item Table element using the Item Table
 *					pointer and the Item Table offset
 *					(from bint_elem).
 *
 *  Inputs:
 *		bt_elem - block table element.
 *      it - 	item table pointer that contains the requested item
 *      param_list_offset - The offset into the list of parameter lists
 *							of the requested block.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_param_list_to_ite(
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
int					param_list_offset,
ITEM_TBL_ELEM		**it_elem)
{
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	PARAM_LIST_TBL_ELEM			*plt_elem;
	int							rs;

	/*
	 *	Check the parameter list offset to see if it is in range.
	 */

	rs = CHECK_PARAM_LIST_OFFSET(BTE_PLT(bt_elem),param_list_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Get the Item Table location from the param offset and bt_elem.
	 */

	plt_elem = PLTE(BTE_PLT(bt_elem),param_list_offset);
	bint_elem = BINTE(BTE_BINT(bt_elem),
			PLTE_BINT_OFFSET(plt_elem));
	*it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));

	return(DDS_SUCCESS);
}



/***********************************************************************
 *
 *  Name: tr_name_si_to_ite
 *  ShortDesc:  Convert a block_handle, parameter name, and subindex
 *				into the item table element pointer of the 
 *				corresponding variable.
 *
 *  Description:
 *				1.	Find the name in the Block Item Name Table. 
 *				2.	Create a param_offset from the Block Item Name Table 
 *					element pointer.  Convert the parameter offset and
 *					and subindex to an item table element pointer
 *				3.	Create an Item Table element using the Item Table
 *					pointer and the Item Table offset (from bint_elem).
 *
 *  Inputs:
 *      it - 	item table pointer that contains the requested item
 *		bt_elem - block table element.
 *		name		 - the name of a parameter or parameter list in the
 *					   specified block.
 *		subindex	 - the offset of the member of a parameter that is a
 *					   record or the offset of the parameter in a
 *					   parameter list.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_name_si_to_ite(
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
ITEM_ID				name,
SUBINDEX			subindex,
ITEM_TBL_ELEM		**it_elem)
{
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							rs;
	int							param_offset;

	/*
	 *	Find the name in the Block Item Name Table.
	 */

	rs = find_bint_elem(BTE_BINT(bt_elem),name,&bint_elem);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}

	/*
	 *	Calculate the parameter offset and convert if it is in range.
	 */

	param_offset = BINTE_PT_OFFSET(bint_elem);
	rs = tr_param_si_to_ite(it,bt_elem,param_offset,subindex,it_elem) ;
	return(rs);
}


/***********************************************************************
 *
 *  Name: tr_op_index_si_to_ite
 *  ShortDesc:  Convert a block_handle, operational object index, and
 *				subindex into a item table element pointer for 
 *				the corresponding variable.
 *
 *  Description:
 *		1) Convert op_index to param offset
 *		2) Get an item table element pointer using param offset and 
 *		   subindex
 *
 *  Inputs:
 *		block_handle - the block handle
 *      it - 	item table pointer that contains the requested item
 *		bt_elem - block table element.
 *      op index - The operational object index of parameter of the
 *				   requested block.
 *		subindex - The offset into the list of member for the record
 *				   parameter requested.
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested op index.
 *
 *
 *  Returns:
 *			DDS_SUCCESS
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_op_index_si_to_ite(
ENV_INFO			*env_info,
BLOCK_HANDLE		block_handle,
ITEM_TBL			*it,
BLK_TBL_ELEM		*bt_elem,
OBJECT_INDEX		op_index,
SUBINDEX			subindex,
ITEM_TBL_ELEM		**it_elem)
{
	int							rs;
	int							param_offset;

	/*
	 *	Calculate the parameter offset and see if it is in range.
	 */

	rs = tr_op_index_to_param_offset(env_info, block_handle, op_index, &param_offset);
	if (rs != DDS_SUCCESS) {

		return DDI_TAB_BAD_OP_INDEX;
	}
	/*
	 *	Use the parameter offset and convert it
	 */

	rs = tr_param_si_to_ite(it,bt_elem,param_offset,subindex,it_elem) ;
	return(rs);
}



/*********************************************************************
 *
 *	Name: tr_block_tag_to_block_handle
 *	ShortDesc: shell for a conman call
 *
 *	Description:
 *		 tr_block_tag_to_block_handle is a shell for a connection
 *		 manager call
 *
 *	Inputs:
 *		tag:	the block tag
 *
 *	Outputs:
 *		handle:	the loaded block handle
 *
 *	Returns:
 *		DDI_INVALID_BLOCK_TAG, DDS_SUCCESS
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_block_tag_to_block_handle(
ENV_INFO		*env_info,
BLOCK_TAG		tag,
BLOCK_HANDLE	*handle)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

    *handle = pConnMgr->ct_block_search((LPTSTR)tag);

	if(*handle < 0) {

		return DDI_INVALID_BLOCK_TAG;
	}
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: tr_id_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_id_to_bint_offset finds the Block Item Name Table offset
 *		for the given block handle and item id
 *
 *	Inputs:
 *		bt_elem:	The Block Table element
 *		item_id:	The item to obtain the bint_offset for
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_id_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
ITEM_ID			item_id,
int				*bint_offset)
{

	BLK_ITEM_TBL		*bit;
	BLK_ITEM_TBL_ELEM	*bit_elem;
	int					rc;

	/*
	 *	Find the bit element that corresponds to the Item ID.
	 */

	bit = BTE_BIT(bt_elem);

	rc = find_bit_elem(bit, item_id, &bit_elem);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	*bint_offset = BITE_BINT_OFFSET(bit_elem);

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: tr_name_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_name_to_bint_offset finds the Block Item Name Table offset
 *		for the given block handle and name
 *
 *	Inputs:
 *		bt_elem:	The Block Table element
 *		name:		The name to obtain the bint_offset for
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_name_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
ITEM_ID			name,
int				*bint_offset)
{
	BLK_ITEM_NAME_TBL			*bint;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							rc;

	bint = BTE_BINT(bt_elem);

	rc = find_bint_elem(bint, name, &bint_elem);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	*bint_offset = (int) (bint_elem - bint->list);

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: tr_param_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_param_to_bint_offset finds the Block Item Name Table offset
 *		for the given block handle and param
 *
 *	Inputs:
 *		bt_elemt:	The Block Table element
 *		param_offset:		The param to obtain the bint_offset for
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_param_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
int				param_offset,
int				*bint_offset)
{
	PARAM_TBL				*pt;
	PARAM_TBL_ELEM			*pt_elem;
	int						rc;

	pt = BTE_PT(bt_elem);

	/*
	 *	Check the parameter offset to see if it is in range.
	 */

	rc = CHECK_PARAM_OFFSET(pt, param_offset);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	pt_elem = PTE(pt, param_offset);

	*bint_offset = PTE_BINT_OFFSET(pt_elem);

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: tr_op_index_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_op_index_to_bint_offset finds the Block Item Name Table
 *		offset for the given block handle and op_index
 *
 *	Inputs:
 *		block_handle:	The block handle
 *		bt_elem:		The Block Table element
 *		op_index:		The op_index to obtain the bint_offset for
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_op_index_to_bint_offset(
ENV_INFO			*env_info,
BLOCK_HANDLE		block_handle,
BLK_TBL_ELEM		*bt_elem,
OBJECT_INDEX		op_index,
int					*bint_offset)
{
	int				param_offset;		/* temp param */
	int				rc;			/* return code */
	

	rc = tr_op_index_to_param_offset(env_info, block_handle, op_index, &param_offset);
	if (rc != DDS_SUCCESS) {

		return DDI_TAB_BAD_OP_INDEX;
	}

	rc = tr_param_to_bint_offset(bt_elem, param_offset, bint_offset);

	return rc;
}


/*********************************************************************
 *
 *	Name: tr_op_index_si_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_op_index_si_to_bint_offset finds the Block Item Name Table
 *		offset for the given block handle, op_index and subindex
 *
 *	Inputs:
 *		block_handle:	The block handle
 *		bt_elem:	The Block Table element
 *		op_index:	The op_index to obtain the bint_offset for
 *              subindex:       The subindex      
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
tr_op_index_si_to_bint_offset(
ENV_INFO			*env_info, 
BLOCK_HANDLE		block_handle,
BLK_TBL_ELEM		*bt_elem,
OBJECT_INDEX		op_index,
int			*bint_offset,
SUBINDEX                subindex)
{
	int				param_offset;		/* temp param */
	int				rc;			/* return code */
	
	rc = tr_op_index_to_param_offset(env_info, block_handle, op_index, &param_offset);
	if (rc != DDS_SUCCESS) {

		return DDI_TAB_BAD_OP_INDEX;
	}

	rc = tr_param_si_to_bint_offset(bt_elem, subindex, param_offset, bint_offset);

	return rc;
}


/***********************************************************************
 *
 *  Name: tr_char_to_bint_offset
 *  ShortDesc:  Convert a block_handle into the corresponding block item
 *				table offset
 *
 *  Description:
 *		1) Get the block item table offset from the block table
 *
 *  Inputs:
 *     
 *	bt_elem - block table element containing the requested block
 *  Outputs:
 *      it_elem - item table element containing the
 *					dd reference of the requested block.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_char_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
int		*bint_offset)
{

	/*
	 *	Get the block Item name Table location from the bt_elem.
	 */

	*bint_offset = BTE_CR_BINT_OFFSET(bt_elem);

	return(DDS_SUCCESS);
}



/*********************************************************************
 *
 *	Name: tr_name_si_to_bint_offset
 *	ShortDesc: find Block Item Name Table offset
 *
 *	Description:
 *		tr_name_si_to_bint_offset finds the Block Item Name Table offset
 *		for the given bt_elem name and subindex
 *
 *	Inputs:
 *		bt_elem:	The Block Table element
 *		subindex:	The subindex of the name
 *		name:		The name to obtain the bint_offset for
 *
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
tr_name_si_to_bint_offset(
BLK_TBL_ELEM	*bt_elem,
SUBINDEX		subindex,
ITEM_ID			name,
int				*bint_offset)
{
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	int							param_offset;
	int							rc;


	/*
	 *	Find the name in the Block Item Name Table.
	 */

	rc = find_bint_elem(BTE_BINT(bt_elem), name, &bint_elem);
	if (rc != DDS_SUCCESS) {
		return(rc);
	}

	/*
	 *	Calculate the parameter offset.
	 */

	param_offset = BINTE_PT_OFFSET(bint_elem);

	/*
	 *	Convert the parameter offset into a block item name
	 *	table offset
	 */

	rc = tr_param_si_to_bint_offset(bt_elem,subindex,
		param_offset,bint_offset);
	return rc;
}


/*********************************************************************
 *
 *	Name: tr_bint_offset_to_rt_offset
 *	ShortDesc: find Relation Table offset for a Block Item
 * 				Name Table Offset
 *
 *	Description:
 *		tr_bint_offset_to_rt_offset finds the Relation Table Offset 
 *		offset for the given Block Item Name Table Offset and block
 *		Table Element.
 *
 *	Inputs:
 *		bt_elem:		The Block Table element
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Outputs:
 *		rt_offset:		The Relation Table offset desired 
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Kent Anderson
 *
 **********************************************************************/

int
tr_bint_offset_to_rt_offset(
BLK_TBL_ELEM		*bt_elem,
int					bint_offset,
int					*rt_offset)
{

	*rt_offset = BINTE_RT_OFFSET(BINTE(BTE_BINT(bt_elem),bint_offset)) ;

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: tr_bint_si_offset_si_to_rt_offset
 *	ShortDesc: find Relation Table offset
 *
 *	Description:
 *		tr_bint_offset_to_rt_offset finds the Relation Table
 *		offset for the given block item name table offset and
 *		subindex.
 *
 *	Inputs:
 *		bt_elem:		The Block Table element
 *		subindex:		The op_index to obtain the bint_offset for
 *
 *	Outputs:
 *		bint_offset:	a pointer to the bint_offset
 *
 *	Returns:
 *		DDS_SUCCESS and returns from find_bt_elem()
 *
 *	Author: Kent Anderson
 *
 **********************************************************************/

int
tr_bint_offset_si_to_rt_offset(
BLK_TBL_ELEM		*bt_elem,
SUBINDEX			subindex,
int					bint_offset,
int					*rt_offset)
{
	int						rc;			/* return code */
	BLK_ITEM_NAME_TBL_ELEM	*bint_elem ;/* Block item name tbl elem */
	CHAR_MEM_TBL			*cmt ;		/* Characteristics member tbl */
	CHAR_MEM_TBL_ELEM		*cmt_elem ;	/* Char member tbl element */
	PARAM_MEM_TBL			*pmt ;		/* Parameter mem table */
	PARAM_MEM_TBL_ELEM		*pmt_elem ;	/* Parameter mem table elem */
	PARAM_ELEM_TBL			*pet ;		/* Parameter element table */
	PARAM_ELEM_TBL_ELEM		*pet_elem ;	/* Parameter elem tbl elem */
	PARAM_TBL_ELEM			*pt_elem ;	/* Parameter table element */

	bint_elem = BINTE(BTE_BINT(bt_elem),bint_offset) ;
	if ((BINTE_PT_OFFSET(bint_elem) == -1 && 
			(BINTE_PLT_OFFSET(bint_elem) == -1))) {
		/*
		 * It is a Reference to a member of the Characteristics Record 
		 */
		cmt = BTE_CMT(bt_elem) ;
		if ((subindex >= 1) && (subindex <= (unsigned long )CMT_COUNT(cmt))) {

			/*
			 * The subindex is valid so get the Relation Table
			 * Offset
			 */

			cmt_elem = CMTE(cmt,subindex - 1) ;
			*rt_offset = CMTE_RT_OFFSET(cmt_elem) ;
			rc = DDS_SUCCESS ;
		}
		else {
			rc = DDI_TAB_BAD_SUBINDEX ;
		}
	}
	else if (BINTE_PT_OFFSET(bint_elem) != -1) {

		/*
		 * It is a Reference to a Parameter so get the
		 * parameter element.
		 */

		pt_elem = PTE(BTE_PT(bt_elem),BINTE_PT_OFFSET(bint_elem)) ;
		if ((PTE_PM_COUNT(pt_elem) > 0) && 
				(PTE_PMT_OFFSET(pt_elem) != -1)) {

			/*
			 * It is a Record so use the subindex to calculate pmt_offset
			 */
			int pmt_offset = PTE_PMT_OFFSET(pt_elem) + subindex - 1;

			pmt = BTE_PMT(bt_elem) ;
			if ((pmt_offset >= 0) && (pmt_offset < PMT_COUNT(pmt))) {

				/*
				 * The pmt_offset is valid so get the Relation
				 * table offset from the Parameter Member
				 * table
				 */

				pmt_elem = PMTE(pmt, pmt_offset) ;
				*rt_offset = PMTE_RT_OFFSET(pmt_elem) ;
				rc = DDS_SUCCESS ;
			}
			else {
				rc = DDI_TAB_BAD_SUBINDEX ;
			}
		}
		else if (PTE_PE_MX_COUNT(pt_elem) > 0) {

			/*
			 * It is an Array so see if the specified
			 * subindex is in the parameter element table
			 */

			pet = BTE_PET(bt_elem);
			rc = find_pet_elem(pet,pt_elem,subindex,&pet_elem) ;
			if (rc != DDS_SUCCESS) {

				/*
				 * Use the default for the array
				 */

				bint_elem = BINTE(BTE_BINT(bt_elem),PTE_BINT_OFFSET(pt_elem)) ;
				*rt_offset = BINTE_RT_OFFSET(bint_elem) ;
				rc = DDS_SUCCESS ;

			}
			else {

				/*
				 * Use the Parameter Table Element 
				 */

				*rt_offset = PETE_RT_OFFSET(pet_elem) ;
			}
		}
		else {

			/*
			 * It is a variable so the bint_offset is invalid
			 */

			rc = DDI_TAB_BAD_PARAM_TYPE ;
		}

	}
	else {
		rc = DDI_TAB_BAD_PARAM_TYPE ;
	}

	return rc;
}



/***********************************************************************
 *
 *  Name: check_param_type_is
 *  ShortDesc: 	Check the Item Type of the specified parameter
 *
 *  Description:
 *		Compare the passed in item type versus the item type stored in
 *		the item table.  Use the specified parameter table element
 *		to index the block item name table.  Use the corresponding
 *		block item name table element to index the item table.  Get
 *		the item type from the corresponding item table element.
 *
 *  Inputs:
 *		block_handle - The unique identifier of the block containing the
 *			parameter
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *		pt_elem - The parameter to check
 *		item_type - The item type that the specified parameter must be
 *  Outputs:
 *		None
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

static int
check_param_type_is(
ENV_INFO			*env_info,
BLOCK_HANDLE		block_handle,
BLK_TBL_ELEM		*bt_elem,
PARAM_TBL_ELEM		*pt_elem,
int					item_type)
{
	ITEM_TBL					*it ;
	ITEM_TBL_ELEM				*it_elem ;
	BLK_ITEM_NAME_TBL_ELEM		*bint_elem;
	FLAT_DEVICE_DIR				*flat_device_dir ;
	int							rs ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rs = pConnMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rs != SUCCESS) {
		return(rs) ;
	}

	it = &flat_device_dir->item_tbl;
	bint_elem = BINTE(BTE_BINT(bt_elem),PTE_BINT_OFFSET(pt_elem)) ;
	it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));
	if (item_type != ITE_ITEM_TYPE(it_elem)) {
		return(DDI_TAB_BAD_PARAM_TYPE) ;
	}
	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *  Name: tr_param_to_size
 *  ShortDesc: 	Convert a block_handle and parameter offset into the  
 *		size and type of the variable.
 *
 *  Description:
 *		1.	Check the parameter offset is in range for the block
 *		2.	Check the specified parameter is a variable
 *		3.	Get the type and size from the corresponding element in
 *			the parameter table.
 *
 *  Inputs:
 *		block_handle - the block handle
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *      param_offset - The offset of a parameter to convert to the type and size
 *				of the corresponding variable.
 *  Outputs:
 *      type_size - the type and size of the requested variable.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_param_to_size(
ENV_INFO			*env_info,
BLOCK_HANDLE		block_handle,
BLK_TBL_ELEM		*bt_elem,
int					param_offset,
TYPE_SIZE			*type_size)
{
	int					rs;
	PARAM_TBL_ELEM		*pt_elem;

	/*
	 * Check the parameter offset is in the range of parameters
	 * of the block.
	 */
	rs = CHECK_PARAM_OFFSET(BTE_PT(bt_elem),param_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	/*
	 * Be sure the specified parameter is a variable
	 * (not an array or a record).
	 */
	pt_elem = PTE(BTE_PT(bt_elem),param_offset) ;
	rs = check_param_type_is(env_info, block_handle,bt_elem,pt_elem,VARIABLE_ITYPE) ;
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	type_size->size = (unsigned short)PTE_AE_VAR_SIZE(pt_elem) ;
	type_size->type = (unsigned short)PTE_AE_VAR_TYPE(pt_elem) ;
	return(DDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: tr_param_si_to_size
 *  ShortDesc: 	Convert a block_handle, parameter offset,and subindex
 *		into the size and type of the variable.
 *
 *  Description:
 *		1.	Check the parameter offset is in range for the block
 *		2.	Check the specified parameter is a record or an array
 *		3.  Check the specified subindex is in range for the parameter
 *		4.	If the parameter is an array, get the type and size from
 *			the parameter table.  If the parameter is a record, get the
 *			type and size from the parameter member table.
 *
 *  Inputs:
 *		block_handle - the block handle
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *      param_offset - The offset of a parameter to convert to the type and size
 *				of the corresponding variable.
 *		subindex - is the element of the record (or array) to get the size
 *					and type
 *  Outputs:
 *      type_size - the type and size of the requested variable.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/
//not used in legacy engine
int
tr_param_si_to_size(
ENV_INFO *env_info,
BLOCK_HANDLE		block_handle,
BLK_TBL_ELEM		*bt_elem,
int					param_offset,
SUBINDEX			subindex,
TYPE_SIZE			*type_size)
{
	int					rs;
	int					rs_record;
	int					rs_array;
	int					param_mem_tbl_offset; /* param member offset */
	PARAM_TBL_ELEM		*pt_elem;
	PARAM_MEM_TBL_ELEM	*pmt_elem;

	/*
	 * Check the parameter offset is in the range of parameters
	 * of the block.
	 */
	rs = CHECK_PARAM_OFFSET(BTE_PT(bt_elem),param_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	/*
	 * Be sure the specified parameter is a Record or and Array
	 * (not a Variable).
	 */
	pt_elem = PTE(BTE_PT(bt_elem),param_offset) ;
	rs_record = check_param_type_is(env_info, block_handle,bt_elem,pt_elem,
			RECORD_ITYPE) ;
	rs_array = check_param_type_is(env_info, block_handle,bt_elem,pt_elem,
			ARRAY_ITYPE) ;
	if (rs_record != DDS_SUCCESS && rs_array != DDS_SUCCESS) {
		return(DDI_TAB_BAD_PARAM_TYPE) ;
	}	

	/*
	 *	Convert subindex to a param member table offset
	 */

	param_mem_tbl_offset = PARAM_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex);

	/*
	 * Check the offset to be sure it is in range
	 */
	rs = CHECK_PARAM_MEMBER_OFFSET(pt_elem, param_mem_tbl_offset) ;
	if (rs != DDS_SUCCESS) {
		return(rs) ;
	}	
	if (rs_record == DDS_SUCCESS) {
		pmt_elem = PMTE(BTE_PMT(bt_elem), param_mem_tbl_offset + PTE_PMT_OFFSET(pt_elem)) ;
		type_size->size = (unsigned short)PMTE_PM_SIZE(pmt_elem) ;
		type_size->type = (unsigned short)PMTE_PM_TYPE(pmt_elem) ;
	} else if (rs_array == DDS_SUCCESS) {
		type_size->size = (unsigned short)PTE_AE_VAR_SIZE(pt_elem) ;
		type_size->type = (unsigned short)PTE_AE_VAR_TYPE(pt_elem) ;
	} else {
		return (DDI_TAB_BAD_PARAM_TYPE);
	}
	return(DDS_SUCCESS);
}

/***********************************************************************
 *
 *  Name: tr_char_to_size
 *  ShortDesc: 	Convert a block_handle and characteristic record offset 
 *		into the size and type of the variable.
 *
 *  Description:
 *		1.	Check the characteristic record offset.
 *		2.	Index into the charcteristic member table using the
 *			characteristic record offset.
 *		3.	Get the type and size of the characteristic member from
 *			the corresponding characteristic member table element.
 *
 *  Inputs:
 *      bt_elem - The block table element (i.e. block type) that contains
 *				the list of parameters.  The type and size are retrieved
 *				from a parameter in the list.
 *      char_offset - The offset of a characteristic member to convert to 
 *				the type and size of the corresponding variable.
 *  Outputs:
 *      type_size - the type and size of the requested variable.
 *
 *  Returns:
 *      DDS_SUCCESS.
 *      DDI_TAB_NO_BLOCK.
 *
 *  Author:
 *      Kent Anderson
 *
 **********************************************************************/

int
tr_char_to_size(
BLK_TBL_ELEM		*bt_elem,
SUBINDEX			char_offset,
TYPE_SIZE			*type_size)
{
	int					rs;
	CHAR_MEM_TBL_ELEM   *cmt_elem;

	/*
	 * Check the Characteristic member offset is in the range of 
	 * characteristic members of the block.
	 */
	rs = CHECK_CHAR_MEMBER_OFFSET(BTE_CMT(bt_elem), ( int) char_offset);
	if (rs != DDS_SUCCESS) {
		return(rs);
	}
	cmt_elem = CMTE(BTE_CMT(bt_elem),char_offset) ;
	type_size->size = (unsigned short)CMTE_CM_SIZE(cmt_elem) ;
	type_size->type = (unsigned short)CMTE_CM_TYPE(cmt_elem) ;
	return(DDS_SUCCESS);
}
 
