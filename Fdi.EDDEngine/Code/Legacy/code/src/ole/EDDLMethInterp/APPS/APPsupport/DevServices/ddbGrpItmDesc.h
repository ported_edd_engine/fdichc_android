/*************************************************************************************************
 *
 * $Workfile: ddbGrpItmDesc.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		More item classes
 *		10/24/02	sjv	created from ddbCollAndArr.h to use in other areas
 *		8/19/04		sjv made itmArr,Coll & File use these (expanded) classes
 *
 *		
 * #include "ddbGrpItmDesc.h"
 */

#ifndef _DDB_GRPITMDESC_H
#define _DDB_GRPITMDESC_H
#ifdef INC_DEBUG
#pragma message("In ddbGrpItmDesc.h") 
#endif


class hCattrBase;

/*************************************************************************************************
 *  An individual element
 ************************************************************************************************/

// a new base type for array elements and collection members
class hCgroupItemDescriptor : public hCobject
{
	unsigned long    itmIdx;
	hCreference		 ref;
	string           memName;

	hCddlString      descS;
	hCddlString      helpS;
      
public:

	hCgroupItemDescriptor(DevInfcHandle_t h)
		:hCobject(h), ref(h), descS(h), helpS(h) {  clear(); };
	hCgroupItemDescriptor(const hCgroupItemDescriptor& giD)
		:hCobject(giD.devHndl()),ref(giD.devHndl()),
		 descS(giD.devHndl()),helpS(giD.devHndl()),memName(giD.memName){*this = giD;};
	virtual ~hCgroupItemDescriptor();

	RETURNCODE destroy(void){return (ref.destroy()&descS.destroy()&helpS.destroy());};

	/* 15dec04-stevev for list/array instance generation */
	hCgroupItemDescriptor(hCreference& inRef)
		:hCobject(inRef.devHndl()), ref(inRef), descS(inRef.devHndl()), helpS(inRef.devHndl()) { };

	hCgroupItemDescriptor& operator=(const hCreference& inRef);
	hCgroupItemDescriptor& operator=(const hCgroupItemDescriptor& giD);

	void setIndex(unsigned long newIndex);
	void setDesc(wstring newDesc);
	/* end new */

	void setEqual(void* pAitem);
	void setDuplicate(hCgroupItemDescriptor& rGid);

	void clear(void);

	// accessors
	unsigned long getIIdx(void) { return itmIdx;};
	hCreference&  getRef (void) { return ref;   };
	hCreference*  getpRef(void) { return &ref;  };
	hCddlString&  getDesc(void) { return descS; };
	hCddlString&  getHelp(void) { return helpS; };
	string&       getName(void) { return memName;};
	RETURNCODE    getItemPtr(hCitemBase*& pItem) 
					{ return (ref.resolveID( pItem,false )); };// stevev 27jul06
};


typedef vector<hCgroupItemDescriptor>  groupItemList_t;
typedef vector<hCgroupItemDescriptor*> groupItemPtrList_t;
typedef vector<hCgroupItemDescriptor*>::iterator          groupItemPtrIterator_t;//ptr2ptr2gid


/*************************************************************************************************
 *  The attribute for holding the group-items of the class
 ************************************************************************************************/



/*************************************************************************************************
 *  The generic class - for item-arrays, collections, files
 ************************************************************************************************/


class hCgrpItmBasedClass: public hCitemBase /* class for collections,itemarrays, & files */
{
public:
	hCgrpItmBasedClass(DevInfcHandle_t h, aCitemBase* paItemBase);
    virtual ~hCgrpItmBasedClass(){};
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
    virtual RETURNCODE Label(wstring& retStr) { return baseLabel(0, retStr); }
	RETURNCODE getList(groupItemPtrList_t& r2grpIL);
	bool       isInGroup (UINT32 indexValue);
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getAllindexValues(UIntList_t& allValidindexes);//all possible index numbers
	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);//match index
};




#endif //_DDB_GRPITMDESC_H

/*************************************************************************************************
 *
 *   $History: ddbGrpItmDesc.h $
 * 
 *************************************************************************************************
 */
