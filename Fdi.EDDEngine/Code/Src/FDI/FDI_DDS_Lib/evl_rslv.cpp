/*
 *	@(#) $Id: evl_rslv.cpp,v 1.3 2012/10/04 23:50:57 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to
 *	resolving references
 */


#include "stdafx.h"
#include "evl_loc.h"
#include "dds_upcl.h"
#include "fch_lib.h"
#include "dds_tab.h"
#include "conversions.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif
#include "FDI/FDIEDDEngine/DDSSupport.h"

extern int
get_attribute_value(::nsEDDEngine::AttributeName attrib_name,	DDL_UINT which,	DESC_REF *desc_ref,	EXPR *expr,
					nsEDDEngine::OP_REF_LIST	*depinfo, ENV_INFO *env_info,nsEDDEngine::OP_REF			*var_needed);
/*********************************************************************
* lookup table for converting parse tags to item types
* used by both resolve_ref() and parse_ref()
*********************************************************************/
extern const int resolve_id_table[];	/* converts parse tags to item types */

const
int
resolve_id_table[] =
{
	0,						//0		ITEM_ID_REF							ITEM_ID_REF
	VARIABLE_ITYPE,			//1		VARIABLE_ID_REF						ITEM_ARRAY_ID_REF
	MENU_ITYPE,				//2		MENU_ID_REF							COLLECTION_ID_REF
	EDIT_DISP_ITYPE,		//3		EDIT_DISP_ID_REF					VIA_ITEM_ARRAY_REF
	METHOD_ITYPE,			//4		METHOD_ID_REF						VIA_COLLECTION_REF
	REFRESH_ITYPE,			//5		REFRESH_ID_REF						VIA_RECORD_REF
	UNIT_ITYPE,				//6		UNIT_ID_REF							VIA_ARRAY_REF
	WAO_ITYPE,				//7		WAO_ID_REF							VIA_VAR_LIST_REF
	ITEM_ARRAY_ITYPE,		//8		ITEM_ARRAY_ID_REF					VIA_PARAM_REF
	COLLECTION_ITYPE,		//9		COLLECTION_ID_REF					VIA_PARAM_LIST_REF
	BLOCK_B_ITYPE,			//10	BLOCK_B_ID_REF						VIA_BLOCK_REF
	BLOCK_ITYPE,			//11	BLOCK_ID_REF						BLOCK_ID_REF
	RECORD_ITYPE,			//12	RECORD_ID_REF						VARIABLE_ID_REF
	ARRAY_ITYPE,			//13	ARRAY_ID_REF						MENU_ID_REF
	VAR_LIST_ITYPE,			//14	VAR_LIST_ID_REF						EDIT_DISP_ID_REF
	RESP_CODES_ITYPE,		//15	RESP_CODES_ID_REF					METHOD_ID_REF
	FILE_ITYPE,				//16	FILE_ID_REF							REFRESH_ID_REF
	CHART_ITYPE,			//17	CHART_ID_REF						UNIT_ID_REF
	GRAPH_ITYPE,			//18	GRAPH_ID_REF						WAO_ID_REF
	AXIS_ITYPE,				//19	AXIS_ID_REF							RECORD_ID_REF
	WAVEFORM_ITYPE,			//20	WAVEFORM_ID_REF						ARRAY_ID_REF
	SOURCE_ITYPE,			//21	SOURCE_ID_REF						VAR_LIST_ID_REF
	LIST_ITYPE,				//22	LIST_ID_REF					
	GRID_ITYPE,				//23	GRID_ID_REF					
	IMAGE_ITYPE,			//24	IMAGE_ID_REF						RESP_CODES_ID_REF
	BLOB_ITYPE,				//25	BLOB_ID_REF							FILE_ID_REF
	PLUGIN_ITYPE,			//26	PLUGIN_ID_REF						CHART_ID_REF
	TEMPLATE_ITYPE,			//27	TEMPLATE_ID_REF						GRAPH_ID_REF
	COMPONENT_ITYPE,		//28	COMPONENT_ID_REF					AXIS_ID_REF
	COMPONENT_FOLDER_ITYPE,	//29	COMPONENT_FOLDER_ID_REF				WAVEFORM_ID_REF
	COMPONENT_REFERENCE_ITYPE,	//30	COMPONENT_REFERENCE_ID_REF			SOURCE_ID_REF
	COMPONENT_REL_ITYPE,	//31	COMPONENT_RELATION_ID_REF			LIST_ID_REF
	0,						//32	INTERFACE_ID_REF					IMAGE_ID_REF
	ROWBREAK_ITYPE,			//33	ROWBREAK_REF						SEPARATOR_REF
	COLUMNBREAK_ITYPE,		//34	SEPARATOR_REF/COLUMNBREAK_REF   	CONSTANT_REF
	0,						//35	CONSTANT_REF						VIA_FILE_REF
	ITEM_ARRAY_ITYPE,		//36	VIA_ITEM_ARRAY_REF					VIA_LIST_REF
	COLLECTION_ITYPE,		//37	VIA_COLLECTION_REF					VIA_BITENUM_REF
	RECORD_ITYPE,			//38	VIA_RECORD_REF						GRID_ID_REF
	ARRAY_ITYPE,			//39	VIA_ARRAY_REF						ROWBREAK_REF
	VAR_LIST_ITYPE,			//40	VIA_VAR_LIST_REF					VIA_CHART_REF
	FILE_ITYPE,				//41	VIA_FILE_REF						VIA_GRAPH_REF
	CHART_ITYPE,			//42	VIA_CHART_REF						VIA_SOURCE_REF
	GRAPH_ITYPE,			//43	VIA_GRAPH_REF						VIA_SELECTOR_REF
	SOURCE_ITYPE,			//44	VIA_SOURCE_REF						VIA_LOCAL_PARAM_REF
	LIST_ITYPE,				//45	VIA_LIST_REF						BLOCK_B_ID_REF
	ENUM_BIT_ITYPE,			//46	VIA_BITENUM_REF						CONNECTION_ID_REF
	0,						//47	VIA_ATTRIBUTE_REF					COLUMNBREAK_REF
	0,						//48	reserved */		METH_ARGS_REF		// Method call w/ arguments
	BLOCK_ITYPE,			//					VIA_XBLOCK_REF				
	BLOCK_ITYPE,			//					VIA_BLOCK_REF				
	0,						//					VIA_PARAM_REF				
	0,						//					VIA_PARAM_LIST_REF			
	RECORD_ITYPE,			//					BLOCK_CHAR_REF				
	0,						//					VIA_LOCAL_PARAM_REF			
	METH_ARGS_ITYPE			//					REF_WITH_ARGS_REF			

};


/*********************************************************************
 *
 *	Name: resolve_fetch()
 *	ShortDesc: fetch the binary for the given flat item
 *
 *	Description:
 *		resolve_fetch determines which type of fetch we are using
 *		and makes the appropriate call to fetch the binary for the
 *		given flat item
 *
 *	Inputs:
 *		item_id:      the item id of the item to fetch
 *		mask:         the request mask for the attributes of the item
 *		item_type:    the type of the flat item
 *		block_handle: the handle of the current block
 *
 *	Outputs:
 *		item:        the flat item with binarys attached
 *
 *
 *	Returns: DDI_INVALID_BLOCK_HANDLE
 *           DDL_RESOLVE_FETCH_FAIL
 *			 DDL_MEMORY_ERROR
 *			 and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
resolve_fetch(
ENV_INFO		*env_info,
ITEM_ID         item_id,
::nsEDDEngine::AttributeNameSet	   mask,
::nsEDDEngine::ItemBase          *item,
unsigned short  item_type,
BLOCK_HANDLE    block_handle)
{
	DEVICE_HANDLE   device_handle = 0; /* device handle from the block handle */
	int             rc;	               /* return code */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

#ifdef DDSTEST
	TEST_FAIL(RESOLVE_FETCH);
#endif

	/*
	 * Check for a valid block handle
	 */

	if (!pConnectionMgr->valid_block_handle(block_handle)) {

		return DDI_INVALID_BLOCK_HANDLE;
	}

	rc  = pConnectionMgr->get_abt_adt_offset(block_handle, &device_handle);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	rc = fch_item(env_info, device_handle, item_id, mask, item, item_type);

	return rc;
}


/**********************************************************************
 *
 *  Name: rslv_find_local
 *  ShortDesc:	Given Member ID, find the Item ID for a LOCAL
 *
 *  Include:
 *		pc_loc.h
 *
 *  Description:
 *		Given a member ID for a LOCAL PARAMETER of a block, find the
 *      DESC_REF record associated with it.
 *
 *  Inputs:
 *		block_handle, Member ID
 *
 *  Outputs:
 *      DESC_REF record will be overwritten if successful, zeroed if not
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/

int rslv_find_local(ENV_INFO *env_info, ITEM_ID memberID, DESC_REF *out_mem)
{
    DDI_BLOCK_SPECIFIER bs={0};
	nsEDDEngine::FDI_ITEM_SPECIFIER is={nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID, 0, 0};
	DDI_GENERIC_ITEM gi={0};
    int rs;
    nsEDDEngine::FLAT_BLOCK *fb=NULL;
    int i;

    bs.type = DDI_BLOCK_HANDLE;
    bs.block.handle = env_info->block_handle;
	is.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_BLOCK;

	//fix masks throughout , indent level when evaling indentLevel++ inside of eval
	nsEDDEngine::AttributeNameSet masks;
	masks+=nsEDDEngine::local_parameters;

    rs = ddi_get_item(&bs, &is, env_info, masks, &gi); // BLOCK_LOCAL_PARAM
    if (rs)
	{
		if(gi.item)
			delete ((::nsEDDEngine::ItemBase *)gi.item);
        return rs;
	}

    fb = (nsEDDEngine::FLAT_BLOCK *)gi.item;
    for (i=0; i < fb->local_param.count; i++)
    {
        if (memberID == fb->local_param.list[i].name)
        {
            //memcpy(out_mem, &fb->local_param.list[i].ref, sizeof(DESC_REF));
			::FDI_ConvDESC_REF::CopyTo( &fb->local_param.list[i].ref, out_mem );

 			delete fb;
            return SUCCESS;
        }
    }

	delete fb;
    return PC_PARAM_NOT_FOUND;
}

/**********************************************************************
 *
 *  Name: rslv_get_one_local
 *  ShortDesc:	 Get a single local_param for a block
 *
 *  Include:
 *
 *  Description:
 *		pc get_one_local gets the specified local parameter for the block.
 *      The caller is responsible for freeing the DDI_GENERIC_ITEM record
 *      by calling ddi_clean_item.
 *
 *  Inputs:
 *		block_handle, MEMBER that represents the local param
 *
 *  Outputs:
 *      A DDI_GENERIC_ITEM structure for the VARIABLE, RECORD, or ARRAY.
 *
 *  Returns:
 *      0 on success, nonzero on failure
 *********************************************************************/

int rslv_get_one_local(ENV_INFO *env_info, MEMBER *m, DDI_GENERIC_ITEM *gi)
{
    DDI_BLOCK_SPECIFIER bs;
	nsEDDEngine::FDI_ITEM_SPECIFIER is;

    bs.type = DDI_BLOCK_HANDLE;
	bs.block.handle = env_info->block_handle;
	is.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
    is.item.id = m->ref.id;    
    memset(gi, 0, sizeof(DDI_GENERIC_ITEM));
	
	::nsEDDEngine::AttributeNameSet mask;

    switch(m->ref.type)
    {
    case VARIABLE_ITYPE:
		mask=nsEDDEngine::variable_type|nsEDDEngine::class_attr;
        break;
    case ARRAY_ITYPE:
 		mask=nsEDDEngine::number_of_elements|nsEDDEngine::type_definition;
        break;
    case RECORD_ITYPE:
 		mask=nsEDDEngine::members;
        break;
    case LIST_ITYPE:
 		mask=nsEDDEngine::type_definition;
       break;
    default:
        return PC_WRONG_DATA_TYPE;
    }

    return ddi_get_item(&bs, &is, env_info, mask, gi);
}

/////////////////////////////////////////////////////
//
// find_member() - Finds the member in the MEMBER_LIST binary and adjusts the
//					reference and current environment from that member
//					This function is used for Collection, File, Chart, Graph
//					and Source items.

static int
find_member(OP_REF_TRAIL	*ref,			// OP REF TRAIL we are building (in/out)
			unsigned short	*trail_inc,		// current environment passed (in/out)
			unsigned short  *refInfo_inc,	//  "	"	"	"
			ITEM_ID			*current_id,	//	"	"	"	"
			ITEM_TYPE		*current_type,	//	"	"	"	"
			REF_INFO		*info_ptr,		// current REF INFO data	(in)
			int				 info_inc,		//	"	"	"	"
			DEPBIN			*cur_depbin,	// MEMBER binary to search	(in)
			nsEDDEngine::OP_REF_LIST		*depinfo,		// standard inputs	(in/out)
			ENV_INFO		*env_info,		//	"	"	"	"
			nsEDDEngine::OP_REF			*var_needed,	//	"	"	"	"
			unsigned int	 options)		//	"	"	"	"
{
	/*
	 * eval the binary
	 */
	MEMBER_LIST members = {0};
	bool free_attr = false;

	int rc = eval_attr_members(cur_depbin->bin_chunk,
			cur_depbin->bin_size,
			&members,
			&cur_depbin->dep,
			env_info,
			var_needed);

	if (rc == DDL_SUCCESS) {
		free_attr = true;
	}

	// Process depinfo
	rc = append_and_free_depinfo( depinfo, &cur_depbin->dep, rc);

	if (rc == DDL_SUCCESS)
	{
		/*
		 * find the member name in the collection list
		 */

		MEMBER* member_ptr = members.list;
		int count = members.count;
		int inc = 0;

		for (inc = 0; inc < count; member_ptr++, inc++) {

			if (member_ptr->name == info_ptr->val.member) {

				*current_id = member_ptr->ref.id;
				*current_type = member_ptr->ref.type;

				/*
				 * if the current type is an
				 * ITEM_ID_REF convert this to
				 * an actual item type
				 */

				if (current_type == ITEM_ID_REF) {

					if (info_inc > 0) {
						*current_type = (ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
					}
				}

				if (options & RESOLVE_OP_REF) 
				{
					if( *refInfo_inc == 0 )
					{
						ref->op_ref_type = STANDARD_TYPE;
						ref->op_info.id = *current_id;
						ref->op_info.type = *current_type;
						ref->op_info.member = 0;
					}
					else
					{
						ref->op_info_list.list[*refInfo_inc].member = member_ptr->name;
						(*refInfo_inc)++;
						ref->op_info_list.list[*refInfo_inc].id = *current_id;
						ref->op_info_list.list[*refInfo_inc].type = *current_type;
						if( *current_type == VARIABLE_ITYPE )
						{
							ref->op_info_list.list[*refInfo_inc].member = 0;
							ref->op_info_list.count = *refInfo_inc + 1;
						}
					}
				}

				if (options & RESOLVE_TRAIL) {

					ref->trail[*trail_inc].element = member_ptr->name;
					++(*trail_inc);
					ref->trail[*trail_inc].id = *current_id;
					ref->trail[*trail_inc].type = *current_type;
				}

				break;
			}
		}

		if (inc == count) {
			rc = DDL_ENCODING_ERROR;
		}
	}

	if (free_attr) {
		ddl_free_members_list(&members, FREE_ATTR);
	}
	
	return rc;
}



/////////////////////////////////////////////////////
//
// find_op_member() - Finds the member in the OP_MEMBER_LIST binary and adjusts the
//					reference and current environment from that member
//					This function is used for Collection, File, Chart, Graph
//					and Source items.

static int
find_op_member(OP_REF_TRAIL	*ref,			// OP REF TRAIL we are building (in/out)
			unsigned short	*trail_inc,		// current environment passed (in/out)
			unsigned short  *refInfo_inc,	//  "	"	"	"
			ITEM_ID			*current_id,	//	"	"	"	"
			ITEM_TYPE		*current_type,	//	"	"	"	"
			REF_INFO		*info_ptr,		// current REF INFO data	(in)
			int				 info_inc,		//	"	"	"	"
			DEPBIN			*cur_depbin,	// MEMBER binary to search	(in)
			nsEDDEngine::OP_REF_LIST		*depinfo,		// standard inputs	(in/out)
			ENV_INFO		*env_info,		//	"	"	"	"
			nsEDDEngine::OP_REF			*var_needed,	//	"	"	"	"
			unsigned int	 options)		//	"	"	"	"
{
	/*
	 * eval the binary
	 */
	OP_MEMBER_LIST op_members = {0};
	bool free_attr = false;

	int rc = eval_attr_op_members(cur_depbin->bin_chunk,
			cur_depbin->bin_size,
			&op_members,
			&cur_depbin->dep,
			env_info,
			var_needed);

	if (rc == DDL_SUCCESS) {
		free_attr = true;
	}

	// Process depinfo
	rc = append_and_free_depinfo( depinfo, &cur_depbin->dep, rc);

	if (rc == DDL_SUCCESS)
	{
		/*
		 * find the member name in the collection list
		 */

		OP_MEMBER* op_member_ptr = op_members.list;
		int count = op_members.count;
		int inc = 0;

		for (inc = 0; inc < count; op_member_ptr++, inc++) {

			if (op_member_ptr->name == info_ptr->val.member) {

				*current_id = op_member_ptr->ref.op_info.id;
				*current_type = op_member_ptr->ref.op_info.type;

				/*
				 * if the current type is an
				 * ITEM_ID_REF convert this to
				 * an actual item type
				 */

				if (current_type == ITEM_ID_REF) {

					if (info_inc > 0) {
						*current_type = (ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
					}
				}

				if (options & RESOLVE_OP_REF) 
				{
					if( *refInfo_inc == 0 )
					{
						ref->op_ref_type = STANDARD_TYPE;
						ref->op_info.id = *current_id;
						ref->op_info.type = *current_type;
						ref->op_info.member = 0;
					}
					else
					{
						ref->op_info_list.list[*refInfo_inc].member = op_member_ptr->name;
						(*refInfo_inc)++;
						ref->op_info_list.list[*refInfo_inc].id = *current_id;
						ref->op_info_list.list[*refInfo_inc].type = *current_type;
						if( *current_type == VARIABLE_ITYPE )
						{
							ref->op_info_list.list[*refInfo_inc].member = 0;
							ref->op_info_list.count = *refInfo_inc + 1;
						}
					}
				}

				if (options & RESOLVE_TRAIL) {

					ref->trail[*trail_inc].element = op_member_ptr->name;
					++(*trail_inc);
					ref->trail[*trail_inc].id = *current_id;
					ref->trail[*trail_inc].type = *current_type;
				}

				break;
			}
		}

		if (inc == count) {
			rc = DDL_ENCODING_ERROR;
		}
	}

	if (free_attr) {
		ddl_free_op_members_list(&op_members, FREE_ATTR);
	}
	
	return rc;
}
/*********************************************************************
 *
 *	Name: resolve_ref
 *
 *	ShortDesc: resolve a ddl reference
 *
 *	Description:
 *		resolve_ref takes a ref_info array and resolves it
 *		to the description reference which is the item_id and
 * 		type of the reference. It will also load an operational
 *		reference and a resolve trail if the options argument is
 *		set accordingly
 *
 *	Inputs:
 *		ref_info:	  A pointer to the list of reference information
 *					  used to resolve a reference.
 *		depinfo:	  The dependency information if any part of the
 *					  reference has a dependency.
 *		env_info:     The env_info for the reference to be resolved.
 *		var_needed:	  The variable for which a value is needed if
 *					  the parameter value service did not succeed.
 *		options:	  The options mask for requesting different types
 *					  of reference information including RESOLVE_TRAIL
 *					  RESOLVE_OP_REF and RESOLVE_DESC_REF
 *		extra_trail:  The number of trail elements to be added beyond
 *					  the standard count.
 *
 *	Outputs:
 *		ref:		  A pointer to the op_ref_trail which is loaded with
 *					  the requested resolved reference information.
 *
 *	Returns:
 *		DDI_INVALID_BLOCK_HANDLE
 *		and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
resolve_ref(
REF_INFO_LIST  *ref_info,
OP_REF_TRAIL   *ref,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
unsigned int    options,
unsigned short	extra_trail,
bool ignore_validity)
{

	int             rc = DDL_SUCCESS;	/* return code */
	int             free_attr = FALSE;	/* free attribute indicator */
	int             info_inc;	        /* ref_info counter */
	REF_INFO       *info_ptr;	        /* ref_info pointer */
	unsigned short  inc;	            /* incrementer for item lists */
	int             count;	            /* count for item lists */
	unsigned short  trail_inc = 0;	    /* trail incrementer */
	unsigned short	refInfo_inc = 0;	/* op ref info list incrementer */

	SUBINDEX        subindex;	    /* subindex for name to subindex */
	nsEDDEngine::BLK_TBL_ELEM   *bt_elem = NULL;	/* Block Table element pointer */
	nsEDDEngine::ITEM_TBL       *it = NULL;	        /* item table */
	nsEDDEngine::ITEM_TBL_ELEM  *it_elem = NULL;     /* item table element */

	nsEDDEngine::ITEM_ARRAY_ELEMENT *item_array_ptr; /* item array pointer for speed */

	nsEDDEngine::DEPBIN		   *cur_depbin;			/* Current dependency and binary */

	ITEM_ID         next_id = 0;	    /* id argument to table request functions */
	ITEM_ID         current_id = 0;	    /* the current id in the resolve trail */
	ITEM_TYPE       current_type = 0;	/* current type in the resolve trail */
	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir ;
    ENV_INFO		env={0};

#ifdef DDSTEST
	TEST_FAIL(RESOLVE_REF);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	ref->block_instance = 0;

	if (options & RESOLVE_TRAIL) {

		ref->trail_limit = ref_info->count + extra_trail;

		ASSERT(ref->trail==NULL);

		ref->trail = (RESOLVE_INFO *) malloc((size_t) ref->trail_limit * sizeof(RESOLVE_INFO));		
		memset(ref->trail, 0, (size_t) ref->trail_limit * sizeof(RESOLVE_INFO));

		if (ref->trail == NULL)
		{
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"resolve_ref: failed to malloc memory");
		}
	}
    else
    {
        ref->trail = NULL;
    }

	/*
	 * Check for a cross block referennce. If so, switch to the new block
	 */
		if (   (ref_info->count > 2) 
				&& (ref_info->list[ref_info->count - 2].type == VIA_BLOCK_REF)
				&& (ref_info->list[ref_info->count - 1].type == BLOCK_ID_REF)
		   )
		{
			int id_index = ref_info->count - 1;
			int instance_index = ref_info->count -2;

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = ref_info->list[id_index].val.id;
				ref->trail[trail_inc].type = BLOCK_XREF_ITYPE; 
				ref->trail[trail_inc].element = ref_info->list[instance_index].val.index;
				trail_inc++;
			}

			// this makes a copy of the current env_info, but connects to a new block_handle
			env.app_info    = env_info->app_info;
			env.value_spec  = env_info->value_spec;
			env.handle_type = ENV_INFO::BlockHandle;	// env.block_handle is filled in below.

			rc = pDDSSupport->GetBlockInstAndHandle( ref_info->list[id_index].val.id, 
												ref_info->list[instance_index].val.index, 
												&ref->block_instance,
												&env.block_handle );
			if (rc != SUCCESS)
			{
				ddl_free_op_ref_trail(ref);
				return CM_BLOCK_NOT_FOUND;
			}

			// Now point the local env_info to our newly redirected block_handle.
			env_info = &env;

			// Note that we are currently on a block
			current_id = ref_info->list[id_index].val.id;
			current_type = BLOCK_ITYPE;

			ref_info->count-=2;			// Adjust the count to skip over the ones already processed
		}

		if (!pConnectionMgr->valid_block_handle(env_info->block_handle)) {
			ddl_free_op_ref_trail(ref);
			return DDI_INVALID_BLOCK_HANDLE;
		}

		rc = block_handle_to_bt_elem(env_info, env_info->block_handle, &bt_elem);
		if (rc != DDS_SUCCESS) {
			ddl_free_op_ref_trail(ref);
			return rc;
		}

		if(!bt_elem)
		{
			ddl_free_op_ref_trail(ref);
			return DDI_INVALID_HANDLE_TYPE;
		}

		DESC_REF desc_ref = {0};
	/*
	 * Move backward through the info list to resolve the reference
	 */

	for (info_inc = (ref_info->count - 1), info_ptr = &ref_info->list[info_inc];
		info_inc >= 0; info_inc--, info_ptr--) {

		switch (info_ptr->type) {

		case ITEM_ID_REF:
		case ITEM_ARRAY_ID_REF:
		case COLLECTION_ID_REF:
		case BLOCK_ID_REF:
		case VARIABLE_ID_REF:
		case MENU_ID_REF:
		case EDIT_DISP_ID_REF:
		case METHOD_ID_REF:
		case REFRESH_ID_REF:
		case UNIT_ID_REF:
		case WAO_ID_REF:
		case RECORD_ID_REF:
		case ARRAY_ID_REF:
		case VAR_LIST_ID_REF:
		case RESP_CODES_ID_REF:
        case CHART_ID_REF:
        case AXIS_ID_REF:
        case FILE_ID_REF:
        case GRAPH_ID_REF:
        case LIST_ID_REF:
        case SOURCE_ID_REF:
        case WAVEFORM_ID_REF:
		case IMAGE_ID_REF:
		case GRID_ID_REF:
		case BLOCK_B_ID_REF:
		case PLUGIN_ID_REF:
		case BLOB_ID_REF:
		case TEMPLATE_ID_REF:
		case COMPONENT_ID_REF:
		case COMPONENT_FOLDER_ID_REF:
		case COMPONENT_REFERENCE_ID_REF:
		case COMPONENT_RELATION_ID_REF:
		case INTERFACE_ID_REF:
			current_id = info_ptr->val.id;
			current_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];

			if (options & RESOLVE_OP_REF) 
			{
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}
			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case REF_WITH_ARGS_REF:
			if (current_type == METHOD_ITYPE)	// This is a method call with arguments
			{
				ref->expr.type = ASCII;		// Store the argument list in the ref->expr as an ASCII string
				ref->expr.size = info_ptr->val.str.len;
				memcpy(&ref->expr.val.s, &info_ptr->val.str, sizeof(info_ptr->val.str));

				// info_ptr->val.str contents were copied, including pointers, into ref->expr.val.s
				// ref->expr.val.s is now responsible to make sure that memory gets cleaned up

				current_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];	// METH_ARGS_ITYPE

				if (options & RESOLVE_OP_REF)
				{
					ref->op_ref_type = STANDARD_TYPE;
					ref->op_info.id = current_id;
					ref->op_info.type = current_type;
					ref->op_info.member = 0;
				}
			}
			else if (current_type == VARIABLE_ITYPE)	// This is an enum string constant
			{
				// current_id is the ItemId of the Variable
				// first argument of info_ptr->val.str is the enumeration value which must be an integer
				unsigned long enum_val = 0;
				swscanf(info_ptr->val.str.str, L"%lu", &enum_val);

				ddl_free_string(&info_ptr->val.str);		// Free the string where the index was stored

				// Look up the string using string_enum_lookup().
				rc = string_enum_lookup(current_id, enum_val, &ref->expr.val.s, depinfo, env_info, var_needed);

				// Set the ref members like we do for CONSTANT_REF with a STRING_LITERAL_ITYPE
				if ((rc == DDL_SUCCESS) || (rc == ENUM_NOT_FOUND)) 
				{
					ref->expr.type = ASCII;
					ref->expr.size = wcslen(ref->expr.val.s.str);

					current_id = 0;
					current_type = STRING_LITERAL_ITYPE;

					if (options & RESOLVE_OP_REF) 
					{
						ref->op_ref_type = STANDARD_TYPE;
						ref->op_info.id = current_id;
						ref->op_info.type = current_type;
						ref->op_info.member = 0;
					}

					rc = DDL_SUCCESS;
				}
			}
			else
			{
				rc = DDL_ENCODING_ERROR;
			}
			break;

		case VIA_BITENUM_REF:
			current_type = (ITEM_TYPE) ENUM_BIT_ITYPE;
			ref->bit_mask = info_ptr->val.index;	// Store the bit mask

			//Don't update ref->op_info for BITENUM_REF

			if (options & RESOLVE_TRAIL)
			{
				trail_inc++;					// Create a new trail entry
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].type = ENUM_BIT_ITYPE;
				ref->trail[trail_inc].element = info_ptr->val.index;
			}
			break;


		case VIA_ITEM_ARRAY_REF:
			{
				/*
				 * fetch the binary
				 */
				nsEDDEngine::FLAT_ITEM_ARRAY     item_array;
				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::elements; // (unsigned long) ITEM_ARRAY_ELEMENTS
				rc = resolve_fetch(env_info, current_id, mask, &item_array,
					(unsigned short) ITEM_ARRAY_ITYPE, env_info->block_handle);

				if (!item_array.masks.bin_hooked.isMember(::nsEDDEngine::elements)) {	// Make sure we have binary for MEMBERS
					rc = DDL_ENCODING_ERROR;
				}

				if (rc == DDL_SUCCESS) {


					/*
						* eval the binary
						*/
					cur_depbin = item_array.depbin->db_elements;
					ITEM_ARRAY_ELEMENT_LIST temp_item_array_list = {0};

					rc = eval_attr_itemarray(cur_depbin->bin_chunk,
						cur_depbin->bin_size,
						&temp_item_array_list,
						&cur_depbin->dep,
						env_info,
						var_needed);

					FDI_ConvITEM_ARRAY_ELEMENT_LIST::CopyFrom( &temp_item_array_list, &item_array.elements );
					::ddl_free_itemarray_list( &temp_item_array_list, FREE_ATTR );
					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					rc = append_and_free_depinfo(depinfo, &cur_depbin->dep, rc);
				}

				if (rc == DDL_SUCCESS) {

					/*
					 * find the index in the item_array list
					 */

					item_array_ptr = item_array.elements.list;
					count = item_array.elements.count;

					for (inc = 0; inc < count; item_array_ptr++, inc++) {

						if (item_array_ptr->index == info_ptr->val.index) {

							current_id = item_array_ptr->ref.id;
							current_type = (ITEM_TYPE)item_array_ptr->ref.type;

							/*
							 * if the current type is an
							 * ITEM_ID_REF convert this to
							 * an actual item type
							 */

							if (current_type == ITEM_ID_REF) {

								if (info_inc > 0) {
									current_type = (ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
								}
							}

							if (options & RESOLVE_OP_REF) 
							{
								ref->op_ref_type = STANDARD_TYPE;
								ref->op_info.id = current_id;
								ref->op_info.type = current_type;
								ref->op_info.member = 0;
							}
							if (options & RESOLVE_TRAIL) {

								ref->trail[trail_inc].element = item_array_ptr->index;
								++trail_inc;
								ref->trail[trail_inc].id = current_id;
								ref->trail[trail_inc].type = current_type;
							}
							break;
						}
					}

					if (inc == count) {			/* The element was not found */
						rc = DDL_ENCODING_ERROR;
					}
				}

                if (free_attr == TRUE)
                {
                    // The item_array deletes elements.list in the destructor but inside
                    // this loop the list is overwritten so it leaves memory hanging.  This
                    // makes sure these get cleaned up.
                    delete[] item_array.elements.list;
                    item_array.elements.list = NULL;
                    free_attr = FALSE;
                }
			}
			break;

		case VIA_COLLECTION_REF:
			{
				::nsEDDEngine::FLAT_COLLECTION collection;// = {0};
				/*
				 * fetch the binary
				 */

				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members | ::nsEDDEngine::validity; //(unsigned long) COLLECTION_MEMBERS
				rc = resolve_fetch(env_info, current_id, mask, &collection,
					(unsigned short)COLLECTION_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					DEPBIN db;
					DEPBIN *pdb = NULL;
					if (collection.depbin)
					{
						if (!ignore_validity)
						{
							if (collection.depbin->db_valid) // If VALIDITY is defined for this COLLECTION
							{
								// Need to check if this is invalid reference by checkin the validity of the COLLECTION
								pdb = &db;
								db.bin_chunk = collection.depbin->db_valid->bin_chunk;
								db.bin_size = collection.depbin->db_valid->bin_size;
								db.dep.count = 0;
								db.dep.limit = 0;
								db.dep.list = NULL;

								DDL_UINT valid;

								nsEDDEngine::ATTRIBUTE_DATA tg = { 0 };
								tg.tag = BOOLEAN_TAG;
								// Get the value of validity attribute of the COLLECTION
								rc = eval_attr_ulong(db.bin_chunk,
									db.bin_size,
									&valid,
									&db.dep,
									env_info,
									var_needed,
									&tg
								);

								if (valid == false)
								{
									// This collection is invalid so should not be added in menu items.
									ddl_free_op_ref_trail(ref);
									return DDL_INVALID_REFERENCE;
								}

								db.bin_chunk = NULL;
								db.bin_size = 0;

							}
						}

						pdb = &db;
						db.bin_chunk = collection.depbin->db_members->bin_chunk;
						db.bin_size = collection.depbin->db_members->bin_size;
						db.dep.count = 0;
						db.dep.limit = 0;
						db.dep.list = NULL;
					}

					rc = find_op_member(ref, 
						&trail_inc,	// OP REF TRAIL we are building
						&refInfo_inc,
						&current_id,
						&current_type,		// current item info
						info_ptr, 
						info_inc,				// current REF INFO data
						pdb,		// MEMBER binary to search
						depinfo, 
						env_info, 
						var_needed, 
						options);	// standard inputs
				}
			}
			break;

		case VIA_FILE_REF:
			{
				::nsEDDEngine::FLAT_FILE file;// = {0};

				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members; //(unsigned long) FILE_MEMBERS

				rc = resolve_fetch(env_info, current_id, mask, &file,
					(unsigned short) FILE_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					DEPBIN db;
					DEPBIN *pdb=NULL;
					if(file.depbin)
					{
						pdb=&db;
						db.bin_chunk=file.depbin->db_members->bin_chunk;
						db.bin_size=file.depbin->db_members->bin_size;
						db.dep.count = 0;
						db.dep.limit = 0;
						db.dep.list = NULL;
					}

					rc = find_member(ref, 
						&trail_inc,	// OP REF TRAIL we are building
						&refInfo_inc,
						&current_id, 
						&current_type,		// current item info
						info_ptr, 
						info_inc,				// current REF INFO data
						pdb,		// MEMBER binary to search
						depinfo, 
						env_info, 
						var_needed, 
						options);	// standard inputs
				}
			}
			break;

		case VIA_CHART_REF:
			{
				::nsEDDEngine::FLAT_CHART chart;// = {0};

				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members; //(unsigned long) CHART_MEMBERS

				rc = resolve_fetch(env_info, current_id, mask, &chart,
					(unsigned short) CHART_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					DEPBIN db;
					DEPBIN *pdb=NULL;
					if(chart.depbin)
					{
						pdb=&db;
						db.bin_chunk=chart.depbin->db_members->bin_chunk;
						db.bin_size=chart.depbin->db_members->bin_size;
						db.dep.count = 0;
						db.dep.limit = 0;
						db.dep.list = NULL;
					}

					rc = find_member(ref, 
						&trail_inc,	// OP REF TRAIL we are building
						&refInfo_inc,
						&current_id, 
						&current_type,		// current item info
						info_ptr, 
						info_inc,				// current REF INFO data
						pdb,		// MEMBER binary to search
						depinfo, 
						env_info, 
						var_needed, 
						options);	// standard inputs
				}
			}
			break;

		case VIA_GRAPH_REF:
			{
				::nsEDDEngine::FLAT_GRAPH graph;// = {0};

				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members; //(unsigned long) GRAPH_MEMBERS

				rc = resolve_fetch(env_info, current_id, mask, &graph,
					(unsigned short) GRAPH_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					DEPBIN db;
					DEPBIN *pdb=NULL;
					if(graph.depbin)
					{
						pdb=&db;
						db.bin_chunk=graph.depbin->db_members->bin_chunk;
						db.bin_size=graph.depbin->db_members->bin_size;
						db.dep.count = 0;
						db.dep.limit = 0;
						db.dep.list = NULL;
					}

					rc = find_member(ref, 
						&trail_inc,	// OP REF TRAIL we are building
						&refInfo_inc,
						&current_id, 
						&current_type,		// current item info
						info_ptr, 
						info_inc,				// current REF INFO data
						pdb,		// MEMBER binary to search
						depinfo, 
						env_info, 
						var_needed, 
						options);	// standard inputs
				}
			}
			break;

		case VIA_SOURCE_REF:
			{
				::nsEDDEngine::FLAT_SOURCE source;// = {0};

				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members; //(unsigned long) SOURCE_MEMBERS

				rc = resolve_fetch(env_info, current_id, mask, &source,
					(unsigned short) SOURCE_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					DEPBIN db;
					DEPBIN *pdb=NULL;
					if(source.depbin)
					{
						pdb=&db;
						db.bin_chunk=source.depbin->db_members->bin_chunk;
						db.bin_size=source.depbin->db_members->bin_size;
						db.dep.count = 0;
						db.dep.limit = 0;
						db.dep.list = NULL;
					}

					rc = find_op_member(ref, 
						&trail_inc,	// OP REF TRAIL we are building
						&refInfo_inc,
						&current_id, 
						&current_type,		// current item info
						info_ptr, 
						info_inc,				// current REF INFO data
						pdb,		// MEMBER binary to search
						depinfo, 
						env_info, 
						var_needed, 
						options);	// standard inputs
				}
			}
			break;

		case VIA_RECORD_REF:

			/*
			 * table request change item id and member name to
			 * subindex
			 */


			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id,
				info_ptr->val.member, &subindex,env_info,&desc_ref);

			if (rc != DDS_SUCCESS) {
				if (options & RESOLVE_TRAIL)
				{
					ddl_free_op_ref_trail(ref);
				}
				return rc;
			}

			if (options & RESOLVE_OP_REF) 
			{	/* save previous id and type */
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = subindex;
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL)) {

				if(env_info->handle_type==ENV_INFO::DeviceHandle)
				{
					current_id = desc_ref.id;
					current_type = desc_ref.type;
				}
				else
				{
					rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir) ;
					if (rc != SUCCESS) {
			            ddl_free_op_ref_trail(ref);
						return(rc) ;
					}
					it = &flat_device_dir->item_tbl;

					/*
					 * table request item id and subindex to item table element
					 */
					rc = tr_id_si_to_ite(it, bt_elem, current_id, subindex, &it_elem);
					if (rc != DDS_SUCCESS) {
			            ddl_free_op_ref_trail(ref);
						return rc;
					}
					current_id = ITE_ITEM_ID(it_elem);
					current_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);
				}


				if (options & RESOLVE_TRAIL) {

					ref->trail[trail_inc].element = info_ptr->val.member;
					++trail_inc;
					ref->trail[trail_inc].id = current_id;
					ref->trail[trail_inc].type = current_type;
				}
			}
			break;

		case VIA_LIST_REF:
			if (options & RESOLVE_OP_REF)
			{								/* save previous id and type */
				ref->op_ref_type = COMPLEX_TYPE;
				if( refInfo_inc == 0 )
				{
					// First Array in the reference
					ref->op_ref_type = COMPLEX_TYPE;
					ref->op_info_list.limit = ref_info->count;
					ref->op_info_list.list = (OP_REF_INFO*) malloc((size_t) ref->op_info_list.limit * sizeof( OP_REF_INFO ));
				
					ref->op_info_list.list[refInfo_inc].id = current_id;				
					
				}
				// First Array in the reference plus all others if they exist( i.e. List/Array of Collection of Array or List of List of Collection )
				ref->op_info_list.list[refInfo_inc].type = current_type;
				ref->op_info_list.list[refInfo_inc].member = (SUBINDEX) info_ptr->val.index;
				refInfo_inc++;
				
				DDI_BLOCK_SPECIFIER bs = {0};
				bs.type = DDI_BLOCK_HANDLE;
				bs.block.handle = env_info->block_handle;

				nsEDDEngine::FDI_ITEM_SPECIFIER is = {nsEDDEngine::FDI_ITEM_ID, 0};
				is.item.id = current_id;

				DDI_GENERIC_ITEM gi = {0};

				nsEDDEngine::AttributeNameSet mask = nsEDDEngine::type_definition;

				rc = ddi_get_item(&bs, &is, env_info, mask, &gi);
				if(rc != SUCCESS)
				{
					delete (::nsEDDEngine::ItemBase *)gi.item;
					return rc;
				}
				nsEDDEngine::FLAT_LIST* fl = (nsEDDEngine::FLAT_LIST *)gi.item;

				//assign the item id of the list TYPE to our current id
				ref->op_info_list.list[refInfo_inc].id = is.item.id = fl->type;

				//look up the data type of the list (Variable, Array, Collection)
				rc = ddi_get_type(env_info, &bs, &is, &ref->op_info_list.list[refInfo_inc].type);
				if( rc != SUCCESS )
				{
					delete fl;
					return rc;
				}
				else
				{
					ref->op_info_list.list[refInfo_inc].member = 0;
					ref->op_info_list.count = refInfo_inc + 1;
				}
				delete fl;
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL) || (options & RESOLVE_OP_REF))
			{
				nsEDDEngine::FLAT_LIST list;// = {0};

				// Fetch the binary
				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::type_definition; //(unsigned long) LIST_TYPE

				rc = resolve_fetch(env_info, current_id, mask, &list,
					(unsigned short) LIST_ITYPE, env_info->block_handle);

				if (rc == DDL_SUCCESS)
				{
					desc_ref = {0};

					cur_depbin = list.depbin->db_type;

					rc = eval_attr_desc_ref(cur_depbin->bin_chunk, cur_depbin->bin_size,
						&desc_ref, depinfo, env_info, var_needed);

					if (rc != DDL_SUCCESS)
					{
			            ddl_free_op_ref_trail(ref);
						return rc;
					}

					current_id = desc_ref.id;
					current_type = desc_ref.type;

					if (options & RESOLVE_TRAIL)
					{
						ref->trail[trail_inc].element = info_ptr->val.index;
						++trail_inc;
						ref->trail[trail_inc].id = current_id;
						ref->trail[trail_inc].type = current_type;
					}
				}
			}
			break;

		case VIA_ARRAY_REF:

			if (options & RESOLVE_OP_REF)
			{								/* save previous id and type */
				ref->op_ref_type = COMPLEX_TYPE;
				if( refInfo_inc == 0 )
				{
					// First Array in the reference
					ref->op_ref_type = COMPLEX_TYPE;
					ref->op_info_list.limit = ref_info->count;
					ref->op_info_list.list = (OP_REF_INFO*) malloc((size_t) ref->op_info_list.limit * sizeof( OP_REF_INFO ));
				
					ref->op_info_list.list[refInfo_inc].id = current_id;				
					
				}
				// First Array in the reference plus all others if they exist( i.e. List/Array of Collection of Array or List of List of Collection )
				ref->op_info_list.list[refInfo_inc].type = current_type;
				ref->op_info_list.list[refInfo_inc].member = (SUBINDEX) info_ptr->val.index;
				refInfo_inc++;
				
				DDI_BLOCK_SPECIFIER bs = {0};
				bs.type = DDI_BLOCK_HANDLE;
				bs.block.handle = env_info->block_handle;

				nsEDDEngine::FDI_ITEM_SPECIFIER is = {nsEDDEngine::FDI_ITEM_ID, 0};
				is.item.id = current_id;

				DDI_GENERIC_ITEM gi = {0};

				nsEDDEngine::AttributeNameSet mask = nsEDDEngine::type_definition;
				rc = ddi_get_item(&bs, &is, env_info, mask, &gi);
				if(rc != SUCCESS)
				{
					delete ((::nsEDDEngine::ItemBase *)gi.item);
					return rc;
				}
				nsEDDEngine::FLAT_ARRAY* fa = (nsEDDEngine::FLAT_ARRAY *)gi.item;

				//assign the item id of the list TYPE to our current id
				ref->op_info_list.list[refInfo_inc].id = is.item.id = fa->type;

				//look up the data type of the list (Variable, Array, Collection)
				rc = ddi_get_type(env_info, &bs, &is, &ref->op_info_list.list[refInfo_inc].type);
				if( rc != SUCCESS )
				{
					delete fa;
					return rc;
				}
				else
				{
					if( ref->op_info_list.list[refInfo_inc].type == VARIABLE_ITYPE )
					{
						ref->op_info_list.list[refInfo_inc].member = 0;
						ref->op_info_list.count = refInfo_inc + 1;
					}
				}
				delete fa;
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL)) {

				if(env_info->handle_type==::ENV_INFO::HandleType::DeviceHandle)
				{
					rc = pConnectionMgr->get_adt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir);
				}
				else
				{
					rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir);
				}

				if (rc != SUCCESS) {
        			ddl_free_op_ref_trail(ref);
					return(rc) ;
				}

				it = &flat_device_dir->item_tbl;

				// translate item id and subindex to item
				// table element

				rc = tr_id_si_to_ite(it, bt_elem, current_id,	// Tables require a 1-based index so incr
										(SUBINDEX) info_ptr->val.index+1, &it_elem);

				if (rc != DDS_SUCCESS) 
                {
                    MEMBER m;
					DDI_GENERIC_ITEM gi = {0};
                    m.ref.id = current_id;
                    m.ref.type = ARRAY_ITYPE;

                    rc = rslv_get_one_local(env_info, &m, &gi);

                    if (rc == DDS_SUCCESS)
                    {
						::nsEDDEngine::FLAT_ARRAY *fa=(::nsEDDEngine::FLAT_ARRAY *)gi.item;

                        current_id = fa->type;

						nsEDDEngine::FDI_ITEM_SPECIFIER is = {nsEDDEngine::FDI_ITEM_ID, 0};
						is.item.id = current_id;

						DDI_BLOCK_SPECIFIER bs = {0};
						bs.type = DDI_BLOCK_HANDLE;
						bs.block.handle = env_info->block_handle;

						rc = ddi_get_type(env_info, &bs, &is, &current_type);

						delete ((nsEDDEngine::ItemBase *)gi.item);
                       // ddi_clean_item(&gi);
                    }
                    else
					{
						delete ((nsEDDEngine::ItemBase *)gi.item);
            			ddl_free_op_ref_trail(ref);
    					return rc;
					}
				}
                else
                {
				    current_id = ITE_ITEM_ID(it_elem);
				    current_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);
                }

				if (options & RESOLVE_TRAIL) {

					ref->trail[trail_inc].element = info_ptr->val.index;
					++trail_inc;
					ref->trail[trail_inc].id = current_id;
					ref->trail[trail_inc].type = current_type;
				}
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/

		case BLOCK_CHAR_REF:
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = 0;
				ref->trail[trail_inc].type = BLOCK_CHAR_ITYPE;
				trail_inc++;
			}

			/*
			 * table request characteristic record to item id
			 */

			rc = tr_resolve_char_rec_to_ddid(env_info, env_info->block_handle, bt_elem, &current_id);
			if (rc != DDS_SUCCESS) {
    			ddl_free_op_ref_trail(ref);
				return rc;
			}

			/*
			 * table request item id and member name to subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id,
				info_ptr->val.member, &subindex,env_info,NULL);
			if (rc != DDS_SUCCESS) {
    			ddl_free_op_ref_trail(ref);
				return rc;
			}

			if (options & RESOLVE_OP_REF) {	/* save previous id and type */
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = RECORD_ITYPE;
				ref->op_info.member = subindex;
			}
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = RECORD_ITYPE;
				ref->trail[trail_inc].element = (unsigned long) subindex;
				trail_inc++;
			}

			/*
			 * get item table
			 */

			rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, 
					(void **)&flat_device_dir) ;
			if (rc != SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return(rc) ;
			}
			it = &flat_device_dir->item_tbl;

			/*
			 * table request characteristic to item table element
			 */

			rc = tr_characteristics_to_ite(it, bt_elem, subindex, &it_elem);
			if (rc != DDS_SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return rc;
			}

			current_id = ITE_ITEM_ID(it_elem);
			current_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);

			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
				ref->trail[trail_inc].element = 0;
			}
			break;

        case VIA_PARAM_LIST_REF:
		case VIA_LOCAL_PARAM_REF:
		case VIA_PARAM_REF:

			if(env_info->handle_type==ENV_INFO::DeviceHandle)
			{
				if (options & RESOLVE_TRAIL)
				{
				    ddl_free_op_ref_trail(ref);
				}
				return DDI_INVALID_HANDLE_TYPE;
			}

			if (options & RESOLVE_TRAIL) {
                int new_type = 0;

				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = info_ptr->val.member;

                switch(info_ptr->type)
                {
                case VIA_PARAM_LIST_REF:
					new_type = PARAM_LIST_ITYPE;
                    break;
                case VIA_PARAM_REF:
					new_type = PARAM_ITYPE;
                    break;
                case VIA_LOCAL_PARAM_REF:
					new_type = LOCAL_PARAM_ITYPE;
                    break;
                 default:
					 ASSERT(0);
			        ddl_free_op_ref_trail(ref);
                    return DDL_ENCODING_ERROR;
                }
                ref->trail[trail_inc].type = (ITEM_TYPE)new_type;
				trail_inc++;
			}

			/*
			 * table request member name to item id and type
			 */
            if (info_ptr->type != VIA_LOCAL_PARAM_REF)
            {
    			rc = tr_resolve_name_to_id_type(env_info, env_info->block_handle,
	    			bt_elem, info_ptr->val.member, &current_id, &current_type, false);
            }
            else
            {
                DESC_REF dr;
                rc = rslv_find_local(env_info, info_ptr->val.member, &dr);
                if (!rc)
                {
                    current_id = dr.id;
                    current_type = dr.type;
                }
            }

			if (rc != DDS_SUCCESS) 
			{
				// Need to look at all early returns to see if memory needs to be cleaned up
				ddl_free_op_ref_trail(ref);
				return rc;
			}

			if (options & RESOLVE_OP_REF) {
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case VIA_VAR_LIST_REF:

			/*
			 * table request item id and member name to item id and type
			 */

			rc = tr_resolve_lid_pname_to_id_type(env_info, env_info->block_handle,
				bt_elem, current_id, info_ptr->val.member,
				&next_id, &current_type);
			if (rc != DDS_SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return rc;
			}

			current_id = next_id;

			if (options & RESOLVE_OP_REF) {
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].element = info_ptr->val.member;
				++trail_inc;
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case VIA_BLOCK_REF:
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = 0;
				ref->trail[trail_inc].type = BLOCK_CHAR_ITYPE;
				trail_inc++;
			}

			/*
			 * table request characteristic record to item id
			 */

			rc = tr_resolve_char_rec_to_ddid(env_info, env_info->block_handle, bt_elem, &current_id);
			if (rc != DDS_SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return rc;
			}

			/*
			 * table request item id and member name to subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id,
				info_ptr->val.member, &subindex,env_info,NULL);
			if (rc != DDS_SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return rc;
			}

			if (options & RESOLVE_OP_REF) {	/* save previous id and type */
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = RECORD_ITYPE;
				ref->op_info.member = subindex;
			}
			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = RECORD_ITYPE;
				ref->trail[trail_inc].element = (unsigned long) subindex;
				trail_inc++;
			}

			/*
			 * get item table
			 */

			rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, 
					(void **)&flat_device_dir) ;
			if (rc != SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return(rc) ;
			}
			it = &flat_device_dir->item_tbl;

			/*
			 * table request characteristic to item table element
			 */

			rc = tr_characteristics_to_ite(it, bt_elem, subindex, &it_elem);
			if (rc != DDS_SUCCESS) {
    			ddl_free_op_ref_trail(ref);
				return rc;
			}

			current_id = ITE_ITEM_ID(it_elem);
			current_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);

			if (options & RESOLVE_TRAIL) {
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
				ref->trail[trail_inc].element = 0;
			}
			break;

		case VIA_XBLOCK_REF:
			rc = tr_resolve_name_to_id_type(env_info, env_info->block_handle,
	    		bt_elem, info_ptr->val.member, &current_id, &current_type, 0);

			if (rc != DDS_SUCCESS) {
			    ddl_free_op_ref_trail(ref);
				return rc;
			}

			if (options & RESOLVE_OP_REF) {
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

		case VIA_ATTRIBUTE_REF:
			{
				switch(info_ptr->val.index)
				{
				case ::nsEDDEngine::AttributeName::charts:				/* Block chart members */
				case ::nsEDDEngine::AttributeName::files:				/* Block file members */
				case ::nsEDDEngine::AttributeName::graphs:				/* Block graph members */
				case ::nsEDDEngine::AttributeName::grids:				/* Block grid members */
				case ::nsEDDEngine::AttributeName::menus:				/* Block menu members */
				case ::nsEDDEngine::AttributeName::methods:				/* Block method members */
				case ::nsEDDEngine::AttributeName::plugins:				/* Block plugins members */
				case ::nsEDDEngine::AttributeName::parameters:			/* Block parameters members */
				case ::nsEDDEngine::AttributeName::local_parameters:    /* Block local_parameters members */
				case ::nsEDDEngine::AttributeName::lists:				/* Block list members */
				case ::nsEDDEngine::AttributeName::parameter_lists:		/* Block param_list members */
				case ::nsEDDEngine::AttributeName::characteristics:		/* Block characteristics record */
					
					if (options & RESOLVE_TRAIL) {
						int new_type = 0;

						ref->trail[trail_inc].id = 0;
						ref->trail[trail_inc].element = info_ptr->val.index;

						switch(info_ptr->val.index)
						{
						case ::nsEDDEngine::AttributeName::charts:
							new_type = CHART_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::files:
							new_type = FILE_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::graphs:
							new_type = GRAPH_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::grids:
							new_type = GRID_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::menus:
							new_type = MENU_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::methods:
							new_type = METHOD_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::plugins:
							new_type = PLUGIN_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::parameters:
							new_type = PARAM_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::local_parameters:
							new_type = LOCAL_PARAM_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::lists:
							new_type = LIST_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::parameter_lists:
							new_type = PARAM_LIST_ITYPE;
							break;
						case ::nsEDDEngine::AttributeName::characteristics:	/* Characteristics record */
							new_type = BLOCK_CHAR_ITYPE;
							rc = tr_resolve_char_rec_to_ddid(env_info, env_info->block_handle, bt_elem, &current_id);
							current_type = RECORD_ITYPE;
							break;

						default:
							ASSERT(0);
			                ddl_free_op_ref_trail(ref);
							return DDL_ENCODING_ERROR;
						}

						ref->trail[trail_inc].type = (ITEM_TYPE)new_type;
						trail_inc++;
					}
					break;

				default:
					{
						DDL_UINT which = 0;		// Not used for strings, ignore

						desc_ref.id = current_id;
						desc_ref.type = current_type;

						rc = get_attribute_value((::nsEDDEngine::AttributeName)info_ptr->val.index,
							which,
							&desc_ref,
							&ref->expr,
							depinfo, 
							env_info, 
							var_needed);


						current_id = 0; 
						current_type = get_constant_itype(&ref->expr);

						if (rc == DDL_SUCCESS)
						{
							if (options & RESOLVE_DESC_REF)
							{
								ref->desc_id = 0;
								ref->desc_type = current_type;
							}
						}
					}
					break;
				}

			if (options & RESOLVE_OP_REF) {
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			}
			break;

		default:
			ASSERT(0);
			rc = DDL_ENCODING_ERROR;
			CRASH_RET(DDL_ENCODING_ERROR);
			/* NOTREACHED */
			break;
		}

		if (rc != DDL_SUCCESS) {
			ddl_free_op_ref_trail(ref);
			return rc;

		}
	}

	if (options & RESOLVE_TRAIL) {

		/* finish off the trail */
		++trail_inc;
		ref->trail_count = trail_inc;
	}

	if (options & RESOLVE_DESC_REF) {

		ref->desc_id = current_id;
		ref->desc_type = current_type;
	}

	if (ref->op_ref_type == COMPLEX_TYPE)
	{
		if (   (ref->op_info_list.count == 2)  // Check to see if simple VARIABLE Array or LIST
			&& (   (ref->op_info_list.list[0].type == ARRAY_ITYPE)
				|| (ref->op_info_list.list[0].type == LIST_ITYPE)
				)
			&& (ref->op_info_list.list[1].type == VARIABLE_ITYPE)
			)
		{
			ref->op_ref_type = STANDARD_TYPE;
			ref->op_info.id     = ref->op_info_list.list[0].id;
			ref->op_info.member = ref->op_info_list.list[0].member;
			ref->op_info.type   = ref->op_info_list.list[0].type;
			ddl_free_op_ref_info_list(&ref->op_info_list);				// release the old list
		}
	}

	return rc;
}
