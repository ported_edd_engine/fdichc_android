/*
 * @(#) $Id: ddi_tab.h,v 1.3 1995/05/23 14:29:37 kenta Exp $
 */

#ifndef DDI_TAB_H
#define DDI_TAB_H
 
#include "dds_upcl.h"
#include "ddi_lib.h"

#ifdef __cplusplus
    extern "C" {
#endif



extern int compare_bt_elem P((BLK_TBL_ELEM *,BLK_TBL_ELEM *));

extern int compare_ptoc_t8_elem(PTOC_TBL_8_ELEM *, PTOC_TBL_8_ELEM *);

extern int conv_block_spec P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
                              BLOCK_HANDLE *));


/*	
 * 	function prototypes needed for conversion to dd reference 
 */

extern int	tr_gdr_block_handle	P((ENV_INFO *, BLOCK_HANDLE,DD_REFERENCE *)) ;

extern int 	tr_gdr_device_type_handle_blockname P((ENV_INFO *, DEVICE_TYPE_HANDLE, ITEM_ID, DD_REFERENCE * )) ;

extern int	tr_block_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, ITEM_TBL_ELEM **)) ;

extern int	tr_id_to_ite P((ENV_INFO *, ITEM_TBL *, ITEM_ID, ITEM_TBL_ELEM **)) ;

extern int	tr_name_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *, ITEM_ID, ITEM_TBL_ELEM **)) ;

extern int	tr_param_offset_to_op_index P((ENV_INFO *, BLOCK_HANDLE,int,OBJECT_INDEX *));

extern int	tr_op_index_to_ite P((ENV_INFO *, BLOCK_HANDLE, ITEM_TBL *, BLK_TBL_ELEM *,OBJECT_INDEX, ITEM_TBL_ELEM **)) ;

extern int	tr_param_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *,int, ITEM_TBL_ELEM **)) ;

extern int	tr_param_list_to_ite P((ITEM_TBL *,BLK_TBL_ELEM *,int, ITEM_TBL_ELEM **)) ;


extern int	tr_name_si_to_ite P((ITEM_TBL *, BLK_TBL_ELEM *,ITEM_ID,SUBINDEX, ITEM_TBL_ELEM **)) ;

extern int	tr_op_index_si_to_ite P((ENV_INFO *, BLOCK_HANDLE,ITEM_TBL *,BLK_TBL_ELEM *,OBJECT_INDEX,SUBINDEX, ITEM_TBL_ELEM **));

/*	
 * 	function prototypes needed for conversion to type and size 
 */
extern int	tr_param_to_size P((ENV_INFO *, BLOCK_HANDLE,BLK_TBL_ELEM *,int,TYPE_SIZE *)) ;
extern int	tr_param_si_to_size P((ENV_INFO *, BLOCK_HANDLE,BLK_TBL_ELEM *,int,SUBINDEX,TYPE_SIZE *)) ;
extern int	tr_char_to_size P((BLK_TBL_ELEM *,SUBINDEX,TYPE_SIZE *)) ;

extern int	tr_block_tag_to_block_handle P((ENV_INFO *,BLOCK_TAG, BLOCK_HANDLE *));

/*
 * Prototypes needed for Relation Conveniences
 */

extern int	tr_id_to_bint_offset P((BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_name_to_bint_offset P((BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_op_index_to_bint_offset P((ENV_INFO *, BLOCK_HANDLE, BLK_TBL_ELEM *,OBJECT_INDEX, int *)) ;
extern int	tr_op_index_si_to_bint_offset P((ENV_INFO *, BLOCK_HANDLE, BLK_TBL_ELEM *,OBJECT_INDEX, int *,SUBINDEX)) ;
extern int	tr_param_to_bint_offset P((BLK_TBL_ELEM *,int, int *)) ;
extern int	tr_char_to_bint_offset P((BLK_TBL_ELEM *, int *)) ;

extern int	tr_id_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,ITEM_ID, int *)) ;
extern int	tr_name_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,ITEM_ID,int *)) ;
extern int	tr_param_si_to_bint_offset P((BLK_TBL_ELEM *,SUBINDEX,int, int *)) ;
extern int	tr_bint_offset_to_rt_offset P((BLK_TBL_ELEM *,int,int *)) ;
extern int	tr_bint_offset_si_to_rt_offset P((BLK_TBL_ELEM *, SUBINDEX, int, int *)) ;

#ifdef __cplusplus
    }
#endif

#endif				/* DDI_TAB_H */

