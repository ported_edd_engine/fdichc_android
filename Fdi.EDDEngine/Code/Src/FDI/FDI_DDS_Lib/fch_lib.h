/**
 *	@(#) $Id: fch_lib.h,v 1.5 2012/08/30 22:33:29 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains data structures for the fetch interface functions
 */

#pragma once

#include "rtn_code.h"
#include "table.h"
#include "tags_sa.h"
#include "dds_upcl.h"
#include "env_info.h"
#include "nsEDDEngine/Flats.h"
#include "nsEDDEngine/Table.h"
#ifdef DEBUG
#include "dds_chk.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif


typedef struct tag_SCRATCH_PAD {
	unsigned long   used;	/* Amount of scratchpad memory consumed */
	unsigned long   size;	/* Total amount of scratchpad memory */
	unsigned char  *pad;	/* Pointer to beginning of scratchpad
							   memory area */
}               SCRATCH_PAD;



/************************************************
 *                                              *
 *     Fetch Interface Function Definitions     *
 *                                              *
 ************************************************/


/*
 *	General Interface Function Definitions
 */

extern int fch_item P((ENV_INFO *, DEVICE_HANDLE, ITEM_ID, ::nsEDDEngine::AttributeNameSet, ::nsEDDEngine::ItemBase *, ITEM_TYPE));


/*
 *	ROD Interface Function Definitions
 */

extern int fch_rod_item P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, ::nsEDDEngine::AttributeNameSet, ::nsEDDEngine::ItemBase *, ITEM_TYPE));

extern int fch_rod_dir_spad_size P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, unsigned long *, unsigned long, unsigned short));

extern int fch_rod_device_dir P((ENV_INFO *, ROD_HANDLE, SCRATCH_PAD *, unsigned long *, unsigned long, nsEDDEngine::BIN_DEVICE_DIR *));

extern int fch_rod_block_dir P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long *, unsigned long, nsEDDEngine::BIN_BLOCK_DIR *));

extern int get_rod_object P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, OBJECT **, SCRATCH_PAD *));
extern int get_rod_object_size P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, int *));

#ifdef __cplusplus
    }
#endif

