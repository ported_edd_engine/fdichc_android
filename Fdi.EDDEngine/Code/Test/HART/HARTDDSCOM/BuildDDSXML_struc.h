#pragma once

typedef struct{

	CString			label;
	unsigned long	class_num;
	CString			class_attr;
	CString			kind_as_string;
	int				type;
	CString			type_as_string;
	int				kind;
	CString			name;
	unsigned long	id;
	unsigned long minval;
	unsigned long	maxval;

}XML_VAR_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_MENU_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_EDIT_DISP_ELEM;

typedef struct{

	CString			label;
	unsigned long	type;
	CString			type_as_string;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_METHOD_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_ITEM_ARRY_ELEM;

typedef struct{

	CString			label;
	CString			type;
	CString			type_as_string;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_ARRAY_ELEM;

typedef struct{

	CString			label;
	unsigned long	type;
	CString			type_as_string;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_CHART_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_GRAPH_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_GRID_ELEM;

typedef struct{

	CString			label;
	unsigned long	type;
	CString			type_as_string;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_WAVEFORM_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_IMAGE_ELEM;

typedef struct{

	unsigned long	num;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_CMD_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;

}XML_AXIS_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_COLLECT_ELEM;

typedef struct{

	CString			label;
	int				kind;
	CString			kind_as_string;
	CString			name;
	unsigned long	id;


}XML_RECORD_ELEM;

struct CLASSSTRUCT{

	unsigned long long		num;
	char *					str;
};
#define NUMELEM(x)		(sizeof(x)/sizeof(x[0]))

