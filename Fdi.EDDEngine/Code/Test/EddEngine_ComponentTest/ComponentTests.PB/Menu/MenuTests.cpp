#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_reading_a_menu_with_submenus_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            static ITEM_ID* test_menu_page_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                test_menu_page_item_id = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_page", test_menu_page_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_page_item_id;
                delete test_menu_item_id;
            }
            
            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_11_sub_menus()
            {
                Assert::AreEqual(11u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_third_sub_menu()
            {
                Assert::AreEqual(*test_menu_page_item_id, test_menu->items.list[2].ref.op_info.id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_WINDOW_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_offline_access_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            static ITEM_ID* test_menu_table_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_offline", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_menu_offline", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                test_menu_table_item_id = new ITEM_ID();
                returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_menu_table_offline", test_menu_table_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_table_item_id;
                delete test_menu_item_id;
            }
            
            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_1_sub_menu()
            {
                Assert::AreEqual(1u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_first_sub_menu()
            {
                Assert::AreEqual(*test_menu_table_item_id, test_menu->items.list[0].ref.op_info.id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_WINDOW_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_access_offline()
            {
                Assert::AreEqual((int)nsEDDEngine::OFFLINE_ACCESS, (int)test_menu->access);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_online_access_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            static ITEM_ID* test_menu_table_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_online", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_menu_online", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                test_menu_table_item_id = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_table_online", test_menu_table_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_table_item_id;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_1_sub_menu()
            {
                Assert::AreEqual(1u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_first_sub_menu()
            {
                Assert::AreEqual(*test_menu_table_item_id, test_menu->items.list[0].ref.op_info.id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_WINDOW_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }
            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_access_offline()
            {
                Assert::AreEqual((int)nsEDDEngine::ONLINE_ACCESS, (int)test_menu->access);
            }
        };   

        [TestClass]
        public ref class When_reading_a_menu_that_is_a_submenu_from_a_menu_with_online_access_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();
                
                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_table_online", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_table_online", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_3_sub_menus()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_TABLE_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }
            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_unknown_access()
            {
                Assert::AreEqual((int)nsEDDEngine::UNKNOWN_ACCESS, (int)test_menu->access);
            }
        };   

        [TestClass]
        public ref class When_reading_a_menu_that_is_a_submenu_from_a_menu_with_offline_access_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_table_offline", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_table_offline", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_3_sub_menus()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_TABLE_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_access_offline()
            {
                Assert::AreEqual((int)nsEDDEngine::UNKNOWN_ACCESS, (int)test_menu->access);
            }
        };   

        [TestClass]
        public ref class When_reading_a_menu_with_dialog_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_dialog", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_dialog", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }
            
            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(2u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_DIALOG_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }
            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_window_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_window", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_menu_window", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }
            
            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(2u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_WINDOW_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_page_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_page", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"test_menu_page", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_PAGE_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_group_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_group", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_group", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_GROUP_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_menu_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode= engine->Instance->GetItemIdFromSymbolName(L"test_menu", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_MENU_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_table_style_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID* test_menu_item_id;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_table", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                // get the item ids for all parameters
                test_menu_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_menu_table", test_menu_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_menu_item_id;
            }
            
            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_item_id_for_the_main_menu()
            {
                Assert::AreEqual(*test_menu_item_id, test_menu->id);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_2_items()
            {
                Assert::AreEqual(3u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_correct_menu_style()
            {
                Assert::AreEqual((int)FDI_TABLE_STYLE_TYPE,(int)test_menu->style);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_an_invalid_menu_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_invalid", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_invalid()
            {
                Assert::AreEqual((int)nsEDDEngine::False, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_visible()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_an_invisible_menu_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_invisible", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_invisible()
            {
                Assert::AreEqual((int)nsEDDEngine::False, (int)test_menu->visibility);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_static_text_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000E.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_menu_text", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_be_valid()
            {
                Assert::AreEqual((int)nsEDDEngine::True, (int)test_menu->valid);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_17_elements_in_item_list()
            {
                Assert::AreEqual(17u,(UINT32) test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_text_in_the_first_element_in_the_item_list()
            {
                Assert::AreEqual("Next is a rowbreak.",gcnew String(test_menu->items.list[0].ref.expr.val.s.c_str()));
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_text_in_the_last_element_in_the_item_list()
            {               
                Assert::AreEqual("This is the end.",gcnew String(test_menu->items.list[16].ref.expr.val.s.c_str()));
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_second_element_in_the_item_list()
            {        
                Assert::AreEqual((int)ITYPE_SEPARATOR,(int)test_menu->items.list[11].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_sixth_element_in_the_item_list()
            {       
                Assert::AreEqual((int)ITYPE_SEPARATOR,(int)test_menu->items.list[11].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_twelth_element_in_the_item_list()
            {        
                Assert::AreEqual((int)ITYPE_SEPARATOR,(int)test_menu->items.list[11].ref.op_info.type);
            }

            [TestMethod, TestCategory("MenuTests")]
            void It_should_have_the_expected_type_for_the_first_element_in_the_item_list()
            {         
                Assert::AreEqual((int)ITYPE_STRING_LITERAL,(int)test_menu->items.list[0].ref.op_info.type);
            }
        };

        [TestClass]
        public ref class When_reading_a_menu_with_bitmasked_bitenums_in_PB
        {
        private:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID expectedItemId;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0010.DDL.bin");
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                ITEM_ID* bufferItemId = new ITEM_ID;
                engine->Instance->GetItemIdFromSymbolName(L"test_bitenum", bufferItemId);
                expectedItemId = *bufferItemId;
                delete bufferItemId;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void EDDEngine_should_return_a_FLAT_MENU()
            {
                Assert::IsFalse(nullptr == test_menu);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_menu_should_have_three_items()
            {
                Assert::AreEqual((UInt16)3, test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_first_item_is_an_unbitmasked_ITYPE_VARIABLE()
            {
                Assert::AreEqual((unsigned long) 0, test_menu->items.list[0].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_VARIABLE, (int)(test_menu->items.list[0].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_second_item_is_an_ITYPE_VARIABLE_with_bitmask_of_03()
            {
                Assert::AreEqual((int) ITYPE_ENUM_BIT, (int)(test_menu->items.list[1].ref.desc_type));
                Assert::AreEqual(0x03u, test_menu->items.list[1].ref.desc_bit_mask);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_third_item_is_an_ITYPE_VARIABLE_with_bitmask_FC()
            {
                Assert::AreEqual((int) ITYPE_ENUM_BIT, (int)(test_menu->items.list[1].ref.desc_type));
                Assert::AreEqual(0xFCu, test_menu->items.list[2].ref.desc_bit_mask);
            }

            [TestMethod, TestCategory("MenuBitmask")]
            void All_three_items_reference_test_bitEnum()
            {
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[0].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[1].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[2].ref.desc_id));
            }
        };

    }
}
