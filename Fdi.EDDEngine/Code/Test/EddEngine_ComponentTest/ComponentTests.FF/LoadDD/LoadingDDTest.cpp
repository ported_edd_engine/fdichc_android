#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_a_example_HTK_DD_for_FF6
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestExampleDDFF6\\10ff00\\1000\\02a0.ff6");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x1000u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x02u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0xa0u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x10FF00u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_2000_in_FF6
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
				engine = gcnew EddEngineController("FF\\TestExampleDDFF6\\000310\\2000\\0801.ff6");
                engine->Initialize();
                
                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x2000u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x08u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x01u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x0310u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_4089_in_FF6
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
				engine = gcnew EddEngineController("FF\\TestExampleDDFF6\\524149\\4089\\0107.ff6");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x4089u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x01u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x07u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x524149u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_0200_in_FF6
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
				engine = gcnew EddEngineController("FF\\TestExampleDDFF6\\545758\\0200\\0203.ff6");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x0200u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x02u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x03u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x545758u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_HTK_DD_for_FF5
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;


            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestExampleDDFF5\\10ff00\\1000\\0206.ff5");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x1000u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x02u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x06u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x10FF00u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_0c02_in_FF5
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestExampleDDFF5\\001151\\0c02\\0201.ff5");

                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x0c02u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x02u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x01u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x1151u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_0120_in_FF5
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestExampleDDFF5\\001151\\0120\\0104.ff5");

                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x0120u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x01u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x04u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x1151u,(UINT32) header->device_id.manufacturer);
            }
        };

        [TestClass]
        public ref class When_loading_a_example_DD_for_device_0130_in_FF5
        {
        public:
            static EddEngineController^ engine;
            static EDDFileHeader* header;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestExampleDDFF5\\001151\\0130\\0102.ff5");
                engine->Initialize();

                header = new EDDFileHeader();
                engine->Instance->GetEDDFileHeader(header);

            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete header;
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_type()
            {
                Assert::AreEqual(0x0130u,(UINT32) header->device_id.device_type);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_device_revision()
            {
                Assert::AreEqual(0x01u,(UINT32) header->device_id.device_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_dd_revision()
            {
                Assert::AreEqual(0x02u,(UINT32) header->device_id.dd_revision);
            }

            [TestMethod, TestCategory("LoadDDTest")]
            void It_should_have_loaded_the_correct_manufacturer_id()
            {
                Assert::AreEqual(0x1151u,(UINT32) header->device_id.manufacturer);
            }
        };

    } // namespace FF
} // namespace ComponentTests
