#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_checking_a_variable_with_only_mandatory_attributes_for_PB
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_var_mandatory_item_id;
            static FLAT_VAR* test_var_mandatory;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0009.DDL.bin");                
                engine->Initialize();

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("var_only_mandetory_values", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_var_mandatory = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_var_mandatory_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"var_only_mandetory_values", test_var_mandatory_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_var_mandatory_item_id;
                delete genericItem;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_of_type_integer()
            {
                Assert::AreEqual((int) VT_INTEGER, (int)test_var_mandatory->type_size.type);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_with_class_local()
            {
                Assert::AreEqual((int) CT_LOCAL_B, (int)test_var_mandatory->class_attr);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(test_var_mandatory->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(test_var_mandatory->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_be_a_valid_variable()
            {
                Assert::IsTrue(test_var_mandatory->valid == True);
            }
        };

        [TestClass]
        public ref class When_checking_a_variable_with_all_attributes_for_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_VAR* test_var_all_values;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0009.DDL.bin");                
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("var_all_values", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_var_all_values = dynamic_cast<FLAT_VAR*>(genericItem->item);

                ITEM_ID test_var_all_values_item_id;
                int returnCode =  engine->Instance->GetItemIdFromSymbolName(L"var_all_values", &test_var_all_values_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_not_be_a_valid_variable()
            {
                Assert::IsFalse(test_var_all_values->valid == True);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_of_type_integer()
            {
                Assert::AreEqual((int) VT_INTEGER, (int)test_var_all_values->type_size.type);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_with_class_local()
            {
                Assert::AreEqual((int) CT_TUNE, (int)test_var_all_values->class_attr);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_label_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_constant_unit_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::constant_unit), "constant_unit");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_default_value_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::default_value), "default_value");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_handling_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::handling), "handling");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_help_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::help), "help");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_validity_as_mask()
            {
                Assert::IsTrue(test_var_all_values->isAvailable(nsEDDEngine::validity), "validity");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_as_constant_unit_my_unit()
            {
                Assert::AreEqual("my unit", gcnew String(test_var_all_values->constant_unit.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_handling_read()
            {
                Assert::AreEqual((int) FDI_READ_HANDLING, (int) test_var_all_values->handling);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_help_hallo()
            {
                Assert::AreEqual("hallo", gcnew String(test_var_all_values->help.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_label_abc()
            {
                Assert::AreEqual("abc", gcnew String(test_var_all_values->label.c_str()));
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_validity_false()
            {
                Assert::AreEqual((int) FALSE, (int) test_var_all_values->valid);
            }
        };

        [TestClass]
        public ref class When_getting_the_label_of_a_variable_which_is_referenced_to_a_complex_structure_PB
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_VAR* test_int;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0020.DDL.bin");                
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_int", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                test_int = dynamic_cast<FLAT_VAR*>(genericItem->item);

            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }
            

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_of_type_integer()
            {
                Assert::AreEqual((int) VT_INTEGER, (int)test_int->type_size.type);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_return_a_variable_with_class_Device()
            {
                Assert::AreEqual((int) CT_DEVICE, (int)test_int->class_attr);
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_variable_type_as_mask()
            {
                Assert::IsTrue(test_int->isAvailable(nsEDDEngine::variable_type), "variable_type");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_label_as_mask()
            {
                Assert::IsTrue(test_int->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_class_attr_as_mask()
            {
                Assert::IsTrue(test_int->isAvailable(nsEDDEngine::class_attr), "class_attr");
            }





            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_handling_as_mask()
            {
                Assert::IsTrue(test_int->isAvailable(nsEDDEngine::handling), "handling");
            }



            [TestMethod, TestCategory("VariableTest")]
            void It_should_contain_the_validity_as_mask()
            {
                Assert::IsTrue(test_int->isAvailable(nsEDDEngine::validity), "validity");
            }



            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_handling_read()
            {
                Assert::AreEqual((int) FDI_READ_HANDLING, (int) test_int->handling);
            }



            [TestMethod, TestCategory("VariableTest")]
            void It_should_provide_as_label_Volume()
            {
                Assert::AreEqual("Volume", gcnew String(test_int->label.c_str()));
            }


        };

        [TestClass, Ignore]
        public ref class BaseClassDefaultValues
        {
        protected:
            static FLAT_VAR* elementToCheck = nullptr;

        public: 
            static void SetupBase(String^ eddFilename, String^ variableName)
            {
                EddEngineController^ engine = gcnew EddEngineController(eddFilename);
                engine->Initialize();

                FDI_PARAM_SPECIFIER paramSpec;
                engine->FillParamSpecifier(variableName, &paramSpec);
                elementToCheck =  dynamic_cast<FLAT_VAR*>(engine->GetGenericItem(paramSpec.id, ITYPE_VARIABLE)->item);
            }

            static void CleanupBase()
            {
                delete elementToCheck;
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_convertible_to_FLAT_VAR()
            {
                Assert::IsFalse(elementToCheck == nullptr);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_have_a_default_value()
            {
                Assert::IsTrue(elementToCheck->isAvailable(AttributeName::default_value));
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_String_variable_defaulted_in_exact_length_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("TestString", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_short_defaulted_String_variable_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_ascii_short");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_the_default_value_defined_in_the_dd_padded_whith_whitespaces()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Test", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_too_long_defaulted_String_variable_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_ascii_long");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_string()
            {
                Assert::AreEqual((int) VariableType::VT_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_the_first_10_charachters_from_the_default_value_defined_in_the_dd()
            {
                String^ bufferForCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Too long s", bufferForCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_an_integer_variable_from_testcase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_int");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_integer()
            {
                Assert::AreEqual((int) VariableType::VT_INTEGER, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_INTEGER, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(-24ll, elementToCheck->default_value.val.i);
            }
        };

        [TestClass]
        public ref class When_reading_an_unsigned_integer_variable_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_uint");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) VariableType::VT_UNSIGNED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_an_unsigned_integer()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(42ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_an_enumerated_variable_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_enumerated");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_an_enumerated()
            {
                Assert::AreEqual((int) VariableType::VT_ENUMERATED, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_an_unsigned()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_UNSIGNED, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                Assert::AreEqual(2ull, elementToCheck->default_value.val.u);
            }
        };

        [TestClass]
        public ref class When_reading_a_float_variable_from_TestCase_0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_float");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_float()
            {
                Assert::AreEqual((int) VariableType::VT_FLOAT, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_float()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_FLOAT, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                float varToCheck = elementToCheck->default_value.val.f - 3.142f;
                Assert::IsTrue(varToCheck < 0.001 && varToCheck > -0.001);
            }
        };

        [TestClass]
        public ref class When_reading_a_double_variable_from_TestCase0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_double");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_double()
            {
                Assert::AreEqual((int) VariableType::VT_DOUBLE, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_also_be_a_double()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_DOUBLE, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                double varToCheck = elementToCheck->default_value.val.d + 2.721;
                Assert::IsTrue(varToCheck < 0.001 && varToCheck > -0.001);
            }
        };

        [TestClass]
        public ref class When_reading_a_Password_variable_defaulted_in_exact_length_from_TestCase0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_password");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_password_string()
            {
                Assert::AreEqual((int) VariableType::VT_PASSWORD, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferToCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferToCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };

        [TestClass]
        public ref class When_reading_a_Packed_ASCII_variable_defaulted_in_exact_length_from_TestCase0003 : BaseClassDefaultValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_packed_ascii");
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_be_a_password_string()
            {
                Assert::AreEqual((int) VariableType::VT_PACKED_ASCII, (int) elementToCheck->type_size.type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_a_normal_string()
            {
                Assert::AreEqual((int) EXPR::EXPR_TYPE_STRING, (int) elementToCheck->default_value.eType);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_as_defined_in_the_dd()
            {
                String^ bufferToCheck = gcnew String(elementToCheck->default_value.val.s.c_str());
                Assert::AreEqual("Teststring", bufferToCheck);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void The_default_value_should_be_of_the_same_size_as_the_FLAT_VAR()
            {
                Assert::AreEqual(elementToCheck->type_size.size, elementToCheck->default_value.size);
            }
        };


        [TestClass, Ignore]
        public ref class BaseClassVariableTypes
        {
        protected:
            static TYPE_SIZE *_expectedTypesize, *_receivedTypeSize;
        public: 
            static void SetupBase(String^ eddFilename, String^ variableName, TYPE_SIZE expected)
            {
                _expectedTypesize = new TYPE_SIZE(expected);
                _receivedTypeSize = nullptr;
                EddEngineController^ engine = gcnew EddEngineController(eddFilename);
                engine->Initialize();

                FDI_PARAM_SPECIFIER paramSpec;
                engine->FillParamSpecifier(variableName, &paramSpec);
                FLAT_VAR *elementToCheck =  dynamic_cast<FLAT_VAR*>(engine->GetGenericItem(paramSpec.id, ITYPE_VARIABLE)->item);
                _receivedTypeSize = new TYPE_SIZE(elementToCheck->type_size);
                delete elementToCheck;
            }

            static void CleanupBase()
            {
                if(_receivedTypeSize) delete(_receivedTypeSize);
                if(_expectedTypesize) delete(_expectedTypesize);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_have_the_expected_type()
            {
                Assert::AreEqual((int)_expectedTypesize->type,(int)_receivedTypeSize->type);
            }

            [TestMethod, TestCategory("DefaultValueCheck")]
            void It_should_have_the_expected_size()
            {
                Assert::AreEqual(_expectedTypesize->size, _receivedTypeSize->size);
            }
        };

        [TestClass]
        public ref class When_loading_a_DATE_parameter_from_TestCase0003 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToCheck;
                typeSizeToCheck.type = VT_EDD_DATE;
                typeSizeToCheck.size = 3;
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_date", typeSizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_TIME_VALUE_parameter_from_TestCase0003 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToCheck;
                typeSizeToCheck.type = VT_TIME_VALUE;
                typeSizeToCheck.size = 8;
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_time_value", typeSizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_DATE_AND_TIME_parameter_from_TestCase0003 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToCheck;
                typeSizeToCheck.type = VT_DATE_AND_TIME;
                typeSizeToCheck.size = 7;
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_date_and_time", typeSizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_TIME_parameter_from_TestCase0003 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToCheck;
                typeSizeToCheck.type = VT_TIME;
                typeSizeToCheck.size = 6;
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_time", typeSizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_loading_a_DURATION_parameter_from_TestCase0003 : BaseClassVariableTypes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                TYPE_SIZE typeSizeToCheck;
                typeSizeToCheck.type = VT_DURATION;
                typeSizeToCheck.size = 6;
                SetupBase("ProfiBus\\FDI_PB_Test_0003.DDL.bin", "test_duration", typeSizeToCheck);
            }

            [TestCleanup]
            void Cleanup()
            {
                CleanupBase();
            }
        };

    }
}
