// ParserBuilder.cpp //

#include "stdafx.h"
#include "SymbolTable.h"
#include "ParserBuilder.h"
#include "LexicalAnalyzer.h"
#include "BreakStatement.h"
#include "ReturnStatement.h"
#include "ContinueStatement.h"
#include "EmptyStatement.h"
#include "CompoundStatement.h"
#include "IterationStatement.h"
#include "IterationDoWhile.h"
#include "IterationFor.h"
#include "SelectionStatement.h"
#include "SwitchStatement.h"
#include "Declaration.h"
#include "ParserDeclarations.h"
#include "ELSEStatement.h"
#include "CaseStatement.h"
#include "ServiceStatement.h"
#include "RuleServiceStatement.h"

#include "ErrorDefinitions.h"


CParserBuilder::CParserBuilder()
{

}

CParserBuilder::~CParserBuilder()
{

}

//Check whether the LOOKAHEAD symbol is a FIRST of a Statement:
//	a.	Assignment Statement
//	b.	Selection Statement
//	c.	Iteration Statement
//	d.	Compound Statement
//	e.	Declaration Statement
//	f.	Else Statement
//	g.	Rule Service Statment
//	h.	Service Statement
CGrammarNode* CParserBuilder::CreateParser(
					CLexicalAnalyzer* plexAnal,
					STATEMENT_TYPE stmt_type)
{
	CGrammarNode* pNode = 0;
	CToken* pToken = 0;

	try
	{
		if((LEX_FAIL != (plexAnal->LookAheadToken(&pToken)))
			&& pToken)
		{
			
			switch(stmt_type)
			{
			case STMT_ASSIGNMENT:
				switch(pToken->GetType())
				{
				case RUL_SYMBOL:
					switch(pToken->GetSubType())
					{
					case RUL_LPAREN:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					}
					break;
				case RUL_KEYWORD:
					switch(pToken->GetSubType())
					{
					case RUL_FUNCTION:
						pNode = new CExpression;
						break;
					case RUL_OM:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					}
					break;
				case RUL_SIMPLE_VARIABLE:
				case RUL_ARRAY_VARIABLE:
				case RUL_DD_ITEM:
				case RUL_ARITHMETIC_OPERATOR:
				case RUL_LOGICAL_OPERATOR:
				case RUL_ASSIGNMENT_OPERATOR:
				case RUL_RELATIONAL_OPERATOR:
					pNode = CreateParserAssignment(plexAnal, pToken);
					break;
				case RUL_NUMERIC_CONSTANT:
					switch(pToken->GetSubType())
					{
					case RUL_REAL_CONSTANT:
					case RUL_BOOL_CONSTANT:
					case RUL_CHAR_CONSTANT:
					case RUL_INT_CONSTANT:
					case RUL_STRING_CONSTANT:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					}
					break;
				case RUL_SERVICE:
					switch(pToken->GetSubType())
					{
					case RUL_SERVICE_ATTRIBUTE:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					}
				}
				break;
			case STMT_asic:
				switch(pToken->GetType())
				{
				case RUL_SYMBOL:
					switch(pToken->GetSubType())
					{
					case RUL_SEMICOLON:
						pNode = new CEmptyStatement;
						break;
					case RUL_LPAREN:
						pNode = new CExpression;
						break;
					case RUL_LBRACK:
						pNode = new CCompoundStatement;
						break;
					case RUL_COLON:
						pNode = new CCompoundStatement;
						break;
					}
					break;
				case RUL_KEYWORD:
					switch(pToken->GetSubType())
					{
					case RUL_FUNCTION:
						pNode = new CExpression;
						break;
					case RUL_IF:
						pNode = new CSelectionStatement;
						break;
					case RUL_SWITCH:
						pNode = new CSwitchStatement;
						break;
					case RUL_RULE_ENGINE:
						pNode = new CRuleServiceStatement;
						break;
					case RUL_BREAK:
						pNode = new CBreakStatement;
						break;
					case RUL_CONTINUE:
						pNode = new CContinueStatement;
						break;
					case RUL_RETURN:
						pNode = new CReturnStatement;
						break;
					case RUL_WHILE:
						pNode = new CIterationStatement;
						break;
					case RUL_DO:
						pNode = new CIterationDoWhileStatement;
						break;
					case RUL_FOR:
						pNode = new CIterationForStatement;
						break;
					case RUL_OM:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					default:
						if (pToken->IsDeclaration())
						{
							pNode = new CDeclaration;
						}
						break;
					}
					break;
				case RUL_SIMPLE_VARIABLE:
				case RUL_ARRAY_VARIABLE:
				case RUL_DD_ITEM:
				case RUL_ARITHMETIC_OPERATOR:
				case RUL_LOGICAL_OPERATOR:
				case RUL_ASSIGNMENT_OPERATOR:
				case RUL_RELATIONAL_OPERATOR:
					pNode = CreateParserAssignment(plexAnal, pToken);
					break;
				case RUL_NUMERIC_CONSTANT:
					switch(pToken->GetSubType())
					{
					case RUL_REAL_CONSTANT:
					case RUL_BOOL_CONSTANT:
					case RUL_CHAR_CONSTANT:
					case RUL_INT_CONSTANT:
					case RUL_STRING_CONSTANT:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					}
					break;
				case RUL_SERVICE:
					switch(pToken->GetSubType())
					{
					case RUL_SERVICE_ATTRIBUTE:
						pNode = CreateParserAssignment(plexAnal, pToken);
						break;
					case RUL_SERVICE_INVOKE:
						pNode = new CServiceStatement;
						break;
					}
				}
				break;
			case STMT_DECL:
				switch(pToken->GetType())
				{
				case RUL_SYMBOL:
					switch(pToken->GetSubType())
					{
					case RUL_SEMICOLON:
						pNode = new CEmptyStatement;
						break;
					case RUL_LPAREN:
						pNode = new CExpression;
						break;
					}
					break;
				case RUL_KEYWORD:
					if (pToken->IsDeclaration())
					{
						pNode = new CDeclaration;
					}
					break;
				}
				break;
			case STMT_ITERATION:
				switch(pToken->GetType())
				{
				case RUL_KEYWORD:
					switch(pToken->GetSubType())
					{
					case RUL_WHILE:
						pNode = new CIterationStatement;
						break;
					case RUL_DO:
						pNode = new CIterationDoWhileStatement;
						break;
					case RUL_FOR:
						pNode = new CIterationForStatement;
						break;
					}
					break;
				}
				break;
			case STMT_COMPOUND:
				switch(pToken->GetType())
				{
				case RUL_SYMBOL:
					switch(pToken->GetSubType())
					{
					case RUL_LBRACK:
						pNode = new CCompoundStatement;
						break;
					case RUL_COLON:
						pNode = new CCompoundStatement;
						break;
					}
				}
				break;
			case STMT_SELECTION:
				switch(pToken->GetType())
				{
				case RUL_KEYWORD:
					switch(pToken->GetSubType())
					{
					case RUL_CASE:
					case RUL_DEFAULT:
						pNode = new CCASEStatement;
						break;
					case RUL_ELSE:
						pNode = new CELSEStatement;
						break;
					}
				}
				break;
			case STMT_SERVICE:
				switch(pToken->GetType())
				{
				case RUL_SERVICE:
					switch(pToken->GetSubType())
					{
					case RUL_SERVICE_INVOKE:
						pNode = new CServiceStatement;
						break;
					}
				}
			case STMT_ASSIGNMENT_FOR:
				switch(pToken->GetType())
				{
				case RUL_SERVICE:
					switch(pToken->GetSubType())
					{
					case RUL_SERVICE_INVOKE:
						pNode = new CServiceStatement;
						break;
					}
				}
				break;
			}

			DELETE_PTR(pToken);
		}
	}
	catch(...)
	{
		DELETE_PTR(pToken);
		DELETE_PTR(pNode);
		throw(C_UM_ERROR_UNKNOWNERROR);
	}
	return pNode;
}

CGrammarNode* CParserBuilder::CreateParserAssignment(CLexicalAnalyzer* plexAnal, CToken* pToken)
{
	CGrammarNode* pNode = 0;
	CToken* pNewToken = 0;	//TSRPRASAD 09MAR2004 Fix the memory leaks	*/

	try
	{
		bool bLineIsAssignment = false;

		//check for all assignment operators, including =, +=, -=, *=, /=, %=, &=, ^=, |=, <<=, >>=
		if (
				plexAnal->ScanLineForToken(RUL_ASSIGNMENT_OPERATOR, RUL_ASSIGN, &pNewToken)
		   )
		{
			DELETE_PTR(pNewToken);
	
			//look for comma token in a expression.
			if (!plexAnal->ScanLineForToken(RUL_SYMBOL, RUL_COMMA, &pNewToken))
			{
				//it is assignment without ',' comma operator
				bLineIsAssignment = true;
			}
			//else means assignment right expression containing ',' comma
		}		

		DELETE_PTR(pNewToken);

		if (bLineIsAssignment)
			pNode = new CAssignmentStatement;
		else
			pNode = new CExpression;
	}
	catch(...)
	{
		DELETE_PTR(pNewToken);
		DELETE_PTR(pNode);
		throw(C_UM_ERROR_UNKNOWNERROR);
	}
	return pNode;
}
