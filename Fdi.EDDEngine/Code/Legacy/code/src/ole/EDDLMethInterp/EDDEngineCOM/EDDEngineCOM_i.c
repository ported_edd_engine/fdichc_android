

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Feb 21 10:20:08 2020
 */
/* Compiler settings for EDDEngineCOM.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IEDDMethods,0x1957AB75,0x1215,0x492D,0xB7,0x73,0x70,0xBC,0xC3,0x4A,0x99,0x5C);


MIDL_DEFINE_GUID(IID, LIBID_EDDEngineCOMLib,0xC72B1E80,0x87CC,0x424B,0x89,0x38,0x34,0x26,0x53,0x48,0x16,0x97);


MIDL_DEFINE_GUID(IID, DIID__IEDDMethodsEvents,0x927A875B,0x5F1F,0x4001,0xB9,0x33,0x85,0x04,0x22,0x3A,0x72,0x03);


MIDL_DEFINE_GUID(CLSID, CLSID_EDDMethods,0x7D93CDE5,0xD062,0x4630,0x98,0x13,0xEB,0x84,0x38,0x66,0xC2,0x9C);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



