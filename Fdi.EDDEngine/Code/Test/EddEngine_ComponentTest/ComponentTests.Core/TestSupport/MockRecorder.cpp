#include "stdafx.h"
#include "MockRecorder.h"
#include <wchar.h>

using namespace std;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            MockRecorder::MockRecorder()
                : _Parameters(), _ReturnValues(), _MethodDelegates()
            {
                _inputValue = nullptr;
            }

            MockRecorder::~MockRecorder()
            {
                if (_inputValue != nullptr)
                {
                    delete _inputValue;
                }
            }

            void MockRecorder::RegisterParameter(std::wstring key, void* parameter )
            {
                _Parameters[key] = parameter;
            }

            void MockRecorder::RegisterParameter(std::wstring key, nsConsumer::EVAL_VAR_VALUE* parameter )
            {
                _inputValue = new nsConsumer::EVAL_VAR_VALUE(*parameter);
                _inputValue->val.s = (wchar_t*) Copy(parameter->val.s.c_str());
                _Parameters[key] = (void *)_inputValue;
            }

            void MockRecorder::UnregisterParameter(std::wstring key)
            {
                _Parameters.erase(key);
            }

            void MockRecorder::RegisterMethodDelegate(const std::wstring methodName, MockMethod methodDelegate)
            {
                _MethodDelegates[methodName] = methodDelegate;
            }

            void MockRecorder::RegisterNewMethodDelegate(const std::wstring methodName, MockMethod methodDelegate)
            {
                if (_MethodDelegates.find(methodName) != _MethodDelegates.end())
                {
                    _MethodDelegates.erase(methodName);
                }
                _MethodDelegates[methodName] = methodDelegate;
            }

            void MockRecorder::InvokeMethdodDelegate(const std::wstring methodName)
            {
                MethodDelegateMap::iterator valueIterator = _MethodDelegates.find(methodName);

                if (valueIterator != _MethodDelegates.end())
                {
                    MockMethod method = (MockMethod) valueIterator->second;
                    method(methodName.c_str(), this);
                }
            }

            bool MockRecorder::HasMethodDelegate(const std::wstring methodName)
            {
                bool result = false;
                MethodDelegateMap::iterator valueIterator = _MethodDelegates.find(methodName);
                if (valueIterator != _MethodDelegates.end())
                {
                    result = true;
                }
                return result;
            }


            void MockRecorder::Record( std::wstring key, void* returnValue )
            {
                _ReturnValues[key] = returnValue;
            }

            void MockRecorder::RecordAnew( std::wstring key, void* returnValue )
            {
                if (IsReturnValueRecorded(key))
                {
                    _ReturnValues.erase(key);
                }

                _ReturnValues[key] = returnValue;
            }

            void* MockRecorder::ReplayReturnValue( std::wstring key )
            {
                ParameterMap::iterator valueIterator = _ReturnValues.find(key);

                if (valueIterator != _ReturnValues.end())
                {
                    return valueIterator->second;
                }

                throw L"return value not found";
            }

            void* MockRecorder::GetParameter( std::wstring key )
            {
                ParameterMap::iterator valueIterator = _Parameters.find(key);

                if (valueIterator != _Parameters.end())
                {
                    return valueIterator->second;
                }

                throw L"parameter not found";
            }

            wchar_t* MockRecorder::Copy( const wchar_t* itemToCopy )
            {
                return _wcsdup(itemToCopy);
            }

            void MockRecorder::RegisterCall( std::wstring key )
            {
                CountMap::iterator valueIterator = _CallCount.find(key);

                if (valueIterator != _CallCount.end())
                {
                    valueIterator->second += 1;
                }
                else
                {
                    _CallCount.insert(CountPair(key, 1));
                }
            }

            int MockRecorder::GetCallCount( std::wstring key )
            {
                CountMap::iterator valueIterator = _CallCount.find(key);

                if (valueIterator != _CallCount.end())
                {
                    return valueIterator->second;
                }

                return 0;
            }

            bool MockRecorder::WasCalled( std::wstring key, int times )
            {
                return GetCallCount(key) == times;
            }

            bool MockRecorder::WasCalled( std::wstring key )
            {
                return GetCallCount(key) > 0;
            }

            bool MockRecorder::IsReturnValueRecorded(std::wstring key)
            {
                return _ReturnValues.find(key) != _ReturnValues.end();
            }

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests
