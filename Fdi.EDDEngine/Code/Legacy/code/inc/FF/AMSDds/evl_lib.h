/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */
/**
 *		Copyright 1995 - HART Communication Foundation
 *		All rights reserved.
 */

/*
 *
 *	@(#)evl_lib.h	30.3  30  14 Nov 1996
 */

#pragma once

#include <stddef.h>
#include <limits.h>

#include <ServerProjects/rtn_code.h>		/** All of these .h files are needed by the eval library **/
#include "tags_sa.h"		/** To make it easier for a user to use eval, the user **/
#include "DDLDEFS.H"		/** only needs to include evl_lib.h **/
#include "flats.h"
#include "table.h"
#include "cm_lib.h"
#include <env_info.h>
#include "evl_ret.h"


#ifdef __cplusplus
    extern "C" {
#endif


/**
 * Range Values
 **/

#ifndef DDL_HUGE_INTEGER
#define DDL_HUGE_INTEGER ULONG_MAX	/** defines the maximum size of an unsigned **/
									/** integer (also defined in ddldefs.h) **/
#endif /* DDL_HUGE_INTEGER */


/**
 ** Structures and function prototypes to support upcalls to an application
 **/

typedef struct {
	unsigned short  type;	/** valid types are defined in ddldefs.h (ie. INTEGER) **/
	unsigned int  size;	/** number of bytes a value can occupy **/
	union {
		float				f;
		double				d;
		unsigned long		u;
		long				i;
		STRING				s;
		unsigned short		sa[4];
        char				ca[8];

	}               val;	/** value of a variable (obtained from app) **/
}               EVAL_VAR_VALUE;


/*
 *	Structure used to pass info to the device specific string service
 */
typedef struct {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;


/**
 ** flat interface functions
 **/
extern int eval_item P((void *, unsigned long, ENV_INFO2 *, RETURN_LIST *, ITEM_TYPE));
extern int eval_dir_block_tables P(( FLAT_BLOCK_DIR *, BIN_BLOCK_DIR *, unsigned long ));
extern int eval_dir_device_tables P(( FLAT_DEVICE_DIR *, BIN_DEVICE_DIR *, unsigned long));


/**
 ** flat destructor functions
 **/

extern void eval_clean_item P((void *, ITEM_TYPE));


/**
 ** directory (ie. table) destructor functions
 **/

extern void eval_clean_device_dir P((FLAT_DEVICE_DIR *));
extern void eval_clean_block_dir P((FLAT_BLOCK_DIR *));


/**
 ** application function prototypes
 **/

extern int app_func_get_param_value P((ENV_INFO *, OP_REF *, EVAL_VAR_VALUE *));
extern int app_func_get_dict_string P((ENV_INFO *, DDL_UINT, STRING *));
extern int app_func_get_dev_spec_string P((ENV_INFO *, DEV_STRING_INFO *, STRING *));
extern int list_read_element_upcall P ((ENV_INFO *, ITEM_ID, SUBINDEX, ITEM_ID, SUBINDEX, EVAL_VAR_VALUE *));


#ifdef __cplusplus
}
#endif /* __CPLUSPLUS */
