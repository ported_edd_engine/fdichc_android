#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        namespace LISTFILE
        {


            // BaseTests Classes
            [TestClass, Ignore]
            public ref class BaseTestGetListWithAttributes
            {
            public: 
                static EddEngineController^ engine;
                static FDI_GENERIC_ITEM* genericItem;
                static FLAT_LIST* test_List;
                static ITEM_ID expectedItemIdList;
                static ITEM_ID expectedItemIdType;

                static void SetupBase(String^ testFileName)
                {
                    engine = gcnew EddEngineController(testFileName);
                    engine->Initialize();                

                    FDI_PARAM_SPECIFIER test_lists_specifier;
                    engine->FillParamSpecifier("list_of_test_var_2", &test_lists_specifier);

                    FDI_PARAM_SPECIFIER test_type_specifier;
                    engine->FillParamSpecifier("test_var_2", &test_type_specifier);

                    genericItem = engine->GetGenericItem(test_lists_specifier.id, nsEDDEngine::ITYPE_LIST);
                    test_List = dynamic_cast<FLAT_LIST*>(genericItem->item);
                    expectedItemIdList = test_lists_specifier.id;

                    expectedItemIdType = test_type_specifier.id;

                }

                static void CleanupBase()
                {
                    delete engine;
                    delete genericItem;
                }

                [TestMethod, TestCategory("ListTest")]
                void It_should_return_the_expected_Item_Id_for_the_list()
                {                
                    Assert::AreEqual(expectedItemIdList, test_List->id);
                }    

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_value_0_for_initial_COUNT()
                {
                    Assert::AreEqual((ulong)0, test_List->count);
                }  

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_value_7_for_CAPACITY()
                {
                    Assert::AreEqual(7u, test_List->capacity);
                } 

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_expected_Item_ID_for_the_type()
                {
                    Assert::AreEqual(expectedItemIdType, test_List->type);
                } 
            };

            [TestClass, Ignore]
            public ref class BaseTestGetList
            {
            public: 
                static EddEngineController^ engine;
                static FDI_GENERIC_ITEM* genericItem;
                static FLAT_LIST* test_List;
                static ITEM_ID expectedItemIdList;
                static ITEM_ID expectedItemIdType;

                static void SetupBase(String^ testFileName)
                {
                    engine = gcnew EddEngineController(testFileName);
                    engine->Initialize();                

                    FDI_PARAM_SPECIFIER test_lists_specifier;
                    engine->FillParamSpecifier("list_of_test_var_1", &test_lists_specifier);

                    FDI_PARAM_SPECIFIER test_type_specifier;
                    engine->FillParamSpecifier("test_var_1", &test_type_specifier);

                    genericItem = engine->GetGenericItem(test_lists_specifier.id, nsEDDEngine::ITYPE_LIST);
                    test_List = dynamic_cast<FLAT_LIST*>(genericItem->item);
                    expectedItemIdList = test_lists_specifier.id;

                    expectedItemIdType = test_type_specifier.id;

                }

                static void CleanupBase()
                {
                    delete engine;
                    delete genericItem;
                }

                [TestMethod, TestCategory("ListTest")]
                void It_should_return_the_expected_Item_Id_for_the_list()
                {                
                    Assert::AreEqual(expectedItemIdList, test_List->id);
                }    

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_value_0_for_COUNT()
                {
                    Assert::AreEqual((ulong)0, test_List->count);
                }  

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_value_0_for_CAPACITY()
                {
                    Assert::AreEqual((ulong)0, test_List->capacity);
                } 

                [TestMethod, TestCategory("ListTest")]
                void It_should_have_the_expected_Item_ID_for_the_type()
                {
                    Assert::AreEqual(expectedItemIdType, test_List->type);
                } 
            };

            [TestClass, Ignore]
            public ref class BaseTestGetFile
            {
            public: 
                static EddEngineController^ engine;
                static FDI_GENERIC_ITEM* genericItem;
                static FLAT_FILE* test_File;
                static ITEM_ID expectedItemIdList;
                static ITEM_ID expectedItemIdType;

                static void SetupBase(String^ testFileName)
                {
                    engine = gcnew EddEngineController(testFileName);
                    engine->Initialize();                

                    FDI_PARAM_SPECIFIER test_file_specifier;
                    engine->FillParamSpecifier("stored_var_1", &test_file_specifier);

                    FDI_PARAM_SPECIFIER test_type_specifier;
                    engine->FillParamSpecifier("list_of_test_var_1", &test_type_specifier);

                    genericItem = engine->GetGenericItem(test_file_specifier.id, nsEDDEngine::ITYPE_FILE);
                    test_File = dynamic_cast<FLAT_FILE*>(genericItem->item);
                    expectedItemIdList = test_file_specifier.id;

                    expectedItemIdType = test_type_specifier.id;

                }

                static void CleanupBase()
                {
                    delete engine;
                    delete genericItem;
                }

                [TestMethod, TestCategory("FileTest")]
                void It_should_return_the_expected_Item_Id_for_the_file()
                {                
                    Assert::AreEqual(expectedItemIdList, test_File->id);
                }    

                [TestMethod, TestCategory("FileTest")]
                void It_should_have_two_member()
                {
                    Assert::AreEqual(2u, (UInt32)test_File->members.count);
                } 

                [TestMethod, TestCategory("FileTest")]
                void It_should_have_the_expected_item_id_for_the_first_member()
                {
                    Assert::AreEqual(expectedItemIdType, test_File->members.list[0].ref.id);
                } 
            };

            [TestClass, Ignore]
            public ref class BaseTestFileWithDifferentMember
            {
            public: 
                static EddEngineController^ engine;
                static FDI_GENERIC_ITEM* genericItem;
                static FLAT_FILE* test_File;
                static ITEM_ID expectedItemIdList;
                static ITEM_ID expectedItemIdType;

                static void SetupBase(String^ testFileName)
                {
                    engine = gcnew EddEngineController(testFileName);
                    engine->Initialize();                

                    FDI_PARAM_SPECIFIER test_file_specifier;
                    engine->FillParamSpecifier("stored_signals", &test_file_specifier);

                    FDI_PARAM_SPECIFIER test_type_specifier;
                    engine->FillParamSpecifier("recorded_signals", &test_type_specifier);

                    genericItem = engine->GetGenericItem(test_file_specifier.id, nsEDDEngine::ITYPE_FILE);
                    test_File = dynamic_cast<FLAT_FILE*>(genericItem->item);
                    expectedItemIdList = test_file_specifier.id;

                    expectedItemIdType = test_type_specifier.id;

                }

                static void CleanupBase()
                {
                    delete engine;
                    delete genericItem;
                }

                [TestMethod, TestCategory("FileTest")]
                void It_should_return_the_expected_Item_Id_for_the_file()
                {                
                    Assert::AreEqual(expectedItemIdList, test_File->id);
                }    

                [TestMethod, TestCategory("FileTest")]
                void It_should_have_three_member()
                {
                    Assert::AreEqual(3u, (UInt32)test_File->members.count);
                } 

                [TestMethod, TestCategory("FileTest")]
                void It_should_have_the_expected_item_id_for_the_third_member()
                {
                    Assert::AreEqual(expectedItemIdType, test_File->members.list[2].ref.id);
                } 
            };

            // TestClasses for the Binary Versions

            [TestClass]
            public ref class When_requesting_a_LIST_with_defined_Attributes_FM8 : BaseTestGetListWithAttributes
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_LIST_with_defined_Attributes_FMA : BaseTestGetListWithAttributes
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_LIST_with_FM8 : BaseTestGetList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_LIST_with_FMA : BaseTestGetList
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };


            [TestClass]
            public ref class When_requesting_a_FILE_with_FM8 : BaseTestGetFile
            {
            public:
                [TestInitialize]
                void Setup()
                {
                     SetupBase("Hart\\TestCase0031.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_FILE_with_FMA : BaseTestGetFile
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_FILE_with_more_member_FM8 : BaseTestFileWithDifferentMember
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fm8");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };

            [TestClass]
            public ref class When_requesting_a_FILE_with_more_member_FMA : BaseTestFileWithDifferentMember
            {
            public:
                [TestInitialize]
                void Setup()
                {
                    SetupBase("Hart\\TestCase0031.fmA");
                }

                [TestCleanup]
                void CleanUp()
                {
                    CleanupBase();
                }
            };
        }
    } // namespace Hart
} // namespace ComponentTests
