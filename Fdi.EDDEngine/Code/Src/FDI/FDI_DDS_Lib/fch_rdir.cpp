/**
 *	@(#) $Id: fch_rdir.cpp,v 1.5 2012/08/24 21:25:37 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for the ROD Directory Fetch
 *
 */

/* #includes */

#include "stdafx.h"
#include "fch_lib.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"
#include "DeviceTypeMgr.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif

/* #defines */

/* typedefs */

/* structure definitions */

/* global variables */

/* local procedure declarations */
/* FD_FDI externs */

extern "C" int ddl_parse_integer_func(unsigned char **chunkp,DDL_UINT *size, DDL_UINT *value);

extern int fdi_table_size_lookup(unsigned char  *obj_extn_ptr, unsigned long ext_byte_count, int mask);
extern int fdi_table_ptr_lookup(unsigned char  *obj_extn_ptr, unsigned long ext_byte_count, int mask, unsigned char **table_ptr, unsigned long *table_length);

/*********************************************************************
 *
 *	Name:	set_dir_bin_exists
 *
 *	ShortDesc: Sets the bin_exists field in the nsEDDEngine::BININFO structure
 *
 *	Description:
 *		A bit is set in the bin_exists field in the nsEDDEngine::BININFO structure
 *		corresponding to each non-zero length table reference in the
 *		directory object extension.
 *
 *	Inputs:
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
set_dir_bin_exists(
unsigned char  *local_data_ptr,
unsigned long	extension_byte_count,
unsigned long  *dir_bin_exists,
unsigned short  dir_type)
{
	int             i, max_table_id;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(dir_bin_exists, FETCH_INVALID_PARAM);

	/*
	 * Set a bit in the bin_exists field for each non-zero length field in
	 * the table references in the object extension
	 */

	*dir_bin_exists = 0L;
	max_table_id = (dir_type == DEVICE_DIR_TYPE) ?
		MAX_DEVICE_TBL_ID : MAX_BLOCK_TBL_ID;

	for (i = 0; i < max_table_id; i++) {

		bool dir_bin_is_existing = false;
		unsigned long table_mask=(1<<i);

		dir_bin_is_existing = ::fdi_table_size_lookup(local_data_ptr, extension_byte_count, table_mask) ? true : false;
		
		if (dir_bin_is_existing)
		{
			*dir_bin_exists |= table_mask;
		}
	}

	return (SUCCESS);
}


/***********************************************************************
 *
 *	Name: get_major_rev_number
 *
 *	ShortDesc: Get the major rev number of the DDROD.
 *
 *	Description:
 *		The get_major_rev_number function takes a handle to a DDROD,
 *		and gets its DDOD Major Revision Number.
 *
 *	Inputs:
 *		rod_handle - handle of the DDROD.
 *
 *	Outputs:
 *		major_rev_number - DDOD Major Revision Number of the DDROD.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_major_rev_number(
ENV_INFO		* /* env_info */,
ROD_HANDLE		 rod_handle,
UINT8			*major_rev_number)
{

	/*
	 *	Point to the Format Object in the DDROD.
	 */
	*major_rev_number=(UINT8)(g_DeviceTypeMgr.rod_get_version(rod_handle)>>8);

	/*
	 *	Check to make sure that there were no errors in pointing
	 *	to the Format Object, and that the Format Object's extension
	 *	exists.
	 */

	/*
	 *	Set the major rev number.
	 */

	return(SUCCESS);
}

/*********************************************************************
 *
 *	Name: fetch_rod_device_dir
 *
 *	ShortDesc: Perform ROD fetch for device directories
 *
 *	Description:
 *		Retrieves the ROD object for the device directories and
 *		the associated tables in the local data area.  The table
 *		binaries are then attached to the appropriate nsEDDEngine::BININFO
 *		structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_device_dir( // fd_fdi decodes need to change
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned char  *obj_extn_ptr,
nsEDDEngine::BIN_DEVICE_DIR *bin_dev_dir,
unsigned long  *req_mask)
{
	unsigned short  tag;	/* indicator for table type */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	nsEDDEngine::BININFO        *bin_table_ptr;	/* points to a specific table nsEDDEngine::BININFO	 * structure */
	int             rcode;
	unsigned char *table_ptr=NULL;
	unsigned long table_length=0;

	scrpad=scrpad;
	/*
	 * Set the bin_exists field in the nsEDDEngine::BININFO structure if not already set
	 */

	if (!(bin_dev_dir->bin_exists)) {

		rcode = set_dir_bin_exists(obj_extn_ptr, obj->ext_length, &bin_dev_dir->bin_exists, DEVICE_DIR_TYPE);

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

	tag = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (nsEDDEngine::BININFO *) 0L;

	// FD_FDI you cannot ask for things that do not exist
	*req_mask&=bin_dev_dir->bin_exists;

	while ((*req_mask) && (tag < MAX_DEVICE_TBL_ID)) {

		/*
		 * Check for request mask bit corresponding to the tag value
		 * Skip to next tag value if not requested
		 */

		if (!((*req_mask) & (1L << tag))) {
			tag++;
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		switch (tag++) {

		case BLK_TBL_ID:	/* Block Table */
			tbl_mask_bit = BLK_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->blk_tbl;
			break;

		case ITEM_TBL_ID:	/* Item Table */
			tbl_mask_bit = ITEM_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->item_tbl;
			break;

		case STRING_TBL_ID:	/* String Table */
			tbl_mask_bit = STRING_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->string_tbl;
			break;

		case DICT_REF_TBL_ID:	/* Dictionary Reference Table */
			tbl_mask_bit = DICT_REF_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->dict_ref_tbl;
			break;

		case IMAGE_TBL_ID:	/* Image Table */
			tbl_mask_bit = IMAGE_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->image_tbl;
			break;

		case SYMBOL_TBL_ID:			/* Symbol Table */
			tbl_mask_bit = SYMBOL_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->symbol_table;
			break;

		case ITEM_TO_COMMAND_TBL_ID:
			tbl_mask_bit = ITEM_TO_COMMAND_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->item_to_command_tbl;
			break;

		case COMMAND_NUMBER_TBL_ID:
			tbl_mask_bit = COMMAND_NUMBER_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->command_number_tbl;
			break;

		default:	/* goes here for reserved or undefined table * IDs */
			break;
		}

		int ret=fdi_table_ptr_lookup(obj_extn_ptr, obj->ext_length, tbl_mask_bit, &table_ptr, &table_length);

		if(ret)
		{
			if(ret==FETCH_INVALID_DIR_TYPE)
				continue;
			return ret;
		}

		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and if it not zero length
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {

				/*
				 * Attach the table if non-zero length, else go
				 * to the next table
				 */

			if (table_length) {
				bin_table_ptr->chunk = table_ptr;
				bin_table_ptr->size = table_length;
				bin_dev_dir->bin_hooked |= tbl_mask_bit;
			}

		}

	}			/* end while */

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: fetch_rod_block_dir
 *
 *	ShortDesc: Perform ROD fetch for block directories
 *
 *	Description:
 *		Retrieves the ROD object for the block directories and
 *		the associated tables in the local data area.  The table
 *		binaries are then attached to the appropriate nsEDDEngine::BININFO
 *		structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_block_dir(
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned char  *obj_extn_ptr,
nsEDDEngine::BIN_BLOCK_DIR  *bin_blk_dir,
unsigned long  *req_mask)
{
	unsigned short  tag;	/* indicator for table type */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	nsEDDEngine::BININFO        *bin_table_ptr;	/* points to a specific table nsEDDEngine::BININFO	 * structure */
	int             rcode;
	unsigned char *table_ptr=NULL;
	unsigned long table_length=0;

	scrpad=scrpad;

	/*
	 * Set the bin_exists field in the nsEDDEngine::BININFO structure if not already set
	 */

	if (!(bin_blk_dir->bin_exists)) {

		rcode = set_dir_bin_exists(obj_extn_ptr, obj->ext_length, &bin_blk_dir->bin_exists, BLOCK_DIR_TYPE);

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

	tag = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (nsEDDEngine::BININFO *) 0L;

	// FD_FDI you cannot ask for things that do not exist
	*req_mask&=bin_blk_dir->bin_exists;

	while ((*req_mask) && (tag < MAX_BLOCK_TBL_ID)) {

		/*
		 * Check for request mask bit corresponding to the tag value
		 * Skip to next tag value if not requested
		 */

		if (!((*req_mask) & (1L << tag))) {
			tag++;
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		switch (tag++) {

		case CRIT_PARAM_TBL_ID:	/* Critical Parameters Table */
			tbl_mask_bit = CRIT_PARAM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->crit_param_tbl;
			break;

		case BLK_ITEM_TBL_ID:	/* Block Item Table */
			tbl_mask_bit = BLK_ITEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->blk_item_tbl;
			break;

		case BLK_ITEM_NAME_TBL_ID:	/* Block Item Name Table */
			tbl_mask_bit = BLK_ITEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->blk_item_name_tbl;
			break;

		case PARAM_TBL_ID:	/* Parameter Table */
			tbl_mask_bit = PARAM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_tbl;
			break;

		case PARAM_MEM_TBL_ID:	/* Parameter Member Table */
			tbl_mask_bit = PARAM_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_mem_tbl;
			break;

		case PARAM_MEM_NAME_TBL_ID:	/* Parameter Member Table */
			tbl_mask_bit = PARAM_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_mem_name_tbl;
			break;

		case PARAM_ELEM_TBL_ID:	/* Parameter Element Table */
			tbl_mask_bit = PARAM_ELEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_elem_tbl;
			break;
		
		case PARAM_LIST_TBL_ID:	/* Parameter List Table */
			tbl_mask_bit = PARAM_LIST_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_tbl;
			break;

		case PARAM_LIST_MEM_TBL_ID:	/* Parameter List Member Table */
			tbl_mask_bit = PARAM_LIST_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_mem_tbl;
			break;

		case PARAM_LIST_MEM_NAME_TBL_ID:	/* Parameter List Member Name Table */
			tbl_mask_bit = PARAM_LIST_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_mem_name_tbl;
			break;

		// In PROFIBUS Binary, but not used
		case CHAR_MEM_TBL_ID:	
			tbl_mask_bit = CHAR_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->char_mem_tbl;
			break;
		case CHAR_MEM_NAME_TBL_ID:
			tbl_mask_bit = CHAR_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->char_mem_name_tbl;
			break;

		case REL_TBL_ID:	/* Relation Table */
			tbl_mask_bit = REL_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->rel_tbl;
			break;

		case UPDATE_TBL_ID:	/* Update Table */
			tbl_mask_bit = UPDATE_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->update_tbl;
			break;

		default:	/* goes here for reserved or undefined table
				 * IDs */
			break;

		}

		int ret=fdi_table_ptr_lookup(obj_extn_ptr, obj->ext_length, tbl_mask_bit, &table_ptr, &table_length);

		if(ret)
		{
			if(ret==FETCH_INVALID_DIR_TYPE)
				continue;
			return ret;
		}
		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and it is not zero length
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {

			/*
				* Attach the table if non-zero length, else go
				* to the next table
				*/

			if (table_length) {
				bin_table_ptr->chunk = table_ptr;
				bin_table_ptr->size = table_length;
				bin_blk_dir->bin_hooked |= tbl_mask_bit;
			}

		}

	}			/* end while */

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name:	deobfuscate
 *
 *	ShortDesc:	decodes obfuscated string
 *
 *	Description:
 *		decodes obfuscated string
 *
 *	Inputs:
 *		str -			string
 *		size -			size
 *		ddtype -		ddtype
 *		
 *
 *********************************************************************/
void deobfuscate(char *str, int size, int ddtype)
{
	switch(ddtype)
	{
	case ::nsEDDEngine::PROFILE_HART:
	case ::nsEDDEngine::PROFILE_PB:
	case ::nsEDDEngine::PROFILE_FF:
	case ::nsEDDEngine::PROFILE_PN:
	case ::nsEDDEngine::PROFILE_ISA100:
	case ::nsEDDEngine::PROFILE_GPE:
	case ::nsEDDEngine::PROFILE_COMMSERVER:
		for(int i=0;i<size;i++)
		{
			str[i]=str[i]^0xff;
		}
		break;

	default:
		ASSERT(0);
		break;
	}
}

/*********************************************************************
 *
 *	Name:	decode_BYTE_STRING
 *
 *	ShortDesc:	decodes BYTE_STRING
 *
 *	Description:
 *		decodes BYTE_STRING 
 *
 *	Inputs:
 *		ptr -			chunk
 *		bin_size -		chunk size
 *		str	-			destination string
 *		size -			destination string size
 *		ddtype -		ddtype
 *		
 *
 *********************************************************************/
void decode_BYTE_STRING(unsigned char **ptr, unsigned long *bin_size, char *str, int size, int ddtype)
{
	memcpy(str,*ptr,size);

	deobfuscate(str, size, ddtype);

	(*ptr)+=size;
	(*bin_size)-=size;
}

/*********************************************************************
 *
 *	Name:	decode_UINT32
 *
 *	ShortDesc:	decodes UINT32
 *
 *	Description:
 *		decodes UINT32 
 *
 *	Inputs:
 *		ptr -			chunk
 *		bin_size -		chunk size
 *
 *	Returns
 *		decoded UINT32
 *		
  *********************************************************************/
UINT32 decode_UINT32(unsigned char **ptr, unsigned long *size)
{
	UINT32 val= ((*ptr)[0] << 24) | ((*ptr)[1] << 16) | ((*ptr)[2] << 8) | ((*ptr)[3]);

	(*ptr)+=4;

	if(size)
		(*size)-=4;

	return val;
}

/*********************************************************************
 *
 *	Name:	decode_UINT8
 *
 *	ShortDesc:	decodes UINT8
 *
 *	Description:
 *		decodes UINT8 
 *
 *	Inputs:
 *		ptr -			chunk
 *		bin_size -		chunk size
 *
 *	Returns
 *		decoded UINT8
 *		
  *********************************************************************/
UINT8 decode_UINT8(unsigned char **ptr)
{
	UINT8 val= ((*ptr)[0]);
	(*ptr)++;
	return val;
}


/*********************************************************************
 *
 *	Name:	fdi_table_size_lookup
 *
 *	ShortDesc:	calculates fdi table size
 *
 *	Description:
 *		calculates fdi table size
 *
 *	Inputs:
 *		obj_extn_ptr -			extension pointer
 *		ext_byte_count -		extension count
 *		mask -					table to decode
 *	Returns
 *		decoded size
 *		
  *********************************************************************/
int fdi_table_size_lookup(unsigned char  *obj_extn_ptr, unsigned long ext_byte_count, int mask)
{
	unsigned char *ptr=obj_extn_ptr;
	int table_type;
	unsigned char table_count;

	table_count = decode_UINT8(&ptr);
	
	for(int i=0;i<table_count;i++)
	{
		table_type=decode_UINT8(&ptr);

		unsigned long temp_mask = 1 << table_type;
		unsigned long size=ext_byte_count;
		DDL_UINT value;
		ddl_parse_integer_func(&ptr,&size,&value);

		// if correct table return
		if(temp_mask==(unsigned long)mask)
			return value;

		ptr+=value;
	}

	return 0;
}

/*********************************************************************
 *
 *	Name:	fdi_table_ptr_lookup
 *
 *	ShortDesc:	locates requested table pointer
 *
 *	Description:
 *		locates requested table pointer
 *
 *	Inputs:
 *		obj_extn_ptr -			extension pointer
 *		ext_byte_count -		extension count
 *		mask -					table to decode
 *	Outputs:
 *		table_ptr -				table ptr
 *		table_length -			table  length
 *	Returns
 *		decoded size
 *		
  *********************************************************************/
int fdi_table_ptr_lookup(unsigned char  *obj_extn_ptr, unsigned long ext_byte_count, int mask, unsigned char **table_ptr, unsigned long *table_length)
{
	unsigned char *ptr=obj_extn_ptr;
	int table_type;
	unsigned char table_count;
	*table_length=0;
	*table_ptr=0;

	table_count = decode_UINT8(&ptr);
	
	for(int i=0;i<table_count;i++)
	{
		table_type=decode_UINT8(&ptr);

		unsigned long temp_mask = 1 << table_type;
		unsigned long size=ext_byte_count;
		DDL_UINT value;
		ddl_parse_integer_func(&ptr, &size, &value);

		// if correct table return
		if(temp_mask==(unsigned long)mask)
		{
			*table_length=value;
			*table_ptr=ptr;
			return 0;
		}

		ptr+=value;
	}

	return FETCH_INVALID_DIR_TYPE;
}

/*********************************************************************
 *
 *	Name:	fch_rod_dir_spad_size
 *
 *	ShortDesc: Compute needed scratchpad memory size for directory fetch
 *
 *	Description:
 *		This function returns the amount of scratchpad memory required
 *		for a ROD Directory Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		tbl_req_mask -	tables requested for the directory, indicated
 *						by individual bits set for each table
 *						(tables depend on the directory type)
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *		sp_addlsize -	pointer to the value which is the minimum size
 *						of scratchpad memory needed to complete
 *						the fetch request.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INVALID_EXTN_LEN
 *		FETCH_INVALID_DIR_TYPE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_dir_spad_size( // fd_fdi
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
unsigned short  dir_type)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;	/* pointer to ROD object extension in
					 * scratchpad */
	unsigned long   dir_extn_len;	/* length of directory object extension */
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
					 * object extension */
	unsigned char   *pad_buf=NULL;//[MAX_OBJ_EXTN_LEN];	/* holds object
							/* extension to allow
							 * calculation of
							 * scratchpad size */
	unsigned char	major_rev_number;
	int             rcode;

	/*
	 * This function or fch_rod_device_dir() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */
	switch (dir_type) {  

		case DEVICE_DIR_TYPE:
			rcode = get_major_rev_number(env_info, rhandle,&major_rev_number);
			if (rcode != SUCCESS) {
				return(rcode);
			}

			PS_TRACE(L"\nLoading FDI DD ver. %d (fch_rod_dir_spad_size)\n",major_rev_number);

			if (major_rev_number != DDOD_REVISION_MAJOR) {
				return(DDS_WRONG_DDOD_REVISION); 
			}

			break;

		case BLOCK_DIR_TYPE:
			break;

		default:
			return(FETCH_INVALID_DIR_TYPE);
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(sp_addlsize, FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(ROD_FCH_DIR_SCRATCHPAD_SIZE);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	int size=0;
	rcode=get_rod_object_size(env_info, rhandle, obj_index, &size);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	pad_buf=(unsigned char *)malloc(size);

	if(!pad_buf)
		return DDL_MEMORY_ERROR;

	local_scrpad.pad = pad_buf;
	local_scrpad.size=size;
	local_scrpad.used=0;

	obj_extn_ptr = local_scrpad.pad + local_scrpad.used;

	rcode = get_rod_object(env_info, rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		free(pad_buf);
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory
	 * table reference.  Return if the extension length is not large
	 * enough for the particular type of directory.
	 */

	dir_extn_len = rod_obj->ext_length;

	/*
	 * Minimum required scratchpad size is the object length
	 */

	*sp_addlsize += (dir_extn_len );

	/*
	 * Calculate the required size for each requested table data block
	 */
	
	if (dir_type == DEVICE_DIR_TYPE) {

		if (tbl_req_mask & BLK_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, BLK_TBL_MASK);
		}
		if (tbl_req_mask & ITEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len,  ITEM_TBL_MASK);
		}
		if (tbl_req_mask & STRING_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, STRING_TBL_MASK);
		}
		if (tbl_req_mask & DICT_REF_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len,  DICT_REF_TBL_MASK);
		}
		if (tbl_req_mask & IMAGE_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, IMAGE_TBL_MASK);
		}
		if (tbl_req_mask & ITEM_TO_COMMAND_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len,  ITEM_TO_COMMAND_TBL_MASK);
		}
		if (tbl_req_mask & COMMAND_NUMBER_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len,  COMMAND_NUMBER_TBL_MASK);
		}

	}
	else if (dir_type == BLOCK_DIR_TYPE) {

		if (tbl_req_mask & CRIT_PARAM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, CRIT_PARAM_TBL_MASK);
		}
		if (tbl_req_mask & BLK_ITEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, BLK_ITEM_TBL_MASK);
		}
		if (tbl_req_mask & BLK_ITEM_NAME_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, BLK_ITEM_NAME_TBL_MASK);
		}
		if (tbl_req_mask & PARAM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_TBL_MASK);
		}
		if (tbl_req_mask & PARAM_MEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_MEM_TBL_MASK);
		}
		if (tbl_req_mask & PARAM_MEM_NAME_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_MEM_NAME_TBL_MASK);
		}
		if (tbl_req_mask & PARAM_ELEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_ELEM_TBL_MASK);
		}
		if (tbl_req_mask & PARAM_LIST_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_LIST_TBL_MASK);
		}		
		if (tbl_req_mask & PARAM_LIST_MEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_LIST_MEM_TBL_MASK);
		}		
		if (tbl_req_mask & PARAM_LIST_MEM_NAME_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, PARAM_LIST_MEM_NAME_TBL_MASK);
		}
		if (tbl_req_mask & CHAR_MEM_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, CHAR_MEM_TBL_MASK);
		}
		if (tbl_req_mask & CHAR_MEM_NAME_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, CHAR_MEM_NAME_TBL_MASK);
		}
		if (tbl_req_mask & REL_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, REL_TBL_MASK);
		}
		if (tbl_req_mask & UPDATE_TBL_MASK) {
			*sp_addlsize += fdi_table_size_lookup(obj_extn_ptr, dir_extn_len, UPDATE_TBL_MASK);
		}
	}
	else {
		CRASH_RET(FETCH_INVALID_DIR_TYPE);
	}

	free(pad_buf);

	return (SUCCESS);

}

/*********************************************************************
 *
 *	Name:	fch_rod_device_dir
 *
 *	ShortDesc:	Interface for device ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for device directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_dev_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_device_dir( // fd_fdi tables are no longer in the rod they are in META section
ENV_INFO	   *env_info, 
ROD_HANDLE      rhandle,
SCRATCH_PAD    *scrpad,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
nsEDDEngine::BIN_DEVICE_DIR *bin_dev_dir)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned long  dir_extn_len;	/* extension length (first field in
					 * object extension) */
	unsigned char	major_rev_number;
	int             rcode;

	/*
	 * This function or fch_rod_dir_spad_size() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */
	 
	rcode = get_major_rev_number(env_info, rhandle,&major_rev_number);
	if (rcode != SUCCESS) {
		return(rcode);
	}

	PS_TRACE(L"\nLoading FDI DD ver. %d (fch_rod_device_dir)\n",major_rev_number);

	if (major_rev_number != DDOD_REVISION_MAJOR) {
		return(DDS_WRONG_DDOD_REVISION);
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_dev_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(ROD_FETCH_DEVICE_DIR);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;


	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, DEVICE_DIRECTORY_INDEX, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */


	dir_extn_len = rod_obj->ext_length;

	/*
	 * Call the device directory fetch function
	 */

#ifdef DEBUG
	dds_dev_dir_flat_chk(bin_dev_dir, (nsEDDEngine::FLAT_DEVICE_DIR *) 0);
#endif
	rcode = fetch_rod_device_dir(rod_obj, scrpad,
		obj_extn_ptr, bin_dev_dir, &tbl_req_mask);
#ifdef DEBUG
	dds_dev_dir_flat_chk(bin_dev_dir, (nsEDDEngine::FLAT_DEVICE_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, DEVICE_DIRECTORY_INDEX,
			sp_addlsize, tbl_req_mask, DEVICE_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	return (rcode);
}

/*********************************************************************
 *
 *	Name:	fch_rod_block_dir
 *
 *	ShortDesc:	Interface for block ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for block directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		block_index -	index of the block directory object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_block_dir(
ENV_INFO		*env_info, 
ROD_HANDLE      rhandle,
OBJECT_INDEX    block_index,
SCRATCH_PAD    *scrpad,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
nsEDDEngine::BIN_BLOCK_DIR  *bin_blk_dir)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned short  dir_extn_len;	/* extension length (first field in
					 * object extension) */
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_blk_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(ROD_FETCH_BLOCK_DIR);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, block_index, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */

	dir_extn_len = (unsigned short)rod_obj->ext_length;

	/*
	 * Return with an error code if the extension length is not
	 * large enough for a block directory object extension.
	 */

	/*
	 * Call the block directory fetch function
	 */

#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (nsEDDEngine::FLAT_BLOCK_DIR *) 0);
#endif
	rcode = fetch_rod_block_dir(rod_obj, scrpad,
		obj_extn_ptr, bin_blk_dir, &tbl_req_mask);
#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (nsEDDEngine::FLAT_BLOCK_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, block_index,
			sp_addlsize, tbl_req_mask, BLOCK_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	return (rcode);
}

