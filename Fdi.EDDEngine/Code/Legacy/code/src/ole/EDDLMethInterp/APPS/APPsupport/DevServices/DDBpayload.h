/*************************************************************************************************
 *
 * $Workfile: DDBpayload.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the abstract base class required of all payload classes
 *		Provides a common interface for conditional handling
 *		9/10/2	sjv	created
 *
 * #include "ddbPayload.h"
 */

#ifndef _DDBPAYLOAD_H
#define _DDBPAYLOAD_H
#ifdef INC_DEBUG
#pragma message("In ddbPayload.h") 
#endif

#pragma warning (disable : 4786) 
#include "ddbdefs.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbPayload.h") 
#endif


class hCpayload  // an (almost)abstract base class
{
public:
	// this sets the payload class from an abstract payload class in ddlapi
	// each payload must override it since they are the only ones to know 
	//      what their matching dllapi abstract class is
	//
	// We cannot make these pure virtual methods or we wouldn't be able
	//		to refer to their children as payloads
	//*?* virtual void  setEqual(void* pAclass) = 0;                      // a required method
	virtual void  setEqual(void* pAclass) =0;                 // a required method
//	{
//		std::cerr<<"+ERROR+++++ Payload Virtual function 'setEqual' called."<<std::endl;
//		return;
//	};
	// each payload must tell if the abstract payload type is correct for itself
	//*?* virtual boolT validPayload(payloadType_t ldType) = 0;
	virtual bool validPayload(payloadType_t ldType) = 0;
//	{
//		std::cerr<<"+ERROR+++++ Payload Virtual function 'validPayload' called."
//		<<std::endl; return isFALSE;
//	};
	
	virtual void duplicate(hCpayload* pPayLd, bool replicate /* = false */) = 0;
	
	virtual ~hCpayload(){};
	virtual RETURNCODE destroy(void) = 0;
};
//	boolT validPayload(payloadType_t ldType){ if (ldType != ) return isFALSE; else return isTRUE;};

#endif//_DDBPAYLOAD_H

/*************************************************************************************************
 *
 *   $History: DDBpayload.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
