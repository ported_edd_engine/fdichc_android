#ifndef lint
static char SCCSID[] = "@(#)pc_lib.c	30.4  30  14 Nov 1996";
#endif /* lint */

/**
 *              Device Description Services Rel. 4.2
 *              Copyright 1994-1996 - Fieldbus Foundation
 *              All rights reserved.
 */


#include "DDI_LIB.H"

int list_read_element_upcall(ENV_INFO *env_info, ITEM_ID, SUBINDEX, ITEM_ID param_id, SUBINDEX param_subindex, EVAL_VAR_VALUE *param_value)
{
	if (param_value == (EVAL_VAR_VALUE *) NULL)
	{
		return (LC_NVALID_POINTER);
	}

	/* setup block specifier and and obtain parameter info */
	DDI_BLOCK_SPECIFIER			block_spec = {0};
	DDI_PARAM_SPECIFIER			param_spec = {0};

	block_spec.type = DDI_BLOCK_HANDLE;
	block_spec.block.handle = env_info->block_handle;
	param_spec.type = (param_subindex != 0) ? DDI_PS_ITEM_ID_SI : DDI_PS_ITEM_ID;
	param_spec.subindex = param_subindex;
	param_spec.item.id = param_id;

	/*
	 * Get the variable type
	 */
	TYPE_SIZE type_size ;
	int rcode = ddi_get_var_type(env_info, &block_spec, &param_spec, &type_size);
	if (rcode != DDS_SUCCESS)
	{
		return (LC_LIST_PARAM_NOT_FOUND);
	}

	/* make up simulated param value based on type */

	param_value->type = type_size.type ;
	param_value->size = type_size.size ;
	switch (type_size.type) {
	case FLOATG_PT:
		param_value->val.f = 1.0f;
		break;
    case DOUBLEG_PT:
		param_value->val.d = 1.0;
		break;
	case UNSIGNED:
		param_value->val.u = 1;
		break;
	case INTEGER:
		param_value->val.i = 1;
		break;
	case ENUMERATED:
		param_value->val.i = 1;
		break;
	case BIT_ENUMERATED:
		param_value->val.i = 1;
		break;
	case INDEX:
		param_value->val.i = 1;
		break;
	case ASCII:
		param_value->val.s.str = wcsdup(L"dummy");
		param_value->val.s.len = 5;
		param_value->val.s.flags = FREE_STRING;
		break;
	default:
		param_value->val.i = 2;
		break;
	}

	return (SUCCESS);
}
