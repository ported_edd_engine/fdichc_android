#include "stdinc.h"
#include <CommonLibraries/AmsFileReader/AmsFileReader.h>
#include <Inf/NtcSpecies/Ntcassert2.h>

const size_t BOM_UTF16_SIZE = 2;
const size_t BOM_UTF8_SIZE = 3;  // Size in bytes of the BOM (Byte Order Mark) for UTF-8

AmsFileReader::AmsFileReader(void) 
: m_pFilePtr(NULL), 
  m_Encoding(FILE_ENCODING_UNKNOWN)
{
}

AmsFileReader::~AmsFileReader(void)
{
	CloseFile();
}

AmsFileReaderStatus AmsFileReader::OpenFile(LPCTSTR fileName)
{
	AmsFileReaderStatus status = FILE_READER_SUCCESS;

    if (m_pFilePtr != NULL)
	{
		m_sErrorInfo.Format(_T("Only one file can be opened at a time.  The current open file must be closed prior to opening a new file."));
		status = FILE_READER_FAILURE;
	}
	else
	{
        errno_t retVal = PS_wfopen_s(&m_pFilePtr, fileName, _T("rb"));
        if (retVal != 0)
		{
			m_sErrorInfo.Format(_T("Failed to open file: %s in mode 'rb' for reason: %s"), fileName, _T("N/A"));
			status = FILE_READER_FAILURE;
		}
		else
		{
			DetermineFileEncoding();
		}
	}

	return status;
}

void AmsFileReader::CloseFile()
{
	if (m_pFilePtr)
	{
		fclose(m_pFilePtr);
		m_pFilePtr = NULL;
	}
	m_Encoding = FILE_ENCODING_UNKNOWN;
	m_sfileName.Empty();
}

FileEncoding AmsFileReader::GetFileEncoding()
{
	return m_Encoding;
}

AmsFileReaderStatus AmsFileReader::GetString(CStdString& string)
{
	AmsFileReaderStatus status = FILE_READER_SUCCESS;

	if (m_pFilePtr == NULL)
	{
		m_sErrorInfo = _T("No valid file was found to read");
		return FILE_READER_INVALID_FILE;
	}

	if (m_Encoding == FILE_ENCODING_UNKNOWN)
	{
		m_sErrorInfo = _T("The file encoding could not be determined.  The file must be encoded with a byte order mark in order for fileBuffer to be set");
		return FILE_READER_FAILURE;
	}

	if (!feof(m_pFilePtr))
	{
		if (m_Encoding == FILE_ENCODING_ANSI)
		{
			char buffer[MAX_PATH];
			char* retVal = fgets(buffer, MAX_PATH, m_pFilePtr);
			string = buffer;  // CStdString will convert this to Unicode
			if (!retVal)
			{
				if (feof(m_pFilePtr))
				{
					status = FILE_READER_END_OF_FILE;
				}
				else
				{
					status = FILE_READER_READ_ERROR;
					m_sErrorInfo.Format(_T("Error reading file: %s for reason: %d"), m_sfileName, ferror(m_pFilePtr));
				}
			}
		}
		else
		{
#ifdef __GNUC__
            char buffer[MAX_PATH];
            char* retcVal = fgets(buffer, MAX_PATH, m_pFilePtr);
            size_t requiredBufSize = (size_t)PS_MultiByteToWideChar(CP_UTF8, 0, buffer, (int)MAX_PATH, NULL, 0);
            wchar_t* retVal;
            if (requiredBufSize == 0)
            {
                retVal = NULL;
            }
            else
            {
                retVal = new wchar_t[requiredBufSize+1];
                ntcassert2(retVal);

                size_t rVal = (size_t)PS_MultiByteToWideChar(CP_UTF8, 0, (char*)buffer, (int)MAX_PATH, retVal, requiredBufSize+1);

                if (rVal == 0)
                {
                   //dwRetVal = GetLastError();
                   //retVal = NULL;
                }
                else
                {
                   retVal[rVal] = WEOF;
                }
            }
            
#else
             wchar_t* retVal = _fgetts(string.GetBuffer(MAX_PATH), MAX_PATH, m_pFilePtr);
#endif
            string.ReleaseBuffer();
			if (!retVal)
			{
				if (feof(m_pFilePtr))
				{
					status = FILE_READER_END_OF_FILE;
				}
				else
				{
					status = FILE_READER_READ_ERROR;
					m_sErrorInfo.Format(_T("Error reading file: %s for reason: %d"), m_sfileName, ferror(m_pFilePtr));
				}
			}
		}
	}
	else
	{
		status = FILE_READER_END_OF_FILE;
	}

	return status;
}

AmsFileReaderStatus AmsFileReader::GetFileAsBuffer(wchar_t** fileBuffer, size_t* fileCharacterCount)
{
	AmsFileReaderStatus retVal = FILE_READER_SUCCESS;
	
	if (fileBuffer == NULL || fileCharacterCount == NULL)
	{
		m_sErrorInfo = _T("Parameter values must not be NULL");
		return FILE_READER_INVALID_PARAMETERS;
	}

	if (m_pFilePtr == NULL)
	{
		m_sErrorInfo = _T("No valid file was found to read");
		return FILE_READER_INVALID_FILE;
	}

	if (m_Encoding == FILE_ENCODING_UNKNOWN)
	{
		m_sErrorInfo = _T("The file encoding could not be determined.  The file must be encoded with a byte order mark in order for fileBuffer to be set");
		return FILE_READER_FAILURE;
	}

	// Deterimine the size of the file
	size_t fileDataSize = GetFileDataSize();

	if (fileDataSize == 0)
	{
		retVal = FILE_READER_FILE_EMPTY;
	}
	else
	{
		// Read the file in 
		BYTE* fileBufferBytes = new BYTE[fileDataSize];
		ntcassert2(fileBufferBytes);
		size_t bytesRead = fread(fileBufferBytes, 1, fileDataSize, m_pFilePtr);

		int readStatus = ferror(m_pFilePtr);
		if (readStatus)
		{
			m_sErrorInfo.Format(_T("Failed to read file: %s for reason: %d"), m_sfileName, readStatus); 
			retVal = FILE_READER_READ_ERROR;
		}
		else
		{
			if ((GetFileEncoding() == FILE_ENCODING_UTF8) || (GetFileEncoding() == FILE_ENCODING_ANSI))
			{
				// Convert it from UTF-8 to UTF-16
				size_t requiredBufSize = (size_t)PS_MultiByteToWideChar(CP_UTF8, 0, (char*)fileBufferBytes, (int)bytesRead, NULL, 0);

				if (requiredBufSize == 0)
				{
					(*fileBuffer) = NULL;
					(*fileCharacterCount) = 0;
				}
				else
				{
					(*fileBuffer) = new wchar_t[requiredBufSize+1];
					ntcassert2(*fileBuffer);
					(*fileCharacterCount) = requiredBufSize+1;

					size_t rVal = (size_t)PS_MultiByteToWideChar(CP_UTF8, 0, (char*)fileBufferBytes, (int)bytesRead, (*fileBuffer), (int)(*fileCharacterCount)); 

					if (rVal == 0)
					{
						//dwRetVal = GetLastError();
						(*fileBuffer) = NULL;
						(*fileCharacterCount) = 0;
					}
					else
					{
						(*fileBuffer)[requiredBufSize] = WEOF;
					}
				}
				delete[] fileBufferBytes;
			}
			else
			{
				(*fileBuffer) = (wchar_t*)fileBufferBytes;
				(*fileCharacterCount) = fileDataSize / sizeof(wchar_t);
			}
		}
	}

	return retVal;
}

bool AmsFileReader::EndOfFile()
{
	bool bEOF = true;

	if (m_pFilePtr)
	{
		int nEnd = feof(m_pFilePtr);
		if (nEnd == 0)
		{
			bEOF = false;
		}
	}
	return bEOF;
}

void AmsFileReader::DetermineFileEncoding()
{
	if (m_Encoding == FILE_ENCODING_UNKNOWN)
	{
		if (IsFileEncodedAsUTF8())
		{
			m_Encoding = FILE_ENCODING_UTF8;
		}
		else if (IsFileEncodedUTF16LE())
		{
			m_Encoding = FILE_ENCODING_UTF16LE;
		}
		else if (IsFileEncodedUTF16BE())
		{
			m_Encoding = FILE_ENCODING_UTF16BE;
		}
		else // ANSI has no byte order mark; if none is found, assume ANSI
		{
			m_Encoding = FILE_ENCODING_ANSI;
		}
	}
}

bool AmsFileReader::IsFileEncodedAsUTF8()
{
	size_t bomSize = GetBOMSize(FILE_ENCODING_UTF8);
	BYTE* bom = new BYTE[bomSize];
	bom[0] = 0xEF;
	bom[1] = 0xBB;
	bom[2] = 0xBF;

	bool bFileIsUTF8 = IsCorrectBomForFile(bom, bomSize);

	if (!bFileIsUTF8)
		fseek(m_pFilePtr, 0, SEEK_SET);

	delete[] bom;

	return bFileIsUTF8;
}

bool AmsFileReader::IsFileEncodedUTF16LE()
{
	size_t bomSize = GetBOMSize(FILE_ENCODING_UTF16LE);
	BYTE* bom = new BYTE[bomSize];
	bom[0] = 0xFF;
	bom[1] = 0xFE;

	bool bFileIsUTF16LE = IsCorrectBomForFile(bom, bomSize);

	if (!bFileIsUTF16LE)
		fseek(m_pFilePtr, 0, SEEK_SET);

	delete[] bom;

	return bFileIsUTF16LE;
}

bool AmsFileReader::IsFileEncodedUTF16BE()
{
	size_t bomSize = GetBOMSize(FILE_ENCODING_UTF16BE);
	BYTE* bom = new BYTE[bomSize];
	bom[0] = 0xFE;
	bom[1] = 0xFF;

	bool bFileIsUTF16BE = IsCorrectBomForFile(bom, bomSize);

	if (!bFileIsUTF16BE)
		fseek(m_pFilePtr, 0, SEEK_SET);

	delete[] bom;

	return bFileIsUTF16BE;
}

bool AmsFileReader::IsCorrectBomForFile(BYTE* bomValues, size_t bomSize)
{
	bool bEncodingMatches = true;

	if (m_pFilePtr != NULL)
	{
		BYTE* fileBuffer = new BYTE[bomSize];
		size_t bytesRead = fread(fileBuffer, 1, bomSize, m_pFilePtr);

		if (bytesRead == bomSize)
		{
			for (size_t i = 0; i < bomSize; i++)
			{
				if (fileBuffer[i] != bomValues[i])
				{
					bEncodingMatches = false;
				}
			}
		}

		delete[] fileBuffer;
	}

	return bEncodingMatches;
}

size_t AmsFileReader::GetBOMSize(FileEncoding fe)
{
	switch (fe)
	{
	case FILE_ENCODING_UTF8:
		return BOM_UTF8_SIZE;
	case FILE_ENCODING_UTF16BE:
	case FILE_ENCODING_UTF16LE:
		return BOM_UTF16_SIZE;
	case FILE_ENCODING_UNKNOWN:
	default:
		return 0;
	}
}

size_t AmsFileReader::GetFileDataSize()
{
	fseek(m_pFilePtr, 0, SEEK_END);
	size_t totalFileSize = ftell(m_pFilePtr);
	AdvancePastBom();

	size_t dataFileSize = totalFileSize - GetBOMSize(GetFileEncoding());
	
	return dataFileSize;
}

void AmsFileReader::AdvancePastBom()
{
	fseek(m_pFilePtr, 0, SEEK_SET);
	size_t bomSize = GetBOMSize(GetFileEncoding());
	fseek(m_pFilePtr, (long)bomSize, 0);
}

void AmsFileReader::GetErrorInfo(CStdString& sErrorInfo)
{
	sErrorInfo = m_sErrorInfo;
}
