/*
 *  @(#) $Id: dds_err.cpp,v 1.1 2012/07/10 20:59:41 rgretta Exp $
 *  Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	rtn_code.c -
 *  These are the return codes for all routines.  There is a set of
 *  general purpose codes and a set for each of the subsystems.
 */

#include "stdafx.h"
#include "rtn_code.h"
#include "ddi_lib.h"


#define nelem(x) (sizeof(x)/sizeof(x[0]))


struct err_struct {
	int             code;
	wchar_t         *string;
};

static const
struct err_struct errstrs[] = {

#include "rtn_def.h"

};

/*********************************************************************
 *
 *	Name:	dds_error_string
 *
 *	ShortDesc:	format dds error string
 *
 *	Description:
 *		format dds error string
 *
 *	Inputs:
 *		err -			error
 *
 *	Returns:
 *		formatted error string
 *
 *
 *********************************************************************/

wchar_t           *
dds_error_string(int             err)
{
    const struct err_struct	*ptr;
    const struct err_struct	*end;
    static wchar_t		buf[BUF_LEN]{0};
 
    end = errstrs + nelem(errstrs);
    ptr = errstrs;

    /*
     * Scan through the structure of error messages to find the one with
     * the specified error code.  If it has a message associated with it,
     * get a copy of the English version of the message (right now, English
     * is the only version available) and return.  If there is no message,
     * or the string translation fails, get out of this loop and fill the
     * output buffer with the 'Error not found' message, then return.
     */
 
    for (ptr = errstrs; ptr < end; ptr++)
	{
       if ((err == ptr->code) && (ptr->string != NULL))
		{
			return (ptr->string);
		}
    }
 
    swprintf(buf, BUF_LEN, L"Error #%d: not found", err);
 
    return (buf);
}
