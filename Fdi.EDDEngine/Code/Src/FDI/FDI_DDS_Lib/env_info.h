/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)env_info.h	30.3  30  14 Nov 1996
 */
 
#ifndef ENV_INFO_FDI_H
#define ENV_INFO_FDI_H
#include "dds_upcl.h"
/*
 * This file contains the definition for ENV_INFO.  It is used to pass
 * environment information throughout the application and libraries.  This
 * structure may be modified as needed to communicate needed parameters to
 * appropriate sub-systems.  The block_handle element within ENV_INFO is
 * the only element that is required by DDS.
 */

typedef struct tag_ENV_INFO {
	BLOCK_HANDLE	block_handle;	/* This is used to specify the block */
  	void			*app_info;
	void*			value_spec;
    wchar_t			lang_code[10];
	enum HandleType
	{
		BlockHandle,
		DeviceHandle 
	};

	HandleType handle_type;

} ENV_INFO;

#endif 	/* ENV_INFO_H */
