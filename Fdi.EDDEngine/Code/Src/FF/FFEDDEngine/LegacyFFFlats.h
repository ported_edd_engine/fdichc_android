

#include "DDI_LIB.H"
#include "LegacyFF.h"

// derived class from DEPBIN
class FF_ConvDEPBIN : public nsEDDEngine::DEPBIN
{
public:
	FF_ConvDEPBIN( ::DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvDEPBIN() {};

private:
	FF_ConvDEPBIN();
};

// FLAT_ARRAY classes
class FF_ConvFLAT_ARRAY : public nsEDDEngine::FLAT_ARRAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	FF_ConvFLAT_ARRAY( ::FLAT_ARRAY *, CLegacyFF * );  

	virtual ~FF_ConvFLAT_ARRAY() {};

private:
	FF_ConvFLAT_ARRAY();

};

class FF_ConvARRAY_DEPBIN : public nsEDDEngine::ARRAY_DEPBIN
{
public:
	FF_ConvARRAY_DEPBIN( ::ARRAY_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvARRAY_DEPBIN() {};

private:
	FF_ConvARRAY_DEPBIN();
};


// FLAT_COLLECTION classes
class FF_ConvFLAT_COLLECTION : public nsEDDEngine::FLAT_COLLECTION
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	FF_ConvFLAT_COLLECTION( ::FLAT_COLLECTION *, CLegacyFF * );

	virtual ~FF_ConvFLAT_COLLECTION() {};

private:
	FF_ConvFLAT_COLLECTION();
};

class FF_ConvCOLLECTION_DEPBIN : public nsEDDEngine::COLLECTION_DEPBIN
{
public:
	FF_ConvCOLLECTION_DEPBIN( ::COLLECTION_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvCOLLECTION_DEPBIN() {};

private:
	FF_ConvCOLLECTION_DEPBIN();
};


// FLAT_ITEM_ARRAY classes
class FF_ConvFLAT_ITEM_ARRAY : public nsEDDEngine::FLAT_ITEM_ARRAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	FF_ConvFLAT_ITEM_ARRAY( ::FLAT_ITEM_ARRAY *, CLegacyFF * );

	virtual ~FF_ConvFLAT_ITEM_ARRAY() {};

private:
	FF_ConvFLAT_ITEM_ARRAY();
};

class FF_ConvITEM_ARRAY_DEPBIN : public nsEDDEngine::ITEM_ARRAY_DEPBIN
{
public:
	FF_ConvITEM_ARRAY_DEPBIN( ::ITEM_ARRAY_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvITEM_ARRAY_DEPBIN() {};

private:
	FF_ConvITEM_ARRAY_DEPBIN();
};


// FLAT_LIST classes
class FF_ConvFLAT_LIST : public nsEDDEngine::FLAT_LIST
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	FF_ConvFLAT_LIST( ::FLAT_LIST *, CLegacyFF * );

	virtual ~FF_ConvFLAT_LIST() {};

private:
	FF_ConvFLAT_LIST();
};

class FF_ConvLIST_DEPBIN : public nsEDDEngine::LIST_DEPBIN
{
public:
	FF_ConvLIST_DEPBIN( ::LIST_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvLIST_DEPBIN() {};

private:
	FF_ConvLIST_DEPBIN();
};


// FLAT_FILE classes
class FF_ConvFLAT_FILE : public nsEDDEngine::FLAT_FILE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];

	FF_ConvFLAT_FILE( ::FLAT_FILE *, CLegacyFF * );

	virtual ~FF_ConvFLAT_FILE() {};

private:
	FF_ConvFLAT_FILE();
};

class FF_ConvFILE_DEPBIN : public nsEDDEngine::FILE_DEPBIN
{
public:
	FF_ConvFILE_DEPBIN( ::FILE_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvFILE_DEPBIN() {};

private:
	FF_ConvFILE_DEPBIN();
};

// FLAT_RESP_CODE classes
class FF_ConvFLAT_RESP_CODE : public nsEDDEngine::FLAT_RESP_CODE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_RESP_CODE( ::FLAT_RESP_CODE *, CLegacyFF * );

	virtual ~FF_ConvFLAT_RESP_CODE() {};

private:
	FF_ConvFLAT_RESP_CODE();
};

class FF_ConvRESP_CODE_DEPBIN : public nsEDDEngine::RESP_CODE_DEPBIN
{
public:
	FF_ConvRESP_CODE_DEPBIN( ::RESP_CODE_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvRESP_CODE_DEPBIN() {};

private:
	FF_ConvRESP_CODE_DEPBIN();
};


// FLAT_VAR classes
class FF_ConvFLAT_VAR : public nsEDDEngine::FLAT_VAR
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_VAR( ::FLAT_VAR *, CLegacyFF * );

	virtual ~FF_ConvFLAT_VAR() {};

private:
	FF_ConvFLAT_VAR();
};

class FF_ConvVAR_DEPBIN : public nsEDDEngine::VAR_DEPBIN
{
public:
	FF_ConvVAR_DEPBIN( ::FLAT_VAR *, CLegacyFF * );

	virtual ~FF_ConvVAR_DEPBIN() {};

private:
	FF_ConvVAR_DEPBIN();
};

class FF_ConvFLAT_CHART : public nsEDDEngine::FLAT_CHART
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_CHART( ::FLAT_CHART *, CLegacyFF * );

	virtual ~FF_ConvFLAT_CHART() {};

private:
	FF_ConvFLAT_CHART();

};

class FF_ConvCHART_DEPBIN : public nsEDDEngine::CHART_DEPBIN
{
public:
	FF_ConvCHART_DEPBIN( ::CHART_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvCHART_DEPBIN() {};

private:
	FF_ConvCHART_DEPBIN();
};


class FF_ConvFLAT_GRAPH : public nsEDDEngine::FLAT_GRAPH
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_GRAPH( ::FLAT_GRAPH *, CLegacyFF * );

	virtual ~FF_ConvFLAT_GRAPH() {};

private:
	FF_ConvFLAT_GRAPH();

};

class FF_ConvGRAPH_DEPBIN : public nsEDDEngine::GRAPH_DEPBIN
{
public:
	FF_ConvGRAPH_DEPBIN( ::GRAPH_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvGRAPH_DEPBIN() {};

private:
	FF_ConvGRAPH_DEPBIN();
};

class FF_ConvFLAT_AXIS : public nsEDDEngine::FLAT_AXIS
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_AXIS( ::FLAT_AXIS *, CLegacyFF * );

	virtual ~FF_ConvFLAT_AXIS() {};

private:
	FF_ConvFLAT_AXIS();

};

class FF_ConvAXIS_DEPBIN : public nsEDDEngine::AXIS_DEPBIN
{
public:
	FF_ConvAXIS_DEPBIN( ::AXIS_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvAXIS_DEPBIN() {};

private:
	FF_ConvAXIS_DEPBIN();
};

class FF_ConvFLAT_WAVEFORM : public nsEDDEngine::FLAT_WAVEFORM
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_WAVEFORM( ::FLAT_WAVEFORM *, CLegacyFF * );

	virtual ~FF_ConvFLAT_WAVEFORM() {};

private:
	FF_ConvFLAT_WAVEFORM();

};

class FF_ConvWAVEFORM_DEPBIN : public nsEDDEngine::WAVEFORM_DEPBIN
{
public:
	FF_ConvWAVEFORM_DEPBIN( ::WAVEFORM_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvWAVEFORM_DEPBIN() {};

private:
	FF_ConvWAVEFORM_DEPBIN();
};

class FF_ConvFLAT_SOURCE : public nsEDDEngine::FLAT_SOURCE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_SOURCE( ::FLAT_SOURCE *, CLegacyFF *pLegacyFF );

	virtual ~FF_ConvFLAT_SOURCE() {};

private:
	FF_ConvFLAT_SOURCE();

};

class FF_ConvSOURCE_DEPBIN : public nsEDDEngine::SOURCE_DEPBIN
{
public:
	FF_ConvSOURCE_DEPBIN( ::SOURCE_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvSOURCE_DEPBIN() {};

private:
	FF_ConvSOURCE_DEPBIN();
};

class FF_ConvFLAT_GRID : public nsEDDEngine::FLAT_GRID
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_GRID( ::FLAT_GRID *, CLegacyFF * );

	virtual ~FF_ConvFLAT_GRID() {};

private:
	FF_ConvFLAT_GRID();

};

class FF_ConvGRID_DEPBIN : public nsEDDEngine::GRID_DEPBIN
{
public:
	FF_ConvGRID_DEPBIN( ::GRID_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvGRID_DEPBIN() {};

private:
	FF_ConvGRID_DEPBIN();
};

class FF_ConvFLAT_IMAGE : public nsEDDEngine::FLAT_IMAGE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_IMAGE( ::FLAT_IMAGE *, CLegacyFF * );

	virtual ~FF_ConvFLAT_IMAGE() {};

private:
	FF_ConvFLAT_IMAGE();

};

class FF_ConvIMAGE_DEPBIN : public nsEDDEngine::IMAGE_DEPBIN
{
public:
	FF_ConvIMAGE_DEPBIN( ::IMAGE_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvIMAGE_DEPBIN() {};

private:
	FF_ConvIMAGE_DEPBIN();
};

class FF_ConvFLAT_METHOD : public nsEDDEngine::FLAT_METHOD
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_METHOD( ::FLAT_METHOD *, CLegacyFF * );

	virtual ~FF_ConvFLAT_METHOD() {};

private:
	FF_ConvFLAT_METHOD();

};

class FF_ConvMETHOD_DEPBIN : public nsEDDEngine::METHOD_DEPBIN
{
public:
	FF_ConvMETHOD_DEPBIN( ::METHOD_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvMETHOD_DEPBIN() {};

private:
	FF_ConvMETHOD_DEPBIN();
};

class FF_ConvFLAT_MENU : public nsEDDEngine::FLAT_MENU
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_MENU( ::FLAT_MENU *, CLegacyFF * );

	virtual ~FF_ConvFLAT_MENU() {};

private:
	FF_ConvFLAT_MENU();

};

class FF_ConvMENU_DEPBIN : public nsEDDEngine::MENU_DEPBIN
{
public:
	FF_ConvMENU_DEPBIN( ::MENU_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvMENU_DEPBIN() {};

private:
	FF_ConvMENU_DEPBIN();
};


class FF_ConvFLAT_EDIT_DISPLAY : public nsEDDEngine::FLAT_EDIT_DISPLAY
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_EDIT_DISPLAY( ::FLAT_EDIT_DISPLAY *, CLegacyFF * );

	virtual ~FF_ConvFLAT_EDIT_DISPLAY() {};

private:
	FF_ConvFLAT_EDIT_DISPLAY();

};

class FF_ConvEDIT_DISPLAY_DEPBIN : public nsEDDEngine::EDIT_DISPLAY_DEPBIN
{
public:
	FF_ConvEDIT_DISPLAY_DEPBIN( ::EDIT_DISPLAY_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvEDIT_DISPLAY_DEPBIN() {};

private:
	FF_ConvEDIT_DISPLAY_DEPBIN();
};

class FF_ConvFLAT_REFRESH : public nsEDDEngine::FLAT_REFRESH
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_REFRESH( ::FLAT_REFRESH *, CLegacyFF * );

	virtual ~FF_ConvFLAT_REFRESH() {};

private:
	FF_ConvFLAT_REFRESH();

};

class FF_ConvREFRESH_DEPBIN : public nsEDDEngine::REFRESH_DEPBIN
{
public:
	FF_ConvREFRESH_DEPBIN( ::REFRESH_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvREFRESH_DEPBIN() {};

private:
	FF_ConvREFRESH_DEPBIN();
};

class FF_ConvFLAT_UNIT : public nsEDDEngine::FLAT_UNIT
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_UNIT( ::FLAT_UNIT *, CLegacyFF * );

	virtual ~FF_ConvFLAT_UNIT() {};

private:
	FF_ConvFLAT_UNIT();

};

class FF_ConvUNIT_DEPBIN : public nsEDDEngine::UNIT_DEPBIN
{
public:
	FF_ConvUNIT_DEPBIN( ::UNIT_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvUNIT_DEPBIN() {};

private:
	FF_ConvUNIT_DEPBIN();
};

class FF_ConvFLAT_WAO : public nsEDDEngine::FLAT_WAO
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_WAO( ::FLAT_WAO *, CLegacyFF * );

	virtual ~FF_ConvFLAT_WAO() {};

private:
	FF_ConvFLAT_WAO();

};

class FF_ConvWAO_DEPBIN : public nsEDDEngine::WAO_DEPBIN
{
public:
	FF_ConvWAO_DEPBIN( ::WAO_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvWAO_DEPBIN() {};

private:
	FF_ConvWAO_DEPBIN();
};

class FF_ConvFLAT_BLOCK : public nsEDDEngine::FLAT_BLOCK
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_BLOCK( ::FLAT_BLOCK *, CLegacyFF * );

	virtual ~FF_ConvFLAT_BLOCK() {};

private:
	FF_ConvFLAT_BLOCK();

};

class FF_ConvBLOCK_DEPBIN : public nsEDDEngine::BLOCK_DEPBIN
{
public:
	FF_ConvBLOCK_DEPBIN( ::BLOCK_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvBLOCK_DEPBIN() {};

private:
	FF_ConvBLOCK_DEPBIN();
};

class FF_ConvFLAT_RECORD : public nsEDDEngine::FLAT_RECORD
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_RECORD( ::FLAT_RECORD *, CLegacyFF * );

	virtual ~FF_ConvFLAT_RECORD() {};

private:
	FF_ConvFLAT_RECORD();

};

class FF_ConvRECORD_DEPBIN : public nsEDDEngine::RECORD_DEPBIN
{
public:
	FF_ConvRECORD_DEPBIN( ::RECORD_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvRECORD_DEPBIN() {};

private:
	FF_ConvRECORD_DEPBIN();
};

class FF_ConvFLAT_VAR_LIST : public nsEDDEngine::FLAT_VAR_LIST
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_VAR_LIST( ::FLAT_VAR_LIST *, CLegacyFF * );

	virtual ~FF_ConvFLAT_VAR_LIST() {};

private:
	FF_ConvFLAT_VAR_LIST();

};

class FF_ConvVAR_LIST_DEPBIN : public nsEDDEngine::VAR_LIST_DEPBIN
{
public:
	FF_ConvVAR_LIST_DEPBIN( ::VAR_LIST_DEPBIN *, CLegacyFF * );

	virtual ~FF_ConvVAR_LIST_DEPBIN() {};

private:
	FF_ConvVAR_LIST_DEPBIN();
};

class FF_ConvFLAT_TEMPLATE : public nsEDDEngine::FLAT_TEMPLATE
{
public:
	static const nsEDDEngine::AttributeName attr_map[];
	FF_ConvFLAT_TEMPLATE(int iBlockInstance, ::FLAT_TEMPLATE *, CLegacyFF *);

	virtual ~FF_ConvFLAT_TEMPLATE() {};

private:
	FF_ConvFLAT_TEMPLATE();
	void PostProcessFlatTemplate(int iBlockInstance, nsEDDEngine::FLAT_TEMPLATE* pFlatTemplate, CLegacyFF *pLegacyFF);

};

class FF_ConvTEMPLATE_DEPBIN : public nsEDDEngine::TEMPLATE_DEPBIN
{
public:
	FF_ConvTEMPLATE_DEPBIN(::TEMPLATE_DEPBIN *, CLegacyFF *);

	virtual ~FF_ConvTEMPLATE_DEPBIN() {};

private:
	FF_ConvTEMPLATE_DEPBIN();
};