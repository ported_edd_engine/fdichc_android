#ifndef DEBUGON_H
#define DEBUGON_H

#include <exception>
#include <Uc.h>
#ifdef _WIN32
#include "crtdbg.h"
#endif
//#include "PlatformCommon.h"

//LOOK at this if something freaky happens. This disables a warning 
//that may be important in the future

#pragma warning(disable : 4793)

inline void DEBUG_BREAK(LPCTSTR 
#ifdef _DEBUG						
						message
#endif
						)
{
#ifdef _DEBUG
	Uc2A messageA(message);
	if(1 == PS_CrtDbgReport(_CRT_ASSERT, __FILE__, __LINE__, NULL, messageA.asLPCSTR()))
	{
		PS_CrtDbgBreak();
	}
#endif
}

//note that strings longer thatn 512 chars cannot be passed to PS_TRACE.
inline void
SAFE_TRACE(LPCTSTR str)
{
	CStdString s = str;
	if(s.GetLength() > 500)
	{
		PS_TRACE(s.Left(500));
	}
	else
	{
		PS_TRACE(s);
	}
}

#define __DONT_USE_FUNCTION_DEBUG__		// Comment this out if you want to use the FunctionDebug class!!

#ifdef __DONT_USE_FUNCTION_DEBUG__

class FunctionDebug
{
public:
	FunctionDebug() {}

	void DEBUG_WHEN(int) {}

	void SET_METHOD(LPCTSTR , LPCTSTR ) {}
	void SET_FUNCTION(LPCTSTR ) {}

	void ENTER_METHOD() {}

	void ENTER_METHOD(LPCTSTR , ...) {}

	void OUTPUT_METHOD_MESSAGE(LPCTSTR , ...) {}
	void OUTPUT_MESSAGE(LPCTSTR , ...) {}
	void OUTPUT_METHOD_EVENT(LPCTSTR , ...) {}

	void OUTPUT_RETURN(HRESULT ) {}

	long m_uniq_id;	// made public to ease conversion of legacy code.
};

#else

/******************************************************************************
Class:			FunctionDebug

Description:	Function Debug 'Macros' that haven converted into a class.

				The FunctionDebug class is activated by including debugon.h in
				the modules of interest and instrumenting the code with the
				a FunctionDebug object and method calls.

				This class was originally a set a debug macros designed to provide
				a level of function call tracing. They have been converted to 
				a class to simply enable OneRealm's i18nExpeditor tool to filter
				out occurances these debug calls. i18nExpeditor is not able to
				filter out macros, it is able to filter out functions. KSM

				uniq_id - There is a slight glitch with FunctionDebug::m_uniq_id.
				When these functions were macros they would directly reference the 
				uniq_id member variable of whatever class's member functions the
				macros where placed in. Now that uniq_id is a member of 
				FunctionDebug, FunctionDebug's copy of uniq_id 
				(FunctionDebug::m_m_uniq_id) must be set/updated explicitly. Each
				class that contains its own uniq_id member and also uses
				FunctionDebug calls must explicity keep FunctionDebug::m_uniq_id
				updated by setting its value appropriately.

				For Example:
					SomeClass::SomeMethod()
					{
						FunctionDebug FD;
						FD.m_uniq_id = uniq_id;		// update FD's copy of uniq_id
						...
					}

Example:
	an example of using the FunctionDebug class:
	
	#include "debugon.h"

	...

	CBlah::Method(long arg1, char* arg2)
	{
		FunctionDebug FD;
		FD.SET_METHOD(_T("CBlah"),_T("Method"));          //tell the debugging code what class & method we're in.
		FD.DEBUG_WHEN(g_debugBlah);               //when do you want the debug messages to show up in the log?
		FD.ENTER_METHOD(_T("%ld, %s"), arg1, arg2); //output a message in the log that we've called this method.

		//do some work
		long value = blah blah blah;

		//log a message that something happened
		//the message will go to only the application text-file log.
		FD.OUTPUT_METHOD_MESSAGE("value=%ld", value);

		//an error occurred - output an event and a log message
		//the message will go to both the windows event log and the application text-file log.
		FD.OUTPUT_METHOD_EVENT("Error: invalid value.    value=%ld", value);

		HRESULT hr;
		hr = blah blah blah;

		//log a return-value message, and return.
		FD.OUTPUT_RETURN(hr);
		return hr;
	}

Author:			
******************************************************************************/
#undef _BUFFER_SIZE
#define _BUFFER_SIZE 1024

class FunctionDebug
{
public:
	FunctionDebug();

	void DEBUG_WHEN(int);

	void SET_METHOD(LPCTSTR the_class, LPCTSTR method);
	void SET_FUNCTION(LPCTSTR method);

	void ENTER_METHOD();
	void ENTER_METHOD(LPCTSTR frmt, ...);

	void OUTPUT_METHOD_MESSAGE(LPCTSTR frmt, ...);
	void OUTPUT_MESSAGE(LPCTSTR frmt, ...);
	void OUTPUT_METHOD_EVENT(LPCTSTR frmt, ...);

	void OUTPUT_RETURN(HRESULT result);

	long m_uniq_id;	// made public to ease conversion of legacy code.

private:
	void SEND_METHOD_MESSAGE(LPCTSTR);
	void SEND_METHOD_EVENT(LPCTSTR);

	void FORMAT_METHOD_CALL(LPTSTR);
	void FORMAT_MESSAGE_LOCATION(LPTSTR);
	void FORMAT_METHOD_CALL_MSG(LPTSTR _format_method_call_dest, LPCTSTR _msg);
	void FORMAT_METHOD_CALL_ARGS(LPTSTR _format_method_call_dest, LPCTSTR _method_args);
	void FORMAT_EXCEPTION(LPTSTR _format_exception_dest);
	void FORMAT_EXCEPTION_ARGS(LPTSTR _format_exception_dest, LPCTSTR _format_exception_args);
	void FORMAT_RETURN(LPTSTR _FORMAT_RETURN_s, long _result);

	LPCTSTR m_className;
	LPCTSTR m_methodName;
	TCHAR m_szOutputException[_BUFFER_SIZE];
	int m_fDebugThisMethod;
};

inline
FunctionDebug::FunctionDebug() : m_className(0), m_methodName(0), m_uniq_id(0), m_fDebugThisMethod(0)
{
#if defined(DEBUG)
	memset(m_szOutputException, 0x0, sizeof(m_szOutputException));
#endif // defined(DEBUG)
}

inline void
FunctionDebug::DEBUG_WHEN(int
#ifdef DEBUG
						  intExpression
#endif
						  )
{
#if defined(DEBUG)
	m_fDebugThisMethod = intExpression;
#endif // defined(DEBUG)
}

//The base function for writing a message to the log
inline void 
FunctionDebug::SEND_METHOD_MESSAGE(LPCTSTR
#ifdef DEBUG
								   str
#endif
								   ) 
{	
#if defined(DEBUG)
	g_log.Log(str); 
#endif // defined(DEBUG)
}

//The base function for writing an event to the log
inline void 
FunctionDebug::SEND_METHOD_EVENT(LPCTSTR
#ifdef DEBUG
								 str
#endif
								 ) 
{ 
#if defined(DEBUG)
	g_EventLogWrapper.LogEvent(EVENTLOG_ERROR_TYPE, str); 
#endif // defined(DEBUG)
}

//----------------------------------------------------------------------------


//Make a call to this macro at the beginning of a method.
inline void
FunctionDebug::SET_METHOD(LPCTSTR
#ifdef DEBUG
						  the_class
#endif
						  , LPCTSTR
#ifdef DEBUG
						  method
#endif
						  )
{
#if defined(DEBUG)
	m_className = the_class;
	m_methodName = method;
#endif // defined(DEBUG)
}

//Make a call to this macro at the beginning of a function
inline void
FunctionDebug::SET_FUNCTION(LPCTSTR
#ifdef DEBUG
							method
#endif
							)
{
#if defined(DEBUG)
	m_className = NULL; 
	m_uniq_id=0; 
	m_methodName = method;
#endif // defined(DEBUG)
}

//----------------------------------------------------------------------------


//Message formatting macros.

inline void 
FunctionDebug::FORMAT_MESSAGE_LOCATION(LPTSTR _format_location_dest)
{
	_timeb now;
	_ftime(&now);
	DWORD _dwThreadID = GetCurrentThreadId();
	_sntprintf(_format_location_dest, _BUFFER_SIZE, _T("0x%8.8lx	\"%03d.%03d\"	\"%hs\"	%ld	"), _dwThreadID, (long)(now.time % 1000), now.millitm, __FILE__, __LINE__);
}


//class<inst>::method()    or
//class::method()          or
//function()
inline void
FunctionDebug::FORMAT_METHOD_CALL(LPTSTR _format_method_call_dest)
{
	TCHAR _where[_BUFFER_SIZE] = {0};
	FORMAT_MESSAGE_LOCATION(_where);
	if(m_className)
	{
		if(m_uniq_id)
		{
			_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s<%ld>::%s() called.\""), _where, m_className, m_uniq_id, m_methodName);
		}
		else
		{
			_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s::%s() called.\""), _where, m_className, m_methodName);
		}
	}
	else
	{
		_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s() called.\""), _where, m_methodName);
	}
}


//class<inst>::method() <message>    or
//class::method() <message>          or
//function() <message>
inline void
FunctionDebug::FORMAT_METHOD_CALL_MSG(LPTSTR _format_method_call_dest, LPCTSTR _msg)
{
	TCHAR _where[_BUFFER_SIZE] = {0};
	FORMAT_MESSAGE_LOCATION(_where);
	if(m_className)
	{
		if(m_uniq_id)
		{
			_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s<%ld>::%s() %s\""), _where, m_className, m_uniq_id, m_methodName, _msg);
		}
		else
		{
			_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s::%s() %s\""), _where, m_className, m_methodName, _msg);
		}
	}
	else
	{
		_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s() %s\""), _where, m_methodName, _msg);
	}
}


//class<inst>::method(<args>)    or
//class::method(<args>)          or
//function(<args>)
inline void
FunctionDebug::FORMAT_METHOD_CALL_ARGS(LPTSTR _format_method_call_dest, LPCTSTR _method_args)
{
	TCHAR _where[_BUFFER_SIZE] = {0};
	FORMAT_MESSAGE_LOCATION(_where);
	if(m_className)
	{
		if(m_uniq_id)
		{
			_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s<%ld>::%s(%s) called.\""), _where, m_className, m_uniq_id, m_methodName, _method_args);
		}
		else
		{
			 _sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s::%s(%s) called.\""), _where, m_className, m_methodName, _method_args);
		}
	}
	else
	{
		_sntprintf(_format_method_call_dest, _BUFFER_SIZE, _T("%s \"%s(%s) called.\""), _where, m_methodName, _method_args);
	}
}


//exception caught in class<inst>::method()    or
//exception caught in class::method()          or
//exception caught in function()
inline void
FunctionDebug::FORMAT_EXCEPTION(LPTSTR _format_exception_dest)
{
	TCHAR _where[_BUFFER_SIZE] = {0};
  FORMAT_MESSAGE_LOCATION(_where);
  if(m_className)
    if(m_uniq_id)
      _sntprintf(_format_exception_dest, _BUFFER_SIZE, _T("%s \"exception caught in %s<%ld>::%s()\""), _where, m_className, m_uniq_id, m_methodName);
    else
      _sntprintf(_format_exception_dest, _BUFFER_SIZE, _T("%s \"exception caught in %s::%s()\""), _where, m_className, m_methodName);
  else
    _sntprintf(_format_exception_dest, _BUFFER_SIZE, _T("%s \"exception caught in %s()\""), _where, m_methodName);
}


//exception caught in class<inst>::method(<args>)    or
//exception caught in class::method(<args>)          or
//exception caught in function(<args>)
inline void 
FunctionDebug::FORMAT_EXCEPTION_ARGS(LPTSTR /*_format_exception_dest*/, LPCTSTR _format_exception_args)
{
  TCHAR _where[_BUFFER_SIZE] = {0};
  FORMAT_MESSAGE_LOCATION(_where);
  if(m_className)
    if(m_uniq_id)
      _sntprintf(m_szOutputException, _BUFFER_SIZE, _T("%s \"exception caught in %s<%ld>::%s(%s)\""), _where, m_className, m_uniq_id, m_methodName, _format_exception_args);
    else
      _sntprintf(m_szOutputException, _BUFFER_SIZE, _T("%s \"exception caught in %s::%s(%s)\""), _where, m_className, m_methodName, _format_exception_args);
  else
    _sntprintf(m_szOutputException, _BUFFER_SIZE, _T("%s \"exception caught in %s(%s)\""), _where, m_methodName, _format_exception_args);
}

inline void 
FunctionDebug::FORMAT_RETURN(LPTSTR _FORMAT_RETURN_s, long _result)
{
  TCHAR _where[_BUFFER_SIZE] = {0};
  FORMAT_MESSAGE_LOCATION(_where);
  if(m_className)
    if(m_uniq_id)
      _sntprintf(_FORMAT_RETURN_s, _BUFFER_SIZE, _T("%s \"%s<%ld>::%s() returning 0x%8.8lx\""), _where, m_className, m_uniq_id, m_methodName, _result);
    else
      _sntprintf(_FORMAT_RETURN_s, _BUFFER_SIZE, _T("%s \"%s::%s() returning 0x%8.8lx\""), _where, m_className, m_methodName, _result);
  else
    _sntprintf(_FORMAT_RETURN_s, _BUFFER_SIZE, _T("%s \"%s() returning 0x%8.8lx\""), _where, m_methodName, _result);
}


//----------------------------------------------------------------------------


//Make a call to one of these macros at the beginning of a method, if you want
//a 'class::method(arg, arg, ...)' message to appear.

//class<inst>::method() called
inline void
FunctionDebug::ENTER_METHOD()
{
#if defined(DEBUG)
	if (m_fDebugThisMethod)
	{
		TCHAR _enter_method_s[_BUFFER_SIZE] = {0};
		FORMAT_METHOD_CALL(_enter_method_s);
		SEND_METHOD_MESSAGE(_enter_method_s);
	}
#endif // defined(DEBUG)
}


inline void
FunctionDebug::ENTER_METHOD(LPCTSTR 
#ifdef DEBUG
							frmt
#endif
							, ... )
{
#if defined(DEBUG)
	if (m_fDebugThisMethod)
	{
		va_list ap;
		va_start(ap, frmt);

		TCHAR _enter_method_args[_BUFFER_SIZE] = {0};
		_vsntprintf(_enter_method_args, _BUFFER_SIZE, frmt, ap);

		va_end(ap);

		TCHAR _enter_method_s[_BUFFER_SIZE] = {0};
		FORMAT_METHOD_CALL_ARGS(_enter_method_s, _enter_method_args);
		SEND_METHOD_MESSAGE(_enter_method_s);
		
	}
#endif // defined(DEBUG)
}

inline void 
FunctionDebug::OUTPUT_METHOD_MESSAGE(LPCTSTR
#ifdef DEBUG
									 frmt
#endif
									 , ...)
{
#if defined(DEBUG)
	if (m_fDebugThisMethod)
	{
		va_list ap;
		va_start(ap, frmt);

		TCHAR _output_method_args[_BUFFER_SIZE] = {0};
		_vsntprintf(_output_method_args, _BUFFER_SIZE, frmt, ap);

		va_end(ap);

		TCHAR _output_method_s[_BUFFER_SIZE] = {0};
		FORMAT_METHOD_CALL_MSG(_output_method_s, _output_method_args);
		SEND_METHOD_MESSAGE(_output_method_s);
	}
#endif // defined(DEBUG)
}

inline void 
FunctionDebug::OUTPUT_MESSAGE(LPCTSTR
#ifdef DEBUG
							  frmt
#endif
							  , ...)
{
#if defined(DEBUG)
	if (m_fDebugThisMethod)
	{
		va_list ap;
		va_start(ap, frmt);

		TCHAR _output_method_args[_BUFFER_SIZE] = {0};
		_vsntprintf(_output_method_args, _BUFFER_SIZE, frmt, ap);

		va_end(ap);

		SEND_METHOD_MESSAGE(_output_method_args);
	}
#endif // defined(DEBUG)
}

inline void 
FunctionDebug::OUTPUT_METHOD_EVENT(LPCTSTR 
#ifdef DEBUG
								   frmt
#endif
								   , ...)
{
#if defined(DEBUG)
	va_list ap;
	va_start(ap, frmt);

	TCHAR _output_method_args[_BUFFER_SIZE] = {0};
	_vsntprintf(_output_method_args, _BUFFER_SIZE, frmt, ap);

	va_end(ap);

	TCHAR _output_method_s[_BUFFER_SIZE] = {0};
	FORMAT_METHOD_CALL_MSG(_output_method_s, _output_method_args);
	SEND_METHOD_MESSAGE(_output_method_s);
	SEND_METHOD_EVENT(_output_method_s);
#endif // defined(DEBUG)
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/************************************************************************
 THESE ARE ONLY FOR ERROR LOGGING - NOT FOR PROGRAM TRACING OR DEBUGGING.
************************************************************************/


//----------------------------------------------------------------------------


//class<inst>::method() returning 0x99999999   or
//class::method() returning 0x99999999   or
//function() returning 0x99999999   or
inline void 
FunctionDebug::OUTPUT_RETURN(HRESULT
#ifdef DEBUG
							 result
#endif
							 )
{
#if defined(DEBUG)
	if(m_fDebugThisMethod)
	{
		HRESULT _OUTPUT_RETURN_hr = result;
		TCHAR _OUTPUT_RETURN_s[_BUFFER_SIZE] = {0};
		FORMAT_RETURN(_OUTPUT_RETURN_s, _OUTPUT_RETURN_hr);
		SEND_METHOD_MESSAGE(_OUTPUT_RETURN_s);
	}
#endif // defined(DEBUG)
}

#endif // !__DONT_USE_FUNCTION_DEBUG

#endif // !defined(DEBUGON_H)
