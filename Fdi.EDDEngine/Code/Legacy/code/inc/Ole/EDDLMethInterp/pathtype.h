////////////////////////////////////////////////////////////////////////
//        Header file that describes the type and property constants used.
//
//   ALL CONSTANTS HAVING TO DO WITH PATHS OR PROPERTIES MUST BE IN THIS
//   INCLUDE FILE SO THAT THE APPLICATIONS CAN USE THEM!!!!!
//
//        @(#) $Id: methodtype.h, 11/09/11 feiliu $
////////////////////////////////////////////////////////////////////////

#pragma once


////////////////////////////////////////////////////////////////////////
////    HART Path object types.
////////////////////////////////////////////////////////////////////////

#define PT_PATH                 0 // Base class (always invalid)
#define PT_NEW                  1 // Newly created object
#define PT_BAD                  2 // Bad moniker object

#define PT_NODE                 5 // Top of Remote object tree
#define PT_REMOTE               6 // Remote object

#define PT_ROOT                 10 // Root object

#define PT_ID_COL               11 // Unique id collection

#define PT_NODE_COL             14 // Network node collection (debug)

#define PT_MODEM_COL            16 // Modem collection
#define PT_MODEM                17 // Individual modem

#define PT_RNI_COL              21 // RNI node collection
#define PT_RNI_BC               22 // RNI batch controller
#define PT_RNI_MC               23 // RNI multiloop controller

#define	PT_FF_DEVICE			80 // Fieldbus Device

#define PT_PROFIBUS_DP_DEVICE	90 // Profibus DP Device

#define PT_DEVICE               100 // HART Device

#define PT_BLOCK_COL            101 // Block collection
#define PT_BLOCK                102 // Block
#define PT_TAG_COL              103 // Collection of tags

#define PT_PARAM_COL            105 // Parameter collection

#define PT_VAR_COL              106 // Variable collection
#define PT_VAR                  107 // Variable

#define PT_REC_COL              108 // Record collection
#define PT_REC                  109 // Record
#define PT_REC_MEM              110 // Record Member collection

#define PT_ARRAY_COL            111 // Array collection
#define PT_ARRAY                112 // Array
#define PT_ARRAY_MEM            113 // Array Member collection

#define PT_ENUM_COL             114 // Enumeration Value collection
#define PT_ENUM                 115 // Enumeration Value

#define PT_RESP_COL             116 // Response Codes collection
#define PT_RESP                 117 // Response Codes

#define PT_PLIST_COL            118 // Parameter Lists collection
#define PT_PLIST                119 // Parameter Lists

#define PT_METHOD_COL           120 // Methods collection
#define PT_METHOD               121 // Methods

#define PT_MENU_COL             122 // Menu collection
#define PT_MENU                 123 // Menu
#define PT_MITEM_COL            124 // Menu Item collection
#define PT_MITEM                125 // Menu Item

#define PT_DISP_COL             126 // Display collection
#define PT_DISP                 127 // Display

#define PT_EDIT_COL             128 // Edit Item collection
#define PT_EDIT                 129 // Edit Item
#define PT_EDIT_MEM             130 // Edit Item Member collection

#define PT_COLLECT_COL          131 // Collection collection
#define PT_COLLECT              132 // Collection
#define PT_COLLECT_MEM          133 // Collection Member collection

#define PT_IARRAY_COL           134 // Item Array collection
#define PT_IARRAY               135 // Item Array
#define PT_IARRAY_MEM           136 // Item Array Member collection

#define PT_DDL_COL              137 // DDL Items collection
#define PT_DDL                  138 // DDL Item

#define PT_REFRESH_COL          139 // Refresh relation collection
#define PT_REFRESH              140 // Refresh relation

#define PT_UNITS_COL            141 // Unit Relation collection
#define PT_UNITS                142 // Unit Relation

#define PT_WAO_COL              143 // Write As One collection
#define PT_WAO                  144 // Write As One
#define PT_WAO_MEM              145 // Write As One

#define PT_EDIT_DISP_COL        146 // Edit Display collection
#define PT_EDIT_DISP            147 // Edit Display

#define PT_UNIT_MEM             148 // Unit members
#define PT_UNIT_UNITS           149 // Unit units

#define PT_LEFT_MEM             150 // Left side of refresh relation
#define PT_RIGHT_MEM            151 // Right side of refresh relation

#define PT_DBASE_COL            152 // Collection of Data Base Objects
#define PT_DBASE                153 // Data Base Object

#define PT_NETWORK_COL          154 // Network Collection
#define PT_NETWORK_NODE         155 // Network Node

#define PT_PORT_COL             156 // Port Collection of Port Types

#define PT_BLOCKTAG_COL         157 // Block Tag Collection
#define PT_DEVICETAG_COL        158 // Device Tag Collection
#define PT_PHYSICALTAG_COL      159 // Physical Tag Collection
#define PT_DEVICEID_COL         160 // Device Id Collection
#define PT_NAMEDCONFIG_COL      161 // Named Configuration Collection
#define PT_LOWER_TEST           162 // The lower hierarchy test Collection
#define PT_REMOTE_BLOCK_PARENT  163 // Parent of a block created by a request 
                                    // from another node
#define PT_REMOTE_DEVICE_PARENT 164 // Parent of a device created by a request 
                                    // from another node
#define PT_NAMEDCONFIG          165 // A named configuration
#define PT_DDITEM_COL			166 // DDItem Collection

#define PT_PASS_THROUGH_ROOT	169 // Pass-through root object.
#define PT_PASS_THROUGH_LEVEL   170 // Pass-through level object.  

#define PT_CONVENDEV			171	// Conventional Device
#define PT_BLOCK_INDEX_COL		172	// Block Index Collection
#define PT_DATABASE_ROOT		173	// Database Root Object
#define PT_DEVICEVIEW_COL		174	// DeviceView Collection
#define PT_TESTEQUIPVIEW_COL	175	// Test Equipment View
#define PT_CATEGORYVIEW_COL		176	// Category View
#define PT_MFRVIEW_COL			177	// Manufacturer View
#define PT_POLLVIEW_COL			178 // Poll View

// New device hierarchies
#define PT_MANUFACTURER			180
#define PT_DEVICE_PROTOCOL		181
#define PT_DEVICE_TYPE			182
#define PT_DEVICE_REVISION		183
#define PT_CATEGORY				184
#define PT_SUB_CATEGORY			185
#define PT_DEVICE_DISPOSITION	186

// Test Equipment Objects
#define PT_TEQ_MANUFACTURER		190
#define PT_TEQ_DEVICE_TYPE		191

// Poll View hierarchy
#define PT_PLANT_SERVER			200
#define PT_POLLED_DEVICE		201

// EDDL
#define PT_CHART_COL			210	//Chart Collection
#define PT_GRAPH_COL			211	//Graph Collection
#define PT_CHART				212 //Chart
#define PT_GRAPH				213	//Graph
#define PT_IMAGE				214	//Image
#define PT_CHART_MEM_COL		215	//Chart Member Collection
#define PT_GRAPH_MEM_COL		216	//Graph Member Collection
#define PT_CHART_MEM			217	//Chart Member
#define PT_GRAPH_MEM			218	//Graph Member
#define PT_SOURCE_MEM_COL		219	//Source Member Collection
#define PT_SOURCE_MEM			220	//Source Member
#define PT_SOURCE				221	//Source
#define PT_WAVE_FORM			222	//Wave Form
#define PT_AXIS					223	//Axis
#define PT_POINT_COL			224	//Point Collection
#define PT_POINT				225	//Point
#define PT_SEPARATOR			226	//Separator
#define PT_STRING_LITERAL		227	//String Literal
#define PT_IMAGE_COL			228	//Image Collection
#define PT_ROWBREAK				229	//Row Break
#define PT_COLUMNBREAK			230	//Column Break
#define PT_ENUM_BIT				231	//Enum Bit
#define PT_GRID					232	//Grid
#define PT_CONSTANT				233	//Constant
#define PT_VECTOR_COL			234	//Vector Collections
#define PT_VECTOR				235	//Vector
#define PT_VECTOR_ITEM_COL		236	//Vector Item Collections
#define PT_GRID_COL				237	//Grid Collections
#define PT_CRITICAL_PARAM_COL	238	// Critical Parameter collection
#define PT_FILE_COL				239
#define PT_FILE					240
#define PT_FILE_MEM_COL			241
#define PT_DDLIST				242
#define PT_DDLIST_ELEMENT_COL	243
#define PT_DDLIST_ELEMENT		244
#define PT_DDLIST_ELMNT_PRM_COL 245
#define PT_DDLIST_RECORD		246
#define PT_DDLIST_ARRAY			247
#define PT_DDLIST_VAR			248
#define PT_DDLIST_COL			249
#define PT_DDLIST_REC_MEM		250
#define PT_DDLIST_ARRY_ELMNT	251

#define PT_HART_LAST			252	// THIS MUST BE THE LAST IN THE LIST!
									// It is used by FF projects!

////////////////////////////////////////////////////////////////////////
////    FF Path object types.	(Extends HART list)
////////////////////////////////////////////////////////////////////////

#define PT_PHYSICAL_DEVICE_COL			(PT_HART_LAST+1) // Physical Device collection 
#define PT_LOGICAL_HSI_COL				(PT_HART_LAST+2) // Logical HSI Collection 
#define PT_LOGICAL_DEVICE_COL			(PT_HART_LAST+3) // Logical Device Collection 
#define PT_LOGICAL_DEVICE				(PT_HART_LAST+4) // Logical Device
#define PT_BLOCK_ID_COL					(PT_HART_LAST+5) // Block Id Collection
#define PT_DEVICE_PARAM_COL				(PT_HART_LAST+7) // Device Parameter collection
#define PT_LOCAL_PARAM_COL				(PT_HART_LAST+8) // Local Parameter collection
#define PT_CROSS_BLOCK_ITEM				(PT_HART_LAST+9) // Cross Block Item
#define PT_NAMED_MENU_COL				(PT_HART_LAST+10) // Named Menu collection
#define PT_NAMED_METHOD_COL				(PT_HART_LAST+11) // Named Method collection
#define PT_NAMED_CHART_COL				(PT_HART_LAST+12) // Named Chart collection
#define PT_NAMED_GRAPH_COL				(PT_HART_LAST+13) // Named Graph collection
#define PT_NAMED_GRID_COL				(PT_HART_LAST+14) // Named Grid collection

#define PT_FF_LAST						(PT_HART_LAST+15) // THIS MUST BE THE LAST IN THE LIST!
														  // It is used by PB projects!

////////////////////////////////////////////////////////////////////////
////    PB Path object types.	(Extends FF list)
////////////////////////////////////////////////////////////////////////

#define PT_ACTION_METHOD_COL		(PT_FF_LAST+1) // Methods collection
#define PT_EMBEDDED_METHOD			(PT_FF_LAST+2) // Physical Device collection
#define PT_COMMAND					(PT_FF_LAST+3) // Command


////////////////////////////////////////////////////////////////////////
////    HART Path Property List.
////////////////////////////////////////////////////////////////////////

#define PP_Address                       1 // Network Address (dot format)
#define PP_BlockTag                  3 // External Block Tag
#define PP_Class                   4 // Parameter Class(int)
#define PP_ClassAsString            5 // Paramater Class (string)
#define PP_Count                    6 // Enumeration Count
#define PP_Definition                7 // Device Definition
#define PP_Description                8 // Device Description
#define PP_DeviceID                    9 // Device Mfg/Model/Rev/Serial
#define PP_DeviceTag                    10 // External Device Tag
#define PP_DisplayFormat                11 // Display Format
#define PP_EditFormat                    12 // Edit Format
#define PP_EnumDescription                13 // Enumeration Description
#define PP_EnumHelp                        14 // Enumeration Help
#define PP_Flags                        15 // Parameter Flags (int)
#define PP_FlagsAsString                16 // Parameter Flags (string)
#define PP_Handling                        17 // Parameter Handling (int)
#define PP_HandlingAsString                18 // Parameter Handling (string)
#define PP_Help                            19 // Helpful info
#define PP_Index                    20 // Index in list
#define PP_ItemHelp                    21 // DDL Help
#define PP_ItemID                    22 // DDL Item ID
#define PP_ItemIndex                23 // DDL Index
#define PP_ItemLabel                24 // DDL Label
#define PP_ItemMemberId                25 // DDL Member ID
#define PP_ItemName                    26 // DDL Item Name
#define PP_Kind                        27 // Object Kind (integer)
#define PP_KindAsString                28 // Object Kind (string)
#define PP_Label                    29 // Label
#define PP_MemberID                    31 // Member ID
#define PP_MinMaxCount                32 // Minimum/Maximum count
#define PP_Moniker                    34 // Moniker
#define PP_Name                        35 // Name
#define PP_NetAddress                36 // Network Address (moniker)
#define PP_NumberOfElements            37 // Number of Elements
#define PP_ParamHelp                38 // Parameter Help
#define PP_ParamIndex                39 // Parameter Index
#define PP_ParamLabel                40 // Parameter Label
#define PP_PhysicalTag                42 // Internal Device Tag 
#define PP_ReadTimeout                43 // Read Timeout
#define PP_ScalingFactor            44 // ScalingFactor
#define PP_Size                        45 // Size
#define PP_StationAddress            46 // Device network station address
#define PP_StatusClasses            47 // Status Classes (int)
#define PP_StatusClassesAsString    48 // Status Classes (string)
#define PP_Tag                        49 // Internal Block Tag (Hart Tag)
#define PP_Type                        50 // Parameter type
#define PP_TypeAsString                51 // Parameter type as string
#define PP_Units                    52 // Units
#define PP_Validity                    53 // Parameter validity
#define PP_Value                    54 // Parameter Value (variant)
#define PP_ValueAsString            55 // Parameter Value (string)
#define PP_WriteTimeout                56 // Write Timeout
#define PP_MethodParams					57 //Arguments to a method
#define PP_NetworkHandling			58 // Network Handling
#define PP_NetworkHandlingAsString	59 // Network Handling As String
#define PP_Style					60
#define PP_StyleAsString			61

#define PP_AxisScaling				63
#define PP_Capacity					64
#define PP_XAxisItemID				65
#define PP_YAxisItemID				66
#define PP_Length					67

#define PP_MinValue1                71 // Minimum Values
#define PP_MinValue2                72
#define PP_MinValue3                73
#define PP_MinValue4                74
#define PP_MinValue5                75
#define PP_MinValue6                76
#define PP_MinValue7                77
#define PP_MinValue8                78
#define PP_MinValue9                79

#define PP_MaxValue1                81 // Maximum Values
#define PP_MaxValue2                82
#define PP_MaxValue3                83
#define PP_MaxValue4                84
#define PP_MaxValue5                85
#define PP_MaxValue6                86
#define PP_MaxValue7                87
#define PP_MaxValue8                88
#define PP_MaxValue9                89

#define PP_Full_Moniker             100 // Moniker

#define PP_LoggableParamsReadCount	101 // Count of the loggable params that have been read by DDBS
#define PP_TotalLoggableParamsCount	102 // Total Count of the loggable params that can be read by DDBS
#define PP_DeviceDbSyncState		103 // State of DeviceDbSync (see legal values in enumDeviceDbSyncState below)
#define PP_DeviceDbSyncStatus		104 // status of DeviceDbSync (return code)
#define PP_ENUMS_AS_XML				105
#define PP_HART_LAST                106 // THIS MUST BE THE LAST IN THE LIST!
                                        // It is used by FF projects!

////////////////////////////////////////////////////////////////////////
////    FF Path Property List.	(Extends HART list)
////////////////////////////////////////////////////////////////////////

#define PP_FullLabel				(PP_HART_LAST+2) // Fully qualified label
#define PP_IndexItemArray			(PP_HART_LAST+3) // Item Array that this is an index for
#define PP_IsModified				(PP_HART_LAST+4) // Whether or not this is a modified parameter
#define PP_ServerCount				(PP_HART_LAST+5) // Count of enums on the server
#define PP_CompLabel				(PP_HART_LAST+6) // Composite Label
#define PP_EnumValueList			(PP_HART_LAST+7) // Enumeration Value List (composite)
#define PP_ObjectIndex				(PP_HART_LAST+8) // Object Index for this block
#define PP_IndexedValueList			(PP_HART_LAST+9) //	Index value
#define PP_OutOfDateDDL				(PP_HART_LAST+10) // Are we using an out of date DLL for this block
#define PP_DDL						(PP_HART_LAST+11) // What is the Mfg, DevRev, DevType, DDRev that we are using
#define PP_ProtocolType				(PP_HART_LAST+12) // What protocol are we using?
#define PP_BlockType				(PP_HART_LAST+13) // What type of block is this?
#define PP_MaxCount					(PP_HART_LAST+16) // Max count
#define PP_MinCount					(PP_HART_LAST+17) // Min count
#define PP_BlockName				(PP_HART_LAST+18) // BlockName (ie. RESOURCE, TRANSDUCER300, PID1200, AI520...)
#define PP_SubIndex					(PP_HART_LAST+19) // SubIndex for a member of a record or array
#define PP_ParamOffset				(PP_HART_LAST+20) // Param Offset of this parameter in the block
#define PP_OperationalId			(PP_HART_LAST+21) // Operational Id for a parameter
#define PP_OperationalSubIndex		(PP_HART_LAST+22) // Operational Subindex for a parameter
#define PP_DefaultValue				(PP_HART_LAST+23) // Operational Subindex for a parameter

