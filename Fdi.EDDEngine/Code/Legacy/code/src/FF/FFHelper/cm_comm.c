#ifndef lint
static char SCCSID[] = "@(#)cm_comm.c	30.6  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_comm.c - Connection Manager Communication module
 */

#include <stdlib.h>

#include "cm_lib.h"
#include "panic.h"
#include "comm.h"
#include "DDI_LIB.H"
#include "int_diff.h"



#include "ConnectionMgr.h"
#pragma warning ( disable : 4702 )		// Turn of "unreachable code" warning


/*
 *	Type A Comm Stack Function Pointers
 */

static int	(*initialize_fn_ptr) P(()) = 0;
static int	(*init_network_fn_ptr) P((NETWORK_HANDLE)) = 0;
static int	(*find_block_fn_ptr)
	P((char *, NETWORK_HANDLE, FIND_CNF_A **, ENV_INFO *)) = 0;
static int	(*initiate_comm_fn_ptr)
	P((DEVICE_HANDLE, CREF *, CREF *, CREF *, ENV_INFO *)) = 0;
static int	(*terminate_comm_fn_ptr) P((DEVICE_HANDLE, ENV_INFO *)) = 0;
static int	(*comm_get_fn_ptr)
	P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT **, ENV_INFO *)) = 0;
static int	(*comm_put_fn_ptr)
	P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, OBJECT *, ENV_INFO *)) = 0;
static int	(*comm_read_fn_ptr)
	P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
		CM_VALUE_LIST *, ENV_INFO *)) = 0;
static int	(*comm_write_fn_ptr)
	P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, SUBINDEX,
		CM_VALUE_LIST *, ENV_INFO *)) = 0;
static int	(*comm_upload_fn_ptr)
	P((DEVICE_HANDLE, COMM_TYPE, OBJECT_INDEX, UINT8 *, ENV_INFO *)) = 0;
static int	(*param_read_fn_ptr) P((ENV_INFO *, int, SUBINDEX)) = 0;
static int	(*param_write_fn_ptr) P((ENV_INFO *, int, SUBINDEX)) = 0;


/***********************************************************************
 *
 *	Name:	init_network_table
 *
 *	ShortDesc:	Initialize the Network Table.
 *
 *	Description:
 *		The init_network_table function initializes the Network Table.
 *		If the Connection Manager mode is On-Line, it reads in the
 *		"network.dat" data file and uses this information to set up
 *		the Network Table.  If the Connection Manager mode is Off-Line,
 *		it sets up the Network Table with one generic entry.
 *
 *	Inputs:
 *		cm_mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_CM_MODE.
 *		CM_BAD_FILE_OPEN.
 *		CM_NO_MEMORY.
 *		CM_BAD_FILE_CLOSE.
 *		CM_FILE_ERROR.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

#undef USING_NETWORK_DAT	// We are not using the network.dat file for the AMSFhxGen project.
							// So we will kludge the numbers that we normally would get from
							// network.dat

int CConnectionMgr::init_network_table(int cm_mode)
{

#ifdef USING_NETWORK_DAT
	FILE				*data_file;
	char				 file_line[256];
	int					 r_code;
#endif
	NET_TYPE			 network_type;
	UINT32				 network_route_1;
	UINT32				 network_route_2;
	UINT32				 network_route_3;
	int					 device_family;
	int					 config_file_id;
	NET_ROUTE			 network_route;

	/*
	 *	Preliminarily Initialize the Network Table.
	 */

	network_tbl.count = 0;
	network_tbl.list = (NETWORK_TBL_ELEM *) malloc(1);
	if (!network_tbl.list) {
		return(CM_NO_MEMORY);
	}

	switch (cm_mode) {

		case CMM_ON_LINE:

#ifdef USING_NETWORK_DAT
			/*
			 *	Open the network data file.
			 */

			data_file = fopen("network.dat", "r");
			if (!data_file) {
				CRASH_DBG();
				return(CM_BAD_FILE_OPEN);
			}
#endif
			break;

		case CMM_OFF_LINE:

			/*
			 *	Set up for off-line mode (one Network Table entry).
			 */

			network_tbl.list =
					(NETWORK_TBL_ELEM *) realloc((char *)network_tbl.list,
					(unsigned int)(sizeof(NETWORK_TBL_ELEM)));
			if (!network_tbl.list) {
				return(CM_NO_MEMORY);
			}
			network_tbl.count = 1;
			(void)memset((char *)network_tbl.list, 0, (sizeof(NETWORK_TBL_ELEM)));
			set_nt_net_type(0, NT_OFF_LINE);
			set_nt_dev_family(0, DF_FF);
			return(CM_SUCCESS);

		default:
			return(CM_BAD_CM_MODE);
	}

#ifdef USING_NETWORK_DAT

	/*
	 *	Go through the file and read in each network entry.
	 */

	while (!feof(data_file)) {

		/*
		 *	Read in one line of the data file.
		 */

		(void) fgets(file_line, 256, data_file);

		/*
		 *	Check for comments or blank lines.
		 */

		if ((file_line[0] == '#') || (file_line[0] == '\n')) {

			/*
			 *	Go on to read the next line of the data file.
			 */

			continue;

		} else {
#endif
			/*
			 *	Scan in the following information for each
			 *	network entry:
			 *
			 *	1. Type of the network
			 *	2. Routing information of the network
			 *	3. Routing information of the network
			 *	4. Routing information of the network
			 *	5. Device Family used on the network
			 *	6. Configuration File ID for the network
			 */

#ifdef USING_NETWORK_DAT
			r_code = sscanf(file_line, "%d %d %d %d %d %d",
					&network_type, &network_route_1,
					&network_route_2, &network_route_3,
					&device_family, &config_file_id);

			if (r_code != 6) {
				return(CM_FILE_ERROR);
			}
#else
			network_type = NT_A_SIM;
			network_route_1 = 0;
			network_route_2 = 0;
			network_route_3 = 0;
			device_family = DF_FF;
			config_file_id = 1;
#endif

			/*
			 *	Increase the Network Table by one element.
			 */

			network_tbl.list =
				(NETWORK_TBL_ELEM *) realloc((char *)network_tbl.list,
				(unsigned int)(sizeof(NETWORK_TBL_ELEM) *(network_tbl.count + 1)));
			if (!network_tbl.list) {
				return(CM_NO_MEMORY);
			}
			network_tbl.count ++;

			/*
			 *	Check to make sure that the network type is valid.
			 */
			if (

				(network_type != NT_A) && (network_type != NT_A_SIM)
				&& (network_type != NT_ETHER) &&
				 (network_type != NT_CONTROL)) {
				return(CM_FILE_ERROR);
			}

			/*
			 *	Set up the Network Table entry with network type,
			 *	device family, and config file id information.
			 */
			set_nt_net_type(network_tbl.count - 1, network_type);
			set_nt_dev_family(network_tbl.count - 1, device_family);
			set_nt_cfg_file_id(network_tbl.count - 1, config_file_id);
			

			/*
			 *	Set up the Network Table entry with network route
			 *	information.  This is network type specific.
			 */

			(void)memset((char *)&network_route, 0, sizeof(NET_ROUTE));

			switch (network_type) {

				case NT_ETHER:
					network_route.ether_network.ip_address =
							(UINT32)network_route_1;
					network_route.ether_network.socket_address =
							(UINT16)network_route_2;
					network_route.ether_network.board =
							(UINT16)network_route_3;
					break;

				case NT_CONTROL:
					network_route.control_network.controller_address =
							(UINT16)network_route_1;
					network_route.control_network.controller_port =
							(UINT16)network_route_2;
					network_route.control_network.controller_device_address =
							(UINT16)network_route_3;
					break;

				default:
					break;
			}
			set_nt_net_route(network_tbl.count - 1, &network_route);

#ifdef USING_NETWORK_DAT
		}
	}

	/*
	 *	Close the network data file.
	 */

	r_code = fclose(data_file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}
#endif

	return(CM_SUCCESS);
}


void CConnectionMgr::cleanup_network_table()
{
	/*
	 *	Free the Network Table.
	 */

	network_tbl.count = 0;
	free (network_tbl.list);
	network_tbl.list = nullptr;
}

/***********************************************************************
 *
 *	Name:	cr_init
 *
 *	ShortDesc:	Initialize the communication support.
 *
 *	Description:
 *		The cr_init function does whatever is necessary to initialize
 *		all communication.  In the case of a PC with communication
 *		boards, this process would initialize all the boards.
 *
 *	Inputs:
 *		cm_mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *		CM_NO_COMM.
 *		Return values from init_network_table function.
 *		Return values from ...
 *		Return values from initialize function.
 *		Return values from init_network function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::cr_init(int cm_mode)
{

	NETWORK_HANDLE	network_handle;
	int				r_code;

	/*
	 *	Initialize the Network Table for whatever networks will be used. 
	 *	This information is obtained from a data file when using on-line mode.
	 */

	r_code = init_network_table(cm_mode);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Perform any general network initialization.
	 */


	if (initialize_fn_ptr != NULL) {
		r_code = (*initialize_fn_ptr)() ;
		if (r_code) {
			return(r_code) ;
		}
	}

	/*
	 *	Go through each network.
	 */

	for (network_handle = 0; network_handle < network_tbl.count; network_handle ++) {

		/*
		 *	Check the type of the network.
		 */
		NET_TYPE net_type;
		get_nt_net_type(network_handle, &net_type);
		switch (net_type) {

			case NT_OFF_LINE:
				break;

			case NT_ETHER:

				/*
				 *	Put code here for initializing an Ethernet Network. 
				 *	Use the specified network route if necessary.
				 */

				 return(CM_NO_COMM); /* For now return an error. */

			case NT_CONTROL:

				/*
				 *	Put code here for initializing a Control Network. 
				 *	Use the specified network route if necessary.
				 */

				 return(CM_NO_COMM); /* For now return an error. */


			case NT_A:
			case NT_A_SIM:

				/*
				 *	Initialize a type A Network.
				 */

				if (init_network_fn_ptr != NULL) {
					r_code = (*init_network_fn_ptr)(network_handle);
					if (r_code) {
						return(r_code);
					}
				}
				break;

			default:

				break;
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:	cr_cleanup
 *
 *	ShortDesc:	Cleanup the communication support.
 *
 *	Description:
 *		The cr_init function does whatever is necessary to initialize
 *		all communication.  In the case of a PC with communication
 *		boards, this process would initialize all the boards.
 *
 *	Inputs:
 *		cm_mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *		CM_NO_COMM.
 *		Return values from init_network_table function.
 *		Return values from ...
 *		Return values from initialize function.
 *		Return values from init_network function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::cr_cleanup()
{
	/*
	 *	Initialize the Network Table for whatever networks will be used. 
	 *	This information is obtained from a data file when using on-line mode.
	 */

	cleanup_network_table();
}


/***********************************************************************
 *
 *	Name:  cr_object_free
 *
 *	ShortDesc:  Free an object and all of its allocated contents
 *				from system memory.
 *
 *	Description:
 *		The cr_object_free function takes an object in system memory
 *		and frees it along with all of its allocated contents.
 *
 *	Inputs:
 *		object - the object to be freed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void
cr_object_free(OBJECT *object)

{

	if (object->name) {
		free((char *)object->name);
	}
	if (object->extension) {
		free((char *)object->extension);
	}
	if (object->value) {
		free((char *)object->value);
	}
	if (object->specific) {

		switch (object->code) {

			case OC_NULL:
				break;

			case OC_OD_DESCRIPTION:
				break;

			case OC_DOMAIN:
				break;

			case OC_PROGRAM_INVOCATION:
				if (object->specific->program_invocation.domain_list) {
					free((char *)object->specific->
							program_invocation.domain_list);
				}
				break;

			case OC_EVENT:
				break;

			case OC_DATA_TYPE:
				if (object->specific->data_type.symbol) {
					free((char *)object->specific->data_type.symbol);
				}
				break;

			case OC_DATA_TYPE_STRUCTURE:
				if (object->specific->data_type_structure.type_list) {
					free((char *)object->specific->
							data_type_structure.type_list);
				}
				if (object->specific->data_type_structure.size_list) {
					free((char *)object->specific->
							data_type_structure.size_list);
				}
				break;

			case OC_SIMPLE_VARIABLE:
				break;

			case OC_ARRAY:
				break;

			case OC_RECORD:
				break;

			case OC_VARIABLE_LIST:
				if (object->specific->variable_list.list) {
					free((char *)object->specific->variable_list.list);
				}
				break;

			default:
				break;
		}

		free((char *)object->specific);
	}

	free((char *)object);
}




