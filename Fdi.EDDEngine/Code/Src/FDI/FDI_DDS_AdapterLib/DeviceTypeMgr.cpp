#include "STDINC.H"
#include "DeviceTypeMgr.h"
CDeviceTypeMgr g_DeviceTypeMgr;

CLockDeviceTypeMgr::CLockDeviceTypeMgr()
{
	g_DeviceTypeMgr.m_cs.lock();
}

CLockDeviceTypeMgr::~CLockDeviceTypeMgr()
{
	g_DeviceTypeMgr.m_cs.unlock();
}

CDeviceTypeMgr::CDeviceTypeMgr()
{
	
	active_dev_type_tbl.count = 0;
	active_dev_type_tbl.limit = 0;
	active_dev_type_tbl.list = nullptr;

}

CDeviceTypeMgr::~CDeviceTypeMgr()
{

	for (int i = 0; i < active_dev_type_tbl.limit;i++)
	{
		delete active_dev_type_tbl.list[i];
		active_dev_type_tbl.list[i] = NULL;
	}

	free(active_dev_type_tbl.list);
	active_dev_type_tbl.list = NULL;
	active_dev_type_tbl.count = 0;
	active_dev_type_tbl.limit = 0;
}

/***********************************************************************
 *
 *	Name:  ct_remove_device_type
 *
 *	ShortDesc:  Remove a specific member of the Active Device Type
 *				Table.
 *
 *	Description:
 *		The ct_remove_device_type function removes a specific element
 *		of the Active Device Type Table.  Note that the element will
 *		be deallocated but the Active Device Type Table list will not
 *		be reduced.
 *
 *	Inputs:
 *		device_type_handle - handle (offset) of the specific Active
 *							 Device Type Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CDeviceTypeMgr::ct_remove_device_type(DEVICE_TYPE_HANDLE device_type_handle)
{
	/*
	 *	Free the Active Device Type Table element and null the
	 *	element pointer.
	 */

	delete active_dev_type_tbl.list[device_type_handle] ;
	active_dev_type_tbl.list[device_type_handle] = 0;
}


/***********************************************************************
 *
 *	Name:  ct_device_type_search
 *
 *	ShortDesc:  Search for a device type in the Active Device Type
 *				Table using a DD device ID.
 *
 *	Description:
 *		The ct_device_type_search function takes a DD device ID and
 *		searches through the valid (allocated) Active Device Type
 *		Table elements in the Active Device Type Table list, looking
 *		for the element that matches the DD device ID.  If found, the
 *		corresponding device type handle will be returned.  Otherwise
 *		an error will be returned.
 *
 *	Inputs:
 *		dd_device_id - unique identifier of a device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device type handle of the matching Active Device Type
 *		Table element if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
 
DEVICE_TYPE_HANDLE CDeviceTypeMgr::ct_device_type_search(DD_DEVICE_ID *dd_device_id)
{
	DEVICE_TYPE_HANDLE	device_type_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (device_type_handle = 0; device_type_handle <
			active_dev_type_tbl.count; device_type_handle ++)
	{

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_type_tbl.list[device_type_handle]) 
		{

		/*
			*	Check the DD device ID of the element.
			*/
		
			DD_DEVICE_ID * pDeviceID = ADTT_DD_DEVICE_ID(device_type_handle);

			if ((device_type_handle >=0) &&
				(pDeviceID->ddid_manufacturer == dd_device_id->ddid_manufacturer) &&
				(pDeviceID->ddid_device_type == dd_device_id->ddid_device_type) &&
				(pDeviceID->ddid_device_rev == dd_device_id->ddid_device_rev) &&
				(pDeviceID->ddid_dd_rev == dd_device_id->ddid_dd_rev) &&
				(wcscmp(pDeviceID->dd_file_path, dd_device_id->dd_file_path) ==0)
				)
				{
					return(device_type_handle);
				}
		}
	}

	return(-1);
}
