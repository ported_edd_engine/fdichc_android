﻿<Method>
{
 meth_interp_integer = method_collection_index.DEFAULT_VALUE;
 PUT_MESSAGE(literal_string(xxx));
}
</Method>
<Function>
IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 16445, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: INTEGER, Value: 1
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: method_expression_index_DEFAULT_VALUE = %{meth_interp_integer}
End of Function literal_string
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 16445, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: INTEGER, Value: 1
IBuiltinSupport InfoRequest() is being called with "method_expression_index_DEFAULT_VALUE = 1"
IBuiltinSupport InfoRequest with returned value 0 () is returned
Function Name: PUT_MESSAGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: method_expression_index_DEFAULT_VALUE = %{meth_interp_integer}
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function PUT_MESSAGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
