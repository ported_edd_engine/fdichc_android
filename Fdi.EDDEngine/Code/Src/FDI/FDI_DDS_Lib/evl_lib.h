/*
 * @(#) $Id: evl_lib.h,v 1.7 2012/09/12 22:40:58 rgretta Exp $
 */

#pragma once

#include <stddef.h>
#include <limits.h>

#ifndef RTN_CODE_H
#include "rtn_code.h"		/** All of these .h files are needed by the eval library **/
#endif

#ifndef DDL_TAGS_H
#include "tags_sa.h"		/** To make it easier for a user to use eval, the user **/
#endif

#include "ddldefs.h"		/** only needs to include evl_lib.h **/

#ifndef FLATS_H
#include "flats.h"
#endif

#ifndef TABLE_H
#include "table.h"
#endif

#ifndef DDS_UPCL_H
#include "dds_upcl.h"
#endif

#ifndef ENV_INFO_H
#include "env_info.h"
#endif

#ifndef EVL_RET_H
#include "evl_ret.h"
#endif


#include "nsEDDEngine/Table.h"
#include "nsEDDEngine/Flats.h"

#ifdef DEBUG
#include "dds_chk.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif


/**
 * Range Values
 **/






/*
 *	Structure used to pass info to the device specific string service
 */
typedef struct tag_DEV_STRING_INFO {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;


/**
 ** flat interface functions
 **/

extern int eval_item P((void *, nsEDDEngine::AttributeNameSet, ENV_INFO *, RETURN_LIST *, ITEM_TYPE));
extern int eval_dir_block_tables P(( nsEDDEngine::FLAT_BLOCK_DIR *, nsEDDEngine::BIN_BLOCK_DIR *, unsigned long, ROD_HANDLE,nsEDDEngine::FLAT_DEVICE_DIR *flat_device_di));
extern int eval_dir_device_tables P(( nsEDDEngine::FLAT_DEVICE_DIR *, nsEDDEngine::BIN_DEVICE_DIR *, unsigned long, ROD_HANDLE));



/**
 ** flat destructor functions
 **/

extern void eval_clean_item P((::nsEDDEngine::ItemBase *, ITEM_TYPE));

extern int item_id_to_name(BLOCK_HANDLE bh, ENV_INFO *env_info, ITEM_ID item_id, LPTSTR item_name, int outbuf_size);

/**
 ** directory (ie. table) destructor functions
 **/

extern void eval_clean_device_dir P((nsEDDEngine::FLAT_DEVICE_DIR *));
extern void eval_clean_block_dir P((nsEDDEngine::FLAT_BLOCK_DIR *));


/**
 ** application function prototypes
 **/

#define DICT_DFLT_SEC 0xFFFF	// Standard Dictionary Default Section

#include "nsConsumer/IParamCache.h"

extern int app_func_get_param_value (ENV_INFO *, ::nsEDDEngine::OP_REF *, nsConsumer::EVAL_VAR_VALUE *);
extern int app_func_get_dict_string (ENV_INFO *, DDL_UINT dict_string_index, STRING *);
extern int app_func_get_dict_string_x (ENV_INFO *, DDL_UINT dict_string_index, ::nsEDDEngine::STRING *);
extern int app_func_get_dev_spec_string (ENV_INFO *, DEV_STRING_INFO *, STRING *);
extern int app_func_get_dict_string_by_name(ENV_INFO *env_info, const wchar_t *str_name, STRING *str_value );

extern int app_func_get_block_handle(ENV_INFO *env_info, 
									ITEM_ID block_id,
                                    unsigned int instance_num, 
									BLOCK_HANDLE *out_bh);




#ifdef __cplusplus
    }
#endif


