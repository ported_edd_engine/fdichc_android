/**
 *	@(#) $Id: cm_rod.cpp,v 1.1 2012/07/10 20:59:28 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_rod.c - Connection Manager - ROD Manager Module
 *
 *	This file contains all functions pertaining to ROD's (Remote
 *	Object Dictionaries).
 *
 *	A Remote Object Dictionary (ROD) is a host's copy of an Object
 *	Dictionary (OD) that exists in an actual device.  The data
 *	structures used to describe a ROD are local to this system and
 *	are defined in "cm_rod.h".  These structures are not identical
 *	memory copies of what is in the device's OD, but contain the
 *	same information.
 *
 *	All data within a ROD must be allocated in the ROD heap.  The
 *	following functions are used to maintain the ROD heap:
 *
 *	rodmgr constructor			rod_malloc			rod_calloc
 *	rod_realloc			rod_free			rod_member
 *
 *	A ROD is initiated by using the rod_open function with an OD
 *	description object (OD Object #0) as its parameter.  The OD
 *	description object contains information that dictates the size
 *	of the four sections of an OD.
 *
 *	A ROD's information may be accessed using the following functions:
 *
 *	rod_get		rod_put		rod_read		rod_write
 *
 */

#include "STDINC.H"

#include "cm_loc.h"
#include "rmalloc.h"
#include <fch_lib.h>
#include "ConnectionMgr.h"

#include "RodMgr.h"
#include "DeviceTypeMgr.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/*
 *	The ROD Heap
 */

#define INITIAL_ROD_HEAP_SIZE      0x80000
#define SUBSEQUENT_ROD_HEAP_SIZE   0x40000






/***********************************************************************
 *
 *	Name:  CRodMgr::CRodMgr()
 *
 *	ShortDesc:  Initialize the ROD Manager.
 *
 *	Description:
 *		The rod manager constructor initializes the ROD heap, and
 *		initializes the ROD Table by setting the table count
 *		to zero and initializing the table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Ken Johnson- Changed rod_init to CRodMgr constructor which is used globally.
 *
 **********************************************************************/

CRodMgr::CRodMgr()
{
	/*
	 *	Initialize the ROD heap.
	 */

	rcreateheap(&rod_heap, INITIAL_ROD_HEAP_SIZE,
			SUBSEQUENT_ROD_HEAP_SIZE);

	/*
	 *	Initialize the ROD Table.
	 */

	rod_tbl.count = 0;
	rod_tbl.list = (OBJECT_DICTIONARY **)rod_malloc(1);
	
}


CRodMgr::~CRodMgr()
{
	/*
	 *	Free the ROD heap.
	 */
	rfreeheap(&rod_heap);

}


/***********************************************************************
 *
 *	Name:  rod_malloc
 *
 *	ShortDesc:  Allocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_malloc function takes a size and allocates a segment
 *		in the ROD heap.
 *
 *	Inputs:
 *		size - number of bytes to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_malloc(int size)
{
	/*
	 *	Allocate a segment in the ROD heap.
	 */

	return((char *)rmalloc(&rod_heap,size));
}


/***********************************************************************
 *
 *	Name:  rod_calloc
 *
 *	ShortDesc:  Allocate and clear a segment in the ROD heap.
 *
 *	Description:
 *		The rod_calloc function takes in a number and size and
 *		allocates and clears a segment in the ROD heap.
 *
 *	Inputs:
 *		number - the number of elements to allocate.
 *		size - the size of each element to allocate.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the allocated and cleared memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_calloc(int number, int size)
{
	/*
	 *	Allocate and clear a segment in the ROD heap.
	 */

	return((char *)rcalloc(&rod_heap,number,size));
}


/***********************************************************************
 *
 *	Name:  rod_realloc
 *
 *	ShortDesc:  Reallocate a segment in the ROD heap.
 *
 *	Description:
 *		The rod_realloc function takes a pointer and size and
 *		reallocates the corresponding segment in the ROD heap
 *		to the new size.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to reallocate.
 *		size - the new size.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		A pointer to the reallocated memory.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

char *CRodMgr::rod_realloc(char *pointer, int size)
{
	/*
	 *	Reallocate the segment in the ROD heap.
	 */

	return((char *)rrealloc(&rod_heap,pointer,size));
}


/***********************************************************************
 *
 *	Name:  rod_free
 *
 *	ShortDesc:	Free a segment in the ROD heap
 *
 *	Description:
 *		The rod_free function takes a pointer and frees the
 *		corresponding segment in the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to free.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CRodMgr::rod_free(char *pointer)
{
	/*
	 *	Free the segment in the ROD heap.
	 */

	rfree(&rod_heap,pointer);
}


/*********************************************************************
 *
 *	Name:  rod_member
 *
 *	ShortDesc:  Determine if a pointer is a member of the ROD heap.
 *
 *	Description:
 *		The rod_member function takes a pointer and determines if
 *		it is a memeber of the ROD heap.
 *
 *	Inputs:
 *		pointer - the pointer of the segment to be identified.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		1 if the pointer is a member of the ROD heap.
 *		0 if the pointer is not a member of the ROD heap.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_member(char *pointer)
{
	/*
	 *	Determine if the pointer is a member of the ROD heap.
	 */

	return(rmemberof(&rod_heap,pointer));
}


/***********************************************************************
 *
 *	Name:  rod_object_heap_check
 *
 *	ShortDesc:  Check if an object is in the ROD heap.
 *
 *	Description:
 *		The rod_object_heap_check function takes an object and checks
 *		if all of its components are located in the ROD heap.
 *
 *	Inputs:
 *		object - the object to be checked.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_OBJECT_NOT_IN_HEAP.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_object_heap_check(OBJECT *object)
{
	if (!object) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}

	if (!rod_member((char *)object)) {
		return(CM_OBJECT_NOT_IN_HEAP);
	}

	if (object->extension) {
		if (!rod_member((char *)object->extension)) {
			return(CM_OBJECT_NOT_IN_HEAP);
		}
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_new_rod
 *
 *	ShortDesc:  Create a new ROD in the ROD Table.
 *
 *	Description:
 *		The rod_new_rod function creates a new ROD either in the
 *		first unused (deallocated) ROD space or at the end of the
 *		existing ROD Table list.  The corresponding ROD handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the new ROD.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

ROD_HANDLE CRodMgr::rod_new_rod(wchar_t * filename)
{
	ROD_HANDLE	rod_handle = 0;

	/*
	 *	Go through each ROD in the ROD Table.
	 */

	for (rod_handle = 0; rod_handle < rod_tbl.count; rod_handle ++) 
	{

		/*
		 *	Check if the ROD has been deallocated.
		 */

		if (!rod_tbl.list[rod_handle]) 
		{
			break;
		} 
	}

	/*
	 *	Check if the entire ROD Table was gone through without
	 *	finding any deallocated ROD's.
	 */

	if (rod_handle >= rod_tbl.count) 
	{

		/*
		 *	Extend the ROD Table.
		 */

		rod_tbl.count ++;
		rod_tbl.list = (OBJECT_DICTIONARY **) rod_realloc(
				(char *)rod_tbl.list,
				sizeof(OBJECT_DICTIONARY *) * rod_tbl.count);
		if (!rod_tbl.list) 
		{
			return(CM_NO_ROD_MEMORY);
		}

		rod_handle = rod_tbl.count - 1;
	}

	/*
	 *	Allocate a new ROD where the ROD handle is.
	 */

	rod_tbl.list[rod_handle] =
			(OBJECT_DICTIONARY *) rod_calloc(1,
			sizeof(OBJECT_DICTIONARY));
	if (!rod_tbl.list[rod_handle]) 
	{
		return(CM_NO_ROD_MEMORY);
	}

	wcscpy(rod_tbl.list[rod_handle]->EDDBinaryFilename,filename);
	return(rod_handle);
}

// FD_FDI comment
//Not used
//ROD_HANDLE rod_find(DDOD_HEADER *header)
//{
//	ROD_HANDLE					 rod_handle;
//	for (rod_handle = 0; rod_handle < rod_tbl.count; rod_handle ++) {
//
//		/*
//		 *	Check if the ROD has been deallocated.
//		 */
//
//		if (!rod_tbl.list[rod_handle]) {
//			break;
//		}
//
//		if(::wcscmp(rod_tbl.list[rod_handle]->header.filepath,header->filepath)==0)
//			return rod_handle;
//	}
//	return -1;
//}

/***********************************************************************
 *
 *	Name:  rod_open
 *
 *	ShortDesc:  Open a ROD.
 *
 *	Description:
 *		The rod_open function takes an OD description object and 
 *		opens a new ROD.  The maximum number of objects will be
 *		set.  All objects will be empty.  The corresponding ROD
 *		handle will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		object_0 - the OD description object.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The ROD handle of the opened ROD.
 *		Return values from rod_new_rod function (CM_NO_ROD_MEMORY).
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

ROD_HANDLE CRodMgr::rod_open(DDEF_DESC *ddef_desc,ROD_HANDLE rod_handle) // fd_fdi
{

	OBJECT_DICTIONARY			*rod;
// FD_FDI Delete	OD_DESCRIPTION_SPECIFIC		*od_description;
//	int							 r_code;

	/*
	 *	Check to make sure that the OD description object is in the
	 *	ROD heap.
	 */
/* FD_FDI delete ????
	r_code = rod_object_heap_check(object_0);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
*/
	/*
	 *	Allocate a new ROD and initialize its description.
	 */
/*
	rod_handle=rod_find(header);
	// if the rod already exists return it
	if(rod_handle>=0)
		return (rod_handle);
*/
	
	rod = rod_tbl.list[rod_handle];
	rod->ddef_desc = *ddef_desc;
	
	/*
	 *	Allocate the four sections of the ROD.
	 *
	 *	(Note that this does not allocate any objects - just
	 *	object pointers.)
	 */

	if(ddef_desc->first_item_index < (UINT32)(ddef_desc->block_count+2))
		return CM_BAD_DDOD_FILE_FORMAT;

	rod->sod = (OBJECT **) rod_calloc((int)ddef_desc->first_item_index+ddef_desc->item_count, sizeof(OBJECT *));

	if (!rod->sod)
		return (CM_NO_ROD_MEMORY);

  //  rod->version = header->major_revision<<8 | header->minor_revision;  
	rod->version = 0;// set later.

	return(rod_handle);
}

int CRodMgr::rod_set_version(ROD_HANDLE rhandle, unsigned long version)
{
    OBJECT_DICTIONARY *rod;
	if (!VALID_ROD_HANDLE(rhandle)) {
		return CM_INVALID_ROD_HANDLE;
	}
	rod = rod_tbl.list[rhandle];
    rod->version = version;
    return CM_SUCCESS;
}

unsigned long CRodMgr::rod_get_version(ROD_HANDLE rhandle)
{
    OBJECT_DICTIONARY *rod;
	if (!VALID_ROD_HANDLE(rhandle)) {
		return 0;
	}
	rod = rod_tbl.list[rhandle];
	return rod->version;
}

unsigned long CDeviceTypeMgr::rod_get_version(ROD_HANDLE rhandle)
{
	return m_RodMgr.rod_get_version(rhandle);
}

/***********************************************************************
 *
 *	Name:  rod_object_free
 *
 *	ShortDesc:  Free an object and all of its allocated contents.
 *
 *	Description:
 *		The rod_object_free function takes an object and frees it
 *		from the ROD heap along with any of its allocated contents.
 *
 *	Inputs:
 *		object - the object to be freed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CRodMgr::rod_object_free(OBJECT *object)
{
	/*
	 *	Go through all possible allocated components of an object
	 *	and free them if necessary.
	 */

	if (object->extension) {
		rod_free((char *)object->extension);
	}

	rod_free((char *)object);
}


/***********************************************************************
 *
 *	Name:  rod_close
 *
 *	ShortDesc:  Close a ROD.
 *
 *	Description:
 *		The rod_close function takes a ROD handle and closes a
 *		specific ROD in the ROD Table.  Note that the ROD will be
 *		deallocated but the ROD Table list will not be reduced.
 *		This function will be successful unless an invalid ROD
 *		handle is passed in.
 *
 *	Inputs:
 *		rod_handle - the handle of the specific ROD to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_close(ROD_HANDLE rod_handle)
{

	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;

	OBJECT_DICTIONARY			*rod;
//	OD_DESCRIPTION_SPECIFIC		*od_description;
	int		object_num;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
// FD_FDI delete	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Go through each object in the Static Object Dictionary
	 *	(S-OD) and free the object if necessary.
	 *	Then free the S-OD.
	 */
	 
	for (object_num = 0; object_num < (int)(rod->ddef_desc.item_count); object_num ++) {
		if (rod->sod[object_num]) {
			rod_object_free(rod->sod[object_num]);
		}
	}

	rod_free((char *)rod->sod);

	/*
	 *	Free the ROD.
	 */


	rod_free((char *)rod);
	rod_tbl.list[rod_handle] = (OBJECT_DICTIONARY *)0;

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_close(ROD_HANDLE rod_handle)
{
	return m_RodMgr.rod_close(rod_handle);
}


/***********************************************************************
 *
 *	Name:  rod_object_lookup
 *
 *	ShortDesc:  Look up an object in a ROD.
 *
 *	Description:
 *		The rod_object_lookup function takes a ROD handle and object
 *		index and searches the four sections of the ROD for the
 *		corresponding object.  The (found) object output is a pointer
 *		to an object.  If this object pointer is null, it indicates
 *		that the object index is valid, but the object is not yet
 *		allocated.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the object that is being looked up.
 *
 *	Outputs:
 *		object - the found object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_INDEX.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	OBJECT_DICTIONARY			*rod;
//	OD_DESCRIPTION_SPECIFIC		*od_description;

	/*
	 *	Get the ROD and its description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];
//	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check which section of the ROD the object index resides.
	 */

	if (object_index == 0) {
		ASSERT(0); // FD_FDI
//		*object = rod->od_description;
	}

	else if (object_index < (rod->ddef_desc.first_item_index + rod->ddef_desc.item_count)) {
		*object = rod->sod[object_index];
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get
 *
 *	ShortDesc:  Get an object from a ROD.
 *
 *	Description:
 *		The rod_get function takes a ROD handle and object index and
 *		gets the corresponding object.  The (requested) object output
 *		is a pointer to an object.  This object and any of its
 *		allocated contents reside in the ROD.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the requested object.
 *
 *	Outputs:
 *		object - the requested object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_OBJECT.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	int		r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object.
	 */

	r_code = rod_object_lookup(rod_handle,object_index,object);
	if (r_code != CM_SUCCESS) {
		return (r_code);
	}

	/*
	 *	Check to make sure that the object exists.
	 */

	if (!*object) {
		return (CM_NO_OBJECT);
	}

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	return m_RodMgr.rod_get(rod_handle, object_index, object);
}



/***********************************************************************
 *
 *	Name:  rod_put
 *
 *	ShortDesc:  Put an object into a ROD.
 *
 *	Description:
 *		The rod_put function takes a ROD handle, object index, and
 *		object and puts the object into the corresponding ROD.  The
 *		object and any of its allocated contents must be allocated
 *		in the ROD heap, and checking is done to ensure this.  If an
 *		object exists where the new object is being put, the existing
 *		object will be freed.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD where the object will be put.
 *		object_index - the index of the object to be put.
 *		object - the object to be put.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_ILLEGAL_PUT.
 *		CM_BAD_INDEX.
 *		Return values from rod_object_heap_check function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
//int last_index = -1;

int CRodMgr::rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT *object) // fd_fdi
{
//	ASSERT(object_index!=1217);
	ASSERT(object);

	if(object==NULL)
	{
		PS_TRACE(L"rod_index = %d, size = <NULL>\n",object_index);
	}
	else
	{
		//PS_TRACE(L"rod_index = %d, code = %d, size = %d\n",object_index, object->object_code, object->ext_length);
	}

//	last_index=object_index;
	OBJECT_DICTIONARY			*rod;
//	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 r_code;

	/*
	 *	Check to make sure that the ROD handle and the object
	 *	are valid.
	 */

	if (!VALID_ROD_HANDLE(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	if (!object) {
		return(CM_NO_OBJECT);
	}

	/*
	 *	Get the ROD and description from the ROD handle.
	 */

	rod = rod_tbl.list[rod_handle];

//	od_description = &rod->od_description->specific->od_description;

	/*
	 *	Check to make sure that OD description object is not
	 *	being overwritten.
	 */

	if (object_index == 0) {
		return(CM_ILLEGAL_PUT);
	}

	/*
	 *	Check to make sure that the object to be put is in the
	 *	ROD heap.
	 */

	r_code = rod_object_heap_check(object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Check which section of the Object Dictionary the object
	 *	index resides.  Then free any existing object and put
	 *	in the new object.
	 */

	// FD_FDI delete ->sod_first_index
	if (object_index < rod->ddef_desc.first_item_index + rod->ddef_desc.item_count) 
	{
		if (rod->sod[object_index]) {
			ASSERT(0);
			return(CM_BAD_INDEX);
//			rod_object_free(rod->sod[object_index]);
		}
/*		ASSERT(rod->sod[object_index]==NULL);
		if(rod->sod[object_index]!=NULL)
			return CM_BAD_INDEX;
*/
		rod->sod[object_index] = object;
	}
	else {
		return(CM_BAD_INDEX);
	}

	return(CM_SUCCESS);
}


int CRodMgr::rod_set_header(ROD_HANDLE rhandle, DDOD_HEADER header)
{
    OBJECT_DICTIONARY *rod = NULL;
	if (!VALID_ROD_HANDLE(rhandle)) {
		return CM_INVALID_ROD_HANDLE;
	}
	rod = rod_tbl.list[rhandle];
    rod->header = header;
	
    return CM_SUCCESS;
}

