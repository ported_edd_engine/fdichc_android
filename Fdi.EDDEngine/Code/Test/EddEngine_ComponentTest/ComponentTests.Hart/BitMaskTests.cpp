#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <vcclr.h>
#include <string>
#include <nsEDDEngine/Flats.h>

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass]
        public ref class BitMaskTests
        {
        public: 
            [TestMethod, TestCategory("BitMaskTests")]
            void CheckSimpleBitmask()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase0005.fmA");
             
                ParamCacheSimulator *paramCache = new ParamCacheSimulator();
                engine->Initialize(paramCache);
                paramCache->SetEddEngine(engine->Instance);
                
                // item id of VARIABLE in COMMAND
                ITEM_ID id = 16386;

                nsEDDEngine::FDI_PARAM_SPECIFIER ps;
                engine->FillParamSpecifier("bitmask_test1", &ps);

                nsEDDEngine::FDI_COMMAND_LIST cmdList;
                cmdList.count = 0;
                cmdList.list = 0;

                int rs = engine->Instance->GetCommandList(0, &ps, nsEDDEngine::FDI_READ_COMMAND, &cmdList);

                ITEM_ID commandItemId;
                rs = engine->Instance->GetCmdIdFromNumber(0, cmdList.list->command_number, &commandItemId);


                nsEDDEngine::FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(commandItemId, ITYPE_COMMAND);
                FLAT_COMMAND* command = dynamic_cast<FLAT_COMMAND*>(genericItem->item);

                TRANSACTION* transaction(nullptr);
                for (int currentTransaction = 0; currentTransaction < command->trans.count; currentTransaction++)
                {
                    if (command->trans.list[currentTransaction].number == cmdList.list->transaction)
                    {
                        transaction = &command->trans.list[currentTransaction];
                    }
                }

                UINT16 expected = 2u;
                Assert::AreEqual(expected, transaction->reply.count);

                // Check bit mask for reply item 1
                Assert::AreEqual((unsigned long long)0xf0u, transaction->reply.list[0].data_item_mask);

                // Check bit mask for reply item 2
                Assert::AreEqual((unsigned long long)0x0fu, transaction->reply.list[1].data_item_mask);

                delete genericItem;
            }

        };

    } // namespace Hart
} // namespace ComponentTests

