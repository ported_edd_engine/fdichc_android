/**
 *	@(#) $Id: fch_rdir.c,v 1.1 1995/05/02 20:44:40 donmarq Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for the ROD Directory Fetch
 *
 */

/* #includes */
#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"

#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif

/* #defines */

#define	DEVICE_DIR_LEN_8 42

#define	DEVICE_DIR_LEN	((MAX_DEVICE_TBL_ID * TABLE_REF_LEN) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE)

#define	DEVICE_DIR_LEN_5 ((MAX_DEVICE_TBL_ID_5 * TABLE_REF_LEN_5) +	\
						 DIRECTORY_DATA_OFFSET_5 - EXTEN_LENGTH_SIZE_5)


// For _8, this does not include the optional ResolvedRef table...
#define	BLOCK_DIR_LEN_8	((NUM_REQ_BLOCK_TBLS_8 * TABLE_REF_LEN) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE_8)

#define	BLOCK_DIR_LEN	((NUM_REQ_BLOCK_TBLS * TABLE_REF_LEN) +	\
						 DIRECTORY_DATA_OFFSET - EXTEN_LENGTH_SIZE)

#define	BLOCK_DIR_LEN_5	((NUM_REQ_BLOCK_TBLS_5 * TABLE_REF_LEN_5) +	\
						 DIRECTORY_DATA_OFFSET_5 - EXTEN_LENGTH_SIZE_5)

/* typedefs */

/* structure definitions */

/* global variables */

/* local procedure declarations */


/*********************************************************************
 *
 *	Name:	set_dir_bin_exists
 *
 *	ShortDesc: Sets the bin_exists field in the BININFO structure
 *
 *	Description:
 *		A bit is set in the bin_exists field in the BININFO structure
 *		corresponding to each non-zero length table reference in the
 *		directory object extension.
 *
 *	Inputs:
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
set_dir_bin_exists(
ENV_INFO	   * /*env_info */,
ROD_HANDLE      rhandle,
unsigned char  *obj_extn_ptr,
unsigned long  *dir_bin_exists,
unsigned short  dir_type)
{

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(dir_bin_exists, FETCH_INVALID_PARAM);

	*dir_bin_exists = 0L;
	/*
	 * Set a bit in the bin_exists field for each non-zero length field in
	 * the table references in the object extension
	 */

	unsigned char  *local_data_ptr = nullptr;

	if (tok_major_rev == 6 || tok_major_rev == 8)
	{
		local_data_ptr = obj_extn_ptr + DIRECTORY_DATA_OFFSET;
	}
	else
	{
		local_data_ptr = obj_extn_ptr + DIRECTORY_DATA_OFFSET_5;
	}

	unsigned char dir_extn_len = *obj_extn_ptr;						// Get the directory extension length
	unsigned char* end_of_dir_ptr = obj_extn_ptr + dir_extn_len;	// Point at the end of the directory

	for (int i = 0; local_data_ptr < end_of_dir_ptr; i++)			// Loop through the dir entries until at the end.
	{
		if (tok_major_rev == 8 && dir_type == DEVICE_DIR_TYPE)		// Rev 8 Tokenizer doesn't have these device tables
		{
			switch (i)
			{
				case BLK_TBL_ID:
				case PROG_TBL_ID:
				case DOMAIN_TBL_ID:
				case LOCAL_VAR_TBL_ID:
					continue;
				default:
					;
			}
		}

		if (tok_major_rev == 8 && dir_type == BLOCK_DIR_TYPE)		// Rev 8 Tokenizer doesn't have these block tables
		{
			switch (i)
			{
				case PARAM_MEM_TBL_ID:
				case PARAM_MEM_NAME_TBL_ID:
				case PARAM_ELEM_TBL_ID:
				case PARAM_LIST_TBL_ID:
				case PARAM_LIST_MEM_TBL_ID:
				case PARAM_LIST_MEM_NAME_TBL_ID:
				case CHAR_MEM_TBL_ID:
				case CHAR_MEM_NAME_TBL_ID:
					continue;
				default:
					;
			}
		}

		bool dir_bin_is_existing = false;				// Initialize check

		if (tok_major_rev == 6 || tok_major_rev == 8)	// Look at the "length" part of the dir tables
		{
			dir_bin_is_existing = local_data_ptr[4] || local_data_ptr[5] ||
			local_data_ptr[6] || local_data_ptr[7];
		}
		else
		{
			dir_bin_is_existing = local_data_ptr[2] || local_data_ptr[3];
		}

		if (dir_bin_is_existing)				// If a length is found, set the appropriate bit
		{
			*dir_bin_exists |= (1L << i);
		}

		if (tok_major_rev == 6 || tok_major_rev == 8)	// Move the "local_data_ptr" to the next table
		{
			local_data_ptr += TABLE_REF_LEN;
		}
		else
		{
			local_data_ptr += TABLE_REF_LEN_5;
		}
	}

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: get_ref_data
 *
 *	ShortDesc: Get directory reference (local) data
 *
 *	Description:
 *		Retrieves the table from the local data area.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		obj_ref_ptr -	pointer to the directory object extension
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		table_size -	size of the requested table
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_ref_data(
ENV_INFO		* /* env_info */,
ROD_HANDLE      rhandle,
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned char  *obj_ref_ptr,
unsigned long  *table_size)
{
	unsigned long  dir_data_offset;	/* unpacked table offset */
	unsigned long  dir_data_size;	/* unpacked table size */
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(obj && scrpad && obj_ref_ptr,
		FETCH_INVALID_PARAM);

	/*
	 * Get the local data area offset and data size from the directory
	 * reference
	 */
	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

#ifndef SUN
#pragma warning (disable : 4244)
#endif
	if (tok6or8)
	{
		dir_data_offset = (((unsigned short) obj_ref_ptr[0]) << 24) |
			obj_ref_ptr[1] << 16 | obj_ref_ptr[2] << 8 | obj_ref_ptr[3];
		dir_data_size = (((unsigned short) obj_ref_ptr[4]) << 24) |
			obj_ref_ptr[5] << 16 | obj_ref_ptr[6] << 8 | obj_ref_ptr[7];
	}
	else
	{
		dir_data_offset = (((unsigned short) obj_ref_ptr[0]) << 8) |
			obj_ref_ptr[1];
		dir_data_size = (((unsigned short) obj_ref_ptr[2]) << 8) |
			obj_ref_ptr[3];
	}
#ifndef SUN
#pragma warning (default : 4244)
#endif

	/*
	 * If the scratchpad size is too small to load the data, return with an
	 * insufficient scratchpad error
	 */

	*table_size = (unsigned long) dir_data_size;
	if (dir_data_size > (scrpad->size - scrpad->used)) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	/*
	 * Retrieve the data from the ROD if there is any (non-zero length).
	 * Otherwise just return.
	 */

	if (dir_data_size) {
		rcode = get_local_data(obj, dir_data_offset,
			dir_data_size, scrpad);

		return (rcode);
	}

	return (SUCCESS);
}

/***********************************************************************
 *
 *	Name: get_major_rev_number
 *
 *	ShortDesc: Get the major rev number of the DDROD.
 *
 *	Description:
 *		The get_major_rev_number function takes a handle to a DDROD,
 *		and gets its DDOD Major Revision Number.
 *
 *	Inputs:
 *		rod_handle - handle of the DDROD.
 *
 *	Outputs:
 *		major_rev_number - DDOD Major Revision Number of the DDROD.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_major_rev_number(
ENV_INFO		* /*env_info */,
ROD_HANDLE		 rod_handle,
UINT8			*major_rev_number)
{
	OBJECT			*format_object;
	int				 r_code;

	/*
	 *	Point to the Format Object in the DDROD.
	 */

	r_code = g_DeviceTypeMgr.rod_get(rod_handle,(OBJECT_INDEX)FORMAT_OBJECT_INDEX,
			&format_object);

	/*
	 *	Check to make sure that there were no errors in pointing
	 *	to the Format Object, and that the Format Object's extension
	 *	exists.
	 */

	if (r_code || !format_object->extension) {
		return(FETCH_OBJECT_NOT_FOUND);
	}

	/*
	 *	Set the major rev number.
	 */

	*major_rev_number =
			format_object->extension[CODING_FMT_MAJOR_OFFSET];

	return(SUCCESS);
}

/*********************************************************************
 *
 *	Name: fetch_rod_device_dir
 *
 *	ShortDesc: Perform ROD fetch for device directories
 *
 *	Description:
 *		Retrieves the ROD object for the device directories and
 *		the associated tables in the local data area.  The table
 *		binaries are then attached to the appropriate BININFO
 *		structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_device_dir(
ENV_INFO	   *env_info,
ROD_HANDLE      rhandle,
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned char  *obj_extn_ptr,
BIN_DEVICE_DIR *bin_dev_dir,
unsigned long  *req_mask)
{
	unsigned char  *local_data_ptr;	/* points to table data in scratchpad */
	unsigned short  tag;	/* indicator for table type */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	unsigned long  dir_extn_offset;	/* offset in object extension
						 * for table reference field */
	BININFO        *bin_table_ptr;	/* points to a specific table BININFO
					 * structure */
	unsigned long   tbl_length;	/* size of table data */
	int             rcode;
	unsigned short max_dev_tbl_id = 0;

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * Set the bin_exists field in the BININFO structure if not already set
	 */

	if (!(bin_dev_dir->bin_exists)) {

		if ( (tok_major_rev == 8) || (tok_major_rev == 6) || (tok_major_rev == 5) )
		{
			rcode = set_dir_bin_exists(env_info, rhandle, obj_extn_ptr,
				&bin_dev_dir->bin_exists, DEVICE_DIR_TYPE);
		}
		else
		{
			rcode = DDS_WRONG_DDOD_REVISION;
			ASSERT_DBG(false);
		}

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

	tag = 0;
	dir_extn_offset = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (BININFO *) 0L;

	if (tok_major_rev == 8 || tok_major_rev == 6)
	{
		max_dev_tbl_id = MAX_DEVICE_TBL_ID;
	}
	else if (tok_major_rev == 5)
	{
		max_dev_tbl_id = MAX_DEVICE_TBL_ID_5;
	}
	else
	{
		ASSERT_DBG(false);
		return DDS_WRONG_DDOD_REVISION;
	}

	while ((*req_mask) && (tag < max_dev_tbl_id)) {

		/*
		 * Check for request mask bit corresponding to the tag value
		 * Skip to next tag value if not requested
		 */

		if (!((*req_mask) & (1L << tag))) {
			tag++;
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		switch (tag++) {

		case BLK_TBL_ID:	/* Block Table */
			tbl_mask_bit = BLK_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->blk_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = BLK_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = BLK_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case ITEM_TBL_ID:	/* Item Table */
			tbl_mask_bit = ITEM_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->item_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = ITEM_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = ITEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = ITEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case PROG_TBL_ID:	/* Program Table */
			tbl_mask_bit = PROG_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->prog_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PROG_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PROG_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case DOMAIN_TBL_ID:	/* Domain Table */
			tbl_mask_bit = DOMAIN_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->domain_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = DOMAIN_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = DOMAIN_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case STRING_TBL_ID:	/* String Table */
			tbl_mask_bit = STRING_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->string_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = STRING_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = STRING_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = STRING_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case DICT_REF_TBL_ID:	/* Dictionary Reference Table */
			tbl_mask_bit = DICT_REF_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->dict_ref_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = DICT_REF_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = DICT_REF_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = DICT_REF_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case LOCAL_VAR_TBL_ID:	/* Dictionary Reference Table */
			tbl_mask_bit = LOCAL_VAR_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->local_var_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = LOCAL_VAR_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = LOCAL_VAR_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case CMD_NUM_ID_TBL_ID:	/* Command Number to Item ID Table */
			tbl_mask_bit = CMD_NUM_ID_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->cmd_num_id_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = CMD_NUM_ID_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = CMD_NUM_ID_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = CMD_NUM_ID_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case IMAGE_TBL_ID:	/* Image Table */
			tbl_mask_bit = IMAGE_TBL_MASK;
			bin_table_ptr = &bin_dev_dir->image_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = IMAGE_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = IMAGE_REF_OFFSET;
					break;
				case 5:
					continue;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;
			
		default:	/* goes here for reserved or undefined table
				 * IDs */
			break;

		}

		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and if it not zero length
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {

			local_data_ptr = scrpad->pad + scrpad->used;

			rcode = get_ref_data(env_info, rhandle, obj, scrpad,
				obj_extn_ptr + dir_extn_offset, &tbl_length);

			if (rcode == SUCCESS) {

				/*
				 * Attach the table if non-zero length, else go
				 * to the next table
				 */

				if (tbl_length) {
					bin_table_ptr->chunk = local_data_ptr;
					bin_table_ptr->size = tbl_length;
					bin_dev_dir->bin_hooked |= tbl_mask_bit;
				}
			}
			else {
				return (rcode);
			}
		}

	}			/* end while */

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: fetch_rod_block_dir
 *
 *	ShortDesc: Perform ROD fetch for block directories
 *
 *	Description:
 *		Retrieves the ROD object for the block directories and
 *		the associated tables in the local data area.  The table
 *		binaries are then attached to the appropriate BININFO
 *		structure.
 *
 *	Inputs:
 *		obj -    		pointer to the ROD object structure
 *		req_mask -		tables requested for the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested block tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
fetch_rod_block_dir(
ENV_INFO	   *env_info,
ROD_HANDLE      rhandle,
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned char  *obj_extn_ptr,
BIN_BLOCK_DIR  *bin_blk_dir,
unsigned long  *req_mask)
{
	unsigned char  *local_data_ptr;	/* points to table data in scratchpad */
	unsigned long   tbl_mask_bit;	/* bit in req_mask for a specific table */
	unsigned short  dir_extn_offset;	/* offset in object extension
						 * for table reference field */
	BININFO        *bin_table_ptr;	/* points to a specific table BININFO
					 * structure */
	unsigned long   tbl_length;	/* size of table data */
	int             rcode;
	
	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * Set the bin_exists field in the BININFO structure if not already set
	 */

	if (!(bin_blk_dir->bin_exists)) {

		if ( (tok_major_rev == 8) || (tok_major_rev == 6) || (tok_major_rev == 5) )
		{
			rcode = set_dir_bin_exists(env_info, rhandle, obj_extn_ptr,
				&bin_blk_dir->bin_exists, BLOCK_DIR_TYPE);
		}
		else
		{
			rcode = DDS_WRONG_DDOD_REVISION;
			ASSERT_DBG(false);
		}

		if (rcode != SUCCESS) {
			return (rcode);
		}
	}

	/*
	 * Get the tables corresponding to the bits in the request mask
	 */

	dir_extn_offset = 0;
	tbl_mask_bit = 0L;
	bin_table_ptr = (BININFO *) 0L;

	*req_mask &= bin_blk_dir->bin_exists; // Only fetch tables that exist

	for ( unsigned tag = 0; *req_mask; tag++) {

		/*
		 * Check for request mask bit corresponding to the tag value
		 * Skip to next tag value if not requested
		 */

		if (!((*req_mask) & (1L << tag))) {
			continue;
		}

		/*
		 * Point to appropriate values for the table type
		 */

		switch ( tag ) {

		case BLK_ITEM_TBL_ID:	/* Block Item Table */
			tbl_mask_bit = BLK_ITEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->blk_item_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = BLK_ITEM_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = BLK_ITEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = BLK_ITEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case BLK_ITEM_NAME_TBL_ID:	/* Block Item Name Table */
			tbl_mask_bit = BLK_ITEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->blk_item_name_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = BLK_ITEM_NAME_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = BLK_ITEM_NAME_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = BLK_ITEM_NAME_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case PARAM_TBL_ID:	/* Parameter Table */
			tbl_mask_bit = PARAM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = PARAM_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = PARAM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case PARAM_MEM_TBL_ID:	/* Parameter Member Table */
			tbl_mask_bit = PARAM_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_mem_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_MEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_MEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case PARAM_MEM_NAME_TBL_ID:	/* Parameter Member Table */
			tbl_mask_bit = PARAM_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_mem_name_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_MEM_NAME_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_MEM_NAME_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case PARAM_ELEM_TBL_ID:	/* Parameter Element Table */
			tbl_mask_bit = PARAM_ELEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_elem_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_ELEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_ELEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case PARAM_LIST_TBL_ID:	/* Parameter List Table */
			tbl_mask_bit = PARAM_LIST_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_LIST_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_LIST_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case PARAM_LIST_MEM_TBL_ID:	/* Parameter List Member Table */
			tbl_mask_bit = PARAM_LIST_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_mem_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_LIST_MEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_LIST_MEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case PARAM_LIST_MEM_NAME_TBL_ID:	/* Parameter List Member Table */
			tbl_mask_bit = PARAM_LIST_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->param_list_mem_name_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = PARAM_LIST_MEM_NAME_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = PARAM_LIST_MEM_NAME_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}	
			break;

		case CHAR_MEM_TBL_ID:	/* Characteristic Member Table */
			tbl_mask_bit = CHAR_MEM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->char_mem_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = CHAR_MEM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = CHAR_MEM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}
			break;

		case CHAR_MEM_NAME_TBL_ID:	/* Characteristic Member Name Table */
			tbl_mask_bit = CHAR_MEM_NAME_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->char_mem_name_tbl;
			switch (tok_major_rev)
			{
				case 8:
					continue;
					break;
				case 6:
					dir_extn_offset = CHAR_MEM_NAME_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = CHAR_MEM_NAME_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case REL_TBL_ID:	/* Relation Table */
			tbl_mask_bit = REL_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->rel_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = REL_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = REL_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = REL_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case UPDATE_TBL_ID:	/* Update Table */
			tbl_mask_bit = UPDATE_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->update_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = UPDATE_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = UPDATE_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = UPDATE_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case COMMAND_TBL_ID:	/* Command Table */
			tbl_mask_bit = COMMAND_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->command_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = COMMAND_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = COMMAND_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = COMMAND_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}				
			break;

		case CRIT_PARAM_TBL_ID:	/* Critical Parameters Table */
			tbl_mask_bit = CRIT_PARAM_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->crit_param_tbl;
			switch (tok_major_rev)
			{
				case 8:
					dir_extn_offset = CRIT_PARAM_REF_OFFSET_8;
					break;
				case 6:
					dir_extn_offset = CRIT_PARAM_REF_OFFSET;
					break;
				case 5:
					dir_extn_offset = CRIT_PARAM_REF_OFFSET_5;
					break;
				default:
					ASSERT_DBG(false);
					break;
			}			
			break;

		case RESLV_REF_TBL_ID:	/* Resolve Reference Table */
			tbl_mask_bit = RESLV_REF_TBL_MASK;
			bin_table_ptr = &bin_blk_dir->reslv_ref_tbl;
			dir_extn_offset = RESLV_REF_OFFSET_8;		
			break;

		default:	/* goes here for reserved or undefined table
				 * IDs */
			break;

		}

		/*
		 * Attach the binary for the table if it was requested and if
		 * it has not already been attached and it is not zero length
		 */

		*req_mask &= ~tbl_mask_bit;	/* clear request mask bit */
		if (!(bin_table_ptr->chunk)) {
			local_data_ptr = scrpad->pad + scrpad->used;

			rcode = get_ref_data(env_info, rhandle, obj, scrpad,
				obj_extn_ptr + dir_extn_offset, &tbl_length);

			if (rcode == SUCCESS) {

				/*
				 * Attach the table if non-zero length, else go
				 * to the next table
				 */

				if (tbl_length) {
					bin_table_ptr->chunk = local_data_ptr;
					bin_table_ptr->size = tbl_length;
					bin_blk_dir->bin_hooked |= tbl_mask_bit;
				}
			}
			else {
				return (rcode);
			}
		}

	}			/* end while */

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name:	fch_rod_dir_spad_size
 *
 *	ShortDesc: Compute needed scratchpad memory size for directory fetch
 *
 *	Description:
 *		This function returns the amount of scratchpad memory required
 *		for a ROD Directory Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		tbl_req_mask -	tables requested for the directory, indicated
 *						by individual bits set for each table
 *						(tables depend on the directory type)
 *		dir_type -		identifies the type of directory requested
 *
 *	Outputs:
 *		sp_addlsize -	pointer to the value which is the minimum size
 *						of scratchpad memory needed to complete
 *						the fetch request.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INVALID_EXTN_LEN
 *		FETCH_INVALID_DIR_TYPE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_dir_spad_size(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
unsigned short  dir_type)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr=NULL;	/* pointer to ROD object extension in
					 * scratchpad */
	unsigned char  *local_data_ptr=NULL;	/* pointer to current table reference */
	unsigned short  dir_extn_len=0;	/* length of directory object extension */
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
					 * object extension */
	unsigned char   pad_buf[MAX_OBJ_EXTN_LEN]={0};	/* holds object
							 * extension to allow
							 * calculation of
							 * scratchpad size */
	unsigned char	major_rev_number=0;
	int             rcode=0;
	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * This function or fch_rod_device_dir() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */

	if (dir_type == DEVICE_DIR_TYPE) // 129 CAY
	{
		rcode = get_major_rev_number(env_info, rhandle,&major_rev_number);
		if (rcode != SUCCESS) 
		{
			return(rcode);
		}
	
		if (major_rev_number != tok_major_rev) 
		{
			// we set the "tok_major_rev" based on the file extension. e.g. 5 for ".fms" , 6 for ".fm6" and 8 for ".fm8".
			// in some cases ".fms" files can give "major_rev_number" 4 whereas 5 is expected.
			// We need to allow ".fms" files with major_rev_number 4 if tok_major_rev is 5.
			if(!((major_rev_number == 4) && (tok_major_rev == 5)))
			{
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"This DDL was compiled with an incompatible tokenizer version %d", major_rev_number);
				return(DDS_WRONG_DDOD_REVISION);
			}
		}
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(sp_addlsize, FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(ROD_FCH_DIR_SCRATCHPAD_SIZE);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Use a temporary buffer for the directory object extension
	 */

	local_scrpad.pad = pad_buf;
	local_scrpad.size = sizeof(pad_buf);
	local_scrpad.used = 0L;

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = local_scrpad.pad + local_scrpad.used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory
	 * table reference.  Return if the extension length is not large
	 * enough for the particular type of directory.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	switch (dir_type) {

		case DEVICE_DIR_TYPE:
			if (tok_major_rev == 8)
			{
				if (dir_extn_len < DEVICE_DIR_LEN_8) {
					return(FETCH_INVALID_EXTN_LEN);
				}
			}
			else if (tok_major_rev == 6)
			{
				if (dir_extn_len < DEVICE_DIR_LEN) {
					return(FETCH_INVALID_EXTN_LEN);
				}
			}
			else if (tok_major_rev == 5)
			{
				if (dir_extn_len < DEVICE_DIR_LEN_5) {
					return(FETCH_INVALID_EXTN_LEN);
				}
			}
			else
			{
				ASSERT_DBG(false);
				return DDS_WRONG_DDOD_REVISION;
			}
			break;

		case BLOCK_DIR_TYPE:
			if (tok_major_rev == 8)
			{
				if (dir_extn_len < BLOCK_DIR_LEN_8) {
					return(FETCH_INVALID_EXTN_LEN);
				}			
			}
			else if (tok_major_rev == 6)
			{
				if (dir_extn_len < BLOCK_DIR_LEN) {
					return(FETCH_INVALID_EXTN_LEN);
				}
			}
			else if (tok_major_rev == 5)
			{
				if (dir_extn_len < BLOCK_DIR_LEN_5) {
					return(FETCH_INVALID_EXTN_LEN);
				}
			}
			else
			{
				ASSERT_DBG(false);
				return DDS_WRONG_DDOD_REVISION;
			}
			break;

		default:
			return(FETCH_INVALID_DIR_TYPE);
	}

	/*
	 * Validate that the object is the correct type of directory.
	 */

	if (*(obj_extn_ptr + 1) != dir_type) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Minimum required scratchpad size is the object length
	 */

	*sp_addlsize += (dir_extn_len + EXTEN_LENGTH_SIZE);

	/*
	 * Calculate the required size for each requested table data block
	 */

	if (dir_type == DEVICE_DIR_TYPE) {

		if (tbl_req_mask & BLK_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + BLK_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + BLK_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & ITEM_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + ITEM_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + ITEM_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + ITEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PROG_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PROG_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5) 
			{
				local_data_ptr = obj_extn_ptr + PROG_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & DOMAIN_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + DOMAIN_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + DOMAIN_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & STRING_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + STRING_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + STRING_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + STRING_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & DICT_REF_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + DICT_REF_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + DICT_REF_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + DICT_REF_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & LOCAL_VAR_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + LOCAL_VAR_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + LOCAL_VAR_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & CMD_NUM_ID_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + CMD_NUM_ID_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + CMD_NUM_ID_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + CMD_NUM_ID_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & IMAGE_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + IMAGE_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + IMAGE_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
		}
	}
	else if (dir_type == BLOCK_DIR_TYPE) {

		if (tbl_req_mask & BLK_ITEM_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + BLK_ITEM_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + BLK_ITEM_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + BLK_ITEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & BLK_ITEM_NAME_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + BLK_ITEM_NAME_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + BLK_ITEM_NAME_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + BLK_ITEM_NAME_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + PARAM_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + PARAM_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_MEM_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_MEM_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_MEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_MEM_NAME_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_MEM_NAME_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_MEM_NAME_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_ELEM_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_ELEM_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_ELEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_LIST_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_LIST_MEM_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & PARAM_LIST_MEM_NAME_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_NAME_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + PARAM_LIST_MEM_NAME_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & CHAR_MEM_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + CHAR_MEM_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + CHAR_MEM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & CHAR_MEM_NAME_TBL_MASK) {
			if (tok_major_rev == 6)
			{
				local_data_ptr = obj_extn_ptr + CHAR_MEM_NAME_REF_OFFSET;
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + CHAR_MEM_NAME_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & REL_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + REL_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + REL_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + REL_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & UPDATE_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + UPDATE_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + UPDATE_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + UPDATE_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & COMMAND_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + COMMAND_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + COMMAND_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + COMMAND_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & CRIT_PARAM_TBL_MASK) {
			if (tok_major_rev == 6 || tok_major_rev == 8)
			{
				local_data_ptr = obj_extn_ptr + CRIT_PARAM_REF_OFFSET;
				if (tok_major_rev == 8)
				{
					local_data_ptr = obj_extn_ptr + CRIT_PARAM_REF_OFFSET_8;
				}
				*sp_addlsize +=
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
			}
			else if (tok_major_rev == 5)
			{
				local_data_ptr = obj_extn_ptr + CRIT_PARAM_REF_OFFSET_5;
				*sp_addlsize +=
					(((unsigned short) local_data_ptr[2]) << 8) |
					(unsigned short) local_data_ptr[3];
			}
		}

		if (tbl_req_mask & RESLV_REF_TBL_MASK)
		{											// Make sure that this optional table is really there.
			if ( (tok_major_rev == 8) && (dir_extn_len >= (RESLV_REF_OFFSET_8+TABLE_REF_LEN-1)) )
			{
				local_data_ptr = obj_extn_ptr + RESLV_REF_OFFSET_8;
				unsigned long addlsize =
					(((unsigned long) local_data_ptr[4]) << 24) |
					(((unsigned long) local_data_ptr[5]) << 16) |
					(((unsigned long) local_data_ptr[6]) << 8 ) |
					(unsigned long) local_data_ptr[7];
				*sp_addlsize += addlsize;
			}
		}

	}
	else {
		CRASH_RET(FETCH_INVALID_DIR_TYPE);
	}

	if (*sp_addlsize < MAX_OBJ_EXTN_LEN) {
		*sp_addlsize = MAX_OBJ_EXTN_LEN;
	}

	return (SUCCESS);

}

/*********************************************************************
 *
 *	Name:	fch_rod_device_dir
 *
 *	ShortDesc:	Interface for device ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for device directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_dev_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_device_dir(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
SCRATCH_PAD    *scrpad,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
BIN_DEVICE_DIR *bin_dev_dir)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned short  dir_extn_len;	/* extension length (first field in
					 * object extension) */
	unsigned char	major_rev_number;
	int             rcode;

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * This function or fch_rod_dir_spad_size() will be the very first
	 * call into DDS.  Therefore, DDS compatibility must be checked.
	 */
	 
	rcode = get_major_rev_number(env_info, rhandle,&major_rev_number);
	if (rcode != SUCCESS) {
		return(rcode);
	}


	if (tok_major_rev != major_rev_number)
	{
		// we set the "tok_major_rev" based on the file extension. e.g. 5 for ".fms" , 6 for ".fm6" and 8 for ".fm8".
		// in some cases ".fms" files can give "major_rev_number" 4 whereas 5 is expected.
		// We need to allow ".fms" files with major_rev_number 4 if tok_major_rev is 5.
		if(!((major_rev_number == 4) && (tok_major_rev == 5)))
		{
			return(DDS_WRONG_DDOD_REVISION);
		}
	}

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_dev_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(ROD_FETCH_DEVICE_DIR);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Check the available scratchpad size.  If there is not enough space
	 * for a directory object extension, call the size calculation and
	 * return the computed value for the minimum scratchpad size for the
	 * request.
	 */

	if ((scrpad->size - scrpad->used) < MAX_OBJ_EXTN_LEN) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, DEVICE_DIRECTORY_INDEX,
			sp_addlsize, tbl_req_mask, DEVICE_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, DEVICE_DIRECTORY_INDEX, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	/*
	 * Return with an error code if the extension length is not
	 * large enough for a device directory object extension.
	 */

	if (tok_major_rev == 8)
	{
		if (dir_extn_len < DEVICE_DIR_LEN_8) {
			return (FETCH_INVALID_EXTN_LEN);
		}	
	}
	else if (tok_major_rev == 6)
	{
		if (dir_extn_len < DEVICE_DIR_LEN) {
			return (FETCH_INVALID_EXTN_LEN);
		}
	}
	else if (tok_major_rev == 5)
	{
		if (dir_extn_len < DEVICE_DIR_LEN_5) {
			return (FETCH_INVALID_EXTN_LEN);
		}
	}
	else
	{
		ASSERT_DBG(false);
		rcode = DDS_WRONG_DDOD_REVISION;
	}

	/*
	 * Validate that the object is a device directory.
	 */

	if (*(obj_extn_ptr + 1) != DEVICE_DIR_TYPE) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Call the device directory fetch function
	 */

#ifdef DEBUG
	dds_dev_dir_flat_chk(bin_dev_dir, (FLAT_DEVICE_DIR *) 0);
#endif
	rcode = fetch_rod_device_dir(env_info, rhandle, rod_obj, scrpad,
		obj_extn_ptr, bin_dev_dir, &tbl_req_mask);
#ifdef DEBUG
	dds_dev_dir_flat_chk(bin_dev_dir, (FLAT_DEVICE_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, DEVICE_DIRECTORY_INDEX,
			sp_addlsize, tbl_req_mask, DEVICE_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	return (rcode);
}

/*********************************************************************
 *
 *	Name:	fch_rod_block_dir
 *
 *	ShortDesc:	Interface for block ROD directory fetch
 *
 *	Description:
 *		This routine provides an application interface for the ROD
 *		fetch for block directory tables.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		block_index -	index of the block directory object
 *		tbl_req_mask -	tables requested from the directory, indicated
 *						by individual bits set for each table
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and the local data
 *						referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		bin_blk_dir -	pointer to the binary structure containing the
 *						pointers to the requested tables in the
 *						scratchpad memory
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *		FETCH_INVALID_EXTN_LEN
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_block_dir(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    block_index,
SCRATCH_PAD    *scrpad,
unsigned long  *sp_addlsize,
unsigned long   tbl_req_mask,
BIN_BLOCK_DIR  *bin_blk_dir)
{
	OBJECT         *rod_obj;/* pointer to ROD object structure */
	unsigned char  *obj_extn_ptr;
	unsigned short  dir_extn_len;	/* extension length (first field in
					 * object extension) */
	int             rcode;

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && bin_blk_dir && sp_addlsize,
		FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(ROD_FETCH_BLOCK_DIR);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Check the available scratchpad size.  If there is not enough space
	 * for a directory object extension, call the size calculation and
	 * return the computed value for the minimum scratchpad size for the
	 * request.
	 */

	if ((scrpad->size - scrpad->used) < MAX_OBJ_EXTN_LEN) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, block_index,
			sp_addlsize, tbl_req_mask, BLOCK_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = scrpad->pad + scrpad->used;

	/*
	 * Retrieve the directory object.  Return with the resulting error code
	 * if not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, block_index, &rod_obj, scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length and point to the first directory table
	 * reference.
	 */

	dir_extn_len = (unsigned short) *obj_extn_ptr;

	/*
	 * Return with an error code if the extension length is not
	 * large enough for a block directory object extension.
	 */

	if (tok_major_rev == 8)
	{
		if (dir_extn_len < BLOCK_DIR_LEN_8) {
			return (FETCH_INVALID_EXTN_LEN);
		}	
	}
	else if (tok_major_rev == 6)
	{
		if (dir_extn_len < BLOCK_DIR_LEN) {
			return (FETCH_INVALID_EXTN_LEN);
		}
	}
	else if (tok_major_rev == 5)
	{
		if (dir_extn_len < BLOCK_DIR_LEN_5) {
			return (FETCH_INVALID_EXTN_LEN);
		}
	}
	else
	{
		ASSERT_DBG(false);
		rcode = DDS_WRONG_DDOD_REVISION;
	}

	/*
	 * Validate that the object is a block directory.
	 */

	if (*(obj_extn_ptr + 1) != BLOCK_DIR_TYPE) {
		return(FETCH_DIR_TYPE_MISMATCH);
	}

	/*
	 * Call the block directory fetch function
	 */

#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (FLAT_BLOCK_DIR *) 0);
#endif
	rcode = fetch_rod_block_dir(env_info, rhandle, rod_obj, scrpad,
		obj_extn_ptr, bin_blk_dir, &tbl_req_mask);
#ifdef DEBUG
	dds_blk_dir_flat_chk(bin_blk_dir, (FLAT_BLOCK_DIR *) 0);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_dir_spad_size(env_info, rhandle, block_index,
			sp_addlsize, tbl_req_mask, BLOCK_DIR_TYPE);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
		else {
			return (rcode);
		}
	}

	return (rcode);
}

