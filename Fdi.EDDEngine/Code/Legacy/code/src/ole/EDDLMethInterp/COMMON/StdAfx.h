// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if 0
#if !defined(AFX_STDAFX_H__6A994185_EDC5_404C_9EE4_47B0855D3529__INCLUDED_)
#define AFX_STDAFX_H__6A994185_EDC5_404C_9EE4_47B0855D3529__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif
#define _ATL_APARTMENT_THREADED

#include <afxwin.h>
#include <afxdisp.h>
#include <afxext.h>         // MFC extensions
#include <afxcmn.h>			// MFC support for Windows Common Controls

#include <afxtempl.h>       // MFC Templates 
#include <stdlib.h>
#endif
#include <stdarg.h>
#include <time.h>
#include <map>

#if 0
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

extern HINSTANCE g_AmsFFDevDLL;
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__6A994185_EDC5_404C_9EE4_47B0855D3529__INCLUDED)

#include "PlatformCommon.h"
#include "stdstring.h"
#include "varient.h"
#include "varientTag.h"