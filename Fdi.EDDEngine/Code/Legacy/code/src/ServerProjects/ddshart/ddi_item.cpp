/*
 *	@(#) $Id: ddi_item.c,v 1.6 1996/06/28 18:58:35 stevbey Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	This file contains all functions relating to ddi_get_item
 */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
/*
 * defines for requesting specific var depbin lists
 */

#define	VAR_MAIN	0
#define	VAR_ACT		1
#define	VAR_MISC	2

#define	VAR_SUBS_COUNT	3

#define NOT_VAR		0




/*********************************************************************
 *
 *	Name: convert_item_spec
 *	ShortDesc: convert the item specifier into a dd_ref
 *
 *	Description:
 *		convert_item_spec takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request function to
 *		get the device description reference
 *
 *	Inputs:
 *		dd_item_spec:	a pointer to the DDI_ITEM_SPECIFIER structure
 *		block_handle:	the current block handle
 *		bt_elem:		a pointer to a Block Table element
 *
 *	Outputs:
 *		dd_ref:			a pointer to the device description reference
 *		item_type:		a pointer to the type of the item
 *
 *	Returns: DDI_INVALID_TYPE and returns from dd_ref table
 *  request functions
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int
convert_item_spec(
ENV_INFO		*env_info,
DDI_ITEM_SPECIFIER *dd_item_spec,
BLOCK_HANDLE    block_handle,
BLK_TBL_ELEM   *bt_elem,
DD_REFERENCE   *dd_ref,
ITEM_TYPE      *item_type)
{

	int             rc;	/* return code */
	ITEM_TBL       *it;
	ITEM_TBL_ELEM  *it_elem;
	FLAT_DEVICE_DIR *flat_device_dir;

	it_elem = NULL;
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, (void **) &flat_device_dir);
	if (rc != SUCCESS) {
		return (rc);
	}

	it = &flat_device_dir->item_tbl;

	switch (dd_item_spec->type) {

	case DDI_ITEM_BLOCK:
		rc = tr_block_to_ite(it, bt_elem, &it_elem);
		break;

	case DDI_ITEM_ID:
		rc = tr_id_to_ite(env_info, it, dd_item_spec->item.id, &it_elem);
		break;

	case DDI_ITEM_NAME:
		rc = tr_name_to_ite(it, bt_elem, dd_item_spec->item.name, &it_elem);
		break;

	case DDI_ITEM_OP_INDEX:
		rc = tr_op_index_to_ite(env_info, block_handle, it, bt_elem, dd_item_spec->item.op_index, &it_elem);
		break;

	case DDI_ITEM_PARAM:
		rc = tr_param_to_ite(it, bt_elem, dd_item_spec->item.param, &it_elem);
		break;

	case DDI_ITEM_PARAM_LIST:
		rc = tr_param_list_to_ite(it, bt_elem, dd_item_spec->item.param_list, &it_elem);
		break;

	case DDI_ITEM_CHARACTERISTICS:
		rc = tr_characteristics_to_ite(it, bt_elem, dd_item_spec->item.characteristics, &it_elem);
		break;

	case DDI_ITEM_ID_SI:
		rc = tr_id_si_to_ite(it, bt_elem, dd_item_spec->item.id, dd_item_spec->subindex, &it_elem);
		break;

	case DDI_ITEM_NAME_SI:
		rc = tr_name_si_to_ite(it, bt_elem, dd_item_spec->item.name, dd_item_spec->subindex, &it_elem);
		break;

	case DDI_ITEM_OP_INDEX_SI:
		rc = tr_op_index_si_to_ite(env_info, block_handle, it, bt_elem, dd_item_spec->item.op_index, dd_item_spec->subindex, &it_elem);
		break;

	case DDI_ITEM_PARAM_SI:
		rc = tr_param_si_to_ite(it, bt_elem, dd_item_spec->item.param, dd_item_spec->subindex, &it_elem);
		break;

	default:
		rc = DDI_INVALID_TYPE;
	}

	if (it_elem != NULL) {

		/*
		 * copy the dd reference and set item type for our return
		 * values
		 */

		memcpy((char *) dd_ref, (char *) &(ITE_DD_REF(it_elem)), sizeof(*dd_ref));

		*item_type = ITE_ITEM_TYPE(it_elem);
	}

	return rc;

}


/*********************************************************************
 *
 *	Name: get_masks_ptr
 *	ShortDesc: get the flat masks pointer
 *
 *	Description:
 *		This function will return the address for the masks of the
 *		specified item
 *
 *	Inputs:
 *		item_type:		type of item requested
 *		item:			the loaded FLAT structure
 *
 *	Outputs:
 *		see Returns:
 *
 *	Returns:
 *		a pointer to the masks in the item, pointer is NULL if the
 *		type is not in the switch statement
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
FLAT_MASKS     *
get_masks_ptr(
ITEM_TYPE       item_type,
void           *item)
{
	FLAT_MASKS     *masks_ptr;	/* the pointer to return to the caller */

	switch (item_type) {
	case VARIABLE_ITYPE:
		masks_ptr = &((FLAT_VAR *) item)->masks;
		break;

	case MENU_ITYPE:
		masks_ptr = &((FLAT_MENU *) item)->masks;
		break;

	case EDIT_DISP_ITYPE:
		masks_ptr = &((FLAT_EDIT_DISPLAY *) item)->masks;
		break;

	case METHOD_ITYPE:
		masks_ptr = &((FLAT_METHOD *) item)->masks;
		break;

	case REFRESH_ITYPE:
		masks_ptr = &((FLAT_REFRESH *) item)->masks;
		break;

	case UNIT_ITYPE:
		masks_ptr = &((FLAT_UNIT *) item)->masks;
		break;

	case WAO_ITYPE:
		masks_ptr = &((FLAT_WAO *) item)->masks;
		break;

	case ITEM_ARRAY_ITYPE:
		masks_ptr = &((FLAT_ITEM_ARRAY *) item)->masks;
		break;

	case COLLECTION_ITYPE:
		masks_ptr = &((FLAT_COLLECTION *) item)->masks;
		break;

	case BLOCK_ITYPE:
		masks_ptr = &((FLAT_BLOCK *) item)->masks;
		break;

	case PROGRAM_ITYPE:
		masks_ptr = &((FLAT_PROGRAM *) item)->masks;
		break;

	case RECORD_ITYPE:
		masks_ptr = &((FLAT_RECORD *) item)->masks;
		break;

	case ARRAY_ITYPE:
		masks_ptr = &((FLAT_ARRAY *) item)->masks;
		break;

	case VAR_LIST_ITYPE:
		masks_ptr = &((FLAT_VAR_LIST *) item)->masks;
		break;

	case RESP_CODES_ITYPE:
		masks_ptr = &((FLAT_RESP_CODE *) item)->masks;
		break;

	case DOMAIN_ITYPE:
		masks_ptr = &((FLAT_DOMAIN *) item)->masks;
		break;
#ifdef HART
	case COMMAND_ITYPE:
		masks_ptr = &((FLAT_COMMAND *) item)->masks;
		break;
#endif

	case AXIS_ITYPE:
		masks_ptr = &((FLAT_AXIS *) item)->masks;
		break;

	case CHART_ITYPE:
		masks_ptr = &((FLAT_CHART *) item)->masks;
		break;

	case FILE_ITYPE:
		masks_ptr = &((FLAT_FILE *) item)->masks;
		break;

	case GRAPH_ITYPE:
		masks_ptr = &((FLAT_GRAPH *) item)->masks;
		break;

	case GRID_ITYPE:
		masks_ptr = &((FLAT_GRID *) item)->masks;
		break;

	case IMAGE_ITYPE:
		masks_ptr = &((FLAT_IMAGE *) item)->masks;
		break;

	case LIST_ITYPE:
		masks_ptr = &((FLAT_LIST *) item)->masks;
		break;

	case SOURCE_ITYPE:
		masks_ptr = &((FLAT_SOURCE *) item)->masks;
		break;

	case WAVEFORM_ITYPE:
		masks_ptr = &((FLAT_WAVEFORM *) item)->masks;
		break;

	default:
		return NULL;
	}
	return masks_ptr;
}


/*********************************************************************
 *
 *	Name: get_item_id()
 *
 *	ShortDesc: get the DDL item's item ID
 *
 *	Description:
 *		This function will return the item ID of the given
 *      DDL item
 *
 *	Inputs:
 *		item_type:		type of item requested
 *		item:			pointer to the DDL item structure
 *
 *	Outputs:
 *		see Returns:
 *
 *	Returns:
 *      the item ID of the given item
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
                ITEM_ID
get_item_id(
ITEM_TYPE       item_type,
void           *item)
{
	ITEM_ID         item_id;

	switch (item_type) {
	case VARIABLE_ITYPE:
		item_id = ((FLAT_VAR *) item)->id;
		break;

	case MENU_ITYPE:
		item_id = ((FLAT_MENU *) item)->id;
		break;

	case EDIT_DISP_ITYPE:
		item_id = ((FLAT_EDIT_DISPLAY *) item)->id;
		break;

	case METHOD_ITYPE:
		item_id = ((FLAT_METHOD *) item)->id;
		break;

	case REFRESH_ITYPE:
		item_id = ((FLAT_REFRESH *) item)->id;
		break;

	case UNIT_ITYPE:
		item_id = ((FLAT_UNIT *) item)->id;
		break;

	case WAO_ITYPE:
		item_id = ((FLAT_WAO *) item)->id;
		break;

	case ITEM_ARRAY_ITYPE:
		item_id = ((FLAT_ITEM_ARRAY *) item)->id;
		break;

	case COLLECTION_ITYPE:
		item_id = ((FLAT_COLLECTION *) item)->id;
		break;

	case BLOCK_ITYPE:
		item_id = ((FLAT_BLOCK *) item)->id;
		break;

	case PROGRAM_ITYPE:
		item_id = ((FLAT_PROGRAM *) item)->id;
		break;

	case RECORD_ITYPE:
		item_id = ((FLAT_RECORD *) item)->id;
		break;

	case ARRAY_ITYPE:
		item_id = ((FLAT_ARRAY *) item)->id;
		break;

	case VAR_LIST_ITYPE:
		item_id = ((FLAT_VAR_LIST *) item)->id;
		break;

	case RESP_CODES_ITYPE:
		item_id = ((FLAT_RESP_CODE *) item)->id;
		break;

	case DOMAIN_ITYPE:
		item_id = ((FLAT_DOMAIN *) item)->id;
		break;

#ifdef HART
	case COMMAND_ITYPE:
		item_id = ((FLAT_COMMAND *) item)->id;
		break;
#endif

	case AXIS_ITYPE:
		item_id = ((FLAT_AXIS *) item)->id;
		break;

	case CHART_ITYPE:
		item_id = ((FLAT_CHART *) item)->id;
		break;

	case FILE_ITYPE:
		item_id = ((FLAT_FILE *) item)->id;
		break;

	case GRAPH_ITYPE:
		item_id = ((FLAT_GRAPH *) item)->id;
		break;

	case GRID_ITYPE:
		item_id = ((FLAT_GRID *) item)->id;
		break;

	case IMAGE_ITYPE:
		item_id = ((FLAT_IMAGE *) item)->id;
		break;

	case LIST_ITYPE:
		item_id = ((FLAT_LIST *) item)->id;
		break;

	case SOURCE_ITYPE:
		item_id = ((FLAT_SOURCE *) item)->id;
		break;

	case WAVEFORM_ITYPE:
		item_id = ((FLAT_WAVEFORM *) item)->id;
		break;

	default:
		return 0;
	}

	return item_id;
}



/*********************************************************************
 *
 *	Name: get_depbin_list_ptrs
 *	ShortDesc: returns a list of pointers for all depbin lists pertaining
 *	to the item requested
 *
 *	Description:
 *		get_depbin_list_ptrs loads an array of void pointers with the
 *		addresses of the depbin lists for a given flat structure (up to
 *		3 pointers for a VARIABLE and one pointer for all other items
 *
 *	Inputs:
 *		item_type:	type of item requested
 *		item:		the FLAT item structure
 *
 *	Outputs:
 *		depbins:	the array of pointers
 *
 *	Returns: DDI_INVALID_ITEM_TYPE, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int
get_depbin_list_ptrs(
ITEM_TYPE       item_type,
void           *item,
void          **depbins[])
{

	depbins[0] = NULL;
	depbins[1] = NULL;
	depbins[2] = NULL;

	switch (item_type) {

	case VARIABLE_ITYPE:{

			FLAT_VAR       *flat_var = (FLAT_VAR *) item;

			depbins[VAR_MAIN] = (void **) &(flat_var->depbin);

			if (flat_var->actions) {

				depbins[VAR_ACT] = (void **) &(flat_var->actions->depbin);
			}
			if (flat_var->misc) {

				depbins[VAR_MISC] = (void **) &(flat_var->misc->depbin);
			}
		}
		break;

	case MENU_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_MENU *) item)->depbin;
		break;

	case EDIT_DISP_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_EDIT_DISPLAY *) item)->depbin;
		break;

	case METHOD_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_METHOD *) item)->depbin;
		break;

	case REFRESH_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_REFRESH *) item)->depbin;
		break;

	case UNIT_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_UNIT *) item)->depbin;
		break;

	case WAO_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_WAO *) item)->depbin;
		break;

	case ITEM_ARRAY_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_ITEM_ARRAY *) item)->depbin;
		break;

	case COLLECTION_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_COLLECTION *) item)->depbin;
		break;

	case BLOCK_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_BLOCK *) item)->depbin;
		break;

	case PROGRAM_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_PROGRAM *) item)->depbin;
		break;

	case RECORD_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_RECORD *) item)->depbin;
		break;

	case ARRAY_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_ARRAY *) item)->depbin;
		break;

	case VAR_LIST_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_VAR_LIST *) item)->depbin;
		break;

	case RESP_CODES_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_RESP_CODE *) item)->depbin;
		break;

	case DOMAIN_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_DOMAIN *) item)->depbin;
		break;
#ifdef HART
	case COMMAND_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_COMMAND *) item)->depbin;
		break;
#endif

	case AXIS_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_AXIS *) item)->depbin;
		break;

	case CHART_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_CHART *) item)->depbin;
		break;

	case FILE_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_FILE *) item)->depbin;
		break;

	case GRAPH_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_GRAPH *) item)->depbin;
		break;

	case GRID_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_GRID *) item)->depbin;
		break;

	case IMAGE_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_IMAGE *) item)->depbin;
		break;

	case LIST_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_LIST *) item)->depbin;
		break;

	case SOURCE_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_SOURCE *) item)->depbin;
		break;

	case WAVEFORM_ITYPE:
		depbins[NOT_VAR] = (void **) &((FLAT_WAVEFORM *) item)->depbin;
		break;

	default:
		return DDI_INVALID_ITEM_TYPE;
	}
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: get_depbin_list_sizes
 *	ShortDesc: returns a list of all the depbin list sizes for the item
 *	requested
 *
 *	Description:
 *		get_depbin_list_sizes loads an array with the sizes
 *		of the depbin lists for a given flat structure (3 lists for a
 *		VARIABLE and one list for all other items)
 *
 *	Inputs:
 *		item_type:	type of item requested
 *
 *	Outputs:
 *		size:		the array of sizes to fill
 *
 *	Returns: DDI_INVALID_ITEM_TYPE, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/


static int
get_depbin_list_sizes(
ITEM_TYPE       item_type,
size_t          size[])
{

	switch (item_type) {

	case VARIABLE_ITYPE:
		size[VAR_MAIN] = sizeof(VAR_DEPBIN);
		size[VAR_ACT] = sizeof(VAR_ACTIONS_DEPBIN);
		size[VAR_MISC] = sizeof(VAR_MISC_DEPBIN);
		break;

	case MENU_ITYPE:
		size[NOT_VAR] = sizeof(MENU_DEPBIN);
		break;

	case EDIT_DISP_ITYPE:
		size[NOT_VAR] = sizeof(EDIT_DISPLAY_DEPBIN);
		break;

	case METHOD_ITYPE:
		size[NOT_VAR] = sizeof(METHOD_DEPBIN);
		break;

	case REFRESH_ITYPE:
		size[NOT_VAR] = sizeof(REFRESH_DEPBIN);
		break;

	case UNIT_ITYPE:
		size[NOT_VAR] = sizeof(UNIT_DEPBIN);
		break;

	case WAO_ITYPE:
		size[NOT_VAR] = sizeof(WAO_DEPBIN);
		break;

	case ITEM_ARRAY_ITYPE:
		size[NOT_VAR] = sizeof(ITEM_ARRAY_DEPBIN);
		break;

	case COLLECTION_ITYPE:
		size[NOT_VAR] = sizeof(COLLECTION_DEPBIN);
		break;

	case BLOCK_ITYPE:
		size[NOT_VAR] = sizeof(BLOCK_DEPBIN);
		break;

	case PROGRAM_ITYPE:
		size[NOT_VAR] = sizeof(PROGRAM_DEPBIN);
		break;

	case RECORD_ITYPE:
		size[NOT_VAR] = sizeof(RECORD_DEPBIN);
		break;

	case ARRAY_ITYPE:
		size[NOT_VAR] = sizeof(ARRAY_DEPBIN);
		break;

	case VAR_LIST_ITYPE:
		size[NOT_VAR] = sizeof(VAR_LIST_DEPBIN);
		break;

	case RESP_CODES_ITYPE:
		size[NOT_VAR] = sizeof(RESP_CODE_DEPBIN);
		break;

	case DOMAIN_ITYPE:
		size[NOT_VAR] = sizeof(DOMAIN_DEPBIN);
		break;

#ifdef HART
	case COMMAND_ITYPE:
		size[NOT_VAR] = sizeof(COMMAND_DEPBIN);
		break;
#endif

	case AXIS_ITYPE:
		size[NOT_VAR] = sizeof(AXIS_DEPBIN);
		break;

	case CHART_ITYPE:
		size[NOT_VAR] = sizeof(CHART_DEPBIN);
		break;

	case FILE_ITYPE:
		size[NOT_VAR] = sizeof(FILE_DEPBIN);
		break;

	case GRAPH_ITYPE:
		size[NOT_VAR] = sizeof(GRAPH_DEPBIN);
		break;

	case GRID_ITYPE:
		size[NOT_VAR] = sizeof(GRID_DEPBIN);
		break;

	case IMAGE_ITYPE:
		size[NOT_VAR] = sizeof(IMAGE_DEPBIN);
		break;

	case LIST_ITYPE:
		size[NOT_VAR] = sizeof(LIST_DEPBIN);
		break;

	case SOURCE_ITYPE:
		size[NOT_VAR] = sizeof(SOURCE_DEPBIN);
		break;

	case WAVEFORM_ITYPE:
		size[NOT_VAR] = sizeof(WAVEFORM_DEPBIN);
		break;

	default:
		return DDI_INVALID_PARAM;
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: malloc_depbin_list
 *	ShortDesc: malloc the depbin list for the specified item
 *
 *	Description:
 *		malloc_depbin_list will malloc the depbin list for the
 *		requested item and hook it up to the depbin pointer
 *		in the flat item structure
 *
 *	Inputs:
 *		item_type:	type of item requested
 *		var_req:	the request mask for the variable
 *
 *	Outputs:
 *		item:	the FLAT item structure with depbin list hooked up
 *
 *	Returns:	DDI_MEMORY_ERROR, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/


static
int
malloc_depbin_list(
ITEM_TYPE       item_type,
void           *item,
unsigned short  var_req)
{

	void           *new_depbin;	/* a pointer to the malloced memory */
	size_t          size[VAR_SUBS_COUNT];	/* this is the size of the
						 * depbin list */
	void          **depbins[VAR_SUBS_COUNT];	/* array for holding
							 * depbin list addresses */
	int             rc;	/* return code */


	if (var_req >= VAR_SUBS_COUNT) {

		return DDI_INVALID_PARAM;
	}

	rc = get_depbin_list_ptrs(item_type, item, depbins);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_depbin_list_sizes(item_type, size);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * malloc the depbin list
	 */

	new_depbin = (void *) malloc(size[var_req]);
	if (new_depbin == NULL) {

		return DDI_MEMORY_ERROR;
	}

	memcpy((char *) new_depbin, (char *) *depbins[var_req], size[var_req]);
	*depbins[var_req] = new_depbin;

	return DDS_SUCCESS;
}


/**********************************************************************
 *
 *	Name: get_depbin_ptr
 *	ShortDesc: get a pointer to the depbin of the requested item/attribute
 *
 *	Description:
 *		get_depbin_ptr returns the address of the DEPBIN for the
 *		requested item and attribute
 *
 *	Inputs:
 *		item_type:		type of item requested
 *		item:			the loaded FLAT structure
 *		attribute:		attribute requested
 *
 *	Outputs:
 *		See Returns:
 *
 *	Returns:
 *		a pointer to the requested DEPBIN structure or NULL
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
DEPBIN        **
get_depbin_ptr(
ITEM_TYPE       item_type,
void           *item,
unsigned long   attribute)
{
	DEPBIN        **depbin;	/* the pointer to return to the caller */

	switch (item_type) {

	case VARIABLE_ITYPE:{
			FLAT_VAR       *flat_var = (FLAT_VAR *) item;

			switch (attribute) {

				/* variable BASE */
			case VAR_CLASS:
				depbin = &flat_var->depbin->db_class;
				break;
			case VAR_HANDLING:
				depbin = &flat_var->depbin->db_handling;
				break;
			case VAR_LABEL:
				depbin = &flat_var->depbin->db_label;
				break;
			case VAR_HELP:
				depbin = &flat_var->depbin->db_help;
				break;
			case VAR_RESP_CODES:
				depbin = &flat_var->depbin->db_resp_codes;
				break;
			case VAR_TYPE_SIZE:
				depbin = &flat_var->depbin->db_type_size;
				break;
			case VAR_DISPLAY:
				depbin = &flat_var->depbin->db_display;
				break;
			case VAR_EDIT:
				depbin = &flat_var->depbin->db_edit;
				break;
			case VAR_ENUMS:
				depbin = &flat_var->depbin->db_enums;
				break;
			case VAR_INDEX_ITEM_ARRAY:
				depbin = &flat_var->depbin->db_index_item_array;
				break;
			case VAR_DEFAULT_VALUE:
				depbin = &flat_var->depbin->db_default_value;
				break;

				/* variable MISC */
			case VAR_UNIT:
				depbin = &flat_var->misc->depbin->db_unit;
				break;
			case VAR_READ_TIME_OUT:
				depbin = &flat_var->misc->depbin->db_read_time_out;
				break;
			case VAR_WRITE_TIME_OUT:
				depbin = &flat_var->misc->depbin->db_write_time_out;
				break;
			case VAR_MIN_VAL:
				depbin = &flat_var->misc->depbin->db_min_val;
				break;
			case VAR_MAX_VAL:
				depbin = &flat_var->misc->depbin->db_max_val;
				break;
			case VAR_SCALE:
				depbin = &flat_var->misc->depbin->db_scale;
				break;
			case VAR_VALID:
				depbin = &flat_var->misc->depbin->db_valid;
				break;
			case VAR_DEBUG:
				depbin = &flat_var->misc->depbin->db_debug_info;
				break;
				/* variable ACTIONS */
			case VAR_PRE_READ_ACT:
				depbin = &flat_var->actions->depbin->db_pre_read_act;
				break;
			case VAR_POST_READ_ACT:
				depbin = &flat_var->actions->depbin->db_post_read_act;
				break;
			case VAR_PRE_WRITE_ACT:
				depbin = &flat_var->actions->depbin->db_pre_write_act;
				break;
			case VAR_POST_WRITE_ACT:
				depbin = &flat_var->actions->depbin->db_post_write_act;
				break;
			case VAR_REFRESH_ACT:
				depbin = &flat_var->actions->depbin->db_refresh_act;
				break;

			case VAR_PRE_EDIT_ACT:
				depbin = &flat_var->actions->depbin->db_pre_edit_act;
				break;
			case VAR_POST_EDIT_ACT:
				depbin = &flat_var->actions->depbin->db_post_edit_act;
				break;
            case VAR_TIME_SCALE:
				depbin = &flat_var->misc->depbin->db_time_scale;
				break;
			case VAR_TIME_FORMAT:
				depbin = &flat_var->misc->depbin->db_time_format;
				break;
			default:
				return NULL;
			}
		}
		break;

	case MENU_ITYPE:{
			FLAT_MENU      *flat_menu = (FLAT_MENU *) item;

			switch (attribute) {
			case MENU_LABEL:
				depbin = &flat_menu->depbin->db_label;
				break;
			case MENU_HELP:
				depbin = &flat_menu->depbin->db_help;
				break;
			case MENU_ITEMS:
				depbin = &flat_menu->depbin->db_items;
				break;
			case MENU_STYLE:
				depbin = &flat_menu->depbin->db_style;
				break;
			case MENU_VALID:
				depbin = &flat_menu->depbin->db_valid;
				break;
			case MENU_DEBUG:
				depbin = &flat_menu->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case EDIT_DISP_ITYPE:{
			FLAT_EDIT_DISPLAY *flat_edit_display = (FLAT_EDIT_DISPLAY *) item;

			switch (attribute) {
			case EDIT_DISPLAY_LABEL:
				depbin = &flat_edit_display->depbin->db_label;
				break;
			case EDIT_DISPLAY_HELP:
				depbin = &flat_edit_display->depbin->db_help;
				break;
			case EDIT_DISPLAY_VALID:
				depbin = &flat_edit_display->depbin->db_valid;
				break;
			case EDIT_DISPLAY_EDIT_ITEMS:
				depbin = &flat_edit_display->depbin->db_edit_items;
				break;
			case EDIT_DISPLAY_DISP_ITEMS:
				depbin = &flat_edit_display->depbin->db_disp_items;
				break;
			case EDIT_DISPLAY_PRE_EDIT_ACT:
				depbin = &flat_edit_display->depbin->db_pre_edit_act;
				break;
			case EDIT_DISPLAY_POST_EDIT_ACT:
				depbin = &flat_edit_display->depbin->db_post_edit_act;
				break;
			case EDIT_DISPLAY_DEBUG:
				depbin = &flat_edit_display->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case METHOD_ITYPE:{
			FLAT_METHOD    *flat_method = (FLAT_METHOD *) item;

			switch (attribute) {
			case METHOD_CLASS:
				depbin = &flat_method->depbin->db_class;
				break;
			case METHOD_LABEL:
				depbin = &flat_method->depbin->db_label;
				break;
			case METHOD_HELP:
				depbin = &flat_method->depbin->db_help;
				break;
			case METHOD_DEF:
				depbin = &flat_method->depbin->db_def;
				break;
			case METHOD_VALID:
				depbin = &flat_method->depbin->db_valid;
				break;
			case METHOD_SCOPE:
				depbin = &flat_method->depbin->db_scope;
				break;
			case METHOD_TYPE:
				depbin = &flat_method->depbin->db_type;
				break;
			case METHOD_PARAMS:
				depbin = &flat_method->depbin->db_params;
				break;
			case METHOD_DEBUG:
				depbin = &flat_method->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case REFRESH_ITYPE:{
			FLAT_REFRESH   *flat_refresh = (FLAT_REFRESH *) item;

			switch (attribute) {
			case REFRESH_ITEMS:
				depbin = &flat_refresh->depbin->db_items;
				break;
			case REFRESH_DEBUG:
				depbin = &flat_refresh->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case UNIT_ITYPE:{
			FLAT_UNIT      *flat_unit = (FLAT_UNIT *) item;

			switch (attribute) {
			case UNIT_ITEMS:
				depbin = &flat_unit->depbin->db_items;
				break;
			case UNIT_DEBUG:
				depbin = &flat_unit->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case WAO_ITYPE:{
			FLAT_WAO       *flat_wao = (FLAT_WAO *) item;

			switch (attribute) {
			case WAO_ITEMS:
				depbin = &flat_wao->depbin->db_items;
				break;
			case WAO_DEBUG:
				depbin = &flat_wao->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case ITEM_ARRAY_ITYPE:{
			FLAT_ITEM_ARRAY *flat_item_array = (FLAT_ITEM_ARRAY *) item;

			switch (attribute) {
			case ITEM_ARRAY_ELEMENTS:
				depbin = &flat_item_array->depbin->db_elements;
				break;
			case ITEM_ARRAY_LABEL:
				depbin = &flat_item_array->depbin->db_label;
				break;
			case ITEM_ARRAY_HELP:
				depbin = &flat_item_array->depbin->db_help;
				break;
			case ITEM_ARRAY_VALIDITY:
				depbin = &flat_item_array->depbin->db_valid;
				break;
			case ITEM_ARRAY_DEBUG:
				depbin = &flat_item_array->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case COLLECTION_ITYPE:{
			FLAT_COLLECTION *flat_collection = (FLAT_COLLECTION *) item;

			switch (attribute) {
			case COLLECTION_MEMBERS:
				depbin = &flat_collection->depbin->db_members;
				break;
			case COLLECTION_LABEL:
				depbin = &flat_collection->depbin->db_label;
				break;
			case COLLECTION_HELP:
				depbin = &flat_collection->depbin->db_help;
				break;
			case COLLECTION_DEBUG:
				depbin = &flat_collection->depbin->db_debug_info;
				break;
			case COLLECTION_VALIDITY:
				depbin = &flat_collection->depbin->db_valid;
				break;
			default:
				return NULL;
			}
		}
		break;

	case BLOCK_ITYPE:{
			FLAT_BLOCK     *flat_block = (FLAT_BLOCK *) item;

			switch (attribute) {

			case BLOCK_CHARACTERISTIC:
				depbin = &flat_block->depbin->db_characteristic;
				break;
			case BLOCK_LABEL:
				depbin = &flat_block->depbin->db_label;
				break;
			case BLOCK_HELP:
				depbin = &flat_block->depbin->db_help;
				break;
			case BLOCK_PARAM:
				depbin = &flat_block->depbin->db_param;
				break;
			case BLOCK_MENU:
				depbin = &flat_block->depbin->db_menu;
				break;
			case BLOCK_EDIT_DISP:
				depbin = &flat_block->depbin->db_edit_disp;
				break;
			case BLOCK_METHOD:
				depbin = &flat_block->depbin->db_method;
				break;
			case BLOCK_UNIT:
				depbin = &flat_block->depbin->db_unit;
				break;
			case BLOCK_REFRESH:
				depbin = &flat_block->depbin->db_refresh;
				break;
			case BLOCK_WAO:
				depbin = &flat_block->depbin->db_wao;
				break;
			case BLOCK_COLLECT:
				depbin = &flat_block->depbin->db_collect;
				break;
			case BLOCK_ITEM_ARRAY:
				depbin = &flat_block->depbin->db_item_array;
				break;
			case BLOCK_PARAM_LIST:
				depbin = &flat_block->depbin->db_param_list;
				break;
			case BLOCK_DEBUG:
				depbin = &flat_block->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case PROGRAM_ITYPE:{
			FLAT_PROGRAM   *flat_program = (FLAT_PROGRAM *) item;

			switch (attribute) {
			case PROGRAM_ARGS:
				depbin = &flat_program->depbin->db_args;
				break;
			case PROGRAM_RESP_CODES:
				depbin = &flat_program->depbin->db_resp_codes;
				break;
			default:
				return NULL;
			}
		}
		break;

	case RECORD_ITYPE:{
			FLAT_RECORD    *flat_record = (FLAT_RECORD *) item;

			switch (attribute) {
			case RECORD_MEMBERS:
				depbin = &flat_record->depbin->db_members;
				break;
			case RECORD_LABEL:
				depbin = &flat_record->depbin->db_label;
				break;
			case RECORD_HELP:
				depbin = &flat_record->depbin->db_help;
				break;
			case RECORD_RESP_CODES:
				depbin = &flat_record->depbin->db_resp_codes;
				break;
			case RECORD_DEBUG:
				depbin = &flat_record->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case ARRAY_ITYPE:{
			FLAT_ARRAY     *flat_array = (FLAT_ARRAY *) item;

			switch (attribute) {
			case ARRAY_LABEL:
				depbin = &flat_array->depbin->db_label;
				break;
			case ARRAY_HELP:
				depbin = &flat_array->depbin->db_help;
				break;
			case ARRAY_TYPE:
				depbin = &flat_array->depbin->db_type;
				break;
			case ARRAY_NUM_OF_ELEMENTS:
				depbin = &flat_array->depbin->db_num_of_elements;
				break;
			case ARRAY_RESP_CODES:
				depbin = &flat_array->depbin->db_resp_codes;
				break;
			case ARRAY_VALID:
				depbin = &flat_array->depbin->db_valid;
				break;
			case ARRAY_DEBUG:
				depbin = &flat_array->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case VAR_LIST_ITYPE:{
			FLAT_VAR_LIST  *flat_var_list = (FLAT_VAR_LIST *) item;

			switch (attribute) {
			case VAR_LIST_MEMBERS:
				depbin = &flat_var_list->depbin->db_members;
				break;
			case VAR_LIST_LABEL:
				depbin = &flat_var_list->depbin->db_label;
				break;
			case VAR_LIST_HELP:
				depbin = &flat_var_list->depbin->db_help;
				break;
			case VAR_LIST_RESP_CODES:
				depbin = &flat_var_list->depbin->db_resp_codes;
				break;
			case VAR_LIST_DEBUG:
				depbin = &flat_var_list->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case RESP_CODES_ITYPE:{
			FLAT_RESP_CODE *flat_resp_codes = (FLAT_RESP_CODE *) item;

			switch (attribute) {
			case RESP_CODE_MEMBER:
				depbin = &flat_resp_codes->depbin->db_member;
				break;
			default:
				return NULL;
			}
		}
		break;

	case DOMAIN_ITYPE:{
			FLAT_DOMAIN    *flat_domain = (FLAT_DOMAIN *) item;

			switch (attribute) {
			case DOMAIN_HANDLING:
				depbin = &flat_domain->depbin->db_handling;
				break;
			case DOMAIN_RESP_CODES:
				depbin = &flat_domain->depbin->db_resp_codes;
				break;
			default:
				return NULL;

			}
		}
		break;

#ifdef HART
	case COMMAND_ITYPE:{
			FLAT_COMMAND   *flat_command = (FLAT_COMMAND *) item;

			switch (attribute) {
			case COMMAND_NUMBER:
				depbin = &flat_command->depbin->db_number;
				break;
			case COMMAND_OPER:
				depbin = &flat_command->depbin->db_oper;
				break;
			case COMMAND_TRANS:
				depbin = &flat_command->depbin->db_trans;
				break;
			case COMMAND_RESP_CODES:
				depbin = &flat_command->depbin->db_resp_codes;
				break;
			case COMMAND_DEBUG:
				depbin = &flat_command->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;
#endif

	case AXIS_ITYPE:{
			FLAT_AXIS *flat_axis = (FLAT_AXIS *) item;

			switch (attribute) {
			case AXIS_HELP:
				depbin = &flat_axis->depbin->db_help;
				break;
			case AXIS_LABEL:
				depbin = &flat_axis->depbin->db_label;
				break;
			case AXIS_MINVAL:
				depbin = &flat_axis->depbin->db_min_value;
				break;
			case AXIS_MAXVAL:
				depbin = &flat_axis->depbin->db_max_value;
				break;
			case AXIS_MAX_VAL_REF:
				depbin = &flat_axis->depbin->db_max_value;
				break;
			case AXIS_MIN_VAL_REF:
				depbin = &flat_axis->depbin->db_min_value;
				break;
			case AXIS_SCALING:
				depbin = &flat_axis->depbin->db_scaling;
				break;
			case AXIS_CONSTUNIT:
				depbin = &flat_axis->depbin->db_constant_unit;
				break;
			case AXIS_VALID:
				depbin = &flat_axis->depbin->db_valid;
				break;
			case AXIS_DEBUG:
				depbin = &flat_axis->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case CHART_ITYPE:{
			FLAT_CHART *flat_chart = (FLAT_CHART *) item;

			switch (attribute) {
			case CHART_HELP:
				depbin = &flat_chart->depbin->db_help;
				break;
			case CHART_LABEL:
				depbin = &flat_chart->depbin->db_label;
				break;
			case CHART_MEMBERS:
				depbin = &flat_chart->depbin->db_members;
				break;
			case CHART_CYCLETIME:
				depbin = &flat_chart->depbin->db_cycle_time;
				break;
            case CHART_HEIGHT:
				depbin = &flat_chart->depbin->db_height;
				break;
            case CHART_WIDTH:
				depbin = &flat_chart->depbin->db_width;
				break;
            case CHART_LENGTH:
				depbin = &flat_chart->depbin->db_length;
				break;
            case CHART_TYPE:
				depbin = &flat_chart->depbin->db_type;
				break;
            case CHART_VALID:
				depbin = &flat_chart->depbin->db_valid;
				break;
			case CHART_DEBUG:
				depbin = &flat_chart->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case FILE_ITYPE:{
			FLAT_FILE *flat_file = (FLAT_FILE *) item;

			switch (attribute) {
			case FILE_HELP:
				depbin = &flat_file->depbin->db_help;
				break;
			case FILE_LABEL:
				depbin = &flat_file->depbin->db_label;
				break;
			case FILE_MEMBERS:
				depbin = &flat_file->depbin->db_members;
				break;
			case FILE_DEBUG:
				depbin = &flat_file->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case GRAPH_ITYPE:{
			FLAT_GRAPH *flat_graph = (FLAT_GRAPH *) item;

			switch (attribute) {
			case GRAPH_HELP:
				depbin = &flat_graph->depbin->db_help;
				break;
			case GRAPH_LABEL:
				depbin = &flat_graph->depbin->db_label;
				break;
			case GRAPH_MEMBERS:
				depbin = &flat_graph->depbin->db_members;
				break;
            case GRAPH_HEIGHT:
				depbin = &flat_graph->depbin->db_height;
				break;
            case GRAPH_WIDTH:
				depbin = &flat_graph->depbin->db_width;
				break;
            case GRAPH_XAXIS:
				depbin = &flat_graph->depbin->db_x_axis;
				break;
            case GRAPH_VALID:
				depbin = &flat_graph->depbin->db_valid;
				break;
			case GRAPH_CYCLETIME:
				depbin = &flat_graph->depbin->db_cycle_time;
				break;
			case GRAPH_DEBUG:
				depbin = &flat_graph->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case GRID_ITYPE:{
			FLAT_GRID *flat_grid = (FLAT_GRID *) item;

			switch (attribute) {
			case GRID_HELP:
				depbin = &flat_grid->depbin->db_help;
				break;
			case GRID_LABEL:
				depbin = &flat_grid->depbin->db_label;
				break;
			case GRID_MEMBERS:
				depbin = &flat_grid->depbin->db_vectors;
				break;
            case GRID_HEIGHT:
				depbin = &flat_grid->depbin->db_height;
				break;
            case GRID_WIDTH:
				depbin = &flat_grid->depbin->db_width;
				break;
            case GRID_ORIENT:
				depbin = &flat_grid->depbin->db_orientation;
				break;
            case GRID_VALID:
				depbin = &flat_grid->depbin->db_valid;
				break;
            case GRID_HANDLING:
				depbin = &flat_grid->depbin->db_handling;
				break;
			case GRID_DEBUG:
				depbin = &flat_grid->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case IMAGE_ITYPE:{
			FLAT_IMAGE *flat_image = (FLAT_IMAGE *) item;

			switch (attribute) {
			case IMAGE_HELP:
				depbin = &flat_image->depbin->db_help;
				break;
			case IMAGE_LABEL:
				depbin = &flat_image->depbin->db_label;
				break;
            case IMAGE_PATH:
			case IMAGE_BINARY:
				depbin = &flat_image->depbin->db_path;
				break;
            case IMAGE_LINK:
				depbin = &flat_image->depbin->db_link;
				break;
            case IMAGE_VALID:
				depbin = &flat_image->depbin->db_valid;
				break;
			case IMAGE_DEBUG:
				depbin = &flat_image->depbin->db_debug_info;
				break;
			default:
				return NULL;

			}
		}
		break;

	case LIST_ITYPE:{
			FLAT_LIST *flat_list = (FLAT_LIST *) item;

			switch (attribute) {
			case LIST_HELP:
				depbin = &flat_list->depbin->db_help;
				break;
			case LIST_LABEL:
				depbin = &flat_list->depbin->db_label;
				break;
			case LIST_VALID:
				depbin = &flat_list->depbin->db_valid;
				break;
			case LIST_TYPE:
				depbin = &flat_list->depbin->db_type;
				break;
			case LIST_COUNT:
				depbin = &flat_list->depbin->db_count;
				break;
			case LIST_COUNT_REF:
				depbin = &flat_list->depbin->db_count;
				break;
			case LIST_CAPACITY:
				depbin = &flat_list->depbin->db_capacity;
				break;
			case LIST_DEBUG:
				depbin = &flat_list->depbin->db_debug_info;
				break;
			default:
				return NULL;
			}
		}
		break;

	case SOURCE_ITYPE:{
			FLAT_SOURCE *flat_source = (FLAT_SOURCE *) item;

			switch (attribute) {
			case SOURCE_HELP:
				depbin = &flat_source->depbin->db_help;
				break;
			case SOURCE_LABEL:
				depbin = &flat_source->depbin->db_label;
				break;
			case SOURCE_MEMBERS:
				depbin = &flat_source->depbin->db_members;
				break;
			case SOURCE_EMPHASIS:
				depbin = &flat_source->depbin->db_emphasis;
				break;
            case SOURCE_LINETYPE:
				depbin = &flat_source->depbin->db_line_type;
				break;
            case SOURCE_LINECOLOR:
				depbin = &flat_source->depbin->db_line_color;
				break;
            case SOURCE_YAXIS:
				depbin = &flat_source->depbin->db_y_axis;
				break;
            case SOURCE_VALID:
				depbin = &flat_source->depbin->db_valid;
				break;
			case SOURCE_DEBUG:
				depbin = &flat_source->depbin->db_debug_info;
				break;
			case SOURCE_INIT_ACTIONS:
				depbin = &flat_source->depbin->db_init_actions;
				break;
			case SOURCE_RFRSH_ACTIONS:
				depbin = &flat_source->depbin->db_refresh_actions;
				break;
			case SOURCE_EXIT_ACTIONS:
				depbin = &flat_source->depbin->db_exit_actions;
				break;
			default:
				return NULL;

			}
		}
		break;

	case WAVEFORM_ITYPE:{
			FLAT_WAVEFORM *flat_waveform = (FLAT_WAVEFORM *) item;

			switch (attribute) {
			case WAVEFORM_HELP:
				depbin = &flat_waveform->depbin->db_help;
				break;
			case WAVEFORM_LABEL:
				depbin = &flat_waveform->depbin->db_label;
				break;
			case WAVEFORM_EMPHASIS:
				depbin = &flat_waveform->depbin->db_emphasis;
				break;
            case WAVEFORM_LINETYPE:
				depbin = &flat_waveform->depbin->db_line_type;
				break;
            case WAVEFORM_LINECOLOR:
				depbin = &flat_waveform->depbin->db_line_color;
				break;
           case WAVEFORM_TYPE:
				depbin = &flat_waveform->depbin->db_type;
				break;
           case WAVEFORM_INIT_ACTIONS:
				depbin = &flat_waveform->depbin->db_init_actions;
				break;
           case WAVEFORM_RFRSH_ACTIONS:
				depbin = &flat_waveform->depbin->db_refresh_actions;
				break;
           case WAVEFORM_EXIT_ACTIONS:
				depbin = &flat_waveform->depbin->db_exit_actions;
				break;
           case WAVEFORM_X_INITIAL:
				depbin = &flat_waveform->depbin->db_x_initial;
				break;
           case WAVEFORM_X_INCREMENT:
				depbin = &flat_waveform->depbin->db_x_increment;
				break;
           case WAVEFORM_POINT_COUNT:
				depbin = &flat_waveform->depbin->db_number_of_points;
				break;
           case WAVEFORM_Y_VALUES:
				depbin = &flat_waveform->depbin->db_y_values;
				break;
           case WAVEFORM_X_VALUES:
				depbin = &flat_waveform->depbin->db_x_values;
				break;
           case WAVEFORM_KEYPTS_Y:
				depbin = &flat_waveform->depbin->db_key_y_values;
				break;
           case WAVEFORM_KEYPTS_X:
				depbin = &flat_waveform->depbin->db_key_x_values;
				break;
           case WAVEFORM_YAXIS:
				depbin = &flat_waveform->depbin->db_y_axis;
				break;
           case WAVEFORM_HANDLING:
				depbin = &flat_waveform->depbin->db_handling;
				break;
		   case WAVEFORM_DEBUG:
				depbin = &flat_waveform->depbin->db_debug_info;
				break;
			case WAVEFORM_VALID:
				depbin = &flat_waveform->depbin->db_valid;
				break;
		   default:
				return NULL;

			}
		}
		break;

	default:
		return NULL;
	}

	return depbin;

}


/*********************************************************************
 *
 *	Name: bin_saver
 *	ShortDesc: make the depbin and binary chunks persistent
 *
 *	Description:
 *		bin_saver mallocs space for DEPBINs and binary chunks and
 *		copies them into the new space and loads the address of
 *		the malloced list into the depbin list
 *
 *	Inputs:
 *		item_type:	type of item
 *		dynamic:	the mask for which attribute binary chunks to make persistent
 *
 *	Outputs:
 *		item:	the item flat with the malloced binarys hooked up
 *
 *	Returns: DDI_MEMORY_ERROR, DDS_SUCCESS, DDI_INVALID_PARAM and
 *		returns from other DDS functions.
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int
bin_saver(
ITEM_TYPE       item_type,
void           *item,
unsigned long   new_dynamic,
unsigned long   old_dynamic)
{

	DEPBIN        **depbin_ptr;	/* pointer to a DEPBIN pointer */
	DEPBIN         *depbin;	/* pointer to a DEPBIN */
	unsigned long   mask = 0X01;	/* mask for checking item attributes */
	char           *mem_ptr;/* allocated memory pointer */
	int             rc;	/* return code */


	new_dynamic ^= old_dynamic;	/* shut off the old_dynamic bits */


	/* check to see if depbin list needs to be allocated */

	if (item_type == VARIABLE_ITYPE) {

		if ((new_dynamic & VAR_MAIN_MASKS) && !(old_dynamic & VAR_MAIN_MASKS)) {

			rc = malloc_depbin_list(item_type, item, VAR_MAIN);
			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}

		if ((new_dynamic & VAR_ACT_MASKS) && !(old_dynamic & VAR_ACT_MASKS)) {

			rc = malloc_depbin_list(item_type, item, VAR_ACT);
			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}

		if ((new_dynamic & VAR_MISC_MASKS) && !(old_dynamic & VAR_MISC_MASKS)) {

			rc = malloc_depbin_list(item_type, item, VAR_MISC);
			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}
	}
	else {

		if ((new_dynamic) && (!old_dynamic)) {

			rc = malloc_depbin_list(item_type, item, 0);
			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}
	}

	/*
	 * Malloc the depbins for the dynamic attributes
	 */

	while (new_dynamic) {

		if (new_dynamic & mask) {

			depbin_ptr = get_depbin_ptr(item_type, item, mask);
			if ((depbin_ptr == NULL) || (*depbin_ptr == NULL)) {

				return DDI_INVALID_PARAM;
			}

			depbin = *depbin_ptr;

			/*
			 * malloc DEPBIN struct and bin_size contiguously
			 */

			mem_ptr = (char *) malloc((size_t) (sizeof(DEPBIN) + depbin->bin_size));
			if (mem_ptr == NULL) {

				return DDI_MEMORY_ERROR;
			}

			/*
			 * memcpy DEPBIN struct and bin_size contiguously
			 */

			memcpy((char *) mem_ptr, (char *) depbin, (size_t) sizeof(DEPBIN));
			memcpy((char *) (mem_ptr + sizeof(DEPBIN)), (char *) depbin->bin_chunk, (size_t) depbin->bin_size);

			*depbin_ptr = (DEPBIN *) mem_ptr;
			(*depbin_ptr)->bin_chunk = (unsigned char *) (mem_ptr + sizeof(DEPBIN));

			new_dynamic &= (~mask);	/* turn off the bit */
		}

		mask <<= 1;	/* shift the mask */
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: clean_item
 *	ShortDesc: clean the depbin list of scratchpad bin chunks
 *
 *	Description:
 *		clean_item will clean the depbin list of all scratchpad
 *		pointers and adjust the bin_hooked masks to reflect the
 *		current state of the flat structure
 *
 *	Inputs:
 *		item_type:	type of item
 *		masks:		a pointer to the flat structure's masks
 *
 *	Outputs:
 *		item:	a pointer to the item flat structure with the cleaned out list
 *
 *	Returns: DDI_INVALID_FLAT_FORMAT, DDS_SUCCESS and returns from
 *		other DDS functions.
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int
clean_item(
ITEM_TYPE       item_type,
void           *item,
FLAT_MASKS     *masks)
{

	DEPBIN        **depbin_ptr;	/* pointer to a depbin pointer */
	void          **depbins[VAR_SUBS_COUNT];	/* array for depbin list
							 * pointers */
	unsigned long   clean;	/* attributes to clean */
	unsigned long   mask = 0X01;	/* mask for a single attribute */
	int             rc;	/* return code */


	/* check to see if VAR depbin lists need to be NULLed */

	if (item_type == VARIABLE_ITYPE) {

		rc = get_depbin_list_ptrs(item_type, item, depbins);
		if (rc != DDS_SUCCESS) {

			return rc;
		}

		if (!(masks->dynamic & VAR_MAIN_MASKS)) {

			*depbins[VAR_MAIN] = NULL;

			/* turn off the bin_hooked masks */
			masks->bin_hooked &= (~VAR_MAIN_MASKS);
		}

		if (depbins[VAR_ACT]) {
			if (!(masks->dynamic & VAR_ACT_MASKS)) {

				*depbins[VAR_ACT] = NULL;

				/* turn off the bin_hooked masks */
				masks->bin_hooked &= (~VAR_ACT_MASKS);
			}
		}

		if (depbins[VAR_MISC]) {
			if (!(masks->dynamic & VAR_MISC_MASKS)) {

				*depbins[VAR_MISC] = NULL;

				/* turn off the bin_hooked masks */
				masks->bin_hooked &= (~VAR_MISC_MASKS);
			}
		}
	}


	clean = masks->bin_hooked ^ masks->dynamic;

	while (clean) 
	{
		if (clean & mask) 
		{
			depbin_ptr = get_depbin_ptr(item_type, item, mask);

			if (depbin_ptr == NULL) 
			{
				return DDI_INVALID_FLAT_FORMAT;
			}

			*depbin_ptr = NULL;

			clean &= (~mask);	/* turn off the bits */
		}

		mask <<= 1;	/* shift the mask */
	}

	masks->bin_hooked = masks->dynamic;

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: make_generic_item()
 *
 *	ShortDesc: create if necessary the DDL item and hook it to the
 *             generic item
 *
 *	Description:
 *      make_generic_item() will malloc and attach the memory necessary
 *      for the DD item which is to be fetched and evaluated by ddi_get_item()
 *
 *	Inputs:
 *
 *		generic_item:		the pointer to the generic item structure
 *
 *      attr_request:       the attr_request mask which indicates if the
 *                          variable substructures need to be allocated
 *                          and attached to the VARIABLE_ITEM struct                    
 *
 *	Outputs:
 *
 *		item:	the generic item with base item allocated
 *
 *	Returns:	DDI_MEMORY_ERROR, DDS_SUCCESS
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

static
int 
make_generic_item(
DDI_GENERIC_ITEM *generic_item,
unsigned long   attr_request)
{

	void	**tmp_item;


	tmp_item = &generic_item->item;

	/*
     * if this is a call for reevaluation
	 * initialize the errors list, if this is
     * not a call back we assume the error list
     * has been initialized by ddi_clean_item()
     * or by the caller
	 */


	if (*tmp_item) {

		memset((char *) &generic_item->errors, 0, sizeof(generic_item->errors));
	}


	/*
	 * handle the special case of the ddl item variable structure
	 */

	if (generic_item->item_type == VARIABLE_ITYPE) {


		if (*tmp_item == NULL) {

			*tmp_item = calloc(1, sizeof(FLAT_VAR));
			if (*tmp_item == NULL) {

				return DDI_MEMORY_ERROR;
			}
		}


		/*
		 * Malloc the Actions and Misc attribute lists if the item is a
		 * variable and the attr_request mask is set for either list
		 */

		{
			FLAT_VAR *var = (FLAT_VAR *) *tmp_item;

			if ((attr_request & VAR_ACT_MASKS) && (var->actions == NULL)) {

				var->actions = (FLAT_VAR_ACTIONS *) calloc(1, sizeof(FLAT_VAR_ACTIONS));
				if (var->actions == NULL) {

					return DDI_MEMORY_ERROR;
				}
			}

			if ((attr_request & VAR_MISC_MASKS) && (var->misc == NULL)) {

				var->misc = (FLAT_VAR_MISC *) calloc(1, sizeof(FLAT_VAR_MISC));
				if (var->misc == NULL) {

					return DDI_MEMORY_ERROR;
				}
			}
		}

		return DDS_SUCCESS;
	}


	if (*tmp_item) {

		/*
		 * This item has already been malloced
		 */
		return DDS_SUCCESS;

	}


	switch (generic_item->item_type) {

	case MENU_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_MENU));
		break;

	case EDIT_DISP_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_EDIT_DISPLAY));
		break;

	case METHOD_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_METHOD));
		break;

	case REFRESH_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_REFRESH));
		break;

	case UNIT_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_UNIT));
		break;

	case WAO_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_WAO));
		break;

	case ITEM_ARRAY_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_ITEM_ARRAY));
		break;

	case COLLECTION_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_COLLECTION));
		break;

	case BLOCK_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_BLOCK));
		break;

	case PROGRAM_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_PROGRAM));
		break;

	case RECORD_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_RECORD));
		break;

	case ARRAY_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_ARRAY));
		break;

	case VAR_LIST_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_VAR_LIST));
		break;

	case RESP_CODES_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_RESP_CODE));
		break;

	case DOMAIN_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_DOMAIN));
		break;

#ifdef HART
	case COMMAND_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_COMMAND));
		break;
#endif

	case AXIS_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_AXIS));
		break;

	case CHART_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_CHART));
		break;

	case FILE_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_FILE));
		break;

	case GRAPH_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_GRAPH));
		break;

	case GRID_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_GRID));
		break;

	case IMAGE_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_IMAGE));
		break;

	case LIST_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_LIST));
		break;

	case SOURCE_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_SOURCE));
		break;

	case WAVEFORM_ITYPE:
		*tmp_item = calloc(1, sizeof(FLAT_WAVEFORM));
		break;

	default:
		return DDI_INVALID_PARAM;
	}

	if (*tmp_item == NULL) {

		return DDI_MEMORY_ERROR;
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_item()
 *
 *	ShortDesc: Gets the requested item and returns it to the caller
 *
 *	Description:
 *		ddi_get_item fetches an item from either a ROD or a FLAT, calls
 *		eval if necessary to evaluate the values and copies the dynamic
 *		attributes binary into malloced memory for possible future evaluation
 *		by the calling application
 *
 *		If the application calls ddi_get_item() with a generic_item which
 *		has already been created and filled with DD information by
 *		ddi_get_item(), this is known as calling in for reevaluation.
 *		When the application calls ddi_get_item() for the reevaluation
 *		of an item, ddi_get_item() does not use the DDI_ITEM_SPECIFIER
 *		input structure as the specification for the item, instead
 *		it uses	the item_id and the item_type which are part of the previously
 *		evaluated and DDI_GENERIC_ITEM structure.
 *
 *	Inputs:
 *
 *      block_spec: specifies the block with either a BLOCK_TAG or BLOCK_HANDLE
 *
 *		item_spec:	specifies the item being requested
 *
 *		env_info:	the application's environment information
 *
 *      req_mask: the mask which specifies the item attributes
 *                the caller wants ddi_get_item() to fetch and evaluate
 *                for the requested item
 *
 *	Outputs:
 *
 *		item:		the generic DDI item
 *
 *
 *	Returns: DDI_INVALID_PARAM, DDI_INVALID_FLAT_FORMAT,
 *		DDI_BAD_DD_BLOCK_LOAD, and returns from other DDS functions.
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

/*
 * the stack scratch pad size used by ddi_get_item
 */

#define PAD_SIZE 512


int
ddi_get_item(
DDI_BLOCK_SPECIFIER *block_spec,
DDI_ITEM_SPECIFIER  *item_spec,
ENV_INFO            *env_info,
DDI_ATTR_REQUEST     req_mask,
DDI_GENERIC_ITEM    *generic_item)
{

	int             rc;	/* return code */
	int             return_code;	/* saved return code from eval_item() call */
	int             inc;	/* incrementer */

	BLOCK_HANDLE    block_handle;	/* handle for the block */
	DD_REFERENCE    dd_ref;	/* device description reference */
	ROD_HANDLE      rod_handle = 0;	/* device description handle */

	FLAT_MASKS     *masks;	/* pointer to the masks of the flat item */
	unsigned long   old_dynamic;	/* state of the dynamic mask before
					 * eval */

	void          **depbins[VAR_SUBS_COUNT];	/* array for holding
							 * depbin list pointers */

	SCRATCH_PAD     scratch;/* memory structure for a fetch request */
	unsigned long   add_size;	/* additional size returned by fetch
					 * request */
	unsigned char  *pad_2;	/* pointer to the malloced scratchpad */

    unsigned char   pad_1[PAD_SIZE]{0};	/* the stack scratchpad */

	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */

	ITEM_TYPE       item_type;	/* type of the item being requested */
	void           *item;	/* pointer to the item inside of generic_item */
	DDI_ITEM_SPECIFIER local_ispec;	/* local copy of the item specifier */
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr* pConnMgr = pDDSSupport->GetConnectionManager();

	ASSERT_RET(block_spec && item_spec && env_info && generic_item, DDI_INVALID_PARAM);

	/*
	 * if the generic item has an item hooked up, check to see that type exists
	 */

	if (generic_item->item) { 

		ASSERT_RET(generic_item->item_type, DDI_INVALID_PARAM);
	}



	return_code = DDS_SUCCESS;



	memset((char *) &dd_ref, 0, sizeof(DD_REFERENCE));
	
	/*
	 * We put the scratch pad on the stack with the hope that 90% of the
	 * time it's big enough for fetch. If it isn't, we malloc the
	 * additional memory fetch requires and return for a second round of
	 * fetching.
	 */

	/* initialize the scratch pad */

	scratch.used = 0;
	scratch.size = PAD_SIZE;
	scratch.pad = pad_1;

	pad_2 = NULL;



	/*
	 * convert the block specifier to a block handle
	 */

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}
	else {

		/*
		 * load environment information with block handle
		 */

		env_info->block_handle = block_handle;
	}

	/*
	 * convert block handle to a block table element
	 */

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * convert block handle to a dd handle
	 */

	rc = pConnMgr->get_abt_dd_handle(block_handle, &rod_handle);
	if (rc != SUCCESS) {
		return (rc);
	}


	/*
	 * If this is a call back for the reevaluation of an item, force the item
	 * specifier to be the item ID contained by the ddl Item structure.
     * We're doing this for speed reasons, item_id to dd reference is fast
     * in the tables. This also resolves the problem of a call to ddi_get_item()
     * which includes a previously loaded GENERIC_ITEM structure but a
     * DDI_ITEM_SPECIFIER structure which specifies a different item then the
     * the item loaded in the GENERIC_ITEM. If there's a conflict, we ignore
     * it and simply reevaluate the item loaded in the GENERIC_ITEM
	 */


	if (generic_item->item) {

#ifdef DEBUG
		dds_item_flat_check(generic_item->item, generic_item->item_type);
#endif
		local_ispec.type = DDI_ITEM_ID;
		local_ispec.item.id = get_item_id(generic_item->item_type, generic_item->item);
	}
	else {

	/*
	 * make a local copy of item_spec
	 */

		(void) memcpy((char *) &local_ispec, (char *) item_spec, sizeof(DDI_ITEM_SPECIFIER));
	}


	/*
	 * convert item_spec to a dd reference and item type
	 */

	rc = convert_item_spec(env_info, &local_ispec, block_handle, bt_elem, &dd_ref, &item_type);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_item_spec fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnMgr->ds_dd_block_load(env_info, block_handle, &rod_handle, 1);
			if (!rc) {

				rc = convert_item_spec(env_info, &local_ispec, block_handle, bt_elem, &dd_ref, &item_type);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	/*
	 * Scratch pad is set up, we have our Block Handle we have a Device
	 * Description reference for the item, we're ready to allocate the item
	 * and call fetch and eval
	 */

	/*
        * We call make_generic_item() even if the generic_item.item
        * has been allocated. This is for two reasons. First, make_generic_item()
        * zeros out the return list. Second, make_generic_item() may need to
        * allocate substructuress below the FLAT_VAR structure, i.e. FLAT_VAR_MISC
        * or FLAT_VAR_ACTIONS.
		*/

	generic_item->item_type = item_type;
	rc = make_generic_item(generic_item, req_mask);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
		* set the temp item pointer
		*/

	item = generic_item->item;

	/*
		* get the pre-eval masks to save for compares later
		*/

	masks = get_masks_ptr(item_type, item);
	old_dynamic = masks->dynamic;

	/*
		* sanity check: if there are bin_hooked bits set which don't
		* match the dynamic mask bits, this depbin list was not
		* created by DDI
		*/

	if (masks->bin_hooked ^ masks->dynamic) {

		return DDI_INVALID_FLAT_FORMAT;
	}


	bool bListCountAdded	= false;
	bool bAxisMinValueAdded = false;
	bool bAxisMaxValueAdded = false;
	bool bImagePathAdded	= false;

	if(item_type == LIST_ITYPE)
	{
		if( req_mask & LIST_COUNT_REF)
		{
			if(!(req_mask & LIST_COUNT))
			{
				req_mask += LIST_COUNT;
				bListCountAdded = true;
			}
		}
	}

	if(item_type == AXIS_ITYPE)
	{
		if( req_mask & AXIS_MIN_VAL_REF)
		{
			if(!(req_mask & AXIS_MINVAL))
			{
				req_mask += AXIS_MINVAL;
				bAxisMinValueAdded = true;
			}
		}

		if( req_mask & AXIS_MAX_VAL_REF)
		{
			if(!(req_mask & AXIS_MAXVAL))
			{
				req_mask += AXIS_MAXVAL;
				bAxisMaxValueAdded = true;
			}
		}
	}

	if(item_type == IMAGE_ITYPE)
	{
		if( req_mask & IMAGE_BINARY)
		{
			if(!(req_mask & IMAGE_PATH))
			{
				req_mask += IMAGE_PATH;
				bImagePathAdded = true;
			}
		}
	}
	/* call fetch item */

	rc = fch_rod_item(env_info, rod_handle, dd_ref.object_index,
		&scratch, &add_size, req_mask, item, item_type);

	if (rc != DDS_SUCCESS) {

		if (rc == FETCH_INSUFFICIENT_SCRATCHPAD) {

			pad_2 = (unsigned char *) malloc((size_t) (add_size) * sizeof(unsigned char));
			if (pad_2 == NULL) {

				return DDI_MEMORY_ERROR;
			}

			scratch.used = 0;
			scratch.size = add_size;
			scratch.pad = pad_2;

			rc = fch_rod_item(env_info, rod_handle, dd_ref.object_index,
				&scratch, &add_size, req_mask, item, item_type);

			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}
		else {

			return rc;
		}
	}

#ifndef _TEST_TOKENIZER_	// If we aren't testing the tokenizers, then go ahead and eval.

	if(bListCountAdded)
	{
		req_mask -= LIST_COUNT;
	}

	if(bAxisMinValueAdded)
	{
		req_mask -= AXIS_MINVAL;
	}

	if(bAxisMaxValueAdded)
	{
		req_mask -= AXIS_MAXVAL;
	}

	if(bImagePathAdded)
	{
		req_mask -= IMAGE_PATH;
	}

	/* call eval */
	rc = eval_item(item, req_mask, env_info, &generic_item->errors, item_type);
	if (rc != DDS_SUCCESS) {

		return_code = rc;	/* save the return code */
	}

#else	// Make it think that all are dynamic so that the bins will be saved.

	masks->dynamic = masks->bin_hooked;

#endif

	if (masks->dynamic == 0) {

		/*
			* if there are no dynamics
			*/

		rc = get_depbin_list_ptrs(item_type, item, depbins);
		if (rc != DDS_SUCCESS) {

			return rc;
		}

		for (inc = 0; inc < VAR_SUBS_COUNT; inc++) {

			if (depbins[inc]) {
				*depbins[inc] = NULL;
			}
		}
		masks->bin_hooked = 0;

	}
	else if (masks->dynamic == old_dynamic) {

		/*
			* The dynamics have not changed
			*/

		rc = clean_item(item_type, item, masks);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
	}
	else {

		/*
			* New binarys need to be added
			*/


		rc = bin_saver(item_type, item, masks->dynamic, old_dynamic);
		if (rc != DDS_SUCCESS) {

			return rc;
		}

		rc = clean_item(item_type, item, masks);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
	}

	if (pad_2) {	/* if second scratch pad was allocated */

		free((void *) pad_2);
	}

#ifdef DEBUG
	dds_item_flat_check(generic_item->item, generic_item->item_type);
#endif
	return return_code;
}



/*********************************************************************
 *
 *	Name: ddi_clean_item
 *
 *	ShortDesc: frees memory allocated to the generic item structure
 *
 *	Description:
 *	           ddi_clean_item() frees all memory allocated to the
 *             GENERIC_ITEM item void pointer as well as resetting the
 *             GENERIC_ITEM structure to zero.
 *
 *	Inputs:
 *		item:	a pointer to a generic item
 *
 *	Outputs:
 *		item:   the cleaned generic item structure
 *
 *	Returns:	DDI_INVALID_FLAT_FORMAT, DDI_INVALID_FLAT_MASKS, DDS_SUCCESS
 *		and returns from other DDS functions.
 *
 *	Author: Christian Gustafson
 *
 **********************************************************************/

int
ddi_clean_item(DDI_GENERIC_ITEM *generic_item)
{

	int             rc;			/* return code */
	int             inc;	    /* increment */
	FLAT_MASKS     *masks;	    /* masks pointer */
	DEPBIN        **depbin_ptr;	/* pointer to a DEPBIN structure */
	void          **depbins[VAR_SUBS_COUNT];	/* array for holding
												 * depbin list pointers */

	ITEM_TYPE       item_type;	/* the type of the item */
	void           *item;	    /* a pointer to the item */
	unsigned long   kfree;	    /* attributes which have depbins which need freeing */
	unsigned long   mask = 0X01;/* temp attribute bit mask */



	ASSERT_RET(generic_item, DDI_INVALID_PARAM);

	/*
	 * if the void item pointer is NULL,
     * reset the structure to zero and return
	 */

	if (generic_item->item == NULL) {

		memset((char *) generic_item, 0, sizeof(*generic_item));
		return DDS_SUCCESS;		
	}
		

	item_type = generic_item->item_type;
	item = generic_item->item;


#ifdef DEBUG
	dds_item_flat_check(item, item_type);
#endif

	/*
	 * Get the flat item masks
	 */

	masks = get_masks_ptr(item_type, item);
	if (masks == NULL) {

		return DDI_INVALID_PARAM;
	}

	/*
	 * sanity check: if there are bin_hooked bits set which don't match the
	 * dynamic mask bits, this depbin list was not created by DDI
	 */

	if (masks->bin_hooked ^ masks->dynamic) {

		return DDI_INVALID_FLAT_FORMAT;
	}


	/*
	 * free the flat values and depinfos
	 */

	eval_clean_item(item, item_type);


	kfree = masks->bin_hooked;

	/*
	 * free the binary chunks
	 */

	while (kfree) {

		if (kfree & mask) {

			depbin_ptr = get_depbin_ptr(item_type, item, mask);
			if (depbin_ptr == NULL) {

				return DDI_INVALID_FLAT_MASKS;
			}

			/*
			 * freeing the depbin structure frees the bin chunk at
			 * the same time because they were allocated with one
			 * call to malloc
			 */

			free((void *) *depbin_ptr);

			kfree &= (~mask);	/* turn off the bit */
		}

		mask <<= 1;	/* shift the mask */
	}


	/*
	 * Free the depbin lists
	 */

	rc = get_depbin_list_ptrs(item_type, item, depbins);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	for (inc = 0; inc < VAR_SUBS_COUNT; inc++) {

		if ((depbins[inc]) && (*depbins[inc])) {

			free((void *) *depbins[inc]);
			*depbins[inc] = NULL;
		}
	}

	/*
	 * Free the Actions and Misc attribute lists if the item is a variable
	 * and the lists are hooked up
	 */

	if (item_type == VARIABLE_ITYPE) {

		FLAT_VAR       *flat_var = (FLAT_VAR *) item;

		if (flat_var->misc != NULL) {

			free((void *) flat_var->misc);
		}
		if (flat_var->actions != NULL) {

			free((void *) flat_var->actions);
		}
	}
	
	for( int i = 0; i < generic_item->errors.count; i++ )	// Clean out any op_info_lists
	{
		if( generic_item->errors.list[i].var_needed.op_ref_type == COMPLEX_TYPE )
		{
			ddl_free_op_ref_info_list( &generic_item->errors.list[i].var_needed.op_info_list );
		}
	}

	free((void *) item);

	memset((char *) generic_item, 0, sizeof(DDI_GENERIC_ITEM));

	return DDS_SUCCESS;
}
