/**
 *	@(#) $Id: evl_tran.cpp,v 1.2 2012/09/24 23:54:37 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  TRANSACTION, DATA_ITEM_LIST, and DATA_ITEM..
 */


#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif

extern int
ddl_actions_choice(
unsigned char **chunkp,
DDL_UINT       *size,
ACTION_LIST	   *pActionList,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed);

/*********************************************************************
 *  Name: ddl_shrink_dataitems_list
 *  ShortDesc: Shrink a DATA_ITEM_LIST.
 *
 *  Description:
 *      ddl_shrink_dataitems_list reallocates the DATA_ITEM_LIST to only
 *		contain space for the DATA_ITEM's currently being used
 *
 *  Inputs:
 *      items:   a pointer to the DATA_ITEM_LIST
 *
 *  Outputs:
 *      items:   a pointer to the condensed DATA_ITEM_LIST
 *
 *  Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *  Author: steve beyerl
 ***************************************************************************/
int
ddl_shrink_dataitems_list(
DATA_ITEM_LIST *items)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_DATAITEMS_LIST);
#endif

	if (!items) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (!items->list) {
		ASSERT_DBG(!items->count && !items->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (items->count == items->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (items->count == 0) {
		ddl_free_dataitems(items, FREE_ATTR);
	}
	else {
		items->limit = items->count;

		items->list = (DATA_ITEM *) realloc((void *) items->list,
			(size_t) (items->limit * sizeof(DATA_ITEM)));
		if (!items->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_dataitems
 *	ShortDesc: Free the list of DATA_ITEMs
 *
 *	Description:
 *		ddl_free_dataitems will free the list of DATA_ITEMs in the
 *		DATA_ITEM_LIST structure.
 *
 *	Inputs:
 *		data_items: pointer to the DATA_ITEM_LIST structure.
 *
 *	Outputs:
 *		data_items: pointer to an empty DATA_ITEM_LIST structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/

void
ddl_free_dataitems(
DATA_ITEM_LIST *data_items,
uchar           dest_flag)
{

	int             i;
	DATA_ITEM      *temp_list;

	if (data_items == NULL) {
		return;
	}

	if (data_items->list == NULL) {
		ASSERT_DBG(!data_items->count && !data_items->limit);
		data_items->limit = 0;
	}
	else {

		temp_list = data_items->list;
		for (i = 0; i < data_items->count; temp_list++, i++) {
			if (temp_list->type == DATA_REFERENCE) 
            {
				ddl_free_op_ref_trail(&temp_list->data.ref);
			}
            if (temp_list->data.ref.op_ref_type == COMPLEX_TYPE)
			{
				ddl_free_op_ref_info_list( &temp_list->data.ref.op_info_list );
			}
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of DATA_ITEMs
			 */

			free((void *) data_items->list);
			data_items->list = NULL;
			data_items->limit = 0;
		}
	}

	data_items->count = 0;
}


/***************************************************************
 *	Name:	ddl_mask_width
 *	ShortDesc:	Counts the number of bits set in mask.
 *
 *	Description:
 *		Counts the number of bits set in mask.
 *
 *	Inputs:
 *		mask -	Entity about which we want to know how many bits
 *				are set.
 *
 *	Outputs:	None.
 *
 *	Returns:	Number of bits set in mask.
 *
 *	Author: Steve Beyerl
 *
 ***************************************************************/
static          DDL_UINT
ddl_mask_width(
DDL_UINT_LONG        mask)
{
	DDL_UINT        count;
	DDL_UINT_LONG        bitmask;

	for (bitmask = 1; bitmask; bitmask <<= 1) {
		if (mask & bitmask) {
			break;
		}
	}

	for (count = 0; bitmask; bitmask <<= 1, count++) {
		if (!(mask & bitmask)) {
			break;
		}
	}

	return count;
}


/***************************************************************
 *	Name:	ddl_parse_dataitems
 *	ShortDesc:	Parses a list of DATA_ITEMS from binary.
 *
 *	Description:
 *		ddl_parse_dataitems will parse a binary chunk and load the
 *		DATA_ITEM_LIST and/or load the dependency information into
 *		the nsEDDEngine::OP_REF_LIST.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      ref - pointer to an ITEM_ID
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      ref - pointer to an ITEM_ID
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			DDL_PARSE_TAG()
 *			DDL_PARSE_INTEGER()
 *			ddl_parse_op_ref_trail()
 *			ddl_parse_bitstring()
 *			ddl_parse_float()
 *
 *	Author: Steve Beyerl
 *
 ***************************************************************/
static int
ddl_parse_dataitems(
unsigned char **chunkp,
DDL_UINT       *size,
DATA_ITEM_LIST *items,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        i;	/* temp storage for a parsed integer */
	DDL_UINT        tag;	/* temp storage of a parsed tag */
	DATA_ITEM      *tmp = NULL;	/* temp ptr to a list of data items */
	float           fvalue;	/* temp storage for a parsed float */

	DDL_UINT        tmp_flags;	/* temp storage for a parsed bitstring */

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_DATAITEMS);
#endif


	while (*size > 0) {

		/*
		 * Parse a series of DATA_ITEMs.  If we need more room in the
		 * array of structures, malloc more room.  Then parse the next
		 * DATA_ITEM structure.
		 */

		if (items) {
			if (items->count >= items->limit) {
				items->limit += DATA_ITEM_LIST_INCSZ;
				tmp = (DATA_ITEM *) realloc((void *) items->list,
					(size_t) (items->limit * sizeof *tmp));
				if (!tmp) {
					items->limit = items->count;
					ddl_free_dataitems(items, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				items->list = tmp;
				memset((char *) &items->list[items->count], 0,
					DATA_ITEM_LIST_INCSZ * sizeof *items->list);
			}
			tmp = items->list + items->count++;
		}

		/*
		 * Parse the tag to see what kind of DATA_ITEM this is.
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		if(tag!=DATA_ELEMENT_TAG)
			return DDL_ENCODING_ERROR;

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		switch (tag) {
		case DATA_CONSTANT:

			/*
			 * A constant data item.  Parse the constant value.
			 * Note the call to free and subreferences chained to
			 * this structure.  This is necessary, since we may be
			 * re-using this structure, and we need to clean up any
			 * previously allocated memory.
			 */

			DDL_PARSE_INTEGER(chunkp, size, &i);

			if (items) {
				tmp->type = DATA_CONSTANT;
				tmp->flags = 0;
				tmp->data.iconst = i;
				tmp->width = 0;
				tmp->data_item_mask = 0;
			}
			break;

		case DATA_REFERENCE:

			/*
			 * A data reference.  Parse the reference.
			 */

			if (items) {
				tmp->type = DATA_REFERENCE;
				tmp->flags = 0;
				tmp->width = 0;
				tmp->data_item_mask = 0;

				rc = ddl_parse_op_ref_trail(chunkp, size, &tmp->data.ref,
					depinfo, env_info, var_needed, true);

				if (rc != DDL_SUCCESS) {
					return rc;
				}

				switch (tmp->data.ref.op_info.type)
				{
				case LIST_ITYPE:
					break;
				case FILE_ITYPE:
					rc = SM_UNSUPPORTED_FEATURE_IN_DD;
					EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"FDI_DDS", L"FILE is used in COMMAND");
					return rc;
				default:
					break;
				}
			}
			else {
				rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			break;

		case DATA_REF_FLAGS:

			/*
			 * A data reference with INFO or INDEX flag bits
			 */

			if (items) {
				tmp->type = DATA_REFERENCE;
				tmp->width = 0;
				tmp->data_item_mask = 0;

				rc = ddl_parse_op_ref_trail(chunkp, size, &tmp->data.ref,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				switch (tmp->data.ref.op_info.type)
				{
				case ARRAY_ITYPE:
				case LIST_ITYPE:
					break;
				case FILE_ITYPE:
					rc = SM_UNSUPPORTED_FEATURE_IN_DD;
					EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"FDI_DDS", L"FILE is used in COMMAND");
					return rc;
				default:
					break;
				}

				rc = ddl_parse_bitstring(chunkp, size, &tmp_flags);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				tmp->flags = (unsigned short) tmp_flags;
			}
			else {
				rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				rc = ddl_parse_bitstring(chunkp, size, (DDL_UINT *) NULL);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			break;

		case DATA_REF_WIDTH:

			/*
			 * A data reference with a data mask.
			 */

			if (items) {
				tmp->type = DATA_REFERENCE;
				tmp->flags = WIDTH_PRESENT;

				rc = ddl_parse_op_ref_trail(chunkp, size, &tmp->data.ref,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				switch (tmp->data.ref.op_info.type)
				{
				case ARRAY_ITYPE:
				case LIST_ITYPE:
					break;
				case FILE_ITYPE:
					rc = SM_UNSUPPORTED_FEATURE_IN_DD;
					EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"FDI_DDS", L"FILE is used in COMMAND");
					return rc;
				default:
					break;
				}

				// Get the data item mask.
				DDL_PARSE_INTEGER_LONG(chunkp, size, &tmp->data_item_mask);

				tmp->width = (unsigned short) ddl_mask_width(tmp->data_item_mask);
			}
			else {
				rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);
			}

			break;

		case DATA_REF_FLAGS_WIDTH:

			/*
			 * A data reference INFO or INDEX flag bits and a data
			 * mask
			 */

			if (items) {
				tmp->type = DATA_REFERENCE;

				rc = ddl_parse_op_ref_trail(chunkp, size, &tmp->data.ref,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				switch (tmp->data.ref.op_info.type)
				{
				case ARRAY_ITYPE:
				case LIST_ITYPE:
					break;
				case FILE_ITYPE:
					rc = SM_UNSUPPORTED_FEATURE_IN_DD;
					EddEngineLog(env_info, __FILE__, __LINE__, BssWarning, L"FDI_DDS", L"FILE is used in COMMAND");
					return rc;
				default:
					break;
				}

				// Get the data item mask.
				DDL_PARSE_INTEGER_LONG(chunkp, size, &tmp->data_item_mask);

				tmp->width = (unsigned short) ddl_mask_width(tmp->data_item_mask);

				// Get the flags.
				rc = ddl_parse_bitstring(chunkp, size, &tmp_flags);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				tmp->flags = (unsigned short) tmp_flags;
				tmp->flags |= WIDTH_PRESENT;
			}
			else {
				rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
					depinfo, env_info, var_needed, true);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				DDL_PARSE_INTEGER(chunkp, size, (DDL_UINT *) NULL_PTR);

				rc = ddl_parse_bitstring(chunkp, size, (DDL_UINT *) NULL);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			break;

		case DATA_FLOATING:

			/*
			 * A floating point data item.  Parse the data value.
			 * Note the call to free and subreferences chained to
			 * this structure.  This is necessary, since we may be
			 * re-using this structure, and we need to clean up any
			 * previously allocated memory.
			 */

			rc = ddl_parse_float(chunkp, size, &fvalue);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (items) {
				tmp->type = DATA_FLOATING;
				tmp->flags = 0;
				tmp->data.fconst = fvalue;
				tmp->width = 0;
				tmp->data_item_mask = 0;
			}
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_dataitems_choice
 *	ShortDesc: Choose the correct data item from a binary.
 *
 *	Description:
 *		ddl_dataitems_choice will parse the binary for a list of data items,
 *		according to the current conditionals (if any).  The value of
 *		the data items is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		items - pointer to a DATA_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		items - pointer to an DATA_ITEM_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_cond_list()
 *			ddl_parse_dataitems()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_dataitems_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DATA_ITEM_LIST *items,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk
					 * list */

#ifdef DDSTEST
	TEST_FAIL(DDL_DATAITEMS_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		DATA_FIELD_SPECIFIER_TAG, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (items && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (items == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_dataitems(&(chunk_ptr->chunk), &(chunk_ptr->size),
				items, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (items && data_valid) {
			*data_valid = TRUE;	/* items has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;

}

/***********************************************************************
*
* Name: eval_dataitem
* ShortDesc: evaluate a dataitem attribute
*
* Description:
*	The eval_dataitem function evaluates a dataitem attribute.
*	The buffer pointed to by CHUNK should contain a binary chunk for a
*	dataitem attribute.	SIZE should specify the size of the binary.
*	If ITEMS is not a null pointer, the list of ITEMS is returned in ITEMS.
*	If depinfo is not a null pointer, dependency information about the
*	DATAITEMS is returned in depinfo.
*
* Inputs:
*      chunk - pointer to the address of the binary
*      size  - size of the binary
*      items - pointer to a DATA_ITEM_LIST structure where the result will
*              be stored.  If this is NULL, no result is computed
*              or stored.
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*              information will be stored.  If this is NULL, no
*              dependency information will be stored.
*      env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*      chunk - pointer to the address following this binary
*      size  - size of the binary
*      items  - pointer to a DATA_ITEM_LIST structure containing
*				the result
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*              dependency information.
*	   var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
* Returns:
*	DDL_DEFAULT_ATTR,
*	DDL_SUCCESS
* 	return codes from:
*		ddl_dataitems_choice()
*		ddl_shrink_depinfo()
*
* Author:	Steve Beyerl
****************************************************************/
int
eval_attr_dataitems(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	DATA_ITEM_LIST* items = static_cast<DATA_ITEM_LIST*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in
				 * "items" */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DATAITEMS);
#endif

	valid = 0;

	if (items) {
		ddl_free_dataitems(items, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_dataitems_choice(&chunk, &size, items, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_dataitems_list(items);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_dataitems(items, FREE_ATTR);
		return rc;
	}

	if (items && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *  Name: ddl_shrink_trans_list
 *  ShortDesc: Shrink the list of transactions.
 *
 *  Description:
 *      ddl_shrink_trans_list reallocs the list of TRANSACTIONs to contain
 *      only the TRANSACTIONs being used.  The list is shrunk to count
 *		elements.
 *
 *  Inputs:
 *      trans_list: pointer to the TRANSACTION_LIST structure
 *
 *  Outputs:
 *      trans_list: pointer to the resized TRANSACTION_LIST structure
 *
 *  Returns:
 *      DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *  Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static int
ddl_shrink_trans_list(
TRANSACTION_LIST *trans_list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_TRANS_LIST);
#endif

	if (!trans_list) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (!trans_list->list) {
		ASSERT_DBG(!trans_list->count && !trans_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (trans_list->count == trans_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (trans_list->count == 0) {
		ddl_free_trans_list(trans_list, FREE_ATTR);
	}
	else {
		trans_list->limit = trans_list->count;

		trans_list->list = (TRANSACTION *) realloc((void *) trans_list->list,
			(size_t) (trans_list->limit * sizeof(TRANSACTION)));

		if (!trans_list->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_trans_list
 *	ShortDesc: Free the list of TRANSACTIONs
 *
 *	Description:
 *		ddl_free_trans_list will check the TRANSACTION_LIST pointer and
 *		the list, if they are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero.
 *
 *	Inputs:
 *		trans_list: pointer to the TRANSACTION_LIST structure.
 *
 *	Outputs:
 *		trans_list: pointer to an empty TRANSACTION_LIST structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
ddl_free_trans_list(
TRANSACTION_LIST *trans_list,
uchar           dest_flag)
{

	int             i;	/* loop index */
	TRANSACTION    *trans_ptr;	/* temp ptr to a TRANSACTION entity */

	if (trans_list == NULL) {
		return;
	}

	if (trans_list->list == NULL) {

		ASSERT_DBG(!trans_list->count && !trans_list->limit);
		trans_list->limit = 0;
		trans_list->count = 0;
	}
	else {
		trans_ptr = trans_list->list;

		for (i = 0; i < trans_list->count; i++) {

			/*
			 * Loop through all transactions, freeing all
			 * "kling-on" entities.
			 */

			ddl_free_dataitems(&trans_ptr->request, FREE_ATTR);
			ddl_free_dataitems(&trans_ptr->reply, FREE_ATTR);
			ddl_free_response_codes(&trans_ptr->resp_codes, FREE_ATTR);
			ddl_free_actions(&trans_ptr->post_actions, FREE_ATTR);
			trans_ptr++;
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of TRANSACTIONs
			 */

			free((void *) trans_list->list);
			trans_list->list = NULL;
			trans_list->limit = 0;
		}

		trans_list->count = 0;
	}
}


/*********************************************************************
 *
 *	Name:	ddl_add_transaction_list()
 *	ShortDesc:	Add transaction elements to the transaction list.
 *
 *	Description:
 *		Add transaction elements to the transaction list, if necessary
 *
 *	Inputs:
 *		list - List of transactions.
 *
 *	Outputs:
 *		list - List of transactions.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author: Steve Beyerl
 *******************************************************************/

static int
ddl_add_transaction_list(
TRANSACTION_LIST *list)
{

	TRANSACTION    *tmp;	/** temporary list pointer */
	TRANSACTION    *endp;	/** ptr to end of transaction list */


	ASSERT_DBG(list);

#ifdef DDSTEST
	TEST_FAIL(DDL_ADD_TRANSACTION_LIST);
#endif

	/*
	 * Update the TRANSACTION_LIST count. If count is >= limit reallocate
	 * more space for another transaction item.
	 */

	if (list->count >= list->limit) {
		list->limit += TRANSACTION_INC;
		tmp = (TRANSACTION *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof(*tmp)));

		if (!tmp) {
			list->limit = list->count;
			ddl_free_trans_list(list, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}

		list->list = tmp;
		endp = &list->list[list->limit];
		for (tmp = &list->list[list->count]; tmp < endp; tmp++) {
			tmp->number = 0;
			tmp->request.count = 0;
			tmp->request.limit = 0;
			tmp->request.list = NULL;
			tmp->reply.count = 0;
			tmp->reply.limit = 0;
			tmp->reply.list = NULL;
			tmp->resp_codes.type = nsEDDEngine::RESPONSE_CODES::ResponseCodesType::RESP_CODE_TYPE_NONE;
			tmp->resp_codes.response_codes.resp_code_ref = 0;
			tmp->resp_codes.response_codes.resp_code_list.count = 0;
			tmp->resp_codes.response_codes.resp_code_list.limit = 0;
			tmp->resp_codes.response_codes.resp_code_list.list = NULL;
			tmp->post_actions.count = 0;
			tmp->post_actions.limit = 0;
			tmp->post_actions.list = NULL;
		}
	}

	return DDL_SUCCESS;
}


/************************************************************************
*
* Name: eval_transaction
* ShortDesc: evaluate a transaction attribute
*
* Description:
*	The eval_transaction function evaluates a transaction attribute.
*	The buffer pointed to by chunk should contain a transaction binary.
*	SIZE should specify the size of the binary.
*	If trans is not a null pointer, the transaction is returned in trans.
*	If depinfo is not a null pointer, dependency information about the
*	transaction is returned in depinfo.
*
*  Inputs:
*      chunk - pointer to the address of the binary
*      size - pointer to the size of the binary
*      trans - pointer to a TRANSACTION where the result will
*              be stored.  If this is NULL, no result is computed
*              or stored.
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*              information will be stored.  If this is NULL, no
*              dependency information will be stored.
*      env_info - environment information
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
*  Outputs:
*      chunk - pointer to the address following this binary
*      size - pointer to the size of the binary following this one
*      trans - pointer to a TRANSACTION structure containing the result
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*              dependency information.
*      data_valid - pointer to an int which, if set to 1, indicates
*              that there is a valid value in trans.
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
* Returns:
*	DDL_SUCCESS
*	DDL_ENCODING_ERROR
*	return codes from:
*		ddl_parse_integer_func()
*		ddl_parse_tag_func()
*		ddl_dataitems_choice()
*		ddl_rspcodes_choice()
*		ddl_shrink_depinfo()
*		ddl_shrink_dataitems_list()
*		ddl_shrink_resp_codes()
*
* Author steve beyerl
************************************************************************/


static int
eval_transaction(
unsigned char  *chunk,
DDL_UINT        size,
TRANSACTION    *trans,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in trans */
	DDL_UINT        tag;	/* temp storage for a parsed tag */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_TRANSACTION);
#endif

	valid = 0;

	if (trans) {
		ddl_free_dataitems(&trans->request, CLEAN_ATTR);
		ddl_free_dataitems(&trans->reply, CLEAN_ATTR);
		ddl_free_response_codes(&trans->resp_codes, CLEAN_ATTR);
		ddl_free_actions(&trans->post_actions, CLEAN_ATTR);
		ddl_free_depinfo(depinfo, CLEAN_ATTR);
	}

	/*
	 * Parse the transaction number
	 */

	if (trans) {
		rc = ddl_parse_integer_func(&chunk, &size, &trans->number);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}
	}
	else {
		rc = ddl_parse_integer_func(&chunk, &size, (DDL_UINT *) NULL_PTR);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}
	}

	/*
	 * The rest of the TRANSACTION definition consists of a series of
	 * REQUEST, REPLY, and RESPONSE_CODE entries.  Parse the tag to
	 * determine which of the entries each one is, and then parse the
	 * entry.
	 */

	while (size > 0) {
		rc = ddl_parse_tag_func(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}

		switch (tag) {
		case TRANS_REQ_TAG:	/* REQUEST */
			if (trans) {
				rc = ddl_dataitems_choice(&chunk, &size, &trans->request,
					depinfo, &valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}
			else {
				rc = ddl_dataitems_choice(&chunk, &size, (DATA_ITEM_LIST *) NULL,
					depinfo, (int *) NULL, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}

			break;

		case TRANS_REPLY_TAG:	/* REPLY */
			if (trans) {
				rc = ddl_dataitems_choice(&chunk, &size, &trans->reply,
					depinfo, &valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}
			else {

				rc = ddl_dataitems_choice(&chunk, &size, (DATA_ITEM_LIST *) NULL,
					depinfo, (int *) NULL, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}

			break;

		case TRANS_RESP_CODES_TAG:	/* RESPONSE_CODES */
			if (trans) {
				rc = eval_response_codes(&chunk, &size, &trans->resp_codes,
					depinfo, &valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}
			else {
				rc = eval_response_codes(&chunk, &size,
					(RESPONSE_CODES *) NULL,
					depinfo, (int *) NULL, env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}

			break;

		case TRANS_POST_ACTIONS:
			if(trans)
			{
				rc = ddl_actions_choice(&chunk, &size, &trans->post_actions,
					depinfo, &valid, env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}
			}

		default:
			rc = DDL_ENCODING_ERROR;
			goto err_exit;
		}
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_dataitems_list(&trans->request);
	}
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_dataitems_list(&trans->reply);
	}
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_dataitems(&trans->request, FREE_ATTR);
	ddl_free_dataitems(&trans->reply, FREE_ATTR);
	ddl_free_response_codes(&trans->resp_codes, FREE_ATTR);
	ddl_free_actions(&trans->post_actions, FREE_ATTR);
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}



/*******************************************************************
*
* Name: eval_attr_transaction_list
* ShortDesc: evaluate a list of transactions
*
* Description:
*	The eval_attr_transaction function evaluates a list of transactions.
*	The buffer pointed to by chunk should contain a transaction list
* 	binary.  SIZE should specify the size of the binary.
*	If trans_list is not a null pointer, the list of transactions is
*	returned in trans_list. If depinfo is not a null pointer, dependency
*	information about the list of transactions is returned in depinfo.
*
*  Inputs:
*      chunk - pointer to the address of the binary
*      size - pointer to the size of the binary
*      trans_list - pointer to a TRANSACTION_LIST where the result will
*              be stored.  If this is NULL, no result is computed
*              or stored.
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*              information will be stored.  If this is NULL, no
*              dependency information will be stored.
*      env_info - environment information
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
*  Outputs:
*      chunk - pointer to the address following this binary
*      size - pointer to the size of the binary following this one
*      trans_list - pointer to a TRANSACTION_LIST structure containing
*				the result
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*              dependency information.
*      data_valid - pointer to an int which, if set to 1, indicates
*              that there is a valid data in trans_list
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
* Returns:
*	DDL_SUCCESS
*	DDL_ENCODING_ERROR
*	return codes from:
*		ddl_parse_integer_func()
*		DDL_PARSE_TAG()
*		ddl_dataitems_choice()
*		ddl_rspcodes_choice()
*		ddl_shrink_depinfo()
*		ddl_shrink_dataitems_list()
*		ddl_shrink_resp_codes()
*
* Author steve beyerl
********************************************************************/

int
eval_attr_transaction_list(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	TRANSACTION_LIST* trans_list = static_cast<TRANSACTION_LIST*>(voidP);
	int             rc;
	DDL_UINT        tag;
	DDL_UINT        len;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_TRANSACTION_LIST);
#endif

	if (trans_list) {
		ddl_free_trans_list(trans_list, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	while (size > 0) {

		/*
		 * Strip off TRANSACTION_TAG
		 */

		DDL_PARSE_TAG(&chunk, &size, &tag, &len);

		if (tag != TRANSACTION_TAG) {
			return DDL_ENCODING_ERROR;
		}

		if (trans_list->count >= trans_list->limit) {
			rc = ddl_add_transaction_list(trans_list);
			if (rc != DDL_SUCCESS) {

				/*
				 * rc = free_trans_list();
				 */

				return rc;
			}
		}


		rc = eval_transaction(chunk, len, &trans_list->list[trans_list->count],
			depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}

		trans_list->count++;
		size -= len;
		chunk += len;

	}			/* end of WHILE */


	rc = ddl_shrink_trans_list(trans_list);
	if (rc != DDL_SUCCESS) {
		ddl_free_trans_list(trans_list, FREE_ATTR);
		return DDL_MEMORY_ERROR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_trans_list(trans_list, FREE_ATTR);
	return rc;

}


