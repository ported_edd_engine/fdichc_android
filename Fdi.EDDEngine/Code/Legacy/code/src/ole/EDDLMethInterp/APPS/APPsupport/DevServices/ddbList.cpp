/*************************************************************************************************
 *
 * $Workfile: ddbList.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
#include "stdafx.h"
#include "ddbList.h"
#include <CMethodInterpreterDevice.h>
#include "varientTag.h"

RETURNCODE hClist::getList(ddbItemList_t& r2diL) 
{ 
	std::vector<hCitemBase*>::iterator anIT;
	for (anIT  = activeIL.begin(); anIT != activeIL.end(); ++anIT )
	{ 
		r2diL.push_back( *anIT );	
	}	
	return (r2diL.size() != activeIL.size());
}

hClist::hClist(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), capacityValue(0), pTypeDef(NULL)
{
	// label, help and validity are set in the base class
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		//get pTypeDef
		(void) pDevice->getElementOrMemberPointer(itemId, 0, &pTypeDef);

		//get capacityValue
		CValueVarient vtValue{};
		if (pDevice->ReadParameterData2( &vtValue, "Capacity", 0, 0, itemId, 0 )  == BLTIN_SUCCESS)
		{
			
			capacityValue = CV_UI4(&vtValue);
		}
	}
}

hClist::hClist(hClist* pSrc, hClist* pVal, itemIdentity_t newID)
	: hCitemBase((hCitemBase*)pSrc,newID), pTypeDef(NULL)
{	
	capacityValue   = pSrc->capacityValue;
}

hClist::~hClist()  
{
}


itemType_t    hClist::getIType(void)
{	
	return iT_List;
}


RETURNCODE hClist::getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = SUCCESS;
	CValueVarient rV{};

	rV = getCount();// 03aug06-was:iCount;

	if ( rV.vIsValid )
	{
		if ( ((int)rV) > 0 )
		{
			if ( indexValue < (UINT32)rV )
			{
				hCreference localRef(devHndl());
				// make a reference from activeIL[indexValue]
				localRef.setRef(activeIL[indexValue]->getID(),0,
					            (ITEM_TYPE)activeIL[indexValue]->getTypeVal(),false,rT_Item_id);
				returnedItemRefs.push_back(localRef);
				rc = SUCCESS;
			}
			// else no match to index - do nothing, return success
		}
		// else no content - do nothing, return success
	}
	else
	{
		cerr <<"ERROR: List has no members in getAllByIndex."<<endl;
		rc = APP_RESOLUTION_ERROR;
	}
	return rc;
}


RETURNCODE hClist::getAllindexValues(vector<UINT32>& allValidindexes)
{
	/****** NOTE: this assumes a contiguous list - not yet specified ********/
	for ( int i = 0; i < (int)activeIL.size(); i++ )  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		allValidindexes.push_back(i);
	}
	return SUCCESS;
}


hCitemBase* hClist::operator[](int idx0)
{
	if ( idx0 < 0 || idx0 > (int)(activeIL.size()-1) ) // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{
		return NULL;// exception
	}

	return activeIL[idx0];// a list of pointers, this returns the pointer
}


RETURNCODE hClist::getByIndex(UINT32 indexValue,hCgroupItemDescriptor** ppGID,bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	
		return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
	}

	//List index is always 1 based in the MI data process.
	//Since the input index is 0 based, add 1 to the index here.
	hCreference locRef( devHndl() );
	locRef.setRef( itemId, indexValue+1, iT_Variable, false, rT_Item_id);
	(*pGid) = locRef;
	pGid->setIndex(indexValue+1);
	rc = SUCCESS;

	//if ( activeIL.size() > 0 && isInGroup(indexValue) )
	//{
	//	ptr2hCitemBase_t pIb = activeIL[indexValue];
	//	if ( pIb != NULL && pIb->getIType() != 0 && pGid != NULL )
	//	{
	//		hCreference locRef(devHndl() );
	//		locRef.setRef(pIb->getID(), 0, pIb->getTypeVal(), false,rT_Item_id) ;
	//		(*pGid) = locRef;
	//		pGid->setIndex(indexValue);
	//		rc = SUCCESS;
	//	}
	//}
	//if (rc != SUCCESS)
	//{
	//	if ( !suppress)
	//	{
	//		cerr << "ERROR: List Item getByIndex failed to find index '" 
	//				 << hex << indexValue << dec <<"' in "<< activeIL.size() <<" elements."<<endl;
	//	}
	//	rc = DB_ATTRIBUTE_NOT_FOUND;

	//	if(pGid)	//Vibhor: 200904
	//	{
	//		RAZE(pGid);		
	//	}
	//	*ppGID = NULL;
	//}
	return rc;
}

// 03aug06 -stevev begin code --- the psuedoVariable handling for references
itemID_t      hClist::getAttrID (unsigned  attr, int which)
{
	itemID_t r = 0;	
	return r;
}


hCinteger* hClist::getPsuedoInt(int initialValue)
{
	hCinteger* pInt = NULL;
	return pInt;
}


hCVar*        hClist::getAttrPtr(unsigned  attr, int which)// 03aug06 
{
	hCVar* pVar = NULL;

	return pVar;
}


// 03aug06 COUNT access - do not access any other
unsigned int  hClist::setCount(unsigned  newCnt)
{
	return 0;
}

// moved from above and modified
unsigned int  hClist::getCount(void)
{
	//activeIL.size();
	// 03aug06-was:return (iCount = activeIL.size());
	unsigned int r = 0;
	setCount( r = (unsigned int)activeIL.size() );// updates to actual while retrieving
	return r;
}


//other class constructors
hCchart::hCchart(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase)
{
}

unsigned long long hCchart::getLength()
{
	//get CHART length
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	CValueVarient vtValue{};
	int iRetVal = pDevice->ReadParameterData2( &vtValue, _T("LENGTH"), 0, 0, this->itemId, 0 );
	if( iRetVal == SUCCESS ) 
	{
		
		return CV_UI8(&vtValue);
	}
	else
	{
		return 0;
	}
}


hCgraph::hCgraph(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pXAxisIB(NULL)
{
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		//get X_AXIS reference item ID
		CValueVarient vtValue{};
		RETURNCODE rc = pDevice->ReadParameterData2( &vtValue, _T("XAxisItemID"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			ITEM_ID ulXAxisID = CV_UI4(&vtValue);

			//get X_AXIS reference item pointer
			if (pDevice->getItemBySymNumber(ulXAxisID, &pXAxisIB) != BLTIN_SUCCESS)
			{
				pXAxisIB = NULL;
			}
		}
	}
}


hCwaveform::hCwaveform(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pYAxisIB(NULL)
{
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		//get Y_AXIS reference item ID
		CValueVarient vtValue{};
		RETURNCODE rc = pDevice->ReadParameterData2( &vtValue, _T("YAxisItemID"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			ITEM_ID ulYAxisID = CV_UI4(&vtValue);

			//get Y_AXIS reference item pointer
			if (pDevice->getItemBySymNumber(ulYAxisID, &pYAxisIB) != BLTIN_SUCCESS)
			{
				pYAxisIB = NULL;
			}
		}
	}
}


hCsource::hCsource(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCitemBase(h, paItemBase), pYAxisIB(NULL)
{
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		//get Y_AXIS reference item ID
		CValueVarient vtValue{};
		RETURNCODE rc = pDevice->ReadParameterData2( &vtValue, _T("YAxisItemID"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			ITEM_ID ulYAxisID = CV_UI4(&vtValue);

			//get Y_AXIS reference item pointer
			if (pDevice->getItemBySymNumber(ulYAxisID, &pYAxisIB) != BLTIN_SUCCESS)
			{
				pYAxisIB = NULL;
			}
		}
	}
}
