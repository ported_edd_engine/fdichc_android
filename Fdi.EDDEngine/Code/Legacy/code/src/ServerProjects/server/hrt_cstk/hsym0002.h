/*
 * This header file was generated from file  
 *
 *  ****  DO NOT MODIFY THIS FILE, IT IS GENERATED  ****
 *  ****        FROM THE NAMED SYMBOL FILE          ****
 */

#define SYM_0002_first											100
#define SYM_0002_last											999
#define SYM_response_code_ID									(ITEM_ID) 150
#define SYM_device_status_ID									(ITEM_ID) 151
#define SYM_comm_status_ID										(ITEM_ID) 152
#define SYM_manufacturer_id_ID									(ITEM_ID) 153
#define SYM_device_type_ID										(ITEM_ID) 154
#define SYM_request_preambles_ID								(ITEM_ID) 155
#define SYM_universal_revision_ID								(ITEM_ID) 156
#define SYM_transmitter_revision_ID								(ITEM_ID) 157
#define SYM_software_revision_ID								(ITEM_ID) 158
#define SYM_hardware_revision_ID								(ITEM_ID) 159
#define SYM_device_flags_ID										(ITEM_ID) 160
#define SYM_device_id_ID										(ITEM_ID) 161
#define SYM_polling_address_ID									(ITEM_ID) 162
#define SYM_tag_ID												(ITEM_ID) 163
#define SYM_message_ID											(ITEM_ID) 164
#define SYM_descriptor_ID										(ITEM_ID) 165
#define SYM_date_ID												(ITEM_ID) 166
#define SYM_write_protect_ID									(ITEM_ID) 167
#define SYM_private_label_distributor_ID						(ITEM_ID) 168
#define SYM_final_assembly_number_ID							(ITEM_ID) 169
#define SYM_data_block_ID										(ITEM_ID) 170
#define SYM_physical_signaling_code_ID							(ITEM_ID) 171
#define SYM_dynamic_variables_ID								(ITEM_ID) 172
#define SYM_read_unique_identifier_ID							(ITEM_ID) 174
#define SYM_read_pv_ID											(ITEM_ID) 175
#define SYM_read_pv_current_and_percent_range_ID				(ITEM_ID) 176
#define SYM_read_dynamic_variables_and_pv_current_ID			(ITEM_ID) 177
#define SYM_write_polling_address_ID							(ITEM_ID) 178
#define SYM_read_unique_identifier_with_tag_ID					(ITEM_ID) 179
#define SYM_read_message_ID										(ITEM_ID) 180
#define SYM_read_tag_descriptor_date_ID							(ITEM_ID) 181
#define SYM_read_pv_sensor_info_ID								(ITEM_ID) 182
#define SYM_read_pv_output_info_ID								(ITEM_ID) 183
#define SYM_read_final_assembly_number_ID						(ITEM_ID) 184
#define SYM_write_message_ID									(ITEM_ID) 185
#define SYM_write_tag_descriptor_date_ID						(ITEM_ID) 186
#define SYM_write_final_assembly_number_ID						(ITEM_ID) 187
#define SYM_read_common_static_data_ID							(ITEM_ID) 188
#define SYM_write_common_static_data_ID							(ITEM_ID) 189
#define SYM_DIGITAL_UNITS_ID									(ITEM_ID) 190
#define SYM_DIGITAL_VALUE_ID									(ITEM_ID) 191
#define SYM_ANALOG_VALUE_ID										(ITEM_ID) 193
#define SYM_PERCENT_RANGE_ID									(ITEM_ID) 194
#define SYM_MINIMUM_SPAN_ID										(ITEM_ID) 195
#define SYM_ALARM_CODE_ID										(ITEM_ID) 196
#define SYM_TRANSFER_FUNCTION_ID								(ITEM_ID) 197
#define SYM_DAMPING_VALUE_ID									(ITEM_ID) 198
#define SYM_SENSOR_SERIAL_NUMBER_ID								(ITEM_ID) 199
#define SYM_SENSOR_UNITS_ID										(ITEM_ID) 200
#define SYM_UPPER_SENSOR_LIMIT_ID								(ITEM_ID) 201
#define SYM_LOWER_SENSOR_LIMIT_ID								(ITEM_ID) 202
#define SYM_RANGE_UNITS_ID										(ITEM_ID) 203
#define SYM_UPPER_RANGE_VALUE_ID								(ITEM_ID) 204
#define SYM_LOWER_RANGE_VALUE_ID								(ITEM_ID) 205
#define SYM_max_device_vars_ID									(ITEM_ID) 206
#define SYM_config_change_counter_ID							(ITEM_ID) 207
#define SYM_device_profile_ID									(ITEM_ID) 255
#define SYM_extended_field_device_status_ID						(ITEM_ID) 2055
#define SYM_response_preambles_ID								(ITEM_ID) 4066
/*
 * End of header generation from file hrt_cstk/0002.sym
 */
