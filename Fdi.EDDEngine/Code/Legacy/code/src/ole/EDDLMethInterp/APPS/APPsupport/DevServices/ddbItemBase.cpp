/*************************************************************************************************
 *
 * $Workfile: ddbItemBase.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		method code for the base-base class for all items 
 *		9/4/2	sjv	 creation
 */

#include "stdafx.h"
#include "ddbItemBase.h"
#include "varientTag.h"
#include <CMethodInterpreterDevice.h>


hCitemBase::hCitemBase(DevInfcHandle_t h) : hCobject(h)
{
	itemId      = 0;
	itemType    = 0;
	itemSize    = 0;
	itemSubType = 0;
	itemName.erase();
	isConditional = false;	
	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
}


hCitemBase::hCitemBase(DevInfcHandle_t h, ITEM_TYPE iType, ITEM_SIZE  iSz)
	: hCobject(h), itemType(iType)
{	
	itemId      = 0;
	itemSize    = iSz;
	itemSubType = 0;
	itemName.erase();
	isConditional = false;
	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
}

// construction from dllapi class
hCitemBase::hCitemBase(DevInfcHandle_t h, aCitemBase* apIb) : hCobject(h)
{
	if( apIb )
	{
		itemId      = apIb->itemId;
		itemType    = apIb->itemType;
		itemName	= apIb->itemName;
	}
	else
	{
		itemId      = 0;
		itemType    = 0;
		itemName.erase();
	}
	itemSize    = 0;
	itemSubType = 0;
	isConditional = false;	
	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
};

hCitemBase::hCitemBase( DevInfcHandle_t h, ITEM_TYPE iType, itemID_t   iId,  unsigned long Dkey )
: hCobject(h)
{
	itemId      = iId;
	itemType    = (long)iType;
	itemSize    = 0;
	itemSubType = 0;
	itemName.erase();
	isConditional = false;	
	pValid = NULL;
	pLabel = NULL;
	pHelp  = NULL;
	pDebug = NULL;
}


/* from-typedef constructor !! */
hCitemBase::hCitemBase( hCitemBase* pSrc, itemIdentity_t newID) : hCobject(pSrc->devHndl())
{
	itemId			= newID.newID;
	itemType		= pSrc->itemType;
	itemSize		= pSrc->itemSize;
	itemSubType		= pSrc->itemSubType;
	itemName		= newID.newName;
	isConditional	= pSrc->isConditional;
	pValid    = pSrc->pValid;
	pLabel    = pSrc->pLabel;
	pHelp     = pSrc->pHelp;
	pDebug    = pSrc->pDebug;
}


hCitemBase::~hCitemBase() 
{
}

RETURNCODE hCitemBase::destroy(void)
{
	RETURNCODE rc = SUCCESS;
	return rc;
}
 
hCitemBase:: operator hCVar*() 
{ 
	if(IsVariable() && this != NULL)
	{ 
		return (hCVar*)this;
	}
	else
	{ 
		return (hCVar*)NULL;
	} 
};

hCitemBase& hCitemBase::operator=(const aCitemBase& ib) 
{
	if ( &ib == NULL ) 
		return *this;
	itemId =      ib.itemId;
	itemType =    ib.itemType;
	itemSize =    ib.itemSize;
	itemSubType = ib.itemSubType;
	isConditional = ib.isConditional;
	itemName =    (ib.itemName.c_str()); // note that straight equal copies the ptr (to dll mem)	
	pValid  =     NULL;
	pLabel  =     NULL;
	pHelp   =     NULL;
	pDebug  =     NULL;

	return *this;
};

hCitemBase& hCitemBase::operator=(const hCitemBase& ib) 
{	
	if ( &ib == NULL ) 
	{
		return *this;
	}
	itemId =      ib.itemId;
	itemType =    ib.itemType;
	itemSize =    ib.itemSize;
	itemSubType = ib.itemSubType;	
	itemName =    ib.itemName;	
	isConditional = ib.isConditional;

	pValid  =     ib.pValid;
	pLabel  =     ib.pLabel;
	pHelp   =     ib.pHelp;
	pDebug  =     ib.pDebug;

	return *this;
};



RETURNCODE hCitemBase::baseLabel(int key, wstring &label)
{
	RETURNCODE  rc = FAILURE;

	label.clear();
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtValue{};
		rc = pDevice->ReadParameterData2( &vtValue, _T("Label"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			label = CV_WSTR(&vtValue);
		}
	}
	return rc;
}

RETURNCODE hCitemBase::dumpSelf(int indent, char* typeName)  // to cout
{
	return SUCCESS;
};

/*virtual */
bool hCitemBase::IsValidTest()  /*returns true if valid and CACHED (does not read to reolve */
{
	return true;
}

/* virtual */
bool hCitemBase::is_UNKNOWN(void)
{
	return false;
}

/* virtual */
bool hCitemBase::IsValid(void) /*returns true if the item is currently valid*///return validity				/*returns the help string (or null)*/
{
	return true;
};


bool hCitemBase::isValidityConditional(void)
{
	return false;
}

void hCitemBase::setDependentsUNK(void)
{
}


//VMKP Modified for Help conditional handling
RETURNCODE hCitemBase::Help(wstring &help)
{ 
	RETURNCODE  rc = FAILURE;

	help.clear();
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtValue{};
		rc = pDevice->ReadParameterData2( &vtValue, _T("Help"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{
			
			help = CV_WSTR(&vtValue);
		}
	}
	return rc;
};	


// attr_Type in a hCattrBase is an enum value! (in a aCattrBase it is a mask)
// this is for variables ONLY - over ride the virtual for other types
hCattrBase*  hCitemBase::getaAttr(varAttrType_t attrType)
{
	return NULL;// not found
};


string& hCitemBase::getName(void)
{ 
	return itemName; 
};
void hCitemBase::setName(string& s) 
{ 
	itemName = s;
}

// overide this virtual if your item is a container class
// eg menu, collection-as-menu,grid,graph (waveform), chart (source)
void  hCitemBase::fillContainList(ddbItemList_t&  iDependOn) 
{
	return;
}

// no inputs or outputs...it will do the best it can
// overide this virtual if your item has more than label (call this to get label depends)
void  hCitemBase::fillCondDepends(ddbItemList_t&  iDependOn) 
{
}

// 03aug06 - support for attribute references for label & help	
//			returns NULL on error (ie not an accesible attribute number)
itemID_t hCitemBase::getAttrID (unsigned  attr, int which)
{
	itemID_t ID = 0;
	return ID;
}

hCVar* hCitemBase::getAttrPtr(unsigned  attr, int which)
{
	hCVar* pV = NULL;
	return pV;
}


RETURNCODE hCitemBase::setSize( hv_point_t& returnedSize, ulong /*qual*/ ){ returnedSize.xval=returnedSize.yval = 1; return 0;}
ITEM_ID	      hCitemBase::getID(void)		{ return itemId; }
itemType_t    hCitemBase::getIType(void)   { return ((itemType_t)itemType.getType()); }
unsigned int  hCitemBase::getTypeVal(void) { return itemType.getType();}
const char*   hCitemBase::getTypeName(void){ return itemType.getTypeStr();}
unsigned int  hCitemBase::getSize(void)    { return itemSize; }
bool  hCitemBase::IsConditional(void)    { return isConditional; }
hCattrLabel*  hCitemBase::getLabelAttr(void)   { return pLabel;}
bool hCitemBase::HasHelp(){ return true; }
bool hCitemBase::IsDynamic(void)  { return false; } //the only class the counts
bool hCitemBase::IsReadOnly(void) { return false; } // these need to be overridden in the hCvar class
bool hCitemBase::IsVariable(void) { return false; }
bool hCitemBase::IsCommand(void) { return false; } //CJK was here 9/3/2009
bool hCitemBase::IsMethod(void) { return false; }
hCattrBase*  hCitemBase::newHCattr(aCattrBase* pACattr){return NULL;}
RETURNCODE hCitemBase::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress ){ return VIRTUALCALLERR;}
RETURNCODE hCitemBase::getAllindexes(vector<UINT32>& allindexedIDsReturned){ return VIRTUALCALLERR;}
RETURNCODE hCitemBase::getAllindexValues(vector<UINT32>& allValidindexes){ return VIRTUALCALLERR;}

/*************************************************************************************************
 *
 *   $History: ddbItemBase.cpp $
 * 
 * *****************  Version 4  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
