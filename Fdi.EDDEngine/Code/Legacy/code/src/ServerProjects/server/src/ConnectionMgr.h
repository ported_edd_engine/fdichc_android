#pragma once


#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"
#include "DeviceTypeMgr.h"


const int DD_PATH_LENGTH = 256 ;


class CConnectionMgr
{
private:
	ACTIVE_BLK_TBL				active_blk_tbl;
	ACTIVE_DEV_TBL				active_dev_tbl;
	TCHAR						hart_dd_path[DD_PATH_LENGTH] ;

	/* Function Prototypes for Active Block Information */
public:
	CConnectionMgr();
	~CConnectionMgr();
	DDOD_HEADER * get_header(ROD_HANDLE rhandle);
	int set_abt_dd_blk_tbl_offset (BLOCK_HANDLE,int);
	int set_abt_param_count (BLOCK_HANDLE,unsigned int);
	int set_abt_operation( BLOCK_HANDLE, CM_OP op );
	int set_abt_dd_blk_id (BLOCK_HANDLE,UINT32);
	int set_abt_app_info (BLOCK_HANDLE,void *);
	int set_abt_comm_status( BLOCK_HANDLE bh,CM_STATUS el );
	int set_abt_db_handle( BLOCK_HANDLE bh,CM_DB_HANDLE h );
	int set_abt_op_index (BLOCK_HANDLE,OBJECT_INDEX);
	int set_abt_hart_sub_station (BLOCK_HANDLE,int) ;
	int set_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE) ;
	int set_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ;
	int set_abt_adt_offset (BLOCK_HANDLE,DEVICE_HANDLE);
	int set_abt_dd_handle (BLOCK_HANDLE,ROD_HANDLE *);
	int set_abt_dd_dev_tbls (BLOCK_HANDLE,void *);
	int set_abt_dev_family (BLOCK_HANDLE,int);
	LPTSTR get_abt_tag( BLOCK_HANDLE bh );
	void set_abt_tag( BLOCK_HANDLE bh, LPTSTR t );
	int get_abt_db_handle (BLOCK_HANDLE, CM_DB_HANDLE *) ;
	int get_abt_desc_date (BLOCK_HANDLE, TCHAR **, TCHAR **) ;
	int get_abt_count (void);
	int get_abt_operation( BLOCK_HANDLE bh, CM_OP *op );
	int get_abt_op_index (BLOCK_HANDLE, OBJECT_INDEX *);
	int get_abt_param_count(BLOCK_HANDLE, unsigned int *);
	int get_abt_dd_blk_id (BLOCK_HANDLE, ITEM_ID *);
	int get_abt_dd_blk_tbl_offset (BLOCK_HANDLE, int *);
	int get_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE *) ;
	int get_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ;
	int get_abt_app_info (BLOCK_HANDLE, void **);
	int get_abt_adt_offset (BLOCK_HANDLE, DEVICE_HANDLE *);
	int get_abt_usage (BLOCK_HANDLE, int *);
	int get_abt_dd_handle (BLOCK_HANDLE, ROD_HANDLE *);
	int get_abt_dd_dev_tbls (BLOCK_HANDLE, void **);
	int set_abt_hart_find_cnf (wchar_t *, BLOCK_HANDLE, HART_IDENTIFY_CNF *);
	
	LPTSTR get_adt_device_tag( DEVICE_HANDLE dh);
	int get_adt_dd_handle (DEVICE_HANDLE, ROD_HANDLE *);
	int get_adt_dd_dev_tbls(DEVICE_HANDLE dh, void **ddt);
	DEVICE_MANUFACTURER_HANDLE get_adt_man_tbl_offset( DEVICE_HANDLE dh );
	LPTSTR get_adt_desc( DEVICE_HANDLE dh );
	LPTSTR get_adt_date( DEVICE_HANDLE dh );
	CM_IDENTIFIED_TYPE get_adt_identified( DEVICE_HANDLE dh );
	int get_adt_usage( DEVICE_HANDLE dh );
	NETWORK_HANDLE get_adt_network( DEVICE_HANDLE dh );
	HART_ADDR_TYPE get_address_type( DEVICE_HANDLE dh );
	int get_adt_station_address( DEVICE_HANDLE dh );
	CM_OP get_adt_operation( DEVICE_HANDLE dh );
	CM_STATUS get_adt_comm_status( DEVICE_HANDLE dh );
	void* get_adt_app_info( DEVICE_HANDLE dh );
	DEVICE_TYPE_HANDLE get_adt_adtt_offset( DEVICE_HANDLE dh );
	DD_DEVICE_ID* get_adt_dd_device_id( DEVICE_HANDLE dh );

	void set_adt_dd_handle( DEVICE_HANDLE dh, ROD_HANDLE *ddh );
	int set_adt_device_tag( DEVICE_HANDLE dh, LPTSTR dt );
	void set_adt_time( DEVICE_HANDLE dh, CM_TIME t );
	void set_adt_view( DEVICE_HANDLE dh, CM_VIEW v );
	void set_adt_desc( DEVICE_HANDLE dh, LPTSTR d );
	void set_adt_date( DEVICE_HANDLE dh, LPTSTR d );
	void set_adt_identified( DEVICE_HANDLE dh, CM_IDENTIFIED_TYPE i );
	void set_adt_usage( DEVICE_HANDLE dh, int u );
	void set_adt_network( DEVICE_HANDLE dh, NETWORK_HANDLE n );
	void set_adt_address_type( DEVICE_HANDLE dh, HART_ADDR_TYPE ht );
	void set_adt_station_address( DEVICE_HANDLE dh, int sa );
	void set_adt_operation( DEVICE_HANDLE dh, CM_OP op);
	void set_adt_comm_status( DEVICE_HANDLE dh, CM_STATUS s );
	void set_adt_app_info( DEVICE_HANDLE dh, void* ai );
	void set_adt_adtt_offset( DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto );


	

	int valid_block_handle (BLOCK_HANDLE);
	int valid_device_handle (DEVICE_HANDLE);
/* Function Prototypes for Active Device Type Information */

/* cm_init.cpp - Connection Manager Initialization Functions */
	int cm_cleanup(ENV_INFO *) ;

/* cm_tbl.cpp - Connection Manager Table Functions */
	void set_hart_dd_path(  wchar_t* sHartPath );
	int ct_simple_hart_open (ENV_INFO *, wchar_t*, SIMPLE_OPEN *, BLK_LIST *) ;
	
	int ct_block_open (LPTSTR, ENV_INFO *, BLK_LIST *) ;
	int ct_block_close (ENV_INFO *, BLOCK_HANDLE);
	BLOCK_HANDLE ct_block_search (LPTSTR);
	int ct_block_time_search (LPTSTR, BLK_LIST *);
	void ct_remove_block(BLOCK_HANDLE block_handle);
	int cm_comm_init(int);
	
	int ct_substation_time_search(DEVICE_HANDLE, int, CM_TIME, BLOCK_HANDLE);

	void ct_remove_device (DEVICE_HANDLE device_handle);

	int cm_find_block(LPTSTR,ENV_INFO *, BLK_LIST *, CM_OP);
	int ct_new_block(BLOCK_HANDLE *);
	int ct_new_device(DEVICE_HANDLE *);

	/* cm_dds.cpp - Connection Manager DD Support Functions */
	int ds_dd_load (ENV_INFO *, BLOCK_HANDLE, int);
	int ds_ddod_load( ENV_INFO *, DEVICE_HANDLE	device_handle, ROD_HANDLE* dd_handle);
	int ds_dd_block_load (ENV_INFO *, BLOCK_HANDLE, ROD_HANDLE *,int);
	int ds_dd_device_load (ENV_INFO *env_info, DEVICE_HANDLE, ROD_HANDLE *);

	int get_string_tbl_handle(BLOCK_HANDLE block_handle, STRING_TBL **string_tbl_handle);
};


