#include "StdAfx.h"
#include <CMethodInterpreterDevice.h>
#include <Ole/EDDLMethInterp/pathtype.h>
#include "ddbMethod.h"
#include "ddbArray.h"
#include "ddbAxis.h"
#include "hCRecord.h"
#include <3rdParty/panic.h>
#include "varientTag.h"



bool CMethodInterpreterDevice::m_bmethodProcessed = false;
//////////////////////////////////////////////////////////////////////////////////////////////
// Name: CMethodInterpreterDevice constructor														//
// Description: Default all the pointers to their initial values							//
//////////////////////////////////////////////////////////////////////////////////////////////
CMethodInterpreterDevice::CMethodInterpreterDevice(  DevInfcHandle_t h ) : hCddbDevice(h)
{
	m_pCommandList = new CCmdList( devHndl() );	
}

//
// Destructor
//
CMethodInterpreterDevice::~CMethodInterpreterDevice()
{
	ClearItemMapping();
	if( m_pCommandList )
	{
		delete m_pCommandList;
		m_pCommandList = NULL;
	}
}

//
// Clean up memory by emptying the item-name mappings
//
void CMethodInterpreterDevice::ClearItemMapping()
{
	int nCount = (int)m_aItemIdMapping.size();
	for( int nIndex=0; nIndex < nCount; nIndex++ )
	{
		CItemMapping *pItemMapping = (CItemMapping *)m_aItemIdMapping.at(nIndex);
		if( pItemMapping->m_pItemPointer )
		{
			delete pItemMapping->m_pItemPointer;
			pItemMapping->m_pItemPointer = NULL;
		}
		delete pItemMapping;
	}
	m_aItemIdMapping.clear();
}


//
// For a given ItemId get the map object that contains all the info for this item.
//
CItemMapping *CMethodInterpreterDevice::GetItemMapObject( long lItemID )
{
	CItemMapping *pRetVal = NULL;

	int nCount = (int)m_aItemIdMapping.size();
	for( int nIndex=0; nIndex < nCount; nIndex++ )
	{
		CItemMapping *pItem = (CItemMapping *)m_aItemIdMapping[nIndex];
		if( pItem->m_lItemID == lItemID )
		{
			pRetVal = pItem;		// if the specific item is found, break
			break;
		}
	}

	if( pRetVal == NULL )
	{
		// add map of simple variable or group variable with member ID being 0
		if( AddMapBasedOnItemID( lItemID ) )
		{
			int nCount = (int)m_aItemIdMapping.size();
			for( int nIndex=0; nIndex < nCount; nIndex++ )
			{
				CItemMapping *pItem = (CItemMapping *)m_aItemIdMapping[nIndex];
				if (pItem->m_lItemID == lItemID)
				{
					pRetVal = pItem;
					break;
				}
			}
		}
	}
	return pRetVal;
}

//
// for a given ItemName get the object that has all the info for this item.
//
CItemMapping *CMethodInterpreterDevice::GetItemMapObject( CStdString sName )
{
	CItemMapping *pRetVal = NULL;

	int nCount = (int)m_aItemIdMapping.size();
	for( int nIndex=0; nIndex < nCount; nIndex++ )
	{
		CItemMapping *pItem = (CItemMapping *)m_aItemIdMapping[nIndex];
		if( pItem->m_sName == sName )
		{
			pRetVal = pItem;
			break;
		}
	}

	if( pRetVal == NULL )
	{
		if( AddMapBasedOnName( sName ) )
		{
			int nCount = (int)m_aItemIdMapping.size();
			for( int nIndex=0; nIndex < nCount; nIndex++ )
			{
				CItemMapping *pItem = (CItemMapping *)m_aItemIdMapping[nIndex];
				if( pItem->m_sName == sName )
				{
					pRetVal = pItem;
					break;
				}
			}
		}
	}

	return pRetVal;
}

//
//
//
itemType_e CMethodInterpreterDevice::GetItemType( long lKind )
{
	itemType_e itemTypeRetVal = iT_ReservedZeta;
	
	try
	{
		switch( lKind )
		{
			case PT_VAR:
			case PT_ENUM:
			case PT_ENUM_BIT:
			{
				itemTypeRetVal = iT_Variable;
				break;
			}
			case PT_METHOD:
			{
				itemTypeRetVal = iT_Method;
				break;
			} 
			case PT_IARRAY:
			{
				itemTypeRetVal = iT_ItemArray;
				break;
			} 
			case PT_COLLECT:
			{
				itemTypeRetVal = iT_Collection;
				break;
			}
			case PT_FILE:
			{
				itemTypeRetVal = iT_File;
				break;
			}
			case PT_ARRAY:
			{
				itemTypeRetVal = iT_Array;
				break;
			} 
			case PT_REC:
			{
				itemTypeRetVal = iT_Record;
				break;
			} 
			case PT_MENU:
			{
				itemTypeRetVal = iT_Menu;
				break;
			}
			case PT_GRID:
			{
				itemTypeRetVal = iT_Grid;
				break;
			}
			case PT_AXIS:
			{
				itemTypeRetVal = iT_Axis;
				break;
			}
			case PT_SOURCE:
			{
				itemTypeRetVal = iT_Source;
				break;
			}
			case PT_WAVE_FORM:
			{
				itemTypeRetVal = iT_Waveform;
				break;
			}
			case PT_CHART:
			{
				itemTypeRetVal = iT_Chart;
				break;
			}
			case PT_GRAPH:
			{
				itemTypeRetVal = iT_Graph;
				break;
			}
			case PT_IMAGE:
			{
				itemTypeRetVal = iT_Image;
				break;
			}
			case PT_EDIT_DISP:
			{
				itemTypeRetVal = iT_EditDisplay;
				break;
			}
			case PT_COMMAND:
			{
				itemTypeRetVal = iT_Command;
				break;
			}
			case PT_DDLIST:
			{
				itemTypeRetVal = iT_List;
				break;
			}
			case PT_PLIST:
			{
				itemTypeRetVal = iT_VariableList;
				break;
			}
			default:
			{
				CStdString strLog;
				strLog.Format(L"Unsupported type [%d] called in GetItemType()", lKind);
				MILog(strLog.GetBuffer(), nsConsumer::Error, L"MI", __FILE__, __LINE__);
				break;
			}
		}
	}
	catch(...)
	{
	}
	return itemTypeRetVal;
}

//
//
//
RETURNCODE CMethodInterpreterDevice::getItemBySymName ( string& symName, hCitemBase** ppReturnedItempointer )
{
	RETURNCODE nRetVal = FAILURE;

	//this is a bug fix, it seems the parser does not remove all leading/trailing spaces.
	CStdString sName(symName.data());
	sName.Trim();

	CItemMapping *pItemMapping = GetItemMapObject( sName );

	if( pItemMapping )//do we have a mapping
	{
		nRetVal = getItemBySymNumber( pItemMapping->m_lItemID, ppReturnedItempointer );
	}
	return nRetVal;
}

//
// Get the data
//
RETURNCODE CMethodInterpreterDevice::getItemBySymNumber( itemID_t symNum, hCitemBase** ppReturnedItempointer )
{
	RETURNCODE nRetVal = FAILURE;

	//look up the symNum from the symbol table for a simple item or a group item
	CItemMapping *pItemMapping = GetItemMapObject(symNum);

	if( pItemMapping )//do we have a mapping?
	{
		if( pItemMapping->m_pItemPointer )//does the item pointer already exist
		{
			*ppReturnedItempointer = pItemMapping->m_pItemPointer;//yes, then return it.
			nRetVal = SUCCESS;//and return success
		}
		else	//No, we need to create a new item pointer and add it to our list
		{
			// add item data pointer. it could be a simple or group item. A specific item could be added.
			pItemMapping->m_pItemPointer = GetItemPointer( pItemMapping->m_eItemType, symNum, pItemMapping->m_sName );
				
			//look up the symNum from the symbol table for the specific item
			if( pItemMapping->m_pItemPointer )//were we successful in creating an item mapping and pointer?
			{
				*ppReturnedItempointer = pItemMapping->m_pItemPointer;//Yes, good, then return it.
				nRetVal = SUCCESS;//and return success
			}
		}
	}
	else
	{
		CStdString strLog;
		strLog.Format(L"CMethodInterpreterDevice::getItemBySymNumber(%d) pItemMapping is NULL.", symNum);
		MILog(strLog.GetBuffer(), nsConsumer::Warning, L"MI", __FILE__, __LINE__);
	}

	return nRetVal;
}

//
// Instatiate the appropriate class for the appropriate item type
//
hCitemBase* CMethodInterpreterDevice::GetItemPointer( itemType_e itemType, symbolNumb_t symNum, CStdString sName )
{
	hCitemBase* pRetVal=NULL;
	switch( itemType )//what type of item is this
	{
		case iT_Array:
			{
				pRetVal = GetValueArrayPointer( symNum );
			}
			break;
		case iT_ItemArray:
			{
				pRetVal = GetArrayPointer( symNum );
			}
			break;
		case iT_Method:
			{
				pRetVal = GetMethodPointer( symNum, sName );
			}
			break;
		case iT_Variable:
			{
				pRetVal = GetParameterPointer( symNum );
			}
			break;
		case iT_Collection:
			{
				pRetVal = GetCollectionPointer( symNum );
			}
			break;
		case iT_File:
			{
				pRetVal = GetFilePointer( symNum );
			}
			break;
		case iT_Record:
			{
				pRetVal = GetRecordPointer( symNum, sName );
			}
			break;
		case iT_Command:
			{
				pRetVal = GetCommandPointer( symNum, sName );
			}
			break;
		case iT_List:
			{
				pRetVal = GetListPointer( symNum, sName );
			}
			break;
		case iT_Chart:
			{
				pRetVal = GetChartPointer( symNum );
			}
			break;
		case iT_Graph:
			{
				pRetVal = GetGraphPointer( symNum );
			}
			break;
		case iT_Waveform:
			{
				pRetVal = GetWaveformPointer( symNum );
			}
			break;
		case iT_Source:
			{
				pRetVal = GetSourcePointer( symNum );
			}
			break;
		case iT_Axis:
		case iT_Grid:
		case iT_Image:
		case iT_Menu:
		case iT_EditDisplay:
		case iT_VariableList:
			{
				pRetVal = GetGenericPointer( symNum, itemType );	//the same as axis
			}
			break;
		default:
			{
				MILog(L"CSelectUpcall::HideWindow(...) m_pListBox is NULL.", nsConsumer::Error, L"MI", __FILE__, __LINE__);
			}
			break;
	}

	return pRetVal;
}

//
// This is a work around attempt to support "MUNGE" values.  The HCF refuses to support this
// but if we want the Method interpreter to support Rev4 tokenized DD's we will need to support this.
//
hCitemBase* CMethodInterpreterDevice::GetRecordPointer( symbolNumb_t symNum, CStdString sName )
{
	hCRecord* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_Record;
	item.itemName = TStr2AStr(sName.GetBuffer(0));	// convert to narrow

	pReturnedItempointer = new hCRecord( devHndl(), &item, sName );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetGenericPointer( symbolNumb_t symNum, itemType_e itemType )
{
	hCaxis* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = itemType;

	pReturnedItempointer = new hCaxis( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetCollectionPointer( symbolNumb_t symNum )
{
	hCcollection* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_Collection;

	pReturnedItempointer = new hCcollection( devHndl(), &item );


	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetCommandPointer( symbolNumb_t symNum, CStdString sName )
{
	hCcommand* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_Command;
	item.itemName = TStr2AStr(sName.GetBuffer(0));	// convert to narrow

	pReturnedItempointer = new hCcommand( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetFilePointer( symbolNumb_t symNum )
{
	hCfile* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_File;

	pReturnedItempointer = new hCfile( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetValueArrayPointer( symbolNumb_t symNum )
{
	hCarray* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_Array;

	pReturnedItempointer = new hCarray( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetArrayPointer( symbolNumb_t symNum )
{
	hCitemArray* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_ItemArray;

	pReturnedItempointer = new hCitemArray( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetListPointer( ITEM_ID itemID, CStdString sName )
{
	hClist* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = itemID;
	item.itemType = iT_List;
	item.itemName = TStr2AStr(sName.GetBuffer(0));	// convert to narrow

	pReturnedItempointer = new hClist( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetChartPointer( ITEM_ID itemID )
{
	hCchart* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = itemID;
	item.itemType = iT_Chart;

	pReturnedItempointer = new hCchart( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetGraphPointer( ITEM_ID itemID )
{
	hCgraph* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = itemID;
	item.itemType = iT_Chart;

	pReturnedItempointer = new hCgraph( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetWaveformPointer( ITEM_ID itemID )
{
	hCwaveform* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = itemID;
	item.itemType = iT_Chart;

	pReturnedItempointer = new hCwaveform( devHndl(), &item );

	return pReturnedItempointer;
}

//
//
//
hCitemBase* CMethodInterpreterDevice::GetSourcePointer( ITEM_ID itemID )
{
	hCsource* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = itemID;
	item.itemType = iT_Chart;

	pReturnedItempointer = new hCsource( devHndl(), &item );

	return pReturnedItempointer;
}

//
// 
//
hCitemBase* CMethodInterpreterDevice::GetMethodPointer( symbolNumb_t symNum, CStdString sName )
{
	hCitemBase* pReturnedItempointer = NULL;

	aCitemBase item;
	item.itemId = symNum;
	item.itemType = iT_Method;
	item.itemName = TStr2AStr(sName.GetBuffer(0));	// convert to narrow
	
	pReturnedItempointer = new hCmethod( devHndl(), &item );

	return pReturnedItempointer;
}

//
// This instantiates a parameter object pointer in the appropriate class for its type.
//
hCitemBase* CMethodInterpreterDevice::GetParameterPointer( symbolNumb_t symNum )
{
	hCitemBase* pReturnedItempointer = NULL;

	CValueVarient vtType{};

	int iRetVal = ReadParameterData2( &vtType, _T("Type"), 0, 0, symNum, 0 );
	if( iRetVal == BLTIN_SUCCESS )
	{
		aCitemBase item;
		item.itemId = symNum;
		item.itemType = iT_Variable;

		
		switch( CV_I4(&vtType) )
		{
			case vT_Enumerated:
				pReturnedItempointer = new hCEnum( devHndl(), &item );
				break;
			case vT_BitEnumerated:
				pReturnedItempointer = new hCBitEnum( devHndl(), &item );
				break;
			case vT_FloatgPt:
				pReturnedItempointer = new hCFloat( devHndl(), &item );
				break;
			case vT_Double:
				pReturnedItempointer = new hCDouble(devHndl(), &item );
				break;
			case vT_Unsigned:
			case vT_Boolean:
			case vT_Duration:
			case vT_Time:
				pReturnedItempointer = new hCUnsigned( devHndl(), &item );
				break;
			case vT_Integer:
				pReturnedItempointer = new hCSigned( devHndl(), &item );
				break;
			case vT_BitString:
			case vT_OctetString:	//sequence of unformatted data bytes 
				pReturnedItempointer = new hCBitString( devHndl(), &item );
				break;
			case vT_Ascii:
			case vT_VisibleString:	//FF
			case vT_EUC:
				pReturnedItempointer = new hCascii( devHndl(), &item );
				break;
			case vT_PackedAscii:
				pReturnedItempointer = new hCpackedAscii( devHndl(), &item );
				break;
			case vT_Password:
				pReturnedItempointer = new hCpassword( devHndl(), &item );
				break;
			case vT_HartDate:
				pReturnedItempointer = new hChartDate( devHndl(), &item );
				break;
			case vT_DateAndTime:
				pReturnedItempointer = new hCDateAndTime( devHndl(), &item );
				break;
					  //kludge - HART and FF both have a time_value, so we defined 255 as time_value in the HART Server
			case 255: //see ServerProjects\dds\DDLDefs.h for the define of HART_TIME_VALUE
			case vT_TimeValue:
				pReturnedItempointer = new hCTimeValue( devHndl(), &item );
				break;
			case vT_Index:
				pReturnedItempointer = new hCindex( devHndl(), &item );
				break;
			default:
				MILog(L"CMethodInterpreterDevice::GetParameterPointer(...) Variable Type is not defined properly.", nsConsumer::Error, L"MI", __FILE__, __LINE__);
				break;
		}

	}

	return pReturnedItempointer;
}



//
// This is used to resolve which century we are in when we get a two digit
// date from a HART device
//
void CMethodInterpreterDevice::getStartDate(dmDate_t& dt)
{
		struct tm *pTime=NULL;
        time_t long_time;

        time( &long_time ); 
        pTime = localtime( &long_time );

		dt.day = (unsigned char)pTime->tm_mday;
		dt.mnth = (unsigned char)pTime->tm_mon;
		dt.year = (unsigned char)pTime->tm_year;
}

//
//  This gets a list of items of a particular type... specifically it is used to get a list of commands
// to resolve a command for each Var
//
hCobject* CMethodInterpreterDevice::getListPtr(CitemType  it)
{
	hCobject *hRetVal = NULL;

	switch( it.getType() )
	{
		case iT_Command:
			hRetVal = m_pCommandList;
			break;
		default:
			MILog(L"CMethodInterpreterDevice::getListPtr(...) it.getType() is not set correctly.", nsConsumer::Error, L"MI", __FILE__, __LINE__);
			break;
	}
	
	return hRetVal;
}

//
//
//
RETURNCODE CMethodInterpreterDevice::executeMethod(hCmethodCall /*oneMethod*/, CValueVarient& /*returnValue*/)
{
	RETURNCODE nRetVal = FAILURE;

	MILog(L"CMethodInterpreterDevice::executeMethod(...) method is not implemented.", nsConsumer::Error, L"MI", __FILE__, __LINE__);
	return nRetVal;
}


//This function finds the pointer to element or member item by upstream itme ID and member ID. The pointer shall be used to get item property only
int CMethodInterpreterDevice::getElementOrMemberPointer(unsigned long ulItemId, unsigned long ulMemberId, hCitemBase **pIB)
{
	CValueVarient vtItemId{};
	int iRetVal = ReadParameterData2( &vtItemId, _T("ItemID"), 0, 0, ulItemId, ulMemberId );

	*pIB = NULL;
	if (iRetVal == SUCCESS)
	{
		
	
		iRetVal = getItemBySymNumber(CV_UI4(&vtItemId), pIB);
	}

	return iRetVal;
}

bool CMethodInterpreterDevice::LockMethodUIEvent(unsigned long milliSeconds)
{
	bool bEventSignaled = false;
	std::unique_lock <std::mutex> condVarLock(m_methodLock);

	if (milliSeconds == 0)
	{
		// wait until object is signalled
		m_methodCondVariable.wait(condVarLock, []{return m_bmethodProcessed;});
		bEventSignaled = true;
	}
	else
	{
		// wait until object is signalled or timout occurred
		if (m_methodCondVariable.wait_for(condVarLock, std::chrono::milliseconds(milliSeconds), []
			{return m_bmethodProcessed;}))
		{
			bEventSignaled = true;
		}
	}

	return bEventSignaled;
}

void CMethodInterpreterDevice::UnlockMethodUIEvent()
{
	m_bmethodProcessed = true;
	// unlock and notify the waiting thread
	std::unique_lock <std::mutex> condVarLock(m_methodLock);
	condVarLock.unlock();
	m_methodCondVariable.notify_one();	
}

void CMethodInterpreterDevice::ResetMethodUIEvent()
{
	//reset the flag
	m_bmethodProcessed = false;
}

