
/**
 *	@(#) $Id: cm_loc.h,v 1.30 1996/09/13 19:52:04 kimwolk Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_loc.h - Connection Manager Local Definitions
 *
 *	This file contains all internal definitions for
 *	the Connection Manager Library.
 */

#pragma once

#include "cm_rod.h"
#include "Inf/NtcSpecies/Ntcassert2.h"
#include "sym.h"
#include "map_dict.h"



typedef struct {
	LPTSTR           			tag;
	ABT_NET_SPEC				abt_net_spec ;
	unsigned int   				param_count;
	UINT32						dd_blk_id;
	int							dd_blk_tbl_offset;
	void						*app_info;
	DEVICE_HANDLE				active_dev_tbl_offset;
  	CM_STATUS				 	block_comm_status ;
	CM_OP						block_operation ;
	CM_DB_HANDLE				hDBHandle;
	long						m_lGenericRefCount;
} ACTIVE_BLK_TBL_ELEM;

typedef struct {
	int						  count;
	ACTIVE_BLK_TBL_ELEM		**list;
} ACTIVE_BLK_TBL;

	
typedef struct {
	int			 				station_address ;
	NETWORK_HANDLE				network;
	HART_ADDR_TYPE				AddressType ;
	void						*app_info;
	DEVICE_TYPE_HANDLE			active_dev_type_tbl_offset;
  	CM_STATUS  					device_comm_status ;
	CM_OP						device_operation ;
	CM_TIME						deviceTime ;
	CM_VIEW						deviceView;
	LPTSTR						device_tag ;
	LPTSTR						desc;
	LPTSTR						date;
	CM_IDENTIFIED_TYPE			identified;
	int							usage ;
} ACTIVE_DEV_TBL_ELEM;

typedef struct {
	int						  count;
	ACTIVE_DEV_TBL_ELEM		**list;
} ACTIVE_DEV_TBL;

/* Active Device Type Table */

typedef struct {
	DD_DEVICE_ID    dd_device_id;
	ROD_HANDLE		dd_rod_handle;
	void           	*dd_dev_tbls;
	SYMINFO		   	*syminfo;
	CM_COMPAT		compatibility ;
	DEVICE_MANUFACTURER_HANDLE	active_dev_manufacturer_tbl_offset;
	int				usage;
} ACTIVE_DEV_TYPE_TBL_ELEM;

typedef struct {
	int							  count;
	ACTIVE_DEV_TYPE_TBL_ELEM	**list;
} ACTIVE_DEV_TYPE_TBL;


class ActiveDevManufacturerTblElem
{
public:				
	void				SetDctUsage(int u);
	int					GetDctUsage();

	void				SetDictionaryTableCreated(bool created);
	bool				GetDictionaryTableCreated();

	void				SetDictionaryTable(DictionaryTable* dic_tbl);
	DictionaryTable*	GetDictionaryTable();

	bool				SetDeviceManufacturerID(DD_MANUFACTURER_ID *dmi);
	DD_MANUFACTURER_ID*	GetDeviceManufacturerID();

	void				SetMfgDictPath(const wchar_t* pDictPath);
	wchar_t *			GetMfgDictPath();

	ActiveDevManufacturerTblElem();
	~ActiveDevManufacturerTblElem();

private:
	bool				m_bDct_tbl_created;
	int					m_iDct_usage;
	DictionaryTable		*m_pDictionary_tbl;
	DD_MANUFACTURER_ID	m_Manufacturer_id;	
    wchar_t				m_MfgDictPath[MAX_PATH] ; // the Manufacturer Dictionary Path

};

class ActiveDevManufacturerTbl
{
public:
	ActiveDevManufacturerTbl();
	~ActiveDevManufacturerTbl();

	bool	DeviceManufacturerHandleIsValid(DEVICE_MANUFACTURER_HANDLE dmh);

	int		AddElement(DEVICE_MANUFACTURER_HANDLE *device_manufacturer_handle,
												DD_MANUFACTURER_ID *dd_manufacturer_id);
	void	RemoveElement(DEVICE_MANUFACTURER_HANDLE device_manufacturer_handle);

	ActiveDevManufacturerTblElem *	GetElement(DEVICE_MANUFACTURER_HANDLE dmh);
	DEVICE_MANUFACTURER_HANDLE	SearchDeviceManufacturer(DD_MANUFACTURER_ID *dd_manufacturer_id);

private:
	int								count; // # of device manufacturer table elements
	ActiveDevManufacturerTblElem	**list;

};

