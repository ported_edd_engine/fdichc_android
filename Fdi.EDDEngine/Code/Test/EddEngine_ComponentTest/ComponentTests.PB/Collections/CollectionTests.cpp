#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_requesting_a_COLLECTION_OF_VARIABLES_with_PB
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_collection_item_id;
            static ITEM_ID* test_collection_member_item_id;
            static FLAT_COLLECTION* test_collection;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_000D.DDL.bin");                
                engine->Initialize();                

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_collection", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_COLLECTION);

                test_collection = dynamic_cast<FLAT_COLLECTION*>(genericItem->item);

                // get the item ids for all parameters
                test_collection_item_id = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_collection", test_collection_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_collection_member_item_id = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_variable_in_collection", test_collection_member_item_id);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_collection_item_id;
                delete genericItem;
                delete test_collection_member_item_id;
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_collection()
            {
                Assert::AreEqual(test_collection->id, *test_collection_item_id );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_number_of_members()
            {
                Assert::AreEqual(1u, (UINT32)test_collection->op_members.count );
            }

            [TestMethod, TestCategory("CollectionTest")]
            void It_should_return_the_correct_item_id_for_the_first_member()
            {
                Assert::AreEqual(*test_collection_member_item_id, test_collection->op_members.list[0].ref.desc_id );
            }

        };

    }
}
