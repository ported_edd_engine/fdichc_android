/*************************************************************************************************
 *
 * $Workfile: ddbCollAndArr.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the hCcollection and hCitemArray classes
 *		01/28/03	sjv	created 
 */
#include "stdafx.h"
#include <ddbCollAndArr.h>
#include "ddbDevice.h"
#include <CMethodInterpreterDevice.h>
#include <3rdParty/panic.h>
#include "varientTag.h"


/*=============================== support code ============================*/
void hCvarGroupTrail::clear(void)
{
	if (pSourceTrail)    
	{	pSourceTrail->clear();    delete pSourceTrail;    pSourceTrail = NULL;}
	if (pLookupTrail)    
	{	pLookupTrail->clear();    delete pLookupTrail;    pLookupTrail = NULL;}
	if (pExprTrail)    
	{	pExprTrail->clear();      delete pExprTrail;      pExprTrail = NULL;}
	if ( ! lablStr.empty() ) lablStr.erase(); 
	if ( ! helpStr.empty() ) helpStr.erase();
	itmID=0x0000;   containerNumber=0; 
	containerDesc.erase(); containerHelp.erase();
	itmPtr=NULL;
}

bool hCvarGroupTrail::isEmpty(void)
{
	return(itmID==0 && itmPtr==NULL);
}

hCvarGroupTrail& hCvarGroupTrail::operator=(const hCvarGroupTrail& src)
{
	itmID =src.itmID;
	itmPtr=src.itmPtr;
	if (! src.lablStr.empty())	lablStr = src.lablStr;
	if (! src.helpStr.empty())	helpStr = src.helpStr;
	if (! src.containerDesc.empty())	containerDesc = src.containerDesc;
	if (! src.containerHelp.empty())	containerHelp = src.containerHelp;
	containerNumber=src.containerNumber;

	if (pSourceTrail)    
	{	pSourceTrail->clear();    delete pSourceTrail;    pSourceTrail = NULL;}
	if (src.pSourceTrail)
	{
		pSourceTrail = new hCvarGroupTrail(*src.pSourceTrail);      // copy constructor
	}// else leave it NULL
	
	if (pLookupTrail)    
	{	pLookupTrail->clear();    delete pLookupTrail;    pLookupTrail = NULL;}
	if (src.pLookupTrail)
	{
		pLookupTrail = new hCvarGroupTrail(*src.pLookupTrail);      // copy constructor
	}// else leave it NULL
	
	if (pExprTrail)    
	{	pExprTrail->clear();    delete pExprTrail;    pExprTrail = NULL;}
	if (src.pExprTrail)
	{
		pExprTrail = new hCexpressionTrail(*src.pExprTrail);      // copy constructor
	}// else leave it NULL

	return *this;
}

// the trail for an element or member
hCvarGroupTrail& hCvarGroupTrail::operator=( hCgroupItemDescriptor& src)
{
	RETURNCODE rc = SUCCESS;
	hCvarGroupTrail localGT;
	hCreference localRef(src.getRef());
 
	if ( localRef.resolveID( itmPtr ) == SUCCESS && itmPtr != NULL )
	{
		itmID = itmPtr->getID();
		if (itmPtr->Label(lablStr) != SUCCESS )
		{
			lablStr.erase();
		}
		if (itmPtr->Help(helpStr) != SUCCESS )
		{
			helpStr.erase();
		}
	}

	containerDesc = src.getDesc().procureVal();
	containerHelp = src.getHelp().procureVal();

	containerNumber = src.getIIdx();
	
	//hCvarGroupTrail* pSourceTrail;	
	// leave this since it may already be set correctly
	localRef.destroy();
	return *this;
}

void hCvarGroupTrail::clogSelf(int indent)
{
}

void hCexpressionTrail::clogSelf(int indent)
{
	if (isVar)
	{
	CLOGSPACE<< "      Value:0x"<<hex<< srcIdxVal <<"  from Item 0x"<<srcID<<dec<<endl;
	}
	else
	if ( ! isEmpty() )
	{
	CLOGSPACE<< "Const Value:0x"<<hex<< srcIdxVal <<dec<<endl;
	}// else don't print anything
	else
	{
		clog<<endl;
	}
}





/*************************************************************************************************
 *  The common class for group items
 ************************************************************************************************/

/************************************************************************************************/
// aquires a current (resolved) list of items

RETURNCODE hCgrpItmBasedClass::getList(groupItemPtrList_t& r2grpIL)
{
	return APP_ATTR_NOT_SUPPLIED;
}
	


// gets all possible references for a given index into this list of group-items 
// get the indexed value from all valid lists (used in the unitialized mode - no reads allowed)
//				ref& expr pointers must be destroyed by caller...What is pointed to must NOT
RETURNCODE hCgrpItmBasedClass::getAllByIndex(UINT32 indexValue,HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	return rc;
}

/* aquires a unique list of index values for all possible group-item access */
RETURNCODE hCgrpItmBasedClass::getAllindexValues(UIntList_t& allValidindexes)
{
	RETURNCODE rc = FAILURE;
	return rc;
}


RETURNCODE 
hCgrpItmBasedClass::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	return rc;
}


// note that the device map is required to process ItemRef to ITEMID
RETURNCODE 
hCgrpItmBasedClass::getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	return rc;
}


// check the existance
bool hCgrpItmBasedClass::isInGroup(UINT32 indexValue)
{
	bool retVal = true;
	return retVal;
}


/* Label, Help  and Validity should be handled by the item base class */
hCattrBase*   hCgrpItmBasedClass::newHCattr(aCattrBase* pACattr)  // builder 
{
	return NULL; /* an error */
}


hCgrpItmBasedClass::hCgrpItmBasedClass(DevInfcHandle_t h, aCitemBase* paItemBase) 
				  : hCitemBase(h,paItemBase)
{
}

/*************************************************************************************************
 *  This is a Collection Only Function  ie collection-as-menu
 ************************************************************************************************/
//
//
//
hCcollection::hCcollection( DevInfcHandle_t h, aCitemBase* paItemBase ) : hCgrpItmBasedClass( h, paItemBase )
{
	if( paItemBase )
	{
		itemId = paItemBase->itemId;
	}
}

//
//
//
RETURNCODE hCcollection::Help(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadCollectionData( itemId, _T("Help"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCcollection::Label(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadCollectionData( itemId, _T("Label"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCcollection::getByIndex(UINT32 lMemberID, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		sMoniker.Format(_T("Member!$%lu!ItemID"), lMemberID );
		if( pDevice->ReadCollectionData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(lMemberID);
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			cerr << "ERROR: Array Item getByIndex failed to find index '0x" 
					 << hex << lMemberID << dec <<" elements."<<endl;
		}
		rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
//
//
RETURNCODE hCcollection::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;
	CStdString sName = indexName.c_str();

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		CStdString sIndexName = indexName.c_str();
		sMoniker.Format(_T("Member!%s!ItemID"), sIndexName );
		if( pDevice->ReadCollectionData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			
			CValueVarient vtMemberID{};
			sMoniker.Format(_T("Member!%s!MemberId"), sIndexName );
			if( pDevice->ReadCollectionData( itemId, sMoniker, &vtMemberID ) ) 
			pGid->setIndex(CV_UI4(&vtMemberID));
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

/*Vibhor 131004: End of Code*/




/*************************************************************************************************
 *  The individual elements
 ************************************************************************************************/

hCgroupItemDescriptor::~hCgroupItemDescriptor()
{
	ref.destroy();
	memName.erase();
	descS.destroy();
	helpS.destroy();
}

void hCgroupItemDescriptor::clear(void)
{	
	itmIdx = 0; 
	ref.clear(); 
	descS.clear(); 
	helpS.clear();
	memName.erase();
}

void hCgroupItemDescriptor::setIndex(unsigned long newIndex)
{
	itmIdx = newIndex;
}

void hCgroupItemDescriptor::setDesc(wstring newDesc)
{
	descS.setStr(newDesc);
}

hCgroupItemDescriptor& hCgroupItemDescriptor::operator=(const hCgroupItemDescriptor& giD)
{	
	itmIdx = giD.itmIdx;
	ref    = giD.ref;
	descS  = giD.descS;
	helpS  = giD.helpS;
	memName= giD.memName;
	return *this;
}

hCgroupItemDescriptor& hCgroupItemDescriptor::operator=(const hCreference& inRef)
{	
	itmIdx = -1;       /* you must set the correct index separately */
	ref    = inRef;
	descS.clear();		
	helpS.clear(); 
	memName.erase();
	return *this;
}

void hCgroupItemDescriptor::setEqual(void* pAitem)
{
}

void hCgroupItemDescriptor::setDuplicate(hCgroupItemDescriptor& rGid)
{
}

//
//
//
hCitemArray::hCitemArray(DevInfcHandle_t h, aCitemBase* paItemBase) : hCgrpItmBasedClass(h, paItemBase)
{
	if( paItemBase )
	{
		itemId = paItemBase->itemId;
	}
}

//
//
//
RETURNCODE hCitemArray::Help(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadItemArrayData( itemId, _T("Help"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCitemArray::Label(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadItemArrayData( itemId, _T("Label"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCitemArray::getByIndex(UINT32 lMemberID, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CValueVarient vtLabel{};
		CStdString sMoniker;
		sMoniker.Format(_T("Element!%lu!ItemID"), lMemberID );
		if( pDevice->ReadItemArrayData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef(CV_UI4(&vtItemID), 0, iT_Collection, false, rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(lMemberID);
			
			sMoniker.Format(_T("Element!%lu!ItemLabel"), lMemberID );
			if( pDevice->ReadItemArrayData( itemId, sMoniker, &vtLabel ) )
			{
				wstring newDesc;
				if( vtLabel.vTagType == ValueTagType::CVT_WSTR)
				{
					newDesc = CV_WSTR(&vtLabel);
					pGid->setDesc( newDesc );
				}
			}
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
//
//
RETURNCODE hCitemArray::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;
	CStdString sName = indexName.c_str();

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		CStdString sIndexName = indexName.c_str();
		sMoniker.Format(_T("Element!%s!ItemID"), sIndexName );
		if( pDevice->ReadItemArrayData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			
			CValueVarient vtMemberID{};
			sMoniker.Format(_T("Element!%s!MemberId"), sIndexName );
			if( pDevice->ReadItemArrayData( itemId, sMoniker, &vtMemberID ) ) 
			pGid->setIndex(CV_UI4(&vtMemberID));
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
//
//
hCitemArrayValues::hCitemArrayValues( DevInfcHandle_t h, long lItemID ):hCitemArray(h,0)
{
	m_lItemID = lItemID;
	itemType = iT_ItemArray;
}

//
//
//
itemType_t hCitemArrayValues::getIType(void)
{
	itemType_t eRetVal = iT_Variable;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		sMoniker.Format(_T("IndexedItemArray!ItemID") );
		if( pDevice->ReadParameterData2( &vtItemID, sMoniker, 0, 0, m_lItemID, 0  ) == BLTIN_SUCCESS ) 
		{
			if( vtItemID.vTagType == ValueTagType::CVT_I4 )
			{
				itemId = vtItemID.vValue.varSymbolID;
				eRetVal = iT_ItemArray;
			}
		}
	}
	return eRetVal;
}

/* aquires a unique list of index values for all possible group-item access */
RETURNCODE hCitemArrayValues::getAllindexValues(UIntList_t& allValidindexes)
{
	RETURNCODE rc = FAILURE;

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtValList{};
		CStdString sMoniker;
		sMoniker.Format(_T("INDEXEDVALUELIST") );
		itemID_t itemID = getID();
		if( pDevice->ReadParameterData2( &vtValList, sMoniker, 0, 0, itemID, 0 ) == BLTIN_SUCCESS ) 
		{
			
			

			wchar_t strDelimit[] = L",";
			wchar_t* next_index = const_cast<wchar_t*>(CV_WSTR(&vtValList).c_str());
            wchar_t* index = PS_WcsTok(NULL, strDelimit, &next_index);
			UINT32 indexVal = 0;
			while (index != NULL)
			{
				indexVal = wcstoul(index, NULL, 10);
				allValidindexes.push_back(indexVal);

                index = PS_WcsTok(NULL, strDelimit, &next_index);
			}
			
		}

	}

	return rc;
}

/*************************************************************************************************
 *  This is a File Only Function  ie file-as-menu
 ************************************************************************************************/
//
//
//
hCfile::hCfile( DevInfcHandle_t h, aCitemBase* paItemBase ) : hCgrpItmBasedClass( h, paItemBase )
{
	if( paItemBase )
	{
		itemId = paItemBase->itemId;
	}
}

//
//
//
RETURNCODE hCfile::Help(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadFileData( itemId, _T("Help"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCfile::Label(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadFileData( itemId, _T("Label"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//
//
//
RETURNCODE hCfile::getByIndex(UINT32 lMemberID, hCgroupItemDescriptor** ppGID, bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		sMoniker.Format(_T("Member!$%lu!ItemID"), lMemberID );
		if( pDevice->ReadFileData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef( CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			pGid->setIndex(lMemberID);
			rc = SUCCESS;
		}
	}
	if (rc != SUCCESS)
	{
		if ( !suppress)
		{
			cerr << "ERROR: Array Item getByIndex failed to find index '0x" 
					 << hex << lMemberID << dec <<" elements."<<endl;
		}
		rc = DB_ATTRIBUTE_NOT_FOUND;

		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

//
//
//
RETURNCODE hCfile::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;
	CStdString sName = indexName.c_str();

	if ( ppGID == NULL )
	{
		return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		CStdString sIndexName = indexName.c_str();
		sMoniker.Format(_T("Member!%s!ItemID"), sIndexName );
		if( pDevice->ReadFileData( itemId, sMoniker, &vtItemID ) ) 
		{
			hCreference locRef( devHndl() );
			locRef.setRef(CV_UI4(&vtItemID), 0, iT_Variable, false, rT_Item_id) ;
			(*pGid) = locRef;
			
			CValueVarient vtMemberID{};
			sMoniker.Format(_T("Member!%s!MemberId"), sIndexName );
			if( pDevice->ReadFileData( itemId, sMoniker, &vtMemberID ) )
			{
				pGid->setIndex(CV_UI4(&vtMemberID));
				rc = SUCCESS;
			}
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}
