// stdafx.cpp : source file that includes just the standard includes
// EDD_Engine_Interfaces.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#ifdef _DEBUG		// Force the EDD_Engine_Interfaces.dll to load the MFC library.
					// This changes the DLL unload order so that the EDD_Engine_Interfaces.DLL has a chance to release its static
					// memory before it is falsely reported as leaked, which happens when the MFC DLL unloads.

					// This is done only to make sure that any memory leaks reported are "real" ones, and not just a result of the wrong dll unloading order.

#pragma comment(lib, "mfc140ud.lib")

extern int __stdcall AfxDumpMemoryLeaks();


int UseMFC()
{
	int i = AfxDumpMemoryLeaks();

	return i;
}

#endif

// Reference any additional headers you need in STDAFX.H
// and not in this file
