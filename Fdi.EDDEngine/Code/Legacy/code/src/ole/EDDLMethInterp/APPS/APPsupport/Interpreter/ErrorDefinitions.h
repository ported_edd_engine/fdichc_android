#if !defined(INCLUDE_256D73A7_64E0_40a0_829D_D3354EF44DCC)
#define INCLUDE_256D73A7_64E0_40a0_829D_D3354EF44DCC

#include "typedefs.h"

class CRIDEError;

#if NOT_USED
#define UNUSED_LOCAL(x)					\
	x;						

#define THROW_ERROR(ErrNo)				\
		throw new CRIDEError(			\
			plexAnal->GetRuleName(),	\
			ErrNo,						\
			plexAnal->GetLineNumber()	\
			);

#define THROW_ERROR1(ErrNo,Node)				\
		throw new CRIDEError(			\
			m_pszRuleName,				\
			ErrNo,						\
			Node->GetLineNumber()		\
			);

#define THROW_LEXICAL_ERROR(ErrNo)		\
		throw new CRIDEError(				\
			(_CHAR*)m_szRuleName,		\
			ErrNo,						\
			m_i32LineNo);					
		
#define BEGIN_RETHROW_ERROR			\
	catch(CRIDEError& err)			\
	{								\
		UNUSED_LOCAL(err);		


#define END_RETHROW_ERROR			\
		throw;						\
	}

#define BEGIN_RETHROW_ALL			\
	catch(...)						\
	{								

#define END_RETHROW_ALL				\
		THROW_LEXICAL_ERROR(COMPILE_ERROR_UNKNOWNERROR);	\
	}

#endif	//NOT_USED

/* ErrNo is in unsigned int */
#define ADD_ERROR(ErrNo)							\
	{												\
		if (pvecErrors != nullptr) {				\
			if (!plexAnal->isErrorIn(ErrNo, pvecErrors)) {	\
				CRIDEError* perr = new CRIDEError(	\
					plexAnal->GetRuleName(),		\
					(_INT32)ErrNo					\
				);									\
				pvecErrors->push_back(perr);		\
			}										\
		}											\
		return PARSE_FAIL;							\
	}

/* ErrNo is in unsigned int
   node is a pointer to CGrammarNode instance
 */
#define ADD_EXEC_ERROR(ErrNo, node)						\
	{													\
		if (pvecErrors != nullptr) {					\
			if (!node->isErrorIn(ErrNo, pvecErrors)) {	\
			    _CHAR statementName[RULENAMELEN] = "";	\
				node->Identify(statementName);			\
			    _CHAR *pStatName1 = strchr(statementName, '<');			\
			    _CHAR *pStatName2 = strchr(statementName, '>');			\
				if ((pStatName1 != NULL) && (pStatName2 != NULL)) {		\
					pStatName2[0] = '\0';								\
					pStatName1++;										\
				}														\
				else {													\
					pStatName1 = &statementName[0];						\
				}														\
				CRIDEError* perr = new CRIDEError(		\
					(_CHAR *)pStatName1,				\
					(_INT32)ErrNo						\
				);										\
				pvecErrors->push_back(perr);			\
			}											\
		}												\
		return VISIT_ABORT;								\
	}


//Parser Errors...
/*
*	Totally 4 bytes for errors
*	MSB 1 byte for module within the Parser.
*	LSB 3 bytes for errors within the module
*/

#define LEX_SUCCESS			1
#define LEX_FAIL			0

#define PARSE_SUCCESS		1
#define PARSE_FAIL			0

#define TOKEN_SUCCESS		1
#define TOKEN_FAILURE		0

#define TYPE_SUCCESS		1
#define TYPE_FAILURE		0

#define RET_SUCCESS(X)		((X) == TOKEN_SUCCESS)


#define		UNKNOWN_MODULE			0x00
#define		LEXICAL_ANALYZER		0x01
#define		ASSIGNMENT_PARSER		0x02

// Reserve 0xFxxxxxxx for Runtime errors
//Unknown Module
#define	C_UM_ERROR_UNKNOWNERROR		(_INT32)0x00000001
#define	C_UM_ERROR_LOWMEMORY		(_INT32)0x00000002
#define	C_UM_ERROR_METHODERROR		(_INT32)0x00000003
#define	C_UM_ERROR_MISSINGTOKEN		(_INT32)0x00000004
#define	C_UM_ERROR_COMM_ABORTED		(_INT32)0x00000005
#define	C_UM_ERROR_COMM_RETRY_ABORTED	(_INT32)0x00000006
#define	C_UM_ERROR_UI_CANCELLED		(_INT32)0x00000007
#define	C_UM_ERROR_VAR_ABORTED		(_INT32)0x00000008
#define	C_UM_ERROR_OPER_ABORTED		(_INT32)0x00000009
#define	C_UM_ERROR_METH_CANCELLED	(_INT32)0x0000000A
#define	C_UM_ERROR_DDS_PARAM		(_INT32)0x0000000B		//DDS error or wrong parameter type
#define	C_UM_ERROR_COMM_NO_DEVICE	(_INT32)0x0000000C
#define	C_UM_ERROR_COMM_FAIL_RESPONSE	(_INT32)0x0000000D
#define	C_UM_ERROR_INDEX_OUT_OF_RANGE	(_INT32)0x0000000E

//CLexicalAnalyzer
#define	C_LEX_ERROR_IDLONG			(_INT32)0x01000001
#define	C_LEX_ERROR_ILLEGALCHAR		(_INT32)0x01000002
#define	C_LEX_ERROR_ILLEGALITEM		(_INT32)0x01000003

//CAssignmentParser
#define	C_AP_ERROR_LVALUE			(_INT32)0x02000001
#define	C_AP_ERROR_MISSINGEQ		(_INT32)0x02000002
#define C_AP_ERROR_MISSINGEXP		(_INT32)0x02000003
#define	C_AP_ERROR_MISSINGSC		(_INT32)0x02000004

//CExpParser
#define	C_EP_ERROR_ILLEGALOP		(_INT32)0x03000001
#define	C_EP_ERROR_MISSINGOP		(_INT32)0x03000002
#define	C_EP_ERROR_MISSINGPAREN		(_INT32)0x03000003
#define	C_EP_ERROR_MISSINGSC		(_INT32)0x03000004
#define	C_EP_ERROR_LEXERROR			(_INT32)0x03000005 /* Walt EPM 08sep08 */

//CParser
#define	C_CP_ERROR_HASERRORS		(_INT32)0x04000001

//CSelectionStatement
#define C_IF_ERROR_MISSINGLP		(_INT32)0x05000001
#define C_IF_ERROR_MISSINGEXP		(_INT32)0x05000002
#define C_IF_ERROR_MISSINGRP		(_INT32)0x05000003
#define C_IF_ERROR_MISSINGSTMT		(_INT32)0x05000004

//CIterationStatement
#define C_WHILE_ERROR_MISSINGLP		(_INT32)0x06000001
#define C_WHILE_ERROR_MISSINGEXP	(_INT32)0x06000002
#define C_WHILE_ERROR_MISSINGRP		(_INT32)0x06000003
#define C_WHILE_ERROR_MISSINGSTMT	(_INT32)0x06000004

//CCompoundStatement
#define C_CS_ERROR_MISSINGRBRACK	(_INT32)0x07000001

//CELSEStatement
#define C_ES_ERROR_MISSINGSTMT		(_INT32)0x08000001

//CRuleServiceStatement
#define C_RS_ERROR_MISSINGSCOPE		(_INT32)0x09000001
#define C_RS_ERROR_MISSINGINVOKE	(_INT32)0x09000002
#define C_RS_ERROR_MISSINGLPAREN	(_INT32)0x09000003
#define C_RS_ERROR_MISSINGRNAME		(_INT32)0x09000004
#define C_RS_ERROR_MISSINGRPAREN	(_INT32)0x09000005
#define C_RS_ERROR_MISSINGSC		(_INT32)0x09000006

//CDeclarations
#define C_DECL_ERROR_EXPRESSION		(_INT32)0x0A000001
#define C_DECL_ERROR_LBOX			(_INT32)0x0A000002
#define C_DECL_ERROR_RBOX			(_INT32)0x0A000003
#define C_DECL_ERROR_NUM			(_INT32)0x0A000004
#define C_DECL_ERROR_IDMISSING		(_INT32)0x0A000005
#define C_DECL_ERROR_COMMAMISSING	(_INT32)0x0A000006
#define C_DECL_ERROR_UNKNOWN		(_INT32)0x0A000007

//CTypeCheckVisitor
#define C_TC_ERROR_DIM_MISMATCH		(_INT32)0x0B000000
#define C_TC_ERROR_TYP_MISMATCH		(_INT32)0x0B000001
#define C_TC_ERROR_TYP_NOTIMPL		(_INT32)0x0B000002


#endif