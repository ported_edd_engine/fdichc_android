#include "stdafx.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "MEE.h"


extern void add_textMsg( ACTION_UI_DATA& aud, tchar* pMsg);

/***** Start Abort builtins *****/
int CBuiltIn::abort(tchar *message)
{
	ACTION_UI_DATA structUIData;
	int retval = BI_ERROR;


	if (m_pMeth->GetMethodAbortStatus())
	{
		//if MI is already in abort process, return
		return BI_SUCCESS;
	}

	if (message != nullptr) 
	{
		//fill out userInterfaceDataType = TEXT_MESSAGE, textMessage.pchTextMessage and textMessage.iTextMessageLength
		wchar_t *prompt = new tchar[wcslen(message) + 1];
		retval = m_pDevice->dictionary->get_string_translation(message, /*out*/prompt, (int)wcslen(message) + 1, m_pDevice->device_sLanguageCode);
		if (retval != BLTIN_SUCCESS)
		{
			if (prompt)
			{
				delete [] prompt;
			}
			return retval;
		}

		add_textMsg(structUIData, prompt);
		if (prompt)
		{
			delete [] prompt;
		}
	}
	else
	{
		//may be called by process_abort()
		structUIData.userInterfaceDataType = TEXT_MESSAGE;
		structUIData.textMessage.pchTextMessage = NULL;
	}

	structUIData.bUserAcknowledge=false;
	structUIData.bDelayBuiltin = false;
	structUIData.bMethodAbortedSignalToUI = true;
	structUIData.bDisplayDynamic = false;

	//abort request
	retval = m_pDevice->sendUIMethod(structUIData);
	
	//waiting for user to send acknowledgement
	if ( (retval == BI_SUCCESS) && (structUIData.textMessage.pchTextMessage != NULL) )
	{
		bool bEventSignalled = false;

		bEventSignalled = m_pDevice->LockMethodUIEvent();
	
		//get UI builtin response
		if (bEventSignalled)
		{
			retval = BI_SUCCESS;	//no matter builtin acknowledged or method cancelled
		}
		else
		{
			retval = BI_ERROR;
		}
	}

	//signal is received, event is done used.
	m_pDevice->ResetMethodUIEvent();

	if(structUIData.textMessage.pchTextMessage != NULL)
	{
		delete[] structUIData.textMessage.pchTextMessage; 
		structUIData.textMessage.pchTextMessage = NULL;
	}

	m_pMeth->process_abort();

	return (METHOD_ABORTED);
}

int CBuiltIn::process_abort()
{
	return (abort(NULL));
}

int CBuiltIn::_add_abort_method(long lMethodId)
{
	int iRet = m_pMeth->_add_abort_method (lMethodId);

	return iRet;
}	

int CBuiltIn::_remove_abort_method(long lMethodId)
{
	int iRet = m_pMeth->_remove_abort_method (lMethodId);
	
	return iRet;
}

void CBuiltIn::remove_all_abort()
{
	m_pMeth->remove_all_abort();
}

/*Arun 190505 Start of code*/

int CBuiltIn::push_abort_method(long lMethodId)
{
	int iRet = m_pMeth->_push_abort_method(lMethodId);

	if(iRet != BI_SUCCESS)
	{
		iRet = BI_ERROR;	
	}
	return (iRet);
}

int CBuiltIn::pop_abort_method()
{	

	int iRet = m_pMeth->_pop_abort_method();
		
	if(iRet != BI_SUCCESS)
	{
		iRet = BI_ERROR;	
	}
	return (iRet);
}

/*End of code*/
/* End Abort builtins */
