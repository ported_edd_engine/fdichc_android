/**
 *	@(#) $Id: evl_iary.c,v 1.2 1996/01/04 20:12:32 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  ITEM_ARRAY_ELEMENT_LIST and ITEM_ARRAY_ELEMENT.
 */
#include "stdinc.h"

#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif


/*********************************************************************
 *
 *	Name: ddl_free_itemarray_list
 *	ShortDesc: Free the list of ITEM_ARRAY_ELEMENTs
 *
 *	Description:
 *		ddl_free_itemarray_list will check the ITEM_ARRAY_ELEMENT_LIST pointer and
 *		the list, if the are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero
 *	Inputs:
 *		item_array: pointer to the ITEM_ARRAY_ELEMENT_LIST structure
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		item_array: pointer to the list of ITEM_ARRAY_ELEMENT_LIST with an empty
 *		list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_itemarray_list(
ITEM_ARRAY_ELEMENT_LIST *item_array,
uchar           dest_flag)
{

	int             inc;	/* incrementer */
	ITEM_ARRAY_ELEMENT *temp_element;	/* temp pointer for the element */

	if (item_array == NULL) {
		return;
	}

	if (item_array->list == NULL) {

		ASSERT_DBG(!item_array->count && !item_array->limit);
		item_array->count = 0;
		item_array->limit = 0;
	}
	else {

		/*
		 * Free the STRINGs for each ITEM_ARRAY_ELEMENT
		 */

		temp_element = item_array->list;
		for (inc = 0; inc < item_array->limit; inc++, temp_element++) {
			ddl_free_string(&temp_element->help);
			ddl_free_string(&temp_element->desc);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of ITEM_ARRAY_ELEMENTs
			 */

			free((void *) item_array->list);
			item_array->list = NULL;
			item_array->limit = 0;
		}
		else {

			/**
			 *	Initializing the list of item_array values to 0.
			 *	It is OK to initialize "evaled","index","ref","desc",and "help"
			 */

			memset((char *) item_array->list, (int) 0,
				(size_t) item_array->limit * sizeof(*item_array->list));
		}
		item_array->count = 0;
	}

	return;
}




/*********************************************************************
 *
 *	Name: ddl_shrink_itemarray_list
 *	ShortDesc: Shrink the list of ITEM_ARRAY_ELEMENTs
 *
 *	Description:
 *		ddl_shrink_itemarray_list reallocs the list of ITEM_ARRAY_ELEMENTs to contain
 *		only the ITEM_ARRAY_ELEMENTs being used
 *
 *	Inputs:
 *		item_array: pointer to the ITEM_ARRAY_ELEMENT_LIST structure
 *
 *	Outputs:
 *		item_array: pointer to the resized ITEM_ARRAY_ELEMENT_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static
int
ddl_shrink_itemarray_list(
ITEM_ARRAY_ELEMENT_LIST *item_array)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_ITEM_ARRAY_LIST);
#endif

	if (item_array == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (item_array->list == NULL) {
		ASSERT_DBG(!item_array->count && !item_array->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (item_array->count == item_array->limit) {
		return DDL_SUCCESS;
	}


	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (item_array->count == 0) {
		ddl_free_itemarray_list(item_array, FREE_ATTR);
	}
	else {
		item_array->limit = item_array->count;

		item_array->list = (ITEM_ARRAY_ELEMENT *) realloc((void *) item_array->list,
			(size_t) (item_array->limit * sizeof(ITEM_ARRAY_ELEMENT)));

		if (item_array->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}




/*********************************************************************
 *
 *	Name: ddl_parse_itemarray
 *
 *	ShortDesc: Parse a list of ITEM_ARRAY_ELEMENTs
 *
 *	Description:
 *		ddl_parse_itemarray parses the binary data and loads an ITEM_ARRAY_ELEMENT_LIST
 *		structure is the pointer to the ITEM_ARRAY_ELEMENT_LIST is not NULL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		item_array - pointer to an ITEM_ARRAY_ELEMENT_LIST structure where the result will
 *					be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				  information will be stored.  If this is NULL, no
 *				  dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		item_array - pointer to an ITEM_ARRAY_ELEMENT_LIST structure containing the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from DDL_PARSE_INTEGER(),ddl_parse_string()
 *		ddl_parse_desc_ref(), DDL_PARSE_TAG()
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR, DDL_ENCODING_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/


static int
ddl_parse_itemarray(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ARRAY_ELEMENT_LIST *item_array,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        len, tag;	/* used by parse_tag call */
	ITEM_ARRAY_ELEMENT *temp_item_array;	/* temp pointer for an element */

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_ITEM_ARRAY);
#endif


	while (*size > 0) {

		/*
		 * Parse the next item, and make sure it is an
		 * ITEM_ARRAY_ELEMENT
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != ITEM_ARRAY_ELEMENT_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		if (item_array) {

			/*
			 * Parse a series of ITEM_ARRAY_ELEMENTs.  If we need
			 * more room in the item_array of structures, malloc
			 * more room. Then parse the next ITEM_ARRAY_ELEMENT
			 * structure.
			 */

			if (item_array->count == item_array->limit) {

				item_array->limit += ITEM_ARRAY_INC;

				temp_item_array = (ITEM_ARRAY_ELEMENT *) realloc((void *) item_array->list,
					(size_t) (item_array->limit * sizeof(ITEM_ARRAY_ELEMENT)));

				if (temp_item_array == NULL) {

					item_array->limit = item_array->count;
					ddl_free_itemarray_list(item_array, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_item_array[item_array->count], 0,
					((size_t) ITEM_ARRAY_INC * sizeof(ITEM_ARRAY_ELEMENT)));

				item_array->list = temp_item_array;
			}

			/*
			 * Parse the index, entry reference, description
			 * (desc), and help (if present)
			 */

			temp_item_array = &item_array->list[item_array->count++];

			DDL_PARSE_INTEGER(chunkp, &len, &temp_item_array->index);

			rc = ddl_parse_desc_ref(chunkp, &len, &temp_item_array->ref,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			temp_item_array->evaled |= IA_INDEX_EVALED | IA_REF_EVALED;

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_item_array->desc,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_item_array->evaled |= IA_DESC_EVALED;
			}
			else {

				/*
				 * If a DESCRIPTION string was not found in the
				 * binary, use the default description string
				 * from the standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_DESC,
					&temp_item_array->desc);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_item_array->desc);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}


			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_item_array->help,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_item_array->evaled |= IA_HELP_EVALED;
			}
			else {

				/*
				 * If a HELP string was not found in the
				 * binary, use the default description string
				 * from the standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_HELP,
					&temp_item_array->help);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_item_array->help);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}
		}
		else {		/* item_array is NULL so parse without loading */

			DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);

			/*
			 * Because a value is not desired, and speed is
			 * important, ddl_parse_item_id() is called instead of
			 * ddl_parse_desc_ref().
			 */

			rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, (STRING *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, (STRING *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}

		/*
		 * If there are any new fields, just ignore them.
		 */

		if (len > 0) {
			*chunkp += len;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddl_itemarray_choice
 *	ShortDesc: Choose the correct item_array from a binary.
 *
 *	Description:
 *		ddl_itemarray_choice will parse the binary for an item_array,
 *		according to the current conditionals (if any).  The value of
 *		the item_array items is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an ITEM_ARRAY_ELEMENT_LIST structure where the result will
 *   				be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an ITEM_ARRAY_ELEMENT_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_itemarray().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static
int
ddl_itemarray_choice(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ARRAY_ELEMENT_LIST *list,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp pointer for chunk list */

#ifdef ISPTEST
	TEST_FAIL(DDL_ITEM_ARRAY_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		ELEMENT_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_itemarray(&(chunk_ptr->chunk),
				&(chunk_ptr->size), list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}




/*********************************************************************
 *
 * Name: eval_attr_itemarray
 *
 * ShortDesc: evaluate an item_array
 *
 * Description:
 *
 *	The eval_attr_itemarray function evaluates an item_array.
 *	The buffer pointed to by chunk
 *	should contain an item_array returned from fetch_item_array,
 *	and size should specify its size.
 *	If item_array is not a null pointer, the item_array
 *	is returned in item_array. If depinfo is not
 *	a null pointer, dependency information about the item_array
 *	is returned in depinfo.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      item_array - pointer to an ITEM_ARRAY_ELEMENT_LIST
 *			 where the result will be stored.
 *			  If this is NULL, no result is computed  or stored.
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      item_array - pointer to an ITEM_ARRAY_ELEMENT_LIST structure
 *		containing the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 * Returns:
 *	DDL_SUCCESS, DDL_ENCODING_ERROR, DDL_MEMORY_ERROR
 *	and return codes from ddl_members_choice(), ddl_parse_tag_func()
 *	ddl_parse_integer_func(), ddl_parse_desc_ref(), ddl_parse_string()
 *  ddl_shrink_depinfo(), ddl_shrink_members_list()
 *
 ***************************************************************************/

int
eval_attr_itemarray(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	ITEM_ARRAY_ELEMENT_LIST* item_array = static_cast<ITEM_ARRAY_ELEMENT_LIST*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* data valid flag */

#ifdef PROCESS_IMPORTS
	DDL_UINT        len;	/* used by parse tag */
	DDL_UINT        tag, item_array_tag;	/* temp tag values */
	unsigned long   index;	/* used by redefinition */
	int             match_inc;	/* used by redefinition */
	int             match_position;	/* used by redefinition */
	ITEM_ARRAY_ELEMENT *temp_item_array;	/* temp pointer for member in
						 * list */

#endif


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ITEM_ARRAY);
#endif

	valid = 0;

	ddl_free_itemarray_list(item_array, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	/*
	 * Parse the item_array information
	 */

	rc = ddl_itemarray_choice(&chunk, &size, item_array, depinfo, &valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

#ifdef PROCESS_IMPORTS

	/*
	 * If we are not returning any value , we can skip the additions,
	 * deletions, and redefinitions.
	 */

	if (!item_array) {
		goto exit;
	}

	while (size) {

		/*
		 * Parse the tag to find out if this is a deletion, addition,
		 * or redefinition.
		 */

		rc = ddl_parse_tag_func(&chunk, &size, &tag, &len);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}

		if (len > size) {
			rc = DDL_ENCODING_ERROR;
			goto err_exit;
		}

		/*
		 * Additions and redefinitions, have a tag that must be
		 * ITEM_ARRAY_ELEMENT_TAG
		 */

		if (tag != DELETE_TAG) {
			rc = ddl_parse_tag_func(&chunk, &size, &item_array_tag, &len);

			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			if (item_array_tag != ITEM_ARRAY_ELEMENT_TAG) {
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
		size -= len;

		if (item_array) {

			/*
			 * Find the existing item_array element that this
			 * alteration is referencing.
			 */

			rc = ddl_parse_integer_func(&chunk, &len, &index);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			match_position = item_array->count;
			for (match_inc = 0; match_inc < match_position; match_inc++) {
				if (item_array->list[match_inc].index == index) {
					match_position = match_inc;
				}
			}
			temp_item_array = &item_array->list[match_position];

			switch (tag) {
			case DELETE_TAG:

				/*
				 * Delete the item_array element by freeing any
				 * subreferences and then squishing the list
				 * down and eliminating the deleted structure.
				 */

				if (match_position < item_array->count - 1) {
					memcpy((char *) temp_item_array, (char *) (temp_item_array + 1),
						(size_t) ((item_array->count - match_position - 1) *
							sizeof *temp_item_array));
					memset((char *) &item_array->list[item_array->count - 1], 0,
						sizeof *item_array->list);
				}
				else {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}
				item_array->count--;
				break;

			case REDEFINE_TAG:

				/*
				 * Redefine the item_array elements by parsing
				 * the new item, description, and help
				 */

				if (match_position < item_array->count) {

					rc = ddl_parse_desc_ref(&chunk, &len, &temp_item_array->ref, depinfo,
						env_info, var_needed);

					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}

					if (len > 0) {
						rc = ddl_parse_string(&chunk, &len, &temp_item_array->desc,
							depinfo, env_info, var_needed);
						if (rc != DDL_SUCCESS) {
							goto err_exit;
						}
					}

					if (len > 0) {
						rc = ddl_parse_string(&chunk, &len, &temp_item_array->help,
							depinfo, env_info, var_needed);
						if (rc != DDL_SUCCESS)
							goto err_exit;
					}

					valid = 1;
				}
				else {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}
				break;

			case ADD_TAG:
				if (match_position < item_array->count) {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}

				/*
				 * Expand the ITEM_ARRAY_ELEMENT_LIST if
				 * necessary, and add the new item_array
				 * element.
				 */

				if (item_array->count >= item_array->limit) {

					item_array->limit += ITEM_ARRAY_INC;
					temp_item_array = (ITEM_ARRAY_ELEMENT *) realloc(
						(void *) item_array->list,
						(size_t) (item_array->limit * sizeof *temp_item_array));

					if (!temp_item_array) {
						rc = DDL_MEMORY_ERROR;
						goto err_exit;
					}

					item_array->list = temp_item_array;
					memset((char *) &item_array->list[item_array->count], 0,
						(size_t) ITEM_ARRAY_INC * sizeof *item_array->list);
				}

				/*
				 * Parse the item_array item, description
				 * (desc), and help.
				 */

				temp_item_array = item_array->list + item_array->count;
				item_array->count++;

				temp_item_array->index = index;

				rc = ddl_parse_desc_ref(&chunk, &len, &temp_item_array->ref,
					depinfo, env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}


				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, &temp_item_array->desc,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}


				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, &temp_item_array->help,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				valid = 1;
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
		else {
			rc = ddl_parse_integer_func(&chunk, &len, (DDL_UINT *) NULL_PTR);

			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			switch (tag) {

			case DELETE_TAG:
				break;

			case REDEFINE_TAG:
			case ADD_TAG:
				rc = ddl_parse_desc_ref(&chunk, &len, (DESC_REF *) NULL, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, (STRING *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}

				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, (STRING *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
	}

exit:
#endif
	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_itemarray_list(item_array);
	}
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (item_array && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	ddl_free_itemarray_list(item_array, FREE_ATTR);
	return rc;
}




/*********************************************************************
 *
 *	Name: link_array
 *
 *	Author:
 *		Christian Gustafson
 *
 *********************************************************************/


int
link_array(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        len, tag;	/* used by parse_tag call */



	while (*size > 0) {

		/*
		 * Parse the next item, and make sure it is an
		 * ITEM_ARRAY_ELEMENT
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != ITEM_ARRAY_ELEMENT_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;


		DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);


		rc = link_ref(chunkp, &len, depinfo, env_info, var_needed, 0);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (len > 0) {
			rc = link_string(chunkp, &len, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		if (len > 0) {
			rc = link_string(chunkp, &len, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		/*
		 * If there are any new fields, just ignore them.
		 */

		if (len > 0) {
			*chunkp += len;
		}
	}

	return DDL_SUCCESS;
}
