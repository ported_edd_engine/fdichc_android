/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */
/**
 *		Copyright 1995 - HART Communication Foundation
 *		All rights reserved.
 */

/*
 *	@(#)dds_chk.h	30.4	 30.4  14 Apr 1997
 */

#pragma once

#include "std.h"
#include "table.h"
#include "tags_sa.h"
#include "DDLDEFS.H"
#include "flats.h"

#ifdef __cplusplus
	extern "C" {
#endif /* __cplusplus */

/*
 * Flat item list structure checking macros
 */

#define	CHK_FLAT_LIST(A,M,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_VAR_FLAT_LIST(A,M,F,L)	\
{	\
	if ((A) & (M)) {	\
		if ((L).count) {	\
			ASSERT_DBG((L).limit && (L).list);	\
		}	\
		else {  \
			ASSERT_DBG(((L).limit && (L).list) ||	\
				(!(L).limit && !(L).list));	\
		}	\
	}	\
	else if (F) {	\
		ASSERT_DBG(!(L).count && !(L).limit && !(L).list);	\
	}	\
}

#define	CHK_STRING(A,M,S)	\
{	\
	if ((A) & (M)) {	\
		if ((S).len) {	\
			ASSERT_DBG((S).str);	\
		}	\
	}	\
	else {	\
		ASSERT_DBG(!(S).str && !(S).len);	\
	}	\
}

/*
 * Directory table structure checking macros
 */

#define CHK_BIN_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG((T).chunk && (T).size);	\
	}	\
	else {	\
		ASSERT_DBG(!(T).chunk && !(T).size);	\
	}	\
}

#define CHK_FLAT_TABLE(A,T,M)	\
{	\
	if ((A) & (M)) {	\
		ASSERT_DBG(((T).list && (T).count) ||	\
			(!(T).list && !(T).count));	\
	}	\
	else {	\
		ASSERT_DBG(!(T).list && !(T).count);	\
	}	\
}


#ifdef __cplusplus
    }
#endif /* __cplusplus */

