﻿
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Microsoft.TeamFoundation.Client;
//using Microsoft.TeamFoundation.Framework;
//using Microsoft.TeamFoundation.VersionControl.Client;
using HARTDDSCOM = Emerson.AMS.Public.HARTDDSCOM.Interop;

namespace VerifyXml
{


	/// <summary>
	///This is a test class for MainWindowVMTest and is intended
	///to contain all MainWindowVMTest Unit Tests 
	///This copy is for HART
	///</summary>
	[TestClass()]
	public class MainWindowVMTest
	{
		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section,
				 string key, string def, StringBuilder retVal,
			int size, string filePath);


		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion


        /// <summary>
        /// Save the XML in this filename
        ///</summary>
        public void SaveXMLFile(string sOutputFilename, string sXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sXml);

            XmlTextWriter writer = new XmlTextWriter(sOutputFilename, null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
        }


        /// <summary>
        /// Only Create the XML for all the EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void CreateHARTXMLOnly()
        {
            RunTests(false, "*.fms");
            RunTests(false, "*.fm6");
            RunTests(false, "*.fm8");
        }


        /// <summary>
        /// Only Create the XML for all the EDD files (FF)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestMethod()]
        public void CreateFFXMLOnly()
        {
            RunTests(false, "*.ff5");
            RunTests(false, "*.ffo");
        }

        /// <summary>
        /// Dump only a single Legacy HART Binary
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void DumpSingleLegacyHARTBinary()
        {
            RunTests(true, "*.fm8", @"C:\FDI EDD Engine\Code\Legacy\code\Devices\HART\00001F\0015\0405.fm8");
        }

        /// <summary>
		/// Run the regression tests on the fms EDD files (HART)
		///</summary>
		[DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
		public void HART_fms_RegressionTest()
		{
			RunTests(true, "*.fms");
        }

        /// <summary>
        /// Run the regression tests on the fms EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_HART_fms()
        {
            ReplaceTestExpectedXML("*.fms");
        }

        /// <summary>
        /// Run the regression tests on the fm6 EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void HART_fm6_RegressionTest()
        {
            RunTests(true, "*.fm6");
        }

        /// <summary>
        /// Run the regression tests on the fm6 EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_HART_fm6()
        {
            ReplaceTestExpectedXML("*.fm6");
        }

        /// <summary>
        /// Run the regression tests on the fm8 EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void HART_fm8_RegressionTest()
        {
            RunTests(true, "*.fm8");
        }

        /// <summary>
        /// Run the regression tests on the fm8 EDD files (HART)
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_HART_fm8()
        {
            ReplaceTestExpectedXML("*.fm8");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (HART FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void HART_FDI_RegressionTest()
        {
            RunTests(true, "*.fmA");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (HART FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_HART_FDI()
        {
            ReplaceTestExpectedXML("*.fmA");
        }

        /// <summary>
        /// Run the regression tests on the ffo EDD files (FF)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestMethod()]
        public void FF_ffo_RegressionTest()
        {
            RunTests(true, "*.ffo");
        }

        /// <summary>
        /// Run the regression tests on the ffo EDD files (FF)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_FF_ffo()
        {
            ReplaceTestExpectedXML("*.ffo");
        }

        /// <summary>
        /// Run the regression tests on the ff5 EDD files (FF)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestMethod()]
        public void FF_ff5_RegressionTest()
        {
             RunTests(true, "*.ff5");
        }

        /// <summary>
        /// Run the regression tests on the ff5 EDD files (FF)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FFEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_FF_ff5()
        {
            ReplaceTestExpectedXML("*.ff5");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (FF FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void FF_FDI_RegressionTest()
        {
            RunTests(true, "*.ff6");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (FF FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_FF_FDI()
        {
            ReplaceTestExpectedXML("*.ff6");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (ISA100 FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void ISA100_FDI_RegressionTest()
        {
            RunTests(true, "*.is6");
        }

        /// <summary>
        /// Run the regression tests on all the EDD files (ISA100 FDI)
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_ISA100_FDI()
        {
            ReplaceTestExpectedXML("*.is6");
        }

        /// <summary>
		/// Run the regression tests on all the Profibus EDD files
		///</summary>
		[DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
		public void PB_FDI_RegressionTest()
		{
			RunTests(true, "*.bin");
		}

        /// <summary>
        /// Run the regression tests on all the Profibus EDD files
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void ReplaceTestExpectedXML_PB_FDI()
        {
            ReplaceTestExpectedXML("*.bin");
        }

        /// <summary>
		/// Search through the directory listed in App.config and process all files found with extension provided in 
        /// argment fileSearchPattern
		///</summary>
		public void RunTests(bool bDoCompare, string fileSearchPattern, string ddBinaryFile="----")
		{
			int correct = 0;
			int mistake = 0;
			int FilesProcessed = 0;
            string TestDevicePathAttr = "";

            fileSearchPattern = fileSearchPattern.ToLower();

            switch (fileSearchPattern)
            {
                case "*.ffo":
                case "*.ff5":
                    TestDevicePathAttr = "DevicePath_FF";
                    break;
				case "*.fms":
				case "*.fm6":
				case "*.fm8":
                    TestDevicePathAttr = "DevicePath_Hart";
                    break;
                case "*.ff6":
                    TestDevicePathAttr = "DevicePath_FDI_FF";
                    break;
                case "*.is6":
                    TestDevicePathAttr = "DevicePath_FDI_ISA100";
                    break;
                case "*.fma":
                    TestDevicePathAttr = "DevicePath_FDI_Hart";
                    break;
                case "*.bin":
                    TestDevicePathAttr = "DevicePath_FDI_PB";
                    break;
				default:
					Assert.Fail("fileSearchPattern ({0}) is not supported", fileSearchPattern);
					break;
            }

            Assert.IsTrue(TestDevicePathAttr != "", "AppSettings key not set; tried: " + TestDevicePathAttr);

            string strDevicePath = ConfigurationManager.AppSettings[TestDevicePathAttr];

            Assert.IsTrue(strDevicePath != null, "AppSettings failed, tried: " + TestDevicePathAttr);
            Assert.IsTrue(Directory.Exists(strDevicePath), "AppSettings failed, tried: " + TestDevicePathAttr);

			// recursively search starting at strDevicePath
			try
			{
				foreach (string Mfgr in Directory.GetDirectories(strDevicePath))
				{
					foreach (string DevType in Directory.GetDirectories(Mfgr))
					{
                        foreach (string EDDBinaryFile in Directory.GetFiles(DevType, fileSearchPattern))
						{
                            // If a Binary filename was passed in, and it is not the one found, then continue.
                            if ((ddBinaryFile != "----") && (ddBinaryFile != EDDBinaryFile))
                            {
                                continue;
                            }

							string expectedXml = "";

							if (bDoCompare)		// If we are comparing, read in the expected file.
							{
								// does the expected xml file exist?
								string expectedFile = EDDBinaryFile + ".Expected.xml";

								if (!File.Exists(expectedFile))
								{
									Console.WriteLine("{0} doesn't exist. Skipping.", expectedFile);
									continue;
								}

								// read XML string from expected XML file
								string fileXml = File.ReadAllText(expectedFile);
								expectedXml = Regex.Replace(fileXml, @"\s", "");
							}

							/* read XML string from DDS info */
							string ddsXml = "";      // XML from EDD Engine
							string compareXml = "";  // XML used for comparison

							try
							{
								System.Diagnostics.Trace.TraceInformation("Processing: {0}", EDDBinaryFile);

								HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();


								loader.GetDDSInfo(EDDBinaryFile, out ddsXml);

								Marshal.ReleaseComObject(loader);

								if (bDoCompare)	// If comparing, create xml that is easy to compare
								{
									compareXml = Regex.Replace(ddsXml, @"\s", "");
								}
								else			// If we aren't comparing, just create output file
								{
                                    SaveXMLFile(EDDBinaryFile + ".xml", ddsXml);
                                }

								FilesProcessed++;
							}
							catch (COMException ex)
							{
								mistake++;
								System.Diagnostics.Trace.TraceWarning(String.Format("AddMultipleDDHartTest (xml) - GetDDSInfo({0}), {1}", EDDBinaryFile, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
								continue;
							}

							if (bDoCompare)
							{
								if (String.Compare(compareXml, expectedXml, false) == 0)
								{
									correct++;
									Console.WriteLine("Passed: {0}", EDDBinaryFile);
								}
								else
								{
									mistake++;
									Console.WriteLine("Failed: {0}", EDDBinaryFile);

                                    // If there is an error, save the XML for later study
                                    SaveXMLFile(EDDBinaryFile + ".xml", ddsXml);
								}
							}

						} // file search
					}
				}
			} //try
			catch (Exception e)
			{
				Assert.Fail("The XML test failed with exception: {0}", e.ToString());
			}

			System.Diagnostics.Trace.TraceInformation("Processed: {0} files.", FilesProcessed);

			if ((correct != 0) && (mistake != 0))
			{
				Assert.Inconclusive("INconclusive: {0} number of XML test is failed. And {1} number of XML test is passed", mistake, correct);
			}
			else
			{
				Assert.IsTrue((mistake == 0), "Failed: {0} number of XML test is failed.", mistake);
			}
		}


        private const string tfsServer = @"http://usaust-tfs.emrsn.org:8080/tfs/knoxdevcollection";

        public void CheckOutFromTFS(string fileName)
        {
            //using (TfsTeamProjectCollection tpc = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(new Uri(tfsServer)))
            //{
            //    if (tpc != null)
            //    {
            //        WorkspaceInfo workspaceInfo = Workstation.Current.GetLocalWorkspaceInfo(fileName);
            //        if (null != workspaceInfo)
            //        {
            //            Workspace workspace = workspaceInfo.GetWorkspace(tpc);
            //            workspace.PendEdit(fileName);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Search through the directory listed in App.config and process all files found with extension provided in 
        /// argment fileSearchPattern
        /// This function replaces existing *.Expected.xml files as output.
        ///</summary>
        public void ReplaceTestExpectedXML(string fileSearchPattern)
        {
            int correct = 0;
            int mistake = 0;
            int FilesProcessed = 0;
            string TestDevicePathAttr = "";

            fileSearchPattern = fileSearchPattern.ToLower();

            switch (fileSearchPattern)
            {
                case "*.ffo":
                case "*.ff5":
                    TestDevicePathAttr = "DevicePath_FF";
                    break;
                case "*.fms":
                case "*.fm6":
                case "*.fm8":
                    TestDevicePathAttr = "DevicePath_Hart";
                    break;
                case "*.ff6":
                    TestDevicePathAttr = "DevicePath_FDI_FF";
                    break;
                case "*.is6":
                    TestDevicePathAttr = "DevicePath_FDI_ISA100";
                    break;
                case "*.fma":
                    TestDevicePathAttr = "DevicePath_FDI_Hart";
                    break;
                case "*.bin":
                    TestDevicePathAttr = "DevicePath_FDI_PB";
                    break;
                default:
                    Assert.Fail("fileSearchPattern ({0}) is not supported", fileSearchPattern);
                    break;
            }

            Assert.IsTrue(TestDevicePathAttr != "", "AppSettings key not set; tried: " + TestDevicePathAttr);

            string strDevicePath = ConfigurationManager.AppSettings[TestDevicePathAttr];

            Assert.IsTrue(strDevicePath != null, "AppSettings failed, tried: " + TestDevicePathAttr);
            Assert.IsTrue(Directory.Exists(strDevicePath), "AppSettings failed, tried: " + TestDevicePathAttr);

            // recursively search starting at strDevicePath
            try
            {
                foreach (string Mfgr in Directory.GetDirectories(strDevicePath))
                {
                    foreach (string DevType in Directory.GetDirectories(Mfgr))
                    {
                        foreach (string EDDBinaryFile in Directory.GetFiles(DevType, fileSearchPattern))
                        {
                            // read XML string from expected XML file
                            string expectedFile = EDDBinaryFile + ".Expected.xml";

                            // does the expected xml file exist?
                            if (!File.Exists(expectedFile))
                            {
                                Console.WriteLine("{0} doesn't exist. Skipping.", expectedFile);
                                continue;
                            }

                            string fileXml = File.ReadAllText(expectedFile);
                            string expectedXml = Regex.Replace(fileXml, @"\s", "");

                            /* read XML string from DDS info */
                            string ddsXml = "";      // XML from EDD Engine
                            string compareXml = "";  // XML used for comparison

                            try
                            {
                                System.Diagnostics.Trace.TraceInformation("Processing: {0}", EDDBinaryFile);

                                HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();


                                loader.GetDDSInfo(EDDBinaryFile, out ddsXml);

                                Marshal.ReleaseComObject(loader);
                                compareXml = Regex.Replace(ddsXml, @"\s", "");

                                FilesProcessed++;   //for info display only
                            }
                            catch (COMException ex)
                            {
                                mistake++;
                                System.Diagnostics.Trace.TraceWarning(String.Format("AddMultipleDDHartTest (xml) - RenameTests({0}), {1}", EDDBinaryFile, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                                continue;
                            }

                            if (String.Compare(compareXml, expectedXml, false) != 0)
                            {
                                //If the output XML is different from the expected, replace the expected with the XML
                                CheckOutFromTFS(expectedFile);
                                SaveXMLFile(expectedFile, ddsXml);
                                correct++;
                            }
                            else
                            {
                                //else skipping
                                mistake++;
                            }
                        } // file search
                    }
                }
            } //try
            catch (Exception e)
            {
                Assert.Fail("The XML test failed with exception: {0}", e.ToString());
            }

            System.Diagnostics.Trace.TraceInformation("Processed: {0} files.", FilesProcessed);

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of XML test is skipped. And {1} number of XML test is processed", mistake, correct);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of XML test is failed or skipped.", mistake);
            }
        }
        
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        public void RenameXMLs()
        {
            string ddsXml;
            string path;

            StringBuilder sbDevicePath = new StringBuilder(300);

            int iCnt = GetPrivateProfileString("INSTALLATION",
                                                "DevicePath",
                                                "",
                                                sbDevicePath,
                                                300,
                                                @"C:\Windows\fms.ini");

            Assert.IsTrue(iCnt != 0, "GetPrivateProfileString() failed, check fms.ini file");

            string strDevicePath = sbDevicePath.ToString();


            /* recursively search .fm8 files from Hart device directory */
            try
            {
                foreach (string d in Directory.GetDirectories(strDevicePath))
                {
                    foreach (string dd in Directory.GetDirectories(d))
                    {
                        foreach (string f in Directory.GetFiles(dd, "*.fm8"))
                        {
                            /* is the expected xml file existing? */

                            // Code to delete .xml files
                            //path = f.Replace(".fm8", ".Expected.xml");
                            //if (File.Exists(path))
                            //{
                            //    File.SetAttributes(path, FileAttributes.Normal);
                            //    File.Delete(path);
                            //}

                            // Code to rename Expected_All_New.xml to Expcted_All.xml
                            //string strExpectedAllNew = f.Replace(".fm8", ".Expected_All_New.xml");
                            //if (File.Exists(strExpectedAllNew))
                            //{
                            //    string strExpectedAll = strExpectedAllNew.Remove(strExpectedAllNew.Length - 8, 4);
                            //    File.Move(strExpectedAllNew, strExpectedAll);
                            //}

                            // Code to create xml in proper format and rename to Expected.xml
                            /* is the expected xml file existing? */
                            //string path1 = f.Replace(".fm8", ".Expected_All.xml");
                            path = f.Replace(".fm8", ".Expected_All.xml");
                            if (File.Exists(path))
                            {



                                /* read XML string from DDS info */
                                try
                                {
                                    HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();
                                    loader.GetDDSInfo(f, out ddsXml);
                                    Marshal.ReleaseComObject(loader);

                                    FileStream pFileName = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);

                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(ddsXml);
                                    doc.Save(pFileName);

                                    pFileName.Close();
                                    //   xml = Regex.Replace(ddsXml, @"\s", "");
                                }
                                catch (COMException ex)
                                {
                                    System.Diagnostics.Trace.TraceWarning(String.Format("AddMultipleDDHartTest (xml) - GetDDSInfo({0}), {1}", f, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                                }
                                catch (Exception e)
                                {
                                    Assert.Fail("The XML test failed with exception: {0} - GetDDSInfo({1}", e.ToString(), f);
                                }

                            }
                        } 
                    }
                }
            } 
            catch (Exception e)
            {
                Assert.Fail("The XML test failed with exception: {0}", e.ToString());
            }

        }

        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod()]
        public void RenameFDIHartXMLs()
        {
            string ddsXml = "";
            string path = "";

            StringBuilder sbDevicePath = new StringBuilder(300);


            //FDI HART DD location
            string strDevicePath = ConfigurationManager.AppSettings["DevicePath_FDI_Hart"];


            /* recursively search .fmA files from Hart device directory */
            try
            {
                foreach (string d in Directory.GetDirectories(strDevicePath))
                {
                    foreach (string dd in Directory.GetDirectories(d))
                    {
                        foreach (string f in Directory.GetFiles(dd, "*.fmA"))
                        {
                            //generate expected xml file name
                            path = f + ".Expected.xml";

                            /* is the expected xml file existing? */
                            if (File.Exists(path))
                            {
                                //checked if this File is read only
                                FileAttributes attributes = File.GetAttributes(path);
                                if ((attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
                                {
                                    /* read XML string from DDS info */
                                    try
                                    {
                                        HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();
                                        loader.GetDDSInfo(f, out ddsXml);
                                        Marshal.ReleaseComObject(loader);

                                        FileStream pFileName = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);

                                        XmlDocument doc = new XmlDocument();
                                        doc.LoadXml(ddsXml);
                                        doc.Save(pFileName);

                                        pFileName.Close();
                                        //   xml = Regex.Replace(ddsXml, @"\s", "");
                                    }
                                    catch (COMException ex)
                                    {
                                        System.Diagnostics.Trace.TraceWarning(String.Format("RenameFDIHartXMLs (xml) - GetDDSInfo({0}), {1}", f, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                                    }
                                    catch (Exception e)
                                    {
                                        Assert.Fail("The XML test failed with exception: {0} - GetDDSInfo({1}", e.ToString(), f);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Assert.Fail("The XML test failed with exception: {0}", e.ToString());
            }

        }

        ///// <summary>
        ///// Only Create the XML for all the EDD files
        /////</summary>
        //[DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        //public void TestICnvenienceFunc_GetErrorString()
        //{
        //    /* read XML string from DDS info */
        //    //string compareXml;

        //    try
        //    {
        //        System.Diagnostics.Trace.TraceInformation("Processing: {0}");

        //        HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();

        //        string ddsXml = "";
        //        int i = -1;
        //        loader.GetErrorString("E:\\Sai\\ProjectData\\FDI\\FDI EDD Engine\\Code\\Legacy\\code\\Devices\\HART\\0000f9\\00d5\\0201.fm8", i, out ddsXml);

        //        //int []l_arrErrorCode = {-1,0,1,2,10,903};
        //        //using (XmlWriter writer = XmlWriter.Create(@"E:\Sai\UnitTestFile\UnitTestgenerated.xml"))
        //        //{
        //        //    writer.WriteStartDocument();
        //        //    writer.WriteStartElement("ErrorString");
        //        //    for (int j = 0; j < l_arrErrorCode.Length; j++)
        //        //    {
        //        //        loader.GetErrorString("E:\\Sai\\ProjectData\\FDI\\FDI EDD Engine\\Code\\Legacy\\code\\Devices\\HART\\0000f9\\00d5\\0201.fm8", l_arrErrorCode[j], out ddsXml);
        //        //        writer.WriteStartElement("ErrorString");

        //        //        writer.WriteElementString("ErrorCode", l_arrErrorCode[j].ToString());
        //        //        writer.WriteElementString("String", ddsXml);
        //        //        writer.WriteEndElement();
        //        //        ddsXml += "\"\n";
        //        //    }
        //        //    Marshal.ReleaseComObject(loader);
        //        //    writer.WriteEndElement();
        //        //    writer.WriteEndDocument();
        //        //}
 
        //        //compareXml = Regex.Replace(ddsXml, @"\s", "");
        //    }
        //    catch (COMException ex)
        //    {
        //        //mistake++;
        //      //  System.Diagnostics.Trace.TraceWarning(String.Format("AddMultipleDDHartTest (xml) - GetDDSInfo({0}), {1}", EDDBinaryFile, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
        //        //continue;
        //    }
        //}

        ///// <summary>
        ///// Only Create the XML for all the EDD files
        /////</summary>
        //[DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod()]
        //public void TestIConvenienceFunc_GetItemType()
        //{
        //    /* read XML string from DDS info */
        //    //string compareXml;

        //    try
        //    {
            //    System.Diagnostics.Trace.TraceInformation("Processing: {0}");

            //    HARTDDSCOM.IGetHARTDDSXML loader = new HARTDDSCOM.CGetHARTDDSXML();

            //    int pItemType = 0;
            //    uint i = 1;
            //    int j = 0;
            //    uint subindex = 0;
            //    int nInstance = 0;
            //    loader.GetItemType("E:\\Sai\\ProjectData\\FDI\\FDI EDD Engine\\Code\\Legacy\\code\\Devices\\HART\\0000f9\\00d5\\0201.fm8",nInstance,j, i, subindex, out pItemType);
            //}
            //catch (COMException ex)
            //{
        //        //mistake++;
        //        //  System.Diagnostics.Trace.TraceWarning(String.Format("AddMultipleDDHartTest (xml) - GetDDSInfo({0}), {1}", EDDBinaryFile, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
        //        //continue;
        //    }
        //}
	}
}
