/*
 *	@(#) $Id: evl_rslv.c,v 1.3 1996/01/04 20:12:44 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to
 *	resolving references
 */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"

#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif



/*********************************************************************
* lookup table for converting parse tags to item types
* used by both resolve_ref() and parse_ref()
*********************************************************************/
extern const int resolve_id_table[];	/* converts parse tags to item types */

const
int
resolve_id_table[] =
{
	0,
	ITEM_ARRAY_ITYPE,		//1
	COLLECTION_ITYPE,		//2
	ITEM_ARRAY_ITYPE,		//3
	COLLECTION_ITYPE,		//4
	RECORD_ITYPE,			//5
	ARRAY_ITYPE,			//6
	VAR_LIST_ITYPE,			//7
	PARAM_ITYPE,			//8
	PARAM_LIST_ITYPE,		//9
	BLOCK_ITYPE,			//10
	BLOCK_ITYPE,			//11
	VARIABLE_ITYPE,			//12
	MENU_ITYPE,				//13
	EDIT_DISP_ITYPE,		//14
	METHOD_ITYPE,			//15
	REFRESH_ITYPE,			//16
	UNIT_ITYPE,				//17
	WAO_ITYPE,				//18
	RECORD_ITYPE,			//19
	ARRAY_ITYPE,			//20
	VAR_LIST_ITYPE,			//21
	PROGRAM_ITYPE,			//22
	DOMAIN_ITYPE,			//23
	RESP_CODES_ITYPE,		//24
	FILE_ITYPE,				//25
	CHART_ITYPE,			//26
	GRAPH_ITYPE,			//27
	AXIS_ITYPE,				//28
	WAVEFORM_ITYPE,			//29
	SOURCE_ITYPE,			//30
	LIST_ITYPE,				//31
	IMAGE_ITYPE,			//32
	SEPARATOR_ITYPE,		//33
	CONST_ITYPE,			//34
	FILE_ITYPE,				//35
	LIST_ITYPE,				//36
	ENUM_BIT_ITYPE,			//37
	GRID_ITYPE,				//38
	ROWBREAK_ITYPE,			//39
	CHART_ITYPE,			//40
	GRAPH_ITYPE,			//41
	SOURCE_ITYPE,			//42
	ATTR_ITYPE				//43
};


/*********************************************************************
 *
 *	Name: resolve_fetch()
 *	ShortDesc: fetch the binary for the given flat item
 *
 *	Description:
 *		resolve_fetch determines which type of fetch we are using
 *		and makes the appropriate call to fetch the binary for the
 *		given flat item
 *
 *	Inputs:
 *		item_id:      the item id of the item to fetch
 *		mask:         the request mask for the attributes of the item
 *		item_type:    the type of the flat item
 *		block_handle: the handle of the current block
 *		scratch:	  a pointer to an array of at least two SCRATCH_PADs
 *                    which are used by isp fetch to store the binarys
 *
 *	Outputs:
 *		item:        the flat item with binarys attached
 *
 *
 *	Returns: DDI_INVALID_BLOCK_HANDLE
 *           DDL_RESOLVE_FETCH_FAIL
 *			 DDL_MEMORY_ERROR
 *			 and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

/*
 * PAD_START_SIZE is the number of bytes initially malloced
 *  as a scratchpad
 *
 * PAD_MAX_SIZE defines the maximum value that the static
 * variable pad_size can grow to
 */

#define PAD_START_SIZE		128
#define PAD_MAX_SIZE 		1024

int
resolve_fetch(
ENV_INFO		*env_info,
ITEM_ID         item_id,
unsigned long   mask,
void           *item,
unsigned short  item_type,
BLOCK_HANDLE    block_handle,
SCRATCH_PAD    *scratch)
{
	static unsigned long pad_size = PAD_START_SIZE;	/* pad malloc size */

	unsigned long   add_size;	       /* additional size for fetch */
	DEVICE_HANDLE   device_handle = 0; /* device handle from the block handle */
	//int             fetch_type = 0;	   /* isp or alternate fetch indicator */
	int             rc;	               /* return code */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

#ifdef ISPTEST
	TEST_FAIL(RESOLVE_FETCH);
#endif

	/*
	 * Check for a valid block handle
	 */

	if (!pConnMgr->valid_block_handle(block_handle)) {

		return DDI_INVALID_BLOCK_HANDLE;
	}

	rc  = pConnMgr->get_abt_adt_offset(block_handle, &device_handle);
	if (rc != SUCCESS) {
		return(rc) ;
	}
	//rc = pConnMgr->get_abt_dev_family(block_handle, &fetch_type);
	//if (rc != SUCCESS) {
	//	return(rc) ;
	//}

	//switch (fetch_type) {

	//case DF_ISP:

	/*
		* Check scratch pointers and isp_fetch_item_func pointer
		*/

	ASSERT_RET(scratch || (scratch + 1), DDL_NULL_POINTER);

	scratch->pad = NULL;
	scratch->used = 0;
	scratch->size = 0;

	(scratch + 1)->pad = NULL;
	(scratch + 1)->used = 0;
	(scratch + 1)->size = 0;


	scratch->pad = (unsigned char *) malloc((size_t) (pad_size) * sizeof(unsigned char));
	if (scratch->pad == NULL) {

		return DDL_MEMORY_ERROR;
	}

	scratch->used = 0;
	scratch->size = pad_size;

	rc = fch_item(env_info, device_handle, item_id, scratch, &add_size, mask, item, item_type);

	if (rc == FETCH_INSUFFICIENT_SCRATCHPAD) 
	{

		/*
			* Not enough memory on first round malloc more and
			* call fetch again
			*/

		++scratch;	/* increment the pointer */

		scratch->pad = (unsigned char *) malloc((size_t) (add_size) * sizeof(unsigned char));

		if (scratch->pad == NULL) {

			return DDL_MEMORY_ERROR;
		}

		scratch->used = 0;
		scratch->size = add_size;

		if ((add_size + pad_size) < PAD_MAX_SIZE) {

			pad_size += add_size;
		}

		rc = fch_item(env_info, device_handle, item_id, scratch, &add_size, mask, item, item_type);
	}


	return rc;
}


/*********************************************************************
 *
 *	Name: resolve_fetch_free()
 *	ShortDesc: free the memory malloced by calls to resolve_fetch()
 *
 *	Description:
 *		resolve_fetch_free determines which fetch was used with
 *		the item and then frees the memory which was malloced
 *		for the binarys
 *
 *	Inputs:
 *		item_type:    the type of flat item to free
 *		scratch:	  the pointers to the malloced scratchpads
 *		block_handle: the block handle associated with the item
 *
 *	Outputs:
 *		item:         a pointer to the flat item with the
 *		              binarys freed
 *	Returns:
 *		 DDI_INVALID_BLOCK_HANDLE
 *		 DDL_BAD_FETCH_TYPE
 *		 DDL_SUCCESS
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
resolve_fetch_free(
ENV_INFO	   *env_info,
void           *item,
unsigned short  item_type,
SCRATCH_PAD    *scratch,
BLOCK_HANDLE    block_handle)
{

	//int             fetch_type = 0;	    /* fetch type indicator */
	//int				rc ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	if (!pConnMgr->valid_block_handle(block_handle)) {

		return;
	}

	//rc = pConnMgr->get_abt_dev_family(block_handle, &fetch_type);
	//ASSERT_DBG (rc == SUCCESS) ; 

	//switch (fetch_type) {

	//case DF_ISP:
	if (scratch->pad != NULL) {
		free((void *) scratch->pad);
	}
	++scratch;

	if (scratch->pad != NULL) {
		free((void *) scratch->pad);
	}

	switch (item_type) {

	case ITEM_ARRAY_ITYPE:
		((FLAT_ITEM_ARRAY *) item)->masks.bin_hooked = 0;
		((FLAT_ITEM_ARRAY *) item)->depbin = NULL;
		break;

	case COLLECTION_ITYPE:
		((FLAT_COLLECTION *) item)->masks.bin_hooked = 0;
		((FLAT_COLLECTION *) item)->depbin = NULL;
		break;

	case VARIABLE_ITYPE:
		((FLAT_VAR *) item)->masks.bin_hooked = 0;
		((FLAT_VAR *) item)->depbin = NULL;
		break;

	default:
		CRASH_DBG();

	}


	return;
}



/*********************************************************************
 *
 *	Name: resolve_ref
 *
 *	ShortDesc: resolve a ddl reference
 *
 *	Description:
 *		resolve_ref takes a ref_info array and resolves it
 *		to the description reference which is the item_id and
 * 		type of the reference. It will also load an operational
 *		reference and a resolve trail if the options argument is
 *		set accordingly
 *
 *	Inputs:
 *		ref_info:	  A pointer to the list of reference information
 *					  used to resolve a reference.
 *		depinfo:	  The dependency information if any part of the
 *					  reference has a dependency.
 *		env_info:     The env_info for the reference to be resolved.
 *		var_needed:	  The variable for which a value is needed if
 *					  the parameter value service did not succeed.
 *		options:	  The options mask for requesting different types
 *					  of reference information including RESOLVE_TRAIL
 *					  RESOLVE_OP_REF and RESOLVE_DESC_REF
 *		extra_trail:  The number of trail elements to be added beyond
 *					  the standard count.
 *
 *	Outputs:
 *		ref:		  A pointer to the op_ref_trail which is loaded with
 *					  the requested resolved reference information.
 *
 *	Returns:
 *		DDI_INVALID_BLOCK_HANDLE
 *		and returns from other ddl functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
resolve_ref(
REF_INFO_LIST  *ref_info,
OP_REF_TRAIL   *ref,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed,
unsigned int    options,
unsigned short	extra_trail)
{

	//int             fetch_type = 0;	    /* fetch type indicator */
	int             rc = DDL_SUCCESS;	/* return code */
	int             rc2 = DDL_SUCCESS;	/* secondary return code */
	int             free_attr = FALSE;	/* free attribute indicator */
	int             info_inc;	        /* ref_info counter */
	REF_INFO       *info_ptr;	        /* ref_info pointer */
	unsigned short  inc;	            /* incrementer for item lists */
	int             count;	            /* count for item lists */
	unsigned short  trail_inc = 0;	    /* trail incrementer */
	unsigned short	op_ref_info_inc = 0;
	SCRATCH_PAD     scratch[2];	        /* scratch_pad array */

	SUBINDEX        subindex;	    /* subindex for name to subindex */
	BLK_TBL_ELEM   *bt_elem = NULL;	/* Block Table element pointer */
	ITEM_TBL       *it;	            /* item table */
	ITEM_TBL_ELEM  *it_elem;        /* item table element */


	FLAT_ITEM_ARRAY     item_array;	    /* item array for fetch and resolve */
	ITEM_ARRAY_ELEMENT *item_array_ptr; /* item array pointer for speed */

	FLAT_COLLECTION collection;	        /* collection for fetch and resolve */
	OP_MEMBER      *op_member_ptr;	    /* collection pointer for speed */

	DEPBIN		   *cur_depbin;			/* Current dependency and binary */

	ITEM_ID         current_id = 0;	    /* the current id in the resolve trail */
	ITEM_ID         next_id = 0;	    /* id argument to table request functions */
	ITEM_TYPE       current_type = 0;	/* current type in the resolve trail */
	int				blk_tbl_offset ;
	FLAT_DEVICE_DIR	*flat_device_dir ;

#ifdef ISPTEST
	TEST_FAIL(RESOLVE_REF);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 * Check for valid block handle
	 */

	if (!pConnMgr->valid_block_handle(env_info->block_handle)) {

		return DDI_INVALID_BLOCK_HANDLE;
	}

	/*
	 * get the fetch type
	 */

	/*rc = pConnMgr->get_abt_dev_family(env_info->block_handle, &fetch_type);
	if (rc != SUCCESS) {
		return(rc) ;
	}*/

	//if (fetch_type == DF_ISP) { //WE'RE ALWAYS DF_ISP

		/*
		 * Check that tables exist
         */

		rc = pConnMgr->get_abt_dd_blk_tbl_offset(env_info->block_handle, 
				&blk_tbl_offset) ;
		if (rc != SUCCESS) {
			return(rc) ;
		}
		if ((blk_tbl_offset) < 0) {

			return DDL_BLOCK_TABLES_NOT_LOADED; 
		}

		rc = block_handle_to_bt_elem(env_info, env_info->block_handle, &bt_elem);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
	//}

	/*
	 * memset the flat item structs
	 */

	memset((char *) &item_array, 0, sizeof(FLAT_ITEM_ARRAY));
	memset((char *) &collection, 0, sizeof(FLAT_COLLECTION));

	/*
	 * malloc the ref_trail if requested
	 */

	if (options & RESOLVE_TRAIL) {

#ifndef SUN
#pragma warning (disable : 4244)
#endif
		ref->trail_limit = ref_info->count + extra_trail;
#ifndef SUN
#pragma warning (default : 4244)
#endif
		ref->trail = (RESOLVE_INFO *) calloc(ref->trail_limit, sizeof(RESOLVE_INFO));
		if (ref->trail == NULL)
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"resolve_ref: failed to malloc memory");
	}

	/*
	 * Move backward through the info list to resolve the reference
	 */

	for (info_inc = (ref_info->count - 1), info_ptr = &ref_info->list[info_inc];
		info_inc >= 0; info_inc--, info_ptr--) {

		switch (info_ptr->type) {

		case ITEM_ID_REF:
		case ITEM_ARRAY_ID_REF:
		case COLLECTION_ID_REF:
		case BLOCK_ID_REF:
		case VARIABLE_ID_REF:
		case MENU_ID_REF:
		case EDIT_DISP_ID_REF:
		case METHOD_ID_REF:
		case REFRESH_ID_REF:
		case UNIT_ID_REF:
		case WAO_ID_REF:
		case RECORD_ID_REF:
		case ARRAY_ID_REF:
		case VAR_LIST_ID_REF:
		case PROGRAM_ID_REF:
		case DOMAIN_ID_REF:
		case RESP_CODES_ID_REF:
        case CHART_ID_REF:
        case AXIS_ID_REF:
        case FILE_ID_REF:
        case GRAPH_ID_REF:
        case LIST_ID_REF:
        case SOURCE_ID_REF:
        case WAVEFORM_ID_REF:
		case IMAGE_ID_REF:
		case GRID_ID_REF:
			current_id = info_ptr->val.id;
			current_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];

			if (options & RESOLVE_OP_REF) {

				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}
			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/

		case VIA_BITENUM_REF:
			current_type = (ITEM_TYPE) ENUM_BIT_ITYPE;
			ref->bit_mask = info_ptr->val.index;	// Store the bit mask

			//Don't update ref->op_info for BITENUM_REF

			if (options & RESOLVE_TRAIL)
			{
				trail_inc++;					// Create a new trail entry
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].type = ENUM_BIT_ITYPE;
				ref->trail[trail_inc].element = info_ptr->val.index;
			}
			break;


		case VIA_ITEM_ARRAY_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(env_info, current_id, (unsigned long) ITEM_ARRAY_ELEMENTS, (void *) &item_array,
				(unsigned short) ITEM_ARRAY_ITYPE, env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(item_array.masks.attr_avail & ITEM_ARRAY_ELEMENTS)) {

					/*
					 * eval the binary
					 */
					cur_depbin = item_array.depbin->db_elements;

					rc = eval_attr_itemarray(cur_depbin->bin_chunk,
						cur_depbin->bin_size,
						&item_array.elements,
						&cur_depbin->dep,
						env_info,
						var_needed);

					if (rc == DDL_SUCCESS) {
						free_attr = TRUE;
					}

					if (depinfo != NULL) {
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) {

				/*
				 * find the index in the item_array list
				 */

				item_array_ptr = item_array.elements.list;
				count = item_array.elements.count;

				for (inc = 0; inc < count; item_array_ptr++, inc++) {

					if (item_array_ptr->index == info_ptr->val.index) {

						current_id = item_array_ptr->ref.id;
						current_type = item_array_ptr->ref.type;

						/*
						 * if the current type is an
						 * ITEM_ID_REF convert this to
						 * an actual item type
						 */

						if (current_type == ITEM_ID_REF) {

							if (info_inc > 0) {
								current_type = (ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) {
							ref->op_ref_type = STANDARD_TYPE;
							ref->op_info.id = current_id;
							ref->op_info.type = current_type;
							ref->op_info.member = 0;
						}
						if (options & RESOLVE_TRAIL) {

							ref->trail[trail_inc].element = item_array_ptr->index;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}
						break;
					}
				}

				if (inc == count) {			/* The element was not found */
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free(env_info, (void *) &item_array,
						(unsigned short) ITEM_ARRAY_ITYPE, scratch, env_info->block_handle);

			if (free_attr) {
				ddl_free_itemarray_list(&item_array.elements, FREE_ATTR);
				free_attr = FALSE;
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/
		case VIA_FILE_REF:
			/*
			 * We do not support FILE, so we stop here and the reference has been
			 * chaned into FILE_ID_REF from VIA_FILE_REF
			 */
			break;

		case VIA_COLLECTION_REF:
			/*
			 * fetch the binary
			 */
			rc = resolve_fetch(env_info, current_id, (unsigned long) COLLECTION_MEMBERS, (void *) &collection,
				(unsigned short) COLLECTION_ITYPE, env_info->block_handle, scratch);

			if (rc == DDL_SUCCESS) {

				if (!(collection.masks.attr_avail & COLLECTION_MEMBERS)) {

					/*
					 * eval the binary
					 */
					cur_depbin = collection.depbin->db_members;

					rc = eval_attr_op_members(cur_depbin->bin_chunk, cur_depbin->bin_size, &collection.members,
																		&cur_depbin->dep, env_info, var_needed);

					if (rc == DDL_SUCCESS) 
					{
						free_attr = TRUE;
					}

					if (depinfo != NULL) 
					{
						rc2 = append_depinfo(depinfo, &cur_depbin->dep);
						if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) 
						{
							rc = rc2;
						}
					}
					ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
				}
			}

			if (rc == DDL_SUCCESS) 
			{	/*
				 * find the member name in the collection list
				 */

				op_member_ptr = collection.members.list;
				count = collection.members.count;

				for (inc = 0; inc < count; op_member_ptr++, inc++) 
				{
					if (op_member_ptr->name == info_ptr->val.member)
					{
						current_id = op_member_ptr->ref.op_info.id;
						current_type = op_member_ptr->ref.op_info.type;

						/*
						 * if the current type is an
						 * ITEM_ID_REF convert this to
						 * an actual item type
						 */

						if (current_type == ITEM_ID_REF) 
						{
							if (info_inc > 0) 
							{
								current_type = (ITEM_TYPE) resolve_id_table[(info_ptr - 1)->type];
							}
						}

						if (options & RESOLVE_OP_REF) 
						{
							if( op_ref_info_inc == 0 )
							{
								ref->op_ref_type = STANDARD_TYPE;
								ref->op_info.id = current_id;
								ref->op_info.type = current_type;
								ref->op_info.member = 0;
							}
							else
							{
								ref->op_info_list.list[op_ref_info_inc].member = op_member_ptr->name;
								op_ref_info_inc++;
								ref->op_info_list.list[op_ref_info_inc].id = current_id;
								ref->op_info_list.list[op_ref_info_inc].type = current_type;
								if( current_type == VARIABLE_ITYPE )
								{
									ref->op_info_list.list[op_ref_info_inc].member = 0;
									ref->op_info_list.count = op_ref_info_inc + 1;
								}
							}
						}

						if (options & RESOLVE_TRAIL) 
						{
							ref->trail[trail_inc].element = op_member_ptr->name;
							++trail_inc;
							ref->trail[trail_inc].id = current_id;
							ref->trail[trail_inc].type = current_type;
						}

						break;
					}
				}

				if (inc == count) 
				{
					rc = DDL_ENCODING_ERROR;
				}
			}

			resolve_fetch_free(env_info, (void *) &collection, (unsigned short) COLLECTION_ITYPE, scratch, 
																				env_info->block_handle);

			if (free_attr) 
			{
				ddl_free_op_members_list(&collection.members, FREE_ATTR);
				free_attr = FALSE;
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/

		case VIA_RECORD_REF:

			/*
			 * table request change item id and member name to
			 * subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id, info_ptr->val.member, &subindex);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}


			rc = pConnMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir) ;
			if (rc != SUCCESS)
			{
				return(rc) ;
			}
			it = &flat_device_dir->item_tbl;

			/*
			 * table request item id and subindex to item table element
			 */

			rc = tr_id_si_to_ite(it, bt_elem, current_id, subindex, &it_elem);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}

			current_id = ITE_ITEM_ID(it_elem);
			current_type = ITE_ITEM_TYPE(it_elem);

			if (options & RESOLVE_OP_REF) 
			{	/* save previous id and type */
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].element = info_ptr->val.member;
				++trail_inc;
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;

				if (  (trail_inc == 2)        // Remove a munged PARAM reference in .fms
					&& (ref->trail[0].type == PARAM_ITYPE)
					&& (ref->trail[1].type == RECORD_ITYPE)
					&& (ref->trail[2].type == VARIABLE_ITYPE)
					)
				{
					trail_inc = (unsigned short)-1;        // Effectively erase this trail
				}

			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/
		case VIA_LIST_REF:
		case VIA_ARRAY_REF:
			
			if (options & RESOLVE_OP_REF) 
			{	/* save previous id and type */
				if( op_ref_info_inc == 0 )
				{
					// First List/Array in the reference
					ref->op_ref_type = COMPLEX_TYPE;
					ref->op_info_list.count = ref_info->count;
					ref->op_info_list.list = (OP_REF_INFO*) malloc((size_t) ref->op_info_list.count * sizeof( OP_REF_INFO ));
				
					ref->op_info_list.list[op_ref_info_inc].id = current_id;				
					
				}
				// First List/Array in the reference plus all others if they exist( i.e. List/Array of Collection of Array or List of List of Collection )
				ref->op_info_list.list[op_ref_info_inc].type = current_type;
				ref->op_info_list.list[op_ref_info_inc].member = (SUBINDEX) info_ptr->val.index;
				op_ref_info_inc++;
				
				DDI_BLOCK_SPECIFIER bs = {0};
				bs.type = DDI_BLOCK_HANDLE;
				bs.block.handle = env_info->block_handle;

				DDI_ITEM_SPECIFIER is = {0};
				is.type = DDI_ITEM_ID;
				is.item.id = current_id;

				DDI_GENERIC_ITEM gi = {0};

				if( info_ptr->type == VIA_LIST_REF )
				{
					rc = ddi_get_item(&bs, &is, env_info, LIST_TYPE, &gi);
					if(rc != SUCCESS)
					{
						ddi_clean_item( &gi );
						return rc;
					}

					FLAT_LIST* fl = (FLAT_LIST *)gi.item;

					//assign the item id of the list TYPE to our current id
					ref->op_info_list.list[op_ref_info_inc].id = is.item.id = fl->type;

					//look up the data type of the list (Variable, Array, Collection)
					rc = ddi_get_type(env_info, &bs, &is, &ref->op_info_list.list[op_ref_info_inc].type);
					if( rc != SUCCESS )
					{
						ddi_clean_item( &gi );
						return rc;
					}
					else
					{
						if( ref->op_info_list.list[op_ref_info_inc].type == VARIABLE_ITYPE )
						{
							ref->op_info_list.list[op_ref_info_inc].member = 0;
							ref->op_info_list.count = op_ref_info_inc + 1;
						}
					}
					ddi_clean_item( &gi );
				}
				else if( info_ptr->type == VIA_ARRAY_REF )
				{
					rc = ddi_get_item(&bs, &is, env_info, ARRAY_TYPE, &gi);
					if(rc != SUCCESS)
					{
						ddi_clean_item( &gi );
						return rc;
					}

					FLAT_ARRAY* fa = (FLAT_ARRAY *)gi.item;

					//assign the item id of the list TYPE to our current id
					ref->op_info_list.list[op_ref_info_inc].id = is.item.id = fa->type;

					//look up the data type of the list (Variable, Array, Collection)
					rc = ddi_get_type(env_info, &bs, &is, &ref->op_info_list.list[op_ref_info_inc].type);
					if( rc != SUCCESS )
					{
						ddi_clean_item( &gi );
						return rc;
					}
					else
					{
						ref->op_info_list.list[op_ref_info_inc].member = 0;
						ref->op_info_list.count = op_ref_info_inc + 1;
					}
					ddi_clean_item( &gi );
				}
			}

			if ((options & RESOLVE_DESC_REF) || (options & RESOLVE_TRAIL)) 
			{
				rc = pConnMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir);
				
				if (rc != SUCCESS) 
				{
					return(rc) ;
				}

				it = &flat_device_dir->item_tbl;

				/*
				 * table request item id and subindex to item
				 * table element
				 */

				rc = tr_id_si_to_ite(it, bt_elem, current_id, (SUBINDEX) info_ptr->val.index, &it_elem);
				if (rc == DDS_SUCCESS) 
				{
					current_id = ITE_ITEM_ID(it_elem);
					current_type = ITE_ITEM_TYPE(it_elem);
				}
				else	// If tr_id_si_to_ite() failed, get the info the hard way
				{
					DDI_BLOCK_SPECIFIER bs = {0};
					bs.type = DDI_BLOCK_HANDLE;
					bs.block.handle = env_info->block_handle;

					DDI_ITEM_SPECIFIER is = {0};
					is.type = DDI_ITEM_ID;
					is.item.id = current_id;

					DDI_GENERIC_ITEM gi = {0};

					if(info_ptr->type == VIA_LIST_REF)
					{
						rc = ddi_get_item(&bs, &is, env_info, LIST_TYPE, &gi );
						if(rc != SUCCESS)
						{
							ddi_clean_item( &gi );
							return rc;
						}

						FLAT_LIST* fl = (FLAT_LIST *)gi.item;

						//assign the item id of the list TYPE to our current id
						current_id = is.item.id = fl->type;

						//look up the data type of the list (Variable, Array, Collection)
						rc = ddi_get_type(env_info, &bs, &is, &current_type);
						if( rc != SUCCESS )
						{
							ddi_clean_item( &gi );
							return rc;
						}
						ddi_clean_item( &gi );
					}
					else if( (info_ptr->type == VIA_ARRAY_REF) )
					{
						rc = ddi_get_item(&bs, &is, env_info, ARRAY_TYPE, &gi);
						if( rc != SUCCESS )
						{
							ddi_clean_item( &gi );
							return rc;
						}

						FLAT_ARRAY* fa = (FLAT_ARRAY*)gi.item;

						current_id = is.item.id = fa->type;

						rc = ddi_get_type(env_info, &bs, &is, &current_type);
						if( rc != SUCCESS )
						{
							ddi_clean_item( &gi );
							return rc;
						}
						ddi_clean_item( &gi );
					}
				}

				if (options & RESOLVE_TRAIL) 
				{
					ref->trail[trail_inc].element = info_ptr->val.index;
					++trail_inc;
					ref->trail[trail_inc].id = current_id;
					ref->trail[trail_inc].type = current_type;
				}
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/

		case VIA_VAR_LIST_REF:

			/*
			 * table request item id and member name to item id and
			 * type
			 */

			rc = tr_resolve_lid_pname_to_id_type(env_info, env_info->block_handle,
				bt_elem,
				current_id,
				info_ptr->val.member,
				&next_id, &current_type);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			current_id = next_id;

			if (options & RESOLVE_OP_REF) 
			{
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) {

				ref->trail[trail_inc].element = info_ptr->val.member;
				++trail_inc;
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;


			/********************************************************************************************/
			/********************************************************************************************/

		case VIA_PARAM_LIST_REF:
		case VIA_PARAM_REF:
			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = info_ptr->val.member;

				if (info_ptr->type == VIA_PARAM_LIST_REF) 
				{
					ref->trail[trail_inc].type = PARAM_LIST_ITYPE;
				}
				else 
				{
					ref->trail[trail_inc].type = PARAM_ITYPE;
				}
				trail_inc++;
			}

			/*
			 * table request member name to item id and type
			 */
			rc = tr_resolve_name_to_id_type(env_info, env_info->block_handle, bt_elem, info_ptr->val.member, 
																		&current_id, &current_type);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}

			if (options & RESOLVE_OP_REF)
			{
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = current_type;
				ref->op_info.member = 0;
			}

			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
			}
			break;

			/********************************************************************************************/
			/********************************************************************************************/

		case VIA_BLOCK_REF:
			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].id = 0;
				ref->trail[trail_inc].element = 0;
				ref->trail[trail_inc].type = BLOCK_CHAR_ITYPE;
				trail_inc++;
			}

			/*
			 * table request characteristic record to item id
			 */

			rc = tr_resolve_char_rec_to_ddid(env_info, env_info->block_handle, bt_elem, &current_id);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}

			/*
			 * table request item id and member name to subindex
			 */

			rc = tr_resolve_ddid_mem_to_si(bt_elem, current_id, info_ptr->val.member, &subindex);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}

			if (options & RESOLVE_OP_REF) 
			{	/* save previous id and type */
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.id = current_id;
				ref->op_info.type = RECORD_ITYPE;
				ref->op_info.member = subindex;
			}
			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = RECORD_ITYPE;
				ref->trail[trail_inc].element = (unsigned long) subindex;
				trail_inc++;
			}

			/*
			 * get item table
			 */

			rc = pConnMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir);
			if (rc != SUCCESS) 
			{
				return(rc) ;
			}
			it = &flat_device_dir->item_tbl;

			/*
			 * table request characteristic to item table element
			 */

			rc = tr_characteristics_to_ite(it, bt_elem, subindex, &it_elem);
			if (rc != DDS_SUCCESS) 
			{
				return rc;
			}

			current_id = ITE_ITEM_ID(it_elem);
			current_type = ITE_ITEM_TYPE(it_elem);

			if (options & RESOLVE_TRAIL) 
			{
				ref->trail[trail_inc].id = current_id;
				ref->trail[trail_inc].type = current_type;
				ref->trail[trail_inc].element = 0;
			}
			break;


		default:
			CRASH_RET(DDL_ENCODING_ERROR);
			/* NOTREACHED */
			break;
		}

		if (rc != DDL_SUCCESS) 
		{
			return rc;
		}
	}

	if (options & RESOLVE_TRAIL) 
	{	/* finish off the trail */
		++trail_inc;
		ref->trail_count = trail_inc;
	}

	if (options & RESOLVE_DESC_REF) 
	{
		ref->desc_id = current_id;
		ref->desc_type = current_type;
	}

	if (ref->op_ref_type == COMPLEX_TYPE)
	{
		if (   (ref->op_info_list.count == 2)  // Check to see if simple VARIABLE Array or LIST
			&& (   (ref->op_info_list.list[0].type == ARRAY_ITYPE)
				|| (ref->op_info_list.list[0].type == LIST_ITYPE)
				)
			&& (ref->op_info_list.list[1].type == VARIABLE_ITYPE)
			)
		{
			ref->op_ref_type = STANDARD_TYPE;
			ref->op_info.id     = ref->op_info_list.list[0].id;
			ref->op_info.member = ref->op_info_list.list[0].member;
			ref->op_info.type   = ref->op_info_list.list[0].type;
			ddl_free_op_ref_info_list(&ref->op_info_list);				// release the old list
		}
	}

	return rc;
}
