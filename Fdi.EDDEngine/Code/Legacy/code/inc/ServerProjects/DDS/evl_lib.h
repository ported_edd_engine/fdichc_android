/*
 * @(#) $Id: evl_lib.h,v 1.4 1996/03/14 04:08:03 stevbey Exp $ 
 */

#ifndef LIBDDL_H
#define LIBDDL_H

#include <stddef.h>
#include <limits.h>

#ifndef RTN_CODE_H
#include "rtn_code.h"		/** All of these .h files are needed by the eval library **/
#endif

#ifndef DDL_TAGS_H
#include "tags_sa.h"		/** To make it easier for a user to use eval, the user **/
#endif

#ifndef DDLDEFS_H
#include "ddldefs.h"		/** only needs to include evl_lib.h **/
#endif

#ifndef FLATS_H
#include "flats.h"
#endif

#ifndef TABLE_H
#include "table.h"
#endif

#ifndef DDS_UPCL_H
#include "dds_upcl.h"
#endif

#ifndef ENV_INFO_H
#include "env_info.h"
#endif

#ifndef EVL_RET_H
#include "evl_ret.h"
#endif


#ifdef DEBUG
#ifndef DDS_CHK_H
#include "dds_chk.h"
#endif
#endif

#ifdef __cplusplus
    extern "C" {
#endif


/**
 * Range Values
 **/

#define DDL_HUGE_INTEGER ULONG_MAX	/** defines the maximum size of an **/
									/** unsigned integer **/

#define DDL_HUGE_LONG_INTEGER ULLONG_MAX	/** defines the maximum size of an **/
											/** unsigned long long integer **/



/*
 *	Structure used to pass info to the device specific string service
 */
typedef struct {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;


/**
 ** flat interface functions
 **/

extern int eval_item P((void *, unsigned long, ENV_INFO *, RETURN_LIST *, ITEM_TYPE));
extern int eval_dir_block_tables P(( FLAT_BLOCK_DIR *, BIN_BLOCK_DIR *, ENV_INFO *, unsigned long, ROD_HANDLE));
extern int eval_dir_device_tables P(( FLAT_DEVICE_DIR *, BIN_DEVICE_DIR *, ENV_INFO *, unsigned long, ROD_HANDLE));



/**
 ** flat destructor functions
 **/

extern void eval_clean_item P((void *, ITEM_TYPE));



/**
 ** directory (ie. table) destructor functions
 **/

extern void eval_clean_device_dir P((FLAT_DEVICE_DIR *));
extern void eval_clean_block_dir P((FLAT_BLOCK_DIR *));


/*
 * Function which support the HART linker
 */

extern int link_item P((void *, unsigned long, ENV_INFO *, RETURN_LIST *, ITEM_TYPE));

/**
 ** application function prototypes
 **/

extern int app_func_get_param_value P((ENV_INFO *, OP_REF *, EVAL_VAR_VALUE *));
extern int app_func_get_dict_string P((ENV_INFO *, DDL_UINT, STRING *));
extern int app_func_get_dev_spec_string P((ENV_INFO *, DEV_STRING_INFO *, STRING *));

#ifdef __cplusplus
    }
#endif

#endif



