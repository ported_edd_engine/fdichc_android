/*
 * @(#) $Id: evl_ret.h,v 1.2 1995/05/22 11:44:37 donmarq Exp $
 */

#ifndef EVL_RET_H
#define EVL_RET_H

#include "attrs.h"

#ifdef __cplusplus
    extern "C" {
#endif

/*
 *	The following structures define the information returned from DD Services
 */

#define RETURN_LIST_SIZE	32	/** Max number of elements which can be **/
								/** returned in a RETURN_INFO list **/

typedef struct {
	unsigned long   bad_attr;	/* Bit indicating which attribute is bad */
	int             rc;			/* Return code to be passed back */
	OP_REF          var_needed;	/* Used to store op_ref if unable to get */
								/* value from parameter value service. */
}               RETURN_INFO;


typedef struct {
	unsigned short  count;		/** number of items in the RETURN_INFO list **/
	RETURN_INFO     list[RETURN_LIST_SIZE];	 /** list of return information **/
}               RETURN_LIST;

#ifdef __cplusplus
    }
#endif

#endif	/* EVL_RET_H */

