/***
 *
 * @(#) $Id: sysproto.h,v 1.1 1995/05/02 22:13:28 donmarq Exp $
 * Copyright 1994 Rosemount, Inc., all rights reserved.
 *
 * This file provides prototypes for library functions with no
 * corresponding #include file prototype (K & R)
 *
 ***/

#ifndef SYSPROTO_H
#define SYSPROTO_H

#ifdef SUN

extern int	printf();
extern int	fprintf();
extern int	vprintf();
extern int	sscanf();
extern int	scanf();
extern int	fread();
extern int	fseek();
extern int	fclose();
extern void rewind();

#endif

#endif
