#pragma once

namespace nsEDDEngine
{
	//Return code from member function EndMethod() in IMethods class
	enum Mth_ErrorCode {
		METH_SUCCESS               = 0,   // Method was successfully completed
		METH_ABORTED               = 1,   // Method aborted due to abort built-in or abort mask set
		METH_CANCELLED             = 2,   // IMethods::CancelMethod() has been called or UI builtin has been cancelled
		METH_FAILED                = 3    // Method stopped due to DD error and aborted
	};

	enum ProtocolType
	{
		HART		= 1,
		FF			= 2,
		PROFIBUS	= 3,
		PROFIBUS_PN	= 4,
		ISA100		= 5,
		GPE			= 6,
		COMSERVER	= 7
	};

	enum ChangedParamActivity
		{ Discard, Save, Send };

	enum DDSType
	{
		LegacyHART_DDS = 0,
		LegacyFF_DDS   = 1,
		FDI_DDS	       = 2
	};
}
