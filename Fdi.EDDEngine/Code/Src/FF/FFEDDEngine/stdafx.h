// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#pragma warning(disable: 4127)
#pragma warning(disable: 4482)

#include "PlatformCommon.h"
#ifdef _WIN32
#include "targetver.h"
#endif
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS         // This hides the security warnings (strcpy, etc) from showing in the compiler
#endif
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CStdString constructors will be explicit

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#ifndef STRICT
#define STRICT
#endif

#if 0
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <afxwin.h>         // MFC core and standard components
// Windows Header Files:
#include <windows.h>

#endif

// TODO: reference additional headers your program requires here

//#include "PlatformCommon.h"
#include "stdstring.h"

#include <Inf/NtcSpecies/BssProgLog.h>
#include <Inf/NtcSpecies/Ntcassert2.h>
#include <EddEngineLog.h>

#ifdef _DEBUG
   #ifndef DBG_NEW
      #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
      #define new DBG_NEW
   #endif
#endif  // _DEBUG

