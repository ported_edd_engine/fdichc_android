/*****************************************************************************
 *
 * File: Macros.h
 * Version: 4.1.0
 * Date: 06/11/10
 *
 *****************************************************************************
 * Copyright (c) 2010, HART Communication Foundation, All Rights Reserved
 *****************************************************************************
 *
 * Description: HART Device Description Macro Definitions
 *
 *****************************************************************************
 */

#define USER_INTERFACE LOCAL
#define LOCAL_INTERFACE LOCAL_DISPLAY
#define ANALOG_CHANNEL ANALOG_OUTPUT
#define RANGE INPUT

#define I_NO_COMMAND_SPECIFIC_ERRORS     0
#define I_INVALID_SELECTION              2
#define I_PARAMETER_TOO_LARGE            3
#define I_PARAMETER_TOO_SMALL            4
#define I_TOO_FEW_DATA_BYTES             5
#define I_TRANSMITTER_SPECIFIC_ERROR     6
#define I_IN_WRITE_PROTECT_MODE          7
#define I_ACCESS_RESTRICTED             16


/*
 * Strings for the single definition response codes.
 */
#define S_NO_COMMAND_SPECIFIC_ERRORS [no_command_specific_errors]
#define S_INVALID_SELECTION [invalid_selection]
#define S_PARAMETER_TOO_LARGE [passed_parameter_too_large]
#define S_PARAMETER_TOO_SMALL [passed_parameter_too_small]
#define S_TOO_FEW_DATA_BYTES [too_few_data_bytes_recieved]
#define S_TRANSMITTER_SPECIFIC_ERROR [xmtr_specific_command_error]
#define S_IN_WRITE_PROTECT_MODE [in_write_protect_mode]
#define S_ACCESS_RESTRICTED [access_restricted]

#define PRIMARY         0
#define SECONDARY       1
#define TERTIARY        2
#define QUATERNARY      3

#define PV  dynamic_variables[PRIMARY]
#define SV  dynamic_variables[SECONDARY]
#define TV  dynamic_variables[TERTIARY]
#define QV  dynamic_variables[QUATERNARY]


/*
 * Common table references.
 */
#define ROSEMOUNT_MODEL_CODE(N) { N, rosemount_model_code(N) }
#define ROSEMOUNT_ANALYTICAL_MODEL_CODE(N) { N, rosemount_analytical_model_code(N) }
#define KAYRAY_MODEL_CODE(N) { N, kayray_model_code(N) }
#define MICRO_MOTION_MODEL_CODE(N) { N, micro_motion_model_code(N) }
#define UNITS_CODE(N) { N, units_code(N) }
#define TRANSFER_FUNCTION_CODE(N) { N, transfer_function_code(N) }
#define MATERIAL_CODE(N) { N, material_code(N) }
#define ALARM_SELECTION_CODE(N) { N, alarm_selection_code(N) }
#define WRITE_PROTECT_CODE(N) { N, write_protect_code(N) }
#define COMPANY_IDENTIFICATION_CODE(N) { N, company_identification_code(N) }
#define DEVICE_TYPE_CODE(N) { N, device_type_code(N) }
#define BURST_MODE_CONTROL_CODE(N) { N, burst_mode_control_code(N) }
#define PHYSICAL_SIGNALING_CODE(N) { N, physical_signaling_codes(N) }
#define FLAG_ASSIGNMENT(N) { N, flag_assignment(N) }
#define OPERATING_MODE_CODE(N) { N, operating_mode_code(N) }
#define ANALOG_OUTPUT_NUMBERS_CODE(N) { N, analog_output_numbers_code(N) }
#define LOOP_CURRENT_MODE_CODE(N)  { N, loop_current_mode_codes(N) }
#define EXTENDED_DEVICE_STATUS_CODE(N)  { N, extended_device_status_codes(N)}
#define TRIM_POINT_CODE(N)   { N, trim_point_codes(N) }
#define DEVICE_VARIABLE_FAMILY_CODE(N)  { N, device_variable_family_codes(N) }
#define DEVICE_VARIABLE_CLASSIFICATION_CODE(N)  { N, device_variable_classification_codes(N) }
#define WRITE_DEVICE_VARIABLE_CODE(N) { N, write_device_variable_codes(N) }
#define CAPTURE_MODE_CODE(N) { N, capture_mode_codes(N) }
#define LOCK_DEVICE_CODE(N) { N, lock_device_codes(N) }
#define ANALOG_CHANNEL_FLAG(N) { N, analog_channel_flags(N) }
#define UNITS_CODE_TEMPERATURE(n) { n, units_code_temperature(n) }
#define UNITS_CODE_PRESSURE(n) { n, units_code_pressure(n) }
#define UNITS_CODE_VOLUMETRIC_FLOW(n) { n, units_code_volumetric_flow(n) }
#define UNITS_CODE_VELOCITY(n) { n, units_code_velocity(n) }
#define UNITS_CODE_VOLUME(n) { n, units_code_volume(n) }
#define UNITS_CODE_LENGTH(n) { n, units_code_length(n) }
#define UNITS_CODE_TIME(n) { n, units_code_time(n) }
#define UNITS_CODE_MASS(n) { n, units_code_mass(n) }
#define UNITS_CODE_MASS_FLOW(n) { n, units_code_mass_flow(n) }
#define UNITS_CODE_MASS_PER_VOLUME(n) { n, units_code_mass_per_volume(n) }
#define UNITS_CODE_VISCOSITY(n) { n, units_code_viscosity(n) }
#define UNITS_CODE_ANGULAR_VELOCITY(n) { n, units_code_angular_velocity(n) }
#define UNITS_CODE_ENERGY(n) { n, units_code_energy(n) }
#define UNITS_CODE_FORCE(n) { n, units_code_force(n) }
#define UNITS_CODE_POWER(n) { n, units_code_power(n) }
#define UNITS_CODE_FREQUENCY(n) { n, units_code_frequency(n) }
#define UNITS_CODE_ANALYTICAL(n) { n, units_code_analytical(n) }
#define UNITS_CODE_CAPACITANCE(n) { n, units_code_capacitance(n) }
#define UNITS_CODE_EMF(n) { n, units_code_emf(n) }
#define UNITS_CODE_CURRENT(n) { n, units_code_current(n) }
#define UNITS_CODE_RESISTANCE(n) { n, units_code_resistance(n) }
#define UNITS_CODE_ANGLE(n) { n, units_code_angle(n) }
#define UNITS_CODE_VOLUME_PER_VOLUME(n) { n, units_code_volume_per_volume(n) }
#define UNITS_CODE_VOLUME_PER_MASS(n) { n, units_code_volume_per_mass(n) }
#define UNITS_CODE_CONCENTRATION(n) { n, units_code_concentration(n) }

#define PROCESS_DATA_STATUS_CODES \
        {0 , process_data_status(0)}, \
        {1 , process_data_status(1)}, \
        {2 , process_data_status(2)}, \
        {3 , process_data_status(3)}

#define LIMIT_STATUS_CODES \
        {0 , limit_status(0)}, \
        {1 , limit_status(1)}, \
        {2 , limit_status(2)}, \
        {3 , limit_status(3)}

/*
 * The reply message of an identify request is defined here as a macro because
 * the same reply is returned by command 0,11,21,73 and 75 (the identify commands).
 */

#define IDENTIFY_REPLY_7 \
    response_code, device_status, 254, device_type, \
    request_preambles, universal_revision, transmitter_revision, \
    software_revision, hardware_revision <0xf8>, physical_signaling_code <0x07>, \
    device_flags, device_id, response_preambles, max_num_device_variables, \
    config_change_counter, extended_fld_device_status, manufacturer_id, \
    private_label_distributor, device_profile

#define IDENTIFY_REPLY_6 \
    response_code, device_status, 254, manufacturer_id, device_type, \
    request_preambles, universal_revision, transmitter_revision, \
    software_revision, hardware_revision <0xf8>, physical_signaling_code <0x07>, \
    device_flags, device_id, response_preambles, max_num_device_variables, \
    config_change_counter, extended_fld_device_status

#define IDENTIFY_REPLY_5 \
    response_code, device_status, 254, manufacturer_id, device_type, \
    request_preambles, universal_revision, transmitter_revision, \
    software_revision, hardware_revision <0xf8>, physical_signaling_code <0x07>, \
    device_flags, device_id

#define HART_DEVICE_TABLES \
    VARIABLE device_status; \
    VARIABLE units_code; \
    VARIABLE transfer_function_code; \
    VARIABLE material_code; \
    VARIABLE alarm_selection_code; \
    VARIABLE write_protect_code; \
    VARIABLE company_identification_code; \
    VARIABLE physical_signaling_codes; \
    VARIABLE flag_assignment; \
    VARIABLE operating_mode_code; \
    VARIABLE analog_output_numbers_code; \
    VARIABLE loop_current_mode_codes; \
    VARIABLE extended_device_status_codes; \
    VARIABLE lock_device_codes; \
    VARIABLE write_device_variable_codes; \
    VARIABLE device_variable_family_codes; \
    VARIABLE device_variable_classification_codes; \
    VARIABLE trim_point_codes; \
    VARIABLE capture_mode_codes; \
    VARIABLE physical_layer_type_codes; \
    VARIABLE lock_device_status_codes; \
    VARIABLE analog_channel_flags; \
    VARIABLE analog_channel_saturated_codes; \
    VARIABLE analog_channel_fixed_codes; \
    VARIABLE standardized_status_0_codes; \
    VARIABLE standardized_status_1_codes; \
    VARIABLE standardized_status_2_codes; \
    VARIABLE standardized_status_3_codes; \
    VARIABLE device_variable_code_codes; \
    VARIABLE time_set_codes; \
    VARIABLE real_time_clock_flag_codes; \
    VARIABLE si_control_codes; \
    VARIABLE device_profile_codes; \
    VARIABLE process_data_status; \
    VARIABLE limit_status; \
    VARIABLE device_family_status; \
    VARIABLE manufacturer_id; \
    VARIABLE private_label_distributor; \
    VARIABLE device_type; \
    VARIABLE write_protect; \
    VARIABLE physical_signaling_code; \
    VARIABLE operatingMode; \
    VARIABLE loop_current_mode; \
    VARIABLE device_flags; \
    VARIABLE device_profile; \
    VARIABLE country_code_codes; \
    VARIABLE master_mode_code; \
    VARIABLE extended_fld_device_status; \
    VARIABLE standardized_status_0; \
    VARIABLE standardized_status_1; \
    VARIABLE standardized_status_2; \
    VARIABLE standardized_status_3; \
    VARIABLE analog_channel_saturated1; \
    VARIABLE analog_channel_fixed1; \
    VARIABLE time_set; \
    VARIABLE real_time_clock_flag; \
    VARIABLE country_code; \
    VARIABLE si_control

#define BURST_TABLES \
    VARIABLE burst_trigger_units; \
    VARIABLE burst_mode_control_code; \
    VARIABLE burst_trigger_classification; \
    VARIABLE burst_message_trigger_mode_codes; \
    VARIABLE event_nofication_control_codes; \
    VARIABLE event_status_codes; \
    VARIABLE burst_mode_select; \
    VARIABLE burst_message_trigger_mode; \
    VARIABLE event_status; \
    VARIABLE event_notification_control_n; \
    VARIABLE device_status_mask; \
    VARIABLE extended_fld_device_status_mask; \
    VARIABLE standardized_status_0_mask; \
    VARIABLE standardized_status_1_mask; \
    VARIABLE standardized_status_2_mask; \
    VARIABLE standardized_status_3_mask; \
    VARIABLE analog_channel_saturated1_mask; \
    VARIABLE analog_channel_fixed1_mask; \
    VARIABLE device_status_latched_value; \
    VARIABLE dev_operating_mode_mask; \
    VARIABLE extended_fld_device_status_latched_value; \
    VARIABLE standardized_status_0_latched_value; \
    VARIABLE standardized_status_1_latched_value; \
    VARIABLE standardized_status_2_latched_value; \
    VARIABLE standardized_status_3_latched_value; \
    VARIABLE analog_channel_saturated1_latched_value; \
    VARIABLE analog_channel_fixed1_latched_value; \
    VARIABLE dev_operating_mode_latched_value; \
    VARIABLE lock_device_code; \
    VARIABLE lock_device_status_code

#define WIRELESS_TABLES \
    VARIABLE power_source; \
    VARIABLE wireless_operation_mode_codes; \
    VARIABLE wireless_mode; \
    VARIABLE join_process_status_codes; \
    VARIABLE join_status; \
    VARIABLE power_source_codes; \
    VARIABLE network_access_mode_codes; \
    VARIABLE network_access_mode; \
    VARIABLE join_mode_codes; \
    VARIABLE join_mode; \
    VARIABLE device_power_status_codes; \
    VARIABLE device_power_status; \
    VARIABLE wireless_module_device_type; \
    VARIABLE wireless_module_manufacturer_id

#define IO_ADAPTER_TABLES \
    VARIABLE master_mode; \
    VARIABLE subdev_expanded_device_type; \
    VARIABLE subdev_manufacturer_id

#define VAR_TREND_TABLES \
    VARIABLE trend_digital_units; \
    VARIABLE trend_classification; \
    VARIABLE trend_control_codes; \
    VARIABLE trend_control

//This macro includes everything except all the manufacturer device tables
#define ALL_TABLES \
    HART_DEVICE_TABLES; \
    BURST_TABLES; \
    WIRELESS_TABLES; \
    IO_ADAPTER_TABLES; \
    VAR_TREND_TABLES
    
/* 
 * Define all the Engineering units
 */
#define ALL_UNITS  \
        UNITS_CODE(1), \
        UNITS_CODE(2), \
        UNITS_CODE(3), \
        UNITS_CODE(4), \
        UNITS_CODE(5), \
        UNITS_CODE(6), \
        UNITS_CODE(7), \
        UNITS_CODE(8), \
        UNITS_CODE(9), \
        UNITS_CODE(10), \
        UNITS_CODE(11), \
        UNITS_CODE(12), \
        UNITS_CODE(13), \
        UNITS_CODE(14), \
        UNITS_CODE(15), \
        UNITS_CODE(16), \
        UNITS_CODE(17), \
        UNITS_CODE(18), \
        UNITS_CODE(19), \
        UNITS_CODE(20), \
        UNITS_CODE(21), \
        UNITS_CODE(22), \
        UNITS_CODE(23), \
        UNITS_CODE(24), \
        UNITS_CODE(25), \
        UNITS_CODE(26), \
        UNITS_CODE(27), \
        UNITS_CODE(28), \
        UNITS_CODE(29), \
        UNITS_CODE(30), \
        UNITS_CODE(31), \
        UNITS_CODE(32), \
        UNITS_CODE(33), \
        UNITS_CODE(34), \
        UNITS_CODE(35), \
        UNITS_CODE(36), \
        UNITS_CODE(37), \
        UNITS_CODE(38), \
        UNITS_CODE(39), \
        UNITS_CODE(40), \
        UNITS_CODE(41), \
        UNITS_CODE(42), \
        UNITS_CODE(43), \
        UNITS_CODE(44), \
        UNITS_CODE(45), \
        UNITS_CODE(46), \
        UNITS_CODE(47), \
        UNITS_CODE(48), \
        UNITS_CODE(49), \
        UNITS_CODE(50), \
        UNITS_CODE(51), \
        UNITS_CODE(52), \
        UNITS_CODE(53), \
        UNITS_CODE(54), \
        UNITS_CODE(55), \
        UNITS_CODE(56), \
        UNITS_CODE(57), \
        UNITS_CODE(58), \
        UNITS_CODE(59), \
        UNITS_CODE(60), \
        UNITS_CODE(61), \
        UNITS_CODE(62), \
        UNITS_CODE(63), \
        UNITS_CODE(64), \
        UNITS_CODE(65), \
        UNITS_CODE(66), \
        UNITS_CODE(67), \
        UNITS_CODE(68), \
        UNITS_CODE(69), \
        UNITS_CODE(70), \
        UNITS_CODE(71), \
        UNITS_CODE(72), \
        UNITS_CODE(73), \
        UNITS_CODE(74), \
        UNITS_CODE(75), \
        UNITS_CODE(76), \
        UNITS_CODE(77), \
        UNITS_CODE(78), \
        UNITS_CODE(79), \
        UNITS_CODE(80), \
        UNITS_CODE(81), \
        UNITS_CODE(82), \
        UNITS_CODE(83), \
        UNITS_CODE(84), \
        UNITS_CODE(85), \
        UNITS_CODE(86), \
        UNITS_CODE(87), \
        UNITS_CODE(88), \
        UNITS_CODE(89), \
        UNITS_CODE(90), \
        UNITS_CODE(91), \
        UNITS_CODE(92), \
        UNITS_CODE(93), \
        UNITS_CODE(94), \
        UNITS_CODE(95), \
        UNITS_CODE(96), \
        UNITS_CODE(97), \
        UNITS_CODE(98), \
        UNITS_CODE(99), \
        UNITS_CODE(100), \
        UNITS_CODE(101), \
        UNITS_CODE(102), \
        UNITS_CODE(103), \
        UNITS_CODE(104), \
        UNITS_CODE(105), \
        UNITS_CODE(106), \
        UNITS_CODE(107), \
        UNITS_CODE(108), \
        UNITS_CODE(109), \
        UNITS_CODE(110), \
        UNITS_CODE(111), \
        UNITS_CODE(112), \
        UNITS_CODE(113), \
        UNITS_CODE(114), \
        UNITS_CODE(115), \
        UNITS_CODE(116), \
        UNITS_CODE(117), \
        UNITS_CODE(118), \
        UNITS_CODE(119), \
        UNITS_CODE(120), \
        UNITS_CODE(121), \
        UNITS_CODE(122), \
        UNITS_CODE(123), \
        UNITS_CODE(124), \
        UNITS_CODE(130), \
        UNITS_CODE(131), \
        UNITS_CODE(132), \
        UNITS_CODE(133), \
        UNITS_CODE(134), \
        UNITS_CODE(135), \
        UNITS_CODE(136), \
        UNITS_CODE(137), \
        UNITS_CODE(138), \
        UNITS_CODE(139), \
        UNITS_CODE(140), \
        UNITS_CODE(141), \
        UNITS_CODE(142), \
        UNITS_CODE(143), \
        UNITS_CODE(144), \
        UNITS_CODE(145), \
        UNITS_CODE(146), \
        UNITS_CODE(147), \
        UNITS_CODE(148), \
        UNITS_CODE(149), \
        UNITS_CODE(150), \
        UNITS_CODE(151), \
        UNITS_CODE(152), \
        UNITS_CODE(153), \
        UNITS_CODE(154), \
        UNITS_CODE(155), \
        UNITS_CODE(160), \
        UNITS_CODE(161), \
        UNITS_CODE(162), \
        UNITS_CODE(163), \
        UNITS_CODE(164), \
        UNITS_CODE(165), \
        UNITS_CODE(166), \
        UNITS_CODE(167), \
        UNITS_CODE(168), \
        UNITS_CODE(169), \
        UNITS_CODE(235), \
        UNITS_CODE(236), \
        UNITS_CODE(237), \
        UNITS_CODE(238), \
        UNITS_CODE(239), \
        UNITS_CODE(250), \
        UNITS_CODE(251), \
        UNITS_CODE(252), \
        UNITS_CODE(253)

/* 
 * Define device variable classifications 
 */
#define ALL_CLASSIFICATIONS \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(0),  \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(64), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(65), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(66), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(67), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(68), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(69), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(70), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(71), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(72), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(73), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(74), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(75), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(76), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(77), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(78), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(79), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(80), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(81), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(82), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(83), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(84), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(85), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(86), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(87), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(88), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(89), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(90), \
        DEVICE_VARIABLE_CLASSIFICATION_CODE(91)

#define ALL_FAMILIES \
        DEVICE_VARIABLE_FAMILY_CODE(4), \
        DEVICE_VARIABLE_FAMILY_CODE(5), \
        DEVICE_VARIABLE_FAMILY_CODE(6), \
        DEVICE_VARIABLE_FAMILY_CODE(7), \
        DEVICE_VARIABLE_FAMILY_CODE(8), \
        DEVICE_VARIABLE_FAMILY_CODE(9), \
        DEVICE_VARIABLE_FAMILY_CODE(10), \
        DEVICE_VARIABLE_FAMILY_CODE(250)


/*
 * Defined Colors.
 */
#define BLACK   0x000000                        
#define SILVER  0xC0C0C0                        
#define GRAY    0x808080                        
#define WHITE   0xFFFFFF                        
#define MAROON  0x800000                        
#define RED     0xFF0000                        
#define ORANGE  0xFFA500                        
#define PURPLE  0x800080                        
#define FUCHSIA 0xFF00FF                                
#define GREEN   0x008000
#define LIME    0x00FF00
#define OLIVE   0x808000
#define YELLOW  0xFFFF00
#define NAVY    0x000080
#define BLUE    0x0000FF
#define TEAL    0x008080
#define AQUA    0x00FFFF


#ifdef __GNUC__
#define LOCALVAR( x) #x     /*this is used by the get_local_var commands*/
#else
#define LOCALVAR( x) "x"
#endif


