#pragma once


#include "afxtempl.h"
// Import MSXML6 namespace
#import	 "msxml6.dll"
// Use msxml2 namespace
using namespace MSXML2;


#include "BuildDDSXML_struc.h"
#include "nsEDDEngine\EDDEngineFactory.h"
#include "nsEDDEngine\Table.h"
#include <unordered_set>

#define CONFIG_XML_MAX 5000

using namespace nsEDDEngine;
class CDDSXMLBuilder 
{
	public:
		CDDSXMLBuilder();
		~CDDSXMLBuilder();
		
		BOOL CreateXMLFrom( CString sFilename, BSTR* DataAsXml );
        void GetErrorString(BSTR sFileName, int ErrorCode, BSTR* sErrorString);
        //void GetErrorString( BSTR sFileName, BSTR* sErrorString );
        int GetItemType(BSTR sFileName, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItemType);
        //int GetItemType(BSTR sFileName, int *pItemType);
        int GetItemTypeAndItemId(BSTR sFileName, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItem_Type, unsigned long *pItemId);
        int GetCmdIdFromNumber(BSTR sFileName, int iBlockInstance, unsigned long ulCmdNumber, unsigned long * pCmdItemId);
        int GetAxisUnitRelItemId(BSTR sFileName, int iBlockInstance, unsigned long AxisItemId, unsigned long *pUnitItemId);
        int GetParamUnitRelItemId(BSTR bstrFilename, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, unsigned long* pUnitItemId);
		int GetDeviceInstanceMemoryUsage(){return m_iDeviceInstanceMemoryUsage;};
		void SetLoadXML(bool bLoadXML);
		
		static int CDDSXMLBuilder::ReadConfigXML(wchar_t* configXML, int configXMLLen);

	private://member variables
		MSXML2::IXMLDOMDocument2Ptr m_pDOMDoc;
		CString  m_szFilename;
		CComBSTR bstr_DataAsXml;	
		nsEDDEngine::IEDDEngine* m_EDDEngine;
		int m_iDeviceInstanceMemoryUsage;
		ProtocolType m_Protocol;

		// Used for memory test to cancel out XML loading.
		bool m_bLoadXML;

		// Used when calling AddFFBlocks
		nsEDDEngine::CFieldbusBlockInfo *m_pFieldbusBlockInfo;
		int *m_iBlockInstances;
		int m_iBlockCount;
		CList<ITEM_ID> item_id_work_list;
			
	private:
		int AmsHartDDS_init();
		int AmsHartDDS_get_dd_list( FLAT_DEVICE_DIR *flat_device_dir, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int	CDDSXMLBuilder::AmsHartDDS_clean_device(  );
		BOOL BuildHeader( MSXML2::IXMLDOMElementPtr pDDODBinElmt  );
		void AmsHartDDS_convert_attribute_name(nsEDDEngine::AttributeName attribute_name, CString &attribute_name_as_string);
		void AmsHartDDS_convert_class_name(unsigned long long class_type, CString &class_type_as_string);
		void AmsHartDDS_convert_variable_type(VariableType variable_type, CString &var_type_as_string);
		void AmsHartDDS_convert_chart_type(unsigned long chart_type, CString &chart_type_as_string);
		void AmsHartDDS_convert_waveform_type(unsigned long waveform_type, CString &waveform_type_as_string);
		void AmsHartDDS_convert_method_type(unsigned long method_type, CString &method_type_as_string);
		void AmsHartDDS_convert_item_type(ITEM_TYPE item_type, CString &item_type_as_string);
		void AmsHartDDS_convert_handling(long handling, CString &handling_as_string);
		void AmsHartDDS_convert_menu_style(unsigned long menu_style, CString &style_as_string);
		CString StripOffNPChars( const TCHAR* EnumDescription );
		BOOL AddItemProperty( MSXML2::IXMLDOMElementPtr pDDItems, CString sPropertyName, CString sPropertyValue );
		BOOL AddEnumProperty( MSXML2::IXMLDOMElementPtr pDDItems, CString sPropertyName, CString sPropertyValue );
		MSXML2::IXMLDOMElementPtr AddItem( MSXML2::IXMLDOMElementPtr pDDItemList, unsigned long ItemID, ITEM_TYPE item_type );
		int AddItemWithName( MSXML2::IXMLDOMElementPtr pDDItemList, wchar_t *wName, ITEM_ID ItemID );
		void FDIHartDDS_convert_size_to_name(DisplaySize item_size, CString &item_size_as_string);
		void FDIHartDDS_convert_line_type(unsigned long line_type, CString &line_type_as_string);
		void FDIHartDDS_convert_scaling_to_name(Scaling scaling_value, CString &scaling_as_string);
		void FDIHartDDS_convert_DataItemType_to_name(DataItemType dataType, CString &scaling_as_string);
		void FDIHartDDS_convert_writeMode_to_name(WriteMode wMode, CString &wMode_as_string);
		void FDIHartDDS_convert_TimeScale_to_name(TimeScale tScale, CString &tScale_as_string);
		void FDIHartDDS_convert_BlockType_to_name(BlockBType type, CString &type_as_string);
        void FDIHartDDS_convert_base_class(BaseClass base_class, CString &output_class_as_string);
        void FDIHartDDS_convert_mode_and_reliability(ModeAndReliability mode_and_reliability, CString &output_class_as_string);
		void FDIHartDDS_convert_layout_type(LayoutType layout_type, CString &output_layout_as_string);
		void FDIHartDDS_convert_Edd_Profile(EDD_Profile edd_profile, CString &output_edd_profile_as_string);
		void FDIHartDDS_convert_classification_to_name(Classification classification, CString &classification_as_string);
		void FDIHartDDS_convert_ByteOrder_to_name(ByteOrder byteorder, CString &byteorder_as_string);
		void FDIHartDDS_convert_Protocol_to_name(Protocol protocol, CString &protocol_as_string);
		void FDIHartDDS_convert_RelationType_to_name(RelationType relation, CString &relation_as_string);
		void FormatItemIdAndName(ITEM_ID id, CString &strIdAndName);
		// Functions to get item information for specified type
		int AmsHartDDS_get_any_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );

		int AmsHartDDS_get_variable_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_menu_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_editDisplay_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_method_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_itemArray_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_array_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_command_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_axis_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_chart_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_graph_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_waveform_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_image_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_grid_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_record_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_collection_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_source_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_file_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_varList_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_wao_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_unit_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_refresh_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_block_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_block_b_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_list_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_plugin_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int AmsHartDDS_get_resp_codes_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int	AmsHartDDS_get_template_item( int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		int	AmsHartDDS_get_component_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt);
		int	AmsHartDDS_get_component_folder_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt);
		int	AmsHartDDS_get_component_relation_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDODSODElmt);
		int AmsHartDDS_get_component_reference_item(int iBlockIndex, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem);
		CString GetCommandIdType( int iType );
		void AddReturnList(MSXML2::IXMLDOMElementPtr pDDODSODElmt, FDI_GENERIC_ITEM *generic_item);
		void AddCommandListForItem( MSXML2::IXMLDOMElementPtr pDDItem, ITEM_ID itemId, ITEM_TYPE type );
		void DoCommandExecution(MSXML2::IXMLDOMElementPtr & pCommandList, FDI_PARAM_SPECIFIER* paramSpec );
		int	 Dump_ITEM_ID( ITEM_ID itemID, std::unordered_set<ITEM_ID> *pIdSet, int iBlockInstance, MSXML2::IXMLDOMElementPtr pDDODSODElmt );
		void AddItemInformation(MSXML2::IXMLDOMElementPtr pDDItem, ITEM_INFORMATION *item_info);

		void Add_ITEM_ID_LIST( const ITEM_ID_LIST * ItemIdList);
		void Add_MEMBER_LIST( const MEMBER_LIST * MemberList);
		void Add_ITEM_ID( ITEM_ID itemID);
		void Add_OP_REF_TRAIL(const OP_REF_TRAIL * oprefTrail);
		void Add_ACTION(const ACTION *action);
		void Add_ACTION_LIST(const ACTION_LIST * actionList);
		void Add_OP_REF_TRAIL_LIST(const OP_REF_TRAIL_LIST * opRefTrailList);
		void Add_OP_REF(const OP_REF * opRef);
		void Add_ITEM_ARRAY_ELEMENT_LIST(const ITEM_ARRAY_ELEMENT_LIST * elements);
		void Add_DATA_ITEM_LIST(const DATA_ITEM_LIST * dataItemList);
		void Add_VECTOR_LIST(const VECTOR_LIST * vectorList);
		void Add_DESC_REF(const DESC_REF * descRef);
		void Add_OP_MEMBER_LIST(const OP_MEMBER_LIST * opMemberList);
		void Add_DEFAULT_VALUES_LIST(const DEFAULT_VALUES_LIST * defaultValuesList);
		void Add_RESPONSE_CODES(const RESPONSE_CODES *responseCodes);
		void Add_TRANSACTION_LIST(const TRANSACTION_LIST * transactionList);
		
		int GetDDListByBlocks( nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir, MSXML2::IXMLDOMElementPtr pDDODBinElmt );
		void GetAnyItem( int iBlockInstance, nsEDDEngine::ITEM_TYPE, ITEM_ID item_id, MSXML2::IXMLDOMElementPtr pDDItem );
		int CreateBlockIndexes( nsEDDEngine::FLAT_DEVICE_DIR * flat_device_dir );

		// NEW
		void AmsHartDDS_get_string_table( MSXML2::IXMLDOMElementPtr pDDItem );
		int FDIHartDDS_AddConstantChild (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, nsEDDEngine::VECTOR_ITEM* pVectorItem );
		int FDIHartDDS_AddChildMenuItem (MSXML2::IXMLDOMElementPtr pDDItem, MENU_ITEM *pMenuItem );

		int FDIHartDDS_AddChildDescRef (MSXML2::IXMLDOMElementPtr & pElement, DESC_REF *pRefItem );
		int FDIHartDDS_AddChildDescRef(MSXML2::IXMLDOMElementPtr & pElement, wchar_t *wName, DESC_REF *pRefItem);
		int FDIHartDDS_AddChildOpRefTrail (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, OP_REF_TRAIL *pRefItem );
		int FDIHartDDS_AddChildOpRef (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, OP_REF *pRefItem );
		int FDIHartDDS_AddOpRef (MSXML2::IXMLDOMElementPtr & pDDItem, OpRefType  OpType, const OP_REF_INFO *pOpRefInfo, const OP_REF_INFO_LIST *pOpRefInfoList,
									BLOCK_INSTANCE BlockInstance, ITEM_ID DescId = 0, ITEM_TYPE DescType = ITYPE_NO_VALUE);

		int FDIHartDDS_AddChildPostPreAction (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, ACTION *pActionItem );
		int	FDIHartDDS_AddChildVectorAttr(MSXML2::IXMLDOMElementPtr pDDItem, VECTOR *pVector);
		int FDIHartDDS_AddChildDataItem (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, DATA_ITEM *pPointItem );
		int FDIHartDDS_AddChildResponseCodes (MSXML2::IXMLDOMElementPtr pDDItem, RESPONSE_CODES *pRespCodesItem );
		int FDIHartDDS_AddChildResponseCodeList (MSXML2::IXMLDOMElementPtr pDDItem, RESPONSE_CODE *pRespCodeItem );
		int FDIHartDDS_AddChildItemIdList (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, ITEM_ID_LIST *pItemIdList );
		int FDIHartDDS_AddChildMemberList (MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, MEMBER_LIST *pMemberList );
		int FDIHartDDS_AddChildMember (MSXML2::IXMLDOMElementPtr pDDItem, MEMBER *pMemberItem );
		int FDIHartDDS_AddChildOPMembers (MSXML2::IXMLDOMElementPtr pDDItem, OP_MEMBER *pOpMemberItem );
		int FDIHartDDS_AddChildTransaction (MSXML2::IXMLDOMElementPtr pDDItem, TRANSACTION *pTransItem );
		int FDIHartDDS_AddChildDefaultValues (MSXML2::IXMLDOMElementPtr pDDItem, DEFAULT_VALUE_ITEM *pDefaultVals);
		int FDIHartDDS_AddChildComponentInitialValues(MSXML2::IXMLDOMElementPtr pDDItem, DEFAULT_VALUE_ITEM *pInitialVals);
		int FDIHartDDS_AddChildCommandItems( MSXML2::IXMLDOMElementPtr & pDDItem, wchar_t *wName, COMMAND_ELEM* pCommandItem );
		void AmsHartDDS_AddRelationsAndUpdateInfo(MSXML2::IXMLDOMElementPtr & pDDItem, int iBlockIndex, FDI_PARAM_SPECIFIER *ParamSpec);
		int FDIHartDDS_AddChildComponents(MSXML2::IXMLDOMElementPtr pDDItem, wchar_t *wName, COMPONENT_SPECIFIER *pComponentDepictionItem);
		void GetRecursiveRelations( MSXML2::IXMLDOMElementPtr& pDDItem, int iBlockIndex, FDI_PARAM_SPECIFIER *pParamSpec, DESC_REF *curDescRef);
		void ParamSpecAddSI(int iBlockIndex, FDI_PARAM_SPECIFIER *pNewParamSpec, const FDI_PARAM_SPECIFIER* pOrigParamSpec, const DESC_REF *pOrigDescRef, const DESC_REF *pNewDescRef, ulong index);
		bool ParamSpecGetSI(FDI_PARAM_SPECIFIER *pParamSpec, CString &strSubIndex);
		void FDIHartDDS_AddRangeList(MSXML2::IXMLDOMElementPtr pDDItem, RANGE_SET *pRangeSet);
	
		// debug
		int AmsHartDDS_displayDebugInfo();
};

