/**********************************************************************************************
FILE:


**********************************************************************************************/


/**********************************************************************************************
** Library Header files
*********************************************************************************************/

#include "stdafx.h"
/**********************************************************************************************
** User Header files
*********************************************************************************************/
#pragma warning (disable : 4786)
#include "nsEDDEngine/Ddldefs.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "RTN_CODE.H"
#include "MEE.h"
#include <ServerProjects/CommonToolkitStrings.h>
#include "varientTag.h"
#include <locale.h>
/**********************************************************************************************
** Defines
*********************************************************************************************/

//in dictionary.h   #define COUNTRY_CODE_MARK 	'|'
//in dictionary.h   #define DEF_LANG_CTRY		"|en|"	/* Of the form "|LL|" or "|LL CC|" */

/**********************************************************************************************
** Global Functions
*********************************************************************************************/

//                in & out        in & out             in           in
int CopyToOutbuf(tchar*& dest,int &availChars,  tchar* source, int srcLen = -1)//-1 is copy till '\0'
{
	int r = BLTIN_SUCCESS;

	if ( dest == NULL || source == NULL )
	{
		SDCLOG(CERR_LOG,"Method Format Error: CopyToOutbuf: Empty Parameter!\n");
		return BLTIN_INVALID_PROMPT;
	}

	if (srcLen < 0)		
	{
		srcLen = (int)_tstrlen (source);
	}
	if (srcLen > availChars) 
	{// buffer full
		r = BLTIN_BUFFER_TOO_SMALL;
#ifdef _UNICODE
		SDCLOG(CERR_LOG,"Method Format Error: Buffer too small for %ls.\n",source);
#else
		SDCLOG(CERR_LOG,"Method Format Error: Buffer too small for %s.\n",source);
#endif
	}
	else
	{//	leave r = SUCCESS-copy over terminating null
		(void)_tstrncpy(dest, source, srcLen);
		dest[srcLen] = _T('\0');
		dest       += (srcLen);
		availChars -= srcLen;
	}

	return r;
}


/*  doFormat rolls the format string into the the value
		basically:  sprintf(retStr,formatStr,vValue);  with some bells and whistles
    pV isn't used at this time (I think we may have to later)   

	returns true on there was an error,  false at SUCCESS
*/
bool CBuiltIn::
     doFormat(tchar* formatStr, int formatLen, INTER_VARIANT& ivValue, tchar* retStr, int rsLen, hCVar *pVar)
{
	if ( (retStr == NULL) || (rsLen <= 0) || (formatLen <= 0) )
	{
		return true;
	}

	if(pVar != NULL && m_pDevice && ( formatStr[0] == _T('\0') ) )
	{
		//get variable format
		CValueVarient vtFormat{};
		long iRetValue = m_pDevice->ReadParameterData2( &vtFormat, DCI_PROPERTY_DISPLAY_FORMAT, 0, 0, pVar->getID() , 0 );
		if(iRetValue != BLTIN_SUCCESS)
		{
			return false;
		}
		
		if (wcschr(CV_WSTR(&vtFormat).c_str(), _T('%')) == NULL)
		{
			//add _T('%')
			swprintf( formatStr, formatLen, _T("%%%s"), CV_WSTR(&vtFormat).c_str() );
		}
		else
		{
			swprintf( formatStr, formatLen, CV_WSTR(&vtFormat).c_str() );
		}
	}

	// get DD variable type if there is no one in format
	if ((formatStr[0] == _T('\0')) || (wcscmp(formatStr, _T("%")) == 0))
	{
		switch( ivValue.GetVarType() )
		{
			case RUL_FLOAT:
				wcscpy(formatStr,  _T("%12.5g"));
				break;
			case RUL_DOUBLE:
				wcscpy(formatStr,  _T("%18.8g"));
				break;
			case RUL_CHAR:
				wcscpy(formatStr, _T("%c"));
				break;
			case RUL_SHORT:
				wcscpy(formatStr, _T("%hd"));
				break;
			case RUL_INT:
				wcscpy(formatStr, _T("%d"));
				break;
			case RUL_UINT:
				wcscpy(formatStr, _T("%u"));
				break;
			case RUL_UNSIGNED_CHAR:
				wcscpy(formatStr, _T("%hhu"));
				break;
			case RUL_USHORT:
				wcscpy(formatStr, _T("%hu"));
				break;
			case RUL_LONGLONG:
				wcscpy(formatStr, _T("%lld"));
				break;
			case RUL_ULONGLONG:
				wcscpy(formatStr, _T("%llu"));
				break;
			case RUL_CHARPTR:
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
			case RUL_SAFEARRAY:
				wcscpy(formatStr, _T("%ls"));
				break;
			case RUL_BYTE_STRING:
			case RUL_BOOL:
			default:
				break;
		}
	}
	//else, formatStr[0] != _T('\0')

	tchar* pch    =  _tstrchar( formatStr, _T('%') );
	tchar theChar = _T('\0');
	if ( pch != NULL ) // real formatting
	{//	no spaces allowed in formatting, so get the last char
		theChar = *pch;// start with the '%'
		while ( pch )
		{
			pch++;
			if (*pch == _T('\0') || *pch == _T(' ') || *pch == _T('\t') || *pch == _T('\n'))
				break;
			//else
			theChar = *pch;// we want the last char
		}
	}// else - leave cahr at null to get to default

	switch (theChar)
	{
	case 'c':
	case 'C':
        swprintf(retStr,(formatLen + sizeof(char)), formatStr, (char)ivValue);
		break;
	case 'd':
	case 'i':
		if (wcsstr(formatStr, _T("ll")) != NULL)
		{
            swprintf(retStr, (formatLen + sizeof(long long)), formatStr, (long long)ivValue);
		}
		else
		{
            swprintf(retStr, (formatLen + sizeof(int)), formatStr, (int)ivValue);
		}
		break;
	case 'o':
	case 'u':
	case 'x':
	case 'X':
		if (wcsstr(formatStr, _T("ll")) != NULL)
		{
            swprintf(retStr, (formatLen + sizeof(unsigned long long)), formatStr, (unsigned long long)ivValue);
		}
		else
		{
            swprintf(retStr, (formatLen + sizeof(unsigned int)),formatStr, (unsigned int)ivValue);
		}
		break;
	case 'e':
	case 'E':
	case 'f':
	case 'g':
	case 'G':
	case 'a':
	case 'A':
        swprintf(retStr,(formatLen + sizeof(float)), formatStr,(float)ivValue);
		break;
	case 's':
	case 'S':
		{
			wchar_t *stringVal = nullptr;
			ivValue.GetStringValue(&stringVal);
			if (stringVal != NULL)
			{
                swprintf(retStr, (formatLen +(int) wcslen(stringVal)), formatStr, stringVal);
				delete stringVal;
				stringVal = NULL;
			}
		}
		break;
	case '%':
	default:
		{
			switch (ivValue.GetVarType())
			{
			case RUL_BOOL:
				if ((long long)ivValue == 0)
				{
                    swprintf(retStr, 5, _T("False"));
				}
				else
				{
                    swprintf(retStr,4, _T("True"));
				}
				break;
			case RUL_BYTE_STRING:
				{
					tchar *pStr = retStr;
					for (int i = 0; i < (int)(ivValue.GetValue()).bString.bsLen; i++)
					{
                        swprintf(pStr++, sizeof(wchar_t),_T("%c"), (ivValue.GetValue()).bString.bs[i]);
					}
				}
				break;
			default:
                swprintf(retStr,formatLen,formatStr);
				break;
			}
		}
		break;
	}

	return false; // no error
}
							 



#define INTERNAL_BUFFER_SIZE  (1024*3)  /* long for wide char sets */


/**********************************************************************
 *
 *  Name: bltin_format_string2
 *  ShortDesc: Format a string into the specified buffer
 *
 *  Description:
 *		Bltin_format_string2 takes the supplied prompt string, strips out the
 *		current language version, and parses the resulting prompt.  This
 *		parsing means putting the taking the printf-like prompt format, and
 *		inserting param values or labels in the appropriate places.
 *
 *		The type of variable is being checked against the assumed type of 
 *		variable based on the %[] string.  For instance:  if the user specifies
 *		a printf-like string of "%s" but is using a variable of type FLOATG_PT,
 *		an error will be returned.
 *
 *		Types DATE_AND_TIME, TIME, TIME_VALUE, and DURATION all use a default
 * 		printf format.  The user's format is always ignored.
 *
 *  Inputs:
 *		max_length - The length of the output buffer.
 *		passed_prompt - The prompt to strip and parse.
 *		blk_ids - the array of block IDs.
 *		blk_nums - the array of block occurrences.
 *		glob_var_ids - The array of param IDs used to parse the prompt.
 *		glob_var_memberids - The array of param member IDs/subindices used to parse	the prompt.
 *		nCnt_Ids - The number of elements in the above arrays.
 *		bDynamicVarValChanged - if this input is true, this functoin is calle by UI builtin
 *								with requesting dynamic variable value updates.
 *								if this output is true, the prompt variable is truely dynamic and value is updated.
 *
 *  Outputs:
 *		out_buf - The buffer to put the formatted string.
 *
 *  Returns:
 *		Any of the error returns specified in builtins.h &/| DDParser/Retn_Code.h
 *
 *  Author:
 *		Bruce Davis
 *   Re-written 10aug07 - stevev
 *
 *********************************************************************/
//This function reads prompt string with device variable value.
//The last argument bDyanmicVarvalChanged as input indicates whether the prompt string shall be updated dynamically or not.
//The last argument bDyanmicVarvalChanged as output indicates this function caller whether the prompt needed to be updated or not
//NOTE: the data that are retrieved here are scaled data and are used for UI use only
long CBuiltIn::
bltin_format_string2 (/* out */ tchar *out_buf, /* in */ int max_length, /* in */ tchar *passed_prompt, 
					  /* in */ unsigned long *blk_ids, /* in */ unsigned long *blk_nums, 
					  /* in */ unsigned long *glob_var_ids, /* in */ unsigned long *glob_var_memberids, /* in */ long nCnt_Ids, /* in, out */ bool *bDyanmicVarvalChanged)
{
	long			retCode = BLTIN_SUCCESS;
	int				remainingOut = max_length,  copy_length = 0,    paramIdx    = -1;
	bool			isNeedUpdated = *bDyanmicVarvalChanged;		// tells whether FF builtin wants dynamically update value or DD format indicates dynamically update by %[?,D]
	tchar			*prompt_buf  = NULL, *prompt = NULL, *curr_ptr = NULL, *percent_ptr = NULL;
	tchar			*out_ptr     = NULL;

	tchar			temp_buf[INTERNAL_BUFFER_SIZE];// bigger to handle multibyte chars
	tchar			curr_format[20],	curr_param[256] = {0};
	tchar			prompt_char = 0;
	
	unsigned long	blockId = 0;
	unsigned long	blockNum = 0;
	unsigned long	itemId = 0;
	unsigned long	memberId = 0;
	PARAM_REF		opRef;
	hCitemBase		*pIB = NULL;
	hCVar           *pVar = NULL;
	INTER_VARIANT	VariantVarVal;

	
	prompt_buf = new tchar[((_tstrlen(passed_prompt)+1) * 2)];
	if (prompt_buf == NULL)
	{
		*out_buf = _T( '\0' );
		return BLTIN_NO_MEMORY;	//#exit if null returned - nothing to work with
	}
	
	(void)_tstrcpy (prompt_buf, passed_prompt);
	// copies the string to prompt_buf from internal loc
	retCode = m_pDevice->dictionary->get_string_translation(prompt_buf, /*out*/prompt_buf, (int)wcslen(prompt_buf) + 1, m_pDevice->device_sLanguageCode);
	if ((prompt_buf == NULL) || (retCode != BLTIN_SUCCESS))
	{
		delete [] (prompt_buf);
		*out_buf = _T( '\0' );
		return BLTIN_NO_LANGUAGE_STRING;	//#exit if null returned - nothing to work with
	}
	prompt = prompt_buf;

	out_ptr = out_buf;										
	*bDyanmicVarvalChanged = false;		//start from false until variable value is found a need to be updated

	//#for - ever, loop through prompt string for '%'
	while ( retCode == BLTIN_SUCCESS )// stop on error code
	{
	//#	clear vars as required for the loop								strings				
		temp_buf[0]   = _T('\0');
		paramIdx      = -1;
		curr_format[0] = curr_param[0] = _T('\0');
		
		VariantVarVal.Clear();
		blockId = 0;
		blockNum = 0;
		itemId = 0;
		memberId = 0;
		opRef.~PARAM_REF();
		pIB = NULL;
		pVar = NULL;

	//#		move short circuit inside loop so we get the language string reguardless
		curr_ptr = _tstrchar(prompt, _T('%'));
	//#	if no more '%'	
		if (curr_ptr == NULL)
		{
	//#		no more %, copy the rest to output and leave
			retCode = CopyToOutbuf(out_ptr, remainingOut,  prompt) ;
			// retCode determines success or failure, we're leaving reguardless
			break;	//normal exit
		}// else we have a '%' - so process it												
	//#
	//#	current location, to the '%' char
		copy_length = (int)(curr_ptr - prompt);

		// inherently copies upto but not including '%'
		if( copy_length > 0 )
		{
			retCode = CopyToOutbuf(out_ptr,remainingOut,  prompt, copy_length);
			if ( retCode != BLTIN_SUCCESS)	//testing retcode
			{// probable full buffer
				break;// error exit now
			}	
		}
		prompt = curr_ptr + 1;		//point to the char right after '%'
		prompt_char = prompt[0];	//prompt_char may be 'n' in "%n" or "%nd{x} display message
		
		/*
		 *	Handle the formatting.  The HART formatting consists of:
		 *
		 *	%[format]{param ID array index} for a device param
		 *	%[format]{method-local param name} for a local paramparam
		 *	%[format]{complex-Var-reference} for a attributes and the like
		 *  %[format]n  when n is 0 to 9 id index (update dynamic if allowed)
		 *  
		 *	The FF formatting consists of:
		 *
		 *	%format{param ID array index} for a device param
		 *	%format{method-local param name} for a local paramparam
		 *	%format{complex-Var-reference} for a attributes and the like
		 *  
		 *	The format is optional, and consists of standard scanf
		 *	format string, with the addition of 'L', which specifies
		 *	the label of the param & 'U' which is the Units
		 */
	//# acquire the formatting in curr_format[]

	//#	get following '%' in order to get "%n" display message
		percent_ptr = _tstrchar (prompt, _T('%'));

	//#	get following '{'		
		curr_ptr = _tstrchar (prompt, _T('{'));

		if ( (prompt_char == _T('[')) || 		// whitespace not allowed
			 ((curr_ptr != NULL) && (curr_ptr != prompt)) && ((percent_ptr == NULL) || (curr_ptr < percent_ptr))
		   )
		{	
			/**	Capture the format string.	 */
			if (prompt_char == _T('['))
			{
				prompt++; // skip the '['
	//#			get following ']'		
				curr_ptr = _tstrchar (prompt, _T(']'));

				if (curr_ptr == NULL || curr_ptr == prompt)
				{											
		//#		exit if not found
					retCode = BLTIN_INVALID_PROMPT;
					SDCLOG(CERR_LOG,"Method Format Error: format's closing ']' not found.\n");
					break; // exit 
				}
			}

			copy_length = (int)(curr_ptr - prompt);
	//#		curr_format  =  "%" + string between '%[' & ']{' or "%" + string between '%' & '{'
			curr_format[0] = _T('%');
			(void)_tstrncpy (&(curr_format[1]), prompt, copy_length);
			if (wcsstr(curr_format, _T(",D")) != NULL)
			{
				//%[?,D] format is found
				isNeedUpdated = true;
				copy_length = copy_length - 2;	//remove ",D" from the curr_format[]
			}
			else if (wcsstr(curr_format, _T("D")) != NULL)
			{
				//%[?,D] format is found
				isNeedUpdated = true;
				copy_length = copy_length - 1;	//remove "D" from the curr_format[]
			}

			curr_format[copy_length+1] = _T('\0');
			if (prompt_char == _T('['))
			{
				prompt = curr_ptr + 1;	// + 1: skip the ']'
			}
			else
			{
				prompt = curr_ptr;
			}
			prompt_char = prompt[0];
		} 
		else
		{										
	//#	else curr_format == empty string	
			curr_format[0] = _T('\0');
		}
		
	//#
	//#	whitespace not allowed			
		if (prompt_char == _T('{'))
		{/**	Get the param string. */			
			prompt++;		// skip the '{'
	//#		get following '}'		
			curr_ptr = _tstrchar (prompt, _T('}'));
			if (curr_ptr == NULL)
			{											
	//#		exit if not found				
				retCode = BLTIN_INVALID_PROMPT;
				SDCLOG(CERR_LOG,"Method Format Error: param's closing '}' not found.\n");
				break; // leave the loop & return
			}
			copy_length = (int)(curr_ptr - prompt);
	//#		curr_param  =  string between '{' & '}'		
			(void)_tstrncpy (curr_param, prompt, copy_length);		//curr_param maybe a local variable name or a digit for DD variable
			curr_param[copy_length] = _T('\0');
			prompt = curr_ptr + 1;
			prompt_char = prompt[0];
		} 												
	//#	else														
	//#	whitspace not allowed - single digit only
		else if (isdigit (prompt_char))
		{	/**	Special case of param string... %X is same as %{X}
			 	where X is a single digit number.		 */													
	//#		curr_param = one digit + '\0'	
			// As per HCF test case UIB19080 requirement we need to distinguish between %0 and %{0}
			// if using the �varids� index notation, if the �{� and �}� are left off i.e. %0,
			// then if the variable is of CLASS DYNAMIC, it should update continuously.
			isNeedUpdated = true; 
			curr_param[0] = prompt_char;
			curr_param[1] = 0;
			prompt++;
			prompt_char = prompt[0];
		}											
	//#	else														
	//#	if got no format string	
		else if(*curr_format == _T('\0'))	//no '{' and no '[' found
		{													
	//#		copy '%' to out_buf   - sometimes a percent is just a percent					
			retCode = CopyToOutbuf(out_ptr,remainingOut,  _T("%"), 1) ;
			continue;// will exit if there was an error above...
		}
		else		//no '{' and no digit, but '[' found -- odd
	//#	else		// '%' [ stuff ] nothing
		{
			retCode = BLTIN_INVALID_PROMPT;
			SDCLOG(CERR_LOG,"Method Format Error:No parameter after format."
							"(whitespace is not allowed)\n");
	//#		error - exit now
			break;
		}
	//#
	//#	// we have captured 'em both. Now the parameter MUST resolve to an hCVar*  
	//# //		                                            OR a method-local-variable		
	//#
	//#	DD item with item ID	
		if (isdigit(curr_param[0]))
		{
			//getting DD variable value
			paramIdx = _tatoi (curr_param);
	//#		do range check - exit on error	
			if ( (glob_var_ids == NULL) || (paramIdx > (nCnt_Ids-1)) )// zero based glob_var_ids array   
			{				
				retCode = BLTIN_BAD_ID;
				SDCLOG(CERR_LOG,"Method Format Error:Param Index not found in Var-ID array.\n");
	//#		error - exit now
				break;
			}// else - OK, so process it

	//#		lookup itemid in idarray
			if (blk_ids != 0)
			{
				blockId = blk_ids[paramIdx];
			}
			if (blk_nums != 0)
			{
				blockNum = blk_nums[paramIdx];
			}
			itemId = glob_var_ids[paramIdx];
			if (glob_var_memberids != 0)
			{
				memberId = glob_var_memberids[paramIdx];
			}

			//get device variable reference IDs in case the above itemId is a reference proxy or FF member ID
			if (GetPARAM_REF(blockId, blockNum, itemId, memberId, &opRef) != BLTIN_SUCCESS)
			{
				retCode = BLTIN_VAR_NOT_FOUND;
				break;
			}

			//get item ID and member ID of the last layer in complex reference
			//they are good to get variable attributes
			if (opRef.op_ref_type == STANDARD_TYPE)
			{
				itemId = opRef.op_info.id;
				memberId = opRef.op_info.member;
			}
			else
			{
				itemId = opRef.op_info_list.list[opRef.op_info_list.count - 1].id;
				memberId = opRef.op_info_list.list[opRef.op_info_list.count - 1].member;
			}

			//get variable pointer for variable attributes
			retCode = m_pDevice->getElementOrMemberPointer(itemId, memberId, &pIB);
			if ( (retCode == SUCCESS) && (pIB != NULL) )
	//#		verify it's a var or collection or item array - exit on error	
			{
				if ( (pIB->IsVariable()) && (m_pDevice->whatCompatability() == dm_Standard) )// NOT lenient mode
				{
					pVar = (hCVar*)pIB;
				}
			}
			else // unfound item
			{
				SDCLOG(CERR_LOG,"Method Format Error:ID array # %d was not found in the DD\n", paramIdx);
	//#		error - exit	
				break;
			}
		}
	//#	else    // not a digit, it's a (possibly complex) variable reference	
		else
		{
	//#		interpret -- DD item name or complex expression
			paramIdx = -1;
			wstring wS(curr_param);
			string   S;
			S = TStr2AStr(wS);
		
			//	/* get variable pointer and PARAM_REF without getting variable value */
			retCode = (m_pMeth->m_pMEE)->ResolveComplexReference((char*)S.c_str(), NULL, pIB, &opRef);

				if (retCode == SUCCESS)	
				{
					retCode = BLTIN_SUCCESS;
				}
				else if (retCode == MEE_NOT_DDITEM)
				{
					//if the variable is not a DD item, it is okay to display blank for label or unit
					VariantVarVal.Clear();
					pIB = NULL;
					retCode = BLTIN_SUCCESS;
				}
				else
				{
					VariantVarVal.Clear();
					pIB = NULL;
					retCode = BLTIN_DDS_ERROR;
				}
				//check if it is variable attribute
				basic_string <char>::size_type indexChDot = 0;
				indexChDot = S.find_last_of(".");			//The index of the last element of '.' after the 0th position in string S
				string sVarAttr = S.substr(indexChDot + 1);	//possible variable attribute name

															//check if it is enum or bit-enum variable
				if (pIB != NULL)
				{
					if (pIB->IsVariable())
					{
						pVar = (hCVar *)pIB;
					}
				}

				if (
					((curr_format == NULL)
						|| ((_tstrcmp(curr_format, _T("%L"))) && (_tstrcmp(curr_format, _T("%U")))))	//not getting variable label or unit
					&&
					((pVar == NULL)																//local variable
						|| (pVar && (m_pMeth->m_pMEE->isAttr(sVarAttr.c_str())					//DD variable attribute
							|| (pVar->VariableType() == (int)vT_Enumerated)						//DD enum variable
							|| (pVar->VariableType() == (int)vT_BitEnumerated))))				//DD bit-enum variable
					)
			{
					//get local variable value or DD variable attribute (DEFULT_VALUE, SCALING_FACTOR, VARIABLE_STATUS etc) or enum/bit enum variable value.
				retCode = (int) m_pInterpreter->//                 in             return values.......
									GetVariableValue( (char*)S.c_str(), VariantVarVal, &pIB);
				if (retCode != SUCCESS)
				{
		//			SDCLOG(CERR_LOG,"Method Format Error:Failed to find Variable Value.'%s'\n",	(char *)S.c_str());
					if (retCode == MEE_INVALID_REFERENCE)
					{
						//failed to read variable
						retCode = BLTIN_CANNOT_READ_VARIABLE;
					}
					else
					{
						retCode = BLTIN_DDS_ERROR;
					}
		////#		error - exit	
		//			break;
					VariantVarVal.Clear();
					pIB = NULL;
				}
				else
				{
					retCode = BLTIN_SUCCESS;
				}
			}

	//#		if it's a DD-var, hCVar* has value	
	//#		else will use INTER_VARIENT to hold the method-local variable's value	
		}


	//# # # # # # # # # # we have the format string and the local variable value # # # # # # # # #

	//#	if  DD variable
		if (pIB != NULL)
		{
		//#	if  curr_format '%L' || '%U'		
		//#			value_string <<= label or unit	
			if ( curr_format != NULL &&
				( (!_tstrcmp (curr_format, _T("%L")) ) || (!_tstrcmp (curr_format, _T("%U")))))
			{	/*	Print the label of the param. */
				wstring	value_string;	
				if ( ( pIB != NULL ) && !_tstrcmp (curr_format, _T("%L")) )
				{
					pIB->Label(value_string); 
                    (void)_tsprintf (temp_buf, wcslen(value_string.c_str()) + 1,_T("%s"), value_string.c_str());
				}
				else if ( (pVar != NULL) && (!_tstrcmp (curr_format, _T("%U"))) )
                {
					pVar->getUnitString(value_string); 
                    (void)_tsprintf (temp_buf, wcslen(value_string.c_str()) + 1, _T("%s"), value_string.c_str());
				}
				else // %L & %U are only valid on DD variables
				{
					retCode = BLTIN_INVALID_PROMPT;
					SDCLOG(CERR_LOG,"Method Format Error:%L & %U are only valid on DD variables\n");
					break;
				}
            }
			else 
		//#	else     // not L or U format
			{
				//is dynamic DD variable?
				if ( pIB->IsDynamic() && (isNeedUpdated) )
				{
					*bDyanmicVarvalChanged = true;
				}
				CValueVarient vValue{};
		//#		get Value to INTER_VARIANT (dynamic & non-dynamic the same) if it is not done yet
				if (VariantVarVal.GetVarType() == RUL_NULL && opRef.isOperationalIDValid())
				{
					//get value when the value is not available yet and the variable is not password

					retCode = m_pDevice->devAccessTypeValue2(&opRef, &vValue, READ);
					if (retCode == BLTIN_SUCCESS)
					{
						VariantVarVal = INTER_VARIANT::InitFrom(&vValue);
					}
					else if (retCode == BI_ABORT)
					{
						SDCLOG(CERR_LOG, "Method Format Error:accessing invalid variable.'Item Id = %d'\n", itemId);
						//#		error - exit	
						break;
					}
					else
					{

						retCode = BLTIN_CANNOT_READ_VARIABLE;


						VariantVarVal.Clear();
					}
				}
				else if (VariantVarVal.GetVarType() == RUL_NULL && !opRef.isOperationalIDValid())
				{
					//Couldn't get value neither from local variable nor from DD item
					retCode = BLTIN_CANNOT_READ_VARIABLE;
				}

				// If it is the HART we want to run refresh action here.
				if (retCode == BLTIN_SUCCESS && (m_pMeth->GetStartedProtocol() == nsEDDEngine::HART))
				{
					//Check to see if there is variable refresh action list.
					//If the answer is yes, each refresh action in the list will be executed
					//If the number of list is zero, ExecuteRefreshActionsInMethod() returns BLTIN_NOT_IMPLEMENTED
					INTER_VARIANT ivActionVal = VariantVarVal;
					retCode = m_pMeth->ExecuteRefreshActionsInMethod(pVar, &ivActionVal);
					if (retCode == SUCCESS)
					{
						// copy refresh action output value for comparison
						ivActionVal.CopyTo(&vValue);

						// copy refresh action input value for comparison
						CValueVarient vInitialValue{};
						VariantVarVal.CopyTo(&vInitialValue);

						// Do the comparision.
						if (vInitialValue == vValue)
						{
							// Refresh action did not chnaged final value by action builtin, 
							// therefore the final value is probably in param cache.
							retCode = m_pDevice->devAccessTypeValue2(&opRef, &vValue, READ);

							if (retCode == SUCCESS)
							{
								VariantVarVal = INTER_VARIANT::InitFrom(&vValue);
							}
							else if (retCode == BI_ABORT)
							{
								SDCLOG(CERR_LOG, "Method Format Error:accessing invalid variable.'Item Id = %d'\n", itemId);
								break;
							}
							else
							{
								retCode = BLTIN_CANNOT_READ_VARIABLE;
								//clean display value
								VariantVarVal.Clear();
							}
						}
						else
						{
							//if the refresh actions did chnage the final value by action builtin.
							VariantVarVal = ivActionVal;
						}
					}
					else if (retCode == BLTIN_NOT_IMPLEMENTED)
					{
						// there is no refresh action defined for this variable
						retCode = BLTIN_SUCCESS;
					}
					else
					{
						// failed to run the refresh action
						retCode = BLTIN_CANNOT_READ_VARIABLE;
					}
				}

				// Copy DD variable value to temp_buf in format and do scaled
				if (retCode == BLTIN_SUCCESS)
				{
					if (wcsstr(curr_param, _T("SCALING_FACTOR")))
					{
						retCode = doFormat(temp_buf, INTERNAL_BUFFER_SIZE, curr_format, 20, &VariantVarVal, pVar, false);
					}
					else
					{
						retCode = doFormat(temp_buf, INTERNAL_BUFFER_SIZE, curr_format, 20, &VariantVarVal, pVar, true);
					}
				}
			}
		}									
	//#	if local variable and the variable name is found		
		else if ( VariantVarVal.GetVarType() != RUL_NULL )
		{
			// copy string with local variable value to temp_buf
			if ( doFormat(curr_format, 20, VariantVarVal, temp_buf, INTERNAL_BUFFER_SIZE,pVar) )
			{
				// was an error
				_tstrcpy(temp_buf,_T("<Formatting Error>") );
			}
		}
	//#	- all the rest...		
		else if(retCode == BLTIN_DDS_ERROR)
		{	// have no user format
			_tstrcpy(temp_buf, _T("<Formatting Error>") );
		}// end-else  (not L or U)
		//else blank here
	//#	
	//#	Copy2Output(  value_string  )
		long retCode2 = CopyToOutbuf(out_ptr, remainingOut, temp_buf, -1);
		if ( retCode2 != BLTIN_SUCCESS )//testing retcode2
		{// probable full buffer
			retCode = retCode2;
			break;// error exit
		}	
		else if ( retCode != BLTIN_SUCCESS )//testing retcode
		{// variable reading error
			break;// error exit
		}	
	//#	Loop to the forever
	}// while loop forever

	if ( prompt_buf )  delete [] (prompt_buf);
	prompt_buf = NULL;

	return retCode;
}// end of bltin_format_string2()


//This function converts source value in INTER_VARIANT* type to value in tchar* type in specific format.
//It also scales value for display use if the value is integer or float.
//Augument retStr is output. The rest of auguments are inputs.
long CBuiltIn::
	doFormat (tchar* retStr, int rsLen, tchar* formatStr, int formatLen, INTER_VARIANT	*pSrcValue, hCVar *pVar, bool isDisplay)
{
	long retCode = BLTIN_SUCCESS;


	if ( (retStr == NULL) || (rsLen <= 0) || (formatLen <= 0) )
	{
		return BLTIN_CANNOT_READ_VARIABLE;	//nothing can be done
	}
	// else we have some unique formatting, we'll have to deal with it

	//find variable type
	variableType_t iValType = vT_undefined;
	if (pVar != NULL)
	{
		iValType = (variableType_t)pVar->VariableType();
	}
	
	if ( (pSrcValue->GetVarType() == RUL_NULL) && (iValType != vT_Password) )
	{
		//displayed value is not valid, maybe due to refresh action failure. return SUCCESS, i.e display no value
		return retCode;
	}

	switch(iValType)
	{
	case vT_Enumerated:
		{
			if (pSrcValue->isNumeric())
			{
				wstring tmp;
						
				if ( BLTIN_SUCCESS != ((hCEnum*)pVar)->procureString ((UINT32)(*pSrcValue), tmp) )
				{
					tmp = L"";
				}
				PS_Wcscpy(retStr, rsLen , tmp.c_str() );
			}
			else
			{
				tchar *szLocalString;
				pSrcValue->GetStringValue(&szLocalString);
				if (szLocalString != NULL)
				{
					PS_Wcscpy(retStr, rsLen, szLocalString);

					delete szLocalString;
					szLocalString = NULL;
				}
			}
		}
		break;
	case vT_BitEnumerated:
		{
			if (pSrcValue->isNumeric())
			{
				wstring tmp;
						
				if ( BLTIN_SUCCESS != ((hCBitEnum*)pVar)->procureString ((UINT32)(*pSrcValue), tmp) )
				{
					tmp = L"";
				}
				PS_Wcscpy(retStr, rsLen , tmp.c_str() );
			}
			else
			{
				tchar *szLocalString;
				pSrcValue->GetStringValue(&szLocalString);
				if (szLocalString != NULL)
				{
					PS_Wcscpy(retStr, rsLen, szLocalString);

					delete szLocalString;
					szLocalString = NULL;
				}
			}
		}
		break;
	case vT_Index:	// a string substitution...
		{
			wstring tmp;
						
			if ( BLTIN_SUCCESS != ((hCindex*)pVar)->procureString ((UINT32)(*pSrcValue), tmp) )
			{
				tmp = L"";
			}
			PS_Wcscpy(retStr, rsLen , tmp.c_str() );
		}
		break;
	case vT_Password:	// a string substitution "*********"
		{
			for(unsigned int i=0; i < 9; i++)
			{
				retStr[i] = '*';
			}
			retStr[9] = '\0';
		}
		break;
	case vT_HartDate:
		{
			struct tm StDateNTime;

			if (get_tm(&StDateNTime, (long)(int)(*pSrcValue)) == BI_SUCCESS)
			{
                _locale_t locale = _get_current_locale();
                _wcsftime_l(retStr, rsLen, _T("%x"), &StDateNTime, locale);	//short date for current locale
			}
		}
		break;
	case vT_DateAndTime:
		{
			struct tm StDateNTime;

			if (get_tm_from_date_and_time(&StDateNTime, (unsigned long long)(*pSrcValue)) == BI_SUCCESS)
			{
                _locale_t locale = _get_current_locale();
				_wcsftime_l(retStr, rsLen, _T("%x %X"), &StDateNTime, locale);	//date and time for current locale
			}
		}
		break;
	case vT_Time:
		{
			struct tm StDateNTime;

			if (get_tm_from_time(&StDateNTime, (unsigned long long)(*pSrcValue)) == BI_SUCCESS)
			{
				_locale_t locale = _get_current_locale();
				_wcsftime_l(retStr, rsLen, _T("%x %X"), &StDateNTime, locale);	//date and time for current locale
			}
		}
		break;
	case vT_TimeValue:
		{
			struct tm StDateNTime;
			if (get_tm_from_time_value(&StDateNTime, (long long)(*pSrcValue)) == BI_SUCCESS)
			{
				//find variable size
				unsigned int uiValSize = pVar->VariableSize();

				if (uiValSize <= 4)
				{
					/*4 bytes, absolute time duration*/
					/* display format hh : mm : ss */
					PS_VsnwPrintf(retStr, rsLen, L"%02d:%02d:%02d", StDateNTime.tm_hour,
						StDateNTime.tm_min, StDateNTime.tm_sec);
				}
				else
				{
					/* 8 bytes, relative date and time*/
					_locale_t locale = _get_current_locale();
					_wcsftime_l(retStr, rsLen, _T("%x %X"), &StDateNTime, locale);	//TimeValue for current locale
				}
			}
		}
		break;
	case vT_Duration:
		{
			_DURATION duration;

			if (split_duration((long long)(*pSrcValue), duration) == BI_SUCCESS)
			{
				/* display format dd : hh : mm : ss */
				PS_VsnwPrintf(retStr, rsLen, L"%d : %02d : %02d : %06.3f", duration.iDays, 
					duration.iHours, duration.iMinutes, duration.dSeconds); 				
			}
			
		}
		break;
	case vT_Integer:
	case vT_Unsigned:
	case vT_FloatgPt:
	case vT_Double:
		{
			//scaling
			if (pVar->IsNumeric() && isDisplay)
			{
				double dScalingFactor = pVar->GetScalingFactor();
				if( dScalingFactor < 0.9999 || dScalingFactor > 1.0001 )
				{
					double orgValue = (double)(*pSrcValue);
					*pSrcValue = orgValue * dScalingFactor;
				}
			}
		}
		//continue;
	default:
		{
			//#		Make temp_buf string	
			if ( doFormat(formatStr, formatLen, *pSrcValue, retStr, rsLen,pVar) )
			{// was an error
				PS_Wcscpy(retStr, rsLen , _T("<Formatting Error>") );
				retCode = BLTIN_CANNOT_READ_VARIABLE;
			}
		}
		break;
	}// endswitch iValType

	return retCode;
}


void CBuiltIn::PackedASCIIToASCII (

						 unsigned char *pbyPackedASCII
						 , unsigned short wPackedASCIISize
						 , char *pchASCII
						 )

{

	unsigned short wSize = wPackedASCIISize;

	int iLoopVar,iASCIICount = 0, iStreamIndex = 0, iASCIIIndex = 0;



	unsigned char byTemp;

	for (iLoopVar = 0; iLoopVar < wSize; )

	{

		switch(iASCIICount)

		{

			case 0:

				{

					/* The first byte....*/

					byTemp = pbyPackedASCII[iLoopVar];



					/* Get the six byte Packed ACSCII value */

					byTemp = byTemp & 0xfc;

					byTemp = byTemp >> 2;





					/* Now convert it to normal ASCII */

					/* Set bit 6 as the complement of bit 5 */

					unsigned char byTest = byTemp & 0x20;

					if(byTest)

					{

						byTemp = byTemp & 0xbf;

					}

					else

					{

						byTemp = byTemp | 0x40;

					}



					/* Now store this ascii value */

					pchASCII[iASCIIIndex] = byTemp;



					iASCIIIndex++;

					iASCIICount++;

					break;

				}

			case 1:

				{

					/* The second unsigned char....*/

					byTemp = pbyPackedASCII[iLoopVar];

					byTemp = byTemp << 6;



					byTemp = byTemp | ((pbyPackedASCII[iLoopVar + 1] & 0xf0) >> 2);



					/* Get the six unsigned char Packed ACSCII value */

					byTemp = byTemp & 0xfc;

					byTemp = byTemp >> 2;





					/* Now convert it to normal ASCII */

					/* Set bit 6 as the complement of bit 5 */

					if ((byTemp & 0x20))

					{

						byTemp = byTemp & 0xbf;

					}

					else

					{

						byTemp = byTemp | 0x40;

					}



					/* Now store this ascii value */

					pchASCII[iASCIIIndex] = byTemp;



					iASCIIIndex++;

					iASCIICount++;

					iLoopVar ++;

					break;

				}

			case 2:

				{

					/* The third unsigned char....*/

					byTemp = pbyPackedASCII[iLoopVar];

					byTemp = byTemp << 4;



					byTemp = byTemp | ((pbyPackedASCII[iLoopVar + 1] & 0xc0) >> 4);



					/* Get the six unsigned char Packed ACSCII value */

					byTemp = byTemp & 0xfc;

					byTemp = byTemp >> 2;





					/* Now convert it to normal ASCII */

					/* Set bit 6 as the complement of bit 5 */

					if((byTemp & 0x20))

					{

						byTemp = byTemp & 0xbf;

					}

					else

					{

						byTemp = byTemp | 0x40;

					}



					/* Now store this ascii value */

					pchASCII[iASCIIIndex] = byTemp;



					iASCIIIndex++;

					iASCIICount++;

					iLoopVar++;

					break;

				}

			case 3:

				{

					/* The fourth unsigned char....*/

					byTemp = pbyPackedASCII[iLoopVar];

					byTemp = byTemp << 2;



					/* Get the six unsigned char Packed ASCII value */

					byTemp = byTemp & 0xfc;

					byTemp = byTemp >> 2;





					/* Now convert it to normal ASCII */

					/* Set bit 6 as the complement of bit 5 */

					if((byTemp & 0x20))

					{

						byTemp = byTemp & 0xbf;

					}

					else

					{

						byTemp = byTemp | 0x40;

					}



					/* Now store this ascii value */

					pchASCII[iASCIIIndex] = byTemp;



					iASCIIIndex++;

					iASCIICount = 0;

					iLoopVar++;

					break;

				}



		}



	}

	pchASCII[iASCIIIndex] = 0;



	return;

}/* End of Function: PackedASCIIToASCII() */



/*F*/

/***************************************************************************

** FUNCTION NAME: ASCIIToPackedASCII

**

** PURPOSE		: Takes a char pointer to a common ASCII string,

**	and converts it to packed ASCII

**

** PARAMETER	:

**				- Char Pointer to to a common ASCII string

**	- unsigned char pointer to buffer where the converted packed ASCII string

**	will be copied

**	- The size of the packed ASCII string in bytes

**

** RETURN VALUE	: None

****************************************************************************/

void CBuiltIn::ASCIIToPackedASCII (

						 char *pchASCII

						 , unsigned char *pbyPackedASCIIOutput

						 , unsigned short *pwPackedASCIISize

						 )

{

	if ( (pchASCII == NULL) || (pbyPackedASCIIOutput == NULL) )

	{

		return;

	}

	PS_Strupr(pchASCII);



	int iLoopVar, iASCIICount = 0, iStreamIndex = 0;

	unsigned char pbyPackedASCII[4];

	unsigned char byTemp;

	DWORD dwPackedASCIITemp, *pdwTemp = NULL;

	for (iLoopVar = 0; iLoopVar < (int)strlen(pchASCII);iLoopVar++)  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
	{

		byTemp = pchASCII[iLoopVar];



		/* Truncate the BIT 7 & 6 and pack the unsigned char */

		byTemp = byTemp & 0x3F;

		byTemp = byTemp << 2;



		switch(iASCIICount)

		{

			case 0:

				{

					/* Initialize the unsigned char stream */

					pdwTemp = (DWORD *)pbyPackedASCII;

					*pdwTemp = 0;



					/* This is the first unsigned char */

					dwPackedASCIITemp = byTemp;

					dwPackedASCIITemp = dwPackedASCIITemp << 24;



					*pdwTemp = dwPackedASCIITemp;



					iASCIICount++;

					continue;

				}

			case 1:

				{

					/* This is the second unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 18;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					iASCIICount++;

					continue;

				}

			case 2:

				{

					/* This is the third unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 12;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					iASCIICount++;

					continue;

				}

			case 3:

				{

					/* This is the fourth & last unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 6;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* Now lets copy our unsigned char steam */

					pbyPackedASCIIOutput [0 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0xff000000) >> 24);

					pbyPackedASCIIOutput [1 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x00ff0000) >> 16);

					pbyPackedASCIIOutput [2 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x0000ff00) >> 8);



					iStreamIndex += 3;



					iASCIICount = 0;

					continue;

				}



		}



	}



	if (iASCIICount != 0)

	{

		byTemp = ' ';



		/* Truncate the BIT 7 & 6 and pack the unsigned char */

		byTemp = byTemp & 0x3F;

		byTemp = byTemp << 2;



		switch(iASCIICount)

		{

			case 1:

				{

					/* This is the second unsigned char */

					dwPackedASCIITemp = byTemp;

					dwPackedASCIITemp = dwPackedASCIITemp << 18;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* This is the third unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 12;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* This is the fourth & last unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 6;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* Now lets copy our unsigned char steam */

					pbyPackedASCIIOutput [0 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0xff000000) >> 24);

					pbyPackedASCIIOutput [1 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x00ff0000) >> 16);

					pbyPackedASCIIOutput [2 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x0000ff00) >> 8);



					iStreamIndex += 3;



					iASCIICount = 0;

					break;

				}

			case 2:

				{

					/* This is the third unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 12;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* This is the fourth & last unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 6;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* Now lets copy our unsigned char steam */

					pbyPackedASCIIOutput [0 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0xff000000) >> 24);

					pbyPackedASCIIOutput [1 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x00ff0000) >> 16);

					pbyPackedASCIIOutput [2 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x0000ff00) >> 8);



					iStreamIndex += 3;



					iASCIICount = 0;

					break;

				}

			case 3:

					/* This is the fourth & last unsigned char */

					dwPackedASCIITemp = byTemp;



					dwPackedASCIITemp = dwPackedASCIITemp << 6;



					*pdwTemp = *pdwTemp | dwPackedASCIITemp;



					/* Now lets copy our unsigned char steam */

					pbyPackedASCIIOutput [0 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0xff000000) >> 24);

					pbyPackedASCIIOutput [1 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x00ff0000) >> 16);

					pbyPackedASCIIOutput [2 + iStreamIndex] = (unsigned char) ((*pdwTemp & 0x0000ff00) >> 8);



					iStreamIndex += 3;



					iASCIICount = 0;

					break;

		}

	}



	*pwPackedASCIISize = iStreamIndex;



	return;

}/* End of Function: ASCIIToPackedASCII () */





