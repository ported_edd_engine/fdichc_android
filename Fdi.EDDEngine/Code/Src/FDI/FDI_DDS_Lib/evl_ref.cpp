/**
 *	@(#) $Id: evl_ref.cpp,v 1.13 2012/09/12 22:40:59 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  ITEM_ID_LIST and ITEM_ID.
 */


#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif

//
// build_orig_reference
//
static int
build_orig_reference(ENV_INFO *env_info, wchar_t *arg_buffer, size_t buf_size, OP_REF_TRAIL *ref)
{
	int rc = DDL_SUCCESS;

	wchar_t name_buf[64] = {0};	// Buffer for the name
	wchar_t suffix_buf[64] = {0};	// Buffer for the suffix
	const size_t suffix_buf_size = (sizeof(suffix_buf)/sizeof(suffix_buf[0]));
	
	// Get the name of the op_id
	rc = item_id_to_name( env_info->block_handle, env_info, ref->op_info.id, name_buf, (sizeof(name_buf)/sizeof(name_buf[0])) );

	if (rc != DDL_SUCCESS)
	{
		return rc;
	}

	PS_Wcscpy(arg_buffer, buf_size, name_buf);	// copy to the output

	switch (ref->op_info.type)		// Find suffix
	{
		case VARIABLE_ITYPE:	// No suffix needed
			break;

		case ARRAY_ITYPE:
			// create the "[n]" array suffix
			// Not finished - check out
			PS_VsnwPrintf(suffix_buf, suffix_buf_size, L"[%d]", ref->op_info.member);
			break;

		case RECORD_ITYPE:
			{
				::nsEDDEngine::FLAT_RECORD record;// = {0};
				RETURN_LIST error_list = {0};
				
				::nsEDDEngine::AttributeNameSet mask=::nsEDDEngine::members; // (unsigned long) RECORD_MEMBERS
				// Fetch the binary
				rc = resolve_fetch(env_info, ref->op_info.id, mask, &record,
					(unsigned short) RECORD_ITYPE, env_info->block_handle);

				if (!record.masks.bin_hooked.isMember(::nsEDDEngine::members)) {	// Make sure we have binary for MEMBERS
					rc = DDL_ENCODING_ERROR;
				}

				if (rc == DDL_SUCCESS)
				{
					// Eval the RECORD_MEMBERS attribute
//					rc = eval_item_record(&record, RECORD_MEMBERS, env_info, &error_list);
					mask=::nsEDDEngine::members;
					rc = eval_item_any(&record, mask, env_info, &error_list);

					if ((rc == DDL_SUCCESS) && (record.masks.attr_avail.isMember(::nsEDDEngine::members)) )
					{
						if (ref->op_info.member > record.members.count)	// Make sure our sub_index is in range
						{
							rc = DDL_ENCODING_ERROR;
						}
						else
						{
							wchar_t member_name_buf[64] = {0};	// Buffer for the Member name
							
							// Look up the member id
							ITEM_ID member_id = record.members.list[ref->op_info.member - 1].name;

							// Convert to from id to name
							rc = item_id_to_name(env_info->block_handle, env_info, member_id, member_name_buf, (sizeof(member_name_buf)/sizeof(member_name_buf[0])));

							if (rc == DDL_SUCCESS)		// Create the Record member notation
							{
								PS_VsnwPrintf(suffix_buf, suffix_buf_size, L".%s", member_name_buf);
							}
						}
					}

//					ddl_free_members_list(&record.members, FREE_ATTR);
				}
			}
			break;

		default:
			rc = DDL_ENCODING_ERROR;
			break;
	}

	if (rc == DDL_SUCCESS)
	{
		PS_Wcscat(arg_buffer, buf_size, suffix_buf);		// Add the suffix to the name
	}

	return rc;
}


//
// ddl_parse_argument_list - Parse the Method Argument_List
//			return a string representation
//
static int
ddl_parse_argument_list(
	unsigned char **chunkp,
	DDL_UINT       *size,
	STRING		   *str,
	nsEDDEngine::OP_REF_LIST    *depinfo,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed,
	unsigned int    options)
{
	int			rc = DDL_SUCCESS;

	DDL_UINT	tag = 0;
	DDL_UINT	len = 0;
	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	if (tag != ARGUMENT_LIST_TAG)
		return DDL_ENCODING_ERROR;

	*size -= len;	// adjust size for exit

	OP_REF_TRAIL	ref = {STANDARD_TYPE,0};					// Stores the reference for each argument

	wchar_t		args_buffer[256] = {0};			// Complete argument string
	const size_t args_buf_size = (sizeof(args_buffer)/sizeof(args_buffer[0]));

	wchar_t		single_arg_buffer[64] = {0};	// Individual Argument string
	const size_t single_buf_size = (sizeof(single_arg_buffer)/sizeof(single_arg_buffer[0]));

	while (len > 0)
	{
		rc = ddl_parse_ref(chunkp, &len, &ref, depinfo, env_info, var_needed, options);

		if (rc != DDL_SUCCESS) {
			break;
		}

		single_arg_buffer[0] = '\0';	// start with an empty buffer

		switch (ref.op_info.type)
		{
		case STRING_LITERAL_ITYPE:
			PS_VsnwPrintf(single_arg_buffer, single_buf_size, L"\"%s\"", ref.expr.val.s.str);	// Strings have quotes around them
			break;
		case CONST_INTEGER_ITYPE:
			PS_VsnwPrintf(single_arg_buffer, single_buf_size, L"%lld", ref.expr.val.i);
			break;
		case CONST_UNSIGNED_ITYPE:
			PS_VsnwPrintf(single_arg_buffer, single_buf_size, L"%llu", ref.expr.val.u);
			break;
		case CONST_DOUBLE_ITYPE:
			PS_VsnwPrintf(single_arg_buffer, single_buf_size, L"%lf", ref.expr.val.d);
			break;
		case CONST_FLOAT_ITYPE:
			PS_VsnwPrintf(single_arg_buffer, single_buf_size, L"%f", ref.expr.val.f);
			break;
		case VARIABLE_ITYPE:
		case RECORD_ITYPE:
		case ARRAY_ITYPE:
			{
				// Build the original reference argument as found in the EDD itself
				rc = build_orig_reference(env_info, single_arg_buffer, single_buf_size, &ref);
			}
			break;
		default:
			rc = DDL_ENCODING_ERROR;
			break;
		}

		if (rc == DDL_SUCCESS)
		{
			if (args_buffer[0] != '\0')			// Add a delimiter before all but the first one
			{
				PS_Wcscat(args_buffer, args_buf_size, L",");		// Args are delimited by a ','
			}

			PS_Wcscat(args_buffer, args_buf_size, single_arg_buffer);	// Add to the list
		}

		ddl_free_op_ref_trail(&ref);		// Clean out used ref for use next time.
		
		if(rc != DDL_SUCCESS)
			break;
	}

	if (rc == DDL_SUCCESS)
	{
		str->len = (unsigned short)wcslen(args_buffer);
		str->str = (wchar_t*)malloc((str->len + 1) * sizeof(wchar_t));
		PS_Wcscpy(str->str, str->len+1, args_buffer);
		str->flags = STRING::FREE_STRING;
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_ref will parse the binary data chunk and load
 *		a REFERENCE_INFO structure and call resolve_ref() to get
 *		the OP_REF_TRAIL loaded. If the pointer to the structure is
 *		NULL, then ddl_pars_ref just unloads the binary without
 *		loading a structure.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an OP_REF_TRAIL
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *      options - these are flags which are passed through to resolve_ref()
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an OP_REF_TRAIL
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_ENCODING_ERROR
 *		error returns from resolve_ref(), ddl_eval_expr(),
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

/*
 * These defines specify the extra length to be appended to the
 * OP_REF_TRAIL_LIST
 */

#define NO_ET	   0		/* no extra trail count */
#define PARAM_ET   1		/* PARM and PARM_LIST extra trail count */
#define BLOCK_ET   2		/* BLOCK extra trail count */

extern const int resolve_id_table[];	/* converts parse tags to item types */

int
ddl_parse_ref(
unsigned char **chunkp,
DDL_UINT       *xsize,
OP_REF_TRAIL   *ref,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
unsigned int    options,
bool ignore_validity)
{
	REF_INFO_LIST   info_list = {0};	/* list where the parsed binary is stored */
	REF_INFO       *info_ptr;			/* temp pointer for info_list */
	unsigned short  info_inc = 0;		/* info list counter */
	DDL_UINT        tag;				/* stores the binary REFERENCE type number */
	int             rc = DDL_SUCCESS;	/* return code */
	int             parse = TRUE;		/* conditional for while loop */
	DDL_UINT		ref_len=0;

	ASSERT_DBG(chunkp && *chunkp && xsize);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_REF);
#endif


	/*
	 * if they want a reference to be loaded
	 */

	if (ref != NULL) {

		info_ptr = info_list.list;

		bool first=true;

		while (parse) {

			if (info_inc >= REF_INFO_LIST_SIZE) {

				return DDL_ENCODING_ERROR;
			}

			/*
			 * Parse the tag to find out what kind of reference
			 * this is.
			 */
			if(first)
			{
				DDL_PARSE_TAG(chunkp, xsize, &tag, (DDL_UINT *) &ref_len);
				first=false;
				(*xsize)-=ref_len;
			}
			else
			{
				DDL_PARSE_TAG(chunkp,  &ref_len, &tag, (DDL_UINT *) NULL);
			}

			if(tag!=REFERENCE_TAG)
			{
//				ASSERT(0);
				return DDL_ENCODING_ERROR;
			}

			DDL_PARSE_TAG(chunkp, &ref_len, &tag, (DDL_UINT *) NULL_PTR);

			switch (tag) {

			case SEPARATOR_REF:
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.type = (ITEM_TYPE) resolve_id_table[tag];
				ref->op_info.member = 0;
				if (options & RESOLVE_DESC_REF)
				{
					ref->desc_id = 0;
					ref->desc_type = (ITEM_TYPE) resolve_id_table[tag];
				}
				parse = FALSE;
				break;


			case ROWBREAK_REF:
				ref->op_ref_type = STANDARD_TYPE;
				ref->op_info.type = (ITEM_TYPE) resolve_id_table[tag];
				ref->op_info.member = 0;
				if (options & RESOLVE_DESC_REF)
				{
					ref->desc_id = 0;
					ref->desc_type = (ITEM_TYPE) resolve_id_table[tag];
				}
				parse = FALSE;
				break;

			case CONSTANT_REF:
			case EXTERNAL_REF:
				{
					rc = ddl_eval_expr(chunkp, &ref_len, &ref->expr, depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						PS_TRACE(L"\n*** error (ddl_parse_ref) : could not eval CONSTANT_REF");
						return rc;
					}

                    ref->op_ref_type = STANDARD_TYPE;
					ref->op_info.id = 0;
					ref->op_info.member = 0;
					ref->op_info.type = get_constant_itype(&ref->expr);

					if (rc == DDL_SUCCESS)
					{
						if (options & RESOLVE_DESC_REF)
						{
							ref->desc_id = 0;
							ref->desc_type = ref->op_info.type;
						}
					}

					parse = FALSE;
				}
				break;
			
			case REF_WITH_ARGS_REF:
				{
					STRING arg_string = {0};

					rc = ddl_parse_argument_list(chunkp, &ref_len, &arg_string, depinfo, env_info, var_needed, options);
					if (rc != DDL_SUCCESS) {
						return rc;
					}

					info_ptr->type = (unsigned short) tag;
					memcpy (&info_ptr->val.str, &arg_string, sizeof(arg_string));	// Save args for resolve_ref

					// arg_string contents were copied, including pointers, into info_ptr->val.str
					// info_ptr->val.str is now responsible to make sure that memory gets cleaned up
				}
				break;

			case ITEM_ID_REF:
			case ITEM_ARRAY_ID_REF:
			case COLLECTION_ID_REF:
			case BLOCK_ID_REF:
			case VARIABLE_ID_REF:
			case MENU_ID_REF:
			case EDIT_DISP_ID_REF:
			case METHOD_ID_REF:
			case REFRESH_ID_REF:
			case UNIT_ID_REF:
			case WAO_ID_REF:
			case RECORD_ID_REF:
			case ARRAY_ID_REF:
			case VAR_LIST_ID_REF:
			case RESP_CODES_ID_REF:
			case FILE_ID_REF:
			case CHART_ID_REF:
			case GRAPH_ID_REF:
			case AXIS_ID_REF:
			case WAVEFORM_ID_REF:
			case SOURCE_ID_REF:
			case LIST_ID_REF:
			case IMAGE_ID_REF:
			case GRID_ID_REF:
			case BLOCK_B_ID_REF:
			case PLUGIN_ID_REF:
			case BLOB_ID_REF:
			case TEMPLATE_ID_REF:
			case COMPONENT_ID_REF:
			case COMPONENT_FOLDER_ID_REF:
			case COMPONENT_REFERENCE_ID_REF:
			case COMPONENT_RELATION_ID_REF:
			case INTERFACE_ID_REF:
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, &ref_len, &info_ptr->val.id);

				/*
				 * If it's a simple item_id we don't call
				 * resolve
				 */

				if (info_inc == 0) {

					if (options & RESOLVE_OP_REF)
					{
						ref->op_ref_type = STANDARD_TYPE;
						ref->op_info.id = info_ptr->val.id;
						ref->op_info.type = (ITEM_TYPE) resolve_id_table[info_ptr->type];
						ref->op_info.member = 0;
					}
					if (options & RESOLVE_DESC_REF) {

						ref->desc_id = info_ptr->val.id;
						ref->desc_type = (ITEM_TYPE) resolve_id_table[info_ptr->type];
					}
				}
				else {

					if (info_ptr->type == ITEM_ID_REF) {

						if (info_list.list[info_inc - 1].type == VIA_COLLECTION_REF) {

							info_ptr->type = COLLECTION_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_ITEM_ARRAY_REF) {

							info_ptr->type = ITEM_ARRAY_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_FILE_REF) {

							info_ptr->type = FILE_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_CHART_REF) {

							info_ptr->type = CHART_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_GRAPH_REF) {

							info_ptr->type = GRAPH_ID_REF;
						}
						else if (info_list.list[info_inc - 1].type == VIA_SOURCE_REF) {

							info_ptr->type = SOURCE_ID_REF;
						}
					}

					info_list.count = info_inc + 1;

					rc = resolve_ref(&info_list, ref, depinfo, env_info, var_needed, options, NO_ET, ignore_validity);
				}
				parse = FALSE;
				break;

			case VIA_LIST_REF:
			case VIA_ITEM_ARRAY_REF:	/* item array reference */
			case VIA_ARRAY_REF:	/* profibus array reference */
			case VIA_BLOCK_REF:	// block characteristics ref - Not supported in PROFIBUS
			{
					EXPR expr = {0};				/* expression for converting ARRAY_REFs */

					info_ptr->type = (unsigned short) tag;
					rc = ddl_eval_expr(chunkp, &ref_len, &expr, depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}

					switch (expr.type) {
					case UNSIGNED:
					case INDEX:
					case ENUMERATED:
					case BIT_ENUMERATED:
						info_ptr->val.index = (ITEM_ID) expr.val.u;
						break;

					case INTEGER:
						info_ptr->val.index = (ITEM_ID) expr.val.i;
						break;

					default:
						return DDL_ENCODING_ERROR;

					}
				}
				break;

			case VIA_BITENUM_REF:
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, &ref_len, &info_ptr->val.index);
				break;

			case VIA_COLLECTION_REF:	/* collection reference */
			case VIA_RECORD_REF:	/* record reference */
			case VIA_FILE_REF:
			case VIA_CHART_REF:
			case VIA_GRAPH_REF:
			case VIA_SOURCE_REF:
			case VIA_VAR_LIST_REF:	
			case VIA_XBLOCK_REF:
			case VIA_ATTRIBUTE_REF:
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, &ref_len, &info_ptr->val.member);
				break;

			case VIA_LOCAL_PARAM_REF:
			case VIA_PARAM_REF:	/* param name ref */
			case VIA_PARAM_LIST_REF:	
				info_ptr->type = (unsigned short) tag;
				DDL_PARSE_INTEGER(chunkp, &ref_len, &info_ptr->val.member);

				/*
				 * Because this is a PARAM_LIST or a
				 * PARAM_LIST_REF we're at the end
				 */

				if(ref_len==0)
				{
					info_list.count = info_inc + 1;

					rc = resolve_ref(&info_list, ref, depinfo, env_info, var_needed, options, PARAM_ET, ignore_validity);
					parse = FALSE;
				}
				break;
			case BLOCK_CHAR_REF:
			
				info_ptr->type = (unsigned short)tag;
				DDL_PARSE_INTEGER(chunkp, &ref_len, &info_ptr->val.member);

				/*
				* Because this is a BLOCK_REF we're at the end
				*/

				if (ref_len == 0)
				{
					info_list.count = info_inc + 1;

					rc = resolve_ref(&info_list, ref, depinfo, env_info, var_needed, options, BLOCK_ET, ignore_validity);
					parse = FALSE;
				}
				break;
			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
			}
			/*******************************************************************
			 * LOOK HERE to see info_inc AND info_ptr INCREMENTED !!!
			 ******************************************************************/
			info_inc++;
			info_ptr++;
		}
	}
	else {			/* parsing without saving a value */

		while (parse) {

			DDL_PARSE_TAG(chunkp, xsize, &tag, (DDL_UINT *) NULL_PTR);

			switch (tag) {

			case SEPARATOR_REF:
				parse = FALSE;
				break;

			case ROWBREAK_REF:
//			case COLUMNBREAK_REF:
				parse = FALSE;
				break;

			case CONSTANT_REF:
				rc = ddl_eval_expr(chunkp, xsize, (EXPR *) NULL, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				parse = FALSE;
				break;

			case REF_WITH_ARGS_REF:
				{
					STRING arg_string = {0};

					rc = ddl_parse_argument_list(chunkp, xsize, &arg_string, depinfo, env_info, var_needed, options);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
				break;

			case IMAGE_ID_REF:
			case ITEM_ID_REF:
			case ITEM_ARRAY_ID_REF:
			case COLLECTION_ID_REF:
			case BLOCK_ID_REF:
			case VARIABLE_ID_REF:
			case MENU_ID_REF:
			case EDIT_DISP_ID_REF:
			case METHOD_ID_REF:
			case REFRESH_ID_REF:
			case UNIT_ID_REF:
			case WAO_ID_REF:
			case RECORD_ID_REF:
			case ARRAY_ID_REF:
			case VAR_LIST_ID_REF:
			case RESP_CODES_ID_REF:
			case FILE_ID_REF:
			case CHART_ID_REF:
			case GRAPH_ID_REF:
			case AXIS_ID_REF:
			case WAVEFORM_ID_REF:
			case SOURCE_ID_REF:
			case LIST_ID_REF:
			case GRID_ID_REF:
			case BLOCK_B_ID_REF:
			case PLUGIN_ID_REF:
			case BLOB_ID_REF:
			case TEMPLATE_ID_REF:
			case COMPONENT_ID_REF:
			case COMPONENT_FOLDER_ID_REF:
			case COMPONENT_REFERENCE_ID_REF:
			case COMPONENT_RELATION_ID_REF:
			case INTERFACE_ID_REF:
			case VIA_PARAM_REF:
				DDL_PARSE_INTEGER(chunkp, xsize, (DDL_UINT *) NULL_PTR);
				parse = FALSE;
				break;

			case VIA_LIST_REF:
			case VIA_ITEM_ARRAY_REF:
			case VIA_ARRAY_REF:
				rc = ddl_eval_expr(chunkp, xsize, (EXPR *) NULL, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				break;

			case VIA_COLLECTION_REF:
			case VIA_RECORD_REF:
			case VIA_FILE_REF:
			case VIA_CHART_REF:
			case VIA_GRAPH_REF:
			case VIA_SOURCE_REF:
			case VIA_BITENUM_REF:
				DDL_PARSE_INTEGER(chunkp, xsize, (DDL_UINT *) NULL_PTR);
				break;

			default:
				ASSERT(0);
				return DDL_ENCODING_ERROR;
			}
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_item_id
 *	ShortDesc: Parse an item_id.
 *
 *	Description:
 *		ddl_parse_item_id calls ddl_parse_ref and converts the output
 *		to an ITEM_ID
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		item_id - pointer to an ITEM_ID
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		item_id - pointer to an ITEM_ID
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_item_id(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ID        *item_id,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc = DDL_SUCCESS;	// return code
	OP_REF_TRAIL	ref = {STANDARD_TYPE, 0};			// temp OP_REF_TRAIL for ddl_parse_ref() call

	if (item_id) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF);

		if (rc == DDL_SUCCESS)
		{
			*item_id = ref.desc_id;		// This is the only part we wanted
		}
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	ddl_free_op_ref_trail(&ref);		// Clean out used ref.

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_data_item
 *	ShortDesc: Parse an data item.
 *
 *	Description:
 *		Parse an data itemt
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		data_item - pointer to an DATA_ITEM
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		item_id - pointer to an ITEM_ID
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *
 ********************************************************************************/
int
ddl_parse_data_item(
unsigned char **chunkp,
DDL_UINT       *size,
DATA_ITEM      *data_item,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc = DDL_SUCCESS;	/* return code */
	OP_REF_TRAIL    ref = {STANDARD_TYPE,0};	/* temp OP_REF_TRAIL for calling parse_ref()*/


	if (data_item) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF | RESOLVE_OP_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}
		
		if (ref.desc_id == 0)
		{
			if (ref.expr.type == INTEGER)
			{
				data_item->type = DATA_CONSTANT;
				data_item->data.iconst = (long)ref.expr.val.i;

			}
            else if (ref.expr.type == FLOATG_PT)
			{
				data_item->type = DATA_FLOATING;
				data_item->data.fconst = ref.expr.val.f;
			}			
            else if (ref.expr.type == DOUBLEG_PT)
			{
				data_item->type = DATA_DOUBLE;
				data_item->data.dconst = ref.expr.val.d;
			}
			else if (ref.expr.type == UNSIGNED)
			{
				data_item->type = DATA_UNSIGNED;
				data_item->data.uconst = (unsigned long)ref.expr.val.u;
			}
			else
			{
				ASSERT_DBG(false);
				return DDL_ENCODING_ERROR;
			}
		}
		else
		{
			data_item->type = DATA_REFERENCE;
			memcpy(&data_item->data.ref, &ref, sizeof(OP_REF_TRAIL));

		}
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_op_ref_trail
 *	ShortDesc: Parse an op_ref_trail
 *
 *	Description:
 *		ddl_parse_op_ref_trail calls ddl_parse_ref with the options
 *		flag set for DESC_REF, OP_REF and OP_REF_TRAIL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an OP_REF_TRAIL
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an OP_REF_TRAIL
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: steve beyerl
 *
 ********************************************************************************/

int
ddl_parse_op_ref_trail(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_TRAIL   *op_ref_trail,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
bool ignore_validity)
{
	int             rc;	/* return code */
	unsigned int    resolve_options = 0;	/* temp options for resolving */

	if (op_ref_trail) {

		resolve_options = RESOLVE_OP_REF | RESOLVE_DESC_REF | RESOLVE_TRAIL;

		rc = ddl_parse_ref(chunkp, size, op_ref_trail, depinfo,
			env_info, var_needed, resolve_options, ignore_validity);
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_op_ref_trail_list
 *	ShortDesc: Parse the binary and load an OP_REF_TRAIL_LIST.
 *
 *	Description:
 *		ddl_parse_op_ref_trail_list will parse the binary data chunk and
 *		load a OP_REF_TRAIL_LIST structure unless the pointer to the
 *		OP_REF_TRAIL_LIST passed in is NULL, in which case it will
 *		simply read through the binary data.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		op_ref_trail_list - pointer to a OP_REF_TRAIL_LIST structure
 *			where the result will be stored.  If this is NULL, no result
 *			is computed or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		op_ref_trail_list - pointer to an OP_REF_TRAIL_LIST structure
 *				containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from:
 *			ddl_parse_op_ref_trail()
 *			ddl_shrink_op_ref_trail_list()
 *
 *	Author: steve beyerl
 *
 **********************************************************************/

int
ddl_parse_op_ref_trail_list(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_TRAIL_LIST *op_ref_trail_list,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	OP_REF_TRAIL   *temp_ptr;	/* temporary pointer */
	DDL_UINT        tag = 0;		/** "label" associated with current chunk */



	ASSERT_DBG(chunkp && *chunkp && size);

	/*
	 * Parse an OP_REF_TRAIL_LIST.  If we need more room in the array of
	 * structures, realloc more room.  Then call ddl_parse_op_ref_trail to
	 * load the next OP_REF_TRAIL_LIST structure.
	 */
	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

	if(tag!=REFERENCE_LIST_TAG)
	{
		PS_TRACE(L"\n*** error (ddl_parse_reflist) : invalid tag %d",tag);
		return DDL_ENCODING_ERROR;
	}


	if (op_ref_trail_list != NULL) {

		/*
		 * parse the binary and load the OP_REF_TRAIL_LIST structure
		 */

		while (*size > 0) {

			if (op_ref_trail_list->count == op_ref_trail_list->limit) {

				/*
				 * reallocate the OP_REF_TRAIL_LIST structure
				 * for more OP_REF_TRAILs
				 */

				op_ref_trail_list->limit += REF_LIST_INC;

				/*
				 * realloc OP_REF_TRAIL  array
				 */

				temp_ptr = (OP_REF_TRAIL *) realloc((void *) op_ref_trail_list->list,
					(size_t) (op_ref_trail_list->limit * sizeof(OP_REF_TRAIL)));

				if (temp_ptr == NULL) {
					op_ref_trail_list->limit = op_ref_trail_list->count;
					ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				op_ref_trail_list->list = temp_ptr;


				/*
				 * Initialize the new OP_REF_TRAILs to zero
				 */

				memset((char *) &op_ref_trail_list->list[op_ref_trail_list->count],
					0, (REF_LIST_INC * sizeof(OP_REF_TRAIL)));

			}

			rc = ddl_parse_op_ref_trail(chunkp, size,
				&op_ref_trail_list->list[op_ref_trail_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
				return rc;
			}

			op_ref_trail_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_op_ref_trail_list(op_ref_trail_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the OP_REF_TRAIL_LIST structure
			 */

			rc = ddl_parse_op_ref_trail(chunkp, size, (OP_REF_TRAIL *) NULL,
				depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_desc_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_desc_ref calls ddl_parse_ref and converts the output
 *		to an DESC_REF
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		desc_ref - pointer to an DESC_REF
 *		depinfo - pointer to a DEPINFO structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		desc_ref - pointer to an DESC_REF
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_desc_ref(
unsigned char **chunkp,
DDL_UINT       *size,
DESC_REF       *desc_ref,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc = DDL_SUCCESS;	/* return code */
	OP_REF_TRAIL    ref = {STANDARD_TYPE,0};	/* temp OP_REF_TRAIL */

	if (desc_ref) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		desc_ref->id = ref.desc_id;
		desc_ref->type = ref.desc_type;
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_op_ref
 *	ShortDesc: Parse a reference.
 *
 *	Description:
 *		ddl_parse_desc_ref calls ddl_parse_ref and converts the output
 *		to an OP_REF
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		op_ref - pointer to an OP_REF
 *		depinfo - pointer to a DEPINFO structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		op_ref - pointer to an OP_REF
 *		var_needed - pointer to a DEPEND_ITEM structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *		pBitmask - optional pointer to an unsigned long in which to store the bit_mask value
 *
 *	Returns:
 *		error returns from ddl_parse_ref()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_op_ref(
unsigned char **chunkp,
DDL_UINT       *size,
nsEDDEngine::OP_REF         *op_ref,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
unsigned long  *pBitmask /* = nullptr */)
{
	int             rc = DDL_SUCCESS;	/* return code */

	OP_REF_TRAIL    ref = {STANDARD_TYPE,0};	/* temp OP_REF_TRAIL */

	if (op_ref) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_OP_REF);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		
		//for crossblock
		op_ref->block_instance = ref.block_instance;
		if( ref.op_ref_type == STANDARD_TYPE )
		{
			//create an equals operator for the op_ref that takes in op_ref_trail.
			op_ref->op_ref_type		= nsEDDEngine::STANDARD_TYPE;

			op_ref->op_info.id		= ref.op_info.id;
			op_ref->op_info.member	= ref.op_info.member;
			op_ref->op_info.type	= (nsEDDEngine::ITEM_TYPE)ref.op_info.type;			
		}
		else
		{
            op_ref->op_ref_type = ::nsEDDEngine::COMPLEX_TYPE;

			op_ref->op_info_list.count = ref.op_info_list.count;
            op_ref->op_info_list.list = new nsEDDEngine::OP_REF_INFO[op_ref->op_info_list.count];

			for( int i = 0; i < op_ref->op_info_list.count; i++ )
			{
				op_ref->op_info_list.list[i].id		= ref.op_info_list.list[i].id;
				op_ref->op_info_list.list[i].type	= (nsEDDEngine::ITEM_TYPE)ref.op_info_list.list[i].type;
				op_ref->op_info_list.list[i].member	= ref.op_info_list.list[i].member;
			}

		}

		free(ref.op_info_list.list);	// Free the op_info_list, if it was used
		ref.op_info_list.list = nullptr;

		if (pBitmask != nullptr)
		{
			*pBitmask = ref.bit_mask;	// Return the bit_mask value, if it was requested
		}
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_ref_choice
 *	ShortDesc: Choose the correct reference from a binary.
 *
 *	Description:
 *		ddl_ref_choice will parse the binary for a reference,
 *		according to the current conditionals (if any).  The value of
 *		the reference is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to an ITEM_ID structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to an ITEM_ID structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_ref().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_ref_choice(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ID        *item_id,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_REF_CHOICE);
#endif


	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (item_id && (val.chunk)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (item_id == NULL));

		/* We have finally gotten to the actual value!  Parse it. */

		rc = ddl_parse_item_id(&val.chunk, &val.size, item_id, depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (item_id && data_valid) {
			*data_valid = TRUE;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_attr_ref
 *
 *	ShortDesc: Evaluate a reference
 *
 * Description:
 *	The eval_attr_ref function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference and size should specify its size. If ref is
 *	not a null pointer, the item id is returned in ref. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref - pointer to a REFERENCE where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref - pointer to a REFERENCE structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_ref_choice(), ddl_shrink_depinfo()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
eval_ref(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void * /*optional*/)
{
	ITEM_ID*		ref = static_cast<ITEM_ID*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_REF);
#endif

	valid = 0;

	if (ref) {
		*ref = 0;	/** initialize output parameter **/
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_parse_item_id(&chunk, &size, ref, depinfo, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {

		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	return DDL_SUCCESS;
}

int
eval_attr_ref(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *  /*optional */)
{
	ITEM_ID*		ref = static_cast<ITEM_ID*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_REF);
#endif

	valid = 0;

	if (ref) {
		*ref = 0;	/** initialize output parameter **/
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_ref_choice(&chunk, &size, ref, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {

		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	if (ref && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_desc_ref_choice
 *	ShortDesc: Choose the correct descriptive reference from a binary.
 *
 *	Description:
 *		ddl_desc_ref_choice will parse the binary for a descriptive reference,
 *		according to the current conditionals (if any).  The value of
 *		the reference is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		desc_ref - pointer to an DESC_REF structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		data_valid - pointer to indicator whether we found data
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		desc_ref - pointer to an DESC_REF structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_desc_ref().
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

static int
ddl_desc_ref_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DESC_REF       *desc_ref,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int rc=DDL_SUCCESS;

	CHUNK val = {0};	// Stores the binary chunk found by this conditional

	if (data_valid) {
		*data_valid = FALSE;
	}

	val.chunk=*chunkp;
	val.size=*size;

	// If a value is requested and a chunk was found
	if (desc_ref && (val.chunk)) {

		// If the calling routine is expecting a value, data_valid cannot be NULL
		ASSERT_DBG((data_valid != NULL) || (desc_ref == NULL));

		// We have finally gotten to the actual value!  Parse it.
		rc = ddl_parse_desc_ref(&val.chunk, &val.size, desc_ref, depinfo, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (desc_ref && data_valid) {	// If we were looking for a value, data_valid
			*data_valid = TRUE;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_desc_ref
 *
 *	ShortDesc: Evaluate a reference and produce a DESC REF
 *
 * Description:
 *	The eval_attr_desc_ref function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference and size should specify its size. If voidP is
 *	not a null pointer, the desc ref is returned in voidP. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		voidP - pointer to a DESC_REF where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		voidP - pointer to a DESC_REF structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_desc_ref_choice(), ddl_shrink_depinfo()
 *
 *	Author: Mike Dieter
 *
 *********************************************************************/

int
eval_attr_desc_ref(	unsigned char	*chunk,
					DDL_UINT		 size,
					void			*voidP,
					nsEDDEngine::OP_REF_LIST		*depinfo,
					ENV_INFO		*env_info,
					nsEDDEngine::OP_REF			*var_needed,
					void *)
{
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	DESC_REF *desc_ref = static_cast<DESC_REF*>(voidP);

	int data_valid = FALSE;	// indicates whether we have valid data

	if (desc_ref) {			// initialize the output data structure
		desc_ref->id = desc_ref->type = 0;
	}

	if (depinfo) {			// initialize the dependency structure
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	// Parse the desc ref, running conditionals, if found
	int rc = ddl_desc_ref_choice(&chunk, &size, desc_ref, depinfo, &data_valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	if (desc_ref && (data_valid == FALSE)) {		// If we asked for data, but didn't get it, return default
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 *	Name: ddl_free_actions
 *	ShortDesc: frees the action attribute
 *	Description:
 *		ddl_free_actions will free a list of ITEM_IDSs or DEFINITION
 *		depending upon what is stored.
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		ref_list:   points to the empty list
 *
 *	Returns: void
 *
 *	Author: Chris Gustafson
 *
 ************************************************************************/

void
ddl_free_actions(
ACTION_LIST	*pActionList,
uchar		dest_flag)
{
	if (pActionList == NULL) {		// Nothing is passed in
		return;
	}

	if (pActionList->list == NULL)	// The list is completely empty
	{
		ASSERT_DBG(!pActionList->count && !pActionList->limit);
		pActionList->count = 0;
		pActionList->limit = 0;
		return;
	}

	for (int i = 0; i < pActionList->count; i++)	// Free any memory in this list
	{
		ACTION *pAction = &(pActionList->list[i]);	// Grab this ACTION

		switch (pAction->type)
		{
		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_NONE:		// Nothing to do
			break;

		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_DEFINITION:
			if (pAction->action.meth_definition.data) {
				free((void *) pAction->action.meth_definition.data);
			}
			pAction->action.meth_definition.data = NULL;
			pAction->action.meth_definition.size = 0;
			break;

		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REFERENCE:
			pAction->action.meth_ref = 0;
			break;

		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REF_WITH_ARGS:
			ddl_free_op_ref_trail(&(pAction->action.meth_ref_args));
			break;

		default:
			ASSERT_DBG( pAction->type == nsEDDEngine::ACTION::ActionType::ACTION_TYPE_NONE );
			break;
		}
		
		pAction->type =nsEDDEngine::ACTION::ActionType:: ACTION_TYPE_NONE;
	}

	if (dest_flag == FREE_ATTR)
	{

		free((void *) pActionList->list);
		pActionList->list = NULL;
		pActionList->limit = 0;
	}

	pActionList->count = 0;
}


/***********************************************************************
 *
 *	Name: ddl_free_reflist
 *	ShortDesc: frees the list of ITEM_IDs
 *	Description:
 *		ddl_free_reflist will free a list of ITEM_IDSs including the
 *		array of ITEM_ID_ITEMs included in each ITEM_ID
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		ref_list:   points to the empty list
 *
 *	Returns: void
 *
 *	Author: Chris Gustafson
 *
 ************************************************************************/

void
ddl_free_reflist(
ITEM_ID_LIST   *ref_list,
uchar           dest_flag)
{

	if (ref_list == NULL) {
		return;
	}

	if (ref_list->list == NULL) {

		ASSERT_DBG(!ref_list->count && !ref_list->limit);
		ref_list->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list in ITEM_ID_LIST
		 */

		free((void *) ref_list->list);
		ref_list->list = NULL;
		ref_list->limit = 0;
	}

	ref_list->count = 0;

}


/*********************************************************************
 *
 *	Name: ddl_shrink_reflist
 *	ShortDesc: Shrink a ITEM_ID_LIST.
 *
 *	Description:
 *		ddl_shrink_reflist reallocates the ITEM_ID_LIST to only contain
 *		space for the ITEM_IDS currently being used
 *
 *	Inputs:
 *		ref_list:	a pointer to the ITEM_ID_LIST
 *
 *	Outputs:
 *		ref_list:   points to the new list
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

static int
ddl_shrink_reflist(
ITEM_ID_LIST   *ref_list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_REFLIST);
#endif

	if (ref_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (ref_list->list == NULL) {
		ASSERT_DBG(!ref_list->count && !ref_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (ref_list->count == ref_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (ref_list->count == 0) {
		ddl_free_reflist(ref_list, FREE_ATTR);
	}
	else {
		ref_list->limit = ref_list->count;

		ref_list->list = (ITEM_ID *) realloc((void *) ref_list->list,
			(size_t) (ref_list->limit * sizeof(ITEM_ID)));

		if (ref_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_reflist
 *	ShortDesc: Parse the binary and load a ITEM_ID_LIST.
 *
 *	Description:
 *		ddl_parse_reflist will parse the binary data chunk and load a ITEM_ID_LIST structure
 *		unless the pointer to the ITEM_ID_LIST passed in is NULL, in which case it will
 *		simply read through the binary data.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		ref_list - pointer to a ITEM_ID_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref_list - pointer to a ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from ddl_parse_ref(), ddl_shrink_reflist()
 *
 *	Author: Chris Gustafson
 *
 ********************************************************************************/

int
ddl_parse_reflist(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ID_LIST   *id_list,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	ITEM_ID        *temp_id;/* temporary pointer */
//	DDL_UINT        len = 0;		/** size of chunk currently being evaluated */
	DDL_UINT        tag = 0;		/** "label" associated with current chunk */



	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_REFLIST);
#endif

	/*
	 * Parse a ITEM_ID_LIST.  If we need more room in the array of
	 * structures, realloc more room.  Then call ddl_parse_item_id to load
	 * the next ITEM_ID structure.
	 */
	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

	if(tag!=REFERENCE_LIST_TAG)
	{
		return DDL_ENCODING_ERROR;
	}


	if (id_list != NULL) {

		/*
		 * parse the binary and load the ITEM_ID_LIST structure
		 */

		while (*size > 0) {

			if (id_list->count == id_list->limit) {

				/*
				 * reallocate the ITEM_ID_LIST structure for
				 * more ITEM_IDs
				 */

				id_list->limit += REF_LIST_INC;

				/*
				 * realloc ITEM_ID array
				 */

				temp_id = (ITEM_ID *) realloc((void *) id_list->list,
					(size_t) (id_list->limit * sizeof(ITEM_ID)));

				if (temp_id == NULL) {

					id_list->limit = id_list->count;
					ddl_free_reflist(id_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				id_list->list = temp_id;

				/*
				 * Initialize the new ITEM_IDs to zero
				 */
				memset((char *) &id_list->list[id_list->count], 0, (REF_LIST_INC * sizeof(ITEM_ID)));

			}

			rc = ddl_parse_item_id(chunkp, size, &id_list->list[id_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_reflist(id_list, FREE_ATTR);
				return rc;
			}

			id_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_reflist(id_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the ITEM_ID_LIST structure
			 */

			rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_parse_actions
 *	ShortDesc: Parse the binary and load a ACTION_LIST.
 *
 *	Description:
 *		Parse the binary and load a ACTION_LIST.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		pActionList - pointer to a ACTION_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref_list - pointer to a ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from ddl_parse_ref(), ddl_shrink_reflist()
 *
 *
 ********************************************************************************/
static int
ddl_parse_actions(
unsigned char **chunkp,
DDL_UINT       *size,
ACTION_LIST	   *pActionList,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int         rc = DDL_SUCCESS;	/* return code */
	DDL_UINT	tag = 0;
	DDL_UINT	len = 0;
	DDL_UINT	len_temp = 0;
	ACTION		*pAction = NULL;

	while (*size > 0)
	{
		ASSERT_DBG(chunkp && *chunkp && size);

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if(tag!=ACTION_LIST_ELEMENT_TAG)
		{
			ASSERT(0);
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		/*
		 * Parse the next item
		 */

		/*
		 * Parse a ACTION_LIST.  If we need more room in the array of
		 * structures, realloc more room.
		 */

		DDL_PARSE_TAG(chunkp, &len, &tag, &len_temp);

		// len = *size; //*size -= len;	// Adjust size to skip over the tagged section

		if (pActionList != NULL) {

			/*
			 * Parse a series of ACTIONs.  If we need more room, malloc more room.
			 * Then, parse the next ACTION structure.
			 */

			if (pActionList->count == pActionList->limit) {

				pActionList->limit += ACTION_LIST_INC;

				pAction = (ACTION *) realloc((void *) pActionList->list,
					(size_t) (pActionList->limit * sizeof(ACTION)));

				if (pAction == NULL) {
					pActionList->limit = pActionList->count;
					ddl_free_actions(pActionList, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &pAction[pActionList->count], 0,
					(size_t) (ACTION_LIST_INC * sizeof(ACTION)));

				pActionList->list = pAction;
			}

			pAction = &pActionList->list[pActionList->count++];

			/*
			 * Figure out which kind of action we have
			 */

			switch (tag)
			{
			case ACTION_SPECIFIER_REFERENCE:
				ASSERT_DBG(	pAction->type == nsEDDEngine::ACTION::ActionType::ACTION_TYPE_NONE );

				rc = ddl_parse_op_ref_trail(chunkp, &len, &(pAction->action.meth_ref_args), depinfo, env_info, var_needed);
				if (rc == DDL_SUCCESS)
				{
					if (pAction->action.meth_ref_args.expr.size == 0)	// No arguments were found
					{
						pAction->action.meth_ref = pAction->action.meth_ref_args.desc_id;	// Copy to correct union
						pAction->type = nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REFERENCE;
					}
					else
					{
						pAction->type = nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REF_WITH_ARGS;	// Arguments were found
					}

				}

				break;

			case ACTION_SPECIFIER_DEFINITION:
				ASSERT_DBG(	pAction->type == nsEDDEngine::ACTION::ActionType::ACTION_TYPE_NONE );

				rc = eval_attr_definition(*chunkp, len, &(pAction->action.meth_definition), depinfo, env_info, var_needed);

				*chunkp+=len;
				len=0;
				
				if (rc == DDL_SUCCESS)
				{
					pAction->type = nsEDDEngine::ACTION::ActionType::ACTION_TYPE_DEFINITION;
				}
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				break;
			}
		}
		else
		{
			switch (tag)
			{
			case ACTION_SPECIFIER_REFERENCE:
				rc = ddl_parse_op_ref_trail(chunkp, &len, NULL, depinfo, env_info, var_needed);

				break;

			case ACTION_SPECIFIER_DEFINITION:
				rc = eval_attr_definition(*chunkp, len, NULL, depinfo, env_info, var_needed);
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				break;
			}
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_parse_data_reflist
 *	ShortDesc: Parse the binary and load a DATA_ITEM_LIST.
 *
 *	Description:
 *		Parse the binary and load a DATA_ITEM_LIST.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		pActionList - pointer to a DATA_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		ref_list - pointer to a ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *		error returns from ddl_parse_ref(), ddl_shrink_reflist()
 *
 *
 ********************************************************************************/

static int
ddl_parse_data_reflist(
unsigned char **chunkp,
DDL_UINT       *size,
DATA_ITEM_LIST *items,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	DATA_ITEM      *tmp = NULL;	/* temp ptr to a list of data items */
	DDL_UINT        tag = 0;		/** "label" associated with current chunk */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_REFLIST);
#endif

	/*
	 * Parse a ITEM_ID_LIST.  If we need more room in the array of
	 * structures, realloc more room.  Then call ddl_parse_item_id to load
	 * the next ITEM_ID structure.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

	if(tag!=REFERENCE_LIST_TAG)
	{
		return DDL_ENCODING_ERROR;
	}



	if (items != NULL) {

		/*
		 * parse the binary and load the ITEM_ID_LIST structure
		 */

		while (*size > 0) {

			if (items->count >= items->limit) {
				items->limit += DATA_ITEM_LIST_INCSZ;
				tmp = (DATA_ITEM *) realloc((void *) items->list,
					(size_t) (items->limit * sizeof *tmp));
				if (!tmp) {
					items->limit = items->count;
					ddl_free_dataitems(items, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				items->list = tmp;
				memset((char *) &items->list[items->count], 0,
					DATA_ITEM_LIST_INCSZ * sizeof *items->list);
			}
			
			rc = ddl_parse_data_item(chunkp, size, &items->list[items->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				PS_TRACE(L"\n*** error (ddl_parse_data_reflist) : could not decode reflist item index %d",items->count);
				ddl_free_dataitems(items, FREE_ATTR);
				return rc;
			}

			items->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_dataitems_list(items);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			/*
			 * Don't load the ITEM_ID_LIST structure
			 */

			rc = ddl_parse_data_item(chunkp, size, (DATA_ITEM *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_reflist_choice
 *	ShortDesc: Choose the correct reference list from a binary.
 *
 *	Description:
 *		ddl_reflist_choice will parse the binary for an reference list,
 *		according to the current conditionals (if any).  The value of
 *		the reference list is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an REFERENCE_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_reflist().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static
int
ddl_reflist_choice(
unsigned char **chunkp,
DDL_UINT       *size,
ITEM_ID_LIST   *list,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

#ifdef DDSTEST
	TEST_FAIL(DDL_REFLIST_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	// check if it is conditional
	DDL_UINT tag=DDL_PEEK_TAG(**chunkp);

	if(tag==REFERENCE_LIST_TAG)
	{
		rc = DDL_SUCCESS;

		/*
			* If the calling routine is expecting a value, data_valid
			* cannot be NULL
			*/

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (*size > 0) {	/* Parse them */
			rc = ddl_parse_reflist(chunkp, size,
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}
		}

		*data_valid = TRUE;
	}
	else
	{
		rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
			REFERENCE_SEQLIST_TAG, env_info, var_needed);

		if (list && (chunk_list_ptr.size > 0)) {

			/*
			 * If the calling routine is expecting a value, data_valid
			 * cannot be NULL
			 */

			ASSERT_DBG((data_valid != NULL) || (list == NULL));

			chunk_ptr = chunk_list_ptr.list;
			while (chunk_list_ptr.size > 0) {	/* Parse them */
				rc = ddl_parse_reflist(&(chunk_ptr->chunk), &(chunk_ptr->size),
					list, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				chunk_ptr++;
				chunk_list_ptr.size--;
			}

			if (list && data_valid) {
				*data_valid = TRUE;	/* list has been modified */
			}
		}
	}

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */
	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_actions_choice
 *	ShortDesc: Choose the correct action list from a binary.
 *
 *	Description:
 *		will parse the binary for an action list,
 *		according to the current conditionals (if any).  The value of
 *		the actions is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an ACTION_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_reflist().
 *
 *
 *********************************************************************/

int
ddl_actions_choice(
unsigned char **chunkp,
DDL_UINT       *size,
ACTION_LIST	   *pActionList,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		ACTIONS_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (pActionList && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (pActionList == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_actions(&(chunk_ptr->chunk), &(chunk_ptr->size),
				pActionList, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (pActionList && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_data_reflist_choice
 *	ShortDesc: Choose the correct data item list from a binary.
 *
 *	Description:
 *		will parse the binary for an data item list,
 *		according to the current conditionals (if any).  The value of
 *		the data item list is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an DATA_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an ITEM_ID_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_reflist().
 *
 *
 *********************************************************************/
static
int
ddl_data_reflist_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DATA_ITEM_LIST *list,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

#ifdef DDSTEST
	TEST_FAIL(DDL_REFLIST_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	DDL_UINT tag=DDL_PEEK_TAG(**chunkp);

	if(tag==REFERENCE_LIST_TAG)
	{
		rc = DDL_SUCCESS;

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (*size > 0) {	/* Parse them */
			rc = ddl_parse_data_reflist(chunkp, size,
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}
		}

		*data_valid = TRUE;
	}
	else
	{
		rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
			REFERENCE_SEQLIST_TAG, env_info, var_needed);
	}

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
		* A list of chunks was found and a value is requested.
		*/

	if (list && (chunk_list_ptr.size > 0)) {

		/*
			* If the calling routine is expecting a value, data_valid
			* cannot be NULL
			*/

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_data_reflist(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;


err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_reflist
 *
 *	ShortDesc: Evaluate a reference
 *
 * Description:
 *	The eval_reflist function evaluates a reference.
 *	The buffer pointed to by chunk should contain the binary
 *	for a reference and size should specify its size. If list is
 *	not a null pointer, the item id is returned in list. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to a REFERENCE_LIST where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to a REFERENCE_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_reflist_choice(), ddl_shrink_depinfo()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/


int
eval_reflist(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	ITEM_ID_LIST   *list = static_cast<ITEM_ID_LIST*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_REFLIST);
#endif

	valid = 0;

	ddl_free_reflist(list, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_reflist_choice(&chunk, &size, list, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_reflist(list, FREE_ATTR);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_actions
 *
 *	ShortDesc: Evaluate actions
 *
 * Description:
 *	The eval_actions function evaluates actions.
 *	The buffer pointed to by chunk should contain the binary
 *	for actions and size should specify its size. If list is
 *	not a null pointer, the item id is returned in list. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		voidP - pointer to a ACTION_LIST where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to a REFERENCE_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_reflist_choice(), ddl_shrink_depinfo()
 *
 *
 *********************************************************************/
int
eval_actions(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	ACTION_LIST   *pActionList = static_cast<ACTION_LIST*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	valid = 0;

	ddl_free_actions(pActionList, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_actions_choice(&chunk, &size, pActionList, depinfo, &valid, env_info, var_needed);

	if (rc == DDL_SUCCESS)
	{
		rc = ddl_shrink_depinfo(depinfo);
	}

	if (rc == DDL_SUCCESS)
	{
		if (pActionList && !valid)
		{
			return DDL_DEFAULT_ATTR;
		}
	}
	else
	{
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_actions(pActionList, FREE_ATTR);
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_data_reflist
 *
 *	ShortDesc: Evaluate data item list
 *
 * Description:
 *	The eval_data_reflist function evaluates data item list.
 *	The buffer pointed to by chunk should contain the binary
 *	for a data item list and size should specify its size. If list is
 *	not a null pointer, the item id is returned in list. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		voidP - pointer to a DATA_ITEM_LIST where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to a REFERENCE_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_reflist_choice(), ddl_shrink_depinfo()
 *
 *
 *********************************************************************/
int
eval_data_reflist(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	DATA_ITEM_LIST   *list = static_cast<DATA_ITEM_LIST*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_REFLIST);
#endif

	valid = 0;

	ddl_free_dataitems(list, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_data_reflist_choice(&chunk, &size, list, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_dataitems(list, FREE_ATTR);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 *	Name: ddl_free_op_ref_trail
 *	ShortDesc: frees the resolve trail from the OP_REF_TRAIL structure.
 *
 *	Description:
 *		ddl_free_op_ref_trail will free a list of resolve trail items
 *		associated with an OP_REF_TRAIL.
 *
 *	Inputs:
 *		op_ref_trail:	a pointer to an OP_REF_TRAIL structure.
 *
 *	Outputs:
 *		op_ref_trail:	a pointer to an OP_REF_TRAIL structure.
 *
 *	Returns: void
 *
 *	Author: steve beyerl
 *
 ************************************************************************/

void
ddl_free_op_ref_trail(
OP_REF_TRAIL   *op_ref_trail)
{
	if (op_ref_trail == NULL) {
		return;
	}

	if (op_ref_trail->trail == NULL) {

		ASSERT_DBG(!op_ref_trail->trail_count && !op_ref_trail->trail_limit);
	}
	else {

		/*
		 * Free the trail list in OP_REF_TRAIL
		 */

		free((void *) op_ref_trail->trail);
		op_ref_trail->trail = NULL;
	}

	op_ref_trail->trail_limit = 0;
	op_ref_trail->trail_count = 0;

	ddl_free_expr(&op_ref_trail->expr);

	op_ref_trail->desc_id = 0;
	op_ref_trail->desc_type = 0;
		
	ddl_free_op_ref_info_list( &op_ref_trail->op_info_list );

	return;
}


void
ddl_free_op_ref_info_list(OP_REF_INFO_LIST *op_ref_info_list)
{
	if (op_ref_info_list == NULL) 
	{
		return;
	}

	if (op_ref_info_list->list == NULL) 
	{
		ASSERT_DBG(!op_ref_info_list->count && !op_ref_info_list->limit);
	}
	else 
	{
		/*
		 * Free the list of OP_REF_INFOs.
		 */

		free((void *) op_ref_info_list->list);
		op_ref_info_list->list = NULL;
	}
	
	op_ref_info_list->count = 0;
	op_ref_info_list->limit = 0;
}


/*********************************************************************
 *
 *  Name: ddl_op_ref_trail_choice
 *  ShortDesc: Choose the correct reference from a binary.
 *
 *  Description:
 *      ddl_op_ref_trail_choice will parse the binary for a reference,
 *      according to the current conditionals (if any).  The value of
 *      the reference is returned, along with dependency information.
 *      If a value is found, the valid flag is set to 1.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      op_ref_trail - pointer to an OP_REF_TRAIL structure where the result
 *              will be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      op_ref_trail - pointer to an OP_REF_TRAIL structure containing
 *              the result
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *              dependency information.
 *      data_valid - pointer to an int which, if set to 1, indicates
 *              that there is a valid value in ref
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Returns:
 *      DDL_SUCCESS
 *      error returns from ddl_cond() and ddl_parse_ref().
 *
 *  Author:
 *      Steve Beyerl
 *
 *********************************************************************/

int
ddl_op_ref_trail_choice(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_TRAIL   *op_ref_trail,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_OP_REF_TRAIL_CHOICE);
#endif


	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	DDL_UINT tag=DDL_PEEK_TAG(**chunkp);

	if(tag==REFERENCE_TAG)
	{
		ASSERT_DBG((data_valid != NULL) || (op_ref_trail == NULL));

		/* We have finally gotten to the actual value!  Parse it. */

		rc = ddl_parse_op_ref_trail(chunkp, size, op_ref_trail,
			depinfo, env_info, var_needed);

	}
	else
	{
		rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		/*
		 * A chunk was found and a value is requested.
		 */

		if (op_ref_trail && (val.chunk)) {

			/*
			 * If the calling routine is expecting a value, data_valid
			 * cannot be NULL
			 */

			ASSERT_DBG((data_valid != NULL) || (op_ref_trail == NULL));

			/* We have finally gotten to the actual value!  Parse it. */

			rc = ddl_parse_op_ref_trail(&val.chunk, &val.size, op_ref_trail,
				depinfo, env_info, var_needed);
		}
	}

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (op_ref_trail && data_valid) {
		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}

/***********************************************************************
 *
 *	Name: ddl_free_op_ref_trail_list
 *	ShortDesc: frees the list of OP_REF_TRAILs
 *	Description:
 *		ddl_free_op_ref_trail_list will free the list of OP_REF_TRAIL
 *		structures associated with an OP_REF_TRAIL_LIST.
 *
 *	Inputs:
 *		op_ref_trail_list:	a pointer to an OP_REF_TRAIL_LIST
 *		destruct_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		op_ref_trail_list:	a pointer to an OP_REF_TRAIL_LIST
 *
 *	Returns: void
 *
 *	Author: steve beyerl
 *
 ************************************************************************/

void
ddl_free_op_ref_trail_list(
OP_REF_TRAIL_LIST *op_ref_trail_list,
unsigned char   destruct_flag)
{

	int             i;	/* incrementer */

	if (op_ref_trail_list == NULL) {
		return;
	}

	if (op_ref_trail_list->list == NULL) {

		ASSERT_DBG(!op_ref_trail_list->count && !op_ref_trail_list->limit);
		op_ref_trail_list->count = 0;
		op_ref_trail_list->limit = 0;
		return;
	}

	/*
	 * Delete the list of RESOLVE INFO associated with each OP_REF_TRAIL.
	 * This should happen irregardless of the value of "destruct_flag".
	 */

//	for (i = 0; i < op_ref_trail_list->list->trail_count; i++) {
	for (i = 0; i < op_ref_trail_list->count; i++) {
		ddl_free_op_ref_trail(&op_ref_trail_list->list[i]);
	}

	if (destruct_flag == FREE_ATTR) {

		/*
		 * Free the list of OP_REF_TRAILs.
		 */

		free((void *) op_ref_trail_list->list);
		op_ref_trail_list->list = NULL;
		op_ref_trail_list->limit = 0;
	}
	else {			/* destruct_flag == CLEAN_ATTR */

		/*
		 * zero out the list. Leave the list and limit intact.
		 */

		memset((char *) op_ref_trail_list->list, 0,
			(size_t) op_ref_trail_list->limit * sizeof(OP_REF_TRAIL));
	}

	op_ref_trail_list->count = 0;

	return;
}



/*********************************************************************
 *
 *	Name: ddl_shrink_op_ref_trail_list
 *	ShortDesc: Shrink a OP_REF_TRAIL_LIST.
 *
 *	Description:
 *		ddl_shrink_op_ref_trail_list reallocates the OP_REF_TRAIL_LIST
 *		to only contain	space for the OP_REF_TRAILs currently being used
 *
 *	Inputs:
 *		op_ref_trail_list:	a pointer to the ITEM_ID_LIST
 *
 *	Outputs:
 *		op_ref_trail_list:   points to the new list
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: steve beyerl
 *
 ***********************************************************************/

int
ddl_shrink_op_ref_trail_list(
OP_REF_TRAIL_LIST *op_ref_trail_list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_OP_REF_TRAIL_LIST);
#endif

	if (op_ref_trail_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (op_ref_trail_list->list == NULL) {
		ASSERT_DBG(!op_ref_trail_list->count && !op_ref_trail_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (op_ref_trail_list->count == op_ref_trail_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (op_ref_trail_list->count == 0) {
		ddl_free_op_ref_trail_list(op_ref_trail_list, FREE_ATTR);
	}
	else {
		op_ref_trail_list->limit = op_ref_trail_list->count;

		op_ref_trail_list->list = (OP_REF_TRAIL *) realloc(
			(void *) op_ref_trail_list->list,
			(size_t) (op_ref_trail_list->limit * sizeof(OP_REF_TRAIL)));

		if (op_ref_trail_list->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_op_ref_trail_list_choice
 *	ShortDesc: Choose the correct reference list from a binary.
 *
 *	Description:
 *		ddl_op_ref_trail_trail_choice will parse the binary for an reference list,
 *		according to the current conditionals (if any).  The value of
 *		the op_ref_trail_list is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an OP_REF_TRAIL_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an OP_REF_TRAIL_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in ref.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_op_ref_trail_list().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_op_ref_trail_list_choice(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_TRAIL_LIST *list,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp pointer for chunk list */

#ifdef DDSTEST
	TEST_FAIL(DDL_OP_REF_TRAIL_LIST_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		REFERENCE_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_op_ref_trail_list(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


/*********************************************************************
 *
 *  Name: eval_op_ref_trail_list
 *
 *  ShortDesc: Evaluate an operational reference trail list
 *
 * Description:
 *  The eval_op_ref_trail_list function evaluates an operational
 *  reference trail list.  The buffer pointed to by chunk should
 *  contain the binary for a reference list and size should specify
 *  its size. If list is not a null pointer, the item id is returned
 *  in list. If depinfo is not a null pointer, dependency information
 *  about the reference is returned in depinfo.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      list - pointer to an OP_REF_TRAIL where the result will
 *              be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      list - pointer to an OP_REF_TRAIL structure containing the result
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *              dependency information.
 *      data_valid - pointer to an int which, if set to 1, indicates
 *              that there is a valid value in expr
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 * Returns:
 *  DDL_SUCCESS, DDL_DEFAULT_ATTR
 *  return codes from:
 *		ddl_reflist_choice()
 *		ddl_shrink_depinfo()
 *
 *  Author: steve beyerl
 *
 *********************************************************************/

int
eval_op_ref_trail_list(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	OP_REF_TRAIL_LIST* list = static_cast<OP_REF_TRAIL_LIST*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_OP_REF_TRAIL_LIST);
#endif

	valid = 0;

	if (list) {
		ddl_free_op_ref_trail_list(list, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_op_ref_trail_list_choice(&chunk, &size, list, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_op_ref_trail_list(list, FREE_ATTR);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_op_ref_trail
 *
 *	ShortDesc: Evaluate op ref trail
 *
 * Description:
 *	The eval_op_ref_trail function evaluates op ref trail.
 *	The buffer pointed to by chunk should contain the binary
 *	for a op ref trail and size should specify its size. If list is
 *	not a null pointer, the item id is returned in list. If
 *	depinfo is not a null pointer, dependency information
 *	about the reference is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		voidP - pointer to a OP_REF_TRAIL where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to a REFERENCE_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_reflist_choice(), ddl_shrink_depinfo()
 *
 *
 *********************************************************************/
int
eval_op_ref_trail(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	OP_REF_TRAIL* list = static_cast<OP_REF_TRAIL*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_OP_REF_TRAIL_LIST);
#endif

	valid = 0;

	if (list) {
		ddl_free_op_ref_trail(list);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_op_ref_trail_choice(&chunk, &size, list, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_op_ref_trail(list);
		return rc;
	}

	if (list && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


////////////////////
//
// get_constant_itype - gets the appropriate ITEM_TYPE when the reference is a constant
//
////////////////////
ITEM_TYPE
get_constant_itype(EXPR* expr)
{
	ITEM_TYPE type = 0;

	switch(expr->type)
	{
	case ASCII:
		type = (ITEM_TYPE)STRING_LITERAL_ITYPE;
		break;
	case INTEGER:
		type = (ITEM_TYPE)CONST_INTEGER_ITYPE;
		break;
    case FLOATG_PT:
		type = (ITEM_TYPE)CONST_FLOAT_ITYPE;
		break;
	case UNSIGNED:
		type = (ITEM_TYPE)CONST_UNSIGNED_ITYPE;
		break;
    case DOUBLEG_PT:
		type = (ITEM_TYPE)CONST_DOUBLE_ITYPE;
		break;

	default:
		ASSERT_DBG(false);
		break;
	}

	return type;
}

