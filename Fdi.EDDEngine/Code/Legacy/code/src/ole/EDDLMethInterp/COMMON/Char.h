/*************************************************************************************************
 *
 * Workfile: Char.h 
 * 11Dec07 - stevev
 *     Revision, Date and Author are not in the file!
 *
 *************************************************************************************************
 * The content of this file is the
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2007, HART Communication Foundation, All Rights Reserved
 *************************************************************************************************
 *
 * Description:
 *		HANDLES THE DIFFERENCES BETWEEN WIDE AND NARROW CHARACTERS
 *
 * Component History:
 * 
 *
 * #include "Char.h"
 */

#pragma once
#ifndef _CHAR_H
#define _CHAR_H

#ifdef INC_DEBUG
#pragma message("In char.h")
#endif
#pragma warning (disable : 4786)

#include <string>
#include <iostream>
#include <iomanip>
//#include "PlatformCommon.h"
using namespace std;
#pragma warning (disable : 4786)

#ifdef INC_DEBUG
#pragma message("    Finished Includes::char.h")
#endif



#if defined(_UNICODE) || defined (UNICODE)
#define tstring wstring

#ifndef tcerr
#define tcerr	wcerr
#define tclog	wclog
#define tcout	wcout
#endif

#ifndef  _T
#define _T(s) L##s
#define _ultot( a, b, c ) _ultow ( a, b, c )
#endif /*not IS_UI */

typedef wchar_t tchar;
#ifndef TCHAR
//#define TCHAR   wchar_t
#endif
//#define _ttof( a ) _wtof( a )
#define _tsscanf( a, b, c ) swscanf( a, b, c )
//#define _tsprintf( a, b, c ) swprintf( a, b, c )
#define _tsprintf            swprintf
#define lmemcpy( a, b, c )   wmemcpy( a, b, c )
#define ttoi( a ) PS_Wtoi ( a )
//double _wtof( const wchar_t* str );
#define _itoT( a , b , c )    PS_Itow(  a , b , c  )
#define tctime( a )          _wctime ( a )
#define _tatoi				 PS_Wtoi
#define _tatol				 PS_Wtol

#define tisalpha			  iswalpha
#define tisdigit			  iswdigit
#define _tstrcpy( a, b )      wcscpy( a , b )
#define _tstrncpy( a, b, c )  wcsncpy( a, b, c )
#define _tstrlen			  wcslen
#define _tscanf				  wscanf
#define _tstrchar			  wcschr
#define _tstrcmp			  wcscmp
#define _tstrncmp			  wcsncmp
#define _tstrcat              wcscat
#define  tcstoul              wcstoul
// os #define _tfullpath( a, b, c )  _wfullpath( a, b, c )
#else

#ifndef tcerr
#define tcerr	cerr
#define tclog	clog
#define tcout	cout
#endif


#define tstring string
#ifndef _T
#define _T(s) s
#define _ultot( a, b, c ) _ultoa ( a, b, c )
#endif

typedef  char   tchar;
#ifndef TCHAR
//#define TCHAR   char
#endif
#define _ttof( a ) atof( a )
#define _itoT(  a , b , c  )  _itoa(  a , b , c  )
#define _tatoi				  atoi
#define _tatol				  atol
#define _tsprintf             sprintf

#define tisalpha			  isalpha
#define tisdigit			  isdigit
#define _tstrcpy( a, b )      strcpy( a , b )
#define _tstrncpy( a, b, c )  strncpy( a, b, c )
#define _tstrlen              strlen
#define _tscanf				  scanf
#define _tstrchar			  strchr
#define _tstrcmp			  strcmp
#define _tstrncmp			  strncmp
#define _tstrcat              strcat
#define tcstoul               strtoul
//os  #define _tfullpath( a, b, c )  _fullpath( a, b, c )
#endif //USINGTCHAR
// stevev - WIDE2NARROW char interface
#define mt_String	_T("")
string TStr2AStr(const  wstring& str );
wstring AStr2TStr(const string& str );
#define _tDestroyTheStr() theStr.~wstring()

tchar char2wchar(char *);
//extern double _wtof(const wchar_t* a );

#endif /* _CHAR_H  */
