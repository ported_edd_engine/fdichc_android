/*************************************************************************************************
 *
 * $Workfile: ddbResponseCode.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the response code base class
 * Response Codes appear in items,transactions & commands
 *
 *	4/22/02	sjv	created ddbrespcd.h
 *	9/6/02  sjv generated ddb... from responsecdbase.h
 *
 *
 * #include "ddbResponseCode.h"
 */

#ifndef _RESPONSECODE_H
#define _RESPONSECODE_H
#ifdef INC_DEBUG
#pragma message("In ddbResponseCode.h") 
#endif


#include "ddbVar.h"

#ifdef INC_DEBUG
#pragma message("    Finished includes from ddbResponseCode.h") 
#endif


/*****************************
 * hCrespCode holds the actual data
 * from the structure RESPONSE_CODE
 ****************************/
class hCrespCode  : public hCobject
{
	unsigned long    val;
	unsigned long    type;
	hCddlString      descS;
	hCddlString      helpS;

public:
	// no default constructor (we MUST have the handle)
	hCrespCode(DevInfcHandle_t h)
		:hCobject(h),descS(h),helpS(h) {clear();};
	// copy constructor
	hCrespCode(const hCrespCode& src )
		:hCobject(src.devHndl()),descS(src.devHndl()),helpS(src.devHndl()){ *this = src;};
	RETURNCODE destroy(void){  return (descS.destroy() | helpS.destroy() );};

	hCrespCode& operator=(const hCrespCode& src )
	{val = src.val; type = src.type; descS = src.descS; helpS = src.helpS; return (*this);};
	
	hCrespCode& operator=(const hCenumDesc& src )
	{val = src.val; type = 0; descS = src.descS; helpS = src.helpS; return (*this);};

	unsigned long getVal(void) {return val;};
	wstring        getDescStr(void) {return descS.procureVal();};
	unsigned long getType(void) { return type; };
	static
	unsigned long getType(unsigned char c ) /* stevev 7mar05 - make translation available to all */
		{	if ( (c>=0 && c <= 7) || (c >=16 && c <=23) || ( c>=32 && c <=64 )
			  || (c>=9 && c <=13) || (c >=65 && c <=95) || ( c== 15) 
			  || (c ==28 ) || ( c== 29) )
			{/* it is an error */	return MISC_ERROR_RSPCODE;			}
			else
			{/* it is a warning */	return MISC_WARNING_RSPCODE;		}
		};
#define	RESPCD_ISERROR(v)  ((v==MISC_ERROR_RSPCODE)||(v==DATA_ENTRY_ERROR_RSPCODE)||(v==MODE_ERROR_RSPCODE)||(v==PROCESS_ERROR_RSPCODE))

	void setEqual(aCrespCode* pArc)
	{	val = pArc->val; type = pArc->type; descS = pArc->descS; helpS = pArc->helpS; };
	void setDuplicate(hCrespCode* pSrc)
	{	val = pSrc->val; type = pSrc->type; 
		descS.duplicate(&(pSrc->descS), false); helpS.duplicate(&(pSrc->helpS), false); };
		// duplicate strings with any replicate, they don't use it

	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	void       clear(void){ val = type = 0; descS.clear(); helpS.clear();};
};

typedef vector<hCrespCode>   responseCodeList_t;

/*****************************
 * hCRespCodeList holds a single list of response code data classes
 * 
 ****************************/
class hCRespCodeList  : public hCpayload, public responseCodeList_t, public hCobject
{
public:

	hCRespCodeList(DevInfcHandle_t h) : hCobject(h) {};
// temp regress to previous version::	hCRespCodeList(const hCRespCodeList& src):hCobject(src.devHndl()){ *this = src; };
	
	void duplicate(hCpayload* pPayLd, bool replicate);

	virtual ~hCRespCodeList(){clear();};
	RETURNCODE destroy(void){  RETURNCODE rc = 0;
		FOR_this_iT(responseCodeList_t,this){ rc |= iT->destroy();} return rc;};

	// see below  RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	RETURNCODE getDescription(int oSet = 0) ;
								//{ cerr << "____stubout___CrespCodeList" << endl;return FAILURE;};

	void append(hCRespCodeList* addlist);// modified to append with added list overwriting existing
										 // was::>{	insert(end(),addlist->begin(),addlist->end()); };

	RETURNCODE clear(void){ responseCodeList_t::clear(); return SUCCESS; };
	void  setEqual(void* pAclass); // a hCpayload required method	                   
	// each payload must tell if the abstract payload type is correct for itself
	bool validPayload(payloadType_t ldType)
		{	if (ldType != pltRespCdList) return false;  else  return true;  };
	RETURNCODE dumpSelf(int indent = 0){ 
		LOGIT(COUT_LOG,"%sRespCodeList does not dump at this time.\n"," "/*MI_LOG::Space(indent)*/);return 0;};
};


class hCRespCodeBase
{
public:
	hCRespCodeBase(DevInfcHandle_t h){}
	hCRespCodeBase(const hCRespCodeBase& src){}	
	RETURNCODE destroy(void){  return SUCCESS; }
	RETURNCODE dumpSelf (int indent = 0, char* typeName = NULL);
	void setEqualTo(void* pSrcCnd){}
};


#endif//_RESPONSECODE_H

/*************************************************************************************************
 *
 *   $History: ddbResponseCode.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/22/03    Time: 3:16p
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * stopping point
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
