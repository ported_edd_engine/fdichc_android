////////////////////////////////////////////////////////////////////////
//        Header file that describes the path and property constants used.
//
//   ALL CONSTANTS HAVING TO DO WITH PATHS OR PROPERTIES MUST BE IN THIS
//   INCLUDE FILE SO THAT THE APPLICATIONS CAN USE THEM!!!!!
//
//        @(#) $Id: pathtype.h,v 1.1 2012/07/10 20:59:29 rgretta Exp $
////////////////////////////////////////////////////////////////////////

#pragma once

#include <pathtype_server.h>	// Start with the contents of FF\AMSServer\pathtype.h
									// Then add our additional defines.
									// This ensures that there are no duplicates


////////////////////////////////////////////////////////////////////////
////    PB Path object types.	(Extends FF list)
////////////////////////////////////////////////////////////////////////

#define PT_ACTION_METHOD_COL		(PT_FF_LAST+1) // Methods collection
#define PT_EMBEDDED_METHOD			(PT_FF_LAST+2) // Physical Device collection
#define PT_COMMAND					(PT_FF_LAST+3) // Command