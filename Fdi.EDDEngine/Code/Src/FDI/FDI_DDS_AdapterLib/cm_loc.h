
/**
 *	@(#) $Id: cm_loc.h,v 1.2 2012/10/03 23:51:57 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_loc.h - Connection Manager Local Definitions
 *
 *	This file contains all internal definitions for
 *	the Connection Manager Library.
 */

#ifndef CM_LOC_H
#define CM_LOC_H

#include "cm_rod.h"
#include "pathtype.h"
#include "sym.h"
#include "nsEDDEngine/FFBlockInfo.h"
#include "Ntcassert2.h"



class ACTIVE_BLK_TBL_ELEM {
public:
	char           				*tag;
	OBJECT_INDEX				op_index;
	unsigned int   				param_count;
	UINT32						dd_blk_id;
	int							dd_blk_tbl_offset;
	DEVICE_HANDLE  				active_dev_tbl_offset;
	nsEDDEngine::CCharacteristicRecord	blk_char_record;
	nsEDDEngine::BLOCK_TYPE		block_type;
	UINT32						blockAlertUserCount;
	UINT32						blockNonAlertUserCount;
};

class ACTIVE_BLK_TBL {
public:
	int						count;
	int						limit;
	ACTIVE_BLK_TBL_ELEM**	list;
};

	
class ACTIVE_DEV_TBL_ELEM {
public:
	DEVICE_TYPE_HANDLE		active_dev_type_tbl_offset;
	int						usage;
	CStdString					m_strDeviceId;	// Filled in only if built through !PhysicalDevice
	bool					m_bAmBeingCommissioned;	// Is this device currently being commissioned?

private:
	VIEWTYPE				m_eView;
	DATE					m_dtHistoricalTime;
	friend void SET_ADT_VIEW_TIME(DEVICE_HANDLE dh, VIEWTYPE eView, DATE dt);

	CStdString					m_strDeviceTag;	// Filled in only if built through !LogicalDevice

							// Filled in only if built through !LogicalDevice!<HSI Section Name>
	CStdString					m_strSectionName;
};

class ACTIVE_DEV_TBL {
public:
	int						count;
	int						limit;
	ACTIVE_DEV_TBL_ELEM**	list;
};

/* Active Device Type Table */

class ACTIVE_DEV_TYPE_TBL_ELEM {
public:
	DD_DEVICE_ID					m_dd_device_id;
	CStdString							m_strManufacturerName;
	CStdString							m_strDeviceTypeName;
	ULONG							m_ulAlarmBlockIndex;
	ROD_HANDLE						rod_handle;
	nsEDDEngine::FLAT_DEVICE_DIR					*dd_dev_tbls;
	SYMINFO*	   					pSymInfo;
	CM_COMPAT						compatibility;
	int								usage;
} ;

class ACTIVE_DEV_TYPE_TBL {
public:
	int							count;
	int							limit;
	ACTIVE_DEV_TYPE_TBL_ELEM**	list;
} ;




nsEDDEngine::FLAT_DEVICE_DIR *get_flat_device_directory(BLOCK_HANDLE bh);

#endif	/* CM_LOC_H */

