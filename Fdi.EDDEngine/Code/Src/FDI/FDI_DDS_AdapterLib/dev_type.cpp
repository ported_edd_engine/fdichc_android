////////////////////////////////////////////////////////////////////////
////	Device Type Load/Unload Functions.
////
////	@(#) $Id: dev_type.cpp,v 1.1 2012/07/10 20:59:29 rgretta Exp $
////////////////////////////////////////////////////////////////////////

#include "STDINC.H"

#include "cm_loc.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"


#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////
////
////    Load Symbol Information for Device Type 
////
////////////////////////////////////////////////////////////////////////

int CConnectionMgr::load_sym_info (DEVICE_TYPE_HANDLE dth) 
{
	SYMINFO			*syminfo  = NULL;

 	int rc = g_DeviceTypeMgr.get_adtt_symbol_info(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;
	
	if (syminfo != NULL)
	{
		return SUCCESS ;
	}

	syminfo = new SYMINFO ;

	if (syminfo == NULL) {
		return (CM_NO_MEMORY) ;
	}

	// fill in SYMINFO data
	rc = Fill_Syminfo(dth, syminfo);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	/*
	 * Save the symbol information
	 */

	rc = g_DeviceTypeMgr.set_adtt_symbol_info(dth, syminfo) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}


	return(SUCCESS) ;
}	

////////////////////////////////////////////////////////////////////////
////
////    Remove Symbol Information for Device Type 
////
////////////////////////////////////////////////////////////////////////

int CConnectionMgr::remove_sym_info (DEVICE_TYPE_HANDLE dth) 
{
	SYMINFO	*syminfo ;
	int		rc ;

	rc = g_DeviceTypeMgr.get_adtt_symbol_info(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	if (syminfo) {
		Free_Syminfo(syminfo);
		delete(syminfo);
	}

	rc = g_DeviceTypeMgr.set_adtt_symbol_info(dth, (SYMINFO *)NULL) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	return(SUCCESS) ;
}	
	
