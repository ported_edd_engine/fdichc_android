#include "stdinc.h"


#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

CConnectionMgr::CConnectionMgr()
{
	active_blk_tbl.count = 0;
	active_blk_tbl.list = nullptr;	

	active_dev_tbl.count = 0;
	active_dev_tbl.list = nullptr;

}

CConnectionMgr::~CConnectionMgr()
{
	active_blk_tbl.count = 0;
	active_blk_tbl.list = nullptr;

	active_dev_tbl.count = 0;
	active_dev_tbl.list = nullptr;

}

void CConnectionMgr::SET_ABT_DD_BLK_ID(BLOCK_HANDLE bh, UINT32 dbi)
{
	active_blk_tbl.list[bh]->dd_blk_id = dbi;
}

//int CConnectionMgr::valid_block_handle (BLOCK_HANDLE bh)
//{
//	return	((bh >= 0) && (bh < active_blk_tbl.count) 
//			&& (active_blk_tbl.list[bh]));
//}

DEVICE_HANDLE CConnectionMgr::ABT_ADT_OFFSET(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->active_dev_tbl_offset;
}

int CConnectionMgr::ABT_PARAM_COUNT(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->param_count;
}

int CConnectionMgr::get_active_blk_tbl_count()
{
	return active_blk_tbl.count;
}

UINT32 CConnectionMgr::ABT_DD_BLK_ID(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->dd_blk_id;
}

void CConnectionMgr::SET_ABT_CHAR_RECORD( BLOCK_HANDLE bh, nsEDDEngine::CCharacteristicRecord charrecord )
{
	active_blk_tbl.list[bh]->blk_char_record = charrecord;
}

void CConnectionMgr::SET_ABT_BLOCK_TYPE( BLOCK_HANDLE bh, nsEDDEngine::BLOCK_TYPE block )
{
	active_blk_tbl.list[bh]->block_type = block;
}

OBJECT_INDEX CConnectionMgr::ABT_A_OP_INDEX(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->op_index;
}

void CConnectionMgr::SET_ABT_A_OP_INDEX(BLOCK_HANDLE bh, OBJECT_INDEX oi)
{
	active_blk_tbl.list[bh]->op_index = oi;
}

UINT32 CConnectionMgr::ABT_CR_ITEM_ID(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->blk_char_record.m_ulDDItemId;
}

char* CConnectionMgr::ABT_TAG(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->tag;
}

void CConnectionMgr::SET_ABT_TAG(BLOCK_HANDLE bh, char* t)
{
	active_blk_tbl.list[bh]->tag = t;
}

char* CConnectionMgr::ABT_CR_TAG( BLOCK_HANDLE bh )
{
	return active_blk_tbl.list[bh]->blk_char_record.m_pTag;
}

void CConnectionMgr::SET_ABT_PARAM_COUNT(BLOCK_HANDLE bh, unsigned int pc)
{
	active_blk_tbl.list[bh]->param_count = pc;
}

void CConnectionMgr::SET_ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh, int dbto)
{
	active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto;
}

int CConnectionMgr::ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->dd_blk_tbl_offset;
}

void CConnectionMgr::SET_ABT_ADT_OFFSET(BLOCK_HANDLE bh, DEVICE_HANDLE adto)
{
	active_blk_tbl.list[bh]->active_dev_tbl_offset = adto;
}

DEVICE_TYPE_HANDLE CConnectionMgr::ABT_ADTT_OFFSET(BLOCK_HANDLE bh)
{
	return ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh));
}

DEVICE_TYPE_HANDLE CConnectionMgr::ADT_ADTT_OFFSET(DEVICE_HANDLE dh)
{
	ASSERT_RET(valid_device_handle(dh), -1);

	return active_dev_tbl.list[dh]->active_dev_type_tbl_offset;
}

void CConnectionMgr::SET_ABT_ADTT_OFFSET(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto)
{
	SET_ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh), adtto);
}

void CConnectionMgr::SET_ADT_ADTT_OFFSET(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto)
{
	ASSERT(valid_device_handle(dh));

	active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto;
}

ROD_HANDLE CConnectionMgr::ABT_DD_HANDLE(BLOCK_HANDLE bh)
{
	return ADT_DD_HANDLE(ABT_ADT_OFFSET(bh));
}

ROD_HANDLE CConnectionMgr::ADT_DD_HANDLE(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh));
}

ROD_HANDLE CDeviceTypeMgr::ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->rod_handle;
}

DD_DEVICE_ID* CConnectionMgr::ADT_DD_DEVICE_ID(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh));
}

DD_DEVICE_ID* CDeviceTypeMgr::ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth)
{
	return &active_dev_type_tbl.list[dth]->dd_device_id;
}

void CConnectionMgr::SET_ADT_DD_DEVICE_ID(DEVICE_HANDLE dh, DD_DEVICE_ID* ddi)
{
	g_DeviceTypeMgr.SET_ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh), ddi);
}

void CDeviceTypeMgr::SET_ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID* ddi)
{
	memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_device_id),
			(char *)ddi,
			sizeof(DD_DEVICE_ID));
}

void CDeviceTypeMgr::SET_ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth, ROD_HANDLE rod_handle)
{
	active_dev_type_tbl.list[dth]->rod_handle = rod_handle;
}

void* CDeviceTypeMgr::ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->dd_dev_tbls;
}

wchar_t* CConnectionMgr::ADT_DEVICE_NAME(DEVICE_HANDLE dh)
{
	return active_dev_tbl.list[dh]->DeviceName;
}

void CConnectionMgr::SET_ADT_DEVICE_NAME(DEVICE_HANDLE dh, wchar_t* DeviceName)
{
	wcscpy(active_dev_tbl.list[dh]->DeviceName, DeviceName);
}

void CConnectionMgr::SET_ADT_DD_HANDLE(DEVICE_HANDLE dh, ROD_HANDLE ddh)
{
	g_DeviceTypeMgr.SET_ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh), ddh);
}

void* CConnectionMgr::ADT_DD_DEV_TBLS(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh));
}

NETWORK_HANDLE CConnectionMgr::ADT_NETWORK(DEVICE_HANDLE dh)
{
	return active_dev_tbl.list[dh]->network;
}

ROD_HANDLE CConnectionMgr::ADT_OPROD_HANDLE(DEVICE_HANDLE dh)
{
	return active_dev_tbl.list[dh]->oprod_handle;
}

int CConnectionMgr::ADT_USAGE(DEVICE_HANDLE dh)
{
	return active_dev_tbl.list[dh]->usage;
}

SYMINFO* CDeviceTypeMgr::ADTT_SYM_INFO(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->syminfo;
}

int CDeviceTypeMgr::ADTT_USAGE(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->usage;
}