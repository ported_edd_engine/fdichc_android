#include "stdafx.h"
#include "LegacyHart.h"



//Fill_FLAT_DEVICE_DIR() - Fills in an FDI FLAT_DEVICE_DIR from DDS FLAT_DEVICE_DIR
void CLegacyHart::Fill_FLAT_DEVICE_DIR(nsEDDEngine::FLAT_DEVICE_DIR *pFdiFlatDeviceDir, FLAT_DEVICE_DIR *pDdiFlatDeviceDir)
{
	pFdiFlatDeviceDir->attr_avail = pDdiFlatDeviceDir->attr_avail;
	Fill_BLOCK_TABLE(&pFdiFlatDeviceDir->blk_tbl,&pDdiFlatDeviceDir->blk_tbl);
	
#if 0	// The DDS DICT_REF_TBL does not convert to the nsEDDEngine::DICT_REF_TBL well 
	pFdiFlatDeviceDir->dict_ref_tbl.count = pDdiFlatDeviceDir->dict_ref_tbl.count;
	pFdiFlatDeviceDir->dict_ref_tbl.list = pDdiFlatDeviceDir->dict_ref_tbl.list;
#endif


	// IMAGE_TBL 
	// FDI IMAGE_TBL is not supporting BINARY_LANGUAGE_IMAGE. Need to think about it.
#if 0
	if(pDdiFlatDeviceDir->image_tbl.list != nullptr)
	{
		pFdiFlatDeviceDir->image_tbl.count = pDdiFlatDeviceDir->image_tbl.count;
		pFdiFlatDeviceDir->image_tbl.list = (nsEDDEngine::IMAGE_LIST*)malloc(sizeof(nsEDDEngine::IMAGE_LIST)*pDdiFlatDeviceDir->image_tbl.count);

		for(int i = 0; i < pFdiFlatDeviceDir->image_tbl.count; i++)
		{
			pFdiFlatDeviceDir->image_tbl.list[i].count = pDdiFlatDeviceDir->image_tbl.list[i].count;
			//pFdiFlatDeviceDir->image_tbl.list[i]. data = pDdiFlatDeviceDir->image_tbl.list[i].list[i].data;
			//pFdiFlatDeviceDir->image_tbl.list[i].list[i].data`= pDdiFlatDeviceDir->image_tbl.list[0].
		}
	}
	
#endif
	
	// ITEM_TBL
	if(pDdiFlatDeviceDir->item_tbl.count != 0)
	{
		pFdiFlatDeviceDir->item_tbl.count = pDdiFlatDeviceDir->item_tbl.count;
		pFdiFlatDeviceDir->item_tbl.list = (nsEDDEngine::ITEM_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::ITEM_TBL_ELEM)*pDdiFlatDeviceDir->item_tbl.count);

		for(int i = 0; i < pDdiFlatDeviceDir->item_tbl.count; i++)
		{
			pFdiFlatDeviceDir->item_tbl.list[i].dd_ref.object_index	= pDdiFlatDeviceDir->item_tbl.list[i].dd_ref.object_index;
			pFdiFlatDeviceDir->item_tbl.list[i].item_id				= pDdiFlatDeviceDir->item_tbl.list[i].item_id;
			
			pFdiFlatDeviceDir->item_tbl.list[i].item_type			= Set_ITEM_TYPE(pDdiFlatDeviceDir->item_tbl.list[i].item_type);
		}

	}


	// STRING_TBL

	if(pDdiFlatDeviceDir->string_tbl.count != 0)
	{
		pFdiFlatDeviceDir->string_tbl.count = pDdiFlatDeviceDir->string_tbl.count;

		pFdiFlatDeviceDir->string_tbl.root = nullptr;

		pFdiFlatDeviceDir->string_tbl.list = (nsEDDEngine::STRING*)calloc(1, sizeof(nsEDDEngine::STRING)*pDdiFlatDeviceDir->string_tbl.count);

		for(int i = 0; i < pDdiFlatDeviceDir->string_tbl.count; i++)
		{
			Fill_STRING(&pFdiFlatDeviceDir->string_tbl.list[i], &pDdiFlatDeviceDir->string_tbl.list[i]);
		}
	}
	
	// SYMBOL_TBL is not available in DDS FLAT_DEVICE_DIR
	//pFdiFlatDeviceDir->symbol_tbl.count = pDdiFlatDeviceDir->;
	
}


// Clean function for FLAT_DEVICE_DIR
void CLegacyHart::Clean_FLAT_DEVICE_DIR(nsEDDEngine::FLAT_DEVICE_DIR *pFlatDeviceDir)
{
	int	count = 0;	/* number of elements in BLOCK_NAME_TABLE */
	nsEDDEngine::BLK_TBL_ELEM	*bt_ptr = nullptr;	/* temp ptr to the BLOCK_NAME_TABLE */

	
	if (!pFlatDeviceDir) 
	{
		return;
	}

	count = (int) pFlatDeviceDir->blk_tbl.count;
	bt_ptr = pFlatDeviceDir->blk_tbl.list;


	//Loop to remove all block tables from the BLOCK_NAME_TABLE

	for (int i = 0; i < count; i++) 
	{
		Clean_FLAT_BLOCK_DIR(&bt_ptr[i].flat_block_dir);
	}


	//free the device table.
	ulong	temp_attr_avail = 0X00;
	temp_attr_avail = pFlatDeviceDir->attr_avail;

	/*
	* Free attribute structures
	*/

	if ((temp_attr_avail & BLK_TBL_MASK) && (pFlatDeviceDir->blk_tbl.list))
	{
		free((void *) pFlatDeviceDir->blk_tbl.list);
	}

	if ((temp_attr_avail & DICT_REF_TBL_MASK) && (pFlatDeviceDir->dict_ref_tbl.list)) 
	{
		free((void *) pFlatDeviceDir->dict_ref_tbl.list);
	}

	//if ((temp_attr_avail & SYMBOL_TBL_MASK) && (pFlatDeviceDir->symbol_tbl.list)) 
	//{
	//	free((void *) pFlatDeviceDir->symbol_tbl.list);
	//}

	if ((temp_attr_avail & ITEM_TBL_MASK) && (pFlatDeviceDir->item_tbl.list))
	{
		free((void *) pFlatDeviceDir->item_tbl.list);
	}

	if ((temp_attr_avail & STRING_TBL_MASK) && (pFlatDeviceDir->string_tbl.list))
	{
		free((void *) pFlatDeviceDir->string_tbl.root);
		free((void *) pFlatDeviceDir->string_tbl.list);
	}
	
	// As IMAGE_TBL is not handled no need to clean it for now. 
#if o
	if ((temp_attr_avail & IMAGE_TBL_MASK) && (pFlatDeviceDir->image_tbl.list)) 
	{
		for(int i=0; i<pFlatDeviceDir->image_tbl.count; i++)
		{
			for (unsigned long j=0; j<pFlatDeviceDir->image_tbl.list[i].count; j++)
			{
				/*if (pFlatDeviceDir->image_tbl.list[i]. list[j].language_code.str)
				{
					 Clean_STRING(&pFlatDeviceDir->image_tbl.list[i].list[j].language_code);
				}*/

					// note that device_dir->image_tbl.list[i].list[j].data should
					// not be freed here, because it is freed by rod_close 

			}
			//free((void *)pFlatDeviceDir->image_tbl.list[i].list);
		}

		free((void *) pFlatDeviceDir->image_tbl.list);
		pFlatDeviceDir->image_tbl.list = NULL;
	}

#endif
	////////////////////////////////////////
	// ****REMOVED FROM FDI*****
	//
	//////////////////////////////////////
	//if ((temp_attr_avail & LOCAL_VAR_TBL_MASK) && (pFlatDeviceDir->local_var_tbl.list))
	//{
	//	free((void *) pFlatDeviceDir->local_var_tbl.list);
	//}

	memset((char *) pFlatDeviceDir, 0, sizeof(nsEDDEngine::FLAT_DEVICE_DIR));

}


// Clean function for FLAT_BLOCK_DIR
void CLegacyHart::Clean_FLAT_BLOCK_DIR(nsEDDEngine::FLAT_BLOCK_DIR *pFlatBlockDir)
{
	ulong           temp_attr_avail;

	if (pFlatBlockDir == NULL)
	{
		return;
	}

	temp_attr_avail = pFlatBlockDir->attr_avail;

	/*
	* Free attribute structures
	*/

	if ((temp_attr_avail & BLK_ITEM_TBL_MASK) && (pFlatBlockDir->blk_item_tbl.list)) 
	{
		free((void *) pFlatBlockDir->blk_item_tbl.list);
	}

	if ((temp_attr_avail & BLK_ITEM_NAME_TBL_MASK) && (pFlatBlockDir->blk_item_name_tbl.list))
	{
		free((void *) pFlatBlockDir->blk_item_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_TBL_MASK) && (pFlatBlockDir->param_mem_tbl.list)) 
	{
		free((void *) pFlatBlockDir->param_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_NAME_TBL_MASK) && (pFlatBlockDir->param_mem_name_tbl.list)) 
	{
		free((void *) pFlatBlockDir->param_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_ELEM_TBL_MASK) && (pFlatBlockDir->param_elem_tbl.list))
	{
		free((void *) pFlatBlockDir->param_elem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_TBL_MASK) && (pFlatBlockDir->param_tbl.list)) 
	{
		free((void *) pFlatBlockDir->param_tbl.list);
	}

	if ((temp_attr_avail & REL_TBL_MASK) && (pFlatBlockDir->rel_tbl.list)) 
	{
		free((void *) pFlatBlockDir->rel_tbl.list);
	}

	if ((temp_attr_avail & UPDATE_TBL_MASK) && (pFlatBlockDir->update_tbl.list))
	{
		free((void *) pFlatBlockDir->update_tbl.list);
	}

	if( (temp_attr_avail & CRIT_PARAM_TBL_MASK) && (pFlatBlockDir->crit_param_tbl.list) )
	{
		for(int i = 0; i < pFlatBlockDir->crit_param_tbl.count; i++)
		{
			pFlatBlockDir->crit_param_tbl.list[i].Dispose();
		}
		free((void *) pFlatBlockDir->crit_param_tbl.list);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	//
	// ** This did not translate to the new interface table well ***
	//	** Need to figure out a conversion for the new tables **
	//
	//////////////////////////////////////////////////////////////////////////////////////
	//if ((temp_attr_avail & COMMAND_TBL_MASK) && (pFlatBlockDir->command_tbl.list)) 
	//{
	//	nsEDDEngine::COMMAND_TBL_ELEM *pTempElement;
	//
	//	// Before freeing the command table,free the index element lists.
	//	
	//	pTempElement = pFlatBlockDir->command_tbl.list;

	//	for (int i = 0; i < pFlatBlockDir->command_tbl.count; i++, pTempElement++)
	//	{
	//		if(pTempElement->index_list)
	//		{
	//			free((void *) pTempElement->index_list);
	//		}
	//	}

	//	free((void *) pFlatBlockDir->command_tbl.list);
	//}



	memset((char *) pFlatBlockDir, 0, sizeof(nsEDDEngine::FLAT_BLOCK_DIR));

}


//Fill_FLAT_BLOCK_DIR() - Fills in a FLAT_BLOCK_DIR structure from DDS FLAT_BLOCK_DIR
void CLegacyHart::Fill_FLAT_BLOCK_DIR(nsEDDEngine::FLAT_BLOCK_DIR *pFdiBlockDir, FLAT_BLOCK_DIR * pDdiBlockDir)
{

	pFdiBlockDir->attr_avail = pDdiBlockDir->attr_avail;
	
	// BLK_ITEM_NAME_TBL Assignement
	if( pDdiBlockDir->blk_item_name_tbl.count != 0)
	{
		pFdiBlockDir->blk_item_name_tbl.count = pDdiBlockDir->blk_item_name_tbl.count;
		pFdiBlockDir->blk_item_name_tbl.list = (nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM)*pDdiBlockDir->blk_item_name_tbl.count);

		for(int i= 0; i < pDdiBlockDir->blk_item_name_tbl.count; i++)
		{
			pFdiBlockDir->blk_item_name_tbl.list[i].blk_item_name			= pDdiBlockDir->blk_item_name_tbl.list[i].blk_item_name;
			pFdiBlockDir->blk_item_name_tbl.list[i].item_tbl_offset			= pDdiBlockDir->blk_item_name_tbl.list[i].item_tbl_offset;
			pFdiBlockDir->blk_item_name_tbl.list[i].param_tbl_offset		= pDdiBlockDir->blk_item_name_tbl.list[i].param_tbl_offset;
			//pFdiBlockDir->blk_item_name_tbl.list[i].param_list_tbl_offset		// This does not exist in Legacy HART
			pFdiBlockDir->blk_item_name_tbl.list[i].rel_tbl_offset			= pDdiBlockDir->blk_item_name_tbl.list[i].rel_tbl_offset;
			//pFdiBlockDir->blk_item_name_tbl.list[i].item_to_command_tbl_offset	// This does not exist in Legacy HART

				// These do not exist in FDI
			//pFdiBlockDir->blk_item_name_tbl.list[i].?????		= pDdiBlockDir->blk_item_name_tbl.list[i].read_cmd_count;
			//pFdiBlockDir->blk_item_name_tbl.list[i].?????		= pDdiBlockDir->blk_item_name_tbl.list[i].read_cmd_tbl_offset;
			//pFdiBlockDir->blk_item_name_tbl.list[i].?????		= pDdiBlockDir->blk_item_name_tbl.list[i].write_cmd_count;
			//pFdiBlockDir->blk_item_name_tbl.list[i].?????		= pDdiBlockDir->blk_item_name_tbl.list[i].write_cmd_tbl_offset;
		}
	}


	// BLK_ITEM_TBL Assignement
	if(pDdiBlockDir->blk_item_tbl.count != 0)
	{
		pFdiBlockDir->blk_item_tbl.count = pDdiBlockDir->blk_item_tbl.count;
		pFdiBlockDir->blk_item_tbl.list = (nsEDDEngine::BLK_ITEM_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::BLK_ITEM_TBL_ELEM)*pDdiBlockDir->blk_item_tbl.count);

		for(int i= 0; i < pDdiBlockDir->blk_item_tbl.count; i++)
		{
			pFdiBlockDir->blk_item_tbl.list[i].blk_item_id = pDdiBlockDir->blk_item_tbl.list[i].blk_item_id;
			pFdiBlockDir->blk_item_tbl.list[i].blk_item_name_tbl_offset = pDdiBlockDir->blk_item_tbl.list[i].blk_item_name_tbl_offset;
		}
	}

	// char_mem_name_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->char_mem_name_tbl.count = pDdiBlockDir->

	// char_mem_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->char_mem_tbl

	// COMMAND_TBL Assignement
	//////////////////////////////////////////////////////////////////////////////////////
	// **TO-DO
	// ** This did not translate to the new interface table well ***
	//	** Need to figure out a conversion for the new tables **
	//
	//////////////////////////////////////////////////////////////////////////////////////
	//if(pDdiBlockDir->command_tbl.count != 0)
	//{
	//	pFdiBlockDir->command_tbl.count = pDdiBlockDir->command_tbl.count;
	//	pFdiBlockDir->command_tbl.list = (nsEDDEngine::COMMAND_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::COMMAND_TBL_ELEM)*pDdiBlockDir->command_tbl.count); 

	//	for(int i = 0; i < pDdiBlockDir->command_tbl.count; i++)
	//	{
	//		pFdiBlockDir->command_tbl.list[i].count				= pDdiBlockDir->command_tbl.list[i].count;

	//		if(pDdiBlockDir->command_tbl.list[i].count != 0)
	//		{
	//			pFdiBlockDir->command_tbl.list[i].index_list		= (nsEDDEngine::COMMAND_INDEX*)calloc(1,sizeof(nsEDDEngine::COMMAND_INDEX)*pDdiBlockDir->command_tbl.list[i].count);

	//			for(int j = 0; j < pDdiBlockDir->command_tbl.list[i].count; j++)
	//			{
	//				pFdiBlockDir->command_tbl.list[i].index_list[j].id		= pDdiBlockDir->command_tbl.list[i].index_list[j].id;
	//				pFdiBlockDir->command_tbl.list[i].index_list[j].value	= pDdiBlockDir->command_tbl.list[i].index_list[j].value;
	//			}
	//		}

	//		pFdiBlockDir->command_tbl.list[i].command_number		= pDdiBlockDir->command_tbl.list[i].number;
	//		pFdiBlockDir->command_tbl.list[i].command_item_info_index			= pDdiBlockDir->command_tbl.list[i].subindex;
	//		pFdiBlockDir->command_tbl.list[i].transaction_number		= pDdiBlockDir->command_tbl.list[i].transaction;
	//		pFdiBlockDir->command_tbl.list[i].weight			= pDdiBlockDir->command_tbl.list[i].weight;
	//	}
	//}

	//command_to_var_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->command_to_var_tbl

	if(pDdiBlockDir->crit_param_tbl.count != 0)
	{
		pFdiBlockDir->crit_param_tbl.count = pDdiBlockDir->crit_param_tbl.count;
		
		pFdiBlockDir->crit_param_tbl.list = (nsEDDEngine::RESOLVED_REFERENCE*)calloc(1,sizeof(nsEDDEngine::RESOLVED_REFERENCE)*pDdiBlockDir->crit_param_tbl.count);
		
		for(int i = 0; i < pDdiBlockDir->crit_param_tbl.count; i++)
		{
			pFdiBlockDir->crit_param_tbl.list[i].Set( &pDdiBlockDir->crit_param_tbl.list[i]/*item_id*/, 1 );
		}
	}
	

	//PARAM_ELEM_TBL
	if(pDdiBlockDir->param_elem_tbl.count != 0)
	{
		pFdiBlockDir->param_elem_tbl.count = pDdiBlockDir->param_elem_tbl.count; 
		pFdiBlockDir->param_elem_tbl.list = (nsEDDEngine::PARAM_ELEM_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::PARAM_ELEM_TBL_ELEM)*pDdiBlockDir->param_elem_tbl.count);

		for(int i = 0; i < pDdiBlockDir->param_elem_tbl.count; i++)
		{
			pFdiBlockDir->param_elem_tbl.list[i].param_elem_subindex = pDdiBlockDir->param_elem_tbl.list[i].param_elem_subindex;
			pFdiBlockDir->param_elem_tbl.list[i].rel_tbl_offset = pDdiBlockDir->param_elem_tbl.list[i].rel_tbl_offset;
		}
	}
	

	//param_list_mem_name_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->param_list_mem_name_tbl

	//param_list_mem_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->param_list_mem_tbl

	//param_list_tbl is not available in nsEDDEngine::FLAT_BLOCK_DIR
	//pFdiBlockDir->param_list_tbl

	// PARAM_MEM_NAME_TBL Assignment
	if(pDdiBlockDir->param_mem_name_tbl.count != 0)
	{
		pFdiBlockDir->param_mem_name_tbl.count = pDdiBlockDir->param_mem_name_tbl.count;
		pFdiBlockDir->param_mem_name_tbl.list = (nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM)*pDdiBlockDir->param_mem_name_tbl.count);

		for(int i = 0; i < pDdiBlockDir->param_mem_name_tbl.count; i++)
		{
			pFdiBlockDir->param_mem_name_tbl.list[i].param_mem_name		= pDdiBlockDir->param_mem_name_tbl.list[i].param_mem_name;
			pFdiBlockDir->param_mem_name_tbl.list[i].param_mem_offset	= pDdiBlockDir->param_mem_name_tbl.list[i].param_mem_offset;
		}
	}
	


	//PARAM_MEM_TBL Assignment
	if(pDdiBlockDir->param_mem_tbl.count != 0)
	{
		pFdiBlockDir->param_mem_tbl.count = pDdiBlockDir->param_mem_tbl.count;
		pFdiBlockDir->param_mem_tbl.list = (nsEDDEngine::PARAM_MEM_TBL_ELEM*) calloc(1,sizeof(nsEDDEngine::PARAM_MEM_TBL_ELEM)*pDdiBlockDir->param_mem_tbl.count);

		for(int i = 0; i < pDdiBlockDir->param_mem_tbl.count; i++)
		{
			pFdiBlockDir->param_mem_tbl.list[i].item_tbl_offset	= pDdiBlockDir->param_mem_tbl.list[i].item_tbl_offset;
			pFdiBlockDir->param_mem_tbl.list[i].param_mem_class	= pDdiBlockDir->param_mem_tbl.list[i].param_mem_class;
			pFdiBlockDir->param_mem_tbl.list[i].param_mem_size	= pDdiBlockDir->param_mem_tbl.list[i].param_mem_size;
			pFdiBlockDir->param_mem_tbl.list[i].param_mem_type	= pDdiBlockDir->param_mem_tbl.list[i].param_mem_type;
			pFdiBlockDir->param_mem_tbl.list[i].rel_tbl_offset	= pDdiBlockDir->param_mem_tbl.list[i].rel_tbl_offset;
		}
	}
	

	//PARAM_TBL Assignment
	if(pDdiBlockDir->param_tbl.count != 0)
	{
		pFdiBlockDir->param_tbl.count = pDdiBlockDir->param_tbl.count;
		pFdiBlockDir->param_tbl.list = (nsEDDEngine::PARAM_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::PARAM_TBL_ELEM)*pDdiBlockDir->param_tbl.count);

		for(int i = 0; i < pDdiBlockDir->param_tbl.count; i++)
		{
			pFdiBlockDir->param_tbl.list[i].array_elem_class_or_var_class = pDdiBlockDir->param_tbl.list[i].array_elem_class_or_var_class;
			pFdiBlockDir->param_tbl.list[i].array_elem_count			  = pDdiBlockDir->param_tbl.list[i].array_elem_count;
			pFdiBlockDir->param_tbl.list[i].array_elem_item_tbl_offset	  = pDdiBlockDir->param_tbl.list[i].array_elem_item_tbl_offset;
			pFdiBlockDir->param_tbl.list[i].array_elem_size_or_var_size	  = pDdiBlockDir->param_tbl.list[i].array_elem_size_or_var_size;
			pFdiBlockDir->param_tbl.list[i].array_elem_type_or_var_type	  = pDdiBlockDir->param_tbl.list[i].array_elem_type_or_var_type;
			pFdiBlockDir->param_tbl.list[i].blk_item_name_tbl_offset	  = pDdiBlockDir->param_tbl.list[i].blk_item_name_tbl_offset;
			pFdiBlockDir->param_tbl.list[i].param_elem_count			  = pDdiBlockDir->param_tbl.list[i].param_elem_count;
			pFdiBlockDir->param_tbl.list[i].param_elem_tbl_offset		  = pDdiBlockDir->param_tbl.list[i].param_elem_tbl_offset;
			pFdiBlockDir->param_tbl.list[i].param_mem_count				  = pDdiBlockDir->param_tbl.list[i].param_mem_count;
			pFdiBlockDir->param_tbl.list[i].param_mem_tbl_offset		  = pDdiBlockDir->param_tbl.list[i].param_mem_tbl_offset;
		}
	}

	//REL_TBL Assignment
	if(pDdiBlockDir->rel_tbl.count != 0)
	{
		pFdiBlockDir->rel_tbl.count = pDdiBlockDir->rel_tbl.count;
		pFdiBlockDir->rel_tbl.list = (nsEDDEngine::REL_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::REL_TBL_ELEM)*pDdiBlockDir->rel_tbl.count);

		for(int i = 0; i < pDdiBlockDir->rel_tbl.count; i++)
		{
			//pFdiBlockDir->rel_tbl.list[i].unit_count			= pDdiBlockDir->rel_tbl.list[i].unit_count;	// FDI doesn't have unit_count
			pFdiBlockDir->rel_tbl.list[i].unit_item_tbl_offset	= pDdiBlockDir->rel_tbl.list[i].unit_item_tbl_offset;
			pFdiBlockDir->rel_tbl.list[i].update_count			= pDdiBlockDir->rel_tbl.list[i].update_count;
			pFdiBlockDir->rel_tbl.list[i].update_tbl_offset		= pDdiBlockDir->rel_tbl.list[i].update_tbl_offset;
			pFdiBlockDir->rel_tbl.list[i].wao_item_tbl_offset	= pDdiBlockDir->rel_tbl.list[i].wao_item_tbl_offset;
		}
	}

	//UPDATE_TBL Assignment
	if(pDdiBlockDir->update_tbl.count != 0)
	{
		pFdiBlockDir->update_tbl.count = pDdiBlockDir->update_tbl.count;
		pFdiBlockDir->update_tbl.list = (nsEDDEngine::UPDATE_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::UPDATE_TBL_ELEM)*pDdiBlockDir->update_tbl.count);

		for(int i = 0; i < pDdiBlockDir->update_tbl.count; i++)
		{
			pFdiBlockDir->update_tbl.list[i].desc_it_offset	= pDdiBlockDir->update_tbl.list[i].desc_it_offset;
			//pFdiBlockDir->update_tbl.list[i].op_it_offset	= pDdiBlockDir->update_tbl.list[i].op_it_offset;
			//pFdiBlockDir->update_tbl.list[i].op_subindex	= pDdiBlockDir->update_tbl.list[i].op_subindex;
		}
	}


}


// Fill_BLOCK_TABLE() - Fills in FDI block table structure
void CLegacyHart::Fill_BLOCK_TABLE(nsEDDEngine::BLK_TBL *pFdiBlockTbl, BLK_TBL * pDdiBlockTbl)
{
	if(pDdiBlockTbl->count != 0)
	{
		pFdiBlockTbl->count = pDdiBlockTbl->count;
		pFdiBlockTbl->list = (nsEDDEngine::BLK_TBL_ELEM*)calloc(1,sizeof(nsEDDEngine::BLK_TBL_ELEM)*pDdiBlockTbl->count);

		for(int i = 0; i < pDdiBlockTbl->count; i++)
		{
			pFdiBlockTbl->list[i].blk_dir_dd_ref.object_index =  pDdiBlockTbl->list[i].blk_dir_dd_ref.object_index;
			pFdiBlockTbl->list[i].blk_id					  = pDdiBlockTbl->list[i].blk_id;
			pFdiBlockTbl->list[i].char_rec_bint_offset		  = pDdiBlockTbl->list[i].char_rec_bint_offset;
			pFdiBlockTbl->list[i].char_rec_item_tbl_offset	  = pDdiBlockTbl->list[i].char_rec_bint_offset;
			pFdiBlockTbl->list[i].item_tbl_offset			  = pDdiBlockTbl->list[i].char_rec_item_tbl_offset;
			pFdiBlockTbl->list[i].usage						  = pDdiBlockTbl->list[i].usage;
			Fill_FLAT_BLOCK_DIR(&pFdiBlockTbl->list[i].flat_block_dir,&pDdiBlockTbl->list[i].flat_block_dir);
		}
	}

}