#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass]
        public ref class When_getting_the_item_id_of_a_variable_with_two_different_instances_of_the_edd_engine_in_fmA
        {
            static ITEM_ID* ItemIdOfFirstInstance;
            static ITEM_ID* ItemIdOfSecondInstance;
            static ITEM_ID* ItemIdDeviceRootMenuInstanceOne;
            static ITEM_ID* ItemIdDeviceRootMenuInstanceTwo;
            static ITEM_ID* ItemIdTestVarInstanceOne;
            static ITEM_ID* ItemIdTestVarInstanceTwo;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                EddEngineController^ engine1 = gcnew EddEngineController("Hart\\TestCase0001.fmA");
                ParamCacheSimulator *paramCache1 = new ParamCacheSimulator();
                engine1->Initialize(paramCache1);
                paramCache1->SetEddEngine(engine1->Instance);

                EddEngineController^ engine2 = gcnew EddEngineController("Hart\\TestCase0001.fmA");
                ParamCacheSimulator *paramCache2 = new ParamCacheSimulator();
                engine2->Initialize(paramCache2);
                paramCache2->SetEddEngine(engine2->Instance);

                ItemIdOfFirstInstance = new ITEM_ID;
                ItemIdOfSecondInstance = new ITEM_ID;

                ItemIdTestVarInstanceOne = new ITEM_ID;
                ItemIdTestVarInstanceTwo = new ITEM_ID;
                ItemIdDeviceRootMenuInstanceOne = new ITEM_ID;
                ItemIdDeviceRootMenuInstanceTwo = new ITEM_ID;


                engine1->Instance->GetItemIdFromSymbolName(L"test_var_unsigned", ItemIdTestVarInstanceOne);
                engine2->Instance->GetItemIdFromSymbolName(L"test_var_unsigned", ItemIdTestVarInstanceTwo);

                engine1->Instance->GetItemIdFromSymbolName(L"device_root_menu", ItemIdDeviceRootMenuInstanceOne);
                engine2->Instance->GetItemIdFromSymbolName(L"device_root_menu", ItemIdDeviceRootMenuInstanceTwo);

                engine1->Instance->GetItemIdFromSymbolName(L"page_menu_2", ItemIdOfFirstInstance);
                engine2->Instance->GetItemIdFromSymbolName(L"page_menu_2", ItemIdOfSecondInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_as_item_id_from_the_first_instance_16387()
            {
                Assert::AreEqual(16387u, *ItemIdOfFirstInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_as_item_id_from_the_second_instance_16387()
            {
                Assert::AreEqual(16387u, *ItemIdOfSecondInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_the_correct_item_id_for_device_root_menu_in_instance_one()
            {
                Assert::AreEqual(1020u, *ItemIdDeviceRootMenuInstanceOne);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_the_correct_item_id_for_device_root_menu_in_instance_two()
            {
                Assert::AreEqual(1020u, *ItemIdDeviceRootMenuInstanceTwo);
            }


            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_the_correct_item_id_for_test_var_unsigned_in_instance_one()
            {
                Assert::AreEqual(16391u, *ItemIdTestVarInstanceOne);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_the_correct_item_id_for_test_var_unsigned_in_instance_two()
            {
                Assert::AreEqual(16391u, *ItemIdTestVarInstanceTwo);
            }
        };

        [TestClass]
        public ref class When_getting_the_item_id_of_a_variable_with_the_same_name_from_different_dds_from_different_instances_of_the_edd_engine_in_fmA
        {
            static ITEM_ID* ItemIdOfFirstInstance;
            static ITEM_ID* ItemIdOfSecondInstance;
            static ITEM_ID* ItemIdOfThirdInstance;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {

                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase0003.fmA");
                ParamCacheSimulator *paramCache = new ParamCacheSimulator();
                engine->Initialize(paramCache);
                paramCache->SetEddEngine(engine->Instance);
                ItemIdOfFirstInstance = new ITEM_ID;
                engine->Instance->GetItemIdFromSymbolName(L"test_int", ItemIdOfFirstInstance);

                engine = gcnew EddEngineController("Hart\\TestCase0006.fm8");
                paramCache = new ParamCacheSimulator();
                engine->Initialize(paramCache);
                paramCache->SetEddEngine(engine->Instance);
                ItemIdOfSecondInstance = new ITEM_ID;
                engine->Instance->GetItemIdFromSymbolName(L"test_int", ItemIdOfSecondInstance);

                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");
                paramCache = new ParamCacheSimulator();
                engine->Initialize(paramCache);
                paramCache->SetEddEngine(engine->Instance);
                ItemIdOfThirdInstance = new ITEM_ID;
                engine->Instance->GetItemIdFromSymbolName(L"test_int", ItemIdOfThirdInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_as_item_id_from_the_first_instance_16392()
            {
                Assert::AreEqual(16392u, *ItemIdOfFirstInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_as_item_id_from_the_second_instance_16388()
            {
                Assert::AreEqual(16388u, *ItemIdOfSecondInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_as_item_id_from_the_third_instance_131075()
            {
                Assert::AreEqual(131075u, *ItemIdOfThirdInstance);
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_not_be_the_same_item_id_for_the_first_and_second_item()
            {
                Assert::AreNotEqual(*ItemIdOfFirstInstance, *ItemIdOfSecondInstance);
            }

        };

        [TestClass]
        public ref class When_instantiating_multiple_edd_engine_instances_from_different_threads_with_FM8
        {
            static bool _isCrashed = false;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                System::Threading::Thread^ thread1 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread2 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread3 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread4 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread5 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread6 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 

                thread1->Start();
                thread2->Start();
                thread3->Start();
                thread4->Start();
                thread5->Start();
                thread6->Start();

                thread1->Join();
                thread2->Join();
                thread3->Join();
                thread4->Join();
                thread5->Join();
                thread6->Join();
            }

            static void ThreadProc()
            {
                try
                {
                    EddEngineController^ controller = gcnew EddEngineController("Hart\\TestCase0003.fm8");
                    ParamCacheSimulator *paramCache = new ParamCacheSimulator();
                    controller->Initialize(paramCache);
                    paramCache->SetEddEngine(controller->Instance);
                }
                catch (...)
                {
                    _isCrashed = true;
                }

            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_not_crash()
            {
                Assert::IsFalse(_isCrashed);
            }
        };

        [TestClass]
        public ref class When_instantiating_multiple_edd_engine_instances_from_different_threads_with_FMA
        {
            static bool _isCrashed = false;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                System::Threading::Thread^ thread1 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread2 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread3 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread4 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread5 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
                System::Threading::Thread^ thread6 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 

                thread1->Start();
                thread2->Start();
                thread3->Start();
                thread4->Start();
                thread5->Start();
                thread6->Start();

                thread1->Join();
                thread2->Join();
                thread3->Join();
                thread4->Join();
                thread5->Join();
                thread6->Join();
            }

            static void ThreadProc()
            {
                try
                {
                    EddEngineController^ controller = gcnew EddEngineController("Hart\\TestCase0003.fmA");
                    ParamCacheSimulator *paramCache = new ParamCacheSimulator();
                    controller->Initialize(paramCache);
                    paramCache->SetEddEngine(controller->Instance);
                }
                catch (...)
                {
                    _isCrashed = true;
                }
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_not_crash()
            {
                Assert::IsFalse(_isCrashed);
            }

        };


        [TestClass]
        public ref class When_instantiating_the_edd_engine_two_times_with_the_same_package_with_only_minor_intern_changes_FMA
        {
        public:

            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
           

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                String^ firstTestCaseName = "Bug1242\\Original\\TestCase002f.fmA";
                String^ secondTestCaseName = "Bug1242\\TestCase002f.fmA";;

                // initialize first time with the original test package
                engine = gcnew EddEngineController(firstTestCaseName);
                engine->Initialize();

                // now delete the initialized EDD Engine 
                delete engine;

                // copy the modified file to the original place
                System::IO::File::Delete(firstTestCaseName);
                System::IO::File::Copy(secondTestCaseName, firstTestCaseName);

                // initialize a new EDD Engine with the changed test package
                engine = gcnew EddEngineController(firstTestCaseName);
                engine->Initialize();


                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("main_test_menu", &test_var_specifier);
                genericItem = engine->GetGenericItem(test_var_specifier.id, nsEDDEngine::ITYPE_MENU);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);


            }

            [ClassCleanup]
            static void CleanUp()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MultipleInstancesTest")]
            void It_should_return_the_label_for_the_menu_from_the_second_loaded_package()
            {
                Assert::AreEqual("Changed the Label", gcnew String(test_menu->label.c_str()));
            }
        };

      
    } // namespace Hart
} // namespace ComponentTests
