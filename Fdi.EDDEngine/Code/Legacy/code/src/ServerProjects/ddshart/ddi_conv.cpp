/*
 *	@(#) $Id: ddi_conv.c,v 1.7 1996/04/18 14:43:48 davcomf Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 *
 *  This file contains all of the functions for DDI convenience
 */

#include "stdinc.h"
#include <../src/ServerProjects/server/hrt_cstk/hsym0002.h>
#ifdef ISPTEST
#include "tst_fail.h"
#endif

#include "HART/HARTEDDEngine/DDSSupport.h"
#include "DeviceTypeMgr.h"
#include "ManualUnitLookup.h"

/*********************************************************************
 *
 *	Name: conv_block_spec
 *	ShortDesc: convert the block_specifier
 *
 *	Description:
 *		ddi_conv_specifiers converts the block_specifier into a
 *		block_handle if the type is DDI_BLOCK_TAG.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *
 *	Outputs:
 *		block_handle:	the block_handle
 *
 *	Returns:DDS_SUCCESS, DDI_INVALID_PARAM
 *		returns from other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
conv_block_spec(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
BLOCK_HANDLE   *block_handle)
{
	int             rc;	/* return code */


	switch (block_spec->type) {


	case DDI_BLOCK_TAG:
		rc = tr_block_tag_to_block_handle(env_info, block_spec->block.tag,
			block_handle);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
		return DDS_SUCCESS;

	case DDI_BLOCK_HANDLE:
		*block_handle = block_spec->block.handle;
		return DDS_SUCCESS;

	default:
		return DDI_INVALID_TYPE;

	}
}


/*********************************************************************
 *
 *	Name: convert_param_spec
 *	ShortDesc: convert the param_specifer structure into a bint offset
 *
 *	Description:
 *		convert_param_spec takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the Block Item Name Table offset
 *
 *	Inputs:
 *		block_handle:	the block handle
 *		param_spec:	    a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		bint_offset:	a pointer to the block item name table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_param_spec(
ENV_INFO			*env_info,
BLOCK_HANDLE         block_handle,
DDI_PARAM_SPECIFIER *param_spec,
BLK_TBL_ELEM        *bt_elem,
int                 *bint_offset)
{

	int             rc;	/* return code */


	/*
	 * Convert the param specifier to a block item name table offset
	 */

	switch (param_spec->type) {

	case DDI_PS_ITEM_ID:
	case DDI_PS_ITEM_ID_SI:
		rc = tr_id_to_bint_offset(bt_elem, param_spec->item.id, bint_offset);
		break;

	case DDI_PS_PARAM_NAME:
	case DDI_PS_PARAM_NAME_SI:
		rc = tr_name_to_bint_offset(bt_elem, param_spec->item.name,
			bint_offset);
		break;

	case DDI_PS_OP_INDEX:
	case DDI_PS_OP_INDEX_SI:
		rc = tr_op_index_to_bint_offset(env_info, block_handle, bt_elem,
			param_spec->item.op_index, bint_offset);
		break;	

	case DDI_PS_PARAM_OFFSET:
	case DDI_PS_PARAM_OFFSET_SI:
		rc = tr_param_to_bint_offset(bt_elem, param_spec->item.param,
			bint_offset);
		break;

	case DDI_PS_CHARACTERISTICS_SI:

/* It's not clear what this does because the block item name table entry
   for characteristics is empty except for the item table offset, and the
   core entries are useless (i.e. parameter table offset */


		rc = tr_char_to_bint_offset(bt_elem, bint_offset);

		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: conv_block_and_param_spec
 *
 *	Description:
 *       convert a block specifier to a block handle and a
 *       parameter specifier to a block item name table offset
 *
 *	Inputs:
 *		block_spec: a pointer to the block specifier
 *      param_spec: a pointer to the parameter specifier
 *
 *	Outputs:
 *		block_handle:	a block handle
 *		bt_elem:		a pointer to a pointer of block table element
 *		bint_offset:	an index into the Block Item Name Table
 *
 *	Returns:
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
conv_block_and_param_spec(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
BLOCK_HANDLE   *block_handle,
BLK_TBL_ELEM   **bt_elem,
int            *bint_offset)
{
	int             rc;	/* return code */
	ROD_HANDLE       rod_handle =0;	/* device description handle */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_spec(env_info, block_spec, block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, *block_handle, bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = convert_param_spec(env_info, *block_handle, param_spec, *bt_elem, bint_offset);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_param_spec fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnMgr->ds_dd_block_load(env_info, *block_handle, &rod_handle, 1);
			if (!rc) {

				rc = convert_param_spec(env_info, *block_handle,
					param_spec, *bt_elem, bint_offset);

				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: convert_param_spec_to_bint_offset
 *	ShortDesc:  convert the param specifier structure into an bint
 *				offset
 *
 *	Description:
 *		convert_param_spec_to_bint_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the named item table offset
 *
 *	Inputs:
 *              block_handle    the block handle
 *
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER
 *						structure
 *		bt_elem:	the current block table element
 *
 *	Outputs:
 *		bint_offset:	a pointer to the Block Item Name Table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from bint_offset table
 *  request functions
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

static
int
convert_param_spec_to_bint_offset(
ENV_INFO *env_info,
BLOCK_HANDLE block_handle,
DDI_PARAM_SPECIFIER *param_spec,
BLK_TBL_ELEM   *bt_elem,
int            *bint_offset)
{

  int             rc;	/* return code */

  switch (param_spec->type) {

  case DDI_PS_ITEM_ID:
    rc = tr_id_to_bint_offset(bt_elem, param_spec->item.id,bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME:
    rc = tr_name_to_bint_offset(bt_elem, param_spec->item.name,bint_offset);
    break;
    
  case DDI_PS_PARAM_OFFSET:
    rc = tr_param_to_bint_offset(bt_elem, param_spec->item.param,bint_offset);
    break;
    
  case DDI_PS_OP_INDEX:
    rc = tr_op_index_to_bint_offset(env_info, block_handle, bt_elem, param_spec->item.op_index, bint_offset);
      break;

  case DDI_PS_ITEM_ID_SI:
    rc = tr_id_si_to_bint_offset(bt_elem, param_spec->subindex,param_spec->item.id, bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME_SI:
    rc = tr_name_si_to_bint_offset(bt_elem, param_spec->subindex,param_spec->item.name, bint_offset);
    break;
    
  case DDI_PS_OP_INDEX_SI:
    rc = tr_op_index_si_to_bint_offset(env_info, block_handle, bt_elem, param_spec->item.op_index, bint_offset,
				    param_spec->subindex);
    break;

  case DDI_PS_PARAM_OFFSET_SI:
    rc = tr_param_si_to_bint_offset(bt_elem, param_spec->subindex, param_spec->item.param, bint_offset);
    break;
  case DDI_PS_CHARACTERISTICS_SI:
    rc = tr_char_to_bint_offset(bt_elem, bint_offset);
    break;
  default:
    rc = DDI_INVALID_REQUEST_TYPE;
  }
  
  return rc;
}




/*********************************************************************
 *
 *	Name: get_rt_offset
 *	ShortDesc: convert a bint_offset or bint_offset_si into a relation table offset
 *
 *	Description:
 *		get_rt_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate conversion function to
 *              convert the bint_offset to a relation table offset.
 *
 *	Inputs:
 *		bint_offset:	the bint offset
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		rt_offset:		a pointer to the Relation Table
 *						offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
get_rt_offset(
int            bint_offset,
DDI_PARAM_SPECIFIER *param_spec,
BLK_TBL_ELEM   *bt_elem,
int            *rt_offset)
{

	int             rc;	/* return code */

	/*
	 * Convert the request to a Relation Offset
	 */

	switch (param_spec->type) {

	case DDI_PS_ITEM_ID:
	case DDI_PS_PARAM_NAME:
	case DDI_PS_OP_INDEX:
	case DDI_PS_PARAM_OFFSET:
		rc = tr_bint_offset_to_rt_offset(bt_elem, bint_offset,
			rt_offset);
		break;


	case DDI_PS_ITEM_ID_SI:
	case DDI_PS_PARAM_NAME_SI:
	case DDI_PS_OP_INDEX_SI:
	case DDI_PS_PARAM_OFFSET_SI:
	case DDI_PS_CHARACTERISTICS_SI:
		rc = tr_bint_offset_si_to_rt_offset(bt_elem,
			param_spec->subindex, bint_offset, rt_offset);
		break;


	default:
		rc = DDI_INVALID_REQUEST_TYPE;
		break;

	}
	return rc;
}


/***********************************************************************
 *
 *  Name:  compare_cmd_num
 *  ShortDesc:  compare two command numbers for bsearch()
 *
 *  Description:
 *		The compare_cmd_num function compares to command numbers
 *		and exists to support a call to the binary search function
 *		bsearch()
 *
 *  Inputs:
 *		cmd_1 : a pointer to an element from the cmd_num_id_tbl
 *		cmd_2 : a pointer to an element from the cmd_num_id_tbl
 *
 *  Outputs:
 *      None
 *
 *  Returns:
 *		-1
 *		 0
 *		 1
 *
 *  Author:	Christian Gustafson
 *
 **********************************************************************/

static int
compare_cmd_num(
CMD_NUM_ID_TBL_ELEM	*cmd_1,
CMD_NUM_ID_TBL_ELEM	*cmd_2)
{
	/*
	 *	Compare the two Subindexes and return the reflective value.
	 */

	if (cmd_1->number <	cmd_2->number) {
		return(-1);
	}
	else if (cmd_1->number > cmd_2->number) {
		return(1);
	}
	else {
		return(0);
	}
}

/*********************************************************************
 *
 *	Name: ddi_get_cmd_id
 *	ShortDesc: get the item id of a command
 *
 *	Description:
 *		ddi_get_cmd_id() will return the item id
 *      of a command which corresponds to the given
 *      
 *
 *	Inputs:
 *		block_spec : a pointer to the block specifier
 *      cmd_number : the command number
 *
 *	Outputs:
 *      cmd_id : a pointer to the item id of the command
 *
 *	Returns: DDI_SUCCESS, DDI_COMMAND_NOT_FOUND
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_cmd_id(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
ulong				 cmd_number,
ITEM_ID  			*cmd_id)
{
	BLOCK_HANDLE    		block_handle;     /* Block Handle */
	CMD_NUM_ID_TBL			*cmd_numbers_tbl; /* Command number to item id table pointer */
	CMD_NUM_ID_TBL_ELEM		*command;         /* Pointer at element of cmd_num_id_tbl */
	int						rc;				  /* return code */
	FLAT_DEVICE_DIR			*flat_device_dir ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	ASSERT_RET(block_spec && cmd_id, DDI_INVALID_PARAM);

	rc = conv_block_spec(env_info, block_spec, &block_handle);  /* get a block handle */
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir);
	if (rc != SUCCESS) {
		return(rc) ;
	}
	cmd_numbers_tbl = &flat_device_dir->cmd_num_id_tbl;

	if (! cmd_numbers_tbl->count) {

		return DDI_COMMAND_NOT_FOUND;
	}


	command = (CMD_NUM_ID_TBL_ELEM *) bsearch(
		(char *)&cmd_number,
		(char *)cmd_numbers_tbl->list,
		(unsigned int) cmd_numbers_tbl->count,
		sizeof(CMD_NUM_ID_TBL_ELEM),
		(CMP_FN_PTR)compare_cmd_num);

	if(! command) {

		return DDI_COMMAND_NOT_FOUND;
	}

	*cmd_id = command->item_id;
	
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_check_enum_var_value
 *	ShortDesc: check for the value in the FLAT_VAR
 *
 *	Description:
 *		ddi_check_enum_var_value searches the enum_value_list for
 *		the given value if the FLAT_VAR is of type ENUMERATED
 *		 or BIT_ENUMERATED
 *
 *	Inputs:
 *		var:	a pointer to the flat variable to search
 *		value:	a pointer to the value to look for
 *
 *	Outputs:
 *		none
 *
 *	Returns: DDI_LEGAL_ENUM_VAR_VALUE, DDI_ILLEGAL_ENUM_VAR_VALUE
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_check_enum_var_value(
FLAT_VAR       *var,
unsigned long  *value)
{

	unsigned short  inc;	/* counter for enum list */
	ENUM_VALUE     *enum_ptr;	/* temp pointer for the enum_value */

	ASSERT_RET(var && value, DDI_INVALID_PARAM);

	enum_ptr = var->enums.list;

	if ((var->type_size.type == ENUMERATED) ||
		(var->type_size.type == BIT_ENUMERATED)) {

		for (inc = 0; inc < var->enums.count; inc++, enum_ptr++) {

			/*
			 * Check that the value is available
			 */

			if (enum_ptr->evaled & ENUM_VAL_EVALED) {

				if (enum_ptr->val == *value) {

					return DDI_LEGAL_ENUM_VAR_VALUE;
				}
			}
		}
		return DDI_ILLEGAL_ENUM_VAR_VALUE;
	}
	else {

		return DDI_ILLEGAL_ENUM_VAR_VALUE;
	}
}




/*********************************************************************
 *
 *	Name: convert_item_spec_to_item_type
 *	ShortDesc: convert the item_spec structure into an item_type
 *
 *	Description:
 *		convert_item_spec_to_item_type takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the item_type.
 *
 *	Inputs:
 *		block_handle:	the current block handle
 *		item_spec:	a pointer to the DDI_ITEM_SPECIFIER structure
 *		bt_elem:	the Block Table element
 *
 *	Outputs:
 *		item_type:		a pointer to the item type
 *		item_id 		a pointer to the item id
 *
 *	Returns:DDI_INVALID_REQUEST_TYPE and returns from
 *  	other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_item_spec_to_item_type(
ENV_INFO		*env_info,
BLOCK_HANDLE    block_handle,
DDI_ITEM_SPECIFIER *item_spec,
BLK_TBL_ELEM   *bt_elem,
ITEM_TYPE      *item_type,
ITEM_ID		   *item_id)
{

	int             rc;			/* return code */
	ITEM_TBL       	*it;		/* item table */
	ITEM_TBL_ELEM  	*it_elem;	/* item table element */
	FLAT_DEVICE_DIR	*flat_device_dir ;

#ifdef ISPTEST
	TEST_FAIL(CONVERT_ITEM_SPEC);
#endif
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	switch (item_spec->type) {

	case DDI_ITEM_ID:
	  rc = tr_id_to_ite(env_info, it, item_spec->item.id, &it_elem);
		break;


	case DDI_ITEM_NAME:
	  rc = tr_name_to_ite(it, bt_elem, item_spec->item.name,
			      &it_elem);
	  break;

	case DDI_ITEM_OP_INDEX:
	  rc = tr_op_index_to_ite(env_info, block_handle, it,
				  bt_elem, item_spec->item.op_index, &it_elem);
	  break;
		
	case DDI_ITEM_PARAM:
	  rc = tr_param_to_ite(it, bt_elem, item_spec->item.param,
			       &it_elem);
	  break;
		
	case DDI_ITEM_ID_SI:
	  rc = tr_id_si_to_ite(it, bt_elem, item_spec->item.id,
			       item_spec->subindex, &it_elem);
	  break;
		
	case DDI_ITEM_NAME_SI:
	  rc = tr_name_si_to_ite(it, bt_elem, item_spec->item.name,
				 item_spec->subindex, &it_elem);
	  break;

	case DDI_ITEM_OP_INDEX_SI:
	  rc = tr_op_index_si_to_ite(env_info, block_handle, it, bt_elem,
				     item_spec->item.op_index, item_spec->subindex,
				     &it_elem);
	  break;

	case DDI_ITEM_PARAM_SI:
	  rc = tr_param_si_to_ite(it, bt_elem,
				  item_spec->item.param, item_spec->subindex,
				  &it_elem);
	  break;
	case DDI_ITEM_BLOCK:
	  *item_type = BLOCK_ITYPE;
	  return DDS_SUCCESS;
	  
	case DDI_ITEM_PARAM_LIST:
	  *item_type = PARAM_LIST_ITYPE;
	  return DDS_SUCCESS;
	  
	case DDI_ITEM_CHARACTERISTICS:
	  *item_type = BLOCK_CHAR_ITYPE;
	  return DDS_SUCCESS;

	default:
		return DDI_INVALID_REQUEST_TYPE;
	}

	if (rc != DDS_SUCCESS) {
		return rc;
	}

	*item_type = ITE_ITEM_TYPE(it_elem);
	*item_id = ITE_ITEM_ID(it_elem) ;

	return rc;
}




/*****************************************************************
 *
 *	Name: ddi_get_type_and_item_id
 *	ShortDesc: get the type and the item id of the item
 *
 *	Description:
 *		ddi_get_type will return the item_type and the item id
 *		of the requested type.  
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		item_spec:		a pointer to the item specifier
 *
 *	Outputs:
 *		item_type:		a pointer to the item type of item requested
 *		item_id:		a pointer to the item id of item requested
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 *****************************************************************/

int
ddi_get_type_and_item_id(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_ITEM_SPECIFIER *item_spec,
ITEM_TYPE      *item_type,
ITEM_ID        *item_id)
{

	int             rc;	/* return code */
	ROD_HANDLE       rod_handle = 0;	/* device description handle */
	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	BLOCK_HANDLE    block_handle;	/* Block Handle */

	ASSERT_RET(block_spec && item_spec && item_type, DDI_INVALID_PARAM);

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = convert_item_spec_to_item_type(env_info, block_handle, item_spec,
		bt_elem, item_type, item_id);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_item_spec_to_item_type fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnMgr->ds_dd_block_load(env_info, block_handle, &rod_handle, 1);
			if (!rc) {

				rc = convert_item_spec_to_item_type(env_info, block_handle, item_spec,
					bt_elem, item_type, item_id);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}

/*****************************************************************
 *
 *	Name: ddi_get_type
 *	ShortDesc: get the type of the item
 *
 *	Description:
 *		ddi_get_type will return the item_type of the
 *		requested type
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		item_spec:		a pointer to the item specifier
 *
 *	Outputs:
 *		item_type:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *	Author: Steve Beyerl
 *
 *****************************************************************/

int
ddi_get_type(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_ITEM_SPECIFIER *item_spec,
ITEM_TYPE      *item_type)
{

	int		rc;		// return code
	ITEM_ID	item_id;  // item id of requested item

	rc = ddi_get_type_and_item_id(env_info, block_spec, item_spec, 
			item_type, &item_id);

	return rc;
}


int
ddi_find_unit_ids(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID id_to_find,
	ITEM_ID *item_id_of_unit_rel, ITEM_ID *item_id_of_unit_var)
{
    DDI_GENERIC_ITEM gi;
	BLK_TBL_ELEM   *bt_elem;		/* Block Table element pointer */
    int rc;
    //ENV_INFO env_info;
    DDI_ITEM_SPECIFIER blk_item_spec;
    BLOCK_HANDLE block_handle;
    int req_mask;
    FLAT_BLOCK *flat;
    int i;

	/*
	 * convert the block specifier to a block handle
	 */

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {
		return rc;
	}
	else 
	{
		env_info->block_handle = block_handle;
	}
	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

    blk_item_spec.type = DDI_ITEM_BLOCK;
    blk_item_spec.item.id = bt_elem->blk_id;

    req_mask = BLOCK_UNIT;

    memset(&gi, 0, sizeof(gi));
    rc = ddi_get_item(block_spec, &blk_item_spec, env_info, req_mask, &gi); 
    if (rc)
        return rc;

	rc = DDI_TAB_NO_UNIT;	// Init to No units, in case we skip over the loop completely

    flat = (FLAT_BLOCK *)gi.item;
    for (i=0; i < flat->unit.count; i++)
    {
		DDI_GENERIC_ITEM gi_unit = {0};
		DDI_ITEM_SPECIFIER is_unit = {0};

        is_unit.type = DDI_ITEM_ID;
        is_unit.item.id = flat->unit.list[i];

        rc = ddi_get_item(block_spec, &is_unit, env_info, UNIT_ITEMS, &gi_unit);
        if (rc)
        {
            ddi_clean_item(&gi);
            return rc;
        }

		bool bFound = false;
        FLAT_UNIT *unit = (FLAT_UNIT *)gi_unit.item;

		if ( ManualUnitLookup::IsMyUnit(unit, id_to_find) )
		{								// We found our unit relation
			if (item_id_of_unit_rel)
				*item_id_of_unit_rel = unit->id;
			if (item_id_of_unit_var)
				*item_id_of_unit_var = unit->items.var.desc_id;

			bFound = true;
			rc = DDS_SUCCESS;
		}
		else
		{
			rc = DDI_TAB_NO_UNIT;
		}

        ddi_clean_item(&gi_unit);

		if (bFound)
		{
			break;
		}
    }

    ddi_clean_item(&gi);
    return rc;
}


/*********************************************************************
 *
 *	Name: ddi_get_unit
 *	ShortDesc: get the item_id of the unit
 *
 *	Description:
 *		ddi_get_unit will return the item_id of the unit relation
 *		that contains the unit variable of the requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the parameter specifier
 *
 *	Outputs:
 *		item_id:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_unit(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
ITEM_ID				*unit_item_id)
{
	int             rc;	/* return code */
	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	ITEM_TBL       *it;	/* item table */
	ITEM_TBL_ELEM  *it_elem;/* item table element */
	BLOCK_HANDLE    block_handle;	/* Block Handle */
	REL_TBL_ELEM   *rt_elem;/* relation table element */
	int             rt_offset;	/* relation table offset */
	int             bint_offset;    /* bint offset */
	FLAT_DEVICE_DIR	*flat_device_dir ;
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr* pConnMgr = pDDSSupport->GetConnectionManager();
	
	ASSERT_RET(block_spec && param_spec && unit_item_id, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(env_info, block_spec, 
				       param_spec,
				       &block_handle,
				       &bt_elem,
				       &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}


	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rt_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	if (rt_offset == -1) {
		return DDI_TAB_NO_UNIT;
	}

	REL_TBL *rt = BTE_RT(bt_elem);
	rt_elem = RTE(rt, rt_offset);

	if (RTE_UNIT_IT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_UNIT;
	}

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	

	it_elem = ITE(it, RTE_UNIT_IT_OFFSET(rt_elem));	// Get the UNIT ITEM_ID from the Relation Table
	ITEM_ID unitFromTbl = ITE_ITEM_ID(it_elem);


	if (rt->UnitLookup == nullptr)	// If we don't have the UnitLookup class yet, create it.
	{
		rt->UnitLookup = new ManualUnitLookup();

		rc = rt->UnitLookup->Init(env_info, block_spec);
		if (rc != SUCCESS) {
			return(rc) ;
		}
	}

	// Find the Unit relation that best matches this param_spec
	ITEM_ID unit_rel = 0;

	rc = rt->UnitLookup->FindUnitRel(env_info, block_spec, unitFromTbl, param_spec, &unit_rel);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	*unit_item_id = unit_rel;

	return DDS_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddi_get_wao
 *	ShortDesc: get the item_id of the wao
 *
 *	Description:
 *		ddi_get_wao will return the item_id of the wao of the
 *		requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the param specifier
 *
 *	Outputs:
 *		item_id:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
//not used in legacy engine
int
ddi_get_wao(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
ITEM_ID        *item_id)
{
	int             rc;	/* return code */
	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	ITEM_TBL       *it;	/* item table */
	ITEM_TBL_ELEM  *it_elem;/* item table element */
	BLOCK_HANDLE    block_handle;	/* a Block Handle */
	int             rt_offset;	/* relation table offset */
	int             bint_offset;	/* bint offset */
	REL_TBL_ELEM   *rt_elem;/* relation table element */
	FLAT_DEVICE_DIR	*flat_device_dir ;

	ASSERT_RET(block_spec && param_spec && item_id, DDI_INVALID_PARAM);

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_and_param_spec(env_info, block_spec, 
				       param_spec,
				       &block_handle,
				       &bt_elem,
				       &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rt_offset);



	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * Use the Relation Table Offset to form the item id of the Write As
	 * One Relation.
	 */

	if (rt_offset == -1) {
		return DDI_TAB_NO_WAO;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rt_offset);

	if (RTE_WAO_IT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_WAO;
	}

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it, RTE_WAO_IT_OFFSET(rt_elem));
	*item_id = ITE_ITEM_ID(it_elem);
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_update_items
 *	ShortDesc: get the items of a relation
 *
 *	Description:
 *		ddi_get_update_items will return the list of item_ids associated
 *      with the right side of a relation.  This only affects wao and
 *      refresh relations.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:	a pointer to the relation request
 *
 *	Outputs:
 *		op_desc_list:	a pointer to the requested list of operational
 *						references (and descriptive references).
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
//not used in the legacy engine
int
ddi_get_update_items(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
OP_DESC_LIST   *op_desc_list)
{
	int             rc;	/* return code */
	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	REL_TBL_ELEM   *rt_elem;/* relation table element */
	int             count;	/* number of relation items */
	int             rel_tbl_offset;	/* relation table offset */
	OP_DESC        *update_ptr;	/* update list pointer */
	int             bint_offset;    /* bint offset */
	int             ut_offset;	/* Update table offset */
	UPDATE_TBL     *update_tbl;	/* Update table */
	int             inc;	/* loop variable */
	BLOCK_HANDLE    block_handle;	/* A Block Handle */
	ITEM_TBL_ELEM  *op_it_elemp;	/* Operational reference */
	ITEM_TBL_ELEM  *desc_it_elemp;	/* Descriptive reference */
	ITEM_TBL       *it;
	FLAT_DEVICE_DIR	*flat_device_dir ;

	ASSERT_RET(block_spec && param_spec && op_desc_list, DDI_INVALID_PARAM);

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_and_param_spec(env_info, block_spec, 
				       param_spec,
				       &block_handle,
				       &bt_elem,
				       &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rel_tbl_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}


	if (rel_tbl_offset == -1) {
		return DDI_TAB_NO_UPDATE;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rel_tbl_offset);

	if (RTE_UT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_UPDATE;
	}

	count = RTE_U_COUNT(rt_elem);
	ut_offset = RTE_UT_OFFSET(rt_elem);
	update_tbl = BTE_UT(bt_elem);

	update_ptr = (OP_DESC *) realloc((void *) op_desc_list->list,
		(size_t) count * sizeof(OP_REF_TRAIL));

	if (update_ptr == NULL) {
		return DDI_MEMORY_ERROR;
	}

	op_desc_list->list = update_ptr;
	op_desc_list->count = (unsigned short) count;

	rc  = pConnMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl ;

	for (inc = 0; inc < count; inc++, update_ptr++) {

		// Get this Update Table Element
		UPDATE_TBL_ELEM *pUTElem = &update_tbl->list[ut_offset + inc];

		// Get the Item Table rows that correspond to the op_it_offset and desc_it_offset
		op_it_elemp   = &it->list[pUTElem->op_it_offset];
		desc_it_elemp = &it->list[pUTElem->desc_it_offset];

		// If the ITEM_TYPE of the op_it_offset is wrong, (Tokenizer bug), fix it
		if ((op_it_elemp->item_type == COLLECTION_ITYPE) || (op_it_elemp->item_type == RECORD_ITYPE))
		{                                                             // Change the op_it_offset in the Update Table
			pUTElem->op_it_offset = pUTElem->desc_it_offset;       // Set it to the desc_it_offset instead
			pUTElem->op_subindex = 0;                              // Zero the op_subindex

			op_it_elemp = &it->list[pUTElem->op_it_offset];        // Now, point to the new Item Table row
		}

		// Set the desc_ref
		update_ptr->desc_ref.id = ITE_ITEM_ID(desc_it_elemp);
		update_ptr->desc_ref.type = ITE_ITEM_TYPE(desc_it_elemp);

		// Set the op_ref
		update_ptr->op_ref.op_ref_type = STANDARD_TYPE;
		update_ptr->op_ref.op_info.member = UTE_OP_SUBINDEX( pUTElem );
		update_ptr->op_ref.op_info.id = ITE_ITEM_ID(op_it_elemp);
		update_ptr->op_ref.op_info.type = ITE_ITEM_TYPE(op_it_elemp);

	}
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_clean_update_item_list
 *	ShortDesc: free the relation item id list
 *
 *	Description:
 *		ddi_clean_update_item_list() will free the relation item id list
 *		 malloced by ddi_get_update_items() call
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		update_list:		a pointer to the freed list
 *
 *	Returns: none
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
ddi_clean_update_item_list(OP_DESC_LIST   *update_list)
{

	if (!update_list) {
		return;
	}

	free((void *) update_list->list);

	update_list->count = 0;

	return;
}

/**************************************
*
*  adjust_command_index() - adjusts the value of the COMMAND_INDEX to be the value of the op_ref_info_list subindex
*
*  This is called by ddi_load_command_tbl_elem() if the op_ref_info_list subindex was modified in order to get the proper cmd_table_index.
*  See ddi_get_ext_ptoc_8()
*
**************************************/

static void adjust_command_index(
    ENV_INFO			*env_info,
    DDI_BLOCK_SPECIFIER *block_spec,
    OP_REF_INFO_LIST *op_ref_info_list,
    COMMAND_INDEX *pCommandIndex)
{
    // Look up this pCommandIndex VARIABLE definition to see if it is indexing a list or array in this op_ref_info_list

    DDI_ITEM_SPECIFIER itemSpec = { 0 };
    itemSpec.type = DDI_ITEM_ID;
    itemSpec.item.id = pCommandIndex->id;

    DDI_ATTR_REQUEST attr_request = VAR_TYPE_SIZE | VAR_INDEX_ITEM_ARRAY;
    DDI_GENERIC_ITEM generic_item = { 0 };

    int r_code = ddi_get_item(block_spec, &itemSpec, env_info, attr_request, &generic_item);

    if (r_code == DDS_SUCCESS)
    {
        ::FLAT_VAR *pFlatVar = (::FLAT_VAR *)generic_item.item;

        // Check to see if this index variable is an index for the list or value array that was passed in
        if (pFlatVar->type_size.type == INDEX)
        {
            for (int i = 0; i < op_ref_info_list->count; i++)
            {
                OP_REF_INFO *pOpRefInfo = &op_ref_info_list->list[i];

                if (pFlatVar->index_item_array == pOpRefInfo->id)	// If this index var is an index for the pOpRefInfo
                {
                    pCommandIndex->value = pOpRefInfo->member;		// then, set to the member that was passed in
                    break;  // Once we have found the list or array, we can exit.
                }
            }
        }

        delete pFlatVar;
    }
}


/*********************************************************************
 *
 *	Name: ddi_load_command_tbl_elem
 *	ShortDesc: Load a list of HART commands that either read or write the
 *             given DDL parameter in the DD binary generated by tokenizer 8
 *
 *	Description:
 *		ddi_load_command_tbl_elem
 *
 *	Inputs:
 *      op_ref_info_list: The OpRef for this command
 *
 *      bModifiedIndex: Indicates whether the index was modified during lookup, so it needs to be adjusted here
 *
 *		cmd_table_elem:	A pointer to the structure which specifies
 *						the block which contains the parameter for
 *						which the list of commands is being requested.
 *
 *      cmd_count:	Specifies no. of commands that either READ or WRITE commands
 *       
 *		subindex:	Specifies subindex for param spec
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *		
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Talreja Kapil
 *
 **********************************************************************/

// Load COMMAND_TBL_ELEM entries into the DDI_COMMAND_LIST
int ddi_load_command_tbl_elem( 
    ENV_INFO *env_info,
    DDI_BLOCK_SPECIFIER *block_spec,
    OP_REF_INFO_LIST *op_ref_info_list,
    bool bModifiedIndex,
    COMMAND_TBL_ELEM *cmd_table_elem,
    int cmd_count,
    SUBINDEX subindex,
    DDI_COMMAND_LIST *cmd_list)
{
	/* Malloc and load the table */

	cmd_list->list = (COMMAND_ELEM *) calloc( cmd_count, sizeof(COMMAND_ELEM) );
	if (cmd_list->list == NULL)
	{
		return DDI_MEMORY_ERROR;
	}
	cmd_list->count = cmd_count;


	int current_command_count = 0;
	for (int i = 0; i < cmd_count; i++)
	{
		// If we need to consider the subindex, skip if it doesn't match
		if (subindex != 0 && subindex != cmd_table_elem[i].subindex)
		{
			continue;
		}

		// Point to the current rows in the tables
		COMMAND_ELEM *pCommandElem = &cmd_list->list[current_command_count];
		COMMAND_TBL_ELEM *pTableElem = &cmd_table_elem[i];

		// Transfer the data
		pCommandElem->number = pTableElem->number;
		pCommandElem->transaction = pTableElem->transaction;
		pCommandElem->weight = pTableElem->weight;
		pCommandElem->count = pTableElem->count;

		if (pCommandElem->count == 0)
		{
			pCommandElem->index_list = NULL;
		}
		else
		{
			/*
			 * Malloc and copy the command index list if there is one
			 */
			pCommandElem->index_list = (COMMAND_INDEX *) calloc(
				pCommandElem->count, sizeof(COMMAND_INDEX));

			if (pCommandElem->index_list == NULL)
			{
				return DDI_MEMORY_ERROR;
			}

			COMMAND_INDEX  *index_ptr = pCommandElem->index_list;
			COMMAND_INDEX  *index_tbl_ptr = pTableElem->index_list;

			for (int j = 0; j < pCommandElem->count; j++)
			{
				index_ptr[j].id = index_tbl_ptr[j].id;
				index_ptr[j].value = index_tbl_ptr[j].value;

                // If this param_spec was modified to find the cmd_table_index, adjust the index_ptr->value to the right value.
                if (bModifiedIndex)
                {
                    adjust_command_index(env_info, block_spec, op_ref_info_list, &index_ptr[j]);
                }
            }
		}
		current_command_count++;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_ptoc_8
 *	ShortDesc: Gets a list of HART commands that either read or write the
 *             given DDL parameter in the DD binary generated by tokenizer 8
 *
 *	Description:
 *		ddi_get_ptoc_8
 *
 *	Inputs:
 *		block_spec:	A pointer to the structure which specifies
 *                  the block which contains the parameter for
 *                  which the list of commands is being requested.
 *
 *		param_spec:	A pointer to the structure which specifies the
 *					parameter for which the list of commands is being
 *					requested.
 *
 *      cmd_type:	Specifies that either READ or WRITE commands are
 *                  being requested.
 *
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

int
ddi_get_ptoc_8(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
unsigned int    cmd_type,
DDI_COMMAND_LIST *cmd_list)
{
	ASSERT_RET(block_spec && param_spec && cmd_list, DDI_INVALID_PARAM);
	ASSERT_RET(!cmd_list->list, DDI_INVALID_PARAM);

	BLOCK_HANDLE    block_handle = -1;	/* Handle for the block */
	BLK_TBL_ELEM   *bt_elem = NULL;	    /* Block Table Element pointer */
	int             bint_offset = -1;	/* Block Item Name Table offset */

	int rc = conv_block_and_param_spec(env_info, block_spec, param_spec,
							&block_handle, &bt_elem, &bint_offset);

	if (rc != DDS_SUCCESS)
	{
		return rc;
	}

	if (bint_offset == -1) {
		return DDI_NO_COMMANDS_FOR_PARAM;
	}

	ITEM_ID	  dd_id = 0;
	ITEM_TYPE item_type = 0;
	ITEM_ID blk_item_name = bt_elem->flat_block_dir.blk_item_name_tbl.list[bint_offset].blk_item_name;
	rc = tr_resolve_name_to_id_type(env_info, block_handle, bt_elem, blk_item_name, &dd_id, &item_type);

	if (rc != DDS_SUCCESS)
	{
		return rc;
	}

	PTOC_TBL_8_ELEM *ptoc_tbl_8_elem =
		(PTOC_TBL_8_ELEM *) bsearch(
			(char *)&dd_id,
			(char *)bt_elem->flat_block_dir.command_to_var_tbl.list,
			(unsigned int)bt_elem->flat_block_dir.command_to_var_tbl.count,
			sizeof(PTOC_TBL_8_ELEM),
			(CMP_FN_PTR)compare_ptoc_t8_elem);

	if (ptoc_tbl_8_elem == 0)
	{
		return DDI_NO_COMMANDS_FOR_PARAM;
	}

	COMMAND_TBL_ELEM *cmd_table_elem = NULL;
	int  cmd_count = 0;
	switch (cmd_type)
	{
	case DDI_READ_COMMAND:
		if (ptoc_tbl_8_elem->rd_count == 0 || ptoc_tbl_8_elem->rd_list == NULL)
		{
			return DDI_NO_READ_COMMANDS_FOR_PARAM;
		}
		cmd_table_elem = ptoc_tbl_8_elem->rd_list;
		cmd_count = ptoc_tbl_8_elem->rd_count;
		break;

	case DDI_WRITE_COMMAND:
		if (ptoc_tbl_8_elem->wr_count == 0 || ptoc_tbl_8_elem->wr_list == NULL)
		{
			return DDI_NO_WRITE_COMMANDS_FOR_PARAM;
		}
		cmd_table_elem = ptoc_tbl_8_elem->wr_list;
		cmd_count = ptoc_tbl_8_elem->wr_count;
		break;

	default:
		return DDI_INVALID_PARAM;
	}

    // No need to pass in OP_REF_INFO_LIST, since it will never be used.
	rc = ddi_load_command_tbl_elem( env_info, block_spec, nullptr, false, cmd_table_elem, cmd_count, param_spec->subindex, cmd_list);

	return rc;
}

/*********************************************************************
 *
 *	Name: ddi_get_ext_ptoc_8
 *	ShortDesc: Gets a list of HART commands that either read or write the
 *             given DDL parameter in the DD binary generated by ddi_get_ext_ptoc_8
 *
 *	Description:
 *		ddi_get_ext_ptoc_8
 *
 *	Inputs:
 *		block_spec:	A pointer to the structure which specifies
 *                  the block which contains the parameter for
 *                  which the list of commands is being requested.
 *
 *
 *      cmd_type:	Specifies that either READ or WRITE commands are
 *                  being requested.
 *
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Ying Xu
 *
 **********************************************************************/
bool RESOLVED_REFERENCE::operator==(const RESOLVED_REFERENCE &rhs)
{
	bool ret = false;

	if (	(size == rhs.size)		// They are the same if their contents is the same
		&&	(memcmp(pResolvedRef, rhs.pResolvedRef, (size*sizeof(*pResolvedRef)) ) == 0) )
	{
		ret = true;
	}

	return ret;
}


int
ddi_get_ext_ptoc_8(
	ENV_INFO			*env_info,
	DDI_BLOCK_SPECIFIER *block_spec,
	OP_REF_INFO_LIST	*op_ref_info_list,
	bool				bItemIdOnly,
	unsigned int		cmd_type,
	DDI_COMMAND_LIST	*cmd_list)
{
	ASSERT_RET(block_spec && cmd_list, DDI_INVALID_PARAM);
	ASSERT_RET(!cmd_list->list, DDI_INVALID_PARAM);

	BLOCK_HANDLE    block_handle = -1;	/* Handle for the block */
	BLK_TBL_ELEM   *bt_elem = NULL;	    /* Block Table Element pointer */

	int rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	OP_REF_INFO_LIST MyOpRefInfoList = {0};

	// Make my own copy so I can simplify, if needed
	Assign_OP_REF_INFO_LIST(MyOpRefInfoList, *op_ref_info_list);


	RESLV_REF_TO_CMD_TBL *pReslvTbl = &bt_elem->flat_block_dir.reslv_ref_tbl;
	int iTblCnt = bt_elem->flat_block_dir.reslv_ref_tbl.count;

	RESLV_TBL_ELEM *reslv_tbl_elem = nullptr;	// This will point to the result of our search

	// Create a ReslvRef, but don't fill it yet
	RESOLVED_REFERENCE ReslvRef = {0};

	// Determine how many RESOLVED_REFERENCE entries there will be
	if (bItemIdOnly || (MyOpRefInfoList.list[0].type == VARIABLE_ITYPE) )
	{
		ReslvRef.size = 1;
	}
	else
	{
		ReslvRef.size = MyOpRefInfoList.count + 1;
	}

	ReslvRef.pResolvedRef = (unsigned long*)calloc(ReslvRef.size, sizeof(unsigned long));
	
	// If we need to simplify the Resolved Ref, start at the beginning.
	int iOpRefIdx = -1;
    bool bModifiedIndex = false;        // Assume that we don't need to modify the index during command table lookup

	do
	{
		// Fill the RESOLVED_REFERENCE from my OP_REF_INFO_LIST
		ReslvRef.pResolvedRef[0] = MyOpRefInfoList.list[0].id;

		for(int j=1; j < ReslvRef.size; j++)
		{
			ReslvRef.pResolvedRef[j] = MyOpRefInfoList.list[j-1].member;
		}

		// Now see if we can find it.
		for(int i=0; i < iTblCnt; i++)	// Do a linear search for the first match
		{
			if (pReslvTbl->list[i].element_ref == ReslvRef)	// Compare the RESOLVED_REFERENCE keys
			{
				reslv_tbl_elem = &pReslvTbl->list[i];	// We found the row!
				break;
			}
		}

		if (reslv_tbl_elem != nullptr)	// We found it!
		{
			break;
		}

		// Didn't find it. Try looking for a List index and zero it
		for ( iOpRefIdx++; iOpRefIdx < MyOpRefInfoList.count; iOpRefIdx++)
		{
            if (    (MyOpRefInfoList.list[iOpRefIdx].type == LIST_ITYPE)
                ||  (MyOpRefInfoList.list[iOpRefIdx].type == ARRAY_ITYPE) )
            {
				if (MyOpRefInfoList.list[iOpRefIdx].member != 0)	// If we aren't looking
				{													// for the 0th member,
					MyOpRefInfoList.list[iOpRefIdx].member = 0;		// reset to do so and retry.
                    bModifiedIndex = true;                          // We had to modify the index, so we need to adjust later
                    break;
				}
			}
		}

	} while (iOpRefIdx < MyOpRefInfoList.count);

	free (ReslvRef.pResolvedRef);	// Don't leak this
	ReslvRef.pResolvedRef = nullptr;

	free (MyOpRefInfoList.list);
	MyOpRefInfoList.list = nullptr;

	if (reslv_tbl_elem == nullptr)		// None found
	{
		return DDI_NO_COMMANDS_FOR_PARAM;
	}

	COMMAND_TBL_ELEM *cmd_table_elem = nullptr;
	int  cmd_count = 0;

	switch (cmd_type)
	{
	case DDI_READ_COMMAND:
		if (reslv_tbl_elem->read_count == 0 || reslv_tbl_elem->reslv_read_table_list == NULL)
		{
			return DDI_NO_READ_COMMANDS_FOR_PARAM;
		}
		cmd_table_elem = reslv_tbl_elem->reslv_read_table_list;
		cmd_count = reslv_tbl_elem->read_count;
		break;

	case DDI_WRITE_COMMAND:
		if (reslv_tbl_elem->write_count == 0 || reslv_tbl_elem->reslv_write_table_list == NULL)
		{
			return DDI_NO_WRITE_COMMANDS_FOR_PARAM;
		}
		cmd_table_elem = reslv_tbl_elem->reslv_write_table_list;
		cmd_count = reslv_tbl_elem->write_count;
		break;

	default:
		return DDI_INVALID_PARAM;
	}

	rc = ddi_load_command_tbl_elem( env_info, block_spec, op_ref_info_list, bModifiedIndex, cmd_table_elem, cmd_count, 0, cmd_list);

	return rc;
}



/*********************************************************************
 *
 *	Name: ddi_get_ptoc_5_and_6
 *	ShortDesc: Gets a list of HART commands that either read or write the
 *             given DDL parameter in the DD binary generated by tokenizer 5 or 6
 *
 *	Description:
 *		ddi_get_ptoc_5_and_6
 *
 *	Inputs:
 *		block_spec:	A pointer to the structure which specifies
 *                  the block which contains the parameter for
 *                  which the list of commands is being requested.
 *
 *		param_spec:	A pointer to the structure which specifies the
 *					parameter for which the list of commands is being
 *					requested.
 *
 *      cmd_type:	Specifies that either READ or WRITE commands are
 *                  being requested.
 *
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

int
ddi_get_ptoc_5_and_6(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
unsigned int    cmd_type,
DDI_COMMAND_LIST *cmd_list)
{
	int             rc;	            /* Return code */
	int             inc;	        /* Incrementer */
	int             inc2;	        /* Incrementer number two */
	int				temp_cmd_table_count;
	BLOCK_HANDLE    block_handle;	/* Handle for the block */
	BLK_TBL_ELEM   *bt_elem = NULL;	/* Block Table Element pointer */
	int             bint_offset;	/* Block Item Name Table offset */
	COMMAND_TBL_ELEM *cmd_table_elem; /* Command Table Element
						 * pointer */
	int             cmd_table_offset; /* Command Table offset */
	int             cmd_table_count;  /* Command Table count */

	COMMAND_ELEM   *cmd_ptr;        /* DDI Command Element pointer */
	COMMAND_INDEX  *index_ptr;	    /* Command index pointer */
	COMMAND_INDEX  *index_tbl_ptr;	/* Command index pointer */



	ASSERT_RET(block_spec && param_spec && cmd_list, DDI_INVALID_PARAM);

	ASSERT_RET(!cmd_list->list, DDI_INVALID_PARAM);



	rc = conv_block_and_param_spec(env_info, block_spec, param_spec,
							&block_handle, &bt_elem, &bint_offset);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	if (bint_offset == -1) {
		return DDI_NO_COMMANDS_FOR_PARAM;
	}

	/*
	 * Switch on type of commands (read or write) requested and then
	 * convert the block item name table offset and the block table element
	 * to a command table offset.
	 */


	switch (cmd_type) {

	case DDI_READ_COMMAND:

		cmd_table_offset = BINTE_RCT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
		if (cmd_table_offset == -1) {

			return DDI_NO_READ_COMMANDS_FOR_PARAM;
		}
		cmd_table_count = BINTE_RCT_COUNT(BINTE(BTE_BINT(bt_elem), bint_offset));
		break;

	case DDI_WRITE_COMMAND:
		cmd_table_offset = BINTE_WCT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
		if (cmd_table_offset == -1) {

			return DDI_NO_WRITE_COMMANDS_FOR_PARAM;
		}
		cmd_table_count = BINTE_WCT_COUNT(BINTE(BTE_BINT(bt_elem), bint_offset));
		break;

	default:
		return DDI_INVALID_PARAM;

	}

	/*
	 * Get the command table element pointer
	 */

	cmd_table_elem = CTE(BTE_CT(bt_elem), cmd_table_offset);


	/*
	 * If the request is for an array element or record member get the real
	 * count and move the cmd_table_elem pointer
	 */

	switch (param_spec->type) {
	
	case DDI_PS_ITEM_ID:
	case DDI_PS_OP_INDEX:
	case DDI_PS_PARAM_NAME:
	case DDI_PS_PARAM_OFFSET:
			break ;

	case DDI_PS_ITEM_ID_SI:
	case DDI_PS_OP_INDEX_SI:
	case DDI_PS_PARAM_NAME_SI:
	case DDI_PS_PARAM_OFFSET_SI:
	case DDI_PS_CHARACTERISTICS_SI:

			/*
			 * Match the requested subindex
			 */

			inc = 0;
			temp_cmd_table_count = cmd_table_count;
			while  (inc < temp_cmd_table_count) {

				if (param_spec->subindex == cmd_table_elem->subindex) {

					cmd_table_count = 0;

					while (inc < temp_cmd_table_count && param_spec->subindex == cmd_table_elem->subindex) {

						++inc;
						++cmd_table_count;
						++cmd_table_elem;
					}
					goto new_count_success;
				}

				++inc;
			}

			return DDI_TAB_BAD_SUBINDEX;

new_count_success:

			/*
			 * set the element pointer back to the beginning of the list
			 */

			cmd_table_elem = cmd_table_elem - cmd_table_count;
			break;

	default:
			return DDI_INVALID_PARAM ;
	}


	/* Malloc and load the table */


	cmd_ptr = (COMMAND_ELEM *) malloc((size_t) cmd_table_count * sizeof(COMMAND_ELEM));
	if (cmd_ptr == NULL) {
		return DDI_MEMORY_ERROR;
	}

	cmd_list->list = cmd_ptr;
	cmd_list->count = cmd_table_count;


	for (inc = 0; inc < cmd_table_count; inc++, cmd_ptr++, cmd_table_elem++) {


		cmd_ptr->number = cmd_table_elem->number;
		cmd_ptr->transaction = cmd_table_elem->transaction;
		cmd_ptr->weight = cmd_table_elem->weight;
		cmd_ptr->count = cmd_table_elem->count;

		if (cmd_ptr->count) {

			/*
			 * Malloc and copy the command index list if there is
			 * one
			 */

			cmd_ptr->index_list = (COMMAND_INDEX *) malloc((size_t)
				cmd_ptr->count * sizeof(COMMAND_INDEX));
			if (cmd_ptr->index_list == NULL)
			{
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"ddi_get_ptoc: failed to malloc memory");
			}

			index_ptr = cmd_ptr->index_list;
			index_tbl_ptr = cmd_table_elem->index_list;

			for (inc2 = 0; inc2 < cmd_ptr->count; inc2++, index_ptr++, index_tbl_ptr++) {

				index_ptr->id = index_tbl_ptr->id;
				index_ptr->value = index_tbl_ptr->value;
			}
		}
		else {

			cmd_ptr->index_list = NULL;
		}
	}


	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_ptoc
 *	ShortDesc: Gets a list of HART commands that either read or write the
 *             given DDL parameter
 *
 *	Description:
 *		ddi_get_ptoc
 *
 *	Inputs:
 *		block_spec:	A pointer to the structure which specifies
 *                  the block which contains the parameter for
 *                  which the list of commands is being requested.
 *
 *		param_spec:	A pointer to the structure which specifies the
 *					parameter for which the list of commands is being
 *					requested.
 *
 *      cmd_type:	Specifies that either READ or WRITE commands are
 *                  being requested.
 *
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

int
ddi_get_ptoc(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
unsigned int    cmd_type,
DDI_COMMAND_LIST *cmd_list)
{
	int rc = DDS_SUCCESS;
	
	DEVICE_TYPE_HANDLE dth = -1;
	ROD_HANDLE rod_handle = 0;
	BLOCK_HANDLE bh = -1;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_spec(env_info, block_spec, &bh);
	if (rc  == DDS_SUCCESS)
	{
		rc = pConnMgr->get_abt_adtt_offset(bh, &dth);
	}

	if (rc  == DDS_SUCCESS)
	{
		rc = g_DeviceTypeMgr.get_adtt_dd_handle (dth, &rod_handle);
	}

	if (rc  == DDS_SUCCESS)
	{
		int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rod_handle);

		if (tok_major_rev == 8)
		{
			rc = ddi_get_ptoc_8(env_info, block_spec, param_spec, cmd_type, cmd_list);

			//Start of workaround for device_status commands being removed from ptoc table.
			if( rc == DDI_NO_READ_COMMANDS_FOR_PARAM )
			{
				DDI_PARAM_INFO paramInfo = {0};

				int rs = ddi_get_param_info( env_info, block_spec, param_spec, &paramInfo );
				if( rs == DDS_SUCCESS )
				{
					if( (paramInfo.item_id == SYM_device_status_ID) || (paramInfo.item_id == SYM_response_code_ID) || (paramInfo.item_id == SYM_comm_status_ID) )
					{
						DDI_PARAM_SPECIFIER param_spec2 = {0};
						if( cmd_list->list )
						{
							ddi_clean_command_list(cmd_list);
						}
						param_spec2.type = DDI_PS_ITEM_ID;
						param_spec2.item.id = SYM_manufacturer_id_ID;
						param_spec2.subindex = 0;
						rc = ddi_get_ptoc_8(env_info, block_spec, &param_spec2, cmd_type, cmd_list);
					}
				}
			}
			//End of workaround for device_status commands being removed from ptoc table.
		}
		else if (tok_major_rev == 5 || tok_major_rev == 6)
		{
			rc = ddi_get_ptoc_5_and_6(env_info, block_spec, param_spec, cmd_type, cmd_list);
		}
		else
		{
			rc = DDS_WRONG_DDOD_REVISION;
			ASSERT_DBG(false);
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_clean_command_list
 *	ShortDesc: free the command list
 *
 *	Description:
 *		ddi_clean_command_list() will free the command list
 *		 malloced by ddi_get_ptoc() call
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		ddi_command_list:		a pointer to the freed list
 *
 *	Returns: none
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
ddi_clean_command_list(DDI_COMMAND_LIST *ddi_command_list)
{
	int             inc;
	COMMAND_ELEM   *temp_element;

	if (!ddi_command_list) {
		return;
	}

	if ( !ddi_command_list->list ) {
		ddi_command_list->count = 0;
		return;
	}

	/*
	 * Before freeing the command table, free the index element lists.
	 */

	temp_element = ddi_command_list->list;

	for (inc = 0; inc < ddi_command_list->count; inc++, temp_element++) {

		if (temp_element->index_list) {

			free((void *) temp_element->index_list);
		}
	}


	free((void *) ddi_command_list->list);
	ddi_command_list->count = 0;


	return;
}


/*********************************************************************
 *
 *	Name: ddi_get_var_type
 *	ShortDesc: get the variable type and size
 *
 *	Description:
 *		ddi_get_var_type will return the type and size of
 *		the given variable
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier structure
 *		param_spec:		a pointer to the parameter specifier structure
 *
 *	Outputs:
 *		type_size:		the loaded type/size structure
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
ddi_get_var_type(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
TYPE_SIZE           *type_size)
{
	BLK_TBL_ELEM   *bt_elem;      /* Block Table element pointer */
	BLOCK_HANDLE    block_handle; /* Block Handle */
	int				param_offset; /* Param Table offset */
	int				bint_offset;  /* Block Item Name Table offset */
	ulong			rec_mem_count;/* Temp count for record member count */
	int				arr_ele_count;/* Temp count for array element count */
	int				offset;       /* Temp count offset */
	int             rc;	          /* return code */


	ASSERT_RET(block_spec && param_spec && type_size, DDI_INVALID_PARAM);

	// If item id is 151 i.e. communication paramater "device_status" or 152 i.e. "comm_status"
	if( (param_spec->item.id == SYM_device_status_ID) || (param_spec->item.id == SYM_comm_status_ID) )
	{
		type_size->type = BIT_ENUMERATED; 
		type_size->size = 1;

		return DDS_SUCCESS;
	}
	// If item id is 150 i.e. "response_code".
	else if(param_spec->item.id == SYM_response_code_ID)
	{
		type_size->type = ENUMERATED;
		type_size->size = 1;

		return DDS_SUCCESS;
	}

	rc = conv_block_and_param_spec(env_info, block_spec, param_spec,
								&block_handle, &bt_elem, &bint_offset);
	if (rc != DDS_SUCCESS) {

		return rc;
	}


	/*
	 * Handle characteristics individually
     */

	if (param_spec->type == DDI_PS_CHARACTERISTICS_SI) {

		rec_mem_count = CMT_COUNT(BTE_CMT(bt_elem));

		if ((param_spec->subindex >rec_mem_count) || (param_spec->subindex < 1)) {

			return DDI_TAB_BAD_SUBINDEX;
		}

		type_size->type = (ushort) CMTE_CM_TYPE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));
		type_size->size = (ushort) CMTE_CM_SIZE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));

		return DDS_SUCCESS;
	}



	param_offset = BINTE_PT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
	if (param_offset == -1) {

		return DDI_TAB_BAD_PARAM_OFFSET;
	}

	rec_mem_count  = PTE_PM_COUNT(PTE(BTE_PT(bt_elem), param_offset));
	arr_ele_count  = PTE_PE_MX_COUNT(PTE(BTE_PT(bt_elem), param_offset));


	switch (param_spec->type) {

	/*
	 * get type and size of parameter without subindex
	 */

	case DDI_PS_ITEM_ID:
	case DDI_PS_OP_INDEX:
	case DDI_PS_PARAM_NAME:
	case DDI_PS_PARAM_OFFSET:

		if((rec_mem_count) || (arr_ele_count)) {

			return DDI_INVALID_REQUEST_TYPE;		
		}

		type_size->type = (ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
		type_size->size = (ushort) PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
		break;

	/*
	 * get type and size of parameter with subindex
	 */

	case DDI_PS_ITEM_ID_SI:
	case DDI_PS_OP_INDEX_SI:
	case DDI_PS_PARAM_NAME_SI:
	case DDI_PS_PARAM_OFFSET_SI:

		if (rec_mem_count) {

			offset = PTE_PMT_OFFSET(PTE(BTE_PT(bt_elem), param_offset));

			if ((param_spec->subindex > rec_mem_count) || (param_spec->subindex < 1)) {

				return DDI_TAB_BAD_SUBINDEX;
			}

			type_size->type = (ushort) PMTE_PM_TYPE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));
			type_size->size = (ushort) PMTE_PM_SIZE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));
		}
		else {


			if (arr_ele_count) {

				type_size->type = (ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
				type_size->size = (ushort) PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
			}
			else {

				return DDI_INVALID_REQUEST_TYPE;
			}
		}
		break;

		default:
			rc = DDI_INVALID_REQUEST_TYPE;
	}


	return rc;
}



/*********************************************************************
 *
 *	Name: ddi_get_item_id
 *	ShortDesc: get the item id
 *
 *	Description:
 *		ddi_get_item_id will return the item id for the parameter
 *		name, or characteristic, or parameter list with member name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		item_id_req:	a pointer to the item id request structure
 *
 *	Outputs:
 *		item_id:		the item_id of the requested item
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

//not used in legacy engine
int
ddi_get_item_id(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_ITEM_ID_REQUEST *item_id_req,
ITEM_ID        *item_id)
{

	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	int             rc;	/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */
	ITEM_TYPE       item_type;	/* Throw away item type */

	ASSERT_RET(block_spec && item_id_req && item_id, DDI_INVALID_PARAM);
	
	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}


	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}


	switch (item_id_req->type) {


	case DDI_ITEM_ID_PARAM_NAME:
		rc = tr_resolve_name_to_id_type(env_info, block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id, &item_type);
		break;


	case DDI_ITEM_ID_PARAM_LIST_MEMBER:
		rc = tr_resolve_lname_pname_to_ddid(env_info, block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id_req->member_name,
			item_id);
		break;


	case DDI_ITEM_ID_PARAM_LIST_ID_MEMBER:
		rc = tr_resolve_lid_pname_to_id_type(env_info, block_handle, bt_elem,
			item_id_req->param_name_or_id, item_id_req->member_name,
			item_id, &item_type);
		break;


	case DDI_ITEM_ID_CHARACTERISTIC:
		rc = tr_resolve_char_rec_to_ddid(env_info, block_handle, bt_elem, item_id);
		break;


	default:
		rc = DDI_INVALID_REQUEST_TYPE;

	}

	return rc;

}


/*********************************************************************
 *
 *	Name: ddi_get_subindex
 *	ShortDesc: get the subindex of the member of the item
 *
 *	Description:
 *		ddi_get_subindex() will return the subindex of the
 *		item based on the inputs of item_id and member name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		item_id:		the item_id of the item to which the member belongs
 *		mem_name:		the item_id of the member
 *
 *	Outputs:
 *		subindex:		the subindex of the member
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
ddi_get_subindex(
ENV_INFO *env_info,
DDI_BLOCK_SPECIFIER *block_spec,
ITEM_ID         item_id,
ITEM_ID         mem_name,
SUBINDEX       *subindex)
{

	BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	int             rc;	/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */

	ASSERT_RET(block_spec && item_id && mem_name && subindex, DDI_INVALID_PARAM);

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = tr_resolve_ddid_mem_to_si(bt_elem, item_id, mem_name, subindex);

	return rc;

}

/*********************************************************************
 *
 *	Name: param_spec_to_item_spec
 *
 *	ShortDesc: convert a DDI_PARAM_SPECIFIER into a DDI_ITEM_SPECIFIER
 *
 *	Description: 
 *              param_spec_to_item_spec() will convert a DDI_PARAM_SPECIFIER
 *              structure into a DDI_ITEM_SPECIFIER.  This is safe since a parameter
 *              is an item.
 *
 *	Inputs:
 *		param_spec:    a pointer to the param specifier
 *
 *	Outputs:
 *              item_spec:     a pointer to the item specifier        
 *
 *	Returns:
 *		returns DDI_INVALID_REQUEST_TYPE or DDS_SUCCESS
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
param_spec_to_item_spec(
DDI_PARAM_SPECIFIER *param_spec,
DDI_ITEM_SPECIFIER *item_spec)
{

  item_spec->subindex = param_spec->subindex;

  switch(param_spec->type) {
  case DDI_PS_ITEM_ID:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID;
    break;
  case DDI_PS_PARAM_NAME:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME;
    break;
  case DDI_PS_PARAM_OFFSET:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM;
    break;
  case DDI_PS_OP_INDEX:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX;
    break;
  case DDI_PS_ITEM_ID_SI:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID_SI;
    break;
  case DDI_PS_OP_INDEX_SI:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX_SI;
    break;
  case DDI_PS_PARAM_NAME_SI:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME_SI;
    break;
  case DDI_PS_PARAM_OFFSET_SI:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM_SI;
    break;
  default:
    return DDI_INVALID_REQUEST_TYPE;
    
  }
  return DDS_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddi_get_param_info
 *	ShortDesc: Get information about a parameter
 *
 *	Description:
 *		ddi_get_param_info() accepts a DDI_PARAM_SPECIFIER
 *		and fills in a PARAM_INFO structure with the parameter's
 *              information.
 *     
 *              The idea behind this function is that a user knows one piece
 *              of information about a parameter ( either the item id, 
 *              param name, param offset or object index ) and the user
 *              is interested in either some or all of the other information        
 *              about the parameter. 
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		param_spec:             a pointer to the param specifier
 *
 *	Outputs:
 *		param_info:		a pointer to the parameter information
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
ddi_get_param_info(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
DDI_PARAM_INFO      *param_info)
{
  int                rc;                 /* return code */
  BLK_TBL_ELEM       *bt_elem;           /* Block Table element pointer */
  BLOCK_HANDLE       block_handle;       /* Block Handle */
  int                bint_offset;       /* Block item Name Table Offset */
  ROD_HANDLE          rod_handle = 0;          /* device description handle */
  BLK_ITEM_NAME_TBL_ELEM *bint_elem;     /* blk item name tb elem */
  FLAT_DEVICE_DIR       *flat_device_dir ;
  ITEM_TBL              *it;            /* item table */
  ITEM_TBL_ELEM         *it_elem;       /* item table element */

  ASSERT_RET(block_spec && param_spec && param_info, DDI_INVALID_PARAM);
  
  IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
  CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

  rc = conv_block_spec(env_info, block_spec, &block_handle);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  rc = block_handle_to_bt_elem(env_info,block_handle, &bt_elem);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  /*
   * Get the Block Item Name Table Offset of the Parameter
   */

  rc = convert_param_spec_to_bint_offset(env_info, block_handle,param_spec, bt_elem,&bint_offset);
  if (rc != DDS_SUCCESS) {
    
    if (rc == DDI_TAB_NO_BLOCK) {
      
      /*
       * If convert_param_spec_to_bint_offset() fails because of no block,
       * request a load for the dd
       */
      
      rc = pConnMgr->ds_dd_block_load(env_info, block_handle, &rod_handle, 1);
      if (!rc) {
        
        rc = convert_param_spec_to_bint_offset(env_info, block_handle,param_spec, bt_elem,&bint_offset);
        if (rc != DDS_SUCCESS) {
          
          return rc;
        }
      }
      else {
        
        return DDI_BAD_DD_BLOCK_LOAD;
      }
    }
    else {
      
      return rc;
    }
  }
  
  bint_elem = BINTE(BTE_BINT(bt_elem),bint_offset);

  /* 
   * Fill in param offset
   */
  
  param_info->param_offset = BINTE_PT_OFFSET(bint_elem);

  /* 
   * Fill in item id
   */
  
  rc = pConnMgr->get_abt_dd_dev_tbls(block_handle,(void **)&flat_device_dir) ;
  if (rc != SUCCESS) {
    return(rc) ;
  }
  it = &flat_device_dir->item_tbl;
  it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));
  param_info->item_id = ITE_ITEM_ID(it_elem);

  /* 
   * Fill in param name 
   */

  param_info->param_name = BINTE_BLK_ITEM_NAME(bint_elem);

  /* 
   * Fill in op index  
   */

  rc = tr_param_offset_to_op_index(env_info, block_handle,
                BINTE_PT_OFFSET(bint_elem), &param_info->op_index);

  /* 
   * Fill in item type
   */
  
  param_info->item_type = ITE_ITEM_TYPE(it_elem);

  return rc;
}


