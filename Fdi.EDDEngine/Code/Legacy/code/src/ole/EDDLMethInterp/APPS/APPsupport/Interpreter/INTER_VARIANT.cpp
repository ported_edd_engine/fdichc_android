/*************************************************************************************************
 *
 * INTER_VARIENT.cpp
 * 
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002-2008, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The self contained varient used in the Method Execution Engine
 *
 * Component History: 
 *	
 */


#include <stdafx.h>
#include "nsEDDEngine/Ddldefs.h"
#include "INTER_VARIANT.h"
#include "INTER_SAFEARRAY.h"
#include "NAN_DEF.h"
#include "varientTag.h"
#include "stdlib.h"
/** 
*	stevev feb08 - rework entire class 
*  all strings are WIDE unicode chars: RUL_DD_STRING is NOT char but wchar_t now
*  none of the 5 string types may be cast to a const eg: int k = (int)DD_STRING-type is k = 0;
*  operator= NEVER changes the lvalue's type UNLESS the lvalue's type is RUL_NULL 
*  The string-type operator= SAFEARRAY operation will only work if the safearray type matches
*		the string type.  eg it is a safearray of char and the lvalue is a charptr
*	Use GetString() if you want a string conversion.
*  This feb workover HAS NOT TOUCHED the operator methods (eg <=, == etc)
*  This feb workover HAS NOT TOUCHED the safearray class AT ALL!
*********************************************************************
*  WHS EP reworked / refactored the whole thing june of 2008.
*  Indications are that safearray was not changed in this workover.
*  All operators are reportedly functional for all types. 
**/

const char vtype_strings[VAR_TYPE_STRING_CNT][VAR_TYPE_STRING_MXLEN] = { VARIANT_TYPE_STRINGS };


struct INT_RANK{ int rank; bool is_unsigned; };
#define isSigned( a )    ( ! a.is_unsigned )
#define isUNSigned( b )  ( a.is_unsigned )

/* promotes the two types according to C rules, returns type of result */
static VARIANT_TYPE promote( INTER_VARIANT& inOne, INTER_VARIANT& outOne, 
							 INTER_VARIANT& inTwo, INTER_VARIANT& outTwo) 
{
	VARIANT_TYPE  retType = RUL_NULL;
	INTER_VARIANT local;
	outOne.Clear(); outOne = inOne;
	outTwo.Clear(); outTwo = inTwo;
	if ( (! inOne.isNumeric()) || (! inTwo.isNumeric()) )
	{	return retType; // an error 
	}

	INT_RANK oneRank, twoRank, oneCnvt={0,false},twoCnvt= {0,false};
	
/*	First, if the corresponding real type of either operand is long double, the other
operand is converted, without change of type domain, to a type whose
corresponding real type is long double.   */
	/*
	--- We don't support long double at this time 
	*/ 
/*  Otherwise, if the corresponding real type of either operand is double, the other
operand is converted, to a double. */
	if ( outOne.GetVarType() == RUL_DOUBLE && outTwo.GetVarType() != RUL_DOUBLE )
	{	local.Clear(); local = (double)outTwo;  outTwo.Clear(); outTwo = local;
		retType = RUL_DOUBLE;
	}
	else if ( outOne.GetVarType() != RUL_DOUBLE && outTwo.GetVarType() == RUL_DOUBLE )
	{	local.Clear(); local = (double)outOne;  outOne.Clear(); outOne = local;
		retType = RUL_DOUBLE;
	}
	else if ( outOne.GetVarType() == RUL_DOUBLE && outTwo.GetVarType() == RUL_DOUBLE )
	{	local.Clear(); local = (double)outOne;  outOne.Clear(); outOne = local;
		retType = RUL_DOUBLE;
	}

/*  Otherwise, if the corresponding real type of either operand is float, the other
operand is converted to a float.  */
	else if ( outOne.GetVarType() == RUL_FLOAT && outTwo.GetVarType() != RUL_FLOAT )
	{	local.Clear(); local = (float)outTwo;  outTwo.Clear(); outTwo = local;
		retType = RUL_FLOAT;
	}
	else if ( outOne.GetVarType() != RUL_FLOAT && outTwo.GetVarType() == RUL_FLOAT )
	{	local.Clear(); local = (float)outOne;  outOne.Clear(); outOne = local;
		retType = RUL_FLOAT;
	}
	else if ( outOne.GetVarType() == RUL_FLOAT && outTwo.GetVarType() == RUL_FLOAT )
	{	local.Clear(); local = outOne;  outOne.Clear(); outOne = local;
		retType = RUL_FLOAT;
	}

/*  Otherwise, the integer promotions are performed on both operands. Then the
following rules are applied to the promoted operands:   */
	if ( retType == RUL_NULL ) // not a float type...
	{
		switch (outOne.GetVarType())
		{
		case	RUL_BOOL:			oneRank.is_unsigned = false; oneRank.rank = 1;  break;
		case	RUL_CHAR:			oneRank.is_unsigned = false; oneRank.rank = 2;  break;
		case	RUL_UNSIGNED_CHAR:	oneRank.is_unsigned =  true; oneRank.rank = 2;  break;
		case	RUL_SHORT:			oneRank.is_unsigned = false; oneRank.rank = 3;  break;
		case	RUL_USHORT:			oneRank.is_unsigned =  true; oneRank.rank = 3;  break;
		case	RUL_INT:			oneRank.is_unsigned = false; oneRank.rank = 4;  break;
		case	RUL_UINT:			oneRank.is_unsigned =  true; oneRank.rank = 4;  break;
		case	RUL_LONGLONG:		oneRank.is_unsigned = false; oneRank.rank = 5;  break;
		case	RUL_ULONGLONG:		oneRank.is_unsigned =  true; oneRank.rank = 5;  break;
		//case	RUL_FLOAT:
		//case	RUL_DOUBLE:
		default:	return RUL_NULL; /* error */ break;
		}// end switch
		switch (outTwo.GetVarType())
		{
		case	RUL_BOOL:			twoRank.is_unsigned = false; twoRank.rank = 1;  break;
		case	RUL_CHAR:			twoRank.is_unsigned = false; twoRank.rank = 2;  break;
		case	RUL_UNSIGNED_CHAR:	twoRank.is_unsigned =  true; twoRank.rank = 2;  break;
		case	RUL_SHORT:			twoRank.is_unsigned = false; twoRank.rank = 3;  break;
		case	RUL_USHORT:			twoRank.is_unsigned =  true; twoRank.rank = 3;  break;
		case	RUL_INT:			twoRank.is_unsigned = false; twoRank.rank = 4;  break;
		case	RUL_UINT:			twoRank.is_unsigned =  true; twoRank.rank = 4;  break;
		case	RUL_LONGLONG:		twoRank.is_unsigned = false; twoRank.rank = 5;  break;
		case	RUL_ULONGLONG:		twoRank.is_unsigned =  true; twoRank.rank = 5;  break;
		//case	RUL_FLOAT:
		//case	RUL_DOUBLE:
		default:	return RUL_NULL; /* error */ break;	
		}// end switch

	/* If both operands have the same type, then no further conversion is needed. */
		if ( outOne.GetVarType() == outTwo.GetVarType() )
		{	return outTwo.GetVarType();// done
		}

	/* Otherwise, if both operands have signed integer types or both have unsigned
	integer types, the operand with the type of lesser integer conversion rank is
	converted to the type of the operand with greater rank. */
		if ( (( oneRank.is_unsigned) && ( twoRank.is_unsigned)) || 
			 ((!oneRank.is_unsigned) && (!twoRank.is_unsigned))  )
		{// lower to higher
			oneCnvt,twoCnvt;
			if (oneRank.rank > twoRank.rank)
			{
				twoCnvt = oneRank;// other stays empty
			}
			else
			{
				oneCnvt = twoRank;// other stays empty
			}
		}
		else // one is signed, the other is unsigned
	/* Otherwise, if the operand that has unsigned integer type has rank greater or
	equal to the rank of the type of the other operand, then the operand with
	signed integer type is converted to the type of the operand with unsigned
	integer type.*/
		if ( oneRank.is_unsigned && oneRank.rank >= twoRank.rank )
		{// two converted to one's type
			twoCnvt = oneRank;// other stays empty
		}
		else
		if ( twoRank.is_unsigned && twoRank.rank >= oneRank.rank )
		{// one converted to two's type
			oneCnvt = twoRank;// other stays empty
		}
		else
	/* Otherwise, if the type of the operand with signed integer type can represent
	all of the values of the type of the operand with unsigned integer type, then
	the operand with unsigned integer type is converted to the type of the
	operand with signed integer type.  */
		if ( (!oneRank.is_unsigned) && oneRank.rank > twoRank.rank)
		{//two converted to one's type
			twoCnvt = oneRank;// other stays empty
		}
		else
		if ( (!twoRank.is_unsigned) && twoRank.rank > oneRank.rank )
		{// one converted to two's type
			oneCnvt = twoRank;// other stays empty
		}
		else
	/* Otherwise, both operands are converted to the unsigned integer type
	corresponding to the type of the operand with signed integer type.	*/
		if ( oneRank.is_unsigned )// two is SIGNED
		{//both to twoRank.rank and unsigned
			twoCnvt = oneCnvt   = twoRank;// other stays empty
			twoCnvt.is_unsigned = true;
			oneCnvt.is_unsigned = true;
		}
		else // one is SIGNED
		{//both to oneRank.rank and unsigned
			twoCnvt = oneCnvt   = oneRank;// other stays empty
			twoCnvt.is_unsigned = true;
			oneCnvt.is_unsigned = true;
		}

		// do the conversion(s)
		if (oneCnvt.rank > 0 )
		{// convert oneOut to oneCnvt type
			switch (oneCnvt.rank)
			{
			case 1:	// bool
				{	local.Clear(); local = (bool)outOne;  
					outOne.Clear(); outOne = local;
					retType = RUL_BOOL;
				}
				break;
			case 2:	// char
				{
					if (oneCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned char)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_UNSIGNED_CHAR;
					}
					else//signed
					{	local.Clear();  local = (char)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_CHAR;
					}
				}
				break;
			case 3:	// short
				{
					if (oneCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned short)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_USHORT;
					}
					else//signed
					{	local.Clear();  local = (short)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_SHORT;
					}
				}
				break;
			case 4:	// int
				{
					if (oneCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned int)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_UINT;
					}
					else//signed
					{	local.Clear();  local = (int)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_INT;
					}
				}
				break;
			case 5:	// long long
				{
					if (oneCnvt.is_unsigned)
                    {	local.Clear();  local = (_UINT64)outOne;
						outOne.Clear(); outOne = local;
						retType = RUL_ULONGLONG;
					}
					else//signed
					{	local.Clear();  local = (__int64)outOne;  
						outOne.Clear(); outOne = local;
						retType = RUL_LONGLONG;
					}
				}
				break;
			default:
				outOne.Clear();// error
				retType = RUL_NULL;
				break;
			}// endswitch
		}// else no conversion on one

		if (twoCnvt.rank > 0 )
		{// convert twoOut to twoCnvt type
			switch (twoCnvt.rank)
			{
			case 1:	// bool
				{	local.Clear(); local = (bool)outTwo;  
					outTwo.Clear(); outTwo = local;
					retType = RUL_BOOL;
				}
				break;
			case 2:	// char
				{
					if (twoCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned char)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_UNSIGNED_CHAR;
					}
					else//signed
					{	local.Clear();  local = (char)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_CHAR;
					}
				}
				break;
			case 3:	// short
				{
					if (twoCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned short)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_USHORT;
					}
					else//signed
					{	local.Clear();  local = (short)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_SHORT;
					}
				}
				break;
			case 4:	// int
				{
					if (twoCnvt.is_unsigned)
					{	local.Clear();  local = (unsigned int)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_UINT;
					}
					else//signed
					{	local.Clear();  local = (int)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_INT;
					}
				}
				break;
			case 5:	// long long
				{
					if (twoCnvt.is_unsigned)
                    {	local.Clear();  local = (_UINT64)outTwo;
						outTwo.Clear(); outTwo = local;
						retType = RUL_ULONGLONG;
					}
					else//signed
					{	local.Clear();  local = (__int64)outTwo;  
						outTwo.Clear(); outTwo = local;
						retType = RUL_LONGLONG;
					}
				}
				break;
			default:
				outTwo.Clear();// error
				retType = RUL_NULL;
				break;
			}// endswitch
		}// else no conversion on two

	}//else let the float types stay
	return retType;
}


INTER_VARIANT::INTER_VARIANT()
{
	memset(&val,0, sizeof(val));
	varType = RUL_NULL;
}

INTER_VARIANT::INTER_VARIANT(const INTER_VARIANT& variant)
{
	memset(&val,0, sizeof(val));
	varType = variant.varType;

	switch(varType)
	{
	case RUL_NULL:
		break;
	case RUL_UNSIGNED_CHAR:
		val.ucValue = variant.val.ucValue;
		break;
	case RUL_CHAR:
		val.cValue = variant.val.cValue;
		break;
	case RUL_INT:
		val.nValue = variant.val.nValue;
		break;
	case RUL_SHORT:
		val.sValue = variant.val.sValue;
		break;
	case RUL_UINT:
		val.unValue = variant.val.unValue;
		break;
	case RUL_USHORT:
		val.usValue = variant.val.usValue;
		break;
	case RUL_LONGLONG:
		val.lValue = variant.val.lValue;
		break;
	case RUL_ULONGLONG:
		val.ulValue = variant.val.ulValue;
		break;
	case RUL_BOOL:
		val.bValue = variant.val.bValue;
		break;
	case RUL_FLOAT:
		val.fValue = variant.val.fValue;
		break;
	case RUL_DOUBLE:
		val.dValue = variant.val.dValue;
		break;

	case RUL_CHARPTR:
		{
			if ( variant.val.pszValue == NULL ) /* throw error */ return;
			size_t  nLen = strlen(variant.val.pzcVal) + 1;
			val.pzcVal = new char[nLen];
			memset(val.pzcVal,0,nLen);
			memcpy(val.pzcVal,variant.val.pzcVal,nLen);
		}
		break;

	case RUL_WIDECHARPTR:/* aka _USHORTPTR */
	case RUL_DD_STRING:
		{
			if ( variant.val.pszValue == NULL ) /* throw error */ return;
			size_t  nLen = wcslen(variant.val.pszValue) + 1;
			val.pszValue = new wchar_t[nLen];
			memset(val.pszValue,0,(nLen*sizeof(wchar_t)));
			memcpy(val.pszValue,variant.val.pszValue,(nLen*sizeof(wchar_t)));
		}
		break;
	case RUL_SAFEARRAY:
		{
			if ( variant.val.prgsa == NULL ) /* throw error */ return;
			val.prgsa = new INTER_SAFEARRAY(*(variant.val.prgsa));
		}
		break;
		
	case RUL_BYTE_STRING:
		{
			if (variant.val.bString.bs == NULL ) /* throw error */ return;
            val.bString.bs = new _UCHAR[variant.val.bString.bsLen];
			memcpy(val.bString.bs,variant.val.bString.bs,variant.val.bString.bsLen);
			val.bString.bsLen = variant.val.bString.bsLen;
		}
		break;
	}
}

// all instances where used, isNumber is true... I don't know what it's for.
INTER_VARIANT::INTER_VARIANT(bool bIsNumber,const wchar_t* szNumber)
{
	memset(&val,0, sizeof(val));
	varType = RUL_NULL;
	if(bIsNumber && szNumber != NULL)
	{	
		wstring lngStr(szNumber);
		string  shtStr;
		// convert to a short string, we don't care about letters, numbers are the same
		shtStr = TStr2AStr(lngStr);

		if ( narrowStr2number(val, varType, shtStr.c_str()) != SUCCESS )
		{// throw error
			memset(&val,0, sizeof(val));
			varType = RUL_NULL;
		}
		// else - return what we have
	}
	else
	{// NaN
		// leave clear and NULL
	}
}

// all instances where used, isNumber is true...I don't know what it's for.
INTER_VARIANT::INTER_VARIANT(bool bIsNumber,const _CHAR* szNumber)
{
	memset(&val,0, sizeof(val));
	varType = RUL_NULL;
	if(bIsNumber && szNumber != NULL)
	{	
		if ( narrowStr2number(val, varType, szNumber) != SUCCESS )
		{// throw error
			memset(&val,0, sizeof(val));
			varType = RUL_NULL;
		}
		// else - return what we have
	}
	else
	{// NaN
		// leave clear and NULL
	}
	/* old code deleted 01feb08 - see earlier version to see content of replaced code */
}

INTER_VARIANT::INTER_VARIANT(void* pmem,VARIANT_TYPE vt)
{
	SetValue(pmem, vt);
}

_INT32 INTER_VARIANT::XMLizeForMethodDebug(_CHAR* szData, size_t dataLen, bool bIsArrayElement)
{
	int slen = MAX_DD_STRING;
	_CHAR str[MAX_DD_STRING];
	memset(str, 0, slen);
	bool bIsStr = false;
	
	
	switch (varType)
	{
	case RUL_NULL:			sprintf(str, "%d", 0);					break;

	case RUL_BOOL:
	{
		/*PS_Strncat(szData, dataLen, " type=\"bool\"\0", _TRUNCATE);
		PS_Strncat(szData, dataLen, " array_size=\"0\"\0", _TRUNCATE);
		PS_Strncat(szData, dataLen, " value=\"", _TRUNCATE);
		sprintf(str, "%d", val.bValue ? 1 : 0);*/

		sprintf(str, " type=\"bool\" array_size=\"0\" value=\"%d\"/>", val.bValue ? 1 : 0);
	}
	break;
	case RUL_CHAR:
	{
		if (bIsArrayElement)
		{
			if (val.cValue == 0)
			{
				sprintf(str, " value=\"0\"/>");
			}
			else
			{
				sprintf(str, " value=\"%c\"/>", val.cValue);
			}
			
		}
		else
		{
			if (val.cValue == 0)
			{
				sprintf(str, " type=\"char\" array_size=\"0\" value=\"0\"/>");
			}
			else
			{
				sprintf(str, " type=\"char\" array_size=\"0\" value=\"%c\"/>", val.cValue);
			}
		}
	}
	break;
	case RUL_UNSIGNED_CHAR:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%hhu\"/>", val.ucValue);
		}
		else
		{
			sprintf(str, " type=\"unsigned char\" array_size=\"0\" value=\"%hhu\"/>", (unsigned int)val.ucValue);
		}
	}
	break;
	case RUL_SHORT:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%hd\"/>", val.sValue);
		}
		else
		{
			sprintf(str, " type=\"short\" array_size=\"0\" value=\"%hd\"/>", val.sValue);
		}
	}
	break;
	case RUL_USHORT:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%hu\"/>", val.usValue);
		}
		else
		{
			sprintf(str, " type=\"unsigned short\" array_size=\"0\" value=\"%hu\"/>", val.usValue);
		}
	}
	break;
	case RUL_INT:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%d\"/>", val.nValue);
		}
		else
		{
			//PS_Strncat(szData, dataLen, " type=\"int\"", _TRUNCATE);
			//PS_Strncat(szData, dataLen, " array_size=\"0\"", _TRUNCATE);
			//PS_Strncat(szData, dataLen, " value=", _TRUNCATE);
			//sprintf(str, "\"%d\"/>", val.nValue);
			sprintf(str, " type=\"int\" array_size=\"0\" value=\"%d\"/>", val.nValue);
		}
	}
	break;
	case RUL_UINT:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%u\"/>", val.unValue);
		}
		else
		{
			sprintf(str, " type=\"unsigned int\" array_size=\"0\" value=\"%u\"/>", val.unValue);
		}
	}
	break;
	case RUL_LONGLONG:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%lld\"/>", val.lValue);
		}
		else
		{
			sprintf(str, " type=\"long long\" array_size=\"0\" value=\"%lld\"/>", val.lValue);
		}
	}
	break;
	case RUL_ULONGLONG:	
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%llu\"/>", val.ulValue);
		}
		else
		{
			sprintf(str, " type=\"unsigned long long\" array_size=\"0\" value=\"%llu\"/>", val.ulValue);
		}
	}
	break;

	case RUL_FLOAT:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%.5f\"/>", val.fValue);
		}
		else
		{
			sprintf(str, " type=\"float\" array_size=\"0\" value=\"%.5f\"/>", val.fValue);
		}
	}
	break;
	case RUL_DOUBLE:
	{
		if (bIsArrayElement)
		{
			sprintf(str, " value=\"%.12g\"/>", val.dValue);
		}
		else
		{
			sprintf(str, " type=\"double\" array_size=\"0\" value=\"%.12g\"/>", val.dValue);
		}
	}
	break;

	case RUL_CHARPTR:
	{
		// for which dd data type this is used??
		/*size_t strLen = strlen(val.pzcVal) + 2;	//including ',' and null
		_CHAR *szStr = new _CHAR[strLen];
		PS_VsnPrintf(szStr, strLen, "%s\0", val.pzcVal);

		PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
		delete[] szStr;
		bIsStr = true;*/
		break;
	}
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
	{
		// local variable of type DD_STRING is always handled in RUL_SAFEARRAY case. When this case comes in picture??
		/*size_t strLen = wcslen(val.pszValue) + 2;	//including ','
		_CHAR *szStr = new _CHAR[strLen * 2];
		PS_VsnPrintf(szStr, strLen, "%ls\0", val.pszValue);

		PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
		delete[] szStr;
		bIsStr = true;*/
		break;
	}
	case RUL_BYTE_STRING:
	{
		// Not sure which dd type is handled here?
		/*
		_CHAR num[4];
		size_t strLen = (val.bString.bsLen * 2) + 5;
		_CHAR *szStr = new _CHAR[strLen];
		PS_VsnPrintf(szStr, strLen, "%d:", val.bString.bsLen);
		for (int j = 0; j < (int)val.bString.bsLen; j++)
		{
			PS_VsnPrintf(num, strLen, "%02x ", val.bString.bs[j]);
			strcat(szStr, num);
		}
		strcat(szStr, ";\0");

		PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
		delete[] szStr;
		bIsStr = true;*/
	}
	break;
	case RUL_SAFEARRAY:	
	{
		(val.prgsa)->XMLizeForMethodDebugInfo(szData, dataLen);
		bIsStr = true;
	}
	break;
	default:
		break;
	}

	if (!bIsStr)
	{
		PS_Strncat(szData, dataLen, str, _TRUNCATE);
	}

	return 0;
}





_INT32 INTER_VARIANT::XMLize(_CHAR* szData, size_t dataLen)
{
	int slen = MAX_DD_STRING;
	_CHAR str[MAX_DD_STRING]; 
	memset(str,0,slen);
	bool bIsStr = false;

	switch(varType)
	{		
	case RUL_NULL:			sprintf(str,"%d,",0);					break;
	case RUL_BOOL:			sprintf(str,"%d,",  val.bValue? 1:0);	break;
	case RUL_CHAR:			sprintf(str,"%c,",  val.cValue);		break;
	case RUL_UNSIGNED_CHAR:	sprintf(str,"%hhu,",  (unsigned int)val.ucValue);break;
	case RUL_SHORT:			sprintf(str,"%hd,",  val.sValue);		break;
	case RUL_USHORT:		sprintf(str,"%hu,",  val.usValue);		break;
	case RUL_INT:			sprintf(str,"%d,",   val.nValue);		break;
	case RUL_UINT:			sprintf(str,"%u,",   val.unValue);		break;
	case RUL_LONGLONG:		sprintf(str,"%lld,",val.lValue);		break;
	case RUL_ULONGLONG:		sprintf(str,"%llu,",val.ulValue);		break;

	case RUL_FLOAT:			sprintf(str,"%.5f,", val.fValue);		break;
	case RUL_DOUBLE:		sprintf(str,"%.12g,",val.dValue);		break;

	case RUL_CHARPTR:
		{
			size_t strLen = strlen(val.pzcVal) + 2;	//including ',' and null
			_CHAR *szStr = new _CHAR[strLen];
			PS_VsnPrintf(szStr, strLen, "%s,\0", val.pzcVal);
		
			PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
			delete [] szStr;
			bIsStr = true;
			break;
		}
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
		{
			size_t strLen = wcslen(val.pszValue) + 2;	//including ','
			_CHAR *szStr = new _CHAR[strLen * 2];
			PS_VsnPrintf(szStr, strLen, "%ls,\0", val.pszValue);
			
			PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
			delete [] szStr;
			bIsStr = true;
			break;
		}
	case RUL_BYTE_STRING:
		{
			_CHAR num[4];
			size_t strLen = (val.bString.bsLen * 2) + 5;
			_CHAR *szStr = new _CHAR[strLen];
			PS_VsnPrintf(szStr, strLen, "%d:", val.bString.bsLen);
			for (int j = 0; j < (int)val.bString.bsLen; j++)
			{
				PS_VsnPrintf(num, strLen, "%02x ", val.bString.bs[j]);
				strcat(szStr,num);
			}
			strcat(szStr,";\0");
			
			PS_Strncat(szData, dataLen, szStr, _TRUNCATE);
			delete [] szStr;
			bIsStr = true;
		}
		break;
	case RUL_SAFEARRAY:		(val.prgsa)->XMLize(szData, dataLen);			break;
	default:
		break;
	}

	if (!bIsStr)
	{
		PS_Strncat(szData, dataLen, str, _TRUNCATE);
	}

	return 0;
}

void INTER_VARIANT::SetValue(void* pmem,VARIANT_TYPE vt)
{
	Clear(); 
	memset(&val,0, sizeof(val));
	varType = vt;
	if ( pmem == NULL ) /* throw an error */return;
	size_t L = 0;
	switch(vt)
	{
	case RUL_NULL:
		//throw error
		break;
	case RUL_BOOL:
		memcpy(&(val.bValue),pmem,sizeof(val.bValue));
		break;
	case RUL_UNSIGNED_CHAR:
		memcpy(&(val.ucValue),pmem,sizeof(val.ucValue));
		break;
	case RUL_CHAR:
		memcpy(&(val.cValue),pmem,sizeof(val.cValue));
		break;
	case RUL_INT:
		memcpy(&(val.nValue),pmem,sizeof(val.nValue));
		break;
	case RUL_SHORT:
		memcpy(&(val.sValue),pmem,sizeof(val.sValue));
		break;
	case RUL_UINT:
		memcpy(&(val.unValue),pmem,sizeof(val.unValue));
		break;		
	case RUL_USHORT:
		memcpy(&(val.usValue),pmem,sizeof(val.usValue));
		break;		
	case RUL_LONGLONG:
		memcpy(&(val.lValue),pmem,sizeof(val.lValue));
		break;
	case RUL_ULONGLONG:
		memcpy(&(val.ulValue),pmem,sizeof(val.ulValue));
		break;
	case RUL_FLOAT:
		memcpy(&(val.fValue),pmem,sizeof(val.fValue));
		break;
	case RUL_DOUBLE:
		memcpy(&(val.dValue),pmem,sizeof(val.dValue));
		break;
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
		L = wcslen((wchar_t*)pmem);
		val.pszValue = new wchar_t[L+1];
		PS_Wcscpy(val.pszValue, L+1, (wchar_t*)pmem);
		break;
	case RUL_CHARPTR:
		L = strlen((char*)pmem);
		val.pzcVal = new char[L+1];
		PS_Strcpy(val.pzcVal, L+1, (char*)pmem);
		break;
	case RUL_SAFEARRAY:
		//this must be DD_STRING value
		L = wcslen((wchar_t*)pmem);
		val.prgsa = new INTER_SAFEARRAY(pmem, L, RUL_USHORT, 2);
		break;
	case RUL_BYTE_STRING:
		L = ((_BYTE_STRING*)pmem)->bsLen;
        val.bString.bs = new _UCHAR[L];
		PS_Memcpy(val.bString.bs, L, (_UCHAR*)pmem, L);
		break;
	default:
		varType = RUL_NULL;
		break;
	}
}

// apparently equal to the cast...(vt)INTER_VARIANT
void INTER_VARIANT::GetValue(void* pmem,VARIANT_TYPE vt)
{
	__VAL v;
	if ( isNumeric() )//else, string conversion is not supported
	{
		switch(vt)
		{
		case RUL_BOOL:
			v.bValue = (bool)(*this);
			memcpy(pmem,&(v.bValue),sizeof(bool));
			break;
		case RUL_CHAR:
			v.cValue = (char)(*this);
			memcpy(pmem,&(v.cValue),sizeof(char));
			break;
		case RUL_UNSIGNED_CHAR:
			v.ucValue = (unsigned char)(*this);
			memcpy(pmem,&v.ucValue,sizeof(unsigned char));
			break;
		case RUL_SHORT:
			v.sValue = (short)(*this);
			memcpy(pmem,&(v.sValue),sizeof(short));
			break;
		case RUL_USHORT:
			v.usValue = (unsigned short)(*this);
			memcpy(pmem,&(v.usValue),sizeof(unsigned short));
			break;
		case RUL_INT:
			v.nValue = (int)(*this);
			memcpy(pmem,&(v.nValue),sizeof(int));
			break;
		case RUL_UINT:
			v.unValue = (unsigned int)(*this);
			memcpy(pmem,&(v.unValue),sizeof(unsigned int));
			break;
		case RUL_LONGLONG:
			v.lValue = (__int64)(*this);
			memcpy(pmem,&(v.lValue),sizeof(__int64));
			break;
		case RUL_ULONGLONG:
            v.ulValue = (_UINT64)(*this);
            memcpy(pmem,&(v.ulValue),sizeof(_UINT64));
			break;
		case RUL_FLOAT:
			v.fValue = (float)*this;
			memcpy(pmem,&(v.fValue),sizeof(float));
			break;
		case RUL_DOUBLE:
			v.dValue = (double)*this;
			memcpy(pmem,&(v.dValue),sizeof(double));
			break;
		default:
			/* unsupported conversion error */
			break;
		}
	}
	else
	{// NON numeric
		INTER_VARIANT localVar;
		localVar = vt;// set the type
		localVar = (*this);// converts self to desired type via operator equal
		switch(vt)
		{
		case RUL_CHARPTR:
			{
				char* pchar = localVar.GetValue().pzcVal;
				memcpy(pmem,pchar,strlen(pchar)+1);
			}
			break;
		case RUL_WIDECHARPTR:	
		case RUL_DD_STRING:		
			{
				wchar_t* pchar = localVar.GetValue().pszValue;
				memcpy(pmem,pchar,wcslen(pchar)+1);
			}
			break;
		case RUL_BYTE_STRING:
			{
				unsigned char* pchar = localVar.GetValue().bString.bs;
				memcpy(pmem,pchar,localVar.GetValue().bString.bsLen);
			}
			break;
		case RUL_SAFEARRAY:	
			{
				INTER_SAFEARRAY *pchar = localVar.GetSafeArray();
				if (pchar)
				//	memcpy(pmem,pchar->getDataPtr(),pchar->MemoryAllocated());
					memcpy(pmem,(char*)pchar, pchar->MemoryAllocated());
			}
			break;
			
		case RUL_NULL:
		default:	/* throw an error this can't happen */
			break;
		}
	}// end else numeric or not
}
/**** commented out code removed from here on 07feb08 - see earlier version for contents ****/

INTER_SAFEARRAY *INTER_VARIANT::GetSafeArray()
{
	if (varType == RUL_SAFEARRAY)
	{
		return val.prgsa;
	}
	else
	{
		return NULL;
	}
}


_INT32 INTER_VARIANT::VariantSize(VARIANT_TYPE vt)
{
	_INT32 r = 0;
	switch(vt)
	{	
	case RUL_NULL:			r = 0;					break;
	case RUL_BOOL:			r = sizeof(_BOOL);		break;
	case RUL_CHAR:			r = sizeof(_CHAR);		break;
	case RUL_UNSIGNED_CHAR:	r = sizeof(_UCHAR);		break;
	case RUL_SHORT:			r = sizeof(_INT16);		break;
	case RUL_USHORT:		r = sizeof(_UINT16);	break;
	case RUL_INT:			r = sizeof(_INT32);		break;
	case RUL_UINT:			r = sizeof(_UINT32);	break;
	case RUL_LONGLONG:		r = sizeof(_INT64);		break;
	case RUL_ULONGLONG:		r = sizeof(_UINT64);	break;
	case RUL_FLOAT:			r = sizeof(_FLOAT);		break;
	case RUL_DOUBLE:		r = sizeof(_DOUBLE);	break;
		
	case RUL_CHARPTR:
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
							r = sizeof(_CHAR*);		break;
	case RUL_BYTE_STRING:	r = sizeof(_BYTE_STRING);break;
	case RUL_SAFEARRAY:		r = sizeof(INTER_SAFEARRAY*); break;
	default:
		r = 0;
		break;
	}// end switch

	return r;
}

//Added By Anil June 17 2005 --starts here
//This Function Has to be used Only to get the string value
// stevev - 11feb08 - will get a narrow-character version of a string
//		note that that VARIANT_TYPE makes no sense here and will be discarded
//			return will be char string, source will be valType of self
void INTER_VARIANT::GetStringValue(char** pmem,VARIANT_TYPE vt)
{
	if ( pmem == NULL ) return;
	wstring wStr;
	 string  Str;
	
	if ( varType == RUL_SAFEARRAY )
	{
		INTER_VARIANT lIV;
		int cnt = 0;

		INTER_SAFEARRAY *pSA = GetSafeArray();
		if ( pSA && (cnt = pSA->GetNumberOfElements()) > 0 )
		{
			if ( pSA->Type() == RUL_CHARPTR  || pSA->Type() == RUL_CHAR)
			{
				for ( int i = 0; i < cnt; i++)
				{
					pSA->GetElement(i,&lIV);
					Str += ((char) lIV);
					lIV.Clear();
				}
			}
			else
			if (pSA->Type() == RUL_WIDECHARPTR || pSA->Type() == RUL_DD_STRING || pSA->Type() == RUL_USHORT)
			{
				for ( int i = 0; i < cnt; i++)
				{
					pSA->GetElement(2*i,&lIV);
					wStr += ((wchar_t) lIV);
					lIV.Clear();
				}
				Str  = TStr2AStr(wStr);
			}
			else
			if (pSA->Type() == RUL_SAFEARRAY)
			{
				;// multi-dimensional strings are not supported
			}
			//else - numeric conversion not supported
		}
	}
	
	if ( varType == RUL_CHARPTR  )
	{
		if(val.pzcVal)
		{
			Str = val.pzcVal;
		}
	}

	if ( varType == RUL_WIDECHARPTR || varType == RUL_DD_STRING )
	{
		if(val.pszValue)
		{
			wStr = val.pszValue;
			Str  = TStr2AStr(wStr);
		}
	}

	*pmem = new _CHAR[Str.size()+1]; // ws 12mar08 return empty string instead od null pointer
	memset(*pmem,0,Str.size()+1);
	if ( Str.size() > 0 )
	{
		memcpy(*pmem,Str.c_str(),Str.size()+1);
	}
}
// stevev - 11feb08 - will get a wide-character version of a string
//		note that that VARIANT_TYPE makes no sense here and will be discarded
//			return will be wide-char string; source will be valType of self
void INTER_VARIANT::GetStringValue(wchar_t** pTmem, bool bIsLog)
{
	if ( pTmem == NULL ) return;
	wstring wStr;
	string  Str;
	
	switch (varType)
	{
	case RUL_SAFEARRAY:
	{
		INTER_VARIANT lIV;
		int cnt = 0;

		INTER_SAFEARRAY *pSA = GetSafeArray();
		if ( pSA && (cnt = pSA->GetNumberOfElements()) > 0 )
		{
			switch(pSA->Type())
			{
				case RUL_CHAR:
				case RUL_UNSIGNED_CHAR:
				{
					for ( int i = 0; i < cnt; i++)
					{
						pSA->GetElement(i, &lIV);
						Str += ((char) lIV);
						lIV.Clear();
					}
					
					if (bIsLog)
					{
						int size = (int)strlen(Str.c_str());
						if (size < cnt)
						{
							//terminate the string
							Str = Str.substr(0, size);
						}

						Str = "\"" + Str + "\"";					
					}

					wStr = AStr2TStr(Str);
				}
				break;
				case RUL_SHORT:
				case RUL_INT:
				case RUL_LONGLONG:
				{
					if (bIsLog)
					{
						wStr = _T("[");
					}

					for (int i = 0, index = 0;
						 index < cnt;
						 i+=pSA->GetElementSize(), index++)
					{
						if ((bIsLog) && (i>0))
						{
							wStr += _T(", ");
						}
						pSA->GetElement(i, &lIV);
						long long value = (long long) lIV;
						wStr += to_wstring(value);
						lIV.Clear();
					}
			
					if (bIsLog)
					{
						wStr += _T("]");
					}
				}
					break;
				case RUL_USHORT:
				{
					if (bIsLog)
					{
						wStr = _T("[");
					}

					for (int i = 0, index = 0;
						 index < cnt;
						 i+=pSA->GetElementSize(), index++)
					{
						pSA->GetElement(i, &lIV);

						if ((bIsLog) && (i > 0))
						{
							wStr += _T(", ");
						}
						wStr += (wchar_t)lIV;

						lIV.Clear();
					}
			
					//cut off null terminator
					int size = (int)wcslen(wStr.c_str());
					if (size < cnt)
					{
						//terminate the string
						wStr = wStr.substr(0, size);
					}

					if (bIsLog)
					{
						if (size > 2)
						{
							//cut of last _T(", ")
							wStr = wStr.substr(0, size-2);
						}
						wStr += _T("]");
					}
				}
					break;
				case RUL_UINT:
				case RUL_ULONGLONG:
				{
					if (bIsLog)
					{
						wStr = _T("[");
					}

					for (int i = 0, index = 0;
						 index < cnt;
						 i+=pSA->GetElementSize(), index++)
					{
						if ((bIsLog) && (i>0))
						{
							wStr += _T(", ");
						}
						pSA->GetElement(i, &lIV);
						unsigned long long value = (unsigned long long) lIV;
						wStr += to_wstring(value);
						lIV.Clear();
					}
			
					if (bIsLog)
					{
						wStr += _T("]");
					}
				}
					break;
				case RUL_FLOAT:
				case RUL_DOUBLE:
				{
					if (bIsLog)
					{
						wStr = _T("[");
					}

					for (int i = 0, index = 0;
						 index < cnt;
						 i+=pSA->GetElementSize(), index++)
					{
						if ((bIsLog) && (i>0))
						{
							wStr += _T(", ");
						}
						pSA->GetElement(i, &lIV);
						long double value = (double) lIV;
						wStr += to_wstring(value);
						lIV.Clear();
					}
			
					if (bIsLog)
					{
						wStr += _T("]");
					}
				}
				break;
				default:	// RUL_SAFEARRAY
				{
					// numeric conversion not supported
					// multi-dimensional strings are not supported
				}
				break;
			}
		}
	}
		break;
	case RUL_CHARPTR:
	{
		if (bIsLog)
		{
			Str = "\"";
		}
		Str  += val.pzcVal;
		if (bIsLog)
		{
			Str += "\"";
		}

		wStr = AStr2TStr(Str);
	}
		break;
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
	{
		if (bIsLog)
		{
			wStr = _T("\"");
		}
		wStr += val.pszValue;
		if (bIsLog)
		{
			wStr += _T("\"");
		}

#ifdef _DEBUG
		Str = TStr2AStr(wStr);
#endif
	}
		break;
	case RUL_BYTE_STRING:
	{
		if (bIsLog)
		{
			Str = "\"";
		}
		Str  += (char *)val.bString.bs;
		if (bIsLog)
		{
			Str += "\"";
		}

		wStr = AStr2TStr(Str);
	}
		break;
	}

	if ( varType != RUL_NULL  )
	{
		*pTmem = new wchar_t[wStr.size()+1]; 
		memset(*pTmem,0,wStr.size()+1);
		memcpy(*pTmem,wStr.c_str(),((wStr.size()+1)*sizeof(wchar_t)));
	}
	else
	{
		*pTmem = NULL;
	}

}

// Function Name: GetCharBuffValue
// Purpose: This function gets the unsigned char buffer from param, specifically modified for 
// handling null character in date-time buffer which was failing due to abnormal termination of
// character buffer due to null character inbetween.
bool INTER_VARIANT::GetCharBuffValue(string &Str)
{
	bool retValue = false;

	if ( varType == RUL_SAFEARRAY )
	{
		INTER_VARIANT lIV;
		int cnt = 0;

		INTER_SAFEARRAY *pSA = GetSafeArray();
		if ( pSA && (cnt = pSA->GetNumberOfElements()) > 0 )
		{
			if (( pSA->Type() == RUL_CHAR ) || 
					( pSA->Type() == RUL_UNSIGNED_CHAR ))
			{
				for ( int i = 0; i < cnt; i++)
				{
					pSA->GetElement(i,&lIV);
					Str += ((char) lIV);
					lIV.Clear();
				}

				retValue = true;
			}
		}
	}

	return retValue;

}


// stevev 05jun07 - used to detect if promotion is possible
//moved isNumeric() to .h file (it wouldn't link in TOK)


INTER_VARIANT::~INTER_VARIANT()
{
	Clear();
}

void INTER_VARIANT::Clear()
{
	switch(varType)
	{
	case RUL_NULL:
		break;
	case RUL_BOOL:
	case RUL_CHAR:
	case RUL_UNSIGNED_CHAR:
	case RUL_INT:
	case RUL_SHORT:
	case RUL_UINT:
	case RUL_USHORT:
	case RUL_LONGLONG:
	case RUL_ULONGLONG:
	case RUL_FLOAT:
	case RUL_DOUBLE:
		varType = RUL_NULL;
		memset(&val,0, sizeof(val));
		break;

	case RUL_CHARPTR:
		varType = RUL_NULL;
		if(val.pzcVal)
			delete[] val.pzcVal;
		memset(&val,0, sizeof(val));
		break;
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:	
		varType = RUL_NULL;
		if(val.pszValue)
			delete[] val.pszValue;
		memset(&val,0, sizeof(val));
		break;
	case RUL_BYTE_STRING:
		varType = RUL_NULL;
		if(val.bString.bsLen > 0)
		{
			delete[] val.bString.bs;
			val.bString.bs = NULL;
			val.bString.bsLen = 0;
		}
		memset(&val,0, sizeof(val));
		break;

	case RUL_SAFEARRAY:
		varType = RUL_NULL;
		//Anil 250407 I am wondering why this is here from such a long time.
		//This memory gets allocated during the Declaration List execution(ie char sztemp[100];). 
		//This should not be deleted afterwards.<were does it go?...sjv 01jun07??>
		//Hence Commenting
		DELETE_PTR(val.prgsa);// uncommented WS:EPM 17jul07 checkin
		memset(&val,0, sizeof(val));// make sure its alllll zero
		break;
	}
}

/***********************************************************************************************
 * start of operator= functions
 *
 * operator= ONLY changes the varType if the varType was RUL_NULL (untyped) otherwise
 *	the incoming value is cast to the lvalue's varType
 *
 * string types cannot be set operator= to numeric types NOR vice versa
 *
 *************************/
// a helper - duplicate code extracted to a macro
#define CLEAR_DATA( typ ) \
{  VARIANT_TYPE vt=varType; \
	if (vt != RUL_SAFEARRAY) { Clear(); varType=( (vt==RUL_NULL) ? ( typ ) : vt); }\
	else if (val.prgsa) val.prgsa->makeEmpty(); /*else leave it null*/  }

/*** the following code is used inside all of the numeric data type's operator= functions ***/
#define NUMERIC_EQUALS( x ) \
switch (varType )	{\
	case RUL_BOOL:			val.bValue = ((x != 0 )?true:false);	break;\
	case RUL_CHAR:			val.cValue = ( char )      x;			break;\
	case RUL_UNSIGNED_CHAR:	val.ucValue = (unsigned char)  x;		break;\
	case RUL_SHORT:			val.sValue  = ( short )    x;			break;\
	case RUL_USHORT:		val.usValue = (unsigned short) x;		break;\
	case RUL_INT:			val.nValue  = ( int )      x;			break;\
	case RUL_UINT:			val.unValue = ( unsigned int ) x;		break;\
	case RUL_LONGLONG:		val.lValue  = ( __int64 )  x;			break;\
    case RUL_ULONGLONG:		val.ulValue = ( _UINT64) x;	break;\
	case RUL_FLOAT:			val.fValue  = ( float )    x;			break;\
	case RUL_DOUBLE:		val.dValue  = (double)     x;			break;\
	case RUL_CHARPTR:		case RUL_WIDECHARPTR:	case RUL_BYTE_STRING: \
	case RUL_DD_STRING:		case RUL_SAFEARRAY:/* NUMERIC to STRING conversion attempt*/break;\
	case RUL_NULL:	default:  /* throw an error - shouldn't be possible to be here */   break;\
	}/* end switch */

/*
 ***********************************************************************************************/


const INTER_VARIANT& INTER_VARIANT::operator=(bool n)
{
	CLEAR_DATA( RUL_BOOL );
	int x = 0;  if (n) x = 1;
	
	switch (varType )	
	{
	case RUL_BOOL:			val.bValue = n;							break;
	case RUL_CHAR:			val.cValue = ( char )      x;			break;
	case RUL_UNSIGNED_CHAR:	val.ucValue = (unsigned char)  x;		break;
	case RUL_SHORT:			val.sValue  = ( short )    x;			break;
	case RUL_USHORT:		val.usValue = (unsigned short) x;		break;
	case RUL_INT:			val.nValue  = ( int )      x;			break;
	case RUL_UINT:			val.unValue = ( unsigned int ) x;		break;
	case RUL_LONGLONG:		val.lValue  = ( __int64 )  x;			break;
    case RUL_ULONGLONG:		val.ulValue = ( _UINT64) x;	break;
	case RUL_FLOAT:			val.fValue  = ( float )    x;			break;
	case RUL_DOUBLE:		val.dValue  = (double)     x;			break;

	case RUL_CHARPTR:		
	case RUL_WIDECHARPTR:	
	case RUL_BYTE_STRING: 
	case RUL_DD_STRING:		
	case RUL_SAFEARRAY:
		// * NUMERIC to STRING conversion attempt * //
		break;
	case RUL_NULL:	
	default:  
		// * throw an error - shouldn't be possible to be here * //   
		break;
	}// * end switch * //

	return *this;
}


const INTER_VARIANT& INTER_VARIANT::operator=(_CHAR n)
{
	CLEAR_DATA( RUL_CHAR );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_UCHAR n)
{
	CLEAR_DATA( RUL_UNSIGNED_CHAR );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(wchar_t n)
{
	CLEAR_DATA( RUL_USHORT );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_USHORT n)
{
	CLEAR_DATA(RUL_USHORT);

	NUMERIC_EQUALS(n);

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_SHORT n)
{
	CLEAR_DATA( RUL_SHORT );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(int n)
{
	CLEAR_DATA( RUL_INT );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(unsigned int n)
{
	CLEAR_DATA( RUL_UINT );

	NUMERIC_EQUALS( n );

	return *this;
}
const INTER_VARIANT& INTER_VARIANT::operator=(_INT32 n)
{
    CLEAR_DATA( RUL_INT );

    NUMERIC_EQUALS( n );

    return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_UINT32 n)
{
    CLEAR_DATA( RUL_UINT );

    NUMERIC_EQUALS( n );

    return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_INT64 n)
{
	CLEAR_DATA( RUL_LONGLONG );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_UINT64 n)
{
	CLEAR_DATA( RUL_ULONGLONG );

	// * bill has not implemented a cast from uint64 to float or double...* //
	switch (varType )	
	{
	case RUL_BOOL:			val.bValue = ((n != 0 )?true:false);	break;
	case RUL_CHAR:			val.cValue = ( char )      n;			break;
	case RUL_UNSIGNED_CHAR:	val.ucValue = (unsigned char)  n;		break;
	case RUL_SHORT:			val.sValue  = ( short )    n;			break;
	case RUL_USHORT:		val.usValue = (unsigned short) n;		break;
	case RUL_INT:			val.nValue  = ( int )      n;			break;
	case RUL_UINT:			val.unValue = ( unsigned int ) n;		break;
	case RUL_LONGLONG:		val.lValue  = ( __int64 )  n;			break;
    case RUL_ULONGLONG:		val.ulValue = ( _UINT64) n;	break;
	case RUL_FLOAT:			val.fValue  = ( float ) (( __int64 )n);	break;
	case RUL_DOUBLE:		val.dValue  = (double)  (( __int64 )n);	break;

	case RUL_CHARPTR:		
	case RUL_WIDECHARPTR:	
	case RUL_BYTE_STRING: 
	case RUL_DD_STRING:		
	case RUL_SAFEARRAY:
		// * NUMERIC to STRING conversion attempt* //
		break;
	case RUL_NULL:	
	default:  
		// * throw an error - shouldn't be possible to be here * //   
		break;
	}// * end switch * //

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_FLOAT n)
{
	CLEAR_DATA( RUL_FLOAT );

	NUMERIC_EQUALS( n );

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(_DOUBLE n)
{
	CLEAR_DATA( RUL_DOUBLE );

	NUMERIC_EQUALS( n );

	return *this;
}

/********* strings ********** strings ************* strings ********** strings *****************/
/* stevev 26dec07
	If we are wide bodied (wide or dd-string) then the user meant to convert.
	If we are NULL (just generated) or normal char type then just put it in.
   We treat this as a char string but put it into the (overloaded) pszValue
*/

const INTER_VARIANT& INTER_VARIANT::operator=(_CHAR* psz)
{	//Added By Anil June 16 2005 --starts here
	//This was required as this may get called even if it is RUL_DD_STRING: 
	//This has to be implimented Evey Where
//stevev 13feb08.this occurs on empty string for some reason...	assert(psz != NULL);
//       change to:
	char mtStr[] = {0};
	if (psz == NULL)
	{
		psz = &(mtStr[0]);
	}

	size_t inLen    = strlen(psz);
	size_t storeLen = inLen + 1;
	
	CLEAR_DATA( RUL_CHARPTR );
	
	if ( varType == RUL_CHARPTR )
	{
		val.pzcVal = new char[storeLen]; 
		memcpy(val.pzcVal,psz,storeLen);
		val.pzcVal[inLen] = 0;
	}
	else
	if ( varType == RUL_WIDECHARPTR || varType == RUL_DD_STRING)
	{// narrow to wide conversion (destination never changes in operator=)
		wstring tmpstr;
		string  tz(psz);		
		tmpstr = AStr2TStr(tz);

		val.pszValue = new tchar[tmpstr.size() +1]; 
		_tstrcpy(val.pszValue,tmpstr.c_str());
		val.pszValue[tmpstr.size()] = 0;
	}
	else
	if ( varType == RUL_BYTE_STRING )
	{// converted to unsigned
		val.bString.bs    = new uchar[storeLen]; 
		val.bString.bsLen = (_UINT32)storeLen;
		memset(val.bString.bs,0,storeLen);
		memcpy(val.bString.bs,psz,storeLen);
	}
	else 
	if( varType == RUL_SAFEARRAY )
	{
		if ( val.prgsa == NULL )
			val.prgsa = new INTER_SAFEARRAY(psz);
		else
			*(val.prgsa) = psz;
	}
	// else throw an error - its a string to numeric conversion attempt!!!!!!!!!!!!!!!

	return *this;
}

/****** code commented * 1feb08 * was removed 07feb08 - see earlier versions for content ********/

const INTER_VARIANT& INTER_VARIANT::operator=(const wchar_t* psz)
{
	if( psz )
	{
	size_t inLen    = wcslen(psz);
	size_t storeLen = inLen + 1;
	
	CLEAR_DATA( RUL_WIDECHARPTR );
	
	if ( varType == RUL_CHARPTR )
	{// wide to narrow conversion
		wstring tmpstr(psz);
		string  tz;		
		tz = TStr2AStr(tmpstr);

		val.pzcVal = new char[tz.size() +1]; 
		strncpy(val.pzcVal,tz.c_str(),(tz.size() +1));
		val.pzcVal[tz.size()] = 0;
	}
	else
	if ( varType == RUL_WIDECHARPTR || varType == RUL_DD_STRING)
	{// wide to copy of wide
		val.pszValue = new wchar_t[storeLen]; 
		memcpy(val.pszValue,psz,storeLen * sizeof(wchar_t));
		val.pszValue[inLen] = 0;
	}
	else
	if ( varType == RUL_BYTE_STRING )
	{// converted to unsigned bytes
		storeLen = inLen * sizeof(wchar_t);
		val.bString.bs    = new uchar[storeLen]; 
		val.bString.bsLen = (_UINT32)storeLen;
		memcpy(val.bString.bs,(unsigned char*)psz,storeLen);
	}
	else 
	if( varType == RUL_SAFEARRAY )
	{
		if ( val.prgsa == NULL )
			val.prgsa = new INTER_SAFEARRAY(psz);
		else
			*(val.prgsa) = psz;
	}
	// else throw an error - its a string to numeric conversion attempt!!!!!!!!!!!!!!!
	}
	else
	{
		CLEAR_DATA( RUL_WIDECHARPTR );
	}

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::AssignWithType(VARIANT_TYPE var_type)
{
	if (varType == var_type)
	{
		return operator=((const INTER_VARIANT)*this);
	}

	INTER_VARIANT var = *this;		//R-value/type

	//clear instance and change type
	*this = var_type;				//L-type
	
	switch(varType)
	{	
	case RUL_BOOL:		
		if ( var.isNumeric() )			operator=( (bool)var);
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_CHAR:		
		if ( var.isNumeric() )			operator=( (char)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_UNSIGNED_CHAR:		
		if ( var.isNumeric() )			operator=( (unsigned char)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_SHORT:		
		if ( var.isNumeric() )			operator=( (short)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_USHORT:	
		if (var.isNumeric() )			operator=( (wchar_t)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_INT:	
		if ( var.isNumeric() )			operator=( (int)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_UINT:
		if ( var.isNumeric() )			operator=( (unsigned int)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_LONGLONG:
		if ( var.isNumeric() )			operator=( (__int64)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_ULONGLONG:
        if ( var.isNumeric() )			operator=( (_UINT64)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_FLOAT:
		if ( var.isNumeric() )			operator=( (float)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_DOUBLE:
		if ( var.isNumeric() )			operator=( (double)var );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_CHARPTR:		//	-|---------- LVALUE type
		if (var.varType == RUL_CHARPTR)   // rvalue type
		{
			operator=((char*)(var.GetValue().pzcVal));  // converts to all 5
		}
		else
		if (var.varType == RUL_WIDECHARPTR || var.varType == RUL_DD_STRING) // rvalue type
		{
			if( var.GetValue().pszValue )
			{
				operator=((char*)(var.GetValue().pszValue));// converts to all 5
			}
		}
		else
		if (var.varType == RUL_SAFEARRAY)  // rvalue type
		{
			operator=((char*)(var.GetValue().prgsa));//NO conversion! 
		}
		break;
	case RUL_WIDECHARPTR:	//	 |
	case RUL_DD_STRING:		//	 |---------- LVALUE type
		if (var.varType == RUL_CHARPTR)   // rvalue type
		{
			operator=((wchar_t*)(var.GetValue().pzcVal));  // converts to all 5
		}
		else
		if (var.varType == RUL_WIDECHARPTR || var.varType == RUL_DD_STRING) // rvalue type
		{
			if( var.GetValue().pszValue )
			{
				operator=((wchar_t*)(var.GetValue().pszValue));// converts to all 5
			}
		}
		else
		if (var.varType == RUL_SAFEARRAY)  // rvalue type
		{
			operator=((wchar_t*)(var.GetValue().prgsa));//NO conversion! 
		}
		break;
	case RUL_BYTE_STRING:	//	 |---------- LVALUE type
		if (var.varType == RUL_BYTE_STRING)// rvalue type
		{
#ifdef _WIN32
			operator=(((_BYTE_STRING)var.GetValue().bString));// converts to all 5
#else
			operator=(((_BYTE_STRING&)(var.GetValue().bString)));// converts to all 5
#endif
		}
		break;
	case RUL_SAFEARRAY:		//	-|---------- LVALUE type
		if (var.varType == RUL_CHARPTR)   // rvalue type
		{
			operator=((INTER_SAFEARRAY*)(var.GetValue().pzcVal));  // converts to all 5
		}
		else
		if (var.varType == RUL_WIDECHARPTR || var.varType == RUL_DD_STRING) // rvalue type
		{
			if( var.GetValue().pszValue )
			{
				operator=((INTER_SAFEARRAY*)(var.GetValue().pszValue));// converts to all 5
			}
		}
		else
		if (var.varType == RUL_SAFEARRAY)  // rvalue type
		{
			operator=((INTER_SAFEARRAY*)(var.GetValue().prgsa));//NO conversion! 
		}
		//else numeric to string conversion attempt - returns clear
		break;
	case RUL_NULL:
	default:
		/* throw an error this can't happen */
		break;

	}//end switch


	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=(const INTER_VARIANT& var)
{	
	const INTER_VARIANT* ptV = &var;
	INTER_VARIANT* pV = (INTER_VARIANT*)ptV;

	CLEAR_DATA( var.varType );// only changes type is its currently RUL_NULL
	
	int z; unsigned int w;

	switch(varType)
	{	
	case RUL_BOOL:		
		if ( pV->isNumeric() )			operator=( (bool)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_CHAR:		
		if ( pV->isNumeric() )			operator=( (char)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_UNSIGNED_CHAR:		
		if ( pV->isNumeric() )			operator=( (unsigned char)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_SHORT:		
		if ( pV->isNumeric() )			operator=( (short)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_USHORT:	
		if ( pV->isNumeric() )			operator=( (wchar_t)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_INT:	
		z = (int)*pV;
		if ( pV->isNumeric() )			operator=( z );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_UINT:
		w = (unsigned int)*pV;
		if ( pV->isNumeric() )			operator=( w );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_LONGLONG:
		if ( pV->isNumeric() )			operator=( (__int64)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_ULONGLONG:
        if ( pV->isNumeric() )			operator=( (_UINT64)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_FLOAT:
		if ( pV->isNumeric() )			operator=( (float)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_DOUBLE:
		if ( pV->isNumeric() )			operator=( (double)*pV );
		// else - string to numeric conversion attempt - returns clear
		break;
	case RUL_CHARPTR:		//	-|
	case RUL_WIDECHARPTR:	//	 |
	case RUL_DD_STRING:		//	 |---------- LVALUE type
	case RUL_BYTE_STRING:	//	 |
	case RUL_SAFEARRAY:		//	-|
		if (var.varType == RUL_CHARPTR)   // rvalue type
		{
			operator=((char*)(pV->GetValue().pzcVal));  // converts to all 5
		}
		else
		if (var.varType == RUL_WIDECHARPTR || var.varType == RUL_DD_STRING) // rvalue type
		{
			if( pV->GetValue().pszValue )
			{
				operator=((wchar_t*)(pV->GetValue().pszValue));// converts to all 5
			}
		}
		else
		if (var.varType == RUL_SAFEARRAY)  // rvalue type
		{
			operator=((INTER_SAFEARRAY*)(pV->GetValue().prgsa));//NO conversion! 
		}
		else
		if (var.varType == RUL_BYTE_STRING)// rvalue type
		{
#ifdef _WIN32
			operator=(((_BYTE_STRING)pV->GetValue().bString));// converts to all 5
#else
			operator=(((_BYTE_STRING&)(pV->GetValue().bString)));// converts to all 5
#endif
		}
		//else numeric to string conversion attempt - returns clear
		break;
	case RUL_NULL:
	default:
		/* throw an error this can't happen */
		break;

/**** commented-out code removed 13feb08.  See earlier version for content ****/

	}//end switch
	return *this;
}


const INTER_VARIANT& INTER_VARIANT::operator=(VARIANT_TYPE type)
{
	Clear();
	varType = type;

	return *this;
}

const INTER_VARIANT& INTER_VARIANT::operator=( INTER_SAFEARRAY *sa )
{	
	CLEAR_DATA( RUL_SAFEARRAY );
	int aryTyp = sa->Type();
	int arySiz = sa->GetNumberOfElements();
	int i;
	int elemSz = sa->GetElementSize();
	INTER_VARIANT localIV;

	switch (varType)
	{
	case RUL_BOOL:
	case RUL_CHAR:
	case RUL_UNSIGNED_CHAR:
	case RUL_SHORT:
	case RUL_USHORT:
	case RUL_INT:
	case RUL_UINT:
	case RUL_LONGLONG:
	case RUL_ULONGLONG:
	case RUL_FLOAT:
	case RUL_DOUBLE:
		/* error --- attempt to set a numeric from an array */
		break;//return cleared

	/*  safearray to string is currently only allowed for matching types 
		eg array of wide chars to widecharptr  **/

	case RUL_CHARPTR:
		if ( aryTyp == RUL_CHAR )
		{
			val.pzcVal = new char[arySiz+1];
			for (i = 0; i < arySiz; i++)
			{
				sa->GetElement(i,&localIV);
				val.pzcVal[i] = (char)localIV;
				localIV.Clear();
			}
			val.pzcVal[arySiz] = 0;
		}
		// else conversion desired... not supported at this time
		break;
	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
		switch (aryTyp)
		{
			case RUL_USHORT:
			case RUL_CHAR:
			{
				val.pszValue = new wchar_t[arySiz + 1];
				int offset = 0;
				for (i = 0; i < arySiz; offset += elemSz, i++)
				{
					sa->GetElement(offset, &localIV);
					val.pszValue[i] = (wchar_t)localIV;
					localIV.Clear();
				}
				val.pszValue[arySiz] = 0;
			}
			break;
			default:
				break;
			// else conversion desired... not supported at this time
		}
		break;
	case RUL_BYTE_STRING:
		if ( aryTyp == RUL_UNSIGNED_CHAR )
		{
			val.bString.bsLen = arySiz;
			val.bString.bs    = new unsigned char[arySiz];
			for (i = 0; i < arySiz; i++)
			{
				sa->GetElement(i,&localIV);
				val.bString.bs[i] = (unsigned char)localIV;
				localIV.Clear();
			}
			val.bString.bs[arySiz] = 0;
		}
		// else conversion desired... not supported at this time
		break;

	case RUL_SAFEARRAY:	
		if ( val.prgsa == NULL )
			val.prgsa = new INTER_SAFEARRAY(*sa);//allocate new safe array
		else
			*(val.prgsa) = *sa;
		break;
	case RUL_NULL:
	default:
		/* throw an error - this is impossible to get into */
		break;
	}

	return *this;
}

/* a bytestring has an internal length since it may contain embedded nulls */
const INTER_VARIANT& INTER_VARIANT::operator=(_BYTE_STRING& f)
{
	int inLen    = f.bsLen;
	int storeLen = inLen + 1;
	
	CLEAR_DATA( RUL_BYTE_STRING );
	
	if ( varType == RUL_CHARPTR )
	{// byte to string conversion
		if ( f.bs != NULL )
		{
			val.pzcVal = new char[storeLen]; 
			memcpy(val.pzcVal,(char*)f.bs,storeLen);// copy past first null just-in-case
			val.pzcVal[inLen] = 0;					// force string termination if not-a-string
		}
		else
		{
			val.pzcVal = new char; 
			val.pzcVal = 0;
		}
	}
	else
	if ( varType == RUL_WIDECHARPTR || varType == RUL_DD_STRING)
	{// byte to wide
		val.pszValue = new wchar_t[storeLen]; 
		for (int y = 0; y < inLen; val.pszValue[y]=f.bs[y],y++);// one wide-char per char
		val.pszValue[inLen] = 0;// force string termination if not-a-string
	}
	else
	if ( varType == RUL_BYTE_STRING )
	{
		val.bString.bs    = new uchar[inLen]; 
		val.bString.bsLen = inLen;
		memcpy(val.bString.bs,(unsigned char*)f.bs,inLen);
	}
	else 
	if( varType == RUL_SAFEARRAY )
	{
		if ( val.prgsa == NULL )
			val.prgsa = new INTER_SAFEARRAY(&f);
		else
			*(val.prgsa) = f;
	}
	// else throw an error - its a string to numeric conversion attempt!!!!!!!!!!!!!!!

	return *this;
}




/**** casts ******** casts ******** casts ******** casts ******** casts ******** casts **********/

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator bool() 
{
	bool retVal = 0;
	switch (varType)
	{	
	case RUL_BOOL:
		retVal = val.bValue;
		break;
	case RUL_CHAR:
		retVal = (bool)( val.cValue != 0 );
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (bool)(val.ucValue != 0 );
		break;
	case RUL_SHORT:
		retVal = (bool)(val.sValue!=0);
		break;
	case RUL_USHORT:
		retVal = (bool)(val.usValue!=0);
		break;
	case RUL_INT:
		retVal = (bool)(val.nValue!=0);
		break;
	case RUL_UINT:
		retVal = (bool)(val.unValue!=0);
		break;
	case RUL_LONGLONG:
		retVal = (bool) (val.lValue!=0);
		break;
	case RUL_ULONGLONG:
		retVal = (bool) (val.ulValue!=0);
		break;
	case RUL_FLOAT:
		retVal =  (bool) (val.fValue!=0.0);
		break;
	case RUL_DOUBLE:
		retVal =  (bool) (val.dValue!=0.0);
		break;
	default:
		retVal = (bool)0; 
		break;
	}// end switch

	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator int()
{
	int retVal = 0;
	switch(varType)
	{	
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (int)1;
		}
		break;
	case RUL_CHAR:
		retVal = (int)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (int)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (int)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (int)val.usValue;
		break;
	case RUL_INT:
		retVal = (int)val.nValue;
		break;
	case RUL_UINT:
		retVal = (int)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (int) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (int) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (int) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (int) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (int)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator unsigned int(void)
{
	unsigned int retVal = 0;
	switch(varType)
	{	
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (unsigned int)1;
		}
		break;
	case RUL_CHAR:
		retVal = (unsigned int)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (unsigned int)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (unsigned int)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (unsigned int)val.usValue;
		break;
	case RUL_INT:
		retVal = (unsigned int)val.nValue;
		break;
	case RUL_UINT:
		retVal = (unsigned int)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (unsigned int) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (unsigned int) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (unsigned int) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (unsigned int) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (unsigned int)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator float()
{
	float retVal = 0.0;
	switch(varType)
	{	
	case RUL_BOOL:
		if (val.bValue )
		{
			retVal = 1.0;
		} 
		break;
	case RUL_CHAR:
		retVal = (float)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (float)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (float)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (float)val.usValue;
		break;
	case RUL_INT:
		retVal = (float)val.nValue;
		break;
	case RUL_UINT:
		retVal = (float)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (float) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (float) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (float) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (float)val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (float)0.0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator char()
{
	char retVal = 0;
	switch(varType)
	{	
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (char)1;
		}
		break;
	case RUL_CHAR:
		retVal = (char)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (char)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (char)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (char)val.usValue;
		break;
	case RUL_INT:
		retVal = (char)val.nValue;
		break;
	case RUL_UINT:
		retVal = (char)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (char) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (char) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (char) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (char) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (char)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator unsigned char(void)
{
	unsigned char retVal = 0;
	switch(varType)
	{	
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (unsigned char)1;
		}
		break;
	case RUL_CHAR:
		retVal = (unsigned char)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (unsigned char)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (unsigned char)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (unsigned char)val.usValue;
		break;
	case RUL_INT:
		retVal = (unsigned char)val.nValue;
		break;
	case RUL_UINT:
		retVal = (unsigned char)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (unsigned char) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (unsigned char) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (unsigned char) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (unsigned char) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (unsigned char)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator double()
{
	double retVal = 0.0;
	switch(varType)
	{	
	case RUL_BOOL:
		if (val.bValue )
		{
			retVal = 1.0;
		} 
		break;
	case RUL_CHAR:
		retVal = (double)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (double)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (double)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (double)val.usValue;
		break;
	case RUL_INT:
		retVal = (double)val.nValue;
		break;
	case RUL_UINT:
		retVal = (double)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (double) val.lValue;
		break;
	case RUL_ULONGLONG:
        retVal = (double) ((_UINT64)val.ulValue);
		break;
	case RUL_FLOAT:
		retVal =  (double) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (double)0.0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator short(void)
{
	short retVal = 0;
	switch(varType)
	{	
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (short)1;
		}
		break;
	case RUL_CHAR:
		retVal = (short)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (short)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (short)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (short)val.usValue;
		break;
	case RUL_INT:
		retVal = (short)val.nValue;
		break;
	case RUL_UINT:
		retVal = (short)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (short) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (short) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (short) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (short) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (short)0; 
		break;
	}// end switch
	return retVal;

}
INTER_VARIANT::operator wchar_t(void)	// aka unsigned short() 
{
	wchar_t retVal = '\0';

	retVal = (unsigned short) *this;

	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator unsigned short(void)	// aka unsigned short() 
{
	unsigned short retVal = 0;
	switch(varType)
	{
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (unsigned short)1;
		}
		break;	
	case RUL_CHAR:
		retVal = (unsigned short)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (unsigned short)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (unsigned short)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (unsigned short)val.usValue;
		break;
	case RUL_INT:
		retVal = (unsigned short)val.nValue;
		break;
	case RUL_UINT:
		retVal = (unsigned short)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (unsigned short) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (unsigned short) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (unsigned short) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (unsigned short) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (unsigned short)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator __int64(void)
{
	__int64 retVal = 0;
	switch(varType)
	{
	case RUL_BOOL:
		if( val.bValue )
		{
			retVal = (__int64)1;
		}
		break;	
	case RUL_CHAR:
		retVal = (__int64)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retVal = (__int64)val.ucValue;
		break;
	case RUL_SHORT:
		retVal = (__int64)val.sValue;
		break;
	case RUL_USHORT:
		retVal = (__int64)val.usValue;
		break;
	case RUL_INT:
		retVal = (__int64)val.nValue;
		break;
	case RUL_UINT:
		retVal = (__int64)val.unValue;
		break;
	case RUL_LONGLONG:
		retVal = (__int64) val.lValue;
		break;
	case RUL_ULONGLONG:
		retVal = (__int64) val.ulValue;
		break;
	case RUL_FLOAT:
		retVal =  (__int64) val.fValue;
		break;
	case RUL_DOUBLE:
		retVal =  (__int64) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
		retVal = (__int64)0; 
		break;
	}// end switch
	return retVal;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
INTER_VARIANT::operator _UINT64(void)
{
    _UINT64 retVal = 0;
	switch(varType)
	{
	case RUL_BOOL:
		if( val.bValue )
		{
            retVal = (_UINT64)1;
		}
		break;	
	case RUL_CHAR:
        retVal = (_UINT64)val.cValue;
		break;
	case RUL_UNSIGNED_CHAR:
        retVal = (_UINT64)val.ucValue;
		break;
	case RUL_SHORT:
        retVal = (_UINT64)val.sValue;
		break;
	case RUL_USHORT:
        retVal = (_UINT64)val.usValue;
		break;
	case RUL_INT:
        retVal = (_UINT64)val.nValue;
		break;
	case RUL_UINT:
        retVal = (_UINT64)val.unValue;
		break;
	case RUL_LONGLONG:
        retVal = (_UINT64) val.lValue;
		break;
	case RUL_ULONGLONG:
        retVal = (_UINT64) val.ulValue;
		break;
	case RUL_FLOAT:
        retVal =  (_UINT64) val.fValue;
		break;
	case RUL_DOUBLE:
        retVal =  (_UINT64) val.dValue;
		break;

	/* for now, all strings will NOT cast to a const */
	default:
        retVal = (_UINT64)0;
		break;
	}// end switch
	return retVal;
}

/********** casting strings is not currently supported ******************************************
INTER_VARIANT::operator char *() 
{
	//Vibhor 240204: Donno if this is also required
	// stevev changed 20dec07  return val.pszValue;
	wstring tc(val.pszValue);
	charout = TStr2AStr(tc);
	strcpy((char*)val.pszValue,charout.c_str());
	return ((char*)val.pszValue);
}

INTER_VARIANT::operator wchar_t*() 
{
	//Vibhor 240204: Donno if this is also required
	return val.pszValue;
}

INTER_VARIANT::operator _BYTE_STRING(void)
{
}
************************** end of string casting ************************************************/






/** operations ******** operations ******** operations ******** operations ******** operations **/




//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator+(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;
	if(isNumeric() && temp.isNumeric())
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			if( 
                ( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) ) ||
				( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) != _FPCLASS_NINF) ) ||
				( (_fpclass( (double)*this ) != _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) )
			  )
            {
                _UINT64 ulVal = DOUBLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else if( ( (_fpclass( (double)*this ) == _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_NINF) ) )
			{
                _UINT64 ulVal = (0x8000000000000000) | DOUBLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else if
			(
				( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) == _FPCLASS_NINF) ) ||
				( (_fpclass( (double)*this ) == _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) )	
			)
			{

				#ifdef _WIN64
                    _UINT64 ulVal = DOUBLE_POS_QUIET_NAN_VALUE;				//default NaN
				    retValue = (*reinterpret_cast<double*>(&ulVal));
				#else
                   _UINT64 ulVal = DOUBLE_POS_SIGNAL_NAN_VALUE;				//default NaN
				   retValue = (*reinterpret_cast<double*>(&ulVal));
				#endif
			}
			else
			{
				retValue = (double)*this + (double)temp;
			}
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			if( 
				( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) ) ||
				( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) != _FPCLASS_NINF) ) ||
				( (_fpclass( (float)*this ) != _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) )
			  )
			{
                _UINT32 ulVal = SINGLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ( (_fpclass( (float)*this ) == _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_NINF) ) )
			{
                _UINT32 ulVal = (0x80000000) | SINGLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if
			(
				( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) == _FPCLASS_NINF) ) ||
				( (_fpclass( (float)*this ) == _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) )	
			)
			{
                _UINT32 ulVal = SINGLE_INF_DIVBY_INF_NAN;				//default NaN
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else
			{
				retValue = (float)*this + (float)temp;
			}
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this + (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this + (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this + (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this + (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this + (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this + (short)temp;
		}// end switch
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this + (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{// not both - we can't do this...
			retValue = (char)*this + (char)temp;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)*this & (bool)temp;
		}
	}
	else if(!isNumeric() && !temp.isNumeric())  //we are to strings of some sort
	{
		if(  
			(((varType == RUL_DD_STRING)||(varType == RUL_WIDECHARPTR)) && var.varType == RUL_CHARPTR)
			||
			(((var.varType == RUL_DD_STRING)||(var.varType == RUL_WIDECHARPTR)) && varType == RUL_CHARPTR)
			)
		{// narrow to wide conversion
			INTER_VARIANT widend, wideAlready;
			bool widendFirst = true;
			widend.varType = RUL_DD_STRING;
			if (var.varType == RUL_CHARPTR)
			{
				widend      =  var;  // convert to DD_STRING
				wideAlready = (*this);
				widendFirst = false;
			}
			else
			{
				widend      =  (*this);  // convert to DD_STRING
				wideAlready = var;
				widendFirst = true;
			}
			retValue.varType = RUL_DD_STRING;
			StripLangCode(widend.val.pszValue);
			StripLangCode(wideAlready.val.pszValue);

			size_t nLen1 = _tstrlen(widend.val.pszValue);
			size_t nLen2 = _tstrlen(wideAlready.val.pszValue)+1;
			retValue.val.pszValue = new tchar[nLen1+nLen2+1];
			memset(retValue.val.pszValue,0,sizeof(tchar)*(nLen1+nLen2+1));

			if (widendFirst)
			{
				wcscpy( retValue.val.pszValue, widend.val.pszValue );			
				wcscat( retValue.val.pszValue, wideAlready.val.pszValue );
			}
			else
			{
				wcscpy( retValue.val.pszValue, wideAlready.val.pszValue );			
				wcscat( retValue.val.pszValue, widend.val.pszValue );
			}
					
		}
		//Added By Stevev 20dec07 --starts here
		else if( 
				((varType == RUL_DD_STRING) || (varType == RUL_WIDECHARPTR)) 
				&&
				((var.varType == RUL_WIDECHARPTR)||(var.varType == RUL_DD_STRING)) 
			   )
		{
			retValue.varType = RUL_DD_STRING;

			StripLangCode(val.pszValue);
			size_t nLen1 = _tstrlen(val.pszValue);

			StripLangCode(var.val.pszValue);
			size_t nLen2 = _tstrlen(var.val.pszValue)+1;

			retValue.val.pszValue = new tchar[nLen1+nLen2+1];
			memset(retValue.val.pszValue,0,sizeof(tchar)*(nLen1+nLen2+1));

			wcscpy( retValue.val.pszValue, val.pszValue );			
			wcscat( retValue.val.pszValue, var.val.pszValue );		
		}
		else if ((varType == RUL_CHARPTR)  &&(var.varType == RUL_CHARPTR))
		{
			retValue.varType = RUL_CHARPTR;
			size_t nLen1 = strlen(val.pzcVal);
			//Remove the Language Code if it is Present:This is Required as Both temp and var has Language Code
			//I am not Sure Whether I need to be support this???????????????????
			if( (var.val.pzcVal[0] == '|') &&  (var.val.pzcVal[3] == '|' ) )
			{
				size_t count, itemp = strlen(var.val.pzcVal);// WS - 9apr07 - 2005 checkin
				for(count = 4; count <itemp ;count++)// WS - 9apr07 - 2005 checkin
				{
					var.val.pzcVal[count-4] = var.val.pzcVal[count];
				}
				var.val.pzcVal[count-4] = '\0';
			
			}
			//Added By Anil June 16 2005 --Ends here
			size_t nLen2 = strlen(var.val.pzcVal)+1;
			retValue.val.pzcVal = new _CHAR[nLen1+nLen2+1];
			memset(retValue.val.pzcVal,0,nLen1+nLen2+1);

			memcpy(retValue.val.pzcVal,val.pszValue, nLen1*sizeof(_CHAR));
			memcpy(retValue.val.pzcVal+nLen1,var.val.pzcVal,nLen2*sizeof(_CHAR));
			retValue.varType = varType;
		}
		else if ((varType == RUL_BYTE_STRING)  &&(var.varType == RUL_BYTE_STRING))
		{
			size_t nLen1 = val.bString.bsLen;
			size_t nLen2 = var.val.bString.bsLen;

			retValue.val.bString.bs = new _UCHAR[nLen1 + nLen2];
			memset(retValue.val.bString.bs, 0, nLen1 + nLen2);
		
			memcpy(retValue.val.bString.bs, val.bString.bs, nLen1*sizeof(_UCHAR));
			
			memcpy(retValue.val.bString.bs + nLen1, var.val.bString.bs, nLen2*sizeof(_UCHAR));
			
			retValue.varType = varType;
			retValue.val.bString.bsLen = nLen1 + nLen2;
			
		}
		// Walt EPM - 05sep08 - add
		else if ((varType == RUL_DD_STRING)  &&(var.varType == RUL_SAFEARRAY))
		{//TODO test if this is a bug when var.varType is type RUL_CHAR
			retValue.varType = RUL_DD_STRING;

			size_t nLen1 = _tstrlen((wchar_t *)*var.val.prgsa);

			StripLangCode(val.pszValue);
			size_t nLen2 = _tstrlen(val.pszValue)+1;

			retValue.val.pszValue = new tchar[nLen1+nLen2+1];
			memset(retValue.val.pszValue,0,sizeof(tchar)*(nLen1+nLen2+1));

			wcscpy( retValue.val.pszValue, val.pszValue );			
			wcscat( retValue.val.pszValue, (wchar_t *)*var.val.prgsa );
		}
		// Walt EPM - 05sep08 - add
		else if ((varType == RUL_CHARPTR)  &&(var.varType == RUL_SAFEARRAY))
		{
			retValue.varType = RUL_CHARPTR;
			
			INTER_VARIANT narrowEnd;
			narrowEnd.varType = RUL_CHARPTR;
			narrowEnd = var;

			size_t nLen1 = strlen(narrowEnd.val.pzcVal);

			size_t nLen2 = strlen(val.pzcVal)+1;

			retValue.val.pzcVal = new _CHAR[nLen1+nLen2+1];
			memset(retValue.val.pzcVal,0,(nLen1+nLen2+1));

			strcpy( retValue.val.pzcVal, val.pzcVal );			
			strcat( retValue.val.pzcVal, narrowEnd.val.pzcVal );
		}
		else if (varType == RUL_SAFEARRAY) 
		{//i could have overload safe array but that was more work... or not
			//retValue.varType = RUL_SAFEARRAY;
			INTER_SAFEARRAY *pSA = GetSafeArray();
			switch (pSA->Type())
			{
			case RUL_CHAR:
			{//we are narrow safe array
				switch (var.varType)
				{
					case RUL_CHARPTR:
					{
						retValue.varType =RUL_CHARPTR;	
						char* pBuf = NULL;
						GetStringValue(&pBuf, RUL_CHARPTR);
						size_t nLen1 = strlen(var.val.pzcVal);
						size_t nLen2 = strlen(pBuf);
						retValue.val.pzcVal = new _CHAR[nLen1+nLen2+1];
						memset(retValue.val.pzcVal,0,(nLen1+nLen2+1));
						strcpy( retValue.val.pzcVal, pBuf);			
						strcat( retValue.val.pzcVal, temp.val.pzcVal);
						delete pBuf;
						pBuf = NULL;
					}
					break;
					case RUL_DD_STRING:
					case RUL_WIDECHARPTR:
					{//it appears that the desire is to upcast to a wide... look above a few if's at how they handle RUL_CHARPTR with DD_STRING and wideptr
						retValue.varType = var.varType ;
				

						size_t nLen1 = _tstrlen(var.val.pszValue);
						wchar_t *pwStr = (wchar_t *)(*pSA);
						size_t nLen2 = _tstrlen(pwStr);

						retValue.val.pszValue = new tchar[nLen1+nLen2+1];
						memset(retValue.val.pszValue,0,sizeof(tchar)*(nLen1+nLen2+1));

						wcscpy( retValue.val.pszValue, pwStr );			
						wcscat( retValue.val.pszValue, var.val.pszValue );
					}
					break;
					case RUL_SAFEARRAY:
					{
						retValue.varType = var.varType;

						INTER_SAFEARRAY *pSAvar = temp.GetSafeArray();
						INTER_SAFEARRAY SAvar = *pSAvar;
                        INTER_SAFEARRAY *pSAthis = GetSafeArray();
                        INTER_SAFEARRAY SAthis = *pSAthis;
#ifdef _WIN32
						INTER_SAFEARRAY& pSAret = (SAthis + SAvar);
#else
						INTER_SAFEARRAY& pSAret = SAthis;
                        pSAret.Add(SAvar);
#endif
                        retValue.val.prgsa = new INTER_SAFEARRAY(pSAret);
                    }
					break;
				}
			}
			break;
			case RUL_USHORT:
			{//we are wide safe array
				switch (var.varType)
				{
					case RUL_CHARPTR:
					{
						retValue.varType = RUL_CHARPTR;

						char* pBuf = NULL;
						GetStringValue(&pBuf, RUL_CHARPTR);

						size_t nLen1 = strlen(var.val.pzcVal);
						size_t nLen2 = strlen(pBuf);

						retValue.val.pzcVal = new _CHAR[nLen1 + nLen2 + 1];
						memset(retValue.val.pzcVal, 0, (nLen1 + nLen2 + 1));
						strcpy(retValue.val.pzcVal, pBuf);
						strcat(retValue.val.pzcVal, temp.val.pzcVal);

						delete pBuf;
						pBuf = NULL;
					}
					break;
					case RUL_DD_STRING:
					case RUL_WIDECHARPTR:
					{//it appears that the desire is to upcast to a wide... look above a few if's at how they handle RUL_CHARPTR with DD_STRING and wideptr
						retValue.varType = var.varType;

						size_t nLen1 = _tstrlen(var.val.pszValue);

						wchar_t *pWideString = NULL;
						GetStringValue(&pWideString, false);
						size_t nLen2 = _tstrlen(pWideString);

						retValue.val.pszValue = new tchar[nLen1 + nLen2 + 1];
						memset(retValue.val.pszValue, 0, sizeof(tchar)*(nLen1 + nLen2 + 1));

						wcscpy(retValue.val.pszValue, pWideString);
						wcscat(retValue.val.pszValue, var.val.pszValue);

						if (pWideString != NULL)
						{
							delete[] pWideString;//clean up memory
						}
					}
					break;
					case RUL_SAFEARRAY:
						retValue.varType = var.varType;

						INTER_SAFEARRAY *pSAvar = temp.GetSafeArray();
						INTER_SAFEARRAY SAvar = *pSAvar;
						INTER_SAFEARRAY *pSAthis = GetSafeArray();
                        INTER_SAFEARRAY SAthis = *pSAthis;
#ifdef _WIN32
						INTER_SAFEARRAY& pSAret = (SAthis + SAvar);
#else
						INTER_SAFEARRAY& pSAret = SAthis;
                        pSAret.Add(SAvar);
#endif
                        retValue.val.prgsa = new INTER_SAFEARRAY(pSAret);
                        break;
				}
			}
			break;
			default:
				break;
			}
		}
	}
	else //we have a combo platter of string and numeric
	{
		INTER_VARIANT alphaNumber; 
		if(isNumeric())//
		{	
			if( (varType == RUL_DOUBLE) || (varType == RUL_FLOAT) )
			{
				float fValue = (float)*this;	
				char chAlpha[MAX_DD_STRING]={0};
				if(sprintf(chAlpha, "%f", fValue) > -1 )
				{
					alphaNumber.SetValue(chAlpha, RUL_CHARPTR);
					retValue = alphaNumber + var;
				}
				else
				{
					//error?
				}
			}
			else
			{
				int nValue  = (int)*this;
				char chNumber[50]={0};// bigger then any possible int
				PS_Itoa_s(nValue, chNumber, sizeof(chNumber), 10);
				alphaNumber.SetValue(&chNumber, RUL_CHARPTR);
				retValue = alphaNumber + var;
			}
							
		}
		else //the passed in parameter is a number
		{
			INTER_VARIANT meVariant = *this;
			if( (var.varType == RUL_DOUBLE) || (var.varType == RUL_FLOAT) )
			{
				float fValue = (float)temp;	
				char chAlpha[MAX_DD_STRING]={0};
				if(sprintf(chAlpha, "%f", fValue) > -1 )
				{
					alphaNumber.SetValue(chAlpha, RUL_CHARPTR);
					retValue = meVariant + alphaNumber;
				}
			}
			else
			{
				int nValue  = (int)temp;
				char chNumber[50]={0};// bigger then any possible int
				PS_Itoa_s(nValue, chNumber, sizeof(chNumber), 10);
				alphaNumber.SetValue(&chNumber, RUL_CHARPTR);
				retValue = meVariant + alphaNumber;
			}
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator-(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			if(
				( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) ) ||
				( (_fpclass( (double)*this ) == _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_NINF) )
			  )
			{
				#ifdef _WIN64
                    _UINT64 ulVal = DOUBLE_POS_QUIET_NAN_VALUE;				//default NaN
				    retValue = (*reinterpret_cast<double*>(&ulVal));
				#else
                   _UINT64 ulVal = DOUBLE_POS_SIGNAL_NAN_VALUE;				//default NaN
				   retValue = (*reinterpret_cast<double*>(&ulVal));
				#endif
			}
			else if( ( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) == _FPCLASS_NINF) ) )
			{
                _UINT64 ulVal = DOUBLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else if( ( (_fpclass( (double)*this ) == _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) ) )
			{
                _UINT64 ulVal = (0x8000000000000000) | DOUBLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else
			{
				retValue = (double)*this - (double)temp;
			}
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			if(
				( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) ) ||
				( (_fpclass( (float)*this ) == _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_NINF) )
			  )
			{
                _UINT32 ulVal = SINGLE_INF_DIVBY_INF_NAN;				//default NaN
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) == _FPCLASS_NINF) ) )
			{
                _UINT32 ulVal = SINGLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ( (_fpclass( (float)*this ) == _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) ) )
			{
                _UINT32 ulVal = (0x80000000) | SINGLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else
			{
				retValue = (float)*this - (float)temp;
			}
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this - (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this - (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this - (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this - (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this - (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this - (short)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this - (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (char)*this - (char)temp;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)*this - (bool)temp;
		}
	}

	return retValue;	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator*(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			if(
				(   (  ((double)*this == 0.0)  &&  (_fpclass( (double)temp ) ==  _FPCLASS_NINF) )  )   ||
				(   (  (_fpclass( (double)*this ) == _FPCLASS_NINF)  &&  ((double)temp == 0.0)  )  )   ||
				(   (  ((double)*this == 0.0)  &&  (_fpclass( (double)temp ) == _FPCLASS_PINF) )  )   ||
				(   (  (_fpclass( (double)*this ) == _FPCLASS_PINF)  &&  ((double)temp == 0.0)  )  ) 
			  )
			{
				#ifdef _WIN64
                    _UINT64 ulVal = DOUBLE_POS_QUIET_NAN_VALUE;				//default NaN
				    retValue = (*reinterpret_cast<double*>(&ulVal));
				#else
                   _UINT64 ulVal = DOUBLE_POS_SIGNAL_NAN_VALUE;				//default NaN
				   retValue = (*reinterpret_cast<double*>(&ulVal));
				#endif
			}
			else if( ( (_fpclass( (double)*this ) == _FPCLASS_PINF) && (_fpclass( (double)temp ) == _FPCLASS_PINF) ) ||
				     ( (_fpclass( (double)*this ) == _FPCLASS_NINF) && (_fpclass( (double)temp ) == _FPCLASS_NINF) )
				   )
			{
                _UINT64 ulVal = DOUBLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else if(
				( (_fpclass( (double)*this ) == _FPCLASS_PINF) && ((double)temp > 0.0) ) ||
				( (_fpclass( (double)*this ) == _FPCLASS_NINF) && ((double)*this > 0.0) )
				)
			{
                _UINT64 ulVal = (0x8000000000000000) | DOUBLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else
			{
			    retValue = (double)*this * (double)temp;
			}
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			if(
				(   (  ((float)*this == 0.0)  &&  (_fpclass( (float)temp ) ==  _FPCLASS_NINF) ) )   ||
				(   (  (_fpclass( (float)*this ) == _FPCLASS_NINF)  &&  ((float)temp == 0.0)  ) )   ||
				(   (  ((float)*this == 0.0)  &&  (_fpclass( (float)temp ) == _FPCLASS_PINF)  ) )   ||
				(   (  (_fpclass( (float)*this ) == _FPCLASS_PINF)  &&  ((float)temp == 0.0)  ) ) 
			  )
			{
                   _UINT32 ulVal = SINGLE_INF_DIVBY_INF_NAN;				//default NaN
				   retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ( (_fpclass( (float)*this ) == _FPCLASS_PINF) && (_fpclass( (float)temp ) == _FPCLASS_PINF) ) ||
				     ( (_fpclass( (float)*this ) == _FPCLASS_NINF) && (_fpclass( (float)temp ) == _FPCLASS_NINF) )
				   )
			{
                _UINT32 ulVal = SINGLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( 
				( (_fpclass( (float)*this ) == _FPCLASS_NINF) && ((float)temp > 0.0) ) ||
				( (_fpclass( (float)temp ) == _FPCLASS_NINF) && ((float)*this > 0.0) )     
			)
			{
                _UINT32 ulVal = (0x80000000) | SINGLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else
			{
				retValue = (float)*this * (float)temp;
			}
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this * (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this * (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this * (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this * (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this * (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this * (short)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this * (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (char)*this * (char)temp;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)*this - (bool)temp;
		}
	}

	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator/(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			double dDenominator = (double)temp;

			if
			(
			  ( ((double)*this == 0.0) && ( dDenominator == 0.0) ) ||
			  ( (_fpclass( (double)*this ) == _FPCLASS_PINF) && ( _fpclass(dDenominator) == _FPCLASS_PINF) ) ||
			  ( (_fpclass( (double)*this ) == _FPCLASS_NINF) && ( _fpclass(dDenominator) == _FPCLASS_PINF) ) ||
			  ( (_fpclass( (double)*this ) == _FPCLASS_PINF) && ( _fpclass(dDenominator) == _FPCLASS_NINF) ) ||
			  ( (_fpclass( (double)*this ) == _FPCLASS_NINF) && ( _fpclass(dDenominator) == _FPCLASS_NINF) )
			)
			{
				#ifdef _WIN64
                    _UINT64 ulVal = DOUBLE_POS_QUIET_NAN_VALUE;				//default NaN
				    retValue = (*reinterpret_cast<double*>(&ulVal));
				#else
                   _UINT64 ulVal = DOUBLE_POS_SIGNAL_NAN_VALUE;				//default NaN
				   retValue = (*reinterpret_cast<double*>(&ulVal));
				#endif
			}
			else if( ((double)*this > 0.0) && (_fpclass(dDenominator) == _FPCLASS_PINF) ||			//*this value won't be INF/-INF here because of previous check
				((double)*this < 0.0) && (_fpclass(dDenominator) == _FPCLASS_NINF) )
			{
				retValue = (double)0.0;
			}
			else if( ((double)*this > 0.0) && (_fpclass(dDenominator) == _FPCLASS_NINF) ||
				((double)*this < 0.0) && (_fpclass(dDenominator) == _FPCLASS_PINF) )
			{
				retValue = (double)-0.0;
			}
			else if( ((double)*this > 0.0) && (dDenominator == 0.0) )
			{
                _UINT64 ulVal = DOUBLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else if( ((double)*this < 0.0) && (dDenominator == 0.0) )
			{
                _UINT64 ulVal = (0x8000000000000000) | DOUBLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<double*>(&ulVal));
			}
			else
			{
				retValue = (double)*this / dDenominator;
			}
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			float fDenominator = (float)temp;

			if
			(
			  ( ((float)*this == 0.0) && ( fDenominator == 0.0) ) ||
			  ( (_fpclass( (float)*this ) == _FPCLASS_PINF) && ( _fpclass(fDenominator) == _FPCLASS_PINF) ) ||
			  ( (_fpclass( (float)*this ) == _FPCLASS_NINF) && ( _fpclass(fDenominator) == _FPCLASS_PINF) ) ||
			  ( (_fpclass( (float)*this ) == _FPCLASS_PINF) && ( _fpclass(fDenominator) == _FPCLASS_NINF) ) ||
			  ( (_fpclass( (float)*this ) == _FPCLASS_NINF) && ( _fpclass(fDenominator) == _FPCLASS_NINF) )
			)
			{
                _UINT32 ulVal = SINGLE_INF_DIVBY_INF_NAN;				//default NaN
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ((float)*this > 0.0) && (_fpclass(fDenominator) == _FPCLASS_PINF) ||			//*this value won't be INF/-INF here because of previous check
				((float)*this < 0.0) && (_fpclass(fDenominator) == _FPCLASS_NINF) )
			{
				retValue = (float)0.0;
			}
			else if( ((float)*this > 0.0) && (_fpclass(fDenominator) == _FPCLASS_NINF) ||
				((float)*this < 0.0) && (_fpclass(fDenominator) == _FPCLASS_PINF) )
			{
				retValue = (float)-0.0;
			}
			else if( ((float)*this > 0.0) && (fDenominator == 0.0) )
			{
                _UINT32 ulVal = SINGLE_INFINITY_VALUE;					//INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else if( ((float)*this < 0.0) && (fDenominator == 0.0) )
			{
                _UINT32 ulVal = (0x80000000) | SINGLE_INFINITY_VALUE;	//-INF
				retValue = (*reinterpret_cast<float*>(&ulVal));
			}
			else
			{
				retValue = (float)*this / fDenominator;
			}

		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            _UINT64 ullDenominator = (_UINT64)temp;
			if( ullDenominator != 0 )
			{
                retValue = (_UINT64)*this / ullDenominator;
			}
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			__int64 llDenominator = (__int64)temp;
			if( llDenominator != 0 )
			{
				retValue = (__int64)*this / llDenominator;
			}
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			unsigned int nDenominator = (unsigned int)temp;
			if( nDenominator != 0 )
			{
				retValue = (unsigned int)*this / nDenominator;
			}
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			int nDenominator = (int)temp;
			if( nDenominator != 0 )
			{
				retValue = (int)*this / nDenominator;
			}
		
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			int usDenominator = (wchar_t)temp;
			if( usDenominator != 0 )
			{
				retValue = (wchar_t)*this / usDenominator;
			}
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			int sDenominator = (short)temp;
			if( sDenominator != 0 )
			{
				retValue = (short)*this / sDenominator;
			}
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			int ucDenominator = (unsigned char)temp;
			if( ucDenominator != 0 )
			{
				retValue = (unsigned char)*this / ucDenominator;
			}
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			int cDenominator = (char)temp;
			if( cDenominator != 0 )
			{
				retValue = (char)*this / cDenominator;
			}
		}
	}

	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator%(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;
	
	
	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            _UINT64 ullDenominator = (_UINT64)temp;
			if( ullDenominator != 0 )
			{
                retValue = (_UINT64)*this % ullDenominator;
			}
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			__int64 llDenominator = (__int64)temp;
			if( llDenominator != 0 )
			{
				retValue = (__int64)*this % llDenominator;
			}
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			unsigned int unDenominator = (unsigned int)temp;
			if( unDenominator != 0 )
			{
				retValue = (unsigned int)*this % unDenominator;
			}
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			int nDenominator = (int)temp;
			if( nDenominator != 0 )
			{
				retValue = (int)*this % nDenominator;
			}
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			wchar_t usDenominator = (wchar_t)temp;
			if( usDenominator != 0 )
			{
				retValue = (wchar_t)*this % usDenominator;
			}
	
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			short sDenominator = (short)temp;
			if( sDenominator != 0 )
			{
				retValue = (short)*this % sDenominator;
			}
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			unsigned char ucDenominator = (unsigned char)temp;
			if( ucDenominator != 0 )
			{
				retValue = (unsigned char)*this % ucDenominator;
			}
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			char cDenominator = (char)temp;
			if( cDenominator != 0 )
			{
				retValue = (char)*this % cDenominator;
			}
		}
	}
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator&(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;	
	
	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this & (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this & (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this & (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this & (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this & (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this & (short)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this & (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (char)*this & (char)temp;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)*this & (bool)temp;
		}
	}
	return retValue;	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator|(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this | (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this | (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this | (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this | (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this | (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this | (short)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this | (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (char)*this | (char)temp;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)*this | (bool)temp;
		}
	}
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator^(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;	
	
	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this ^ (_UINT64)temp;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this ^ (__int64)temp;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this ^ (unsigned int)temp;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (int)*this ^ (int)temp;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this ^ (wchar_t)temp;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (short)*this ^ (short)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this ^ (unsigned char)temp;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (char)*this ^ (char)temp;
		}
	}
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator~()
{
	INTER_VARIANT retValue;
		
	switch(varType)
	{
	case RUL_ULONGLONG:
		retValue = ~val.ulValue;
		break;
	case RUL_LONGLONG:
		retValue = ~val.lValue;
		break;
	case RUL_UINT:
		retValue = (unsigned int)~val.unValue;
		break;
	case RUL_INT:
        retValue = (int)~val.nValue;
		break;
	case RUL_USHORT:
		retValue = ~val.usValue;
		break;
	case RUL_SHORT:
		retValue = ~val.sValue;
		break;
	case RUL_UNSIGNED_CHAR:
		retValue = ~val.ucValue;
		break;
	case RUL_CHAR:
		retValue = ~val.cValue;
		break;
	case RUL_BOOL:
		retValue = !val.bValue;
		break;
	}

	return retValue;	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator>>(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG))
		{
            retValue = (_UINT64)*this >> (int)temp;
		}
		else if( (varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this >> (int)temp;
		}
		else if( (varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this >> (int)temp;
		}
		else if( (varType == RUL_INT) )
		{
			retValue = (int)*this >> (int)temp;
		}
		else if( (varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this >> (int)temp;
		}
		else if( (varType == RUL_SHORT) )
		{
			retValue = (short)*this >> (int)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this >> (int)temp;
		}
		else if( (varType == RUL_CHAR) )
		{
			retValue = (char)*this >> (int)temp;
		}
	}
	return retValue;	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator<<(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_ULONGLONG) )
		{
            retValue = (_UINT64)*this << (int)temp;
		}
		else if( (varType == RUL_LONGLONG) )
		{
			retValue = (__int64)*this << (int)temp;
		}
		else if( (varType == RUL_UINT) )
		{
			retValue = (unsigned int)*this << (int)temp;
		}
		else if( (varType == RUL_INT) )
		{
			retValue = (int)*this << (int)temp;
		}
		else if( (varType == RUL_USHORT) )
		{
			retValue = (wchar_t)*this << (int)temp;
		}
		else if( (varType == RUL_SHORT) )
		{
			retValue = (short)*this << (int)temp;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (unsigned char)*this << (int)temp;
		}
		else if( (varType == RUL_CHAR) )
		{
			retValue = (char)*this << (int)temp;
		}
	}
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator!=(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;
	
	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this != (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this != (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this != (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this != (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this != (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this != (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this != (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this != (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this != (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this != (char)temp)?true:false;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)((bool)*this != (bool)temp)?true:false;
		}
		else
		{
			retValue = (bool)true;
		}
	}
	else if( (varType == RUL_BYTE_STRING) && (var.varType == RUL_BYTE_STRING) )
	{
		size_t size1 = GetMaxSize();
		size_t size2 = temp.GetMaxSize();

		if (size2 != size1)
		{
			retValue = (bool)true;
		}
		else
		{
			uchar *pMem1 = new uchar[size1];
			GetValue(pMem1, varType);

			uchar *pMem2 = new uchar[size2];
			temp.GetValue(pMem2, var.varType);

			//comparison
			retValue = (bool)false;
			for (unsigned long i = 0; i < size1; i++)
			{
				if (pMem1[i] != pMem2[i])
				{
					retValue = (bool)true;
					break;
				}
			}

			delete [] pMem1;
			delete [] pMem2;
		}
	}
	else //string compares
	{
		wstring str1;
		wstring str2;
		bool bIsString = true;

		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
			default:
				bIsString = false;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
			default:
				bIsString = false;
				break;
		}

		if (bIsString)
		{
			if( wcscmp(str1.c_str(), str2.c_str()) == 0 )
			{
				retValue = (bool)false;
			}
			else
			{
				retValue = (bool)true;
			}
		}
		else
		{
			retValue = (bool)true;
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator==(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )	
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this == (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this == (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this == (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this == (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this == (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this == (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this == (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this == (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this == (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this == (char)temp)?true:false;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)((bool)*this == (bool)temp)?true:false;
		}
		else
		{
			retValue = (bool)false;
		}
	}
	else if( (varType == RUL_BYTE_STRING) && (var.varType == RUL_BYTE_STRING) )
	{
		size_t size1 = GetMaxSize();
		size_t size2 = temp.GetMaxSize();

		if (size2 != size1)
		{
			retValue = (bool)false;
		}
		else
		{
			uchar *pMem1 = new uchar[size1];
			GetValue(pMem1, varType);

			uchar *pMem2 = new uchar[size2];
			temp.GetValue(pMem2, var.varType);

			//comparison
			retValue = (bool)true;
			for (unsigned long i = 0; i < size1; i++)
			{
				if (pMem1[i] != pMem2[i])
				{
					retValue = (bool)false;
					break;
				}
			}

			delete [] pMem1;
			delete [] pMem2;
		}
	}
	else//todo string compares
	{
		// Walt EPM - 05sep08 - add
		wstring str1;
		wstring str2;
		bool bIsString = true;

		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
			default:
				bIsString = false;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
			default:
				bIsString = false;
				break;
		}

		if (bIsString)
		{
			if( wcscmp(str1.c_str(), str2.c_str())== 0 )
			{
				retValue = (bool)true;
			}
			else
			{
				retValue = (bool)false;
			}
		}
		else
		{
			retValue = (bool)false;
		}
		// Walt EPM - 05sep08 - end
	}
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator<(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this < (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this < (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this < (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this < (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this < (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this < (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this < (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this < (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this < (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this < (char)temp)?true:false;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)((bool)*this < (bool)temp)?true:false;
		}
	}
	// Walt EPM - 05sep08 -  add
	else //string compares
	{
		wstring str1;
		wstring str2;
		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
		}
		if( wcscmp(str1.c_str(), str2.c_str()) < 0 )
		{
			retValue = (bool)true;
		}
		else
		{
			retValue = (bool)false;
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator>(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this > (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this > (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this > (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this > (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this > (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this > (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this > (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this > (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this > (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this > (char)temp)?true:false;
		}
		else if( (varType == RUL_BOOL) || (var.varType == RUL_BOOL) )
		{
			retValue = (bool)((bool)*this > (bool)temp)?true:false;
		}
	}
	// Walt EPM - 05sep08 - add
	else //string compares
	{
		wstring str1;
		wstring str2;
		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
		}
		if( wcscmp(str1.c_str(), str2.c_str()) > 0 )
		{
			retValue = (bool)true;
		}
		else
		{
			retValue = (bool)false;
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator<=(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this <= (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this <= (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this <= (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this <= (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this <= (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this <= (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this <= (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this <= (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this <= (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this <= (char)temp)?true:false;
		}
	}
	// Walt EPM - 05sep08 - add
	else //string compares
	{
		wstring str1;
		wstring str2;
		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
		}
		if( wcscmp(str1.c_str(), str2.c_str()) <= 0 )
		{
			retValue = (bool)true;
		}
		else
		{
			retValue = (bool)false;
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator>=(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;

	if( isNumeric() && temp.isNumeric() )
	{
		if( (varType == RUL_DOUBLE) || (var.varType == RUL_DOUBLE) )
		{
			retValue = (bool)((double)*this >= (double)temp)?true:false;
		}
		else if( (varType == RUL_FLOAT) || (var.varType == RUL_FLOAT) )
		{
			retValue = (bool)((float)*this >= (float)temp)?true:false;
		}
		else if( (varType == RUL_ULONGLONG) || (var.varType == RUL_ULONGLONG) )
		{
            retValue = (bool)((_UINT64)*this >= (_UINT64)temp)?true:false;
		}
		else if( (varType == RUL_LONGLONG) || (var.varType == RUL_LONGLONG) )
		{
			retValue = (bool)((__int64)*this >= (__int64)temp)?true:false;
		}
		else if( (varType == RUL_UINT) || (var.varType == RUL_UINT) )
		{
			retValue = (bool)((unsigned int)*this >= (unsigned int)temp)?true:false;
		}
		else if( (varType == RUL_INT) || (var.varType == RUL_INT) )
		{
			retValue = (bool)((int)*this >= (int)temp)?true:false;
		}
		else if( (varType == RUL_USHORT) || (var.varType == RUL_USHORT) )
		{
			retValue = (bool)((wchar_t)*this >= (wchar_t)temp)?true:false;
		}
		else if( (varType == RUL_SHORT) || (var.varType == RUL_SHORT) )
		{
			retValue = (bool)((short)*this >= (short)temp)?true:false;
		}
		else if( (varType == RUL_UNSIGNED_CHAR) || (var.varType == RUL_UNSIGNED_CHAR) )
		{
			retValue = (bool)((unsigned char)*this >= (unsigned char)temp)?true:false;
		}
		else if( (varType == RUL_CHAR) || (var.varType == RUL_CHAR) )
		{
			retValue = (bool)((char)*this >= (char)temp)?true:false;
		}
	}
	// Walt EPM - 05sep08 - add
	else //string compares
	{
		wstring str1;
		wstring str2;
		switch( varType )
		{
			case RUL_CHARPTR:
				str1 = AStr2TStr(val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str1 = val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str1 = (wchar_t *)*val.prgsa;
				break;
		}
		switch( var.varType )
		{
			case RUL_CHARPTR:
				str2 = AStr2TStr(var.val.pzcVal);
				break;
			case RUL_WIDECHARPTR:
			case RUL_DD_STRING:
				str2 = var.val.pszValue;
				break;
			case RUL_SAFEARRAY:
				str2 = (wchar_t *)*var.val.prgsa;
				break;
		}
		if( wcscmp(str1.c_str(), str2.c_str()) >= 0 )
		{
			retValue = (bool)true;
		}
		else
		{
			retValue = (bool)false;
		}
	}
	// Walt EPM - 05sep08 - end
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator&&(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;
	
	if( isNumeric() && temp.isNumeric() )
	{
		retValue = (bool)*this && (bool)temp;
	}
	return retValue;
	
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator||(const INTER_VARIANT& var)
{
	INTER_VARIANT retValue;
	INTER_VARIANT temp = var;
	
	if( isNumeric() && temp.isNumeric() )
	{
		retValue = (bool)*this || (bool)temp;
	}
	
	return retValue;
}

//WHS EP June17-2008 have changed this to make sure that it works for all data types
const INTER_VARIANT INTER_VARIANT::operator!()
{
	INTER_VARIANT retValue;
	
	if( isNumeric())
	{
		retValue = !((bool)*this);
	}
	
	return retValue;
}







const INTER_VARIANT& INTER_VARIANT::operator=(const CValueVarient& src)
{	
	CLEAR_DATA( CvarientType2VARIENTtype(src) );
	
	CValueVarient locCV = src;// we have a non const copy that will self destruct on exit

	if ( locCV.isNumeric() )		//	rvalue type
	{
		switch(varType) // our type (lvalue type)
		{
		case RUL_BOOL:			val.bValue = (bool)locCV;			break;
		case RUL_CHAR:			val.cValue = (char)locCV;			break;
		case RUL_UNSIGNED_CHAR:	val.ucValue= (unsigned char)locCV;	break;
		case RUL_SHORT:			val.sValue = (short)locCV;			break;
		case RUL_USHORT:		val.usValue= (unsigned short)locCV;	break;
		case RUL_INT:			val.nValue = (int)locCV;			break;
		case RUL_UINT:			val.unValue= (unsigned int)locCV;	break;
		case RUL_LONGLONG:		val.lValue = (__int64)locCV;		break;
        case RUL_ULONGLONG:		val.ulValue= (_UINT64)locCV;break;
		case RUL_FLOAT:			val.fValue = (float)locCV;			break;
		case RUL_DOUBLE:		val.dValue = (double)locCV;			break;
		default:
			// error: attempted numeric to string conversion
			break;
		}// end numeric switch
	}
	else
	{// NOT numeric
		switch(varType) // lvalue
		{
		case RUL_CHARPTR:	
			{			
				string locStr = (string)locCV;
				val.pzcVal    = new char[locStr.size()+1]; 
				memcpy(val.pzcVal,locStr.c_str(),(locStr.size()+1) );
			}
			break;
		case RUL_WIDECHARPTR:
		case RUL_DD_STRING:		
			{			
				wstring locStr = (wstring)locCV;
				val.pszValue   = new tchar[locStr.size()+1]; 
				memcpy(val.pszValue,locStr.c_str(),(locStr.size()+1)*sizeof(tchar));
			}
			break;

		case RUL_SAFEARRAY:
			{
				if (locCV.vType == CValueVarient::isString )
				{
					if ( val.prgsa == NULL )
						val.prgsa = new INTER_SAFEARRAY( (char*) locCV.sStringVal.c_str() );
					else
						*(val.prgsa) = (char*) locCV.sStringVal.c_str();
				}
				else
				if (locCV.vType == CValueVarient::isWideString )
				{
					if ( val.prgsa == NULL )
						val.prgsa = new INTER_SAFEARRAY( (wchar_t*) locCV.sWideStringVal.c_str());
					else
						*(val.prgsa) = (wchar_t*) locCV.sWideStringVal.c_str();
				}
				// else -  error - not numeric and not string....
			}
			break;
		case RUL_BYTE_STRING:	
			break;//  string to BYTE array NOT supported!!!!
		case RUL_NULL:
		default:	/* throw an error this is a non-numeric r value to numeric lvalue*/
			break;
		}// end switch
	}
	return *this;
}


// stevev 14aug07 convert inter-variant to a hcVarient
// returns true on error
bool inter2hcVARIANT(CValueVarient& destvarient, INTER_VARIANT& srcvariant)
{
	__VAL lV = srcvariant.GetValue();
	destvarient.clear();
	destvarient.vIsValid = true;// until proven otherwise

	switch(srcvariant.GetVarType())
	{
	case RUL_BOOL:
		destvarient.vValue.bIsTrue = lV.bValue;				//bool			bValue;
		destvarient.vType          = CValueVarient::isBool;
		break;
	case RUL_CHAR:
		destvarient.vValue.iIntConst = lV.cValue;				//_CHAR			cValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_CHAR);
		destvarient.vIsUnsigned      = false;
		break;
	case RUL_UNSIGNED_CHAR:
		destvarient.vValue.iIntConst = lV.ucValue;				//_CHAR			cValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_CHAR);
		destvarient.vIsUnsigned      = true;
		break;
	case RUL_INT:
		destvarient.vValue.iIntConst = lV.nValue;				//_INT32			nValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_INT32);
		destvarient.vIsUnsigned      = false;
		break;
	case RUL_UINT:
		destvarient.vValue.iIntConst = lV.unValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_UINT32);
		destvarient.vIsUnsigned      = true;
		break;		
	case RUL_SHORT:
		destvarient.vValue.iIntConst = lV.sValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_INT16);
		destvarient.vIsUnsigned      = false;
		break;
	case RUL_USHORT:
		destvarient.vValue.iIntConst = lV.usValue;
		destvarient.vType            = CValueVarient::isIntConst;
		destvarient.vSize            = sizeof(_UINT16);
		destvarient.vIsUnsigned      = true;
		break;
	case RUL_LONGLONG:
		destvarient.vValue.longlongVal = lV.lValue;
		destvarient.vType            = CValueVarient::isVeryLong;
		destvarient.vSize            = sizeof(_INT64);
		destvarient.vIsUnsigned      = false;
		break;
	case RUL_ULONGLONG:
		destvarient.vValue.longlongVal = lV.ulValue;
		destvarient.vType            = CValueVarient::isVeryLong;
		destvarient.vSize            = sizeof(_UINT64);
		destvarient.vIsUnsigned      = true;
		break;

	case RUL_FLOAT:
		destvarient.vValue.fFloatConst = lV.fValue;				//_FLOAT			fValue;
		destvarient.vType            = CValueVarient::isFloatConst;
		destvarient.vSize            = sizeof(_FLOAT);
		destvarient.vIsUnsigned      = false;
		destvarient.vIsDouble        = false;
		break;
	case RUL_DOUBLE:
		destvarient.vValue.fFloatConst = lV.dValue;				//_DOUBLE			dValue;
		destvarient.vType            = CValueVarient::isFloatConst;
		destvarient.vSize            = sizeof(_DOUBLE);
		destvarient.vIsUnsigned      = false;
		destvarient.vIsDouble        = true;
		break;

	case RUL_CHARPTR:
		if( lV.pzcVal )
		{
			destvarient.sStringVal		 = lV.pzcVal;				
		}
		else
		{
			destvarient.sStringVal = "";
		}
		destvarient.vType            = CValueVarient::isString;
		destvarient.vSize            = (int)destvarient.sStringVal.size();
		break;

	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
		if( lV.pszValue )
		{
			destvarient.sWideStringVal   = lV.pszValue;					
		}
		else
		{
			destvarient.sWideStringVal   = _T("");					
		}
		destvarient.vType            = CValueVarient::isWideString;
		destvarient.vSize            = (int)destvarient.sWideStringVal.size();
		break;

	case RUL_SAFEARRAY:
	{// others treat SAFEARRAY like a char array, Do that if it is a char array
		INTER_SAFEARRAY *sa = srcvariant.GetSafeArray();
		int idims      = sa->GetDims(NULL);
		int elemSz     = sa->GetElementSize();

		if (idims == 1)
		{
			int saLen = sa->GetNumberOfElements();
			if ( saLen > MAX_DD_STRING )
			{
				saLen = MAX_DD_STRING;
			}
			INTER_VARIANT elemValue;
			if (sa->Type() == RUL_CHAR || sa->Type() == RUL_UNSIGNED_CHAR)
			{
				char pchString[MAX_DD_STRING + 1]={0};
				int i=0;
				int index=0;
				for (i = 0,index = 0; index < saLen;  i+=elemSz,index++)
				{
					sa->GetElement(i, &elemValue);
					pchString[index] = (char)elemValue;
					elemValue.Clear();
				}// next element
				pchString[index] = 0;

				destvarient.sStringVal       = pchString;
				destvarient.vType            = CValueVarient::isString;
				destvarient.vSize            = (int)destvarient.sStringVal.size();
			}
			else
			if (sa->Type() == RUL_USHORT || sa->Type() == RUL_SHORT)
			{
				wchar_t pchString[MAX_DD_STRING]={0};
				int i=0;
				int index=0;
				for (i = 0,index = 0; index < saLen;  i+=elemSz,index++)
				{
					sa->GetElement(i, &elemValue);
					pchString[index] = (wchar_t)elemValue;
					elemValue.Clear();
				}// next element
				pchString[index] = 0;

				destvarient.sWideStringVal   = pchString;
				destvarient.vType            = CValueVarient::isWideString;
				destvarient.vSize            = (int)destvarient.sWideStringVal.size();
			}
			// else arrays of other stuff can't convert to a (w)string
		}		
		else //  idims != 1 --- can't handle
		{
			destvarient.vIsValid = false;
			return true;			// unknown/illegal variant type
		}
	}
	break;

	case RUL_BYTE_STRING:
		break;

	case RUL_NULL:
	default:
		destvarient.vIsValid = false;
		return true;			// unknown/illegal variant type
		break;
	}
	return false;// no error

}


// convert a CValueVarient instance to INTER_VARIANT instance
// returns true on error
bool cval2interVARIANT(INTER_VARIANT& destvarient, CValueVarient& srcvariant)
{
	bool bRetVal = false ;	//no error until proven otherwise


	switch(CvarientType2VARIENTtype(srcvariant))
	{
	case RUL_BOOL:
		{
			bool bTemp = false; 
			if( srcvariant.valueIsZero() )
			{
				bTemp=true;
			}
			destvarient = bTemp;				
		}
		break;
	case RUL_CHAR:
		{
			destvarient = (char)srcvariant;
		}
		break;
	case RUL_UNSIGNED_CHAR:
		{
			destvarient = (unsigned char)srcvariant;
		}
		break;
	case RUL_INT:
		destvarient = (int)srcvariant;
		break;
	case RUL_UINT:
		destvarient = (unsigned int)srcvariant;
		break;		
	case RUL_SHORT:
		destvarient = (short)srcvariant;
		break;
	case RUL_USHORT:
		destvarient = (unsigned short)srcvariant;
		break;
	case RUL_LONGLONG:
		destvarient = (long long)srcvariant;
		break;
	case RUL_ULONGLONG:
		destvarient = (signed long long)srcvariant;
		break;

	case RUL_FLOAT:
		destvarient = (float)srcvariant;
		break;
	case RUL_DOUBLE:
		destvarient = (double)srcvariant;
		break;

	case RUL_CHARPTR:
		{
			destvarient = (char *)srcvariant.sStringVal.c_str();
		}
		break;

	case RUL_WIDECHARPTR:
	case RUL_DD_STRING:
		{
			destvarient = (wchar_t *)srcvariant.sWideStringVal.c_str();
		}
		break;

	case RUL_SAFEARRAY:
	case RUL_BYTE_STRING:
	// UN-SUPPORTED by CValueVarient class
	case RUL_NULL:
	default:
		bRetVal = true;			// unknown/illegal variant type
		break;
	}

	return bRetVal;
}


int INTER_VARIANT::narrowStr2number(__VAL& retVal, VARIANT_TYPE& retType,const _CHAR* pStr)
{
	
	size_t nLen  = 0;
	_INT32 nVal  = 0;
	_UINT64 ulVal= 0;
	size_t i     = 0;
	_DOUBLE fVal = 0;
	_CHAR* pEnd = NULL;
	bool bIsFloat = false;
	//bool bIsHex   = false, bIsOctal = false/*, bIsNeg = false*/;

	if (pStr == NULL || (nLen = strlen(pStr)) == 0 )
	{
		return -1; // failure
	}

	/************************* reworked by stevev 10oct05 *********************************/
	bool bDetermineType = false;

	if (nLen >= 2)
	{
		//bIsNeg = (pStr[0] == '-');
		if (pStr[0] == '0')
		{// has to be octal || hex
			if ((pStr[1] == 'x') || (pStr[1] == 'X'))
			{
				bIsFloat = false;
			}
			else
			if ( pStr[1] >= '0' && pStr[1] < '8' )
			{// octal
				bIsFloat = false;
			}
			else
			{// float or decimal - actually an error....
				for(i=1;i<nLen;i++)// we'll try to recover via float
				{
					if (pStr[i] == '.' || pStr[i] == 'E' || pStr[i] == 'e')
					{
						bIsFloat = true;
						break;
					}
				}
				if ( ! bIsFloat )
				{// if all still false then its decimal eg 0999 - actually an error
					// throw error
					return -2;
				}
			}//end else			
		}
		else
		{// starts with a non-zero [1-9+\-]
		    // Walt EPM - 17oct08- make '.025' work as well as '0.025'
			for(i=0;i<nLen;i++)// we'll try to recover via float/
			{
				if (pStr[i] == '.' || pStr[i] == 'E' || pStr[i] == 'e')
				{
					bIsFloat = true;
					break;
				}
			}
		}// endelse a decimal/float
	}
	// else: length == 1, can't be octal or hex or float, must be decimal...process as such

	if ( bIsFloat )
	{
#if 1
		//needed for internationalization
		//Set the locale to be English without specifying the country/region like Australia or New Zealand.
		//But for the numerical formatting category of the locale, use United States because we need
		//the decimal point to be a period for use in xml

        _locale_t locale = _create_locale(LC_ALL, "LC_COLLATE=English;LC_CTYPE=English;LC_NUMERIC=en-US;LC_TIME=English");

#else
		// Code for setting decimal point to period before migration to VS2015
		_locale_t locale = _create_locale(LC_ALL, "English");
		if( locale )
		{
			if( locale->locinfo )
			{
				if( locale->locinfo->lconv )	// VS2015 error C2027: use of undefined type '__crt_locale_data'
				{
					if( locale->locinfo->lconv->decimal_point )
					{//force the decimal point to period... xml is period.
						locale->locinfo->lconv->decimal_point[0]='.';
					}
				}
			}
		}
#endif
        retVal.dValue = _strtod_l( pStr, &pEnd, locale );

		if( locale )
		{
            _free_locale(locale);
		}

		retType = RUL_DOUBLE;
	}
	else
	{
		// numerical integer is the only option left
		ulVal = strtoull(pStr, nullptr, 0);	// go unsigned until proven otherwise

		//determine the number varaible size
        if (ulVal <= 127)
		{
			char cV = (char) ulVal;
			retVal.cValue = cV;
			retType       = RUL_CHAR;
		}
        else if (ulVal <= 255)
		{
			unsigned char ucV = (unsigned char) ulVal;
			retVal.ucValue = ucV;
			retType       = RUL_UNSIGNED_CHAR;
		}
        else if (ulVal <= 32767)
		{
			short sV = (short) ulVal;
			retVal.sValue = sV;
			retType       = RUL_SHORT;
		}
        else if (ulVal <= 65535)
		{
			unsigned short usV = (unsigned short) ulVal;
			retVal.usValue = usV;
			retType       = RUL_USHORT;
		}
        else if (ulVal <= 2147483647)
		{
			int iV = (int) ulVal;
			retVal.nValue = iV;
			retType       = RUL_INT;
		}
        else if (ulVal <= 4294967295)
		{
			unsigned int uiV = (unsigned int) ulVal;
			retVal.unValue = uiV;
			retType       = RUL_UINT;
		}
        else if (ulVal <= 0x7FFFFFFFFFFFFFFF)
		{
			long long llV = (long long) ulVal;
			retVal.lValue = llV;
			retType       = RUL_LONGLONG;
		}
		else
		{
			retVal.ulValue = ulVal;
			retType       = RUL_ULONGLONG;
		}
	}
	
	return 0; // SUCCESS
}

enum VARIANT_TYPE CvarientType2VARIENTtype(const CValueVarient& srcVar)
{
	enum VARIANT_TYPE retVal = RUL_NULL;

	switch (srcVar.vType)
	{
	case CValueVarient::isBool:
		{	
			retVal = RUL_BOOL;
		}
		break;
	case CValueVarient::isIntConst:
		{
			if (srcVar.vSize == 1 )
			{
				if (srcVar.vIsUnsigned)
					retVal = RUL_UNSIGNED_CHAR;
				else
					retVal = RUL_CHAR;
			}
			else
			if (srcVar.vSize == 2 )
			{
				if (srcVar.vIsUnsigned)
					retVal = RUL_USHORT;
				else
					retVal = RUL_SHORT;
			}
			else // must be three or four
			{
				if (srcVar.vIsUnsigned)
					retVal = RUL_UINT;
				else
					retVal = RUL_INT;
			}
		}
		break;
	case CValueVarient::isFloatConst:
		{
			if (srcVar.vIsDouble)
				retVal = RUL_DOUBLE;
			else
				retVal = RUL_FLOAT;
		}
		break;
	case CValueVarient::isVeryLong:
		{
			if (srcVar.vIsUnsigned)
				retVal = RUL_ULONGLONG;
			else
				retVal = RUL_LONGLONG;
		}
		break;
	case CValueVarient::isString:
		{
			retVal = RUL_CHARPTR;
		}
		break;
	case CValueVarient::isWideString:
		{
			retVal = RUL_WIDECHARPTR;
		}
		break;

	case CValueVarient::isOpcode:
	case CValueVarient::isDepIndex:
	case CValueVarient::isSymID:
	case CValueVarient::invalid:
	default:
		retVal = RUL_NULL;
		break;
	// UN-SUPPORTED:  RUL_DD_STRING, RUL_BYTE_STRING, RUL_SAFEARRAY
	}// end switch src type

	return retVal;
}

enum VARIANT_TYPE variableType2VARIANTtype(const variableType_t& srcType, const unsigned long ulSize)
{
	enum VARIANT_TYPE retVal = RUL_NULL;

	switch (srcType)
	{
	case vT_Integer:
		{	
			if (ulSize == 1)
			{
				retVal = RUL_CHAR;
			}
			else if (ulSize <= 2)
			{
				retVal = RUL_SHORT;
			}
			else if (ulSize <= 4)
			{
				retVal = RUL_INT;
			}
			else
			{
				retVal = RUL_LONGLONG;
			}
		}
		break;
	case vT_HartDate:
		{	
			retVal = RUL_INT;
		}
		break;
	case vT_Unsigned:
	case vT_Enumerated:
	case vT_BitEnumerated:
	case vT_Index:
		{	
			if (ulSize == 1)
			{
				retVal = RUL_UNSIGNED_CHAR;
			}
			else if (ulSize <= 2)
			{
				retVal = RUL_USHORT;
			}
			else if (ulSize <= 4)
			{
				retVal = RUL_UINT;
			}
			else
			{
				retVal = RUL_ULONGLONG;
			}
		}
		break;
	case vT_Time:
	case vT_DateAndTime:
	case vT_Duration:
		{
			retVal = RUL_ULONGLONG;
		}
		break;
	case vT_FloatgPt:
		{
			retVal = RUL_FLOAT;
		}
		break;
	case vT_Double:
		{
			retVal = RUL_DOUBLE;
		}
		break;
	case vT_Ascii:
	case vT_PackedAscii:
	case vT_Password:
	case vT_EUC:
	case vT_OctetString:
	case vT_VisibleString:
		{
			retVal = RUL_CHARPTR;
		}
		break;
	case vT_BitString:
		{
			retVal = RUL_BYTE_STRING;
		}
		break;
	case vT_TimeValue:
		{
			if (ulSize <= 4)
			{
				retVal = RUL_UINT;
			}
			else
			{
				retVal = RUL_LONGLONG;
			}
		}
		break;
	case vT_Boolean:
		{
			retVal = RUL_BOOL;
		}
		break;

	default:
		retVal = RUL_NULL;
		break;
	// UN-SUPPORTED:  RUL_DD_STRING, RUL_BYTE_STRING, RUL_SAFEARRAY
	}// end switch src type

	return retVal;
}


/*static*/
void INTER_VARIANT::StripLangCode(wstring& szString, wchar_t* szLangCode, bool* bLangCodePresent)
{
	if (bLangCodePresent)
	{
		*bLangCodePresent =false;
	}
	if( (szString[0] == _T('|')) &&  (szString[3] == _T('|')) )
	{
		if (bLangCodePresent)
		{
			*bLangCodePresent = true;
		}
		if (szLangCode)
		{
			wcsncpy(szLangCode, szString.substr(1,2).c_str(),2);
			szLangCode[2] = 0;
		}
		szString = szString.substr(4);
	}
	return;
}
/*static*/
void INTER_VARIANT::StripLangCode(wchar_t* szString, wchar_t* szLangCode, bool* bLangCodePresent)
{
	if ( szString )
	{
		size_t y = wcslen(szString);
		if (y > 0)
		{
			wstring lstr(szString);
			StripLangCode(lstr,szLangCode,bLangCodePresent);
			wcsncpy(szString, lstr.c_str(),y);
			szString[lstr.size()] = 0;
		}
		else
		{
			if (bLangCodePresent)
			{
				*bLangCodePresent =false;
			}
		}
	}
	return;
}

/*static*/ INTER_VARIANT INTER_VARIANT::InitFrom(const CValueVarient *pvtValue)
{
	INTER_VARIANT retValue;

	switch (pvtValue->vTagType)
	{
	case ValueTagType::CVT_BOOL:
		retValue = (BOOL)pvtValue->vValue.bIsTrue;
		break;
	case ValueTagType::CVT_I1:
	{
		retValue = (INT8)CV_I1(pvtValue);
		break;
	}
	case ValueTagType::CVT_I2:
	{
		retValue = (INT16)CV_I2(pvtValue);
		break;
	}
	case ValueTagType::CVT_I4:
	{
        retValue = (INT32)CV_I4(pvtValue);
		break;
	}
	case ValueTagType::CVT_INT:
	{
        retValue = (INT32)CV_INT(pvtValue);
		break;
	}
	case ValueTagType::CVT_I8:
	{
		retValue = (INT64)CV_I8(pvtValue);
		break;
	}
	case ValueTagType::CVT_UI1:
	{
		retValue = (UINT8)CV_UI1(pvtValue);
		break;
	}
	case ValueTagType::CVT_UI2:
	{
		retValue = (UINT16)CV_UI2(pvtValue);
		break;
	}
	case ValueTagType::CVT_UI4:
	{
		retValue = (unsigned int)CV_UI4(pvtValue);
		break;
	}
	case ValueTagType::CVT_UINT:
	{
		retValue = (unsigned int)CV_UINT(pvtValue);
		break;
	}
	case ValueTagType::CVT_UI8:
	{
		retValue = (UINT64)CV_UI8(pvtValue);
		break;
	}
	case ValueTagType::CVT_R4:
	{
		retValue = (float)CV_R4(pvtValue);
		break;
	}
	case ValueTagType::CVT_R8:
	{
		retValue = (double)CV_R8(pvtValue);
		break;
	}
	case ValueTagType::CVT_WSTR:
	{
		retValue = CV_WSTR(pvtValue).c_str();
		break;
	}
	case ValueTagType::CVT_BITSTR:
	{
        unsigned long uSize = CV_BITSTR_LEN(pvtValue);
		
		_BYTE_STRING f;
		f.bs = CV_BITSTR_BUFFER(pvtValue);
		f.bsLen = uSize;
		retValue = f;	
		break;
	}
	default:
		retValue.Clear();
		break;
	}

	return retValue;
}

void INTER_VARIANT::CopyTo(CValueVarient *pvtValue)
{

	switch (varType)
	{
	case RUL_BOOL:
		CV_VT(pvtValue, ValueTagType::CVT_BOOL);
		CV_BOOL(pvtValue) = (val.bValue != false);
		break;
	case RUL_CHAR:
	{
		CV_VT(pvtValue, ValueTagType::CVT_I1);
		CV_I1(pvtValue) = val.cValue;
		break;
	}
	case RUL_SHORT:
	{
		CV_VT(pvtValue, ValueTagType::CVT_I2);
		CV_I2(pvtValue) = val.sValue;
		break;
	}
	case RUL_INT:
	{
		CV_VT(pvtValue, ValueTagType::CVT_I4);
		CV_I4(pvtValue) = val.nValue;
		break;
	}
	case RUL_LONGLONG:
	{
		CV_VT(pvtValue, ValueTagType::CVT_I8);
		CV_I8(pvtValue) = val.lValue;
		break;
	}
	case RUL_UNSIGNED_CHAR:
	{
		CV_VT(pvtValue, ValueTagType::CVT_UI1);
		CV_UI1(pvtValue) = val.ucValue;
		break;
	}
	case RUL_USHORT:
	{
		CV_VT(pvtValue, ValueTagType::CVT_UI2);
		CV_UI2(pvtValue) = val.usValue;
		break;
	}
	case RUL_UINT:
	{
		CV_VT(pvtValue, ValueTagType::CVT_UI4);
		CV_UI4(pvtValue) = val.unValue;
		break;
	}
	case RUL_ULONGLONG:
	{
		CV_VT(pvtValue, ValueTagType::CVT_UI8);
        CV_UI8(pvtValue) =  val.ulValue;
		break;
	}
	case RUL_FLOAT:
	{
		CV_VT(pvtValue, ValueTagType::CVT_R4);
		CV_R4(pvtValue) = val.fValue;
		break;
	}
	case RUL_DOUBLE:
	{
		CV_VT(pvtValue, ValueTagType::CVT_R8);
		CV_R8(pvtValue) = val.dValue;
		break;
	}
	case RUL_DD_STRING:
	case RUL_WIDECHARPTR:
	case RUL_CHARPTR:
	case RUL_SAFEARRAY:// only of type (one of three above)
	{
		wchar_t *wstr = NULL;
		GetStringValue(&wstr);
		CV_VT(pvtValue, ValueTagType::CVT_WSTR);
		CV_WSTR(pvtValue) = wstr;
		delete [] wstr;
		break;
	}
	case RUL_BYTE_STRING:
	{
		CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
		CV_BITSTR(pvtValue, val.bString.bs, val.bString.bsLen);	
	}
	break;

	case RUL_NULL:
	default:
		pvtValue->clear();
		break;
	}

	return;
}

/*static*/ INTER_VARIANT INTER_VARIANT::InitFrom(const nsConsumer::EVAL_VAR_VALUE *pevvValue)
{
	INTER_VARIANT retValue;

	switch (pevvValue->type)
	{
	case nsEDDEngine::VT_BOOLEAN:
		retValue = (BOOL)pevvValue->val.u;
		break;
	case nsEDDEngine::VT_INTEGER:
	case nsEDDEngine::VT_EDD_DATE:
	{
		if (pevvValue->size <= 2)
		{
			retValue = (INT8)pevvValue->val.i;
		}
		else if (pevvValue->size <= 4)
		{
			retValue = (INT16)pevvValue->val.i;
		}
		else
		{
			retValue = (INT64)pevvValue->val.i;
		}
		break;
	}
	case nsEDDEngine::VT_UNSIGNED:
	case nsEDDEngine::VT_ENUMERATED:
	case nsEDDEngine::VT_BIT_ENUMERATED:
	case nsEDDEngine::VT_INDEX:
	case nsEDDEngine::VT_TIME:
	case nsEDDEngine::VT_DATE_AND_TIME:
	case nsEDDEngine::VT_DURATION:
	case nsEDDEngine::VT_OBJECT_REFERENCE:
	{
		if (pevvValue->size <= 2)
		{
			retValue = (UINT8)pevvValue->val.u;
		}
		else if (pevvValue->size <= 4)
		{
			retValue = (UINT16)pevvValue->val.u;
		}
		else
		{
			retValue = (UINT64)pevvValue->val.u;
		}
		break;
	}
	case nsEDDEngine::VT_FLOAT:
	{
		retValue = (float)pevvValue->val.f;
		break;
	}
	case nsEDDEngine::VT_DOUBLE:
	{
		retValue = (double)pevvValue->val.d;
		break;
	}
	case nsEDDEngine::VT_TIME_VALUE:
		if (pevvValue->size <= 4)
		{
			retValue = (UINT32)pevvValue->val.u;
		}
		else
		{
			retValue = (INT64)pevvValue->val.i;
		}
		break;
	case nsEDDEngine::VT_ASCII:
	case nsEDDEngine::VT_PACKED_ASCII:
	case nsEDDEngine::VT_PASSWORD:
	case nsEDDEngine::VT_EUC:
	case nsEDDEngine::VT_OCTETSTRING:
	case nsEDDEngine::VT_VISIBLESTRING:
	{
		retValue = pevvValue->val.s.c_str();
		break;
	}
	case nsEDDEngine::VT_BITSTRING:	 
	{
		_BYTE_STRING f;
		f.bs = (uchar*)pevvValue->val.b.ptr(); 
		f.bsLen = pevvValue->val.b.length();
		retValue = f;
		break;
	}
	default:
		retValue.Clear();
		break;
	}

	return retValue;
}

void INTER_VARIANT::CopyTo(nsConsumer::EVAL_VAR_VALUE *pevvValue)
{

	switch (varType)
	{
	case RUL_CHAR:
	{
		pevvValue->val.i = (INT8)val.cValue;
		pevvValue->type = nsEDDEngine::VT_INTEGER;
		pevvValue->size = 1;
		break;
	}
	case RUL_SHORT:
	{
		pevvValue->val.i = (INT16)val.sValue;
		pevvValue->type = nsEDDEngine::VT_INTEGER;
		pevvValue->size = 2;
		break;
	}
	case RUL_INT:
	{
		pevvValue->val.i = (long)val.nValue;
		pevvValue->type = nsEDDEngine::VT_INTEGER;
		pevvValue->size = 4;
		break;
	}
	case RUL_LONGLONG:
	{
		pevvValue->val.i = (INT64)val.lValue;
		pevvValue->type = nsEDDEngine::VT_INTEGER;
		pevvValue->size = 8;
		break;
	}
	case RUL_BOOL:
		pevvValue->val.u = (bool)val.bValue;
		pevvValue->type = nsEDDEngine::VT_UNSIGNED;
		pevvValue->size = sizeof(bool);
		break;
	case RUL_UNSIGNED_CHAR:
	{
		pevvValue->val.u = (UINT8)val.ucValue;
		pevvValue->type = nsEDDEngine::VT_UNSIGNED;
		pevvValue->size = 1;
		break;
	}
	case RUL_USHORT:
	{
		pevvValue->val.u = (UINT16)val.usValue;
		pevvValue->type = nsEDDEngine::VT_UNSIGNED;
		pevvValue->size = 2;
		break;
	}
	case RUL_UINT:
	{
		pevvValue->val.u = (unsigned long)val.unValue;
		pevvValue->type = nsEDDEngine::VT_UNSIGNED;
		pevvValue->size = 4;
		break;
	}
	case RUL_ULONGLONG:
	{
		pevvValue->val.u = (UINT64)val.ulValue;
		pevvValue->type = nsEDDEngine::VT_UNSIGNED;
		pevvValue->size = 8;
		break;
	}
	case RUL_FLOAT:
	{
		pevvValue->val.f = (float)val.fValue;
		pevvValue->type = nsEDDEngine::VT_FLOAT;
		pevvValue->size = 4;
		break;
	}
	case RUL_DOUBLE:
	{
		pevvValue->val.d = (double)val.dValue;
		pevvValue->type = nsEDDEngine::VT_DOUBLE;
		pevvValue->size = 8;
		break;
	}
	case RUL_DD_STRING:
	case RUL_WIDECHARPTR:
	case RUL_CHARPTR:
	case RUL_SAFEARRAY:// only of type (one of three above)
	{
		wchar_t *wstr = NULL;
		GetStringValue(&wstr);
		size_t strLen = wcslen(wstr) + 1;

		pevvValue->val.s = wstr;
		delete [] wstr;
		pevvValue->type = nsEDDEngine::VT_ASCII;
		pevvValue->size = strLen;
		break;
	}
	case RUL_BYTE_STRING:
	{
		pevvValue->val.b.assign( val.bString.bs, val.bString.bsLen);  
		pevvValue->type = nsEDDEngine::VT_BITSTRING  ;
		pevvValue->size = pevvValue->val.b.length();
	}
	case RUL_NULL:
	default:
		break;
	}

	return;
}

//This function returns the number of bytes available for numeric variable and for local array,
//This function returns the number of bytes used for string without NULL character and for BYTE_STRING value.
size_t INTER_VARIANT::GetMaxSize()
{
	size_t iSize = 0;
	switch (varType)
	{
		case RUL_CHARPTR:		//maybe local DD_STRING
			iSize = strlen(val.pzcVal);
			break;
		case RUL_DD_STRING:
		case RUL_WIDECHARPTR:
			iSize = wcslen(val.pszValue);
			break;
		case RUL_SAFEARRAY:
			if (val.prgsa->Type() == RUL_CHAR)
			{
				iSize = val.prgsa->MemoryAllocated() - 1;	//get rid of one byte used as NULL terminal character
			}
			else
			{
				iSize = val.prgsa->MemoryAllocated();
			}
			break;
		case RUL_BYTE_STRING: 
			iSize = val.bString.bsLen;
			break;
		case RUL_BOOL:
		case RUL_CHAR:
		case RUL_UNSIGNED_CHAR:
			iSize = 1;
			break;
		case RUL_INT:
		case RUL_UINT:
		case RUL_FLOAT:
			iSize = 4;
			break;
		case RUL_SHORT:
		case RUL_USHORT:
			iSize = 2;
			break;
		case RUL_LONGLONG:
		case RUL_ULONGLONG:
		case RUL_DOUBLE:
			iSize = 8;
			break;
		default:
			break;
	}
	return iSize;
}


RUL_TOKEN_SUBTYPE VARIANTtype2TokenSubType(const VARIANT_TYPE vt)
{
	RUL_TOKEN_SUBTYPE subtoken = RUL_SUBTYPE_NONE;

	switch(vt)
	{
	case RUL_BOOL:
		subtoken = RUL_BOOLEAN_DECL;
		break;
	case RUL_CHAR:
		subtoken = RUL_CHAR_DECL;
		break;
	case RUL_UNSIGNED_CHAR:
		subtoken = RUL_UNSIGNED_CHAR_DECL;
		break;
	case RUL_SHORT:
		subtoken = RUL_SHORT_INTEGER_DECL;
		break;
	case RUL_USHORT:
		subtoken = RUL_UNSIGNED_SHORT_INTEGER_DECL;
		break;
	case RUL_INT:
		subtoken = RUL_INTEGER_DECL;	//or RUL_LONG_DECL
		break;
	case RUL_UINT:
		subtoken = RUL_UNSIGNED_INTEGER_DECL;
		break;
	case RUL_LONGLONG:
		subtoken = RUL_LONG_LONG_DECL;
		break;
	case RUL_ULONGLONG:
		subtoken = RUL_UNSIGNED_LONG_LONG_DECL;
		break;
	case RUL_FLOAT:
		subtoken = RUL_REAL_DECL;
		break;
	case RUL_DOUBLE:
		subtoken = RUL_DOUBLE_DECL;
		break;
		// end add  Walt EPM 08sep08
	case RUL_CHARPTR:
	case RUL_WIDECHARPTR:
	case RUL_BYTE_STRING:
		subtoken = RUL_STRING_DECL;
		break;
	case RUL_DD_STRING:
		subtoken = RUL_DD_STRING_DECL;
		break;
	case RUL_SAFEARRAY:
		subtoken = RUL_ARRAY_DECL;
		break;
	}

	return subtoken;
}
