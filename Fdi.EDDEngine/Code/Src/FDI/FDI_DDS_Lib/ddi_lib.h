/**
 *	@(#) $Id: ddi_lib.h,v 1.7 2012/09/06 22:28:00 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 */
 
#pragma once

#ifndef  TABLE_H
#include "table.h"
#endif

#include "attrs.h"
#include "evl_lib.h"

#ifndef ENV_INFO_FDI_H
#include "env_info.h"
#endif

#ifndef DDI_ITEM_H
#include "ddi_item.h"
#endif
 
#ifdef __cplusplus
    extern "C" {
#endif

/*
 *	The DDS Revision number is a 3-part id defined as follows:
 *	
 *	<major ddod revision>.<minor ddod revision>.<software revision>
 *
 *	The major and minor revisions are shared with the Tokenizer
 *	and are found in ddldefs.h
 */
 
/* This is the software Revision of the DDS Library */

#define DDS_REVISION_NUMBER		1

//	global buffer length of strings gotten from rtn_code.h
//  increased to 256 to handle messages coming from DDS - davcomf
#define BUF_LEN	256

/*
 *  DDI interface structures
 */


typedef struct tag_DIR_SPECIFIER_REFERENCE {
	ROD_HANDLE		rod_handle;
	nsEDDEngine::DD_REFERENCE	ref;
}		DIR_SPECIFIER_REFERENCE;

typedef struct tag_DIR_SPECIFIER_BLOCKNAME {
	DEVICE_TYPE_HANDLE	dt_handle;
	ITEM_ID				blockname;
}		DIR_SPECIFIER_BLOCKNAME;

typedef struct tag_OP_DESC {
	::nsEDDEngine::OP_REF				op_ref;
	DESC_REF			desc_ref;
}		OP_DESC;

typedef struct tag_OP_DESC_LIST {
	int					count;
	OP_DESC				*list;
}		OP_DESC_LIST;


/*
 *	DDI defines for calling ddi_block_dir_request() and
 * ddi_device_dir_request().
 */

#define	BD_BLOCK_HANDLE		1	/* BLOCK DIR request by BLOCK_HANDLE */
#define BD_DD_HANDLE		2	/* BLOCK_DIR request by DD_REFERENCE, DD_HANDLE */
#define BD_BLOCKNAME		3	/* BLOCK_DIR reauest by DEVICE_TYPE_HANDLE_BLOCKNAME */
#define	DD_DEVICE_HANDLE	4	/* DEVICE DIR request by DEVICE_HANDLE */
#define DD_DD_HANDLE		5	/* DEVICE_DIR request by DD_REFERENCE, DD_HANDLE */
#define	DD_DT_HANDLE		6	/* DEVICE_DIR request by DEVICE_TYPE_HANDLE */

typedef struct tag_DDI_DEVICE_DIR_REQUEST {
	unsigned short			type;			/* See defines above */
	unsigned long 			mask;			/* bit enum: */
	union {
		DEVICE_TYPE_HANDLE				device_type_handle;
		DIR_SPECIFIER_REFERENCE			ref;
		DEVICE_HANDLE					device_handle;
	} 			spec;
}	DDI_DEVICE_DIR_REQUEST;

typedef struct tag_DDI_BLOCK_DIR_REQUEST {
	unsigned short			type;			/* See defines above */
	unsigned long 			mask;			/* bit enum: */
	union {
		DIR_SPECIFIER_REFERENCE			ref;
		BLOCK_HANDLE					block_handle;
		DIR_SPECIFIER_BLOCKNAME			block;
	}		spec;
}	DDI_BLOCK_DIR_REQUEST;


/*
 * These are the possible types of item specifier for calling
 * ddi_get_item().
 */

/* DD Item types without subindex */

#define	DDI_ITEM_BLOCK              0
#define	DDI_ITEM_ID                 1
#define	DDI_ITEM_NAME               2
#define	DDI_ITEM_OP_INDEX           3
#define	DDI_ITEM_PARAM              4


/* DD Item types with subindex */

#define	DDI_ITEM_ID_SI              7
#define	DDI_ITEM_NAME_SI			8
#define	DDI_ITEM_OP_INDEX_SI        9
#define	DDI_ITEM_PARAM_SI          10



typedef struct tag_DDI_ITEM_SPECIFIER {

	int			type;				/* see defines above */
	SUBINDEX	subindex;

	union {
		ITEM_ID			id;
		ITEM_ID			name;
		OBJECT_INDEX	op_index;
		int				param;
	}	item;

} DDI_ITEM_SPECIFIER;

/*
 * typedef for Item Attribute Request
 * plus a define for getting every 
 * Item Attribute in one request
 */


#define ALL_ITEM_ATTR_MASK   0XFFFF


typedef unsigned long DDI_ATTR_REQUEST;



/*
 * DDI convenience Parameter Specifier.
 * This structure is used to specify a
 * DDL parameter and it contains every 
 * meaningful way of describing a parameter
 * to DDS
 */

/* parameter specifier types */

#define	DDI_PS_ITEM_ID          0  /* specify parameter with item ID */
#define	DDI_PS_OP_INDEX	        1  /* specify parameter with operational index */
#define	DDI_PS_PARAM_NAME       2  /* specify parameter with parameter name */
#define	DDI_PS_PARAM_OFFSET     3  /* specify parameter with parameter offset */

/* Parameter specifier with subindex */
/* for when the parameter is a member of an ARRAY or RECORD */

#define	DDI_PS_ITEM_ID_SI            4 /* spec param with item ID and subindex */
#define	DDI_PS_OP_INDEX_SI            5 /* spec param with operational index and subindex */
#define	DDI_PS_PARAM_NAME_SI         6 /* spec param with parameter name and subindex */
#define	DDI_PS_PARAM_OFFSET_SI       7 /* spec param with parameter offset and subindex */


typedef struct tag_DDI_PARAM_SPECIFIER {

	int			type;			/* see defines above */
	SUBINDEX	subindex;		

	union {

		ITEM_ID			id;
		ITEM_ID			name;
		OBJECT_INDEX	op_index;
		int				param;

	}	item;

} DDI_PARAM_SPECIFIER;


/*
 * These are the possible types of DDI_BLOCK_SPECIFIER
 */

#define DDI_BLOCK_TAG      0
#define DDI_BLOCK_HANDLE   1
#define DDI_DEVICE_HANDLE  2

typedef char *BLOCK_TAG;

typedef struct tag_DDI_BLOCK_SPECIFIER {

	int			type;				/* see defines above */

	union {

		BLOCK_TAG		tag;
		BLOCK_HANDLE	handle;

	}	block;

} DDI_BLOCK_SPECIFIER;



/*
 * Structure used with ddi_get_param_info()
 */

typedef struct tag_DDI_PARAM_INFO {

  ITEM_TYPE	item_type;
  ITEM_ID       item_id; 
  ITEM_ID       param_name;
  OBJECT_INDEX	op_index;
  int           param_offset;

} DDI_PARAM_INFO;

/*
 *	DDI routines called by the Connection Manager.
 */

extern int ddi_build_dd_dev_tbls		P((ENV_INFO *, DEVICE_HANDLE));
extern int ddi_remove_dd_dev_tbls		P((ENV_INFO *, DEVICE_TYPE_HANDLE));
extern int ddi_build_dd_blk_tbls		P((ENV_INFO *, BLOCK_HANDLE));
extern int ddi_remove_dd_blk_tbls		P((ENV_INFO *, BLOCK_HANDLE));


/*
 *	DDI_LOAD_TABLES interface routines
 */

extern int ddi_load_device_tables P(( ENV_INFO *, DIR_SPECIFIER_REFERENCE *, nsEDDEngine::FLAT_DEVICE_DIR * ));
extern int ddi_load_block_tables  P(( ENV_INFO *, DIR_SPECIFIER_REFERENCE *, nsEDDEngine::FLAT_BLOCK_DIR * , nsEDDEngine::FLAT_DEVICE_DIR *));

/*
 *	DDI_GET_DIRECTORY interface routines
 */

extern int ddi_device_dir_request P(( ENV_INFO *, DDI_DEVICE_DIR_REQUEST *, nsEDDEngine::FLAT_DEVICE_DIR *));
extern int ddi_block_dir_request  P(( ENV_INFO *,  DDI_BLOCK_DIR_REQUEST *, nsEDDEngine::FLAT_BLOCK_DIR *, nsEDDEngine::FLAT_DEVICE_DIR * ));


/*
 *	DDI_DESTRUCT interface routines
 */

extern void ddi_clean_device_dir P(( nsEDDEngine::FLAT_DEVICE_DIR * ));
extern void ddi_clean_block_dir  P(( nsEDDEngine::FLAT_BLOCK_DIR * ));


/*
 *	DDI item interface functions
 */


extern int ddi_get_item P((DDI_BLOCK_SPECIFIER *,
						nsEDDEngine::FDI_ITEM_SPECIFIER *,
						ENV_INFO *,
						nsEDDEngine::AttributeNameSet, //DDI_ATTR_REQUEST,
						DDI_GENERIC_ITEM *));


extern int ddi_clean_item P((DDI_GENERIC_ITEM *));

/*
 * DDI convenience
 */

extern int ddi_check_enum_var_value P((nsEDDEngine::FLAT_VAR *, unsigned long *));


extern int ddi_get_type P(( ENV_INFO *, DDI_BLOCK_SPECIFIER *,
						    nsEDDEngine::FDI_ITEM_SPECIFIER *,
						    ITEM_TYPE *));

extern int ddi_get_type_and_item_id P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
						    nsEDDEngine::FDI_ITEM_SPECIFIER *,
						    ITEM_TYPE *,
						    ITEM_ID *));

extern int ddi_get_param_info P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
								DDI_PARAM_SPECIFIER *,
								DDI_PARAM_INFO *));

extern int ddi_find_unit_ids P((ENV_INFO *, DDI_BLOCK_SPECIFIER *block_spec,
                                ITEM_ID id_to_find,
	                            ITEM_ID *item_id_of_unit_rel,
                                ITEM_ID *item_id_of_unit_var));

extern int ddi_get_unit (ENV_INFO *, DDI_BLOCK_SPECIFIER *,
							nsEDDEngine::FDI_PARAM_SPECIFIER *,
							ITEM_ID *);

extern int ddi_get_wao P((ENV_INFO *, DDI_BLOCK_SPECIFIER 	*,
						   nsEDDEngine::FDI_PARAM_SPECIFIER *,
						   ITEM_ID *));

extern int ddi_get_update_items P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
								nsEDDEngine::FDI_PARAM_SPECIFIER *,
								OP_DESC_LIST *));

extern void ddi_build_dominant_tbl P((ENV_INFO *env_info,
								nsEDDEngine::FLAT_BLOCK_DIR *));

extern void ddi_load_dominant_tables P(( OP_DESC_LIST *, nsEDDEngine::FDI_PARAM_SPECIFIER *, nsEDDEngine::ITEM_TYPE, nsEDDEngine::DOMINANT_TBL *));

extern int ddi_get_ptoc (ENV_INFO *, DDI_BLOCK_SPECIFIER *,
							nsEDDEngine::FDI_PARAM_SPECIFIER *,
							nsEDDEngine::CommandType eCmdType,
							nsEDDEngine::FDI_COMMAND_LIST * pCommandList);

extern int ddi_get_var_type P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
							   nsEDDEngine::FDI_PARAM_SPECIFIER *,
							   TYPE_SIZE *));

extern int ddi_get_item_id P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
							  ITEM_ID param_name,
							  ITEM_ID *));

extern int ddi_get_cmd_id P(( ENV_INFO *, DDI_BLOCK_SPECIFIER *,
								ulong cmd_number,
								ITEM_ID	*));

extern int ddi_get_subindex P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
							   ITEM_ID,
							   ITEM_ID,
							   SUBINDEX *));


extern void ddi_clean_update_item_list P((OP_DESC_LIST *));


/*
 * DDI language
 */

extern wchar_t g_dds_language_code[];

extern int ddi_set_language_code(wchar_t *new_code);

extern int ddi_get_string_translation (const wchar_t *string, const wchar_t *lang,
										wchar_t *outbuf, int outbuf_size_in_chars);


#ifdef __cplusplus
    }
#endif
