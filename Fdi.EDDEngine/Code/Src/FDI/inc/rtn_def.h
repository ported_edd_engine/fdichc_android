/*
 *      @(#) $Id: rtn_def.h,v 1.1 2012/07/10 20:59:53 rgretta Exp $
 *      
 *      This file is used to generate rtn_code.h and as an include
 *      file to dds_err.c
 */

#ifdef GEN_RTN_H

#define PP_DEFINE #define
#define PP_IFNDEF #ifndef
#define PP_IFDEF #ifdef
#define PP_ENDIF #endif
#define PP_INCLUDE #include

#define rcode_def(x,y,z) PP_DEFINE x            y

/********************************************************************
 * rtn_code.h is a generated from rtn_def.h!!
 *     Edit rtn_def.h to make changes!!!
 ********************************************************************/
PP_IFNDEF RTN_CODE_H
PP_DEFINE RTN_CODE_H

PP_DEFINE RETURN_CODE int

PP_IFDEF __cplusplus
    extern "C" {
PP_ENDIF

extern wchar_t *dds_error_string (int); //used by HART and PB
extern wchar_t *dds_error_user_string (int, wchar_t *, int); //used by HART and PB
extern wchar_t *dds_error_string_ff (int, wchar_t *, int); //used by FF
extern wchar_t *dds_error_user_string_ff (int, wchar_t *, int); //used by FF

#else
#define rcode_def(x,y,z) { y, z }, 
#endif

#define SC_START            0
#define SM_START         -900
#define DDS_START       -1400
#define EVAL_START      -1500
#define FETCH_START     -1600
#define DDI_START       -1700
#define HART_BLTIN_START -1800
#define METH_START      -1900
#define PC_START        -2000
#define L7_START        -2100
#define NM_START        -2200
#define CS_START        -2300
#define CM_START        -2400
#define ISPDS_START     -2500
#define HRTDS_START     -2600
#define RCSIM_START     -2700
#define COMM_MGR_START  -2800
#define DB_COMM_START   -2900
#define DBWRAP_START    -3000
#define RBS_START       -3100
#define MKINI_START     -3200
#define GEN_START       -3300	// Generic errors, NOT meant to be subsystem specific errors.
#define DB_START        -3400

#define HART_PT_START       -3800
#define PATH_PT_LEVEL_START -3900
/* Please allow 200 error codes for pass-through communications.
 * So -4000 to -4199 should be reserved for these errors.
*/
#define PASS_THROUGH_COMMUNICATIONS_START -4000

/* Please allow 200 error codes for pass-through hierarchy errors.
 * So -4200 to -4399 should be reserved for these errors.
*/
#define PASS_THROUGH_HIERARCHY_START -4200
#define PTNET_START                  -4400
/*
*	Allow 200 error codes for the IDispatch interface for pass-through.
*	The range should be -4500 to -4699
*/
#define PASS_THROUGH_DISPATCH_START -4500
#define AMS_SERVER_DISPATCH_START   -4700
#define DEVICE_LOCATION_STORE_START -4800

#define UNKNOWN_START               -5000
#define LC_START					-6000

/* SC status codes */
rcode_def (     SC_NO_MORE_ENUM,                        (SC_START - 1),         L"SC No more enumeration")
rcode_def (     SC_NO_PROP,                             (SC_START - 10),        L"SC No such property")
rcode_def (     SC_NO_ENUM,                             (SC_START - 11),        L"SC No such enumeration")
rcode_def (     SC_ENUM_FAIL,                           (SC_START - 13),        L"SC Enumeration fail")
rcode_def (     SC_NO_CONNECT,                          (SC_START - 14),        L"SC Connect failed")
rcode_def (     SC_SERVER_DOWN,                         (SC_START - 15),        L"SC Remote server down")
rcode_def (     SC_NO_PATH,                             (SC_START - 16),        L"SC No such path")
rcode_def (     SC_NO_METHOD,                           (SC_START - 17),        L"SC No such method")
rcode_def (     SC_PATH_CHANGED,                        (SC_START - 18),        L"SC Path changed underneath us")
rcode_def (     SC_NO_DEVICE,                           (SC_START - 19),        L"SC No such Device")
rcode_def (     SC_NO_BLOCK,                            (SC_START - 20),        L"SC No such Block")
rcode_def (     SC_NO_CONFIG,                           (SC_START - 21),        L"SC No such NamedConfig")
rcode_def (     SC_PROVISIONING_FAIL,                   (SC_START - 22),        L"SC Wireless device provisioning failed")
rcode_def (     SC_ERROR_END,                           (SC_START - 99),        L"SC Error end")


/* SM error codes */
rcode_def (     SM_BAD_PATH_TYPE,                       (SM_START - 1),         L"SM Bad path type")
rcode_def (     SM_INDEX_OUT_OF_RANGE,                  (SM_START - 2),         L"SM Index out of range")
rcode_def (     SM_BAD_MIN_MAX_VALUE_TYPE,              (SM_START - 3),         L"SM Bad min max value type")
rcode_def (     SM_CT_BLOCK_SEARCH_FAILED,              (SM_START - 4),         L"SM Ct block search failed")
rcode_def (     SM_ITEM_ID_NOT_PARAMETER,               (SM_START - 5),         L"SM Item id not parameter")
rcode_def (     SM_FREE_STORE_FAIL,                     (SM_START - 6),         L"SM Free store fail")
rcode_def (     SM_BAD_MONIKER_ITEM_ID,                 (SM_START - 7),         L"SM Bad moniker item id")
rcode_def (     SM_BAD_MONIKER_NAME,                    (SM_START - 8),         L"SM Bad moniker name")
rcode_def (     SM_BAD_MONIKER_ITEM_INDEX,              (SM_START - 9),         L"SM Bad moniker item index")
rcode_def (     SM_BAD_MONIKER_INDEX,                   (SM_START - 10),        L"SM Bad moniker index")
rcode_def (     SM_BAD_MONIKER,                         (SM_START - 11),        L"SM Bad moniker")
rcode_def (     SM_NO_DEVICE_TAG,                       (SM_START - 12),        L"SM No device tag")
rcode_def (     SM_NO_PHYSICAL_TAG,                     (SM_START - 13),        L"SM No physical tag")
rcode_def (     SM_NO_DEVICE_ID,                        (SM_START - 14),        L"SM No device id")
rcode_def (     SM_NO_NETWORK_ADDRESS,                  (SM_START - 15),        L"SM No network address")
rcode_def (     SM_NO_SHORT_ADDRESS,                    (SM_START - 16),        L"SM No short address")
rcode_def (     SM_NO_ENUMS,                            (SM_START - 17),        L"SM No enums")
rcode_def (     SM_PV_NOT_ENUM,                         (SM_START - 18),        L"SM Parameter not of enumerated type")
rcode_def (     SM_PV_NOT_IN_ENUM,                      (SM_START - 19),        L"SM No matching enumeration for the value")
rcode_def (     SM_NOT_IMPLEMENTED,                     (SM_START - 20),        L"SM Not implemented")
rcode_def (     SM_DEVICETAG_NOT_UNIQUE,                (SM_START - 21),        L"SM Devicetag not unique")
rcode_def (     SM_PHYSICALTAG_NOT_UNIQUE,              (SM_START - 22),        L"SM Phyicaltag not unique")
rcode_def (     SM_FS_COMM_ERR,                         (SM_START - 23),        L"SM Fs comm err")
rcode_def (     SM_PS_COMM_ERR,                         (SM_START - 24),        L"SM Ps comm err")
rcode_def (     SM_NO_HARDWARE_REV,                     (SM_START - 25),        L"SM No hardware rev")
rcode_def (     SM_NO_SOFTWARE_REV,                     (SM_START - 26),        L"SM No software rev")
rcode_def (     SM_BAD_REMOTE_BLOCK,                    (SM_START - 27),        L"SM Bad remote block")
rcode_def (     SM_NO_VAR_RESP_CODE,                    (SM_START - 28),        L"SM No var resp code")
rcode_def (     SM_NO_MATCHING_RESP_CODE,               (SM_START - 29),        L"SM No matching resp code")
rcode_def (     SM_SYMFILE_OPEN_ERR,                    (SM_START - 30),        L"SM Symfile open err")
rcode_def (     SM_ID_NOT_FOUND,                        (SM_START - 31),        L"SM Id not found")
rcode_def (     SM_NAME_NOT_FOUND,                      (SM_START - 32),        L"SM Name not found")
rcode_def (     SM_NO_SYMFILE_FOUND,                    (SM_START - 33),        L"SM No symfile found")
rcode_def (     SM_VAR_TYPE_NOT_INDEX,                  (SM_START - 34),        L"SM Var type not index")
rcode_def (     SM_NO_MATCHING_COMM_CODE,               (SM_START - 35),        L"SM No matching comm status code")
rcode_def (     SM_NO_ACTION_FOR_ENUM,                  (SM_START - 36),        L"SM No Actions for this EnumerationValue")
rcode_def (     SM_ERROR_GETTING_HANDLING,              (SM_START - 37),        L"SM An Error Occurred Getting Handling Attribute")
rcode_def (     SM_SYNC_ON_FILE_SERVER,                 (SM_START - 38),        L"SM Attempting to synchronize a non-live device with the database")
rcode_def (     SM_PROP_NOT_VALID_ON_FILE_SERVER,       (SM_START - 39),        L"SM Attempting to read a property that is not valid on the file server")
rcode_def (     SM_DEVICE_NOT_SYNCHRONIZED_WITH_DB,     (SM_START - 40),        L"SM The device has not been synchronized with the database")
rcode_def (     SM_NO_NETWORK_TYPE,						(SM_START - 41),        L"SM No network type")
rcode_def (     SM_FAILED_TO_CREATE_DOM_DOC,			(SM_START - 42),        L"SM Failed to create DOM Document")
rcode_def (     SM_FAILED_TO_LOAD_DD_FILE,				(SM_START - 43),        L"SM Failed to load DD file")
rcode_def (     SM_DD_FILE_NOT_FULLY_INITIALIZED,		(SM_START - 44),        L"SM DD file not fully initialized")
rcode_def (     SM_EMPTY_DD_FILE,						(SM_START - 45),        L"SM Empty DD File")
rcode_def (     SM_UNSUPPORTED_FEATURE_IN_DD,			(SM_START - 46),        L"SM Unsupported feature in DD")
rcode_def (     SM_FF_CONTROL_NET_TYPES_NOT_FOUND,		(SM_START - 47),        L"SM FF Control Net Comp Types not found in fms.ini")
rcode_def (     SM_UNEXPECTED_COM_EXCEPTION,		    (SM_START - 48),        L"SM An unpected COM exception has occured during processing")
rcode_def (     SM_ERROR_END,                           (SM_START - 99),        L"SM Error end")


/* DDS error codes */
rcode_def (     SUCCESS,                                (0),                    L"Success")
rcode_def (     FAILURE,                                (1),					L"Failure")
rcode_def (     DDS_SUCCESS,                            (0),                    L"DDS Success")
rcode_def (     DDS_OUT_OF_MEMORY,                      (DDS_START - 2),        L"Out of Memory")
rcode_def (     VARIABLE_VALUE_NEEDED,                  (DDS_START - 3),        L"Variable Value Needed")
rcode_def (     DDS_WRONG_DDOD_REVISION,                (DDS_START - 4),        L"DDS Wrong DDOD Revision")
rcode_def (		DDS_WRONG_TOK_TYPE,						(DDS_START - 5),		L"DDS Wrong Tokenizer Type")
rcode_def (		ENUM_NOT_FOUND,							(DDS_START - 6),		L"DDS Enumeration Not Found")
rcode_def (     DDS_ERROR_END,                          (DDS_START - 99),       L"end of DDS error numbers")


/* EVAL error codes */
rcode_def (     DDL_SUCCESS,                            (0),                    L"EVAL Success")
rcode_def (     DDL_MEMORY_ERROR,                       (EVAL_START - 1),       L"EVAL Memory Error")
rcode_def (     DDL_INSUFFICIENT_OCTETS,                (EVAL_START - 2),       L"EVAL Insufficient octets")
rcode_def (     DDL_SHORT_BUFFER,                       (EVAL_START - 3),       L"EVAL Short buffer")
rcode_def (     DDL_ENCODING_ERROR,                     (EVAL_START - 4),       L"EVAL Encoding error")
rcode_def (     DDL_LARGE_VALUE,                        (EVAL_START - 5),       L"EVAL Large value")
rcode_def (     DDL_DIVIDE_BY_ZERO,                     (EVAL_START - 6),       L"EVAL Divide by zero")
rcode_def (     DDL_BAD_VALUE_TYPE,                     (EVAL_START - 7),       L"EVAL Bad value type")
rcode_def (     DDL_SERVICE_ERROR,                      (EVAL_START - 8),       L"EVAL Service error")
rcode_def (     DDL_FILE_ERROR,                         (EVAL_START - 9),       L"EVAL File error")
rcode_def (     DDL_BAD_FILE_TYPE,                      (EVAL_START - 10),      L"EVAL Bad file type")
rcode_def (     DDL_FILE_NOT_FOUND,                     (EVAL_START - 11),      L"EVAL File not found")
rcode_def (     DDL_OUT_OF_DATA,                        (EVAL_START - 12),      L"EVAL Out of data")
rcode_def (     DDL_DEFAULT_ATTR,                       (EVAL_START - 13),      L"EVAL Default attr")
rcode_def (     DDL_NULL_POINTER,                       (EVAL_START - 14),      L"EVAL Null pointer")
rcode_def (     DDL_INCORRECT_FILE_FORMAT,              (EVAL_START - 15),      L"EVAL Incorrect file format")
rcode_def (     DDL_INVALID_PARAM,                      (EVAL_START - 16),      L"EVAL Invalid parameter")
rcode_def (     DDL_CHECK_RETURN_LIST,                  (EVAL_START - 17),      L"EVAL errors are loaded in the return list")
rcode_def (     DDL_DEV_SPEC_STRING_NOT_FOUND,          (EVAL_START - 18),      L"EVAL Device Specific string not found")
rcode_def (     DDL_DICT_STRING_NOT_FOUND,              (EVAL_START - 19),      L"EVAL Dict string not found")
rcode_def (     DDL_READ_VAR_VALUE_FAILED,              (EVAL_START - 20),      L"EVAL Read var value failed")
rcode_def (     DDL_BINARY_REQUIRED,                    (EVAL_START - 21),      L"EVAL Binary required")
rcode_def (     DDL_VAR_TYPE_NEEDED,                    (EVAL_START - 22),      L"EVAL Var type needed")
rcode_def (     DDL_EXPR_STACK_OVERFLOW,                (EVAL_START - 23),      L"EVAL Expression stack has overflowed")
rcode_def (     DDL_INVALID_TYPE_SUBATTR,               (EVAL_START - 24),      L"EVAL subattr not supported by var type")
rcode_def (     DDL_BAD_FETCH_TYPE,                     (EVAL_START - 25),      L"EVAL bad fetch type returned by ABT_DEV_FAMILY")
rcode_def (     DDL_RESOLVE_FETCH_FAIL,                 (EVAL_START - 26),      L"EVAL null fetch upcall pointer")
rcode_def (     DDL_BLOCK_TABLES_NOT_LOADED,            (EVAL_START - 27),      L"EVAL block tables are not loaded")
rcode_def (		DDL_DEVICE_DIR_NOT_LOADED,				(EVAL_START - 28),		L"EVAL device dir is not loaded")
rcode_def(		DDL_INVALID_REFERENCE,					(EVAL_START - 29),		L"EDDL validity is false for this reference")
rcode_def (     DDL_ERROR_END,                          (EVAL_START - 99),      L"end of EVAL error numbers")


/* FETCH error codes */
rcode_def (     FETCH_INVALID_DEVICE_HANDLE,            (FETCH_START - 0),      L"FETCH Invalid device handle")
rcode_def (     FETCH_DEVICE_NOT_FOUND,                 (FETCH_START - 1),      L"FETCH Device not found")
rcode_def (     FETCH_INVALID_DEV_TYPE_HANDLE,          (FETCH_START - 2),      L"FETCH Invalid Device type handle")
rcode_def (     FETCH_DEV_TYPE_NOT_FOUND,               (FETCH_START - 3),      L"FETCH Device type not found")
rcode_def (     FETCH_INVALID_DD_HANDLE_TYPE,           (FETCH_START - 4),      L"FETCH Invalid dd handle type")
rcode_def (     FETCH_TABLES_NOT_FOUND,                 (FETCH_START - 5),      L"FETCH Tables not found")
rcode_def (     FETCH_ITEM_NOT_FOUND,                   (FETCH_START - 6),      L"FETCH Item not found")
rcode_def (     FETCH_DIRECTORY_NOT_FOUND,              (FETCH_START - 7),      L"FETCH Block not found")
rcode_def (     FETCH_INSUFFICIENT_SCRATCHPAD,          (FETCH_START - 10),     L"FETCH Insufficient scratchpad memory provided")
rcode_def (     FETCH_NULL_POINTER,                     (FETCH_START - 11),     L"FETCH Null pointer passed")
rcode_def (     FETCH_ITEM_TYPE_MISMATCH,               (FETCH_START - 12),     L"FETCH Requested item type not in this object")
rcode_def (     FETCH_INVALID_ATTRIBUTE,                (FETCH_START - 13),     L"FETCH Invalid attribute for item type")
rcode_def (     FETCH_INVALID_RI,                       (FETCH_START - 14),     L"FETCH Invalid reference indicator")
rcode_def (     FETCH_INVALID_ITEM_TYPE,                (FETCH_START - 15),     L"FETCH Invalid item type")
rcode_def (     FETCH_EMPTY_ITEM_MASK,                  (FETCH_START - 16),     L"FETCH Object has no mask bits set")
rcode_def (     FETCH_ATTRIBUTE_NO_MASK_BIT,            (FETCH_START - 17),     L"FETCH Mask bit not set for attribute")
rcode_def (     FETCH_ATTRIBUTE_NOT_FOUND,              (FETCH_START - 18),     L"FETCH Attribute not found in object")
rcode_def (     FETCH_ATTR_LENGTH_OVERFLOW,             (FETCH_START - 19),     L"FETCH Unpacked integer exceeds maximum size")
rcode_def (     FETCH_ATTR_ZERO_LENGTH,                 (FETCH_START - 20),     L"FETCH Attribute has zero length")
rcode_def (     FETCH_OBJECT_NOT_FOUND,                 (FETCH_START - 21),     L"FETCH Object not found")
rcode_def (     FETCH_DATA_NOT_FOUND,                   (FETCH_START - 22),     L"FETCH Local data area not found")
rcode_def (     FETCH_DATA_OUT_OF_RANGE,                (FETCH_START - 23),     L"FETCH Requested data out of local data range")
rcode_def (     FETCH_INVALID_DIR_TYPE,                 (FETCH_START - 24),     L"FETCH Invalid directory type")
rcode_def (     FETCH_INVALID_TABLE,                    (FETCH_START - 25),     L"FETCH Invalid table for directory type")
rcode_def (     FETCH_INVALID_EXTN_LEN,                 (FETCH_START - 26),     L"FETCH Invalid extension length")
rcode_def (     FETCH_INVALID_PARAM,                    (FETCH_START - 27),     L"FETCH Invalid parameter")
rcode_def (     FETCH_INVALID_ITEM_ID,                  (FETCH_START - 28),     L"FETCH Invalid item ID")
rcode_def (     FETCH_INVALID_ATTR_LENGTH,              (FETCH_START - 29),     L"FETCH Invalid attribute length value")
rcode_def (     FETCH_BAD_DD_DEVICE_LOAD,               (FETCH_START - 30),     L"FETCH Bad dd device load")
rcode_def (     FETCH_DIR_TYPE_MISMATCH,                (FETCH_START - 31),     L"FETCH Directory type mismatch")
rcode_def (     FETCH_NO_OBJ_EXTN,                      (FETCH_START - 32),     L"FETCH No object extension")
rcode_def (     FETCH_ERROR_END,                        (FETCH_START - 99),     L"end of FETCH error numbers")


/* DDI error codes */
rcode_def (     DDI_INVALID_FLAT_FORMAT,                (DDI_START - 1),        L"DDI reports an invalid flat structure")
rcode_def (     DDI_INVALID_FLAT_MASKS,                 (DDI_START - 2),        L"DDI reports an invalid flat masks")
rcode_def (     DDI_INVALID_TYPE,                       (DDI_START - 3),        L"DDI reports an invalid specifier type")
rcode_def (     DDI_INVALID_ITEM_TYPE,                  (DDI_START - 4),        L"DDI reports an invalid item type")
rcode_def (     DDI_INVALID_PARAM,                      (DDI_START - 5),        L"DDI reports bad parameters in the call")
rcode_def (     DDI_MEMORY_ERROR,                       (DDI_START - 6),        L"DDI reports memory error")
rcode_def (     DDI_TAB_BAD_NAME,                       (DDI_START - 7),        L"DDI bad param name or param list name")
rcode_def (     DDI_TAB_BAD_DDID,                       (DDI_START - 8),        L"DDI bad DDID")
rcode_def (     DDI_TAB_BAD_OP_INDEX,                   (DDI_START - 9),        L"DDI bad operational index")
rcode_def (     DDI_TAB_NO_DEVICE,                      (DDI_START - 10),       L"DDI device does not exist")
rcode_def (     DDI_TAB_NO_BLOCK,                       (DDI_START - 11),       L"DDI block does not exist")
rcode_def (     DDI_TAB_BAD_PARAM_OFFSET,               (DDI_START - 12),       L"DDI bad param table offset")
rcode_def (     DDI_TAB_BAD_PARAM_LIST_OFFSET,          (DDI_START - 13),       L"DDI bad param list table offset")
rcode_def (     DDI_TAB_BAD_CHAR_OFFSET,                (DDI_START - 14),       L"DDI bad character offset")
rcode_def (     DDI_TAB_BAD_PARAM_MEMBER,               (DDI_START - 15),       L"DDI bad param table member")
rcode_def (     DDI_TAB_BAD_PARAM_LIST_MEMBER,          (DDI_START - 16),       L"DDI bad param list table member")
rcode_def (     DDI_TAB_BAD_SUBINDEX,                   (DDI_START - 17),       L"DDI bad table subindex")
rcode_def (     DDI_TAB_BAD_PARAM_TYPE,                 (DDI_START - 18),       L"DDI bad parameter item type")
rcode_def (     DDI_INVALID_BLOCK_HANDLE,               (DDI_START - 19),       L"DDI invalid block handle")
rcode_def (     DDI_BLOCK_NOT_FOUND,                    (DDI_START - 20),       L"DDI block not found")
rcode_def (     DDI_INVALID_DEVICE_HANDLE,              (DDI_START - 21),       L"DDI invalid_device handle")
rcode_def (     DDI_DEVICE_NOT_FOUND,                   (DDI_START - 22),       L"DDI device not found")
rcode_def (     DDI_INVALID_DEV_TYPE_HANDLE,            (DDI_START - 23),       L"DDI invalid device type handle")
rcode_def (     DDI_DEV_TYPE_NOT_FOUND,                 (DDI_START - 24),       L"DDI device type not found")
rcode_def (     DDI_INVALID_DD_HANDLE,                  (DDI_START - 25),       L"DDI invalid dd handle")
rcode_def (     DDI_INVALID_DD_HANDLE_TYPE,             (DDI_START - 26),       L"DDI invalid dd handle type")
rcode_def (     DDI_TAB_BAD,                            (DDI_START - 27),       L"DDI invalid table")
rcode_def (     DDI_LEGAL_ENUM_VAR_VALUE,               (DDI_START - 28),       L"DDI found the value in the flat variable")
rcode_def (     DDI_ILLEGAL_ENUM_VAR_VALUE,             (DDI_START - 29),       L"DDI did not find the value in the flat variable")
rcode_def (     DDI_DD_BLOCK_REF_NOT_FOUND,             (DDI_START - 30),       L"DDI DD block reference not found")
rcode_def (     DDI_DEVICE_TABLES_NOT_FOUND,            (DDI_START - 31),       L"DDI device tables not found")
rcode_def (     DDI_BLOCK_TABLES_NOT_FOUND,             (DDI_START - 32),       L"DDI block tables not found")
rcode_def (     DDI_INVALID_BLOCK_TAG,                  (DDI_START - 33),       L"DDI invalid block tag")
rcode_def (     DDI_INVALID_REQUEST_TYPE,               (DDI_START - 34),       L"DDI invalid request type")
rcode_def (     DDI_TAB_NO_UNIT,                        (DDI_START - 35),       L"DDI no unit available")
rcode_def (     DDI_TAB_NO_WAO,                         (DDI_START - 36),       L"DDI no wao available")
rcode_def (     DDI_TAB_NO_UPDATE,                      (DDI_START - 37),       L"DDI no update available")
rcode_def (     DDI_INVALID_DEV_SPEC_STRING,            (DDI_START - 38),       L"DDI invalid device specific string")
rcode_def (     DDI_INSUFFICIENT_BUFFER,                (DDI_START - 39),       L"DDI invalid device specific string")
rcode_def (     DDI_BAD_DD_BLOCK_LOAD,                  (DDI_START - 40),       L"DDI Bad dd block load")
rcode_def (     DDI_BAD_DD_DEVICE_LOAD,                 (DDI_START - 41),       L"DDI Bad dd device load")
rcode_def (     DDI_NO_COMMANDS_FOR_PARAM,              (DDI_START - 42),       L"DDI Param has no commands")
rcode_def (     DDI_NO_READ_COMMANDS_FOR_PARAM,         (DDI_START - 43),       L"DDI Param has no read commands")
rcode_def (     DDI_NO_WRITE_COMMANDS_FOR_PARAM,        (DDI_START - 44),       L"DDI Param has no write commands")
rcode_def (     DDI_COMMAND_NOT_FOUND,                  (DDI_START - 45),       L"DDI Command was not found")
rcode_def (     DDI_TAB_BAD_PARAM_COUNT,                (DDI_START - 46),       L"DDI Bad param count")
rcode_def (     DDI_ERROR_END,                          (DDI_START - 99),       L"end of DDI error numbers")


/* Method Interpreter Errors */                    
rcode_def (     METH_METHOD_COLLISION,                  (METH_START-1),         L"Multiple incompatible methods active")
rcode_def (     METH_INTERNAL_ERROR,                    (METH_START-2),         L"Internal program problem - Aborting")
rcode_def (     METH_TOO_MANY_METHODS,                  (METH_START-3),         L"Too many methods active at once")
rcode_def (     METH_METHOD_ABORT,                      (METH_START-4),         L"Method aborted")
rcode_def (     METH_METHOD_RETRY,                      (METH_START-5),         L"Method retrying")
rcode_def ( METH_PARSE_ERROR,                           (METH_START-6),         L"Method syntax error")
rcode_def ( METH_INIT_ERROR,							(METH_START-7),         L"Method initialization failed")
rcode_def ( METH_METHOD_INVALID,                        (METH_START-8),         L"Method is currently invalid")
rcode_def (     METH_ERROR_END,                         (METH_START-99),        L"end of method error numbers")

/* Parameter Cache Errors */                    
rcode_def ( PC_SUCCESS,                                 (0),                    L"PC Success")
rcode_def ( PC_ERROR_START,                             (PC_START-1),           L"The start of the PC errors")
rcode_def ( PC_NO_READ_OVER_CHANGED_PARAM,              (PC_START-2),           L"Attempt to read a value which was changed")
rcode_def ( PC_INTERNAL_ERROR,                          (PC_START-3),           L"Inconsistent state detected internally")
rcode_def ( PC_WRONG_DATA_TYPE,                         (PC_START-4),           L"The param request has the wrong value type")
rcode_def ( PC_NO_MEMORY,                               (PC_START-5),           L"No memory to perform requested action")
rcode_def ( PC_VALUE_NOT_SET,                           (PC_START-6),           L"Get would result in an invalid read operation")
rcode_def ( PC_ENTRY_BUSY,                              (PC_START-7),           L"Another process is using the param cache entry")
rcode_def ( PC_PARAM_NOT_FOUND,                         (PC_START-8),           L"The requested param reference does not exist")
rcode_def ( PC_BUFFER_TOO_SMALL,                        (PC_START-9),           L"Return buffer too small for data, data truncated")
rcode_def ( PC_BAD_BUCKET,                              (PC_START-10),          L"The bucket parameter is not set or wrong")
rcode_def ( PC_BAD_REQ_TYPE,                            (PC_START-11),          L"The parameter req_type is not set properly")
rcode_def ( PC_BAD_BLOCK_HANDLE,                        (PC_START-12),          L"The block_handle parameter is invalid")
rcode_def ( PC_NO_DEV_VALUE_WRITE,                      (PC_START-13),          L"The device values cannot be written")
rcode_def ( PC_NO_ACTION_VALUE,                         (PC_START-14),          L"No action value exists")
rcode_def ( PC_PARAM_CANNOT_BE_WRITTEN,                 (PC_START-15),          L"A param of this class cannot be written")
rcode_def ( PC_PARAM_CANNOT_BE_READ,                    (PC_START-16),          L"A param of this class cannot be read")
rcode_def ( PC_BAD_POINTER,                             (PC_START-17),          L"A passed parameter which is a pointer is NULL")
rcode_def ( PC_BAD_METHOD_TAG,                          (PC_START-18),          L"The specified method_tag is invalid")
rcode_def ( PC_BAD_PARAM_REQUEST,                       (PC_START-19),          L"value access to only single parameter values")
rcode_def ( PC_NO_DATA_TO_SEND,                         (PC_START-20),          L"the requested param to send has not been changed")
rcode_def ( PC_BUSY,                                    (PC_START-21),          L"Comm. for this request is already in progress")
rcode_def ( PC_VALUE_NOT_SET_IN_FS,                     (PC_START-22),          L"The value is not set in the File Server")
rcode_def ( PC_METHOD_COLLISION,                        (PC_START-23),          L"Multiple methods attempting to change same param")
rcode_def ( PC_NOT_READY_FOR_WRITING,                   (PC_START-24),          L"The Param Cache Is Performing a Background Read--Please wait")
rcode_def ( PC_DEPEND_VAR_NOT_COMMITTED,                (PC_START-25),          L"Dependent Variable not yet committed")
rcode_def ( PC_REFRESH_TARGET_EDITED,                   (PC_START-26),          L"Refresh Target Edited")
rcode_def ( PC_LINK_DUPLICATE_COMMAND,                  (PC_START-27),          L"This request resolves to a duplicate (pending) command")
rcode_def ( PC_INVALID_BGR_PARAM_REQUEST,               (PC_START-28),          L"The background read property requested is invalid")
rcode_def ( PC_BGR_OBJECT_INACCESSABLE,                 (PC_START-29),          L"The background read object is inaccessable")
rcode_def ( PC_BGR_OBJECT_UNKNOWN_RETURN_CODE,          (PC_START-30),          L"The background read object returned an unknown return code")
rcode_def ( PC_BRG_UPDATE_PARAMS_FAILED,				(PC_START-31),          L"Device/database synchronization failed during parameter update with database")
rcode_def ( PC_BGR_COMMIT_PARAMS_FAILED,				(PC_START-32),          L"Device/database synchronization failed during commit operation with database")
rcode_def ( PC_BGR_DEVICE_CANCELLED_BY_USER,            (PC_START-33),          L"Device/database synchronization cancelled by user")
rcode_def ( PC_BGR_DEVICE_DB_SYNC_EVENT_FAILED,         (PC_START-34),          L"Device/database synchronization failed while logging device/database synchronization event to database")
rcode_def ( PC_PARAM_CACHE_NOT_FOUND,		            (PC_START-35),          L"Param cache not open")
rcode_def ( PC_FAIL,									(PC_START-36),          L"General param cache failure")
rcode_def ( PC_ATTEMPT_FS_METHOD_RUN,					(PC_START-37),          L"Cannot run user methods on file server")
rcode_def ( PC_TOO_MANY_METHODS,						(PC_START-38),          L"Only one user method can run at a time")
rcode_def ( PC_NO_METHOD_RUNNING,						(PC_START-39),          L"No method running")
rcode_def ( PC_WILL_NOTIFY_WHEN_DONE,					(PC_START-40),          L"Client object will be notified when current operation is complete")
rcode_def ( PC_TAKE_OVER_COMMITTER,						(PC_START-41),          L"The current committer must be taken over by the next committer")
rcode_def ( PC_PARAM_NOT_VALID,							(PC_START-42),          L"Trying to fetch a value that is currently invalid")
rcode_def ( PC_VALUE_IS_EMPTY,							(PC_START-43),          L"Value has not been read yet")
rcode_def ( PC_USE_EXISTING_COMMITTER,					(PC_START-44),          L"Commit operation should use the current committer")
rcode_def ( PC_ERROR_END,                               (PC_START-99),           L"end of Parameter Cache error numbers")

/* layer 7 errors */
rcode_def ( L7_ERROR_START,								(L7_START-1),           L"start of Layer 7 error numbers")
rcode_def ( L7_INVALID_VAR_TYPE,                        (L7_START-2),           L"var_type not an element of enum VAR_TYPE")
rcode_def ( L7_INVALID_DD_ITEM_TYPE,                    (L7_START-3),           L"DDLI passed L7 Invalid Item Type")
rcode_def ( L7_ALREADY_ACTIVE,                          (L7_START-4),           L"layer7 is not reentrant") /* NOT USED?? */
rcode_def ( L7_INVALID_FUNCTION_PTR,                    (L7_START-5),           L"pointer to function was not initialized")
rcode_def ( L7_NO_RESPONSE,                             (L7_START-6),           L"field device did not respond to a layer7 request")
rcode_def ( L7_LOCAL_COMM_ERROR,                        (L7_START-7),           L"Comm error detected by the host on a response from a field device")
rcode_def ( L7_REMOTE_COMM_ERROR,                       (L7_START-8),           L"Comm error detected by a field device on a request from the host")
rcode_def ( L7_DEVICE_BUSY,                             (L7_START-9),           L"field device can not be interrupted")
rcode_def ( L7_CMD_NOT_IMPLEMENTED,                     (L7_START-10),          L"field device does not support current command")
rcode_def ( L7_DEVICE_ERROR,                            (L7_START-11),          L"Device error occurred during identification")
rcode_def ( L7_INVALID_RESPONSE_CODE,                   (L7_START-12),          L"response code not found in command description")
rcode_def ( L7_UNKNOWN_DEVICE,                          (L7_START-13),          L"netman: device does not exist")
rcode_def ( L7_NO_VAR_MATCH,                            (L7_START-14),          L"variable not found in cmd trans") /*NOT USED??*/
rcode_def ( L7_NO_READ_COMMAND,                         (L7_START-15),          L"No read command for variable")
rcode_def ( L7_NO_WRITE_COMMAND,                        (L7_START-16),          L"No Write Command for Variable")
rcode_def ( L7_NO_COMMAND,                              (L7_START-17),          L"No Command description")
rcode_def ( L7_INVALID_REQ_DATA_FIELD,                  (L7_START-18),          L"DD specifies too many request data bytes")
rcode_def ( L7_INVALID_REPLY_DATA_FIELD,                (L7_START-19),          L"DD expecting more data bytes in response")
rcode_def ( L7_INVALID_NUM_OF_REQ_ITEMS,                (L7_START-20),          L"Too many request items")
rcode_def ( L7_INVALID_NUM_OF_REPLY_ITEMS,              (L7_START-21),          L"Too many reply items")
rcode_def ( L7_INVALID_VARIABLE_LENGTH,                 (L7_START-22),          L"Variable is larger than expected")
rcode_def ( L7_INVALID_RESP_CODE_COUNT,                 (L7_START-23),          L"Too many response code items")
rcode_def ( L7_INVALID_COMM_STAT_COUNT,                 (L7_START-24),          L"Too many communication status items")
rcode_def ( L7_INVALID_DEVICE_STAT_COUNT,               (L7_START-25),          L"Too many device_status items")
rcode_def ( L7_DDI_NO_MATCH,                            (L7_START-26),          L"Variable not found in command list")
rcode_def ( L7_INVALID_RESPONSE_CMD,                    (L7_START-27),          L"Response command does not equal request command" )
rcode_def ( L7_POLL_ADDR_ALREADY_USED,                  (L7_START-28),          L"A device already exists at this poll address" )
rcode_def ( L7_WARNING_DATA_EXPECTED,                   (L7_START-29),          L"Too few data bytes received")
rcode_def ( L7_TRANSMITTER_FAULT,                       (L7_START-30),          L"Transmitter fault")
rcode_def ( L7_INVALID_TRANSACTION_NUMBER,              (L7_START-31),          L"Invalid transaction number")
rcode_def ( L7_INVALID_DEVICE_HANDLE,                   (L7_START-32),          L"Invalid device handle")
rcode_def ( L7_INVALID_BLOCK_HANDLE,                    (L7_START-33),          L"Invalid block handle")
rcode_def ( L7_INVALID_DATE,                            (L7_START-34),          L"Invalid date specified")
rcode_def ( L7_CIRCULAR_DEPENDENCY_DETECTED,			(L7_START-35),			L"Circular Dependency Detected")
rcode_def ( L7_ERROR_END,                               (L7_START-99),          L"end of Layer 7 error numbers")

/* network management errors */
rcode_def ( NM_ERROR_START,                             (NM_START-1),           L"start of netman error numbers")
rcode_def ( HC_UNKNOWN_DEVICE,                          (NM_START-2),           L"data transmission error")
rcode_def ( HC_MSG_NOT_SENT,                            (NM_START-3),           L"DLL was not able to send request")
rcode_def ( HC_NO_RESPONSE,                             (NM_START-4),           L"field device did not respond")
rcode_def ( HC_COMM_ERROR,                              (NM_START-5),           L"Comm error detected by the host on a response from a field device")
rcode_def ( HC_REMOTE_COMM_ERROR,                       (NM_START-6),           L"Comm error detected by a field device on a request from the host")
rcode_def ( HC_INVALID_CMD,                             (NM_START-7),           L"field device does not support this command")
rcode_def ( HC_DEVICE_ERROR,                            (NM_START-8),           L"bits 0-5 of first response code byte were set")
rcode_def ( HC_INVALID_PARAM,                           (NM_START-9),           L"invalid_parameter")
rcode_def ( HC_NO_MEM,                                  (NM_START-10),          L"Memory exhausted")
rcode_def ( HC_ACTIVE ,                                 (NM_START-11),          L"Communications port already initialized")
rcode_def ( HC_INACTIVE,                                (NM_START-12),          L"Communications port not initialized")
rcode_def ( HC_NO_PROCESS,                              (NM_START-13),          L"All communications ports active")
rcode_def ( HC_CLEANUP,                                 (NM_START-14),          L"Installation of cleanup procedures failed ")
rcode_def ( HC_EXIST,                                   (NM_START-15),          L"Master address has been initilized")
rcode_def ( HC_UART_FAILURE,                            (NM_START-16),          L"Uart initialization error")
rcode_def ( HC_CMD_NOT_IMPLEMENTED,                     (NM_START-17),          L"Command not implemented")
rcode_def ( HC_DEVICE_BUSY,                             (NM_START-18),          L"Device is currently busy")
rcode_def ( HC_RE_IDENT,                                (NM_START-19),          L"Attempt to re-identify device")
rcode_def ( HC_UPDATE_KNOWN_DEVICE,                     (NM_START-20),          L"Update netman table for existing device")
rcode_def ( HC_HANDLE_UNAVAILABLE,                      (NM_START-21),          L"DDLI was not able to provide a handle")
rcode_def ( NM_UNKNOWN_VARIABLE,                        (NM_START-22),          L"Variable not in netman table")
rcode_def ( NM_UNKNOWN_TABLE,                           (NM_START-23),          L"No netman table for specified handle")
rcode_def ( HC_DEVICE_DISCONNECTED,                     (NM_START-24),          L"Device has been disconnected")
rcode_def ( HC_DR_BUSY,                                 (NM_START-25),          L"Delayed Response - BUSY")
rcode_def ( HC_DR_INITIATE,                             (NM_START-26),          L"Delayed Response - INITIATE")
rcode_def ( HC_DR_RUNNING,                              (NM_START-27),          L"Delayed Response - RUNNING")
rcode_def ( HC_DR_DEAD,                                 (NM_START-28),          L"Delayed Response - DEAD")
rcode_def ( L2_INVALID_RESP_CODE_COUNT,                 (NM_START-29),          L"Too many response code items")
rcode_def ( L2_INVALID_COMM_STAT_COUNT,                 (NM_START-30),          L"Too many communication status items")
rcode_def ( HC_CONT_DATA_NOT_READY,                     (NM_START-31),          L"Data not ready for continuous send cmd")
rcode_def ( HC_INVALID_POINTER,                         (NM_START-32),          L"A Valid Pointer was expected")
rcode_def ( HC_INVALID_ADDRESS_TYPE,                    (NM_START-33),          L"Request is using an invalid Hart address type.")
rcode_def ( HC_INVALID_MSG_TYPE,                        (NM_START-34),          L"Comm stack has detected an invalid message type")
rcode_def ( HC_Q_EMPTY,                                 (NM_START-35),          L"Comm stack Q is Empty")
rcode_def ( HC_BAD_POINTER,                             (NM_START-36),          L"Comm stack was given an invalid pointer")
rcode_def ( HC_CANT_SET_DEVICE_ID,                      (NM_START-37),          L"Device Id can not be changed on Rev 5 or greater")
rcode_def ( HC_DRIVER_Q_FULL,                           (NM_START-38),          L"The Driver Q is Full")
rcode_def ( HC_BAD_POLL_ADDR,                           (NM_START-39),          L"Invalid poll address, check Network Config")
rcode_def ( HC_DRIVER_NOT_OPEN,                         (NM_START-40),          L"Comm initialization error.  Either the HART device driver is not loaded, or the communications port is incorrectly configured, or if you are using a USB HART modem, it may be disconnected.")
rcode_def ( HC_DRIVER_NOT_CONFIG,                       (NM_START-41),          L"Unable to configure the HART Driver")
rcode_def ( HC_DRIVER_NOT_SUBSCRIBE,                    (NM_START-42),          L"Unable to subscribe the HART Driver")
rcode_def ( HC_DRIVER_NOT_REGISTER,                     (NM_START-43),          L"Unable to register the HART Driver")
rcode_def ( HC_DRIVER_NOT_READ,                         (NM_START-44),          L"Unable to read the HART Driver")
rcode_def ( HC_DRIVER_NOT_WRITE,                        (NM_START-44),          L"Unable to write the HART Driver")
rcode_def ( HC_DRIVER_NOT_CLOSE,                        (NM_START-45),          L"Unable to close the HART Driver")
rcode_def ( HC_INVALID_RESPONSE,                        (NM_START-46),          L"Invalid response - the id/address/cmd did not match the request")
rcode_def ( HC_LOCKED,							        (NM_START-47),          L"Field Device is under locked logic solver")
rcode_def ( NM_ERROR_END,                               (NM_START-99),          L"end of netman error numbers")
 
/* Comm Stack errors */
rcode_def ( PS_SUCCESS,                                 (0),                    L"Success")
rcode_def ( CS_ERROR_START,                             (CS_START-1),           L"start of comm. stack error numbers")
rcode_def ( PS_INVALID_BLOCK_HANDLE,                    (CS_START-2),           L"Provider Service invalid block handle")
rcode_def ( PS_INVALID_DEVICE_HANDLE,                   (CS_START-3),           L"Provider Service invalid device handle")
rcode_def ( PS_INVALID_HART_TYPE,                       (CS_START-4),           L"Provider Service invalid hart type")
rcode_def ( CS_COMM_STATUS_ERROR,                       (CS_START-5),           L"Comm Stack comm_status error")
rcode_def ( CS_RESPONSE_CODE_ERROR,                     (CS_START-6),           L"Comm Stack response_code error")
rcode_def ( CS_DEVICE_STATUS_ERROR,                     (CS_START-7),           L"Comm Stack device_status error")
rcode_def ( CS_TRANSMITTER_STATUS_ERROR,                (CS_START-8),           L"Comm Stack transmitter_status error")
rcode_def ( CS_RESPONSE_CODE_WARNING,                   (CS_START-9),           L"Comm Stack response_code warning")
rcode_def ( CS_COMM_STACK_ERROR,                        (CS_START-10),          L"Comm Stack error, look in detail for more info")
rcode_def ( CS_UNSOLICITED_RESPONSE_WARNING,            (CS_START-11),          L"Comm Stack received unsolicited response. Check for matching (Pri/Sec) master.")
rcode_def ( CS_ERROR_END,                               (CS_START-99),          L"end of comm. stack error numbers")


/* Connection Manager Errors */
rcode_def ( CM_SUCCESS,                                 (0),                    L"Success")
rcode_def ( CM_NO_MEMORY,                               (CM_START-0),           L"CM No memory")
rcode_def ( CM_BLOCK_NOT_FOUND,                         (CM_START-1),           L"CM Block not found")
rcode_def ( CM_DEVICE_NOT_FOUND,                        (CM_START-2),           L"CM Device not found")
rcode_def ( CM_DEVICE_TYPE_NOT_FOUND,                   (CM_START-3),           L"CM Device type not found")
rcode_def ( CM_BAD_BLOCK_OPEN,                          (CM_START-4),           L"CM Bad block open")
rcode_def ( CM_BAD_BLOCK_CLOSE,                         (CM_START-5),           L"CM Bad block close")
rcode_def ( CM_BAD_BLOCK_HANDLE,                        (CM_START-6),           L"CM Bad block handle")
rcode_def ( CM_BAD_PARAM_OFFSET,                        (CM_START-7),           L"CM Bad parameter offset")
rcode_def ( CM_BAD_INDEX,                               (CM_START-8),           L"CM Bad index")
rcode_def ( CM_BAD_OPEN,                                (CM_START-9),           L"CM Bad open")
rcode_def ( CM_NOT_DDOD_FILE,                           (CM_START-10),          L"CM Not DDOD file")
rcode_def ( CM_FILE_ERROR,                              (CM_START-11),          L"CM File error")
rcode_def ( CM_NO_DEVICE,                               (CM_START-12),          L"CM No device")
rcode_def ( CM_NO_BLOCK,                                (CM_START-13),          L"CM No block")
rcode_def ( CM_BAD_BLOCK_OBJECT,                        (CM_START-14),          L"CM Bad block object")
rcode_def ( CM_NO_FUNCTION,                             (CM_START-15),          L"CM No function")
rcode_def ( CM_BAD_SYMBOL,                              (CM_START-16),          L"CM Bad symbol")
rcode_def ( CM_BAD_DEVICE_HANDLE,                       (CM_START-17),          L"CM Bad device handle")
rcode_def ( CM_NO_COMM,                                 (CM_START-18),          L"CM No communications")
rcode_def ( CM_BAD_ROD_OPEN,                            (CM_START-19),          L"CM Bad ROD open")
rcode_def ( CM_BLOCK_TAG_NOT_FOUND,                     (CM_START-20),          L"CM Block tag not found")
rcode_def ( CM_INVALID_ROD_HANDLE,                      (CM_START-21),          L"CM Invalid ROD handle")
rcode_def ( CM_ILLEGAL_PUT,                             (CM_START-22),          L"CM Illegal put")
rcode_def ( CM_NO_ROD_MEMORY,                           (CM_START-23),          L"CM No ROD memory")
rcode_def ( CM_OBJECT_NOT_IN_HEAP,                      (CM_START-24),          L"CM Object not in heap")
rcode_def ( CM_NO_OBJECT,                               (CM_START-25),          L"CM No object")
rcode_def ( CM_NO_VALUE,                                (CM_START-26),          L"CM No value")
rcode_def ( CM_BAD_DD_BLK_LOAD,                         (CM_START-27),          L"CM Bad DD block load")
rcode_def ( CM_BAD_DD_DEV_LOAD,                         (CM_START-28),          L"CM Bad DD device load")
rcode_def ( CM_NO_DD_BLK_REF,                           (CM_START-29),          L"CM No DD block reference")
rcode_def ( CM_DD_NOT_FOUND,                            (CM_START-30),          L"CM DDL file not found")
rcode_def ( CM_BAD_DD_FLAT_LOAD,                        (CM_START-31),          L"CM Bad DD flat load")
rcode_def ( CM_BAD_DDOD_LOAD,                           (CM_START-32),          L"CM Bad DDOD load")
rcode_def ( CM_BAD_DD_HANDLE,                           (CM_START-33),          L"CM Bad DD handle")
rcode_def ( CM_BAD_BLOCK_LOAD,                          (CM_START-34),          L"CM Bad block load")
rcode_def ( CM_ILLEGAL_WRITE,                           (CM_START-35),          L"CM Illegal write")
rcode_def ( CM_VALUE_NOT_IN_HEAP,                       (CM_START-36),          L"CM Value not in heap")
rcode_def ( CM_NO_DATA_TYPE,                            (CM_START-37),          L"CM No data type")
rcode_def ( CM_BAD_OBJECT,                              (CM_START-38),          L"CM Bad object")
rcode_def ( CM_BAD_SUBINDEX,                            (CM_START-39),          L"CM Bad subindex")
rcode_def ( CM_BAD_OPROD_OPEN,                          (CM_START-40),          L"CM Bad OPROD open")
rcode_def ( CM_BAD_FILE_OPEN,                           (CM_START-41),          L"CM Bad file open")
rcode_def ( CM_BAD_FILE_CLOSE,                          (CM_START-42),          L"CM Bad file close")
rcode_def ( CM_COMM_ERR,                                (CM_START-43),          L"CM Communications error")
rcode_def ( CM_RESPONSE_CODE,                           (CM_START-44),          L"CM Response code")
rcode_def ( CM_BAD_COMM_TYPE,                           (CM_START-45),          L"CM Bad communications type")
rcode_def ( CM_ISP_PARAM_LOAD_FAILED,                   (CM_START-46),          L"CM ISP parameter load failed")
rcode_def ( CM_ISP_PARAM_WRITE_FAILED,                  (CM_START-47),          L"CM ISP parameter write failed")
rcode_def ( CM_ISP_PARAM_READ_FAILED,                   (CM_START-48),          L"CM ISP parameter read failed")
rcode_def ( CM_BAD_POINTER,                             (CM_START-49),          L"CM Bad pointer")
rcode_def ( CM_INTERNAL_ERROR,                          (CM_START-50),          L"CM Internal error")
rcode_def ( CM_BAD_NETWORK_HANDLE,                      (CM_START-51),          L"CM Bad network handle")
rcode_def ( CM_BAD_DEVICE_TYPE_HANDLE,                  (CM_START-52),          L"CM Bad device type handle")
rcode_def ( CM_BAD_DD_PATH,                             (CM_START-53),          L"CM Bad DD path")
rcode_def ( CM_BAD_CM_MODE,                             (CM_START-54),          L"CM Bad CM mode")
rcode_def ( CM_BLOCK_IDLE,                              (CM_START-55),          L"CM Block Not Doing Comm")
rcode_def ( CM_DEVICE_IDLE,                             (CM_START-56),          L"CM Device Not Doing Comm")
rcode_def ( CM_NETWORK_IDLE,                            (CM_START-57),          L"CM Network Not Doing Comm")
rcode_def ( CM_BLOCK_BUSY,                              (CM_START-58),          L"CM Block Already Doing Comm")
rcode_def ( CM_DEVICE_BUSY,                             (CM_START-59),          L"CM Device Already Doing Comm")
rcode_def ( CM_UPDATE_PENDING,                          (CM_START-60),          L"CM Asynchronous Update Is Expected")
rcode_def ( CM_DEVICE_CHANGED,                          (CM_START-61),          L"CM Detected A New Device")
rcode_def ( CM_BAD_DEVICE_CLOSE,                        (CM_START-62),          L"CM Bad device close")
rcode_def ( CM_BAD_DEVICE_TYPE_CLOSE,                   (CM_START-63),          L"CM Bad device type close")
rcode_def ( CM_BAD_NETWORK_TYPE,                        (CM_START-64),          L"CM Bad network type")
rcode_def ( CM_BAD_FILE_MAP_CREATE,                     (CM_START-65),			L"CM Creation of File Mapping Failed")
rcode_def ( CM_BAD_VIEW_MAP,                            (CM_START-66),			L"CM Bad Mapping of File View") 
rcode_def ( CM_NOT_ENCRYPTED,                           (CM_START-67),			L"CM File is not encrypted and will not be decrypted") 
rcode_def ( CM_BAD_DDOD_FILE_FORMAT,                    (CM_START-68),			L"CM File format is not correct for a DDOD binary file") 
rcode_def ( CM_ERROR_END,                               (CM_START-99),          L"End of Connection Manager error numbers")



/* ISP Device Simulator Errors */

rcode_def ( ISPDS_SUCCESS,                              (0),                    L"Success")
rcode_def ( ISPDS_MEMORY_ERR,                           (ISPDS_START-0),        L"ISPDS Memory error")
rcode_def ( ISPDS_FILE_OPEN_ERR,                        (ISPDS_START-1),        L"ISPDS File open error")
rcode_def ( ISPDS_FILE_READ_ERR,                        (ISPDS_START-2),        L"ISPDS File read error")
rcode_def ( ISPDS_FILE_CLOSE_ERR,                       (ISPDS_START-3),        L"ISPDS File close error")
rcode_def ( ISPDS_MULT_BLK_ERR,                         (ISPDS_START-4),        L"ISPDS Multiple block error")
rcode_def ( ISPDS_MULT_DEV_ERR,                         (ISPDS_START-5),        L"ISPDS Multiple device error")
rcode_def ( ISPDS_BLK_CLASS_ERR,                        (ISPDS_START-6),        L"ISPDS Block class error")
rcode_def ( ISPDS_PHYS_BLK_ERR,                         (ISPDS_START-7),        L"ISPDS Physical block error")
rcode_def ( ISPDS_OD_CONFLICT_ERR,                      (ISPDS_START-7),        L"ISPDS OD conflict error")
rcode_def ( ISPDS_SYM_FILE_ERR,                         (ISPDS_START-8),        L"ISPDS Symbol file error")
rcode_def ( ISPDS_SYMBOL_ERR,                           (ISPDS_START-9),        L"ISPDS Symbol error")
rcode_def ( ISPDS_DEV_HANDLE_ERR,                       (ISPDS_START-10),       L"ISPDS Device handle error")
rcode_def ( ISPDS_ROD_ERR,                              (ISPDS_START-11),       L"ISPDS ROD error")
rcode_def ( ISPDS_COMM_ERR,                             (ISPDS_START-12),       L"ISPDS Communications error")
rcode_def ( ISPDS_FIND_BLK_ERR,                         (ISPDS_START-13),       L"ISPDS Find block error")
rcode_def ( ISPDS_CFG_FILE_ID_ERR,                      (ISPDS_START-14),       L"ISPDS Configuration file id error")
rcode_def ( ISPDS_PUT_INT_ERR,                          (ISPDS_START-15),       L"ISPDS Put integer error")
rcode_def ( ISPDS_OBJECT_ERR,                           (ISPDS_START-16),       L"ISPDS Object error")
rcode_def ( ISPDS_ERROR_END,                            (ISPDS_START-99),       L"End of Device Simulator error numbers")


/* ISP Device Simulator Errors */

rcode_def ( HRTDS_SUCCESS,                              (0),                    L"Success")
rcode_def ( HRTDS_MEMORY_ERR,                           (HRTDS_START-0),        L"HRTDS Memory error")
rcode_def ( HRTDS_FILE_OPEN_ERR,                        (HRTDS_START-1),        L"HRTDS File open error")
rcode_def ( HRTDS_FILE_READ_ERR,                        (HRTDS_START-2),        L"HRTDS File read error")
rcode_def ( HRTDS_FILE_CLOSE_ERR,                       (HRTDS_START-3),        L"HRTDS File close error")
rcode_def ( HRTDS_UNKNOWN_VAR_TYPE,                     (HRTDS_START-4),        L"HRTDS Unknown variable type")
rcode_def ( HRTDS_INVALID_CFG_FILE_ID,                  (HRTDS_START-5),        L"HRTDS Invalid cfg_file_id")
rcode_def ( HRTDS_VAR_NOT_FOUND,                        (HRTDS_START-6),        L"HRTDS Variable not found")
rcode_def ( HRTDS_DEVICE_NOT_FOUND,                     (HRTDS_START-7),        L"HRTDS Device not found")
rcode_def ( HRTDS_NETWORK_NOT_FOUND,                    (HRTDS_START-8),        L"HRTDS Network not found")
rcode_def ( HRTDS_TOO_MANY_NETWORKS,                    (HRTDS_START-9),        L"HRTDS Too many simulated networks")
rcode_def ( HRTDS_TOO_MANY_DEVICES,                     (HRTDS_START-10),        L"HRTDS Too many simulated devices")
rcode_def ( HRTDS_DEVICE_DISCONNECTED,                  (HRTDS_START-11),       L"HRTDS Device is disconnected")
rcode_def ( HRTDS_ERROR_END,                            (HRTDS_START-99),       L"End of HART Device Simulator error numbers")

/* RCSIM return types. */

rcode_def ( RCSIM_SUCCESS,                              (0),                    L"Success")
rcode_def ( RCSIM_MATCH,                                (RCSIM_START-0),        L"RCSIM Match found")
rcode_def ( RCSIM_NOMATCH,                              (RCSIM_START-5),        L"RCSIM No match found")
rcode_def ( RCSIM_END_OF_LIST,                          (RCSIM_START-6),        L"RCSIM End of response code list")
rcode_def ( RCSIM_NO_ITEMS,                             (RCSIM_START-7),        L"RCSIM No response codes in list")
rcode_def ( RCSIM_ERR_CLASS_FIND,                       (RCSIM_START-10),       L"RCSIM Response message type not found")
rcode_def ( RCSIM_ERR_NETWORK_FIND,                     (RCSIM_START-11),       L"RCSIM Network not found in network file")
rcode_def ( RCSIM_ERR_STATION_FIND,                     (RCSIM_START-12),       L"RCSIM Station not found in sim file")
rcode_def ( RCSIM_ERR_MEMORY,                           (RCSIM_START-20),       L"RCSIM Memory error")
rcode_def ( RCSIM_ERR_NET_ID,                           (RCSIM_START-21),       L"RCSIM Invalid network handle")
rcode_def ( RCSIM_ERR_CLASS,                            (RCSIM_START-22),       L"RCSIM Invalid rcsim message type")
rcode_def ( RCSIM_ERR_VALUE_TYPE,                       (RCSIM_START-23),       L"RCSIM Invalid response code value type")
rcode_def ( RCSIM_ERR_NET_FILE_OPEN,                    (RCSIM_START-24),       L"RCSIM Network data file open error")
rcode_def ( RCSIM_ERR_NET_FILE_READ,                    (RCSIM_START-25),       L"RCSIM Network data file read error")
rcode_def ( RCSIM_ERR_NET_FILE_CLOSE,                   (RCSIM_START-26),       L"RCSIM Network data file close error")
rcode_def ( RCSIM_ERR_SIM_FILE_OPEN,                    (RCSIM_START-27),       L"RCSIM Sim data file open error")
rcode_def ( RCSIM_ERR_SIM_FILE_READ,                    (RCSIM_START-28),       L"RCSIM Sim data file read error")
rcode_def ( RCSIM_ERR_SIM_FILE_CLOSE,                   (RCSIM_START-29),       L"RCSIM Sim data file close error")
rcode_def ( RCSIM_ERROR_END,                            (RCSIM_START-99),       L"End of RCSIM Simulator error numbers")



/* Comm Manager Errors */

rcode_def ( COMM_MGR_LIST_EMPTY,                        (COMM_MGR_START-0),     L"Comm mgr list is empty")
rcode_def ( COMM_MGR_REQ_NOT_IN_LIST,                   (COMM_MGR_START-1),     L"Comm mgr req is not in the req list")
rcode_def ( COMM_MGR_ERROR_END,                         (COMM_MGR_START-99),    L"End of Comm Mgr error numbers")


/* DB Comm Manager Errors */

rcode_def ( DB_COMM_SUCCESS, (0), L"Success")
rcode_def ( DB_COMM_INVALID_INTEGER_DATA_SIZE,          (DB_COMM_START-1),      L"DB Comm Mgr invalid data size for INTEGER type.")
rcode_def ( DB_COMM_INVALID_VAR_TYPE,                   (DB_COMM_START-2),      L"DB Comm Mgr invalid DDL data type.")
rcode_def ( DB_COMM_INVALID_REQ_TYPE,                   (DB_COMM_START-3),      L"DB Comm Mgr invalid Comm Request.")
rcode_def ( DB_COMM_NOT_INITIALIZED,                    (DB_COMM_START-4),      L"DB Comm Mgr package not initialized." )
rcode_def ( DB_COMM_INVALID_PARAMETER,                  (DB_COMM_START-5),      L"DB Comm Mgr function parameter invalid or NULL.")
rcode_def ( DB_COMM_TID_NOT_FOUND,                      (DB_COMM_START-6),      L"DB Comm Mgr PC transaction id not found.")
rcode_def ( DB_COMM_REQUEST_NOT_FOUND,                  (DB_COMM_START-7),      L"DB Comm Mgr request not found in request list.")
rcode_def ( DB_COMM_INVALID_PARAM_KIND,                 (DB_COMM_START-8),      L"DB Comm Mgr invalid parameter kind found.")
rcode_def ( DB_COMM_MEMORY,                             (DB_COMM_START-9),      L"DB Comm Mgr memory error.")
rcode_def ( DB_COMM_SCANLIST_NOINIT,                    (DB_COMM_START-10),     L"DB Comm Mgr scan list not initialized.")
rcode_def ( DB_COMM_MAX_RETRIES_EXCEEDED,               (DB_COMM_START-11),     L"DB Comm Mgr maximum retries for operation exceeded.")
rcode_def ( DB_COMM_ERROR_END,                          (DB_COMM_START-99),     L"End of DB Comm Mgr error numbers")


/* DB Wrap Manager Errors */
rcode_def (DBW_SUCCESS,                                 (0),                    L"Success")
rcode_def (DBW_ERROR,                                   (DBWRAP_START-1),       L"DBWRAP Error")
rcode_def (DBW_BOL,                                     (DBWRAP_START-2),       L"DBWRAP Beginning of list")
rcode_def (DBW_EOL,                                     (DBWRAP_START-3),       L"DBWRAP End of list")
rcode_def (DBW_NOTFOUND,                                (DBWRAP_START-4),       L"DBWRAP Not found")
rcode_def (DBW_SUCCESS_ADDED,                           (DBWRAP_START-5),       L"DBWRAP used with getadd_ combo functions")
rcode_def (DBW_DB_BUSY,                                 (DBWRAP_START-6),       L"DBWRAP database is busy")
rcode_def (DBW_DB_LOCK,                                 (DBWRAP_START-7),       L"DBWRAP database is locked")
rcode_def (DBW_ERR_NOTINIT,                             (DBWRAP_START-8),       L"DBWRAP Not initialized")
rcode_def (DBW_ERR_ENV,                                 (DBWRAP_START-9),       L"DBWRAP ODBC environment allocation failure")
rcode_def (DBW_ERR_CONNECTION,                          (DBWRAP_START-10),      L"DBWRAP ODBC connection allocation failure")
rcode_def (DBW_ERR_STATEMENT,                           (DBWRAP_START-11),      L"DBWRAP ODBC statement allocation failure")
rcode_def (DBW_ERR_NOMEMORY,                            (DBWRAP_START-12),      L"DBWRAP Memory allocation failure")
rcode_def (DBW_ERR_TRANSACTID,                          (DBWRAP_START-13),      L"DBWRAP Invalid transaction id")
rcode_def (DBW_ERR_BLOCKINFO,                           (DBWRAP_START-14),      L"DBWRAP Incomplete block information")
rcode_def (DBW_ERR_NEWEVENTTIME,                        (DBWRAP_START-15),      L"DBWRAP Event time generation failure")
rcode_def (DBW_ERR_ADDTAGBLOCKASGMS,                    (DBWRAP_START-16),      L"DBWRAP Failed to add TagBlockAsgms record")
rcode_def (DBW_ERR_ADDEVENTLOG,                         (DBWRAP_START-17),      L"DBWRAP Failed to add EventLog record")
rcode_def (DBW_ERR_NEWEVENTID,                          (DBWRAP_START-18),      L"DBWRAP unable to assign new event identifier")
rcode_def (DBW_ERR_STARTTRANSACTION,                    (DBWRAP_START-19),      L"DBWRAP unable to start transaction")
rcode_def (DBW_ERR_COMMITTRANSACTION,                   (DBWRAP_START-20),      L"DBWRAP unable to commit transaction")
rcode_def (DBW_ERR_CANCELTRANSACTION,                   (DBWRAP_START-21),      L"DBWRAP unable to cancel transaction")
rcode_def (DBW_ERR_TRANSACTIDLIMIT,                     (DBWRAP_START-22),      L"DBWRAP unable to assign more transaction ids")
rcode_def (DBW_ERR_GETTYPE,                             (DBWRAP_START-23),      L"DBWRAP invalid or improper use of DBW_GT_xxxx")
rcode_def (DBW_ERR_DATASETID,                           (DBWRAP_START-24),      L"DBWRAP invalid dataset id")
rcode_def (DBW_ERR_PUTBLOCKPARAM,                       (DBWRAP_START-25),      L"DBWRAP unable to write block parameter")
rcode_def (DBW_ERR_DATETIME,                            (DBWRAP_START-26),      L"DBWRAP invalid date / time parameter")
rcode_def (DBW_ERR_NODEVICEREC,                         (DBWRAP_START-27),      L"DBWRAP device not in database")
rcode_def (DBW_ERR_MAXDEVICEKEY,                        (DBWRAP_START-28),      L"DBWRAP no more device keys available")
rcode_def (DBW_ERR_NODEVICETAG,                         (DBWRAP_START-29),      L"DBWRAP no device tag found")
rcode_def (DBW_ERR_NOPHYSICALTAG,                       (DBWRAP_START-30),       L"DBWRAP no physical tag found")
rcode_def (DBW_ERR_NOBLOCKREC,                          (DBWRAP_START-31),       L"DBWRAP block not in database")
rcode_def (DBW_ERR_MAXBLOCKKEY,                         (DBWRAP_START-32),      L"DBWRAP no more block keys available")
rcode_def (DBW_ERR_NOEXTBLOCKTAGREC,                    (DBWRAP_START-33),      L"DBWRAP external block tag not in database")
rcode_def (DBW_ERR_MAXEXTBLOCKTAGKEY,                   (DBWRAP_START-34),      L"DBWRAP no more extBlockTagKeys available")
rcode_def (DBW_ERR_DUPLICATEBLOCK,                      (DBWRAP_START-35),      L"DBWRAP duplicate block in database")
rcode_def (DBW_ERR_ADDBLOCK,                            (DBWRAP_START-36),      L"DBWRAP unable to add block to database")
rcode_def (DBW_ERR_ADDDEVICE,                           (DBWRAP_START-37),      L"DBWRAP unable to add device to database")
rcode_def (DBW_ERR_PARAMKIND,                           (DBWRAP_START-38),      L"DBWRAP invalid parameter kind")
rcode_def (DBW_ERR_ASSIGNPHYSICALTAG,                   (DBWRAP_START-39),      L"DBWRAP unable to assign device hart tag")
rcode_def (DBW_ERR_NONAMEDCONFIG,                       (DBWRAP_START-40),      L"DBWRAP no named config data")
rcode_def (DBW_ERR_CONFIGTYPE,                          (DBWRAP_START-41),      L"DBWRAP invalid configtype")
rcode_def (DBW_ERR_NO_PDEVICE,                          (DBWRAP_START-42),      L"DBWRAP no Pdevice record")
rcode_def (DBW_ERR_NO_PDEV_BLOCKREC,                    (DBWRAP_START-43),      L"DBWRAP no PDevice Block (DevBlock table) record")
rcode_def (DBW_ERR_UDEVREVID,                           (DBWRAP_START-44),      L"DBWRAP invalid unique PDevice identifier")
rcode_def (DBW_ERR_ADDMANUFACTURER,                     (DBWRAP_START-45),      L"DBWRAP unable to add manufactuer")
rcode_def (DBW_ERR_ADDDEVICETYPE,                       (DBWRAP_START-46),      L"DBWRAP unable to add device type")
rcode_def (DBW_ERR_NOMFRID,                             (DBWRAP_START-47),      L"DBWRAP no manufacturer id record")
rcode_def (DBW_ERR_NODEVICETYPE,                        (DBWRAP_START-48),      L"DBWRAP no deviceType record")
rcode_def (DBW_ERR_NOEXTBLOCKTAG_ASGM,                  (DBWRAP_START-49),      L"DBWRAP no block assigned to external block tag")
rcode_def (DBW_ERR_SCANTYPE,                            (DBWRAP_START-50),      L"DBWRAP invalid scan type")
rcode_def (DBW_ERR_VALUEMODE,                           (DBWRAP_START-51),      L"DBWRAP invalid value mode")
rcode_def (DBW_ERR_ADDSCANLIST,                         (DBWRAP_START-52),      L"DBWRAP unable to add to scan list item")
rcode_def (DBW_ERR_UPDATESCANLIST,                      (DBWRAP_START-53),      L"DBWRAP unable to update scan list item")
rcode_def (DBW_ERR_DELETESCANLIST,                      (DBWRAP_START-54),      L"DBWRAP unable to delete scan list item")
rcode_def (DBW_ERR_UPDATE_IPL,                          (DBWRAP_START-55),      L"DBWRAP unable to update block InitialParamLoad")
rcode_def (DBW_ERR_UPDATE_LPM,                          (DBWRAP_START-56),      L"DBWRAP unable to update block LastParamModify")
rcode_def (DBW_ERR_UPDATE_ISD_STATUS,                   (DBWRAP_START-57),      L"DBWRAP unable to update block ISD_Status")
rcode_def (DBW_ERR_ADDUSER,                             (DBWRAP_START-58),      L"DBWRAP unable to add user")
rcode_def (DBW_ERR_UPDATEUSER,                          (DBWRAP_START-59),      L"DBWRAP unable to update user")
rcode_def (DBW_ERR_DUPLICATE_USERNAME,                  (DBWRAP_START-60),      L"DBWRAP duplicate user name")
rcode_def (DBW_ERR_NOUSERKEY,                           (DBWRAP_START-61),      L"DBWRAP UserKey not found")
rcode_def (DBW_ERR_NOUSERNAME,                          (DBWRAP_START-62),      L"DBWRAP UserName not found")
rcode_def (DBW_ERR_MAXUSERKEY,                          (DBWRAP_START-63),      L"DBWRAP UserKey maximum exceeded (cbUSERKEYMAX)")
rcode_def (DBW_ERR_MAXHDBW,                             (DBWRAP_START-64),      L"DBWRAP HDBW channel maximum exceeded (cbHDBW_MAX)")
rcode_def (DBW_ERR_MAXRETRIESEXCEEDED,                  (DBWRAP_START-65),      L"DBWRAP Maximum retries for operation exceeded")
rcode_def (DBW_ERR_DSN_NOT_FOUND,                       (DBWRAP_START-66),      L"DBWRAP ODBC data-source-name not found")
rcode_def (DBW_ERR_DB_FILE_NOT_FOUND,                   (DBWRAP_START-67),      L"DBWRAP ODBC data-source file-name not found")
rcode_def (DBW_ERR_UNAUTHORIZED_ACCESS,                 (DBWRAP_START-68),      L"DBWRAP Unauthorized database access attempted")
rcode_def (DBW_ERR_DUPLICATE_KEY,                       (DBWRAP_START-69),      L"DBWRAP Duplicate key or index")
rcode_def (DBW_ERR_ADD_EXTBLOCKTAG,                     (DBWRAP_START-70),      L"DBWRAP Unable to add external block tag")
rcode_def (DBW_ERR_DELETE_OFFLINE_PARAMS,               (DBWRAP_START-71),      L"DBWRAP Unable to delete offline params")
rcode_def (DBW_ERR_INVALID_DATABASE_KEY,                (DBWRAP_START-72),      L"DBWRAP Invalid database key")
rcode_def (DBW_ERR_ADDDEVICEPROTOCOL,                   (DBWRAP_START-73),      L"DBWRAP Unable to add device protocol")
rcode_def (DBW_ERR_ADDMFRPROTOCOL,                      (DBWRAP_START-74),      L"DBWRAP Unable to add manufacturer-protocol")
rcode_def (DBW_ERR_ADDDEVICEREVISION,                   (DBWRAP_START-75),      L"DBWRAP Unable to add device revision")
rcode_def (DBW_ERR_DUPLICATE_EXTBLOCKTAG,               (DBWRAP_START-76),      L"DBWRAP Attempted duplicate external block tag")
rcode_def (DBW_ERR_ADD_CALSTATUS,                       (DBWRAP_START-77),      L"DBWRAP Unable to add calibration status record")
rcode_def (DBW_ERR_ADD_NAMEDCONFIG,                     (DBWRAP_START-78),      L"DBWRAP Unable to add named config")
rcode_def (DBW_ERR_LIC_TAG_COUNT_REACHED,               (DBWRAP_START-79),      L"DBWRAP License Tag Count has been reached")
rcode_def (DBW_ERR_NO_CONDEV_DD_INFO,					(DBWRAP_START-80),      L"DBWRAP No conventional device dd information found")
rcode_def (DBW_ERR_END,                                 (DBWRAP_START-99),      L"End of DBWRAP function return codes")


/* Remote block server errors */
rcode_def (RBS_SUCCESS,                                  (0),                   L"Success")
rcode_def (RBS_BLOCK_OPEN_FAILURE,                       (RBS_START-1),         L"File server failed to open block")


/* Make  *.ini file errors */
rcode_def (MKINI_SUCCESS,                                (0),                   L"Success")
rcode_def (MKINI_NO_VALUE_IN_DDL,                        (MKINI_START-1),       L"Mkini.exe cannot find a default value in the DDL ")
rcode_def (MKINI_FAILURE,                                (MKINI_START-2),       L"Mkini.exe general Failure")
rcode_def (MKINI_ERR_END,                                (MKINI_START-99),      L"End of Errors for Mkini.exe")

/* Generic errors (ie. not subsystem specific errors) */
/* There errors are meant to be returned from any subsystem. */
rcode_def ( INIT_FAILURE,								 (GEN_START-1),			L"Initialization Failed")
rcode_def ( INVALID_DATA_TYPE,							 (GEN_START-2),			L"Invalid Data Type")
rcode_def ( INVALID_DATA_SIZE,							 (GEN_START-3),			L"Invalid Data Size")
rcode_def ( MEMORY_ALLOCATION_FAILED,					 (GEN_START-4),			L"Memory Allocation Failed")
rcode_def ( INVALID_DEVICE_TAG,							 (GEN_START-5),			L"Invalid Device Tag/ID")
rcode_def ( GENERIC_READ_PARAM_ERROR,					 (GEN_START-6),			L"Cannot read param")
rcode_def ( GENERIC_WRITE_PARAM_ERROR,					 (GEN_START-7),			L"Cannot write param")
rcode_def ( INVALID_VIEW_TYPE,							 (GEN_START-8),			L"Invalid view type")
rcode_def ( GEN_ERR_END,								 (GEN_START-99),		L"End of Generic Errors")

/* Data base interface errors */
rcode_def ( DB_BEGIN_TRANSACTION_FAILED,			 (DB_START-1),	L"Invalid View Type")
rcode_def ( DB_END_TRANSACTION_FAILED,				 (DB_START-2),	L"Invalid View Type")
rcode_def ( DB_CANCEL_TRANSACTION_FAILED,			 (DB_START-3),	L"Invalid View Type")
rcode_def ( DB_UNABLE_TO_CONNECT_TO_DATABASE,		 (DB_START-4),	L"Unable to connect to database")
rcode_def ( DB_DEVICE_NOT_FOUND,					 (DB_START-5),	L"Device not found in database")
rcode_def ( DB_READ_PARAM_FAILED,					 (DB_START-6),	L"Unable to read param from database")
rcode_def ( DB_WRITE_PARAM_FAILED,					 (DB_START-7),	L"Unable to write param to database")
rcode_def ( DB_INVALID_PTM_PATH,					 (DB_START-8),	L"Ptm Path from DB has invalid format")
rcode_def ( DB_UNABLE_TO_GET_PATH_AND_TAG,			 (DB_START-9),	L"Unable to get path and tag from database")
rcode_def ( DB_UNABLE_TO_GET_DD_DEVICE_ID,			 (DB_START-10),	L"Unable to get DB DeviceId")
rcode_def ( DB_DEVICE_IDENTIFY_FAILED,				 (DB_START-11),	L"DB device Identify failed")
rcode_def ( DB_UPDATE_LOGICAL_BLOCK_FAILED,			 (DB_START-12),	L"Update logical block failed.")
rcode_def ( DB_UNABLE_TO_GET_MFR_AND_DEVTYPE,		 (DB_START-13),	L"Unable to get Manufacturer Name and Device Type Name from database")
rcode_def ( DB_UNABLE_TO_GET_ALARM_BLOCK_INDEX,		 (DB_START-14),	L"Unable to get Alarm Block Index from database")
rcode_def ( DB_UNABLE_TO_GET_DEVICE_ID,				 (DB_START-15),	L"Unable to get Device Id from database")
rcode_def ( DB_UNABLE_TO_GET_DEVICE_COMMISSIONED,	 (DB_START-16),	L"Unable to get Device Commissioned State from database")
rcode_def ( DB_END,									 (DB_START-99),	L"End of DbIntrFc Errors")

/* HART_PT errors */
rcode_def (HART_PT_SUCCESS,                              (0),                   L"Success")
rcode_def (HART_PT_NO_ICOMM_PTR,                         (HART_PT_START-1),     L"No IComm pointer found in connection manager for device")
rcode_def (HART_PT_NO_HART_PT_COMM_PTR,                  (HART_PT_START-2),     L"No HARt_PT_COMM pointer found in commstack for a network")
rcode_def (HART_PT_Is_Simulated_FAILED,                  (HART_PT_START-3),     L"The call to IsSimulated failed for HART pass-through")
rcode_def (HART_PT_StartRequest_FAILED,                  (HART_PT_START-4),     L"The call to StartRequest failed for HART pass-through")
rcode_def (HART_PT_GetResponse_FAILED,                   (HART_PT_START-5),     L"The call to GetResponse failed for HART pass-through")
rcode_def (HART_PT_ERR_END,                              (HART_PT_START-99),    L"End of Errors for HART_PT")

/* PATH_PT_LEVEL errors */
rcode_def (PATH_PT_LEVEL_SUCCESS,                        (0),                   L"Success")
rcode_def (PATH_PT_LEVEL_INIT_FAILURE,                   (PATH_PT_LEVEL_START-1),     L"Initialization of HART PT Hierarchy COM object failed")
rcode_def (PATH_PT_LEVEL_GET_CHILD_KIND_FAILURE,         (PATH_PT_LEVEL_START-2),     L"The call to GetChildKind in build_path failed")
rcode_def (PATH_PT_LEVEL_POSITION_TO_CHILD_FAILURE,		 (PATH_PT_LEVEL_START-3),     L"The call to PositionToChild in build_path failed")
rcode_def (PATH_PT_LEVEL_PROP_ACCESS_VIOLATION,			 (PATH_PT_LEVEL_START-4),     L"The property has been protected against this operation")
rcode_def (PATH_PT_LEVEL_PROP_STORE_FAILURE,			 (PATH_PT_LEVEL_START-5),     L"Failure while writing property to COM object")
rcode_def (PATH_PT_LEVEL_NO_PROP_FOUND,					 (PATH_PT_LEVEL_START-6),     L"The property specified was not found")
rcode_def (PATH_PT_LEVEL_METHOD_NOT_FOUND,				 (PATH_PT_LEVEL_START-7),     L"The method was not a valid method")
rcode_def (PATH_PT_LEVEL_METHOD_NOT_IMPL,				 (PATH_PT_LEVEL_START-8),     L"The method is not implemented")
rcode_def (PATH_PT_LEVEL_PT_NET_FAILURE,				 (PATH_PT_LEVEL_START-9),     L"The generic HART PT COM object failed")
rcode_def (PATH_PT_LEVEL_PTNET_NOT_REGISTERED,			 (PATH_PT_LEVEL_START-10),    L"The PTNET COM object is not registered")
rcode_def (PATH_PT_LEVEL_IINITIALIZE_IF_NOT_FOUND,		 (PATH_PT_LEVEL_START-11),    L"Could not get the IInitialize i-f from the PTNET COM object")
rcode_def (PATH_PT_LEVEL_OUT_OF_MEMORY,					 (PATH_PT_LEVEL_START-12),    L"Out of memory in PT Level object")
rcode_def (PATH_PT_ERROR_CODE_OUT_OF_RANGE,				 (PATH_PT_LEVEL_START-13),    L"Error code passed back for PT COM object out of range")
rcode_def (PATH_PT_LEVEL_METHOD_PARAMETER_ERROR,		 (PATH_PT_LEVEL_START-14),    L"Parameter error in method call")
rcode_def (PATH_PT_LEVEL_METHOD_EXECUTION_ERROR,		 (PATH_PT_LEVEL_START-15),    L"Error in executing method")

rcode_def (PATH_PT_LEVEL_ERR_END,                        (PATH_PT_LEVEL_START-99),    L"End of Errors for PATH_PT_LEVEL")

/*	HART pass-through communication error codes....  We are reserving 200 error codes for this */

rcode_def (PASS_THROUGH_COMMUNICATIONS_SUCCESS,          (0),                                     L"Success")
rcode_def (PASS_THROUGH_COMMUNICATIONS_ERROR_RANGE_START,(PASS_THROUGH_COMMUNICATIONS_START),     L"Start of error range for PASS_THROUGH_COMMUNICATIONS")
rcode_def (PASS_THROUGH_COMMUNICATIONS_END,              (PASS_THROUGH_COMMUNICATIONS_START-199), L"End of Errors for PASS_THROUGH_COMMUNICATIONS")

/*	HART pass-through hierarchy error codes....  We are reserving 200 error codes for this */

rcode_def (PASS_THROUGH_HIERARCHY_SUCCESS,          (0),                                L"Success")
rcode_def (PASS_THROUGH_HIERARCHY_ERROR_RANGE_START,(PASS_THROUGH_HIERARCHY_START),     L"Start of error range for PASS_THROUGH_HIERARCHY")
rcode_def (PASS_THROUGH_HIERARCHY_END,              (PASS_THROUGH_HIERARCHY_START-199), L"End of Errors for PASS_THROUGH_HIERARCHY")

/*	HART pass-through network object PTNET error codes */
rcode_def (PTNET_SUCCESS,							(0),                L"Success")
rcode_def (PTNET_ERR_START,							(PTNET_START),	    L"Start of error code range")
rcode_def (PTNET_ALREADY_INITIALIZED,				(PTNET_START-1),	L"The pass-through network is already initialized")
rcode_def (PTNET_PROGID_HIERARCHY_COM,				(PTNET_START-2),	L"There was a problem initializing the network specific hierarchy COM object")
rcode_def (PTNET_PROGID_COMM_COM,					(PTNET_START-3),	L"There was a problem initializing the network specific communications COM object")
rcode_def (PTNET_READING_INI_FILE,					(PTNET_START-4),	L"There was trouble getting parameters out of the FMS.INI file")
rcode_def (PTNET_LOADING_HIER_FROM_COM,				(PTNET_START-5),	L"There was trouble loading the hierarchy from the network specific hiearchy COM object")
rcode_def (PTNET_COMMUNICATING_WITH_HARTCOMM,		(PTNET_START-6),	L"There was trouble in a call to the HARTComm COM object")
rcode_def (PTNET_CHILD_NOT_FOUND,					(PTNET_START-7),	L"The pass-through child object was not found")
rcode_def (PTNET_PROPERTY_NOT_FOUND,				(PTNET_START-8),	L"The pass-through property was not found")
rcode_def (PTNET_PROPERTY_ACCESS_VIOLATION,			(PTNET_START-9),	L"The pass-through operation was not completed because of an access violation")
rcode_def (PTNET_TOP_OF_TREE,						(PTNET_START-10),	L"Trying to position above top of tree")
rcode_def (PTNET_NO_HIER_BUILDER_IF,				(PTNET_START-11),	L"The object does not have the IHierarchyBuilder interface")
rcode_def (PTNET_ALREADY_EXISTS,					(PTNET_START-12),	L"Trying to insert a child/property with the same name as an existing child")
rcode_def (PTNET_OUT_OF_MEMORY,						(PTNET_START-13),	L"Operation failed because of no memory available")
rcode_def (PTNET_BAD_CHILD_OBJECT,					(PTNET_START-14),	L"The object is corrupted")
rcode_def (PTNET_NO_ADDRESS_FOUND,					(PTNET_START-15),	L"Could not find the network specific address property")
rcode_def (PTNET_ROOT_MISSING_PROPERTIES,			(PTNET_START-16),	L"The root object of the PTNET tree does not have at least one required property")
rcode_def (PTNET_LOAD_FUNCTION_NOT_CALLED,			(PTNET_START-17),	L"The Load function of the IInitialize interface was not called before calling the Initialize function")
rcode_def (PTNET_SERIALIZATION_FAILURE,				(PTNET_START-18),	L"The serialization failed")
rcode_def (PTNET_NOINTERFACE,						(PTNET_START-19),	L"A required interface is was not found for pass-through")
rcode_def (PTNET_ERROR_CODE_OUT_OF_RANGE,			(PTNET_START-20),	L"A error code recieved from a pass-through COM object was out of the range 0 to -199")
rcode_def (PTNET_PROGID_DISPATCH_COM,				(PTNET_START-21),	L"There was a problem initializing the network specific Dispatch COM object")
rcode_def (PTNET_ERR_END,							(PTNET_START-99),   L"End of Errors for PATH_PT_LEVEL")

/*	HART pass-through dispatch error codes....  We are reserving 200 error codes for this */

rcode_def (PASS_THROUGH_DISPATCH_SUCCESS,			(0),								L"Success")
rcode_def (PASS_THROUGH_DISPATCH_ERROR_RANGE_START,	(PASS_THROUGH_DISPATCH_START),		L"Start of error range for PASS_THROUGH_DISPATCH")
rcode_def (PASS_THROUGH_DISPATCH_ERROR_RANGE_END,	(PASS_THROUGH_DISPATCH_START-199),	L"End of error range for PASS_THROUGH_DISPATCH")

/*	AMS server DISPATCH type errors.  These are errors that occur in the server. */
rcode_def (DISPATCH_BAD_RETURN_VARIANT,			(AMS_SERVER_DISPATCH_START),	L"The variant returned in the IDispatch::Invoke call must be integer. It was not.")
rcode_def (DISPATCH_BAD_HRESULT_RETURNED,		(AMS_SERVER_DISPATCH_START-1),	L"The HRESULT returned from IDispatch::GetIDsOfNames or IDispatch::Invoke was not S_OK")
rcode_def (DISPATCH_ERROR_CODE_OUT_OF_RANGE,	(AMS_SERVER_DISPATCH_START-2),	L"The Invoke error code returned in ReturnVariant was outside of the range of 0 to 199")

/*  Device Location Store Service error codes. */
rcode_def (DLSS_BAD_PACKET,					(DEVICE_LOCATION_STORE_START),		L"The plant server packet is bad")
rcode_def (DLSS_NO_REQUEST_OBJECT,			(DEVICE_LOCATION_STORE_START-1),	L"The Device Location Store Service could not create a Device Location Store Request object")
rcode_def (DLSS_NO_REQUEST_INTERFACE,		(DEVICE_LOCATION_STORE_START-2),	L"The Device Location Store Request object could not return the IDevLocRequest interface")
rcode_def (DLSS_NO_INTERFACE,				(DEVICE_LOCATION_STORE_START-3),	L"The Device Location Store Service object could not return the IDevLocRequestCallback interface")
rcode_def (DLSS_REQUEST_INITIATE_FAILURE,	(DEVICE_LOCATION_STORE_START-4),	L"The Device Location Store Request Initiate call failed")
rcode_def (DLSS_REQUEST_UPDATE_FAILURE,		(DEVICE_LOCATION_STORE_START-5),	L"The Device Location Store Request update failed")
rcode_def (DLSS_REQUEST_NO_INTERFACE,		(DEVICE_LOCATION_STORE_START-6),	L"The Device Location Store could not get a required interface")
rcode_def (DLSS_DEVLOCTABLE_INIT_FAILURE,	(DEVICE_LOCATION_STORE_START-7),	L"The Device Location Store could not initialize the DeviceLocation table object")
rcode_def (DLSS_NO_DEVICE_KEY,				(DEVICE_LOCATION_STORE_START-8),	L"The Device Location Store could not get the Device key given the Block key")
rcode_def (DLSS_NO_BLOCK_ACCESS,			(DEVICE_LOCATION_STORE_START-9),	L"The Device Location Store could not access the Block table")
rcode_def (DLSS_NO_BLOCK_IDENTIFY,			(DEVICE_LOCATION_STORE_START-10),	L"The Device Location Store could not identify the Block table")
rcode_def (DLSS_INVALID_FUNCTION_ARGUMENT,	(DEVICE_LOCATION_STORE_START-11),	L"A function was called with an invalid argument")

/* Make  Unknown errors */
rcode_def (UNKNOWN_SUCCESS,                              (0),                   L"Success")
rcode_def (UNKNOWN_1,                                    (UNKNOWN_START-1),     L"AMS General error 1")
rcode_def (UNKNOWN_2,                                    (UNKNOWN_START-2),     L"AMS General error 2")
rcode_def (UNKNOWN_3,                                    (UNKNOWN_START-3),     L"AMS General error 3")
rcode_def (UNKNOWN_4,                                    (UNKNOWN_START-4),     L"AMS General error 4")
rcode_def (UNKNOWN_5,                                    (UNKNOWN_START-5),     L"AMS General error 5")
rcode_def (UNKNOWN_6,                                    (UNKNOWN_START-6),     L"AMS General error 6")
rcode_def (UNKNOWN_7,                                    (UNKNOWN_START-7),     L"AMS General error 7")
rcode_def (UNKNOWN_8,                                    (UNKNOWN_START-8),     L"AMS General error 8")
rcode_def (UNKNOWN_9,                                    (UNKNOWN_START-9),     L"AMS General error 9")
rcode_def (UNKNOWN_10,                                   (UNKNOWN_START-10),    L"AMS General error 10")
rcode_def (UNKNOWN_11,                                   (UNKNOWN_START-11),    L"AMS General error 11")
rcode_def (UNKNOWN_12,                                   (UNKNOWN_START-12),    L"AMS General error 12")
rcode_def (UNKNOWN_13,                                   (UNKNOWN_START-13),    L"AMS General error 13")
rcode_def (UNKNOWN_14,                                   (UNKNOWN_START-14),    L"AMS General error 14")
rcode_def (UNKNOWN_15,                                   (UNKNOWN_START-15),    L"AMS General error 15")
rcode_def (UNKNOWN_16,                                   (UNKNOWN_START-16),    L"AMS General error 16")
rcode_def (UNKNOWN_17,                                   (UNKNOWN_START-17),    L"AMS General error 17")
rcode_def (UNKNOWN_18,                                   (UNKNOWN_START-18),    L"AMS General error 18")
rcode_def (UNKNOWN_19,                                   (UNKNOWN_START-19),    L"AMS General error 19")
rcode_def (UNKNOWN_20,                                   (UNKNOWN_START-20),    L"AMS General error 20")
rcode_def (UNKNOWN_21,                                   (UNKNOWN_START-21),    L"AMS General error 21")
rcode_def (UNKNOWN_22,                                   (UNKNOWN_START-22),    L"AMS General error 22")
rcode_def (UNKNOWN_23,                                   (UNKNOWN_START-23),    L"AMS General error 23")
rcode_def (UNKNOWN_24,                                   (UNKNOWN_START-24),    L"AMS General error 24")
rcode_def (UNKNOWN_25,                                   (UNKNOWN_START-25),    L"AMS General error 25")
rcode_def (UNKNOWN_26,                                   (UNKNOWN_START-26),    L"AMS General error 26")
rcode_def (UNKNOWN_27,                                   (UNKNOWN_START-27),    L"AMS General error 27")
rcode_def (UNKNOWN_28,                                   (UNKNOWN_START-28),    L"AMS General error 28")
rcode_def (UNKNOWN_29,                                   (UNKNOWN_START-29),    L"AMS General error 29")
rcode_def (UNKNOWN_30,                                   (UNKNOWN_START-30),    L"AMS General error 30")
rcode_def (UNKNOWN_31,                                   (UNKNOWN_START-31),    L"AMS General error 31")
rcode_def (UNKNOWN_32,                                   (UNKNOWN_START-32),    L"AMS General error 32")
rcode_def (UNKNOWN_33,                                   (UNKNOWN_START-33),    L"AMS General error 33")
rcode_def (UNKNOWN_34,                                   (UNKNOWN_START-34),    L"AMS General error 34")
rcode_def (UNKNOWN_35,                                   (UNKNOWN_START-35),    L"AMS General error 35")
rcode_def (UNKNOWN_36,                                   (UNKNOWN_START-36),    L"AMS General error 36")
rcode_def (UNKNOWN_37,                                   (UNKNOWN_START-37),    L"AMS General error 37")
rcode_def (UNKNOWN_38,                                   (UNKNOWN_START-38),    L"AMS General error 38")
rcode_def (UNKNOWN_39,                                   (UNKNOWN_START-39),    L"AMS General error 39")
rcode_def (UNKNOWN_40,                                   (UNKNOWN_START-40),    L"AMS General error 40")
rcode_def (UNKNOWN_41,                                   (UNKNOWN_START-41),    L"AMS General error 41")
rcode_def (UNKNOWN_42,                                   (UNKNOWN_START-42),    L"AMS General error 42")
rcode_def (UNKNOWN_43,                                   (UNKNOWN_START-43),    L"AMS General error 43")
rcode_def (UNKNOWN_44,                                   (UNKNOWN_START-44),    L"AMS General error 44")
rcode_def (UNKNOWN_ERR_END,                              (UNKNOWN_START-99),    L"End Unknown Errors")

/* List Cache Errors */                    
rcode_def ( LC_SUCCESS,                                 (0),                    L"LC Success")
rcode_def ( LC_ERROR_START,                             (LC_START-1),           L"The start of the LC errors")
rcode_def ( LC_EXCEPTION_CAUGHT_WHEN_INIT,              (LC_START-2),           L"Unhandled exception occurred when initializing list cach")
rcode_def ( LC_NVALID_POINTER,			                (LC_START-3),           L"A Valid Pointer was expected")
rcode_def ( LC_EXCEPTION_CAUGHT_WHEN_GET_VALUE,			(LC_START-4),           L"A Unhandled exception occurred when getting values from list cache")
rcode_def ( LC_LIST_NOT_FOUND,							(LC_START-5),           L"List not found")
rcode_def ( LC_ELEMENT_INDEX_OUT_OF_RANGE,				(LC_START-6),           L"List element index out of range")
rcode_def ( LC_LIST_PARAM_NOT_FOUND,					(LC_START-7),           L"List parameter not found")
rcode_def ( LC_EXCEED_LIST_CAPACITY,					(LC_START-8),           L"Exceed list capacity")
rcode_def ( LC_UNSUPPORTED_LIST_TYPE,					(LC_START-10),          L"Unsupported list type used")
rcode_def ( LC_VALUE_TYPE_MISMATCH_LIST_TYPE,			(LC_START-11),          L"Value type mismatches list type")
rcode_def ( LC_ERROR_END,								(LC_START-99),          L"End of Errors for LC")

#ifdef GEN_RTN_H
PP_IFDEF __cplusplus
    }
PP_ENDIF

PP_ENDIF        /* RTN_CODE_H */
#endif


