/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)panic.h	30.3  30  14 Nov 1996
 */
#pragma once

#include "std.h"
#include "stdio.h"


#if !defined(NO_P4_DOS)
#if defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4) || defined(__GNUC__)

extern void panic P((char *,...));

#else
extern void panic P((const char *,...));

#endif /* defined(MSDOS) || defined(CODECENTER) || defined(_MSC_VER) || defined(SVR4) */
#endif /* !defined(NO_P4_DOS) */


extern FILE* gStdOutFile;
extern void final_cleanup();

