// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "PlatformCommon.h"
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CStdString constructors will be explicit

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "std.h"	// Put std.h here until we get the precompiled headers figured out. MHD

#if 0
#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#endif


// TODO: reference additional headers your program requires here
