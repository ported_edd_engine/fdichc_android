
#include "ServerProjects/DDS/env_info.h"
//#include <windows.h>

#pragma once

typedef unsigned long BssStatus;

typedef enum LogSeverity
	{
       Critical,
       Error,
	   Warning,
       Information,
       Verbose,
	} LogSeverity;



void EddEngineLog (
   ENV_INFO	*env_info,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   LogSeverity severity,						// see documentation
   wchar_t* wCategory,
   LPCWSTR wMsgFormat,			        // a 'printf' format
   ...								    //    and it's parameters
);

#define BssError LogSeverity::Error
#define BssWarning LogSeverity::Warning
#define BssInformation LogSeverity::Information



//enum DebugBreakState {
//	CheckRegistry,
//	EnableDebugBreak,
//	CheckIfDebug
//	};
//void DoDebugBreak(DebugBreakState = CheckIfDebug, LPCTSTR mylpString = NULL);

// BssProgLog severity constants


	/*typedef enum DDSSeverity
	{
       DDS_Critical,
       DDS_Error,
	   DDS_Warning,
       DDS_Information,
       DDS_Verbose,
	} DDSSeverity;*/
//

//typedef unsigned long BssStatus;
//
//const BssStatus BSS_COMMON_ERROR_CATEGORY        = static_cast<BssStatus>(0x20000000);
//const BssStatus BSS_SUCCESS                       =                              0x00;
//
//const BssStatus BSS_FAILURE                       =  BSS_COMMON_ERROR_CATEGORY + 0x01;
//const BssStatus BSS_INVALID_PARAMETER             =  BSS_COMMON_ERROR_CATEGORY + 0x0C;





