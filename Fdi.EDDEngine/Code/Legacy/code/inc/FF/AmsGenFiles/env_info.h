/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)env_info.h	30.3  30  14 Nov 1996
 */
#pragma once

class CLegacyFF;	// Forward declaration
class IDDSSupport;	// Forward declaration

typedef int		BLOCK_HANDLE;
typedef int		DEVICE_HANDLE;

/*
 * This file contains the definition for ENV_INFO.  It is used to pass
 * environment information throughout the application and libraries.  This
 * structure may be modified as needed to communicate needed parameters to
 * appropriate sub-systems.
 */

class ENV_INFO {
public:
	BLOCK_HANDLE	block_handle;	// Used to store both the BLOCK_HANDLE and DEVICE_HANDLE
									// depending upon the value of handle_type.
									// (block_handle should really just be called handle,
									//  but there were too many places to change.)

  	IDDSSupport		*app_info;		// This is set to the IDDSSupport interface of the
									// controlling CLegacyFF object
	void*			value_spec;
    wchar_t			lang_code[10];

	enum HandleType
	{
		BlockHandle,		// If handle_type is BlockHandle, block_handle stores it.
		DeviceHandle		// If handle_type is DeviceHandle, block_handle stores a DEVICE_HANDLE
	};

	HandleType handle_type;

	ENV_INFO( const ENV_INFO & src );		// Copy constructor
	ENV_INFO( const CLegacyFF *pLegacyFF, void* pValueSpec, int iBlockInstance, const wchar_t *lang_code );	 // Usual constructor

private:
	ENV_INFO();								// Don't allow the default constructor
	ENV_INFO& operator= (const ENV_INFO &); // Don't allow the default assignment
};

#define ENV_INFO2 ENV_INFO		// Fold these two together. Kludge until we change the names throughout FF Legacy
