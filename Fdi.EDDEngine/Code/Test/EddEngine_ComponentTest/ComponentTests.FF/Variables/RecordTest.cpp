#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <wtypes.h>


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_a_block_with_a_record_in_parameter_list_FF
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem_record;
            static FDI_GENERIC_ITEM* genericItem_block;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_record_item_ID;
            static ITEM_ID* test_var_item_ID;

            static FLAT_VAR* test_var;
            static FLAT_BLOCK* test_block;
            static FLAT_RECORD* test_record;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0007\\TestCase0007.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // get the ItemIds for the test_uint
                test_var_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_uint", test_var_item_ID);

                // get the ItemIds for the test_record
                test_record_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_record", test_record_item_ID);
                
                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                
                genericItem_record = engine->GetGenericItem(
                    *test_record_item_ID,
                    nsEDDEngine::ITYPE_RECORD,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_RECORD),
                    iBlockInstance);

                test_record = dynamic_cast<FLAT_RECORD*>(genericItem_record->item);

                genericItem_block = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem_block->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem_record;
                delete genericItem_block;
                delete test_var_item_ID;
                delete test_var;
                delete test_block_item_ID;
                delete test_record_item_ID;
            }

            [TestMethod, TestCategory("RecordTest")]
            void It_should_have_19_items_in_the_parameter_list()
            {
                Assert::AreEqual(19u,(UINT32) test_block->param.count);
            }

            [TestMethod, TestCategory("RecordTest")]
            void It_should_have_the_record_as_first_item_in_the_parameter_list()
            {
                Assert::AreEqual(*test_record_item_ID,test_block->param.list[0].ref.id);
            }

            [TestMethod, TestCategory("RecordTest")]
            void It_should_have_the_correct_item_id_for_the_record()
            {
                Assert::AreEqual(*test_record_item_ID, test_record->id);
            }

            [TestMethod, TestCategory("RecordTest")]
            void It_should_have_as_second_member_of_the_record_the_test_uint_variable()
            {
                Assert::AreEqual(*test_var_item_ID, test_record->members.list[1].ref.id);
            }
        };

    } // namespace FF
} // namespace ComponentTests
