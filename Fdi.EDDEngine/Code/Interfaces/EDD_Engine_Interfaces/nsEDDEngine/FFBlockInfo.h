
// FFBlockInfo.h

#pragma once

namespace nsEDDEngine
{


	enum BLOCK_TYPE {
		RESOURCE_BLOCK		= 0,
		TRANSDUCER_BLOCK	= 1,
		FUNCTION_BLOCK		= 2,
		UNKNOWN_BLOCK		= 255
	};


	/////////////////////////////////////////////////////////////////////////////
	// CCharacteristicRecord 

	const int BLOCK_TAG_SIZE = 32;

	struct CCharacteristicRecord
	{
        char m_pTag[BLOCK_TAG_SIZE+1];	// FF block tags are 32 bytes long
										// I will be null terminating this string
		unsigned long   m_ulDDMemberId;
		unsigned long   m_ulDDItemId;
		unsigned short  m_usDDRevision;
		unsigned short  m_usProfile;
		unsigned short  m_usProfileRev;
		unsigned long   m_ulExecTime;
		unsigned long   m_ulPeriodOfExec;
		unsigned short  m_usNumOfParams;
		unsigned short  m_usNextFb;
		unsigned short  m_usViewIndex;
		unsigned char   m_ucNumOfView3;
		unsigned char   m_ucNumOfView4;
	};

	//
	/////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////
	// CFieldbusBlockInfo 

	struct CFieldbusBlockInfo
	{
		CCharacteristicRecord	m_CharRecord;
		BLOCK_TYPE				m_BlockType;
		unsigned short			m_usObjectIndex;
	};


	struct CCrossblockInfo
	{
		int	block_handle;
		unsigned short object_index;
		unsigned long   ulItemId;
        char tag[BLOCK_TAG_SIZE+1];
	};

}

