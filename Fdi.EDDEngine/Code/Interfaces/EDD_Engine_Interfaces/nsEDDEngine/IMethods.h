#pragma once

#include "nsEDDEngine/ProtocolType.h"
#include "nsEDDEngine/Common.h"
#include "nsConsumer/IParamCache.h"


namespace nsEDDEngine
{
	class IAsyncResult		// Used like the .Net IAsyncResult, but for native code
	{
	public:
		virtual void * get_AsyncState() = 0;
		virtual void * get_AsyncWaitHandle() = 0;
		virtual bool   get_CompletedSynchronously() = 0;
		virtual bool   get_IsCompleted() = 0;

		virtual ~IAsyncResult(void){};
	};


	class IAsyncCallbackHandler 
	{
	public:
		virtual void AsyncCallback(IAsyncResult* ar) = 0;
	};


	// The IMethods Interface is used by the Consumer to instruct the EDD Engine to run an EDD Method.
	class IMethods
	{
	public:
		virtual ~IMethods(void) {};


		// Runs the specified method
		// Note: The caller should always pass in an AsyncCallback function so that the calling thread never
		// blocks while waiting for the method to complete. This will enable the Consumer to respond to requests on its
		// IBuiltinSupport interface. Other ways of waiting which require the calling thread to block should not be used.
		//
		virtual IAsyncResult* BeginMethod( int iBlockInstance, void* pValueSpec, ACTION* pAction,
										IAsyncCallbackHandler *pCallback, void * asyncState, const wchar_t *lang) = 0;

		// NOTE: After this function is returned, if the instance of class IAsyncResult is completed, the pointer pIAsyncResult must be deleted.
		virtual void CancelMethod( IAsyncResult* pIAsyncResult ) = 0;

		// This function returns enumerations to indicate method completion succeeded, aborted, cancelled or failed.
		// If method return type is defined in DD, the method returned value will be stored in pointer pReturnedValue.
		// NOTE: the pointer *asyncResult pointing to class IAsyncResult doesn't need to be deleted from this function caller.
		virtual Mth_ErrorCode EndMethod( IAsyncResult** asyncResult, nsConsumer::EVAL_VAR_VALUE *pReturnedValue = nullptr ) = 0;


		// Runs a variable action method (called only by methods associated with a pre-edit, post-edit, pre-read, post-read, pre-write or post-write action)
		// Note: Some protocols (like HART and FF) limit the builtins that are used during pre/post read/write actions to
		// access only the Consumer's Parameter Cache. In this case the caller can block while waiting for the method 
		// to complete. This can be done by calling EndMethod() explicitly, waiting on the AsynWaitHandle,
		// or polling until the IsCompleted propery is true.
		//
		virtual IAsyncResult* BeginVariableActionMethod( int iBlockInstance, void* pValueSpec, nsConsumer::EVAL_VAR_VALUE *pActionValue, ACTION* pAction,
										IAsyncCallbackHandler *pCallback, void * asyncState, const wchar_t *lang_code) = 0;

		// Returns true if the method completed successfully, or false, if the method was aborted
		virtual Mth_ErrorCode EndVariableActionMethod( IAsyncResult** asyncResult, nsConsumer::EVAL_VAR_VALUE *pReturnedValue ) = 0;


		// TODO MHD Determine whether we need a more comprehensive set of services (ReadVar, WriteVar, etc.)
		// which encompasses all the Pre/Post actions with a call out to do the main activity.

	};


}
