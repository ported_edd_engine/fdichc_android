#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <vcclr.h>
#include <string>

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass]
        public ref class GetCommandListTests
        {
        public: 
            [TestMethod, TestCategory("GetCommandListTests")]
            void GetSimpleCommandList()
            {
                EddEngineController^ engine = gcnew EddEngineController("Hart\\DeltabarS.fm8");
                engine->Initialize();
                
                // item id of VARIABLE in COMMAND
                ITEM_ID id = 16384;

                nsEDDEngine::FDI_PARAM_SPECIFIER ps;
                ps.eType = nsEDDEngine::FDI_PS_ITEM_ID;
                ps.id = id;
                ps.subindex = 0;

                nsEDDEngine::FDI_COMMAND_LIST cmdList;
                cmdList.count = 0;
                cmdList.list = 0;

                int rc = engine->Instance->GetCommandList(0, &ps, nsEDDEngine::FDI_READ_COMMAND, &cmdList);

                Assert::AreEqual(0, rc);
                Assert::AreEqual(1, cmdList.count);
                // COMMAND read_pv_current_and_percent_range
                Assert::AreEqual(2u, cmdList.list->command_number);
            }
        };
    }
}
