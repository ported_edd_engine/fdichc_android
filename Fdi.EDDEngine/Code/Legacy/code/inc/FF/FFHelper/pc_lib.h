/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *		@(#)pc_lib.h	30.3  30  14 Nov 1996
 */

#ifndef PC_LIB_H
#define PC_LIB_H

#include "evl_lib.h"
#include "comm.h"

typedef unsigned long PARAM_FLAG;

/*
 *  Values for flag
 */

#define	NOT_MODIFIED	0
#define	MODIFIED		1

/*
 *	PC access routines (pc_lib.c)
 */

#ifdef __cplusplus
	extern "C" {
#endif
extern int valid_method_tag (unsigned short method_tag);
extern int pc_open_block P((BLOCK_HANDLE bh));
extern int pc_close_block P((BLOCK_HANDLE bh));

extern int pc_get_param_value P((ENV_INFO *env_info, P_REF *pcpr, 
				EVAL_VAR_VALUE *evv));
extern int pc_put_param_value P((ENV_INFO *env_info, P_REF *pcpr, 
				EVAL_VAR_VALUE *evv));

extern int pc_read_param_value P((ENV_INFO *env_info, P_REF *pr));
extern int pc_write_param_value P((ENV_INFO *env_info, P_REF *pr, 
				EVAL_VAR_VALUE *evv));
extern int pc_write_all_param_values P((ENV_INFO *env_info));

extern int pc_discard_put_param_value P((ENV_INFO *env_info, P_REF *pcpr));
extern int pc_discard_all_put_params P((ENV_INFO *env_info));

extern int pc_param_value_modified P((ENV_INFO *env_info, P_REF *pcpr, 
				int *flag));
extern int pc_any_param_value_modified P((ENV_INFO *env_info, int *flag));

extern int pc_clear_param_flag P((BLOCK_HANDLE bh, P_REF *pcpr, PARAM_FLAG pf));
extern int pc_set_param_flag P((BLOCK_HANDLE bh, P_REF *pcpr, PARAM_FLAG pf));
extern int pc_test_param_flag P((BLOCK_HANDLE bh, P_REF *pcpr,
				PARAM_FLAG test_pf, PARAM_FLAG *set_pf));
#ifdef __cplusplus
	}
#endif

#endif  /*PC_LIB_H*/
