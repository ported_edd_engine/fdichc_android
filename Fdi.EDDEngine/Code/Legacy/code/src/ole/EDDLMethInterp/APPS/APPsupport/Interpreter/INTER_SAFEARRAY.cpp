// INTER_SAFEARRAY.cpp //

#include "stdafx.h"

//#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
//stevev 20feb07-merge- contents moved to ddbGeneral #include "..\DevServices\stdafx.h"   // HOMZ
//#endif


#include <assert.h>
/* comutil.h uses Bill's TRUE/FALSE that the general erroneously defines on purpose */
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

#include "INTER_SAFEARRAY.h"
#include "INTER_VARIANT.h"
//#include "PlatformCommon.h"
const int size_ofs[] = SIZE_OFS;

//////////////////////////////////////////////////
// Simple Constructor
/////////////////////////////////////////////////
INTER_SAFEARRAY::INTER_SAFEARRAY()
{
	m_wcharPtr = NULL;
	m_i32mem = 0;
}

//////////////////////////////////////////////////
// Constructor
/////////////////////////////////////////////////
INTER_SAFEARRAY::INTER_SAFEARRAY( _CHAR* pszValue, _INT32 arraySize )
{
	m_wcharPtr = NULL;
	if (arraySize)
	{
		m_i32mem = arraySize + 1;
	}
	else
	{
		m_i32mem = (_INT32)strlen(pszValue)+1;
	}
	m_data.pvData = new _UCHAR[m_i32mem];
	m_data.varType = RUL_CHAR;
	m_data.cbElements = 1;
	m_data.cDims = 1;
	memcpy( m_data.pvData, pszValue, m_i32mem );

	INTER_SAFEARRAYBOUND lVB;
	lVB.cElements = m_i32mem - 1;
	m_data.vecBounds.push_back(lVB);
}

INTER_SAFEARRAY::INTER_SAFEARRAY( _UCHAR* pszValue )
{
	m_wcharPtr = NULL;
	m_i32mem = (_INT32)strlen((_CHAR*)pszValue)+1;
	m_data.pvData = new _UCHAR[m_i32mem];
	m_data.varType = RUL_UNSIGNED_CHAR;
	m_data.cbElements = 1;
	m_data.cDims = 1;
	memcpy( m_data.pvData, pszValue, m_i32mem );
}

//This is used for value array
INTER_SAFEARRAY::INTER_SAFEARRAY( void* pmem, unsigned long ulNumOfElements, VARIANT_TYPE vt, unsigned short usElementSize )
{
	m_wcharPtr = NULL;
	m_data.cbElements = usElementSize;						//element size
	m_data.varType = vt;									//element type
	m_i32mem = ulNumOfElements * m_data.cbElements;			//entire memory size
	m_data.cDims = 1;										//number of dimensions
	m_data.pvData = new _UCHAR[m_i32mem];					//memory
	if (pmem != nullptr)
	{
		memcpy(m_data.pvData, pmem, m_i32mem);
	}
	else
	{
		memset(m_data.pvData, 0, m_i32mem);
	}

	INTER_SAFEARRAYBOUND lVB;
	lVB.cElements = ulNumOfElements;						//element count in each dimenstion
	m_data.vecBounds.push_back(lVB);
}

INTER_SAFEARRAY::INTER_SAFEARRAY( const tchar* pszValue )
{
	m_wcharPtr = NULL;
	m_i32mem = (_INT32)(_tstrlen(pszValue) + 1) * sizeof(tchar);
	m_data.cbElements = sizeof(tchar);
	m_data.pvData = new _UCHAR[m_i32mem];
	m_data.varType = RUL_USHORT;
	m_data.cDims = 1;
	PS_Wcscpy( (tchar *)m_data.pvData, _tstrlen(pszValue) + 1, pszValue);

	INTER_SAFEARRAYBOUND lVB;
	lVB.cElements = m_i32mem / m_data.cbElements;			//element count in each dimenstion
	m_data.vecBounds.push_back(lVB);
}


INTER_SAFEARRAY::INTER_SAFEARRAY( _BYTE_STRING* bsValue )
{
	m_wcharPtr = NULL;
	m_i32mem = bsValue->bsLen;	// actual size
	m_data.cbElements = sizeof(_UCHAR);
	m_data.pvData = new _UCHAR[m_i32mem + 1];// store an extra null top be kind
	m_data.varType = RUL_UNSIGNED_CHAR;
	m_data.cDims = 1;
	memcpy( m_data.pvData, bsValue->bs, m_i32mem );
	m_data.pvData[m_i32mem] = 0;// just in case...
}

//////////////////////////////////////////////////
// Copy Constructor
/////////////////////////////////////////////////
INTER_SAFEARRAY::INTER_SAFEARRAY( INTER_SAFEARRAY& safearray)
{
	m_wcharPtr = NULL;
	m_i32mem = 0;
	*this = safearray;
}

//////////////////////////////////////////////////
// destructor
/////////////////////////////////////////////////
INTER_SAFEARRAY::~INTER_SAFEARRAY()
{
	if( m_wcharPtr )
	{
		delete m_wcharPtr;
		m_wcharPtr = NULL;
	}

	if( m_data.pvData != nullptr)
	{
		try		//TSRPRASAD 09MAR2004 Fix the memory leaks
		{
			delete []  m_data.pvData;
			m_data.pvData = nullptr;
		}
		catch(...)
		{
		}
	}
	if (m_data.pvDataArray != nullptr)
	{
		try
		{
			for (size_t i = 0; i<m_data.vecBounds[1].cElements; i++)
			{
				delete[]  m_data.pvDataArray[i];
			}
			delete[] m_data.pvDataArray;
			m_data.pvDataArray = nullptr;
		}
		catch (...)
		{
		}
	}
}
//////////////////////////////////////////////////
// + Operator
/////////////////////////////////////////////////
INTER_SAFEARRAY INTER_SAFEARRAY::operator+(INTER_SAFEARRAY& var)
{
	INTER_SAFEARRAY retValue;

    //number of dimensoins
    assert(var.m_data.cDims == m_data.cDims);
    retValue.m_data.cDims = m_data.cDims;
    //element type
    retValue.m_data.varType = m_data.varType;

    //allocate memory
    retValue.m_i32mem = m_i32mem + var.m_i32mem;
    if (retValue.m_i32mem > 0)
    {
        retValue.m_data.pvData = new _UCHAR[retValue.m_i32mem];
        memset(retValue.m_data.pvData, 0, m_i32mem);
    }

    //data
    INTER_VARIANT temp;
    long i = 0;
    for (; (i < m_i32mem); i += m_data.cbElements)
    {
        GetElement(i, &temp);
        //get rid of the last null terminator
        if ((i >= (m_i32mem - m_data.cbElements)) && ((unsigned char)temp == 0))
        {
            temp.Clear();
            retValue.m_i32mem -= m_data.cbElements;
            break;
        }
        else
        {
            retValue.SetElement(i, &temp);
            temp.Clear();
        }
    }
    for (long j = 0; (j < var.m_i32mem); j += var.m_data.cbElements)
    {
        var.GetElement(j, &temp);
        retValue.SetElement(i+j, &temp);
        temp.Clear();
    }

    //element size
    retValue.m_data.cbElements = m_data.cbElements;

    //number of elements
    INTER_SAFEARRAYBOUND lVB;
    lVB.cElements = (_UINT32)retValue.m_i32mem / retValue.m_data.cbElements;
    retValue.m_data.vecBounds.push_back(lVB);

	return retValue;
}


//////////////////////////////////////////////////
// Equal Operators
/////////////////////////////////////////////////
INTER_SAFEARRAY& INTER_SAFEARRAY::operator=( INTER_SAFEARRAY& safearray )
{
	if (&safearray == this)
		return *this;

	//Before calling this function, the destination array element size and array element type are known
	if( m_i32mem )
	{
		//clean destination array memory
		int nDataSize = safearray.m_data.cbElements * safearray.m_data.vecBounds[safearray.m_data.cDims - 1].cElements;
		if (( nDataSize > m_i32mem ) && (m_data.varType == RUL_USHORT))
		{
			//this might be a DD_STRING variable. Expand memory size
			delete [] m_data.pvData;
			m_i32mem = nDataSize;
			m_data.pvData = new _UCHAR[m_i32mem];
			memset( m_data.pvData, 0, m_i32mem );
			
			m_data.vecBounds[m_data.cDims - 1].cElements = (_UINT32)m_i32mem / m_data.cbElements;	//number of elements
		}
		else
		{
			memset(m_data.pvData, 0, m_i32mem);
		}

		//copy number of dimensions
		m_data.cDims = safearray.GetDims(NULL);

		//copy array memory and set number of elements
		int j = 0;		//size copied
		if (m_data.varType == safearray.m_data.varType)
		{
			//find the smallest size
			if (m_i32mem > safearray.m_i32mem)
			{
				j = safearray.m_i32mem;
			}
			else
			{
				j = m_i32mem;
			}
			memcpy(m_data.pvData, safearray.m_data.pvData, j);
		}
		else
		{
			//destination element type is different source element type
			//get element from source array and set element to destination array
			INTER_VARIANT temp;
			for (int i = 0; ((i < safearray.m_i32mem) && (j < m_i32mem)); i += safearray.m_data.cbElements, j += m_data.cbElements)
			{
				safearray.GetElement(i, &temp);
				SetElement(j, &temp);
				temp.Clear();
			}
		}

		if (m_data.varType == RUL_CHAR)
		{
			//If this is string, add NULL termination at the end
			m_data.pvData[j - 1] = '\0';
		}
	}
	else
	{
		m_data.varType = safearray.m_data.varType;
		m_data.cbElements = (_USHORT)safearray.GetElementSize();
		// use calculation below....m_i32mem = safearray.MemoryAllocated();
		m_data.cDims = safearray.GetDims(NULL);
	
		//now look at the vector bounds.
		m_data.vecBounds.clear();
		size_t vecBoundsSize = safearray.m_data.vecBounds.size();
		assert(vecBoundsSize == m_data.cDims);
		for( size_t i=0; i<vecBoundsSize; i++ )
		{
			m_data.vecBounds.push_back(safearray.m_data.vecBounds[i]);
		}

		//allocate memory and set m_i32mem based on m_data.cbElements, m_data.cDims and m_data.vecBounds
		Allocate();

		if (m_data.pvData != NULL && safearray.m_data.pvData != NULL && m_i32mem>0)
		{
			memcpy( m_data.pvData, safearray.m_data.pvData, m_i32mem );
		}
	
	}

	return *this;
}

INTER_SAFEARRAY& INTER_SAFEARRAY::operator=( char*    pSrc)
{
	if (pSrc == NULL)
	{
		return *this;					/* throw a failure - parameter empty */
	}
	if( m_i32mem ) // size set and memory allocated
	{
		if (m_data.cDims != 1)
		{
			return *this;		/* throw a failure - dimensional mismatch*/ 
		}
		memset( m_data.pvData, 0, m_i32mem );//clear data at the start.

		_INT32 nDataSize = (_INT32)strlen(pSrc)+1;//remember to include null terminator

		if ( (m_data.varType == RUL_CHAR) || (m_data.varType == RUL_UNSIGNED_CHAR) )	// c-string to c-string
		{
			if( nDataSize > m_i32mem )//if larger, then truncate.
			{
				//don't change memory size m_i32mem
				nDataSize = m_i32mem;
			}
			memcpy( m_data.pvData, pSrc, nDataSize );//remember to copy datasize.
			if (m_data.varType == RUL_CHAR)
			{
				m_data.pvData[nDataSize - 1] = '\0';//fail-safe for null termination
			}
		}
		else if ( m_data.varType == RUL_USHORT )//c_string to wide-string
		{// convert
			string  srcStr(pSrc);
			wstring dstStr;
			dstStr = AStr2TStr(srcStr);//convert to wide string.
			nDataSize = (_INT32)(dstStr.size()+1) * sizeof(wchar_t);//recalculate length
			if ( nDataSize > m_i32mem )
			{
				delete []  m_data.pvData;//dont leak memory
				m_i32mem = nDataSize;
				m_data.pvData = new _UCHAR[m_i32mem];
				memset( m_data.pvData,0,m_i32mem);
				INTER_SAFEARRAYBOUND lVB;
				lVB.cElements = (_UINT32)m_i32mem / m_data.cbElements;	//number of elements
				m_data.vecBounds.clear();
				m_data.vecBounds.push_back(lVB);
			}
			memcpy( m_data.pvData, dstStr.c_str(), nDataSize);//remember to copy datasize.
			m_data.pvData[m_i32mem-2] =  '\0';//fail-safe for null termination
			m_data.pvData[m_i32mem-1] =  '\0';//fail-safe for null termination
		}
		// else - unsupported string type or  multi dimension array 
	}
	else if (m_data.varType == RUL_NULL )
	{// alloc and copy
		_INT32 srcLen = (_INT32)strlen(pSrc)+1;

		m_data.varType = RUL_CHAR;
		m_data.cbElements = sizeof(char);
		m_data.cDims      = 1;
		if( m_data.pvData != 0)//double check that we do not leak memory
		{
			delete []  m_data.pvData;
			m_data.pvData = 0;
			m_i32mem = 0;
		}
			
		m_i32mem = srcLen;
		m_data.pvData = new _UCHAR[m_i32mem];
		memset( m_data.pvData,0,m_i32mem);
		memcpy( m_data.pvData, pSrc, m_i32mem );

		//now look at the vector bounds.
		INTER_SAFEARRAYBOUND lVB;
		lVB.cElements = (_UINT32)(m_i32mem - 1) / m_data.cbElements;	//number of elements
		m_data.vecBounds.clear();
		m_data.vecBounds.push_back(lVB);
	}
	else
	{
		return *this; /* throw a failure */
	}
	

	return *this;
}

//This function allows to expand char[] array memeory size in order to be back compatible
// for builtins get_dictionary_string and get_stddict_string
void INTER_SAFEARRAY::ExpandString(const wchar_t* pSrc, _UINT32 u32Dim)
{

	if (pSrc == NULL)
	{
		return;					/* parameter empty */
	}
	if( m_i32mem ) // size set and memory allocated
	{
		if (m_data.cDims == 1)
		{
			//one dimension
		memset( m_data.pvData, 0, m_i32mem );//memset the data up front.
	
		wstring srcStr(pSrc);
		string dstStr;
		dstStr = TStr2AStr(srcStr); // convert to narrow
		int nDataSize = (int)((dstStr.size()+1) * sizeof(char));//recalculate narrow length
		if( nDataSize > m_i32mem )//if larger, then re-allocate.
		{
			delete []  m_data.pvData;//avoid memory leaks.
			m_i32mem = nDataSize;
			m_data.pvData = new _UCHAR[m_i32mem];
			memset(m_data.pvData,0,m_i32mem);
			INTER_SAFEARRAYBOUND lVB;
			lVB.cElements = (_UINT32)(m_i32mem - 1) / m_data.cbElements;	//number of elements
			m_data.vecBounds.clear();
			m_data.vecBounds.push_back(lVB);
		}
		memcpy( m_data.pvData, dstStr.c_str(), nDataSize);//remember to copy data size.
		m_data.pvData[m_i32mem-1] =  '\0';//fail-safe for null termination

		m_data.varType = RUL_CHAR;		//string
		}
		else if (m_data.cDims == 2)
		{
			//two dimensions
			const auto dimSize = m_data.cbElements * m_data.vecBounds[0].cElements;

			const size_t nDataSize = (wcslen(pSrc) + 1) * sizeof(wchar_t);
			if (nDataSize > dimSize)//if larger, then re-allocate.
			{
				//update new size
				m_data.vecBounds[0].cElements = nDataSize / m_data.cbElements;	//including NULL termination
				
				//save old values
				_UCHAR** savedDataArray = new _UCHAR*[m_data.vecBounds[1].cElements];
				for (size_t i = 0; i<m_data.vecBounds[1].cElements; i++)
				{
					savedDataArray[i] = new _UCHAR[dimSize];
					memcpy(savedDataArray[i], m_data.pvDataArray[i], dimSize);
				}

				//allocate new memory based on new size
				Allocate();

				//restore old values except index u32Dim
				for (size_t i = 0; i<m_data.vecBounds[1].cElements; i++)
				{
					if (i != u32Dim)
					{
						memcpy(m_data.pvDataArray[i], savedDataArray[i], dimSize);
					}

					//release memory
					delete[] savedDataArray[i];
				}

				//release memory
				delete[] savedDataArray;
			}
			else
			{
				memset(m_data.pvDataArray[u32Dim], 0, dimSize);
			}
			memcpy(m_data.pvDataArray[u32Dim], pSrc, nDataSize);
			m_data.pvDataArray[u32Dim][nDataSize - 2] = '\0';	//fail-safe for null termination
			m_data.pvDataArray[u32Dim][nDataSize - 1] = '\0';	//fail-safe for null termination
		}
	}
}

INTER_SAFEARRAY& INTER_SAFEARRAY::operator=( const wchar_t* pSrc)
{
	if (pSrc == NULL)
	{
		return *this;					/* throw a failure - parameter empty */
	}

	_INT32 nDataSize = (_INT32)(wcslen(pSrc)+1) * sizeof(wchar_t);
	if( m_i32mem ) // size set and memory allocated
	{
		if (m_data.cDims != 1) 
		{
			return *this;		/* throw a failure - dimensional mismatch*/ 
		}
		memset( m_data.pvData, 0, m_i32mem );//memset the data up front.

		if ( m_data.varType == RUL_USHORT )	// wide-string to wide-string
		{
			if( nDataSize > m_i32mem )
			{
				delete []  m_data.pvData;
				m_i32mem = nDataSize;
				m_data.pvData = new _UCHAR[m_i32mem];
				memset(m_data.pvData,0,m_i32mem);
				INTER_SAFEARRAYBOUND lVB;
				lVB.cElements = (_UINT32)(wcslen(pSrc) + 1);	//number of elements
				m_data.vecBounds.clear();
				m_data.vecBounds.push_back(lVB);
			}
			memcpy( m_data.pvData, pSrc, nDataSize );
			m_data.pvData[m_i32mem-2] =  '\0';//fail-safe for null termination
			m_data.pvData[m_i32mem-1] =  '\0';
		}
		else if( (m_data.varType == RUL_UNSIGNED_CHAR) || (m_data.varType == RUL_CHAR ) )	// wide-string to byte-string
		{
			wstring srcStr(pSrc);
			string dstStr;
			dstStr = TStr2AStr(srcStr); // convert to narrow
			nDataSize = (int)((dstStr.size()+1) * sizeof(char));//recalculate narrow length
			if( nDataSize > m_i32mem )//if larger, then truncate.
			{
				//don't change memory size m_i32mem
				nDataSize = m_i32mem;
			}
			memcpy( m_data.pvData, dstStr.c_str(), nDataSize );//remember to copy data size.
			if (m_data.varType == RUL_CHAR)
			{
				m_data.pvData[nDataSize - 1] = '\0';//fail-safe for null termination
			}
		}
		// else - unsupported string type or  multi dimension array 
	}  //end of if( m_i32mem )
	else if (m_data.varType == RUL_NULL )
	{// alloc and copy
		m_data.varType    = RUL_USHORT;
		m_data.cbElements = sizeof(wchar_t);
		m_data.cDims      = 1;
		if( m_data.pvData != 0)//double check that we do not leak memory
		{
			delete []  m_data.pvData;
			m_data.pvData = 0;
			m_i32mem = 0;
		}
			
		m_i32mem = nDataSize;

		m_data.pvData = new _UCHAR[m_i32mem];
		memset( m_data.pvData,0,m_i32mem);
		memcpy( m_data.pvData, pSrc, m_i32mem );

		//now look at the vector bounds.
		INTER_SAFEARRAYBOUND lVB;
		lVB.cElements = (_UINT32)m_i32mem / m_data.cbElements;	//number of elements
		m_data.vecBounds.clear();
		m_data.vecBounds.push_back(lVB);
	}
	else
	{
		return *this; /* throw a failure */
	}
	
	return *this;
}

INTER_SAFEARRAY& INTER_SAFEARRAY::operator=( _BYTE_STRING& src)
{	
	if( m_i32mem ) // size set and memory allocated
	{
		if (m_data.cDims != 1)
		{
			return *this;		/* throw a failure - dimensional mismatch*/
		}
		memset( m_data.pvData, 0, m_i32mem );
		long nDataSize = src.bsLen;
		if ((nDataSize > m_i32mem) && (m_data.varType == RUL_USHORT))
		{
			//this might be a DD_STRING variable. Expand memory size
			delete []  m_data.pvData;//avoid memory leaks.
			m_i32mem = nDataSize;
			m_data.pvData = new _UCHAR[m_i32mem];
			memset(m_data.pvData,0,m_i32mem);
			INTER_SAFEARRAYBOUND lVB;
			lVB.cElements = (_UINT32)m_i32mem / m_data.cbElements;	//number of elements
			m_data.vecBounds.clear();
			m_data.vecBounds.push_back(lVB);
		}
		else
		{
			//no expansion on memory size. find the minimum size
			if (nDataSize > m_i32mem)
			{
				nDataSize = m_i32mem;
			}
		}

		int i = 0;

		if ( m_data.varType == RUL_BOOL )
		{
			bool*  pB = (bool*)m_data.pvData;
			bool*  pF = (bool*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( src.bs[i] ) ? true : false;
			}
		}
		else if ( m_data.varType == RUL_CHAR )
		{
			_CHAR*  pB = (_CHAR*)m_data.pvData;
			for(i = 0 ; i < nDataSize; i++, pB++)
			{
				*pB = ( _CHAR )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_INT )
		{
			_INT32*  pB = (_INT32*)m_data.pvData;
			_INT32*  pF = (_INT32*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _INT32 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_UINT )
		{
			_UINT32*  pB = (_UINT32*)m_data.pvData;
			_UINT32*  pF = (_UINT32*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _UINT32 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_SHORT )
		{
			_INT16*  pB = (_INT16*)m_data.pvData;
			_INT16*  pF = (_INT16*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _INT16 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_USHORT )
		{
			_UINT16*  pB = (_UINT16*)m_data.pvData;
			_UINT16*  pF = (_UINT16*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _UINT16 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_LONGLONG )
		{
			_INT64*  pB = (_INT64*)m_data.pvData;
			_INT64*  pF = (_INT64*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _INT64 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_ULONGLONG )
		{
			_UINT64*  pB = (_UINT64*)m_data.pvData;
			_UINT64*  pF = (_UINT64*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _UINT64 )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_FLOAT )
		{
			_FLOAT*  pB = (_FLOAT*)m_data.pvData;
			_FLOAT*  pF = (_FLOAT*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _FLOAT )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_DOUBLE )
		{
			_DOUBLE*  pB = (_DOUBLE*)m_data.pvData;
			_DOUBLE*  pF = (_DOUBLE*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _DOUBLE )src.bs[i];
			}
		}
		else if ( m_data.varType == RUL_UNSIGNED_CHAR  )
		{
			_UCHAR*  pB = (_UCHAR*)m_data.pvData;
			_UCHAR*  pF = (_UCHAR*)(m_data.pvData+nDataSize);
			for(i = 0 ; pB < pF; i++, pB++)
			{
				*pB = ( _UCHAR )src.bs[i];
			}
		}
		// else - unsupported string type or  multi dimension array 
	}
	else if (m_data.varType == RUL_NULL )
	{// alloc and copy
		int srcLen = src.bsLen;

		m_data.varType    = RUL_UNSIGNED_CHAR;
		m_data.cbElements = sizeof(_UCHAR);
		m_data.cDims      = 1;
		if( m_data.pvData != 0)//double check that we do not leak memory
		{
			delete []  m_data.pvData;
			m_data.pvData = 0;
			m_i32mem = 0;
		}
			
		m_i32mem = srcLen + sizeof(_UCHAR);

		m_data.pvData = new _UCHAR[m_i32mem];
		memset( m_data.pvData,0,m_i32mem);
		memcpy( m_data.pvData, &src, srcLen );

		//now look at the vector bounds.
		INTER_SAFEARRAYBOUND lVB;
		lVB.cElements = srcLen;
		m_data.vecBounds.clear();
		m_data.vecBounds.push_back(lVB);
	}
	else	/* we only support operator= to same type or type null */
	{
		return *this; /* throw a failure */
	}
	
	return *this;
}


INTER_SAFEARRAY::operator _CHAR *(void)
{//   assume string - w/conversion (wide 2 narrow)
	char* pChar = NULL;
	if( (m_data.varType == RUL_CHAR) || (m_data.varType == RUL_UNSIGNED_CHAR) )
	{
		pChar = ( (char *)m_data.pvData );
	}
	else
	{
		assert ( 1 == 0 );
	}
	return pChar;
}
INTER_SAFEARRAY::operator wchar_t *(void)
{//   assume string - w/conversion (narrow 2 wide)
	if( m_wcharPtr )
	{
		delete m_wcharPtr;
		m_wcharPtr = NULL;
	}

	if( (m_data.varType == RUL_CHAR) || (m_data.varType == RUL_UNSIGNED_CHAR) )
	{
		wstring str = AStr2TStr( (char *)m_data.pvData );
		 m_wcharPtr = new wchar_t[wcslen(str.c_str())+1];
		_tstrcpy( m_wcharPtr, str.c_str() );
	}
	else if (m_data.varType == RUL_USHORT)
	{
		INTER_VARIANT temp;
		wstring wStr;
		for (int i = 0; i < m_i32mem; i += m_data.cbElements)
		{
			GetElement(i, &temp);
			wStr += ((wchar_t)temp);
			temp.Clear();
		}
		m_wcharPtr = new wchar_t[wcslen(wStr.c_str()) + 1];
		_tstrcpy(m_wcharPtr, wStr.c_str());
	}
	else if( m_data.varType == RUL_WIDECHARPTR )
	{
		return (wchar_t *)m_data.pvData;
	}
	return m_wcharPtr;
}
void INTER_SAFEARRAY::Add(const INTER_SAFEARRAY& var)
{

    //number of dimensoins
    assert(m_data.cDims == var.m_data.cDims);
    m_data.cDims = var.m_data.cDims;
    //element type
    m_data.varType = var.m_data.varType;

    //allocate memory
    m_i32mem += var.m_i32mem;
    if (m_i32mem > 0)
    {
        m_data.pvData = new _UCHAR[m_i32mem];
        memset(m_data.pvData, 0, m_i32mem);
    }

    //data
    INTER_VARIANT temp;
    long i = 0;
    _INT32				 m_i32mem_copy =m_i32mem ;
    _USHORT				 cbElements = m_data.cbElements;
    for (; (i <m_i32mem_copy); i += cbElements)
    {
        GetElement(i, &temp);
        //get rid of the last null terminator
        if ((i >= (m_i32mem - cbElements)) && ((unsigned char)temp == 0))
        {
            temp.Clear();
            m_i32mem -= cbElements;
            break;
        }
        else
        {
            SetElement(i, &temp);
            temp.Clear();
        }
    }
    for (long j = 0; (j < var.m_i32mem); j += var.m_data.cbElements)
    {
        temp.SetValue(((_UCHAR*)(var.m_data.pvData))+j,var.m_data.varType);
        SetElement(i+j, &temp);
        temp.Clear();
    }

    //number of elements
    INTER_SAFEARRAYBOUND lVB;
    lVB.cElements = (_UINT32)m_i32mem / m_data.cbElements;
    m_data.vecBounds.push_back(lVB);
}


void INTER_SAFEARRAY::SetAllocationParameters(VARIANT_TYPE vt,_USHORT cDims,
																INTER_SAFEARRAYBOUND* prgsaBound)
{
	m_data.cDims = cDims;
	m_data.cbElements = (_USHORT)INTER_VARIANT::VariantSize(vt);
	m_data.varType = vt;

	for (int i=0; i<cDims; i++)
	{
		m_data.vecBounds.push_back(prgsaBound[i]);
	}
}

void INTER_SAFEARRAY::AddDim(INTER_SAFEARRAYBOUND* prgsaBound)
{
	m_data.cDims++;
	m_data.vecBounds.push_back(prgsaBound[0]);
	//throw error if prgsaBound[0].cElements <=0;
}

_USHORT INTER_SAFEARRAY::GetDims(vector<_INT32>* pvecDims)
{
	if(pvecDims)
	{
		size_t i32Size = m_data.vecBounds.size();
		for(size_t i=0; i<i32Size; i++)
		{
			pvecDims->push_back(m_data.vecBounds[i].cElements);
		}
	}
	return m_data.cDims;
}


_INT32	INTER_SAFEARRAY::MemoryAllocated()
{
	return m_i32mem;
}


//This function allocates memory to pvData. cbElements and vecBounds[].cElements are known
_INT32	INTER_SAFEARRAY::Allocate()
{
	//number of dimensions
	const size_t i32Size = m_data.vecBounds.size();
	if( (m_data.cDims > 0) && (i32Size > 0) )
	{
		//Walt:EPM-24aug07
		if( m_data.pvData != nullptr)//double check that we do not leak memory
		{
			delete []  m_data.pvData;
			m_data.pvData = nullptr;
		}
		if (m_data.pvDataArray != nullptr)//double check that we do not leak memory
		{
			for (size_t i=0; i<m_data.vecBounds[1].cElements; i++)
			{
				delete[]  m_data.pvDataArray[i];
			}
			delete[] m_data.pvDataArray;
			m_data.pvDataArray = nullptr;
		}

		//memory size in one dimension
		_UINT32 i32mem = m_data.cbElements * m_data.vecBounds[0].cElements;

		if ((m_data.varType == RUL_CHAR) && ((m_i32mem == 0) || ((_UINT32)m_i32mem > i32mem)))
		{
			//extra byte for null termination
			i32mem += 1;
		}

		if (i32Size == 1)
		{
			//one dimension
			if (i32mem > 0)
			{
				m_data.pvData = new _UCHAR[i32mem];
				memset(m_data.pvData, 0, i32mem);
			}

			m_i32mem = i32mem;
		}
		else if (i32Size == 2)
		{
			//two dimensions
			m_data.pvDataArray = new _UCHAR*[m_data.vecBounds[1].cElements];

			m_i32mem = 0;
			if (i32mem > 0)
			{
				for (size_t i = 0; i<m_data.vecBounds[1].cElements; i++)
				{
					m_data.pvDataArray[i] = new _UCHAR[i32mem];
					memset(m_data.pvDataArray[i], 0, i32mem);

					m_i32mem += i32mem;
				}
			}
		}
	}
	return 1;
}

void INTER_SAFEARRAY::makeEmpty()
{
	size_t i32Size = m_data.vecBounds.size();
	if( (m_data.cDims > 0) && (i32Size > 0) )
	{
		_INT32 i32mem = m_data.cbElements;
		for(size_t i=0; i<i32Size; i++)
		{
			i32mem *= m_data.vecBounds[i].cElements;
		}

		if (m_data.varType == RUL_CHAR)
		{
			assert(m_i32mem == (i32mem + 1));
		}
		else
		{
			assert(m_i32mem == i32mem);
		}
		memset(m_data.pvData,0,i32mem);
	}
}


_INT32	INTER_SAFEARRAY::GetElement(_INT32 i32Idx, INTER_VARIANT* pvar)
{// set return pvar value from array data                       
	pvar->SetValue(((_UCHAR*)(m_data.pvData))+i32Idx,m_data.varType);
	return 0;
}

_INT32	INTER_SAFEARRAY::GetDimension(_UINT32 u32Dim, INTER_VARIANT* pvar)
{
	// set return pvar value from array data in the specific dimension
	const auto rows = m_data.vecBounds[1].cElements;	//number of data array rows
	assert(u32Dim < rows);

	pvar->SetValue(((_UCHAR*)(m_data.pvDataArray[u32Dim])), RUL_SAFEARRAY);
	return 0;
}
_INT32	INTER_SAFEARRAY::SetElement(_INT32 i32Idx,INTER_VARIANT* pvar)
{// get pvar value into array data
	pvar->GetValue(((_UCHAR*)(m_data.pvData))+i32Idx,m_data.varType);
	return 0;
}

_INT32	INTER_SAFEARRAY::SetDimension(_UINT32 u32Dim, INTER_VARIANT* pvar)
{
	// get pvar value into array data in the specific dimension
	const auto rows = m_data.vecBounds[1].cElements;	//number of data array rows
	assert(u32Dim < rows);

	wchar_t *wstr = NULL;
	pvar->GetStringValue(&wstr, false);
	if (wstr)
	{
		ExpandString(wstr, u32Dim);
		delete[] wstr;
	}

	return 0;
}

//This file is used for TraceDump() only
_INT32 INTER_SAFEARRAY::XMLize(_CHAR* szData, size_t dataLen)
{
	INTER_VARIANT temp;

	switch (m_data.varType)
	{
		//case RUL_CHAR:
		//{
		//	string  Str;
		//	for (int i = 0; i < m_i32mem; i += m_data.cbElements)
		//	{
		//		GetElement(i, &temp);
		//		Str += ((char)temp);
		//		temp.Clear();
		//	}
		//	PS_Strncat(szData, dataLen, Str.c_str(), _TRUNCATE);
		//}
		//break;
		case RUL_USHORT:
		{
			//This maybe a local method variable in DD_STRING type
			wstring wStr;
			if (m_data.cDims == 1)
			{
				//one dimension
				for (int i = 0; i < m_i32mem; i += m_data.cbElements)
				{
					GetElement(i, &temp);
					wStr += ((wchar_t)temp);
					temp.Clear();
				}
			}
			else
			{
				//multiple dimensions
				_UINT32 rows = m_data.vecBounds[1].cElements;
				for (_UINT32 i = 0; i < rows; i++)
				{
					wStr += reinterpret_cast<wchar_t *>(m_data.pvDataArray[i]);
					if (i < rows - 1)
					{
						wStr += L", ";
					}
				}
			}

			string Str = TStr2AStr(wStr);
			PS_Strncat(szData, dataLen, Str.c_str(), _TRUNCATE);
		}
		break;
		default:
		{
			_CHAR c_temp[16];
			_INT32 j = 0, iElemNum = m_i32mem;

			if (m_data.varType == RUL_CHAR)
			{
				iElemNum = m_i32mem - 1;	//get rid of null terminator
			}

			for (_INT32 i = 0; i < iElemNum; i += m_data.cbElements)
			{
				temp.SetValue(((_UCHAR*)(m_data.pvData)) + i, m_data.varType);
				sprintf(c_temp, "[%d]", j);
				PS_Strncat(szData, dataLen, c_temp, _TRUNCATE);
				temp.XMLize(szData, dataLen);
				j++;
			}
		}
		break;
	}
	return 0;
}

//This file is used for TraceDump() only
_INT32 INTER_SAFEARRAY::XMLizeForMethodDebugInfo(_CHAR* szData, size_t dataLen)
{
	INTER_VARIANT temp;
	_CHAR c_temp[100];

	switch (m_data.varType)
	{
	
	case RUL_USHORT:
	{
		//This maybe a local method variable in DD_STRING type
		sprintf(c_temp, " type=\"dd_string\" array_size=\"0\"");
		PS_Strncat(szData, dataLen, c_temp, _TRUNCATE);
		wstring wStr;
		if (m_data.cDims == 1)
		{
			//one dimension
			for (int i = 0; i < m_i32mem; i += m_data.cbElements)
			{
				GetElement(i, &temp);
				wStr += ((wchar_t)temp);
				temp.Clear();
			}
		}
		else
		{
			//multiple dimensions
			_UINT32 rows = m_data.vecBounds[1].cElements;
			for (_UINT32 i = 0; i < rows; i++)
			{
				wStr += reinterpret_cast<wchar_t *>(m_data.pvDataArray[i]);
				if (i < rows - 1)
				{
					wStr += L", ";
				}
			}
		}

		string Str = TStr2AStr(wStr);
		PS_Strncat(szData, dataLen, " value=\"", _TRUNCATE);
		PS_Strncat(szData, dataLen, Str.c_str(), _TRUNCATE);
		PS_Strncat(szData, dataLen, "\"/>", _TRUNCATE);
	}
	break;
	default:
	{
		switch (m_data.varType)
		{
		case RUL_BOOL:
			PS_Strncat(szData, dataLen, " type=\"bool\"", _TRUNCATE);
			break;
		case RUL_CHAR:
			PS_Strncat(szData, dataLen, " type=\"char\"", _TRUNCATE);
			break;
		case RUL_UNSIGNED_CHAR:
			PS_Strncat(szData, dataLen, " type=\"unsigned char\"", _TRUNCATE);
			break;
		case RUL_INT:
			PS_Strncat(szData, dataLen, " type=\"int\"", _TRUNCATE);
			break;
		case RUL_UINT:
			PS_Strncat(szData, dataLen, " type=\"unsigned int\"", _TRUNCATE);
			break;
		case RUL_SHORT:
			PS_Strncat(szData, dataLen, " type=\"short\"", _TRUNCATE);
			break;
		case RUL_USHORT:
			PS_Strncat(szData, dataLen, " type=\"unsigned short\"", _TRUNCATE);
			break;
		case RUL_LONGLONG:
			PS_Strncat(szData, dataLen, " type=\"long long\"", _TRUNCATE);
			break;
		case RUL_ULONGLONG:
			PS_Strncat(szData, dataLen, " type=\"unsigned long long\"", _TRUNCATE);
			break;
		case RUL_FLOAT:
			PS_Strncat(szData, dataLen, " type=\"float\"", _TRUNCATE);
			break;
		case RUL_DOUBLE:
			PS_Strncat(szData, dataLen, " type=\"double\"", _TRUNCATE);
			break;
		case RUL_CHARPTR:
			PS_Strncat(szData, dataLen, " type=\"charptr\"", _TRUNCATE);
			break;
		case RUL_WIDECHARPTR:
			PS_Strncat(szData, dataLen, " type=\"wide char ptr\"", _TRUNCATE);
			break;
		case RUL_BYTE_STRING:
			PS_Strncat(szData, dataLen, " type=\"bytestring\"", _TRUNCATE);
			break;
		case RUL_DD_STRING:
			PS_Strncat(szData, dataLen, " type=\"ddstring\"", _TRUNCATE);
			break;
		case RUL_SAFEARRAY:
			PS_Strncat(szData, dataLen, " type=\"safearray\"", _TRUNCATE);
			break;
		}
		
		
		_INT32 j = 0;
		sprintf(c_temp, " array_size=\"%d\">", GetNumberOfElements());
		PS_Strncat(szData, dataLen, c_temp, _TRUNCATE);
		
		for (_INT32 i = 0; i < m_i32mem; i += m_data.cbElements)
		{
			temp.SetValue(((_UCHAR*)(m_data.pvData)) + i, m_data.varType);
			sprintf(c_temp, "<Item id=\"%d\"", j);
			PS_Strncat(szData, dataLen, c_temp, _TRUNCATE);
			temp.XMLizeForMethodDebug(szData, dataLen, true);
			j++;
		}
	}
	break;
	}
	return 0;
}