#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestVariablePREActions
        {
        public:
            static FLAT_VAR* test_var_pre_action;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static  EddEngineController^  engine;
            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();                

                FDI_PARAM_SPECIFIER test_var_pre_specifier;
                engine->FillParamSpecifier("var_one_pre_read_write_action", &test_var_pre_specifier);
                genericItem = engine->GetGenericItem(test_var_pre_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                test_var_pre_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                int result =  engine->Instance->GetItemIdFromSymbolName(L"add_100",test_method_item_ID);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_defined_label()
            {
                Assert::AreEqual("", gcnew String(test_var_pre_action->label.c_str()) );
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_pre_action->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_pre_action->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_metod()
            {
                Assert::AreEqual(*test_method_item_ID, test_var_pre_action->pre_read_act.list->action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_pre_action->pre_read_act.list->eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_pre_action->pre_write_act.list->eType);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestVariablePOSTActions
        {
        public:
            static FLAT_VAR* test_var_post_action;
            static ITEM_ID* test_method_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static  EddEngineController^  engine;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_var_post_specifier;
                engine->FillParamSpecifier("var_one_post_read_write_action", &test_var_post_specifier);
                genericItem = engine->GetGenericItem(test_var_post_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                test_var_post_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_item_ID = new ITEM_ID();
                int result =  engine->Instance->GetItemIdFromSymbolName(L"add_100",test_method_item_ID);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_method_item_ID;
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_post_action->post_read_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(1u, (UINT32)test_var_post_action->post_write_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_metod()
            {
                Assert::AreEqual(*test_method_item_ID, test_var_post_action->post_read_act.list->action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_post_action->post_read_act.list->eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_post_action->post_write_act.list->eType);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestVariableMultipleActions
        {
        public:
            static FLAT_VAR* test_var_multiple_action;
            static ITEM_ID* test_method_add_100_item_ID;
            static ITEM_ID* test_method_add_1000_item_ID;
            static ITEM_ID* test_method_remove_100_item_ID;
            static ITEM_ID* test_method_remove_1000_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static EddEngineController^  engine;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();                

                FDI_PARAM_SPECIFIER test_var_multiple_specifier;
                engine->FillParamSpecifier("var_with_multiple_actions", &test_var_multiple_specifier);
                genericItem = engine->GetGenericItem(test_var_multiple_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                test_var_multiple_action = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_add_100_item_ID = new ITEM_ID();
                test_method_add_1000_item_ID = new ITEM_ID();
                test_method_remove_100_item_ID = new ITEM_ID();
                test_method_remove_1000_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100", test_method_add_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"add_1000", test_method_add_1000_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_100", test_method_remove_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_1000", test_method_remove_1000_item_ID);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_method_add_100_item_ID;
                delete test_method_add_1000_item_ID;
                delete test_method_remove_100_item_ID;
                delete test_method_remove_1000_item_ID;
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->post_read_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)test_var_multiple_action->post_write_act.count);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, test_var_multiple_action->pre_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, test_var_multiple_action->pre_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, test_var_multiple_action->post_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, test_var_multiple_action->post_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, test_var_multiple_action->pre_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, test_var_multiple_action->pre_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, test_var_multiple_action->post_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, test_var_multiple_action->post_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariablePreActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->pre_write_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariablePostActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)test_var_multiple_action->post_write_act.list[1].eType);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestVariableMultipleEditActions
        {
        public:
            static FLAT_VAR* var_with_multiple_actions;
            static ITEM_ID* test_method_add_100_item_ID;
            static ITEM_ID* test_method_add_1000_item_ID;
            static ITEM_ID* test_method_remove_100_item_ID;
            static ITEM_ID* test_method_remove_1000_item_ID;
            static  FDI_GENERIC_ITEM* genericItem ;
            static  EddEngineController^  engine;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                FDI_PARAM_SPECIFIER test_var_multiple_specifier;
                engine->FillParamSpecifier("var_with_multiple_actions", &test_var_multiple_specifier);
                genericItem = engine->GetGenericItem(test_var_multiple_specifier.id, nsEDDEngine::ITYPE_VARIABLE);
                var_with_multiple_actions = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_method_add_100_item_ID = new ITEM_ID();
                test_method_add_1000_item_ID = new ITEM_ID();
                test_method_remove_100_item_ID = new ITEM_ID();
                test_method_remove_1000_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"add_100", test_method_add_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"add_1000", test_method_add_1000_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_100", test_method_remove_100_item_ID);
                engine->Instance->GetItemIdFromSymbolName(L"remove_1000", test_method_remove_1000_item_ID);
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete engine;
                delete test_method_add_100_item_ID;
                delete test_method_add_1000_item_ID;
                delete test_method_remove_100_item_ID;
                delete test_method_remove_1000_item_ID;
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_edit_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_edit_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_edit_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_edit_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_edit_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_edit_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_edit_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_edit_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_edit_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_edit_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_edit_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_edit_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_edit_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_edit_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_edit_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_edit_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_edit_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_read_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_read_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_read_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_pre_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->pre_write_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_number_of_defined_post_write_actions()
            {
                Assert::AreEqual(2u, (UINT32)var_with_multiple_actions->post_write_act.count);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_read_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_read_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_read_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_read_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_100_item_ID, var_with_multiple_actions->pre_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_pre_write_method()
            {
                Assert::AreEqual(*test_method_add_1000_item_ID, var_with_multiple_actions->pre_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_first_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_100_item_ID, var_with_multiple_actions->post_write_act.list[0].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_ItemID_for_the_second_post_write_method()
            {
                Assert::AreEqual(*test_method_remove_1000_item_ID, var_with_multiple_actions->post_write_act.list[1].action.meth_ref);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_read_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_read_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_read_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_first_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_write_act.list[0].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_pre_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->pre_write_act.list[1].eType);
            }

            [TestMethod, TestCategory("VariableEditActionTest")]
            void It_should_have_the_correct_action_type_for_the_second_post_write_action()
            {
                //  ACTION_TYPE_NONE			= 0,
                //  ACTION_TYPE_REFERENCE		= 1,
                //  ACTION_TYPE_REF_WITH_ARGS	= 2,
                //  ACTION_TYPE_DEFINITION		= 3
                Assert::AreEqual(1, (int)var_with_multiple_actions->post_write_act.list[1].eType);
            }
        };

        // WORKAROUND: use TestInitalize 
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with 7, but every test use only 1)

        [TestClass]
        public ref class When_requesting_a_variable_with_pre_actions_with_FM8 : BaseTestVariablePREActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_pre_actions_with_FMA : BaseTestVariablePREActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_post_actions_with_FM8 : BaseTestVariablePOSTActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_post_actions_with_FMA : BaseTestVariablePOSTActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_actions_with_FM8 : BaseTestVariableMultipleActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_actions_with_FMA : BaseTestVariableMultipleActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0015.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }    
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_edit_actions_with_FM8 : BaseTestVariableMultipleEditActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0016.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_variable_with_multiple_edit_actions_with_FMA : BaseTestVariableMultipleEditActions
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0016.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
