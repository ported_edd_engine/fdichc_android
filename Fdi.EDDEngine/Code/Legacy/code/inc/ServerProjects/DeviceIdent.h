
#pragma once


typedef BYTE	LONG_ADDRESS[5];
typedef int		NETWORK_HANDLE;


typedef struct {
	UINT32			mfrid;
	UINT16			mfr_dev_type;
	UINT8			req_preambles;
	UINT8			uni_cmd_rev;
	UINT8			xmtr_spec_rev;
	UINT8			soft_rev;
	UINT8			hard_rev;
	UINT8			flags;
	UINT8			signal_code;
	UINT8			device_id[3];
	UINT8			nResponsePreambles;
	UINT8			nMaxVars;
	UINT32			ePrivateLabelDistributorCode;
	UINT8			eDeviceProfile;
} HART_IDENTIFY_CNF;

