/*************************************************************************************************
 *
 * $Workfile: ddbItemLists.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the All the item lists 
 *	
 *		7/25/2	sjv	creation
*/

#include "stdafx.h"
#include "ddbVar.h"
#include "ddbCmdList.h"


hCcommand* CCmdList::getCmdByNumber(int cmdNumber)// all commands
{
	hCcommand* pCRet = NULL;
	for ( vector<hCcommand*>::iterator iT = begin(); iT < end(); iT++)
	{//iT is a ptr 2 a ptr 2 a hCcommand
		if ( (*iT)->getCmdNumber() == cmdNumber )
		{
			pCRet = (*iT);
			break; // out of for loop
		}
	}
	return pCRet;
}
