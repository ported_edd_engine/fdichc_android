// EDDMethods.h : Declaration of the CEDDMethods

#pragma once
#include "resource.h"       // main symbols

#include "EDDEngineCOM_i.h"
#include "_IEDDMethodsEvents_CP.h"

#include "nsEDDEngine/IEDDEngine.h"
#include "Consumer.h"
#include "EDDEngineLogger.h"

#ifndef _CRTDBG_MAP_ALLOC
//#define _CRTDBG_MAP_ALLOC
#endif // !_CRTDBG_MAP_ALLOC

#ifdef _WIN32_WCE
#error "Neutral-threaded COM objects are not supported on Windows CE."
#endif

using namespace ATL;
using namespace nsEDDEngine;

typedef CTypedPtrList<CPtrList, nsEDDEngine::IAsyncResult*>  CIAsyncList;

// CEDDMethods

class ATL_NO_VTABLE CEDDMethods :
	public IAsyncCallbackHandler,
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CEDDMethods, &CLSID_EDDMethods>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CEDDMethods>,
	public CProxy_IEDDMethodsEvents<CEDDMethods>,
	public IDispatchImpl<IEDDMethods, &IID_IEDDMethods, &LIBID_EDDEngineCOMLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CEDDMethods()
	{
		//Note on using the _CrtSetBreakAlloc to solve memory leaks
		//Put the number inside the curly braces inside of the
		//_CrtBreakAlloc call.  The code will then break where the leaked memory is instantiated.

		//Detected memory leaks!
		//Dumping objects ->
		//{442632} normal block at 0x0DB690F8, 5 bytes long.
		// Data: <case > 63 61 73 65 00 
		//{442631} normal block at 0x0DB69808, 76 bytes long.
		// Data: <  n     J       > A0 05 6E 06 01 00 00 00 4A 00 00 00 F8 90 B6 0D 
		//Object dump complete.		
		//		
		
		//_CrtSetDbgFlag ( _CRTDBG_LEAK_CHECK_DF );
		//_CrtSetBreakAlloc(68402);
		//_CrtDumpMemoryLeaks();
		//initilaize variable pointers
		pDeviceInstance = nullptr;
		m_pMIconsumer = nullptr;
		pEDDEngineLogger = nullptr;

		m_iBlockInstance = 0;
		m_protocol = 1;		//ProtocolType::HART
		InitializeCriticalSection(&cs_EDDMethCriticalSection);
		m_bIsActionMethod = false;
		m_bWithCallbackMethod = false;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_EDDMETHODS)


BEGIN_COM_MAP(CEDDMethods)
	COM_INTERFACE_ENTRY(IEDDMethods)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CEDDMethods)
	CONNECTION_POINT_ENTRY(__uuidof(_IEDDMethodsEvents))
END_CONNECTION_POINT_MAP()
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		if (pDeviceInstance != nullptr)
		{
			delete pDeviceInstance;
			pDeviceInstance = nullptr;
		}
		if (m_pMIconsumer != nullptr)
		{
			delete m_pMIconsumer;
			m_pMIconsumer = nullptr;
		}

		if (pEDDEngineLogger != nullptr)
		{
			delete pEDDEngineLogger;
			pEDDEngineLogger = nullptr;
		}

		if (!lClientAsyncResult.IsEmpty())
		{
			lClientAsyncResult.RemoveAll();
		}
		DeleteCriticalSection(&cs_EDDMethCriticalSection);
	}

private:
	nsConsumer::Consumer *m_pMIconsumer;			//as long life as the UI window
	nsEDDEngine::IEDDEngine *pDeviceInstance;	//as long life as the UI window
	static CIAsyncList lClientAsyncResult;		//as long life as one method execution; deleted in server
	static CRITICAL_SECTION cs_EDDMethCriticalSection;	//this variable is used to protect lClientAsyncResult ownership access between threads
	int m_iBlockInstance;						// iBlockInstance to use for all calls
	SHORT m_protocol;							// protocol identifier
	nsConsumer::EVAL_VAR_VALUE returnedValue;	//used for action method
	bool m_bIsActionMethod;						//used for action method
	bool m_bWithCallbackMethod;					//used for method with callback
	//nsConsumer::IEDDEngineLogger* m_IEDDEngineLogger;
	CEDDEngineLogger* pEDDEngineLogger; 

	int GetFFBlockInstance( wchar_t* pBlockName, int* pIBlockInstance );
	void AsyncCallback(nsEDDEngine::IAsyncResult* ar);
	void getMethodParams(/* in */ unsigned long ulMethID, /* in */ size_t uiLen, /* out */ wchar_t *pMethodParams);

public:
	STDMETHOD(BeginMethodInterp)(BSTR sMethodName, BOOLEAN bUseCallback, INT* error);
	STDMETHOD(EndMethodInterp)(INT* error);
	STDMETHOD(CancelMethodInterp)(void);
    STDMETHOD(isMethodCompleted)(BOOLEAN* result);
    STDMETHOD(validMethodState)(BOOLEAN* result);
    STDMETHOD(waitMethodCompleted)(void);
	STDMETHOD(GetUIPromptNOption)(BSTR* wsText, VARIANT* vOption, unsigned long* winType);
	STDMETHOD(SetUIUserInputNButton)(VARIANT* vUserText, unsigned long currentSel, BOOLEAN bUserButton);
	STDMETHOD(CreateDeviceInstanceNLoadDD)(BSTR sDDName, SHORT protocol, BSTR sFFBlockName, VARIANT* vMethodList, INT* error);
	STDMETHOD(LoadActionList(BSTR sItemID, VARIANT* vActionList, INT* error));
	STDMETHOD(BeginVarActionMethodInterp)(BSTR sVarItemID, INT iActionIndex, BOOLEAN bUseCallback, INT* error);
	STDMETHOD(waitForTestToComplete)(void);

    int ReadConfigXML(wchar_t* configXML, int configXMLLen);

};

OBJECT_ENTRY_AUTO(__uuidof(EDDMethods), CEDDMethods)
