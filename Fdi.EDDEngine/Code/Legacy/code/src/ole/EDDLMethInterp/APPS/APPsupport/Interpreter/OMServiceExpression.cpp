// OMServiceExpression.cpp //

#include "stdafx.h"

/* comutil.h uses Bill's TRUE/FALSE that the general erroneously defines on purpose */
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

#include "OMServiceExpression.h"
#include "GrammarNodeVisitor.h"


COMServiceExpression::COMServiceExpression()
{
	m_pucObjectName		=	0;
	m_i32constant_pool_idx = -1;
}

COMServiceExpression::~COMServiceExpression()
{
	if(m_pucObjectName)
	{
		delete m_pucObjectName;
		m_pucObjectName = 0;
	}
	m_i32constant_pool_idx = -1;
}

_INT32 COMServiceExpression::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitOMExpression(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

//	OM Service Expression is of the form
//	OM Service Expression	=>	<ObjectManager> <::> <Point_Address> <;>|<Operator>
//	Point_Address			=>	<Object_Name><Attribute_List>
//	Attribute_List			=>	<.><Attribute><Attribute_List> | <.><Attribute>
//	Attribute				=>	<Identifier>


_UCHAR* COMServiceExpression::GetObjectName()
{
	return m_pucObjectName;
}

_UCHAR* COMServiceExpression::GetAttributeName(
			_INT32 i32Idx)
{
	return m_attribList[i32Idx].uchAttribName;
}


_INT32 OIDConvertIntToChar(
			_INT32 i32OID, 
			_CHAR* chOID)
{
	union unionOID
	{
		_CHAR				chOID[5];
		struct strOID
		{
			_INT32			nOID;
			_UCHAR			chOID;
		}sOID;
	};
	unionOID uOID;
	uOID.sOID.nOID	=	i32OID;
	uOID.sOID.chOID	=	0;
	memcpy(chOID,uOID.chOID,5);

	return 0;
}


_INT32 COMServiceExpression::GetConstantPoolIdx()
{
	return m_i32constant_pool_idx;
}

_INT32 COMServiceExpression::GetLineNumber()
{
	return -1;
}
