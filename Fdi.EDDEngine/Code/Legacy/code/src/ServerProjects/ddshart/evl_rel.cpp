/**
 *	@(#) $Id: evl_rel.c,v 1.2 1996/01/04 20:12:40 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  RELATION.
 */

#include "stdinc.h"

#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif



/***********************************************************************
 *
 * Name: ddl_free_refresh
 *
 * ShortDesc: free a REFRESH_RELATION value structure
 *
 * Description: ddl_free_refresh will free all memory allocated to the
 *	the reflists in the REFRESH_RELATION value structure
 *
 * Inputs: refresh - a pointer to the REFRESH_RELATION structure
 *		   dest_flat - flag which designates whether or not to clean or
 *		   free the structure
 *
 * Outputs: refresh - the pointer to the freed REFRESH_RELATION structure
 *
 * Returns: DDL_SUCCESS and returns from ddl_free_reflist()
 *
 * Author: Chris Gustafson
 *
 **********************************************************************/

void
ddl_free_refresh(
REFRESH_RELATION *refresh,
uchar           dest_flag)
{

	if (refresh) {
		ddl_free_op_ref_trail_list(&refresh->depend_items, dest_flag);
		ddl_free_op_ref_trail_list(&refresh->update_items, dest_flag);
	}

	return;
}



/***********************************************************************
 *
 * Name: ddl_shrink_refresh
 *
 * ShortDesc: shrink a REFRESH_RELATION value structure
 *
 * Description: ddl_shrink_refresh will resize all memory allocated to the
 *	the reflists in the REFRESH_RELATION value structure
 *
 * Inputs: refresh - a pointer to the REFRESH_RELATION structure
 *
 * Outputs: refresh - the pointer to the resized REFRESH_RELATION structure
 *
 * Returns: DDL_SUCCESS or returns from ddl_shrink_reflist()
 *
 * Author: Chris Gustafson
 *
 **********************************************************************/

static int
ddl_shrink_refresh(
REFRESH_RELATION *refresh)
{
	int             rc;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_REFRESH);
#endif

	if (!refresh) {
		return DDL_SUCCESS;
	}

	rc = ddl_shrink_op_ref_trail_list(&refresh->depend_items);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_op_ref_trail_list(&refresh->update_items);
	}
	return rc;
}



/***************************************************************************
 *
 *	Name: ddl_refresh_choice
 *	ShortDesc: Choose the correct refresh items from a binary.
 *
 *	Description:
 *		ddl_refresh_choice calls reflist_choice for the two reflists
 *		contained by a REFRESH_RELATION
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		refresh - pointer to a REFRESH_RELATION where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		refresh - pointer to an REFRESH_RELATION structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_reflist_choice().
 *
 *	Author:
 *		Steve Beyerl
 **************************************************************************/

static int
ddl_refresh_choice(
unsigned char **chunkp,
DDL_UINT       *size,
REFRESH_RELATION *refresh,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	OP_REF_TRAIL_LIST *list;/* temp pointer for the trail list */
	int             local_data_valid1;	/* temp data valid flag */
	int             local_data_valid2;	/* temp data valid flag */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_REFRESH_CHOICE);
#endif

	if (refresh) {
		refresh->depend_items.count = 0;
		list = &refresh->depend_items;
	}
	else {
		list = NULL;
	}

	rc = ddl_op_ref_trail_list_choice(chunkp, size, list, depinfo, &local_data_valid1, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (refresh) {
		refresh->update_items.count = 0;
		list = &refresh->update_items;
	}
	else {
		list = NULL;
	}

	rc = ddl_op_ref_trail_list_choice(chunkp, size, list, depinfo, &local_data_valid2, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (local_data_valid1 || local_data_valid2) {
		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}


/***********************************************************************
 *
 * Name: eval_attr_refresh
 *
 * ShortDesc: evaluate a refresh relation
 *
 * Description:
 *
 *	The eval_attr_refresh function evaluates a refresh relation.
 *	The buffer pointed to by chunk
 *	should contain a refresh relation returned from fetch_refresh,
 *	and size should specify its size.
 *	If refresh is not a null pointer, the refresh relation
 *	is returned in refresh. If depinfo is not
 *	a null pointer, dependency information about the refresh relation
 *	is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		refresh - pointer to a REFRESH_RELATION where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		refresh - pointer to an REFRESH_RELATION structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_refresh_choice()
 *
 * Author: Chris Gustafson
 *
 ***************************************************************************/

int
eval_attr_refresh(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	REFRESH_RELATION* refresh = static_cast<REFRESH_RELATION*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* flags if return data is valid */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_REFRESH);
#endif

	valid = 0;

	ddl_free_refresh(refresh, CLEAN_ATTR);

	if (depinfo) {

		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_refresh_choice(&chunk, &size, refresh, depinfo, &valid, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_refresh(refresh, FREE_ATTR);
		return rc;
	}
	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {

		rc = ddl_shrink_refresh(refresh);
	}
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_refresh(refresh, FREE_ATTR);
		return rc;
	}
	if (refresh && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}



/***********************************************************************
 *
 * Name: ddl_free_unit
 *
 * ShortDesc: free a UNIT_RELATION value structure
 *
 * Description: ddl_free_unit will free all memory allocated to the
 *	the var and the var_units in the UNIT_RELATION value structure
 *
 * Inputs: unit - a pointer to the UNIT_RELATION structure
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 * Outputs: unit - the pointer to the freed UNIT_RELATION structure
 *
 * Returns: DDL_SUCCESS and returns from ddl_free_reflist()
 *
 * Author: Chris Gustafson
 *
 **********************************************************************/

void
ddl_free_unit(
UNIT_RELATION  *unit,
uchar           dest_flag)
{

	if (unit) {
		ddl_free_op_ref_trail(&unit->var);
		ddl_free_op_ref_trail_list(&unit->var_units, dest_flag);
	}

	return;
}


/***********************************************************************
 *
 * Name: ddl_shrink_unit
 *
 * ShortDesc: shrink a UNIT_RELATION value structure
 *
 * Description: ddl_shrink_unit will resize all memory allocated to the
 *	the var_units in the UNIT_RELATION value structure
 *
 * Inputs: unit - a pointer to the UNIT structure
 *
 * Outputs: unit - the pointer to the resized UNIT_RELATION structure
 *
 * Returns: DDL_SUCCESS or returns from ddl_shrink_reflist()
 *
 * Author: Chris Gustafson
 *
 **********************************************************************/

static int
ddl_shrink_unit(
UNIT_RELATION  *unit)
{
	int             rc;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_UNIT);
#endif

	if (!unit) {
		return DDL_SUCCESS;
	}

	rc = ddl_shrink_op_ref_trail_list(&unit->var_units);

	return rc;
}


/**********************************************************************
 *
 *	Name: ddl_unit_choice
 *	ShortDesc: Choose the correct unit items from a binary.
 *
 *	Description:
 *		ddl_unit_choice calls reflist_choice for the two reflists
 *		contained by a UNIT_RELATION
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		unit - pointer to a UNIT_RELATION where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		unit - pointer to an UNIT_RELATION structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_reflist_choice().
 *
 *	Author:
 *		Steve Beyerl
 *********************************************************************/


static int
ddl_unit_choice(
unsigned char **chunkp,
DDL_UINT       *size,
UNIT_RELATION  *unit,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc;	/* return code */
	int             local_data_valid1;	/* temp data valid flag */
	int             local_data_valid2;	/* temp data valid flag */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_UNIT_CHOICE);
#endif


	rc = ddl_op_ref_trail_choice(chunkp, size, &unit->var, depinfo, &local_data_valid1, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	rc = ddl_op_ref_trail_list_choice(chunkp, size, &unit->var_units, depinfo, &local_data_valid2, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (local_data_valid1 || local_data_valid2) {
		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}

/****************************************************************
 *
 * Name: eval_attr_unit
 *
 * ShortDesc: evaluate a unit relation
 *
 * Description:
 *
 *	The eval_attr_unit function evaluates a unit relation.
 *	The buffer pointed to by chunk
 *	should contain a unit relation returned from fetch_unit,
 *	and size should specify its size.
 *	If unit is not a null pointer, the unit relation
 *	is returned in unit. If depinfo is not
 *	a null pointer, dependency information about the unit relation
 *	is returned in depinfo.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		unit - pointer to a UNIT_RELATION where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		unit - pointer to an UNIT_RELATION structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 * Returns:
 *	DDL_SUCCESS, DDL_DEFAULT_ATTR
 *	return codes from ddl_unit_choice()
 *
 * Author: Chris Gustafson
 *
 ******************************************************************/

int
eval_attr_unit(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	UNIT_RELATION*			unit = static_cast<UNIT_RELATION*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* flags if return data is valid */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_UNIT);
#endif

	valid = 0;

	ddl_free_unit(unit, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_unit_choice(&chunk, &size, unit, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_unit(unit, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_unit(unit);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_unit(unit, FREE_ATTR);
		return rc;
	}

	if (unit && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: link_refresh()
 *	ShortDesc: collect dependencies from refresh relations for the HART linker
 *
 *	Author: Christian Gustafson
 *
 ***********************************************************************/

int
link_refresh(
unsigned char *chunk,
DDL_UINT       size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc = DDL_SUCCESS;                   /* return code */
	CHUNK_LIST      chunk_list_ptr;	                    /* ptr to a list of binaries */
	CHUNK          *chunk_ptr;	                        /* temp pointer for chunk list */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];/* list of binaries */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);


	depinfo->count = 0;	/* reset the depinfo list count to zero */

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);


	rc = link_cond_list(&chunk, &size, &chunk_list_ptr, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * If a list of chunks exists and the
	 * link_attr() function is not NULL,
     * call it to accumulate additional linker dependencies
	 */

	if (chunk_list_ptr.size > 0) {

		chunk_ptr = chunk_list_ptr.list;

		while (chunk_list_ptr.size > 0) {	/* Parse them */

			rc = link_reflist (&(chunk_ptr->chunk),
				&(chunk_ptr->size),
				depinfo,
				env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}
	}

	chunk_list_ptr.size = 0;  /* reset the chunk list counter */


	rc = link_cond_list(&chunk, &size, &chunk_list_ptr, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}
	if (chunk_list_ptr.size > 0) {

		chunk_ptr = chunk_list_ptr.list;

		while (chunk_list_ptr.size > 0) {	/* Parse them */

			rc = link_reflist (&(chunk_ptr->chunk),
				&(chunk_ptr->size),
				depinfo,
				env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}
	}


err_exit:

	/* delete chunk list if it was reallocated*/

	if (chunk_list_ptr.list != chunk_list) {

		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	if (rc != DDL_SUCCESS) {

		depinfo->count = 0;
	}

	return rc;

}


/*********************************************************************
 *
 *	Name: link_unit()
 *	ShortDesc: collect dependencies from unit relations for the HART linker
 *
 *	Author: Christian Gustafson
 *
 ***********************************************************************/

int
link_unit(
unsigned char  *chunk,
DDL_UINT        size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc = DDL_SUCCESS;                   /* return code */
	CHUNK_LIST      chunk_list_ptr;	                    /* ptr to a list of binaries */
	CHUNK          *chunk_ptr;	                        /* temp pointer for chunk list */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];/* list of binaries */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);


	depinfo->count = 0;	/* reset the depinfo list count to zero */

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);


	rc = link_cond(&chunk, &size, &chunk_list_ptr, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * If a list of chunks exists and the
	 * link_attr() function is not NULL,
     * call it to accumulate additional linker dependencies
	 */

	if (chunk_list_ptr.size > 0) {

		chunk_ptr = chunk_list_ptr.list;

		while (chunk_list_ptr.size > 0) {	/* Parse them */

			rc = link_ref (&(chunk_ptr->chunk),
				&(chunk_ptr->size),
				depinfo,
				env_info,
				var_needed, (DDL_UINT) 0);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}
	}

	chunk_list_ptr.size = 0;  /* reset the chunk list counter */


	rc = link_cond_list(&chunk, &size, &chunk_list_ptr, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}
	if (chunk_list_ptr.size > 0) {

		chunk_ptr = chunk_list_ptr.list;

		while (chunk_list_ptr.size > 0) {	/* Parse them */

			rc = link_reflist (&(chunk_ptr->chunk),
				&(chunk_ptr->size),
				depinfo,
				env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}
	}


err_exit:

	/* delete chunk list if it was reallocated*/

	if (chunk_list_ptr.list != chunk_list) {

		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	if (rc != DDL_SUCCESS) {

		depinfo->count = 0;
	}

	return rc;

}

