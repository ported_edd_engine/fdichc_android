#ifndef _TESTSUPPORT_PARAMETERACCESSMETHODHELPER_H_
#define _TESTSUPPORT_PARAMETERACCESSMETHODHELPER_H_

#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"
#include "TestSupport/MockRecorder.h"
#include "TestSupport/MethodHelper.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            [System::Runtime::InteropServices::UnmanagedFunctionPointerAttribute(System::Runtime::InteropServices::CallingConvention::Cdecl)]
            public delegate void ScalingCallback(nsEDDEngine::IAsyncResult* result);

            public ref class ParameterAccessMethodHelper
            {
            private:
                EddEngineController^ _engine;
                BuiltinSupportSimulator* _builtinSupportSimulator;
                bool _wasAsyncCalled;
                nsEDDEngine::Mth_ErrorCode _methodResult;

                ParameterAccessMethodHelper(System::String^ eddFileName);
                ~ParameterAccessMethodHelper();
                
                static void MyMethodCallback(nsEDDEngine::IAsyncResult* asyncResult);
                static void MyScalingMethodCallback(nsEDDEngine::IAsyncResult* asyncResult);

            public:
                static ParameterAccessMethodHelper^ LoadEdd(System::String^ eddFile);

                MockRecorder* GetBuildinRecorder();

                ParameterAccessMethodHelper^ RegisterGetUnscaledValue(nsConsumer::EVAL_VAR_VALUE* returnValue);
                ParameterAccessMethodHelper^ RegisterGetUnscaledValue(nsConsumer::EVAL_VAR_VALUE* returnValue, nsConsumer::PC_ErrorCode* errorCode);
                ParameterAccessMethodHelper^ RegisterSetUnscaledValue();
                ParameterAccessMethodHelper^ RegisterSetUnscaledValue(nsConsumer::PC_ErrorCode* errorCode);

                ParameterAccessMethodHelper^ RegisterActionValueBegin(nsConsumer::EVAL_VAR_VALUE* actionValue);

                ParameterAccessMethodHelper^ RegisterGetParamValue(nsConsumer::EVAL_VAR_VALUE* returnValue);
                ParameterAccessMethodHelper^ RegisterGetParamValue(nsConsumer::EVAL_VAR_VALUE* returnValue, nsConsumer::PC_ErrorCode* errorCode);

                ParameterAccessMethodHelper^ RegisterSetParamValue();
                ParameterAccessMethodHelper^ RegisterSetParamValue(nsConsumer::PC_ErrorCode* errorCode);

                ParameterAccessMethodHelper^ RegisterOnMethodExiting();
                ParameterAccessMethodHelper^ RegisterOnMethodExiting(bool returnCode);

                ParameterAccessMethodHelper^ ExecuteScalingMethodSynchronously(System::String^ methodName);
                ParameterAccessMethodHelper^ ExecuteScalingMethodAsynchronously(System::String^ methodName);

                ParameterAccessMethodHelper^ ExecuteMethodSynchronously(System::String^ methodName);
                ParameterAccessMethodHelper^ ExecuteMethodAsynchronously(System::String^ methodName);
                
                bool WasAsyncCalled();
                nsEDDEngine::Mth_ErrorCode GetMethodResult();
            };

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests

#endif // _TESTSUPPORT_PARAMETERACCESSMETHODHELPER_H_
