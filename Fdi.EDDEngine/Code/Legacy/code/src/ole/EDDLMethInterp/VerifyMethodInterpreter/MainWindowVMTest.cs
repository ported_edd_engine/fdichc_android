﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using EDDEngineCOM.Interop;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Microsoft.TeamFoundation.Client;
//using Microsoft.TeamFoundation.VersionControl.Client;


namespace VerifyMethodInterpreter
{
    enum MethCallback
    {
        NO = 0,
        YES = 1
    };

    /// <summary>
    ///This is a test class for MainWindowVMTest and is intended
    ///to contain all MainWindowVMTest Unit Tests
    ///</summary>
    [TestClass]
    public class MainWindowVMTest
    {
        /* the following enum used in VerifyMethodInterpreter project only */
        protected enum ProtocolType
        {
            HART = 1,
            FF = 2,
            PROFIBUS = 3,

            LGC_FF = 102
        };

        private TestContext testContextInstance;
        protected IEDDMethods iDDSPB = null;


        //If log file is correct, return true. Otherwise, return false.
        protected bool logFileComparison(string sDefinition, short protocol, bool bActionTest)
        {

            String logFile = Directory.GetCurrentDirectory();
            logFile = logFile + "\\..\\..\\..\\..\\MItest\\";


            switch (protocol)
            {
                case (int)ProtocolType.FF:
                    logFile = logFile + "Fieldbus\\";
                    break;
                case (int)ProtocolType.HART:
                    logFile = logFile + "Hart\\";
                    break;
                case (int)ProtocolType.PROFIBUS:
                    logFile = logFile + "Profibus\\";
                    break;
            }

            if (bActionTest)
            {
                logFile = logFile + "actionLog.txt";
            }
            else
            {
                logFile = logFile + "methodLog.txt";
            }

            //verify if the method log is expected
            if (!System.IO.File.Exists(logFile))
            {
                Console.WriteLine("     Failed: The method log file {0} doesn't exist.", logFile);
                return false;
            }

            string[] Split = sDefinition.Split(new Char[] { ',' });
            String methodName = Split[1].Trim();						// Get only the methodName part
            int iIndex = methodName.IndexOf('(', 0);
            if (iIndex >= 0)
            {
                methodName = methodName.Remove(iIndex, methodName.Length - iIndex);
            }

            // Create a filename in which to store the current logfile.
            String CurrentLogFile = "";
            if (Split[0] != "0")
            {
                if (bActionTest)
                {
                    CurrentLogFile = logFile.Replace("actionLog.txt", methodName + ".CurrentLog.txt");
                }
                else
                {
                    CurrentLogFile = logFile.Replace("methodLog.txt", methodName + ".CurrentLog.txt");
                }
            }
            else
            {
                CurrentLogFile = logFile.Replace("methodLog.txt", "myTest.CurrentLog.txt");
            }


            System.IO.File.Delete(CurrentLogFile);          // Delete the old one, if it exists
            System.IO.File.Move(logFile, CurrentLogFile);   // Move (rename) the original logFile to this method specific log.

            String sCurrentLog = System.IO.File.ReadAllText(CurrentLogFile);	// Read the CurrentLogFile into a string

            if (String.Compare(sCurrentLog, "", false) == 0)    //empty?
            {
                Console.WriteLine("     Failed: The result of the method in file {0} is empty.", CurrentLogFile);
                return false;
            }

            // Create the filename where the expected log info is stored
            String ExpectedLogFile = "";
            if (Split[0] != "0")
            {
                if (bActionTest)
                {
                    ExpectedLogFile = logFile.Replace("actionLog.txt", methodName + ".Expected.txt");
                }
                else
                {
                    ExpectedLogFile = logFile.Replace("methodLog.txt", methodName + ".Expected.txt");
                }
            }
            else
            {
                ExpectedLogFile = logFile.Replace("methodLog.txt", "myTest.Expected.txt");
            }

            if (!System.IO.File.Exists(ExpectedLogFile))
            {
                Console.WriteLine("     Failed: The expected method log file {0} doesn't exist.", ExpectedLogFile);
                return false;
            }

            String sExpectedLog = System.IO.File.ReadAllText(ExpectedLogFile); //empty?

            if (String.Compare(sExpectedLog, "", false) == 0)
            {
                Console.WriteLine("     Failed: The expected result of the method in file {0} is empty.", ExpectedLogFile);
                return false;
            }

            if (String.Compare(sCurrentLog, sExpectedLog, StringComparison.Ordinal) == 0)
            {
                Console.WriteLine("     Passed: The {0} result is expected.", methodName);
                System.IO.File.Delete(CurrentLogFile);		// If the Current log file matches the Expected, no need to keep the current one.
                return true;
            }
            else
            {
                //Console.WriteLine("The method log file is {0}", logFile);
                //Console.WriteLine("The method result is {0}", sDefinition);
                //Console.WriteLine("The method expected log file is {0}", ExpectedLogFile);
                //Console.WriteLine("The method expected result is\n  {0}", sExpectedDef);
                Console.WriteLine("     Failed: The {0} result is not expected.", methodName);
                return false;
            }
        }


        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            if (iDDSPB == null)
            {
                iDDSPB = new EDDMethodsClass();
            }
        }

        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if (iDDSPB != null)
            {
                Marshal.ReleaseComObject(iDDSPB);
                iDDSPB = null;
            }
        }

        #endregion

        protected void RunOneMethodTest(short protocol, string strTestName, bool bRunTest)
        {
            RunGenericMethodTest(protocol, strTestName, bRunTest, false);
        }

        protected void RunOneActionTest(short protocol, string strTestName)
        {
            RunGenericMethodTest(protocol, strTestName, true, true);
        }


        public void CheckOutFromTFS(string fileName)
        {
            //WorkspaceInfo wi = Workstation.Current.GetLocalWorkspaceInfo(Environment.CurrentDirectory);
            //TfsTeamProjectCollection tfs = new TfsTeamProjectCollection(wi.ServerUri);

            //VersionControlServer versionControlServer = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));
            //Workspace workSpace = versionControlServer.GetWorkspace(wi);

            //if (null != workSpace)
            //{
            //    workSpace.PendEdit(fileName);
            //}
        }

        private void RunGenericMethodTest(short protocol, string strTestName, bool bRunTest, bool bActionTest)
        {
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            sbyte obj = 1;              //means "OK" button is pressed
            Object obj2 = (string)"";
            uint selection = 0;
            Object oMethodList;
            String ExpectedLogFile = "";
            String logFile = "";
            String sFFBlockName = "";

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";

            if (!bRunTest)
            {   //Make test:
                //log file location
                logFile = currPath + "MItest\\";
                switch (protocol)
                {
                    case (int)ProtocolType.FF:
                    case (int)ProtocolType.LGC_FF:
                        logFile = logFile + "Fieldbus\\";
                        break;
                    case (int)ProtocolType.HART:
                        logFile = logFile + "Hart\\";
                        break;
                    case (int)ProtocolType.PROFIBUS:
                        logFile = logFile + "Profibus\\";
                        break;
                }
            }

            //device DD location
            currPath = currPath + "..\\..\\Devices";
            try
            {
                switch (protocol)   //based on ProtocolType
                {
                    case (int)ProtocolType.FF:
                        sFFBlockName = "DATATYPESTB";
                        binaryName = currPath + "\\PROFIBUS-DP\\FoundationFDI\\10ff00\\008f\\0201.ff6";
                        break;
                    case (int)ProtocolType.LGC_FF:
                        sFFBlockName = "DATATYPESTB";
                        binaryName = currPath + "\\FF\\10ff00\\008f\\0201.ff5";
                        protocol = (int)ProtocolType.FF; //treat legacy FF the same as FDI FF in COM
                        break;
                    case (int)ProtocolType.HART:
                        binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";
                        break;
                    case (int)ProtocolType.PROFIBUS:
                        binaryName = currPath + "\\PROFIBUS-DP\\ProfibusFDI\\MethodTestDD_009f.ddl.bin";
                        break;
                }
                Console.WriteLine("The method file name is {0}", binaryName);

                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)protocol, sFFBlockName, out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                if (!bActionTest)
                {
                    try
                    {
                        // run the method waiting for asynchronous MI completion signal
                        error = 0;
                        iDDSPB.BeginMethodInterp(strTestName, (sbyte)MethCallback.YES, out error);

                        //update UI text
                        // polling for asynchronous MI completion
                        sbyte error2 = 0; //not completed yet
                        while (error2 == 0)
                        {
                            iDDSPB.SetUIUserInputNButton(obj2, selection, obj);
                            Thread.Sleep(250);
                            iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                        }
                        iDDSPB.waitForTestToComplete();
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("AddRunMethodTest - RunGenericMethodTest({0}), ({1})", strTestName, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                        error = 1;
                    }

                    if (bRunTest)
                    {
                        //Run comparison test
                        if ((error == 0) && logFileComparison(strTestName, protocol, false))
                        {
                            correct++;
                        }
                        else
                        {
                            mistake++;
                        }
                    }
                    else
                    {   //Make test:
                        //Copy mathodlog.txt to *.Expected.txt
                        string[] Split = strTestName.Split(new Char[] { ',' });
                        //find filename
                        string expectedFileName = Split[1].Trim();
                        int iIndex = expectedFileName.IndexOf('(', 0);
                        if (iIndex >= 0)
                        {
                            expectedFileName = expectedFileName.Remove(iIndex, expectedFileName.Length - iIndex);
                        }

                        logFile = logFile + "methodLog.txt";
                        if ((error == 0) && System.IO.File.Exists(logFile) && (!logFileComparison(strTestName, protocol, false)))
                        {
                            //If the method log is different from the expected, replace the expected with the current method log

                            //checked if the expected File is read only
                            ExpectedLogFile = logFile.Replace("methodLog.txt", expectedFileName + ".Expected.txt");
                            FileAttributes attributes = File.GetAttributes(ExpectedLogFile);
                            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            {
                                try
                                {
                                    CheckOutFromTFS(ExpectedLogFile);
                                }
                                catch (Exception e)
                                {
                                    mistake++;
                                    Console.WriteLine("The checkout failed: {0}", e.ToString());
                                }
                            }

                            if (mistake == 0)
                            {
                                try
                                {
                                    String CurrentLogFile = logFile.Replace("methodLog.txt", expectedFileName + ".CurrentLog.txt");
                                    System.IO.File.Copy(CurrentLogFile, ExpectedLogFile, true);
                                    System.IO.File.Delete(CurrentLogFile);
                                    correct++;
                                }
                                catch (Exception e)
                                {
                                    mistake++;
                                    Console.WriteLine("The copy failed: {0}", e.ToString());
                                }
                            }
                        }
                    }
                }
                else//Action tests.
                {
  
                    //split variable ID and method name
                    string[] Split = strTestName.Split(new Char[] { ',' });

                    try
                    {
                        // run the method waiting for asynchronous MI completion signal
                        error = 0;
                        iDDSPB.BeginVarActionMethodInterp(Split[0], 0, (sbyte)MethCallback.YES, out error);      //action list index  = 0

                        iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                        iDDSPB.waitForTestToComplete();
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("AddRunMethodTest - RunGenericMethodTest({0}), ({1})", Split[0], ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                        error = 1;
                    }

                    if (bRunTest)
                    {
                        //Run comparison test
                        if ((error == 0) && logFileComparison(strTestName, protocol, true))
                        {
                            correct++;
                        }
                        else
                        {
                            mistake++;
                        }
                    }
                    else
                    {   //Make test:
                        //Copy mathodlog.txt to *.Expected.txt
                        string expectedFileName = Split[1].Trim();
                        int iIndex = expectedFileName.IndexOf('(', 0);
                        if (iIndex >= 0)
                        {
                            expectedFileName = expectedFileName.Remove(iIndex, expectedFileName.Length - iIndex);
                        }

                        logFile = logFile + "actionLog.txt";
                        if ((error == 0) && System.IO.File.Exists(logFile) && (!logFileComparison(strTestName, protocol, true)))
                        {
                            //If the method log is different from the expected, replace the expected with the current method log

                            //checked if the expected File is read only
                            ExpectedLogFile = logFile.Replace("actionLog.txt", expectedFileName + ".Expected.txt");
                            FileAttributes attributes = File.GetAttributes(ExpectedLogFile);
                            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                            {
                                try
                                {
                                    CheckOutFromTFS(ExpectedLogFile);
                                }
                                catch (Exception e)
                                {
                                    mistake++;
                                    Console.WriteLine("The checkout failed: {0}", e.ToString());
                                }
                            }

                            if (mistake == 0)
                            {
                                try
                                {
                                    String CurrentLogFile = logFile.Replace("actionLog.txt", expectedFileName + ".CurrentLog.txt");
                                    System.IO.File.Copy(CurrentLogFile, ExpectedLogFile, true);
                                    System.IO.File.Delete(CurrentLogFile);
                                    correct++;
                                }
                                catch (Exception e)
                                {
                                    mistake++;
                                    Console.WriteLine("The copy failed: {0}", e.ToString());
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                mistake++;
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake, correct);
            }
            else
            {
                Console.WriteLine("The number of correct method cases: {0}.", correct);
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake);
            }
        }

    }

    [TestClass()]
    public class Generic : VerifyMethodInterpreter.MainWindowVMTest
    {

        /// <summary>
        ///A test of Method Interpreter Interface for AddRunMethod
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunMethodInterpInterfaceTest()
        {
            String sDefinition;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition = "16414, init_method_set_values";

                //testing isMethodCompleted()
                try
                {
                    // run the method without waiting here
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);
                    //verify asynchronous MI execution state
                    sbyte error2 = 0;
                    iDDSPB.validMethodState(out error2);    //get_AsyncState()
                    if (error2 != 0)
                    {
                        throw new COMException();       //MI state is different from the initial value
                    }

                    // polling for asynchronous MI completion
                    error2 = 0; //no
                    while (error2 == 0)
                    {
                        Thread.Sleep(250);
                        iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                    }
                    iDDSPB.waitForTestToComplete();
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("isMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing waitMethodCompleted()
                try
                {
                    // run the method waiting for asynchronous MI completion signal
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);

                    // waiting for asynchronous MI completion signal
                    iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                    iDDSPB.waitForTestToComplete();
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("waitMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing EndMethodInterp() without callback
                try
                {
                    // run the method polling for asynchronous MI completion
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.NO, out error);

                    iDDSPB.EndMethodInterp(out error);   //wait here until MI done
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("EndMethodInterp - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing EndMethodInterp() with callback
                try
                {
                    // run the method polling for asynchronous MI completion
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("Callback - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                // waiting for asynchronous MI completion signal
                iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                iDDSPB.waitForTestToComplete();

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake / 3, correct / 3);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake / 3);
                Console.WriteLine("The number of correct method cases: {0}.", correct / 3);
            }
        }


        /// <summary>
        ///A test of Method Interpreter EndMethodInterp for AddRunMethod
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunMethodInterpEndTest()
        {
            String sDefinition;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition = "16414, init_method_set_values";

                //testing EndMethodInterp() without callback
                try
                {
                    // run the method polling for asynchronous MI completion
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.NO, out error);

                    iDDSPB.EndMethodInterp(out error);   //wait here until MI done
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("EndMethodInterp - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing EndMethodInterp() with callback
                try
                {
                    // run the method polling for asynchronous MI completion
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("Callback - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                // waiting for asynchronous MI completion signal
                iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                iDDSPB.waitForTestToComplete();

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake / 3, correct / 3);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake / 3);
                Console.WriteLine("The number of correct method cases: {0}.", correct / 3);
            }
        }


        /// <summary>
        ///A test of Method Interpreter waitMethodCompleted for AddRunMethod
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunMethodInterpWaitCompletionTest()
        {
            String sDefinition;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition = "16414, init_method_set_values";

                //testing waitMethodCompleted() without callback
                try
                {
                    // run the method waiting for asynchronous MI completion signal
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.NO, out error);

                    // waiting for asynchronous MI completion signal
                    iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                    iDDSPB.EndMethodInterp(out error);   //wait here until MI done
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("waitMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing waitMethodCompleted() with callback
                try
                {
                    // run the method waiting for asynchronous MI completion signal
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);

                    // waiting for asynchronous MI completion signal
                    iDDSPB.waitMethodCompleted();    //get_AsyncWaitHandle() and wait
                    iDDSPB.waitForTestToComplete();
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("waitMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake / 3, correct / 3);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake / 3);
                Console.WriteLine("The number of correct method cases: {0}.", correct / 3);
            }
        }


        /// <summary>
        ///A test of Method Interpreter isMethodCompleted for AddRunMethod
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunMethodInterpCheckCompletionTest()
        {
            String sDefinition;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition = "16414, init_method_set_values";

                //testing isMethodCompleted() without callback
                try
                {
                    // run the method without waiting here
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.NO, out error);
                    //verify asynchronous MI execution state
                    sbyte error2 = 0;
                    iDDSPB.validMethodState(out error2);    //get_AsyncState()
                    if (error2 != 0)
                    {
                        throw new COMException();       //MI state is different from the initial value
                    }

                    // polling for asynchronous MI completion
                    error2 = 0; //no
                    while (error2 == 0)
                    {
                        Thread.Sleep(250);
                        iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                    }

                    iDDSPB.EndMethodInterp(out error);   //wait here until MI done
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("isMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }

                //testing isMethodCompleted() with callback
                try
                {
                    // run the method without waiting here
                    error = 0;
                    iDDSPB.BeginMethodInterp(sDefinition, (sbyte)MethCallback.YES, out error);
                    //verify asynchronous MI execution state
                    sbyte error2 = 0;
                    iDDSPB.validMethodState(out error2);    //get_AsyncState()
                    if (error2 != 0)
                    {
                        throw new COMException();       //MI state is different from the initial value
                    }

                    // polling for asynchronous MI completion
                    error2 = 0; //no
                    while (error2 == 0)
                    {
                        Thread.Sleep(250);
                        iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                    }
                    iDDSPB.waitForTestToComplete();
                }
                catch (COMException ex)
                {
                    Trace.TraceWarning(String.Format("isMethodCompleted - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                }

                if (logFileComparison(sDefinition, (short)ProtocolType.HART, false))
                {
                    correct++;
                }
                else
                {
                    mistake++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake / 3, correct / 3);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake / 3);
                Console.WriteLine("The number of correct method cases: {0}.", correct / 3);
            }
        }


        /// <summary>
        ///A test of CancelMethod() call for AddRunMethod 
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunMethodInterpCancelTest()
        {
            String[] sDefinition = new string[10];
            int iDefSize = 0;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;
            sbyte obj = 1;              //means "OK" button is pressed
            Object obj2 = (string)"";
            uint selection = 0;

            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition[0] = "16493, method_trig_methods";
                sDefinition[1] = "16583, method_cancel_test";
                iDefSize = 2;

                //testing CancelMethodInterp()
                for (int i = 0; i < iDefSize; i++)
                {
                    try
                    {
                        // run the method polling for asynchronous MI completion
                        error = 0;
                        if (i==0)
                        {
                            iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.NO, out error);
                        }
                        else
                        {
                            iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.YES, out error);
                        }

                        Thread.Sleep(100);              //wait 0.1 seconds
                        iDDSPB.CancelMethodInterp();    //cancel the method, wait until it is done
                        iDDSPB.SetUIUserInputNButton(obj2, selection, obj);
                        iDDSPB.waitForTestToComplete();

                        //as long as we can get here, correction.
                        correct++;
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("EndMethodInterp - RunMethodInterpInterface({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake / 3, correct / 3);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake / 3);
                Console.WriteLine("The number of correct method cases: {0}.", correct / 3);
            }
        }


        /// <summary>
        ///A test of Method Interpreter aborting behavior
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunHartAbortTest()
        {
            String[] sDefinition = new string[10];
            int iDefSize = 0;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;
            sbyte obj = 1;              //means "OK" button is pressed
            Object obj2 = (string)"";
            uint selection = 0;


            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition[0] = "16434, abort_builtin";
                sDefinition[1] = "16458, process_abort_builtin";
                iDefSize = 2;

                for (int i = 0; i < iDefSize; i++)
                {
                    //testing EndMethodInterp() without callback
                    try
                    {
                        // run the method polling for asynchronous MI completion
                        error = 0;
                        iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.NO, out error);

                        //update UI text
                        // polling for asynchronous MI completion
                        sbyte error2 = 0; //not completed yet
                        while (error2 == 0)
                        {
                            selection = (uint)0;
                            iDDSPB.SetUIUserInputNButton(obj2, selection, obj);
                            Thread.Sleep(250);
                            iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                        }

                        error = 0;
                        iDDSPB.EndMethodInterp(out error);
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("RunHartAbortTest({0}), ({1})", sDefinition[i], ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                    }

                    if (error == 1) //both method aborted
                    {
                        //EndMethodInterp() returns zero
                        correct++;
                    }
                    else
                    {
                        mistake++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake, correct);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake);
                Console.WriteLine("The number of correct method cases: {0}.", correct);
            }
        }


        /// <summary>
        ///A test of Method Interpreter cancelling UI builtin behavior
        ///</summary>
        [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunHartCancelUITest()
        {
            String[] sDefinition = new string[10];
            int iDefSize = 0;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;
            sbyte oUIBuitinbutton = 0;              //means "Cancel" button is pressed
            Object oUIBuiltinInput = (string)"";
            uint selection = 0;


            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\HART\\000026\\008f\\0101.fm8";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.HART, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition[0] = "16484, put_message_builtin";
                sDefinition[1] = "16485, build_message_builtin";
                iDefSize = 2;

                for (int i = 0; i < iDefSize; i++)
                {
                    //testing EndMethodInterp() without callback
                    try
                    {
                        // run the method polling for asynchronous MI completion
                        error = 0;
                        iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.YES, out error);

                        //update UI text
                        // polling for asynchronous MI completion
                        sbyte error2 = 0; //not completed yet
                        while (error2 == 0)
                        {
                            iDDSPB.SetUIUserInputNButton(oUIBuiltinInput, selection, oUIBuitinbutton);
                            Thread.Sleep(250);
                            iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                        }
                        //UI builtin has been cancelled
                        iDDSPB.waitForTestToComplete();
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("RunHartCancelUITest({0}), ({1})", sDefinition[i], ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                    }

                    if ((error == 0) && logFileComparison(sDefinition[i], (short)ProtocolType.HART, false))
                    {
                        //BeginMethodInterp() returns zero and log file is expected
                        correct++;
                    }
                    else
                    {
                        mistake++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake, correct);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake);
                Console.WriteLine("The number of correct method cases: {0}.", correct);
            }
        }

        
        /// <summary>
        ///A test of Method Interpreter aborting behavior
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunFieldbusAbortTest()
        {
            String[] sDefinition = new string[10];
            int iDefSize = 0;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;
            sbyte obj = 1;              //means "OK" button is pressed
            Object obj2 = (string)"";
            uint selection = 0;


            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\PROFIBUS-DP\\FoundationFDI\\10ff00\\008f\\0201.ff6";
            string sFFBlockName = "DATATYPESTB";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.FF, sFFBlockName, out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition[0] = "132266, meth_method_abort";
                sDefinition[1] = "132267, meth_communication_abort";
                iDefSize = 2;

                for (int i = 0; i < iDefSize; i++)
                {
                    //testing EndMethodInterp() without callback
                    try
                    {
                        // run the method polling for asynchronous MI completion
                        error = 0;
                        iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.NO, out error);

                        //update UI text
                        // polling for asynchronous MI completion
                        sbyte error2 = 0; //not completed yet
                        while (error2 == 0)
                        {
                            selection = (uint)0;
                            iDDSPB.SetUIUserInputNButton(obj2, selection, obj);
                            Thread.Sleep(250);
                            iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                        }

                        error = 0;
                        iDDSPB.EndMethodInterp(out error);
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("RunFieldbusAbortTest({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                    }

                    if (((i == 0) && (error == 1)) || ((i == 1) && (error == 0))) //aborted and succeeded after 3 retries of communication builtin.
                    {
                        //EndMethodInterp() returns zero
                        correct++;
                    }
                    else
                    {
                        mistake++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake, correct);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake);
                Console.WriteLine("The number of correct method cases: {0}.", correct);
            }
        }


        /// <summary>
        ///A test of Method Interpreter aborting behavior
        ///</summary>
        [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestMethod(), TestCategory("RunTest")]
        public void RunProfibusAbortTest()
        {
            String[] sDefinition = new string[10];
            int iDefSize = 0;
            String currPath;
            string binaryName = "";
            int correct = 0;
            int mistake = 0;
            int error = 0;
            Object oMethodList;
            sbyte obj = 1;              //means "OK" button is pressed
            Object obj2 = (string)"";
            uint selection = 0;


            /* find current directory */
            currPath = Directory.GetCurrentDirectory();
            currPath = currPath + "\\..\\..\\..\\..\\";
            currPath = currPath + "..\\..\\Devices";
            binaryName = currPath + "\\PROFIBUS-DP\\ProfibusFDI\\MethodTestDD_009f.ddl.bin";

            try
            {
                try
                {
                    iDDSPB.CreateDeviceInstanceNLoadDD(binaryName, (short)ProtocolType.PROFIBUS, "", out oMethodList, out error);
                }
                catch (COMException ex)
                {
                    Trace.TraceError(String.Format("UnhandledException occurred in CreateDeviceInstanceNLoadDD(). error is {0}. \n{1}", error, ex.ToString()), System.Diagnostics.EventLogEntryType.Error);
                }

                Console.WriteLine("The method file name is {0}", binaryName);
                sDefinition[0] = "112, abort_builtin";
                sDefinition[1] = "113, method_parameter_error";
                iDefSize = 2;

                for (int i = 0; i < iDefSize; i++)
                {
                    //testing EndMethodInterp() without callback
                    try
                    {
                        // run the method polling for asynchronous MI completion
                        error = 0;
                        iDDSPB.BeginMethodInterp(sDefinition[i], (sbyte)MethCallback.NO, out error);

                        //update UI text
                        // polling for asynchronous MI completion
                        sbyte error2 = 0; //not completed yet
                        while (error2 == 0)
                        {
                            selection = (uint)0;
                            iDDSPB.SetUIUserInputNButton(obj2, selection, obj);
                            Thread.Sleep(250);
                            iDDSPB.isMethodCompleted(out error2);    //get_IsCompleted()
                        }

                        error = 0;
                        iDDSPB.EndMethodInterp(out error);
                    }
                    catch (COMException ex)
                    {
                        Trace.TraceWarning(String.Format("RunProfibusAbortTest({0}), ({1})", sDefinition, ex.ToString()), System.Diagnostics.EventLogEntryType.Warning);
                    }

                    if (((i == 0) && (error == 1)) || ((i == 1) && (error == 3)))   //aborted and failed
                    {
                        //EndMethodInterp() returns zero
                        correct++;
                    }
                    else
                    {
                        mistake++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The method interpreter test failed: {0}", e.ToString());
            }

            if ((correct != 0) && (mistake != 0))
            {
                Assert.Inconclusive("INconclusive: {0} number of method tests are failed. And {1} number of method tests are passed", mistake, correct);
            }
            else
            {
                Assert.IsTrue((mistake == 0), "Failed: {0} number of method tests are failed.", mistake);
                Console.WriteLine("The number of correct method cases: {0}.", correct);
            }
        }

    }
}
        

