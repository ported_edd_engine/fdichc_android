
#pragma once

typedef struct {
	UINT32		magic_number;
	UINT32		header_size;
	UINT32		objects_size;
	UINT32		data_size;
	UINT32		manufacturer;
	UINT16		device_type;
	UINT8		device_revision;
	UINT8		dd_revision;
	UINT32		reserved1;
	UINT32		reserved2;
	UINT32		reserved3;
	UINT32		reserved4;
} DDOD_HEADER;

