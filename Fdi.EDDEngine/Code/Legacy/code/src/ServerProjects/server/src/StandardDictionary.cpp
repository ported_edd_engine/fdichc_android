#include "stdinc.h"
#include "StandardDictionary.h"
#include <CommonLibraries/AmsFileReader/AmsFileReader.h>

#include "langspec.h"


const TCHAR* StandardDictionary::high_TOKEN_TEXT = _T("|en|@XX@Highest Offset");

StandardDictionaryEntry::StandardDictionaryEntry(unsigned long section, 
											     unsigned long offset, 
												 unsigned long value,
												 LPCTSTR text)
: m_section(section),
  m_offset(offset),
  m_value(value),
  m_text(text)
{
}

StandardDictionaryEntry::~StandardDictionaryEntry(void)
{
}

unsigned long StandardDictionaryEntry::GetSection()
{
	return 	m_section;
}

unsigned long StandardDictionaryEntry::GetOffset()
{
	return m_offset;
}

unsigned long StandardDictionaryEntry::GetValue()
{
	return 	m_value;
}

LPCTSTR StandardDictionaryEntry::GetText()
{
	return 	m_text;
}

StandardDictionary::StandardDictionary() :
	m_nextchar(-1),
	m_dictionaryLineCount(0),
	m_dictionaryErrorCount(0),
	m_dictionaryBufferU(NULL),
	m_dictionaryBufferUSize(0),
	m_dictionaryBufferULocation(0)
{
	m_Env_Info = nullptr;
}

StandardDictionary::~StandardDictionary(void)
{
}

int StandardDictionary::SortStandardDictionary(const void* p1, const void* p2)
{
	ntcassert2(p1);
	ntcassert2(p2);
	StandardDictionaryEntry* entry1 = *((StandardDictionaryEntry**)p1);
	StandardDictionaryEntry* entry2 = *((StandardDictionaryEntry**)p2);

	return (int)(entry1->GetValue() - entry2->GetValue());
}


int StandardDictionary::CreateDictionary(ENV_INFO* env_info, LPCTSTR dictionaryFile, StdDictTable *stdDictTable, DictionaryTable *dictTable)
{
	m_Env_Info = env_info;
	bool bSucceeded = ReadDictionaryFile(dictionaryFile);

	if (bSucceeded)
	{
		ParseDictionary();

		if (m_dictionary.GetSize() > 0)
		{
			qsort(m_dictionary.GetData(),
				  (size_t)m_dictionary.GetSize(),
				  sizeof(StandardDictionaryEntry*),
				  &SortStandardDictionary);
		}

		UpdateCaller(stdDictTable, dictTable);

		Cleanup();
	}

	return m_dictionaryErrorCount;
}

bool StandardDictionary::ReadDictionaryFile(LPCTSTR dictionaryFile)
{
	bool bSucceeded = false;
	CStdString sError;

	AmsFileReader fReader;

	if (fReader.OpenFile(dictionaryFile) == FILE_READER_SUCCESS)
	{
		if ((fReader.GetFileEncoding() == FILE_ENCODING_UTF8) || (fReader.GetFileEncoding() == FILE_ENCODING_ANSI))
		{
			AmsFileReaderStatus status = fReader.GetFileAsBuffer(&m_dictionaryBufferU, &m_dictionaryBufferUSize);

			if (status == FILE_READER_SUCCESS)
			{
				bSucceeded = true;
			}
			else
			{
				CStdString sErrorInfo;
				fReader.GetErrorInfo(sErrorInfo);
				sError.Format(_T("Failed to parse dictionary file: %s, for reason: %s"), dictionaryFile, sErrorInfo);
				LogDictionaryError(__FILE__, __LINE__, sError);
			}
		}
		else
		{
			sError.Format(_T("Dictionary file is not in the correct format (UTF-8): %s"), dictionaryFile);
			LogDictionaryError(__FILE__, __LINE__, sError);
		}
	}
	else
	{
		CStdString sErrorInfo;
		fReader.GetErrorInfo(sErrorInfo);
		sError.Format(_T("Unable to open dictionary file: %s for parsing: %s"), dictionaryFile, sErrorInfo);
		LogDictionaryError(__FILE__, __LINE__, sError);
	}
	
	return bSucceeded;
}

void StandardDictionary::ParseDictionary()
{
	unsigned long section = 0, offset = 0;
	int tokenResult = GetToken();

	while (tokenResult != TOKEN_DONE) 
	{
		/*
		 * Verify the token is a TOKEN_NUMBER.
		 */
		if (tokenResult != TOKEN_NUMBER) 
		{
			LogDictionaryErrorFromResource(__FILE__, __LINE__, IDS_DICT_TOKEN_ERROR);
			ReadDictionaryUntilValidToken();
			continue;
		}

		/*
		 * Extract the section TOKEN_NUMBER and offset from
		 * the token.  The token MUST look like [<number>,<number>] 
		 * which is enforced in GetToken()
		 */
		m_token.Remove(L'[');
		m_token.Remove(L']');
		CStdString sSection = m_token.Left(m_token.Find(L","));
		CStdString sOffset = m_token.Right(m_token.GetLength() - (m_token.Find(L",") + 1));
		section = wcstoul(sSection, NULL, 10);
		offset = wcstoul(sOffset, NULL, 10);

		/*
		 * Get next token and verify it's a string name.
		 */
		tokenResult = GetToken();
		if (tokenResult != TOKEN_NAME) 
		{
			LogDictionaryErrorFromResource(__FILE__, __LINE__, IDS_DICT_TOKEN_ERROR);
			ReadDictionaryUntilValidToken();
			continue;
		}

		/*
		 * Save the name.
		 */
		//CStdString sName = m_token;		// Don't save the name, we never use it.

		/*
		 * Get the next token and verify it's a string.
		 */
		tokenResult = GetToken();
		if (tokenResult != TOKEN_TEXT) 
		{
			LogDictionaryErrorFromResource(__FILE__, __LINE__, IDS_DICT_TOKEN_ERROR);
			ReadDictionaryUntilValidToken();
			continue;
		}

		/*
		 *	Save this entry into dict_array, expanding it if necessary.
		 */
		StandardDictionaryEntry* newDictEntry = new StandardDictionaryEntry(section,
																			offset,
																			section * 65536 + offset,
																			m_token);
		m_dictionary.Add(newDictEntry);

		/*
		 *	Get the next token
		 */

		tokenResult = GetToken();
	}
}



wchar_t StandardDictionary::GetNextCharacter()
{
	wchar_t nextCharacter = WEOF;

	if (m_dictionaryBufferULocation < m_dictionaryBufferUSize)
	{
		nextCharacter = m_dictionaryBufferU[m_dictionaryBufferULocation++];
	}

	return nextCharacter;
}

int StandardDictionary::GetToken()
{
	wchar_t character = 0;

	if (m_nextchar < 0)
	{
		character = GetNextCharacter();
	}
	else
	{
		character = (wchar_t)m_nextchar;
		m_nextchar = -1;
	}

	for (;;) {
		switch (character) {
		case WEOF:
			NullTerminate();
			return TOKEN_DONE;

		/*
		 * Eat whitespace.
		 */
		case L'\n':
			++m_dictionaryLineCount;
			/* FALL THROUGH */

		case L' ':
		case L'\b':
		case L'\f':
		case L'\r':
		case L'\t':
		case L'\v':
			character = GetNextCharacter();
			break;

		/*
		 * Eat a comment.
		 */
		case L'/':
			if (ReadDictionaryComment() != TOKEN_DONE) 
			{
				NullTerminate();
				return TOKEN_ERROR;
			}
			character = GetNextCharacter();
			break;

		case L',':
			NullTerminate();
			return TOKEN_COMMA;

		/*
		 * Read a TOKEN_NUMBER.
		 */
		case L'[':
			/*
			 * Clear the token .
			 */
			m_token.Empty();

			/*
			 * Read a digit sequence.
			 */
			do {
				m_token.append(1, character);
				character = GetNextCharacter();
			} while (iswdigit(character));

			/*
			 * Read a TOKEN_COMMA.
			 */
			if (character != L',') 
			{
				NullTerminate();
				return TOKEN_ERROR;
			}

			/*
			 * Read another digit sequence.
			 */
			do {
				m_token.append(1, character);
				character = GetNextCharacter();
			} while (iswdigit(character));

			/*
			 * Verify it ends correctly.
			 */
			if (character != L']') 
			{
				NullTerminate();
				return TOKEN_ERROR;
			}
			m_token.append(1, character);

			/*
			 * Return it.
			 */
			NullTerminate();
			return TOKEN_NUMBER;

		case L'A': case L'B': case L'C': case L'D': case L'E': case L'F':
		case L'G': case L'H': case L'I': case L'J': case L'K': case L'L':
		case L'M': case L'N': case L'O': case L'P': case L'Q': case L'R':
		case L'S': case L'T': case L'U': case L'V': case L'W': case L'X':
		case L'Y': case L'Z': case L'_': case L'a': case L'b': case L'c':
		case L'd': case L'e': case L'f': case L'g': case L'h': case L'i':
		case L'j': case L'k': case L'l': case L'm': case L'n': case L'o':
		case L'p': case L'q': case L'r': case L's': case L't': case L'u':
		case L'v': case L'w': case L'x': case L'y': case L'z':
			/*
			 * Clear the token .
			 */
			m_token.Empty();

			/*
			 * Read a sequence of alphanumerics and underscores.
			 */
			do {
				m_token.append(1, character);
				character = GetNextCharacter();
			} while (iswalnum(character) || character == L'_');

			/*
			 * Save the lookahead character.
			 */
			m_nextchar = character;

			/*
			 * Return it.
			 */
			NullTerminate();
			return TOKEN_NAME;

		case L'\"':
			/*
			 * Clear the token .
			 */
			m_token.Empty();

			/*
			 * If string does not start with a country code,
			 * add the default country code (English).
			 */
			character = GetNextCharacter();
			if (character != L'|') {
				m_token.append(L"|en|");
			}

			/*
			 * Read the characters of the string.
			 */
			for (;; character = GetNextCharacter()) {
				switch (character) {
				/*
				 * Unterminated string.
				 */
				case WEOF:
				case L'\n':
					NullTerminate();
					return TOKEN_ERROR;

				/*
				 * Anything can be escaped with a backslash,
				 * this will usually be \".
				 */
				case L'\\':
					m_token.append(1, character);
					character = GetNextCharacter();
					if (character == WEOF) 
					{
						NullTerminate();
						return TOKEN_ERROR;
					}
					m_token.append(1, character);
					break;

				/*
				 * We've come to the end of the string. Read ahead
				 * to see if there is string immediately following
				 * this one, and if there is read that one also.
				 */
				case L'\"':
					character = GetNextCharacter();
					for (;;) {
						switch (character) {
						/*
						 * Eat whitespace.
						 */
						case L'\n':
							m_dictionaryLineCount++;
							/* FALL THROUGH */

						case L',':
						case L' ':
						case L'\b':
						case L'\f':
						case L'\r':
						case L'\t':
							character = GetNextCharacter();
							continue;

						/*
						 * Eat a comment.
						 */
						case L'/':
							if (ReadDictionaryComment() != TOKEN_DONE) 
							{
								NullTerminate();
								return TOKEN_ERROR;
							}
							character = GetNextCharacter();
							continue;

						/*
						 * We've seen something other than
						 * a comment or whitespace.
						 */
						default:
							break;
						}
						break;
					}

					/*
					 * If what we've seen is the start
					 * of another string, read it also.
					 */
					if (character == L'\"')
						break;

					/*
					 * We've seen something other than
					 * the start of a string. Save the
					 * lookahead character.
					 */
					m_nextchar = character;
					NullTerminate();
					return TOKEN_TEXT;

				/*
				 * Ordinary character, add it to the token .
				 */
				default:
					m_token.append(1, character);
					break;
				}
			}
			/*NOTREACHED*/

		/*
		 * Invalid input.
		 */
		default:
			NullTerminate();
			return TOKEN_ERROR;
		}
	}

}


void StandardDictionary::UpdateCaller(StdDictTable *stdDictTable, DictionaryTable *dictTable)
{
/*
	 *	Step through the sorted entries, verifying them and passing
	 *	them to the use supplied function.  Verification includes
	 *	that the offsets are continguous within a section, that each
	 *	section starts with offset zero, and that each section ends
	 *	with a special entry with TOKEN_TEXT matching high_TOKEN_TEXT.
	 *
	 *	Dictline is set to zero to signal dicterr() not to print
	 *	and line TOKEN_NUMBERs.
	 */

	INT_PTR entries = m_dictionary.GetSize();
	for (INT_PTR nIndex = 0; nIndex < entries; nIndex++)
	{
		StandardDictionaryEntry* currentEntry = m_dictionary.GetAt((int)(nIndex));

		if (currentEntry != NULL)
		{
			/*
			 *	If this is the last entry, it must be a high TOKEN_TEXT entry.
			 */
			if (nIndex+1 == entries)
			{
                if (!currentEntry->GetText() || wcscmp(currentEntry->GetText(), high_TOKEN_TEXT))
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT5, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT5);
					strFmt.ReleaseBuffer();
					strErr.Format(strFmt,currentEntry->GetSection());
					LogDictionaryError(__FILE__, __LINE__, strErr);
				}
				/*
				 *	This is a "hidden" entry, so don't pass it to the
				 *	user function.
				 */
				continue;
			}

			/*
			*	Each entry must be unique.
			*/

			StandardDictionaryEntry* nextEntry = m_dictionary.GetAt((int)(nIndex+1));

			if (nextEntry != NULL)
			{
				if (currentEntry->GetValue() == nextEntry->GetValue()) 
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT1, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT1);
                    strFmt.ReleaseBuffer();
					strErr.Format(strFmt,currentEntry->GetSection(), currentEntry->GetOffset());
					LogDictionaryError(__FILE__, __LINE__, strErr);
					nIndex++;
					continue;
				}
			}

			/*
			 *	If this entry is the special high TOKEN_TEXT string ...
			 */

            if(currentEntry->GetText() && wcscmp(currentEntry->GetText(), high_TOKEN_TEXT) == 0)
			{
				/*
				 *	... then the next section must be different than
				 *	this section, and ...
				 */

				if (currentEntry->GetSection() == nextEntry->GetSection()) 
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT4, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT4);

                    strFmt.ReleaseBuffer();
					strErr.Format(strFmt,currentEntry->GetSection());
					LogDictionaryError(__FILE__, __LINE__, strErr);
				}

				/*
				 *	... the first offset in the next section must
				 *	be zero.
				 */

				else if (nextEntry->GetOffset() != 0) 
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT3, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT3);
                    strFmt.ReleaseBuffer();
					strErr.Format(strFmt,nextEntry->GetSection());
					LogDictionaryError(__FILE__, __LINE__, strErr);
				}

				/*
				 *	This is a "hidden" entry, so don't pass it to the
				 *	user function.
				 */
				continue;
			}
			else
			{

				/*
				 *	This is a "regular" entry.  It must not be the
				 *	last entry in a section.
				 */

				if (currentEntry->GetSection() != nextEntry->GetSection()) 
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT5, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT5);
                    strFmt.ReleaseBuffer();
					strErr.Format(strFmt,currentEntry->GetSection());
					LogDictionaryError(__FILE__, __LINE__, strErr);
				}

				/*
				 *	The offset of the next entry must be one larger
				 *	than this one.
				 */

				else if (currentEntry->GetOffset() != nextEntry->GetOffset() - 1) 
				{
					CStdString strErr, strFmt;
                    //::LoadString(0, IDS_DICT_ERRFMT2, strFmt.GetBuffer(MAX_PATH), MAX_PATH);
                    strFmt.TryLoad((void *)IDS_DICT_ERRFMT2);
                    strFmt.ReleaseBuffer();
					strErr.Format(strFmt,currentEntry->GetOffset(), nextEntry->GetOffset(), currentEntry->GetSection());
					LogDictionaryError(__FILE__, __LINE__, strErr);
				}

				/*
				 *	Call the install function.
				 */

				if(currentEntry->GetText())
				{
                    // This isn't the most pretty code but it gets the job done
                    // without too many changes to the code.
                    // There was origninally one of two static functions passed into
                    // this call.
                    // To make multi threading possible I had to pass in class objects
                    // If just a stdDictTable is found then call dict_table_install
                    // if a DictionaryTable is also passed in then call
                    // activeNonStandardDictionary->Install
                    // The pointer to dictTable is only used to get a boolean value
                    // so don't spend too much time trying to find other uses.

					if( dictTable != NULL )
					    dictTable->Install(currentEntry->GetValue(), currentEntry->GetText());
                    else if (stdDictTable != NULL)
					    stdDictTable->dict_table_install(m_Env_Info, currentEntry->GetValue(), currentEntry->GetText());

				}
			}
		}
	}
}


int StandardDictionary::ReadDictionaryComment()
{
	wchar_t character = 0;
	
	/*
	 * Verify we're at the start of a comment. If we don't
	 * find a valid start of comment, return an TOKEN_ERROR.
	 */
	character = GetNextCharacter();
	if (character != L'*') {
		return TOKEN_ERROR;
	}

	/*
	 * Consume the comment.
	 */
	character = GetNextCharacter();
	for (;;) {
		if (character == WEOF) {
			return TOKEN_ERROR;
		}

		/*
		 * If it's a *, check for end of comment.
		 */
		if (character == L'\n') {
			++m_dictionaryLineCount;
		} else if (character == L'*') {
			character = GetNextCharacter();
			if (character == WEOF) {
				return TOKEN_ERROR;
			}
			if (character == L'\n') {
				++m_dictionaryLineCount;
			} else if (character == L'/') {
				break;
			} else {
				continue;
			}
		}
		/*
		 * Next please.
		 */
		character = GetNextCharacter();
	}

	/*
	 * Indicates valid comment consumed.
	 */
	return TOKEN_DONE;
}

int StandardDictionary::ReadDictionaryUntilValidToken()
{
	int tokenResult;
	do { 
		tokenResult = GetToken();
	} 
	while (tokenResult != TOKEN_DONE && tokenResult != TOKEN_NUMBER);
	
	return tokenResult;
}

bool StandardDictionary::IsHighTokenText(CStdString& sDictionaryText)
{
	bool bHighTokenText = false;

    if (wcscmp(sDictionaryText, high_TOKEN_TEXT) == 0)
	{
		bHighTokenText = true;
	}

	return bHighTokenText;
}

void StandardDictionary::Cleanup()
{
	if (m_dictionaryBufferU)
		delete[] m_dictionaryBufferU;

	INT_PTR entries = m_dictionary.GetSize();
	for (INT_PTR i = 0; i < entries; i++)
	{
		StandardDictionaryEntry* entry = m_dictionary.GetAt((int)(i));
		if (entry != NULL)
			delete entry;
	}
	m_dictionary.RemoveAll();
}

void StandardDictionary::LogDictionaryErrorFromResource(const char *cFilename, UINT32 lineNumber, int errorID, CStdString& errorText)
{
	CStdString strMsg;

    //::LoadString(0, errorID, strMsg.GetBuffer(_MAX_PATH), _MAX_PATH);
    strMsg.TryLoad((void *)errorID);
    strMsg.ReleaseBuffer();
	strMsg.Format(strMsg, errorText);

	LogDictionaryError(cFilename, lineNumber, strMsg);
}

void StandardDictionary::LogDictionaryErrorFromResource(const char *cFilename, UINT32 lineNumber, int errorID)
{
	CStdString strMsg;

    //::LoadString(0, errorID, strMsg.GetBuffer(_MAX_PATH), _MAX_PATH);
    strMsg.TryLoad((void *)errorID);
    strMsg.ReleaseBuffer();

	LogDictionaryError(cFilename, lineNumber, strMsg);
}

void StandardDictionary::LogDictionaryError(const char *cFilename, UINT32 lineNumber, LPCTSTR strError)
{
	CStdString strTitle;
    //::LoadString(0, IDS_DICT_TITLE, strTitle.GetBuffer(_MAX_PATH), _MAX_PATH);
    strTitle.TryLoad((void *)IDS_DICT_TITLE);
    EddEngineLog(m_Env_Info, cFilename, lineNumber, BssWarning, L"LegacyHART", L"%s %s", strTitle.c_str(), strError);
	m_dictionaryErrorCount++;
}

