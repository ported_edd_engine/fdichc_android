
#include "env_info.h"
//#include <windows.h>

#pragma once

//typedef unsigned long BssStatus;

typedef enum LogSeverity
	{
       Critical,
       Error,
	   Warning,
       Information,
       Verbose,
	} LogSeverity;

void EddEngineLog (
   ENV_INFO	*env_info,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   UINT severity,						// see documentation
   wchar_t* wCategory,
   LPCWSTR wMsgFormat,			        // a 'printf' format
   ...								    //    and it's parameters
);

void EddEngineLog (
   IDDSSupport *pDDSSupport,
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   UINT severity,						// see documentation
   wchar_t* wCategory,
   LPCWSTR wMsgFormat,			        // a 'printf' format
   ...								    //    and it's parameters
);

#if 1 // || BSS_SEVERITY_CONSTANTS)
#define BssError Error
#define BssWarning Warning
#define BssInformation Information
#endif


