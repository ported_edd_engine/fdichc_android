/*************************************************************************************************
 *
 * $Workfile: ddbCollAndArr.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		More item classes
 *		8/29/02	sjv	created from DDBeditdisplay.h
 *
 *		
 * #include "ddbCollAndArr.h".
 */

#ifndef _DDB_COLLANDARR_H
#define _DDB_COLLANDARR_H
#ifdef INC_DEBUG
#pragma message("In ddbCollAndArr.h") 
#endif

#include "ddbGeneral.h"
#include "ddbItemBase.h"

#include "ddbAttributes.h"
#include "ddbGrpItmDesc.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbCollAndArr.h") 
#endif

//class hCattrBase;


/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ITEM ARRAY CLASS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class hCitemArray: public hCgrpItmBasedClass /* itemType 9 ITEM_ARRAY_ITYPE */
{
public:
	hCitemArray(DevInfcHandle_t h, aCitemBase* paItemBase);
    virtual ~hCitemArray(){
#if defined(_WIN32) || defined(__ANDROID__)
        hCgrpItmBasedClass::~hCgrpItmBasedClass(); //NO need to call excplictly
#else
        this->~hCgrpItmBasedClass();
#endif
    }
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE Label(wstring& retStr);
	virtual RETURNCODE Help(wstring& retStr);
};

class hCitemArrayValues : public hCitemArray
{
	long m_lItemID;
	public:
		hCitemArrayValues( DevInfcHandle_t h, long lItemID );
		virtual itemType_t getIType(void);
		virtual RETURNCODE getAllindexValues(UIntList_t& allValidindexes);
};

/*====================================== Collections ===========================================*/


/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx COLLECTION CLASS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class hCcollection: public hCgrpItmBasedClass /* itemType 10 COLLECTION_ITYPE */
{
public:
	hCcollection( DevInfcHandle_t h, aCitemBase* paItemBase );
    virtual ~hCcollection(){
#if defined(_WIN32) || defined(__ANDROID__)
        hCgrpItmBasedClass::~hCgrpItmBasedClass(); //NO need to call excplictly
#else
        this->~hCgrpItmBasedClass();
#endif
    };
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE Label(wstring& retStr);
	virtual RETURNCODE Help(wstring& retStr);
};


class hCfile : public hCgrpItmBasedClass 
{
public:
	hCfile( DevInfcHandle_t h, aCitemBase* paItemBase );
    virtual ~hCfile(){
#if defined(_WIN32) || defined(__ANDROID__)
        hCgrpItmBasedClass::~hCgrpItmBasedClass(); //NO need to call excplictly
#else
        this->~hCgrpItmBasedClass();
#endif
    }
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
	virtual RETURNCODE Label(wstring& retStr);
	virtual RETURNCODE Help(wstring& retStr);
};

#endif //_DDB_COLLANDARR_H

/*************************************************************************************************
 *
 *   $History: ddbCollAndArr.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
