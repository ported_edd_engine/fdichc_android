#ifndef FMS_CRYPT_H
#define	FMS_CRYPT_H
//
// Copyright 1996 Fisher Rosemount Systems, Inc. - All Rights Reserved
//
// fmscrypt.h :  This file contains declarations supporting the algorithm used in 
//               encrypting/decrypting the DDOD files.
//

// Constants

const unsigned char    keySize = 5;
const unsigned char    key [keySize] = {0x31, 0xAD, 0x26, 0x7B, 0x69};
const unsigned char    magicNumberSize = 4;
const unsigned char    magicNumberCiphertext [magicNumberSize] = {0xe3, 0xc8, 0x79, 0x10};
const unsigned char    magicNumberPlaintext [magicNumberSize] = {0x7f, 0x3f, 0x5f, 0x77};

typedef enum
{
	FMSCRYPT_INVAL_REG_KEY_FAIL    = -7,
	FMSCRYPT_ZERO_LENGTH_FAIL      = -6,
	FMSCRYPT_MEMORY_ALLOC_FAIL     = -5,
	FMSCRYPT_REG_SET_VALUE_FAIL    = -4,
	FMSCRYPT_REG_DELETE_VALUE_FAIL = -3,
	FMSCRYPT_REG_OPEN_KEY_FAIL     = -2,
    FMSCRYPT_INVALID_INPUT_PARM    = -1,
    SUCCESSFUL                     =  0
} ReturnStatus;
 
#ifdef _WIN32
class IRegWrapper
{
public:
	virtual long Reg_Open_Key (
			__in HKEY hKey,
			__in_opt LPCTSTR lpSubKey,
			__reserved DWORD ulOptions,
			__in REGSAM samDesired,
			__out PHKEY phkResult
		) = 0;

	virtual long Reg_Delete_Value (
	    __in HKEY hKey,
		__in_opt LPCTSTR lpValueName
    ) = 0;

	virtual long Reg_Set_Value (
		__in HKEY hKey,
		__in_opt LPCTSTR lpValueName,
		__reserved DWORD Reserved,
		__in DWORD dwType,
		__in_bcount_opt(cbData) CONST BYTE* lpData,
		__in DWORD cbData
    ) = 0;

	virtual long Reg_Query_Value(
		__in HKEY hKey,
		__in_opt LPCTSTR lpValueName,
		__reserved LPDWORD lpReserved,
		__out_opt LPDWORD lpType,
		__out_bcount_opt(*lpcbData) LPBYTE lpData,
		__inout_opt LPDWORD lpcbData
    ) = 0;
};


class CRegWrapper : public IRegWrapper
{
public:
	virtual long Reg_Open_Key (
			__in HKEY hKey,
			__in_opt LPCTSTR lpSubKey,
			__reserved DWORD ulOptions,
			__in REGSAM samDesired,
			__out PHKEY phkResult
		)
	{
		return RegOpenKeyEx(hKey,lpSubKey,ulOptions,samDesired,phkResult);
	};

	virtual long Reg_Delete_Value (
	    __in HKEY hKey,
		__in_opt LPCTSTR lpValueName
    )
	{
		return RegDeleteValue(hKey,lpValueName);
	}

	virtual long Reg_Set_Value (
		__in HKEY hKey,
		__in_opt LPCTSTR lpValueName,
		__reserved DWORD Reserved,
		__in DWORD dwType,
		__in_bcount_opt(cbData) CONST BYTE* lpData,
		__in DWORD cbData
    )
	{
		return RegSetValueEx(hKey,lpValueName,Reserved,dwType,lpData,cbData);
	}

	virtual long Reg_Query_Value(
		__in HKEY hKey,
		__in_opt LPCTSTR lpValueName,
		__reserved LPDWORD lpReserved,
		__out_opt LPDWORD lpType,
		__out_bcount_opt(*lpcbData) LPBYTE lpData,
		__inout_opt LPDWORD lpcbData
    )
	{
		return RegQueryValueEx(hKey, lpValueName, lpReserved, lpType, lpData, lpcbData);
	}
};

#ifdef __cplusplus  
extern "C" {
#endif
   
ReturnStatus WINAPI  FmsCrypt (
        BYTE *startByte, 
        long numBytes);

ReturnStatus WINAPI WritePswdToReg (
        TCHAR *sPswd, 
        TCHAR *sRegKey,
		IRegWrapper *regAccess = NULL);

ReturnStatus WINAPI ReadPswdFromReg (
        TCHAR *sPswd, 
        TCHAR *sRegKey,
		IRegWrapper *regAccess = NULL);

ReturnStatus  WINAPI FmsCryptVarying (
        BYTE *startByte, 
        long numBytes);

ReturnStatus  WINAPI FmsCryptVaryingHex (
        TCHAR *sHexString, 
        long lenHexString,
		BYTE *output);

ReturnStatus WINAPI EncryptAndWriteStringToReg (
        TCHAR *sUnencryptedString, 
        TCHAR *sRegKey,
		IRegWrapper *regAccess = NULL);

ReturnStatus WINAPI ConvertFMSiniPasswordForUpgrade (
        TCHAR *sOldEncrptedPassword, 
		long   lOldLength,
        TCHAR *sNewEncrptedPassword,
		long   lMaxLength);

#ifdef __cplusplus
}
#endif
#endif
#endif //FMS_CRYPT_H
