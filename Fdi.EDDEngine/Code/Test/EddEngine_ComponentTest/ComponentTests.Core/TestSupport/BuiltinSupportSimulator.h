#ifndef _BUILTINSUPPORT_SIMULATOR_H_
#define _BUILTINSUPPORT_SIMULATOR_H_

#include <windows.h>
#include <functional>
#include <string>
#include <vector>
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IBuiltinSupport.h"
#include "MockRecorder.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            class BuiltinSupportSimulator : public nsConsumer::IBuiltinSupport
            {
            private:
                void AddCalledMethod(const char* method, const char* paramsFmt, ...);

                int _InvokeCallbackDelay;
            public:
                MockRecorder* Recorder;

                BuiltinSupportSimulator();
                ~BuiltinSupportSimulator();

                std::vector<std::string> CalledMethods;
                std::string GetCalledMethodsAsString();

                std::vector<std::string> CalledMethodParameters;
                std::string GetCalledMethodParametersAsString();

                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec)> ReadValueFunction;
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec)> WriteValueFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, INT8 *pError_Class, INT8 *pError_Code, INT16 *pAddl_code, unsigned long *err_id, unsigned long *err_member_id)> GetResponseCodeFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, unsigned long *pError)> GetCommErrorFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, int cmd_num, int trans, unsigned char *cmd_status)> SendCmdTransFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, unsigned char *more_data_status, unsigned char *more_data_info, int* more_data_info_size)> GetMoreStatusFunction; // Sends Cmd 48
                std::function<int(void* pValueSpec)> IsOfflineFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, ITEM_ID command_id, long *pErrorValue)> SendCommandFunction;
                std::function<void(void* pValueSpec, int iPriority, wchar_t *message)> LogMessageFunction; // used for _ERROR, _WARNING, _TRACE and LOG_MESSAGE
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* op_ref, int start_index, int delete_count)> ListDeleteElementAtFunction;
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef )> ListInsertFunction;
                std::function<nsConsumer::PC_ErrorCode(int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF* op_ref, const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE* pParamValue)> GetDynamicAttributeFunction;
                std::function<nsConsumer::PC_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ nsConsumer::EVAL_VAR_VALUE *pValue)> GetParamValueFunction;
                std::function<nsConsumer::PC_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ nsConsumer::EVAL_VAR_VALUE *pValue)> SetParamValueFunction;
                std::function<nsConsumer::PC_ErrorCode(void* pValueSpec, int iDestBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, int iSrcBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec)> AssignParamValueFunction;
                std::function<void(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity)> OnMethodExitingFunction;
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity)> ProcessChangedValuesFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)> AbortRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)> AcknowledgementRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, unsigned long lSecondsToWait, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)> DelayMessageRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, wchar_t* pPrompt)> InfoRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, wchar_t* pPrompt, const nsConsumer::EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, AckCallback pCallback, void* state)> InputRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, AckCallback pCallback, void* state )> ParameterInputRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(void* pValueSpec, wchar_t* pPrompt, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)> SelectionRequestFunction;
                std::function<nsConsumer::BS_ErrorCode(int iBlockInstance, void* pValueSpec, ITEM_ID menu, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state)> UIDRequestFunction;

                int ReadValueCallCount;
                int WriteValueCallCount;
                int GetResponseCodeCallCount;
                int GetCommErrorCallCount;
                int SendCmdTransCallCount;
                int GetMoreStatusCallCount;
                int SendCommandCallCount;
                int LogMessageCallCount;
                int ListDeleteElementAtCallCount;
                int ListInsertCallCount;
                int GetDynamicAttributeCallCount;
                int GetParamValueCallCount;
                int SetParamValueCallCount;
                int AssignParamValueCallCount;
                int OnMethodExitingCallCount;
                int SendAllValuesCallCount;
                int GetUnscaledValueCallCount;
                int SetUnscaledValueCallCount;
                int AbortRequestCallCount;
                int AcknowledgementRequestCallCount;
                int DelayMessageRequestCallCount;
                int InfoRequestCallCount;
                int ParameterInputRequestCallCount;
                int InputRequestCallCount;
                int SelectionRequestCallCount;
                int UIDRequestCallCount;

                nsConsumer::EVAL_VAR_VALUE ParameterInputRequestCallbackValue;
                nsConsumer::EVAL_VAR_VALUE InputRequestCallbackValue;
                nsConsumer::EVAL_VAR_VALUE GetParamValueCallbackValue;

                nsConsumer::BS_ErrorCode ReadValueReturnValue;
                nsConsumer::BS_ErrorCode WriteValueReturnValue;
                nsConsumer::BS_ErrorCode GetResponseCodeReturnValue;
                nsConsumer::BS_ErrorCode GetCommErrorReturnValue;
                nsConsumer::BS_ErrorCode SendCmdTransReturnValue;
                nsConsumer::BS_ErrorCode GetMoreStatusReturnValue;
                nsConsumer::BS_ErrorCode SendCommandReturnValue;
                nsConsumer::BS_ErrorCode ListDeleteElementAtReturnValue;
                nsConsumer::BS_ErrorCode ListInsertReturnValue;
                nsConsumer::PC_ErrorCode GetDynamicAttributeReturnValue;
                nsConsumer::PC_ErrorCode GetParamValueReturnValue;
                nsConsumer::PC_ErrorCode SetParamValueReturnValue;
                nsConsumer::PC_ErrorCode AssignParamValueReturnValue;
                nsConsumer::BS_ErrorCode SendAllValuesReturnValue;
                nsConsumer::PC_ErrorCode GetUnscaledValueReturnValue;
                nsConsumer::PC_ErrorCode SetUnscaledValueReturnValue;
                nsConsumer::BS_ErrorCode AbortRequestReturnValue;
                nsConsumer::BS_ErrorCode AcknowledgementRequestReturnValue;
                nsConsumer::BS_ErrorCode DelayMessageRequestReturnValue;
                nsConsumer::BS_ErrorCode InfoRequestReturnValue;
                nsConsumer::BS_ErrorCode InputRequestReturnValue;
                nsConsumer::BS_ErrorCode ParameterInputRequestReturnValue;
                nsConsumer::BS_ErrorCode SelectionRequestReturnValue;
                nsConsumer::BS_ErrorCode UIDRequestReturnValue;


                virtual nsConsumer::BS_ErrorCode ReadValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec);
                virtual nsConsumer::BS_ErrorCode WriteValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec);
                virtual nsConsumer::BS_ErrorCode GetResponseCode(void* pValueSpec, INT8 *pError_Class, INT8 *pError_Code, INT16 *pAddl_code, unsigned long *err_id, unsigned long *err_member_id);
                virtual nsConsumer::BS_ErrorCode GetCommError(void* pValueSpec, unsigned long *pError);
                virtual nsConsumer::BS_ErrorCode SendCmdTrans(void* pValueSpec, int cmd_num, int trans, unsigned char *cmd_status);
                virtual nsConsumer::BS_ErrorCode GetMoreStatus(void* pValueSpec, unsigned char *more_data_status, unsigned char *more_data_info, int* more_data_info_size); // Sends Cmd 48
                virtual nsConsumer::BS_ErrorCode SendCommand(void* pValueSpec, ITEM_ID command_id, long *pErrorValue);
                virtual int isOffline(void* pValueSpec);
                virtual void LogMessage(void* pValueSpec, int iPriority, wchar_t *message); // used for _ERROR, _WARNING, _TRACE and LOG_MESSAGE
                virtual nsConsumer::BS_ErrorCode ListDeleteElementAt(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* op_ref, int start_index, int delete_count);
                virtual nsConsumer::BS_ErrorCode ListInsert(int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef);
                virtual nsConsumer::PC_ErrorCode GetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ nsConsumer::EVAL_VAR_VALUE *pValue);
                virtual nsConsumer::PC_ErrorCode SetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ nsConsumer::EVAL_VAR_VALUE *pValue);
                virtual nsConsumer::PC_ErrorCode AssignParamValue(void* pValueSpec, int iDestBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, int iSrcBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec);
                virtual void OnMethodExiting(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity);
                virtual nsConsumer::BS_ErrorCode ProcessChangedValues(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity);
                virtual nsConsumer::BS_ErrorCode AbortRequest(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                virtual nsConsumer::BS_ErrorCode AcknowledgementRequest(void* pValueSpec, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                virtual nsConsumer::BS_ErrorCode DelayMessageRequest(void* pValueSpec, unsigned long lSecondsToWait, wchar_t* pPrompt, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                virtual nsConsumer::BS_ErrorCode InfoRequest(void* pValueSpec, wchar_t* pPrompt, bool bPromptUpdate);
                virtual nsConsumer::BS_ErrorCode InputRequest(void* pValueSpec, wchar_t* pPrompt, const nsConsumer::EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, AckCallback pCallback, void* state);
                virtual nsConsumer::BS_ErrorCode ParameterInputRequest(int iBlockInstance, void* pValueSpec, wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, AckCallback pCallback, void* state );
                virtual nsConsumer::BS_ErrorCode SelectionRequest(void* pValueSpec, wchar_t* pPrompt, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                virtual nsConsumer::BS_ErrorCode UIDRequest(int iBlockInstance, void* pValueSpec, ITEM_ID menu, wchar_t* pOptions, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);

                nsConsumer::PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF* op_ref,
                    const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue);
                nsConsumer::PC_ErrorCode SetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
                    const nsEDDEngine::AttributeName attributeName,  /* in */ const nsConsumer::EVAL_VAR_VALUE * pParamValue);

				virtual void OnMethodDebugInfo(int iBlockInstance, void* pValueSpec, unsigned long lineNumber,
					/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml); // used for method debug info
                void SetInvokeCallBackDelay(int delayInMSec);
            };


            public class AsyncUICallback
            {
            public:
                static AsyncUICallback* Create(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState);

                 static AsyncUICallback* Create(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState, int delay);

                AsyncUICallback* SetCallbackSuccess();
                AsyncUICallback* SetCallbackFailure();
                AsyncUICallback* SetCallbackValue(const nsConsumer::EVAL_VAR_VALUE& value);

                void Start();

               

            private:
                AsyncUICallback(nsConsumer::IBuiltinSupport::AckCallback uiCallbackFunction, void* uiCallbackState, int delay);
                void InvokeCallback();

                int _CallbackDelay;

                static DWORD WINAPI ThreadProc(LPVOID arg);

                nsConsumer::IBuiltinSupport::AckCallback _UICallbackFunction;
                void* _UICallbackState;
                nsConsumer::BS_ErrorCode _UICallbackSuccess;
                nsConsumer::EVAL_VAR_VALUE _UICallbackValue;
            };


            public class UICallbackThread
            {
            public:
                // The following methods are called when the thread starts.
                static nsConsumer::BS_ErrorCode AbortRequestThread(nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static nsConsumer::BS_ErrorCode AcknowledgementRequestThread(nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static nsConsumer::BS_ErrorCode DelayMessageRequestThread(nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static nsConsumer::BS_ErrorCode InputRequestThread(const nsConsumer::EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static nsConsumer::BS_ErrorCode ParameterInputRequestThread(const nsConsumer::EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static nsConsumer::BS_ErrorCode SelectionRequestThread(wchar_t* pOptions, unsigned long Selection, nsConsumer::IBuiltinSupport::AckCallback pCallback, void* state);
                static void ThreadProc();

            private:
                static nsConsumer::IBuiltinSupport::AckCallback pUICallback;
                static nsConsumer::EVAL_VAR_VALUE callbackValue;
                static bool callbackOK;
                static void *callbackState;
            };

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests

#endif
