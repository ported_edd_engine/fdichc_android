
/**
 *	@(#) $Id: cmtblacc.cpp,v 1.76 1996/09/11 22:10:45 kimwolk Exp $
 *	Copyright 1995 Rosemount, Inc. - All rights reserved
 *
 *	cmtblacc.c - Connection Manager Tables Module
 *
 *	This file contains all access functions pertaining to the Connection
 *	(Active) Tables.
 */

#include "stdinc.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

//CAY this may be used by the ddi & ddi_conv
LPTSTR CConnectionMgr::get_abt_tag( BLOCK_HANDLE bh )
{
	return active_blk_tbl.list[bh]->tag;
}


void CConnectionMgr::set_abt_tag( BLOCK_HANDLE bh, LPTSTR t)
{
	active_blk_tbl.list[bh]->tag = t;
}


int CConnectionMgr::get_abt_op_index (BLOCK_HANDLE bh, OBJECT_INDEX *oi)
{
	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	get_abt_adt_offset( bh, &device_handle );

	ASSERT_RET(valid_device_handle(device_handle),
			CM_BAD_DEVICE_HANDLE) ;

	
	ASSERT_RET(oi, CM_BAD_POINTER) ;

	*oi = active_blk_tbl.list[bh]->abt_net_spec.abt_isp.op_index ;
	return(CM_SUCCESS) ;
}

int CConnectionMgr::set_abt_app_info( BLOCK_HANDLE bh, void *ai )
{
	active_blk_tbl.list[bh]->app_info = ai;
	return CM_SUCCESS;
}


int CConnectionMgr::set_abt_comm_status( BLOCK_HANDLE bh,CM_STATUS el )
{
	active_blk_tbl.list[bh]->block_comm_status = el;
	return CM_SUCCESS;
}


int CConnectionMgr::set_abt_db_handle( BLOCK_HANDLE bh,CM_DB_HANDLE h )
{
	active_blk_tbl.list[bh]->hDBHandle = h;
	return CM_SUCCESS;
}
// CAY called by ddi_tab.cpp  tr_param_offset_to_op_index
int CConnectionMgr::get_abt_param_count (BLOCK_HANDLE bh, unsigned int *pc)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(pc, CM_BAD_POINTER) ;
	*pc = active_blk_tbl.list[bh]->param_count ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_param_count (BLOCK_HANDLE bh, unsigned int pc)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->param_count = pc ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_operation( BLOCK_HANDLE bh, CM_OP op )
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->block_operation = op;
	return CM_SUCCESS;
}


int CConnectionMgr::get_abt_operation( BLOCK_HANDLE bh, CM_OP *op )
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	*op = active_blk_tbl.list[bh]->block_operation;
	return CM_SUCCESS;
}


int CConnectionMgr::get_abt_dd_blk_id (BLOCK_HANDLE bh, ITEM_ID *dbi)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dbi, CM_BAD_POINTER) ;
	*dbi = active_blk_tbl.list[bh]->dd_blk_id ;
	return(CM_SUCCESS) ;
}


// CAY called from ddi_cmi.cpp
int CConnectionMgr::set_abt_dd_blk_id (BLOCK_HANDLE bh, UINT32 dbi)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->dd_blk_id = dbi ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int *dbto)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dbto, CM_BAD_POINTER) ;
	*dbto = active_blk_tbl.list[bh]->dd_blk_tbl_offset ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int dbto)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_adt_offset (BLOCK_HANDLE bh, DEVICE_HANDLE *dh)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dh, CM_BAD_POINTER) ;
	*dh = active_blk_tbl.list[bh]->active_dev_tbl_offset ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_adt_offset( BLOCK_HANDLE bh, DEVICE_HANDLE adto)
{
	active_blk_tbl.list[bh]->active_dev_tbl_offset = adto;
	return(CM_SUCCESS);
}


int CConnectionMgr::get_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE *dth)
{
	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dth, CM_BAD_POINTER) ;
	get_abt_adt_offset( bh, &device_handle );
	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE) ;
	}
	*dth = get_adt_adtt_offset(device_handle) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_handle(BLOCK_HANDLE bh, ROD_HANDLE *rod_handle)
{
	DEVICE_HANDLE		dh ;
	DEVICE_TYPE_HANDLE	dth	;
	ROD_HANDLE			adttddh;

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	
	ASSERT_RET(rod_handle, CM_BAD_POINTER) ;

	get_abt_adt_offset( bh, &dh );
	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	
	dth = get_adt_adtt_offset(dh) ;
	if (!g_DeviceTypeMgr.valid_device_type_handle(dth)) {
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}

	g_DeviceTypeMgr.get_adtt_dd_handle( dth, &adttddh );
	*rod_handle = adttddh;

	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_dev_tbls(BLOCK_HANDLE bh, void **ddt)
{
	DEVICE_HANDLE		device_handle ;
	DEVICE_TYPE_HANDLE	device_type_handle ;
	
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	get_abt_adt_offset( bh, &device_handle );

	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE) ;
	}

	device_type_handle = get_adt_adtt_offset(device_handle) ;

	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}

	g_DeviceTypeMgr.get_adtt_dd_dev_tbls( device_type_handle, ddt );

	if ( *ddt == NULL ) {
		return CM_BAD_POINTER;
	}

	return(CM_SUCCESS) ;
}


/*********************************************************************
 *
 *	Name: ConnectDeviceManufacturerTableEntry
 *	
 *	Description:
 *		If the manufacturer table entry is in use, increment the usage variable.  If it is 
 *		not in use, then create a new manufacturer table element and initialize its members.
 *
 *		This code was broken out of the connection manager function set_abt_hart_find because it addresses shared
 *		data in the active device manufacturer table.
 *
 *
 **********************************************************************/


int CDeviceTypeMgr::ConnectDeviceManufacturerTableEntry(DD_DEVICE_ID dd_device_id, DEVICE_TYPE_HANDLE dth)
{
	// Connect up the Device Manufacturer table entry
	int r_code = CM_SUCCESS;
	DD_MANUFACTURER_ID dd_manufacturer_id = {0};

	dd_manufacturer_id.ddid_manufacturer = dd_device_id.ddid_manufacturer;
    PS_Tcscpy(dd_manufacturer_id.base_dd_path, dd_device_id.base_dd_path);

	//	Search the Active Device Manufacturer Table for the DD device ID.
	DEVICE_MANUFACTURER_HANDLE dmh = active_dev_manufacturer_tbl.SearchDeviceManufacturer(&dd_manufacturer_id);

	//	Check if the device manufacturer is already in use.
	if (active_dev_manufacturer_tbl.DeviceManufacturerHandleIsValid(dmh))
	{
		//	Increment the device manufacturer's usage, set the Active
		//	Device Manufacturer Table offset of the device.
		active_dev_manufacturer_tbl.GetElement(dmh)->SetDctUsage(active_dev_manufacturer_tbl.GetElement(dmh)->GetDctUsage() + 1) ;
		g_DeviceTypeMgr.SetDevManufacturerTblOffsetInDevTypeTbl(dth, dmh) ;
	}
	else
	{
		//	Allocate a new Active Device Manufacturer Table element and initialize its members.
		r_code = active_dev_manufacturer_tbl.AddElement(&dmh, &dd_manufacturer_id) ;
		if (r_code == CM_SUCCESS)
		{
		   	g_DeviceTypeMgr.SetDevManufacturerTblOffsetInDevTypeTbl(dth, dmh) ;
		}
	}
	return r_code;
}

// CAY revert the following function

int CConnectionMgr::set_abt_hart_find_cnf(wchar_t *dd_file_path, BLOCK_HANDLE bh, HART_IDENTIFY_CNF *fc)
{
	int							r_code = -2;		// CAY init to unkown error
	DEVICE_HANDLE				dh = 0;
	DD_DEVICE_ID				dd_device_id = {0};
	DEVICE_HANDLE				previous_dh = -2;	// CAY init to bad handle value
	

   	ASSERT_RET(fc, CM_BAD_POINTER) ;

	if (!valid_block_handle(bh))
	{ 
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	// 	Check that the network type is valid
	get_abt_adt_offset( bh, &dh );
	ASSERT_RET(valid_device_handle(dh), CM_BAD_DEVICE_HANDLE) ;

	set_abt_operation(bh, CM_HAVE_VALUE_OP) ;  // CAY 28



	if (get_adt_identified(dh) == DEVICE_NOT_IDENTIFIED) // CAY 0
	{  // CAY we load these with defaults and 0 don't think we have too
				
		r_code = CM_NO_DEVICE;  //(-2400-12)
		// CAY this if should fail & we don't have a connection to a device ever!
		if ( (r_code == SUCCESS) && (previous_dh != dh) && (get_adt_identified(previous_dh) == DEVICE_IDENTIFIED) )
		{
			set_adt_address_type(previous_dh, get_address_type(dh) | LONG_ADDR_TYPE) ;
			set_adt_station_address(previous_dh, get_adt_station_address(dh)) ;
			set_adt_identified(dh, DEVICE_DISAPPEARED) ;
		}
		else  //CAY this must run and load dd_device_id
		{
			set_adt_address_type(dh, get_address_type(dh) | LONG_ADDR_TYPE) ;
			set_adt_identified(dh, DEVICE_IDENTIFIED) ;
		
			dd_device_id.ddid_manufacturer = fc->mfrid ;
			dd_device_id.ddid_device_type =	fc->mfr_dev_type ;
			dd_device_id.ddid_device_rev = fc->xmtr_spec_rev ;
			dd_device_id.ddid_dd_rev = -1;
			dd_device_id.base_dd_path = hart_dd_path ;
			dd_device_id.dd_path = dd_file_path;
			dd_device_id.bUseBasePathUnmodified = false;

			set_abt_dd_blk_tbl_offset(bh, 0) ;

			//	Search the Active Device Type Table for the DD device ID.
			DEVICE_TYPE_HANDLE dth = g_DeviceTypeMgr.ct_device_type_search(&dd_device_id);

			//	Check if the device type is already in use.
			// CAY this if fails
			if (g_DeviceTypeMgr.valid_device_type_handle(dth))
			{
				//	Increment the device type's usage, set the Active Device Type Table offset of the device, and return.
				g_DeviceTypeMgr.set_adtt_usage(dth, g_DeviceTypeMgr.get_adtt_usage(dth) + 1) ;
				set_adt_adtt_offset(dh, dth) ;
				r_code = CM_SUCCESS;
			}
			else
			{
				//	Allocate a new Active Device Type Table element and initialize its members.
				// CAY note to self: ** the next line must execute,
				// and the two sets in the if block **
				r_code = g_DeviceTypeMgr.ct_new_device_type(&dth) ;
				if (r_code == CM_SUCCESS)
				{
		   			g_DeviceTypeMgr.set_adtt_dd_device_id(dth, &dd_device_id) ;
		   			set_adt_adtt_offset(dh, dth) ;
					r_code = g_DeviceTypeMgr.ConnectDeviceManufacturerTableEntry(dd_device_id,dth);
	         	}
			}
		}
	}
	
	//CAY somehow r_code must = SUCCESS 0 before we return
	return(r_code) ;
}


LPTSTR CConnectionMgr::get_adt_device_tag( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->device_tag;
}


int CConnectionMgr::set_adt_device_tag( DEVICE_HANDLE dh, LPTSTR dt )
{
	active_dev_tbl.list[dh]->device_tag = dt;
	return CM_SUCCESS;
}


void CConnectionMgr::set_adt_time( DEVICE_HANDLE dh, CM_TIME t )
{
	active_dev_tbl.list[dh]->deviceTime = t;
}


DEVICE_MANUFACTURER_HANDLE CConnectionMgr::get_adt_man_tbl_offset( DEVICE_HANDLE dh )
{
	DEVICE_MANUFACTURER_HANDLE dmh = -1;
	if (valid_device_handle(dh))
	{
		DEVICE_TYPE_HANDLE dth = get_adt_adtt_offset(dh);
		dmh = g_DeviceTypeMgr.GetDevManufacturerTblOffsetFromDevTypeTbl(dth);
	}
	return dmh;
}


int CConnectionMgr::get_adt_dd_handle(DEVICE_HANDLE dh, ROD_HANDLE *rod_handle)
{
	DEVICE_TYPE_HANDLE	device_type_handle ;
	ROD_HANDLE adttddh = 0;
	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	ASSERT_RET(rod_handle, CM_BAD_POINTER) ;
	
	device_type_handle = get_adt_adtt_offset(dh) ;
	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}

	g_DeviceTypeMgr.get_adtt_dd_handle( device_type_handle, &adttddh );
	*rod_handle = adttddh;
	return(CM_SUCCESS) ;
}


void CConnectionMgr::set_adt_dd_handle( DEVICE_HANDLE dh, ROD_HANDLE *rod_handle )
{
	DEVICE_TYPE_HANDLE dth;
	dth = get_adt_adtt_offset( dh );
	g_DeviceTypeMgr.set_adtt_dd_handle( dth, rod_handle );
}


void CDeviceTypeMgr::set_adtt_dd_handle( DEVICE_TYPE_HANDLE dth, ROD_HANDLE *rod_handle )
{
	active_dev_type_tbl.list[dth]->dd_rod_handle = *rod_handle;
}


DD_DEVICE_ID* CConnectionMgr::get_adt_dd_device_id( DEVICE_HANDLE dh )
{
	DEVICE_TYPE_HANDLE	device_type_handle ;
	
	if (!valid_device_handle(dh)) {
		return(NULL) ;
	}

	device_type_handle = get_adt_adtt_offset(dh) ;
	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(NULL) ;
	}

	return g_DeviceTypeMgr.get_adtt_dd_device_id( device_type_handle );
	
}


int CConnectionMgr::get_adt_dd_dev_tbls(DEVICE_HANDLE dh, void **ddt)
{
	DEVICE_TYPE_HANDLE	device_type_handle ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(ddt, CM_BAD_POINTER) ;
	device_type_handle = get_adt_adtt_offset(dh) ;
	g_DeviceTypeMgr.get_adtt_dd_dev_tbls( device_type_handle, ddt );
	return (CM_SUCCESS) ;
}


DD_DEVICE_ID* CDeviceTypeMgr::get_adtt_dd_device_id (DEVICE_TYPE_HANDLE dth ) const
{
	if (!valid_device_type_handle(dth)) {
		return(NULL) ;
	}
	
	return &active_dev_type_tbl.list[dth]->dd_device_id;
}


void CDeviceTypeMgr::set_adtt_dd_device_id( DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi )
{
	memcpy(&(active_dev_type_tbl.list[dth]->dd_device_id), ddi,sizeof(DD_DEVICE_ID));
}


int CDeviceTypeMgr::get_adtt_dd_handle (DEVICE_TYPE_HANDLE dth, ROD_HANDLE *rod_handle) const
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(rod_handle, CM_BAD_POINTER) ;
	*rod_handle = active_dev_type_tbl.list[dth]->dd_rod_handle;

	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_usage( DEVICE_TYPE_HANDLE dth ) const
{
	return active_dev_type_tbl.list[dth]->usage;
}


void CDeviceTypeMgr::set_adtt_usage( DEVICE_TYPE_HANDLE dth, int u )
{
	active_dev_type_tbl.list[dth]->usage = u;
}


CM_COMPAT CDeviceTypeMgr::get_adtt_compatibility( DEVICE_TYPE_HANDLE dth )
{
	return active_dev_type_tbl.list[dth]->compatibility;
}


void CDeviceTypeMgr::set_adtt_compatibility( DEVICE_TYPE_HANDLE dth, CM_COMPAT c )
{
	active_dev_type_tbl.list[dth]->compatibility = c;
}


int CDeviceTypeMgr::get_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void **ddt) const
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(ddt, CM_BAD_POINTER) ;
	*ddt = active_dev_type_tbl.list[dth]->dd_dev_tbls ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_dd_dev_tbls (DEVICE_TYPE_HANDLE dth, void *ddt)
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->dd_dev_tbls = ddt ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_syminfo (DEVICE_TYPE_HANDLE dth, SYMINFO** si) const
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(si, CM_BAD_POINTER) ;
	*si =  active_dev_type_tbl.list[dth]->syminfo ;
	return (CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_syminfo (DEVICE_TYPE_HANDLE dth, SYMINFO *si)
{
	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->syminfo = si ;
	return(CM_SUCCESS) ;
}


void CConnectionMgr::set_adt_view( DEVICE_HANDLE dh, CM_VIEW v )
{
	active_dev_tbl.list[dh]->deviceView = v;
}


LPTSTR CConnectionMgr::get_adt_desc( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->desc;
}


void CConnectionMgr::set_adt_desc( DEVICE_HANDLE dh, LPTSTR d )
{
	active_dev_tbl.list[dh]->desc = d;
}


LPTSTR CConnectionMgr::get_adt_date( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->date;
}


void CConnectionMgr::set_adt_date( DEVICE_HANDLE dh, LPTSTR d )
{
	active_dev_tbl.list[dh]->date = d;
}


CM_IDENTIFIED_TYPE CConnectionMgr::get_adt_identified( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->identified;
}


void CConnectionMgr::set_adt_identified( DEVICE_HANDLE dh, CM_IDENTIFIED_TYPE i )
{
	active_dev_tbl.list[dh]->identified = i;
}


int CConnectionMgr::get_adt_usage( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->usage;
}


void CConnectionMgr::set_adt_usage( DEVICE_HANDLE dh, int u )
{
	active_dev_tbl.list[dh]->usage = u;
}


NETWORK_HANDLE CConnectionMgr::get_adt_network( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->network;
}


void CConnectionMgr::set_adt_network( DEVICE_HANDLE dh, NETWORK_HANDLE n )
{
	active_dev_tbl.list[dh]->network = n;
}


HART_ADDR_TYPE CConnectionMgr::get_address_type( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->AddressType;
}


void CConnectionMgr::set_adt_address_type( DEVICE_HANDLE dh, HART_ADDR_TYPE ht )
{
	active_dev_tbl.list[dh]->AddressType = ht;
}


int CConnectionMgr::get_adt_station_address( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->station_address;
}


void CConnectionMgr::set_adt_station_address( DEVICE_HANDLE dh, int sa )
{
	active_dev_tbl.list[dh]->station_address = sa;
}


CM_OP CConnectionMgr::get_adt_operation( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->device_operation;
}


void CConnectionMgr::set_adt_operation( DEVICE_HANDLE dh, CM_OP op)
{
	active_dev_tbl.list[dh]->device_operation = op;
}


CM_STATUS CConnectionMgr::get_adt_comm_status( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->device_comm_status;
}


void CConnectionMgr::set_adt_comm_status( DEVICE_HANDLE dh, CM_STATUS s )
{
	active_dev_tbl.list[dh]->device_comm_status = s;
}

void* CConnectionMgr::get_adt_app_info( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->app_info;
}


void CConnectionMgr::set_adt_app_info( DEVICE_HANDLE dh, void* ai )
{
	active_dev_tbl.list[dh]->app_info = ai;
}


DEVICE_TYPE_HANDLE CConnectionMgr::get_adt_adtt_offset( DEVICE_HANDLE dh )
{
	return active_dev_tbl.list[dh]->active_dev_type_tbl_offset;
}


void CConnectionMgr::set_adt_adtt_offset( DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto )
{
	active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto;
}


int CConnectionMgr::valid_block_handle (BLOCK_HANDLE bh)
{
	return	((bh >= 0) && (bh < active_blk_tbl.count) 
			&& (active_blk_tbl.list[bh])) ;
}


int CConnectionMgr::valid_device_handle (DEVICE_HANDLE dh)
{
	return ((dh >= 0) && (dh < active_dev_tbl.count)
			&& (active_dev_tbl.list[dh])) ;
}


int CDeviceTypeMgr::valid_device_type_handle (DEVICE_TYPE_HANDLE dth) const
{
	return ((dth >= 0) && (dth < active_dev_type_tbl.count) 
			&& (active_dev_type_tbl.list[dth])) ;
}


int CRodMgr::valid_rod_handle( ROD_HANDLE rh )
{
	return ((rh >= 0) && (rh < rod_tbl.count) 	&& (rod_tbl.list[rh]));
}

int CDeviceTypeMgr::valid_rod_handle( ROD_HANDLE rh )
{
	return m_RodMgr.valid_rod_handle(rh);
}
