/**
 *	@(#) $Id: evl_type.c,v 1.3 1996/01/04 20:12:51 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to type information.
 */

#include "stdinc.h"

#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif


/*
 *  UNKNOWN parameter macros
 */

#define UNKNOWN_TYPE 0
#ifdef ISP
#define UNKNOWN_SIZE 0
#endif

/*
 * Constant Size values
 */

#define DEFAULT_SIZE       1
#define FLOAT_SIZE         4
#define DURATION_SIZE      6
#define TIME_SIZE          6
#define TIME_VALUE_SIZE	   4
#define DATE_AND_TIME_SIZE 7
#define DOUBLE_SIZE        8

#ifdef HART
#define DATE_SIZE          3
#endif


/*
 * Mask used to peek at the next tag in the chunk
 */

#ifdef HART

#define TAG_MASK 0x7f

#define HART_ARRAYNAME_INDICATOR 0	/* used only for type INDEX */
#define HART_SUBATTR_INDICATOR 1/* list of subattributes exists */

#endif




/*********************************************************************
 *
 *	Name:	ddl_shrink_range()
 *
 *	ShortDesc:	Condense list containing MIN/MAX values.
 *
 *	Description:
 *		Condense list containing MIN/MAX values.  The range list is
 *		"limit" elements long.  Only "size" number of elements are
 *		needed.  The list is condensed to contain "size" number of
 *		elements.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_shrink_range_list(
RANGE_DATA_LIST *list)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_RANGE_LIST);
#endif

	if (!list) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (!list->list) {
		ASSERT_DBG(!list->count && !list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (list->count == list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (list->count == 0) {
		ddl_free_range_list(list, FREE_ATTR);
	}
	else {
		list->limit = list->count;

		list->list = (EXPR *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof(*list->list)));
		if (!list->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name:	ddl_free_range_list()
 *
 *	ShortDesc:	Free the list containing MIN/MAX range values.
 *
 *	Description:
 *		Free the list containing MIN/MAX range values.  The MIN/MAX
 *		range value list is no longer needed.  Return the memory used
 *		by "list" to available memory.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

void
ddl_free_range_list(
RANGE_DATA_LIST *list,
uchar           dest_flag)
{

	if (list == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
		list->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list, and set the values in list.
		 */

		free((void *) list->list);
		list->list = NULL;
		list->limit = 0;
	}

	list->count = 0;

}

/*********************************************************************
 *
 *	Name:	ddl_init_range_list()
 *
 *	ShortDesc:	Initialize the list for storing MIN/MAX range values.
 *
 *	Description:
 *		Initialize the list in which MIN/MAX range values are to be
 *		stored.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - Initialized list for storing MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static void
ddl_init_range_list(
RANGE_DATA_LIST *list)
{
	if( list ) 
	{
		//
		// If there is no list, make sure the count/limit are consistent, then
		// return.
		//
		if (list->list == NULL) 
		{
			ASSERT_DBG(!list->count && !list->limit);
			list->limit = 0;
		}
		else
		{
			for (int nIndex = 0; nIndex < list->count; nIndex++) 
			{
				list->list[nIndex].val.ll = 0;
				list->list[nIndex].size = 0;
				list->list[nIndex].type = DDSUNUSED;
			}
		}
		list->count = 0;
	}

}


/*********************************************************************
 *
 *	Name:	ddl_add_value()
 *
 *	ShortDesc:	Add another MIN/MAX value to the MIN/MAX list.
 *
 *	Description:
 *		Add another MIN/MAX value to the MIN/MAX range value list.
 *		If the type of the MIN/MAX value does not match the type
 *		of the variable, the type of the MIN/MAX value is "promoted"
 *		to match the type of the variable.  Type promotion is
 *		performed before the value is added to the list.
 *		  If "var_type" equals UNKNOWN_TYPE.  The value is NOT
 *		promoted to the type of the variable.  The value (along
 *		with its size and type) is added to the list of MIN/MAX
 *		values.
 *
 *	Inputs:
 *		value -	item to be added to list.
 *		var_type - variable type.
 *		var_size - variable size.
 *		position -	location in list where item is to be added.
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		DDL_LARGE_VALUE
 *		return codes from:
 *			a)	ddl_promote_to()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_add_value(
EXPR           *value,
unsigned short  var_type,
unsigned short  var_size,
DDL_UINT        position,
RANGE_DATA_LIST *list)
{
	EXPR           *tmp;	/** temporary list pointer */
	EXPR           *endp;	/** ptr to end of min/max list */

#ifdef HART
	unsigned short  my_var_type;	/** local variable type */
	int             rs;

#endif

	ASSERT_DBG(list);

#ifdef ISPTEST
	TEST_FAIL(DDL_ADD_VALUE);
#endif

	/*
	 * Add a min or max value to the RANGE_DATA_LIST structure. Expand the
	 * list if necessary, and parse the value. The position parameter
	 * specifies which min/max value (0,1,...) is being referenced.
	 */

	if ((unsigned short) position > list->limit) {
		do {
			list->limit += RANGE_DATA_INC;
		} while ((unsigned short) position > list->limit);
		tmp = (EXPR *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof(*tmp)));
		if (!tmp) {
			return DDL_MEMORY_ERROR;
		}

		list->list = tmp;
		endp = &list->list[list->limit];
		for (tmp = &list->list[list->count]; tmp < endp; tmp++) {
			tmp->val.ll = 0;
			tmp->size = 0;
			tmp->type = DDSUNUSED;
		}
		list->count = (unsigned short) position;
	}
	else if (list->count < (unsigned short) position) {
		list->count = (unsigned short) position;
	}

	tmp = &list->list[position - 1];
	if (value) {		/* a real value is to be added to the list */

#ifdef HART

		/*
		 * Make sure the EXPR value, and the type and size are
		 * compatible.
		 */

		if (var_type != UNKNOWN_TYPE) {

			switch (var_type) {

			case INTEGER:
			case UNSIGNED:
            case FLOATG_PT:
            case DOUBLEG_PT:
				my_var_type = var_type;
				break;

			default:
				return DDL_ENCODING_ERROR;
			}

			if (my_var_type != value->type) {
				rs = ddl_promote_to(value, my_var_type, var_size);
				if (rs != DDL_SUCCESS) {
					return rs;
				}
			}

			if (var_size < value->size) {
				return DDL_LARGE_VALUE;
			}
		}
#endif

		/*
		 * Add the value.
		 */

		tmp->size = value->size;
		tmp->type = value->type;
		switch (value->type) {
		case INTEGER:
			tmp->val.ll = value->val.ll;
			break;
		case UNSIGNED:
			tmp->val.ull = value->val.ull;
			break;
        case FLOATG_PT:
			tmp->val.f = value->val.f;
			break;
        case DOUBLEG_PT:
			tmp->val.d = value->val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}

	}
	else {

		/*
		 * If an existing structure was passed in, We must be sure to
		 * write over existing values in the structure.
		 */

		tmp->type = DDSUNUSED;	/* default values are to be used */
		tmp->size = 0;
		tmp->val.ll = 0;
	}

	return DDL_SUCCESS;
}



#ifdef HART
/*********************************************************************
 *
 *	Name:	ddl_parse_arth_options
 *
 *	ShortDesc:	Parse TYPE subattributes.
 *
 *	Description:
 *		Parse TYPE subattributes associated with arithmetic variable
 *		type.  Arithmetic variable types include: signed/unsigned
 *		integers, float and doubles.  TYPE subattributes include:
 *		EDIT/DISPLAY FORMAT, MIN/MAX VALUE and SCALING_FACTOR.
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		type - pointer to type of the TYPE subattribute.
 *		vtype - variable type.
 *		vsize - variable size.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		tag - identity of TYPE subattribute (ie. MIN/MAX VALUE).
 *		env_info - environment information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			a)	ddl_string_choice()
 *			b)	ddl_expr_choice()
 *			c)	DDL_PARSE_INTEGER()
 *			d)	ddl_add_value()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_parse_arth_options(
unsigned char **chunkp,
DDL_UINT       *size,
TYPEINFO       *type,
unsigned short  vtype,
unsigned short  vsize,
OP_REF_LIST    *depinfo,
int            *data_valid,
DDL_UINT        tag,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	EXPR            value;	/** local var for storing expression. */
	DDL_UINT        which;	/** index number */
	int             local_valid;	/** if TRUE, value has been modified */

	/*
	 * This routine evaluates all of the arithmetic options of the TYPE
	 * information for a valiable. Each part of the TYPE field returns a
	 * different kind of structure, so the TYPEINFO structure contains
	 * information about the part of the TYPE field currently being parsed
	 * (type.tag), and a pointer to whatever structure is appropriate.
	 * 
	 * The possible arithmetic parts of the TYPE field, and their returned
	 * structures are: type.tag					struct
	 * DISPLAY_FORMAT_TAG				STRING EDIT_FORMAT_TAG
	 * TRING SCALING_FACTOR_TAG				EXPR
	 * MIN_VALUE_TAG				RANGE_DATA_LIST
	 * MAX_VALUE_TAG				RANGE_DATA_LIST
	 */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_ARTH_OPTIONS);
#endif

	switch (tag) {
	case DISPLAY_FORMAT_TAG:

		/*
		 * Parse a DISPLAY_FORMAT_TAG by calling ddl_string_choice
		 */

		if (type && type->tag == DISPLAY_FORMAT_TAG) {
			rc = ddl_string_choice(chunkp, size,
				type->ptr ? (STRING *) type->ptr : (STRING *) NULL,
				depinfo, type->ptr ? data_valid : (int *) NULL, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		else {
			rc = ddl_string_choice(chunkp, size, (STRING *) NULL,
				(OP_REF_LIST *) NULL, (int *) NULL, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		break;

	case EDIT_FORMAT_TAG:

		/*
		 * Parse a EDIT_FORMAT_TAG by calling ddl_string_choice
		 */

		if (type && type->tag == EDIT_FORMAT_TAG) {
			rc = ddl_string_choice(chunkp, size,
				type->ptr ? (STRING *) type->ptr : (STRING *) NULL,
				depinfo, type->ptr ? data_valid : (int *) NULL, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		else {
			rc = ddl_string_choice(chunkp, size, (STRING *) NULL,
				(OP_REF_LIST *) NULL, (int *) NULL, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		break;

	case SCALING_FACTOR_TAG:

		/*
		 * Parse a SCALING_FACTOR_TAG by evaluating an expression
		 */

		if (type && type->tag == SCALING_FACTOR_TAG) {
			rc = ddl_expr_choice(chunkp, size,
				type->ptr ? (EXPR *) type->ptr : (EXPR *) NULL,
				depinfo, type->ptr ? data_valid : (int *) NULL, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		else {
			rc = ddl_expr_choice(chunkp, size, (EXPR *) NULL,
				(OP_REF_LIST *) NULL, (int *) NULL, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		break;

	case MIN_VALUE_TAG:

		/*
		 * Parse a MIN_VALUE_TAG by evaluating an expression, and then
		 * calling ddl_add_value to add the value to the
		 * RANGE_DATA_LIST.
		 */

		DDL_PARSE_INTEGER(chunkp, size, &which);

		if (type && type->tag == MIN_VALUE_TAG) {
			local_valid = 0;
			rc = ddl_expr_choice(chunkp, size,
				type->ptr ? &value : (EXPR *) NULL, depinfo,
				type->ptr ? &local_valid : (int *) NULL, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (type->ptr) {
				rc = ddl_add_value(local_valid ? &value : (EXPR *) NULL,
					vtype, vsize, which, (RANGE_DATA_LIST *) type->ptr);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				*data_valid = TRUE;
			}
		}
		else {
			rc = ddl_expr_choice(chunkp, size, (EXPR *) NULL,
				(OP_REF_LIST *) NULL, (int *) NULL, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		break;

	case MAX_VALUE_TAG:

		/*
		 * Parse a MAX_VALUE_TAG by evaluating an expression, and then
		 * calling ddl_add_value to add the value to the
		 * RANGE_DATA_LIST.
		 */

		DDL_PARSE_INTEGER(chunkp, size, &which);

		if (type && type->tag == MAX_VALUE_TAG) {
			local_valid = 0;
			rc = ddl_expr_choice(chunkp, size,
				type->ptr ? &value : (EXPR *) NULL, depinfo,
				type->ptr ? &local_valid : (int *) NULL, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (type->ptr) {
				rc = ddl_add_value(local_valid ? &value : (EXPR *) NULL,
					vtype, vsize, which, (RANGE_DATA_LIST *) type->ptr);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				*data_valid = TRUE;
			}
		}
		else {
			rc = ddl_expr_choice(chunkp, size, (EXPR *) NULL,
				(OP_REF_LIST *) NULL, (int *) NULL, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
		break;
	default:
		return DDL_ENCODING_ERROR;
	}
	return DDL_SUCCESS;
}

#endif

#ifdef HART
/*********************************************************************
 *
 *	Name:	ddl_parse_options
 *
 *	ShortDesc:	Loop to parse all TYPE subattributes.
 *
 *	Description:
 *		Loop which calls ddl_parse_arth_options() to parse each TYPE
 *		subattribute associated with the current variable.
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		type - pointer to type of the TYPE subattribute.
 *		vtype - variable type.
 *		vsize - variable size.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		env_info - environment information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 *			DDL_PARSE_TAG()
 *			ddl_parse_arth_options()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_parse_options(
unsigned char **chunkp,
DDL_UINT       *size,
TYPEINFO       *type,
unsigned short  vtype,
unsigned short  vsize,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	DDL_UINT        tag;	/** identity of TYPE subattribute */

	/*
	 * This routine evaluates a series of arithmetic options of the TYPE
	 * information for a valiable. Each part of the TYPE field returns a
	 * different kind of structure, so the TYPEINFO structure contains
	 * information about the part of the TYPE field currently being parsed
	 * (type.tag), and a pointer to whatever structure is appropriate.
	 * 
	 * The possible arithmetic parts of the TYPE field, and their returned
	 * structures are: type.tag					struct
	 * DISPLAY_FORMAT_TAG				STRING EDIT_FORMAT_TAG
	 * TRING SCALING_FACTOR_TAG				EXPR
	 * MIN_VALUE_TAG				RANGE_DATA_LIST
	 * MAX_VALUE_TAG				RANGE_DATA_LIST
	 */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_OPTIONS);
#endif

	while (*size > 0) {

		/*
		 * Parse the tag (one of the same as listed above), and then
		 * parse the option by calling ddl_parse_arth_options.
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

		rc = ddl_parse_arth_options(chunkp, size, type, vtype, vsize,
			depinfo, data_valid, tag, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		/*
		 * looking for an early exit.
		 */

		if ((type) && (*data_valid) && ((type->tag == DISPLAY_FORMAT_TAG) ||
				(type->tag == EDIT_FORMAT_TAG) ||
				(type->tag == SCALING_FACTOR_TAG))) {

			/*
			 * Get out early.  We do not need to parse the entire
			 * TYPE chunk.
			 */

			break;
		}

	}
	return DDL_SUCCESS;
}

#endif




/*********************************************************************
 *
 *	Name:	ddl_parse_type_size()
 *
 *	ShortDesc:	Parse variable size from binary
 *
 *	Description:
 *		Parse variable size from binary (ie. one-byte integer)
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		typesize - variable size.
 *
 *	Outputs:
 *		typesize - variable size (in bytes)
 *
 *	Returns:
 *		DDL_SUCCESS
 *		return codes from:
 * 			DDL_PARSE_INTEGER()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_parse_type_size(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *typesize)
{
	int             rc;

	ASSERT_DBG(chunkp && *chunkp && size && typesize);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_TYPE_SIZE);
#endif

	/*
	 * Parse out the length of the item.  The length of an item is encoded
	 * with a first byte of zero, and then the following byte(s) encode the
	 * length.  If the first byte of the chunk is non-zero, then there is
	 * no length encoded, and the default value of 1 is returned.
	 */

	if (*size == 0 || **chunkp) {
		*typesize = DEFAULT_SIZE;
	}
	else {

		/*
		 * Skip the zero, and parse the size.
		 */

		(*chunkp)++;
		(*size)--;

		DDL_PARSE_INTEGER(chunkp, size, typesize);
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	ddl_parse_type()
 *
 *	ShortDesc:	Parse TYPE information from binary.
 *
 *	Description:
 *		Parse TYPE information (including subattribute information)
 *		for the current variable from the binary.  TYPE and subattribute
 *		information is all contained in the same chunk of binary.
 *		Variable type and dependency information are returned.  If a
 *		type value is found (every variable must have a TYPE) "data_valid"
 *		will be set to TRUE.
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		vtype - variable type.
 *		vsize - variable size.
 *		env_info - environment information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		vtype - variable type.
 *		vsize - variable size.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			a)	DDL_PARSE_TAG -
 *			b)	ddl_parse_type_size -
 *			c)	ddl_parse_options -
 *			d)	ddl_enums_choice -
 *			e)	ddl_parse_item_id -
 *			f)	DDL_PARSE_INTEGER -
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_parse_type(
unsigned char **chunkp,
DDL_UINT       *size,
TYPEINFO       *type,
OP_REF_LIST    *depinfo,
int            *data_valid,
unsigned short *vtype,
unsigned short *vsize,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	unsigned char  *leave_pointer;	/** chunk ptr for early exit */
	int             rc;
	TYPE_SIZE      *vartype;
	DDL_UINT        len;	/** length of binary associated w/ tag parsed */
	DDL_UINT        tag;	/** parsed tag */
	DDL_UINT        typesize;	/** size of tag item (2-byte int) */

#ifdef HART
	DDL_UINT        t;	/** parsed tag */

#endif

	/*
	 * This routine evaluates all of the TYPE information for a variable.
	 * Each part of the TYPE field returns a different kind of structure,
	 * so the TYPEINFO structure contains information about the part of the
	 * TYPE field currently being parsed (type.tag), and a pointer to
	 * whatever structure is appropriate.
	 * 
	 * The possible parts of the TYPE field, and their returned structures
	 * are: type.tag					struct TYPE_TAG
	 * AR_DATA_TYPE	(arithmetic type) 	DISPLAY_FORMAT_TAG TRING
	 * EDIT_FORMAT_TAG					STRING
	 * SCALING_FACTOR_TAG				EXPR MIN_VALUE_TAG
	 * ANGE_DATA_LIST MAX_VALUE_TAG		RANGE_DATA_LIST
	 * ENUMERATOR_SEQLIST_TAG			ENUM_VALUE_LIST 0
	 * EFERENCE item_array name)
	 */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_TYPE);
#endif

	/*
	 * Parse the tag to find out if this a select statement, and if/else,
	 * or a simple assignment.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	/*
	 * Calcuate the chunk pointer at exit (we may be able to use it later
	 * for an early exit).
	 */

	leave_pointer = *chunkp + len;

	if (type && type->tag == TYPE_TAG) {
		vartype = (TYPE_SIZE *) type->ptr;
	}
	else {
		vartype = NULL;
	}

	if (vtype) {
		*vtype = (unsigned short) tag;
	}

	switch (tag) {
	case INTEGER:		/* INTEGER */
	case UNSIGNED:		/* UNSIGNED_INTEGER */
		if (len > *size) {
			return DDL_ENCODING_ERROR;
		}
		*size -= len;

		/*
		 * If there is nothing to return (value pointer is NULL), we
		 * can just update the chunk pointer and return.
		 */

		if (!type) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * Find the size of the integer
		 */

		rc = ddl_parse_type_size(chunkp, &len, &typesize);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = (unsigned short) tag;
			vartype->size = *vsize;
		}

#ifdef HART

		/*
		 * Parse any remaining options
		 */

		if (len > 0) {

			/*
			 * The tag must be a 1. Indicating that a list of type
			 * subattributes remains to be parsed.
			 */

			DDL_PARSE_TAG(chunkp, &len, &t, (DDL_UINT *) NULL_PTR);

			if (t != HART_SUBATTR_INDICATOR) {
				return DDL_ENCODING_ERROR;
			}

			/*
			 * Arithmetic type options are handled via
			 * ddl_parse_options
			 */

			if (type && type->tag == DISPLAY_FORMAT_TAG
				|| type->tag == EDIT_FORMAT_TAG
				|| type->tag == SCALING_FACTOR_TAG
				|| type->tag == MAX_VALUE_TAG || type->tag == MIN_VALUE_TAG) {
				rc = ddl_parse_options(chunkp, &len, type,
					(unsigned short) tag, *vsize, depinfo,
					data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				/*
				 * Update chunkp.  We may have exited
				 * ddl_parse_options() early.
				 */

				*chunkp = leave_pointer;

			}
			else {
				rc = ddl_parse_options(chunkp, &len, (TYPEINFO *) NULL,
					(unsigned short) tag, *vsize, depinfo,
					(int *) NULL, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
#endif
		break;

    case FLOATG_PT:		/* FLOATG_PT */
    case DOUBLEG_PT:		/* DOUBLEG_PT */
		if (len > *size) {
			return DDL_ENCODING_ERROR;
		}
		*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

		if (!type) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * Save the item type and size
		 */

        if (tag == FLOATG_PT) {
			*vsize = FLOAT_SIZE;
		}
		else {
			*vsize = DOUBLE_SIZE;
		}

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = (unsigned short) tag;
			vartype->size = *vsize;
		}

#ifdef HART

		/*
		 * Parse any remaining options
		 * 
		 * Arithmetic type options are handled via ddl_parse_options
		 */

		if (len > 0) {

			/*
			 * Variable size is implicit.  Length > 0 indicates
			 * TYPE subattribute information remains to be parsed.
			 * 
			 */

			if (type && type->tag == DISPLAY_FORMAT_TAG
				|| type->tag == EDIT_FORMAT_TAG
				|| type->tag == SCALING_FACTOR_TAG
				|| type->tag == MAX_VALUE_TAG || type->tag == MIN_VALUE_TAG) {
				rc = ddl_parse_options(chunkp, &len, type,
					(unsigned short) tag, *vsize, depinfo, data_valid,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				/*
				 * Update chunkp.  We may have exited
				 * ddl_parse_options() early.
				 */

				*chunkp = leave_pointer;

			}
			else {
				rc = ddl_parse_options(chunkp, &len, (TYPEINFO *) NULL,
					(unsigned short) tag, *vsize, depinfo, (int *) NULL,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
#endif
		break;

	case ENUMERATED:
	case BIT_ENUMERATED:
		if (len > *size) {
			return DDL_ENCODING_ERROR;
		}
		*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

		if (!type) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * Find the size of the enumerated item
		 */

		rc = ddl_parse_type_size(chunkp, &len, &typesize);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = (unsigned short) tag;
			vartype->size = *vsize;
		}

#ifdef HART

		/*
		 * Parse any remaining options
		 */

		if (len > 0) {

			/*
			 * The tag must be a 1. Indicating that a list of type
			 * subattributes remains to be parsed.
			 */

			DDL_PARSE_TAG(chunkp, &len, &tag, (DDL_UINT *) NULL_PTR);

			if (tag != HART_SUBATTR_INDICATOR) {
				return DDL_ENCODING_ERROR;
			}

			/*
			 * Enumeration values are handled via ddl_enums_choice
			 */

			if (type && type->tag == ENUMERATOR_SEQLIST_TAG) {
				rc = ddl_enums_choice(chunkp, &len, type, depinfo,
					data_valid, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			else {
				rc = ddl_enums_choice(chunkp, &len, (TYPEINFO *) NULL,
					depinfo, (int *) NULL, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
#endif
		break;

	case INDEX:		/* INDEX */
		if (len > *size) {
			return DDL_ENCODING_ERROR;
		}
		*size -= len;

		/*
		 * If there is nothing to return (the value pointer is NULL),
		 * we can just update the chunk pointer and return.
		 */

		if (!type) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * Find the size of the index
		 */

		rc = ddl_parse_type_size(chunkp, &len, &typesize);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = INDEX;
			vartype->size = *vsize;
		}

#ifdef HART

		/*
		 * Parse any remaining type subattribute information.
		 */

		if (len > 0) {

			/*
			 * The tag must be a 1. Indicating that a list of type
			 * subattributes remains to be parsed.
			 */

			DDL_PARSE_TAG(chunkp, &len, &tag, (DDL_UINT *) NULL_PTR);

			if (tag != HART_SUBATTR_INDICATOR) {
				return DDL_ENCODING_ERROR;
			}

			/*
			 * Array name options are handled by calling
			 * ddl_parse_item_id
			 */

			if (type && type->tag == HART_ARRAYNAME_INDICATOR && type->ptr) {
				*data_valid = TRUE;
				rc = ddl_parse_item_id(chunkp, &len, (ITEM_ID *) type->ptr,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
			else {
				rc = ddl_parse_item_id(chunkp, &len, (ITEM_ID *) NULL, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
#endif
		break;

	case ASCII:		/* ASCII */

		/*
		 * Find the size of the item
		 */

		DDL_PARSE_INTEGER(chunkp, size, &typesize);

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = ASCII;
			vartype->size = *vsize;
		}
		break;

#ifdef HART
	case PACKED_ASCII:	/* PACKED_ASCII */

		/*
		 * Find the size of the item
		 */

		DDL_PARSE_INTEGER(chunkp, size, &typesize);

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = PACKED_ASCII;
			vartype->size = *vsize;
		}
		break;
#endif				/* HART */

	case PASSWORD:		/* PASSWORD */

		/*
		 * Find the size of the item
		 */

		DDL_PARSE_INTEGER(chunkp, size, &typesize);

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = PASSWORD;
			vartype->size = *vsize;
		}
		break;

	case BITSTRING:	/* BITSTRING */

		/*
		 * Find the size of the item
		 */

		DDL_PARSE_INTEGER(chunkp, size, &typesize);

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = BITSTRING;
			vartype->size = *vsize;
		}
		break;

#ifdef HART
	case HART_DATE_FORMAT:		/* DATE */

		*vsize = (unsigned short) DATE_SIZE;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = HART_DATE_FORMAT;
			vartype->size = *vsize;
		}
		break;
#endif

	case TIME_VALUE:
		*vsize = (unsigned short) TIME_VALUE_SIZE;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = TIME_VALUE;
			vartype->size = *vsize;
		}
		break;

	case TIME:		/* TIME */

		*vsize = (unsigned short) TIME_SIZE;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = TIME;
			vartype->size = *vsize;
		}
		break;

	case DATE_AND_TIME:	/* DATE_AND_TIME */

		*vsize = (unsigned short) DATE_AND_TIME_SIZE;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = DATE_AND_TIME;
			vartype->size = *vsize;
		}
		break;

	case DURATION:		/* DURATION */

		*vsize = (unsigned short) DURATION_SIZE;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = DURATION;
			vartype->size = *vsize;
		}
		break;

	case EUC:

		/*
		 * Find the size of the item
		 */

		DDL_PARSE_INTEGER(chunkp, size, &typesize);

		*vsize = (unsigned short) typesize;

		/*
		 * Save the item type and size
		 */

		if (vartype) {
			*data_valid = TRUE;
			vartype->type = EUC;
			vartype->size = *vsize;
		}
		break;

	default:
		return DDL_ENCODING_ERROR;
	}
	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name:	eval_typeinfo()
 *
 *	ShortDesc:	Evaluates all of the TYPE information for a variable.
 *
 *	Description:
 * 		This routine evaluates all of the TYPE information for a variable.
 * 		Each part of the TYPE field returns a different kind of structure,
 * 		so the TYPEINFO structure contains information about the part of the
 * 		TYPE field currently being parsed (ptag), and a pointer to whatever
 * 		structure is appropriate.
 *
 *		This routine also takes care of import information.
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - size of the binary
 *		ptag - item for which information is desired.
 *		value - value of the desired item.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		value - value of the desired item.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		DDL_MEMORY_ERROR
 *		DDL_DEFAULT_ATTR
 *		return codes from:
 *			a)	ddl_parse_type
 *			b)	ddl_parse_tag_func
 *			c)	DDL_PARSE_INTEGER
 *			d)	ddl_expr_choice
 *			e)	ddl_add_value
 *			f)	ddl_parse_arth_options
 *			g)	ddl_free_output_class
 *			h)	ddl_free_refs
 *			i)	ddl_parse_one_enum
 *			j)	ddl_insert_enum
 *			k)	ddl_shrink_depinfo
 *			l)	ddl_shrink_range_list
 *			m)	ddl_shrink_enum_list
 *			n)	ddl_free_depinfo
 *			o)	ddl_free_range_list
 *			p)	ddl_free_enum_list
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/


static int
eval_typeinfo(
unsigned char  *chunk,
DDL_UINT        size,
int             ptag,
void           *value,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;
	int             data_valid;	/** if TRUE, value has been modified */
	TYPEINFO        type;	/** local storage for attribute info */
	unsigned short  vtype;	/** variable type */
	unsigned short  vsize;	/** variable size */

#ifdef HART
	unsigned short  x;	/** loop index variable */
	unsigned short  match_position;	/** ENUM has been matched to a value */
	DDL_UINT        ftag;	/** local tag */
	DDL_UINT        tvalue;	/** temporary location for parsed integer */
	DDL_UINT        tag;	/** parsed tab from binary */
	DDL_UINT        len;	/* length of binary associated w/ tag */
	DDL_UINT        flen;	/** local tag length */
	ENUM_VALUE     *tmp = NULL;	/** temporary ENUM ptr */
	ENUM_VALUE_LIST *enums;	/** list ptr for ENUM values */
	unsigned char  *continue_pointer;	/** chunk ptr for early exit */

#endif

#ifdef PROCESS_IMPORTS
	RANGE_DATA_LIST *range_listp;	/** list ptr for MIN/MAX values */
	EXPR            fvalue;	/** local storage for parsed expression */
	DDL_UINT        which;	/** MIN/MAX index number */

#endif

	ASSERT_DBG(chunk);

#ifdef ISPTEST
	TEST_FAIL(EVAL_TYPEINFO);
#endif

	/*
	 * This routine evaluates all of the TYPE information for a variable.
	 * Each part of the TYPE field returns a different kind of structure,
	 * so the TYPEINFO structure contains information about the part of the
	 * TYPE field currently being parsed (ptag), and a pointer to whatever
	 * structure is appropriate.
	 * 
	 * The possible parts of the TYPE field, and their returned structures
	 * are: ptag						struct TYPE_TAG
	 * AR_DATA_TYPE	(arithmetic type) 	DISPLAY_FORMAT_TAG TRING
	 * EDIT_FORMAT_TAG					STRING
	 * SCALING_FACTOR_TAG				EXPR MIN_VALUE_TAG
	 * ANGE_DATA_LIST MAX_VALUE_TAG		RANGE_DATA_LIST
	 * ENUMERATOR_SEQLIST_TAG		ENUM_VALUE_LIST 0 EFERENCE
	 * item_array name)
	 */

	data_valid = FALSE;

	type.tag = ptag;
	type.ptr = value;

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_parse_type(&chunk, &size, &type, depinfo, &data_valid,
		(unsigned short *) &vtype, &vsize, env_info, var_needed);
	if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR)) {
		goto err_exit;
	}

	/*
	 * Handle any Additions, Redefinitions, and Deletions of the type
	 * attributes.  The way these are handled is determined by the tag type
	 * of the original definition.  So, switch on that tag type.
	 */

	switch (vtype) {
	case IF_TAG:		/* IF */
	case SELECT_TAG:	/* SELECT */
	case INDEX:		/* INDEX */
	case ASCII:		/* ASCII */
	case PASSWORD:		/* PASSWORD */
	case BITSTRING:	/* BITSTRING */
#ifdef HART
	case PACKED_ASCII:	/* PACKED_ASCII */
	case HART_DATE_FORMAT:		/* DATE */
#endif
	case TIME:		/* TIME */
	case DATE_AND_TIME:	/* DATE_AND_TIME */
	case DURATION:		/* DURATION */
	case TIME_VALUE:
	case EUC:		/* Extended Unix Code */
		break;

	case INTEGER:		/* INTEGER */
	case UNSIGNED:		/* UNSIGNED_INTEGER */
    case FLOATG_PT:		/* FLOATG_PT */
    case DOUBLEG_PT:		/* DOUBLEG_PT */

#ifdef PROCESS_IMPORTS

		/*
		 * Arithmetic type options (DISPLAY_FORMAT_TAG,
		 * EDIT_FORMAT_TAG, SCALING_FACTOR_TAG, MIN_VALUE_TAG, and
		 * MAX_VALUE_TAG) may be deleted or redefined.
		 */

		while (size) {

			/*
			 * Find out if this is an deletion, or redefinition.
			 */

			rc = ddl_parse_tag_func(&chunk, &size, &tag, &len);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			if (len > size) {
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
			size -= len;
			continue_pointer = chunk + len;

			/*
			 * Determine which part of the TYPE is being altered.
			 */

			rc = ddl_parse_tag_func(&chunk, &len, &ftag, &flen);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}


			if (type.tag != (int) ftag
				|| (type.tag != DISPLAY_FORMAT_TAG
					&& type.tag != EDIT_FORMAT_TAG
					&& type.tag != SCALING_FACTOR_TAG
					&& type.tag != MAX_VALUE_TAG
					&& type.tag != MIN_VALUE_TAG)) {

				/*
				 * We just skip this alteration.
				 */

				chunk = continue_pointer;
				continue;
			}

			/*
			 * Handle the deletion or redefinition
			 */

			switch (tag) {
			case DELETE_TAG:
				if (!type.ptr) {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}

				if (ftag == MIN_VALUE_TAG || ftag == MAX_VALUE_TAG) {

					/*
					 * For min & max values, find which
					 * value is being deleted, and mark it
					 * deleted.
					 */

					range_listp = (RANGE_DATA_LIST *) type.ptr;
					if (!range_listp->list) {
						rc = DDL_ENCODING_ERROR;
						goto err_exit;
					}

					rc = ddl_parse_integer_func(&chunk, &len, &which);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}

					if ((DDL_UINT) range_listp->count < which) {
						rc = DDL_ENCODING_ERROR;
						goto err_exit;
					}

					range_listp->list[which - 1].val.ull = DDL_HUGE_INTEGER;
					range_listp->list[which - 1].size = 0;
					range_listp->list[which - 1].type = DDSUNUSED;
					if (range_listp->count == 1) {
						data_valid = FALSE;
					}

					/*
					 * if (we are deleting the last item in
					 * the list), decrement the size of the
					 * list. else ( item is not the last in
					 * the list ) the list element has
					 * already been re-initialized.
					 */

					if (range_listp->count == which) {
						range_listp->count--;
					}
				}
				else

					/*
					 * For the other tags, the deletion
					 * means the field is not valid.
					 */

					data_valid = FALSE;
				break;

			case REDEFINE_TAG:
				if (!type.ptr) {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}

				if (ftag == MIN_VALUE_TAG || ftag == MAX_VALUE_TAG) {

					/*
					 * For min & max values, find which
					 * value is being redefine, and mark it
					 * parse the new expression to replace
					 * the old.
					 */

					range_listp = (RANGE_DATA_LIST *) type.ptr;
					if (!range_listp->list) {
						rc = DDL_ENCODING_ERROR;
						goto err_exit;
					}

					rc = ddl_parse_integer_func(&chunk, &flen, &which);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}

					if ((DDL_UINT) range_listp->count + 1 < which) {
						rc = DDL_ENCODING_ERROR;
						goto err_exit;
					}

					rc = ddl_expr_choice(&chunk, &flen, &fvalue, depinfo,
						&data_valid, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}

					rc = ddl_add_value(&fvalue, vtype, vsize, which,
						range_listp);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				else {

					/*
					 * For the other tags, parse the new
					 * value to replace the old.
					 */

					rc = ddl_parse_arth_options(&chunk, &flen, &type, vtype,
						vsize, depinfo, &data_valid, ftag, env_info,
						var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				break;
				/* case ADD_TAG: not allowed */
			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
#endif
		break;

	case ENUMERATED:	/* ENUMERATED */
	case BIT_ENUMERATED:	/* BIT_ENUMERATED */

#ifdef PROCESS_IMPORTS

		/*
		 * Enumeration Values may be added, deleted, or redefined.
		 */

		while (size) {

			/*
			 * Find out if this is an addition, deletion, or
			 * redefinition.
			 */

			rc = ddl_parse_tag_func(&chunk, &size, &tag, &len);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			if (len > size) {
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
			size -= len;
			continue_pointer = chunk + len;

			/*
			 * Determine which part of the TYPE is being altered.
			 */

			rc = ddl_parse_tag_func(&chunk, &len, &ftag, &flen);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			if (ftag != ENUMERATOR_TAG) {
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}

			if (type.tag != ENUMERATOR_SEQLIST_TAG) {

				/*
				 * Just skip this redefinition.
				 */

				chunk = continue_pointer;
				continue;
			}

			enums = (ENUM_VALUE_LIST *) (type.ptr);

			switch (tag) {
			case DELETE_TAG:

				/*
				 * Find the value to delete.
				 */

				rc = ddl_parse_integer_func(&chunk, &len, &tvalue);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				/*
				 * Locate that value in the ENUM_VALUE_LIST
				 */

				match_position = ((ENUM_VALUE_LIST *) type.ptr)->count;
				for (x = 0; x < match_position; x++) {
					if (enums->list[x].val == tvalue) {
						match_position = x;
					}
				}

				/*
				 * Free up the deleted object, and squish the
				 * list down.
				 */

				if (match_position < enums->count - 1) {
					ddl_free_output_class(
						&enums->list[match_position].status.oclasses);
					memcpy((char *) &enums->list[match_position],
						(char *) &enums->list[match_position + 1],
						(int) ((enums->count - match_position - 1) *
							sizeof *enums->list));
					memset((char *) &enums->list[enums->count - 1], 0,
						sizeof *enums->list);
				}
				else if (match_position == enums->count - 1) {
					ddl_free_output_class(
						&enums->list[match_position].status.oclasses);
				}
				else {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}
				enums->count--;
				break;
			case REDEFINE_TAG:

				/*
				 * Malloc a new ENUM_VALUE for the
				 * redefinition.
				 */

				tmp = (ENUM_VALUE *) calloc((size_t) 1, sizeof *tmp);
				if (!tmp) {
					rc = DDL_MEMORY_ERROR;
					goto err_exit;
				}

				tmp->status.oclasses.count = 0;
				tmp->status.oclasses.limit = 0;
				tmp->status.oclasses.list = NULL;

				/*
				 * Parse the redefinition.
				 */

				rc = ddl_parse_one_enum(&chunk, &flen, &type, tmp,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit0;
				}

				/*
				 * Locate the value to redefine
				 */

				tvalue = tmp->val;
				for (match_position = 0, x = 0; x < enums->count; x++) {
					if (enums->list[x].val == tvalue) {
						match_position = x;
						x = enums->count;
					}
				}
				if (match_position >= enums->count) {
					rc = DDL_ENCODING_ERROR;
					goto err_exit0;
				}

				/*
				 * Free up the old definition, and replace it
				 * with the new one.
				 */

				ddl_free_output_class(
					&enums->list[match_position].status.oclasses);
				memcpy((char *) &enums->list[match_position], (char *) tmp,
					sizeof(ENUM_VALUE));
				free((void *) tmp);
				break;
			case ADD_TAG:

				/*
				 * Increase the ENUM_VALUE_LIST if necessary
				 */

				if (enums->count >= enums->limit) {
					enums->limit += ENUM_INC;
					tmp = (ENUM_VALUE *) realloc((void *) enums->list,
						(size_t) (enums->limit * sizeof(*tmp)));
					if (!tmp) {
						rc = DDL_MEMORY_ERROR;
						goto err_exit;
					}

					enums->list = tmp;
					memset((char *) &enums->list[enums->count], 0,
						ENUM_INC * sizeof *enums->list);
				}
				tmp = enums->list + enums->count;
				match_position = enums->count++;

				/*
				 * Parse the new ENUM_VALUE
				 */

				rc = ddl_parse_one_enum(&chunk, &flen, &type, tmp,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				/*
				 * Make sure the new item doesn't duplicate an
				 * existing one.
				 */

				tvalue = tmp->val;
				for (x = 0; x < match_position; x++) {
					if (enums->list[x].val == tvalue) {
						rc = DDL_ENCODING_ERROR;
						goto err_exit;
					}
				}

				/*
				 * ENUM_VALUEs are sorted so that all with
				 * values greater or equal to 250 MUST follow
				 * all with values less than 249.  So, place
				 * this new item in front of the 250s, if
				 * necessary.
				 */

				match_position = enums->count;
				for (x = 0; x < match_position; x++) {
					if (enums->list[x].val > 249 && tvalue < 250) {
						match_position = x;
					}
				}

				/*
				 * Insert the new ENUM_VALUE
				 */

				if (match_position < enums->count) {
					rc = ddl_insert_enum(enums, match_position,
						tmp);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				break;
			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
#endif
		break;
	default:
		rc = DDL_ENCODING_ERROR;
		goto err_exit;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

#ifdef HART

	/*
	 * Shrink the appropriate structure.
	 */

	switch (type.tag) {
	case TYPE_TAG:
	case DISPLAY_FORMAT_TAG:
	case EDIT_FORMAT_TAG:
	case SCALING_FACTOR_TAG:
	case HART_ARRAYNAME_INDICATOR:
		break;
	case MIN_VALUE_TAG:
	case MAX_VALUE_TAG:
		rc = ddl_shrink_range_list((RANGE_DATA_LIST *) type.ptr);
		break;
	case ENUMERATOR_SEQLIST_TAG:
		rc = ddl_shrink_enum_list((ENUM_VALUE_LIST *) type.ptr);
		break;
	default:
		goto err_exit;
	}
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}
#endif

	if (value && !data_valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

#ifdef HART
err_exit0:
	ASSERT_DBG(tmp);
	ddl_free_output_class(&tmp->status.oclasses);
	free((void *) tmp);
#endif
err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
#ifdef HART
	switch (type.tag) {
	case TYPE_TAG:
	case DISPLAY_FORMAT_TAG:
	case EDIT_FORMAT_TAG:
	case SCALING_FACTOR_TAG:
		break;
	case MIN_VALUE_TAG:
	case MAX_VALUE_TAG:
		ddl_free_range_list((RANGE_DATA_LIST *) type.ptr, FREE_ATTR);
		break;
	case ENUMERATOR_SEQLIST_TAG:
		ddl_free_enum_list((ENUM_VALUE_LIST *) type.ptr, FREE_ATTR);
		break;
	case HART_ARRAYNAME_INDICATOR:
		break;
	default:
		CRASH_RET(DDL_SERVICE_ERROR);
	}
#endif
	return rc;
}

/*******************************************************
*
* Name: eval_attr_type
*
* ShortDesc: evaluate variable type.
*
* Description:
*
*	The eval_attr_type function evaluates the datatype
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If type is not a null pointer, the datatype
*	is returned in type. If depinfo is not
*	a null pointer, dependency information about the datatype
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		vartype - variable type.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		vartype - variable type.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_type(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	TYPE_SIZE* vartype = static_cast<TYPE_SIZE*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_TYPE);
#endif

	if (vartype) {
		vartype->type = 0;	/** initialize output parameter **/
		vartype->size = 0;	/** initialize output parameter **/
	}

	return eval_typeinfo(chunk, size, TYPE_TAG, (void *) vartype,
		depinfo, env_info, var_needed);
}

/**********************************************
*
* Name: eval_attr_display_format
*
* ShortDesc: evaluate the display format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_display_format function evaluates the display format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the display format
*	is returned in format. If depinfo is not
*	a null pointer, dependency information about the display format
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - display format of variable
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		format - display format of variable
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string(0
*
* Author:
*	Steve Beyerl
*
*****************/

int
eval_attr_display_format(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	STRING*			format = static_cast<STRING*>(voidP);

#ifdef ISP
	int             rc;
	DDL_UINT        tag;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_DISPLAY_FORMAT);
#endif

	ddl_free_string(format);

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, DISPLAY_FORMAT_TAG,
			(void *) format, depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code:
	 *    The first byte of an ISP chunk will be a DISPLAY_
	 *	FORMAT_TAG.
	 *****************************************************/

	/*
	 * Strip off the DISPLAY_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != TIME_FORMAT_TAG && tag != DISPLAY_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
#else
	return DDL_SUCCESS;
#endif
}

/**********************************************
*
* Name: eval_attr_edit_format
*
* ShortDesc: evaluate the edit format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_edit_format function evaluates the edit format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the edit format
*	is returned in format. If depinfo is not
*	a null pointer, dependency information about the edit format
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - edit format of desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		format - edit format of desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string()
*
* Author:
*	Steve Beyerl
*
******************************************************/

int
eval_attr_edit_format(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	STRING*			format = static_cast<STRING*>(voidP);

#ifdef ISP
	int             rc;
	DDL_UINT        tag;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_EDIT_FORMAT);
#endif

	ddl_free_string(format);

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, EDIT_FORMAT_TAG,
			(void *) format, depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code:
	 *    The first byte of an ISP chunk will be an
	 *	EDIT_FORMAT_TAG.
	 *****************************************************/

	/*
	 * Strip off the EDIT_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != EDIT_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
#else
	return DDL_SUCCESS;
#endif
}

/**********************************************
*
* Name: eval_attr_scaling_factor
*
* ShortDesc: evaluate the scaling factor of an arithmetic variable.
*
* Description:
*
*	The eval_attr_scaling_factor function evaluates the scaling factor
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If factor is not a null pointer, the scaling factor
*	is returned in factor. If depinfo is not
*	a null pointer, dependency information about the scaling factor
*	is returned in depinfo.
*
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_attr_expr()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_scaling_factor(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	EXPR*			factor = static_cast<EXPR*>(voidP);

#ifdef ISP
	int             rc;
	DDL_UINT        tag;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_SCALING_FACTOR);
#endif

	if (factor) {
		memset((char *) factor, 0, sizeof(EXPR));
	}

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, SCALING_FACTOR_TAG,
			(void *) factor, depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code:
	 *    The first byte of an ISP chunk will be a
	 *	SCALING_FACTOR_TAG
	 *****************************************************/

	/*
	 * Strip off the SCALING_FACTOR_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != SCALING_FACTOR_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_attr_expr(chunk, size, factor, depinfo, env_info, var_needed);
#else
	return DDL_SUCCESS;
#endif
}


/**********************************************
*
* Name: eval_attr_time_scale
*
* ShortDesc: evaluate the time scale of an TIME_VALUE variable.
*
* Description:
*
*	The eval_attr_time_scale function evaluates the time scale
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*   If depinfo is not
*	a null pointer, dependency information about the scale
*	is returned in depinfo.
*
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		voidP - time scale of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		voidP - time scale of the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_attr_expr()
*
* Author:
*	Ying Xu
*
********************************************************/

int
eval_attr_time_scale(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    * /*depinfo*/,
ENV_INFO       * /*env_info*/,
OP_REF         * /*var_needed*/)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);

#ifdef ISP
	int             rc;
	DDL_UINT        tag;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(DDL_EVAL_TIME_SCALE);
#endif

	if (u_long)
	{
		*u_long = 0;
	}

	/*
	 * Strip off the TIME_SCALE_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != TIME_SCALE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	DDL_PARSE_INTEGER(&chunk, &size, u_long);


	return DDL_SUCCESS;
}


/**********************************************
*
* Name: eval_attr_min_values
*
* ShortDesc: evaluate the min values of an arithmetic variable
*
* Description:
*	The eval_attr_min_values function evaluates the minimum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the minimum values
*	are returned in values. If depinfo is not
*	a null pointer, dependency information about the minimum values
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
* 	Steve Beyerl
*
********************************************************/

int
eval_attr_min_values(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	RANGE_DATA_LIST* values = static_cast<RANGE_DATA_LIST*>(voidP);

#ifdef ISP
	int             rc;
	int             data_valid;	/** set if "data_value" has been modified **/
	EXPR            data_value;	/* min value parsed from binary */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_MIN_VALUES);
#endif

	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, MIN_VALUE_TAG, (void *) values,
			depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	modified ISP code: (12/22/93 stevbey)
	 *	  This code was modified to support the current status of the ddl spec.
	 *    The first byte of an ISP chunk will be a
	 *	MIN_VALUE_TAG
	 *****************************************************/

	/*
	 * Strip off the MIN_VALUE_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MIN_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	while (size > 0) {	/* assuming multiple MIN_VALUES */

		/***
		 * Parse a MIN_VALUE_TAG by:
		 * a) get the number of this occurence of MIN_VALUE (ie. which).
		 * 	(i.e. MIN_VALUE1, MIN_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		data_valid = FALSE;

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL,
			(unsigned short) UNKNOWN_TYPE, UNKNOWN_SIZE, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
#else
	return DDL_SUCCESS;
#endif
}

/**********************************************
*
* Name: eval_attr_max_values
*
* ShortDesc: evaluate the max values of an arithmetic variable
*
* Description:
*	The eval_attr_max_values function evaluates the maximum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the maximum values
*	are returned in values. If depinfo is not
*	a null pointer, dependency information about the maximum values
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_max_values(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	RANGE_DATA_LIST* values = static_cast<RANGE_DATA_LIST*>(voidP);

#ifdef ISP
	int             rc;
	int             data_valid;	/* set if "data_value" has been
					 * modified */
	EXPR            data_value;	/* max value parsed from binary */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_MAX_VALUES);
#endif


	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, MAX_VALUE_TAG, (void *) values,
			depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code: (Modified to match DDL SPEC)
	 *    The first byte of an ISP chunk will be a
	 *	MAX_VALUE_TAG
	 *****************************************************/

	/*
	 * Strip off the MAX_VALUE_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MAX_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	while (size > 0) {	/* assuming multiple MAX_VALUES */

		/***
		 * Parse a MAX_VALUE_TAG by:
		 * a) get the number of this occurence of MAX_VALUE.
		 * 	(i.e. MAX_VALUE1, MAX_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		data_valid = FALSE;

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL,
			(unsigned short) UNKNOWN_TYPE, UNKNOWN_SIZE, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
#else
	return DDL_SUCCESS;
#endif
}

/**********************************************
*
* Name: eval_attr_enum
*
* ShortDesc: evaluate the enumeration items of an enumerated variable
*
* Description:
*	The eval_attr_enum function evaluates the enumeration items
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If enums is not a null pointer, the enumeration
*	items are returned in enums. If depinfo is not
*	a null pointer, dependency information about the enumeration items
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_enum(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	ENUM_VALUE_LIST* enums = static_cast<ENUM_VALUE_LIST*>(voidP);

#ifdef ISP
	TYPEINFO        type;
	int             data_valid;
	int             rc;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ENUM);
#endif

	if (enums) {
		ddl_free_enum_list(enums, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, ENUMERATOR_SEQLIST_TAG,
			(void *) enums, depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code:
	 *    The first byte of an ISP chunk will be a
	 *	ENUMERATOR_SEQLIST_TAG
	 *****************************************************/

	type.tag = ENUMERATOR_SEQLIST_TAG;
	type.ptr = (void *) enums;
	data_valid = FALSE;

	/*
	 * Enumeration values are handled via ddl_enums_choice
	 */

	rc = ddl_enums_choice(&chunk, &size, &type, depinfo, &data_valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);

		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_enum_list(enums);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);
		return rc;
	}

	if (type.ptr && (data_valid == FALSE)) {
		return DDL_DEFAULT_ATTR;
	}

	return rc;

#else
	return DDL_SUCCESS;
#endif
}

/**********************************************
*
* Name: eval_attr_debug_info()

* ShortDesc: find the symbol name in the debug information
*
* Description:
*	This function parses the symbol name from the debug information
*	of the chunk
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		sym_name - the string of the symbol name
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		sym_name - symbol name of the item requested
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a) ddl_parse_string()
*			b) DDL_PARSE_TAG()
*
* Author:
*	Katie Frost
*
********************************************/


int eval_attr_debug_info( unsigned char  *chunk, DDL_UINT size, void *voidP, OP_REF_LIST *depinfo,
						  ENV_INFO *env_info, OP_REF *var_needed)
{
	STRING*		sym_name = static_cast<STRING*>(voidP);

	DDL_UINT        tag = 0, len = 0;
	int             rc = 0;

	DDL_PARSE_TAG(&chunk, &size, &tag, &len);

	if (tag != DEBUG_INFO_TAG) 
	{
		return DDL_ENCODING_ERROR;
	}
	
	if( len > 2 )
	{
		size -= len;
		rc = ddl_parse_string(&chunk, &size, sym_name, depinfo, env_info, var_needed);
	}
	else
	{
		size = 0;
		rc = DDL_BAD_VALUE_TYPE;
	}
	return rc;
}


/**********************************************
*
* Name: eval_attr_arrayname
*
* ShortDesc: evaluate the item_array name of an index variable.
*
* Description:
*
*	The eval_attr_arrayname function evaluates the item_array name
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If item_arrayref is not a null pointer, the item_array name
*	is returned in item_arrayref. If depinfo is not
*	a null pointer, dependency information about the item_array name
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*				information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/


int
eval_attr_arrayname(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	ITEM_ID*		item_arrayref = static_cast<ITEM_ID*>(voidP);

#ifdef ISP
	DDL_UINT        tag;
	int             rc;

#endif

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ITEM_ARRAYNAME);
#endif

	if (item_arrayref) {
		*item_arrayref = 0;
	}


#ifdef HART
	/******************************************************
 	 *	HART code:
	 *    The first byte of a HART chunk will be a variable
	 *  type tag.  02 through 19 (decimal) have been reserved
 	 *  for variable type tags.  The Most Significant Bit of
	 *  a tag has been reserved to indicate whether or not an
	 *  explicit length is associated with the tag.
 	 ******************************************************/

	if ((*chunk & TAG_MASK) < MIN_ATTR) {	/* HART code */
		return eval_typeinfo(chunk, size, HART_ARRAYNAME_INDICATOR,
			(void *) item_arrayref, depinfo, env_info, var_needed);
	}
#endif

#ifdef ISP
	/*****************************************************
 	 *	ISP code:
	 *    The first byte of an ISP chunk will be a
	 *	ITEM_ARRAYNAME_TAG
	 *****************************************************/

	/*
	 * Strip off the ITEM_ARRAYNAME_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != ITEM_ARRAYNAME_TAG) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Parse the variable item_array name. INDEX info is NOT conditional.
	 */

	return ddl_parse_item_id(&chunk, &size, item_arrayref, depinfo,
		env_info, var_needed);
#else
	return DDL_SUCCESS;
#endif
}


/*********************************************************************
 *
 *	Name: link_min_max
 *	ShortDesc: Parse a min or max attr for the HART linker
 *
 *  Author: Christian Gustafson
 *********************************************************************/

int
link_min_max(
unsigned char  *chunk,
DDL_UINT        size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc = DDL_SUCCESS; /* return code */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK          *chunk_ptr;	/* temp pointer for chunk list */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);


	/*
	 * Strip off the MIN_VALUE_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if ((tag != MIN_VALUE_TAG) && (tag != MAX_VALUE_TAG)) {
		rc = DDL_ENCODING_ERROR;
		goto err_exit;
	}

	depinfo->count = 0;

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared


	while (size > 0) {	/* assuming multiple MIN_VALUES */

		/***
		 * Parse a MIN_..._TAG by:
		 * a) get the number of this occurence of MIN_VALUE (ie. which).
		 * 	(i.e. ..._VALUE1, ..._VALUE2, ... )
		 * b) evaluating the expression,
		 */

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		rc = link_cond(&chunk, &size, &chunk_list_ptr, depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}

		/*
		 * If a list of chunks exists send them to link_expr
		 */

		if (chunk_list_ptr.size > 0) {

			chunk_ptr = chunk_list_ptr.list;

			while (chunk_list_ptr.size > 0) {	/* Parse them */


				rc = link_expr(&(chunk_ptr->chunk),
					&(chunk_ptr->size),
					depinfo,
					env_info,
					var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				chunk_ptr++;
				chunk_list_ptr.size--;
			}
		}
	}

err_exit:

	/* delete chunk list if it was reallocated */

	if (chunk_list_ptr.list != chunk_list) {

		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	if (rc != DDL_SUCCESS) {

		depinfo->count = 0;
	}

	return rc;
}



/*********************************************************************
 *
 *	Name: link_array_name
 *	ShortDesc: parse index array name for the HART linker
 *
 *  Author: Christian Gustafson
 *********************************************************************/


int
link_array_name(
unsigned char  *chunk,
DDL_UINT        size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	DDL_UINT        tag;
	int             rc;


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);


	/*
	 * Strip off the ITEM_ARRAYNAME_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != ITEM_ARRAYNAME_TAG) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Parse the variable item_array name. INDEX info is NOT conditional.
	 */

	return link_ref(&chunk, &size, depinfo, env_info, var_needed, (DDL_UINT) 0);

}
