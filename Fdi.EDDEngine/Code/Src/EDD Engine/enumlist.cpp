#include <stdafx.h>
#include "enumlist.h"
#include "Ole/EDDLMethInterp/MiServerShared.h"

#include <sstream>

EnumList::EnumList(unsigned long long i)
{
	pugi::xml_node enumerationDataNode = m_DOMDoc.append_child(ELMTNAME_ENUM_DATA);

	m_DDItem = enumerationDataNode.append_child(ELMTNAME_DD_ITEMS);

	pugi::xml_attribute itemIdAttr = m_DDItem.append_attribute(ATTRNAME_DD_ITEM_ID);
	itemIdAttr.set_value(i);
}


//There are some non friendly XML characters that might be possible
std::wstring  EnumList::StripOffNPChars( wchar_t* EnumDescription )
{
	std::wstring  sRetVal;
	int nStringLength = (int)wcslen(EnumDescription);

	for(int i = 0; i < nStringLength; i++ )
	{
		if( ((EnumDescription[i] >= 0x00)&&(EnumDescription[i] <= 0x08)) || 
			(EnumDescription[i] == 0x0B) ||
			(EnumDescription[i] == 0x0C) ||
			((EnumDescription[i] >= 0x0e)&&(EnumDescription[i] <= 0x1f)) ) 
		{
			//thow this away
		}
		else
		{
			sRetVal += EnumDescription[i];
		}
	}
	
	return sRetVal;
}

bool EnumList::AddEnumElement(unsigned long long  nNumber, wchar_t* sDescription, wchar_t* sHelp )
{
	bool bReturn = false;

	try
	{
		pugi::xml_node enumSelectionNode = m_DDItem.append_child(ELMTNAME_ENUM_SELECTION);
		pugi::xml_attribute itemIdAttr = enumSelectionNode.append_attribute(ATTRNAME_NUMBER);
		itemIdAttr.set_value(nNumber);
		
		pugi::xml_attribute descAttr = enumSelectionNode.append_attribute(ATTRNAME_DESCRIPTION);


		std::wstring sTemp_desc = StripOffNPChars(sDescription);
		if(sTemp_desc.length() > 0)
		{
			if ( wcscmp( sTemp_desc.c_str(), L" ") == 0)
			{ 
				descAttr.set_value(USEFORBLANKDESCRIPTION);
			}
			else
			{
				descAttr.set_value(sTemp_desc.c_str());
			}					
		}		
		else
		{
			descAttr.set_value(USEFORBLANKDESCRIPTION);
		}

		if(sHelp != NULL)
		{
			std::wstring sTempHelp =  StripOffNPChars( sHelp);
			if( sTempHelp.length() > 0 )
			{
				pugi::xml_attribute helpAttr = enumSelectionNode.append_attribute(ATTRNAME_HELP);
				helpAttr.set_value(sTempHelp.c_str());
			}
		}
		bReturn = true;

	}
#if 0
	catch( _com_error ce)
	{
		_bstr_t s = ce.ErrorMessage();
		bReturn = false;

	}
#endif
	catch(...)
	{
		bReturn = false;
	}


	return bReturn;
}

#if 0
BSTR EnumList::GetList()
#else
std::wstring EnumList::GetList()
#endif
{
	std::wstringstream outStream;
	m_DOMDoc.save(outStream);

	if (outStream.str().length() > 0)
	{
#if 0
		return SysAllocString(outStream.str().c_str());
#else
		return outStream.str();
#endif
	}
	
	return NULL;
}
