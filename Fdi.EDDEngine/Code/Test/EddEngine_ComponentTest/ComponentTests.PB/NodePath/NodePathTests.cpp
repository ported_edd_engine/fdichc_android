#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_request_the_Item_ID_for_Standard_Device_Root_Menu_with_PB
        {
        public:
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0001.DDL.bin");
                engine->Initialize();                

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"device_root_menu", itemID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(3u, *itemID);
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_not_existing_SymbolName_with_PB
        {
        public:
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0001.DDL.bin");
                engine->Initialize();                

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"invalid", itemID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_SM_NAME_NOT_FOUND()
            {
                //SM_NAME_NOT_FOUND = -932
                Assert::AreEqual(-932, returnCode);
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_menu_with_PB
        {
        public: 
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0001.DDL.bin");
                engine->Initialize();                

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", itemID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(6u, *itemID);
            }
        };

        [TestClass]
        public ref class When_request_the_Item_ID_for_a_custom_variable_with_PB
        {
        public: 
            static ITEM_ID* itemID;
            static int returnCode;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0001.DDL.bin");
                engine->Initialize();

                itemID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"test_var_integer", itemID);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete itemID;
                delete engine;
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_as_returnCode_a_DDS_SUCCESS()
            {
                //DDS_SUCCESS = 0
                Assert::AreEqual(0, returnCode);
            }

            [TestMethod, TestCategory("NodePathTests")]
            void It_should_be_the_expected_Item_ID_by_Symbol_Name()
            {
                Assert::AreEqual(9u, *itemID);
            }
        };

    }
}
