/**
 *	@(#) $Id: evl_base.cpp,v 1.4 2012/10/03 23:51:58 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions that are common to all
 *	evaluation data structure parsing and evaluation.  This is
 *	the "base" set of functions.
 */


#include "stdafx.h"
#include "evl_loc.h"
#include "cm_loc.h"
#include "conversions.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif

/* ARGSUSED */
extern void deobfuscate(char *str, int size, int ddtype);
extern int ddl_decode_string(unsigned char **chunkp,DDL_UINT *size,nsEDDEngine::STRING *string,int ddtype);

/*********************************************************************
 *
 *	Name:	memdump
 *
 *	ShortDesc:	dump memory
 *
 *	Description:
 *		sends memory chunk to trace 
 *
 *	Inputs:
 *		chunk -			pointer
 *
 *
 *********************************************************************/

void memdump(unsigned char *chunk)
{
	(void)chunk;
#ifdef _DEBUG
	int bytecnt=20;
	PS_TRACE(L"... memdump last %d bytes -> ( ",bytecnt);

	wchar_t buf[40];
	memset(buf,0,sizeof(buf));
	for(int i=0;i<bytecnt;i++)
	{
		unsigned char uc=((chunk-bytecnt)[i]);
		wsprintf(buf,L"%02x ",uc);
		PS_TRACE(buf);
	}
#endif
}

/*********************************************************************
 *
 *	Name:	get_ddtype
 *
 *	ShortDesc:	get dd profile type
 *
 *	Description:
 *		get dd profile type
 *
 *	Inputs:
 *		env_info -			environ info
 *
 *	Returns:
 *		profile type
 *
 *********************************************************************/
int get_ddtype(ENV_INFO *env_info)
{
	if(!env_info)
		return ::nsEDDEngine::PROFILE_NONE;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	switch(env_info->handle_type)
	{
	case ENV_INFO::BlockHandle:
		{
			DEVICE_HANDLE dh = pConnectionMgr->ABT_ADT_OFFSET(env_info->block_handle);
			DD_DEVICE_ID* dd_device_id = pConnectionMgr->ADT_DD_DEVICE_ID(dh);
			if(dd_device_id)
				return dd_device_id->header.edd_profile;
		}
		break;
	case ENV_INFO::DeviceHandle:
		{
			DD_DEVICE_ID* dd_device_id = pConnectionMgr->ADT_DD_DEVICE_ID(env_info->block_handle);
			if(dd_device_id)
				return dd_device_id->header.edd_profile;
		}
		break;
	}
	return 0;
}

/*********************************************************************
 *  Name: 		ddl_parse_integer_func
 *  ShortDesc: 	parse an integer from the binary
 *
 *  Description:
 *		ddl_parse_integer_func parses an integer (1 to 4 bytes long)
 *	 	from the binary pointed at by "chunkp".  The parsed integer
 *		is stored in "value".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		value - contains the value of the integer parsed from the binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_integer_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *value)
{
	DDL_UINT        cnt;	/* temp ptr to size of binary */
	DDL_UINT        val;	/* temp storage for parsed integer */
	unsigned char  *chunk;	/* temp ptr to binary */
	uchar           c;	/* temp ptr to binary */
	int             more_indicator;	/* need to parse another byte */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_INTEGER_FUNC);
#endif

	/*
	 * Read each character, building the ulong until the high order bit is
	 * not set
	 */

	val = 0;
	chunk = *chunkp;
	cnt = *size;

	do {
		if (cnt == 0) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : not enough octets left to decode int");
			memdump(chunk);

			return DDL_INSUFFICIENT_OCTETS;
		}

		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7f;

		if (val > ((unsigned) ULONG_MAX >> 7)) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val <<= 7;
		if (val > (unsigned) ULONG_MAX - c) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val |= c;
		--cnt;
	} while (more_indicator);

	/*
	 * Update the pointer and size, and return the value
	 */

	*size = cnt;
	*chunkp = chunk;

	if (value) {
		*value = val;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *  Name: 		ddl_parse_integer_long_func
 *  ShortDesc: 	parse an integer from the binary
 *
 *  Description:
 *		ddl_parse_integer_func parses an integer (1 to 4 bytes long)
 *	 	from the binary pointed at by "chunkp".  The parsed integer
 *		is stored in "value".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		value - contains the value of the integer parsed from the binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *
 *  Author: 
 *********************************************************************/
int
ddl_parse_integer_long_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT_LONG       *value)
{
	DDL_UINT        cnt;	/* temp ptr to size of binary */
	DDL_UINT_LONG   val;	/* temp storage for parsed integer */
	unsigned char  *chunk;	/* temp ptr to binary */
	uchar           c;	/* temp ptr to binary */
	int             more_indicator;	/* need to parse another byte */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_INTEGER_FUNC);
#endif

	/*
	 * Read each character, building the ulong until the high order bit is
	 * not set
	 */

	val = 0;
	chunk = *chunkp;
	cnt = *size;

	do {
		if (cnt == 0) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : not enough octets left to decode int");
			memdump(chunk);

			return DDL_INSUFFICIENT_OCTETS;
		}

		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7f;

		if (val > ((unsigned long long) ULLONG_MAX >> 7)) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val <<= 7;
		if (val > (unsigned long long) ULLONG_MAX - c) {
			PS_TRACE(L"\n*** error (ddl_parse_integer_func) : decoded integer value exceeds largest known value");
			return DDL_LARGE_VALUE;
		}

		val |= c;
		--cnt;
	} while (more_indicator);

	/*
	 * Update the pointer and size, and return the value
	 */

	*size = cnt;
	*chunkp = chunk;

	if (value) {
		*value = val;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *  Name: 		ddl_parse_tag_func
 *  ShortDesc: 	parse a tag from the binary
 *
 *  Description:
 *		ddl_parse_tag_func parses a tag, and the length of the binary
 *		chunk associated with this tag, from the binary pointed at
 *		by "chunkp".  The parsed tag is stored in "tagp".  The length
 *		of the associated binary is stored in "lenp".  "Chunkp" and
 *		"size" are then updated to point to the remaining binary chunk
 *		 and size.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - updated pointer to the binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		tagp - contains the value of the tag parsed from the binary.
 *		lenp - contains the lenght of binary chunk associated with the tag.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_ENCODING_ERROR
 *		DDL_SUCCESS
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_tag_func(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *tagp,
DDL_UINT       *lenp)
{
	int             lenflag = 0;/* indicates implicit/explicit binary length */
	unsigned char  *chunk;	/* temp ptr to the binary chunk */
	DDL_UINT        cnt = 0;	/* temp ptr to the size of the binary chunk */
	uchar           c;	/* current value of the char pointed at by
				 * chunk */
	int             rc = 0;	/* return code */
	DDL_UINT        tag = 0;	/* temp storage of parsed tag */
	DDL_UINT        length = 0;	/* temp storage of length of binary assoc with
				 * tag */


	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_TAG_FUNC);
#endif

	chunk = *chunkp;
	cnt = *size;
	if (cnt == 0) {
		PS_TRACE(L"\n*** error (ddl_parse_tag_func) : not enough data to decode %d bytes",cnt); 
		return DDL_INSUFFICIENT_OCTETS;
	}

	/*
	 * Read the first character, and determine if there is an explicit
	 * length specified (bit 7 == 1)
	 */

	c = *chunk++;
	cnt--;
	lenflag = c & 0x80;
	tag = c & 0x7f;

	/*
	 * If the tag from the first character is <= 126, we are through. If
	 * the tag is == 127, we need to build the tag id from the following
	 * characters.
	 */

	if (tag == 127) {
		DDL_PARSE_INTEGER(&chunk, &cnt, &tag);
		tag += 126;
	}

	if (tagp) {
		*tagp = tag;
	}

	/*
	 * If there is an explicit length, get it.
	 */

	if (!lenflag) {
		length = 0;
	}
	else {
		DDL_PARSE_INTEGER(&chunk, &cnt, &length);
	}

	if (length > cnt) {
		PS_TRACE(L"\n*** error (ddl_parse_tag_func) : not enough data following tag : the tag says that %d bytes follow, actual is %d bytes",length,cnt); 
		memdump(chunk);
		return DDL_ENCODING_ERROR;
	}

	if (lenp) {
		*lenp = length;
	}
	*size = cnt;
	*chunkp = chunk;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_float
 *	ShortDesc: Parse a floating point value from the binary
 *
 *	Description:
 *		ddl_parse_float parses a floating point value from the binary
 *		pointed at by "chunkp".  The parsed value is stored in "value".
 *		"Chunkp" and "size" are then updated to point to the remaining
 *		binary chunk and size.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		value - pointer to a float where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		value - pointer to a float containing the result
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_INSUFFICIENT_OCTETS
 *
 *	Author: Steve Beyerl
 *********************************************************************/

REQUIRE(sizeof(float) == 4);	/* assumes that a float will be IEEE 754 format */

int
ddl_parse_float(
unsigned char **chunkp,
DDL_UINT       *size,
float          *value)
{
	float           temp;	/* temporary storage */

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_FLOAT);
#endif

	if (*size < (DDL_UINT) sizeof(float)) {
		return DDL_INSUFFICIENT_OCTETS;
	}

	/*
	 * Move the four bytes into the float, taking care to handle the
	 * different byte ordering.
	 */

#if defined(BIG_ENDIAN)		/* BIG_ENDIAN byte order! */

	(void) memcpy((char *) &temp, (char *) *chunkp, sizeof(float));

#else
	#if defined(LITTLE_ENDIAN)	/* LITTLE_ENDIAN byte order! */
		{
			size_t          i;
			unsigned char  *bytep;
			unsigned char  *chunk;	/* local pointer to binary */

			chunk = *chunkp;
			bytep = ((unsigned char *) &temp) + (sizeof(float) - 1);
			for (i = 0; i < sizeof(float); ++i)
				*(bytep--) = *(chunk++);
		}
	#else
		/* Must define one or the other */

		REQUIRE(BIG_ENDIAN || LITTLE_ENDIAN);

	#endif
#endif

	*chunkp += sizeof(float);
	*size -= (DDL_UINT) sizeof(float);

	if (value) {
		*value = temp;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_parse_floating
 *	ShortDesc: Parse a floating point value from the binary
 *
 *	Description:
 *		ddl_parse_floating parses a floating point value from the binary
 *		pointed at by "chunkp".  The parsed value is stored in "value".
 *		"Chunkp" and "size" are then updated to point to the remaining
 *		binary chunk and size.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		value - pointer to a float where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		value - pointer to a float containing the result
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_INSUFFICIENT_OCTETS
 *
 *********************************************************************/
int
ddl_parse_floating(
unsigned char **chunkp,
DDL_UINT       *size,
double          *value)
{
	double           temp;	/* temporary storage */

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_FLOATING);
#endif

	if (*size < (DDL_UINT) sizeof(double)) {
		return DDL_INSUFFICIENT_OCTETS;
	}

	/*
	 * Move the four bytes into the float, taking care to handle the
	 * different byte ordering.
	 */

#if defined(BIG_ENDIAN)		/* BIG_ENDIAN byte order! */

	(void) memcpy((char *) &temp, (char *) *chunkp, sizeof(double));

#else
	#if defined(LITTLE_ENDIAN)	/* LITTLE_ENDIAN byte order! */
		{
			size_t          i;
			unsigned char  *bytep;
			unsigned char  *chunk;	/* local pointer to binary */

			chunk = *chunkp;
			bytep = ((unsigned char *) &temp) + (sizeof(double) - 1);
			for (i = 0; i < sizeof(double); ++i)
				*(bytep--) = *(chunk++);
		}
	#else
		/* Must define one or the other */

		REQUIRE(BIG_ENDIAN || LITTLE_ENDIAN);

	#endif
#endif

	*chunkp += sizeof(double);
	*size -= (DDL_UINT) sizeof(double);

	if (value) {
		*value = temp;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_ulong_choice
 *	ShortDesc: Choose the correct ulong from a binary.
 *
 *	Description:
 *		ddl_ulong_choice will parse the binary for an unsigned long int,
 *		according to the current conditionals (if any).  The value of
 *		the ulong and/or dependency information is returned.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UINT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and DDL_PARSE_INTEGER().
 *
 *	Author: Steve Beyerl
 *********************************************************************/

static int
ddl_ulong_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *optionalTag)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_ULONG_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (intval && val.chunk) {

		nsEDDEngine::ATTRIBUTE_DATA  *tg=(nsEDDEngine::ATTRIBUTE_DATA  *)optionalTag;

		if(tg && tg->tag)
		{
			DDL_UINT tag = 0;	/* identifier of a binary chunk */
			DDL_UINT len = 0;

			DDL_PARSE_TAG(&val.chunk, &val.size, &tag, &len);	// Explicit tag

			if(tag!=(DDL_UINT)tg->tag)
				return DDL_ENCODING_ERROR;
		}

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */
		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		DDL_PARSE_INTEGER(&val.chunk, &val.size, intval);

		*data_valid = TRUE;	/* intval has been modified */
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_line_type_choice
 *	ShortDesc: Choose the correct ulong from a binary.
 *
 *	Description:
 *		ddl_line_type_choice will parse the binary for an unsigned long int,
 *		according to the current conditionals (if any).  The value of
 *		the ulong and/or dependency information is returned.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UINT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and DDL_PARSE_INTEGER().
 *
 *	Author: Ying Xu
 *********************************************************************/

static int
ddl_line_type_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_ULONG_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (intval && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		DDL_PARSE_TAG(&val.chunk, &val.size, intval, (DDL_UINT *)NULL_PTR);		// Explicit tag
		if (*intval != LINE_TYPE_TAG)
		{
			return DDL_ENCODING_ERROR;
		}

		DDL_PARSE_TAG(&val.chunk, &val.size, intval, (DDL_UINT *)NULL_PTR);	
		if (*intval == DATA_LINETYPE)		// "DATA" type
		{
					// Check for an integer to follow
			DDL_PARSE_INTEGER(&val.chunk, &val.size, intval);

					// Add number to indicator for "DATA<n>" style
			*intval = *intval + DDS_LINE_TYPE_DATA_N;
		}

		*data_valid = TRUE;	/* intval has been modified */
	}

	return DDL_SUCCESS;
}

int
eval_byte(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       * /*env_info*/,
nsEDDEngine::OP_REF         *var_needed,
void *optionalTag)
{
	unsigned char *		byte = static_cast<unsigned char *>(voidP);
	int             rc, valid;
	rc=0;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ULONG);
#endif

	valid = 0;
	*byte = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	if(size<=0)
		return DDL_DEFAULT_ATTR;

	if(optionalTag)
	{
		nsEDDEngine::ATTRIBUTE_DATA  *tg=(nsEDDEngine::ATTRIBUTE_DATA  *)optionalTag;

		if(tg && tg->tag)
		{
			DDL_UINT tag = 0;	/* identifier of a binary chunk */
			DDL_UINT len = 0;

			DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

			if(tag!=(DDL_UINT)tg->tag)
				return DDL_ENCODING_ERROR;
		}
	}

	*byte=*chunk;

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}
	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_ulong
 *	ShortDesc: Evaluate an unsigned long integer
 *
 *	Description:
 *  	The eval_attr_ulong function evaluates a unsigned long integer.
 *  	The buffer pointed to by chunk should contain the binary
 *  	for an unsigned long int.  Size should specify the size.
 *		of the binary chunk.  If ulong is not a null pointer, the
 *		value is returned in u_long.  If depinfo is not a null pointer,
 *		dependency information about the reference is returned in
 *		depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		ulong - space for parsed ulong
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the remaining chunk of binary data
 *		size - size of the remaining chunk of binary data
 *		ulong - contains the value of the parsed unsigned long integer.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_ulong_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: steve beyerl
 *********************************************************************/
int
eval_attr_ulong(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *optionalTag)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);
	int             rc, valid;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ULONG);
#endif

	valid = 0;
	*u_long = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_ulong_choice(&chunk, &size, u_long, depinfo, &valid, env_info, var_needed, optionalTag);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}
	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (u_long && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_attr_line_type
 *	ShortDesc: Evaluate source line type
 *
 *	Description:
 *  	The eval_attr_line_type function evaluates a unsigned long integer.
 *  	The buffer pointed to by chunk should contain the binary
 *  	for an unsigned long int.  Size should specify the size.
 *		of the binary chunk.  If ulong is not a null pointer, the
 *		value is returned in u_long.  If depinfo is not a null pointer,
 *		dependency information about the reference is returned in
 *		depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		ulong - space for parsed ulong
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the remaining chunk of binary data
 *		size - size of the remaining chunk of binary data
 *		ulong - contains the value of the parsed unsigned long integer.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_ulong_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: ying xu
 *********************************************************************/
int
eval_attr_line_type(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);
	int             rc, valid;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ULONG);
#endif

	valid = 0;
	*u_long = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_line_type_choice(&chunk, &size, u_long, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (u_long && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	eval_attr_ascii_string
 *
 *	ShortDesc:	evaluate ascii string
 *
 *	Description:
 *		evaluate ascii string
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		voidP - space for parsed string
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *
 *
 *********************************************************************/
int
eval_attr_ascii_string(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	nsEDDEngine::STRING*		ns = static_cast<nsEDDEngine::STRING*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	int ddtype=get_ddtype(env_info);
	int rc=ddl_decode_string( &chunk, &size, ns, ddtype );

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}
	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddl_menu_style_choice
 *	ShortDesc: Choose the correct ulong from a binary.
 *
 *	Description:
 *		ddl_menu_style_choice will parse the binary for an unsigned long int,
 *		according to the current conditionals (if any).  The value of
 *		the ulong and/or dependency information is returned.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UINT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and DDL_PARSE_INTEGER().
 *
 *	Author: Ying Xu
 *********************************************************************/

static int
ddl_menu_style_choice(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *intval,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	CHUNK val = {0};	/* Stores the binary found by this conditional */

	if (data_valid) {
		*data_valid = FALSE;
	}

	int rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is requested.
	 */

	if (intval && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/* We have finally gotten to the actual value!  Parse it. */

		DDL_PARSE_TAG(&val.chunk, &val.size, intval, (DDL_UINT *)NULL_PTR);

		*data_valid = TRUE;	/* intval has been modified */
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_attr_menu_style
 *	ShortDesc: Evaluate menu style
 *
 *	Description:
 *  	The eval_attr_menu_style function evaluates a unsigned long integer.
 *  	The buffer pointed to by chunk should contain the binary
 *  	for an unsigned long int.  Size should specify the size.
 *		of the binary chunk.  If ulong is not a null pointer, the
 *		value is returned in u_long.  If depinfo is not a null pointer,
 *		dependency information about the reference is returned in
 *		depinfo.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		ulong - space for parsed ulong
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the remaining chunk of binary data
 *		size - size of the remaining chunk of binary data
 *		ulong - contains the value of the parsed unsigned long integer.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_ulong_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: ying xu
 *********************************************************************/
int
eval_attr_menu_style(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	DDL_UINT*		u_long = static_cast<DDL_UINT*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	int valid = 0;
	*u_long = 0;		/** initialize output parameter **/

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	int rc = ddl_menu_style_choice(&chunk, &size, u_long, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (u_long && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *  Name: 		ddl_parse_bitstring
 *  ShortDesc: 	parse a bitstring from the binary
 *
 *  Description:
 *		ddl_parse_bitstring parses a bitstring (up to 32 bits long)
 *	 	from the binary pointed at by "chunkp".  The parsed bitstring
 *		is stored in "bitstring".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - pointer to the remaining binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		bitstring - contains the value of the bitstring parsed from the
 *			binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_long_bitstring(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT_LONG       *bitstring)
{
	unsigned char  *chunk;	/* temp ptr to the binary chunk */
	DDL_UINT_LONG        read_mask;	/* mask used to read bits from the
					 * binary */
	DDL_UINT_LONG        write_mask;	/* mask used to store the bits  */
	DDL_UINT_LONG        value;	/* temp storage of parsed bits */
	DDL_UINT        count;	/* local value of binary size */
	int             more_indicator;	/* indicators more bits to be read */
	int             last_unused;	/* # of bits to ignore in last octet */
	int             bitlimit;	/* loop counter */
	unsigned char   c;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_BITSTRING);
#endif

	/*
	 * Read the first character
	 */

	chunk = *chunkp;
	count = *size;
	value = 0;
	count--;
	c = *chunk++;

	/*
	 * Special case the situation where there is no bitstring (that is, the
	 * bit string is zero).
	 */

	if (c == 0x80) {
		*size = count;
		*chunkp = chunk;

		if (bitstring) {
			*bitstring = value;
		}
		return DDL_SUCCESS;
	}

	/*
	 * Pull out the number of bits to ignore in the last octet, and the
	 * indicator of whether there are more octets.
	 */

	more_indicator = c & 0x10;
	last_unused = (c & 0xE0) >> 5;
	c &= 0xF;

	/*
	 * Build up the return value.  Note that in each octet, the highest
	 * order bit corresponds to the lowest order bit in the returned bit
	 * mask.  That is, in the first octet, bit 3 corresponds to bit 0, bit
	 * 2 to bit 1, etc.  In the next octet, bit 6 corresponds to bit 4, bit
	 * 5 to bit 5, bit 4 to bit 6, ...  In the next octet, bit 6
	 * corresponds to bit 11, bit 5 to bit 12, ...
	 */

	write_mask = 1;
	read_mask = 0x8;
	bitlimit = 4;

	while (more_indicator) {

		if (!count) {

			return DDL_INSUFFICIENT_OCTETS;
		}

		for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

			if (c & read_mask) {

				value |= write_mask;
			}
		}

		/*
		 * Read the next octet.
		 */

		count--;
		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7F;
		read_mask = 0x40;
		bitlimit = 7;
	}

	/*
	 * In the last octet, some of the bits may be ignored.  The number of
	 * bits to ignore was specified in the very first octet, and remembered
	 * in "last_unused".
	 */

	bitlimit -= last_unused;

	if (bitlimit <= 0) {

		return DDL_ENCODING_ERROR;
	}

	for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

		if (!write_mask) {

			return DDL_LARGE_VALUE;
		}
		if (c & read_mask) {

			value |= write_mask;
		}
	}

	*size = count;
	*chunkp = chunk;

	if (bitstring) {

		*bitstring = value;
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *  Name: 		ddl_parse_bitstring
 *  ShortDesc: 	parse a bitstring from the binary
 *
 *  Description:
 *		ddl_parse_bitstring parses a bitstring (up to 32 bits long)
 *	 	from the binary pointed at by "chunkp".  The parsed bitstring
 *		is stored in "bitstring".  "Chunkp" and "size" are then updated.
 *
 *  Inputs:
 *		chunkp - pointer to the binary chunk.
 *		size - pointer to the size of the binary chunk.
 *
 *  Outputs:
 *		chunkp - pointer to the remaining binary chunk.
 *		size - pointer to the size of the remaining binary chunk.
 *		bitstring - contains the value of the bitstring parsed from the
 *			binary.
 *
 *	Note:
 *		This function is a copy of the Tokenizer function of the
 *		same name, residing in module bin_pars.c.  If this function is
 *		changed the Tokenizer version should also be looked at.
 *
 *  Returns:
 *		DDL_INSUFFIENT_OCTETS
 *		DDL_LARGE_VALUE
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *
 *  Author: steve beyerl
 *********************************************************************/
int
ddl_parse_bitstring(
unsigned char **chunkp,
DDL_UINT       *size,
DDL_UINT       *bitstring)
{
	unsigned char  *chunk;	/* temp ptr to the binary chunk */
	DDL_UINT        read_mask;	/* mask used to read bits from the
					 * binary */
	DDL_UINT        write_mask;	/* mask used to store the bits  */
	DDL_UINT        value;	/* temp storage of parsed bits */
	DDL_UINT        count;	/* local value of binary size */
	int             more_indicator;	/* indicators more bits to be read */
	int             last_unused;	/* # of bits to ignore in last octet */
	int             bitlimit;	/* loop counter */
	unsigned char   c;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_BITSTRING);
#endif

	/*
	 * Read the first character
	 */

	chunk = *chunkp;
	count = *size;
	value = 0;
	count--;
	c = *chunk++;

	/*
	 * Special case the situation where there is no bitstring (that is, the
	 * bit string is zero).
	 */

	if (c == 0x80) {
		*size = count;
		*chunkp = chunk;

		if (bitstring) {
			*bitstring = value;
		}
		return DDL_SUCCESS;
	}

	/*
	 * Pull out the number of bits to ignore in the last octet, and the
	 * indicator of whether there are more octets.
	 */

	more_indicator = c & 0x10;
	last_unused = (c & 0xE0) >> 5;
	c &= 0xF;

	/*
	 * Build up the return value.  Note that in each octet, the highest
	 * order bit corresponds to the lowest order bit in the returned bit
	 * mask.  That is, in the first octet, bit 3 corresponds to bit 0, bit
	 * 2 to bit 1, etc.  In the next octet, bit 6 corresponds to bit 4, bit
	 * 5 to bit 5, bit 4 to bit 6, ...  In the next octet, bit 6
	 * corresponds to bit 11, bit 5 to bit 12, ...
	 */

	write_mask = 1;
	read_mask = 0x8;
	bitlimit = 4;

	while (more_indicator) {

		if (!count) {

			return DDL_INSUFFICIENT_OCTETS;
		}

		for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

			if (c & read_mask) {

				value |= write_mask;
			}
		}

		/*
		 * Read the next octet.
		 */

		count--;
		c = *chunk++;
		more_indicator = c & 0x80;
		c &= 0x7F;
		read_mask = 0x40;
		bitlimit = 7;
	}

	/*
	 * In the last octet, some of the bits may be ignored.  The number of
	 * bits to ignore was specified in the very first octet, and remembered
	 * in "last_unused".
	 */

	bitlimit -= last_unused;

	if (bitlimit <= 0) {

		return DDL_ENCODING_ERROR;
	}

	for (; bitlimit; bitlimit--, read_mask >>= 1, write_mask <<= 1) {

		if (!write_mask) {

			return DDL_LARGE_VALUE;
		}
		if (c & read_mask) {

			value |= write_mask;
		}
	}

	*size = count;
	*chunkp = chunk;

	if (bitstring) {

		*bitstring = value;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_bitstring_choice
 *	ShortDesc: Choose the correct bitstring from a binary.
 *
 *	Description:
 *		ddl_bitstring_choice will parse the binary for a bitstring,
 *		according to the current conditionals (if any).  The value of
 *		the bitstring is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		intval - pointer to an DDL_UNIT structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		intval - pointer to an DDL_UINT structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in intval
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_bitstring().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_bitstring_choice(
unsigned char **chunkp,
DDL_UINT       *size,
void		   *voidP, 
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void  *optionalTag)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_BITSTRING_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	/*
	 * A chunk was found and a value is desired
	 */

	if (voidP && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL.
		 */

		ASSERT_DBG(data_valid != NULL);

		nsEDDEngine::ATTRIBUTE_DATA  *tg=(nsEDDEngine::ATTRIBUTE_DATA  *)optionalTag;

		if(tg && tg->tag)
		{
			DDL_UINT tag = 0;	/* identifier of a binary chunk */
			DDL_UINT len = 0;

			DDL_PARSE_TAG(&val.chunk, &val.size, &tag, &len);	// Explicit tag

			if(tag!=(DDL_UINT)tg->tag)
				return DDL_ENCODING_ERROR;
		}

		/* We have finally gotten to the actual value!  Parse it. */
		
		if (tg->size > 4)
		{
			DDL_UINT_LONG*		bitstr = static_cast<DDL_UINT_LONG*>(voidP);
			*bitstr = 0;
			rc = ddl_parse_long_bitstring(&val.chunk, &val.size, bitstr);
		}
		else
		{
			DDL_UINT*		bitstr = static_cast<DDL_UINT*>(voidP);
			*bitstr = 0;
			rc = ddl_parse_bitstring(&val.chunk, &val.size, bitstr);
		}

		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*data_valid = TRUE;	/* intval has been updated */
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_attr_bitstring
 *	ShortDesc: Evalute a bitstring.
 *
 *	Description:
 *		The function eval_attr_bitstring parses a binary chunk of data and loads
 *		bitstr with a bitstring
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		bitstr - place for parsed bitstring
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		bitstr contains the parsed bitstring
 *		depinfo - dependency information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_DEFAULT_ATTR
 *		DDL_SUCCESS
 *		return codes from:
 *			ddl_bitstring_choice()
 *			ddl_shrink_depinfo()
 *
 *	Author: steve beyerl
 *
 *********************************************************************/

int
eval_attr_bitstring(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *optionalTag)
{
	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in output parameter */
				 
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_BITSTRING);
#endif
	
	valid = 0;

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	rc = ddl_bitstring_choice(&chunk, &size, voidP, depinfo, &valid,
		env_info, var_needed, optionalTag);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	if (voidP && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_attr_definition
 *	ShortDesc: Evaluate a definition
 *
 *	Description:
 *		eval_attr_definition takes the definition char pointer and points
 *		it at the binary chunk.
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		def - char pointer
 *
 *	Outputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		def is loaded with the size and pointer to the definition
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_NULL_POINTER
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/

int
eval_attr_definition(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *,
void *)
{
	DEFINITION*			def = static_cast<DEFINITION*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DEFINITION);
#endif

	int rc = DDL_SUCCESS;

	if (def->data) {			// Free any old definition
		free((void *) def->data);
	}
	def->data = NULL;
	def->size = 0;


	// Allocate new buffer
	def->data = (wchar_t *) malloc((size+1) * sizeof(wchar_t));

	char *temp_chunk = (char *)malloc(size);
	memcpy(temp_chunk,chunk,size);

	int ddtype=get_ddtype(env_info);

	::deobfuscate(temp_chunk,size,ddtype);

	// Convert and copy
	int	numChars = PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_chunk, size, def->data, size+1);

	free(temp_chunk);

	_T("");

	if (numChars == 0)	// Error
	{
		rc = DDL_ENCODING_ERROR;
	}

	def->data[numChars] = L'\0';	// NULL terminate
	def->size = numChars;

	return rc;
}



/*********************************************************************
 *
 *	Name: eval_attr_default_values
 *	ShortDesc: Evaluate default values
 *
 *	Description:
 *		Evaluate default values
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		voidP - char pointer
 *		depinfo - dependency information
 *		env_info - envirion info
 *		var_needed - pointer to a OP_REF structure
 *		
 *	Outputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		def is loaded with the size and pointer to the definition
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_NULL_POINTER
 *
 *
 *********************************************************************/

#define MIN_DEFAULT_VALUE_ITEM_BYTE_SIZE 10

int
eval_attr_default_values(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void * /*optionalTag*/)
{
	nsEDDEngine::DEFAULT_VALUES_LIST *def = static_cast<nsEDDEngine::DEFAULT_VALUES_LIST*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DEFINITION);
#endif

	int rc = DDL_ENCODING_ERROR;
	DDL_UINT tag = 0;	/* identifier of a binary chunk */
	DDL_UINT len = 0;

	DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

	if (tag != DEFAULT_VALUES_LIST_TAG) 
	{
		return DDL_ENCODING_ERROR;
	}

	if(len)
	{
		def->limit=(unsigned short)(len/MIN_DEFAULT_VALUE_ITEM_BYTE_SIZE);
		def->count=0;

		ASSERT(def->limit);

		def->list=new ::nsEDDEngine::DEFAULT_VALUE_ITEM[def->limit];

		while(size>0)
		{
			if(def->count>=def->limit)
			{
				ASSERT(0);
				return DDL_ENCODING_ERROR;
			}

			OP_REF_TRAIL ort={STANDARD_TYPE, 0};
			EXPR expr={0};
			int valid;	/* indicates the validity of the data */

			rc=ddl_op_ref_trail_choice(&chunk, &size, &ort, depinfo, &valid,	env_info, var_needed);
			if(rc)
			{
				::ddl_free_op_ref_trail(&ort);
				return rc;
			}


			{
				DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

				if (tag != EXPRESSION_LIST_TAG) 
				{
					return DDL_ENCODING_ERROR;
				}

				size -= len;

				while(len)
				{
					tag=DDL_PEEK_TAG(*chunk);

					if(tag!=EXPRESSION_TAG)
					{
						return DDL_ENCODING_ERROR;
					}

					valid = FALSE;

					rc = ddl_eval_expr(&chunk, &len, &expr, depinfo, env_info, var_needed);

					if (rc != DDL_SUCCESS) {
						::ddl_free_op_ref_trail(&ort);
						::ddl_free_expr(&expr);
						return rc;
					}

					valid = TRUE;

					nsEDDEngine::DEFAULT_VALUE_ITEM *item=&def->list[def->count];
					def->count++;

					::FDI_ConvEXPR::CopyFrom(&expr, &(item->value));
					::FDI_ConvOP_REF_TRAIL::CopyFrom(&ort, &(item->ref));

					::ddl_free_op_ref_trail(&ort);
					::ddl_free_expr(&expr);
				}
			}
		}
	}

	return rc;
}

/*********************************************************************
 *
 * Name: eval_attr_params
 * ShortDesc: Parse method parameters
 *
 * Description: Use this function when you are parsing method parameters.
 */

int
eval_attr_params (
	unsigned char  *chunk,
	DDL_UINT        size,
	void		   *voidP,
	nsEDDEngine::OP_REF_LIST    *,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *,
	void *)
{
	STRING* pstrParams = static_cast<STRING*>(voidP);
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	int rc = DDL_ENCODING_ERROR;
	DDL_UINT tag = 0;	/* identifier of a binary chunk */
	DDL_UINT len = 0;

	DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

	if (tag != METHOD_PARAMETER_SEQLIST_TAG) 
	{
		return DDL_ENCODING_ERROR;
	}

	ddl_free_string(pstrParams);	// free the old string
	
	size_t iCurSize = 64;		// initial size of the value buffer
	wchar_t *pszValue = (wchar_t *)malloc( iCurSize * sizeof(wchar_t));
	pszValue[0] = NULL;			// Null terminate

	int ddtype=get_ddtype(env_info);

	while( size > 0 )
	{
		DDL_PARSE_TAG(&chunk, &size, &tag, &len );		// Explicit tag
		if (tag != METHOD_PARAMETER_TAG) // We need to get the parameter tag
		{
			break;	// Not it. Then bail
		}

		// read the variable type of the parameter
		DDL_UINT lVariableType = 0;
		DDL_PARSE_INTEGER(&chunk, &size, &lVariableType);

		// read the type modifiers
		DDL_UINT modifiers=0;
		ddl_parse_bitstring(&chunk, &size, &modifiers );
		
		DDL_UINT name_str_len=0;
		DDL_PARSE_TAG(&chunk, &size, &tag, &name_str_len);		// Implicit tag
		if(tag != ASCII_STRING_TAG) //This should be followed by the parameter name
		{
			break;	// if not, something is wrong. Bail
		}

		wchar_t szVariableName[64] = {0};	// Buffer for the variable name

		if( name_str_len > 0)	// parameter name should be greater than zero bytes.
		{
			char *temp_chunk = (char *)malloc(name_str_len);
			memcpy(temp_chunk,chunk,name_str_len);

			::deobfuscate(temp_chunk,name_str_len,ddtype);

			//////////////////////////

			int numChars = PS_MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_chunk, name_str_len,
											szVariableName, sizeof(szVariableName)/sizeof(szVariableName[0]));

			//////////////////////////

			free(temp_chunk);


			if (numChars == 0)	// Error
			{
				return DDL_ENCODING_ERROR;
			}
			else
			{
				szVariableName[numChars] = L'\0';
			}
		}

		chunk += name_str_len;//move the pointers
		size -= name_str_len;
		
		wchar_t szParamBuffer [64] = {0};	// Buffer for the whole parameter

        swprintf( szParamBuffer, 64, L"%d;%d;%s;", lVariableType, modifiers, szVariableName );//build the total length string.

		//  len of all params + len of new param + slop > current size
		if ( wcslen(pszValue) + wcslen(szParamBuffer) + 5 > iCurSize)
		{
			iCurSize += wcslen(szParamBuffer) + 5;

			wchar_t * oldpszValue = pszValue;
			pszValue = (wchar_t*)realloc(pszValue, iCurSize * sizeof(wchar_t));
			if (pszValue == NULL)
			{
				free (oldpszValue);
				return DDL_MEMORY_ERROR;
			}
		}

		wcscat(pszValue, szParamBuffer);	// Append the new one to the end
	}

	pstrParams->len = (unsigned short)wcslen(pszValue);		// Set the string length

	pstrParams->str = (wchar_t *)malloc( (pstrParams->len + 1) * sizeof(wchar_t));	// Allocate buffer
	wcscpy( pstrParams->str, pszValue );		// Fill in string
	pstrParams->str[pstrParams->len] = L'\0';	// Null terminate

	pstrParams->flags = STRING::FREE_STRING;	// Mark as needing to be freed

	free( pszValue );//we are done building the parameters string, then free the memory.

	return rc;

}


/*********************************************************************
 *
 * Name: eval_attr_parse_integer
 * ShortDesc: Parse an integer without any conditionals
 *
 * Description: Use this function when you have a non-conditional enumeration
 *		and want to use item_mask_man to keep the masks organized.
 */

int
eval_attr_parse_integer(
	unsigned char  *chunk,
	DDL_UINT        size,
	void		   *voidP,
	nsEDDEngine::OP_REF_LIST    *,
	ENV_INFO       *,
	nsEDDEngine::OP_REF         *,
	void *)
{
	DDL_UINT* piValue = static_cast<DDL_UINT*>(voidP);
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	int rc = DDL_SUCCESS;

	DDL_PARSE_INTEGER(&chunk, &size, piValue);

	return rc;
}

/*********************************************************************
 *
 * Name: eval_attr_parse_item_information
 * ShortDesc: Parse a ITEM_INFORMATION
 *
 * Description: Parse a ITEM_INFORMATION
 */

int
eval_attr_parse_item_information(
	unsigned char  *chunk,
	DDL_UINT        size,
	void		   *voidP,
	nsEDDEngine::OP_REF_LIST    *depinfo,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed,
	void *)
{
	::nsEDDEngine::ITEM_INFORMATION *item_info = static_cast<::nsEDDEngine::ITEM_INFORMATION *>(voidP);
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	int rc = DDL_SUCCESS;

	DDL_UINT tag = 0;	/* identifier of a binary chunk */
	DDL_UINT len = 0;

	DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

	if(tag!=ITEM_INFORMATION_TAG)
	{
		return DDL_ENCODING_ERROR;
	}

	STRING s;
	rc = ddl_parse_string(&chunk, &size, &s,	depinfo, env_info, var_needed);
	::FDI_ConvSTRING::CopyFrom(&s,&item_info->file_name);
	::ddl_free_string(&s);

	DDL_UINT temp = 0;
	DDL_PARSE_INTEGER(&chunk, &size, &temp);

	item_info->line_number=temp;
	
	rc = ddl_parse_attr_info(&chunk, &size, &item_info->attrs, depinfo, env_info, var_needed);

	return rc;
}

#define MIN_ATTR_INFO_ITEM_BYTE_SIZE 6

static
int
ddl_parse_attr_info(
	unsigned char **chunkp,
	DDL_UINT       *size,
	nsEDDEngine::ATTR_INFO_LIST *attr_info_list,
	nsEDDEngine::OP_REF_LIST    *depinfo,
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed)
{
	

#ifdef DDSTEST
	TEST_FAIL(EVAL_DEFINITION);
#endif

	int rc = DDL_SUCCESS;
	DDL_UINT tag = 0;	/* identifier of a binary chunk */

	if (*size)
	{
		attr_info_list->limit = (unsigned short)(*size / MIN_ATTR_INFO_ITEM_BYTE_SIZE);
		attr_info_list->count = 0;

		ASSERT(attr_info_list->limit);

		attr_info_list->list = new ::nsEDDEngine::ATTR_INFO[attr_info_list->limit];
		//TRACE("%d", *size);
		while (*size > 0)
		{
			if (attr_info_list->count >= attr_info_list->limit)
			{
				return DDL_ENCODING_ERROR;
			}

			DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *)NULL_PTR);	// Explicit tag
			
			if (tag != ATTR_INFO_SPECIFIER)
			{
				return DDL_ENCODING_ERROR;
			}
			
			STRING s;
			rc = ddl_parse_string(chunkp, size, &s, depinfo, env_info, var_needed);
						
			
			DDL_UINT lineno = 0;
			DDL_PARSE_INTEGER(chunkp, size, &lineno);
						
			DDL_UINT attribute = 0;
			::ddl_parse_byte(chunkp, size, &attribute);



			nsEDDEngine::ATTR_INFO *item = &attr_info_list->list[attr_info_list->count];
			attr_info_list->count++;


			::FDI_ConvSTRING::CopyFrom(&s, &item->attr_file_name);
				
			item->attr_lineno = lineno;
			item->attr_name = (::nsEDDEngine::AttributeName)attribute;
					

			::ddl_free_string(&s);
			
			}

		//TRACE("%d", attr_info_list->count);
		}

	return rc;
}

/*********************************************************************
 *
 * Name: eval_attr_parse_uuid
 * ShortDesc: Parse an uuid
 *
 * Description: Parse an uuid
 */

int
eval_attr_parse_uuid(
	unsigned char  *chunk,
	DDL_UINT        size,
	void		   *voidP,
	nsEDDEngine::OP_REF_LIST    *,
	ENV_INFO					*,
	nsEDDEngine::OP_REF         *,
	void *)
{
	GUID *uuid = static_cast<GUID *>(voidP);
	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

	int rc = DDL_SUCCESS;

	DDL_UINT_LONG upper = 0;
	DDL_UINT_LONG lower = 0;

	DDL_UINT tag = 0;	/* identifier of a binary chunk */
	DDL_UINT len = 0;

	DDL_PARSE_TAG(&chunk, &size, &tag, &len);	// Explicit tag

	if(tag!=UUID_TAG)
	{
		return DDL_ENCODING_ERROR;
	}

	DDL_PARSE_INTEGER_LONG(&chunk, &size, &upper);
	DDL_PARSE_INTEGER_LONG(&chunk, &size, &lower);

	uuid->Data1=(upper>>32)	&	0xffffffff;
	uuid->Data2=(upper>>16)	&	0xffff;
	uuid->Data3=(upper>>0)	&	0xffff;

	for(int i=0;i<8;i++)
	{
		uuid->Data4[7-i]=(lower>>(i*8))&0xff;
	}

	return rc;
}




/*********************************************************************
 *
 *	Name: ddl_shrink_depinfo
 *	ShortDesc: 	Shrink nsEDDEngine::OP_REF_LIST list to size of value depinfo.limit
 *
 *	Description:
 *		ddl_shrink_depinfo checks nsEDDEngine::OP_REF_LIST list to the size of value
 *		depinfo.size. If nsEDDEngine::OP_REF_LIST is NULL it returns with no change.
 *		If depinfo.limit is equal to depinfo.size it returns with no change.
 *		If depinfo.size is equal to zero it frees the list. Otherwise it
 *		reallocates memory for the resized list.
 *
 *	Inputs:
 *		depinfo - pointer to nsEDDEngine::OP_REF_LIST structure
 *
 *	Outputs:
 *		depinfo - pointer to the resized list
 *
 *	Returns:
 *		DDL_MEMORY_ERROR -  unable to allocate memory for new list
 *		DDL_SUCCESS - nsEDDEngine::OP_REF_LIST has been shrunk
 *
 *	Author: steve beyerl
 *********************************************************************/

int
ddl_shrink_depinfo(
nsEDDEngine::OP_REF_LIST    *depinfo)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_DEPINFO);
#endif

	if (!depinfo) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure count and limit are consistent, then
	 * return.
	 */

	if (!depinfo->list) {
		ASSERT_DBG(!depinfo->count && !depinfo->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (depinfo->count == depinfo->limit) {
		return DDL_SUCCESS;
	}

	/*
	 *	If count = 0, free the list.
	 *	If count != 0, shrink the list
	 */

	if ( depinfo->count == 0 ) {
		ddl_free_depinfo( depinfo, FREE_ATTR );
	}
	else {
		depinfo->limit = depinfo->count;
		depinfo->list = (nsEDDEngine::OP_REF *) realloc((void *) depinfo->list,
			(size_t) (depinfo->limit * sizeof *depinfo->list));
		if (!depinfo->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_free_depinfo
 *	ShortDesc: Free the dependency information.
 *
 *	Description:
 *		ddl_free_depinfo frees the dependency information (ie. the
 *		nsEDDEngine::OP_REF_LIST structure).
 *
 *	Inputs:
 *		depinfo - ptr to the nsEDDEngine::OP_REF_LIST structure to be freed.
 *		dest_flag -
 *          FREE_ATTR - free the entire list
 *          CLEAN_ATTR - cleanup (ie. destroy) any malloc'd memory associated
 *              with each element of the list.  The base list is left intact.
 *
 *	Outputs:
 *		depinfo - ptr to the nsEDDEngine::OP_REF_LIST structure to be freed.
 *
 *	Returns: *		Void
 *
 *	Author: steve beyerl
 *********************************************************************/
void
ddl_free_depinfo(
nsEDDEngine::OP_REF_LIST    *depinfo,
uchar           dest_flag)
{

	if (depinfo == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (depinfo->list == NULL) {

		ASSERT_DBG(!depinfo->count && !depinfo->limit);
		depinfo->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list, and set the values in depinfo.
		 */

		free((void *) depinfo->list);
		depinfo->list = NULL;
		depinfo->limit = 0;
	}

	depinfo->count = 0;
}

/*********************************************************************
 *
 *	Name: append_depinfo
 *	ShortDesc: Add a dependency item to the existing dependency list.
 *
 *	Description:
 *		append_depinfo adds the source depinfo list to the end of the
 *		destination depinfo list.
 *
 *	Inputs:
 *		destination - pointer to a depinfo list
 *		source - pointer to a depinfo list
 *
 *	Outputs:
 *		destination - pointer to the appended depinfo list
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *********************************************************************/

int
append_depinfo(
nsEDDEngine::OP_REF_LIST    *destination,
nsEDDEngine::OP_REF_LIST    *source)
{

	int				add;
	nsEDDEngine::OP_REF         *temp;

	ASSERT_DBG(destination);

#ifdef DDSTEST
	TEST_FAIL(APPEND_DEPINFO);
#endif

	if (!source) {
		return DDL_SUCCESS;
	}

	if (source->count == 0) {
		return DDL_SUCCESS;
	}

	add = (int)source->count - ((int)destination->limit - (int)destination->count);

	if (add > 0) {
		temp = (nsEDDEngine::OP_REF *) realloc((void *) destination->list,
			(size_t) ((destination->limit + add) * sizeof(nsEDDEngine::OP_REF)));

		if (!temp) {

			/*
			 * destination->limit and count have not been modified
			 */

			ddl_free_depinfo(destination, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}

#pragma warning (disable : 4244)

		destination->limit += add;
		destination->list = temp;
	}

	memcpy((char *) &destination->list[destination->count],
		(char *) source->list, (size_t) (source->count * sizeof(nsEDDEngine::OP_REF)));

	destination->count += source->count;

#pragma warning (default : 4244)

	return DDL_SUCCESS;
}

/**********************************************
 *
 * append_and_free_depinfo - group this common sequence into one function
 *
 */

int append_and_free_depinfo(
nsEDDEngine::OP_REF_LIST    *destination,
nsEDDEngine::OP_REF_LIST    *source,
int				orig_rc)
{
	int rc = orig_rc;			// Initialize to original rc

	if (destination != NULL)
	{
		int rc2 = append_depinfo(destination, source);
		if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS))
		{
			rc = rc2;
		}
	}

	ddl_free_depinfo(source, FREE_ATTR);

	return rc;
}

/*********************************************************************
 *
 *	Name: ddl_add_depinfo
 *	ShortDesc: Adds the item to the dependency list.
 *
 *	Description:
 *		ddl_add_depinfo will add the item id and value type to the
 *		dependency list, if it is not already present.  If it is
 *		already in the list, only the value type is recorded.
 *
 *	Inputs:
 *		oper_ref - contains the operational reference (ie. dependency
 *				information) to be added to the oper_ref_list.
 *		depinfo - list of operational references (ie. list of
 *				dependency items) for the current item.
 *
 *	Outputs:
 *		depinfo - list of operational references (ie. list of
 *				dependency items) for the current item.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR - Binary is corrupted
 *		DDL_MEMORY_ERROR - Out of memory
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

#define OP_REF_LIST_INCR	6	/* Increment size for nsEDDEngine::OP_REF_LIST array */

int
ddl_add_depinfo(
nsEDDEngine::OP_REF         *oper_ref,
nsEDDEngine::OP_REF_LIST    *depinfo)
{
	nsEDDEngine::OP_REF         *dep;	/* temp pointer */
	nsEDDEngine::OP_REF         *end;	/* points to the end of the array */

	/*
	 * If the dependency item is already in the list (ie. the item_id and
	 * the subindex of the "oper_ref" match an element of the list, return
	 * success.
	 */

	for (dep = depinfo->list, end = dep + depinfo->count; dep < end; dep++)
	{
		if ((dep->op_info.id == oper_ref->op_info.id) && (dep->op_info.member == oper_ref->op_info.member)) 
		{
			return DDL_SUCCESS;
		}
	}
	/*
	 * Extend the list if necessary.
	 */

	if (depinfo->count >= depinfo->limit) {
		depinfo->limit += OP_REF_LIST_INCR;
		dep = (nsEDDEngine::OP_REF *) realloc((void *) depinfo->list,
			(size_t) (depinfo->limit * sizeof(*dep)));
		if (!dep) {
			depinfo->limit = depinfo->count;
			ddl_free_depinfo(depinfo, FREE_ATTR);
			return DDL_MEMORY_ERROR;
		}

		depinfo->list = dep;
	}

	/*
	 * Add the new dependency to the end, and increment size.
	 */

	dep = depinfo->list + depinfo->count++;

	memset(dep, 0, sizeof(nsEDDEngine::OP_REF));	// Zero the new entry before setting

	*dep = *oper_ref;

	return DDL_SUCCESS;
}


