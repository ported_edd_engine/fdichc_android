/**
 *	@(#) $Id: evl_type.cpp,v 1.3 2012/08/09 22:43:14 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to type information.
 */

#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif



/*
 * Constant Size values
 */

#define FLOAT_SIZE		4
#define DOUBLE_SIZE		8

#define DATE_SIZE			3
#define TIME_SIZE			6
#define DATE_AND_TIME_SIZE	7
#define DURATION_SIZE		6
#define TIME_VALUE_SIZE		8
#define BOOLEAN_SIZE		1



/*********************************************************************
 *
 *	Name:	ddl_shrink_range()
 *
 *	ShortDesc:	Condense list containing MIN/MAX values.
 *
 *	Description:
 *		Condense list containing MIN/MAX values.  The range list is
 *		"limit" elements long.  Only "size" number of elements are
 *		needed.  The list is condensed to contain "size" number of
 *		elements.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_shrink_range_list(
RANGE_DATA_LIST *list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_RANGE_LIST);
#endif

	if (!list) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (!list->list) {
		ASSERT_DBG(!list->count && !list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (list->count == list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (list->count == 0) {
		ddl_free_range_list(list, FREE_ATTR);
	}
	else {
		list->limit = list->count;

		list->list = (EXPR *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof(*list->list)));
		if (!list->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name:	ddl_free_range_list()
 *
 *	ShortDesc:	Free the list containing MIN/MAX range values.
 *
 *	Description:
 *		Free the list containing MIN/MAX range values.  The MIN/MAX
 *		range value list is no longer needed.  Return the memory used
 *		by "list" to available memory.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

void
ddl_free_range_list(
RANGE_DATA_LIST *list,
uchar           dest_flag)
{

	if (list == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
		list->limit = 0;
	}
	else if (dest_flag == FREE_ATTR) {

		/*
		 * Free the list, and set the values in list.
		 */

		free((void *) list->list);
		list->list = NULL;
		list->limit = 0;
	}

	list->count = 0;

}

/*********************************************************************
 *
 *	Name:	ddl_init_range_list()
 *
 *	ShortDesc:	Initialize the list for storing MIN/MAX range values.
 *
 *	Description:
 *		Initialize the list in which MIN/MAX range values are to be
 *		stored.
 *
 *	Inputs:
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - Initialized list for storing MIN/MAX range values.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static void
ddl_init_range_list(
RANGE_DATA_LIST *list)
{
	if (list == NULL) {
		return;
	}

	/*
	 * If there is no list, make sure the count/limit are consistent, then
	 * return.
	 */

	if (list->list == NULL) {

		ASSERT_DBG(!list->count && !list->limit);
		list->limit = 0;
	}

	if (list->limit > 0)
	{
		EXPR *endp = &list->list[list->limit];

		for (EXPR *datap = list->list; datap < endp; datap++) {
			datap->val.i = 0;
			datap->size = 0;
			datap->type = DDS_TYPE_UNUSED;
		}
	}

	list->count = 0;

}


/*********************************************************************
 *
 *	Name:	ddl_add_value()
 *
 *	ShortDesc:	Add another MIN/MAX value to the MIN/MAX list.
 *
 *	Description:
 *		Add another MIN/MAX value to the MIN/MAX range value list.
 *		If the type of the MIN/MAX value does not match the type
 *		of the variable, the type of the MIN/MAX value is "promoted"
 *		to match the type of the variable.  Type promotion is
 *		performed before the value is added to the list.
 *
 *	Inputs:
 *		value -	item to be added to list.
 *		position -	location in list where item is to be added.
 *		list - List of MIN/MAX range values.
 *
 *	Outputs:
 *		list - List of MIN/MAX range values.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_MEMORY_ERROR
 *		DDL_ENCODING_ERROR
 *		DDL_LARGE_VALUE
 *		return codes from:
 *			a)	ddl_promote_to()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_add_value(
EXPR           *value,
DDL_UINT        position,
RANGE_DATA_LIST *list)
{
	EXPR           *tmp;	/** temporary list pointer */
	EXPR           *endp;	/** ptr to end of min/max list */

	ASSERT_DBG(list);

#ifdef DDSTEST
	TEST_FAIL(DDL_ADD_VALUE);
#endif

	/*
	 * Add a min or max value to the RANGE_DATA_LIST structure. Expand the
	 * list if necessary, and parse the value. The position parameter
	 * specifies which min/max value (0,1,...) is being referenced.
	 */

	if(position==0)
		position=1;

	if ((unsigned short) position > list->limit) {
		do {
			list->limit += RANGE_DATA_INC;
		} while ((unsigned short) position > list->limit);
		tmp = (EXPR *) realloc((void *) list->list,
			(size_t) (list->limit * sizeof(*tmp)));
		if (!tmp) {
			return DDL_MEMORY_ERROR;
		}

		list->list = tmp;
		endp = &list->list[list->limit];
		for (tmp = &list->list[list->count]; tmp < endp; tmp++) {
			tmp->val.i = 0;
			tmp->size = 0;
			tmp->type = DDS_TYPE_UNUSED;
		}
		list->count = (unsigned short) position;
	}
	else if (list->count < (unsigned short) position) {
		list->count = (unsigned short) position;
	}

	int actual_index = position - 1;

	if(actual_index >= list->limit) 
	{
		PS_TRACE(L"\n*** error (ddl_add_value) : index is greater than list->limit (list position is invalid %d)", position);
		return DDL_ENCODING_ERROR;
	}

	tmp = &list->list[actual_index];
	if (value) {		/* a real value is to be added to the list */

		/*
		 * Add the value.
		 */

		tmp->size = value->size;
		tmp->type = value->type;
		switch (value->type) {
		case INTEGER:
			tmp->val.i = value->val.i;
			break;
		case UNSIGNED:
			tmp->val.u = value->val.u;
			break;
        case FLOATG_PT:
			tmp->val.f = value->val.f;
			break;
        case DOUBLEG_PT:
			tmp->val.d = value->val.d;
			break;
		default:
			return DDL_ENCODING_ERROR;
		}

	}
	else {

		/*
		 * If an existing structure was passed in, We must be sure to
		 * write over existing values in the structure.
		 */

		tmp->type = DDS_TYPE_UNUSED;	/* default values are to be used */
		tmp->size = 0;
		tmp->val.i = 0;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	ddl_parse_type()
 *
 *	ShortDesc:	Parse TYPE information from binary.
 *
 *	Description:
 *		Parse TYPE information (including subattribute information)
 *		for the current variable from the binary.  TYPE and subattribute
 *		information is all contained in the same chunk of binary.
 *		Variable type and dependency information are returned.  If a
 *		type value is found (every variable must have a TYPE) "data_valid"
 *		will be set to TRUE.
 *
 *	Inputs:
 *		chunkp - pointer to the binary.
 *		size - pointer to size of the binary.
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		env_info - environment information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		type - pointer to type of the TYPE subattribute.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure which contains
 *			dependency information.
 *      data_valid - pointer to an int which, if set to TRUE, indicates
 *              that there is a valid value has been parsed.
 *		vtype - variable type.
 *		vsize - variable size.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_ENCODING_ERROR
 *		return codes from:
 *			a)	DDL_PARSE_TAG -
 *			b)	ddl_parse_type_size -
 *			d)	ddl_enums_choice -
 *			e)	ddl_parse_item_id -
 *			f)	DDL_PARSE_INTEGER -
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
/* ARGSUSED */

static int
ddl_parse_type(
unsigned char **chunkp,
DDL_UINT       *size,
TYPE_SIZE       *typesize,
nsEDDEngine::OP_REF_LIST    * /*depinfo*/,
int            *data_valid,
ENV_INFO       * /*env_info*/,
nsEDDEngine::OP_REF         * /*var_needed*/)
{
	int             rc = DDL_SUCCESS;
	DDL_UINT        len;	/** length of binary associated w/ tag parsed */
	DDL_UINT        tag;	/** parsed tag */
	DDL_UINT		TSize = 0;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_TYPE);
#endif

	/*
	 * Parse the tag to find out if this a select statement, and if/else,
	 * or a simple assignment.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	typesize->type = (unsigned short)tag;

	switch (tag) {
	case INTEGER:
	case UNSIGNED:
	case ENUMERATED:
	case BIT_ENUMERATED:
	case INDEX:
	case ASCII:
	case PACKED_ASCII:
	case PASSWORD:
	case BITSTRING:
	case EUC:
	case OCTETSTRING:
	case VISIBLESTRING:
	case TIME_VALUE:
		// Parse out the size of the variable
		DDL_PARSE_INTEGER(chunkp, size, &TSize);
		break;


		// Variable types with constant sizes
    case FLOATG_PT:				TSize = FLOAT_SIZE;			break;
    case DOUBLEG_PT:			TSize = DOUBLE_SIZE;		break;
	case HART_DATE_FORMAT:	TSize = DATE_SIZE;			break;
	case TIME:				TSize = TIME_SIZE;			break;
	case DURATION:			TSize = DURATION_SIZE;		break;
	case DATE_AND_TIME:		TSize = DATE_AND_TIME_SIZE;	break;
	case OBJECT_REFERENCE:	TSize = len;				break;
	case BOOLEAN_T:			TSize = BOOLEAN_SIZE;		break;

	default:
		return DDL_ENCODING_ERROR;
	}

	typesize->size = TSize;
	*data_valid = TRUE;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name:	eval_typeinfo()
 *
 *	ShortDesc:	Evaluates all of the TYPE information for a variable.
 *
 *	Description:
 * 		This routine evaluates the type and size of a variable
 *
 *	Inputs:
 *		chunk - pointer to the binary
 *		size - size of the binary
 *		typesize - value of the desired item.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		typesize - type and size of the desired item.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *			information is stored.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDL_DEFAULT_ATTR
 *		return codes from:
 *			a)	ddl_parse_type
 *			k)	ddl_shrink_depinfo
 *
 *	Author:
 *		Mike Dieter
 *
 *********************************************************************/

static int
eval_typeinfo(
unsigned char  *chunk,
DDL_UINT        size,
TYPE_SIZE      *typesize,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	ASSERT_DBG(chunk);

#ifdef DDSTEST
	TEST_FAIL(EVAL_TYPEINFO);
#endif

	int data_valid = FALSE;	/** if TRUE, value has been modified */

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	int rc = ddl_parse_type(&chunk, &size, typesize,
				depinfo, &data_valid, env_info, var_needed);

	if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR)) {
		goto err_exit;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (typesize && !data_valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	typesize->type = 0;
	typesize->size = 0;
	ddl_free_depinfo(depinfo, FREE_ATTR);
	return rc;
}

/*******************************************************
*
* Name: eval_attr_type
*
* ShortDesc: evaluate variable type.
*
* Description:
*
*	The eval_attr_type function evaluates the datatype
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If type is not a null pointer, the datatype
*	is returned in type. If depinfo is not
*	a null pointer, dependency information about the datatype
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		vartype - variable type.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		vartype - variable type.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_type(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	TYPE_SIZE* typesize = static_cast<TYPE_SIZE*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_TYPE);
#endif

	if (typesize) {
		typesize->type = 0;	/** initialize output parameter **/
		typesize->size = 0;	/** initialize output parameter **/
	}

	return eval_typeinfo(chunk, size, typesize,
		depinfo, env_info, var_needed);
}

/**********************************************
*
* Name: eval_attr_display_format
*
* ShortDesc: evaluate the display format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_display_format function evaluates the display format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the display format
*	is returned in format. If depinfo is not
*	a null pointer, dependency information about the display format
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - display format of variable
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		format - display format of variable
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string(0
*
* Author:
*	Steve Beyerl
*
*****************/

int
eval_attr_display_format(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	STRING*			format = static_cast<STRING*>(voidP);

	int             rc;
	DDL_UINT        tag;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_DISPLAY_FORMAT);
#endif

	ddl_free_string(format);

	/*
	 * Strip off the DISPLAY_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != DISPLAY_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
}

/**********************************************
*
* Name: eval_attr_edit_format
*
* ShortDesc: evaluate the edit format of an arithmetic variable.
*
* Description:
*
*	The eval_attr_edit_format function evaluates the edit format
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If format is not a null pointer, the edit format
*	is returned in format. If depinfo is not
*	a null pointer, dependency information about the edit format
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		format - edit format of desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		format - edit format of desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_string()
*
* Author:
*	Steve Beyerl
*
******************************************************/

int
eval_attr_edit_format(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	STRING*			format = static_cast<STRING*>(voidP);

	int             rc;
	DDL_UINT        tag;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_EDIT_FORMAT);
#endif

	ddl_free_string(format);

	/*
	 * Strip off the EDIT_FORMAT_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != EDIT_FORMAT_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_string(chunk, size, format, depinfo, env_info, var_needed);
}

/**********************************************
*
* Name: eval_attr_scaling_factor
*
* ShortDesc: evaluate the scaling factor of an arithmetic variable.
*
* Description:
*
*	The eval_attr_scaling_factor function evaluates the scaling factor
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If factor is not a null pointer, the scaling factor
*	is returned in factor. If depinfo is not
*	a null pointer, dependency information about the scaling factor
*	is returned in depinfo.
*
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		factor - scaling factor of the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	eval_attr_expr()
*
* Author:
*	Steve Beyerl
*
********************************************************/

int
eval_attr_scaling_factor(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	EXPR*			factor = static_cast<EXPR*>(voidP);

	int             rc;
	DDL_UINT        tag;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_SCALING_FACTOR);
#endif

	if (factor) {
		memset((char *) factor, 0, sizeof(EXPR));
	}

	/*
	 * Strip off the SCALING_FACTOR_TAG.
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != SCALING_FACTOR_TAG) {
		return DDL_ENCODING_ERROR;
	}

	return eval_attr_expr(chunk, size, factor, depinfo, env_info, var_needed);
}

/**********************************************
*
* Name: eval_attr_min_values
*
* ShortDesc: evaluate the min values of an arithmetic variable
*
* Description:
*	The eval_attr_min_values function evaluates the minimum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the minimum values
*	are returned in values. If depinfo is not
*	a null pointer, dependency information about the minimum values
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
* 	Steve Beyerl
*
********************************************************/

int
eval_attr_range_data_list(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	RANGE_DATA_LIST* values = static_cast<RANGE_DATA_LIST*>(voidP);

	int             rc;
	int             data_valid;	/** set if "data_value" has been modified **/
	EXPR            data_value;	/* min value parsed from binary */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_MIN_VALUES);
#endif

	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);

	/*
	 * Strip off the MIN_VALUE_TAG
	 */

	CHUNK val = {0};	/* Stores the binary found by this conditional */

	rc = ddl_cond(&chunk, &size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}

	DDL_PARSE_TAG(&val.chunk, &val.size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MIN_MAX_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	while (val.size > 0) {	/* assuming multiple MIN_VALUES */

		/***
		 * Parse a MIN_VALUE_TAG by:
		 * a) get the number of this occurence of MIN_VALUE (ie. which).
		 * 	(i.e. MIN_VALUE1, MIN_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		data_valid = FALSE;

		DDL_PARSE_INTEGER(&val.chunk, &val.size, &which);

		rc = ddl_expr_choice(&val.chunk, &val.size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL, which, values);
		
		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}
	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
}

/**********************************************
*
* Name: eval_attr_min_values
*
* ShortDesc: evaluate the min values of an arithmetic variable
*
* Description:
*	The eval_attr_min_values function evaluates the minimum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the minimum values
*	are returned in values. If depinfo is not
*	a null pointer, dependency information about the minimum values
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		values - list of MIN_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
* 	Steve Beyerl
*
********************************************************/

int
eval_attr_min_values(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	RANGE_DATA_LIST* values = static_cast<RANGE_DATA_LIST*>(voidP);

	int             rc;
	int             data_valid;	/** set if "data_value" has been modified **/
	EXPR            data_value;	/* min value parsed from binary */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_MIN_VALUES);
#endif

	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);

	/*
	 * Strip off the MIN_VALUE_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MIN_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	while (size > 0) {	/* assuming multiple MIN_VALUES */

		/***
		 * Parse a MIN_VALUE_TAG by:
		 * a) get the number of this occurence of MIN_VALUE (ie. which).
		 * 	(i.e. MIN_VALUE1, MIN_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		data_valid = FALSE;

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
}

/**********************************************
*
* Name: eval_attr_max_values
*
* ShortDesc: evaluate the max values of an arithmetic variable
*
* Description:
*	The eval_attr_max_values function evaluates the maximum values
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If values is not a null pointer, the maximum values
*	are returned in values. If depinfo is not
*	a null pointer, dependency information about the maximum values
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		values - list of MAX_VALUES associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		DDL_ENCODING_ERROR
*		return codes from:
*			a)	eval_typeinfo()
*			b)	DDL_PARSE_TAG()
*			c)	DDL_PARSE_INTEGER()
*			d)	ddl_expr_choice()
*			e)	ddl_free_depinfo()
*			f)	ddl_add_values()
*			g)	ddl_shrink_depinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_max_values(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	RANGE_DATA_LIST* values = static_cast<RANGE_DATA_LIST*>(voidP);

	int             rc;
	int             data_valid;	/* set if "data_value" has been modified */
	EXPR            data_value;	/* max value parsed from binary */
	DDL_UINT        which;	/* "values" list index */
	DDL_UINT        tag;	/* label associated with current chunk */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_MAX_VALUES);
#endif


	/*
	 * initialize the list
	 */

	ddl_init_range_list(values);

	/*
	 * Strip off the MAX_VALUE_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != MAX_VALUE_TAG) {
		return DDL_ENCODING_ERROR;
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	while (size > 0) {	/* assuming multiple MAX_VALUES */

		/***
		 * Parse a MAX_VALUE_TAG by:
		 * a) get the number of this occurence of MAX_VALUE.
		 * 	(i.e. MAX_VALUE1, MAX_VALUE2, ... )
		 * b) evaluating the expression,
		 * c) add the value to the RANGE_DATA_LIST.
		 */

		DDL_PARSE_INTEGER(&chunk, &size, &which);

		data_valid = FALSE;

		rc = ddl_expr_choice(&chunk, &size, &data_value, depinfo,
			&data_valid, env_info, var_needed);

		if (rc != DDL_SUCCESS) {
			ddl_free_depinfo(depinfo, FREE_ATTR);
			ddl_free_range_list(values, FREE_ATTR);
			return rc;
		}

		rc = ddl_add_value(data_valid ? &data_value : (EXPR *) NULL, which, values);

	}			/* end of while() */

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_range_list(values);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_range_list(values, FREE_ATTR);
	}

	return rc;
}

/**********************************************
*
* Name: eval_attr_enum
*
* ShortDesc: evaluate the enumeration items of an enumerated variable
*
* Description:
*	The eval_attr_enum function evaluates the enumeration items
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If enums is not a null pointer, the enumeration
*	items are returned in enums. If depinfo is not
*	a null pointer, dependency information about the enumeration items
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		enums - list of ENUJMS associated with the desired variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/

int
eval_attr_enum(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	ENUM_VALUE_LIST* enums = static_cast<ENUM_VALUE_LIST*>(voidP);

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ENUM);
#endif

	if (enums) {
		ddl_free_enum_list(enums, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	int data_valid = FALSE;

	/*
	 * Enumeration values are handled via ddl_enums_choice
	 */

	int rc = ddl_enums_choice(&chunk, &size, enums, depinfo, &data_valid,
		env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);

		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_enum_list(enums);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_enum_list(enums, FREE_ATTR);
		return rc;
	}

	if (enums && (data_valid == FALSE)) {
		return DDL_DEFAULT_ATTR;
	}

	return rc;
}




/**********************************************
*
* Name: eval_attr_arrayname
*
* ShortDesc: evaluate the item_array name of an index variable.
*
* Description:
*
*	The eval_attr_arrayname function evaluates the item_array name
*	portion of a type attribute. The buffer pointed to by chunk
*	should contain a type attribute returned from fetch_var_attr,
*	and size should specify its size.
*	If item_arrayref is not a null pointer, the item_array name
*	is returned in item_arrayref. If depinfo is not
*	a null pointer, dependency information about the item_array name
*	is returned in depinfo.
*
* Inputs:
*		chunk - pointer to the binary
*		size - size of the binary
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*				information is stored.
*		env_info - environment information
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Outputs:
*		item_arrayref - item_array references associated with the desired
*				variable.
*		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*			information is stored.
*		var_needed - pointer to a OP_REF structure. If the value
*				of a variable is needed, but is not available,
*				"var_needed" info is returned to the application.
*
*
* Returns:
*		return codes from:
*			a)	eval_typeinfo()
*
* Author:
*	Steve Beyerl
*
********************************************/


int
eval_attr_arrayname(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	ITEM_ID*		item_arrayref = static_cast<ITEM_ID*>(voidP);

	DDL_UINT        tag;
	int             rc;

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_ITEM_ARRAYNAME);
#endif

	if (item_arrayref) {
		*item_arrayref = 0;
	}

	/*
	 * Strip off the ITEM_ARRAYNAME_TAG
	 */

	DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

	if (tag != ITEM_ARRAYNAME_TAG) {
		return DDL_ENCODING_ERROR;
	}

	/*
	 * Parse the variable item_array name. INDEX info is NOT conditional.
	 */

	return ddl_parse_item_id(&chunk, &size, item_arrayref, depinfo,
		env_info, var_needed);
}

