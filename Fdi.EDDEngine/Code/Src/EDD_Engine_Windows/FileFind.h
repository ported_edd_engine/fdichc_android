#pragma once
#include <time.h>

class WindowsFileFind
{
private:

	void *m_FileFinder;
	
public:
	// Constructors
	WindowsFileFind();

	virtual ~WindowsFileFind();
	BOOL FindFileNext();
	BOOL FindFile(const wchar_t* fileName);
	BOOL GetLastWriteTime(time_t& fileModifiedTime);
	unsigned long long GetLength();
	void CloseFile();
};



