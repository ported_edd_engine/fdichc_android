/**
 *	@(#) $Id: evl_dir.c,v 1.2 1996/01/04 20:12:26 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for evaluating a block or device
 *	directory.
 */


#include "stdinc.h"
#pragma warning (disable : 4131)
#include "HART/HARTEDDEngine/DDSSupport.h"
#include "ManualUnitLookup.h"


#ifdef ISPTEST
#include "tst_fail.h"
#endif

#define IMAGE_LANGUAGE_CODE_SIZE 6

typedef int (*eval_type)(ENV_INFO*, void*, BININFO*, ROD_HANDLE);


/*********************************************************************
 *
 *	Name:	ddl_eval_RESOLVED_REFERENCE
 *
 *	ShortDesc:	decodes a RESOLVED_REFERENCE
 *
 *	Description:
 *		decodes a RESOLVED_REFERENCE
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/
static int
ddl_eval_RESOLVED_REFERENCE(
unsigned char **chunkp,
DDL_UINT       *size,
RESOLVED_REFERENCE   *element)
{

	int rc = DDL_SUCCESS;
	DDL_UINT temp_uint = 0;
	DDL_UINT temp_tag = 0;		/* the tag value */

	DDL_UINT *pTempBuffer = new DDL_UINT [ *size ];		// Create temp buffer bigger than we need.
	int iTagCnt = 0;

	// Parse the reference
	while (*size > 0)
	{
		DDL_PARSE_TAG(chunkp, size, &temp_tag,  &temp_uint);

		if(temp_tag==ITEM_ID_REF)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);
			pTempBuffer[iTagCnt++] = temp_uint;
			break;
		}
		else if(temp_tag==VIA_RESOLVED_REF)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
			pTempBuffer[iTagCnt++] = temp_uint;
		}
		else
		{
			return DDL_ENCODING_ERROR;
		}
	}

	// Reverse the list
	for (int i=0; i < iTagCnt/2; i++)
	{
		temp_uint = pTempBuffer[i];
		pTempBuffer[i] = pTempBuffer[iTagCnt-i-1];
		pTempBuffer[iTagCnt-i-1] = temp_uint;
	}
	
	
	// copy value RESOLVED_REFERENCE
	if (element->pResolvedRef != nullptr)	// free any existing ResolvedRef
	{
		free( element->pResolvedRef );
		element->pResolvedRef = nullptr;
	}

	element->size = iTagCnt;

	size_t buff_size = element->size * sizeof(element->pResolvedRef[0]);
	element->pResolvedRef = (unsigned long*) malloc( buff_size );

    PS_Memcpy( element->pResolvedRef, buff_size, pTempBuffer, buff_size );


	delete [] pTempBuffer;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name:	ddl_eval_COMMAND_TBL_ELEM
 *
 *	ShortDesc:	decodes a COMMAND_TBL_ELEM
 *
 *	Description:
 *		decodes a COMMAND_TBL_ELEM
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/

static int
ddl_eval_COMMAND_TBL_ELEM (
	unsigned char **chunkp,
	DDL_UINT       *size,
	int            *pCnt,
	COMMAND_TBL_ELEM **ppCmdTblElem)
{
	int rc = DDL_SUCCESS;
	DDL_UINT temp_uint = 0;

	DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
	*pCnt = temp_uint;

	if( *pCnt != 0 )
	{
		*ppCmdTblElem = (COMMAND_TBL_ELEM *) calloc(*pCnt, sizeof(COMMAND_TBL_ELEM));
			
		if (*ppCmdTblElem == nullptr) {
			return DDL_MEMORY_ERROR;
		}
			
		COMMAND_TBL_ELEM *pCommandTblElem = *ppCmdTblElem;
		for(int i=0; i < *pCnt; i++, pCommandTblElem++)
		{
			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
			pCommandTblElem->subindex = temp_uint;

			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
			pCommandTblElem->number = temp_uint;

			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
			pCommandTblElem->transaction = temp_uint;

			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
			pCommandTblElem->weight = (ushort)temp_uint;

			DDL_PARSE_INTEGER(chunkp, size, &temp_uint);
			pCommandTblElem->count = temp_uint;
				
			if(pCommandTblElem->count > 0)
			{
				pCommandTblElem->index_list = (COMMAND_INDEX *) calloc(pCommandTblElem->count, sizeof(COMMAND_INDEX));

				COMMAND_INDEX *pCmdIndex = pCommandTblElem->index_list;
				for(int j=0; j < pCommandTblElem->count; j++, pCmdIndex++)
				{
					DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
					pCmdIndex->id = temp_uint;

					DDL_PARSE_INTEGER(chunkp, size, &temp_uint);	
					pCmdIndex->value=temp_uint;
				}
			}
			else
			{
				pCommandTblElem->index_list = nullptr;
			}
		}
	}
	else
	{
		*ppCmdTblElem = nullptr;
	}

	return rc;
}

/*********************************************************************
 *
 *	Name:	ddl_eval_RESLV_TBL_ELEM
 *
 *	ShortDesc:	decodes a RESLV_TBL_ELEM
 *
 *	Description:
 *		decodes a RESLV_TBL_ELEM
 *
 *	Inputs:
 *		chunkp	-		pointer to chunck
 *		size -			size of chunck
 *		element -		element
 *
 *	Outputs:
 *		element -		element
 *
 *	Returns:
 *		error
 *
 *********************************************************************/

int
ddl_eval_RESLV_TBL_ELEM(
unsigned char **chunkp,
DDL_UINT       *size,
RESLV_TBL_ELEM *element)
{
	int rc = DDL_SUCCESS;

	if(element == nullptr) {
		return DDL_MEMORY_ERROR;
	}

	// Eval the resolved reference
	rc = ddl_eval_RESOLVED_REFERENCE(chunkp, size, &element->element_ref);
	
	// Eval the read commands
	rc = ddl_eval_COMMAND_TBL_ELEM( chunkp, size, &element->read_count, &element->reslv_read_table_list);

	// Eval the write commands
	rc = ddl_eval_COMMAND_TBL_ELEM( chunkp, size, &element->write_count, &element->reslv_write_table_list);

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_list_tbl
 *	ShortDesc: evaluate the PARAM_LIST_TBL
 *
 *	Description:
 *		eval_dir_param_list_tbl will load the PARAM_LIST_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_table: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	PARAM_LIST_TBL* param_list_tbl = static_cast<PARAM_LIST_TBL*>(tableP);

	PARAM_LIST_TBL_ELEM		*element;	 /* temp pointer for the list */
	PARAM_LIST_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        		temp_int;	 /* integer value */
	int						rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_LIST_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_tbl->count = 0;
		param_list_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_tbl->list = (PARAM_LIST_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(PARAM_LIST_TBL_ELEM) ) );

	if (param_list_tbl->list == NULL) {

		param_list_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_list_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_LIST_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_tbl->list, end_element = element +
			temp_int; element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_count = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_string_tbl
 *	ShortDesc: evaluate the STRING_TBL
 *
 *	Description:
 *		eval_dir_string_tbl will load the STRING_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		string_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_string_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rod_handle)
{
	STRING_TBL* string_tbl = static_cast<STRING_TBL*>(tableP);

	STRING			*string;	 	/* temp pointer for the list */
	STRING			*end_string;	/* end pointer for the list */
	DDL_UINT    	temp_int;		/* integer value */
	int				rc;				/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_STRING_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		string_tbl->count = 0;
		string_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	string_tbl->count = (int) temp_int;

	/* malloc the list */
	string_tbl->list = (STRING *) malloc(
			(size_t) (temp_int * sizeof(STRING) ) );
	if (string_tbl->list == NULL) {

		string_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}


	UINT code_page = SB_CODE_PAGE;
	bool string_encoded_in_utf8 = g_DeviceTypeMgr.is_string_encoded_in_utf8(rod_handle);

	if (string_encoded_in_utf8)
	{
		code_page = CP_UTF8;
	}
	/* malloc the string buffer (number of bytes in binary + a null for each string */
	int iNumRootChars = bin->size + temp_int;
	string_tbl->root = (wchar_t *) malloc(iNumRootChars * sizeof(wchar_t));
	if (string_tbl->root == NULL)
	{
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) string_tbl->list, 0,
			(size_t) (temp_int * sizeof(STRING)));

	/* copy the chunk to the root */
	memcpy((char *) string_tbl->root, (char *) bin->chunk,
			(size_t) (bin->size * sizeof(unsigned char)));

	/*
	 * load the list
	 */

	wchar_t *root_ptr = string_tbl->root;

	for (string = string_tbl->list, end_string = string + temp_int;
		 string < end_string; string++) {

		int numChars = 0;	// Number of wide characters

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

		if (temp_int > 0)	// This check allows there to be a NULL string in the table
		{
			numChars = PS_MultiByteToWideChar(code_page, 0, (const char *)bin->chunk, temp_int,
														root_ptr, iNumRootChars);
			if (numChars == 0)	// Error
			{
				return DDL_ENCODING_ERROR;
			}
		}

		root_ptr[numChars] = L'\0';	// Null terminate

		string->str = root_ptr;
		string->len = (unsigned short)numChars;
		string->flags = DONT_FREE_STRING;

		bin->chunk += temp_int;		// Move bin pointer forward past string
		bin->size  -= temp_int;

		root_ptr      += numChars + 1;	// Move root pointer forward (include the NULL)
		iNumRootChars -= numChars + 1;

	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_image_tbl
 *	ShortDesc: evaluate the IMAGE_TBL
 *
 *	Description:
 *		eval_dir_imag_tbl will load the IMAGE_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		image_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

static
int eval_dir_image_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE	rod_handle)
{
	IMAGE_TBL* image_tbl = static_cast<IMAGE_TBL*>(tableP);
	DDL_UINT    	temp_int;		/* integer value */
	int				rc;				/* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);
	//The image table should only be evaluated once!
	ASSERT_DBG(!image_tbl->count && !image_tbl->list);

#ifdef ISPTEST
	TEST_FAIL(EVAL_IMAGE_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		image_tbl->count = 0;
		image_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	image_tbl->count = (int) temp_int;

	/* malloc the list */
	image_tbl->list = (BINARY_LIST *) malloc(
			(size_t) (temp_int * sizeof(BINARY_LIST) ) );
	if (image_tbl->list == NULL) {

		image_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) image_tbl->list, 0,
			(size_t) (temp_int * sizeof(BINARY_LIST)));

	for (int image_index =0; image_index<image_tbl->count; image_index++)
	{
		/* parse the number of the languages for the current iamge table entry */
		temp_int = 0;
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

		if(!temp_int) 
		{
			image_tbl->list[image_index].count = 0;
			image_tbl->list[image_index].list = NULL;
		}
		else
		{
			/* malloc the list */
			image_tbl->list[image_index].list = (BINARY_LANGUAGE_IMAGE *) malloc(
					(size_t) (temp_int * sizeof(BINARY_LANGUAGE_IMAGE) ) );
			if (image_tbl->list[image_index].list ==NULL)
			{
				image_tbl->list[image_index].count = 0;
				return DDL_MEMORY_ERROR;
			}

			/* load list with zeros */
			memset((char *) image_tbl->list[image_index].list, 0,
					(size_t) (temp_int * sizeof(BINARY_LANGUAGE_IMAGE)));	

			image_tbl->list[image_index].count = (unsigned short)temp_int;
			for (int langauge_index=0; langauge_index<image_tbl->list[image_index].count; langauge_index++)
			{			
				/* get the language code */
				int numAllocatedChars = (IMAGE_LANGUAGE_CODE_SIZE + 1) * sizeof(wchar_t);
				image_tbl->list[image_index].list[langauge_index].language_code.str = (wchar_t *)malloc((size_t)numAllocatedChars);
				if (!image_tbl->list[image_index].list[langauge_index].language_code.str)
				{
					return DDL_MEMORY_ERROR;
				}

				wchar_t *language_code_str_ptr = image_tbl->list[image_index].list[langauge_index].language_code.str;
				int numChars = 0;	// Number of wide characters

				numChars = PS_MultiByteToWideChar(SB_CODE_PAGE, 0, (const char*)bin->chunk, IMAGE_LANGUAGE_CODE_SIZE,
															language_code_str_ptr, numAllocatedChars);

				if (numChars == 0)	// Error
				{
					return DDL_ENCODING_ERROR;
				}
	
				language_code_str_ptr[numChars] = L'\0';	// Null terminate

				bin->chunk += IMAGE_LANGUAGE_CODE_SIZE;
				bin->size -= IMAGE_LANGUAGE_CODE_SIZE;

				image_tbl->list[image_index].list[langauge_index].language_code.len = (unsigned short)numChars;
				image_tbl->list[image_index].list[langauge_index].language_code.flags = FREE_STRING;

				/* get the image offset */
				temp_int = 0;
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				DDL_UINT offset = temp_int;

				/* get the image size */
				temp_int = 0;
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				image_tbl->list[image_index].list[langauge_index].length = temp_int;
				
				/* get the image data*/
				rc = g_DeviceTypeMgr.rod_get_image(rod_handle, offset, temp_int, &image_tbl->list[image_index].list[langauge_index]);
				if (rc != CM_SUCCESS)
				{
					return rc;
				}
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_domain_tbl
 *	ShortDesc: evaluate the DOMAIN_TBL
 *
 *	Description:
 *		eval_dir_domain_tbl will load the DOMAIN_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		domain_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_domain_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	DOMAIN_TBL* domain_tbl = static_cast<DOMAIN_TBL*>(tableP);

	DOMAIN_TBL_ELEM		*element;	 /* temp pointer for the list */
	DOMAIN_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_DOMAIN_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		domain_tbl->count = 0;
		domain_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	domain_tbl->count = (int) temp_int;

	/* malloc the list */
	domain_tbl->list = (DOMAIN_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(DOMAIN_TBL_ELEM) ) );

	if (domain_tbl->list == NULL) {

		domain_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) domain_tbl->list, 0,
			(size_t) temp_int * sizeof(DOMAIN_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = domain_tbl->list, end_element = element + temp_int;
			element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_item_tbl
 *	ShortDesc: evaluate the ITEM_TBL
 *
 *	Description:
 *		eval_dir_item_tbl will load the ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_item_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	ITEM_TBL* item_tbl = static_cast<ITEM_TBL*>(tableP);

	ITEM_TBL_ELEM	*element;	  /* temp pointer for the list */
	ITEM_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT       	temp_int;	  /* integer value */
	int				rc;			  /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_ITEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		item_tbl->count = 0;
		item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	item_tbl->count = (int) temp_int;

	/* malloc the list */
	item_tbl->list = (ITEM_TBL_ELEM *) malloc((size_t) (temp_int * 
			sizeof(ITEM_TBL_ELEM) ) );

	if (item_tbl->list == NULL) {

		item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) item_tbl->list, 0, (size_t) temp_int * sizeof(ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_type = (ITEM_TYPE) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_prog_tbl
 *	ShortDesc: evaluate the PROG_TBL
 *
 *	Description:
 *		eval_dir_prog_tbl will load the PROG_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		prog_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_prog_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	PROG_TBL* prog_tbl = static_cast<PROG_TBL*>(tableP);

	PROG_TBL_ELEM		*element;	 /* temp pointer for the list */
	PROG_TBL_ELEM		*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PROG_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		prog_tbl->count = 0;
		prog_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	prog_tbl->count = (int) temp_int;

	/* malloc the list */
	prog_tbl->list = (PROG_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(PROG_TBL_ELEM) ) );

	if (prog_tbl->list == NULL) {

		prog_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) prog_tbl->list, 0,
			(size_t) temp_int * sizeof(PROG_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = prog_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_local_var_tbl
 *	ShortDesc: evaluate the LOCAL_VAR_TBL
 *
 *	Description:
 *		eval_dir_local_var_tbl will load the LOCAL_VAR_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		local_var_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_local_var_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	LOCAL_VAR_TBL* local_var_tbl = static_cast<LOCAL_VAR_TBL*>(tableP);

	LOCAL_VAR_TBL_ELEM	*element;	  /* temp pointer for the list */
	LOCAL_VAR_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT        	temp_int;	  /* integer value */
	int					rc;			  /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		local_var_tbl->count = 0;
		local_var_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	local_var_tbl->count = (int) temp_int;

	/* malloc the list */
	local_var_tbl->list = (LOCAL_VAR_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(LOCAL_VAR_TBL_ELEM) ) );

	if (local_var_tbl->list == NULL) {

		local_var_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) local_var_tbl->list, 0,
			(size_t) temp_int * sizeof(LOCAL_VAR_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = local_var_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;

		/* parse type */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->type = (unsigned short) temp_int;

		/* parse size */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->size = (unsigned short) temp_int;

		/* parse DD reference */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->dd_ref.object_index = (OBJECT_INDEX) temp_int;

	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_cmd_num_id_tbl
 *	ShortDesc: evaluate the CMD_NUM_ID_TBL
 *
 *	Description:
 *		eval_dir_cmd_num_id_tbl will load the CMD_NUM_ID_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		cmd_num_id_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_cmd_num_id_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	CMD_NUM_ID_TBL* cmd_num_id_tbl = static_cast<CMD_NUM_ID_TBL*>(tableP);

	CMD_NUM_ID_TBL_ELEM	*element;	  /* temp pointer for the list */
	CMD_NUM_ID_TBL_ELEM	*end_element; /* end pointer for the list */
	DDL_UINT        	temp_int;	  /* integer value */
	int					rc;			  /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		cmd_num_id_tbl->count = 0;
		cmd_num_id_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	cmd_num_id_tbl->count = (int) temp_int;

	/* malloc the list */
	cmd_num_id_tbl->list = (CMD_NUM_ID_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(CMD_NUM_ID_TBL_ELEM) ) );

	if (cmd_num_id_tbl->list == NULL) {

		cmd_num_id_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) cmd_num_id_tbl->list, 0,
			(size_t) temp_int * sizeof(CMD_NUM_ID_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = cmd_num_id_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* command number */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->number = (unsigned long) temp_int;

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_id = (ITEM_ID) temp_int;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_dict_ref_tbl
 *	ShortDesc: evaluate the DICT_REF_TBL
 *
 *	Description:
 *		eval_dir_dict_ref_tbl will load the DICT_REF_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		dict_ref_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_dict_ref_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rhandle)
{
	DICT_REF_TBL* dict_ref_tbl = static_cast<DICT_REF_TBL*>(tableP);

	UINT32		*element;	 			/* temp pointer for the list */
	UINT32		*end_element;			/* end pointer for the list */
	DDL_UINT 	temp_int;	 			/* integer value */
	int			rc;						/* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_DICT_REF_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		dict_ref_tbl->count = 0;
		dict_ref_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	dict_ref_tbl->count = (int) temp_int;

	/* malloc the list */
	dict_ref_tbl->list = (UINT32 *) malloc(
			(size_t) (temp_int * sizeof(UINT32) ) );

	if (dict_ref_tbl->list == NULL) {

		dict_ref_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) dict_ref_tbl->list, 0,
			(size_t) temp_int * sizeof(UINT32));

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rhandle);

	/*
	 * load the list
	 */

	for (element = dict_ref_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		*element = (UINT32) temp_int;

		if (tok_major_rev == 8)
		{
			/* parse name length */
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			/* skip name */
			bin->chunk += temp_int;
			bin->size -= temp_int;

			/* parse entry length */
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			/* skip dictionary entry */
			bin->chunk += temp_int;
			bin->size -= temp_int;
		}
	}


	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_blk_tbl
 *	ShortDesc: evaluate the BLK_TBL
 *
 *	Description:
 *		eval_dir_blk_tbl will load the BLK_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	BLK_TBL* blk_tbl = static_cast<BLK_TBL*>(tableP);

	BLK_TBL_ELEM	*element;	 /* temp pointer for the list */
	BLK_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int;	 /* integer value */
	int				rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_BLK_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		blk_tbl->count = 0;
		blk_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_tbl->count = (int) temp_int;

	/* malloc the list */
	blk_tbl->list = (BLK_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(BLK_TBL_ELEM) ) );

	if (blk_tbl->list == NULL) {

		blk_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) blk_tbl->list, 0,
			(size_t) temp_int * sizeof(BLK_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = blk_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* load the name */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_id = (ITEM_ID) temp_int;


		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_rec_item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_rec_bint_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_dir_dd_ref.object_index = 
			(OBJECT_INDEX) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: make_dir_blk_tbl
 *	ShortDesc: make the BLK_TBL
 *
 *	Description:
 *		make_dir_blk_tbl will make the BLK_TBL structure
 *
 *	Inputs:
 *		attr_avail: the attributes that have been evaluated or made 
 *
 *	Outputs:
 *		blk_tbl: a pointer to the loaded table
 *		attr_avail: the attributes that have been evaluated or made 		
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
static
int make_dir_blk_tbl(
void*			tableP,
unsigned long*	attr_avail)
{
	BLK_TBL* blk_tbl = static_cast<BLK_TBL*>(tableP);

	BLK_TBL_ELEM	*element;	 /* temp pointer for the list */


#ifdef ISPTEST
	TEST_FAIL(MAKE_DIR_BLK_TBL);
#endif

	if (BLK_TBL_MASK & *attr_avail)
	{
		return DDL_SUCCESS;
	}

	blk_tbl->count = 1;

	/* malloc the list */
	blk_tbl->list = (BLK_TBL_ELEM *) malloc(
			(size_t) (sizeof(BLK_TBL_ELEM) ) );

	if (blk_tbl->list == NULL) {

		blk_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) blk_tbl->list, 0,
			(size_t) sizeof(BLK_TBL_ELEM));

	/*
	 * load the list
	 */

	element = blk_tbl->list;

	/* load the name */
	element->blk_id = 0x8000ff00;

	element->item_tbl_offset = -1;

	element->char_rec_item_tbl_offset = -1;

	element->char_rec_bint_offset = -1;

	element->blk_dir_dd_ref.object_index = 102;

	*attr_avail |= BLK_TBL_MASK;

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_rel_tbl
 *	ShortDesc: evaluate the REL_TBL
 *
 *	Description:
 *		eval_dir_rel_tbl will load the REL_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		rel_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_rel_tbl(
ENV_INFO	* /*env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rhandle)
{
	REL_TBL* rel_tbl = static_cast<REL_TBL*>(tableP);

	REL_TBL_ELEM	*element;	 /* temp pointer for the list */
	REL_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT		temp_int;	 /* integer value */
	int				rc;			 /* return code */

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_REL_TBL);
#endif


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		rel_tbl->count = 0;
		rel_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	rel_tbl->count = (int) temp_int;

	/* malloc the list */
	rel_tbl->list = (REL_TBL_ELEM *) malloc((size_t) (temp_int * sizeof(REL_TBL_ELEM)));

	if (rel_tbl->list == NULL) {

		rel_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) rel_tbl->list, 0,(size_t) temp_int * sizeof(REL_TBL_ELEM));

	/*
	 * load the list
	 */

    // NOTE: Some very old .fms files do not encode the unit_count. Other .fms files do.
    // Since there is no way to tell if this is one of the older ones, we try evaluating the
    // table first without the unit_count. If all of the binary chunk has been evaluated (bin->size == 0),
    // then we are done. Otherwise, we will try evaluating again, this time reading the unit_count
    // from the binary.
    // PLEASE NOTE that this algorithm works because evaluation only requires the use of DLL_PARSE_INTEGER().

    DDL_UINT unused_offset; // The value that indicates that an offset is not used

    bool bReadUnitCount;    // Indication that we should read the unit_count from the binary
    int iNumberOfTries;     // Number of times that we should try evaluating this table

    if (tok6or8)
    {
        unused_offset = UNUSED_OFFSET;

        bReadUnitCount = true;      // We always read the unit_count
        iNumberOfTries = 1;         // It only takes one try
    }
    else    // This is an .fms file
	{
		unused_offset = UNUSED_OFFSET_5;

        bReadUnitCount = false;     // First time around, assume that unit count is not encoded
        iNumberOfTries = 2;         // We might need to try again.
	}
	
    unsigned char *ucSaveChunk = bin->chunk;    // Save the chunk and size in case we need to try again
    unsigned long ulSaveSize = bin->size;

    for (int i = 0; i < iNumberOfTries; i++)
    {
        bin->chunk = ucSaveChunk;
        bin->size = ulSaveSize;

        for (element = rel_tbl->list, end_element = element + rel_tbl->count;
            element < end_element; element++) {

            DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
            if (temp_int == unused_offset) {
                element->wao_item_tbl_offset = -1;
            }
            else {
                element->wao_item_tbl_offset = (int)temp_int;
            }

            DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
            if (temp_int == unused_offset) {
                element->unit_item_tbl_offset = -1;
            }
            else {
                element->unit_item_tbl_offset = (int)temp_int;
            }

            DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
            if (temp_int == unused_offset) {
                element->update_tbl_offset = -1;
            }
            else {
                element->update_tbl_offset = (int)temp_int;
            }

            DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
            element->update_count = (int)temp_int;

            if (bReadUnitCount)
            {
                DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
                element->unit_count = (int)temp_int;
            }
            else    // If we aren't reading the unit_count from the binary, figure it out ourselves
            {   // If there is no unit_item_tbl_offset, then count is zero, otherwise 1
                element->unit_count = (element->unit_item_tbl_offset == -1) ? 0 : 1;
            }
        }

        if (!tok6or8)   // We are evaluating an .fms file
        {
            if (bin->size != 0)   // we did not read the entire table
            {
                bReadUnitCount = true;  // Go around again, this time reading the unit_count from the binary
            }
            else
            {
                break;  // we are done.
            }
        }
    }

    ASSERT_RET(bin->size == 0, DDL_ENCODING_ERROR);

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_update_tbl
 *	ShortDesc: evaluate the UPDATE_TBL
 *
 *	Description:
 *		eval_dir_update_tbl will load the UPDATE_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		update_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_update_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	UPDATE_TBL* update_tbl = static_cast<UPDATE_TBL*>(tableP);

	UPDATE_TBL_ELEM	*element;			/* temp pointer for the list */
	UPDATE_TBL_ELEM	*end_element;		/* end pointer for the list */
	DDL_UINT 		temp_int;	 		/* integer value */
	int				rc;					/* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_UPDATE_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		update_tbl->count = 0;
		update_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	update_tbl->count = (int) temp_int;

	/* malloc the list */
	update_tbl->list = (UPDATE_TBL_ELEM *) malloc(
		(size_t) (temp_int * sizeof(UPDATE_TBL_ELEM) ) );

	if (update_tbl->list == NULL) {

		update_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) update_tbl->list, 0,
			(size_t) temp_int * sizeof(UPDATE_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = update_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->desc_it_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->op_it_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->op_subindex = (SUBINDEX) temp_int;
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_command_tbl
 *	ShortDesc: evaluate the COMMAND_TBL
 *
 *	Description:
 *		eval_dir_command_tbl will load the COMMAND_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		command_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_command_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	COMMAND_TBL* command_tbl = static_cast<COMMAND_TBL*>(tableP);

	COMMAND_TBL_ELEM	*element;			/* temp pointer for the list of commands*/
	COMMAND_TBL_ELEM	*end_element;		/* end pointer for the list of commands */
	COMMAND_INDEX		*index;				/* temp pointer for the list of indexes */
	COMMAND_INDEX		*end_index;			/* end pointer for the list of indexes */
	DDL_UINT 			temp_int; 		    /* integer value */
	int					rc;				    /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		command_tbl->count = 0;
		command_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	command_tbl->count = (int) temp_int;

	/* malloc the list */
	command_tbl->list = (COMMAND_TBL_ELEM *) malloc(
		(size_t) (temp_int * sizeof(COMMAND_TBL_ELEM) ) );

	if (command_tbl->list == NULL) {

		command_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) command_tbl->list, 0,
			(size_t) temp_int * sizeof(COMMAND_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = command_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->subindex = (unsigned short) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->number = (unsigned long) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->transaction = (unsigned long) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->weight = (unsigned short) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->count = (int) temp_int;

		/*
		 * if there are index elements, parse them
		 */

		if (temp_int) {

				/* malloc the list */
			element->index_list = (COMMAND_INDEX *) malloc(
					(size_t) (temp_int * sizeof(COMMAND_INDEX) ) );

			if (element->index_list == NULL) {

				element->count = 0;
				return DDL_MEMORY_ERROR;
			}

			/*
			 * load the list of indexes
			 */

			for (index = element->index_list, end_index = index + temp_int;
					 index < end_index; index++) {

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				index->id= (ITEM_ID) temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				index->value = (unsigned long) temp_int;

			}
		}
		else {

			element->index_list = NULL;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_command_tbl_8
 *	ShortDesc: evaluate the COMMAND_TBL_8
 *
 *	Description:
 *		eval_dir_command_tbl_8 will load the COMMAND_TBL_8 structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		tableP: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Ying Xu
 *
 **********************************************************************/

static
int eval_dir_command_tbl_8(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	COMMAND_TBL_8* flat_item_tbl = static_cast<COMMAND_TBL_8*>(tableP);

	PTOC_TBL_8_ELEM  *item_tbl_element;
	PTOC_TBL_8_ELEM  *end_item_tbl_element;

	COMMAND_TBL_ELEM	*ptoc_tbl_element;			/* temp pointer for the list of commands*/
	COMMAND_TBL_ELEM	*end_ptoc_tbl_element;		/* end pointer for the list of commands */
	
	COMMAND_INDEX		*cmd_index;				/* temp pointer for the list of indexes */
	COMMAND_INDEX		*end_cmd_index;			/* end pointer for the list of indexes */
	DDL_UINT 			temp_int; 		    /* integer value */
	int					rc;				    /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);


	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
	int ptoc_tbl_8_elem_count = (int) temp_int;

	/*
	 * if count is zero
	 */

	if(!ptoc_tbl_8_elem_count)
	{
		flat_item_tbl->count = 0;
		flat_item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	flat_item_tbl->count = ptoc_tbl_8_elem_count;

	/* malloc the list */
	flat_item_tbl->list = (PTOC_TBL_8_ELEM *) malloc(
		(size_t) (ptoc_tbl_8_elem_count * sizeof(PTOC_TBL_8_ELEM) ) );

	if (flat_item_tbl->list == NULL) 
	{
		flat_item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) flat_item_tbl->list, 0,
			(size_t) ptoc_tbl_8_elem_count * sizeof(PTOC_TBL_8_ELEM));

	/*
	 * load the list
	 */

	for (item_tbl_element = flat_item_tbl->list, end_item_tbl_element = item_tbl_element + ptoc_tbl_8_elem_count;
		 item_tbl_element < end_item_tbl_element; item_tbl_element++) 
	{
		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		item_tbl_element->item_id = (ITEM_ID) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		int command_tbl_8_elem_count = (int)temp_int;
		item_tbl_element->rd_count = command_tbl_8_elem_count;

		if (!command_tbl_8_elem_count)
		{
			item_tbl_element->rd_list = NULL;
		}
		else
		{
			// there are read commands
			item_tbl_element->rd_list = (COMMAND_TBL_ELEM *) malloc(
				(size_t) (command_tbl_8_elem_count * sizeof(COMMAND_TBL_ELEM) ) );

			if (item_tbl_element->rd_list == NULL)
			{
				item_tbl_element->rd_count = 0;
				return DDL_MEMORY_ERROR;
			}
		
			/* load list with zeros */
			memset((char *) item_tbl_element->rd_list, 0,
					(size_t) command_tbl_8_elem_count * sizeof(COMMAND_TBL_ELEM));

			for (ptoc_tbl_element = item_tbl_element->rd_list, end_ptoc_tbl_element = ptoc_tbl_element + command_tbl_8_elem_count;
				ptoc_tbl_element < end_ptoc_tbl_element; ptoc_tbl_element++)
			{
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->subindex = (SUBINDEX)temp_int;			
				
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->number = (ulong)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->transaction = (ulong)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->weight = (ushort)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				int command_index_count = (int)temp_int;
				ptoc_tbl_element->count = command_index_count;

				if (!command_index_count)
				{
					ptoc_tbl_element->index_list = NULL;
				}
				else
				{
					// there are index elements, parse them
					ptoc_tbl_element->index_list = (COMMAND_INDEX *) malloc(
						(size_t) (command_index_count * sizeof(COMMAND_INDEX) ) );

					if (ptoc_tbl_element->index_list == NULL)
					{
						ptoc_tbl_element->count = 0;
						return DDL_MEMORY_ERROR;
					}
				
					/* load list with zeros */
					memset((char *) ptoc_tbl_element->index_list, 0,
							(size_t) command_index_count * sizeof(COMMAND_INDEX));

					//
					// load the list of indexes
					//
					for (cmd_index = ptoc_tbl_element->index_list, end_cmd_index = cmd_index + command_index_count;
						cmd_index < end_cmd_index; cmd_index++)
					{
						DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
						cmd_index->id = (ITEM_ID)temp_int;

						DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
						cmd_index->value = (ulong)temp_int;
					}
				}
			}
		}


		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		command_tbl_8_elem_count  = (int)temp_int;
		item_tbl_element->wr_count = command_tbl_8_elem_count;

		if (!command_tbl_8_elem_count)
		{
			item_tbl_element->wr_list = NULL;
		}
		else
		{
			// there are read commands
			item_tbl_element->wr_list = (COMMAND_TBL_ELEM *) malloc(
				(size_t) (command_tbl_8_elem_count * sizeof(COMMAND_TBL_ELEM) ) );

			if (item_tbl_element->wr_list == NULL)
			{
				item_tbl_element->wr_count = 0;
				return DDL_MEMORY_ERROR;
			}
		
			/* load list with zeros */
			memset((char *) item_tbl_element->wr_list, 0,
					(size_t) command_tbl_8_elem_count * sizeof(COMMAND_TBL_ELEM));

			for (ptoc_tbl_element = item_tbl_element->wr_list, end_ptoc_tbl_element = ptoc_tbl_element + command_tbl_8_elem_count;
				ptoc_tbl_element < end_ptoc_tbl_element; ptoc_tbl_element++)
			{
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->subindex = (SUBINDEX)temp_int;			
				
				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->number = (ulong)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->transaction = (ulong)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				ptoc_tbl_element->weight = (ushort)temp_int;

				DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
				int command_index_count = (int)temp_int;
				ptoc_tbl_element->count = command_index_count;

				if (!command_index_count)
				{
					ptoc_tbl_element->index_list = NULL;
				}
				else
				{
					// there are index elements, parse them
					ptoc_tbl_element->index_list = (COMMAND_INDEX *) malloc(
						(size_t) (command_index_count * sizeof(COMMAND_INDEX) ) );

					if (ptoc_tbl_element->index_list == NULL)
					{
						ptoc_tbl_element->count = 0;
						return DDL_MEMORY_ERROR;
					}
				
					/* load list with zeros */
					memset((char *) ptoc_tbl_element->index_list, 0,
							(size_t) command_index_count * sizeof(COMMAND_INDEX));

					//
					// load the list of indexes
					//
					for (cmd_index = ptoc_tbl_element->index_list, end_cmd_index = cmd_index + command_index_count;
						cmd_index < end_cmd_index; cmd_index++)
					{
						DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
						cmd_index->id = (ITEM_ID)temp_int;

						DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
						cmd_index->value = (ulong)temp_int;
					}
				}
			}
		}
	}

	return DDL_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////////		
//	Name: eval_dir_crit_param_tbl												//
//	ShortDesc: evaluate the CRIT_PARAM_TBL										//
//	Description: eval_dir_crit_param_tbl will load the CRIT_PARAM_TBL structure	//
//			by parsing the binary in bin										//
//	Inputs: bin: a pointer to the binary to parse								//
//	Outputs: crit_param_tbl: a pointer to the loaded table						//
//	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR										//
//	Author: SF																	//
//////////////////////////////////////////////////////////////////////////////////
#ifndef SUN
#pragma warning (disable : 4100)
#endif
static int eval_dir_crit_param_tbl( ENV_INFO*, void* tableP, BININFO* bin, ROD_HANDLE )
{
	CRIT_PARAM_TBL* crit_param_tbl = static_cast<CRIT_PARAM_TBL*>(tableP);

	DDL_UINT temp_int	= 0;			// integer value
	DDL_UINT count		= 0;			// element count
	int rc				= 0;			// return code, used in DDL_PARSE_INTEGER(...)

	ASSERT_DBG(bin && bin->chunk && bin->size);

	// parse count
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);

	//if count is zero
	if( !count )
	{
		crit_param_tbl->count = 0;
		crit_param_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	// set the count
	crit_param_tbl->count = (int)count;

	// allocate memory for the list
	crit_param_tbl->list = (ITEM_ID *)malloc((size_t) (count * sizeof(ITEM_ID)));

	if( crit_param_tbl->list == NULL )
	{
		crit_param_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	// load list with zeros
	memset( (char *) crit_param_tbl->list, 0, (size_t)count *sizeof(ITEM_ID) );

	int nIndex = 0;
	while( bin->size > 0 )
	{
		DDL_PARSE_INTEGER( &bin->chunk, &bin->size, &temp_int );
		if( nIndex < crit_param_tbl->count )
		{
			crit_param_tbl->list[nIndex] = temp_int;
			nIndex++;
		}
		else
		{
			return DDL_MEMORY_ERROR; // error
		}
	}
	return SUCCESS;
}
#ifndef SUN
#pragma warning (default : 4100)
#endif

//////////////////////////////////////////////////////////////////////////////////		
//	Name: eval_dir_rslv_ref_tbl												//
//	ShortDesc: evaluate the RESLV_REF_TO_CMD_TBL										//
//	Description: eval_dir_rslv_ref_tbl will load the RESLV_REF_TO_CMD_TBL structure	//
//			by parsing the binary in bin										//
//	Inputs: bin: a pointer to the binary to parse								//
//	Outputs: crit_param_tbl: a pointer to the loaded table						//
//	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR										//
//	Author: SF																	//
//////////////////////////////////////////////////////////////////////////////////

static int eval_dir_rslv_ref_tbl( ENV_INFO*, void* tableP, BININFO* bin, ROD_HANDLE )
{
	RESLV_REF_TO_CMD_TBL* reslv_ref_cmd_tbl = static_cast<RESLV_REF_TO_CMD_TBL*>(tableP);

	DDL_UINT	temp_uint = 0; 		    /* unsigned integer value */
	int			rc = 0;				    /* return code */

	ASSERT_DBG(bin && bin->chunk && bin->size);

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_uint);

	/*
	 * if count is zero
	 */

	if(temp_uint == 0)
	{
		reslv_ref_cmd_tbl->count = 0;
		reslv_ref_cmd_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	reslv_ref_cmd_tbl->count = (int) temp_uint;

	/* malloc the list */
	reslv_ref_cmd_tbl->list = (RESLV_TBL_ELEM *) calloc(temp_uint, sizeof(RESLV_TBL_ELEM) );

	if (reslv_ref_cmd_tbl->list == NULL) {
		reslv_ref_cmd_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/*
	 * load the list
	 */
	RESLV_TBL_ELEM* pElement = reslv_ref_cmd_tbl->list;

	for (int i=0; i < reslv_ref_cmd_tbl->count; i++, pElement++)
	{
		rc = ddl_eval_RESLV_TBL_ELEM(&bin->chunk, &bin->size, pElement);
		
		if(rc != DDL_SUCCESS) {
			return rc;
		}
	}

	if(bin->size != 0) {
		return DDL_ENCODING_ERROR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_tbl
 *	ShortDesc: evaluate the PARAM_TBL
 *
 *	Description:
 *		eval_dir_param_tbl will load the PARAM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rod_handle)
{
	PARAM_TBL* param_tbl = static_cast<PARAM_TBL*>(tableP);

	PARAM_TBL_ELEM	*element;	 		/* temp pointer for the list */
	DDL_UINT        temp_int;			/* integer value */
	DDL_UINT		count;				/* element count */
	DDL_UINT		tag;				/* the tag value */
	int				rc;					/* return code */
	unsigned long	inc = 0;			/* element tracker */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);

	/*
	 * if count is zero
	 */

	if(!count) {

		param_tbl->count = 0;
		param_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_tbl->count = (int) count;

	/* malloc the list */
	param_tbl->list = (PARAM_TBL_ELEM *)
			malloc((size_t) (count * sizeof(PARAM_TBL_ELEM)));

	if (param_tbl->list == NULL) {

		param_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_tbl->list, 0,
			(size_t) count * sizeof(PARAM_TBL_ELEM));

	/*
	 * load the list
	 */

	element = param_tbl->list;

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rod_handle);

	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {


		case PT_BLK_ITEM_NAME_TBL_OFFSET_TAG:

			/*
			 * Increment the element pointer
			 */

			if (inc > 0) {

				if(inc == count) {

					return DDL_ENCODING_ERROR;
				}
				element++;
			}
			++inc;

			/*
			 * Set optional table offsets to -1
		     */
			element->param_mem_tbl_offset = (int) -1;
			element->param_elem_tbl_offset = (int) -1;
			element->array_elem_item_tbl_offset = (int) -1;

			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name_tbl_offset = (int) temp_int;
			break;

		case PT_PARAM_MEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->param_mem_tbl_offset = (int) temp_int;
			}
			else
			{
				//Tokenizer 8 generates a number that does not make any sense.
				element->param_mem_tbl_offset = (int) -1;
			}
			break;

		case PT_PARAM_MEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->param_mem_count = temp_int;
			}
			else
			{
				//Tokenizer 8 generates a number that does not make any sense.
				element->param_mem_count = (int) 0;
			}
			break;

		case PT_PARAM_ELEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->param_elem_tbl_offset = (int) temp_int;
			}
			else
			{
				//Tokenizer 8 generates a number that does not make any sense.
				element->param_elem_tbl_offset = (int) -1;
			}
			break;

		case PT_PARAM_ELEM_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->param_elem_count = (int) temp_int;
			}
			else
			{
				//Tokenizer 8 generates a number that does not make any sense.
				element->param_elem_count = (int) 0;
			}
			break;

		case PT_PARAM_ELEM_MAX_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_count = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_item_tbl_offset = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_TYPE_OR_VAR_TYPE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_type_or_var_type = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_SIZE_OR_VAR_SIZE_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_size_or_var_size = (int) temp_int;
			break;

		case PT_ARRAY_ELEM_CLASS_VAR_CLASS_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->array_elem_class_or_var_class = (ulong) temp_int;
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_blk_item_tbl
 *	ShortDesc: evaluate the BLK_ITEM_TBL
 *
 *	Description:
 *		eval_dir_blk_item_tbl will load the BLK_ITEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	BLK_ITEM_TBL* blk_item_tbl = static_cast<BLK_ITEM_TBL*>(tableP);

	BLK_ITEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	BLK_ITEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_BLK_ITEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		blk_item_tbl->count = 0;
		blk_item_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_tbl->count = (int) temp_int;

	/* malloc the list */
	blk_item_tbl->list = (BLK_ITEM_TBL_ELEM *) malloc((size_t) (temp_int * 
				sizeof(BLK_ITEM_TBL_ELEM) ) );

	if (blk_item_tbl->list == NULL) {

		blk_item_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) blk_item_tbl->list, 0, (size_t) temp_int * sizeof(BLK_ITEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = blk_item_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_id = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;	
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_param_mem_name_tbl will load the PARAM_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_name_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	PARAM_MEM_NAME_TBL* param_mem_name_tbl = static_cast<PARAM_MEM_NAME_TBL*>(tableP);

	PARAM_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_MEM_NAME_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_name_tbl->count = 0;
		param_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_name_tbl->list = (PARAM_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * 
				sizeof(PARAM_MEM_NAME_TBL_ELEM) ) );

	if (param_mem_name_tbl->list == NULL) {

		param_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_mem_name_tbl->list, 0, (size_t) temp_int * sizeof(PARAM_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_mem_name_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_name = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: eval_dir_param_elem_tbl
 *	ShortDesc: evaluate the PARAM_ELEM_TBL
 *
 *	Description:
 *		eval_dir_param_elem_tbl will load the PARAM_ELEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_elem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_elem_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rhandle)
{
	PARAM_ELEM_TBL* param_elem_tbl = static_cast<PARAM_ELEM_TBL*>(tableP);

	PARAM_ELEM_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_ELEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_ELEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_elem_tbl->count = 0;
		param_elem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_elem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_elem_tbl->list = (PARAM_ELEM_TBL_ELEM *) malloc((size_t) (temp_int * 
				sizeof(PARAM_ELEM_TBL_ELEM) ) );

	if (param_elem_tbl->list == NULL) {

		param_elem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_elem_tbl->list, 0, (size_t) temp_int * sizeof(PARAM_ELEM_TBL_ELEM));

	/*
	 * load the list
	 */

	DDL_UINT unused_offset;
	if (tok6or8)
	{
		unused_offset = UNUSED_OFFSET;
	}
	else
	{
		unused_offset = UNUSED_OFFSET_5;
	}

	for (element = param_elem_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse parameter element subindex */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_elem_subindex = (SUBINDEX) temp_int;		


		/* parse relation table offset */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == unused_offset) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_char_mem_name_tbl
 *	ShortDesc: evaluate the CHAR_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_char_mem_name_tbl will load the CHAR_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		char_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_char_mem_name_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	CHAR_MEM_NAME_TBL* char_mem_name_tbl = static_cast<CHAR_MEM_NAME_TBL*>(tableP);

	CHAR_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	CHAR_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_CHAR_MEM_NAME_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		char_mem_name_tbl->count = 0;
		char_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	char_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	char_mem_name_tbl->list = (CHAR_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * 
				sizeof(CHAR_MEM_NAME_TBL_ELEM) ) );

	if (char_mem_name_tbl->list == NULL) {

		char_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) char_mem_name_tbl->list, 0, (size_t) temp_int * sizeof(CHAR_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = char_mem_name_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_name = (ITEM_ID) temp_int;		


		/* parse item index */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_name_tbl
 *	ShortDesc: evaluate the PARAM_LIST_MEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_param_list_mem_name_tbl will load the PARAM_LIST_MEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_mem_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_name_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	PARAM_LIST_MEM_NAME_TBL* param_list_mem_name_tbl = static_cast<PARAM_LIST_MEM_NAME_TBL*>(tableP);

	PARAM_LIST_MEM_NAME_TBL_ELEM	*element;	 /* temp pointer for the list */
	PARAM_LIST_MEM_NAME_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT        	temp_int;	 /* integer value */
	int					rc;			 /* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_LIST_MEM_NAME_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_mem_name_tbl->count = 0;
		param_list_mem_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_mem_name_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_mem_name_tbl->list = (PARAM_LIST_MEM_NAME_TBL_ELEM *) malloc((size_t) (temp_int * 
				sizeof(PARAM_LIST_MEM_NAME_TBL_ELEM) ) );

	if (param_list_mem_name_tbl->list == NULL) {

		param_list_mem_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_list_mem_name_tbl->list, 0, (size_t) temp_int * sizeof(PARAM_LIST_MEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_mem_name_tbl->list,	end_element = element + temp_int;
		 element < end_element; element++) {

		/* parse ITEM_ID */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_name = (ITEM_ID) temp_int;		


		/* parse item offset */

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_list_mem_tbl_offset = (int) temp_int;		
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_blk_item_name_tbl
 *	ShortDesc: evaluate the BLK_ITEM_NAME_TBL
 *
 *	Description:
 *		eval_dir_blk_item_name_tbl will load the BLK_ITEM_NAME_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		blk_item_name_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_blk_item_name_tbl(
ENV_INFO	* /* env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rod_handle)
{
	BLK_ITEM_NAME_TBL* blk_item_name_tbl = static_cast<BLK_ITEM_NAME_TBL*>(tableP);

	BLK_ITEM_NAME_TBL_ELEM	*element;	/* temp pointer for the list */
	DDL_UINT				count;		/* element count */
	DDL_UINT				tag;		/* the tag value */
	DDL_UINT       			temp_int;	/* integer value */
	int						rc;			/* return code */
	unsigned long			inc = 0;	/* element tracker */

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_BLK_ITEM_NAME_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &count);


	/*
	 * if count is zero
	 */

	if(!count) {

		blk_item_name_tbl->count = 0;
		blk_item_name_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	blk_item_name_tbl->count = (int) count;

	/* malloc the list */
	blk_item_name_tbl->list = (BLK_ITEM_NAME_TBL_ELEM *)
			malloc((size_t)(count * sizeof(BLK_ITEM_NAME_TBL_ELEM) ) );

	if (blk_item_name_tbl->list == NULL) {

		blk_item_name_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) blk_item_name_tbl->list, 0,
		(size_t) count * sizeof(BLK_ITEM_NAME_TBL_ELEM));

	/*
	 * load the list
	 */


	element = blk_item_name_tbl->list;

	int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rod_handle);

	while (bin->size) {

		DDL_PARSE_TAG(&bin->chunk, &bin->size, &tag, (DDL_UINT *) NULL_PTR);
		switch (tag) {

		case BINT_BLK_ITEM_NAME_TAG:
			/*
			 * Increment the element pointer
			 */

			if (inc > 0) {

				if(inc == count) {

					return DDL_ENCODING_ERROR;
				}

				++element;
			}
			++inc;

			/*
			 * Set optional table offsets to -1
		     */

			element->item_tbl_offset = (int) -1;
			element->param_tbl_offset = (int) -1;
			element->param_list_tbl_offset = (int) -1;
			element->rel_tbl_offset = (int) -1;
			element->read_cmd_tbl_offset = (int) -1;
			element->write_cmd_tbl_offset = (int) -1;


			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->blk_item_name = (ITEM_ID) temp_int;
			break;

		case BINT_ITEM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->item_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_tbl_offset = (int) temp_int;
			break;

		case BINT_PARAM_LIST_TBL_OFFSET_TAG:
			//Tokenizer 8 generates a number that does not make any sense.
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->param_list_tbl_offset = (int) -1;
			break;

		case BINT_REL_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			element->rel_tbl_offset = (int) temp_int;
			break;

		case BINT_READ_CMD_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			//Tokenizer 8 generates a number that does not make any sense.
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->read_cmd_tbl_offset = (int) temp_int;
			}
			break;

		case BINT_READ_CMD_TBL_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			//Tokenizer 8 generates a number that does not make any sense.
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->read_cmd_count = (int) temp_int;
			}
			break;

		case BINT_WRITE_CMD_TBL_OFFSET_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			//Tokenizer 8 generates a number that does not make any sense.
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->write_cmd_tbl_offset = (int) temp_int;
			}
			break;

		case BINT_WRITE_CMD_TBL_COUNT_TAG:
			DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
			//Tokenizer 8 generates a number that does not make any sense.
			if (tok_major_rev == 5 || tok_major_rev == 6)
			{
				element->write_cmd_count = (int) temp_int;
			}
			break;

		default:
			return DDL_ENCODING_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_dir_char_mem_tbl
 *	ShortDesc: evaluate the CHAR_MEM_TBL
 *
 *	Description:
 *		eval_dir_char_mem_tbl will load the CHAR_MEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		char_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_char_mem_tbl(
ENV_INFO	* /*env_info */,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rhandle)
{
	CHAR_MEM_TBL* char_mem_tbl = static_cast<CHAR_MEM_TBL*>(tableP);

	CHAR_MEM_TBL_ELEM	*element;		/* temp pointer for the list */
	CHAR_MEM_TBL_ELEM	*end_element;	/* end pointer for the list */
	DDL_UINT 		temp_int;	 		/* integer value */
	int				rc;					/* return code */

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_CHAR_MEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		char_mem_tbl->count = 0;
		char_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	char_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	char_mem_tbl->list = (CHAR_MEM_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(CHAR_MEM_TBL_ELEM) ) );

	if (char_mem_tbl->list == NULL) {

		char_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) char_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(CHAR_MEM_TBL_ELEM));

	/*
	 * load the list
	 */
	DDL_UINT unused_offset;
	if (tok6or8)
	{
		unused_offset = UNUSED_OFFSET;
	}
	else
	{
		unused_offset = UNUSED_OFFSET_5;
	}

	for (element = char_mem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_type = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_size = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->char_mem_class = (ulong) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == unused_offset) {		
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}


	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_list_mem_tbl
 *	ShortDesc: evaluate the PARAM_LIST_MEM_TBL
 *
 *	Description:
 *		eval_dir_param_list_mem_tbl will load the PARAM_LIST_MEM_TBL
 *		structure by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_list_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_list_mem_tbl(
ENV_INFO*	/*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE)
{
	PARAM_LIST_MEM_TBL* param_list_mem_tbl = static_cast<PARAM_LIST_MEM_TBL*>(tableP);

	PARAM_LIST_MEM_TBL_ELEM	*element;	/* temp pointer for the list */
	PARAM_LIST_MEM_TBL_ELEM	*end_element;/* end pointer for the list */
	DDL_UINT 				temp_int;	/* integer value */
	int						rc;			/* return code */


	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_LIST_MEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_list_mem_tbl->count = 0;
		param_list_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_list_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_list_mem_tbl->list = (PARAM_LIST_MEM_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(PARAM_LIST_MEM_TBL_ELEM) ) );

	if (param_list_mem_tbl->list == NULL) {

		param_list_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_list_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_LIST_MEM_TBL_ELEM));

	/*
	 * load the list
	 */

	for (element = param_list_mem_tbl->list, end_element = element +
			temp_int; element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->blk_item_name_tbl_offset = (int) temp_int;
	}


	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_dir_param_mem_tbl
 *	ShortDesc: evaluate the PARAM_MEM_TBL
 *
 *	Description:
 *		eval_dir_param_mem_tbl will load the PARAM_MEM_TBL structure
 *		by parsing the binary in bin
 *
 *	Inputs:
 *		bin: a pointer to the binary to parse
 *
 *	Outputs:
 *		param_mem_tbl: a pointer to the loaded table
 *
 *	Returns: DDL_SUCCESS, DDL_MEMORY_ERROR
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int eval_dir_param_mem_tbl(
ENV_INFO	* /*env_info*/,
void*		tableP,
BININFO*	bin,
ROD_HANDLE rhandle)
{
	PARAM_MEM_TBL* param_mem_tbl = static_cast<PARAM_MEM_TBL*>(tableP);

	PARAM_MEM_TBL_ELEM	*element;		/* temp pointer for the list */
	PARAM_MEM_TBL_ELEM	*end_element;	/* end pointer for the list */
	DDL_UINT            temp_int;	 	/* integer value */
	int                 rc;				/* return code */

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

	ASSERT_DBG(bin && bin->chunk && bin->size);

#ifdef ISPTEST
	TEST_FAIL(EVAL_PARAM_MEM_TBL);
#endif

	/* parse count */
	DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);

	/*
	 * if count is zero
	 */

	if(!temp_int) {

		param_mem_tbl->count = 0;
		param_mem_tbl->list = NULL;
		return DDL_SUCCESS;
	}

	param_mem_tbl->count = (int) temp_int;

	/* malloc the list */
	param_mem_tbl->list = (PARAM_MEM_TBL_ELEM *) malloc(
			(size_t) (temp_int * sizeof(PARAM_MEM_TBL_ELEM) ) );

	if (param_mem_tbl->list == NULL) {

		param_mem_tbl->count = 0;
		return DDL_MEMORY_ERROR;
	}

	/* load list with zeros */
	memset((char *) param_mem_tbl->list, 0,
			(size_t) temp_int * sizeof(PARAM_MEM_TBL_ELEM));

	/*
	 * load the list
	 */
	DDL_UINT unused_offset;
	if (tok6or8)
	{
		unused_offset = UNUSED_OFFSET;
	}
	else
	{
		unused_offset = UNUSED_OFFSET_5;
	}

	for (element = param_mem_tbl->list, end_element = element + temp_int;
		 element < end_element; element++) {

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->item_tbl_offset = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_type = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_size = (int) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		element->param_mem_class = (ulong) temp_int;

		DDL_PARSE_INTEGER(&bin->chunk, &bin->size, &temp_int);
		if (temp_int == unused_offset) {
			element->rel_tbl_offset = -1;
		}
		else {
			element->rel_tbl_offset = (int) temp_int;
		}
	}

	return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: dir_mask_man()
 *	ShortDesc: dir_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		dir_mask_man() handles all mask switch cases for the directory structures
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		bin_exists:		the mask which indicates which binarys are available
 *		bin_hooked:		the mask which indicates which binarys are hooked to the bin structure
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		bin:			the binary to evaluate
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by dir_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
dir_mask_man(
unsigned long   attr_mask,
UINT32			bin_exists,
UINT32			bin_hooked,
unsigned long*	attr_avail,
ENV_INFO		*env_info,
eval_type		eval,
void*			attribute,
BININFO*		bin,
ROD_HANDLE		rod_handle = -1)
{

	int             rc;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(MASK_MAN_DIR);
#endif

	/*
	 * No binary exists
	 */

	if (!(attr_mask & bin_exists)) {

		/*
		 * This is a DDOD error,
         * by definition all directory tables
         * must have binary available
		 */

		rc = DDL_BINARY_REQUIRED;
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & bin_hooked)) {

		/*
		 * If value is already available
		 */

		if (attr_mask & *attr_avail) {

			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else {

			rc = DDL_BINARY_REQUIRED;
		}
	}

	else {

		/*
		 * check masks for evaluating
		 */

		if (!(attr_mask & *attr_avail)) {

			rc = eval(env_info, attribute, bin, rod_handle);

			if (rc == DDL_SUCCESS) {

				*attr_avail |= attr_mask;
			}
		}

		/*
		 * evaluation is not necessary
		 */

		else {

			rc = DDL_SUCCESS;
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_dir_block_tables
 *	ShortDesc: evaluate (ie. create) the block tables
 *
 *	Description:
 *		eval_dir_block_tables will load the desired block tables into the 
 *		FLAT_BLOCK_DIR structure. The user must specify which block tables
 *		are desired by setting the appropriate bits in the "mask parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying which block tables are requested.
 *
 *	Outputs:
 *		block_dir:	pointer to a FLAT_BLOCK_DIR structure. Will contain
 *					the desired block tables.
 *      block_bin:  pointer to a BIN_BLOCK_DIR structure. Stores the 
 *					binary block information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		returns from other dds functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
eval_dir_block_tables(
FLAT_BLOCK_DIR 		*block_dir,
BIN_BLOCK_DIR		*block_bin,
ENV_INFO			*env_info,
unsigned long   	mask,
ROD_HANDLE			rod_handle)
{

	int  rc = DDS_SUCCESS;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_BLOCK_DIR);
#endif

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	if (mask & BLK_ITEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_blk_item_tbl,
								&block_dir->blk_item_tbl,
								&block_bin->blk_item_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & CHAR_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CHAR_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_char_mem_tbl,
								&block_dir->char_mem_tbl,
								&block_bin->char_mem_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & BLK_ITEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) BLK_ITEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_blk_item_name_tbl,
								&block_dir->blk_item_name_tbl,
								&block_bin->blk_item_name_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_list_mem_tbl,
								&block_dir->param_list_mem_tbl,
								&block_bin->param_list_mem_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_list_tbl,
								&block_dir->param_list_tbl,
								&block_bin->param_list_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}	

	if (mask & PARAM_MEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_mem_tbl,
								&block_dir->param_mem_tbl,
								&block_bin->param_mem_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_tbl,
								&block_dir->param_tbl,
								&block_bin->param_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_mem_name_tbl,
								&block_dir->param_mem_name_tbl,
								&block_bin->param_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_ELEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_ELEM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_elem_tbl,
								&block_dir->param_elem_tbl,
								&block_bin->param_elem_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & PARAM_LIST_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PARAM_LIST_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_param_list_mem_name_tbl,
								&block_dir->param_list_mem_name_tbl,
								&block_bin->param_list_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & CHAR_MEM_NAME_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CHAR_MEM_NAME_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_char_mem_name_tbl,
								&block_dir->char_mem_name_tbl,
								&block_bin->char_mem_name_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & REL_TBL_MASK) {

		rc = dir_mask_man((unsigned long) REL_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_rel_tbl,
								&block_dir->rel_tbl,
								&block_bin->rel_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & UPDATE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) UPDATE_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_update_tbl,
								&block_dir->update_tbl,
								&block_bin->update_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & COMMAND_TBL_MASK)
	{
		int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rod_handle);
		if (tok_major_rev != 8)
		{
			rc = dir_mask_man((unsigned long) COMMAND_TBL_MASK,
									block_bin->bin_exists,
									block_bin->bin_hooked,
									&block_dir->attr_avail,
									env_info,
									eval_dir_command_tbl,
									&block_dir->command_tbl,
									&block_bin->command_tbl);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
		else
		{
			rc = dir_mask_man((unsigned long) COMMAND_TBL_MASK,
									block_bin->bin_exists,
									block_bin->bin_hooked,
									&block_dir->attr_avail,
									env_info,
									eval_dir_command_tbl_8,
									&block_dir->command_to_var_tbl,
									&block_bin->command_tbl);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	if (mask & CRIT_PARAM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CRIT_PARAM_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_crit_param_tbl,
								&block_dir->crit_param_tbl,
								&block_bin->crit_param_tbl);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & RESLV_REF_TBL_MASK) {

		rc = dir_mask_man((unsigned long) RESLV_REF_TBL_MASK,
								block_bin->bin_exists,
								block_bin->bin_hooked,
								&block_dir->attr_avail,
								env_info,
								eval_dir_rslv_ref_tbl,
								&block_dir->reslv_ref_tbl,
								&block_bin->reslv_ref_tbl);

		if (rc != DDL_SUCCESS) {
			if(rc == DDL_BINARY_REQUIRED){
				rc = DDL_SUCCESS;		// This is an optional table, so it might not be there
			}
			else
			{
				return rc;
			}
		}
	}

#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	return rc;
}



/*********************************************************************
 *
 *	Name: free_command_table()
 *	ShortDesc: Frees the command tables
 *
 *	Description:
 *		free_command_table() frees the memory allocated
 *      to the command table which is one of the block
 *		directory tables
 *
 *	Inputs:
 *		command_tbl:	pointer to a COMMAND_TBL		
 *
 *	Outputs:
 *		command_tbl:	pointer to the freed COMMAND_TBL
 *
 *	Returns:
 *		None
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


static
void free_command_table(
COMMAND_TBL *command_tbl)
{

	int inc;
	COMMAND_TBL_ELEM *temp_element;

	/*
	 * Before freeing the command table,
	 * free the index element lists.
	 */

	temp_element = command_tbl->list;

	for (inc = 0; inc < command_tbl->count; inc++, temp_element++) {

		if(temp_element->index_list) {

			free((void *) temp_element->index_list);
		}
	}


	free((void *) command_tbl->list);

	return;
}



/*********************************************************************
 *
 *	Name: free_command_table_8()
 *	ShortDesc: Frees the command table 8
 *
 *	Description:
 *		free_command_table_8() frees the memory allocated
 *      to the command table 8 which is one of the block
 *		directory tables
 *
 *	Inputs:
 *		command_tbl_8:	pointer to a COMMAND_TBL_8		
 *
 *	Outputs:
 *		command_tbl_8:	pointer to the freed COMMAND_TBL_8
 *
 *	Returns:
 *		None
 *
 *	Author: Ying Xu
 *
 **********************************************************************/


static
void free_command_table_8(
COMMAND_TBL_8 *command_tbl_8)
{
	PTOC_TBL_8_ELEM *item_tbl_element = command_tbl_8->list;
	for (int i=0; i<command_tbl_8->count; i++, item_tbl_element++) 
	{
		COMMAND_TBL_ELEM *ptoc_tbl_element = item_tbl_element->rd_list;
		for (int j=0; j<item_tbl_element->rd_count; j++, ptoc_tbl_element++)
		{
			if (ptoc_tbl_element->index_list)
			{
				free(ptoc_tbl_element->index_list);
			}
		}

		if (item_tbl_element->rd_list)
		{
			free(item_tbl_element->rd_list);
		}

		ptoc_tbl_element = item_tbl_element->wr_list;
		for (int j=0; j<item_tbl_element->wr_count; j++, ptoc_tbl_element++)
		{
			if (ptoc_tbl_element->index_list)
			{
				free(ptoc_tbl_element->index_list);
			}
		}

		if (item_tbl_element->wr_list)
		{
			free(item_tbl_element->wr_list);
		}
	}

	if (command_tbl_8->list)
	{
		free(command_tbl_8->list);
	}

	return;
}

/*********************************************************************
 *
 *	Name: free_dir_rslv_ref_tbl()
 *	ShortDesc: Frees the Resolve Ref to Command Table
 *
 *	Description:
 *		free_dir_rslv_ref_tbl() frees the memory allocated
 *      to the reslv_ref_tbl which is one of the block
 *		directory tables
 *
 *	Inputs:
 *		reslv_ref_tbl:	pointer to a RESLV_REF_TO_CMD_TBL		
 *
 *	Outputs:
 *		reslv_ref_tbl:	pointer to the freed RESLV_REF_TO_CMD_TBL
 *
 *	Returns:
 *		None
 *
 *	Author: Mike Dieter
 *
 **********************************************************************/


static
void free_dir_rslv_ref_tbl( RESLV_REF_TO_CMD_TBL *reslv_ref_tbl )
{
	RESLV_TBL_ELEM *pReslvTblElem = reslv_ref_tbl->list;

	for (int i=0; i < reslv_ref_tbl->count; i++, pReslvTblElem++) 
	{
		free (pReslvTblElem->element_ref.pResolvedRef);
		pReslvTblElem->element_ref.pResolvedRef = nullptr;

		// Free the reslv_read_table_list
		COMMAND_TBL_ELEM *pCommandTblElem = pReslvTblElem->reslv_read_table_list;

		for (int j=0; j<pReslvTblElem->read_count; j++, pCommandTblElem++)
		{
			if (pCommandTblElem->index_list)
			{
				free(pCommandTblElem->index_list);
				pCommandTblElem->index_list = nullptr;
			}
		}

		if (pReslvTblElem->reslv_read_table_list)
		{
			free(pReslvTblElem->reslv_read_table_list);
			pReslvTblElem->reslv_read_table_list = nullptr;
		}

		// Free the reslv_write_table_list
		pCommandTblElem = pReslvTblElem->reslv_write_table_list;

		for (int j=0; j<pReslvTblElem->write_count; j++, pCommandTblElem++)
		{
			if (pCommandTblElem->index_list)
			{
				free(pCommandTblElem->index_list);
				pCommandTblElem->index_list = nullptr;
			}
		}

		if (pReslvTblElem->reslv_write_table_list)
		{
			free(pReslvTblElem->reslv_write_table_list);
			pReslvTblElem->reslv_write_table_list = nullptr;
		}
	}

	if (reslv_ref_tbl->list)
	{
		free(reslv_ref_tbl->list);
		reslv_ref_tbl->list = nullptr;
	}

	return;
}


/*********************************************************************
 *
 *	Name: eval_clean_block_dir
 *	ShortDesc: Frees the block tables
 *
 *	Description:
 *		eval_clean_block_dir will check the attr_avail flags
 *      in the FLAT_BLOCK_DIR structure to see which block tables
 *		exist and free them. Everything in the structure is then 
 *		set to zero.
 *
 *	Inputs:
 *		block_dir: 	pointer to a FLAT_BLOCK_DIR to be cleaned.
 *
 *	Outputs:
 *		block_dir: 	pointer to the empty FLAT_BLOCK_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_block_dir(
FLAT_BLOCK_DIR     *block_dir)
{
	ulong           temp_attr_avail;


	if (block_dir == NULL) {
		return;
	}

	
#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
	temp_attr_avail = block_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_ITEM_TBL_MASK) && 
		(block_dir->blk_item_tbl.list)) {
		free((void *) block_dir->blk_item_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_TBL_MASK) && 
		(block_dir->char_mem_tbl.list)) {
		free((void *) block_dir->char_mem_tbl.list);
	}

	if ((temp_attr_avail & CHAR_MEM_NAME_TBL_MASK) && 
		(block_dir->char_mem_name_tbl.list)) {
		free((void *) block_dir->char_mem_name_tbl.list);
	}

	if ((temp_attr_avail & BLK_ITEM_NAME_TBL_MASK) && 
		(block_dir->blk_item_name_tbl.list)) {
		free((void *) block_dir->blk_item_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_TBL_MASK) && 
		(block_dir->param_list_mem_tbl.list)) {
		free((void *) block_dir->param_list_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_MEM_NAME_TBL_MASK) && 
		(block_dir->param_list_mem_name_tbl.list)) {
		free((void *) block_dir->param_list_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_TBL_MASK) && 
		(block_dir->param_mem_tbl.list)) {
		free((void *) block_dir->param_mem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_MEM_NAME_TBL_MASK) && 
		(block_dir->param_mem_name_tbl.list)) {
		free((void *) block_dir->param_mem_name_tbl.list);
	}

	if ((temp_attr_avail & PARAM_ELEM_TBL_MASK) && 
		(block_dir->param_elem_tbl.list)) {
		free((void *) block_dir->param_elem_tbl.list);
	}

	if ((temp_attr_avail & PARAM_LIST_TBL_MASK) && 
		(block_dir->param_list_tbl.list)) {
		free((void *) block_dir->param_list_tbl.list);
	}

	if ((temp_attr_avail & PARAM_TBL_MASK) && 
		(block_dir->param_tbl.list)) {
		free((void *) block_dir->param_tbl.list);
	}

	if ((temp_attr_avail & REL_TBL_MASK) && 
		(block_dir->rel_tbl.list))
	{
		delete block_dir->rel_tbl.UnitLookup;
		free((void *) block_dir->rel_tbl.list);
	}

	if ((temp_attr_avail & UPDATE_TBL_MASK) && 
		(block_dir->update_tbl.list)) {
		free((void *) block_dir->update_tbl.list);
	}

	if( (temp_attr_avail & CRIT_PARAM_TBL_MASK) && (block_dir->crit_param_tbl.list) )
	{
		free((void *) block_dir->crit_param_tbl.list);
	}

	if ((temp_attr_avail & COMMAND_TBL_MASK) && 
		(block_dir->command_tbl.list)) {

		free_command_table(&block_dir->command_tbl);
	}

	if ((temp_attr_avail & COMMAND_TBL_MASK) && 
		(block_dir->command_to_var_tbl.list)) {

		free_command_table_8(&block_dir->command_to_var_tbl);
	}

		
	if( (temp_attr_avail & RESLV_REF_TBL_MASK) && (block_dir->reslv_ref_tbl.list) )
	{
		free_dir_rslv_ref_tbl( &block_dir->reslv_ref_tbl );
	}

	if(block_dir->dominant_tbl)
	{
			
		DOMINANT_TBL::iterator it; 

		for (it = block_dir->dominant_tbl->begin(); it != block_dir->dominant_tbl->end(); it++) 
		{
			OP_REF_LIST *opList = NULL;
			opList = it->second;
				
			free((void*)(opList->list)); 
			opList->list = NULL;
				
			free((void*)(opList)); 	
		}

		block_dir->dominant_tbl->clear();
		delete block_dir->dominant_tbl;
			
	}
	
	memset((char *) block_dir, 0, sizeof(FLAT_BLOCK_DIR));
	
#ifdef DEBUG
	dds_blk_dir_flat_chk((BIN_BLOCK_DIR *) 0, block_dir);
#endif
	
}

/*********************************************************************
 *
 *	Name: eval_dir_device_tables
 *	ShortDesc: evaluate (ie. create) the device tables
 *
 *	Description:
 *		eval_dir_device_tables will load the desired device tables into the 
 *		FLAT_DEVICE_DIR structure. The user must specify which device tables
 *		are desired by setting the appropriate bits in the "mask" parameter.
 *
 *	Inputs:
 *		mask:		bit mask specifying the desired device tables.
 *
 *	Outputs:
 *		device_dir:	pointer to a FLAT_DEVICE_DIR structure. Will contain
 *					the desired device tables.
 *      device_bin: pointer to a BIN_DEVICE_DIR structure. Stores the 
 *					binary device information created by the tokenizer.
 *
 *	Returns:
 *		DDS_SUCCESS,
 *		return codes from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_dir_device_tables(
FLAT_DEVICE_DIR		*device_dir,
BIN_DEVICE_DIR		*device_bin,
ENV_INFO			*env_info,
unsigned long   	mask,
ROD_HANDLE			rod_handle)
{

	int  rc = DDS_SUCCESS;	/* return code */
#ifdef ISPTEST
	TEST_FAIL(I_EVAL_DEVICE_DIR);
#endif


#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	if (mask & BLK_TBL_MASK) {

		int  tok_major_rev = g_DeviceTypeMgr.rod_get_tok_major_rev(rod_handle);
		if (tok_major_rev != 8)
		{

			rc = dir_mask_man((unsigned long) BLK_TBL_MASK,
									device_bin->bin_exists,
									device_bin->bin_hooked,
									&device_dir->attr_avail,
									env_info,
									eval_dir_blk_tbl,
									&device_dir->blk_tbl,
									&device_bin->blk_tbl,
									rod_handle);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
		else
		{
			rc = make_dir_blk_tbl(&device_dir->blk_tbl, &device_dir->attr_avail);
			
			if (rc != DDL_SUCCESS) {

				return rc;
			}
			// If the tok rev is 8, find out the index in the item table for _MNG_HART_BLOCK 
			if (device_dir->blk_tbl.list[0].item_tbl_offset == -1)
			{
				rc = dir_mask_man((unsigned long) ITEM_TBL_MASK,
							device_bin->bin_exists,
							device_bin->bin_hooked,
							&device_dir->attr_avail,
							env_info,
							eval_dir_item_tbl,
							&device_dir->item_tbl,
							&device_bin->item_tbl,
							rod_handle);

				if (rc != DDL_SUCCESS) {

					return rc;
				}
				else
				{
					// bsearch does not return the index for the located item, so we cannot use bsearch.
					// _MNG_HART_BLOCK is almost at the bottom of the item table, so the linear search
					// should have good performance
					for (int i= device_dir->item_tbl.count - 1; i >=0; i--)
					{
						if (device_dir->item_tbl.list[i].item_id == 0x8000ff00)
						{
							device_dir->blk_tbl.list[0].item_tbl_offset = i;
							break;
						}
					}
				}
			}
		}
	}
	if (mask & DICT_REF_TBL_MASK) {

		rc = dir_mask_man((unsigned long) DICT_REF_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_dict_ref_tbl,
								&device_dir->dict_ref_tbl,
								&device_bin->dict_ref_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & DOMAIN_TBL_MASK) {

		rc = dir_mask_man((unsigned long) DOMAIN_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_domain_tbl,
								&device_dir->domain_tbl,
								&device_bin->domain_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & ITEM_TBL_MASK) {

		rc = dir_mask_man((unsigned long) ITEM_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_item_tbl,
								&device_dir->item_tbl,
								&device_bin->item_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & PROG_TBL_MASK) {

		rc = dir_mask_man((unsigned long) PROG_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_prog_tbl,
								&device_dir->prog_tbl,
								&device_bin->prog_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}
	if (mask & STRING_TBL_MASK) {

		rc = dir_mask_man((unsigned long) STRING_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_string_tbl,
								&device_dir->string_tbl,
								&device_bin->string_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & LOCAL_VAR_TBL_MASK) {

		rc = dir_mask_man((unsigned long) LOCAL_VAR_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_local_var_tbl,
								&device_dir->local_var_tbl,
								&device_bin->local_var_tbl,
								rod_handle);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & CMD_NUM_ID_TBL_MASK) {

		rc = dir_mask_man((unsigned long) CMD_NUM_ID_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_cmd_num_id_tbl,
								&device_dir->cmd_num_id_tbl,
								&device_bin->cmd_num_id_tbl,
								rod_handle);
		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

	if (mask & IMAGE_TBL_MASK) {

		rc = dir_mask_man((unsigned long) IMAGE_TBL_MASK,
								device_bin->bin_exists,
								device_bin->bin_hooked,
								&device_dir->attr_avail,
								env_info,
								eval_dir_image_tbl,
								&device_dir->image_tbl,
								&device_bin->image_tbl,
								rod_handle);

		if (rc != DDL_SUCCESS) {

			return rc;
		}
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	return rc;
}



/*********************************************************************
 *
 *	Name: eval_clean_device_dir
 *	ShortDesc: Free the device tablesIR structure
 *
 *	Description:
 *		eval_clean_device_dir will check the attr_avail flags
 *      in the FLAT_DEVICE_DIR structure to see which device tables
 *		exist and free them. Everything in the structure is then
 *		set to zero.
 *
 *	Inputs:
 *		device_dir: pointer to the FLAT_DEVICE_DIR to be cleaned.
 *
 *	Outputs:
 *		device_dir: pointer to the empty FLAT_DEVICE_DIR structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/
void
eval_clean_device_dir(
FLAT_DEVICE_DIR     *device_dir)
{
	ulong           temp_attr_avail;


	if (device_dir == NULL) {
		return;
	}

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
	temp_attr_avail = device_dir->attr_avail;

	/*
	 * Free attribute structures
	 */

	if ((temp_attr_avail & BLK_TBL_MASK) && 
		(device_dir->blk_tbl.list)) {
		free((void *) device_dir->blk_tbl.list);
	}

	if ((temp_attr_avail & DICT_REF_TBL_MASK) && 
		(device_dir->dict_ref_tbl.list)) {
		free((void *) device_dir->dict_ref_tbl.list);
	}

	if ((temp_attr_avail & DOMAIN_TBL_MASK) && 
		(device_dir->domain_tbl.list)) {
		free((void *) device_dir->domain_tbl.list);
	}

	if ((temp_attr_avail & ITEM_TBL_MASK) && 
		(device_dir->item_tbl.list)) {
		free((void *) device_dir->item_tbl.list);
	}

	if ((temp_attr_avail & PROG_TBL_MASK) && 
		(device_dir->prog_tbl.list)) {
		free((void *) device_dir->prog_tbl.list);
	}

	if ((temp_attr_avail & STRING_TBL_MASK) && 
		(device_dir->string_tbl.list)) {

		free((void *) device_dir->string_tbl.root);
		free((void *) device_dir->string_tbl.list);
	}

	if ((temp_attr_avail & IMAGE_TBL_MASK) && 
		(device_dir->image_tbl.list)) {
		
		for(int i=0; i<device_dir->image_tbl.count; i++)
		{
			for (int j=0; j<device_dir->image_tbl.list[i].count; j++)
			{
				if (device_dir->image_tbl.list[i].list[j].language_code.str)
				{
					ddl_free_string(&device_dir->image_tbl.list[i].list[j].language_code);
				}

				/* note that device_dir->image_tbl.list[i].list[j].data should
				   not be freed here, because it is freed by rod_close */
		
			}
			free((void *)device_dir->image_tbl.list[i].list);
		}

		free((void *) device_dir->image_tbl.list);
		device_dir->image_tbl.list = NULL;
	}

	if ((temp_attr_avail & LOCAL_VAR_TBL_MASK) && 
		(device_dir->local_var_tbl.list)) {

		free((void *) device_dir->local_var_tbl.list);
	}

	if ((temp_attr_avail & CMD_NUM_ID_TBL_MASK) && 
		(device_dir->cmd_num_id_tbl.list)) {

		free((void *) device_dir->cmd_num_id_tbl.list);
	}

	memset((char *) device_dir, 0, sizeof(FLAT_DEVICE_DIR));

#ifdef DEBUG
	dds_dev_dir_flat_chk((BIN_DEVICE_DIR *) 0, device_dir);
#endif
	
}






