/**
 *	@(#) $Id: fch_rodf.c,v 1.1 1995/05/02 20:44:49 donmarq Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This module provides an interface between the Fetch routines
 *	and the ROD access functions
 *
 */

/* #includes */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif

/* #defines */

/* typedefs */

/* structure definitions */

/* global variables */

/* local procedure declarations */


/*********************************************************************
 *
 *	Name: 	get_rod_object
 *
 *	ShortDesc: Get a ROD object
 *
 *	Description:
 *		This routine retrieves a ROD object using the Connection
 *		Manager functions and stores it in the scratchpad.
 *
 *	Inputs:
 *      handle - handle of the ROD that contains the object.
 *      index - index of the object.
 *
 *	Outputs:
 *		obj - 	pointer to a structure containing information about
 *				object, including pointers to the object extension
 *				and to the local data area
 *      sp - 	pointer to a structure containing a pointer to a
 *           	scratchpad memory area where the object will be copied
 *           	to, the total size of the scratchpad memory area, and
 *           	the amount of scratchpad memory used so far (increased
 *           	by the object size).
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_OBJECT_NOT_FOUND
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int 
get_rod_object(
ENV_INFO		* /*env_info */,
ROD_HANDLE      handle,
OBJECT_INDEX    index,
OBJECT        **obj,
SCRATCH_PAD    *sp)
{
	unsigned char  *local_pad;
	OBJECT         *rod_obj;
	int             rcode, length;

	ASSERT_RET(obj && sp && sp->pad,
		FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(GET_ROD_OBJECT);
#endif

	/*
	 * Point to the first available location in the scratchpad
	 */

	local_pad = sp->pad + sp->used;

	/*
	 * Retrieve the object information from the ROD. Return if an error
	 * occurred or the returned object extension is NULL.
	 */

	rod_obj = (OBJECT *) 0L;/* initialize to NULL */

	rcode = g_DeviceTypeMgr.rod_get(handle, index, &rod_obj);

	if (rcode != SUCCESS) {
		return (FETCH_OBJECT_NOT_FOUND);
	}
	if (!rod_obj->extension) {
		return (FETCH_OBJECT_NOT_FOUND);
	}

	/*
	 * Copy the object extension into the scratchpad.  Byte 0 gives the
	 * extension length less 1 octet.
	 */

	length = 1 + (int) *(rod_obj->extension);

	/*
	 * Return if not enough scratchpad to load the object extension.
	 */

	if ((sp->size - sp->used) < (unsigned long) length) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	memcpy((char *) local_pad, (char *) (rod_obj->extension), length);

	/*
	 * Offset the "used" field in the scratchpad structure
	 */

	*obj = rod_obj;

	sp->used += (unsigned long) length;

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: 	get_local_data
 *
 *	ShortDesc: Get ROD local data
 *
 *	Description:
 *		This routine retrieves a segment of the ROD Local Data area
 *		using the Connection Manager functions and stores it in the
 *		scratchpad.
 *
 *  Inputs:
 *      obj -    pointer to the ROD object that references the data
 *      offset - offset into the object's data area.
 *      size -   size of the requested data.
 *
 *  Outputs:
 *      sp - pointer to a structure containing a pointer to a
 *           scratchpad memory area where the data will be copied
 *           to, the total size of the scratchpad memory area, and
 *           the amount of scratchpad memory used so far (increased
 *           by the data size).
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_DATA_NOT_FOUND
 *		FETCH_DATA_OUT_OF_RANGE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
get_local_data(
OBJECT         *obj,
unsigned long  offset,
unsigned long  size,
SCRATCH_PAD    *sp)
{
	unsigned char  *local_pad, *obj_data;

	ASSERT_RET(obj && sp && sp->pad,
		FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(GET_LOCAL_DATA);
#endif

	/*
	 * Return if not enough scratchpad to load the object data
	 */

	if ((sp->size - sp->used) < size) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	/*
	 * Point to the first available location in the scratchpad
	 */

	local_pad = sp->pad + sp->used;

	/*
	 * Retrieve the data for the object from the object structure. Return
	 * if an error occurred or the object data area is not found
	 */

	if (!(obj->value)) {
		return (FETCH_DATA_NOT_FOUND);
	}

	/*
	 * Verify that the requested data is within the range of the returned
	 * ROD data
	 */

	if (obj->specific->domain.size < (offset + size)) {
		return (FETCH_DATA_OUT_OF_RANGE);
	}

	/*
	 * Copy the local data fragment into the scratchpad
	 */

	obj_data = ((unsigned char *) obj->value) + offset;
	memcpy((char *) local_pad, (char *) obj_data, (int) size);

	/*
	 * Offset the "used" field in the scratchpad structure
	 */

	sp->used += size;

	return (SUCCESS);
}
