/*************************************************************************************************
 *
 * $Workfile: ddbCommand.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the h command class
 *		08/07/02	sjv	created from itemCommand.cpp
 * Component History: 
 * 16 Nov 2006 - Carolyn Holmes (HOMZ) - Port code from VC6 to VS 2003
 */

#include "stdafx.h"
#include "ddbCommand.h"


/*************************************************************************************************
 * The command class 
 * 
 *************************************************************************************************
 */
hCcommand::hCcommand(DevInfcHandle_t h, aCitemBase* paItemBase) : hCitemBase(h, paItemBase)
{
	return;
};

/* from-typedef constructor */
hCcommand::hCcommand( hCcommand* pSrc, itemIdentity_t newID ) : hCitemBase(pSrc,newID)
{
}


hCcommand::~hCcommand(){}


RETURNCODE hCcommand::destroy()
{
	return SUCCESS;
}


hCattrBase*   hCcommand::newHCattr(aCattrBase* pACattr)  // builder 
{
	return NULL; // an error
}

int hCcommand::getCmdNumber(void)// -1 on error
{ 
	return -1;
}


int hCcommand::getRespCodes(hCRespCodeList& rcList) // resolve and fill return list
{
	return SUCCESS;
}


// 10/20/03 - modified to append uniquely with new list supplanting old on duplicates
void hCRespCodeList::append(hCRespCodeList* addlist)
{
};

void  hCRespCodeList::setEqual(void* pAclass)
{
}

void  hCRespCodeList:: duplicate(hCpayload* pPayLd, bool replicate)
{
}

hCcommandDescriptor::hCcommandDescriptor()
{
	clear();
}

void hCcommandDescriptor::clear(void)
{
	cmdNumber=-1;
	transNumb=-1;
}

hCcommandDescriptor::hCcommandDescriptor(const hCcommandDescriptor& cd) 
{
	*this = cd;
}
hCcommandDescriptor& hCcommandDescriptor::operator=(const hCcommandDescriptor& cd)
{
	cmdNumber=cd.cmdNumber;
	transNumb=cd.transNumb;
	return *this;
};
