/*************************************************************************************************
 *
 * $Workfile: ddbReferenceNexpression.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		support objects for the conditional base class
 *		2/6/3	sjv	created - from ddbConditional.h
 *
 * #include "DDBreferenceNexpression.h"
 */


#ifndef _DDBREFNEXPR_H
#define _DDBREFNEXPR_H
#ifdef INC_DEBUG
#pragma message("In DDBreferenceNexpression.h") 
#endif

#include "ddbGeneral.h"
#include "foundation.h"
#include "ddbdefs.h"
#include "varient.h"
#include "DDBpayload.h"
#include "dllapi.h"
#include "ddbDeviceService.h"
#include "ddbGlblSrvInfc.h"
#include "ddbTracking.h"


#ifdef INC_DEBUG
#pragma message("    Finished includes from DDBreferenceNexpression.h") 
#endif



class hCexpression;// forward references
class hCitemBase;  // should be in the include file but some sequences don't work

class hCVar;       
class hCindex;

//typedef vector<hCitemBase*> ddbItemList_t;
/* these are duplicates from dependency and itembase */
class   hCitemBase;
typedef hCitemBase*           ptr2hCitemBase_t;
typedef vector<ptr2hCitemBase_t> ddbItemList_t;
typedef ddbItemList_t::iterator  ddbItemLst_it;
#define _ITEM_BASE_DEFINED
/*--------------------- reference ------------------------*/


class hCreference : public hCpayload, public hCobject
{
	CrefType        rtype; // in foundation .h -> 0 - 24...0 == Item_id,  3 = via item array, 
						   //					  4 = via_Collect, 24 = response code id

	int             isRef; // used as bool:isRef else it's an ID...inited to ID (0)
	ITEM_ID         id;	   // ! isRef && refType == 0,1,2,11 to 24 

	// item type may or may not be valid for the given ITEM_ID... we need to check after a load
	CitemType		type;  // in foundation .h -> 0 - 16...1 == variable, 9 = item_array, 
							//														19 = member
					SUB_INDEX       subindex;	// unknown usage????? may or may not be valid

	// ----- used for via_xxx types (via_item_array, via_collection,etc. (types 3-10))
	int              iName;			// member-name,parameter-name,parameter-list-name,
									//										characteristic-name
	expressionType_t exprType;      // type <direct,if,select,case::> unknown==empty expression>
	// these are pointers since they are used (instantiated) only in particular circumstances
	hCexpression*    pExpr;			// array's (item-array & array) index only
	hCreference*     pRef;			// ItemArray_ref,   (array-ref, collection-ref, record-ref)
	hCvarGroupTrail  grpTrail;

	void onnew(void); // private helper function (common code)
	void addUniqueUInt(UIntList_t& intlist, UINT32 newInt);// private helper function 

public: 
	hCreference(DevInfcHandle_t h);
	virtual ~hCreference();
	RETURNCODE destroy(void);
	// copy constructor
	hCreference(const hCreference& cr);// : hCobject(cr.devHndl()) { clear(); operator=(cr);  };

	hCreference& operator= (const hCreference& cr);
	bool         operator==(const hCreference& ri);

	hCreference& operator=(const aCreference& cr);

	RETURNCODE getRef   (int* pOption=NULL,int* pWhich=NULL,hCreference* pParent=NULL) ;
	RETURNCODE resolveID( itemID_t& returnedID, bool withStrings=true);
	RETURNCODE resolveID( hCitemBase*& r2pReturnedPtr, bool withStrings=true);
	RETURNCODE resolveExpr(CValueVarient& retValue);

	RETURNCODE resolveAllIDs(UIntList_t&      allPossibleIDsReturned, bool stopAtID = true);
#define        resolveAllVars(a)  resolveAllIDs(a,false)
	RETURNCODE resolveAllIDs(vector<hCitemBase*>& allPossiblePtrsReturned);
	void setRef(ITEM_ID i, SUB_INDEX s, ITEM_TYPE t, int ir = 1,referenceType_t rt=rT_Item_id)   
				{id=i;subindex=s;type=(long)t;isRef = ir;rtype=rt;};
	ITEM_ID    getID    (void)                          {return id;};
	SUB_INDEX  getSUBIDX(void)                          {return subindex;};
	ITEM_TYPE  getITYPE (void)                          {return type.getType();};
	ITEM_TYPE  getRTYPE (void)                          {return rtype.getType();};
	referenceType_t getRefType(void)                    {return (referenceType_t)rtype.getType();};
	bool       isaRef(void)                             {bool r = 0; if(isRef){ r = 1;} return r;};
	CitemType& getCtype(void)                           {return type;};
	CrefType&  getRtype(void)                           {return rtype;};
	hCvarGroupTrail* getGroupTrailPtr(void)				{return &grpTrail;};

	void    makeAref(void)                              { isRef = 1; };
	void    makeNotAref(void)                           { isRef = 0; rtype = 0/*item_id*/;};

	// payload required
	void  setEqual(void* pAclass);                  // a required method
	void duplicate(hCpayload* pPayLd, bool replicate /*= false*/);
	bool validPayload(payloadType_t ldType){ bool r = false; if(ldType==pltReference) r = true; return r;	};
	void clear(void) ;// defined after expression
	bool isEmpty(void);
	RETURNCODE getItemLabel(wstring & strLabel);
};

typedef vector<hCreference>        HreferenceList_t;
typedef vector<hCreference*>       HreferencePtrList_t;


/*--------------------- expression ------------------------*/

class hCdepends : public hCobject
{
public:
	arithOp_t      useOptionVal;	// Max/Min/don't Care
	DDL_UINT       which;           // offset+1 int MIN | MAX range->list[]
	hCreference    depRef;			// a reference - resolves to a variable item id
	
	hCdepends(DevInfcHandle_t h) 
		: hCobject(h), depRef(h) {useOptionVal = aOp_dontCare;which = 0;}; /* VMKP added on 090304 */
	hCdepends(hCdepends& src) 
		: hCobject(src.devHndl()), depRef(src.devHndl()) { *this = src;};
	virtual
		~hCdepends()
	{ depRef.destroy(); };

	RETURNCODE destroy(void) 
	{	depRef.destroy(); return SUCCESS;
	};

	void clear(void) { useOptionVal=aOp_dontCare;which=0;depRef.clear();};
	hCdepends& operator=(hCdepends& srcDep){
		useOptionVal = srcDep.useOptionVal;
		which = srcDep.which;
		depRef = srcDep.depRef; return *this;
	};
	hCdepends& operator=(aCdepends& srcDep){
		useOptionVal = srcDep.useOptionVal;
		which = srcDep.which;
		depRef = srcDep.depRef; return *this;
	};
	bool operator==(const hCdepends& di){ if ( ! (depRef == di.depRef) ) return false;
		if (which != di.which) return false;  /* else*/ return true; };
	RETURNCODE resolveIndex(hCreference* pArrayRef, hCitemBase/*hCindex*/*& r2pIndex);
	void setDuplicate(hCdepends& rSrc);
};

class hCexpressElement : public hCobject
{
public: 
	expressElemType_t   exprElemType;    // types 1 thru 21 are opcodes; 22-29 values
	CValueVarient expressionData{};  // holds opcode(1-21),const(22,23)(we don't use index type
	hCdepends			dependency;      // holds !isRef @ 24, holds dependency info (25-29)	

	hCexpressElement(DevInfcHandle_t h) 
		: hCobject(h), dependency(h) {exprElemType = eeT_Unknown;} /* VMKP added on 090304*/
	hCexpressElement(const hCexpressElement& src) 
		: hCobject(src.devHndl()), dependency(src.devHndl())	{ *this = src; };
	virtual ~hCexpressElement()
	{
		dependency.destroy();
		expressionData.clear();
	};
	RETURNCODE destroy(void)
	{
		expressionData.clear();
		return dependency.destroy();
	};
	void clear(void) {exprElemType = eeT_Unknown;expressionData.clear();dependency.clear();};

	bool              operator==(const hCexpressElement& ri);// stevev 091006
	hCexpressElement& operator=(const hCexpressElement& srcElem);
	hCexpressElement& operator=(aCexpressElement& srcElem)
	{   exprElemType  = srcElem.exprElemType; 
	    expressionData= srcElem.expressionData;
		dependency    = srcElem.dependency; return *this;
	};
	void setDuplicate(hCexpressElement& rSrc);
};

typedef vector<hCexpressElement>	hRPNexpress_t;
// use varientList_t typedef vector<CValueVarient>       varientList_t;


#define MAX_EXPR_RE_ENTRY	8

class hCexpression : public hCobject
{	// the expression type always goes with the containing class.
//	varientList_t  executionStack;

	RETURNCODE          lastError;
	int                  entryCnt;

	// private (helper) methods
	RETURNCODE pull_back(CValueVarient& returnVarient, varientList_t& actvStk);
	RETURNCODE    toBoolT(CValueVarient& returnVarient, CValueVarient& op1, CValueVarient& op2);
public: // made these public so others could use 'em (like the binary isEqual)
	CValueVarient execUnary(expressElemType_t Opcode,         CValueVarient& op1);

public:
	hCexpression(DevInfcHandle_t h) : hCobject(h),lastError(0),entryCnt(0) {expressionL.clear();};  
	hCexpression(const hCexpression& src);
	virtual ~hCexpression();
	RETURNCODE destroy(void);

	hRPNexpress_t	 expressionL;	// list of CexpressElement in RPN order   

	//RETURNCODE       resolveAllIndexes(vector<UINT32>& valueList);// for array expressions
	RETURNCODE       resolveAllIndexes(ExprTrailList_t& exprValueList);

	RETURNCODE       resolveIndex(hCreference* pArrayRef, hCindex*& r2pIndex);
	RETURNCODE getExprInfo(ExprTrailList_t& exprTrail);// get all expression resolutions
	void       setDuplicate(hCexpression& rSrc);// make this a duplicate of passed-in
	
	bool          operator==(hCexpression& ri);	// removed const from arg
	hCexpression& operator=(const hCexpression& srcAExpr);
	hCexpression& operator=(aCexpression& srcAExpr) {
		hCexpressElement wrkElem(devHndl());
		FOR_iT(RPNexpress_t, srcAExpr.expressionL)
		{//it is a ptr 2 aCexpressElement
			wrkElem = (*iT);
			expressionL.push_back(wrkElem); wrkElem.clear();// stevev 5jul06 - get rid of extra stuff
		}		return *this;						};
	void clear(void){expressionL.clear();/*executionStack.clear();*/lastError = SUCCESS;entryCnt=0;};
};

class hCexprPayload : public hCpayload, public hCexpression// has a hCobject
{
public:
	hCexprPayload(DevInfcHandle_t h) : hCexpression(h) {};
	hCexprPayload(hCexprPayload& src) : hCexpression(src.devHndl()) {operator=(src);};
	virtual ~hCexprPayload() {hCexpression::destroy();};
	RETURNCODE destroy(void){ return hCexpression::destroy();};

	hCexprPayload& operator=(const hCexprPayload& srcExpr);

	// payload required
	void  setEqual(void* pAclass) ; // aCexpression
	void duplicate(hCpayload* pPayLd, bool replicate);
	bool validPayload(payloadType_t ldType)
	{bool r = false; if (ldType==pltExpression) r = true; return r; };
	void clear(void) {hCexpression::clear();};

	RETURNCODE setValue(CValueVarient newValue); // constant
};




#endif // _DDBREFNEXPR_H

/*************************************************************************************************
 *
 *   $History: ddbReferenceNexpression.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
