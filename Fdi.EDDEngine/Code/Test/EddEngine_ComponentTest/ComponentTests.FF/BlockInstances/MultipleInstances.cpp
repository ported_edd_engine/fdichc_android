#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_and_reading_parameters_from_two_blocks_with_FF
        {
        public: 
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_block_2_item_ID;
            static ITEM_ID* test_var_item_ID;
            static ITEM_ID* test_var_2_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static FDI_GENERIC_ITEM* genericItem_2;
            static FLAT_VAR* test_var_4_from_block;
            static FLAT_VAR* test_var_2_from_block_2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0001\\TestCase0001.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);
                test_block_2_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block_2", test_block_2_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_var_4", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_var_4_from_block = dynamic_cast<FLAT_VAR*>(genericItem->item);

                test_var_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_4", test_var_item_ID);

                // create second Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_2 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_2->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_2->m_CharRecord.m_ulDDItemId = *test_block_2_item_ID;
                pFieldbusBlockInfo_2->m_usObjectIndex = 2000;
                int iBlockInstance_2; // out
                const int iCount_2 = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_2, &iBlockInstance_2, iCount_2);

                FDI_PARAM_SPECIFIER test_var_specifier_2;
                engine->FillParamSpecifier("test_var_2", &test_var_specifier_2);
                genericItem_2 = engine->GetGenericItem(
                    test_var_specifier_2.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_2);

                test_var_2_from_block_2 = dynamic_cast<FLAT_VAR*>(genericItem_2->item);

                test_var_2_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_2", test_var_2_item_ID);
                delete pFieldbusBlockInfo_2;
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete genericItem_2;
                delete test_block_item_ID;
                delete test_block_2_item_ID;
                delete test_var_item_ID;
                delete test_var_2_item_ID;
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_not_contain_the_help_as_mask_in_test_variable_two()
            {
                Assert::IsFalse(test_var_2_from_block_2->isAvailable(nsEDDEngine::help), "help");
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_contain_the_label_as_mask_in_test_variable_two()
            {
                Assert::IsTrue(test_var_2_from_block_2->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_not_contain_the_help_as_mask_in_test_variable_four()
            {
                Assert::IsFalse(test_var_4_from_block->isAvailable(nsEDDEngine::help), "help");
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_contain_the_label_as_mask_in_test_variable_four()
            {
                Assert::IsTrue(test_var_4_from_block->isAvailable(nsEDDEngine::label), "label");
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_first_block()
            {
                Assert::AreEqual(*test_var_item_ID, test_var_4_from_block->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_second_block()
            {
                Assert::AreEqual(*test_var_2_item_ID, test_var_2_from_block_2->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_block()
            {
                Assert::AreEqual(0x20006u, *test_block_item_ID);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_block_two()
            {
                Assert::AreEqual(0x20005u, *test_block_2_item_ID);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_two()
            {
                Assert::AreEqual("Test Variable 2", gcnew String(test_var_2_from_block_2->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_four()
            {
                Assert::AreEqual("Test Variable 4", gcnew String(test_var_4_from_block->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_two()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_2_from_block_2->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_four()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_4_from_block->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_class_type_for_the_test_variable_four()
            {
                Assert::AreEqual((int) CT_CONTAINED, (int)test_var_4_from_block->class_attr);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_class_type_for_the_test_variable_two()
            {
                Assert::AreEqual((int) CT_CONTAINED, (int)test_var_2_from_block_2->class_attr);
            }
        };

        [TestClass]
        public ref class When_creating_multiple_instances_mixed_of_two_blocks_with_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_block_2_item_ID;
            static ITEM_ID* test_var_4_item_ID;
            static ITEM_ID* test_var_2_item_ID;
            static FDI_GENERIC_ITEM* genericItem_1;
            static FDI_GENERIC_ITEM* genericItem_2;
            static FDI_GENERIC_ITEM* genericItem_3;
            static FDI_GENERIC_ITEM* genericItem_4;
            static FDI_GENERIC_ITEM* genericItem_5;
            static FLAT_VAR* test_var_4_from_block_1_instance_1;
            static FLAT_VAR* test_var_4_from_block_1_instance_2;
            static FLAT_VAR* test_var_4_from_block_1_instance_3;
            static FLAT_VAR* test_var_2_from_block_2_instance_1;
            static FLAT_VAR* test_var_2_from_block_2_instance_2;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0001\\TestCase0001.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);
                test_block_2_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block_2", test_block_2_item_ID);

                // create instances of first block
                const int iCount = 1;		// This is the size of the CFieldbusBlockInfo array that is passed to AddFFBlocks
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_1 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_1->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_1->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo_1->m_usObjectIndex = 1000;
                int iBlockInstance_block_1_instance_1; // out

                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_1, &iBlockInstance_block_1_instance_1, iCount);
                //iCount++;

                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_2 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_2->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_2->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo_2->m_usObjectIndex = 1100;
                int iBlockInstance_block_1_instance_2;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_2, &iBlockInstance_block_1_instance_2, iCount);
                //iCount++;

                // create instances of second block
                //const int iCount_2 = 1;	// Don't need a different array count for different blocks.
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_3 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_3->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_3->m_CharRecord.m_ulDDItemId = *test_block_2_item_ID;
                pFieldbusBlockInfo_3->m_usObjectIndex = 2000;
                int iBlockInstance_block_2_instance_1; // out

                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_3, &iBlockInstance_block_2_instance_1, iCount);
                //iCount_2++;

                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_4 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_4->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_4->m_CharRecord.m_ulDDItemId = *test_block_2_item_ID;
                pFieldbusBlockInfo_4->m_usObjectIndex = 2100;
                int iBlockInstance_block_2_instance_2; // out

                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_4, &iBlockInstance_block_2_instance_2, iCount);

                // create instances of first block                
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo_5 = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo_5->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo_5->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo_5->m_usObjectIndex = 1200;
                int iBlockInstance_block_1_instance_3;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo_5, &iBlockInstance_block_1_instance_3, iCount);
                
                // now we have 5 instances of two blocks. Get Parameters from every instance and check

                // get Parameters from blocks
                test_var_2_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_2", test_var_2_item_ID);
                test_var_4_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_4", test_var_4_item_ID);

                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_var_4", &test_var_specifier);
                FDI_PARAM_SPECIFIER test_var_specifier_2;
                engine->FillParamSpecifier("test_var_2", &test_var_specifier_2);

                // get params from first block, all instances
                genericItem_1 = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_block_1_instance_1);

                test_var_4_from_block_1_instance_1 = dynamic_cast<FLAT_VAR*>(genericItem_1->item);

                genericItem_2 = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_block_1_instance_2);

                test_var_4_from_block_1_instance_2 = dynamic_cast<FLAT_VAR*>(genericItem_2->item);

                genericItem_3 = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_block_1_instance_3);

                test_var_4_from_block_1_instance_3 = dynamic_cast<FLAT_VAR*>(genericItem_3->item);
                
                // get params from second block, all instances
                genericItem_4 = engine->GetGenericItem(
                    test_var_specifier_2.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_block_2_instance_1);

                test_var_2_from_block_2_instance_1 = dynamic_cast<FLAT_VAR*>(genericItem_4->item);

                genericItem_5 = engine->GetGenericItem(
                    test_var_specifier_2.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance_block_2_instance_2);

                test_var_2_from_block_2_instance_2 = dynamic_cast<FLAT_VAR*>(genericItem_5->item);

                delete pFieldbusBlockInfo_1;
                delete pFieldbusBlockInfo_2;
                delete pFieldbusBlockInfo_3;
                delete pFieldbusBlockInfo_4;
                delete pFieldbusBlockInfo_5;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem_1;
                delete genericItem_2;
                delete genericItem_3;
                delete genericItem_4;
                delete genericItem_5;

                delete test_block_item_ID;
                delete test_block_2_item_ID;
                delete test_var_4_item_ID;
                delete test_var_2_item_ID;
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_block()
            {
                Assert::AreEqual(0x20006u, *test_block_item_ID);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_block_two()
            {
                Assert::AreEqual(0x20005u, *test_block_2_item_ID);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_first_block_first_instance()
            {
                Assert::AreEqual(*test_var_4_item_ID, test_var_4_from_block_1_instance_1->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_first_block_second_instance()
            {
                Assert::AreEqual(*test_var_4_item_ID, test_var_4_from_block_1_instance_2->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_first_block_third_instance()
            {
                Assert::AreEqual(*test_var_4_item_ID, test_var_4_from_block_1_instance_3->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_second_block_first_instance()
            {
                Assert::AreEqual(*test_var_2_item_ID, test_var_2_from_block_2_instance_1->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_id_for_the_test_variable_from_second_block_second_instance()
            {
                Assert::AreEqual(*test_var_2_item_ID, test_var_2_from_block_2_instance_2->id);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_two_first_instance()
            {
                Assert::AreEqual("Test Variable 2", gcnew String(test_var_2_from_block_2_instance_1->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_two_second_instance()
            {
                Assert::AreEqual("Test Variable 2", gcnew String(test_var_2_from_block_2_instance_2->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_four_first_instance()
            {
                Assert::AreEqual("Test Variable 4", gcnew String(test_var_4_from_block_1_instance_1->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_four_second_instance()
            {
                Assert::AreEqual("Test Variable 4", gcnew String(test_var_4_from_block_1_instance_2->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_label_for_the_test_variable_four_third_instance()
            {
                Assert::AreEqual("Test Variable 4", gcnew String(test_var_4_from_block_1_instance_3->label.c_str()));
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_two_first_instance()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_2_from_block_2_instance_1->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_two_second_instance()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_2_from_block_2_instance_2->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_four_first_instance()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_4_from_block_1_instance_1->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_four_second_instance()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_4_from_block_1_instance_2->type_size.type);
            }

            [TestMethod, TestCategory("BlockInstanceTest")]
            void It_should_have_the_correct_type_for_the_test_variable_four_third_instance()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_var_4_from_block_1_instance_3->type_size.type);
            }
        };

    } // namespace FF
} // namespace ComponentTests
