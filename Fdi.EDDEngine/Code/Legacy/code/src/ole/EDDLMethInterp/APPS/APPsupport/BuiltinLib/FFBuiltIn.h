////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "FFBuiltIn.h"
//
// Creation Date: Dec. 07, 2011
//
// Purpose: Handles all details for a builtin function such as post-tokenized name, 
//			number of params, and type of each param, houses InvokeFunction which
//			actually calls the end point for builtins
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef ___FFBUILTIN_H__
#define ___FFBUILTIN_H__

#include "BuiltIn.h"

class CFFBuiltIn : public CBuiltIn
{
public:
	CFFBuiltIn();
	virtual ~CFFBuiltIn();

	///////////////////////////////////////////////////////////////////////////
	// BuildMapper						05/11/07	Mike DeCoster
	//
	// setup the CBuiltInMapper class by adding the builtin functions
	// I can handle
	///////////////////////////////////////////////////////////////////////////
	virtual void BuildMapper();

	///////////////////////////////////////////////////////////////////////////
	// InvokeFunction
	//
	// Override parent and add specific builtin handling, currently STUBBED
	///////////////////////////////////////////////////////////////////////////
	virtual bool InvokeFunction(
					char *pchFunctionName
					, int iNumberOfParameters
					, INTER_VARIANT *pVarParameters
					, INTER_VARIANT *pVarReturnValue
					, int	*pBuiltinReturnCode
					, FunctionExpression* pFuncExp = 0	// MTD 052407: added to pass func exp * to InvokeFunction for global param issues
					);


protected:

};

#endif