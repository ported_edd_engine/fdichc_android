


MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0007, DEVICE_REVISION 1, DD_REVISION 1

BLOCK test_block
{
    CHARACTERISTICS test_block_characteristics ;
    LABEL "Test Block" ;
    HELP "Help Test Block" ;
    
    PARAMETERS
    {
        BLOCK_TEST_RECORD, test_record;
    
        BLOCK_TEST_VAR_UINT, test_uint;
        BLOCK_TEST_VAR_INT, test_int;
        BLOCK_TEST_VAR_FLOAT, test_float;
        BLOCK_TEST_VAR_DOUBLE, test_double;
        
        BLOCK_TEST_VAR_ENUMERATED, test_enumerated;
        BLOCK_TEST_VAR_BIT_ENUMERATED, test_bit_enumerated;
        
        BLOCK_TEST_VAR_ARRAY_DOUBLE, test_array_double;
        
        BLOCK_TEST_VAR_ASCII, test_ascii;
        BLOCK_TEST_VAR_PACKED_ASCII, test_packed_ascii;
        BLOCK_TEST_VAR_PASSWORD, test_password;
        BLOCK_TEST_VAR_VISIBLESTRING, test_visiblestring;
        BLOCK_TEST_VAR_BITSTRING, test_bitstring;
        BLOCK_TEST_VAR_OCTETSTRING, test_octetstring;
        
        BLOCK_TEST_VAR_DATE, test_date;
        BLOCK_TEST_VAR_TIME, test_time;
        BLOCK_TEST_VAR_DATE_AND_TIME, test_date_and_time;
        BLOCK_TEST_VAR_TIME_VALUE, test_time_value;
        BLOCK_TEST_VAR_DURATION, test_duration;
    }
    
    PLUGINS
    {
        BLOCK_PLUGIN_TEST, test_plugin;
    }
    
    
    
    LOCAL_PARAMETERS
    {
        TEST_VAR_1, test_var_1;
    }
}

RECORD test_block_characteristics
{
    LABEL           "Label test block character" ;
    HELP             "help test block character";
    MEMBERS
    {
        TEST_VAR_1_CHARACTERISTICS, test_var_1;
    }
}

VARIABLE test_var_1
{
    LABEL "Test Variable 1";
    TYPE INTEGER (4);
    CLASS CONTAINED;
}

/****************************************************************************** */
/* Plugin                                                             */
/****************************************************************************** */

PLUGIN test_plugin
{
    LABEL "Test Plugin Label";
    HELP "Test Plugin Help";
    UUID 1EE6C3CE-F9C1-4EC5-8C5D-EB455BADA295;
}


/****************************************************************************** */
/* test record                                                              */
/****************************************************************************** */
RECORD test_record
{
    LABEL           "Label test record" ;
    HELP             "help test record";
    MEMBERS
    {
        TEST_RECORD_1, test_int;
        TEST_RECORD_2, test_uint;
    }
}

/****************************************************************************** */
/* Numberic types                                                               */
/****************************************************************************** */

VARIABLE test_uint
{
    LABEL "Test unsigned int variable";
    HELP "Test variable of type UNSIGNED_INTEGER(4)";
    TYPE UNSIGNED_INTEGER(4);
    CLASS CONTAINED;
}

VARIABLE test_int
{
    LABEL "Test int variable";
    HELP "Test variable of type INTEGER(4)";
    TYPE INTEGER(4);
    CLASS CONTAINED;
}

VARIABLE test_float
{
    LABEL "Test Float variable";
    HELP "Test variable of type FLOAT";
    TYPE FLOAT;
    CLASS CONTAINED;
}

VARIABLE test_double
{
    LABEL "Test Double variable";
    HELP "Test variable of type DOUBLE";
    TYPE DOUBLE;
    CLASS CONTAINED;
}

VARIABLE test_enumerated
{
    LABEL "Test enumerable variable";
    HELP "Test variable of type ENUMERATED";
    TYPE ENUMERATED{
        {1, "First"},
        {2, "Second"}
    }
    CLASS CONTAINED;
}

VARIABLE test_bit_enumerated
{
    LABEL "Test bit enumerable variable";
    HELP "Test variable of type BIT_ENUMERATED";
    TYPE BIT_ENUMERATED{
        {1, "First"},
        {2, "Second"}
    }
    CLASS CONTAINED;
}

/****************************************************************************** */
/* array types                                                                 */
/****************************************************************************** */

ARRAY test_array_double
{
    LABEL "Test array of test_double";
    HELP "Test array of type test_double[5]";
    
    TYPE test_double;
    NUMBER_OF_ELEMENTS 5;
}

/****************************************************************************** */
/* string types                                                                 */
/****************************************************************************** */

VARIABLE test_ascii
{
    LABEL "Test ascii variable";
    HELP "Test variable of type ASCII";
    TYPE ASCII (10) ;
    CLASS CONTAINED;
}

VARIABLE test_packed_ascii
{
    LABEL "Test EUC string variable";
    HELP "Test variable of type EUC string";
    TYPE EUC (10) ;
    CLASS CONTAINED;
}

VARIABLE test_password
{
    LABEL "Test password variable";
    HELP "Test variable of type PASSWORD";
    TYPE PASSWORD (10) ;
    CLASS CONTAINED;
}

VARIABLE test_visiblestring
{
    LABEL "Test VISIBLE string variable";
    HELP "Test variable of type VISIBLE string";
    TYPE VISIBLE (10) ;
    CLASS CONTAINED;
}

VARIABLE test_bitstring
{
    LABEL "Test BITSTRING variable";
    HELP "Test variable of type BITSTRING";
    TYPE BITSTRING (24) ;
    CLASS CONTAINED;
}

VARIABLE test_octetstring
{
    LABEL "Test OCTET variable";
    HELP "Test variable of type OCTET";
    TYPE OCTET (10) ;
    CLASS CONTAINED;
}

/****************************************************************************** */
/* date and time types                                                          */
/****************************************************************************** */

VARIABLE test_date
{
    LABEL "Test date variable";
    HELP "Test variable of type DATE";
    TYPE DATE ;
    CLASS CONTAINED;
}

VARIABLE test_time
{
    LABEL "Test TIME variable";
    HELP "Test variable of type TIME";
    TYPE TIME ;
    CLASS CONTAINED;
}

VARIABLE test_date_and_time
{
    LABEL "Test DATE_AND_TIME variable";
    HELP "Test variable of type DATE_AND_TIME";
    TYPE DATE_AND_TIME ;
    CLASS CONTAINED;
}

VARIABLE test_time_value
{
    LABEL "Test TIME_VALUE variable";
    HELP "Test variable of type TIME_VALUE";
    TYPE TIME_VALUE ;
    CLASS CONTAINED;
}

VARIABLE test_duration
{
    LABEL "Test DURATION variable";
    HELP "Test variable of type DURATION";
    TYPE DURATION ;
    CLASS CONTAINED;
}

