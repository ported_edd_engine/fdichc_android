/*************************************************************************************************
 *
 * $Workfile: ddbDeviceService.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		This is the defined interface to the device.  It is used by many classes throughout the
 * device description (from the items to the primatives).  It is also the principle interface from
 * the application to the device.
 *		
 * See Notes at the end of the file
 *		
 * #include "ddbDeviceService.h"
 */


#ifndef _DDBDEVICESERVICE_H
#define _DDBDEVICESERVICE_H
#ifdef INC_DEBUG
#pragma message("In ddbDeviceService.h") 
#endif

#include "ddbGeneral.h" // includes vector
#include "varient.h"
#include "foundation.h"
//prashant for methods support.oct 2003
#include <ServerProjects/rtn_code.h>
#include <3rdParty/panic.h>

class MEE;
#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbDeviceService.h") 
#endif

#define DEFAULT_COMPATABILITY dm_Standard		/* dm_275compatible */

/*######### systemLog destination channels ##############*/
#define CERR_LOG	1
#define CLOG_LOG	2
#define COUT_LOG	4
#define UI_LOG		8
#define STAT_LOG   16   /* status Bar Update */

/* future *
#define CRITLOG		 32
#define USRACK_LOG	 64
#define USRCCNLLOG	128
***********/


typedef struct dmdate_s
{
	unsigned char  day;
	unsigned char  mnth; 
	unsigned short year;
}dmDate_t;


typedef enum dState_e 
{ 
	ds_OffLine,		/* 0 */
	ds_OnLine,		/* 1 */
	ds_OffIniting,	/* 2 - offline & Initializing */
	ds_OnIniting	/* 3 -  online & Initializing */
} dState_t;


class hCmethod;
class CMenuDlg; 
class CMethodDlg;
typedef enum methodCallSource_e
{
	msrc_UNKNOWN,	// 0
	msrc_ACTION,	// called from a pre/post action
	msrc_EXTERN,	// called from an external location(usually the UI)
	msrc_METHOD		// future method calling a method
}/*typedef*/ methodCallSource_t;

// defines for method calls
class hCmethodCall
{
public:
	itemID_t methodID;
	hCmethod* m_pMeth;
	methodCallSource_t    source;  // what kind of call this is
	vector<CValueVarient> paramList;
	// stevev 27jan06 - track the used dialogs on the stack
	CMethodDlg* m_pMethodDlg;
	CMenuDlg*   m_pMenuDlg;	

	hCmethodCall():methodID(0),source(msrc_UNKNOWN),m_pMeth(NULL),m_pMethodDlg(NULL),m_pMenuDlg(NULL)
		{paramList.clear();};
	~hCmethodCall(){clear();};
	hCmethodCall(const hCmethodCall& src){operator=(src);};
	hCmethodCall& operator = (const hCmethodCall& src)
		{methodID=src.methodID;source=src.source;paramList=src.paramList;
	   m_pMeth=src.m_pMeth;m_pMethodDlg=src.m_pMethodDlg;m_pMenuDlg=src.m_pMenuDlg;return *this;};
	   //VMKP
	void clear(void){methodID=0;source=msrc_UNKNOWN;
			if(paramList.size())
				paramList.clear();
			m_pMeth=NULL;m_pMethodDlg=NULL;m_pMenuDlg=NULL;};
};

typedef vector<hCmethodCall>  methodCallList_t;


/////////////////////////////////////////////////////////////////////////////////////////////////

// external references
class hCitemBase;
typedef vector<hCitemBase*> ddbItemList_t;

class hCreference;
class hCpubsub;


// forward references
class hCobject;

/* * * notice that this is a pure virtual base class! * * */

class hCdeviceSrvc
{
public:     /* this is the ONLY public interface to the device itself */
//assume uneeded until proven otherwise
//	virtual 
//		RETURNCODE InitComm(void)                 = 0;

	virtual 
		RETURNCODE getItemBySymNumber(symbolNumb_t synNum, hCitemBase** ppReturnedItempointer)=0;
	virtual 
		RETURNCODE getItemBySymName  (    string& symName, hCitemBase** ppReturnedItempointer)=0;

	virtual
		void      getStartDate(dmDate_t& dt) = 0;

	virtual // must be cast to proper list type ( like new() is done )
		hCobject* getListPtr(CitemType  it) = 0; 

	virtual unsigned int getMEEdepth() =0; 

	// METHOD HANDLING
	virtual
		RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue) = 0;
};


// The global service interface is GUARANTEED not to return a NULL pointer!
// this class is returned in an error condition from the global service interface
class hCerrDeviceSrvc : public hCdeviceSrvc
{
	Indentity_t   ddbDeviceID;// leave this empty
public:
	hCerrDeviceSrvc();

	RETURNCODE getItemBySymNumber(itemID_t symNum, hCitemBase** ppReturnedItempointer);
	RETURNCODE getItemBySymName  (string& symName, hCitemBase** ppReturnedItempointer);

	RETURNCODE Read(vector<hCitemBase*>&  itemList);
	
	void      getStartDate(dmDate_t& dt){dmDate_t r={0,0,0}; dt = r; return;};

	hCobject*  getListPtr(CitemType  it);// must be cast to proper list type (like new() is done)

	void	 systemLog(int channel, char* format, ...) ;
	void	 systemLog(int channel, int errNumber,...) ;

	unsigned int getMEEdepth();

	RETURNCODE executeMethod(hCmethodCall oneMethod, CValueVarient& returnValue);

	devMode_t whatCompatability(void){return DEFAULT_COMPATABILITY;};
	/* stevev 09dec04 */
	RETURNCODE subscribe(int& returnedHandle,ddbItemList_t subscribedList,	hCpubsub* pPublishTo,			// pointer to the callback function
					unsigned long cycleTime = 1000,	int cyclesPerPublish = 1 )
	{returnedHandle = -1; return FAILURE;};
	virtual RETURNCODE unsubscribe(int handle){return FAILURE;};
};


#endif//_DDBDEVICESERVICE_H

/*************************************************************************************************
* NOTES:
* 5/13/03 - Change of interface design
* The device  Read/Write Interface has been modified.  The methods are ReadImd/WriteImd for
* Read/Write immediate.  This is the Synchronous interface.  Calling these will not return until
* they have a value or error from the device.
* The HCVar Read/Write Interface is now the Asynchronous interface.  Calling these read/write 
* functions will return immediately, with or without a valid value. You can allow the 
* 'push interface' to notify you when they have been updated or you can try again later.
*
* The list handling Read/Write are Asynchronous ONLY.
**************************************************************************************************
*/

/*************************************************************************************************
 *
 *   $History: ddbDeviceService.h $
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:17a
 * Created in $/DD Tools/DDB/ddbRead/ddbLib
 *
 *************************************************************************************************
 */
