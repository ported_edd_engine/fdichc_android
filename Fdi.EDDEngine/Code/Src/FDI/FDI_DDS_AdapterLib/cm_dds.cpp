/******************************************************************************
 * $Header: cm_dds.cpp: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:54 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description: Connection Manager DD Support Module
 *					This file contains all functions pertaining to DD
 *					Support in the Connection Manager.
 *
 *													   
 * $Workfile: cm_dds.cpp$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/


#include "STDINC.H"
//#include "afxconv.h"

#include "cm_loc.h"
#include "sysproto.h"

#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

#include "HARTBreaker.h"

#include "cm_dds.h"
#include "evl_loc.h"
#include "EddEngineLog.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define			EXT_PBO L"pbo"


////////////////////////////////////////////////////////////////////////
//
//	Tracing Disabled 
//
#pragma message("Tracing Disabled")
#undef tracef
#define tracef // 
////////////////////////////////////////////////////////////////////////

/*
 *	Function Pointers
 */


inline BOOL FileExists(const wchar_t* fname) { 
#ifdef	__GNUC__
    struct stat statBuf;
    char cpath[_MAX_PATH]{0};
    int length = PS_WideCharToMultiByte(CP_UTF8, 0, fname,
		(int)wcslen(fname)+1, cpath, 0, NULL, NULL);
   
	if (stat(cpath, &statBuf) == 0)
    {
        return TRUE;
    }
#else

	DWORD attrib = GetFileAttributes(fname);
	if (attrib != INVALID_FILE_ATTRIBUTES)
	{
		return TRUE;
	}
#endif
    return FALSE;
}



/***********************************************************************
 *
 *	Name:  cm_get_integer
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static UINT32
cm_get_integer( UINT8 *bin_ptr, int bin_size)

{
	UINT32	value = 0;
	int		byte_num;

	/*
	 *	Go through each byte and total up the integer value.
	 */

	for (byte_num = 0; byte_num < bin_size; byte_num ++) {

		value <<= 8;
		value += (UINT32)bin_ptr[byte_num];
	}

	return(value);
}


/***********************************************************************
 *
 *	Name:  ds_get_header
 *
 *	ShortDesc:  Get the DDOD file header from the DDOD file.
 *
 *	Description:
 *		The ds_get_header function takes a DDOD file and gets all of
 *		the header information.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		header - the DDOD file header information. 
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/


int
ds_get_header  ( // fd_fdi
	FILE *file,
	wchar_t *filepath,
	DDOD_HEADER *header)


{
	UINT8			header_buffer[HEADER_SIZE];
	int				r_code;

	/*
	 *	Read the DDOD Header into the buffer.
	 */
//	int headerSize=HEADER_SIZE;
	r_code = fread((char *)header_buffer,1,HEADER_SIZE,file);
	if (r_code != HEADER_SIZE) {
		return(CM_FILE_ERROR);
	}

	wcscpy(header->filepath,filepath);

    /*
	 *	Get each field in the DDOD Header, and perform verification
	 *	where applicable.
     */

	/*

#define	HEADER_SIZE					(MAGIC_NUMBER_SIZE +	\
									 HEADER_SIZE_SIZE +		\
									 META_DATA_SIZE +	\
									 ITEM_OBJECTS_SIZE +		\
									 MANUFACTURER_SIZE +	\
									 DEVICE_TYPE_SIZE +		\
									 DEVICE_REV_SIZE +		\
									 DD_REV_SIZE +			\
									LIT80_MAJOR_REV_SIZE +	\
									LIT80_MINOR_REV_SIZE +	\
									HEADER_EDD_PROFILE_SIZE+		\
									TOOL_RELEASE_SIZE +	    \
									 SIGNATURE_SIZE +		\
									 RESERVED3_SIZE +		\
									 RESERVED4_SIZE)
	*/

	header->magic_number =
			cm_get_integer(
			&header_buffer[MAGIC_NUMBER_OFFSET],
			MAGIC_NUMBER_SIZE);

	if (header->magic_number != 0x8a45f533) {
		return(CM_FILE_ERROR);
	}


	header->header_size =
			cm_get_integer(
			&header_buffer[HEADER_SIZE_OFFSET],
			HEADER_SIZE_SIZE);

	header->meta_data_size =
			cm_get_integer(
			&header_buffer[META_DATA_SIZE_OFFSET],
			META_DATA_SIZE_SIZE);

	header->item_objects_size =
			cm_get_integer(
			&header_buffer[ITEM_OBJECTS_SIZE_OFFSET],
			ITEM_OBJECTS_SIZE_SIZE);

	header->manufacturer =
			cm_get_integer(
			&header_buffer[MANUFACTURER_OFFSET],
			MANUFACTURER_SIZE);

	header->device_type =
			(UINT16) cm_get_integer(
			&header_buffer[DEVICE_TYPE_OFFSET],
			DEVICE_TYPE_SIZE);

	header->device_revision =
			(UINT8) cm_get_integer(
			&header_buffer[DEVICE_REV_OFFSET],
			DEVICE_REV_SIZE);

	header->dd_revision =
			(UINT8) cm_get_integer(
			&header_buffer[DD_REV_OFFSET],
			DD_REV_SIZE);

	header->major_revision =
			(UINT8) cm_get_integer(
			&header_buffer[LIT80_MAJOR_REV_OFFSET],
			LIT80_MAJOR_REV_SIZE);

	header->minor_revision =
			(UINT8) cm_get_integer(
			&header_buffer[LIT80_MINOR_REV_OFFSET],
			LIT80_MINOR_REV_SIZE);

	header->edd_profile =
			(HeaderEDDProfile)cm_get_integer(
			&header_buffer[HEADER_EDD_PROFILE_OFFSET],
			HEADER_EDD_PROFILE_SIZE);

	header->tool_release =
			(UINT8) cm_get_integer(
			&header_buffer[TOOL_RELEASE_OFFSET],
			TOOL_RELEASE_SIZE);

	header->signature =
			(UINT32) cm_get_integer(
			&header_buffer[SIGNATURE_OFFSET],
			SIGNATURE_SIZE);

	header->reserved3 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED3_OFFSET],
			RESERVED3_SIZE);

	header->reserved4 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED4_OFFSET],
			RESERVED4_SIZE);

	return(CM_SUCCESS);
}

// FD_FDI Comment
int CRodMgr::ds_get_object(FILE *file, OBJECT *obj, ROD_HANDLE *rod_handle)
{
	int cnt=fread(&obj->index, sizeof(UINT32), 1, file);

	if(cnt != 1)
		return CM_FILE_ERROR;

	obj->index=cm_get_integer((UINT8 *)&obj->index,sizeof(UINT32));

	cnt=fread(&obj->object_code, sizeof(UINT8), 1, file);

	if(cnt != 1)
		return CM_FILE_ERROR;

	cnt=fread(&obj->ext_length, sizeof(UINT32), 1, file);

	if(cnt != 1)
		return CM_FILE_ERROR;

	obj->ext_length=cm_get_integer((UINT8 *)&obj->ext_length,sizeof(UINT32));

	obj->extension=NULL;

	if(obj->ext_length>0)
	{
		obj->extension = (UINT8 *)rod_calloc(obj->ext_length, 1);

		if(!obj->extension)
			return CM_NO_MEMORY;
	}

	cnt=fread(obj->extension, obj->ext_length, 1, file);

	if(cnt != 1)
		return CM_FILE_ERROR;

	// store in rod if handle passed in
	if(rod_handle)
	{
		int r_code = rod_put(*rod_handle,(OBJECT_INDEX) obj->index, obj);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	return(CM_SUCCESS);
}

/***********************************************************************
 * FD_FDI Comment
 *	Name:  ds_get_ddef_desc
 *
 *	ShortDesc:  Get the DDEF DESC from the DDOD file.
 *
 *	Description:
 *		The ds_get_od_description function takes a DDOD file and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		
 *		
 *
 **********************************************************************/
int CRodMgr::ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc)
{
	int rc;

	OBJECT obj;
	rc=ds_get_object(file, &obj, NULL);

	if(rc)
		return rc;

	if(obj.object_code != FDI_OBJECT_CODE_DDEF_DESC)
		return CM_BAD_DDOD_FILE_FORMAT;

	if(obj.index != 0)
		return CM_BAD_DDOD_FILE_FORMAT;

	ddef_desc->ext_byte_count=obj.ext_length;

	int offset = 0;
	ddef_desc->item_count = cm_get_integer((UINT8 *)obj.extension + offset, sizeof(UINT32));
	offset += sizeof(UINT32);

	ddef_desc->block_count = (UINT16)cm_get_integer((UINT8 *)obj.extension + offset, sizeof(UINT16));
	offset += sizeof(UINT16);

	ddef_desc->first_item_index = cm_get_integer((UINT8 *)obj.extension + offset, sizeof(UINT32));
	offset += sizeof(UINT32);

	if (offset < (int)ddef_desc->ext_byte_count)
	{
		//Parse reserved 16 bits
		// We need to skip over this UINT16 but we don't use it so we need not get it
		//UINT16 reserved = (UINT16)cm_get_integer((UINT8 *)obj.extension + offset, sizeof(UINT16));
		offset += sizeof(UINT16);

		unsigned char* chunkp = obj.extension + offset;
		DDL_UINT size = ddef_desc->ext_byte_count - offset;

		if (size > 0)
		{
			DDL_UINT tag = 0;	/* identifier of a binary chunk */
			DDL_UINT len = 0;
			DDL_UINT intval = 0;
			DDL_PARSE_TAG(&chunkp, &size, &tag, &len);

			if (tag != EXTENDED_DESC_TAG)
				return DDL_ENCODING_ERROR;

			while (size > 0) {
				DDL_PARSE_TAG(&chunkp, &size, &tag, &len);

				switch (tag)
				{
				case LAYOUT:
					DDL_PARSE_INTEGER(&chunkp, &size, &intval);
					ddef_desc->desc_ext.layout = (nsEDDEngine::LayoutType)intval;
					break;
				case MANUFACUTRER_EXT:
				{
					STRING Manufacturer = {0};

					rc = ddl_parse_string(&chunkp, &size,
						&Manufacturer,
						nullptr, env_info, nullptr);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					ddef_desc->desc_ext.manufacturer_extended = Manufacturer.str;
				}
				break;
				case DEVICE_TYPE_EXT:
				{
					STRING Device_type = {0};

					rc = ddl_parse_string(&chunkp, &size,
						&Device_type,
						nullptr, env_info, nullptr);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					ddef_desc->desc_ext.device_extended = Device_type.str;
				}
				break;
				}
			}
		}
	}
	
	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  ds_get_od_description
 *
 *	ShortDesc:  Get the OD Description from the DDOD file.
 *
 *	Description:
 *		The ds_get_od_description function takes a DDOD file and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static inline int
ds_get_od_description( // fd_fdi
	FILE *file,
	OD_DESCRIPTION_SPECIFIC *od_description)


{
    UINT8		od_description_buffer[ODES_SIZE];
    int			r_code;

	/*
	 *	Read the OD Description into the buffer.
	 */

    r_code = fread((char *)od_description_buffer,1,ODES_SIZE,file);
    if (r_code != ODES_SIZE) {
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the OD Description.
     */

	od_description->sod_first_index =
			(OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_SOD_OFFSET],
			FIRST_IN_SOD_SIZE);

	od_description->sod_object_count =
			(UINT16) cm_get_integer(
			&od_description_buffer[SOD_LENGTH_OFFSET],
			SOD_LENGTH_SIZE);

	return(CM_SUCCESS);
}



/***********************************************************************
 *
 *	Name:  ds_get_object_value
 *
 *	ShortDesc:  Get an object's value.
 *
 *	Description:
 *		The ds_get_object_value function takes a DDOD file, DDOD file
 *		header, and object and gets the objects value.  It will attach
 *		the object's value to the object (both being in the ROD).
 *
 *	Inputs:
 *		file - the DDOD file.
 *		header - the header of the DDOD file.
 *		object - the object whose value will be retrieved.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/



static inline int
ds_get_object_value(
	FILE *file,
	DDOD_HEADER *header,
	OBJECT *object)


{
	object=object;
	file=file;
	header=header;

	ASSERT(0);
	return(CM_SUCCESS);
	/*
	UINT32		 offset;
	UINT8		*domain_data;

	///*
	// *	Check if there is no object to get a value for, or no
	// *	domain data to get.
	// *

	if (!object) {
		return(CM_SUCCESS);
	}
	if (!object->specific) {
		return(CM_BAD_OBJECT);
	}
	if (!object->specific->domain.size) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}
	if (object->specific->domain.local_address == 0xFFFFFFFF) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}

	/*
	 *	Calculate the offset into the DDOD file where the domain
	 *	data is located.
	 */
	// FD_FDI offset = object->specific->domain.local_address +
	//		header->header_size + header->objects_size;

	/*
	 *	Allocate space in the ROD heap for the domain data.
	 *

	domain_data =
			(UINT8 *) rod_malloc((int)object->specific->domain.size);
	if (!domain_data) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the file position and read in the domain data.
	 *

	int r_code = fseek(file,(long)offset,0);
	if (r_code < 0) {
		return(CM_FILE_ERROR);
	}
	size_t nNumRead = fread((char *)domain_data, 1,
			object->specific->domain.size, file);
	
	if (nNumRead != object->specific->domain.size) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Connect the domain data to the ROD object.
	 *

	object->value = domain_data;

	return(CM_SUCCESS);*/
}


/***********************************************************************
 *
 *	Name:  ds_ddod_file_load
 *
 *	ShortDesc:  Load a DDOD file.
 *
 *	Description:
 *		The ds_ddod_file_load function takes a DDOD file name and loads
 *		the corresponding DDOD file into a ROD.
 *
 *	Inputs:
 *		filename - the name of the DDOD file that is to be loaded.
 *
 *	Outputs:
 *		rod_handle - the handle of the ROD in which the DDOD was
 *					 loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
	
int CRodMgr::ds_ddod_file_load( // fd_fdi
	ENV_INFO *env_info,
	wchar_t *filename,
	ROD_HANDLE *rod_handle)


{
	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;

	FILE						*file;
	DDOD_HEADER					 header;
//	OBJECT						*object_0;
//	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 object_num;
	int							 r_code;
	DDEF_DESC					ddef_desc;

	/*
	 *	Open the DDOD file and get the Header information.
	 */

	file = PS_wfopen(filename, L"rb");
	if (!file) {
		return(CM_BAD_FILE_OPEN);
	}

	r_code = ds_get_header(file, filename, &header);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Allocate space in the ROD heap for an object, and set it up
	 *	as an OD description object.
	 */
/* FD_FDI Delete
	object_0 = (OBJECT *) rod_calloc(1,sizeof(OBJECT)); // fd_fdi
	if (object_0 == NULL) {
		return(CM_NO_ROD_MEMORY);
	}

	object_0->index = 0;
	object_0->code = OC_OD_DESCRIPTION;

	
	Allocate space in the ROD heap for an OD description object's
	specific information, and set it up to describe the DDROD.
	

	object_0->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object_0->specific) {
		return(CM_NO_ROD_MEMORY);
	}
	od_description = (OD_DESCRIPTION_SPECIFIC *)object_0->specific;*/

/*	r_code = ds_get_od_description(file,od_description);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	ds_get_od_description();*/

	r_code = ds_get_ddef_desc(env_info, file, &ddef_desc);

	if(r_code) 
		return r_code;
	
	/*
	 *	Open up the DDROD with object 0.
	 */

	/*
	 *	Allocate a new ROD and initialize its description.
	 */
	*rod_handle = rod_new_rod((LPWSTR)filename);

	if (*rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);

	}

	rod_open(&ddef_desc, *rod_handle);

	/*
	 *	Load all of the objects in the S-OD.  Note that these will
	 *	all be domain objects.
	 */

	// read device & block tables & dditems
//	ASSERT(0); // FD_FDI this is unecessary???
	for (object_num = 0; object_num < (int)(ddef_desc.item_count+ddef_desc.block_count+1);	object_num ++) {

		OBJECT *obj=(OBJECT *)rod_calloc(sizeof(OBJECT), 1);

		r_code = ds_get_object(file, obj, rod_handle);

		if (r_code != CM_SUCCESS) {
			//// fdi_uncomment PROFIBUS missing one item
			if(object_num>0)
			{
				PS_TRACE(L"\n*** WRONG NUMBER OF OBJECTS in DD... continuing anyway\n");
				break;
			}

			return(r_code);
		}
	}

	/*
	 *	Load all of the object's values in the S-OD.  Note that these
	 *	will all be domain data.
	 */

	// fd_fdi probably delete this section
/*	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object_value(file, &header, rod_tbl.list[*rod_handle]->sod[object_num]);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}
	*/

		//Load header
	r_code = rod_set_header(*rod_handle, header);
	if (r_code != CM_SUCCESS)
	{
		return(r_code);
	}

	r_code = rod_set_version(*rod_handle, header.major_revision<<8 | header.minor_revision);
	if (r_code != CM_SUCCESS)
	{
		return(r_code);
	}
	
	r_code = fclose(file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc)
{
	return m_RodMgr.ds_get_ddef_desc(env_info, file, ddef_desc);
}

int CDeviceTypeMgr::ds_ddod_file_load(ENV_INFO *env_info, /* fd_fdi */wchar_t *filename,  ROD_HANDLE *rod_handle)
{
	return m_RodMgr.ds_ddod_file_load(env_info, filename,rod_handle);
}
int CConnectionMgr::ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc)
{
	return g_DeviceTypeMgr.ds_get_ddef_desc(env_info, file, ddef_desc);
}
////////////////////////////////////////////////////////////////////////
//
//	Name:  ds_ddod_load
//
//	ShortDesc:  Load a DDOD.
//
//	Description:
//		The ds_ddod_load function takes a device handle and search
//		mode, and loads the corresponding DDOD into a ROD.  Only DDOD
//		file loading is supported (currently there is no support for
//		obtaining a DDOD over a network).
//
//	Inputs:
//		device_handle - the handle of the device whose DD is to be
//						loaded.
//	Outputs:
//		dd_handle - the handle of the DD that was loaded.
//
//	Returns:
//		CM_SUCCESS.
//		CM_BAD_DEVICE_HANDLE.
//		CM_FILE_NOT_FOUND.
//		Return values from ddod_file_load function.
//
//	Author:
//		Gabe Avila (6/21/96)
//
////////////////////////////////////////////////////////////////////////

int CConnectionMgr::ds_ddod_load(
	ENV_INFO		*env_info,
	DEVICE_HANDLE	device_handle,
	ROD_HANDLE*		rod_handle)
{
	tracef("BEGIN: ds_ddod_load\n");

	int				r_code;
	DD_DEVICE_ID	temp_dd_device_id = {0, 0, 0, 0};

	//
	// 	Check to make sure that the device handle is valid.
	//
	if(!valid_device_handle(device_handle)) {
		return CM_BAD_DEVICE_HANDLE;
	}

	//
	// 	Obtain the device ID.
	//
	DD_DEVICE_ID* dd_device_id = ADT_DD_DEVICE_ID(device_handle);
	
	(void) memcpy((char *)&temp_dd_device_id,
				(char *)dd_device_id,
				sizeof(DD_DEVICE_ID));

	wchar_t* pDDFilename =	temp_dd_device_id.dd_file_path; // FD_FDI  ds_dd_file_search(&temp_dd_device_id, EXT_PBO);

//	if(!pDDFilename)
	if(!::FileExists(pDDFilename))
	{
		tracef("	NOT FOUND:\n");
		tracef("END: ds_ddod_load\n");
		return CM_DD_NOT_FOUND;
	}
	else
	{

		(void) memcpy((char *)dd_device_id, (char *)&temp_dd_device_id, sizeof(DD_DEVICE_ID));
		
	}

	//
	//	Load the DDOD file into a ROD.
	//
	ROD_HANDLE temp_rod_handle;
	r_code = g_DeviceTypeMgr.ds_ddod_file_load(env_info, pDDFilename, &temp_rod_handle);

#ifdef _HARTBREAKER_	// Save the EDD Directory in the ADT Table
	CHARTBreaker::SetEDDDirectory(device_handle, pDDFilename);
#endif

//	delete [] pDDFilename;

	if(r_code != CM_SUCCESS)
	{
		return r_code;
	}

	//
	// Obtain device type handle
	//
	DEVICE_TYPE_HANDLE device_type_handle = ADT_ADTT_OFFSET(device_handle);
	if(!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) return CM_BAD_DEVICE_TYPE_HANDLE;

	//
	//	Update the compatibility flag in the Active Device Type Table.
	//
	#define LOADED	dd_device_id->ddid_device_rev
	#define DESIRED	temp_dd_device_id.ddid_device_rev

	if(LOADED == DESIRED) {
		g_DeviceTypeMgr.SET_ADTT_COMPATIBILITY(device_type_handle, CURRENT_DD_LOADED);
	}
	else if(LOADED < DESIRED) {

		g_DeviceTypeMgr.SET_ADTT_COMPATIBILITY(device_type_handle, OLDER_DD_LOADED);
	}
	else {
		g_DeviceTypeMgr.SET_ADTT_COMPATIBILITY(device_type_handle, NEWER_DD_LOADED);
	}

	#undef DESIRED
	#undef LOADED

	//
	//	Put the DD Handle into the Active Device Type Table.
	//

	SET_ADT_DD_HANDLE(device_handle, temp_rod_handle);

	*rod_handle = temp_rod_handle;

	tracef("END: ds_ddod_load\n");
	
	return CM_SUCCESS;
}



/***********************************************************************
 *
 *	Name:  ds_dd_device_load
 *
 *	ShortDesc:  Load a DD for a device.
 *
 *	Description:
 *		The ds_dd_device_load function takes a device handle and loads
 *		the corresponding DD for the device.  After the DD is loaded,
 *		the DD device tables will be built.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_DEV_LOAD.
 *		Return values from ds_ddod_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_device_load(
	ENV_INFO		*env_info,
	DEVICE_HANDLE	device_handle,
	ROD_HANDLE*		rod_handle)
{
	int		r_code=0;

	//
	//	Check to make sure that the device handle is valid.
	//

	if (!valid_device_handle(device_handle)) 
	{
		return(CM_BAD_DEVICE_HANDLE);
	}

	//
	// Check to make sure the device tables need to be loaded.
	//

	if (ADT_DD_DEV_TBLS(device_handle) != NULL) 
	{
	  	//
		//	Build any device type specific Application information.
		//
		r_code = load_sym_info(ADT_ADTT_OFFSET(device_handle));
		if (r_code) 
		{
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during load_sym_info() Error Code = %d %s", r_code, dds_error_string (r_code) );
			return(CM_BAD_DD_DEV_LOAD);
		}

  		return(CM_SUCCESS);
	}

	//
	//	Load a DDOD.
	//
	r_code = ds_ddod_load(env_info, device_handle,rod_handle);
	if (r_code != CM_SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during ds_ddod_load() Error Code = %d %s", r_code, dds_error_string (r_code) );
		return(r_code);
	}

	//
	//	Build the DD device tables.
	//
	r_code = ddi_build_dd_dev_tbls(env_info, device_handle);
	if (r_code) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during ddi_build_dd_dev_tbls() Error Code = %d %s", r_code, dds_error_string (r_code) );
		return(CM_BAD_DD_DEV_LOAD);
	}
	
	//
	//	Build any device type specific Application information.
	//
	r_code = load_sym_info(ADT_ADTT_OFFSET(device_handle));
	if (r_code) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during load_sym_info() Error Code = %d %s", r_code, dds_error_string (r_code));
		return(CM_BAD_DD_DEV_LOAD);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_block_load
 *
 *	ShortDesc:  Load a DD for a block.
 *
 *	Description:
 *		The ds_dd_block_load function takes a block handle and loads
 *		the corresponding DD for the block.  Loading a DD for a block
 *		consists of first loading the DD for the device (if not already
 *		done so), and then building the DD block tables.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose DD is to be
 *					   loaded.
 *
 *	Outputs:
 *		- none -
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_NO_DD_BLK_REF.
 *		CM_BAD_DD_BLK_LOAD.
 *		Return values from ds_dd_device_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_block_load(
	ENV_INFO *env_info, 
	BLOCK_HANDLE block_handle)
{
	int			r_code=0;

	//
	//	Check to make sure that the block handle is valid.
	//
	if (!valid_block_handle(block_handle)) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"%s", dds_error_string(CM_BAD_BLOCK_HANDLE));
		return(CM_BAD_BLOCK_HANDLE);
	}

	//
	//	Check to make sure that there is a DD Block ID.
	//
	if (ABT_DD_BLK_ID(block_handle) == 0) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"%s", dds_error_string(CM_NO_DD_BLK_REF));
		return(CM_NO_DD_BLK_REF);
	}

	//
 	//	Check if the DD has been loaded for the device.
	//
	if (!valid_dd_handle((ABT_DD_HANDLE(block_handle)))) 
	{
		ROD_HANDLE rod_handle = 0;

		// If not, load it.
		r_code = ds_dd_device_load(env_info, ABT_ADT_OFFSET(block_handle), &rod_handle);
		if (r_code != CM_SUCCESS) 
		{
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during ds_dd_device_load() Error Code = %d %s", r_code, dds_error_string(r_code));
			return(r_code);
		}
	}

	//
	//	Build the DD block tables.
	//
	r_code = ddi_build_dd_blk_tbls(env_info, block_handle);
	if (r_code) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Failed during ddi_build_dd_blk_tbls() Error Code = %d %s", r_code, dds_error_string(r_code));
		return(r_code);
	}
	
	//
	//	Build any block specific Application information.
	//    (currently none.)

	return CM_SUCCESS;
}

