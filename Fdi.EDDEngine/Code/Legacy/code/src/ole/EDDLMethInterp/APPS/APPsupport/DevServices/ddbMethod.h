/*************************************************************************************************
 *
 * $Workfile: ddbMethod.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		The method / method support classes
 *		8/29/02	sjv	created from DDBItems.h
 *
 * #include "ddbMethod.h"
 */

#ifndef _DDB_METHOD_H
#define _DDB_METHOD_H
#ifdef INC_DEBUG
#pragma message("In ddbMethod.h") 
#endif

#include "ddbGeneral.h"
#include "foundation.h"

//#include "ddldefs.h"
#include "ddbItemBase.h"

#include "ddbAttributes.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbMethod.h") 
#endif

// parameter
//	type = methodVarType_t
//  flags .. isArray(), isConst(), isRef()
//  string paramName

// params  - list of parameters
class hCmethod   : public hCitemBase /* itemType 5 METHOD_ITYPE   */
{
public:
	hCmethod(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCmethod(hCmethod*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	virtual ~hCmethod();
	bool IsMethod(void) { return true; };
	
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 1; return 0;};
	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);

	RETURNCODE Label(wstring& retStr);

	size_t getDef(char* &chDefinition, wchar_t* &wchDefinition);

	RETURNCODE getMethodType(hCmethodParam& mP);
	RETURNCODE getMethodParams(ParamList_t& pL);
};


#endif //_DDB_METHOD_H

/*************************************************************************************************
 *
 *   $History: ddbMethod.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
