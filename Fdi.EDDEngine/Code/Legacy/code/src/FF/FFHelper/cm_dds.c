#ifndef lint
static char SCCSID[] = "@(#)cm_dds.c	30.4  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_dds.c - Connection Manager DD Support Module
 *
 *	This file contains all functions pertaining to DD
 *	Support in the Connection Manager.
 */
#include "stdinc.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#ifdef SUN
#include <memory.h>		/* K&R only */
#endif /* SUN */

#include "cm_lib.h"
#include "panic.h"
#include "ConnectionMgr.h"

/***********************************************************************
 *
 *	Name:  ds_init
 *
 *	ShortDesc:  Initialize the DD Support.
 *
 *	Description:
 *		The ds_init function takes a path and sets it as the path
 *		to the DD files.
 *
 *	Inputs:
 *		path - the path to the DD files.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DD_PATH.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
int CConnectionMgr::ds_init(wchar_t *path)
{

	/*
	 *	Check if the path is valid.
	 */

    DWORD attrib = PS_GetFileAttributes(path);
	if (attrib != INVALID_FILE_ATTRIBUTES)
	{
		return(CM_SUCCESS);
	}
	CRASH_DBG();
	return(CM_BAD_DD_PATH);
}
