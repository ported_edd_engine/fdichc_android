/*************************************************************************************************
 *
 * $Workfile: ddbCommand.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		8/29/02	sjv	created from DDBitems.h
 *		9/05/02   sjv added attrTrans.h to the file
 *	
 * #include "ddbCommand.h".
 */

#ifndef _DDB_COMMAND_H
#define _DDB_COMMAND_H
#ifdef INC_DEBUG
#pragma message("In ddbCommand.h") 
#endif

#include "ddbResponseCode.h"
#include "HARTsupport.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbCommand.h") 
#endif


/***************************
 * The command class 
 * 
 **************************/

//CJK was here 9/3/2009
class hCcommand  : public hCitemBase /* itemType 2 COMMAND_ITYPE  */
{

public:
	hCcommand(DevInfcHandle_t h, aCitemBase* paItemBase);
	hCcommand(hCcommand*    pSrc,  itemIdentity_t newID);		/* from-typedef constructor */
	virtual ~hCcommand();
	RETURNCODE destroy(void);
	// visit the weight calculation algorithm upon the transactions
	bool IsCommand(void) {return true; };

	// required virtual
	hCattrBase*   newHCattr(aCattrBase* pACattr);  // builder 
	RETURNCODE setSize( hv_point_t& returnedSize, ulong qual )
						{returnedSize.xval=returnedSize.yval = 0; return 0;};
	RETURNCODE Label(wstring& retStr) { return baseLabel(NO_LABEL, retStr); };
	RETURNCODE Read(wstring &value){value = L"ERROR:Not a Var"; return FAILURE;};

	// accessors //
	int                getCmdNumber(void);// -1 on error
	RETURNCODE         getRespCodes(hCRespCodeList& rcList);// resolve and fill return list

};


#endif //_DDB_COMMAND_H

/*************************************************************************************************
 * NOTES:
 *	Transaction's packet generation is done by looping through the list of data items and asking
 * each one to add itself to the buffer.  The interesting stuff happens when there is a mask 
 * present(hCdataItem.width).  
 * When the dataitem encounters a mask ( if length != 0 then this is a continuation of ) it
 * will calculate the byte width in the buffer (ie 0x8000 has width of 2) and pass the mask & the
 * byte width (length) to the dataitem's variable (via Add())  The variable will place the masked
 * data into the buffer but will NOT increment the buffer ptr or byte count. The length (in bytes)
 * will percolate back up to the transaction who will pass it to the next data item until done.
 * The data item determines it is done when the variable's Add() returns from processing a mask
 * with the least significant bit set. The Dataitem then increments the buffer and byte count and
 * clears the length variable before returning to the transaction.
 *
 * ResHndl
 * A resolution handle.  Set the passed in handle to zero to force a new conditional resolution.
 * a new handle will be set.  Pass that returned handle to methods that you do not want 
 * re-resolved.  If the passed in handle does not match the current resolution, the conditionals
 * will be resolved again.
 *************************************************************************************************
 */
