/*************************************************************************************************
 *
 * $Workfile: ddbArray.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		
 *		8/29/04	sjv	created 
 *
 *		
 * #include "ddbArray.h".
 */

#ifndef _DDB_ARRAY_H
#define _DDB_ARRAY_H
#ifdef INC_DEBUG
#pragma message("In ddbArray.h") 
#endif

#include "ddbGeneral.h"
#include "foundation.h"

#include "ddbdefs.h"
#include "ddbItemBase.h"
#include "ddbVar.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbArray.h") 
#endif

/*
arrayAttrLabel			// base class,	use aCattrString 
arrayAttrHelp,			// base class	use aCattrString
arrayAttrValidity,		// base class	use aCattrCondLong
arrayAttrType,			// required 	use aCattrCondReference
arrayAttrElementCnt,	// required 	use aCattrCondLong
*/


class hCarray : public hCitemBase 
{
public:
	hCarray(DevInfcHandle_t h, aCitemBase* paItemBase);
	RETURNCODE destroy(void);
	virtual ~hCarray()  {/*TODO: see if we need a destructor*/};
	hCattrBase*    newHCattr(aCattrBase* pACattr){return NULL;}

public:
	RETURNCODE getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs);
	RETURNCODE getAllindexValues(vector<UINT32>& allValidindexes);
	virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress=true);
	virtual RETURNCODE Label(wstring& retStr);
	virtual RETURNCODE Help(wstring& retStr);
	hCitemBase* operator[](int idx0);
	bool       isInGroup (UINT32 iV) {  return true; };//todo execute count to truly validate
	unsigned long getCount() { return m_ulCount; };

private:
	unsigned long m_ulCount;
};

#endif	// _DDB_ARRAY_H
