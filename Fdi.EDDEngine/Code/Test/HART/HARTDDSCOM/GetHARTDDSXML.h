// GetHARTDDSXML.h : Declaration of the CGetHARTDDSXML

#pragma once
#include <afxtempl.h>
#include "resource.h"       // main symbols
#include "BuildDDSXML.h"
#include "nsEDDEngine\Common.h"

using namespace nsEDDEngine;
// IGetHARTDDSXML
[
	object,
	uuid("{6A2BE90E-6995-4D9A-8B31-29CE1119C33D}"),
	dual,	helpstring("IGetHARTDDSXML Interface"),
	pointer_default(unique)
]
__interface IGetHARTDDSXML : IDispatch
{
	[id(1), helpstring("method GetDDSInfo")] HRESULT GetDDSInfo([in] BSTR sFileName, [out] BSTR* sXMLOutput);
    [id(2), helpstring("method GetErrorString")] HRESULT GetErrorString([in] BSTR sFileName, [in] int iErrorcode, [out] BSTR* sErrorString);
//    [id(3), helpstring("method GetItemType")] HRESULT GetItemType([in] BSTR sFileName, [in] int iBlockInstance, [in] nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, [out] nsEDDEngine::ITEM_TYPE *pItemType);
    [id(3), helpstring("method GetItemType")] HRESULT GetItemType([in] BSTR sFileName, [in] int iBlockInstance, [in] int eType, [in] unsigned long id,  [in] unsigned long subindex, [out] int *pItemType);
    [id(4), helpstring("method GetCmdIdFromNumber")] HRESULT GetCmdIdFromNumber([in] BSTR sFileName, [in] int iBlockInstance, [in] unsigned long ulCmdNumber, [out] unsigned long *pCmdItemId);
    [id(5), helpstring("method GetItemTypeAndItemId")] HRESULT GetItemTypeAndItemId([in] BSTR sFileName, [in] int iBlockInstance, [in] int eType, [in] unsigned long id,  [in] unsigned long subindex, [out] int *pItemType, [out] unsigned long *pItemId);
    [id(6), helpstring("method GetAxisUnitRelItemId")] HRESULT GetAxisUnitRelItemId([in] BSTR sFileName, [in] int iBlockInstance, [in] unsigned long AxisItemId, [out] unsigned long* pUnitItemId);
    [id(7), helpstring("method GetParamUnitRelItemId")] HRESULT GetParamUnitRelItemId([in] BSTR sFileName, [in] int iBlockInstance, [in] int eType, [in] unsigned long id,  [in] unsigned long subindex, [out] unsigned long* pUnitItemId);
    // [id(8), helpstring("method GetItemType_Auto")] HRESULT GetItemType_Auto([in] BSTR sFileName, [out] int *pItemType);
    //[id(9), helpstring("method GetErrorString_Auto")] HRESULT GetErrorString_Auto([in] BSTR sFileName, [out] BSTR* sErrorString);
	[id(8)] HRESULT OnlyCreateEDDEngine([in] BSTR bstrFileName, [out] LONG* lMemoryFootprint);
	[id(9)] HRESULT DeleteOneEDDEngineElement([out] VARIANT_BOOL* bDone, [out] LONG* lMemoryFootprint);
	[id(10)] HRESULT SetLoadXML([in] VARIANT_BOOL bLoadXML);
	[id(11)] HRESULT GetDeviceInstanceMemoryUsage([out] LONG* lMemoryUsage);
};


class CDDSXMLBuilder;

// CGetHARTDDSXML

[
	coclass,
	threading(free),
	vi_progid("HARTDDSCOM.GetHARTDDSXML"),
	progid("HARTDDSCOM.GetHARTDDSXML.1"),
	version(1.0),
	uuid("1A502BB2-EB23-4898-9C94-3E169B31464B"),
	helpstring("GetHARTDDSXML Class"),
	default(IGetHARTDDSXML)
]
class ATL_NO_VTABLE CGetHARTDDSXML : 
	public IGetHARTDDSXML
{
public:
	CGetHARTDDSXML();
	~CGetHARTDDSXML();


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:

	STDMETHOD(GetDDSInfo)(BSTR sFileName, BSTR* sXMLOutput);
    STDMETHOD(GetErrorString)(BSTR sFileName, int ErrorCode, BSTR* sErrorString);
    //STDMETHOD(GetItemType)(BSTR sFileName, int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec,  nsEDDEngine::ITEM_TYPE *pItemType);
    STDMETHOD(GetItemType)(BSTR sFileName, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItemType);
    STDMETHOD (GetCmdIdFromNumber)(BSTR sFileName, int iBlockInstance, unsigned long ulCmdNumber, unsigned long * pCmdItemId);
    STDMETHOD (GetItemTypeAndItemId)(BSTR sFileName, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, int *pItemType, unsigned long *pItemId);
    STDMETHOD (GetAxisUnitRelItemId)(BSTR sFileName, int iBlockInstance, unsigned long AxisItemId, unsigned long* pUnitItemId);
    STDMETHOD (GetParamUnitRelItemId)(BSTR sFileName, int iBlockInstance, int eType, unsigned long id, unsigned long subindex, unsigned long* pUnitItemId);
    //STDMETHOD (GetItemType_Auto)(BSTR sFileName, int *pItemType);
    //STDMETHOD (GetErrorString_Auto)(BSTR sFileName, BSTR* sErrorString);
	STDMETHOD(OnlyCreateEDDEngine)(BSTR bstrFileName, LONG* ulMemoryFootPrint);

private:
	CTypedPtrList<CPtrList, nsEDDEngine::IEDDEngine*> m_EDDEngineArray;
	int ReadConfigXML(wchar_t* configXML, int configXMLLen);
	bool m_bLoadXML;
	int m_iDeviceInstanceMemoryUsage;
		
public:
	STDMETHOD(DeleteOneEDDEngineElement)(VARIANT_BOOL* bDone, LONG* ulMemoryFootprint);
	STDMETHOD(SetLoadXML)(VARIANT_BOOL bLoadXML);
	STDMETHOD(GetDeviceInstanceMemoryUsage)(LONG* lMemoryUsage);
};
