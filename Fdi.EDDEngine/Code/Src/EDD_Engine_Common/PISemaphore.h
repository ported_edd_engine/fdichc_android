#pragma once
#include <string>
#include "TypesCommon.h"
using namespace std;
template <typename PlatformType>
class PISemaphore
{
private:

	PlatformType* m_pPlatformType;

public:
	// Constructors
	PISemaphore()
	{	
		m_pPlatformType = new PlatformType();
	}

	virtual ~PISemaphore()
	{
		if (m_pPlatformType != NULL)
		{
			delete m_pPlatformType;
			m_pPlatformType = NULL;
		}
	}

    BOOL InitSemaphore(long nInitialCount, long nMaxCount, wstring semaphoreName)
	{
		return m_pPlatformType->InitSemaphore(nInitialCount, nMaxCount, semaphoreName);
	}

	BOOL WaitSemaphore()
	{
		return m_pPlatformType->WaitSemaphore();
	}

	BOOL ReleaseSemaphore(long lCurrentCount, long* plPreviousCount)
	{
		return m_pPlatformType->ReleaseSemaphore(lCurrentCount, plPreviousCount);
	}

	BOOL CloseSemaphore()
	{
		return m_pPlatformType->CloseSemaphore();
	}
};
