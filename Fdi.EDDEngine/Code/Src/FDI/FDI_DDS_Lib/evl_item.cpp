/**
* @(#) $Id: evl_item.cpp,v 1.5 2012/10/04 23:50:57 rgretta Exp $
*/


#include "stdafx.h"
#include "ddi_lib.h"
#include "evl_loc.h"
#include "HARTBreaker.h"
#include "conversions.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"

#include "nsEDDEngine/Flats.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif
extern int get_ddtype(ENV_INFO *env_info);
//#define __LOG_ATTRIBUTES__	// Turns on logging of all the Attributes found, whether they exist, are okay or have errors
//#define __LOG_ITEM_INFO__		// Turns on logging of Items

#define IS_TYPE(x, aitem) (strcmp(typeid(x).name(), aitem.type) ? false : true )
#define CAST_CHECK(x, aitem) (strcmp(typeid(x).name(), aitem.type) ? NULL : (x *)aitem.attribute ); ASSERT(!strcmp(typeid(x).name(), aitem.type));

typedef int (*eval_type)(unsigned char*, DDL_UINT, void*, nsEDDEngine::OP_REF_LIST*, ENV_INFO*, nsEDDEngine::OP_REF*, void*);

#ifdef _WIN32
__declspec(dllimport)
#endif
extern const char *attribute_names[];

#ifdef __LOG_ITEM_INFO__
nsEDDEngine::SYMBOL_TBL_ELEM *get_symbol_tbl_lookup(BLOCK_HANDLE block_handle, ENV_INFO* env_info, int id);

int g_totalItems=0;
int g_indentLevel=0;
#endif

#ifdef __LOG_ATTRIBUTES__
int g_totalErrors=0;
int g_totalExpectedErrors=0;


std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeDataItem> attribute_map;
#endif

/*********************************************************************
 *
 *	Name: item_mask_man()
 *	ShortDesc: item_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		item_mask_man() handles all mask switch cases for the flat structure interface
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		attribute: 		the attribute to load if evaluation is necessary
 *		env_info:	the upcall information for this call to DDS
 *		var_needed:		the if the value of a variable is needed, but is not
 *						available, "var_needed" info is returned to the application
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by item_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *		var_needed:		var_needed specifies the variable needed to complete
 *						evaluation
 *		depbin:			the dependencies for this attribute
 *		var_needed:		the variable needed if parameter value service does not
 *						succeed.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
item_mask_man(
::nsEDDEngine::AttributeName  attr_mask,
FLAT_MASKS     *masks,
eval_type		eval,
DEPBIN         *depbin,
void           *attribute,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void		*optional=0)
{

	int             rc;	/* return code */

#ifdef DDSTEST
	TEST_FAIL(MASK_MAN_EVAL);
#endif

	/*
	 * No binary exists
	 */

	if (!(attr_mask & masks->bin_exists)) {

		if (attr_mask & masks->attr_avail) {

			rc = DDL_SUCCESS;	/* value already exists */
		}

		/*
		 * Must be defaulted
		 */

		else {

			rc = DDL_DEFAULT_ATTR;
		}
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & masks->bin_hooked) && !(attr_mask & masks->dynamic)) {

		/*
		 * If value is already available
		 */

		if (attr_mask & masks->attr_avail) {

			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else {

			rc = DDL_BINARY_REQUIRED;
		}
	}

	else {

		/*
		 * check the pointer
		 */

		if (depbin == NULL) {

			rc = DDL_NULL_POINTER;
		}

		/*
		 * check masks for evaluating
		 */

		else if ((!(attr_mask & masks->attr_avail)) || (attr_mask & masks->dynamic)) {

			rc = eval(depbin->bin_chunk,
						depbin->bin_size,
						attribute,
						&depbin->dep,
						env_info,
						var_needed,
						optional);

			if (rc == DDL_SUCCESS) {

				masks->attr_avail |= attr_mask;
			}

			if (depbin->dep.count) {
				masks->dynamic |= attr_mask;
			}
		}

		/*
		 * evaluation is not necessary
		 */
		else {

			rc = DDL_SUCCESS;
		}
	}



	if (rc != DDL_SUCCESS) {

		/*
		 * Turn off the attr_available mask for this attribute
		 */

		masks->attr_avail &= ~attr_mask;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: get_simple_ref_from_Expression
 *	ShortDesc: Get a variable id if it is all that is in a depbin 
 *
 *	Description:
 *		Get a variable id if the Expression_Specifier sequence in the depbin is:
 *			OBJECT_TAG, EXPRESSION_TAG, REF_OPCODE, and Reference
 *
 *	Inputs:
 *		env_info:
 *		depbin:    		the depbin structure for this attribute
 *		ref:			a pointer to the DESC_REF that should be filled in
 *	Outputs:
 *		ref:			The DESC_REF is filled in on success, or zeros if for any
 *						reason it could not be filled in
 *
 *	Returns:
 *		int				Error returns should be ignored.  It does not have the knowledge
 *						to determine if there is an actual error or the condition should be
 *						expected.  A return value is only provided because DDL_PARSE_TAG is
 *						being used and it contains a return in the definition.
 *
 *	Author: Mark Sandmann
 *
 **********************************************************************/

static
int
get_simple_ref_from_Expression(
	ENV_INFO    *env_info,
	DEPBIN		*depbin,
	nsEDDEngine::OP_REF		*ref)
{
	int             rc = DDL_INSUFFICIENT_OCTETS;		/* rc is set by the DDL_PARSE_TAG() macro */
	int             r_code = DDL_INSUFFICIENT_OCTETS;	/* return code */

	// By default the ID and Type are returned as zeros.  Only when the conditions are
	// just right do we return a variable reference.
	ref->~OP_REF();

	// If there is a binary, we can determine if it is a simple REF_OPCODE
	if (depbin != NULL) 
	{
		ulong bin_size= depbin->bin_size;
		uchar *bin_chunk = depbin->bin_chunk;

		// don't even think about it if bin_chunk is null
		if (bin_chunk != NULL)
		{
			DDL_UINT        len;	/* # of DATA bytes associated with TAG */
			DDL_UINT        tag;	/* "label" associated with current chunk */

			// Must be an OBJECT_TAG
			DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
			if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == OBJECT_TAG) )
			{
				// Must be an EXPRESSION_TAG
				DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
				if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == EXPRESSION_TAG) )
				{
					// Must be a REF_OPCODE
					DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, (DDL_UINT *) NULL_PTR);
					if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == REF_OPCODE) )
					{
						nsEDDEngine::OP_REF tempOpRef;
						nsEDDEngine::OP_REF var_needed;

						// process the remainder of the of the chunk to get the variable reference
						r_code = ddl_parse_op_ref(&bin_chunk, &bin_size, &tempOpRef, NULL, env_info, &var_needed);

						// If we got a successful reference parse and the chunk has nothing else
						// then we can return it
						if ((r_code == DDL_SUCCESS) && (bin_size == 0))
						{
							*ref = tempOpRef;
						}
						else
						{
							r_code = DDL_INSUFFICIENT_OCTETS;
						}
					}
					else
					{
						r_code = DDL_INSUFFICIENT_OCTETS;
					}
				}
			}

		}

	}
	return r_code;
}


/*********************************************************************
 *
 *	Name: get_simple_ref_from_MinMax
 *	ShortDesc: Get a variable id if it is all that is in a depbin 
 *
 *	Description:
 *		Get a variable id if the MinMax_Value_Specifier sequence in the depbin is:
 *			OBJECT_TAG, MIN_MAX_TAG, INTEGER, OBJECT_TAG, EXPRESSION_TAG, REF_OPCODE, Reference
 *
 *	Inputs:
 *		env_info:
 *		depbin:    		the depbin structure for this attribute
 *		ref:			a pointer to the DESC_REF that should be filled in
 *	Outputs:
 *		ref:			The DESC_REF is filled in on success, or zeros if for any
 *						reason it could not be filled in
 *
 *	Returns:
 *		int				Error returns should be ignored.  It does not have the knowledge
 *						to determine if there is an actual error or the condition should be
 *						expected.  A return value is only provided because DDL_PARSE_TAG is
 *						being used and it contains a return in the definition.
 *
 *	Author: Mark Sandmann
 *
 **********************************************************************/

static
int
get_simple_ref_from_MinMax(
	ENV_INFO    *env_info,
	DEPBIN		*depbin,
	nsEDDEngine::OP_REF		*ref)
{
	int             rc = DDL_INSUFFICIENT_OCTETS;		/* rc is set by the DDL_PARSE_TAG() macro */
	int             r_code = DDL_INSUFFICIENT_OCTETS;	/* return code */


	// By default the ID and Type are returned as zeros.  Only when the conditions are
	// just right do we return a variable reference.
	ref->~OP_REF();

	// If there is a binary, we can determine if it is a simple REF_OPCODE
	if (depbin != NULL) 
	{
		ulong bin_size= depbin->bin_size;
		uchar *bin_chunk = depbin->bin_chunk;

		// don't even think about it if bin_chunk is null
		if (bin_chunk != NULL)
		{
			DDL_UINT        len;	/* # of DATA bytes associated with TAG */
			DDL_UINT        tag;	/* "label" associated with current chunk */

			// Must be an OBJECT_TAG
			DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
			if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == OBJECT_TAG) )
			{
				// Must be a MIN_MAX_TAG
				DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
				if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == MIN_MAX_TAG) )
				{
					// Read the "which" value, but don't store
					DDL_PARSE_INTEGER(&bin_chunk, &bin_size, &tag);

					// Must be an OBJECT_TAG
					DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
					if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == OBJECT_TAG) )
					{
						// Must be an EXPRESSION_TAG
						DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
						if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == EXPRESSION_TAG) )
						{
							// Must be a REF_OPCODE
							DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, (DDL_UINT *) NULL_PTR);
							if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == REF_OPCODE) )
							{
								nsEDDEngine::OP_REF tempOpRef;
								nsEDDEngine::OP_REF var_needed;

								// process the remainder of the of the chunk to get the reference
								r_code = ddl_parse_op_ref(&bin_chunk, &bin_size, &tempOpRef, NULL, env_info, &var_needed);

								// If we got a successful reference parse and the chunk has nothing else
								// then we can return it
								if ((r_code == DDL_SUCCESS) && (bin_size == 0))
								{
									*ref = tempOpRef;
								}
								else
								{
									r_code = DDL_INSUFFICIENT_OCTETS;
								}
							}
							else
							{
								r_code = DDL_INSUFFICIENT_OCTETS;
							}
						}
					}
				}
			}
		}
	}
	return r_code;
}


/*********************************************************************
 *
 *	Name: load_errors
 *	ShortDesc: load the RETURN_LIST
 *
 *	Description:
 *		load_errors will append the RETURN_LIST with an error
 *
 *	Inputs:
 *		errors:		the RETURN_LIST
 *		rc:			the return code to add to the list
 *		var_needed:	the OP_REF if parameter value service does not succeed
 *		attr_mask:	the attribute that the error is for
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
void
load_errors(
RETURN_LIST    *errors,
int             rc,
nsEDDEngine::OP_REF         *var_needed,
::nsEDDEngine::AttributeName  attr_mask)
{

	RETURN_INFO    *temp_errors;	/* temp storage for return info */


	if (errors->count >= RETURN_LIST_SIZE) {
		return;
	}

	temp_errors = &errors->list[errors->count++];
	temp_errors->bad_attr = attr_mask;

	temp_errors->var_needed.op_info.id = 0;
	temp_errors->var_needed.op_info.member = 0;
	temp_errors->var_needed.op_info.type = nsEDDEngine::ITYPE_NO_VALUE;

	switch (rc) {

	case DDL_DEFAULT_ATTR:

		/*
		 * if load error is called with a return code of
		 * DDL_DEFAULT_ATTR, convert this to a DDL_ENCODING ERROR
		 */
		temp_errors->rc = DDL_ENCODING_ERROR;
		break;

	default:

		if ( (var_needed != nullptr)
			&& ( (var_needed->op_info.id != 0) || (var_needed->op_info_list.count != 0 ) )
			)
		{
			temp_errors->var_needed = *var_needed;
		}
		temp_errors->rc = rc;
		break;
	}
}

/*********************************************************************
 *
 *	Name: promote_range_data_element()
 *	ShortDesc: promote each element of a range data list to a specified type.
 *
 *	Description:
 *		Promote elements of a range data list to a specified type according to
 *		the following rules:
 *		specified type = int,uint,float,double and
 *				element type = int,uint,float,double
 *				-> promote element type.
 *		specified type = int,uint,float,double and
 *				element type = index,enum,bit_enum
 *				-> do nothing, go to next element.
 *		specified type = index,enum,bit_enum   and
 *				element type = don't care
				-> return.
 *
 *	Inputs:
 *		type:	specified type to promote range data element to.
 *		list:	list of range data elements to be promoted.
 *
 *	Outputs:
 *		list:	list of promoted range data elements.
 *
 *	Returns:
 *		DDL_SUCCESS,
 *		DDL_INVALID_PARAM
 *
 *	Author: Steve Beyerl
 *
 **********************************************************************/
static inline void
promote_range_data_element(
unsigned short  type,		/** desired type **/
RANGE_DATA_LIST *list)		/** list of MIN/MAX values **/
{

	int             i;	/** loop counter **/
	EXPR           *expr;	/** pointer to list of EXPR's **/

	if (!list) {
		return;
	}

	if (!list->list || (list->count == 0)) {
		return;
	}

	expr = list->list;	/** pointer to the first element of the list **/
	for (i = 0; i < list->count; i++, expr++) {
		switch (type) {	/** desired type **/
		case INTEGER:
			switch (expr->type) {	/** current type **/
			case INTEGER:
				expr->size = 4;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.i = (long) expr->val.u;
				expr->type = INTEGER;
				expr->size = 4;
				break;
            case FLOATG_PT:
				expr->val.i = (long) expr->val.f;
				expr->type = INTEGER;
				expr->size = 4;
				break;
            case DOUBLEG_PT:
				expr->val.i = (long) expr->val.d;
				expr->type = INTEGER;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
		case UNSIGNED:
			switch (expr->type) {
			case INTEGER:
				expr->val.u = (unsigned long) expr->val.i;
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
            case FLOATG_PT:
				expr->val.u = (unsigned long) expr->val.f;
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
            case DOUBLEG_PT:
				expr->val.u = (unsigned long) expr->val.d;
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
        case FLOATG_PT:
			switch (expr->type) {
			case INTEGER:
				expr->val.f = (float) expr->val.i;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.f = (float) expr->val.u;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
            case FLOATG_PT:
				break;
            case DOUBLEG_PT:
				expr->val.f = (float) expr->val.d;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
        case DOUBLEG_PT:
			switch (expr->type) {
			case INTEGER:
				expr->val.d = (double) expr->val.i;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.d = (double) expr->val.u;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
            case FLOATG_PT:
				expr->val.d = (double) expr->val.f;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
            case DOUBLEG_PT:
				break;
			default:
				break;
			}
			break;
		default:	/** specified type = enum,bit_enum,index **/
			return;
			/* NOTREACHED */
			break;
		}
	}			/* end of FOR */
}


/*********************************************************************
 *
 *	Name: eval_attr_reflist
 *	ShortDesc: evaluate a reflist
 *
 *	Description:
 *		eval_attr_reflist is a generic handler for reflists which are
 *		an optional attribute. It calls item_mask_man() and if DDL_DEFAULT is
 *		returned it loads an empty list and returns
 *
 *	Inputs:
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked, attr_avail, dynamic)
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		reflist:		the reference list to load
 *		depbin:    		the depbin structure for this attribute
 *
 *	Outputs:
 *		errors:			the appended RETURN_LIST if there was an error
 *		reflist:		the loaded reflist if eval was called
 *		depbin:			the dependencies for this attribute
 *
 *	Returns:
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_reflist(
FLAT_MASKS     *masks,
::nsEDDEngine::AttributeName  attr_mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
ITEM_ID_LIST   *reflist,
DEPBIN         *depbin)
{

	int             rc;	/* return code */
	nsEDDEngine::OP_REF          var_needed;	/* temp var_needed storage */

	rc = item_mask_man(attr_mask,
		masks,
		eval_reflist,
		depbin,
		reflist,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			reflist->count = 0;

			masks->attr_avail |= attr_mask;
		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_attr_actions
 *	ShortDesc: evaluate an action
 *
 *	Description:
 *		eval_attr_actions is a generic handler for actions which are
 *		a optional attributes. It calls item_mask_man() and if DDL_DEFAULT is
 *		returned it loads an empty list and returns
 *
 *	Inputs:
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked, attr_avail, dynamic)
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		reflist:		the reference list to load
 *		depbin:    		the depbin structure for this attribute
 *
 *	Outputs:
 *		errors:			the appended RETURN_LIST if there was an error
 *		reflist:		the loaded reflist if eval was called
 *		depbin:			the dependencies for this attribute
 *
 *	Returns:
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_actions(
FLAT_MASKS     *masks,
::nsEDDEngine::AttributeName    attr_mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
ACTION_LIST	   *pActionList,
DEPBIN         *depbin)
{

	int             rc;	/* return code */
	nsEDDEngine::OP_REF          var_needed;	/* temp var_needed storage */

	rc = item_mask_man(attr_mask,
		masks,
		eval_actions,
		depbin,
		pActionList,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {	// The default is no actions

			pActionList->count = 0;			
			masks->attr_avail |= attr_mask;
		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_attr_data_reflist
 *	ShortDesc: load the reflist
 *
 *	Description:
 *		load the reflist
 *
 *	Inputs:
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked, attr_avail, dynamic)
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		reflist:		the reference list to load
 *		depbin:    		the depbin structure for this attribute
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

static
int
eval_attr_data_reflist(
FLAT_MASKS     *masks,
::nsEDDEngine::AttributeName    attr_mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
DATA_ITEM_LIST *reflist,
DEPBIN         *depbin)
{

	int             rc;	/* return code */
	nsEDDEngine::OP_REF          var_needed;	/* temp var_needed storage */

	rc = item_mask_man(attr_mask,
		masks,
		eval_data_reflist,
		depbin,
		reflist,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			reflist->count = 0;

			masks->attr_avail |= attr_mask;
		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_attr_string
 *	ShortDesc: evaluate a string
 *
 *	Description:
 *		eval_attr_string is a generic function for evaluating a string
 *		when that string can be defaulted
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		string: 		the string to load if evaluation is necessary
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		def_string:		indicates which default string should be gotten from
 *						standart.dct.
 *
 *	Outputs:
 *		depbin:			the dependencies for this attribute
 *		string:			the loaded string if eval was called
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		return 			void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_string(
::nsEDDEngine::AttributeName    attr_mask,
FLAT_MASKS     *masks,
eval_type		eval,
DEPBIN         *depbin,
STRING         *string,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
unsigned long   def_string)
{

	int             rc;	/* return code */
	nsEDDEngine::OP_REF          var_needed;	/* temp var_needed storage */

	rc = item_mask_man(attr_mask,
		masks,
		eval,
		depbin,
		string,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			rc = app_func_get_dict_string(env_info, def_string, string);


			/*
			 * If a string was not found, get the default error
			 * string.
			 */

			if (rc != DDL_SUCCESS) {

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING, string);
				if (rc != DDL_SUCCESS) {

					load_errors(errors, rc, &var_needed, attr_mask);
				}
			}

			if (rc == DDL_SUCCESS) {
				masks->attr_avail |= attr_mask;
			}
			else {
				load_errors(errors, rc, &var_needed, attr_mask);
			}

		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: get_image
 *	ShortDesc: load the image
 *
 *	Description:
 *		load the image
 *
 *	Inputs:
 *		env_info:			the upcall information for this call to DDS
 *		image_table_index:  table index
 *		bin_image:			image
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
int get_image(ENV_INFO *env_info, int image_table_index, ::nsEDDEngine::BINARY_IMAGE *bin_image)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	 nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir=NULL;

	 int rc = DDL_SUCCESS;

	 switch (env_info->handle_type)
	 {
	 case ENV_INFO::BlockHandle:
		 rc = pConnectionMgr->get_abt_dd_dev_tbls (env_info->block_handle, (void **)(&flat_device_dir));
		 break;
	 case ENV_INFO::DeviceHandle:
		 rc = pConnectionMgr->get_adt_dd_dev_tbls(env_info->block_handle,(void **) &flat_device_dir);
		 break;
	 default:
		 return DDI_INVALID_DD_HANDLE_TYPE;
	 }

	 if(rc)
		 return rc;

	 if(0 > image_table_index || image_table_index >= flat_device_dir->image_tbl.count)
		 return DDL_ENCODING_ERROR;

	 int offset=0;
	 int length=0;

	 CStdString sCountryCode(env_info->lang_code);
	 sCountryCode = sCountryCode.Trim(_T('|'));

	 // search for image using current language
	 for(unsigned long i=0;i<flat_device_dir->image_tbl.list[image_table_index].count;i++)
	 {
        wchar_t wLangCode[6]{0};
		int len = strlen(flat_device_dir->image_tbl.list[image_table_index].list[i].language_code);

		mbstowcs(wLangCode, flat_device_dir->image_tbl.list[image_table_index].list[i].language_code, len + 1);

		if(sCountryCode.Compare(wLangCode) == 0)
		{
			 offset=flat_device_dir->image_tbl.list[image_table_index].list[i].image_file_offset;
			 length=flat_device_dir->image_tbl.list[image_table_index].list[i].image_file_length;
			 break;
		}
	 }

	 // did not find it. use the first one in the list
	 if(!length)
	 {
		 offset=flat_device_dir->image_tbl.list[image_table_index].list[0].image_file_offset;
		 length=flat_device_dir->image_tbl.list[image_table_index].list[0].image_file_length;
	 }

	 if(!length)
		 return DDL_ENCODING_ERROR;

     bin_image->data = new (unsigned char[length]);

	 bin_image->length=length;

	 FILE *f=PS_wfopen(flat_device_dir->header.filepath, L"rb");
	 if(f)
	 {
		 int images_offset=flat_device_dir->header.header_size
			 +flat_device_dir->header.metadata_size
			 +flat_device_dir->header.item_objects_size
			 +offset;

		 int size_read=0;
		 rc=::fseek(f,images_offset,0);
		 if(!rc)
			size_read=::fread(bin_image->data,1,length,f);

		 ::fclose(f);

#ifdef _DEBUG

	 ::CreateDirectory(_T("images"),NULL);

     wchar_t buf[256]{0};
     ::wsprintf(buf,_T("%s/%s_%d.jpg"),_T("images"),_T("image"),image_table_index);

	 FILE *fi=PS_wfopen(buf, L"wb");

	 if(fi)
	 {
		 ::fwrite(bin_image->data,1,bin_image->length,fi);
		 ::fclose(fi);
	 }
#endif

	 }

	 return DDL_SUCCESS;
}

/*********************************************************************
 *
 *	Name: eval_item_var
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_item_var will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_var_actions(), eval_attr_var_misc()
 *		eval_attr_string(), eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_attribute_item(
	::nsEDDEngine::ItemBase *flat,
	::nsEDDEngine::AttributeName name, 
	::nsEDDEngine::AttributeItem item,
	::nsEDDEngine::FLAT_MASKS *var_masks,	
	ENV_INFO       *env_info,
	nsEDDEngine::OP_REF         *var_needed,
	RETURN_LIST    *errors)
{
	int rc=DDL_SUCCESS;
	::nsEDDEngine::AttributeName tempName = nsEDDEngine::_end_;

	if(!item.attribute || !var_masks)
	{
		ASSERT(0);
		return DDS_INTERNAL_ERROR;
	}

	var_masks->attr_avail -= name;

	// map new depbin to old depbin
	DEPBIN db;
	DEPBIN *pdb=NULL;
	if(item.depbin)
	{
		pdb=&db;
		db.bin_chunk=item.depbin->bin_chunk;
		db.bin_size=item.depbin->bin_size;
		db.dep.count = 0;
		db.dep.limit = 0;
		db.dep.list = NULL;
	}

	FLAT_MASKS fm;

	// Temporary changed the AttributeName if requested attribute is count_ref, min_value_ref & max_value_ref
	// with count, min_value and max_value respectively to get the binary info

	switch(name)
	{
	case nsEDDEngine::count_ref:
		{
			name = nsEDDEngine::count;
			tempName = nsEDDEngine::count_ref;
		}
		break;
	case nsEDDEngine::min_value_ref:
		{
			name = nsEDDEngine::min_value;
			tempName = nsEDDEngine::min_value_ref;
		}
		break;

	case nsEDDEngine::max_value_ref:
		{
			name = nsEDDEngine::max_value;
			tempName = nsEDDEngine::max_value_ref;
		}
		break;

	case nsEDDEngine::image_id:
		{
			name = nsEDDEngine::image_table_index;
			tempName = nsEDDEngine::image_id;
		}
		break;
	}

	fm.attr_avail	= 0; 
	fm.bin_exists	= var_masks->bin_exists.isMember(name) ? (name ? name : ::nsEDDEngine::_end_) : 0; 
	fm.bin_hooked	= var_masks->bin_hooked.isMember(name) ? (name ? name : ::nsEDDEngine::_end_) : 0; 
	fm.dynamic		= var_masks->dynamic.isMember(name)	   ? (name ? name : ::nsEDDEngine::_end_) : 0;

	switch(tempName)
	{
	case nsEDDEngine::count_ref:
		{
			name = nsEDDEngine::count_ref;
		}
		break;
	case nsEDDEngine::min_value_ref:
		{
			name = nsEDDEngine::min_value_ref;
		}
		break;
	case nsEDDEngine::max_value_ref:
		{
			name = nsEDDEngine::max_value_ref;
		}
		break;
	case nsEDDEngine::image_id:
		{
			name = nsEDDEngine::image_id;
		}
		break;
	}

	// FD_FDI need to remove this check (skip missing attributes bins)
	if(!fm.bin_hooked)
	{
		rc = DDL_SUCCESS;
		unsigned long ulDefaultVal = 0; 
		switch(name)
		{
			
			// Handling Specifer Class Specifer (TAG, BITSTRING)
		case ::nsEDDEngine::handling:
			{
				// by default, an element without a HANDLING attribute can be read and written.
				ulDefaultVal = READ_HANDLING | WRITE_HANDLING; //3
				DDL_UINT*		bitstr = static_cast<DDL_UINT*>(item.attribute);
				*bitstr = ulDefaultVal;
				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::height:
			{
				switch(flat->item_type)
				{
				case nsEDDEngine::ITYPE_VARIABLE:
					ulDefaultVal = nsEDDEngine::DISPSIZE_XXX_SMALL;
					break;
				case nsEDDEngine::ITYPE_GRAPH:
				case nsEDDEngine::ITYPE_CHART:
				case nsEDDEngine::ITYPE_GRID:
					ulDefaultVal = nsEDDEngine::DISPSIZE_MEDIUM;
					break;
				default:
					break;
				}

				unsigned char *		byte = static_cast<unsigned char *>(item.attribute);
				*byte = (unsigned char)ulDefaultVal;
				
				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::width:
			{
				switch(flat->item_type)
				{
				case nsEDDEngine::ITYPE_VARIABLE:
					ulDefaultVal = nsEDDEngine::DISPSIZE_X_SMALL;
					break;
				case nsEDDEngine::ITYPE_GRAPH:
				case nsEDDEngine::ITYPE_CHART:
				case nsEDDEngine::ITYPE_GRID:
					ulDefaultVal = nsEDDEngine::DISPSIZE_MEDIUM;
					break;
				default:
					break;
				}

				unsigned char *		byte = static_cast<unsigned char *>(item.attribute);
				*byte = (unsigned char)ulDefaultVal;
				
				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::line_color:
			{
				//a LINE_COLOR attribute will be displayed using a default color 
				//defined by the EDD application.
				ulDefaultVal = LINE_COLOR_DEFAULT;

				ulong*		linecolor = static_cast<ulong*>(item.attribute);
				*linecolor = ulDefaultVal;
				
				var_masks->attr_avail += name;
			}
			break;
			// Line Type Specifier
		case ::nsEDDEngine::line_type: 
			{
				// by default line_type is DDS_LINE_TYPE_DATA_N;
				ulDefaultVal = DDS_LINE_TYPE_DATA_N;

				DDL_UINT*		linetype = static_cast<DDL_UINT*>(item.attribute);
				*linetype = ulDefaultVal;
				
				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::cycle_time: 
			{
				if(flat->item_type == nsEDDEngine::ITYPE_CHART)
				{
					ulDefaultVal = DEFAULT_REFRESH_CYCLE_TIME;

					if(IS_TYPE(ulong, item))
					{
						ulong *ul=CAST_CHECK(ulong, item);

						if(ul)
						{
							*ul=ulDefaultVal;

							var_masks->attr_avail += name;
						}
					}
					else
					{
						ASSERT(0);
						rc = DDS_INTERNAL_ERROR;
					}
				}
			}
			break;
		case ::nsEDDEngine::length:
			{
				ulDefaultVal = 600000;

				if(IS_TYPE(ulonglong, item))
				{
					ulonglong *ul=CAST_CHECK(ulonglong, item);
					if(ul)
					{
						*ul=ulDefaultVal;

						var_masks->attr_avail += name;
					}
				}
				else
				{
					ASSERT(0);
					rc = DDS_INTERNAL_ERROR;
				}
			}
			break;
		case ::nsEDDEngine::number_of_points:
			{
				ulDefaultVal = 1;
				
				if(IS_TYPE(ulong, item))
				{
					ulong *ul=CAST_CHECK(ulong, item);
					if(ul)
					{
						*ul=ulDefaultVal;

						var_masks->attr_avail += name;
					}
				}
				else
				{
					ASSERT(0);
					rc = DDS_INTERNAL_ERROR;
				}
			}
			break;
		case ::nsEDDEngine::validity: 
			{
				ulDefaultVal = 1;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::orientation:
			{
				ulDefaultVal = 1;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;
				
				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::visibility:
			{
				//The default value of VISIBILITY is TRUE
				ulDefaultVal = 1;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::shared:
			{
				// The default value of SHARED is FALSE
				ulDefaultVal = 0;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::emphasis:
			{
				//by By default, a user interface element without 
				//an EMPHASIS attribute should not be emphasized. i.e. False
				ulDefaultVal = 0;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::access:
			{
				//for menu default access attribute is UNKNOWN_ACCESS
				if(flat->item_type == nsEDDEngine::ITYPE_MENU)
				{
					ulDefaultVal =nsEDDEngine::Access::UNKNOWN_ACCESS;
				}
				//for method default access attribute is ONLINE_ACCESS
				else if(flat->item_type == nsEDDEngine::ITYPE_METHOD)
				{
					ulDefaultVal =nsEDDEngine::Access::ONLINE_ACCESS;
				}
				DDL_UINT* u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		case ::nsEDDEngine::private_attr:
			{
				//The default value of PRIVATE is FALSE
				ulDefaultVal = 0;
				DDL_UINT*		u_long = static_cast<DDL_UINT*>(item.attribute);
				*u_long = ulDefaultVal;

				var_masks->attr_avail += name;
			}
			break;
		default:
			break;
		}

		return rc;
	}
		

	switch(name)
	{
		// String Specifier STRING
	case ::nsEDDEngine::label					: 
	case ::nsEDDEngine::help					:
	case ::nsEDDEngine::display_format			: 
	case ::nsEDDEngine::edit_format				:
	case ::nsEDDEngine::constant_unit			:
	case ::nsEDDEngine::time_format				:
	case ::nsEDDEngine::component_path			:
	case ::nsEDDEngine::edd						:
	case ::nsEDDEngine::header					:
		{ 
			STRING s = {0};

			///////////////////////////////////////////////////////

			rc = eval_attr_string(name,
									&fm,
									eval_string,
									pdb,
									&s,
									env_info,
									errors,
									(unsigned long) DEFAULT_STD_DICT_LABEL);

			///////////////////////////////////////////////////////

			::nsEDDEngine::STRING *ns = CAST_CHECK(nsEDDEngine::STRING, item); 

			if(ns)
				::FDI_ConvSTRING::CopyFrom( &s, ns );

			ddl_free_string(&s);
		}
		break;
	case ::nsEDDEngine::product_uri:
	{
		STRING s = { 0 };
		// Thid is not ASCII string or string specifier. Just parse the chuck to get the string value.
		rc = ddl_parse_string(&pdb->bin_chunk, &pdb->bin_size, &s, &pdb->dep, env_info, var_needed);

		if (rc == DDL_SUCCESS)
		{
			var_masks->attr_avail += name;
			var_masks->bin_exists += name;
		}
		
		::nsEDDEngine::STRING *ns = CAST_CHECK(nsEDDEngine::STRING, item);

		if (ns)
			::FDI_ConvSTRING::CopyFrom(&s, ns);

		ddl_free_string(&s);
	}
	break;

		// Variable Type TYPE_SIZE
	case ::nsEDDEngine::variable_type			: 
		{
			TYPE_SIZE ts = {0};

			///////////////////////////////////////////////////////

			rc=item_mask_man(name,
									&fm,
									eval_attr_type,
									pdb,
									&ts,
									env_info,
									var_needed);

			///////////////////////////////////////////////////////

			::nsEDDEngine::TYPE_SIZE *nts = CAST_CHECK(nsEDDEngine::TYPE_SIZE, item); 

			if(nts)
				::FDI_ConvTYPE_SIZE::CopyFrom( &ts, nts );

		}
		break;

		// Handling Specifer Class Specifer (TAG, BITSTRING)
	case ::nsEDDEngine::class_attr				: 
	case ::nsEDDEngine::handling				: 
	case ::nsEDDEngine::write_mode				:
		{
			nsEDDEngine::ATTRIBUTE_DATA tg = {0};

			switch(name)
			{
			case::nsEDDEngine::class_attr:
				tg.tag=CLASS_TAG;
				tg.size=5;
				break;
			case ::nsEDDEngine::handling: 
				tg.tag=HANDLING_TAG;
				break;
			case ::nsEDDEngine::write_mode: 
				tg.tag=WRITE_MODE_TAG;
				break;
			}

			///////////////////////////////////////////////////////

			rc = item_mask_man(name,
										&fm,
										eval_attr_bitstring,
										pdb,
										item.attribute,
										env_info,
										var_needed,
										&tg);

			///////////////////////////////////////////////////////

			if (name == ::nsEDDEngine::class_attr)
			{

				::nsEDDEngine::ClassType *ct = CAST_CHECK(::nsEDDEngine::ClassType, item);

				if (*ct & ::nsEDDEngine::ClassType::CT_LOCAL_deprecated)
				{
					// Get the profile.
					int ddtype = get_ddtype(env_info);

					switch (ddtype)
					{
					case ::nsEDDEngine::PROFILE_HART:
					case ::nsEDDEngine::PROFILE_FF:
					case ::nsEDDEngine::PROFILE_ISA100:
					{
						*ct |= ::nsEDDEngine::ClassType::CT_LOCAL_A;
					}
					break;
					case ::nsEDDEngine::PROFILE_PB:
					case ::nsEDDEngine::PROFILE_PN:
					case ::nsEDDEngine::PROFILE_GPE:
					case ::nsEDDEngine::PROFILE_COMMSERVER:
					{
						*ct |= ::nsEDDEngine::ClassType::CT_LOCAL_B;
					}
					break;
					}
					// CT_LOCAL has been depricated and this will not be used anymore. but still tokenizers will set this bit and based on protocol 
					// EDD Engine will set LOCAL_A or LOCAL_B and clear the LOCAL bit. 
					*ct &= (::nsEDDEngine::ClassType)(~::nsEDDEngine::ClassType::CT_LOCAL_deprecated);
				}
			}
		}
		break;

		// Enumerations Specifier ENUM_VALUE_LIST
	case ::nsEDDEngine::enumerations			: 
		{
			ENUM_VALUE_LIST evl = {0};

			///////////////////////////////////////////////////////

			rc = item_mask_man(name,
								&fm,
								eval_attr_enum,
								pdb,
								&evl,
								env_info,
								var_needed);

			///////////////////////////////////////////////////////

			::nsEDDEngine::ENUM_VALUE_LIST *el = CAST_CHECK(nsEDDEngine::ENUM_VALUE_LIST, item); 

			if(el)
				::FDI_ConvENUM_VALUE_LIST::CopyFrom( &evl, el );

			ddl_free_enum_list(&evl, FREE_ATTR);
		}
		break;

	// Boolean Specifier BOOLEAN, UINT, Integer
	case ::nsEDDEngine::validity				: 
	case ::nsEDDEngine::emphasis				:
	case ::nsEDDEngine::can_delete				:
	case ::nsEDDEngine::visibility				: 
	case ::nsEDDEngine::access					:
	case ::nsEDDEngine::time_scale				: 
	case ::nsEDDEngine::scaling					:
	case ::nsEDDEngine::orientation				: 
	case ::nsEDDEngine::chart_type				: 
	case ::nsEDDEngine::image_table_index		:
	case ::nsEDDEngine::image_id				:
		{
			nsEDDEngine::ATTRIBUTE_DATA tg = {0};
			tg.tag=BOOLEAN_TAG;
			tg.size=1;

			switch(name)
			{
			case ::nsEDDEngine::orientation				:
				tg.tag=ORIENTATION_TAG;
				break;
			case ::nsEDDEngine::access					:
				tg.tag=ACCESS_TAG;
				break;
			case ::nsEDDEngine::time_scale				: 
				tg.tag=TIME_SCALE_TAG;
				tg.size=1;
				break;
			case ::nsEDDEngine::scaling					:
				tg.tag=AXIS_SCALING_TAG;
				tg.size=4;
				break;
			case ::nsEDDEngine::chart_type				: 
				tg.tag=CHART_TYPE_TAG;
				tg.size=1;
				break;
			case ::nsEDDEngine::number_of_points		:
				tg.tag=NO_TAG;
				tg.size=4;
				break;
			case ::nsEDDEngine::image_table_index		:
			case ::nsEDDEngine::image_id				:
				tg.tag=NO_TAG;
				tg.size=4;
				break;
			}

			if(name == ::nsEDDEngine::image_id)
			{
				ulong uImageId = 0;

				rc = item_mask_man(name,
						&fm,
						eval_attr_ulong,
						pdb,
						&uImageId,
						env_info,
						var_needed,
						&tg);

				if(rc==DDL_SUCCESS)
				{
					wchar_t strImageId[100]={0}; 
					PS_Itow(uImageId, strImageId, 10);

					::nsEDDEngine::FLAT_IMAGE *image=(::nsEDDEngine::FLAT_IMAGE *)flat;
					image->image_id = strImageId;
					
					var_masks->attr_avail += name;
					var_masks->bin_exists += name;
				}

			}
			else
			{
				///////////////////////////////////////////////////////
				rc = item_mask_man(name,
					&fm,
					eval_attr_ulong,
					pdb,
					item.attribute,
					env_info,
					var_needed,
					&tg);
				///////////////////////////////////////////////////////
			}
			if((rc==DDL_SUCCESS) && (name==nsEDDEngine::image_table_index))
			{
				::nsEDDEngine::FLAT_IMAGE *image=(::nsEDDEngine::FLAT_IMAGE *)flat;
				rc=get_image(env_info,image->image_table_index,&image->entry);

				wchar_t strImageId[100] = {0}; 
				PS_Itow(image->image_table_index, strImageId, 10);

				image->image_id = strImageId;
			}
		}
		break;

	// Response Codes Specifier RESPONSE_CODES
	case ::nsEDDEngine::response_codes			: 
		{
			RESPONSE_CODES response_codes = {0};

			///////////////////////////////////////////////////////

			rc = item_mask_man(name,
					&fm,
					eval_attr_response_codes,
					pdb,
					&response_codes,
					env_info,
					var_needed);

			///////////////////////////////////////////////////////

			if(IS_TYPE(nsEDDEngine::RESPONSE_CODE_LIST,item))
			{
				::nsEDDEngine::RESPONSE_CODE_LIST *rcs = CAST_CHECK(nsEDDEngine::RESPONSE_CODE_LIST, item); 
				if(rcs)
					::FDI_ConvRESPONSE_CODE_LIST::CopyFrom(&response_codes.response_codes.resp_code_list,rcs);
			}
			else if(IS_TYPE(nsEDDEngine::RESPONSE_CODES,item))
			{
				::nsEDDEngine::RESPONSE_CODES *rcs = CAST_CHECK(nsEDDEngine::RESPONSE_CODES, item); 
				if(rcs)
					::FDI_ConvRESPONSE_CODES::CopyFrom(&response_codes,rcs);
			}
			else
			{
				ASSERT(0);
				rc=DDS_INTERNAL_ERROR;
			}

			ddl_free_response_codes(&response_codes, FREE_ATTR);

		}
		break;

		// MinMax Value Specifier RANGE_DATA_LIST
	case ::nsEDDEngine::max_value				: 
	case ::nsEDDEngine::min_value				: 
	case ::nsEDDEngine::min_value_ref			: 
	case ::nsEDDEngine::max_value_ref			: 
		{
			if (flat->item_type == ::nsEDDEngine::ITYPE_AXIS)
			{
				::nsEDDEngine::FLAT_AXIS *axis = (::nsEDDEngine::FLAT_AXIS*)flat;

				switch(name)
				{
				case ::nsEDDEngine::min_value:
				case ::nsEDDEngine::min_value_ref:
					{
						// If a MIN_VALUE is only a simple reference, get it
						// otherwise leave it as zeros
						rc = get_simple_ref_from_MinMax(env_info, pdb, &axis->min_axis_ref);

						if(rc == DDL_SUCCESS && name == ::nsEDDEngine::min_value_ref)
						{
							var_masks->attr_avail += name;
							var_masks->bin_exists += name;
						}
						else
						{
							rc = DDL_SUCCESS;
						}
					}
					break;
				case ::nsEDDEngine::max_value:
				case ::nsEDDEngine::max_value_ref:
					{
						// If a MAX_VALUE is only a simple reference, get it
						// otherwise leave it as zeros
						rc = get_simple_ref_from_MinMax(env_info, pdb, &axis->max_axis_ref);

						if(rc == DDL_SUCCESS && name == ::nsEDDEngine::max_value_ref)
						{
							var_masks->attr_avail += name;
							var_masks->bin_exists += name;
						}
						else
						{
							rc = DDL_SUCCESS;
						}
					}
					break;
				default:
					break;	// Intentionally fall through
				}
			}

			if(name == ::nsEDDEngine::min_value || name == ::nsEDDEngine::max_value)
			{
				RANGE_DATA_LIST temp_rdl = {0};

				///////////////////////////////////////

				rc = item_mask_man(name,
					&fm,
					eval_attr_range_data_list,
					pdb,
					&temp_rdl,
					env_info,
					var_needed);	

				///////////////////////////////////////

				if (flat->item_type == nsEDDEngine::ITYPE_AXIS)		// AXIS only allows one MIN_VALUE and one MAX_VALUE
				{
					if (temp_rdl.count > 0)							// It is stored in an EXPR
					{
						::nsEDDEngine::EXPR *exp = CAST_CHECK(nsEDDEngine::EXPR, item);

						if (exp) {
							::FDI_ConvEXPR::CopyFrom(&temp_rdl.list[0], exp);	// Copy out the first one.
						}
					}
				}
				else
				{
					::nsEDDEngine::RANGE_DATA_LIST *rdl = CAST_CHECK(nsEDDEngine::RANGE_DATA_LIST, item); 

					if(rdl) {
						::FDI_ConvRANGE_DATA_LIST::CopyFrom( &temp_rdl, rdl );
					}
				}

				ddl_free_range_list(&temp_rdl, FREE_ATTR);
			}
		}
		break;
		// Expression & Expression Specifer (conditional & unconditional)
	case ::nsEDDEngine::scaling_factor			: 
	case ::nsEDDEngine::default_value			: 
	case ::nsEDDEngine::initial_value			: 
	case ::nsEDDEngine::cycle_time				: 
	case ::nsEDDEngine::length					:
	case ::nsEDDEngine::number_of_elements		: 
	case ::nsEDDEngine::redundancy				: 
	case ::nsEDDEngine::x_initial				: 
	case ::nsEDDEngine::x_increment				: 
	case ::nsEDDEngine::number_of_points		:
	case ::nsEDDEngine::api						: 
		{
			EXPR expr = {0};
			////////////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_expr,
				pdb,
				&expr,
				env_info,
				var_needed);

			////////////////////////////////////////

			
			if(IS_TYPE(ulong, item))
			{
				ulong *ul=CAST_CHECK(ulong, item);
				if(ul)
				{
					switch(expr.type)
					{
					case INTEGER:
						*ul= (ulong)expr.val.i;
						break;

					case UNSIGNED:
						*ul= (ulong)expr.val.u;
						break;

                    case FLOATG_PT:
						*ul= (ulong)expr.val.f;
						break;

                    case DOUBLEG_PT:
						*ul= (ulong)expr.val.d;
						break;

					default:
						//*ul= expr.val.u;
						break;
					}
				}
					
			}
			else if(IS_TYPE(ulonglong, item))
			{
				ulonglong *ul=CAST_CHECK(ulonglong, item);
				if(ul)
				{
					switch(expr.type)
					{
					case INTEGER:
						*ul= expr.val.i;
						break;

					case UNSIGNED:
						*ul= expr.val.u;
						break;

                    case FLOATG_PT:
						*ul= (ulong)expr.val.f;
						break;

                    case DOUBLEG_PT:
						*ul= (ulong)expr.val.d;
						break;

					default:
						//*ul= expr.val.u;
						break;
					}
				}
					
			}
			else if(IS_TYPE(nsEDDEngine::EXPR, item))
			{
				::nsEDDEngine::EXPR *e=CAST_CHECK(::nsEDDEngine::EXPR, item);

				if(e)
					::FDI_ConvEXPR::CopyFrom(&expr,e);
			}
			else
			{
				ASSERT(0);
				rc=DDS_INTERNAL_ERROR;
			}

			::ddl_free_expr(&expr);

		}
		break;

		// Expression Specifier Integer / Integer Specifier
	case ::nsEDDEngine::slot					: 
	case ::nsEDDEngine::sub_slot				: 
	case ::nsEDDEngine::index					: 
	case ::nsEDDEngine::line_color				: 
	case ::nsEDDEngine::count					: 
	case ::nsEDDEngine::block_number			:
		{

			ulong *t=CAST_CHECK(ulong, item);
			t=t;
			///////////////////////////////////

			if ((flat->item_type == ::nsEDDEngine::ITYPE_LIST) && (name == ::nsEDDEngine::count))
			{
				::nsEDDEngine::FLAT_LIST *list = (::nsEDDEngine::FLAT_LIST*)flat;
				// If a COUNT is only a reference, get it
				// otherwise leave it as zeros
				rc = get_simple_ref_from_Expression(env_info, pdb, &list->count_ref);

				if(rc == DDL_SUCCESS)
				{
					var_masks->bin_exists += ::nsEDDEngine::count_ref;
					var_masks->attr_avail += ::nsEDDEngine::count_ref;
				}
			}

	        rc = item_mask_man(name, 
					&fm,
					eval_attr_int_expr,
					pdb,
					item.attribute, 
					env_info, 
					var_needed);

			///////////////////////////////////
		}
		break;
	case ::nsEDDEngine::count_ref				:
		{
			::nsEDDEngine::FLAT_LIST *list = (::nsEDDEngine::FLAT_LIST*)flat;
			// If a COUNT is only a reference, get it
			// otherwise leave it as zeros
			rc = get_simple_ref_from_Expression(env_info, pdb, &list->count_ref);

			if(rc == DDL_SUCCESS)
			{
				var_masks->attr_avail += name;
				var_masks->bin_exists += name;
			}
			else
			{
				rc = DDL_SUCCESS;
			}
		}
		break;

		// Action List Specifier ACTION_LIST
	case ::nsEDDEngine::post_read_actions		: 
	case ::nsEDDEngine::post_write_actions		: 
	case ::nsEDDEngine::pre_read_actions		: 
	case ::nsEDDEngine::pre_write_actions		: 
	case ::nsEDDEngine::post_edit_actions		: 
	case ::nsEDDEngine::pre_edit_actions		: 
	case ::nsEDDEngine::refresh_actions			: 
	case ::nsEDDEngine::post_user_actions		: 
	case ::nsEDDEngine::init_actions			: 
	case ::nsEDDEngine::exit_actions			: 
		{
			ACTION_LIST action_list = {0};

			////////////////////////////////////////

			rc = eval_attr_actions(&fm,
						name, 
						env_info,
						errors, 
						&action_list, 
						pdb);

			////////////////////////////////////////
			::nsEDDEngine::ACTION_LIST *al=CAST_CHECK(::nsEDDEngine::ACTION_LIST, item);

			if(al)
				::FDI_ConvACTION_LIST::CopyFrom( &action_list, al );

			ddl_free_actions(&action_list, FREE_ATTR);
		}
		break;

		// BYTE (Size 1) (unconditional)
	case ::nsEDDEngine::style					: 
	case ::nsEDDEngine::height					: 
	case ::nsEDDEngine::width					: 
	case ::nsEDDEngine::waveform_type			: 
	case ::nsEDDEngine::private_attr			: 
	case ::nsEDDEngine::shared					:
	case ::nsEDDEngine::classification			:	
	case ::nsEDDEngine::component_byte_order	:
	case ::nsEDDEngine::protocol				:
	case ::nsEDDEngine::relation_type			: 
	case ::nsEDDEngine::operation				: 
	case ::nsEDDEngine::block_b_type			:
	{
			nsEDDEngine::ATTRIBUTE_DATA tg={0};

			switch(name)
			{
			case ::nsEDDEngine::shared: 
			case ::nsEDDEngine::private_attr: 
				tg.tag=BOOLEAN_TAG;
				tg.size=1;
				break;			
			case ::nsEDDEngine::block_b_type: 
				tg.tag=BLOCK_B_TAG;
				tg.size=1;
				break;
			case ::nsEDDEngine::component_byte_order:
				tg.tag = BYTE_ORDER_TAG;
				tg.size = 1;
				break;
			}

			///////////////////////////////////////////////////////

			// this one is not conditional
			rc = item_mask_man(name,
						&fm,
						eval_byte,
						pdb,
						item.attribute,
						env_info,
						var_needed,
						(tg.tag) ? & tg: NULL);
						
			///////////////////////////////////////////////////////
		}
		break;

		// Reference List Specifier
	case ::nsEDDEngine::display_items			:
	case ::nsEDDEngine::edit_items				:
	case ::nsEDDEngine::relation_update_list	:
	case ::nsEDDEngine::relation_watch_list		:
	case ::nsEDDEngine::component_relations		:
	{
			OP_REF_TRAIL_LIST list = {0};

			///////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_op_ref_trail_list,
				pdb,
				&list,
				env_info,
				var_needed);

			///////////////////////////////////

			nsEDDEngine::OP_REF_TRAIL_LIST *t=CAST_CHECK(nsEDDEngine::OP_REF_TRAIL_LIST, item);

			if(t)
				::FDI_ConvOP_REF_TRAIL_LIST::CopyFrom( &list, t );

			ddl_free_op_ref_trail_list(&list, FREE_ATTR);

		}
		break;

	case ::nsEDDEngine::addressing:
	{
		OP_REF_TRAIL_LIST list = { 0 };

		rc = ddl_parse_op_ref_trail_list(&pdb->bin_chunk, &pdb->bin_size, &list, &pdb->dep, env_info, var_needed);
			nsEDDEngine::OP_REF_TRAIL_LIST *t=CAST_CHECK(nsEDDEngine::OP_REF_TRAIL_LIST, item);

			if(t)
				::FDI_ConvOP_REF_TRAIL_LIST::CopyFrom( &list, t );

			ddl_free_op_ref_trail_list(&list, FREE_ATTR);

		}
		break;

		// Line Type Specifier
	case ::nsEDDEngine::line_type				: 
		{

			///////////////////////////////////
			rc = item_mask_man(name, 
				&fm,
				eval_attr_line_type,
				pdb,
				item.attribute, 
				env_info, 
				var_needed);
			///////////////////////////////////
		}
		break;

		// Reference & Reference Specifier (conditional OR unconditional)
	case ::nsEDDEngine::type_definition			:// ITEM_ID

	case ::nsEDDEngine::indexed					:// ITEM_ID conditional
	case ::nsEDDEngine::x_axis					:// ITEM_ID conditional
	case ::nsEDDEngine::y_axis					:// ITEM_ID conditional
	case ::nsEDDEngine::block_b					:// ITEM_ID conditional
	case ::nsEDDEngine::characteristics			:// ITEM_ID conditional

	case ::nsEDDEngine::detect					:// DESC_REF
	case ::nsEDDEngine::scan					:// DESC_REF
	case ::nsEDDEngine::scan_list				:// DESC_REF
	case ::nsEDDEngine::component_connect_point	:// DESC_REF
	case ::nsEDDEngine::component_parent		:// DESC_REF
	case ::nsEDDEngine::check_configuration		:// DESC_REF 

	case ::nsEDDEngine::link					:// DESC_REF conditional

	case ::nsEDDEngine::relation_watch_variable	:// OP_REF_TRAIL 
		{
			OP_REF_TRAIL var = {STANDARD_TYPE, 0};

			/////////////////////////////////////////////

			rc = item_mask_man(name, 
				&fm,
				eval_op_ref_trail,
				pdb,
				&var, 
				env_info, 
				var_needed);

			/////////////////////////////////////////////

			if(IS_TYPE(ITEM_ID,item))
			{
				ITEM_ID *id=CAST_CHECK(ITEM_ID, item);

				//Not finished - check out
				*id=var.op_info.id;
			}
			else if(IS_TYPE(::nsEDDEngine::DESC_REF,item))
			{
				::nsEDDEngine::DESC_REF *desc_ref=CAST_CHECK(::nsEDDEngine::DESC_REF, item);

				desc_ref->id=var.desc_id;
				desc_ref->type=(nsEDDEngine::ITEM_TYPE)var.desc_type;
			}
			else
			{
				::nsEDDEngine::OP_REF_TRAIL *ort=CAST_CHECK(::nsEDDEngine::OP_REF_TRAIL, item);

				if(ort)
					::FDI_ConvOP_REF_TRAIL::CopyFrom(&var,ort);
			}

			::ddl_free_op_ref_trail(&var);
		}
		break;

		// Reference List Specifier
	case ::nsEDDEngine::axis_items				: 
	case ::nsEDDEngine::chart_items				: 
	case ::nsEDDEngine::collection_items		: 
	case ::nsEDDEngine::edit_display_items		: 
	case ::nsEDDEngine::file_items				: 
	case ::nsEDDEngine::graph_items				: 
	case ::nsEDDEngine::grid_items				: 
	case ::nsEDDEngine::image_items				: 
	case ::nsEDDEngine::list_items				: 
	case ::nsEDDEngine::menu_items				: 
	case ::nsEDDEngine::method_items			: 
	case ::nsEDDEngine::reference_array_items	: 
	case ::nsEDDEngine::refresh_items			: 
	case ::nsEDDEngine::source_items			: 
	case ::nsEDDEngine::unit_items				: 
	case ::nsEDDEngine::waveform_items			: 
	case ::nsEDDEngine::write_as_one_items		: 
	case ::nsEDDEngine::plugin_items			: 
		{
			ITEM_ID_LIST list = {0}; 

			///////////////////////////////////
			rc = eval_attr_reflist(&fm, 
				name, 
				env_info,
				errors, 
				&list, 
				pdb);

			/////////////////////////////////
			::nsEDDEngine::ITEM_ID_LIST *il=CAST_CHECK(::nsEDDEngine::ITEM_ID_LIST, item);

			if(il)
				::FDI_ConvITEM_ID_LIST::CopyFrom(&list, il);

			ddl_free_reflist(&list, FREE_ATTR);

		}
		break;

		// Reference List (DATA_ITEM_LIST)
	case ::nsEDDEngine::keypoints_x_values		: 
	case ::nsEDDEngine::keypoints_y_values		: 
	case ::nsEDDEngine::x_values				: 
	case ::nsEDDEngine::y_values				:
		{
			DATA_ITEM_LIST list = {0}; 
			///////////////////////////////////
			rc = eval_attr_data_reflist(&fm, 
				name, 
				env_info,
				errors, 
				&list, 
				pdb);

			/////////////////////////////////
			::nsEDDEngine::DATA_ITEM_LIST *dl=CAST_CHECK(::nsEDDEngine::DATA_ITEM_LIST, item);

			if(dl)
				::FDI_ConvDATA_ITEM_LIST::CopyFrom( &list, dl );

			ddl_free_dataitems(&list, FREE_ATTR);

		}
		break;

		// Member List Specifier, Members List Specifier
	case ::nsEDDEngine::charts				: 
	case ::nsEDDEngine::parameters			: 
	case ::nsEDDEngine::graphs				: 
	case ::nsEDDEngine::grids				: 
	case ::nsEDDEngine::lists				: 
	case ::nsEDDEngine::local_parameters	: 
	case ::nsEDDEngine::menus				: 
	case ::nsEDDEngine::methods				: 
	case ::nsEDDEngine::parameter_lists		: 
	case ::nsEDDEngine::members				:
	case ::nsEDDEngine::files				:
	case ::nsEDDEngine::plugins				:
		{
			if (IS_TYPE(::nsEDDEngine::MEMBER_LIST, item))
			{
				MEMBER_LIST list = {0};
				/////////////////////////////////////////////
				rc = item_mask_man(name,
					&fm,
					eval_attr_members,
					pdb,
					&list,
					env_info,
					var_needed);

				/////////////////////////////////////////////
				::nsEDDEngine::MEMBER_LIST *ml=CAST_CHECK(::nsEDDEngine::MEMBER_LIST, item);

				if(ml) {
					::FDI_ConvMEMBER_LIST::CopyFrom( &list, ml );
				}

				ddl_free_members_list(&list, FREE_ATTR);
			}
			else if (IS_TYPE(::nsEDDEngine::OP_MEMBER_LIST, item))
			{
				OP_MEMBER_LIST op_list = {0};
				/////////////////////////////////////////////
				rc = item_mask_man(name,
				&fm,
				eval_attr_op_members,
				pdb,
				&op_list,
				env_info,
				var_needed);

				::nsEDDEngine::OP_MEMBER_LIST *oml=CAST_CHECK(::nsEDDEngine::OP_MEMBER_LIST, item);
			
				if(oml) {
					::FDI_ConvOP_MEMBER_LIST::CopyFrom( &op_list, oml );
				}

				ddl_free_op_members_list(&op_list, FREE_ATTR);
			}
			else
			{
				ASSERT(0);
			}
		}
		break;

		// Menu Item Specifier
	case ::nsEDDEngine::items					: 
		{
			MENU_ITEM_LIST list = {0};

			//////////////////////////////////////////////
			rc = item_mask_man(name,
				&fm,
				eval_attr_menuitems,
				pdb,
				&list,
				env_info,
				var_needed);
			////////////////////////////////////////////////

			::nsEDDEngine::MENU_ITEM_LIST *new_item_list=CAST_CHECK(::nsEDDEngine::MENU_ITEM_LIST, item);

			if(new_item_list)
				::FDI_ConvMENU_ITEM_LIST::CopyFrom( &list, new_item_list );

			ddl_free_menuitems_list(&list, FREE_ATTR);

		}
		break;

		// INTEGER (unconditional)
	case ::nsEDDEngine::device_revision			: 
	case ::nsEDDEngine::device_type				: 
	case ::nsEDDEngine::manufacturer			: 
	case ::nsEDDEngine::maximum_number			: 
	case ::nsEDDEngine::minimum_number			: 
	case ::nsEDDEngine::command_number			:
	case ::nsEDDEngine::capacity				:
	case ::nsEDDEngine::declaration				:
	case ::nsEDDEngine::method_type				:
		{
//			ulong *i=CAST_CHECK(ulong,item);

			////////////////////////////////////
			rc = item_mask_man(name,
				&fm,
				eval_attr_parse_integer,
				pdb,
				item.attribute,
				env_info,
				var_needed);
			////////////////////////////////////
		}
		break;

		// ASCII_String
	case ::nsEDDEngine::identity				: 
		{
			::nsEDDEngine::STRING *ns = CAST_CHECK(nsEDDEngine::STRING, item); 

			rc=item_mask_man(name,
				&fm,
				eval_attr_ascii_string,
				pdb,
				ns,
				env_info,
				var_needed);
		}
		break;

		// Elements Specifier
	case ::nsEDDEngine::elements				:
		{
			ITEM_ARRAY_ELEMENT_LIST list = {0};

			//////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_itemarray,
				pdb,
				&list,
				env_info,
				var_needed);

			/////////////////////////

			::nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST *new_item_list=CAST_CHECK(::nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST, item);

			if(new_item_list)
				::FDI_ConvITEM_ARRAY_ELEMENT_LIST::CopyFrom( &list, new_item_list );

			ddl_free_itemarray_list(&list, FREE_ATTR);
		}
		break;

	case ::nsEDDEngine::vectors					: 
		{
			VECTOR_LIST list={0};

			/////////////////////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_vectors,
				pdb,
				&list,
				env_info,
				var_needed);

			/////////////////////////////////////////////////

			::nsEDDEngine::VECTOR_LIST *new_vector_list=CAST_CHECK(::nsEDDEngine::VECTOR_LIST, item);

			if(new_vector_list)
				::FDI_ConvVECTOR_LIST::CopyFrom( &list, new_vector_list );

			ddl_free_vectors_list(&list, FREE_ATTR);

		}
		break;

		// BYTE_STRING .. encoded
	case ::nsEDDEngine::definition				: 
		{
			DEFINITION def = {0};

			/////////////////////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_definition,
				pdb,
				&def,
				env_info,
				var_needed);

			/////////////////////////////////////////////////

			::nsEDDEngine::DEFINITION *new_def=CAST_CHECK(::nsEDDEngine::DEFINITION, item);

			if(new_def)
				::FDI_ConvDEFINITION::CopyFrom( &def, new_def );

			if(def.data)
				free(def.data);
		}

		break;

	case ::nsEDDEngine::method_parameters		: 
		{

			STRING s = {0};

			///////////////////////////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_params,
				pdb,
				&s,
				env_info,
				var_needed);

			///////////////////////////////////////////////////////

			::nsEDDEngine::STRING *ns = CAST_CHECK(nsEDDEngine::STRING, item); 

			if(ns)
				::FDI_ConvSTRING::CopyFrom(&s, ns);

			ddl_free_string(&s);
		}
		break;

	case ::nsEDDEngine::initial_values			: 
	case ::nsEDDEngine::default_values			: 
		{

		nsEDDEngine::DEFAULT_VALUES_LIST *dl=CAST_CHECK(nsEDDEngine::DEFAULT_VALUES_LIST, item);
		dl=dl;
		///////////////////////////////////////////////////////

		rc = item_mask_man(name,
			&fm,
			eval_attr_default_values,
			pdb,
			item.attribute,
			env_info,
			var_needed);

		///////////////////////////////////////////////////////
		}
		break;

	case ::nsEDDEngine::item_information		: 
		{
		nsEDDEngine::ITEM_INFORMATION *dl=CAST_CHECK(nsEDDEngine::ITEM_INFORMATION, item);
		dl=dl;
		////////////////////////////////////
		rc = item_mask_man(::nsEDDEngine::_end_,
			&fm,
			eval_attr_parse_item_information,
			pdb,
			item.attribute,
			env_info,
			var_needed);
		////////////////////////////////////
		}
		break;

	case ::nsEDDEngine::uuid:
		{
		GUID *dl=CAST_CHECK(GUID, item);
		dl=dl;
		////////////////////////////////////
		rc = item_mask_man(name,
			&fm,
			eval_attr_parse_uuid,
			pdb,
			item.attribute,
			env_info,
			var_needed);
		////////////////////////////////////
		}
		break;

	case ::nsEDDEngine::components				: 
	{
		COMPONENT_SPECIFIER_LIST list = { 0 };

		//////////////////////////////////////////////
		rc = item_mask_man(name,
			&fm,
			eval_attr_components,
			pdb,
			&list,
			env_info,
			var_needed);
		////////////////////////////////////////////////

		::nsEDDEngine::COMPONENT_SPECIFIER_LIST *new_item_list = CAST_CHECK(::nsEDDEngine::COMPONENT_SPECIFIER_LIST, item);

		if (new_item_list)
			::FDI_ConvCOMPONENT_SPECIFIER_LIST::CopyFrom(&list, new_item_list);

		ddl_free_components_list(&list, FREE_ATTR);

	}
		break;

	case ::nsEDDEngine::transaction				: 
		{
			TRANSACTION_LIST trans = {0};

			////////////////////////////////////

			rc = item_mask_man(name,
				&fm,
				eval_attr_transaction_list,
				pdb,
				&trans,
				env_info,
				var_needed);

			////////////////////////////////////

			nsEDDEngine::TRANSACTION_LIST *tl = CAST_CHECK(nsEDDEngine::TRANSACTION_LIST, item); 

			if(tl)
				::FDI_ConvTRANSACTION_LIST::CopyFrom(&trans,tl);

			ddl_free_trans_list(&trans, FREE_ATTR);

		}
		break;

	case ::nsEDDEngine::appinstance				: 
	case ::nsEDDEngine::on_update_actions		: 
	case ::nsEDDEngine::post_rqst_actions		: 
		PS_TRACE(_T("**** ATTRIBUTE NOT FOUND"));
		return DDL_ENCODING_ERROR;
		break;

	case ::nsEDDEngine::first					: 
	case ::nsEDDEngine::last					: 
	case ::nsEDDEngine::view_min				: 
	case ::nsEDDEngine::view_max				: 
	case ::nsEDDEngine::_end_					: 
		ASSERT(0);
		PS_TRACE(_T("**** ATTRIBUTE NOT FOUND"));
		break;

	default:
		ASSERT(0);
		PS_TRACE(_T("**** ATTRIBUTE NOT FOUND"));
		return DDS_INTERNAL_ERROR;
	}

	if(fm.dynamic)
	{
		var_masks->dynamic |= name;
	}

	if(rc==DDL_DEFAULT_ATTR || fm.attr_avail)
	{
		var_masks->attr_avail += name;
	}

	// Assign the depinfo to item.
	if(item.depbin)
	{
		if(pdb->dep.count > 0)
		{
			append_and_free_depinfo(&item.depbin->dep, &pdb->dep, 0); 
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: get_default_format(::nsEDDEngine::TYPE_SIZE typeNsize, bool get_for_edit, ::nsEDDEngine::STRING *string)
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		get_default_format will use type and get_for_edit to set the format string for the
 *      requested variable type and edit/display mode.
 *
 *	Inputs:
 *		typeNsize:		Variable type and size to key default format string off of
 *		get_for_edit:   Specifies that this is for edit format not display format
 *		string:         The STRING object that contains the format (should be initialized to empty)
 *
 *	Outputs:
 *		string:			Format for valid variables of blank string for undefined formats
 *
 *	Returns:
 *		void
 *
 *	Author: Mark Sandmann
 *
 **********************************************************************/

static void get_default_format(::nsEDDEngine::TYPE_SIZE typeNsize, bool get_for_edit, ::nsEDDEngine::STRING *string)
{
    // The string was already created and contains nothing on entry
    *string = L"";  // nothing for default format

	switch(typeNsize.type)
    {
    case ::nsEDDEngine::VariableType::VT_INTEGER:
		if (typeNsize.size <= 4)
		{
			*string = L"d";
		}
		else
		{
			*string = L"lld";
		}
        break;
    case ::nsEDDEngine::VariableType::VT_UNSIGNED:
		if (typeNsize.size <= 4)
		{
			*string = L"u";
		}
		else
		{
			*string = L"llu";
		}
        break;
    case ::nsEDDEngine::VariableType::VT_FLOAT:
        if (get_for_edit)
        {
            *string = L"12.5f";
        }
        else
        {
            *string = L"12.5g";
        }
        break;
    case ::nsEDDEngine::VariableType::VT_DOUBLE:
        if (get_for_edit)
        {
            *string = L"18.8f";
        }
        else
        {
            *string = L"18.8g";
        }
        break;
    case ::nsEDDEngine::VariableType::VT_ENUMERATED:
    case ::nsEDDEngine::VariableType::VT_INDEX:
    case ::nsEDDEngine::VariableType::VT_BIT_ENUMERATED:
    case ::nsEDDEngine::VariableType::VT_ASCII:
    case ::nsEDDEngine::VariableType::VT_PACKED_ASCII:
    case ::nsEDDEngine::VariableType::VT_PASSWORD:
    case ::nsEDDEngine::VariableType::VT_BITSTRING:
    case ::nsEDDEngine::VariableType::VT_EDD_DATE:
    case ::nsEDDEngine::VariableType::VT_TIME:
    case ::nsEDDEngine::VariableType::VT_DATE_AND_TIME:
    case ::nsEDDEngine::VariableType::VT_DURATION:
    case ::nsEDDEngine::VariableType::VT_EUC:
    case ::nsEDDEngine::VariableType::VT_OCTETSTRING:
    case ::nsEDDEngine::VariableType::VT_VISIBLESTRING:
    case ::nsEDDEngine::VariableType::VT_TIME_VALUE:
    case ::nsEDDEngine::VariableType::VT_OBJECT_REFERENCE:
    case ::nsEDDEngine::VariableType::VT_BOOLEAN:
        break;
    default:
        ASSERT(0);
    }
    //string->len = (unsigned short) wcslen(string->str);
}

/*********************************************************************
 *
 *	Name: get_default_scaling_factor(::nsEDDEngine::VariableType type, ::nsEDDEngine::EXPR* pExpr, ::nsEDDEngine::AttributeNameSet *pAttrAvail)
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		get_default_scaling_factor will use type to set the default scaling factor and available attribute mask.
 *
 *	Inputs:
 *		type:			Variable type to key default scaling factor
 *
 *	Outputs:
 *		pExpr:			pointer to EXPR class
 *		pAttrAvail:		pointer to AttributeNameSet class
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

static void get_default_scaling_factor(::nsEDDEngine::VariableType type, ::nsEDDEngine::EXPR* pExpr, ::nsEDDEngine::AttributeNameSet *pAttrAvail)
{
	switch (type)
	{
	case ::nsEDDEngine::VariableType::VT_INTEGER:
	case ::nsEDDEngine::VariableType::VT_UNSIGNED:
	case ::nsEDDEngine::VariableType::VT_FLOAT:
	case ::nsEDDEngine::VariableType::VT_DOUBLE:
		pExpr->eType = ::nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_INTEGER;
		pExpr->val.i = 1;
		*pAttrAvail += ::nsEDDEngine::scaling_factor;
	break;

	case ::nsEDDEngine::VariableType::VT_ENUMERATED:
	case ::nsEDDEngine::VariableType::VT_INDEX:
	case ::nsEDDEngine::VariableType::VT_BIT_ENUMERATED:
	case ::nsEDDEngine::VariableType::VT_ASCII:
	case ::nsEDDEngine::VariableType::VT_PACKED_ASCII:
	case ::nsEDDEngine::VariableType::VT_PASSWORD:
	case ::nsEDDEngine::VariableType::VT_BITSTRING:
	case ::nsEDDEngine::VariableType::VT_EDD_DATE:
	case ::nsEDDEngine::VariableType::VT_TIME:
	case ::nsEDDEngine::VariableType::VT_DATE_AND_TIME:
	case ::nsEDDEngine::VariableType::VT_DURATION:
	case ::nsEDDEngine::VariableType::VT_EUC:
	case ::nsEDDEngine::VariableType::VT_OCTETSTRING:
	case ::nsEDDEngine::VariableType::VT_VISIBLESTRING:
	case ::nsEDDEngine::VariableType::VT_TIME_VALUE:
	case ::nsEDDEngine::VariableType::VT_OBJECT_REFERENCE:
	case ::nsEDDEngine::VariableType::VT_BOOLEAN:
		break;
	default:
		ASSERT(0);
	}
}

/*********************************************************************
 *
 *	Name: get_default_numberOfPoints(::nsEDDEngine::WaveformType type, ::nsEDDEngine::DATA_ITEM_LIST *pXValues, ::nsEDDEngine::DATA_ITEM_LIST *pYValues, ulong &ulNumber)
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		get_default_numberOfPoints will use waveform type to set the default scaling factor.
 *
 *	Inputs:
 *		type:			waveform line type
 *		pXValues:		X values pointer to DATA_ITEM_LIST class
 *		pYValues:		y values pointer to DATA_ITEM_LIST class
 *
 *	Outputs:
 *		ulNumber		number of points of waveform
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

static void get_default_numberOfPoints(::nsEDDEngine::WaveformType type, ::nsEDDEngine::DATA_ITEM_LIST *pXValues, ::nsEDDEngine::DATA_ITEM_LIST *pYValues, ulong &ulNumber)
{
	switch (type)
	{
	case ::nsEDDEngine::FDI_XY_WAVEFORM_TYPE:
		if (pXValues->count < pYValues->count)
		{
			ulNumber = pXValues->count;
		}
		else
		{
			ulNumber = pYValues->count;
		}
		break;
	case ::nsEDDEngine::FDI_YT_WAVEFORM_TYPE:
		ulNumber = pYValues->count;
		break;
	default:
		//no other case needs to be done here 
		break;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_any
 *	ShortDesc: load the item
 *
 *	Description:
 *		loads flat items attributes
 *
 *	Inputs:
 *		var:		flat to evaluate
 *		mask:		desired attributes
 *		env_info:	upcall enviro data
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		error
 *
 *
 **********************************************************************/
int
eval_item_any(
::nsEDDEngine::ItemBase *var,
::nsEDDEngine::AttributeNameSet mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{
	int             rc;	/* return code */
	nsEDDEngine::OP_REF          var_needed;	/* temp storage for var_needed */
	::nsEDDEngine::FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */

	if(!var)
	{
#ifdef __LOG_ATTRIBUTES__
		{
			USES_CONVERSION;
            wchar_t buf[1000]{0};
			PS_OutputDebugString(_T("\n\n\nAttributes Decoded\n===========================================\n"));

			int ddtype=get_ddtype(env_info);

			int protocol=0;

			switch(ddtype)
			{
			case ::nsEDDEngine::PROFILE_HART:
				protocol=nsEDDEngine::ItemBase::ProtocolHART;
				break;
			case ::nsEDDEngine::PROFILE_PB:
				protocol=nsEDDEngine::ItemBase::ProtocolPB;
				break;
			case ::nsEDDEngine::PROFILE_FF:
				protocol=nsEDDEngine::ItemBase::ProtocolFF;
				break;
			default:
				protocol=nsEDDEngine::ItemBase::ProtocolAll;
				break;
			}

			int cnt=0;
			int exists=0;
			int item_count=0;
			for(std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeDataItem>::iterator attribute_item=attribute_map.begin();
				attribute_item!=attribute_map.end(); 
				attribute_item++)
			{
				::nsEDDEngine::AttributeName name=(*attribute_item).first; 
				::nsEDDEngine::AttributeDataItem item=(*attribute_item).second;

				if(item.attribute_data.protocol&protocol || item.exists) 
				{
					wchar_t* attrStr=A2W(attribute_names[(int)name]);

					item_count++;

					if(!(item.attribute_data.protocol&protocol))
					{
						wsprintf(buf,_T("*** invalid attribute for protocol -> "));
						PS_OutputDebugString(buf);
					}

					wsprintf(buf,_T("%s%s%s (%d) %-25s - exists %d, oks %d, errs %d\n"), (item.exists==0) ? _T("x"):_T("_"), (item.errors) ? _T("e") : _T("_"), (item.oks) ? _T("k") : _T("_"), cnt, attrStr, item.exists, item.oks, item.errors);
					PS_OutputDebugString(buf);

					if(item.exists)
						exists++;
				}

				cnt++;
			}

			wsprintf(buf,_T("\ntotal errors = %d\ntotal expected errors = %d\nactual errors = %d\n"), g_totalErrors,g_totalExpectedErrors,g_totalErrors-g_totalExpectedErrors);
			PS_OutputDebugString(buf);
			wsprintf(buf,_T("\ncoverage = %d%% ( exists/item_count = %d/%d )\n\n\n"), (int) ((exists*100)/item_count), exists, item_count);
			
			PS_OutputDebugString(buf);
		}
#endif	// __LOG_ATTRIBUTES__
		return E_FAIL;
	}


	::nsEDDEngine::ItemBase *flat=(::nsEDDEngine::ItemBase *)var;
	var_masks=&flat->masks;
	
	std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeItem> items;
	flat->GetDepbinMap(items);

#ifdef __LOG_ATTRIBUTES__
	if(!attribute_map.size())
	{
		flat->GetAttributeDataMap(attribute_map);
	}
#endif

#ifdef __LOG_ITEM_INFO__
    wchar_t indent[1000]{0};
	wsprintf(indent,_T(""));

	{
		if(g_indentLevel>0)
		{
			PS_OutputDebugString(_T("\n"));
			wsprintf(indent,_T("       >>>%d "),g_indentLevel);
		}

		g_indentLevel++;
		USES_CONVERSION;

		wchar_t* str=A2W(flat->GetTypeName());
        wchar_t buf[1000]{0};

		::nsEDDEngine::SYMBOL_TBL_ELEM *sym=get_symbol_tbl_lookup(env_info->block_handle, env_info, flat->id);
		wchar_t* symbol=_T("<symbol not found>");
		if(sym && sym->name)
			symbol=sym->name;

		wsprintf(buf,_T("%s(%d) decoding %-25s - item = %x, type = %s"), indent, g_totalItems, symbol, flat->id, str);
		g_totalItems++;
		PS_OutputDebugString(buf);
	}
#endif	// __LOG_ITEM_INFO__
    int cnt,errs,oks,exists;
	cnt=errs=oks=exists=0;


//	ASSERT(flat->id!=0x1b5a);

	for(std::map<::nsEDDEngine::AttributeName, ::nsEDDEngine::AttributeItem>::iterator attribute_item=items.begin();
		attribute_item!=items.end(); 
		attribute_item++)
	{
		cnt++;

		::nsEDDEngine::AttributeName name=(*attribute_item).first; 

#ifdef __LOG_ITEM_INFO__
		::nsEDDEngine::AttributeItem item=(*attribute_item).second;
#endif

		// if mask is empty return all items
		if(mask.size())
		{
			if( !mask.isMember(name) )	// If this attribute is not in the mask,
				continue;				//  move to the next one
		}

		if(var_masks->bin_exists.isMember(name) )
		{
			exists++;
#ifdef __LOG_ATTRIBUTES__
			attribute_map[name].exists++;
#endif
		}

//				unsigned int debugItemID2=0x15a;
//				ASSERT(flat->id!=debugItemID2);


        // save the error count so we can later determine if an error
        // has already been logged for this attribute
        int error_count_before_attribute = errors->count;

		rc=eval_attribute_item(
			flat,
			(*attribute_item).first,
			(*attribute_item).second,
			var_masks,
			env_info,
			&var_needed,
			errors);

		if (rc != DDL_SUCCESS)
		{	
			if(rc!=DDL_DEFAULT_ATTR ) 
			{
                errs++;
#ifdef __LOG_ATTRIBUTES__
				attribute_map[name].errors++;

				g_totalErrors++;

				if(((rc==DDI_INVALID_HANDLE_TYPE) || (rc==DDI_TAB_BAD_NAME) || (rc==DDI_TAB_BAD_DDID)) 
					&& (env_info->handle_type==ENV_INFO::DeviceHandle)) 
				{
					g_totalExpectedErrors++;
				}
#endif

                // Only load a new error if no other errors have been loaded for
                // this attribute.
                if (errors->count == error_count_before_attribute)
                {
				    load_errors(errors, rc, &var_needed, (*attribute_item).first);
                }
#ifdef __LOG_ITEM_INFO__
				USES_CONVERSION;
				wchar_t* attrStr=A2W(attribute_names[(int)name]);
				wchar_t* typeStr=A2W(item.type);
                wchar_t buf[1000]{0};
				wsprintf(buf,_T("\n       %sdecode error (%d) - item = %x, attribute = %s, type = %s, type_expected = %s"), indent, rc, flat->id, attrStr, typeStr, _T("<na>"));
				PS_OutputDebugString(buf);
#endif	// __LOG_ITEM_INFO__
			}
		}
		else
		{
			if( var_masks->bin_exists.isMember(name) )
			{
				oks++;
#ifdef __LOG_ATTRIBUTES__
				attribute_map[name].oks++;
#endif
			}
		}
	}

#ifdef __LOG_ITEM_INFO__
	{
        wchar_t buf[1000]{0};
		if((g_indentLevel>1) && errs)
			wsprintf(buf,_T("%s===== ( items %d, exists %d, oks %d, errs %d )\n"), indent, cnt, exists, oks, errs);    
		else
			wsprintf(buf,_T("===== ( items %d, exists %d, oks %d, errs %d )\n"), cnt, exists, oks, errs);    

		PS_OutputDebugString(buf);
		g_indentLevel--;
	}
#endif

	if (errors->count) 
		return DDL_CHECK_RETURN_LIST;

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_item
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		eval_item() is a distributor function for all "eval_item_*" routines.
 *		eval_item() "swithes" on the "item_type" and calls the corresponding
 *		"eval_item_*" routine.
 *
 *	Inputs:
 *		mask:			bit mask specifying which attributes are desired.
 *		env_info:		environment information
 *		item_type:		type of item to be evaluated (var, menu,....).
 *
 *	Outputs:
 *		item:			FLAT structure containing the requested item info.
 *		errors:			list of errors incurred processing an item.
 *
 *	Returns:
 *		DDL_INVALID_PARAM,
 *		return values from other "eval_item_*" functions
 *
 *	Author: steve beyerl
 *
 **********************************************************************/
int
eval_item(
void           *item,
::nsEDDEngine::AttributeNameSet   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
ITEM_TYPE       type)
{

	int             rc=E_FAIL;

#ifdef DDSTEST
	TEST_FAIL(EVAL_ONE_ITEM);
#endif

	::nsEDDEngine::ITEM_TYPE item_type = (::nsEDDEngine::ITEM_TYPE)type;

    // If we are getting the attributes for a item_type which are requiring other attributes,
	// add all related attributes into mask variable
	switch (item_type)
	{
	case ::nsEDDEngine::ITYPE_VARIABLE:
		if ( !(mask.isMember(::nsEDDEngine::variable_type)) )
		{
			mask += ::nsEDDEngine::variable_type;
		}
		break;
	case ::nsEDDEngine::ITYPE_MENU:
		if ( !(mask.isMember(::nsEDDEngine::style)) )
		{
			mask += ::nsEDDEngine::style;
		}
		break;
	case ::nsEDDEngine::ITYPE_WAVEFORM:
		if ( mask.isMember(::nsEDDEngine::number_of_points) )
		{
			mask += nsEDDEngine::waveform_type;
			mask += nsEDDEngine::x_values;
			mask += nsEDDEngine::y_values;
		}
		break;
	}

	switch (item_type) 
	{
	// Fall through
	case ::nsEDDEngine::ITYPE_AXIS				:	
	case ::nsEDDEngine::ITYPE_VARIABLE			:
	case ::nsEDDEngine::ITYPE_RECORD			: 
    case ::nsEDDEngine::ITYPE_MENU              :  
	case ::nsEDDEngine::ITYPE_ARRAY				:    
	case ::nsEDDEngine::ITYPE_REFRESH			:          
	case ::nsEDDEngine::ITYPE_NO_VALUE			:			
	case ::nsEDDEngine::ITYPE_COMMAND			:           
	case ::nsEDDEngine::ITYPE_EDIT_DISP			:         
	case ::nsEDDEngine::ITYPE_METHOD			:          
	case ::nsEDDEngine::ITYPE_UNIT				:          
	case ::nsEDDEngine::ITYPE_WAO 				:     		
	case ::nsEDDEngine::ITYPE_ITEM_ARRAY		:       
	case ::nsEDDEngine::ITYPE_BLOCK_B			:	
	case ::nsEDDEngine::ITYPE_BLOCK				:    
	case ::nsEDDEngine::ITYPE_VAR_LIST			:    
	case ::nsEDDEngine::ITYPE_RESP_CODES		:    
	case ::nsEDDEngine::ITYPE_MEMBER			:    
	case ::nsEDDEngine::ITYPE_FILE				:		
	case ::nsEDDEngine::ITYPE_CHART				:		
	case ::nsEDDEngine::ITYPE_GRAPH				:			
	case ::nsEDDEngine::ITYPE_WAVEFORM			:		
	case ::nsEDDEngine::ITYPE_LIST				:			
	case ::nsEDDEngine::ITYPE_GRID				:			
	case ::nsEDDEngine::ITYPE_IMAGE				:			
	case ::nsEDDEngine::ITYPE_BLOB				:				
	case ::nsEDDEngine::ITYPE_PLUGIN			:		
	case ::nsEDDEngine::ITYPE_TEMPLATE			:		
	case ::nsEDDEngine::ITYPE_RESERVED			:		
	case ::nsEDDEngine::ITYPE_COMPONENT			:		
	case ::nsEDDEngine::ITYPE_COMPONENT_FOLDER	:	
	case ::nsEDDEngine::ITYPE_COMPONENT_REFERENCE	:	
	case ::nsEDDEngine::ITYPE_COMPONENT_RELATION	:
	case ::nsEDDEngine::ITYPE_COLLECTION		:      
	case ::nsEDDEngine::ITYPE_SOURCE			:			
		rc = eval_item_any((::nsEDDEngine::ItemBase *)item, mask, env_info, errors);
		break;

	default:
		return DDL_INVALID_PARAM;
		break;
	}
    
    // Post process a variable to calculate the complex defaults if they do not exist yet.
	::nsEDDEngine::ItemBase		*pItemBase = (::nsEDDEngine::ItemBase *)item;
	::nsEDDEngine::FLAT_MASKS   *var_masks = &pItemBase->masks;
	switch (item_type) 
	{
	case ::nsEDDEngine::ITYPE_VARIABLE:
		{
			::nsEDDEngine::FLAT_VAR* variable = (::nsEDDEngine::FLAT_VAR*)pItemBase;

			// If we are asked for display_format but it doesn't exist we need to do
			// special processing to come up with the default
    		if((mask.isMember(::nsEDDEngine::display_format)) &&
			   !(var_masks->bin_exists.isMember(::nsEDDEngine::display_format)) )
			{
				get_default_format(variable->type_size, false, &(variable->display));
				var_masks->attr_avail += ::nsEDDEngine::display_format;
			}
			// If we are asked for edit_format but it doesn't exist we need to do
			// special processing to come up with the default
    		if((mask.isMember(::nsEDDEngine::edit_format)) &&
			   !(var_masks->bin_exists.isMember(::nsEDDEngine::edit_format)) )
			{
				get_default_format(variable->type_size, true, &(variable->edit));
				var_masks->attr_avail += ::nsEDDEngine::edit_format;
			}
			// If we are asked for scaling_factor but it doesn't exist we need to do
			// special processing to come up with the default
    		if((mask.isMember(::nsEDDEngine::scaling_factor)) &&
			   !(var_masks->bin_exists.isMember(::nsEDDEngine::scaling_factor)) )
			{
				get_default_scaling_factor(variable->type_size.type, &(variable->scaling_factor), &(var_masks->attr_avail));
			}
		}
		break;
	case ::nsEDDEngine::ITYPE_MENU:
		{
			// Post process a menu to convert COLUMN_BREAK to SEPARATOR for STYLE= MENU
			// This is only done for STYLE=MENU, all other styles only return COLUMN_BREAK
			::nsEDDEngine::FLAT_MENU* menu = (::nsEDDEngine::FLAT_MENU*)pItemBase;
			if (menu->style == ::nsEDDEngine::MenuStyle::FDI_MENU_STYLE_TYPE)
			{
				//if var_masks->bin_exists is false, menu->items.count == 0. Therefore, the 'for' loop is still working.
				for ( int childIndex = 0; childIndex < menu->items.count; childIndex ++ )
				{
					::nsEDDEngine::MENU_ITEM *menu_item = &menu->items.list[childIndex];
					if (menu_item->ref.op_info.type == ::nsEDDEngine::ITYPE_COLUMNBREAK)
					{
						menu_item->ref.op_info.type = ::nsEDDEngine::ITYPE_SEPARATOR;
						menu_item->ref.desc_type = ::nsEDDEngine::ITYPE_SEPARATOR;
					}
				}
			}
		}
		break;
	case ::nsEDDEngine::ITYPE_WAVEFORM:
		{
			::nsEDDEngine::FLAT_WAVEFORM *pFlatWave = (::nsEDDEngine::FLAT_WAVEFORM *)pItemBase;

			// If default number of points is needed, but it doesn't exist in DD binary, it is calculated here
    		if((mask.isMember(::nsEDDEngine::number_of_points)) &&
			   !(var_masks->bin_exists.isMember(::nsEDDEngine::number_of_points)))
			{
				get_default_numberOfPoints(pFlatWave->waveform_type, &(pFlatWave->x_values), &(pFlatWave->y_values), pFlatWave->number_of_points);
				var_masks->attr_avail += ::nsEDDEngine::number_of_points;
			}
		}
		break;
	}

	return rc;
}

