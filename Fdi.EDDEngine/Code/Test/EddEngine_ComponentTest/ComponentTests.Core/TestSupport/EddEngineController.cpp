#include "EddEngineController.h"
#include "ParamCacheSimulator.h"
#include "EDDEngineLoggerSimulator.h"
#include "nsEDDEngine\EDDEngineFactory.h"
#include "nsEDDEngine\Flats.h"
#include <wchar.h>
#include <vcclr.h>

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace System;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            EddEngineController::EddEngineController(System::String^ ddFile)
                : _Instance(NULL), _ParamCache(NULL), _eddFileName(ddFile)
            {
                _Instance = nullptr;
                _ParamCache = nullptr;
            }

            // Destructor
            EddEngineController::~EddEngineController()
            {

                this->!EddEngineController();
            }

            // Finalizer -- clean up unmanaged resources
            EddEngineController::!EddEngineController()
            {
                delete _Instance;
                _Instance = nullptr;

                delete _ParamCache;
                _ParamCache = nullptr;
            }

            nsEDDEngine::IEDDEngine *EddEngineController::Instance::get()
            {
                return _Instance;
            }

            void EddEngineController::Initialize()
            {
                Initialize(nullptr, nullptr);
            }
            
            void EddEngineController::Initialize(nsConsumer::IParamCache *pIParamCache)
            {
                Initialize(pIParamCache, nullptr);
            }

            void EddEngineController::Initialize(nsConsumer::IParamCache *pIParamCache, nsConsumer::IBuiltinSupport *pIBuiltinSupport)
            {
                Assert::IsTrue(_Instance == nullptr, "EddEngine is already initialized");
                
                // pIBuiltinSupport has to be provided by individual tests when required.
                // pIParamCache is created by EddEngineController when not provided by test.
                ParamCacheSimulator* paramCacheSimulator = nullptr;
                if (pIParamCache == nullptr)
                {
                    paramCacheSimulator = new ParamCacheSimulator();
                    pIParamCache = paramCacheSimulator;
                }
                
				EDDEngineLoggerSimulator* eddEngineLoggerSimulator = nullptr;
				nsConsumer::IEDDEngineLogger *pEDDEngineLogger = nullptr;
                if (pEDDEngineLogger == nullptr)
                {
                    eddEngineLoggerSimulator = new EDDEngineLoggerSimulator();
                    pEDDEngineLogger = eddEngineLoggerSimulator;
                }

                System::String^ dictionaryPath = System::IO::Path::GetFullPath("Dictionaries");
                if (!System::IO::Directory::Exists(dictionaryPath)) Assert::Fail("Dictionary path not found: " + dictionaryPath);

                System::String^ eddFilePath = System::IO::Path::GetFullPath(_eddFileName);
                if (!System::IO::File::Exists(eddFilePath)) Assert::Fail("EDD file not found: " + eddFilePath);

                pin_ptr<const wchar_t> pEddFilePath = PtrToStringChars(eddFilePath);
               
				System::String^ dictionaryPathHART = dictionaryPath + "\\HART";
				System::String^ dictionaryPathFF = dictionaryPath + "\\FF";
                // build the ConfigXML (fkatfnf = formerly known as the file named FMS.ini)
                String^ configXml = String::Format("<CONFIG><HARTPath>{0}</HARTPath><FFPath>{1}</FFPath></CONFIG>", dictionaryPathHART, dictionaryPathFF);
                pin_ptr<const wchar_t> pConfigXML = PtrToStringChars(configXml);
                
                nsEDDEngine::EDDEngineFactoryError errorCode = nsEDDEngine::EDDE_SUCCESS;

                _Instance = nsEDDEngine::EDDEngineFactory::CreateDeviceInstance(
                    pEddFilePath,
                    pIParamCache, 
                    pIBuiltinSupport,
					pEDDEngineLogger,
                    pConfigXML,
                    &errorCode);

                Assert::AreEqual(0, (int)errorCode, "nsEDDEngine::EDDEngineFactory::CreateDeviceInstance() returned error code " /*+ errorCode*/);
                Assert::AreNotEqual((int)_Instance, 0, "nsEDDEngine::EDDEngineFactory::CreateDeviceInstance() returned NULL value");
                
                // save pIParamCache, so we can delete it in finalizer
                _ParamCache = pIParamCache;

                if (paramCacheSimulator != nullptr)
                {
                    paramCacheSimulator->SetEddEngine(_Instance);
                }
            }
            
            nsEDDEngine::FDI_GENERIC_ITEM* EddEngineController::GetVariableItem(ITEM_ID itemId)
            {
                return GetGenericItem(itemId, nsEDDEngine::ITYPE_VARIABLE);
            }

            nsEDDEngine::FDI_GENERIC_ITEM* EddEngineController::GetGenericItem(ITEM_ID itemId)
            {
                Assert::IsFalse(_Instance == nullptr, "EddEngine isn't initialized");

                nsEDDEngine::FDI_ITEM_SPECIFIER itemSpecifier;
                itemSpecifier.eType = nsEDDEngine::FDI_ITEM_ID;
                itemSpecifier.item.id = itemId;
                itemSpecifier.subindex = 0;
                int iBlockInstance = 0;

                nsEDDEngine::ITEM_TYPE itemType;
                int rc = _Instance->GetItemType(iBlockInstance, &itemSpecifier, &itemType);        
                Assert::IsTrue(rc == 0, "GetItemType() failed; ReturnCode: " + rc);

                return GetGenericItem(itemId, itemType);
            }

            nsEDDEngine::FDI_GENERIC_ITEM* EddEngineController::GetGenericItem(ITEM_ID itemId, nsEDDEngine::ITEM_TYPE type)
            {
                return GetGenericItem(itemId, type, GetAllAttributes(type));
            }

            nsEDDEngine::FDI_GENERIC_ITEM* EddEngineController::GetGenericItem(
                ITEM_ID itemId,
                nsEDDEngine::ITEM_TYPE type, 
                nsEDDEngine::AttributeNameSet attributes)
            {
                return GetGenericItem(itemId, type, attributes, 0);
            }

            nsEDDEngine::FDI_GENERIC_ITEM* EddEngineController::GetGenericItem(
                ITEM_ID itemId, 
                nsEDDEngine::ITEM_TYPE type, 
                nsEDDEngine::AttributeNameSet attributes, 
                int iBlockInstance)
            {
                Assert::IsFalse(_Instance == nullptr, "EddEngine isn't initialized");

                nsEDDEngine::FDI_GENERIC_ITEM*  generic_item   = new nsEDDEngine::FDI_GENERIC_ITEM(type);
                nsEDDEngine::FDI_ITEM_SPECIFIER item_spec      = { nsEDDEngine::FDI_ITEM_ID, itemId, 0 };
                nsEDDEngine::AttributeNameSet   attribute_list = attributes;

                wchar_t* languageCode = L"en";
                
                int returnCode = _Instance->GetItem(iBlockInstance, 0, &item_spec, &attribute_list, languageCode, generic_item);

                // WORKAROUND -1517 check returnlist for to see which attribute had problems
                Assert::IsTrue(returnCode == 0 || returnCode == -1517, System::String::Format(
                    "EddEngineController::GetGenericItem IEDDEngine::GetItem() returned {0}.", returnCode));

                return generic_item;
            }

            void EddEngineController::FillParamSpecifier(System::String^ paramName, nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier)
            {
                Assert::IsFalse(_Instance == nullptr, "EddEngine isn't initialized");
                
                ITEM_ID itemId;
                pin_ptr<const wchar_t> paramNameCstr = PtrToStringChars(paramName);
                int returnCode = _Instance->GetItemIdFromSymbolName(const_cast<wchar_t*>(paramNameCstr), &itemId);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);

                paramSpecifier->eType = nsEDDEngine::FDI_PS_ITEM_ID;
                paramSpecifier->id = itemId;
                paramSpecifier->subindex = 0;      
            }
            
            nsEDDEngine::AttributeNameSet EddEngineController::GetAllAttributes(nsEDDEngine::ITEM_TYPE itemType)
            {
                nsEDDEngine::AttributeNameSet allAttributes;

                switch (itemType)
                {
                case nsEDDEngine::ITYPE_NO_VALUE:              // 0
                    break;
                case nsEDDEngine::ITYPE_VARIABLE:              // 1
                    allAttributes = nsEDDEngine::FLAT_VAR::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_COMMAND:               // 2
                    allAttributes = nsEDDEngine::FLAT_COMMAND::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_MENU:                  // 3
                    allAttributes = nsEDDEngine::FLAT_MENU::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_EDIT_DISP:             // 4
                    allAttributes = nsEDDEngine::FLAT_EDIT_DISPLAY::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_METHOD:                // 5
                    allAttributes = nsEDDEngine::FLAT_METHOD::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_REFRESH:               // 6
                    allAttributes = nsEDDEngine::FLAT_REFRESH::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_UNIT:                  // 7
                    allAttributes = nsEDDEngine::FLAT_UNIT::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_WAO:                   // 8
                    allAttributes = nsEDDEngine::FLAT_WAO::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_ITEM_ARRAY:            // 9
                    allAttributes = nsEDDEngine::FLAT_ITEM_ARRAY::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_COLLECTION:            // 10
                    allAttributes = nsEDDEngine::FLAT_COLLECTION::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_BLOCK_B:               // 11
                    allAttributes = nsEDDEngine::FLAT_BLOCK_B::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_BLOCK:                 // 12
                    allAttributes = nsEDDEngine::FLAT_BLOCK::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_RECORD:                // 14
                    allAttributes = nsEDDEngine::FLAT_RECORD::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_ARRAY:                 // 15
                    allAttributes = nsEDDEngine::FLAT_ARRAY::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_VAR_LIST:              // 16
                    allAttributes = nsEDDEngine::FLAT_VAR_LIST::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_RESP_CODES:            // 17
                    break;
                case nsEDDEngine::ITYPE_MEMBER:                // 19
                    //todo: how to handle this case?
                    break;
                case nsEDDEngine::ITYPE_FILE:                  // 20
                    allAttributes = nsEDDEngine::FLAT_FILE::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_CHART:                 // 21
                    allAttributes = nsEDDEngine::FLAT_CHART::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_GRAPH:                 // 22
                    allAttributes = nsEDDEngine::FLAT_GRAPH::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_AXIS:                  // 23
                    allAttributes = nsEDDEngine::FLAT_AXIS::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_WAVEFORM:              // 24
                    allAttributes = nsEDDEngine::FLAT_WAVEFORM::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_SOURCE:                // 25
                    allAttributes = nsEDDEngine::FLAT_SOURCE::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_LIST:                  // 26
                    allAttributes = nsEDDEngine::FLAT_LIST::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_GRID:                  // 27
                    allAttributes = nsEDDEngine::FLAT_GRID::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_IMAGE:                 // 28
                    allAttributes = nsEDDEngine::FLAT_IMAGE::All_Attrs;
                    break;
                case nsEDDEngine::ITYPE_BLOB:                  // 29
                case nsEDDEngine::ITYPE_PLUGIN:                // 30
                case nsEDDEngine::ITYPE_TEMPLATE:              // 31
                case nsEDDEngine::ITYPE_RESERVED:              // 32
                case nsEDDEngine::ITYPE_COMPONENT:             // 33
                case nsEDDEngine::ITYPE_COMPONENT_FOLDER:      // 34
                case nsEDDEngine::ITYPE_COMPONENT_REFERENCE:  // 35
                case nsEDDEngine::ITYPE_COMPONENT_RELATION:    // 36
                default:
                    // todo: handle error
                    break;
                }
                return allAttributes;
            }

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests
