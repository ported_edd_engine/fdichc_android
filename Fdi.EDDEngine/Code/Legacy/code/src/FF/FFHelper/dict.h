/******************************************************************************
 * $Header: dict.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:22 AM$
 *
 * Copyright 2000 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *													   
 * $Workfile: dict.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#ifndef _AMS_DICT_H
#define _AMS_DICT_H

#include "PlatformCommon.h"
#include "stdstring.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

class DICT_EXT_TBL;

int   app_func_get_dict_string(ENV_INFO* env_info, DDL_UINT index, STRING *str);

/*
*	Local structures for reading in the entire dictionary file
*	to allow for sorting.
*/

struct DICT_ENT
{
	WORD wSection;			/* The entry section TOKEN_NUMBER*/
	WORD wOffset;			/* The entry TOKEN_NUMBER within the section */
	DWORD dwRef;			/* The value is computed from section and offset*/
	wchar_t *dict_text;		/* The TOKEN_TEXT for the entry*/
	unsigned short usLen;	/* The length of dict_text */
} ;
////////////////////////////////////////////////////////////////////////////
//
//	Map file data structures
//
////////////////////////////////////////////////////////////////////////////

struct DICT_TABLE_ENTRY
{
	DWORD dwRef;				// Dictionary table reference
	unsigned short usLen;		// String length
	int iStringOffset;			// Offset into string portion of the mapfile
};


struct SECTIONS
{
	WORD wSection;				// This section number
	WORD wMaxOffset;			// MaxOffset in this section
	int iDictOffset;			// Offset into the Dictionary table
};


struct MAP_HEADER
{
	DWORD dwMapSize;			// Size of this mapfile
	int iSectionSize;			// Number of sections
	int iSectionCount;			// Number of sections used
	int iDictSize;				// Number of Dictionary entries
	int iStringSize;			// Number of characters in string area
};


/**********************************************************************
 * Class CRuntimeMap
 **********************************************************************/
class CRuntimeMap
{
public:
	SECTIONS* Sections;			// Pointer to the SECTIONS table
	DICT_TABLE_ENTRY* Dict;		// Pointer to the DICT_TABLE_ENTRY table
	wchar_t *StringTable;		// Pointer to the String table

	CRuntimeMap(MAP_HEADER* pFieldbusView)
	{
		Sections = (SECTIONS*)(&pFieldbusView[1]);	// Start the sections at the end of MAP_HEADER
		Dict = (DICT_TABLE_ENTRY*)(&(Sections[pFieldbusView->iSectionSize])) ;
		StringTable = (wchar_t*)(&(Dict[pFieldbusView->iDictSize]));
	};

private:
	CRuntimeMap();	// Declare, but don't implement to force use of official constructor
};

///////////////////////////////////////////////////////////////////////
//
//		Directory Extension (DICT_EXT) support
//
//////////////////////////////////////////////////////////////////////

/**********************************************************************
 *	DICT_EXT_ENTRY - single extension entry
 **********************************************************************/
class DICT_EXT_ENTRY
{

public:

	DICT_EXT_ENTRY() { m_dictStr = NULL; m_dictIndex = 0; }

	~DICT_EXT_ENTRY() { delete [] m_dictStr; }

	void setDictStr(const wchar_t* tmp)
	{ 
        size_t iSize = wcslen(tmp) + 1;
		m_dictStr = new wchar_t[iSize];
		PS_Wcscpy(m_dictStr, iSize, tmp); 
	}

	void setDictIndex(unsigned long tmp) { m_dictIndex = tmp; }

	unsigned long getDictIndex() { return m_dictIndex; }
	
	wchar_t* getDictStr() { return m_dictStr; }

	static int dictExtCompare(DICT_EXT_ENTRY *entry1, DICT_EXT_ENTRY *entry2);

private:
	
	unsigned long	m_dictIndex;
	wchar_t*		m_dictStr;
};


/**********************************************************************
 *	DICT_EXT_TBL - table of extension entries
 **********************************************************************/
class DICT_EXT_TBL
{

public:
	
	DICT_EXT_TBL() { m_list = NULL; m_count = 0; }
	
	~DICT_EXT_TBL() { delete [] m_list; }
	
	void createList(int numEntries)
	{
		m_list = new DICT_EXT_ENTRY [numEntries];
		if (m_list != NULL)
		{
			m_count = numEntries;
		}
	}
	
	DICT_EXT_ENTRY*	getListEntry(int index)
	{
		return &(m_list[index]);
	}
	
	int getCount() { return m_count; }

	void dump()
	{
		for (int i = 0; i < m_count; i++)
		{
			PS_TRACE(_T("%x"), m_list[i].getDictIndex());
			PS_TRACE(_T("\"%s\"\n"), m_list[i].getDictStr());
		}
	}

private:
	
	int					m_count;
	DICT_EXT_ENTRY*		m_list;
};


/**********************************************************************
 * Class StdDictTable
 **********************************************************************/
class StdDictTable
{
public:
    StdDictTable();
    ~StdDictTable();
    void  CloseFFStdDictTable();
    BOOL  LoadDict(const wchar_t* pReleasePath);
    BOOL dictExtTableSearch(DDL_UINT index, STRING* str, ENV_INFO *env_info);

    HANDLE hFieldbusMap;
    MAP_HEADER* lpFieldbusView;
    CRuntimeMap* pRuntimeMap;		// class members point into the g_lpFielbusView map file

private:

	bool m_bLoaded;

    /*
     * Set this and TOKEN_ERROR messages will contain the
     * name of the program (see dicterr() for details).
     */
    char *dict_program;
    /*
     * Info kept in globals for efficiency.
     */
    int dicterrs;
    FILE *dictfp;
    int dictline;

    /*
     * The token buffer.
     */
    char *tokbase;
    char *tokptr;
    char *toklim;

    /*
     * Formerly Static from the lex() function
     */
	int lex_nextchar;

    /*
     * formerly Static from the dict_table_install() function
     */
    DWORD install_dwCurrentSection;	// Initialize to something bad
	int install_iSectionIndex;
	int install_iNextDictIndex;
	int install_iStringOffset;

    /*
     * Formerly Static Array of DICT_EXT_ENTRYs for Fieldbus
     */
    DICT_EXT_TBL* pDictExtArrayFieldbus;
    
    /*
     * Private Functions
     */
    DWORD OpenFFStdDictTable(LPCWSTR pDevicePath);
    void  dicterr(wchar_t *msg);
    void  growBuffer();
    inline void inschar(int c);
    int   comment(void);
    int   lex(void);
    void  parse(DICT_ENT** out_DictArray, int* out_iDictCount, int* out_iNumSections, int* out_iCumStringLen, bool isUTF8 );
    int   makedict(const CStdString& strFullPath);
    CStdString GetMappedFilename();
    DWORD dict_FF_table_create(int iSectionSize, int iDictSize, int iCumStringLen);
    DWORD dict_FF_table_open();
    DWORD dict_FF_table_map_read_view();
    DWORD dict_FF_table_unmap_view();
    DWORD dict_FF_table_close();
    void  dict_table_install(DWORD dwRef, wchar_t *str, unsigned short usLength);
    DWORD dictExtFileLoad(const CStdString& strDictExtFile);
    DWORD dictExtFFTableCreate(const CStdString& devicePath);
	
	static bool ChooseDictionaryFile(const wchar_t* DictName, const wchar_t* DictNameDar);

	int LoadBinaryDictionary(LPCTSTR binaryDictionaryFile);
	int WriteBinaryDictionary(LPCTSTR binaryDictionaryFile);

    static int compdict (DICT_ENT *dict_ent1, DICT_ENT *dict_ent2);
};

extern StdDictTable g_StdDictTable;
#endif   /* _AMS_DICT_H */

//////////////////////////////// End Of File ////////////////////////////////
