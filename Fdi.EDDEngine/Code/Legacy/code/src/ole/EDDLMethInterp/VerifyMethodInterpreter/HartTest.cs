﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VerifyMethodInterpreter
{
    [DeploymentItem("$(OutDir)\\HARTEDDEngine.dll"), TestClass]
    public class Hart : VerifyMethodInterpreter.MainWindowVMTest
    {
        private void CallMainVMTest(string strTestName, bool bRunTest)
        {
            RunOneMethodTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.HART, strTestName, bRunTest);
        }
        private void CallActionTest(string strTestName)
        {
            RunOneActionTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.HART, strTestName);
        }

        
        // method_direct_access_collection ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_direct_access_collection()
        {
            CallMainVMTest("16499, method_direct_access_collection", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_direct_access_collection()
        {
            CallMainVMTest("16499, method_direct_access_collection", false);
        }

        // method_common ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_common()
        {
             CallMainVMTest("16532, method_common", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_common()
        {
            CallMainVMTest("16532, method_common",false);
        }

        // init_method_set_values ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void init_method_set_values()
        {
            CallMainVMTest("16414, init_method_set_values", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_init_method_set_values()
        {
            CallMainVMTest("16414, init_method_set_values", false);
        }

        // get_rspcode_string_builtin ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void get_rspcode_string_builtin()
        {
            CallMainVMTest("16468, get_rspcode_string_builtin", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_get_rspcode_string_builtin()
        {
            CallMainVMTest("16468, get_rspcode_string_builtin", false);
        }

        // method_status ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_status()
        {
            CallMainVMTest("16495, method_status", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_status()
        {
            CallMainVMTest("16495, method_status", false);
        }

        // method_multiply_double ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_multiply_double()
        {
            CallMainVMTest("16513, method_multiply_double(1.2, 3.4)", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_multiply_double()
        {
            CallMainVMTest("16513, method_multiply_double(1.2, 3.4)", false);
        }

        // output_using_dot ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void output_using_dot()
        {
            CallMainVMTest("16528, output_using_dot", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_output_using_dot()
        {
            CallMainVMTest("16528, output_using_dot", false);
        }

        // method_all_time_test ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_all_time_test()
        {
            CallMainVMTest("16574, method_all_time_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_all_time_test()
        {
            CallMainVMTest("16574, method_all_time_test", false);
        }

        // meth_unsigned_variable_sizes///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_unsigned_variable_sizes()
        {
            CallMainVMTest("16575, meth_unsigned_variable_sizes", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_unsigned_variable_sizes()
        {
            CallMainVMTest("16575, meth_unsigned_variable_sizes", false);
        }

        // meth_local_scope_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_local_scope_test()
        {
            CallMainVMTest("16576, meth_local_scope_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_local_scope_test()
        {
            CallMainVMTest("16576, meth_local_scope_test", false);
        }

        // method_FDI_builtin_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_FDI_builtin_test()
        {
            CallMainVMTest("16577, method_FDI_builtin_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_FDI_builtin_test()
        {
            CallMainVMTest("16577, method_FDI_builtin_test", false);
        }

        // method_index_size_127_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_index_size_127_test()
        {
            CallMainVMTest("16578, method_index_size_127_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_index_size_127_test()
        {
            CallMainVMTest("16578, method_index_size_127_test", false);
        }

        // method_index_size_128_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_index_size_128_test()
        {
            CallMainVMTest("16580, method_index_size_128_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_index_size_128_test()
        {
            CallMainVMTest("16580, method_index_size_128_test", false);
        }

        // method_index_size_255_direct_ref_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_index_size_255_direct_ref_test()
        {
            CallMainVMTest("16581, method_index_size_255_direct_ref_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_index_size_255_direct_ref_test()
        {
            CallMainVMTest("16581, method_index_size_255_direct_ref_test", false);
        }

        // method_vassign_enum_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_vassign_enum_test()
        {
            CallMainVMTest("16582, method_vassign_enum_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_vassign_enum_test()
        {
            CallMainVMTest("16582, method_vassign_enum_test", false);
        }

        // division_by_zero//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void division_by_zero()
        {
            CallMainVMTest("16584, division_by_zero", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_division_by_zero()
        {
            CallMainVMTest("16584, division_by_zero", false);
        }

        // meth_conditional_on_list_element//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_conditional_on_list_element()
        {
            CallMainVMTest("16588, meth_conditional_on_list_element", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_conditional_on_list_element()
        {
            CallMainVMTest("16588, meth_conditional_on_list_element", false);
        }

        // meth_complex_reference//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_complex_reference()
        {
            CallMainVMTest("16593, meth_complex_reference", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_complex_reference()
        {
            CallMainVMTest("16593, meth_complex_reference", false);
        }

        // methodTestUnitAndTimeScale//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void methodTestUnitAndTimeScale()
        {
            CallMainVMTest("16605, methodTestUnitAndTimeScale", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_methodTestUnitAndTimeScale()
        {
            CallMainVMTest("16605, methodTestUnitAndTimeScale", false);
        }

        // method_do_it_all_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_do_it_all_test()
        {
            CallMainVMTest("16420, method_do_it_all_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_do_it_all_test()
        {
            CallMainVMTest("16420, method_do_it_all_test", false);
        }

        // method_expression_Count_Capacity//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_expression_Count_Capacity()
        {
            CallMainVMTest("16610, method_expression_Count_Capacity", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_expression_Count_Capacity()
        {
            CallMainVMTest("16610, method_expression_Count_Capacity", false);
        }

        // method_expression_enum_DEFAULT_VALUE//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_expression_enum_DEFAULT_VALUE()
        {
            CallMainVMTest("16611, method_expression_enum_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_expression_enum_DEFAULT_VALUE()
        {
            CallMainVMTest("16611, method_expression_enum_DEFAULT_VALUE", false);
        }

        // method_expression_index_DEFAULT_VALUE//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_expression_index_DEFAULT_VALUE()
        {
            CallMainVMTest("16612, method_expression_index_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_expression_index_DEFAULT_VALUE()
        {
            CallMainVMTest("16612, method_expression_index_DEFAULT_VALUE", false);
        }

        // method_expression_viewMin_and_viewMax//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_expression_viewMin_and_viewMax()
        {
            CallMainVMTest("16609, method_expression_viewMin_and_viewMax", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_expression_viewMin_and_viewMax()
        {
            CallMainVMTest("16609, method_expression_viewMin_and_viewMax", false);
        }

        // method_simple_assign_of_time_values//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_simple_assign_of_time_values()
        {
            CallMainVMTest("16614, method_simple_assign_of_time_values", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_simple_assign_of_time_values()
        {
            CallMainVMTest("16614, method_simple_assign_of_time_values", false);
        }

        // MethodAccessInvalidVar//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void MethodAccessInvalidVar()
        {
            CallMainVMTest("16617, MethodAccessInvalidVar", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_MethodAccessInvalidVar()
        {
            CallMainVMTest("16617, MethodAccessInvalidVar", false);
        }

        // test_longlong_plus//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_longlong_plus()
        {
            CallMainVMTest("16622, test_longlong_plus", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_longlong_plus()
        {
            CallMainVMTest("16622, test_longlong_plus", false);
        }

        // test_uint_uminus//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_uint_uminus()
        {
            CallMainVMTest("16623, test_uint_uminus", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_uint_uminus()
        {
            CallMainVMTest("16623, test_uint_uminus", false);
        }

       // test_uint_uminus//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_short_plus()
        {
            CallMainVMTest("16624, test_short_plus", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_short_plus()
        {
            CallMainVMTest("16624, test_short_plus", false);
        }

        // test_uint_uminus//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_longlong_uminus()
        {
            CallMainVMTest("16625, test_longlong_uminus", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_longlong_uminus()
        {
            CallMainVMTest("16625, test_longlong_uminus", false);
        }

        // test_short_shiftright//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_short_shiftright()
        {
            CallMainVMTest("16626, test_short_shiftright", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_short_shiftright()
        {
            CallMainVMTest("16626, test_short_shiftright", false);
        }

        // test_uint_times//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_uint_times()
        {
            CallMainVMTest("16627, test_uint_times", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_uint_times()
        {
            CallMainVMTest("16627, test_uint_times", false);
        }

        // test_ushort_remainder//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_ushort_remainder()
        {
            CallMainVMTest("16628, test_ushort_remainder", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_ushort_remainder()
        {
            CallMainVMTest("16628, test_ushort_remainder", false);
        }

        // test_ushort_remainder//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_ulonglong_shiftright()
        {
            CallMainVMTest("16629, test_ulonglong_shiftright", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_ulonglong_shiftright()
        {
            CallMainVMTest("16629, test_ulonglong_shiftright", false);
        }

        // test_ddstr_operations//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_ddstr_operations()
        {
            CallMainVMTest("16630, test_ddstr_operations", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_ddstr_operations()
        {
            CallMainVMTest("16630, test_ddstr_operations", false);
        }

        // ddstring_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void ddstring_test()
        {
            CallMainVMTest("16633, ddstring_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_ddstring_test()
        {
            CallMainVMTest("16633, ddstring_test", false);
        }

        // meth_test_abort_list_from_selection//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_abort_list_from_selection()
        {
            CallMainVMTest("16634, meth_test_abort_list_from_selection", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_abort_list_from_selection()
        {
            CallMainVMTest("16634, meth_test_abort_list_from_selection", false);
        }

        // meth_test_abort_list_from_iteration//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_abort_list_from_iteration()
        {
            CallMainVMTest("16635, meth_test_abort_list_from_iteration", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_abort_list_from_iteration()
        {
            CallMainVMTest("16635, meth_test_abort_list_from_iteration", false);
        }

        // meth_test_abort_list_from_switch//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_abort_list_from_switch()
        {
            CallMainVMTest("16636, meth_test_abort_list_from_switch", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_abort_list_from_switch()
        {
            CallMainVMTest("16636, meth_test_abort_list_from_switch", false);
        }

        // meth_test_abort_list_from_return//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_abort_list_from_return()
        {
            CallMainVMTest("16637, meth_test_abort_list_from_return", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_abort_list_from_return()
        {
            CallMainVMTest("16637, meth_test_abort_list_from_return", false);
        }

        // meth_test_abort_list//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_abort_list()
        {
            CallMainVMTest("16648, meth_test_abort_list", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_abort_list()
        {
            CallMainVMTest("16648, meth_test_abort_list", false);
        }

        // test_MINn_MAXn_var_axis_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_MINn_MAXn_var_axis_value()
        {
            CallMainVMTest("16645, test_MINn_MAXn_var_axis_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_MINn_MAXn_var_axis_value()
        {
            CallMainVMTest("16645, test_MINn_MAXn_var_axis_value", false);
        }

        // meth_NULL_string//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_NULL_string()
        {
            CallMainVMTest("16649, meth_NULL_string", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_NULL_string()
        {
            CallMainVMTest("16649, meth_NULL_string", false);
        }

        //test_itemArray_index//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_itemArray_index()
        {
            CallMainVMTest("16652, test_itemArray_index", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_itemArray_index()
        {
            CallMainVMTest("16652, test_itemArray_index", false);
        }

        //test_itemArray_index//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_int1_ungined_int1_outofrange()
        {
            CallMainVMTest("16654, method_int1_ungined_int1_outofrange", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_int1_ungined_int1_outofrange()
        {
            CallMainVMTest("16654, method_int1_ungined_int1_outofrange", false);
        }

        //method_calling_method_with_2levels//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_calling_method_with_2levels()
        {
            CallMainVMTest("16639, method_calling_method_with_2levels", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_calling_method_with_2levels()
        {
            CallMainVMTest("16639, method_calling_method_with_2levels", false);
        }

        //method_calling_method_with_complex_ref//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_calling_method_with_complex_ref()
        {
            CallMainVMTest("16681, method_calling_method_with_complex_ref", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_calling_method_with_complex_ref()
        {
            CallMainVMTest("16681, method_calling_method_with_complex_ref", false);
        }

        //get_dictionary_string_builtin//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void get_dictionary_string_builtin()
        {
            CallMainVMTest("16450, get_dictionary_string_builtin", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_get_dictionary_string_builtin()
        {
            CallMainVMTest("16450, get_dictionary_string_builtin", false);
        }

        //Dislplay_for_variable_vai_reference_array_test//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void Dislplay_for_variable_vai_reference_array_test()
        {
            CallMainVMTest("16687, Dislplay_for_variable_vai_reference_array_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_Dislplay_for_variable_vai_reference_array_test()
        {
            CallMainVMTest("16687, Dislplay_for_variable_vai_reference_array_test", false);
        }

        //method_populate_Array_Uint3//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_populate_Array_Uint3()
        {
            CallMainVMTest("16698, method_populate_Array_Uint3", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_populate_Array_Uint3()
        {
            CallMainVMTest("16698, method_populate_Array_Uint3", false);
        }

        //method_populate_Array_Int3//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_populate_Array_Int3()
        {
            CallMainVMTest("16699, method_populate_Array_Int3", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_populate_Array_Int3()
        {
            CallMainVMTest("16699, method_populate_Array_Int3", false);
        }

        //test_method_passing_LIST_as_DD_ITEM//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_method_passing_LIST_as_DD_ITEM()
        {
            CallMainVMTest("16702, test_method_passing_LIST_as_DD_ITEM", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_method_passing_LIST_as_DD_ITEM()
        {
            CallMainVMTest("16702, test_method_passing_LIST_as_DD_ITEM", false);
        }

        //method_response_code_abort_retry_mask_access_restricted//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_response_code_abort_retry_mask_access_restricted()
        {
            CallMainVMTest("16704, method_response_code_abort_retry_mask_access_restricted", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_response_code_abort_retry_mask_access_restricted()
        {
            CallMainVMTest("16704, method_response_code_abort_retry_mask_access_restricted", false);
        }

        //method_response_code_retry_mask//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_response_code_retry_mask()
        {
            CallMainVMTest("16705, method_response_code_retry_mask", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_response_code_retry_mask()
        {
            CallMainVMTest("16705, method_response_code_retry_mask", false);
        }

        //test_DISPLAY_FORMAT_variable//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_DISPLAY_FORMAT_variable()
        {
            CallMainVMTest("16706, test_DISPLAY_FORMAT_variable", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_DISPLAY_FORMAT_variable()
        {
            CallMainVMTest("16706, test_DISPLAY_FORMAT_variable", false);
        }

        //meth_test_read_only_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_test_read_only_value()
        {
            CallMainVMTest("16707, meth_test_read_only_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_test_read_only_value()
        {
            CallMainVMTest("16707, meth_test_read_only_value", false);
        }

        //meth_get_local_var_value//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_get_local_var_value()
        {
            CallMainVMTest("16708, meth_get_local_var_value", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_get_local_var_value()
        {
            CallMainVMTest("16708, meth_get_local_var_value", false);
        }

        //method_calling_method_with_attribute//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_calling_method_with_attribute()
        {
            CallMainVMTest("16711, method_calling_method_with_attribute", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_calling_method_with_attribute()
        {
            CallMainVMTest("16711, method_calling_method_with_attribute", false);
        }

        //meth_ASCII_DEFAULT_VALUE//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_ASCII_DEFAULT_VALUE()
        {
            CallMainVMTest("16712, meth_ASCII_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_ASCII_DEFAULT_VALUE()
        {
            CallMainVMTest("16712, meth_ASCII_DEFAULT_VALUE", false);
        }

        //meth_ASCII_DEFAULT_VALUE//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_password_DEFAULT_VALUE()
        {
            CallMainVMTest("16713, meth_password_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_password_DEFAULT_VALUE()
        {
            CallMainVMTest("16713, meth_password_DEFAULT_VALUE", false);
        }

        //meth_time_value_DEFAULT_VALUE//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_time_value_DEFAULT_VALUE()
        {
            CallMainVMTest("16714, meth_time_value_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_time_value_DEFAULT_VALUE()
        {
            CallMainVMTest("16714, meth_time_value_DEFAULT_VALUE", false);
        }

        //meth_testAllDataTypeVars//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_testAllDataTypeVars()
        {
            CallMainVMTest("16810, meth_testAllDataTypeVars", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_testAllDataTypeVars()
        {
            CallMainVMTest("16810, meth_testAllDataTypeVars", false);
        }

        //meth_calFactorialNumber//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_calFactorialNumber()
        {
            CallMainVMTest("16811, meth_calFactorialNumber", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_calFactorialNumber()
        {
            CallMainVMTest("16811, meth_calFactorialNumber", false);
        }

        //meth_calModulo//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_calModulo()
        {
            CallMainVMTest("16813, meth_calModulo", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_calModulo()
        {
            CallMainVMTest("16813, meth_calModulo", false);
        }

        //meth_calModulo//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_viewMin_and_viewMax_2()
        {
            CallMainVMTest("16816, method_viewMin_and_viewMax_2", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_viewMin_and_viewMax_2()
        {
            CallMainVMTest("16816, method_viewMin_and_viewMax_2", false);
        }

        //method_function_name_as_variable_name//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_function_name_as_variable_name()
        {
            CallMainVMTest("16818, method_function_name_as_variable_name", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_function_name_as_variable_name()
        {
            CallMainVMTest("16818, method_function_name_as_variable_name", false);
        }

        //method_XMTR_ABORT_ON_DATA//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_XMTR_ABORT_ON_DATA()
        {
            CallMainVMTest("16820, method_XMTR_ABORT_ON_DATA", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_XMTR_ABORT_ON_DATA()
        {
            CallMainVMTest("16820, method_XMTR_ABORT_ON_DATA", false);
        }

        //GetHealthStatus//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void GetHealthStatus()
        {
            CallMainVMTest("16821, GetHealthStatus", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_GetHealthStatus()
        {
            CallMainVMTest("16821, GetHealthStatus", false);
        }

        //meth_REFRESH_ACTION_aborts//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_REFRESH_ACTION_aborts()
        {
            CallMainVMTest("16822, meth_REFRESH_ACTION_aborts", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_REFRESH_ACTION_aborts()
        {
            CallMainVMTest("16822, meth_REFRESH_ACTION_aborts", false);
        }

        //method_test_precedence_associativity//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_test_precedence_associativity()
        {
            CallMainVMTest("16826, method_test_precedence_associativity", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_test_precedence_associativity()
        {
            CallMainVMTest("16826, method_test_precedence_associativity", false);
        }

        //test_method_without_UI_and_abort_method_with_acknowledge///////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_method_without_UI_and_abort_method_with_acknowledge()
        {
            CallMainVMTest("16827, test_method_without_UI_and_abort_method_with_acknowledge", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_method_without_UI_and_abort_method_with_acknowledge()
        {
            CallMainVMTest("16827, test_method_without_UI_and_abort_method_with_acknowledge", false);
        }

        //method_string_byte///////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_string_byte()
        {
            CallMainVMTest("16829, method_string_byte", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_string_byte()
        {
            CallMainVMTest("16829, method_string_byte", false);
        }

        //method_test_local_array_index///////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_test_local_array_index()
        {
            CallMainVMTest("16831, method_test_local_array_index", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_test_local_array_index()
        {
            CallMainVMTest("16831, method_test_local_array_index", false);
        }

        //test_method_acknowledge_with_DD_STRING_array///////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_method_acknowledge_with_DD_STRING_array()
        {
            CallMainVMTest("16832, test_method_acknowledge_with_DD_STRING_array", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_method_acknowledge_with_DD_STRING_array()
        {
            CallMainVMTest("16832, test_method_acknowledge_with_DD_STRING_array", false);
        }

        // myTest//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void myTest()
        {
            string sMethodDefinition;

            //sMethodDefinition = "0, {" +
            //                    "_iassign(16470,~0);"
            //                    + "}";

            //show error in ported code in changeset 64249
            sMethodDefinition = "0, {" +
                                "_fassign(16419, 3.1415926);"
                                + "}";

            //no shown error in ported code in changeset 64249
            //sMethodDefinition = "0, {" +
            //                    "eight_byte_integer = 0x7FFFFFFFFFFFFFFF;"
            //                    + "}";

            CallMainVMTest(sMethodDefinition, true);
        }

        
        /////////////////////////////////////ACTION METHODS


        //method_i_scaling//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_i_scaling()
        {
            CallActionTest("16446, method_i_scaling");
        }

        //method_f_scaling//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_f_scaling()
        {
            CallActionTest("16448, method_f_scaling");
        }

        //method_s_scaling//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_s_scaling()
        {
            CallActionTest("16600, method_s_scaling");
        }

        //method_l_scaling//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_l_scaling()
        {
            CallActionTest("16445, method_l_scaling");
        }

        //method_i_scaling_for_index///////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_i_scaling_for_index()
        {
            CallActionTest("16824, method_i_scaling_for_index");
        }

        //method_time_value_scaling///////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void method_time_value_scaling()
        {
            CallActionTest("16527, method_time_value_scaling");
        }

    }
}
