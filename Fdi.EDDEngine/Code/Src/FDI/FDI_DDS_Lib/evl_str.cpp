/*
 *	@(#) $Id: evl_str.cpp,v 1.2 2012/09/24 23:54:37 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  STRING.
 */


#include "stdafx.h"
#include "fch_lib.h"
#include "evl_loc.h"
//#include <afxtempl.h>
#include<list>
#include "conversions.h"

#ifdef DDSTEST
#include "tst_fail.h"
#endif

extern void deobfuscate(char *str, int size, int ddtype);


extern int
get_attribute_value(::nsEDDEngine::AttributeName attrib_name,	DDL_UINT which,	DESC_REF *desc_ref,	EXPR *expr,
					nsEDDEngine::OP_REF_LIST	*depinfo, ENV_INFO *env_info,nsEDDEngine::OP_REF			*var_needed);

/*********************************************************************
 *
 *	Name: ddl_free_string
 *	ShortDesc: Free STRING structure.
 *
 *	Description:
 *		ddl_free_string will check the flags in the STRING structure.
 *      If flags indicate to free string then the STRING structure will
 *      be freed.
 *
 *	Inputs:
 *		str: pointer to the STRING structure
 *
 *	Outputs:
 *		str: pointer to the empty STRING structure
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_string(
STRING         *str)
{
	if (str != NULL)
	{
		if (str->flags & STRING::FREE_STRING) {
			free((void *) str->str);
		}

		str->flags = STRING::DONT_FREE_STRING;
		str->len = 0;
		str->str = NULL;
	}

	return;
}

/*********************************************************************
 *
 *	Name: string_enum_lookup
 *
 *	ShortDesc: lookup an enumerated string
 *
 *	Description:
 *		string_enum_lookup will find the string referred to by an
 *		enumeration value string. This string contains the ITEM_ID
 *		of an enumerated variable, and the enumeration value which
 *		corresponds to the description string desired
 *
 *	Inputs:
 *		enum_id - ITEM_ID of the enumeration
 *		enum_val - value of the enumeration
 *      string - a pointer to the STRING struct to be loaded
 *		depinfo - a pointer to the nsEDDEngine::OP_REF_LIST struct
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string - the loaded STRING structure
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				  dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		FAILURE
 *		return codes from fetch_var(), eval_attr_enum(), append_depinfo()
 *		eval_clean_var()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
string_enum_lookup(
ITEM_ID         enum_id,
unsigned long   enum_val,
STRING         *string,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
EnumMap        *pEnumMap)
{
	int rc = DDL_SUCCESS;

	::nsEDDEngine::FLAT_VAR        *pVar = nullptr;	/* flat variable struct for calling fetch with */
	
	if (pEnumMap)	// If we are using an Enum Cache, look up the enum_id
	{
		EnumMap::iterator it;

		it = pEnumMap->find(enum_id);

		if (it != pEnumMap->end())	// Set pVar, if it is found
		{
			pVar = it->second;
		}
	}

	if (pVar == nullptr)	// We could not set this from our Enum Cache, so we must fetch, eval, and insert it in the cache
	{
		pVar = new nsEDDEngine::FLAT_VAR();	/* flat variable struct for calling fetch with */

		/*
		* Fetch the binary for the enumerated variable
		*/

		::nsEDDEngine::AttributeNameSet mask = ::nsEDDEngine::enumerations;

		rc = resolve_fetch(env_info, enum_id,
			mask,
			(::nsEDDEngine::ItemBase *) pVar, (unsigned short)VARIABLE_ITYPE,
			env_info->block_handle);

		if (!pVar->masks.bin_hooked.isMember(::nsEDDEngine::enumerations)) {	// Make sure we have binary for enumerations
			rc = DDL_ENCODING_ERROR;
		}

		if (rc == DDL_SUCCESS) {
			int             rc2 = DDL_SUCCESS;	/* secondary return code */
			/*
			* Evaluate the binary for the enumerated variable
			*/
			::nsEDDEngine::DEPBIN         *cur_depbin;	/* Current dependency and binary info */
			cur_depbin = pVar->depbin->db_enums;

			ENUM_VALUE_LIST enums = { 0 };

			rc = eval_attr_enum(cur_depbin->bin_chunk,
				cur_depbin->bin_size,
				&enums,
				&cur_depbin->dep,
				env_info,
				var_needed);

			if (rc == DDL_SUCCESS) {
				::FDI_ConvENUM_VALUE_LIST::CopyFrom(&enums, &pVar->enums);
				ddl_free_enum_list(&enums, FREE_ATTR);
				
			}

			if (depinfo != NULL) {
				rc2 = append_depinfo(depinfo, &cur_depbin->dep);
				if ((rc2 != DDL_SUCCESS) && (rc == DDL_SUCCESS)) {
					rc = rc2;
				}
			}

			ddl_free_depinfo(&cur_depbin->dep, FREE_ATTR);
		}

		// free the memory used by fetch
		//resolve_fetch_free(env_info, (void *)pVar, (unsigned short)VARIABLE_ITYPE, scratch, env_info->block_handle);

		if ((rc == DDL_SUCCESS) && (pEnumMap))	// Add this FLAT_VAR to our Enum Cache, if we are using one
		{
			std::pair<EnumMap::iterator, bool> pr = pEnumMap->insert(EnumMap::value_type(enum_id, pVar));
		}
	}
	if (rc == DDL_SUCCESS) {	// We must now search the enum list for the entry we want

		nsEDDEngine::ENUM_VALUE *enum_ptr = pVar->enums.list;
		unsigned short count = pVar->enums.count;

		/*
		* Find the match in the list
		*/
		bool bIsFound = false;

		for (unsigned short inc = 0; inc < count; enum_ptr++, inc++) {

			if (enum_ptr->val.val.u == enum_val) {

				string->str = (wchar_t*)calloc(1, sizeof(wchar_t)*(wcslen(enum_ptr->desc.c_str()) + 1));
				PS_Wcscpy(string->str, wcslen(enum_ptr->desc.c_str()) + 1, enum_ptr->desc.c_str());
				string->len = (unsigned short)wcslen(enum_ptr->desc.c_str());
				string->flags = STRING::FREE_STRING;

				bIsFound = true;
				break;
			}
		}

		if (bIsFound == false) {	/* The enum was not found */
			STRING strDictString = { 0 };

			rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_UNDEFINED, &strDictString);

			CStdString strNumUndefined;

			strNumUndefined.Format(L"0x%x %s", enum_val, strDictString.str);

            string->str = wcsdup(strNumUndefined);
			string->len = (unsigned short)wcslen(string->str);
			string->flags = STRING::FREE_STRING;

			ddl_free_string(&strDictString);


			rc = ENUM_NOT_FOUND;
		}

		
	}

	if (pEnumMap == nullptr) {	// If we didn't save it in our Enum Cache, we must clean it here.
		delete pVar;
		pVar = nullptr;
	}

	
	
	return rc;
}


/*********************************************************************
 *
 *	Name: ddl_parse_string(chunkp, size, string)
 *
 *	ShortDesc: ddl_parse_string takes a binary chunk and loads a STRING structure with it.
 *
 *	Description:
 *		ddl_parse_string takes a binary chunk and reads the tag to determine which string type
 *		the string is: DICTIONARY_STRING, ENUMERATION_STRING, DEV_SPEC_STRING, TEXT_STRING,
 *		UNUSED_STRING or VARIABLE_STRING, it loads the string info structure with that type
 *		and looks up the corresponding string.
 *		If the string argument is NULL the structure is not loaded but the chunk pointer is
 *		updated.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to an STRING structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from DDL_PARSE_INTEGER(), ddl_parse_item_id(),
 *		app_func_get_dev_spec_string(), ddl_add_depinfo(), app_func_get_param_value()
 *		app_func_get_dict_string(), string_enum_lookup()
 *
 *	Author: Chris Gustafson
 *
 *********************************************************************/


int
ddl_parse_string(
unsigned char **chunkp,
DDL_UINT       *size,
STRING         *out_string,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
EnumMap        *pEnumMap)
{
	DDL_UINT temp = 0;		/* temporary storage for casting conversion */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_STRING);
#endif

	/*
	 * initialize string struct
	 */

	if (out_string != NULL)
	{
		out_string->flags = STRING::DONT_FREE_STRING;
		out_string->len = 0;
		out_string->str = NULL;
	}

	// Parse the tag to find out what kind of string it is.

	int rc = DDL_SUCCESS;			/* return code */
	DDL_UINT tag = 0;				/* temporary tag */
	DDL_UINT len = 0;				/* length of binary assoc. w/ parsed tag */

	std::list <STRING*> listStringList;

	DDL_PARSE_TAG(chunkp, size, &tag, &len);	// Explicit tag

	ASSERT_RET(tag == STRING_TAG, DDL_ENCODING_ERROR);

	*size -= len;				// Adjust size out parameter

	while (len > 0)
	{
		STRING strTemp = {0};		// Temp string to parse into

		DDL_PARSE_TAG(chunkp, &len, &tag, (DDL_UINT *) NULL_PTR);	// Implicit tag

		switch (tag) {
		
		case DEV_SPEC_STRING_TAG:
			{
				/*
				* device specific string (ie. string number). Parse the file
				* information and string number.
				*/

				if (out_string) {
					DEV_STRING_INFO dev_str_info = {0};		/* holds device specific string info */

					DDL_PARSE_INTEGER(chunkp, &len, &temp);
					dev_str_info.id = (unsigned int) temp;

					rc = app_func_get_dev_spec_string(env_info, &dev_str_info,
						&strTemp);

					/*
					* If a string was not found, get the default error
					* string.
					*/

					if (rc != DDL_SUCCESS) {
						rc = app_func_get_dict_string(env_info, DEFAULT_DEV_SPEC_STRING,
							&strTemp);
						if (rc != DDL_SUCCESS) {
							return rc;
						}
					}
				}
				else {
					DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);
					DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);
					DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);
					DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);
					DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);
				}
			}
			break;

		case VAR_REF_STRING_TAG:
			{
				nsEDDEngine::OP_REF oper_ref;// = {0};		/* used by variable strings */
				/*
				* variable/variable_reference string. Parse the variable id.
				*/

					rc = ddl_parse_op_ref(chunkp, &len, &oper_ref, depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}


				if (depinfo) {

					/*
					* Add item_id of string to dependency info.
					*/

					rc = ddl_add_depinfo(&oper_ref, depinfo);

				}

				if (out_string) {
					nsConsumer::EVAL_VAR_VALUE  var_value;// = {0};	// Store the retrieved value

					rc = app_func_get_param_value(env_info, &oper_ref, &var_value);

					if (rc != DDL_SUCCESS) 
					{
						*var_needed = oper_ref;
						return rc;
					}
					
					switch(var_value.type)
					{
					case nsEDDEngine::VT_ENUMERATED:
					case nsEDDEngine::VT_BIT_ENUMERATED:
						{
							// If the var_value type is ENUMERATED/BIT_ENUMERATED, we need to lookup for the string associated with enum value
							rc = string_enum_lookup(oper_ref.op_info.id, (unsigned long)var_value.val.u, &strTemp,
								depinfo, env_info, var_needed, pEnumMap);

							if ((rc != DDL_SUCCESS) && (rc != ENUM_NOT_FOUND)) // If the enum was not found, we manufacture one.
							{
								return rc;  // Return the failed error code
							}

							rc = DDL_SUCCESS;
						}
						break;
					case nsEDDEngine::VT_ASCII:
					case nsEDDEngine::VT_PACKED_ASCII:
					case nsEDDEngine::VT_PASSWORD:
					case nsEDDEngine::VT_EUC:
					case nsEDDEngine::VT_VISIBLESTRING:
						{
							FDI_ConvSTRING::CopyTo(&var_value.val.s,&strTemp);
						}
						break;
					default:
						// If var_value type is not string or enumerated type, error will be returned
						return DDL_ENCODING_ERROR;

					}
				}
			}
			break;

		case ENUMERATION_STRING_TAG:
			{
				DDL_PARSE_INTEGER(chunkp, &len, &temp);	// Parse out the enum value

				DESC_REF ref = {0};		// ItemID of the referenced variable

				rc = ddl_parse_desc_ref(chunkp, &len, &ref, depinfo,	env_info, var_needed);

				if (rc != DDL_SUCCESS) {
					return (rc);
				}

				if (out_string) {
					rc = string_enum_lookup(ref.id, temp, &strTemp,	depinfo, env_info, var_needed, pEnumMap);

					if ((rc != DDL_SUCCESS) && (rc != ENUM_NOT_FOUND)) // If the enum was not found, we manufacture one.
					{
						PS_TRACE(L"\n*** error (ddl_parse_string) : ENUMERATION_STRING_TAG could not find string");
						return (rc);
					}

					rc = DDL_SUCCESS;
				}
			}
			break;

		case DICTIONARY_STRING_TAG:
			{
				/*
				* dictionary string. Parse the dictionary string number.
				*/

				DDL_PARSE_INTEGER(chunkp, &len, &temp);

				if (out_string) {

					rc = app_func_get_dict_string(env_info, temp, &strTemp);

					/*
					* If a string was not found, get the default error
					* string.
					*/

					if (rc != DDL_SUCCESS) {
						rc = app_func_get_dict_string(env_info,
							DEFAULT_STD_DICT_STRING, &strTemp);
						if (rc != DDL_SUCCESS) {
							return rc;
						}
					}
				}
			}
			break;

		case ATTR_REF_STRING_TAG:
			{
				DDL_UINT attrib_name = 0;
				DDL_UINT which = 0;		// Not used for strings, ignore
				EXPR xval = {0};

				rc = ddl_parse_byte(chunkp, &len, &attrib_name);
				if (rc)
					return rc;
				
				DESC_REF desc_ref = {0};		// ItemID of the referenced variable

				rc = ddl_parse_desc_ref(chunkp, &len, &desc_ref, depinfo, env_info, var_needed);

				if (out_string && (rc == DDL_SUCCESS))
				{
					rc = get_attribute_value((nsEDDEngine::AttributeName)attrib_name,
						which,
						&desc_ref,
						&xval,
						depinfo, 
						env_info, 
						var_needed);

					if(rc == DDL_SUCCESS)
					{
						strTemp=xval.val.s;
					}

					if ( rc == DDL_DEFAULT_ATTR )
					{
						rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
										&strTemp);
					}

				}

				if (rc != DDL_SUCCESS)
				{
					return (rc);
				}
			}
			break;

		case EXPRESSION_STRING_TAG:
			{
				EXPR expr = {0};
				int  data_valid;
				rc = ddl_expr_choice(chunkp, &len, &expr, depinfo, &data_valid,	env_info, var_needed);
				if(rc)
					return rc;
					
				if (	(expr.type == ASCII)			// If this is a string type...
					||	(expr.type == PACKED_ASCII)
					||	(expr.type == PASSWORD)
					||	(expr.type == EUC)
					||	(expr.type == VISIBLESTRING)
					)
				{
					strTemp.str = expr.val.s.str;
					strTemp.len = expr.val.s.len;
					strTemp.flags = expr.val.s.flags;
				}
				else
					return DDL_ENCODING_ERROR;
			}
			break;
		case ENUM_EXPR_STRING_TAG:
			{
			
				EXPR expr = { 0 };
				
				rc = ddl_eval_expr(chunkp, &len, &expr, depinfo, env_info, var_needed);
			
				if (rc != DDL_SUCCESS) {
					return (rc);
				}

				if ((expr.type == UNSIGNED) || (expr.type == INTEGER))
				{
					DESC_REF ref = { 0 };		// ItemID of the referenced variable

					rc = ddl_parse_desc_ref(chunkp, &len, &ref, depinfo, env_info, var_needed);

					if (rc != DDL_SUCCESS) {
						return (rc);
					}

					if (out_string)
					{
						
						unsigned long enum_val = 0;
						
						if (expr.type == INTEGER)
						{
							enum_val = (unsigned long)expr.val.i;
						}
						else if (expr.type == UNSIGNED)
						{
							enum_val = (unsigned long)expr.val.u;
						}
							
						rc = string_enum_lookup(ref.id, enum_val, &strTemp, depinfo, env_info, var_needed);

						if ((rc != DDL_SUCCESS) && (rc != ENUM_NOT_FOUND)) // If the enum was not found, we manufacture one.
						{
							PS_TRACE(L"\n*** error (ddl_parse_string) : ENUMERATION_STRING_TAG could not find string");
							return (rc);
						}

						rc = DDL_SUCCESS;
					}
				}
				else
				{
					return DDL_BAD_VALUE_TYPE;
				}
				
			}
			break;
		default:
			PS_TRACE(L"\n*** error (ddl_parse_string) : invalid string tag = %d",tag);
			return DDL_ENCODING_ERROR;
		}
		

		if (out_string && (rc == DDL_SUCCESS))	// If everything was fine,
		{										//	allocate a string and place it on the list
			STRING* pNewString = new STRING;

			pNewString->str = strTemp.str;
			pNewString->len = strTemp.len;
			pNewString->flags = strTemp.flags;
		
			listStringList.push_back(pNewString);

			// Clean-up strTemp for use next time around
			strTemp.flags = STRING::DONT_FREE_STRING;	// Set so that ddl_free_string doesn't free the string.
														// The string is being used by pNewString now
			ddl_free_string(&strTemp);
		}
	}

	if (out_string)
	{
		STRING *pString = NULL;

		// Handle simple String
		if (listStringList.size() == 1)
		{								
			pString = listStringList.front();
			listStringList.pop_front();			

			out_string->str = pString->str;
			out_string->len = pString->len;
			out_string->flags = pString->flags;

			delete pString;
		}
		else if (listStringList.size() == 0)
		{
			out_string->str = NULL;
			out_string->len = 0;
			out_string->flags = STRING::FREE_STRING;
		}
		else
		{
			// Count the size of the complete string
			int iTotalLen = 0;			
			std::list<STRING *>::iterator it;
			for (it = listStringList.begin(); it != listStringList.end(); it++)
			{
				iTotalLen += (*it)->len;			
			}

			// Allocate memory for the complete string
			out_string->str = (wchar_t*)malloc((iTotalLen+1) * sizeof(wchar_t));
			out_string->str[0] = NULL;
			out_string->len = (unsigned short)iTotalLen;
			out_string->flags = STRING::FREE_STRING;

			// Concatenate the StringList
			while (listStringList.size() > 0)
			{
				pString = listStringList.front();
				listStringList.pop_front();				
				wcscat(out_string->str, pString->str);

				ddl_free_string(pString);	// Free each string part
				delete pString;
			}
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddl_string_choice
 *	ShortDesc: Choose the correct string from a binary.
 *
 *	Description:
 *		ddl_string_choice will parse the binary for an string,
 *		according to the current conditionals (if any).  The value of
 *		the string is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		string - pointer to a STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		string - pointer to a STRING structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond() and ddl_parse_string().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_string_choice(
unsigned char **chunkp,
DDL_UINT       *size,
STRING         *string,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK           val;	/* Stores the binary found by this conditional */

#ifdef DDSTEST
	TEST_FAIL(DDL_STRING_CHOICE);
#endif

	val.chunk = 0;
	if (data_valid) {
		*data_valid = FALSE;
	}

	rc = ddl_cond(chunkp, size, &val, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		return rc;
	}


	/*
	 * A chunk was found and a value is requested.
	 */

	if (string && val.chunk) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG(data_valid != NULL);

		/*
		 * We have finally gotten to the actual value!  Parse it.
		 */

		rc = ddl_parse_string(&val.chunk, &val.size, string, depinfo, env_info, var_needed);

		
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		*data_valid = TRUE;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: eval_string
 *	ShortDesc: Evaluate a string
 *
 *	Description:
 *		eval_string parses a chunk of binary data and loads string
 *
 *	Inputs:
 *		chunk - pointer to the binary data
 *		size - size of binary data
 *		string - space for parsed string
 *		depinfo - dependency information
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		string is loaded with the string
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		return codes from ddl_string_choice(), ddl_shrink_depinfo()
 *		DDL_DEFAULT_ATTR - invalid string
 *		DDL_SUCCESS - chunk was parsed and string was loaded successfully
 *
 *	Author: Jon Westbrock
 *
 *********************************************************************/
int
eval_string(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	STRING*			string = static_cast<STRING*>(voidP);

	int             rc;	/* return code */
	int             valid;	/* data valid flag */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_STRING);
#endif


	valid = 0;

	if (string) {
		ddl_free_string(string);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	rc = ddl_string_choice(&chunk, &size, string, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		return rc;
	}

	if (string && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}

