﻿<Method>
{
long lx, ly, lz;
DD_STRING result;
int errorLevel;
lx = ReadCommand(testCommand1);
ly = ReadCommand(testCommand1);
lz = get_rspcode_string_by_id(testCommand1, ly, result);
errorLevel = 0;
LOG_MESSAGE(errorLevel, literal_string(xxx));
_ERROR(literal_string(xxx));
errorLevel = 1;
_WARNING(literal_string(xxx));
errorLevel = 2;
_TRACE(literal_string(xxx));
send_on_exit();
display_bitenum(var_bit_enumerated);
lz = WriteCommand(testCommand2);
}
</Method>
<Function>
IBuiltinSupport SendCommand() is being called
IBuiltinSupport SendCommand with returned value 0 () is returned
Function Name: _REINVOKE_ReadWriteCommand
  Number Of Input Parameters: 1
    Param 1 Type: RUL_INT
    Param 1 Value: 68
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_ReadWriteCommand
IBuiltinSupport SendCommand() is being called
IBuiltinSupport SendCommand with returned value 0 () is returned
Function Name: _REINVOKE_ReadWriteCommand
  Number Of Input Parameters: 1
    Param 1 Type: RUL_INT
    Param 1 Value: 68
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_ReadWriteCommand
Function Name: _REINVOKE_get_rspcode_string_by_id
  Number Of Input Parameters: 3
    Param 1 Type: RUL_INT
    Param 1 Value: 68
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_CHARPTR
    Param 3 Value: no_command_specific_errors
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_get_rspcode_string_by_id
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: This is LOG_MESSAGE builtin with errorlevel = %{errorLevel}.
End of Function literal_string
IBuiltinSupport LogMessage() is being called
IBuiltinSupport LogMessage() is returned
Function Name: LOG_MESSAGE
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 0
    Param 2 Type: RUL_DD_STRING
    Param 2 Value: This is LOG_MESSAGE builtin with errorlevel = %{errorLevel}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function LOG_MESSAGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: This is _ERROR builtin with errorlevel = %{errorLevel}.
End of Function literal_string
IBuiltinSupport LogMessage() is being called
IBuiltinSupport LogMessage() is returned
Function Name: _ERROR
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: This is _ERROR builtin with errorlevel = %{errorLevel}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function _ERROR
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: This is _WARNING builtin with errorlevel = %{errorLevel}.
End of Function literal_string
IBuiltinSupport LogMessage() is being called
IBuiltinSupport LogMessage() is returned
Function Name: _WARNING
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: This is _WARNING builtin with errorlevel = %{errorLevel}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function _WARNING
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: This is _TRACE builtin with errorlevel = %{errorLevel}.
End of Function literal_string
IBuiltinSupport LogMessage() is being called
IBuiltinSupport LogMessage() is returned
Function Name: _TRACE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: This is _TRACE builtin with errorlevel = %{errorLevel}.
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function _TRACE
Function Name: send_on_exit
  Number Of Input Parameters: 0
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function send_on_exit
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 159, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: 64 bit ENUMERATED,  Value: 1
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 159, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: 64 bit ENUMERATED,  Value: 1
IBuiltinSupport AcknowledgementRequest() is being called with "Bit 1"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: _REINVOKE_display_bitenum
  Number Of Input Parameters: 1
    Param 1 Type: RUL_INT
    Param 1 Value: 159
  Returned Param Type: RUL_NULL
  Returned Param Value doesn't exist
End of Function _REINVOKE_display_bitenum
IBuiltinSupport SendCommand() is being called
IBuiltinSupport SendCommand with returned value 3 () is returned
Function Name: _REINVOKE_ReadWriteCommand
  Number Of Input Parameters: 1
    Param 1 Type: RUL_INT
    Param 1 Value: 69
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_ReadWriteCommand
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><lx>0,</lx><ly>0,</ly><lz>0,</lz><result>no_command_specific_errors</result><errorLevel>2,</errorLevel></SymbolTable>
IBuiltinSupport AbortRequest() is being called with "WriteCommand: Aborting due to device not responding."
IBuiltinSupport AbortRequest with returned value 0 () is returned
<Abort Method>
Method Process Aborted
</Abort Method>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport Is Done. MI Engine OnMethodInterpreterComplete() is being called with "method aborted"
