#include "stdafx.h"
#pragma warning (disable : 4786)
#include "InterpretedVisitor.h"
#include "Program.h"
#include "Declarations.h"
#include "StatementList.h"
#include "Statement.h"
#include "SelectionStatement.h"
#include "SwitchStatement.h"
#include "AssignmentStatement.h"
#include "BreakStatement.h"
#include "ReturnStatement.h"
#include "ContinueStatement.h"
#include "Variable.h"
#include "Expression.h"
#include "INTER_VARIANT.h"
#include "SymbolTable.h"
#include "PrimaryExpression.h"
#include "CompoundExpression.h"
#include "IterationStatement.h"
#include "IterationDoWhile.h"
#include "IterationFor.h"
#include "CompoundStatement.h"
#include "ELSEStatement.h"
#include "CaseStatement.h"
#include "INTER_SAFEARRAY.h"
#include "assert.h"
#include "ArrayExpression.h"
#include "RuleServiceStatement.h"
#include "OMServiceExpression.h"
#include "FunctionExpression.h"
#include "IFExpression.h"

#include "MEE.h" //Vibhor 070705: Added
#include "ddbDevice.h"
#include "climits"

//Anil August 26 2005 
#ifndef RETURNCODE
/* error codes */
   #define RETURNCODE  int

   #define SUCCESS			(0)
   #define FAILURE			(1)
#endif

/* Define for the maximum number of loops that can be run */
#define MAX_LOOPS	INT_MAX
typedef vector<INTER_VARIANT> INTERVARIANT_VECTOR;

CInterpretedVisitor::CInterpretedVisitor()
{
	m_IsLValue = false;
	// memset(m_fnTable,0,BYTE);//WS - 21may07 - wrong size
	memset(m_fnTable,0,MAX_BYTE*sizeof(PFN_INTERPRETER));// right size
		// WS - 9apr07 - 2005 checkin - fully define functions
	m_fnTable[RUL_PLUS_PLUS]		= &CInterpretedVisitor::uplusplus;
	m_fnTable[RUL_MINUS_MINUS]		= &CInterpretedVisitor::uminusminus;
	m_fnTable[RUL_PRE_PLUS_PLUS]	= &CInterpretedVisitor::upreplusplus;
	m_fnTable[RUL_PRE_MINUS_MINUS]	= &CInterpretedVisitor::upreminusminus;
	m_fnTable[RUL_UPLUS]			= &CInterpretedVisitor::uplus;
	m_fnTable[RUL_UMINUS]			= &CInterpretedVisitor::uminus;
    m_fnTable[RUL_BIT_AND]			= &CInterpretedVisitor::bit_and;
    m_fnTable[RUL_BIT_OR]			= &CInterpretedVisitor::bit_or;
    m_fnTable[RUL_BIT_XOR]			= &CInterpretedVisitor::bitxor;
    m_fnTable[RUL_BIT_NOT]			= &CInterpretedVisitor::bitnot;
    m_fnTable[RUL_BIT_RSHIFT]		= &CInterpretedVisitor::bitrshift;
    m_fnTable[RUL_BIT_LSHIFT]		= &CInterpretedVisitor::bitlshift;
	m_fnTable[RUL_PLUS]				= &CInterpretedVisitor::add;
	m_fnTable[RUL_MINUS]			= &CInterpretedVisitor::sub;
	m_fnTable[RUL_MUL]				= &CInterpretedVisitor::mul;
	m_fnTable[RUL_DIV]				= &CInterpretedVisitor::div;
	m_fnTable[RUL_MOD]				= &CInterpretedVisitor::mod;
	m_fnTable[RUL_EXP]				= &CInterpretedVisitor::exp;
	m_fnTable[RUL_NOT_EQ]			= &CInterpretedVisitor::neq;
	m_fnTable[RUL_LT]				= &CInterpretedVisitor::lt;
	m_fnTable[RUL_GT]				= &CInterpretedVisitor::gt;
	m_fnTable[RUL_EQ]				= &CInterpretedVisitor::eq;
	m_fnTable[RUL_GE]				= &CInterpretedVisitor::ge;
	m_fnTable[RUL_LE]				= &CInterpretedVisitor::le;
	m_fnTable[RUL_LOGIC_AND]		= &CInterpretedVisitor::land;
	m_fnTable[RUL_LOGIC_OR]			= &CInterpretedVisitor::lor;
	m_fnTable[RUL_LOGIC_NOT]		= &CInterpretedVisitor::lnot;
	m_fnTable[RUL_RPAREN]			= &CInterpretedVisitor::rparen;

	m_fnTable[RUL_ASSIGN]			= &CInterpretedVisitor::assign;
	m_fnTable[RUL_COMMA]			= &CInterpretedVisitor::comma;
	m_fnTable[RUL_PLUS_ASSIGN]		= &CInterpretedVisitor::plusassign;
	m_fnTable[RUL_MINUS_ASSIGN]		= &CInterpretedVisitor::minusassign;
	m_fnTable[RUL_DIV_ASSIGN]		= &CInterpretedVisitor::divassign;
	m_fnTable[RUL_MOD_ASSIGN]		= &CInterpretedVisitor::modassign;
	m_fnTable[RUL_MUL_ASSIGN]		= &CInterpretedVisitor::mulassign;

    m_fnTable[RUL_BIT_AND_ASSIGN]	= &CInterpretedVisitor::bitandassign;
    m_fnTable[RUL_BIT_OR_ASSIGN]	= &CInterpretedVisitor::bitorassign;
    m_fnTable[RUL_BIT_XOR_ASSIGN]	= &CInterpretedVisitor::bitxorassign;
	m_fnTable[RUL_BIT_RSHIFT_ASSIGN]	= &CInterpretedVisitor::rshiftassign;
	m_fnTable[RUL_BIT_LSHIFT_ASSIGN]	= &CInterpretedVisitor::lshiftassign;
}

CInterpretedVisitor::~CInterpretedVisitor()
{

}

_INT32 CInterpretedVisitor::visitArrayExpression(
			CArrayExpression* pArrExp,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	assert(pvar!=0);

	_INT32 i32Idx			= pArrExp->GetToken()->GetSymbolTableIndex();
	CVariable* pVariable	= pSymbolTable->GetAt(i32Idx);
	INTER_SAFEARRAY* prgsa	= pVariable->GetValue().GetValue().prgsa;

	//evaluate the expressions...
	EXPR_VECTOR* pvecExpressions = pArrExp->GetExpressions();
	size_t i32Count = pvecExpressions->size();
	assert(i32Count == 1);

	//get array index from expression
	INTER_VARIANT var;
	var.Clear();
	((*pvecExpressions)[0])->Execute(this, pSymbolTable, &var, pvecErrors);

	if ((int)var >= prgsa->GetNumberOfElements())
	{
		//error -- array index out of range
		throw(C_UM_ERROR_INDEX_OUT_OF_RANGE);
	}
	vector<_INT32> vecDims;
	prgsa->GetDims(&vecDims);
	//get element size
	_INT32 i32mem = prgsa->GetElementSize();
	//get element memory location
	_INT32 i32loc = i32mem * (int)var;

	if(m_IsLValue)
	{
		m_IsLValue = false;

		//Write L-value to token
		if (vecDims.size() == 1)
		{
			//one dimension
			prgsa->SetElement(i32loc, pvar);
		}
		else
		{
			//multiple array dimensions
			prgsa->SetDimension((int)var, pvar);
		}
	}
	else
	{
		pvar->Clear();

		//get R-value from token
		if (vecDims.size() == 1)
		{
			//one dimension
			prgsa->GetElement(i32loc, pvar);
		}
		else
		{
			//multiple array dimensions
			prgsa->GetDimension((int)var, pvar);
		}
	}
	return VISIT_NORMAL;
}

//Execute the expression
//Assign the value from the expression to the variable in the Symbol Table.
_INT32 CInterpretedVisitor::visitAssignment(
			CAssignmentStatement* pAssStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CToken* pVariable = 0;
	CExpression* pExp = 0;
	CExpression* pArray = 0;
	CExpression* pDDExp = 0;
	_INT32 iRetValue = VISIT_NORMAL; 

	if(pAssStmt)
	{
		pVariable = pAssStmt->GetVariable();
		pExp	= pAssStmt->GetExpression();
		pArray = pAssStmt->GetArrayExp();
		pDDExp = pAssStmt->GetComplexDDExp();
	}
	else
	{
		//error -- no assign statement
		throw(C_UM_ERROR_METHODERROR);
	}

	INTER_VARIANT var;

/*Vibhor 110205: Start of Code*/
	if(pVariable)
	{
		switch(pVariable->GetSubType())
		{
			case RUL_CHAR_DECL:
				{
		//			var = (char)' ';  //WS:EPM 10aug07::Leave this as RUL_NULL because it could be a RUL_CHAR or a SafeArray of RUL_CHAR's
				}
				break;
            case RUL_LONG_LONG_DECL:
				{
                    var = (INT64)0;
				}
				break;
			case RUL_UNSIGNED_LONG_LONG_DECL:
				{
					var = (UINT64)0;
				}
				break;
			// Walt EPM 08sep08 - start insert
			case RUL_SHORT_INTEGER_DECL:
				{
					var = (short)0;
				}
				break;
			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
				{
					var = (wchar_t)0;
				}
				break;
			case RUL_UNSIGNED_INTEGER_DECL:
				{
					var = (unsigned int)0;
				}
				break;
			// Walt EPM 08sep08 - end insert
			case RUL_INTEGER_DECL:
			case RUL_LONG_DECL:
			case RUL_TIMET_DECL:
				{
                    var = (long)0;
				}
				break;
			case RUL_REAL_DECL:
				{
					var = (float)0.0;
				}
				break;
			case RUL_DOUBLE_DECL:
				{
					var = (double)0.0;//WS:EPM 10aug07
				}
				break;
			case RUL_BOOLEAN_DECL:
				{
					var = (bool)false;
				}
				break;
			case RUL_STRING_DECL:
				{
					var = (char *)"";
				}
				break;
			case RUL_ARRAY_DECL:
				{
					var = (char *)"";
				}
				break;
			//Added By Anil June 15 2005 --starts here
			case RUL_DD_STRING_DECL :
				{
					var = (wchar_t *)_T("");
				}
				break;
			//Added By Anil June 15 2005 --Ends here
			case RUL_UNSIGNED_CHAR_DECL :
				{
					//var = (char *)"";  //WS:EPM 10aug07::Leave this as RUL_NULL because it could be a RUL_UNSIGNED_CHAR or a SafeArray of RUL_UNSIGNED_CHAR's
				}
				break;
		


			default:
				break; //var.varType == RUL_NULL;
		} 
	}
/*Vibhor 110205: End of Code*/
	if(pExp)
	{
		//Read R-value
		try
		{
			iRetValue = pExp->Execute(this, pSymbolTable, &var, pvecErrors);
			switch(iRetValue)
			{
			case VISIT_RETURN:
			case VISIT_ABORT:
				return iRetValue;
				break;
			}
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pExp);
		}
	}
	else
	{
		//error -- no expression in assingment statement
		throw(C_UM_ERROR_METHODERROR);
	}

	if(pVariable)
	{
		//L-value is local variable value
		_INT32 nIdx = -1;
		CVariable* pStore=0;

		nIdx = pVariable->GetSymbolTableIndex();
// commented out code removed 17jul07 
		pStore = pSymbolTable->GetAt(nIdx);
		if( pStore )
		{
			//Read R-value
			INTER_VARIANT var1;
			if (pAssStmt->GetAssignmentType() != RUL_ASSIGN)
			{
				var1 = pStore->GetValue();
			}

			INTER_VARIANT returnedVar = pStore->GetValue();

			//Write L-value which is local variable value to token
			switch (pAssStmt->GetAssignmentType())
			{
				case RUL_ASSIGN:
					{
						returnedVar = var;
						break;
					}
				case RUL_PLUS_ASSIGN:
					{
						returnedVar = var1 + var;
						break;
					}
				case RUL_MINUS_ASSIGN:
					{
						returnedVar = var1 - var;
						break;
					}
				case RUL_DIV_ASSIGN:
					{
						returnedVar = var1 / var;
						break;
					}
				case RUL_MUL_ASSIGN:
					{
						returnedVar = var1 * var;
						break;
					}
				case RUL_MOD_ASSIGN:
					{
						returnedVar = var1 % var;					
						break;
					}			
				case RUL_BIT_AND_ASSIGN:
					{
						returnedVar = var1 & var;					
						break;
					}
				case RUL_BIT_OR_ASSIGN:
					{
						returnedVar = var1 | var;					
						break;
					}
				case RUL_BIT_XOR_ASSIGN:
					{
						returnedVar = var1 ^ var;					
						break;
					}
				case RUL_BIT_RSHIFT_ASSIGN:
					{
						returnedVar = var1 >> var;					
						break;
					}
				case RUL_BIT_LSHIFT_ASSIGN:
					{
						returnedVar = var1 << var;					
						break;
					}
			
			}//end switch

			//Write L-value to token
			pStore->GetValue() = returnedVar;
		}//end of if statement
		else
		{
			// else - NULL pStore
			throw(C_UM_ERROR_METHODERROR);
		}

	}//endif pVariable
	else if(pArray)
	{
		try
		{
			//L-value is value array value
			m_IsLValue = true;
			iRetValue = pArray->Execute(this,pSymbolTable,&var,pvecErrors,pAssStmt->GetAssignmentType()); //Anil August 26 2005 //for Fixing a[10] += 5;
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pArray);
		}
	}
	//Added By Anil August 23 2005 --starts here 
	//This below is handle if it is of type	ComplexDD expression
	else if(pDDExp)
	{
		try
		{
			//L-value is DD item value
			m_IsLValue = true;		
			iRetValue = pDDExp->Execute(this,pSymbolTable,&var,pvecErrors,pAssStmt->GetAssignmentType());
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pDDExp);
		}
	}
	//Added By Anil August 23 2005 --Ends here
	else
	{
		//error -- no variable in assingment statement
		throw(C_UM_ERROR_METHODERROR);
	}
	if (pvar)
		*pvar = var;

	return iRetValue;
}

_INT32 CInterpretedVisitor::visitBreakStatement(
			CBreakStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return VISIT_BREAK;
}

_INT32 CInterpretedVisitor::visitReturnStatement(
			CReturnStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	//this is a Double check that Return statement is only from the Called method..
	//ie  method which does not sit on the Menu
	if(false == m_bIsRoutine)
	{
		//If so return ; ie Do not execute the return statement
	return VISIT_RETURN;
	}
	CExpression* pExp = 0;
	pExp	= pItnStmt->GetExpression();
	//if pExp, then it is return Void statement ..so no need to Execute this
	if(NULL == pExp)
	{
		return VISIT_RETURN;//This is the case of Void return statement ie return;

	}
	//Other wise we may need to Execute this
	else
	{
		INTER_VARIANT var;

		int iSizeSymbTa = pSymbolTable->GetSymbTableSize();
		// removed WS:EPM 17jul07 var.varType = RUL_NULL;
		int iRetVarIndex = -1;
		//Now Loop through the Symbol table and Get the variable 
		//which we pushed as the Return variable in the starting
		//This variable will have m_bIsReturnToken as true..
		//None other var in symbol table should have this flag set

		for(int iCount = 0; iCount < iSizeSymbTa; iCount++)
		{
			CVariable *pCVariable = pSymbolTable->GetAt(iCount);
			if ((NULL != pCVariable) && pCVariable->m_bIsReturnToken)
			{
				//Once we get that Fill its variable type depending on the Return type declared in the method
				iRetVarIndex = iCount;

				switch(pCVariable->GetSubType())
				{
					case RUL_CHAR_DECL:
						{
							var = (char)' ';
						}
						break;
					case RUL_LONG_LONG_DECL:
						{
							var = (INT64)0;
						}
						break;
					case RUL_UNSIGNED_LONG_LONG_DECL:
						{
							var = (UINT64)0;
						}
						break;
					// Walt EPM 08sep08 - start insert
					case RUL_UNSIGNED_SHORT_INTEGER_DECL:
						{
							var = (unsigned short)0;
						}
						break;
					case RUL_SHORT_INTEGER_DECL:
						{
							var = (short)0;
						}
						break;
					case RUL_UNSIGNED_INTEGER_DECL:
						{
							var = (unsigned int)0;
						}
						break;
					// Walt EPM 08sep08 - end insert
					case RUL_INTEGER_DECL:
					case RUL_LONG_DECL:
					case RUL_TIMET_DECL:
						{
                            var = (long)0;
						}
						break;
					case RUL_REAL_DECL:
						{
							var = (float)0.0;
						}
						break;
					case RUL_DOUBLE_DECL:
						{
							var = (double)0.0;//WS:EPM 10aug07
						}
						break;
					case RUL_BOOLEAN_DECL:
						{
							var = (bool)false;
						}
						break;
					case RUL_DD_STRING_DECL :
						{
							var=(wchar_t *)_T("");
						}
						break;

					default:
						break; //var.varType == RUL_NULL;
				} //End of switch

				break;
			}//end of NULL != pCVariable 
		}//End of for loop

		try
		{
			//Execute this return statement and get the value out of that and then store that in the Return variable
			_INT32 iRetValue = pExp->Execute(this, pSymbolTable, &var, pvecErrors);

			if (iRetValue == VISIT_NORMAL)
			{
				if (iRetVarIndex >= 0)
				{
					//store returned value to symbol table for method calling method
					CVariable *pCVariable = pSymbolTable->GetAt(iRetVarIndex);
					pCVariable->GetValue() = var;
				}

				if (m_pMEE->GetIsMethReturnType())
				{
					//save method returned variable value to MI caller
					m_pMEE->WriteMethodRetunedValue(&var);
				}
			}

			return VISIT_RETURN;
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pExp);
		}
	}

	return VISIT_RETURN;
}

_INT32 CInterpretedVisitor::visitContinueStatement(
			CContinueStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return VISIT_CONTINUE;
}

_INT32 CInterpretedVisitor::visitIterationStatement(
			CIterationStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;
	CStatement* pExpStmt=0;
	CStatement* pStmt=0;

	if(pItnStmt)
	{
		if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
		{
			pExp = pItnStmt->GetExpression();
		}
		else
		{
			pExpStmt = pItnStmt->GetExpressionStatement();
		}
		pStmt = pItnStmt->GetStatement();
	}
	else
	{
		//error -- no Iteration statement.
		throw(C_UM_ERROR_METHODERROR);
	}

	INTER_VARIANT var1,var2;
	if(pExp || pExpStmt)
	{
		int iRetValue = VISIT_NORMAL;
		unsigned long ulongLoopCount = 0;

		while(true)
		{
			ulongLoopCount++;

			if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
			{
				if (ulongLoopCount >= MAX_LOOPS)
				{
					ADD_EXEC_ERROR(C_UM_ERROR_DDS_PARAM, pExp);
					break;
				}

				iRetValue = pExp->Execute(this,pSymbolTable,&var1, pvecErrors);
			}
			else
			{
				if (ulongLoopCount >= MAX_LOOPS)
				{
					ADD_EXEC_ERROR(C_UM_ERROR_DDS_PARAM, pExpStmt);
					break;
				}

				iRetValue = pExpStmt->Execute(this,pSymbolTable,&var1, pvecErrors);
			}
			
			if (iRetValue == VISIT_RETURN)
			{
				return iRetValue;
			}

			if (!(iRetValue && (int)var1 && pStmt))
			{
				break;
			}

			try
			{
				var1.Clear();
				iRetValue = pStmt->Execute(this, pSymbolTable, &var2, pvecErrors);
				switch(iRetValue)
				{
					case VISIT_BREAK:
						return VISIT_NORMAL;
						break;
					case VISIT_CONTINUE:
						continue;
						break;
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iRetValue;
						break;
				}
				var2.Clear();
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pStmt);
			}
		}
		return iRetValue;
	}
	else
	{
		//error -- no expression in while statement
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pItnStmt);
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitIterationStatement(
			CIterationDoWhileStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;
	CStatement* pExpStmt=0;
	CStatement* pStmt=0;

	if(pItnStmt)
	{
		if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
		{
			pExp = pItnStmt->GetExpression();
		}
		else
		{
			pExpStmt = pItnStmt->GetExpressionStatement();
		}
		
		pStmt = pItnStmt->GetStatement();
	}
	else
	{
		//error -- no Iteration statement.
		throw(C_UM_ERROR_METHODERROR);
	}

	INTER_VARIANT var1,var2;
	if(pExp || pExpStmt)
	{
		int iRetValue = VISIT_NORMAL;
		unsigned long ulongLoopCount = 0;

		do
		{
			ulongLoopCount++;
			if (ulongLoopCount >= MAX_LOOPS)
			{
				ADD_EXEC_ERROR(C_UM_ERROR_DDS_PARAM, pStmt);
				break;
			}

			try
			{
				var1.Clear();
				iRetValue = pStmt->Execute(this, pSymbolTable, &var2, pvecErrors);
				switch(iRetValue)
				{
					case VISIT_BREAK:
						return VISIT_NORMAL;
						break;
					case VISIT_CONTINUE:
						continue;
						break;
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iRetValue;
						break;
				}
				var2.Clear();
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pStmt);
			}

			if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
			{
				try
				{
					iRetValue = pExp->Execute(this,pSymbolTable,&var1, pvecErrors);
				}
				catch(_INT32 error)
				{
					ADD_EXEC_ERROR(error, pExp);
				}
			}
			else
			{
				try
				{
					iRetValue = pExpStmt->Execute(this,pSymbolTable,&var1, pvecErrors);
				}
				catch(_INT32 error)
				{
					ADD_EXEC_ERROR(error, pExpStmt);
				}
			}
			
			switch(iRetValue)
			{
				case VISIT_RETURN:
				case VISIT_ABORT:
					return iRetValue;
					break;
			}

			if (iRetValue && (int)var1 && pStmt)
			{
				continue;
			}
			else
			{
				break;
			}
		}while( true);
		return iRetValue;
	}
	else
	{
		//error -- no expression in while statement
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pItnStmt);
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitIterationStatement(
			CIterationForStatement* pItnStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;		//check expression
	CStatement* pExpStmt=0;		//check statement
	CStatement* pStmt=0;		//for loop statement
	CStatement* pInitStmt = 0;	//init statement
	CExpression* pInitExp=0;	//init expression
	CStatement* pIncrStmt=0;	//increment statement
	CExpression* pIncrExp=0;	//increment expression

	if(pItnStmt)
	{
		if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
		{
			pExp = pItnStmt->GetExpression();
		}
		else
		{
			pExpStmt = pItnStmt->GetExpressionStatement();
		}
		pStmt = pItnStmt->GetStatement();
		pInitStmt = pItnStmt->GetInitializationStatement();
		pInitExp = pItnStmt->GetInitializationExpression();
		pIncrStmt = pItnStmt->GetIncrementStatement();
		pIncrExp = pItnStmt->GetIncrementExpression();
	}
	else
	{
		//error -- no Iteration statement.
		throw(C_UM_ERROR_METHODERROR);
	}

	INTER_VARIANT var1,var2,var3,var4;

	if(pExp || pExpStmt)
	{
		//for loop initialization
		if(pInitStmt)
		{
			pInitStmt->Execute(this, pSymbolTable, &var3, pvecErrors);
		}
		else if (pInitExp)
		{
			pInitExp->Execute(this, pSymbolTable, &var3, pvecErrors);
		}

		int iLoopVar=0;
		int iRetValue = VISIT_NORMAL;
		unsigned long ulongLoopCount = 0;

		for(;;)
		{
			ulongLoopCount++;

			if (iLoopVar != 0)
			{
				if (pIncrStmt)
				{
					if (ulongLoopCount >= MAX_LOOPS)
					{
						ADD_EXEC_ERROR(C_UM_ERROR_DDS_PARAM, pIncrStmt);
					}

					pIncrStmt->Execute(this,pSymbolTable,&var4, pvecErrors);
				}
				else if (pIncrExp)
				{
					if (ulongLoopCount >= MAX_LOOPS)
					{
						ADD_EXEC_ERROR(C_UM_ERROR_DDS_PARAM, pIncrExp);
					}

					pIncrExp->Execute(this,pSymbolTable,&var4, pvecErrors);
				}

			}
			iLoopVar++;

			if (pItnStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
			{
				iRetValue = pExp->Execute(this,pSymbolTable,&var1, pvecErrors);
			}
			else
			{
				iRetValue = pExpStmt->Execute(this,pSymbolTable,&var1, pvecErrors);
			}
			
			if (iRetValue == VISIT_RETURN)
			{
				return iRetValue;
			}

			if (!(iRetValue && (int)var1 && pStmt))
			{
				break;
			}

			try
			{
				var1.Clear();
				var3.Clear();
				var4.Clear();
				iRetValue = pStmt->Execute(this, pSymbolTable, &var2, pvecErrors);
				switch(iRetValue)
				{
					case VISIT_BREAK:
						return VISIT_NORMAL;
						break;
					case VISIT_CONTINUE:
						continue;
						break;
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iRetValue;
						break;
				}
				var2.Clear();
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pStmt);
			}
		}
		return iRetValue;
	}
	else
	{
		//error -- no expression in while statement
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pItnStmt);
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitSelectionStatement(
			CSelectionStatement* pSelStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;
	CStatement* pStmt=0;
	CELSEStatement* pElse=0;

	if(pSelStmt)
	{
		pExp = pSelStmt->GetExpression();
		pStmt = pSelStmt->GetStatement();
		pElse = pSelStmt->GetELSEStatement();
	}
	else
	{
		//error -- no selection statement..
		throw(C_UM_ERROR_METHODERROR);
	}

	INTER_VARIANT var1,var2;
	if(pExp)
	{
		_INT32 iRetValue;

		try
		{
			// Gowtham 260306: Start of Code Modifications
			// Split the if condition to see if the return value is VISIT_RETURN.
			iRetValue = pExp->Execute(this,pSymbolTable,&var1, pvecErrors);
			switch (iRetValue)
			{
			case VISIT_RETURN:
			case VISIT_ABORT:
				return iRetValue;
				break;
			case VISIT_BREAK:
			case VISIT_CONTINUE:
			case VISIT_NORMAL:
			case VISIT_SCOPE_VAR:
			default:
				break;
			}
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pExp);
		}

		if(iRetValue && (bool)var1 && pStmt)
		{
			try
			{
				var1.Clear();
				int iVisitReturnType = pStmt->Execute(this, pSymbolTable, &var2, pvecErrors);
				switch(iVisitReturnType)
				{
					case VISIT_BREAK:
					case VISIT_CONTINUE:
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iVisitReturnType;
						break;
				}
				var2.Clear();
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pStmt);
			}
		}
		else if(false == (bool)var1)
		{
			if(pElse)
			{
				try
				{
					int iVisitReturnType = pElse->Execute(this, pSymbolTable, &var2, pvecErrors);
					switch(iVisitReturnType)
					{
						case VISIT_BREAK:
						case VISIT_CONTINUE:
						case VISIT_RETURN:
						case VISIT_ABORT:
							return iVisitReturnType;
							break;
					}
				}
				catch(_INT32 error)
				{
					ADD_EXEC_ERROR(error, pElse);
				}
			}
			var2.Clear();
		}
		else
		{
			//either there is no statement or execute failed.
			ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pExp);
		}
	}
	else
	{
		//error -- no expression in if statement...
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitSwitchStatement(
			CSwitchStatement* pSelStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;
	CStatement* pExpStmt=0;
	CStatement* pStmt=0;
	CCASEStatement* pCaseStatement=0;
	int iNumberOfCasesPresent=0;
	_BOOL bIsDefaultPresent=false;

	if(pSelStmt)
	{
		if (pSelStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
		{
			pExp = pSelStmt->GetExpression();
		}
		else
		{
			pExpStmt = pSelStmt->GetExpressionStatement();
		}
		pStmt = pSelStmt->GetStatement();
		iNumberOfCasesPresent = pSelStmt->GetNumberOfCaseStatements();
		bIsDefaultPresent=pSelStmt->IsDefaultPresent();
	}
	else
	{
		//error -- no selection statement..
		throw(C_UM_ERROR_METHODERROR);
	}

	if ((iNumberOfCasesPresent <= 0) && (bIsDefaultPresent == false))
	{
		return VISIT_NORMAL;

	}

	INTER_VARIANT var1,var2,var3;
	if(pExp || pExpStmt)
	{
		int iRetValue;
		if (pSelStmt->GetExpressionNodeType() == NODE_TYPE_EXPRESSION)
		{
			iRetValue = pExp->Execute(this,pSymbolTable,&var1, pvecErrors);
		}
		else
		{
			iRetValue = pExpStmt->Execute(this,pSymbolTable,&var1, pvecErrors);
		}		

		switch(iRetValue)
		{
			case VISIT_RETURN:
			case VISIT_ABORT:
				return iRetValue;
				break;
		}

		_BOOL bMatchFound = false;

		for (int iLoopVar=0;iLoopVar <iNumberOfCasesPresent;iLoopVar++)
		{
			pCaseStatement = pSelStmt->GetCaseStatement(iLoopVar);

			CExpression*pCaseExp = pCaseStatement->GetExpression();

			pCaseExp->Execute(this, pSymbolTable, &var2, pvecErrors);

			if ( ((int)var1 == (int)var2) || bMatchFound)
			{
				try
				{
					bMatchFound = true;
					int iVisitReturnType 
							= pCaseStatement->Execute (this,pSymbolTable,&var3, pvecErrors);
					switch(iVisitReturnType)
					{
						case VISIT_BREAK:
							return VISIT_NORMAL;
							break;
						case VISIT_CONTINUE:
							return VISIT_CONTINUE;
							break;
						case VISIT_RETURN:
						case VISIT_ABORT:
							return iVisitReturnType;
							break;
					}
				}
				catch(_INT32 error)
				{
					ADD_EXEC_ERROR(error, pCaseStatement);
				}
			}
			var2.Clear ();
		}
		if (bIsDefaultPresent)
		{
			try
			{
				pCaseStatement = pSelStmt->GetDefaultStatement();

				int iVisitReturnType 
						= pCaseStatement->Execute (this,pSymbolTable,&var3, pvecErrors);
				switch(iVisitReturnType)
				{
					case VISIT_BREAK:
						return VISIT_NORMAL;
						break;
					case VISIT_CONTINUE:
						return VISIT_CONTINUE;
						break;
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iVisitReturnType;
						break;
				}
				var2.Clear ();
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pCaseStatement);
			}
		}
		return VISIT_NORMAL;
	}
	else
	{
		//error -- no expression in if statement...
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitELSEStatement(
			CELSEStatement* pELSE,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CStatement* pStmt=0;
	INTER_VARIANT var;
	if(pELSE)
	{
		pStmt = pELSE->GetStatement();
	}
	else
	{
		//error -- no else statement.
		throw(C_UM_ERROR_METHODERROR);
	}
	if(pStmt)
	{
		return pStmt->Execute(this,pSymbolTable,&var, pvecErrors);
	}
	else
	{
		//error -- no Statement in Else
		return VISIT_ERROR;	//just return, don't stop
	}

	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitCASEStatement(
			CCASEStatement* pCase,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CStatementList* pStmtList=0;
	INTER_VARIANT var;
	if(pCase)
	{
		pStmtList = pCase->GetStatement();
	}
	else
	{
		//error -- no else statement.
		throw(C_UM_ERROR_METHODERROR);
	}
	if(pStmtList)
	{
		return pStmtList->Execute(this,pSymbolTable,&var, pvecErrors);
	}
	else
	{
		//error -- no Statement in Else
		return VISIT_ERROR;	//just return, don't stop
	}

	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::uplusplus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	INTER_VARIANT temp;
	int i = 1;
	temp.SetValue ((void *)&i, RUL_INT);
	v3 = v1;
	v1 = v1 + temp;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::uminusminus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	INTER_VARIANT temp;
	int i = 1;
	temp.SetValue ((void *)&i, RUL_INT);
	v3 = v1;
	v1 = v1 - temp;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::upreplusplus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	INTER_VARIANT temp;
	int i = 1;
	temp.SetValue ((void *)&i, RUL_INT);
	v3 = v1 + temp;
	v1 = v1 + temp;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::upreminusminus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	INTER_VARIANT temp;
	int i = 1;
	temp.SetValue ((void *)&i, RUL_INT);
	v3 = v1 - temp;
	v1 = v1 - temp;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::uplus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	switch (v1.GetVarType())
	{
		case RUL_CHAR:
				char ch;
				v1.GetValue((void*)&ch, RUL_CHAR);
				ch = +(ch);
				v3.SetValue((void*)&ch, RUL_CHAR);
				break;
		case RUL_UNSIGNED_CHAR:
				{
					unsigned char i;
					v1.GetValue((void*)&i, RUL_UNSIGNED_CHAR);
					i = +(i);
					v3.SetValue((void*)&i, RUL_UNSIGNED_CHAR);
				}
				break;
		case RUL_SHORT:
				{
					short i;
					v1.GetValue((void*)&i, RUL_SHORT);
					i = +(i);
					v3.SetValue((void*)&i, RUL_SHORT);
				}
				break;
		case RUL_USHORT:
				{
					unsigned short i;
					v1.GetValue((void*)&i, RUL_USHORT);
					i = +(i);
					v3.SetValue((void*)&i, RUL_USHORT);
				}
				break;
		case RUL_INT:
				{
				int i;
				v1.GetValue((void*)&i, RUL_INT);
				i = +(i);
				v3.SetValue((void*)&i, RUL_INT);
				}
				break;
		case RUL_UINT:
				{
					unsigned long i=0;
					v1.GetValue((void*)&i, RUL_UINT);
					i = +(i);
					v3.SetValue((void*)&i, RUL_UINT);
				}
				break;
		case RUL_LONGLONG:
				{
					__int64 i=0;
					v1.GetValue((void*)&i, RUL_LONGLONG);
					i = +(i);
					v3.SetValue((void*)&i, RUL_LONGLONG);
				}
				break;
		case RUL_ULONGLONG:
				{
                    _UINT64 i=0;
					v1.GetValue((void*)&i, RUL_ULONGLONG);
					i = +(i);
					v3.SetValue((void*)&i, RUL_ULONGLONG);
				}
				break;
		case RUL_FLOAT:
				float f;
				v1.GetValue((void*)&f, RUL_FLOAT);
				f = +(f);
				v3.SetValue((void*)&f, RUL_FLOAT);
				break;
		case RUL_DOUBLE:
				double d;
				v1.GetValue((void*)&d, RUL_DOUBLE);
				d = +(d);
				v3.SetValue((void*)&d, RUL_DOUBLE);
				break;
	}
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::uminus(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	switch (v1.GetVarType())
	{
		case RUL_CHAR:
				char ch;
				v1.GetValue((void*)&ch, RUL_CHAR);
				ch = -(ch);
				v3.SetValue((void*)&ch, RUL_CHAR);
				break;
		case RUL_UNSIGNED_CHAR://negative of an unsigned... we need to promote this to an integer
				{
					unsigned char i=0;
					v1.GetValue((void*)&i, RUL_UNSIGNED_CHAR);
					int j = -(int)(i);
					v3.SetValue((void*)&j, RUL_INT);
				}
				break;
		case RUL_SHORT:
				{
					short i=0;
					v1.GetValue((void*)&i, RUL_SHORT);
					i = -(i);
					v3.SetValue((void*)&i, RUL_SHORT);
				}
				break;
		case RUL_USHORT://negative of an unsigned... we need to promote this to an short
				{
					short i=0;
					v1.GetValue((void*)&i, RUL_USHORT);
					//fixed for 5100.14 fail
					short j = -(i);
					v3.SetValue((void*)&j, RUL_SHORT);
				}
				break;
		case RUL_INT:
				{
					int i=0;
					v1.GetValue((void*)&i, RUL_INT);
					i = -(i);
					v3.SetValue((void*)&i, RUL_INT);
				}
				break;
		case RUL_UINT://negative of an unsigned long... we need to promote this to a long long
				{
					unsigned long i=0;
					v1.GetValue((void*)&i, RUL_UINT);
					__int64 j = -(__int64)(i);
					v3.SetValue((void*)&j, RUL_LONGLONG);
				}
				break;
		case RUL_LONGLONG:
				{
					__int64 i=0;
					v1.GetValue((void*)&i, RUL_LONGLONG);
					i = -(i);
					v3.SetValue((void*)&i, RUL_LONGLONG);
				}
				break;
		case RUL_ULONGLONG://negative of an unsigned long long... we need to promote this to a long long
				{
                    _UINT64 i=0;
					v1.GetValue((void*)&i, RUL_ULONGLONG);
					__int64 j = -(__int64)(i);
					v3.SetValue((void*)&j, RUL_LONGLONG);
				}
				break;
		case RUL_FLOAT:
				{
					float f=0.0;
					v1.GetValue((void*)&f, RUL_FLOAT);
					f = -(f);
					v3.SetValue((void*)&f, RUL_FLOAT);
				}
				break;
		case RUL_DOUBLE:
				{
					double d=0.0;
					v1.GetValue((void*)&d, RUL_DOUBLE);
					d = -(d);
					v3.SetValue((void*)&d, RUL_DOUBLE);
				}
				break;
	}
	//v3 = -(v1);
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bit_and(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 & v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bit_or(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 | v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitxor(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 ^ v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitnot(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = ~v1;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitrshift(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 >> v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitlshift(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 << v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::add(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 + v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::sub(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 - v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::mul(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 * v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::div(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 / v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::mod(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 % v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::exp(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 ^ v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::neq(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = (v1 != v2);
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::lt(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = (v1 < v2);
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::gt(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = (v1 > v2);
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::eq(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 == v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::ge(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = (v1 >= v2);
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::le(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 <= v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::land(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 && v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::lor(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 || v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::lnot(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = !v1;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::rparen(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::assign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::comma(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::plusassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 + v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::minusassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 - v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::divassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 / v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::modassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 % v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::mulassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 * v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitandassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 & v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitorassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 | v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::bitxorassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 ^ v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::rshiftassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 >> v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::lshiftassign(
			INTER_VARIANT& v1,
			INTER_VARIANT& v2,
			INTER_VARIANT& v3)
{
	v3 = v1 << v2;
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitCompoundExpression(
			CCompoundExpression* pCompStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	_INT32 iReturnValue = VISIT_NORMAL;
	CExpression*		pFirstExp = 0;
	CExpression*		pSecondExp = 0;
	RUL_TOKEN_SUBTYPE	Operator = RUL_SUBTYPE_NONE;
	INTER_VARIANT		var1;
	INTER_VARIANT		var2;

	if(pCompStmt)
	{
		pFirstExp = pCompStmt->GetFirstExpression();
		pSecondExp = pCompStmt->GetSecondExpression();
		Operator = pCompStmt->GetOperator();
	}
	else
	{
		//error
		throw(C_UM_ERROR_METHODERROR);
	}

	if(!pFirstExp)
	{
		//error -- no first operand
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pCompStmt);
	}
	else if (Operator != RUL_ASSIGN)	//for asignment expression, there is no need to execute left expression.
	{
		try
		{
			iReturnValue = pFirstExp->Execute(this,pSymbolTable,&var1, pvecErrors);
			switch(iReturnValue)
			{
				case VISIT_CONTINUE:
					return VISIT_NORMAL;

				case VISIT_BREAK:
				case VISIT_RETURN:
				case VISIT_ABORT:
					return iReturnValue;

				case VISIT_NORMAL:
				case VISIT_SCOPE_VAR:
				default:
					break;
			}
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pFirstExp);
		}
	}
	// evaluate OR and AND for the first expression
	// For OR, if the first expression is true, we are done and return true
	// For AND, if the first expression is false, we are done and return false
	if (Operator == RUL_LOGIC_AND)
	{
		if ((bool)var1 == false)
		{
			pvar->Clear();
			*pvar = false;
			return VISIT_NORMAL;
		}
	}
	else if (Operator == RUL_LOGIC_OR)
	{
		if ((bool)var1 == true)
		{
			pvar->Clear();
			*pvar = true;
			return VISIT_NORMAL;
		}
	}

	//get pSecondExp value in variable var2
	if(pSecondExp)
	{
		try
		{
			int iVisitReturnType = pSecondExp->Execute(this, pSymbolTable, &var2, pvecErrors);
			switch(iVisitReturnType)
			{
				case VISIT_CONTINUE:
					return VISIT_NORMAL;

				case VISIT_BREAK:
				case VISIT_RETURN:
				case VISIT_ABORT:
					return iVisitReturnType;

				case VISIT_NORMAL:
				case VISIT_SCOPE_VAR:
				default:
					break;
			}
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pSecondExp);
		}
	}

	//get pSecondExp value in variable pvar
	pvar->Clear();
	if(m_fnTable[Operator])
	{
		iReturnValue = (this->*m_fnTable[Operator])(var1,var2,*pvar);

		if (iReturnValue != VISIT_NORMAL)
		{
			return iReturnValue;
		}
		else if ((iReturnValue == VISIT_NORMAL) && (pvar->GetVarType() == RUL_NULL))
		{
			//abort if operation gets nothing done such as division by zero
			ADD_EXEC_ERROR(C_UM_ERROR_OPER_ABORTED, pFirstExp);
		}
	}

	//get pFirstExp value from variable pvar
	if ( (Operator == RUL_PLUS_PLUS)
		|| (Operator == RUL_MINUS_MINUS) 
		|| (Operator == RUL_PRE_PLUS_PLUS) 
		|| (Operator == RUL_PRE_MINUS_MINUS) 
		)
	{
		if(pFirstExp)
		{
			CToken *pExpToken = ((CPrimaryExpression *)pFirstExp)->GetToken();
			_INT32 nIdx = pExpToken->GetSymbolTableIndex();
			CVariable* pStore = pSymbolTable->GetAt(nIdx);
			if ( (!pStore) || strcmp(pStore->GetLexeme(), pExpToken->GetLexeme()) )
			{
				pStore = m_pMEE->m_GlobalSymTable.GetAt(nIdx);
			}

			if (pStore && pStore->IsVariable())
			{
				//Write L-value to token
				pStore->GetValue() = var1;
				iReturnValue = VISIT_NORMAL;
			}
			else if (pStore && pStore->IsDDItem())
			{
				//Write L-value to device variable
				try
				{
					iReturnValue = m_pMEE->ResolveNUpdateDDExp(pExpToken->GetLexeme(), pExpToken->GetParamRef(), &var1);
					//convert mixed VISIT_* error code and MEE error code to VISIT_* error code
					iReturnValue = m_pMEE->convertMEEcodeToVISITcode(iReturnValue);
				}
				catch(_INT32 error)
				{
					ADD_EXEC_ERROR(error, pFirstExp);
				}
			}
			else
			{
				ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pFirstExp);
			}
		}
	}

	else if (
		(Operator == RUL_ASSIGN) 
		|| (Operator == RUL_PLUS_ASSIGN) 
		|| (Operator == RUL_MINUS_ASSIGN) 
		|| (Operator == RUL_DIV_ASSIGN) 
		|| (Operator == RUL_MOD_ASSIGN) 
		|| (Operator == RUL_MUL_ASSIGN) 
		|| (Operator == RUL_BIT_AND_ASSIGN) 
		|| (Operator == RUL_BIT_OR_ASSIGN) 
		|| (Operator == RUL_BIT_XOR_ASSIGN) 
		|| (Operator == RUL_BIT_RSHIFT_ASSIGN) 
		|| (Operator == RUL_BIT_LSHIFT_ASSIGN) 
		)
	{
		if (pFirstExp)
		{
			CToken *pExpToken = ((CPrimaryExpression *)pFirstExp)->GetToken();
			if (pExpToken && pExpToken->IsArrayVar())
			{
				//put return value of pvar into variable
				//L-value is value array value
				m_IsLValue = true;
				iReturnValue = pFirstExp->Execute(this, pSymbolTable, pvar, pvecErrors, Operator);
			}
			else
			{
				_INT32 nIdx = pExpToken->GetSymbolTableIndex();
				CVariable* pStore = pSymbolTable->GetAt(nIdx);
				if ( (!pStore) || strcmp(pStore->GetLexeme(), pExpToken->GetLexeme()) )
				{
					pStore = m_pMEE->m_GlobalSymTable.GetAt(nIdx);
				}

				if (pStore && pStore->IsVariable())
				{
					//Write L-value to token
					pStore->GetValue() = *pvar;
					iReturnValue = VISIT_NORMAL;
				}
				else if (pStore && pStore->IsDDItem())
				{
					//it may be DD_ITEM method parameter
					try
					{
						iReturnValue = m_pMEE->ResolveNUpdateDDExp(pExpToken->GetLexeme(), pExpToken->GetParamRef(), pvar);
						//convert mixed VISIT_* error code and MEE error code to VISIT_* error code
						iReturnValue = m_pMEE->convertMEEcodeToVISITcode(iReturnValue);
					}
					catch(_INT32 error)
					{
						ADD_EXEC_ERROR(error, pFirstExp);
					}
				}
				else
				{
					ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pFirstExp);
				}
			}
		}
	}

	return iReturnValue;
}

//Return the value
_INT32 CInterpretedVisitor::visitPrimaryExpression(
			CPrimaryExpression* pPrimStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)
{
	CToken* pToken=0;
	if(pPrimStmt)
	{
		pToken = pPrimStmt->GetToken();
	}
	else
	{
		//error -- no primary statement...
		throw(C_UM_ERROR_METHODERROR);
	}
	if(pToken && pToken->IsVariable())
	{
		if(pToken->GetSubType() == RUL_DICT_STRING)
		{
			char dictBuffer[BUFFER_SIZE];//used BUFFER_SIZE as that is what IsString() used
			memset(dictBuffer,0,BUFFER_SIZE);
			if(m_pMEE && m_pMEE->GetDictionaryString((char*)(pToken)->GetLexeme(), dictBuffer, BUFFER_SIZE) == SUCCESS)
			{
				*pvar = dictBuffer;
			}
			else
			{
				*pvar = "";
			}

		}
		else
		{
			_INT32 nIdx = pToken->GetSymbolTableIndex();
			CVariable* pStore=0;
			if(nIdx >=0)
			{
				pStore = pSymbolTable->GetAt(nIdx);
				
				//Read local variable
				if(pStore)
				{
					*pvar = pStore->GetValue();
				}
			}
		}
		
	}
	else if(pToken->IsNumeric())
	{
		INTER_VARIANT temp(true,pToken->GetLexeme());
		*pvar = temp;
	}
	else if(pToken->IsConstant())
	{
		//Got to fill it this up...
		if(RUL_STRING_CONSTANT == pToken->GetSubType())
		{
			*pvar = (_CHAR*)pToken->GetLexeme();
		}
		else if(RUL_CHAR_CONSTANT == pToken->GetSubType())
		{
			char *pchChar = (_CHAR*)pToken->GetLexeme(); 
			*pvar = pchChar[0];
		}
	}
	else
	{
		//error -- no token in a primary statement.
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pPrimStmt);
	}
	return VISIT_NORMAL;
}
//CJK DP GETS INTO HERE REDO
_INT32 CInterpretedVisitor::visitProgram(
			CProgram* pProgram,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	_INT32 iRetVal = 1;
	CDeclarations* pDecl = 0;
	CStatementList* pStmtList = 0;

	try
	{
		//check if process is cancelled
		if (m_pMEE && (m_pMEE->IsMethodCancelled()))
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}

		if(pProgram)
		{
			pDecl=pProgram->GetDeclarations();
			pStmtList=pProgram->GetStatementList();
		}
		else
		{
			//error -- no program to execute.
			throw(C_UM_ERROR_METHODERROR);
		}

		if(pDecl)
		{
			pDecl->Execute(this,pSymbolTable, pvar, pvecErrors);
		}

		if (m_pMEE && (m_pMEE->IsMethodCancelled()))
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}

		if(pStmtList)
		{
			iRetVal = pStmtList->Execute(this, pSymbolTable, pvar, pvecErrors);
		}
		else
		{
			//error -- no statements in the program...
			throw(C_UM_ERROR_METHODERROR);
		}
	}
	catch(_INT32 error)
	{
		ADD_EXEC_ERROR(error, pProgram);
	}

	return iRetVal;
}

_INT32 CInterpretedVisitor::visitCompoundStatement(
			CCompoundStatement* pCompStmt,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CStatementList* pStmtLst=0;

	if(pCompStmt)
	{
		pStmtLst = pCompStmt->GetStatementList();
	}
	else
	{
		//error -- no compound statement.
		throw(C_UM_ERROR_METHODERROR);
	}

	return visitStatementList(pStmtLst, pSymbolTable, pvar, pvecErrors);
}

_INT32 CInterpretedVisitor::visitExpression(
			CExpression* pExpression,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	CExpression* pExp=0;

	if(pExpression)
	{
		pExp = pExpression->GetExpression();
	}
	else
	{
		throw(C_UM_ERROR_METHODERROR);
	}

	if (0 == pExp)
	{
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pExpression);
	}
	INTER_VARIANT var1;
	
	return pExp->Execute(this, pSymbolTable, &var1, pvecErrors);
}

_INT32 CInterpretedVisitor::visitStatement(
			CStatement* pStatement,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	throw(C_UM_ERROR_METHODERROR);
}

_INT32 CInterpretedVisitor::visitStatementList(
			CStatementList* pStmtList,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC* pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	STATEMENT_LIST* pStmtCol=0;
	CStatement* pStmt=0;

	if(pStmtList)
	{
		pStmtCol = pStmtList->GetStmtList();
		size_t nSize = pStmtCol->size();
		for(size_t i=0; i<nSize; i++)
		{
			pStmt = (*pStmtCol)[i];

			try
			{
				pSymbolTable->m_sScope = pStmt->GetStmtScopeStack();
				int iVisitReturnType = pStmt->Execute(this, pSymbolTable, pvar, pvecErrors);

				switch(iVisitReturnType)
				{
					case VISIT_BREAK:
					case VISIT_CONTINUE:
					case VISIT_RETURN:
					case VISIT_ABORT:				//abort builtin is executed
						return iVisitReturnType;
						break;
				}

				if (m_pMEE && (m_pMEE->IsMethodCancelled()))
				{
					throw(C_UM_ERROR_METH_CANCELLED);
				}

				//Anil 180107 if VISIT_SCOPE_VAR == iVisitReturnType means  we are executing 
				//the Statement list which has declaration
				//This is a bug fix to get rid of the variable which is declared within the scope
				//the below DD Method code was not handled
				/*MethodDefination 
				{
					int x;
					x = 0;
					if(int x == 0)
					{
						int y; 
						ACKNOWLEDGE("This was not executing");
					}
				

				}*/
				if(VISIT_SCOPE_VAR == iVisitReturnType )
				{
					//Anil 240107 Fool the interpreter that you have executed this statement( which is declaration)
					continue;
				}
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pStmt);
			}
		}
	}
	else
	{
		//error -- no statements in the list.
		throw(C_UM_ERROR_METHODERROR);
	}
	return VISIT_NORMAL;
}

//	This function can be used to initialize variables by traversing the list 
//	of variables in the Symbol table before executing the statement list.
_INT32 CInterpretedVisitor::visitDeclarations(
			CDeclarations* pDeclarations,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	,
			RUL_TOKEN_SUBTYPE	AssignType)
{
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitRuleService(
			CRuleServiceStatement* pStatement,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	throw(C_UM_ERROR_METHODERROR);
}

_INT32 CInterpretedVisitor::visitOMExpression(
			COMServiceExpression* pExpression,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return VISIT_NORMAL;
}

_INT32 CInterpretedVisitor::visitIFExpression(
			IFExpression* pIfExp,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	if (pIfExp == NULL)
	{
		throw(C_UM_ERROR_METHODERROR);
	}
	CExpression *pExp;
	CExpression *pTrueExp;
	CExpression *pFalseExp;

	pIfExp->GetExpressions(pExp, pTrueExp, pFalseExp);

	if (pExp)
	{
		INTER_VARIANT Var;
		_INT32 iRetValue;

		iRetValue = pExp->Execute (this, pSymbolTable,&Var,pvecErrors);
		if (iRetValue == VISIT_RETURN)
		{
			return iRetValue;
		}

		if ((bool)Var)
		{
			if (pTrueExp)
			{
				iRetValue = pTrueExp->Execute (this, pSymbolTable,&Var,pvecErrors);
				switch(iRetValue)
				{
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iRetValue;
						break;
				}

				*pvar = Var;
			}
			else
			{
				ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pExp);
			}
		}
		else
		{
			if (pFalseExp)
			{
				iRetValue = pFalseExp->Execute (this, pSymbolTable,&Var,pvecErrors);
				switch(iRetValue)
				{
					case VISIT_RETURN:
					case VISIT_ABORT:
						return iRetValue;
						break;
				}
				*pvar = Var;
			}
			else
			{
				ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pExp);
			}
		}
		return VISIT_NORMAL;
	}
	else
	{
		ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pIfExp);
	}
}

_INT32 CInterpretedVisitor::visitFunctionExpression(
			FunctionExpression* pFuncExp,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	if (NULL == pFuncExp)
	{
		throw(C_UM_ERROR_METHODERROR);
	}

  	INTER_VARIANT pVarParams[MAX_NUMBER_OF_FUNCTION_PARAMETERS];
	
	for (int iLoopVar = 0;iLoopVar < pFuncExp->GetParameterCount();iLoopVar++)
	{
		if (pFuncExp->GetParameterType(iLoopVar) != RUL_STR_CONSTANT)
		{
			CExpression *pExp = pFuncExp->GetExpParameter(iLoopVar);
			if (NULL == pExp)
			{
				char szMessage[1024]={0};
				sprintf( szMessage, "***FAILURE*** Missing Argument %d of %s\n", iLoopVar+1, pFuncExp->GetFunctionName() );
				LOGIT(COUT_LOG,szMessage);

				ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pFuncExp);
			}
			_INT32 iRetValue;

			if ((pFuncExp->GetParameterType(iLoopVar) == RUL_DD_ITEM) && (m_pMEE->GetStartedProtocol() == nsEDDEngine::HART)
				&& ((strcmp((const char *)pFuncExp->GetFunctionName(), "resolve_local_ref")) == 0)
			   )
			{
				//get DD item ID as builtin argument for HART resolve_local_ref since HART tokenizer has put variable name as resolve_local_ref argument.
				CPrimaryExpression *pPrimaryExp = (CPrimaryExpression *)pExp;
				if (pPrimaryExp)
				{
					CToken *pToken = pPrimaryExp->GetToken();
					PARAM_REF *pOpRef = pToken->GetParamRef();

					ITEM_ID ulItemId = 0;
					if ((pOpRef->op_ref_type == STANDARD_TYPE) && (pOpRef->op_info.type != iT_ReservedZeta))
					{
						ulItemId = pOpRef->op_info.id;
					}
					else if (pOpRef->op_ref_type == COMPLEX_TYPE)
					{
						ulItemId = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
					}
					else
					{
						ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pPrimaryExp);
					}

					pVarParams[iLoopVar] = (unsigned int)ulItemId;
				}
				else
				{
					ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pExp);
				}
			}
			else
			{
				//get builtin argumennt value
				iRetValue = 
					pExp->Execute(this,pSymbolTable,&pVarParams[iLoopVar],pvecErrors);
			}

			if (iRetValue == VISIT_RETURN)
			{
				return iRetValue;
			}
		}
		else
		{
			CToken *pToken = pFuncExp->GetConstantParameter(iLoopVar);
			//pVarParams[iLoopVar].SetValue((void *)pToken->GetLexeme(), RUL_CHARPTR);
            if( pToken )  //WaltS - 04may07 this check for NULL pointer 
            { 
				pVarParams[iLoopVar] = (char *)pToken->GetLexeme(); 
            } 
            else   //WaltS - 04may07
            { 
				CExpression *pExp = pFuncExp->GetExpParameter(iLoopVar);
				if (NULL == pExp)
				{
					char szMessage[1024]={0};
					sprintf( szMessage, "***FAILURE*** Missing Argument %d of %s\n", iLoopVar+1, pFuncExp->GetFunctionName() );
					LOGIT(COUT_LOG,szMessage);

					ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pFuncExp);
				}
				_INT32 iRetValue;

				iRetValue = 
					pExp->Execute(this,pSymbolTable,&pVarParams[iLoopVar],pvecErrors);
				if (iRetValue == VISIT_RETURN)
				{
					return iRetValue;
				}				
            } 
		}
	}

	try
	{
		int iReturnStatus = BUILTIN_SUCCESS;
		bool bRetValue = m_pBuiltInLib->InvokeFunction
										(
											(char*)pFuncExp->GetFunctionName()
											, pFuncExp->GetParameterCount()
											, pVarParams
											, pvar
											, &iReturnStatus
											, pFuncExp			// added WS:EPM 17jul07
										);
		if (bRetValue)
		{
			switch (iReturnStatus)
			{
				case BUILTIN_UI_CANCELLED:
				{
					//UI builtin cancelled by user
					throw(C_UM_ERROR_UI_CANCELLED);
				}
				break;
				case BUILTIN_COMM_ABORTED:
				{
					//communication builtin aborted
					throw(C_UM_ERROR_COMM_ABORTED);
				}
				break;
				case BUILTIN_RETRY_FAILED:
				{
					//communication builtin retry failed
					throw(C_UM_ERROR_COMM_RETRY_ABORTED);
				}
				break;
				case BUILTIN_COMM_NO_DEVICE:
				{
					//communication builtin aborted
					throw(C_UM_ERROR_COMM_NO_DEVICE);
				}
				break;
				case BUILTIN_COMM_FAIL_RESPONSE:
				{
					//communication builtin retry failed
					throw(C_UM_ERROR_COMM_FAIL_RESPONSE);
				}
				break;
				case BUILTIN_VAR_ABORTED:
				{
					//Variable access with false VALIDITY
					throw(C_UM_ERROR_VAR_ABORTED);
				}
				break;
				case BUILTIN_ABORTED:
				{
					//abort builtin
					return VISIT_ABORT;
				}
				break;
				default:
				{
					CVariable *p_bi_rc = pSymbolTable->Find("_bi_rc");
					if (p_bi_rc != NULL)
					{
						p_bi_rc->GetValue() = *pvar;
					}

					return VISIT_NORMAL;
				}
				break;
			}
		}
		else
		{
			//DDS error or wrong bulitin parameter type
			throw(C_UM_ERROR_DDS_PARAM);
		}
	}
	catch(_INT32 error)
	{
		ADD_EXEC_ERROR(error, pFuncExp);
	}
}

#define MAX_INT_DIGITS  10

//Anil August 26 2005 For handling DD variable and Expression
_INT32 CInterpretedVisitor::visitComplexDDExpression(
			CComplexDDExpression* pArrExp,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)
{
	assert(pvar!=0);

	int iRetVal = VISIT_NORMAL;
	_INT32 i32Idx			= pArrExp->GetToken()->GetSymbolTableIndex();
	CVariable* pVariable	= pSymbolTable->GetAt(i32Idx);
	

	//if variable is not in local symbol table, take it from global symbol table
	if ( (!pVariable) || strcmp(pVariable->GetLexeme(), pArrExp->GetToken()->GetLexeme()) )
	{
		pVariable = m_pMEE->m_GlobalSymTable.GetAt(i32Idx);
	}

	//evaluate the expressions...
	EXPR_VECTOR* pvecExpressions = pArrExp->GetExpressions();
	_INT32 i32Count = (_INT32)pvecExpressions->size();
	INTER_VARIANT var;	

	char pszComplexDDExpre[1024];
	strcpy(pszComplexDDExpre,(const char*)pVariable->GetLexeme() );

	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	//Added the code for the DD Methos Execution
	if( (RUL_DD_METHOD == pVariable->GetSubType()) && (strstr(pszComplexDDExpre, "(") != NULL) )
	{
		// Fill the current values of all the Method Agrument
		INTERVARIANT_VECTOR vectInterVar;
		_INT32 i=0;	// WS - 9apr07 - 2005 checkin
		for(i=0;i<i32Count;i++)// WS - 9apr07 - 2005 checkin
		{
			var.Clear();
			((*pvecExpressions)[i])->Execute(this,pSymbolTable,&var, pvecErrors);	//do expression calculations
			vectInterVar.push_back(var);		
		}	

		//Fill all the methos Agrument info like pchCallerArgName and ..._TYPE and ..._SUBTYPE
		char szDDitemName[1024];
		strcpy(szDDitemName,(const char*)pVariable->GetDDItemName() );
		int NoOfParams = 0;

		std::vector<CVariable*> vectpCVariable;
		METHOD_ARG_INFO_VECTOR vectMethArgInfo;
		//From here just extrac the Arg list and fill it out
		try
		{
			int iLeftPeranthis = 0;
			bool bValidMethodCall = false;
			size_t i = strlen(szDDitemName);
			//Checkk for the Valid Method call, 
			//ie Method call should Start and end with open and Close Parenthesis respectively
			for(; i < (int)strlen(pszComplexDDExpre); i++)  // warning C4018: '>=' : signed/unsigned mismatch <HOMZ: added cast>
			{
				if(pszComplexDDExpre[i] == '(')
				{
					bValidMethodCall = true;
					iLeftPeranthis =1;
					i++;
					break;
				}
			}
			if( bValidMethodCall ==  false)
			{
				ADD_EXEC_ERROR(C_UM_ERROR_METHODERROR, pArrExp);
			}		
			//Now strat extracting the each Argument name and push it in the strvCallerArgList vector
			int iNoOfchar = 0;
			size_t lstlen = strlen(pszComplexDDExpre);
			for(; i< (lstlen) ; i++)					//i represents first position of each argument
			{
				//Look for the space and do not count
				if ( (' ' != pszComplexDDExpre[i]) || (iNoOfchar > 0) )					
				{
					iNoOfchar++ ;
				}

				//If u find ')', reduce the iLeftPeranthis and 
				//when u go out of this loop iLeftPeranthis dhould be zero		
				if(')' == pszComplexDDExpre[i])
				{
					iLeftPeranthis--;
				}
				//If u find '(', increase the iLeftPeranthis and 
				//when u go out of this loop iLeftPeranthis dhould be zero		
				else if('(' == pszComplexDDExpre[i])
				{
					iLeftPeranthis++;
				}

				//if it is ',' or last ")", AND there is an arg name (no args, skip to else)
				if ( ((pszComplexDDExpre[i] == ',') || (0 == iLeftPeranthis)) && (iNoOfchar > 1) )
				{
					//do insert here, Get the start pos of the arg name
					int iNoOfSpaces = 0;
					//It may so happen that for the arg there are space before , or ')'
					//EG: ( ArgnameOne                  ,   argName2        ), thats why this below loop			
					for(size_t x = i-1; ;x--)
					{
						if(' ' == pszComplexDDExpre[x])
						{
							iNoOfSpaces++;
						}
						else
						{
							break;
						}
					}

					iNoOfchar -= (iNoOfSpaces + 1);	//Because ; or ) is included and because of number of spaces

					//Get the strating position and Char count
					int istartPosOfPassedItem = (int)i - iNoOfchar - iNoOfSpaces;
					int iCount = iNoOfchar + 1 ;
					char* pchDecSource = new char[ iCount ];// +1 for Null Char +1 for ; -1 for as it had counted ]
					memset(pchDecSource, 0, iCount);
					PS_Strncpy(pchDecSource, iCount, (const char*)&pszComplexDDExpre[istartPosOfPassedItem], iNoOfchar);
					pchDecSource[iNoOfchar] = '\0';

					//it may or may not DD item, it may be Constant also...refere below Example
					/*
					Eg;
					M1(DD_ITEM &X);
					M2(DD_STRING X);
					M3
					{
						M1(tag);//in this case Tag is passed as a DD item
						M2(tag);//this case Tag is passes by its value
					}
					*/

					//Find if it is compound expression as method parameter first
					CToken*	pToken = NULL;
					if (i32Count > 0)
					{
						pToken = ((*pvecExpressions)[NoOfParams])->GetToken();
					}

					//Find it in Synbol table
					if (pToken)
					{
						//this is not compound expression. is it a local variable?
						vectpCVariable.push_back(pSymbolTable->GetAt(pToken->GetSymbolTableIndex()));

						//Find it in global symbol table
						if ( !vectpCVariable[NoOfParams] || strcmp(vectpCVariable[NoOfParams]->GetLexeme(), pToken->GetLexeme()) )
						{
							//if it is not a local variable either, is it a global variable?
							vectpCVariable[NoOfParams] = m_pMEE->m_GlobalSymTable.GetAt(pToken->GetSymbolTableIndex());
						}
					}
					else
					{
						vectpCVariable.push_back(nullptr);
					}

					METHOD_ARG_INFO stMethArg;
					//stMethArg

					if (pToken == NULL)
					{
						//This is a compound expression
						stMethArg.SetCallerArgName(pchDecSource);
						stMethArg.SetType( RUL_TYPE_NONE );			//don't know the type and subtype yet
						stMethArg.SetSubType( RUL_SUBTYPE_NONE );
						vectMethArgInfo.push_back(stMethArg);

					}
					else if (vectpCVariable[NoOfParams] != NULL )
					{
						//This is a local or global variable
						stMethArg.SetCallerArgName(pchDecSource);
						stMethArg.SetType(vectpCVariable[NoOfParams]->GetType() );
						stMethArg.SetSubType(vectpCVariable[NoOfParams]->GetSubType() );

						if (vectpCVariable[NoOfParams]->GetType() == RUL_DD_ITEM)
						{
							//get operative item ID for method parameter
							PARAM_REF opRef = *(vectpCVariable[NoOfParams]->GetParamRef());
							int rc = SUCCESS;
							if (!opRef.isOperationalIDValid())
							{
								hCitemBase *pIB = NULL;
								rc = m_pMEE->ResolveComplexReference((char*)pchDecSource, NULL, pIB, &opRef);
							}
							else if (strcmp(pchDecSource, vectpCVariable[NoOfParams]->GetLexeme()) != 0)
							{
								m_pMEE->ResolveVarExp(pchDecSource, false, NULL, &opRef);
							}

							if (rc == SUCCESS)
							{
								stMethArg.prDDItemId = opRef;
							}
						}

						vectMethArgInfo.push_back(stMethArg);
					}
					else	//pToken != NULL
					{
						//This is a constant numeric value or constant string
						//RUL_NUMERIC_CONSTANT, 
						//RUL_STR_CONSTANT,
						//RUL_CHR_CONSTANT,	
						stMethArg.SetCallerArgName("PassedByConstant");
						stMethArg.SetType( RUL_SIMPLE_VARIABLE );	//keep simple variable type
						stMethArg.SetSubType( pToken->GetSubType() );

						vectMethArgInfo.push_back(stMethArg);
					}

					if(pchDecSource)
					{
						delete[] pchDecSource;
						pchDecSource = NULL;
					}

					NoOfParams++;
					if (i32Count < NoOfParams)
					{
						throw(C_UM_ERROR_MISSINGTOKEN);
					}

					//clean indicators
					iNoOfchar = 0;
				}
				//else (not ',' and not last ')') or no chars encountered so far <eg meth(,)>

				if(0 == iLeftPeranthis)// no more parameters <final ')' found>..get out
					break;
			}// next character (i) in parameter list string
		}// end of block
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pArrExp);
		}

		try
		{
			//When I come here, vectMethArgInfo is filled with the Parameter that are passed to 
			//the called method  and  vectInterVar is filled with its corresponding values
 			INTERPRETER_STATUS intStatus = m_pMEE->ResolveMethodExp(
													(const char*)pVariable->GetLexeme(),
													(const char*)pVariable->GetDDItemName(),
													pvar, &vectInterVar, &vectMethArgInfo);

			//sub-method is done
			//convert INTERPRETER_STATUS to VISIT_* error code
			switch (intStatus)
			{
			case INTERPRETER_STATUS_INVALID:
			case INTERPRETER_STATUS_UNKNOWN_ERROR:
			case INTERPRETER_STATUS_PARSE_ERROR:
			case INTERPRETER_STATUS_EXECUTION_ERROR:
			case INTERPRETER_STATUS_EXECUTION_ABORT:
				return VISIT_ABORT;
				break;
			case INTERPRETER_STATUS_EXECUTION_CANCEL:
				throw(C_UM_ERROR_UI_CANCELLED);
				break;
			default:
				break;	//continue
			}
		}
		catch(_INT32 error)
		{
			ADD_EXEC_ERROR(error, pArrExp);
		}

		//Once we Execute this method, the DD ITEM value has already saved into device variable in sub-method.
		//We need to again fill the values of those parameter into symbol table only, 
		//	which are passed by reference
		_INT32 iNoOfArgs = (_INT32)vectMethArgInfo.size();
		for(i = 0; i < iNoOfArgs; i++)
		{	
			//Check for the Parameter which are passed by reference other than return 
			//For return it is self generated parameter for temporory purpose which are
			//	always passed by reference
			if( (vectMethArgInfo[i].ePassedType == DD_METH_AGR_PASSED_BYREFERENCE )&& 
				 !(vectMethArgInfo[i].m_IsReturnVar) )				
			{
				//Check for Simple var , in which case it is direct value assgnment
				switch(vectMethArgInfo[i].GetType())
				{
					case RUL_SIMPLE_VARIABLE:
					case RUL_ARRAY_VARIABLE:
					{
						if (vectpCVariable[i] != NULL)
						{
							//determine the reference is a local variable or a local array
							char *pchBracket = strchr((char*)vectMethArgInfo[i].GetCallerArgName(), '[');
							if (!pchBracket)
							{
								//Since this reference is a local variable, let's update it
								vectpCVariable[i]->GetValue() = vectInterVar[i];
							}
							else
							{
								//get array index and update the local array
								INTER_VARIANT ivIndex;
								m_pMEE->ResolveVarExp(vectMethArgInfo[i].GetCallerArgName(), vectpCVariable[i]->m_bIsRoutineToken, &ivIndex, NULL);

								INTER_SAFEARRAY *theArray = (vectpCVariable[i]->GetValue()).GetSafeArray();
								if (theArray != NULL)
								{
									theArray->SetElement(((int)ivIndex * theArray->GetElementSize()), &(vectInterVar[i]));
								}
							}
						}
					}//End of Simple var
					break;

					default:	//RUL_DD_ITEM
					break;
				}//end of switch

			}//End of Non Return bar
			
			//Check for the return type var, Exclude the return void Statement
			if( (vectMethArgInfo[i].ePassedType == DD_METH_AGR_PASSED_BYREFERENCE ) && 
				(vectMethArgInfo[i].m_IsReturnVar) &&
				(vectMethArgInfo[i].GetSubType() != RUL_SUBTYPE_NONE)
				)
			{
				pvar->Clear();
				*pvar = vectInterVar[i];;
			}//End of return var			
		}
	}//End of Method RUL_DD_METHOD == pVariable->GetSubType()
	else
	{
		//if the token is array or list, get indexes. i32Count is number of dimensions.
		int* lTempArray = new int[i32Count];
		//get token operative item ID
		PARAM_REF opRef = *(pVariable->GetParamRef());

		for(_INT32 i=0; i<i32Count; i++)// WS - 9apr07 - 2005 checkin
		{
			var.Clear();
			((*pvecExpressions)[i])->Execute(this,pSymbolTable,&var, pvecErrors);
			lTempArray[i] = (int)var;		
		
			//update token operative item ID if the token is value array or list.
			if (opRef.isOperationalIDValid())
			{
				//looking for right value array or list depth
				_INT32 j = i;
				if ( pVariable->m_bIsRoutineToken && (opRef.op_ref_type == COMPLEX_TYPE) )
				{
					//This is a method parameter. The depth is counted from method parameter current depth.
					j += opRef.op_info_list.count - 1;
				}

				//adjust item array or value array or list index as one based index in MI
				m_pMEE->UpdatePARAM_REF_withItemArrayListIndex(lTempArray[i], j, &opRef);
			}
		}

		size_t lStrlen = strlen((const char*)pVariable->GetLexeme());

		char* szTempLexeme = new char[lStrlen+1];

		//Here is slight Confusion ,
		//Funda: Complex DD Expression is Actually stored in m_pszLexeme which is got by 
		//	pVariable->GetLexeme().  Where as Actual token is Stored in m_pszComplexDDExpre 
		//	which i got by pVariable->GetDDItemName()	
		PS_Strcpy(szTempLexeme, lStrlen+1, (const char*) pVariable->GetLexeme());

		string szActualstring = "";
		size_t lCout = 0;
		int iNoOfBrackExpre = 0;
		
		for(size_t iTemp = 0; iTemp < lStrlen ; iTemp++)
		{			
			if(szTempLexeme[iTemp] == '[')
			{
				szActualstring += szTempLexeme[iTemp];
				iTemp++;
				char szBuf[MAX_INT_DIGITS + 1] ;
                itoa(lTempArray[iNoOfBrackExpre],szBuf,10);
				szActualstring += szBuf;
				szActualstring += "]";
				lCout += strlen(szBuf) + 1;

				iNoOfBrackExpre++;
				int iLeftBrackCount = 1;
				long int iCount = 0;
				while( (iLeftBrackCount!=0) && (iTemp<lStrlen) )
				{
					if(szTempLexeme[iTemp] == '[')
						iLeftBrackCount++;
					if(szTempLexeme[iTemp] == ']')
						iLeftBrackCount--;
					iTemp++;				
				}
				iTemp--;
			}
			//execute expression of enum/bit_enum variable following by '('
			else if ( (pVariable->IsEnumDDItem()) && (szTempLexeme[iTemp] == '(') )
			{
				szActualstring += szTempLexeme[iTemp];
				iTemp++;
				char szBuf[MAX_INT_DIGITS + 1] ;
                itoa(lTempArray[iNoOfBrackExpre],szBuf,10);
				szActualstring += szBuf;
				szActualstring += ")";
				lCout += strlen(szBuf) + 1;

				iNoOfBrackExpre++;
				int iLeftPeranthis = 1;
				long int iCount = 0;
				while( (iLeftPeranthis!=0) && (iTemp<lStrlen) )
				{
					if(szTempLexeme[iTemp] == '(')
						iLeftPeranthis++;
					if(szTempLexeme[iTemp] == ')')
						iLeftPeranthis--;
					iTemp++;				
				}
				iTemp--;
			}
			else
			{
				szActualstring += szTempLexeme[iTemp];
			}
		}

		if( lTempArray )
		{
			delete [] lTempArray;
			lTempArray = NULL;
		}

		if(szTempLexeme)
		{
			delete[] szTempLexeme;
			szTempLexeme = NULL;
		}


		if(m_IsLValue)
		{
			try
			{
				m_IsLValue = false;
				RETURNCODE iReturnValue = m_pMEE->ResolveNUpdateDDExp(
															(const char*)szActualstring.c_str(),
															&opRef,
															pvar,		AssignType);

				//convert mixed VISIT_* error code and MEE error code to VISIT_* error code
				iRetVal = m_pMEE->convertMEEcodeToVISITcode(iReturnValue);
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pArrExp);
			}
		}
		else
		{
			try
			{
				RETURNCODE iReturnValue = m_pMEE->ResolveDDExp(
															(const char*)szActualstring.c_str(),
															&opRef,
															pvar);

				//convert mixed VISIT_* error code and MEE error code to VISIT_* error code
				iRetVal = m_pMEE->convertMEEcodeToVISITcode(iReturnValue);
			}
			catch(_INT32 error)
			{
				ADD_EXEC_ERROR(error, pArrExp);
			}
		}	

	}//end of Else for Checking for method type
	
	return  iRetVal;
}

//Added:Anil Octobet 5 2005 for handling Method Calling Method
//Function to set the FLAG that whether it is a routine call( ie Called method), ie it is not called by the menu
void CInterpretedVisitor::SetIsRoutineFlag(bool bIsRoutine)
{
	m_bIsRoutine = bIsRoutine;
	return;
}
//Added:Anil Octobet 5 2005 for handling Method Calling Method
//To get the Routine flag
bool CInterpretedVisitor::GetIsRoutineFlag()
{
	return m_bIsRoutine;
}

