#include "MethodHelper.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include "TestSupport/BuiltinSupportSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            nsEDDEngine::FLAT_METHOD* MethodHelper::GetMethod(EddEngineController^ engine, System::String^ methodName)
            {
                FDI_PARAM_SPECIFIER test_method_specifier;
                engine->FillParamSpecifier(methodName, &test_method_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_method_specifier.id);
                FLAT_METHOD* test_method = dynamic_cast<FLAT_METHOD*>(genericItem->item);

                Assert::IsTrue(test_method != nullptr, methodName + " is not available");
                
                genericItem->item = nullptr;
                delete genericItem;
              
                return test_method;
            }

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests