MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0004, DEVICE_REVISION 3, DD_REVISION 9


/****************************************************************************** */
/* Dynamic variables (mandatory)                                                */
/* **************************************************************************** */
/* */  
/* */  VARIABLE dynamic_variable_dummy
/* */  {
/* */      LABEL "Dynamic variable dummy";
/* */      TYPE INTEGER;
/* */      CLASS LOCAL;
/* */  }
/* */  
/* */  ARRAY OF VARIABLE dynamic_variables
/* */  {
/* */      ELEMENTS
/* */      {
/* */          PRIMARY, dynamic_variable_dummy;
/* */      }
/* */  }
/* */  





/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    ITEMS
   {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */


MENU main_test_menu
{
    LABEL "Main test menu";
    STYLE WINDOW;
    ITEMS
    {
        page_menu_1,
        page_menu_2
    }
}

MENU page_menu_1
{
    LABEL "Page 1";
    STYLE PAGE;
    ITEMS
    {
        chart_strip,
        chart_scope,
        chart_vertical,
        chart_default
    }
}

MENU page_menu_2
{
    LABEL "Page 2";
    STYLE PAGE;
    ITEMS
    {
        chart_sweep,
        chart_horizontal,
        chart_gauge
    }
}
/****************************************************************************** */
/* Charts                                                                       */
/****************************************************************************** */

CHART chart_strip
{
    LABEL "Strip Chart";
    HELP "A Strip Chart";
    TYPE STRIP;
    HEIGHT XX_SMALL;
    WIDTH X_SMALL;
    MEMBERS
    {
        SRC1, chart_source_1;
    }
}
CHART chart_sweep
{
    LABEL "Sweep Chart";
    VALIDITY FALSE;
    TYPE SWEEP;
    HEIGHT SMALL;
    WIDTH MEDIUM;
    MEMBERS
    {
        SRC1, chart_source_1;
    }
}
CHART chart_scope
{
    LABEL "Scope Chart";
    VALIDITY TRUE;
    TYPE SCOPE;
    HEIGHT LARGE;
    WIDTH X_LARGE;
    MEMBERS
    {
        SRC1, chart_source_4;
    }
}
CHART chart_horizontal
{
    LABEL "Horizontal Bar Chart";
    TYPE HORIZONTAL_BAR;
    HEIGHT SMALL;
    WIDTH SMALL;
    MEMBERS
    {
        SRC1, chart_source_1;
    }
}

CHART chart_vertical
{
    LABEL "Vertical Bar Chart";
    TYPE VERTICAL_BAR;
    HEIGHT XX_LARGE;
    WIDTH SMALL;
    MEMBERS
    {
        SRC1, chart_source_3;
        SRC2, chart_source_1;
    }
}

CHART chart_gauge
{
    LABEL "Gauge Chart";
    TYPE GAUGE;
    HEIGHT SMALL;
    WIDTH X_LARGE;
    CYCLE_TIME 1500;
    LENGTH 17500;
    MEMBERS
    {
        SRC1, chart_source_2;
    }
}

CHART chart_default
{
    MEMBERS
    {
        SRC1, chart_source_2;
    }
}
/****************************************************************************** */
/* Sources                                                                      */
/****************************************************************************** */
SOURCE chart_source_1
{
    LABEL "Chart Source 1";
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_2
{
    LABEL "Chart Source 2";
    HELP "the Chart Source 2";
    VALIDITY TRUE;
    LINE_TYPE TRANSPARENT;
    LINE_COLOR GREEN;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_2;
}
SOURCE chart_source_3
{
    LABEL "Chart Source 3";
    HELP "the Chart Source 3";
    EMPHASIS TRUE;
    LINE_TYPE DATA1;
    LINE_COLOR GRAY;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_2;
}

SOURCE chart_source_4
{
    LABEL "Chart Source 4";
    HELP "the Chart Source 4";
    EMPHASIS FALSE;
    LINE_TYPE LOW_LIMIT;
    LINE_COLOR YELLOW;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_5
{
    LABEL "Chart Source 5";
    HELP "the Chart Source 5";
    LINE_TYPE DATA;
    LINE_COLOR BLACK;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_6
{
    LABEL "Chart Source 6";
    HELP "the Chart Source 6";
    LINE_TYPE LOW_LOW_LIMIT;
    LINE_COLOR RED;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_7
{
    LABEL "Chart Source 7";
    HELP "the Chart Source 7";
    LINE_TYPE HIGH_LIMIT;
    LINE_COLOR WHITE;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_8
{
    LABEL "Chart Source 8";
    HELP "the Chart Source 8";
    LINE_TYPE HIGH_HIGH_LIMIT;
    LINE_COLOR ORANGE;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}

SOURCE chart_source_9
{
    LABEL "Chart Source 9";
    HELP "the Chart Source 9";
    LINE_TYPE DATA5;
    LINE_COLOR BLUE;
    MEMBERS
    {
        Y1, chart_value_1;
    }
    Y_AXIS chart_axis_1;
}
/****************************************************************************** */
/* Axis                                                                         */
/****************************************************************************** */
AXIS chart_axis_1
{
    LABEL "Axis 1";
    HELP "The Axis 1";
    MAX_VALUE chart_max_value_1;
    MIN_VALUE chart_min_value_1;
    SCALING LINEAR;
}

AXIS chart_axis_2
{
    CONSTANT_UNIT "a constant unit";
    MAX_VALUE chart_max_value_1;
    MIN_VALUE chart_min_value_1;
    SCALING LOGARITHMIC;
}

AXIS chart_axis_3
{
    MAX_VALUE 15;
    MIN_VALUE 4.5;
    SCALING LOGARITHMIC;
}
/****************************************************************************** */
/* Variables                                                                    */
/****************************************************************************** */
VARIABLE chart_value_1
{
    LABEL "Charts Value";
    HELP "The current value of the charts";
    CLASS LOCAL;
    TYPE INTEGER;
}

VARIABLE chart_min_value_1
{
    LABEL "Chart Min Value";
    HELP "Minimum value of the charts";
    CLASS LOCAL;
    TYPE INTEGER;
}

VARIABLE chart_max_value_1
{
    LABEL "Chart Max Value";
    HELP "Maximum value of the charts";
    CLASS LOCAL;
    TYPE INTEGER;
}
