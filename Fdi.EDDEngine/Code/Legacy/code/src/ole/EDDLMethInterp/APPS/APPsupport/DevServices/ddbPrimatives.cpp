/*************************************************************************************************
 *
 * $Workfile: ddbPrimatives.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the lowest level class (string, long, etc) implementation
 * 4/8/2	sjv	created
 */

#include "stdafx.h"
#include "ddbItemBase.h"
#include "ddbPrimatives.h"


#ifndef DEV_STRING_INFO_FLAG
typedef struct {
	unsigned long   id;
	unsigned long	mfg;
	unsigned short  dev_type;
	unsigned char   rev;
	unsigned char   ddrev;
}               DEV_STRING_INFO;
#endif

#if 0
int pcnt=0, icnt=0,izcnt=0;
#endif // 0

#ifdef __cplusplus
    extern "C" {
#endif
extern int keyFromDevInfo(DEV_STRING_INFO& dsi);
//extern int app_func_get_dict_string(ENV_INFO *env_info, ulong index, STRING *str);
//	#ifndef ISDLL
//	extern int app_func_get_dict_string(ENV_INFO *env_info, ulong index, STRING *str);
//	#endif
#ifdef __cplusplus
    }
#endif

//#ifdef ISDLL
//	extern Cdictionary* pMainDict;
//#endif


hCddlString::hCddlString(DevInfcHandle_t h) : hCobject(h)
{
	//theStr     assume the string will instantiate empty()
	len     = 0; 
    flags = FREE_STRING;
	ddKey   = 0;  
	ddItem  = 0;
	enumVal = 0;
	strTag  = 0;
	pRef  = NULL;
#ifdef _DEBUG_ALLOC
		clog<<"   String new     0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
#endif
}

hCddlString::~hCddlString()
{
	//RAZE(pRef); // always ours now
	if(pRef!=NULL)
	{ 
		pRef->destroy();
	//	delete pRef; 
	//	pRef = NULL;
		RAZE(pRef); 
	
	}
	//theStr.~string();
	theStr.erase();
#ifdef _DEBUG_ALLOC
		clog<<"   String destrct 0x"<<hex<<((unsigned int)this) <<dec<<" ( "<<__LINE__<<" )"<<endl;
		if ( ((unsigned int)this) == 0x146cfe8 )
		{
			clog<<"   Critical string deleted."<<endl;
		}
#endif
}




/*************************************************************************************************
 *
 *   $History: ddbPrimatives.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/02/03    Time: 8:03a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * 
 *************************************************************************************************
 */



