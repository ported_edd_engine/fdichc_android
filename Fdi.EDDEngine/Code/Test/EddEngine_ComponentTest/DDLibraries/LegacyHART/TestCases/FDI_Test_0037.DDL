/*** FDI DeviceModelServices EDD TestCase ***/
#include "Macros.h"
#include "Methods.h"

// Manufacturer 0xFD = "Special"
MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0037, DEVICE_REVISION 1, DD_REVISION 1

/****************************************************************************** */
/*    Common Tables and Univeral Commands (mandatory)                           */
/* **************************************************************************** */
/* */                                                                           
/* */  IMPORT STANDARD _TABLES, DEVICE_REVISION 6, DD_REVISION 1                
/* */  {                                                                        
/* */       
/* */       EVERYTHING;                                                       
/* */  }                                                                        
/* */                                                                           
/* */  IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 5, DD_REVISION 1             
/* */  {                                                                        
/* */      EVERYTHING;
/* */  }                                                                        
/* */

/****************************************************************************** */
/* Dynamic variables (mandatory)                                                */
/* **************************************************************************** */
/* */  
/* */  
/* */  ARRAY OF VARIABLE dynamic_variables
/* */  {
/* */      ELEMENTS
/* */      {
/* */          PRIMARY, primary_value;
/* */      }
/* */  }
/* */




VARIABLE primary_value
{
    LABEL "Primary value as float";
    TYPE FLOAT;
    CLASS DEVICE & DYNAMIC;
}


/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    STYLE MENU;
    ITEMS
    {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */
MENU main_test_menu
{
    LABEL "Main test menu";
    STYLE WINDOW;
    ITEMS
    {
        menu_complex_element
    }
}
/****************************************************************************** */
/* Menus                                                             */
/****************************************************************************** */
MENU menu_complex_element
{
    LABEL "";
    ITEMS
    {
        // /ParameterSet/recorded_signals/signal_info_0/TAKEN_BY
        recorded_signals[0].TAKEN_BY,
        // /ParameterSet/recorded_signals/signal_info_1/SIGNAL/sample_5
        recorded_signals[1].SIGNAL[5]

    }
}

/****************************************************************************** */
/* Array and Collection                                                               */
/****************************************************************************** */
ARRAY echo_signal
{
    TYPE sample;
    NUMBER_OF_ELEMENTS 5;
}

COLLECTION signal_info
{
    MEMBERS
    {
        DATE_STAMP, date_taken;
        TAKEN_BY, operator;
        NOTES, operating_conditions;
        DT, sample_interval;
        SIGNAL, echo_signal;
    }
}

LIST recorded_signals
{
    TYPE signal_info;
	COUNT 2;  
}

FILE stored_signals
{
    MEMBERS
    {
        LOCATION, gauge_location;
        INSTR_TAG, taglabel;
        SIGNALS, recorded_signals;
    }
}


/****************************************************************************** */
/* Variables                                                              */
/****************************************************************************** */
VARIABLE gauge_location 
{
    TYPE ASCII(32);
}

VARIABLE taglabel 
{
    TYPE ASCII(32);
}
VARIABLE date_taken 
{
    TYPE DATE;
}

VARIABLE operator 
{
    TYPE ASCII(32);
	CLASS LOCAL;
	DEFAULT_VALUE "Nobody";
    
}
VARIABLE operating_conditions 
{
    TYPE ASCII(32);
}

VARIABLE sample_interval
{
    TYPE FLOAT; 
}

VARIABLE sample 
{
    TYPE UNSIGNED_INTEGER(2);
	CLASS LOCAL;
	DEFAULT_VALUE 42;
}

 