/******************************************************************************
 * $Header: Uc.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:55 AM$
 *
 * Copyright 1999 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:	
 *		General include file for the Uc2X Unicode Conversion utility classes.
 *													   
 * $Workfile: Uc.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#if !defined(UC_H)
#define UC_H

//// Public Uc headers for client

#include <Uc2T.h>
#include <Uc2A.h>
#include <Uc2W.h>

#endif // !defined(UC_H)
