/******************************************************************************
 * $Header: Uc2T.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:55 AM$
 *
 * Copyright 1999 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *		Include file for Uc2T class.
 *													   
 * $Workfile: Uc2T.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#if !defined(AFX_UC2T_H__D6CE8F9A_E928_11D2_81D2_006008AF8E7D__INCLUDED_)
#define AFX_UC2T_H__D6CE8F9A_E928_11D2_81D2_006008AF8E7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <AMSMiscUtilExport.h>

/******************************************************************************
Class:	Uc2T

Description:
	Unicode conversion (Uc) utility class for converting to const generic string.

Responsibilites:
	* Convert strings to const generic string.
	* Convert non-terminated strings to const generic string.
	* Encapsulate string-buffer memory allocation/deallocation.
	* Avoid dynamic allocation when possible.

Author:	Kevin Mixter
******************************************************************************/
class AMSMISCUTIL_EXPORT Uc2T  
{
public:
	Uc2T(LPCSTR);						// Construct from ANSI (narrow) string
	Uc2T(LPCSTR, const int);			// Construct from non-terminated ANSI (narrow) string
	Uc2T(char);							// Construct from a single ANSI character

	Uc2T(LPCWSTR);						// Construct from Unicode (wide) string
	Uc2T(LPCWSTR, const int);			// Construct from non-terminated Unicode (wide) string

	virtual ~Uc2T();					// Destruct

	LPCTSTR asLPCTSTR() const;			// Conversion to const generic string
	TCHAR asTCHAR() const;				// Conversion to const character

	int getLengthInChars() const;		// Get length (not including terminator) of converted string in characters
	int getSizeInBytes() const;			// Get size (including terminator) of converted string in bytes

protected:
	Uc2T();
	void constructA2CT(LPCSTR, const int);	// Construct converted/terminated string from non-terminated ANSI string
	void constructW2CT(LPCWSTR, const int);	// Construct converted/terminated string from non-terminated Unicode string

	LPTSTR m_ptSzBuffer;				// Generic string buffer, allocated as needed
	LPCTSTR m_pctSz;					// Generic string ptr

private:
	// Prevent the use of these methods by making them private and not providing any implementation
	Uc2T(const Uc2T&);
	Uc2T& operator=(const Uc2T&) const;
};

#endif // !defined(AFX_UC2T_H__D6CE8F9A_E928_11D2_81D2_006008AF8E7D__INCLUDED_)
