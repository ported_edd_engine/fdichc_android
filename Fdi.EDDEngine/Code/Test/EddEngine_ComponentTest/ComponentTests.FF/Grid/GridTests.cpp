#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_reading_GRID_from_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_grid_item_ID;
            static  FDI_GENERIC_ITEM* genericItem;
            static FLAT_GRID* test_grid_1;
            static ITEM_ID* test_grid_1_item_ID;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0003\\TestCase0003.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_grid_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block_grid", test_block_grid_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_grid_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                
                FDI_PARAM_SPECIFIER test_grid_specifier;
                engine->FillParamSpecifier("grid_1", &test_grid_specifier);
                genericItem = engine->GetGenericItem(
                    test_grid_specifier.id, 
                    nsEDDEngine::ITYPE_GRID,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_GRID),
                    iBlockInstance);

                test_grid_1 = dynamic_cast<FLAT_GRID*>(genericItem->item);
                
                test_grid_1_item_ID = new ITEM_ID();

                engine->Instance->GetItemIdFromSymbolName(L"grid_1", test_grid_1_item_ID);
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_grid_1_item_ID;
                delete test_block_grid_item_ID;
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_item_id_for_first_grid()
            {
                Assert::AreEqual( *test_grid_1_item_ID, test_grid_1->id);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_label_for_first_grid()
            {
                Assert::AreEqual("Grid 1", gcnew String(test_grid_1->label.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_help_for_first_grid()
            {
                Assert::AreEqual("this is grid 1", gcnew String( test_grid_1->help.c_str()));
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_width_for_first_grid()
            {
                Assert::AreEqual(1, (int) test_grid_1->width);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_height_for_first_grid()
            {
                Assert::AreEqual(0, (int) test_grid_1->height);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_orientation_for_first_grid()
            {
                Assert::AreEqual(0, (int) test_grid_1->orientation);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_handling_for_first_grid()
            {
                Assert::AreEqual(1, (int) test_grid_1->handling);
            }

            [TestMethod, TestCategory("GridTest")]
            void It_should_have_the_correct_number_of_vectors_for_first_grid()
            {
                Assert::AreEqual(6, (int) test_grid_1->vectors.count);
            }
        };
        
    } // namespace FF
} // namespace ComponentTests
