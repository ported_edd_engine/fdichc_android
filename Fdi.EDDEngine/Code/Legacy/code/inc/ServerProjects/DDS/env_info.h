/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)env_info.h	30.3  30  14 Nov 1996
 */
#include "dds_upcl.h"
#include "PlatformCommon.h"
#include <map>

#ifndef ENV_INFO_H
#define ENV_INFO_H

/*
 * This file contains the definition for ENV_INFO.  It is used to pass
 * environment information throughout the application and libraries.  This
 * structure may be modified as needed to communicate needed parameters to
 * appropriate sub-systems.  The block_handle element within ENV_INFO is
 * the only element that is required by DDS.
 */

/**
 ** Structures and function prototypes to support upcalls to an application
 **/
//#ifdef _WIN32    //TODO fixit : refer  ServerProjects/DDS/env_info forward declaration of Struct
typedef struct tag_EVAL_VAR_VALUE{
    unsigned short  type;	/** valid types are defined in ddldefs.h (ie. INTEGER) **/
    unsigned int  size;	/** number of bytes a value can occupy **/
    union {
        float           f;
        double          d;
        LONGLONG        ll;
        ULONGLONG       ull;
        STRING          s;
        BINARY			b;

    }               val;	/** value of a variable (obtained from app) **/

    tag_EVAL_VAR_VALUE();	// constructor
    tag_EVAL_VAR_VALUE(const tag_EVAL_VAR_VALUE &rhs);	// copy constructor

    ~tag_EVAL_VAR_VALUE();	// destructor

    tag_EVAL_VAR_VALUE& operator= (const tag_EVAL_VAR_VALUE &rhs);

}               EVAL_VAR_VALUE;
//#endif

typedef std::map<OP_REF, tag_EVAL_VAR_VALUE> ParamMap;

typedef struct tag_ENV_INFO{
	BLOCK_HANDLE	block_handle;	/* This is used to specify the block */
  	void			*app_info;
	void*			value_spec;
    wchar_t			lang_code[10];
    ParamMap		*pParamMap;

	tag_ENV_INFO()
	{
		block_handle = 0;
		app_info = nullptr;
		value_spec = nullptr;
		if(lang_code != NULL)
		{
			const size_t maxSize = (sizeof(lang_code)/sizeof(wchar_t)); 
			PS_Wcsncpy(lang_code, maxSize, L"", _TRUNCATE);
		}
		pParamMap = nullptr;
	}

	tag_ENV_INFO( BLOCK_HANDLE m_block_handle, void* pAppInfo, void* pValueSpec, const wchar_t* plangCode)
	{
		block_handle = m_block_handle;
		app_info = pAppInfo;
		value_spec = pValueSpec;
		if(lang_code != NULL)
		{
			const size_t maxSize = (sizeof(lang_code)/sizeof(wchar_t)); 
			PS_Wcsncpy(lang_code, maxSize, plangCode, _TRUNCATE);
		}
		pParamMap = nullptr;
	}

	~tag_ENV_INFO()
	{
		if(pParamMap)
		{
			pParamMap->clear();
		}

		delete pParamMap;
	}

} ENV_INFO;


#endif 	/* ENV_INFO_H */
