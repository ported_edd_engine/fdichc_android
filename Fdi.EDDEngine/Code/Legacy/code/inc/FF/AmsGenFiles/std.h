////////////////////////////////////////////////////////////////////////
////	Simple universal definitions.
////////////////////////////////////////////////////////////////////////

#pragma once


// This hides the security warnings (strcpy, etc) from showing in the compiler
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif // !_CRT_SECURE_NO_WARNINGS

// This disables POSIX warnings
#define _CRT_NONSTDC_NO_WARNINGS 1
//// Disables _stprintf warnings 
#define _CRT_NON_CONFORMING_SWPRINTFS 1
// Disable the warning for conditional expression is constant
#pragma warning(disable: 4127)

/*
 *	Allow "prototyping" for reference, but use K&R syntax for code
 */

#define P(x)	x

#ifndef LITTLE_ENDIAN
#define	LITTLE_ENDIAN	1	/* Uses LITTLE_ENDIAN byte order */
#endif

typedef enum { FMS_FALSE, FMS_TRUE } fms_boolean;


/*
 *	For use with qsort() and bsearch()
 */

typedef int (*CMP_FN_PTR) P((const void *, const void *));



/*
 *  One global to set the appropriate message box flags based on operating
 *  system and how process is started.
 */

extern unsigned int message_box_flags;
void set_message_box_flags_for_nt( void );



/*
 *	Assert macros
 */

extern void panic (const char*, unsigned, const char *,...);

#define NELEM(x) (sizeof(x) / sizeof((x)[0]))

#undef VERIFY

#define REQUIRE(cond)	extern int __require[(cond)]

#define insist(x) { if (!(x)) (*(int*) 1) = 0; }

#ifdef DEBUG

#  define ASSERT_DBG(cond)	\
	 if(!(cond)) panic(__FILE__, __LINE__, "%s", #cond)
#  define ASSERT_RET(cond,param) \
     if(!(cond)) panic(__FILE__, __LINE__, "Condition not true: passed value = %d\n", (param))
#  define CRASH_RET(param) \
     panic(__FILE__, __LINE__, "Crash: passed value = %d\n", (param))

#  define CRASH_DBG() \
	panic(__FILE__, __LINE__, "Crash") 
	
	

#else

#  define ASSERT_DBG(cond)
#  define ASSERT_RET(cond,param)	if(!(cond)) return (param)
#  define CRASH_RET(param)		return (param)
#  define CRASH_DBG()

#endif				/* DEBUG */

/*
 *	Standard defines
 */

#define FALSE 0
#define TRUE 1

/*
 *	Standard typedefs
 */

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

typedef unsigned char UINT8;
typedef short INT16;
typedef unsigned short UINT16;
typedef int INT32;
typedef unsigned int UINT32;

typedef ulong DDITEM;


struct BLOCK;

#define SB_CODE_PAGE	1252	// Code Page for singlebyte narrow/wide conversion

