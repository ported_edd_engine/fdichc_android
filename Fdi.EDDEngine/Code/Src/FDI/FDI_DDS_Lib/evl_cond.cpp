/**
 *	@(#) $Id: evl_cond.cpp,v 1.1 2012/07/10 20:59:42 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions used in finding the appropriate
 *	pieces of conditional expressions.
 */

#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif

/*********************************************************************
 *
 *	Name: ddl_add_chunk_list
 *	ShortDesc: Add another chunk to chunk list.
 *
 *	Description:
 *		ddl_add_chunk_list will add the item pointed at by new_chunk to the
 *		list of chunks in list_ptr.  If the current list is full, additional
 *		elements will be added to the list
 *
 *	Inputs:
 *		new_chunkp - pointer to the address of the binary
 *		new_size - size of the binary
 *		list_ptr - pointer to a CHUNK_LIST structure where the result will
 *				be stored.
 *
 *	Outputs:
 *		list_ptr - pointer to a CHUNK_LIST structure containing the result.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		DDS_OUT_OF_MEMORY
 *		DDL_ENCODING_ERROR
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
static int
ddl_add_chunk_list(
unsigned char  *new_chunk,
DDL_UINT        new_size,
CHUNK_LIST     *list_ptr)
{
	CHUNK          *ptr;	/* local ptr to a list -f chunks */

#ifdef DDSTEST
	TEST_FAIL(DDL_ADD_CHUNK_LIST);
#endif

	if (list_ptr->size >= list_ptr->limit) {

		/*
		 * a new structure must be malloc'd or an existing stucture
		 * must be realloc'd.
		 */

		if (list_ptr->limit == DEFAULT_CHUNK_LIST_SIZE) {

			/*
			 * The list local to the calling routine is still being
			 * used.  A new list must be malloc'd and the info
			 * copied to the new structure.
			 */

			ptr = (CHUNK *) malloc(((size_t) list_ptr->limit + INCR_CHUNK_LIST_SIZE) *
				sizeof(CHUNK));
			if (ptr == NULL) {
				return DDS_OUT_OF_MEMORY;
			}

			memcpy((char *) ptr, (char *) list_ptr->list,
				(size_t) (list_ptr->limit * sizeof(CHUNK)));
		}
		else if (list_ptr->limit > DEFAULT_CHUNK_LIST_SIZE) {

			/*
			 * A list has already been malloc'd.  All we have to do
			 * is realloc a new structure.
			 */

			ptr = (CHUNK *) realloc((void *) list_ptr->list,
				((size_t) list_ptr->limit + INCR_CHUNK_LIST_SIZE) * sizeof(CHUNK));
			if (ptr == NULL) {
				return DDS_OUT_OF_MEMORY;
			}
		}
		else {		/* list_ptr->limit < DEFAULT_CHUNK_LIST_SIZE 	 */
			return DDL_ENCODING_ERROR;
		}

		list_ptr->limit += INCR_CHUNK_LIST_SIZE;	/* assign a new limit */
		list_ptr->list = ptr;	/* point to the new list */
	}

	/* update the list */
	ptr = &(list_ptr->list[list_ptr->size++]);

	ptr->chunk = new_chunk;
	ptr->size = new_size;

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddl_delete_chunk_list
 *	ShortDesc: delete a list of chunks.
 *
 *	Description:
 *		ddl_delete_chunk_list will delete the list of chunks pointed at by
 * 		list_ptr.
 *
 *	Inputs:
 *		list_ptr - pointer to a CHUNK_LIST structure.
 *
 *	Outputs:
 *		list_ptr - pointer to a CHUNK_LIST structure.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/
void
ddl_delete_chunk_list(
CHUNK_LIST     *list_ptr)
{

	free((void *) list_ptr->list);

	list_ptr->size = 0;
	list_ptr->limit = 0;
	list_ptr->list = NULL;

}



/*********************************************************************
 *
 *	Name: ddl_cond
 *	ShortDesc: Parse conditional information from a simple binary chunk.
 *
 *	Description:
 *		ddl_cond will parse the conditional information from a simple
 *		binary chunk.  When a simple object is found, it is returned,
 *		along with the dependency information.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		val_ptr - pointer to a CHUNK structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		val_ptr - pointer to a CHUNK structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from DDL_PARSE_TAG()
 *		error returns from ddl_eval_expr()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_cond(
unsigned char **chunkp,
DDL_UINT       *size,
CHUNK          *val_ptr,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             if_true;/* evaluate the THEN or ELSE branch of IF */
	int             this_choice;	/* TRUE if CASE expr equals SELECT expr */
	unsigned char  *leave_pointer;	/* end-of-chunk pointer */
	int             match;	/* TRUE if value was obtained from CASE */
	int             rc;
	DDL_UINT        len;	/* # of DATA bytes associated with TAG */
	DDL_UINT        tag;	/* "label" associated with current chunk */
	EXPR            value;	/* value of an IF or SELECT expression */
	EXPR            cvalue;	/* value of a CASE expression */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_COND);
#endif

	/*
	 * Parse the tag to find out if this is a SELECT statement, an if/else,
	 * or a simple assignment.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	/*
	 * Adjust size of remaining chunk.
	 */

	*size -= len;

	/*
	 * Calcuate the return chunk pointer (we may be able to use it it later
	 * for an early exit).
	 */

	leave_pointer = *chunkp + len;

	switch (tag) {
	case IF_TAG:		/* IF */

		/*
		 * If the application is not looking for a value,  update the
		 * chunk pointer and return.
		 * 
		 * Passing in a NULL for val_ptr is a valid thing to do.  The
		 * calling routine knows nothing about parsing tags and
		 * adjusting size and chunk pointers.
		 */

		if (!val_ptr) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * We are now going to evaluate the expression associated with
		 * the IF tag.  ddl_eval_expr will return either the value of
		 * the of the expression (i.e. &value) or the dependency
		 * information (i.e. depinfo) or both.
		 */

		rc = ddl_eval_expr(chunkp, &len, val_ptr ? &value : NULL,
			depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		/*
		 * "if_true" dictates whether we execute the THEN portion of
		 * the IF, or the ELSE portion of the IF.
		 */

		if (ddl_expr_nonzero(&value)) {
			if_true = TRUE;	/** evaluate the THEN portion of the IF **/
		}
		else {
			if_true = FALSE;	/** evaluate the ELSE portion of the IF **/
		}

		/*
		 * Evaluate the true part of the IF.  If we aren't interested
		 * in dependency information, we will skip the false branch of
		 * the IF.
		 */

		if (if_true == TRUE) {
			rc = ddl_cond(chunkp, &len, val_ptr, depinfo, env_info,
				var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			/*
			 * We have a value and the user has no interest in
			 * dependency info
			 */

			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}
		else {

			/*
			 * The IF condition is false.  Process the true branch
			 * for dependency information and to correctly position
			 * the chunk pointer.
			 */

			rc = ddl_cond(chunkp, &len, (CHUNK *) NULL, (nsEDDEngine::OP_REF_LIST *) NULL,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			/*
			 * Evaluate the false branch of the IF. If this is an
			 * ELSE-LESS IF, len should be 0
			 */

			if (len > 0) {
				rc = ddl_cond(chunkp, &len, val_ptr, depinfo, env_info,
					var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}
		break;

	case SELECT_TAG:	/* SELECT */

		/*
		 * If there is nothing to return (the value pointer is NULL, we
		 * will update the chunk pointer and return.  Note that size
		 * has already been updated.
		 */

		if (!val_ptr) {
			*chunkp = leave_pointer;
			return DDL_SUCCESS;
		}

		/*
		 * We are now going to evaluate the expression associated with
		 * the SELECT tag.  ddl_eval_expr will return either the value
		 * of the of the expression (i.e. &value) or the dependency
		 * information (i.e. depinfo) or both.
		 * 
		 * If dependency information is desired all SELECT paths need to
		 * be traversed.
		 */

		rc = ddl_eval_expr(chunkp, &len, val_ptr ? &value : (EXPR *) NULL,
			depinfo, env_info, var_needed);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		/*
		 * "match == TRUE" indicates that we have obtained a value from
		 * the correct CASE statement.  If "match == FALSE" the DEFAULT
		 * statement(s) will have to be evaluated.
		 */

		match = FALSE;

		/*
		 * Process all of the CASE statements. If tag = 0 indicates a
		 * CASE statement. If tag = 1 indicates a DEFAULT statement. An
		 * expression follows all CASE statements.
		 */

		while (len > 0) {
			DDL_PARSE_TAG(chunkp, &len, &tag, (DDL_UINT *) NULL_PTR);

			switch (tag) {
			case CASE_TAG:	/** CASE label associated with SELECT tag */

				/*
				 * If we are returning a value, evaluate the
				 * expression (i.e. expression associated with
				 * the CASE statement) and see if it equals the
				 * controlling expression (i.e. expression
				 * associated with SELECT statement).
				 * 
				 * If we aren't returning a value, process the
				 * expression to gather dependency information.
				 */

				rc = ddl_eval_expr(chunkp, &len,
					val_ptr ? &cvalue : (EXPR *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				/*
				 * "this_choice" will be set to TRUE when the
				 * the LABEL of the CASE statement is equal the
				 * value of the SELECT expression.
				 */

				if ((val_ptr) && (ddl_exprs_equal(&value, &cvalue))) {
					this_choice = TRUE;
				}
				else {
					this_choice = FALSE;
				}

				/*
				 * This CASE expression equals the controlling
				 * expression and we are returning a value.
				 * Therefore, we will evaluate this choice. If
				 * we are not returning dependency information,
				 * we can just skip the rest of the case
				 * statements.
				 */

				if ((this_choice == TRUE) && (match == FALSE)) {
					rc = ddl_cond(chunkp, &len, val_ptr, depinfo,
						env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}

					*chunkp = leave_pointer;
					return DDL_SUCCESS;
				}
				else {

					/*
					 * Either this is not the choice we
					 * want, or we are only gathering
					 * dependency information.  Process
					 * this choice, and continue.
					 */

					rc = ddl_cond(chunkp, &len, (CHUNK *) NULL, depinfo,
						env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}

				break;

			case DEFAULT_TAG:	/** DEFAULT label associated with  SELECT tag */

				/*
				 * The default statement.  If we have not found
				 * a match, evaluate this.  If we have found a
				 * match, we are only doing this for dependency
				 * information, so process this.
				 */

				rc = ddl_cond(chunkp, &len, match ? (CHUNK *) NULL : val_ptr,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				break;

			default:	/** label was not a CASE or DEFAULT label */
				return DDL_ENCODING_ERROR;
			}
		}

		break;

	case OBJECT_TAG:	/* simple object */

		/*
		 * We have finally gotten to the actual value!
		 */

		if (val_ptr) {
			val_ptr->chunk = *chunkp;
			val_ptr->size = len;
		}

		*chunkp = leave_pointer;
		break;

	default:
		return DDL_ENCODING_ERROR;

	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddl_cond_list
 *	ShortDesc: Parse conditional information from a binary chunk containing
 *			   lists of items.
 *
 *	Description:
 *		ddl_cond_list will parse the conditional information from a binary
 *		chunk containing lists of items.  When a simple objects is found,
 *		it is added to a list.  When the binary chunk has been exhausted,
 *		the list of items, along with the dependency information will be
 *		returned.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list_ptr - pointer to a CHUNK_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		tag_expected - value of the list (i.e. SEQUENCE) tag to be parsed.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list_ptr - pointer to a CHUNK_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from DDL_PARSE_TAG()
 *		error returns from ddl_eval_expr()
 *		error returns from ddl_add_chunk_list()
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

int
ddl_cond_list(
unsigned char **chunkp,
DDL_UINT       *size,
CHUNK_LIST     *list_ptr,
nsEDDEngine::OP_REF_LIST    *depinfo,
int             tag_expected,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             if_true = FALSE;		/** evaluate the THEN or ELSE branch of IF */
	int             this_choice = FALSE;	/** TRUE if CASE expr equals SELECT expr */
	unsigned char  *continue_pointer = NULL;/** ptr to next chunk of the binary */
	int             match = FALSE;			/** TRUE if value was obtained from CASE */
	int             rc = DDL_SUCCESS;
	DDL_UINT        len = 0;		/** size of entire binary */
	DDL_UINT        len1 = 0;		/** size of chunk currently being evaluated */
	DDL_UINT        tag = 0;		/** "label" associated with current chunk */
	EXPR            value = {0};	/** value of an IF or SELECT expression */
	EXPR            cvalue = {0};	/** value of a CASE expression */

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_COND_LIST);
#endif

	/*
	 * Parse the tag.  We are expecting a SEQENCE_LIST of some type (i.e.
	 * REFERENCE_SEQLIST_LIST).  If tag is not a LIST_TAG, get out of here.
	 */

	DDL_PARSE_TAG(chunkp, size, &tag, &len);

	if ((int) tag != tag_expected) {
		PS_TRACE(L"\n*** error (ddl_cond_list) : unexpected tag found : expected %d found %d", tag_expected, tag);
		return DDL_ENCODING_ERROR;
	}

	*size -= len;

	/*
	 * If there is nothing to return (the list pointer is NULL), update the
	 * chunk pointer and return.
	 * 
	 * Passing in a NULL for list is a valid thing to do. The calling routine
	 * knows nothing about parsing tags and adjusting size and chunk
	 * pointers.
	 */

	if (!list_ptr) {
		*chunkp += len;
		return DDL_SUCCESS;
	}

	while (len > 0) {

		/*
		 * Parse the tag to find out if this a SELECT statement, and
		 * if/else, or a simple assignment.
		 */

		DDL_PARSE_TAG(chunkp, &len, &tag, &len1);

		len -= len1;

		/*
		 * Calcuate the chunk pointer for the next loop (we may be able
		 * to use it later for an early continue).
		 */

		continue_pointer = *chunkp + len1;

		switch (tag) {
		case IF_TAG:	/* IF */

			/*
			 * We are now going to evaluate the expression
			 * associated with the IF tag.  ddl_eval_expr will
			 * return either the value of the of the expression
			 * (i.e. &value) or the dependency information (i.e.
			 * depinfo) or both.
			 */

			rc = ddl_eval_expr(chunkp, &len1, list_ptr ? &value : (EXPR *) NULL,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			/*
			 * "if_true" dictates whether we execute the THEN
			 * portion of the IF, or the ELSE portion of the IF.
			 */

			if (ddl_expr_nonzero(&value)) {
				if_true = TRUE;	/** evaluate the THEN portion of the IF **/
			}
			else {
				if_true = FALSE;	/** evaluate the ELSE portion of the IF **/
			}


			/*
			 * Evaluate the true part of the if.  If we aren't
			 * interested in dependency information, we can just
			 * skip the false branch of the if.
			 */

			if (if_true == TRUE) {
				rc = ddl_cond_list(chunkp, &len1, list_ptr, depinfo,
					tag_expected, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				/*
				 * We have a value and the user has no interest
				 * in dependency info
				 */

				*chunkp = continue_pointer;
				continue;

			}
			else {

				/*
				 * The if condition is false.  process the true
				 * branch for dependency information and to
				 * correctly position the chunk pointer.
				 */

				rc = ddl_cond_list(chunkp, &len1, (CHUNK_LIST *) NULL,
					(nsEDDEngine::OP_REF_LIST *) NULL, tag_expected, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}

				/*
				 * Evaluate the false branch of the if.
				 */

				if (len1 > 0) {
					rc = ddl_cond_list(chunkp, &len1, list_ptr, depinfo,
						tag_expected, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}
			break;

		case SELECT_TAG:	/* SELECT */

			/*
			 * We are now going to evaluate the expression
			 * associated with the SELECT tag.  ddl_eval_expr will
			 * return the value of the of the expression (i.e.
			 * &value) and dependency information (i.e. depinfo).
			 */

			rc = ddl_eval_expr(chunkp, &len1, list_ptr ? &value : (EXPR *) NULL,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			/*
			 * "match == TRUE" indicates that we have obtained a
			 * value from the correct CASE statement.  If "match ==
			 * FALSE" the DEFAULT statement(s) will have to be
			 * evaluated.
			 */

			match = FALSE;

			/*
			 * Process all of the CASE statements. tag = 0
			 * indicates a CASE statement. tag = 1 indicates a
			 * DEFAULT statement. An expression follows all CASE
			 * statements.
			 */

			while (len1 > 0) {
				DDL_PARSE_TAG(chunkp, &len1, &tag, (DDL_UINT *) NULL_PTR);

				switch (tag) {
				case CASE_TAG:	/** CASE label associated with SELECT tag */

					/*
					 * If we are returning a value,
					 * evaluate the expression (i.e.
					 * expression associated with the CASE
					 * statement) and see if it equals the
					 * controlling expression (i.e.
					 * expression associated with SELECT
					 * statement).
					 * 
					 * If we aren't returning a value, process
					 * the expression to gather dependency
					 * information.
					 */

					rc = ddl_eval_expr(chunkp, &len1,
						list_ptr ? &cvalue : (EXPR *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}

					/*
					 * "this_choice" will be set to TRUE
					 * when the the LABEL of the CASE
					 * statement is equal to the value of
					 * the SELECT expression.
					 */


					if ((list_ptr) && (ddl_exprs_equal(&value, &cvalue))) {
						this_choice = TRUE;
					}
					else {
						this_choice = FALSE;
					}


					/*
					 * This CASE expression equals the
					 * controlling expression and we are
					 * returning a value.  Evaluate this
					 * choice. If we are not returning
					 * dependency information, we can just
					 * skip the rest of the case
					 * statements.
					 */

					if ((this_choice == TRUE) && (match == FALSE)) {
						rc = ddl_cond_list(chunkp, &len1, list_ptr, depinfo,
							tag_expected, env_info, var_needed);
						if (rc != DDL_SUCCESS) {
							return rc;
						}

						match = TRUE;
						*chunkp = continue_pointer;
						len1 = 0;
						continue;

					}
					else {

						/*
						 * Either this is not the
						 * choice we want, or we are
						 * only gathering dependency
						 * information.  Process this
						 * choice, and continue.
						 */

						rc = ddl_cond_list(chunkp, &len1, (CHUNK_LIST *) NULL,
							depinfo, tag_expected, env_info, var_needed);
						if (rc != DDL_SUCCESS) {
							return rc;
						}
					}

					break;

				case DEFAULT_TAG:	/** DEFAULT label associated with **/
					/** SELECT tag **/

					/*
					 * The default statement.  If we have
					 * not found any match, evaluate this.
					 * If we have found a match, we are
					 * only doing this for dependency
					 * information, so process this.
					 */

					rc = ddl_cond_list(chunkp, &len1,
						match ? (CHUNK_LIST *) NULL : list_ptr,
						depinfo, tag_expected, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}

					break;

				default:
					return DDL_ENCODING_ERROR;
				}
			}

			break;

		case OBJECT_TAG:	/* simple object */

			/*
			 * We have finally gotten to the actual value!
			 */

			if (list_ptr) {
				rc = ddl_add_chunk_list(*chunkp, len1, list_ptr);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			*chunkp = continue_pointer;
			break;

		default:
			return DDL_ENCODING_ERROR;

		}
	}


	return DDL_SUCCESS;
}

