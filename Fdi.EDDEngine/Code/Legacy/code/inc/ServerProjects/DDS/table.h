/*
 *	@(#) $Id: table.h,v 1.3 1996/03/14 04:08:04 stevbey Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 */

#ifndef TABLE_H
#define TABLE_H

#ifndef STD_H
#include "std.h"
#endif

#ifndef FLATS_H
#include "flats.h"
#endif

#ifndef DDS_UPCL_H
#include "dds_upcl.h"
#endif
#include <map>

typedef UINT32	FILE_OFFSET;

typedef union {
	OBJECT_INDEX	object_index;
	FILE_OFFSET		file_offset;
} DD_REFERENCE;

typedef std::map<ITEM_ID, OP_REF_LIST*> DOMINANT_TBL;
/*
 *	Abbreviations Used:
 *
 *	blk - block
 *	char - characteristic
 *	dict - dictionary
 *	dir - directory
 *	elem - element
 *	id - identifier
 *	mem - member
 *	param - parameter
 *	prog - program
 *	rec - record
 *	ref - reference
 *	rel - relation
 *	tbl - table
 *
 */


/*
 *	Block Directory Definitions
 */


/* Block Item Table */

typedef struct {
	ITEM_ID		blk_item_id;
	int			blk_item_name_tbl_offset;
} BLK_ITEM_TBL_ELEM;

typedef struct {
	int					 count;
	BLK_ITEM_TBL_ELEM	*list;
} BLK_ITEM_TBL;


/* Block Item Name Table */

typedef struct {
	ITEM_ID		blk_item_name;
	int			item_tbl_offset;
	int			param_tbl_offset;
	int			param_list_tbl_offset;
	int			rel_tbl_offset;
	int			read_cmd_tbl_offset;
	int			read_cmd_count;                     
	int			write_cmd_tbl_offset;
	int			write_cmd_count;

} BLK_ITEM_NAME_TBL_ELEM;

typedef struct {
	int						 count;
	BLK_ITEM_NAME_TBL_ELEM	*list;
} BLK_ITEM_NAME_TBL;


/* Parameter Table */

typedef struct {
	int		blk_item_name_tbl_offset;
	int		param_mem_tbl_offset;
	int		param_mem_count;
	int		param_elem_tbl_offset;
	int		param_elem_count;
	int		array_elem_item_tbl_offset;
	int		array_elem_count;
	int		array_elem_type_or_var_type;
	int		array_elem_size_or_var_size;
	ulong	array_elem_class_or_var_class;
} PARAM_TBL_ELEM;

typedef struct {
	int				 count;
	PARAM_TBL_ELEM	*list;
} PARAM_TBL;


/* Parameter Member Table */

typedef struct {
	int		item_tbl_offset;
	int		param_mem_type;
	int		param_mem_size;
	ulong	param_mem_class;
	int		rel_tbl_offset;
} PARAM_MEM_TBL_ELEM;

typedef struct {
	int					 count;
	PARAM_MEM_TBL_ELEM	*list;
} PARAM_MEM_TBL;

/* Parameter Element Table */

typedef struct {
	SUBINDEX	param_elem_subindex;
	int			rel_tbl_offset;
} PARAM_ELEM_TBL_ELEM;

typedef struct {
	int					 count;
	PARAM_ELEM_TBL_ELEM	*list;
} PARAM_ELEM_TBL;


/* Parameter Member Name Table */

typedef struct {
	ITEM_ID		param_mem_name;
	int			param_mem_offset;
} PARAM_MEM_NAME_TBL_ELEM;

typedef struct {
	int							 count;
	PARAM_MEM_NAME_TBL_ELEM		*list;
} PARAM_MEM_NAME_TBL;


/* Parameter List Table */

typedef struct {
	int		blk_item_name_tbl_offset;
	int		param_list_mem_tbl_offset;
	int		param_list_mem_count;
} PARAM_LIST_TBL_ELEM;

typedef struct {
	int						 count;
	PARAM_LIST_TBL_ELEM		*list;
} PARAM_LIST_TBL;


/* Parameter List Member Table */

typedef struct {
	int		blk_item_name_tbl_offset;
} PARAM_LIST_MEM_TBL_ELEM;

typedef struct {
	int							 count;
	PARAM_LIST_MEM_TBL_ELEM		*list;
} PARAM_LIST_MEM_TBL;


/* Parameter List Member Name Table */

typedef struct {
	ITEM_ID		param_list_mem_name;
	int			param_list_mem_tbl_offset;
} PARAM_LIST_MEM_NAME_TBL_ELEM;

typedef struct {
	int								 count;
	PARAM_LIST_MEM_NAME_TBL_ELEM	*list;
} PARAM_LIST_MEM_NAME_TBL;


/* Characteristic Member Table */

typedef struct {
	int		item_tbl_offset;
	int		char_mem_type;
	int		char_mem_size;
    ulong   char_mem_class;
	int		rel_tbl_offset;
} CHAR_MEM_TBL_ELEM;

typedef struct {
	int					 count;
	CHAR_MEM_TBL_ELEM	*list;
} CHAR_MEM_TBL;


/* Characteristic Member Name Table */

typedef struct {
	ITEM_ID		char_mem_name;
	int			char_mem_offset;
} CHAR_MEM_NAME_TBL_ELEM;

typedef struct {
	int						 count;
	CHAR_MEM_NAME_TBL_ELEM	*list;
} CHAR_MEM_NAME_TBL;


/* Relation Table */

typedef struct {
	int		wao_item_tbl_offset;
	int		unit_item_tbl_offset;
	int		update_tbl_offset;
	int		update_count;
	int		unit_count;
} REL_TBL_ELEM;

class ManualUnitLookup;

typedef struct {
	int				 count;
	REL_TBL_ELEM	*list;
	ManualUnitLookup *UnitLookup;
} REL_TBL;

/* Update Table */

typedef struct {
	int				op_it_offset ;
	SUBINDEX		op_subindex ;
	int				desc_it_offset ;
} UPDATE_TBL_ELEM;

typedef struct {
	int				 count;
	UPDATE_TBL_ELEM	*list;
} UPDATE_TBL;


/* Command Table Structures */


typedef struct {

	ITEM_ID		id;				
	ulong		value;

} COMMAND_INDEX;


typedef struct {

	ulong				number;
	ulong				transaction;
	SUBINDEX			subindex;
	ushort				weight;
	int					count;
	COMMAND_INDEX		*index_list;

} COMMAND_TBL_ELEM;


typedef struct {
	int					count;
	COMMAND_TBL_ELEM	*list;
} COMMAND_TBL;

typedef struct {
	ITEM_ID				item_id;
	int					rd_count;
	COMMAND_TBL_ELEM*	rd_list;
	int					wr_count;
	COMMAND_TBL_ELEM*	wr_list;
} PTOC_TBL_8_ELEM;

typedef struct {
	int				count;
	PTOC_TBL_8_ELEM	*list;
} COMMAND_TBL_8;

typedef struct {
	int					count;
	ITEM_ID				*list;
} CRIT_PARAM_TBL;

typedef struct tag_RESOLVED_REFERENCE {
	//Members
	unsigned long *pResolvedRef;
	int size;

    bool operator==(const tag_RESOLVED_REFERENCE &rhs);

} RESOLVED_REFERENCE;

typedef struct tag_RESLV_TBL_ELEM {

	RESOLVED_REFERENCE	element_ref;					// RESOLVED_REFERENCE
	int					read_count;					// INTEGER
	COMMAND_TBL_ELEM*	reslv_read_table_list;
	int					write_count;					// INTEGER
	COMMAND_TBL_ELEM*	reslv_write_table_list;

}RESLV_TBL_ELEM;


typedef struct tag_RESLV_REF_TO_CMD_TBL {

	int count;
	RESLV_TBL_ELEM *list;

} RESLV_REF_TO_CMD_TBL;

/* Block Directory Binary */

typedef struct {
	unsigned long	bin_exists;
	unsigned long	bin_hooked;

	BININFO		blk_item_tbl;
	BININFO		blk_item_name_tbl;
	BININFO		param_tbl;
	BININFO		param_mem_tbl;
	BININFO		param_mem_name_tbl;
	BININFO		param_elem_tbl;
	BININFO		param_list_tbl;
	BININFO		param_list_mem_tbl;
	BININFO		param_list_mem_name_tbl;
	BININFO		char_mem_tbl;
	BININFO		char_mem_name_tbl;
	BININFO		rel_tbl;
	BININFO		update_tbl;
	BININFO		command_tbl;
	BININFO		crit_param_tbl;
	BININFO		reslv_ref_tbl;
} BIN_BLOCK_DIR;


/* Block Directory Flat */

typedef struct {
	unsigned long				attr_avail;

	BLK_ITEM_TBL 				blk_item_tbl;
	BLK_ITEM_NAME_TBL			blk_item_name_tbl;
	PARAM_TBL     				param_tbl;
	PARAM_MEM_TBL 				param_mem_tbl;
	PARAM_MEM_NAME_TBL			param_mem_name_tbl;
	PARAM_ELEM_TBL				param_elem_tbl;
	PARAM_LIST_TBL				param_list_tbl;
	PARAM_LIST_MEM_TBL 			param_list_mem_tbl;
	PARAM_LIST_MEM_NAME_TBL		param_list_mem_name_tbl;
	CHAR_MEM_TBL				char_mem_tbl;
	CHAR_MEM_NAME_TBL			char_mem_name_tbl;
	REL_TBL 					rel_tbl;
	UPDATE_TBL					update_tbl;
	COMMAND_TBL					command_tbl;
	COMMAND_TBL_8				command_to_var_tbl;
	CRIT_PARAM_TBL				crit_param_tbl;
	DOMINANT_TBL				*dominant_tbl;
	RESLV_REF_TO_CMD_TBL		reslv_ref_tbl;
} FLAT_BLOCK_DIR;


/*
 *	Device Tables
 */


/* Block Table */

typedef struct {
	ITEM_ID				blk_id;
	int					item_tbl_offset;
	int					char_rec_item_tbl_offset;
	int					char_rec_bint_offset;
	DD_REFERENCE		blk_dir_dd_ref;
	int					usage;
	FLAT_BLOCK_DIR		flat_block_dir;
} BLK_TBL_ELEM;

typedef struct {
	int				 count;
	BLK_TBL_ELEM	*list;
} BLK_TBL;


/* Item Table */

typedef struct {
	ITEM_ID         item_id;
	DD_REFERENCE    dd_ref;
	ITEM_TYPE       item_type;
} ITEM_TBL_ELEM;

typedef struct {
	int				 count;
	ITEM_TBL_ELEM	*list;
} ITEM_TBL;


/* Program Table */

typedef struct {
	ITEM_ID         item_id;
	DD_REFERENCE    dd_ref;
} PROG_TBL_ELEM;

typedef struct {
	int				 count;
	PROG_TBL_ELEM	*list;
} PROG_TBL;


/* Domain Table */

typedef struct {
	ITEM_ID         item_id;
	DD_REFERENCE    dd_ref;
} DOMAIN_TBL_ELEM;

typedef struct {
	int					 count;
	DOMAIN_TBL_ELEM		*list;
} DOMAIN_TBL;


/* String Table */

typedef struct {
	wchar_t			*root;    /* memory chunk pointer */
	int				 count;
	STRING          *list;
} STRING_TBL;


/* Dictionary Reference Table */

typedef struct {
	int			 count;
	UINT32		*list;
} DICT_REF_TBL;


/* Local variable table */


typedef struct {

	ITEM_ID			item_id;
	DD_REFERENCE	dd_ref;
	unsigned short  type;
	unsigned short  size;

} LOCAL_VAR_TBL_ELEM;


typedef struct {

	int				count;
	LOCAL_VAR_TBL_ELEM	*list;

} LOCAL_VAR_TBL;


/* Command number to item id conversion table */

typedef struct {

	ulong		number;
	ITEM_ID		item_id;
	
} CMD_NUM_ID_TBL_ELEM;


typedef struct {

	int					count;
	CMD_NUM_ID_TBL_ELEM	*list;

} CMD_NUM_ID_TBL;


typedef struct
{
    unsigned short  count;
    BINARY_LANGUAGE_IMAGE    *list;
} BINARY_LIST;

typedef struct
{
	int			count;
	BINARY_LIST *list;
} IMAGE_TBL;

/* Device Directory Binary */

typedef struct {
	unsigned long	bin_exists;
	unsigned long	bin_hooked;

	BININFO		blk_tbl;
	BININFO		item_tbl;
	BININFO		prog_tbl;
	BININFO		domain_tbl;
	BININFO		string_tbl;
	BININFO		dict_ref_tbl;
	BININFO		local_var_tbl;
	BININFO		cmd_num_id_tbl;
	BININFO		image_tbl;
} BIN_DEVICE_DIR;


/* Device Directory Flat */

typedef struct {
	unsigned long		attr_avail;

	BLK_TBL				blk_tbl;
	ITEM_TBL			item_tbl;
	PROG_TBL			prog_tbl;
	DOMAIN_TBL			domain_tbl;
	STRING_TBL			string_tbl;
	DICT_REF_TBL		dict_ref_tbl;
	LOCAL_VAR_TBL		local_var_tbl;
	CMD_NUM_ID_TBL		cmd_num_id_tbl;
	IMAGE_TBL			image_tbl;
} FLAT_DEVICE_DIR;


/*
 *	Convenience Macros for Handling Device Tables
 */


/* Block Table macros */

#define	BT_LIST(bt_ptr) (bt_ptr->list)
#define	BT_COUNT(bt_ptr) (bt_ptr->count)
#define	BTE(bt_ptr,elem) (&bt_ptr->list[elem])

/* Block Table element macros */

#define	BTE_BLK_ID(bte_ptr) (bte_ptr->blk_id)
#define	BTE_BD_DD_REF(bte_ptr) (bte_ptr->blk_dir_dd_ref)
#define	BTE_IT_OFFSET(bte_ptr) (bte_ptr->item_tbl_offset)
#define	BTE_CR_IT_OFFSET(bte_ptr) (bte_ptr->char_rec_item_tbl_offset)
#define	BTE_CR_BINT_OFFSET(bte_ptr) (bte_ptr->char_rec_bint_offset)
#define	BTE_USAGE(bte_ptr) (bte_ptr->usage)

#define	BTE_BIT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_tbl)
#define	BTE_BINT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_name_tbl)
#define	BTE_PT(bte_ptr) (&bte_ptr->flat_block_dir.param_tbl)
#define	BTE_PMT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_tbl)
#define	BTE_PET(bte_ptr) (&bte_ptr->flat_block_dir.param_elem_tbl)
#define	BTE_PMNT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_name_tbl)
#define	BTE_PLT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_tbl)
#define	BTE_PLMT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_mem_tbl)
#define	BTE_PLMNT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_mem_name_tbl)
#define	BTE_CMT(bte_ptr) (&bte_ptr->flat_block_dir.char_mem_tbl)
#define	BTE_CMNT(bte_ptr) (&bte_ptr->flat_block_dir.char_mem_name_tbl)
#define	BTE_RT(bte_ptr) (&bte_ptr->flat_block_dir.rel_tbl)
#define	BTE_UT(bte_ptr) (&bte_ptr->flat_block_dir.update_tbl)
#define	BTE_CT(bte_ptr) (&bte_ptr->flat_block_dir.command_tbl)
#define	BTE_CPT(bte_ptr) (&bte_ptr->flat_block_dir.crit_param_tbl)
#define	BTE_RPT(bte_ptr) (&bte_ptr->flat_block_dir.reslv_ref_tbl)


/* Item Table macros */

#define	IT_LIST(it_ptr) (it_ptr->list)
#define	IT_COUNT(it_ptr) (it_ptr->count)
#define	ITE(it_ptr,elem) (&it_ptr->list[elem])

/* Item Table element macros */

#define	ITE_ITEM_ID(ite_ptr) (ite_ptr->item_id)
#define	ITE_DD_REF(ite_ptr) (ite_ptr->dd_ref)
#define	ITE_ITEM_TYPE(ite_ptr) (ite_ptr->item_type)



/*
 *	Convenience Macros for Handling Block Tables
 */


/* Block Item Table macros */

#define	BIT_LIST(bit_ptr) (bit_ptr->list)
#define	BIT_COUNT(bit_ptr) (bit_ptr->count)
#define	BITE(bit_ptr,elem) (&bit_ptr->list[elem])

/* Block Item Table element macros */

#define	BITE_BLK_ITEM_ID(bite_ptr) (bite_ptr->blk_item_id)
#define	BITE_BINT_OFFSET(bite_ptr) (bite_ptr->blk_item_name_tbl_offset)


/* Block Item Name Table macros */

#define	BINT_LIST(bint_ptr) (bint_ptr->list)
#define	BINT_COUNT(bint_ptr) (bint_ptr->count)
#define	BINTE(bint_ptr,elem) (&bint_ptr->list[elem])

/* Block Item Name Table element macros */

#define	BINTE_BLK_ITEM_NAME(binte_ptr) (binte_ptr->blk_item_name)
#define	BINTE_IT_OFFSET(binte_ptr) (binte_ptr->item_tbl_offset)
#define	BINTE_PT_OFFSET(binte_ptr) (binte_ptr->param_tbl_offset)
#define	BINTE_PLT_OFFSET(binte_ptr) (binte_ptr->param_list_tbl_offset)
#define	BINTE_RT_OFFSET(binte_ptr) (binte_ptr->rel_tbl_offset)

#define	BINTE_RCT_OFFSET(binte_ptr) (binte_ptr->read_cmd_tbl_offset)
#define	BINTE_RCT_COUNT(binte_ptr) (binte_ptr->read_cmd_count)

#define	BINTE_WCT_OFFSET(binte_ptr) (binte_ptr->write_cmd_tbl_offset)
#define	BINTE_WCT_COUNT(binte_ptr) (binte_ptr->write_cmd_count)


/* Parameter Table macros */

#define	PT_LIST(pt_ptr) (pt_ptr->list)
#define	PT_COUNT(pt_ptr) (pt_ptr->count)
#define	PTE(pt_ptr,elem) (&pt_ptr->list[elem])

/* Parameter Table element macros */

#define	PTE_BINT_OFFSET(pte_ptr) (pte_ptr->blk_item_name_tbl_offset)
#define	PTE_PMT_OFFSET(pte_ptr) (pte_ptr->param_mem_tbl_offset)
#define	PTE_PM_COUNT(pte_ptr) (pte_ptr->param_mem_count)
#define	PTE_PET_OFFSET(pte_ptr) (pte_ptr->param_elem_tbl_offset)
#define	PTE_PE_COUNT(pte_ptr) (pte_ptr->param_elem_count)
#define	PTE_AE_IT_OFFSET(pte_ptr) (pte_ptr->array_elem_item_tbl_offset)
#define	PTE_PE_MX_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_VAR_TYPE(pte_ptr) (pte_ptr->array_elem_type_or_var_type)
#define	PTE_AE_VAR_SIZE(pte_ptr) (pte_ptr->array_elem_size_or_var_size)
#define	PTE_AE_VAR_CLASS(pte_ptr) (pte_ptr->array_elem_class_or_var_class)


/* Parameter Member Table macros */

#define	PMT_LIST(pmt_ptr) (pmt_ptr->list)
#define	PMT_COUNT(pmt_ptr) (pmt_ptr->count)
#define	PMTE(pmt_ptr,elem) (&pmt_ptr->list[elem])

/* Parameter Member Table element macros */

#define	PMTE_IT_OFFSET(pmte_ptr) (pmte_ptr->item_tbl_offset)
#define	PMTE_PM_TYPE(pmte_ptr) (pmte_ptr->param_mem_type)
#define	PMTE_PM_SIZE(pmte_ptr) (pmte_ptr->param_mem_size)
#define	PMTE_PM_CLASS(pmte_ptr) (pmte_ptr->param_mem_class)
#define PMTE_RT_OFFSET(pmte_ptr) (pmte_ptr->rel_tbl_offset)

/* Parameter Member Name Table macros */

#define	PMNT_LIST(pmnt_ptr) (pmnt_ptr->list)
#define	PMNT_COUNT(pmnt_ptr) (pmnt_ptr->count)
#define	PMNTE(pmnt_ptr,elem) (&pmnt_ptr->list[elem])

/* Parameter Member Name Table element macros */

#define	PMNTE_PM_NAME(pmte_ptr) (pmte_ptr->param_mem_name)
#define	PMNTE_PM_OFFSET(pmte_ptr) (pmte_ptr->param_mem_offset)

/* Parameter Element Table macros */

#define	PET_LIST(pet_ptr) (pet_ptr->list)
#define	PET_COUNT(pet_ptr) (pet_ptr->count)
#define	PETE(pet_ptr,elem) (&pet_ptr->list[elem])

/* Parameter Element Table element macros */

#define	PETE_PE_SUBINDEX(pete_ptr) (pete_ptr->param_elem_subindex)
#define	PETE_RT_OFFSET(pete_ptr) (pete_ptr->rel_tbl_offset)


/* Parameter List Table macros */

#define	PLT_LIST(plt_ptr) (plt_ptr->list)
#define	PLT_COUNT(plt_ptr) (plt_ptr->count)
#define	PLTE(plt_ptr,elem) (&plt_ptr->list[elem])

/* Parameter List Table element macros */

#define	PLTE_BINT_OFFSET(plte_ptr) (plte_ptr->blk_item_name_tbl_offset)
#define	PLTE_PLMT_OFFSET(plte_ptr) (plte_ptr->param_list_mem_tbl_offset)
#define	PLTE_PLM_COUNT(plte_ptr) (plte_ptr->param_list_mem_count)


/* Parameter List Member Table macros */

#define	PLMT_LIST(plmt_ptr) (plmt_ptr->list)
#define	PLMT_COUNT(plmt_ptr) (plmt_ptr->count)
#define	PLMTE(plmt_ptr,elem) (&plmt_ptr->list[elem])

/* Parameter List Member Table element macros */

#define	PLMTE_BINT_OFFSET(plmte_ptr) (plmte_ptr->blk_item_name_tbl_offset)


/* Parameter List Member Name Table macros */

#define	PLMNT_LIST(plmnt_ptr) (plmnt_ptr->list)
#define	PLMNT_COUNT(plmnt_ptr) (plmnt_ptr->count)
#define	PLMNTE(plmnt_ptr,elem) (&plmnt_ptr->list[elem])

/* Parameter List Member Table Name element macros */

#define	PLMNTE_PLM_NAME(plmnte_ptr) (plmnte_ptr->param_list_mem_name)
#define	PLMNTE_PLMT_OFFSET(plmnte_ptr) (plmnte_ptr->param_list_mem_tbl_offset)


/* Characteristic Member Table macros */

#define	CMT_LIST(cmt_ptr) (cmt_ptr->list)
#define	CMT_COUNT(cmt_ptr) (cmt_ptr->count)
#define	CMTE(cmt_ptr,elem) (&cmt_ptr->list[elem])

/* Characteristic Member Table element macros */

#define	CMTE_IT_OFFSET(cmte_ptr) (cmte_ptr->item_tbl_offset)
#define	CMTE_CM_TYPE(cmte_ptr) (cmte_ptr->char_mem_type)
#define	CMTE_CM_SIZE(cmte_ptr) (cmte_ptr->char_mem_size)
#define	CMTE_CM_CLASS(cmte_ptr) (cmte_ptr->char_mem_class)
#define	CMTE_RT_OFFSET(cmte_ptr) (cmte_ptr->rel_tbl_offset)


/* Characteristic Member Name Table macros */

#define	CMNT_LIST(cmnt_ptr) (cmnt_ptr->list)
#define	CMNT_COUNT(cmnt_ptr) (cmnt_ptr->count)
#define	CMNTE(cmnt_ptr,elem) (&cmnt_ptr->list[elem])

/* Characteristic Member Name Table element macros */

#define	CMNTE_CM_NAME(cmnte_ptr) (cmnte_ptr->char_mem_name)
#define	CMNTE_CM_OFFSET(cmnte_ptr) (cmnte_ptr->char_mem_offset)


/* Relation Table macros */

#define	RT_LIST(rt_ptr) (rt_ptr->list)
#define	RT_COUNT(rt_ptr) (rt_ptr->count)
#define	RTE(rt_ptr,elem) (&rt_ptr->list[elem])

/* Relation Table element macros */

#define	RTE_WAO_IT_OFFSET(rte_ptr) (rte_ptr->wao_item_tbl_offset)
#define	RTE_UNIT_IT_OFFSET(rte_ptr) (rte_ptr->unit_item_tbl_offset)
#define	RTE_UT_OFFSET(rte_ptr) (rte_ptr->update_tbl_offset)
#define	RTE_U_COUNT(rte_ptr) (rte_ptr->update_count)
#define	RTE_UNIT_COUNT(rte_ptr) (rte_ptr->unit_count)

/* Update Table macros */

#define	UT_LIST(ut_ptr) (ut_ptr->list)
#define	UT_COUNT(ut_ptr) (ut_ptr->count)
#define	UTE(ut_ptr,elem) (&ut_ptr->list[elem])

/* Update Table element macros */

#define	UTE_OP_IT_OFFSET(ute_ptr) (ute_ptr->op_it_offset)
#define	UTE_OP_SUBINDEX(ute_ptr) (ute_ptr->op_subindex)
#define	UTE_DESC_IT_OFFSET(ute_ptr) (ute_ptr->desc_it_offset)


/* Command Table macros */

#define	CT_LIST(ct_ptr) (ct_ptr->list)
#define	CT_COUNT(ct_ptr) (ct_ptr->count)
#define	CTE(ct_ptr, elem) (&ct_ptr->list[elem])

/* Command Table element macros */

#define	CTE_NUMBER(cte_ptr) (cte_ptr->number)
#define	CTE_SUBINDEX(cte_ptr) (cte_ptr->subindex)
#define	CTE_COUNT(cte_ptr) (cte_ptr->count)
#define	CTE_LIST(cte_ptr) (cte_ptr->list)


/* Critical Parameters Table macros */

#define	CPT_LIST(cpt_ptr) (cpt_ptr->list)
#define	CPT_COUNT(cpt_ptr) (cpt_ptr->count)


/* Default table offset (for optional elements) */

#define DEFAULT_OFFSET      (-1)

#endif	/* TABLE_H */

