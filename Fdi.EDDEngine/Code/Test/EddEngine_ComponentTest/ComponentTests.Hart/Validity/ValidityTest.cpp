#include "TestSupport/EddEngineController.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
   	    [TestClass, Ignore]
        public ref class BaseTestReadingPageMenu
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* pageMenu;

            static void SetupBase(String^ testCaseName)
            {
                pageMenu = nullptr;
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                //Get the page menu 1
                FDI_PARAM_SPECIFIER test_page;
                engine->FillParamSpecifier("page_menu_1", &test_page);
                genericItem = engine->GetGenericItem(test_page.id, nsEDDEngine::ITYPE_MENU);
                pageMenu = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }
            static void CleanupBase()
            {
                delete engine;
                delete genericItem;

            }
            [TestMethod, TestCategory("ValidityTest")]
            void It_should_contain_six_sub_items()
            {
                ushort expected = 6;
                Assert::AreEqual(expected, pageMenu->items.count);
            }

            [TestMethod, TestCategory("ValidityTest")]
            void It_should_be_the_correct_Item_Id()
            {
                Assert::AreEqual(ITEM_ID(16386), genericItem->item->id);
            }
        };

        // WORKAROUND: use TestInitalize instead ClassInitalize
        // Impediment
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        [TestClass]
        public ref class When_reading_the_elements_of_the_page_menu_1_with_FM8 : BaseTestReadingPageMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_reading_the_elements_of_the_page_menu_1_with_FMA : BaseTestReadingPageMenu
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
