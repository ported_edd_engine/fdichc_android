// CompoundStatement.cpp //

#include "stdafx.h"
#include "CompoundStatement.h"
#include "StatementList.h"
#include "GrammarNodeVisitor.h"

#include "ErrorDefinitions.h"

CCompoundStatement::CCompoundStatement()
{
	m_pStmtList = 0;
}

CCompoundStatement::~CCompoundStatement()
{
	DELETE_PTR(m_pStmtList);
}

_INT32 CCompoundStatement::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitCompoundStatement(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

// Selection Statement is of the form
//	<{> <StatementList> <}>
_INT32 CCompoundStatement::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CToken* pToken = 0;

	try
	{
//Munch a <{>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable))
			|| !pToken
			||(pToken->GetSubType() != RUL_LBRACK)
			)
		{
			if (pToken->GetSubType() != RUL_COLON)
			{
				DELETE_PTR(pToken);
				throw(C_UM_ERROR_MISSINGTOKEN);
			}
		}
		DELETE_PTR(pToken);

//Munch List of statments...
		try
		{
			//mark a compound statement starting
			plexAnal->m_nIndentIndex++;

			m_pStmtList= new CStatementList;
			m_pStmtList->CreateParseSubTree(
				plexAnal,
				pSymbolTable,
				pvecErrors);
		}
		catch(_INT32 error)
		{
			plexAnal->MoveTo(
				RUL_SYMBOL,
				RUL_RBRACK,
				pSymbolTable);
			ADD_ERROR(error);
		}

//Munch a <}>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable))
			|| !pToken
			|| (pToken->GetSubType() != RUL_RBRACK))
		{
			plexAnal->UnGetToken();
			throw(C_CS_ERROR_MISSINGRBRACK);
		}
		DELETE_PTR(pToken);

		//mark a compound statement ending
		if (plexAnal->m_nIndentIndex > 1)
		{
			//if this scope index is in the scope stack, remove it at closing curly bracket.
			if ( (plexAnal->m_sDeclarationStack.size() > 0) && (plexAnal->m_nIndentIndex == plexAnal->m_sDeclarationStack.top()) )
			{
				plexAnal->m_sDeclarationStack.pop();

				//symbol table stack also pops one index at the same time
				if (pSymbolTable->m_sScope.size() > 0)
				{
					pSymbolTable->m_sScope.pop();
				}
				else
				{
					throw(C_CS_ERROR_MISSINGRBRACK);
				}
			}

			plexAnal->m_nIndentIndex--;
		}
		else
		{
			throw(C_CS_ERROR_MISSINGRBRACK);
		}
		
		return PARSE_SUCCESS;
	}
	catch(_INT32 error)
	{
		plexAnal->MovePast(
			RUL_SYMBOL,
			RUL_SEMICOLON,
			pSymbolTable);
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}
	return PARSE_FAIL;
}

void CCompoundStatement::Identify(
		_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,"CompoundStatement");
	strcat(szData,">");

	if(m_pStmtList)
		m_pStmtList->Identify(szData);

	strcat(szData,"</");
	strcat(szData,"CompoundStatement");
	strcat(szData,">");

}

CStatementList* CCompoundStatement::GetStatementList()
{
	return m_pStmtList;
}

_INT32 CCompoundStatement::GetLineNumber()
{
	if (m_pStmtList)
	{
		return m_pStmtList->GetLineNumber();
	}
	else
	{
		return 0;
	}
}
