#include "stdafx.h"
#include "FileFind.h"
#include <afx.h>

WindowsFileFind::WindowsFileFind()
{
	m_FileFinder = new CFileFind();
}

WindowsFileFind::~WindowsFileFind()
{
	if (m_FileFinder != NULL)
	{
		delete ((CFileFind*)m_FileFinder);
		m_FileFinder = NULL;
	}
}

BOOL WindowsFileFind::FindFile(const wchar_t* fileName)
{
	return ((CFileFind*)m_FileFinder)->FindFile(fileName);
}

BOOL WindowsFileFind::FindFileNext()
{
	return ((CFileFind*)m_FileFinder)->FindNextFileW();
}

BOOL WindowsFileFind::GetLastWriteTime(time_t& fileModifiedTime)
{
	BOOL bRetVal = FALSE;
	CTime reftime;

	bRetVal = ((CFileFind*)m_FileFinder)->GetLastWriteTime(reftime) != 0 ? true: false;

	fileModifiedTime = reftime.GetTime();

	return bRetVal;
}

unsigned long long WindowsFileFind::GetLength()
{
	return ((CFileFind*)m_FileFinder)->GetLength();
}

void WindowsFileFind::CloseFile()
{
	((CFileFind*)m_FileFinder)->Close();
}
