<Method>
{ 

  int iV1, iV2, iR1, iR2, iR3, iR4, iR5, iR6, iR7, iR8, iR99;

  char cmd_status[3], more_status_info[3], more_data_info[25];



  iV1 = 1;

  iV2 = 0;

  iR1 = _set_comm_status(0x7F,1);	//ABORT_ON_ALL_COMM_STATUS

  iR2 =	_set_device_status(0xFF,1);	//ABORT_ON_ALL_DEVICE_STATUS

  iR3 =	_set_all_resp_code(1);		//ABORT_ON_ALL_RESPONSE_CODES

  iR4 = _set_comm_status(0xFF,1); 	//ABORT_ON_COMM_ERROR

  iR5 = _set_comm_status(0x01,1);	//ABORT_ON_COMM_STATUS

  iR6 = _set_device_status(0x10,1); 	//ABORT_ON_DEVICE_STATUS

  iR7 = _set_no_device(1); 		//ABORT_ON_NO_DEVICE

  iR8 = _set_resp_code(0x01,1); 	//ABORT_ON_RESPONSE_CODE

  iR99 = ext_send_command_trans(iV1, iV2, cmd_status, more_status_info, more_data_info);



}
</Method>
<Function>
Function Name: _set_comm_status
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 127
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_comm_status
Function Name: _set_device_status
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 255
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_device_status
Function Name: _set_all_resp_code
  Number Of Input Parameters: 1
    Param 1 Type: (null)
    Param 1 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_all_resp_code
Function Name: _set_comm_status
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 255
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_comm_status
Function Name: _set_comm_status
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 1
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_comm_status
Function Name: _set_device_status
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 16
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_device_status
Function Name: _set_no_device
  Number Of Input Parameters: 1
    Param 1 Type: (null)
    Param 1 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_no_device
Function Name: _set_resp_code
  Number Of Input Parameters: 2
    Param 1 Type: (null)
    Param 1 Value: 1
    Param 2 Type: (null)
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _set_resp_code
Function Name: ext_send_command_trans
  Number Of Input Parameters: 5
    Param 1 Type: RUL_INT
    Param 1 Value: 1
    Param 2 Type: RUL_INT
    Param 2 Value: 0
    Param 3 Type: RUL_SAFEARRAY
    Param 3 Array: 
    Param 4 Type: RUL_SAFEARRAY
    Param 4 Array: 
    Param 5 Type: RUL_SAFEARRAY
    Param 5 Array: 
  Returned Param Type: RUL_INT
  Returned Param Value: -2
End of Function ext_send_command_trans
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><iV1>1,</iV1><iV2>0,</iV2><iR1>0,</iR1><iR2>0,</iR2><iR3>0,</iR3><iR4>0,</iR4><iR5>0,</iR5><iR6>0,</iR6><iR7>0,</iR7><iR8>0,</iR8><iR99>0,</iR99><cmd_status>[0][1][2],[3]</cmd_status><more_status_info>[0][1][2][3]</more_status_info><more_data_info>[0],[1][2][3],[4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24]</more_data_info></SymbolTable>
<Abort Method>
Method Process Aborted
</Abort Method>
