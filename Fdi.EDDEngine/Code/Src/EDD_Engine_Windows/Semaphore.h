#pragma once

#include <string>

class WindowsSemaphore
{
private:

	void* m_pSemaphore;

public:
	// Constructors
	WindowsSemaphore();

	virtual ~WindowsSemaphore();
	BOOL InitSemaphore(long nInitialCount, long nMaxCount, std::wstring semaphoreName);
	BOOL WaitSemaphore();
	BOOL ReleaseSemaphore(long lCurrentCount, long* plPrevCount);
	BOOL CloseSemaphore();
};

