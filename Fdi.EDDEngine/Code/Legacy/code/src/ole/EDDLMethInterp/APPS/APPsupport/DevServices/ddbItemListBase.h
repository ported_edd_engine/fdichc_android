/*************************************************************************************************
 *
 * $Workfile: ddbItemListBase.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		generated from the top half of previous version of ddbItemLists.h
 *
 * #include "ddbItemListBase.h"
 */

#ifndef _DDBITEMLISTBASE_H
#define _DDBITEMLISTBASE_H


#ifdef INC_DEBUG
#pragma message("In ddbItemListBase.h") 
#endif


class CddbDevice;

#ifdef INC_DEBUG
#pragma message("    Finished Includes::ddbItemListBase.h") 
#endif


/* * * * * * * * * * * list base class * * * * * * * * * * * * */
template<itemType_t MY_ITYPE, class LISTOFCLASS>  
class CitemListBase : public vector< LISTOFCLASS* >, public hCobject
{
public:
	CitemListBase(DevInfcHandle_t h) : hCobject(h) {}
	//virtual ~CitemListBase(){}
	//virtual RETURNCODE populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl,UINT32 theDDkey, aCdevice* pAbstractDevice);
	//virtual void dumpSelf(int indent=0);
	//virtual void destroy(void); // clear contents before deletion
};

//template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
//void CitemListBase <MY_ITYPE,LISTOFCLASS>
//:: destroy(void)
//{
//}

/* * * * * * * base class  method * * * * * * */
//template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
//void CitemListBase <MY_ITYPE,LISTOFCLASS>
//:: dumpSelf(int indent)
//{	
//}

/* * * * * * * base class  method * * * * * * */
/* instantiate all of this item type
 * if we have an abstract, populate from that, else get the data 
 */
//template<itemType_t MY_ITYPE, class LISTOFCLASS>  /*virtual*/
//RETURNCODE CitemListBase <MY_ITYPE,LISTOFCLASS>
//:: populate(hCdeviceSrvc* pDev, DevInfcHandle_t hndl, UINT32 theDDkey, aCdevice* pAbstractDevice)
//{	
//	return SUCCESS;
//};

#endif

/*************************************************************************************************
 *
 *   $History: ddbItemListBase.h $
 * 
 *************************************************************************************************
 */
