#include "nsEDDEngine\IEDDEngine.h"
#include "nsEDDEngine\IMethods.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            typedef void (*UnmanagedAsyncCallback)(nsEDDEngine::IAsyncResult* ar);

            public class AsyncCallbackHandler : public nsEDDEngine::IAsyncCallbackHandler
            {
            private:
                UnmanagedAsyncCallback _asyncCallback;

            public:
                AsyncCallbackHandler();

                AsyncCallbackHandler(ComponentTests::Core::TestSupport::UnmanagedAsyncCallback asyncCallback);

                virtual void AsyncCallback(nsEDDEngine::IAsyncResult* ar);
            };

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests
