#pragma once

#include <errno.h>
#include<string.h>
#include<stdio.h>
#include <stdarg.h>  

inline errno_t PS_Wcscpy(wchar_t *strDestination, size_t numberOfElements, const wchar_t *strSource)
{
	return wcscpy_s(strDestination, numberOfElements, strSource);
}

inline errno_t PS_Strcpy(char * strDestination, size_t numberOfElements, const char * strSource)
{
	return strcpy_s(strDestination, numberOfElements, strSource);
}

inline errno_t PS_Strncpy(char * strDest, size_t numberOfElements, const char * strSource, size_t count)
{
	return strncpy_s(strDest, numberOfElements, strSource, count);
}

inline errno_t PS_Strcat(char * strDestination, size_t numberOfElements, const char * strSource)
{
	return strcat_s(strDestination, numberOfElements, strSource);
}

inline errno_t PS_Strncat(char * strDest, size_t numberOfElements, const char * strSource, size_t count)
{
	return strncat_s(strDest, numberOfElements, strSource, count);
}

inline errno_t PS_Wcscat(wchar_t * strDestination, size_t numberOfElements, const wchar_t * strSource)
{

	return wcscat_s(strDestination, numberOfElements, strSource);
}

inline errno_t PS_Wcsncat(wchar_t * _Destination, size_t _SizeInWords, wchar_t const * _Source, size_t _MaxCount)
{
	return wcsncat_s(_Destination, _SizeInWords, _Source, _MaxCount);
}

inline unsigned __int64 PS_Strtoui64(const char * nptr, char ** endptr, int base)
{
	return _strtoui64(nptr, endptr, base);
}

inline errno_t PS_Wcsncpy(wchar_t * strDest, size_t numberOfElements, const wchar_t * strSource, size_t count)
{
	return wcsncpy_s(strDest, numberOfElements, strSource, count);
}

inline wchar_t * PS_Itow(int value, wchar_t * str, int radix)
{
	return _itow(value, str, radix);
}

inline errno_t PS_Itow_s(int value, wchar_t * buffer, size_t sizeInCharacters, int radix)
{
	return _itow_s(value, buffer, sizeInCharacters, radix);
}

inline int PS_Wtoi(const wchar_t* data)
{
	return _wtoi(data);
}

inline long PS_Wtol(const wchar_t* data)
{
	return _wtol(data);
}

inline float PS_Wtof(const wchar_t* data)
{
	return (float)_wtof(data);
}

inline int PS_Vsnprintf_s(char * buffer, size_t sizeOfBuffer, size_t count, const char * format, char * argptr)
{
	return  vsnprintf_s(buffer, sizeOfBuffer, count, format, argptr);
}

inline int PS_Vsnprintf(char * buffer, size_t count, const char * format, char * argptr)
{
	return _vsnprintf(buffer, count, format, argptr);
}


inline char * PS_Strdup(const char * strSource)
{
	return _strdup(strSource);
}

inline errno_t PS_Wcstombs(size_t * pReturnValue, char * mbstr, size_t sizeInBytes, const wchar_t * wcstr, size_t count)
{
	return wcstombs_s(pReturnValue, mbstr, sizeInBytes, wcstr, count);
}

inline errno_t PS_Mbtowcs(size_t * pReturnValue, wchar_t * wcstr, size_t sizeInWords, const char * mbstr, size_t count)
{
	return mbstowcs_s(pReturnValue, wcstr, sizeInWords, mbstr, count);
}

inline errno_t PS_Memcpy(void * dest, size_t destSize, const void * src, size_t count)
{
	return memcpy_s(dest, destSize, src, count);
}

inline int PS_VsnPrintf(char* const buffer, size_t const bufferCount, char const* const format, va_list vl)
{
	return sprintf_s(buffer, bufferCount, format, vl);
}

template<size_t size>
inline errno_t PS_Tcscpy(wchar_t(&strDestination)[size], const wchar_t * strSource)
{
	return PS_Wcscpy(strDestination, size, strSource);
}

template <size_t size>
inline errno_t PS_Tstrcat(char(&strDestination)[size], const char *strSource)
{
	return PS_Strcat(strDestination, strSource);
}

template <size_t size>
inline errno_t  PS_Itow(int value, wchar_t(&buffer)[size], int radix)
{
	return _itow_s(value, buffer, size, radix);
}
inline char*  PS_Itoa(int value, char* buffer, int radix)
{
	return _itoa(value, buffer, radix);
}

inline errno_t  PS_Itoa_s(int value, char* buffer, size_t sizeInCharacters, int radix)
{
	return _itoa_s(value, buffer, sizeInCharacters, radix);
}


template <size_t size>
inline errno_t PS_Wstrdate(wchar_t(&buffer)[size])
{
	return _wstrdate_s(buffer);
}

template <size_t size>
inline errno_t PS_Wstrtime(wchar_t(&buffer)[size])
{
	return _wstrtime_s(buffer);
}

inline errno_t PS_Wcsupr(wchar_t * str, size_t numberOfElements)
{
	return _wcsupr_s(str, numberOfElements);
}

inline errno_t PS_Wcslwr(wchar_t * str, size_t numberOfElements)
{
	return _wcslwr_s(str, numberOfElements);
}

inline int	PS_VsnPrintf(char* const buffer, size_t const bufferCount, char const* format, ...)
{
	int retCode = 0;
	va_list  ap1 = { 0 };			// Get the rest of the arguments
	va_start(ap1, format);

	retCode = _vsnprintf(buffer, bufferCount, format, ap1);

	va_end(ap1);
	return retCode;
}

inline int	PS_VsnwPrintf(wchar_t* const buffer, size_t const bufferCount, wchar_t const* format, ...)
{
	int retCode = 0;
	va_list  ap1 = { 0 };			// Get the rest of the arguments
	va_start(ap1, format);

	retCode = _vsnwprintf(buffer, bufferCount, format, ap1);

	va_end(ap1);
	return retCode;
}

inline wchar_t *PS_WcsTok(wchar_t *str, const wchar_t *delim, wchar_t **ptr)
{
	return wcstok_s(str, delim, ptr);
}

inline void PS_Strupr(char* args)
{
	_strupr(args);
}
#pragma warning(disable : 4996)
inline int PS_WsPrintf(wchar_t * buffer,
	 const wchar_t * format, ...)
 {
	 int retCode = 0;
	 va_list  ap1 = {0};			// Get the rest of the arguments
	 va_start(ap1, format);
	 retCode = swprintf(buffer, format, ap1);
	 
	 va_end(ap1);
	 return retCode;
 }
