#include "stdafx.h"
#include "BuiltIn.h"
#include "ddbdefs.h"
#include "NAN_DEF.h"
#include <math.h>
#include <sstream>


bool isSystemLittleEndian();

/* Arun 120505 Start of code*/
/*Math support builtins (EDDL)*/

double CBuiltIn:: abs (double x)
{
	// // stevev changed 18oct07 from::> return ::abs(x);
	return ::fabs(x);
}

double CBuiltIn:: acos (double x)
{
	return ::acos(x);
}

double CBuiltIn:: asin (double x)
{
	return ::asin(x);
}

double CBuiltIn:: atan (double x)
{
	return ::atan(x);
}

double CBuiltIn:: cbrt (double x)
{
	return (x >= 0)?::pow(x, 1.0/3.0):-::pow(-x, 1.0/3.0);
}

double CBuiltIn:: ceil (double x)
{
	return ::ceil(x);
}

double CBuiltIn:: cos (double x)
{
	return ::cos(x);
}

double CBuiltIn:: cosh (double x)
{
	return ::cosh(x);
}

double CBuiltIn:: exp (double x)
{
	return ::exp(x);
}

double CBuiltIn:: floor (double x)
{
	return ::floor(x);
}

double CBuiltIn:: fmod (double x, double y)
{
	return ::fmod(x,y);
}

int CBuiltIn:: fpclassify (double x)
{
	return ::_fpclass(x);
}

#ifdef XMTR
double CBuiltIn:: frand ()
{ 
	return ( ::rand()/((double)RAND_MAX) );
}
#endif
double CBuiltIn:: log (double x)
{
	return ::log(x);
}

double CBuiltIn:: log10 (double x)
{
	return ::log10(x);
}

double CBuiltIn:: log2 (double x)
{
// HOMZ - port to 2003, VS7 >> error C2668: 'log' : ambiguous call to overloaded function
	return (::log(x) / ::log((double)2));
//was 	return (::log(x) / ::log(2));
}

double CBuiltIn:: nan(	const char* string )
{
	unsigned long long ullVal, x;
	char *stopstring;
	size_t len;

	if ((string == nullptr) || (strlen(string) == 0))
	{
// There is a problem where 32 bit automatically converts a signalling NAN to a quiet NAN, so we are in Win64.
// We might need to revisit this in the future.
#ifdef _WIN64
		ullVal = (DOUBLE_POS_QUIET_NAN_VALUE);	//default NaN
#else
		ullVal = (DOUBLE_POS_SIGNAL_NAN_VALUE);	//default NaN
#endif
	}
	else if ((strcmp(string, "Q") == 0) || (strcmp(string, "q") == 0))
	{
		ullVal = (DOUBLE_POS_QUIET_NAN_VALUE);	//default quiet NaN
	}
	else if ((strcmp(string, "S") == 0) || (strcmp(string, "s") == 0))
	{
#ifdef _WIN64
		ullVal = (DOUBLE_POS_QUIET_NAN_VALUE);	//default NaN
#else
		ullVal = (DOUBLE_POS_SIGNAL_NAN_VALUE);	//default NaN
#endif
	}
	else if (((string[0] == 'Q') || (string[0] == 'q')) && (string[1] == '_'))
	{
		// get 52 fraction bits from input arqument
        x = _strtoui64(&(string[2]), &stopstring, 16);
		len = strlen(&(string[2]));
		x = x << ((13 - len)*4);
		// combine sign bit and exponent bits from qNaN
		ullVal = (DOUBLE_POS_QUIET_NAN_VALUE) | x;
	}
	else if (((string[0] == 'S') || (string[0] == 's')) && (string[1] == '_'))
	{
		// get 52 fraction bits from input arqument
		x = _strtoui64(&(string[2]), &stopstring, 16);
		len = strlen(&(string[2]));
		x = x << ((13 - len)*4);
		// combine sign bit and exponent bits from qNaN
#ifdef _WIN64
		ullVal = (DOUBLE_POS_QUIET_NAN_VALUE);	//default NaN
#else
		ullVal = (DOUBLE_POS_SIGNAL_NAN_VALUE);	//default NaN
#endif
		ullVal |= x;
	}
	else
	{
		return (double)(0);
	}
	
	double dRet = (*reinterpret_cast<double*>(&ullVal));
	return dRet;
}	

float CBuiltIn:: nanf(	const char* string )
{
	unsigned long ulVal, x;
	char *stopstring;
	size_t len;

	if ((string == nullptr) || (strlen(string) == 0))
	{
		ulVal = (SINGLE_INF_DIVBY_INF_NAN);	//default NaN
	}
	else if ((strcmp(string, "Q") == 0) || (strcmp(string, "q") == 0))
	{
		ulVal = (SINGLE_QUIET_NAN_PREFIX);	//default quiet NaN
	}
	else if ((strcmp(string, "S") == 0) || (strcmp(string, "s") == 0))
	{
		ulVal = (SINGLE_INF_DIVBY_INF_NAN);	//default signaling NaN
	}
	else if (((string[0] == 'Q') || (string[0] == 'q')) && (string[1] == '_'))
	{
		// get 23 fraction bits from input arqument
		x = strtoul(&(string[2]), &stopstring, 16);
		len = strlen(&(string[2]));
		x = x << ((6 - len)*4);
		if (x <= 0x7FFFFF)
		{
			// combine sign bit and exponent bits from qNaN
			ulVal = (SINGLE_QUIET_NAN_PREFIX) | x;
		}
		else
		{
		return (float)(0);
		}
	}
	else if (((string[0] == 'S') || (string[0] == 's')) && (string[1] == '_'))
	{
		// get 23 fraction bits from input arqument
		x = strtoul(&(string[2]), &stopstring, 16);
		len = strlen(&(string[2]));
		x = x << ((6 - len)*4);
		// combine sign bit and exponent bits from sNan
		ulVal = (SINGLE_INF_DIVBY_INF_NAN) | x;
	}
	else
	{
		return (float)(0);
	}

	return (*reinterpret_cast<float*>(&ulVal));
}

double CBuiltIn:: pow (double x, double y)
{
	return ::pow(x,y);
}

double CBuiltIn:: round (double x)
{
	if(x > 0)
	{
		return int(x+0.5F);
	}
	else
	{
		return int(x-0.5F);
	}
}

double CBuiltIn:: sin (double x)
{
	return ::sin(x);
}

double CBuiltIn:: sinh (double x)
{
	return ::sinh(x);
}

double CBuiltIn:: sqrt (double x)
{
	return ::sqrt(x);
}

double CBuiltIn:: tan (double x)
{
	return ::tan(x);
}

double CBuiltIn:: tanh (double x)
{
	return ::tanh(x);
}

double CBuiltIn:: trunc (double x)
{
	return int(x);
}

double CBuiltIn:: drand ()
{
	int number = rand();
	return ((double)number / (RAND_MAX + 1));
}

void CBuiltIn:: dseed (double x)
{
	unsigned int seedVal = (unsigned int)(x * (RAND_MAX + 1));
	srand(seedVal);
}

double CBuiltIn:: atof (char* string)
{
	char* p = string;
	if ( *p++ == '|')
	{
		while(p < (string + strlen(string)) )
		{
			if ( *p++ == '|' )
			{
				string = p;
				break;
			}
		}
	}
	return ::atof(string);
}

int CBuiltIn:: atoi (char* string)
{
	char* p = string;
	if ( *p++ == '|')
	{
		while(p < (string + strlen(string)) )
		{
			if ( *p++ == '|' )
			{
				string = p;
				break;
			}
		}
	}
	return ::atoi(string);
}

wchar_t* CBuiltIn:: itoa (int value, wchar_t* string,int radix)
{
	return PS_Itow(value, string, radix);
}


wchar_t* CBuiltIn:: ftoa (float fValue)
{
	std::wostringstream buffer;
	buffer << fValue;
	std::wstring str = buffer.str();
	wchar_t* pszReturn = new wchar_t[str.size() + 1];
	memset(pszReturn, 0 , 2 *(str.size() + 1));
	for(string::size_type i=0; i<str.size(); ++i)
	{
		pszReturn[i]= str[i];
	}
	return pszReturn;
}

//Convert 8 octets to IEEE double, while the first argument is the most significant byte
// and the last argument is the least significant byte
double CBuiltIn:: ByteToDouble (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4, 
	unsigned char byte5, unsigned char byte6, unsigned char byte7, unsigned char byte8)
{
	unsigned char input[8];
	if (isSystemLittleEndian())
	{
		input[0] = byte8;
		input[1] = byte7;
		input[2] = byte6;
		input[3] = byte5;
		input[4] = byte4;
		input[5] = byte3;
		input[6] = byte2;
		input[7] = byte1;
	}
	else
	{
		input[0] = byte1;
		input[1] = byte2;
		input[2] = byte3;
		input[3] = byte4;
		input[4] = byte5;
		input[5] = byte6;
		input[6] = byte7;
		input[7] = byte8;
	}

	double pszReturn = *reinterpret_cast<double*>(input);
	return pszReturn;
}

//Convert IEEE double to 8 octets, while the second argument is the most significant byte
// and the last argument is the least significant byte
int CBuiltIn:: DoubleToByte(double input,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4, 
	unsigned char *byte5, unsigned char *byte6, unsigned char *byte7, unsigned char *byte8)
{
	unsigned long long *llOutput = reinterpret_cast<unsigned long long*>(&input);
	const unsigned char allOnes = 0xFF;

	*byte1 = (unsigned char)((*llOutput >> 56) & allOnes);
	*byte2 = (unsigned char)((*llOutput >> 48) & allOnes);
	*byte3 = (unsigned char)((*llOutput >> 40) & allOnes);
	*byte4 = (unsigned char)((*llOutput >> 32) & allOnes);
	*byte5 = (unsigned char)((*llOutput >> 24) & allOnes);
	*byte6 = (unsigned char)((*llOutput >> 16) & allOnes);
	*byte7 = (unsigned char)((*llOutput >> 8) & allOnes);
	*byte8 = (unsigned char)(*llOutput & allOnes);

	return BI_SUCCESS;
}

//Convert 4 octets to IEEE float, while the first argument is the most significant byte
// and the last argument is the least significant byte
float CBuiltIn:: ByteToFloat (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4) 
{
	unsigned char input[4];
	if (isSystemLittleEndian())
	{
		input[0] = byte4;
		input[1] = byte3;
		input[2] = byte2;
		input[3] = byte1;
	}
	else
	{
		input[0] = byte1;
		input[1] = byte2;
		input[2] = byte3;
		input[3] = byte4;
	}

	float pszReturn = *reinterpret_cast<float*>(input);
	return pszReturn;
}

//Convert IEEE double to 8 octets, while the second argument is the most significant byte
// and the last argument is the least significant byte
int CBuiltIn:: FloatToByte(float input,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4) 
{
	unsigned long  *lOutput = reinterpret_cast<unsigned long *>(&input);
	const unsigned char allOnes = 0xFF;

	*byte1 = (unsigned char)((*lOutput >> 24) & allOnes);
	*byte2 = (unsigned char)((*lOutput >> 16) & allOnes);
	*byte3 = (unsigned char)((*lOutput >> 8) & allOnes);
	*byte4 = (unsigned char)(*lOutput & allOnes);

	return BI_SUCCESS;
}

// The Builtin ByteToLong will return a long value from a vector of four octets,
// while the first argument is the most significant byte
// and the last argument is the least significant byte
long CBuiltIn:: ByteToLong (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4) 
{
	long pszReturn = (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4;
	return pszReturn;
}

int CBuiltIn:: LongToByte (int l,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4) 
{
	const unsigned char allOnes = 0xFF;

	*byte1 = (unsigned char)(((long)l >> 24) & allOnes);
	*byte2 = (unsigned char)(((long)l >> 16) & allOnes);
	*byte3 = (unsigned char)(((long)l >> 8) & allOnes);
	*byte4 = (unsigned char)((long)l & allOnes);

	return BI_SUCCESS;
}

// The Builtin ByteToShort will return a short value from a vector of two octets,
// while the first argument is the most significant byte
// and the last argument is the least significant byte
short CBuiltIn:: ByteToShort (unsigned char byte1, unsigned char byte2) 
{
	short pszReturn = (byte1 << 8) | byte2;
	return pszReturn;
}

int CBuiltIn:: ShortToByte (short s, unsigned char *byte1, unsigned char *byte2) 
{
	const unsigned char allOnes = 0xFF;

	*byte1 = (unsigned char)((s >> 8) & allOnes);
	*byte2 = (unsigned char)(s & allOnes);

	return BI_SUCCESS;
}

bool isSystemLittleEndian()
{
	int i = 1;
	char *p = (char *) &i;
	if (p[0] == 1)	// Lowest address contains the least significant byte
	{
		return true;
	}
	else
	{
		return false;
	}
}
/* End of code*/

long CBuiltIn:: is_NaN(	const double dVal)
{
    if (isnan(dVal))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


//from an discussion at https://gcc.gnu.org/bugzilla/show_bug.cgi?id=57484
//"On an x86 target using the legacy x87 instructions and the 80-bit registers, 
// a load of a 64-bit or 32-bit value in memory into the 80-bit registers counts as a format conversion
// and an signaling NaN input will turn into a quiet NaN in the register format."
//"The ABI is just wrong for the underlying x87 hardware as far as NaNs are concerned."
//conclusion "The x87 never generates signaling NaN." from http://stackoverflow.com/questions/2247447/usefulness-of-signaling-nan
//
//This function returns double signaling NAN value
long CBuiltIn::NaN_value(double *value)
{
	//Default NaN shall be signaling NaN. see EDDL spec 61804-5_Ed1.docx buitin nan()
	//However, MI is not able to generate signaling NaN in Win32
	//so that MI client shall code this Nan into signal NaN bit pattern before sending it to device.
	*value = std::numeric_limits<double>::quiet_NaN();
    return BI_SUCCESS;
}
