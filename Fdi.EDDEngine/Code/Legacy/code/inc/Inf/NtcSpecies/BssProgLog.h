#ifndef NTCPROGLOG_H
#define NTCPROGLOG_H

//#include <windows.h>
//#include "PlatformCommon.h"

void BssProgLog (
   const char *cFilename,				// source code filename
   UINT32 lineNumber,					// source code line-number
   WORD severity,						// see documentation
   long status,							// a BssStatus value
   const wchar_t* wMsgFormat = NULL,			// a 'printf' format
   ...									//    and it's parameters
);

enum DebugBreakState {
	CheckRegistry,
	EnableDebugBreak,
	CheckIfDebug
	};
void DoDebugBreak(DebugBreakState = CheckIfDebug, LPCTSTR mylpString = NULL);

#ifndef EVENTLOG_ERROR_TYPE
#define EVENTLOG_ERROR_TYPE 0x0001
#endif

#ifndef EVENTLOG_WARNING_TYPE
#define EVENTLOG_WARNING_TYPE 0x0002
#endif

#ifndef EVENTLOG_INFORMATION_TYPE
#define EVENTLOG_INFORMATION_TYPE 0x0004
#endif

#ifndef EVENTLOG_AUDIT_SUCCESS
#define EVENTLOG_AUDIT_SUCCESS 0x0008
#endif

#ifndef EVENTLOG_AUDIT_FAILURE
#define EVENTLOG_AUDIT_FAILURE 0x0010
#endif

#ifndef BSS_SEVERITY_CONSTANTS
#define BSS_SEVERITY_CONSTANTS
// BssProgLog severity constants
const WORD BssError        = EVENTLOG_ERROR_TYPE ;
const WORD BssWarning      = EVENTLOG_WARNING_TYPE ;
const WORD BssInformation  = EVENTLOG_INFORMATION_TYPE ;
const WORD BssAuditSuccess = EVENTLOG_AUDIT_SUCCESS ;
const WORD BssAuditFailure = EVENTLOG_AUDIT_FAILURE ;

typedef unsigned long BssStatus;
const BssStatus BSS_COMMON_ERROR_CATEGORY        = static_cast<BssStatus>(0x20000000);
const BssStatus BSS_SUCCESS                       =                              0x00;

const BssStatus BSS_FAILURE                       =  BSS_COMMON_ERROR_CATEGORY + 0x01;
const BssStatus BSS_INVALID_PARAMETER             =  BSS_COMMON_ERROR_CATEGORY + 0x0C;

#endif

#define ASSERT_ALL(cond) \
		if(!(cond)) BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Assertion failed: \"%s\"", _T(#cond))


#endif
