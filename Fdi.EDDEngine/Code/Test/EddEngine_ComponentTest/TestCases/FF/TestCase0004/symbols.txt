member          TEST_INPUT_BIT_ENUM       parameter       0x40020003 
member          TEST_VAR_1_CHARACTERISTICS record          0x40020008 
record          test_block_characteristics                 0x20001 
variable        cond0027b_enum_var        enumerated      0x20006 
variable        __ffddinfo                enumerated      0x2000C 
member          TEST_VAR_1                parameter       0x40020005 
variable        test_unit                 enumerated      0x20002 
variable        test_input_bit_enum       bit-enumerated  0x20005 
member          BLOCK_TEST_UNIT           parameter       0x40020000 
member          BLOCK_TEST_CONDITIONED_1  parameter       0x40020001 
member          BLOCK_TEST_CONDITIONED_2  parameter       0x40020002 
variable        test_var_contitioned_1    integer         0x20003 
variable        test_var_contitioned_2    integer         0x20004 
member          COND0027B_ENUM_VAR        parameter       0x40020004 
unit            test_unit_relation                        0x2000A 
member          BLOCK_TEST_VAR_2          local           0x40020006 
member          BLOCK_TEST_VAR_3          local           0x40020007 
variable        test_var_1                integer         0x20007 
variable        test_var_2                integer         0x20008 
variable        test_var_3                integer         0x20009 
variable        test_var_bit_enum         bit-enumerated  0x2000B 
block           test_block                                0x20000 
