﻿<Method>
{

int ix;

ix = lsetval(234);

}
</Method>
<Function>
Function Name: lsetval
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: 234
  Returned Param Type: RUL_INT
  Returned Param Value: 234
End of Function lsetval
</Function>
<SymbolTable><_bi_rc>234,</_bi_rc><ix>234,</ix></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being saved"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
