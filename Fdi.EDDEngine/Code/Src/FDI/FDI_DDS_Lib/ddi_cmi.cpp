/**
 *	@(#) $Id: ddi_cmi.cpp,v 1.5 2012/08/24 21:25:36 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all of the functions for DDI's interface to 
 *	the connection manager.  i.e. - the connection manager will be
 *	calling these functions.
 */


#include "stdafx.h"
#include "ddi_lib.h"
#include "table.h"
#include "dds_upcl.h"
#include "ddi_tab.h"
#include "dds_tab.h"
#include "cm_loc.h"
#include "conversions.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"
#include "DeviceTypeMgr.h"

/***********************************************************************
 *
 *	Name:  ddi_build_dd_dev_tbls
 *
 *	ShortDesc:  Build DD device tables.
 *
 *	Description:
 *		The ddi_build_dd_dev_tbls function takes in a device type
 *		handle and does what is necessary to build the device
 *		tables (used by DDS) for a particular DD.  Upon successful
 *		return, the Active Device Type Table element of the device
 *		type handle will contain a pointer to these tables.
 *
 *	Inputs:
 *		device_type_handle - handle of the device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_DEV_TYPE_HANDLE.
 *		DDI_INVALID_DD_HANDLE.
 *		DDI_INVALID_DD_HANDLE_TYPE.
 *		DDI_MEMORY_ERROR.
 *		Return values from ddi_load_device_tables function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
ddi_build_dd_dev_tbls(ENV_INFO *env_info, DEVICE_HANDLE	device_handle)
{
	ROD_HANDLE					rod_handle = 0;
	DIR_SPECIFIER_REFERENCE		 ddi_load_tables_request;
	nsEDDEngine::FLAT_DEVICE_DIR				*flat_device_directory;
	int							 r_code;
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	DEVICE_TYPE_HANDLE		device_type_handle = pConnectionMgr->ADT_ADTT_OFFSET(device_handle);
	/*
	 *	Check to make sure that the device type handle is valid.
	 */

	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) {
		return(DDI_INVALID_DEV_TYPE_HANDLE);
	}

	/*
	 *	Check if the device tables have already been built.
	 */

	r_code = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(device_type_handle, 
			(void **)&flat_device_directory) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_directory != (nsEDDEngine::FLAT_DEVICE_DIR *)NULL) {
		return(DDS_SUCCESS);
	}

	/*
	 *	Get the DD handle from the device type handle, and check
	 *	to make sure it is valid.
	 */

	r_code = g_DeviceTypeMgr.get_adtt_dd_handle(device_type_handle, &rod_handle);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!pConnectionMgr->valid_dd_handle(rod_handle)) {
		return(DDI_INVALID_DD_HANDLE);
	}

	/*
	 *	Set up the load tables request structure.
	 */

	ddi_load_tables_request.ref.object_index =
			(OBJECT_INDEX)DEVICE_DIRECTORY_INDEX;


	ddi_load_tables_request.rod_handle = rod_handle;

	/*
	 *	Allocate the device directory structure, load the device
	 *	tables, and put the device tables pointer in the Active
	 *	Device Type Table.
	 */

	DD_DEVICE_ID* dd_device_id = pConnectionMgr->ADT_DD_DEVICE_ID(device_handle);

	flat_device_directory =
			(nsEDDEngine::FLAT_DEVICE_DIR *) calloc(1,sizeof(nsEDDEngine::FLAT_DEVICE_DIR));

	if(dd_device_id)
	{
		::FDI_ConvEDDFileHeader::CopyFrom(&dd_device_id->header,&flat_device_directory->header);
	}
	else
	{
		ASSERT(0);
	}

	if (flat_device_directory == 0) {
		return(DDI_MEMORY_ERROR);
	}

	r_code = ddi_load_device_tables(env_info, &ddi_load_tables_request,
			flat_device_directory);
	if (r_code != DDS_SUCCESS) {
		free((void *)flat_device_directory);
		r_code = g_DeviceTypeMgr.set_adtt_dd_dev_tbls(device_type_handle,(void *)NULL);
		return(DDI_INVALID_DEV_TYPE_HANDLE);
	}

	r_code = g_DeviceTypeMgr.set_adtt_dd_dev_tbls(device_type_handle,
			(void *)flat_device_directory);

	return(r_code);
}


/***********************************************************************
 *
 *	Name:  ddi_remove_dd_dev_tbls
 *
 *	ShortDesc:  Remove DD device tables.
 *
 *	Description:
 *		The ddi_remove_dd_dev_tbls function takes in a device type
 *		handle and removes the DD device tables (used by DDS) for a
 *		particular DD.  Upon successful return, the Active Device Type
 *		Table element of the device type handle will contain a null
 *		pointer for these tables.
 *
 *	Inputs:
 *		device_type_handle - handle of the device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_DEV_TYPE_HANDLE.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
ddi_remove_dd_dev_tbls(ENV_INFO * /* env_info */, DEVICE_TYPE_HANDLE		device_type_handle)
{
	nsEDDEngine::FLAT_DEVICE_DIR				*flat_device_directory=NULL;
	int							r_code ;


	/*
	 *	Check to make sure that the device type handle is valid.
	 */

	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) {
		return(DDI_INVALID_DEV_TYPE_HANDLE);
	}

	/*
	 *	Get the pointer to the DD device tables, and check if
	 *	they are already gone.
	 */

	r_code = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(device_type_handle, 
			(void **)&flat_device_directory);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_directory == 0) {
		return(DDS_SUCCESS);
	}

	/*
	 *	Destroy the device tables, free the device directory structure,
	 *	and null the device tables pointer in the Active Device Type
	 *	Table.
	 */
	//Function below deletes the block table.
	ddi_clean_device_dir(flat_device_directory);

	free((void *)flat_device_directory);

	//After the block table is deleted, then the active device type table is nulled.
	r_code = g_DeviceTypeMgr.set_adtt_dd_dev_tbls(device_type_handle,(void *)NULL);

	return(r_code);
}

/***********************************************************************
 *
 *	Name:  ddi_offset_build_dd_blk_tbls
 *
 *	ShortDesc:  Build DD block tables using the offset of the block
 *				into the block table.
 *
 *	Description:
 *
 *	Inputs:
 *		block_handle - handle of the block.
 *		block_offset - the offset of the block to load DD for.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Zero for success and non-zero for failure.
 *
 *	Author:
 *		Kent Anderson
 *
 **********************************************************************/

static int
ddi_offset_build_dd_blk_tbls(
ENV_INFO		*env_info, 
BLOCK_HANDLE	block_handle,
int				block_offset)
{
	ROD_HANDLE					rod_handle = 0;

	nsEDDEngine::FLAT_DEVICE_DIR				*flat_device_dir = nullptr;
	nsEDDEngine::BLK_TBL						*bt ;
	nsEDDEngine::BLK_TBL_ELEM				*blk_tbl_elem;
	DIR_SPECIFIER_REFERENCE		 ddi_load_tables_request;
	int							 r_code;
	unsigned int				param_count ;
	nsEDDEngine::PARAM_TBL					*pt ;
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Get the pointer to the DD device tables, and check if
	 *	they exist.
	 */

	r_code = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir);

	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	bt = &flat_device_dir->blk_tbl ;

	if (block_offset > BT_COUNT(bt) || block_offset < 0) {
		return(DDI_DEVICE_TABLES_NOT_FOUND);
	}

	blk_tbl_elem = BTE(bt,block_offset) ;

	/*
	 *	Check the usage of the DD block.  If not used yet build the
	 *	block's tables, otherwise just increment the usage and store 
	 *	the Block Table offset.
	 */

	if (blk_tbl_elem->usage == 0) {

		/*
		 *	Point to the DD handle, and check to make sure it is valid.
		 */

		r_code = pConnectionMgr->get_abt_dd_handle(block_handle, &rod_handle);
		if (r_code != SUCCESS) {
			return(r_code) ;
		}

		if (!pConnectionMgr->valid_dd_handle(rod_handle)) {
			return(DDI_INVALID_DD_HANDLE);
		}

		/*
		 *	Set up the load tables request structure.
		 */

		(void) memcpy((char *)&ddi_load_tables_request.ref,
				(char *)&blk_tbl_elem->blk_dir_dd_ref,
				sizeof(nsEDDEngine::DD_REFERENCE));

		ddi_load_tables_request.rod_handle = rod_handle;

		/*
		 *	Load the block tables.
		 */

		r_code = ddi_load_block_tables(env_info, &ddi_load_tables_request,
				&blk_tbl_elem->flat_block_dir,
				flat_device_dir);

		if (r_code != DDS_SUCCESS) {
			return(r_code);
		}

	}

	blk_tbl_elem->usage ++;

	/*
	 * Figure out how many parameters the block has
	 */

	pt = BTE_PT(blk_tbl_elem) ;
	param_count = (unsigned)PT_COUNT(pt) ;

	/*
	 * Save the number of parameters for later use.
	 */

	r_code = pConnectionMgr->set_abt_param_count(block_handle,param_count) ;
	return(r_code);
}


/***********************************************************************
 *
 *	Name:  ddi_id_build_dd_blk_tbls
 *
 *	ShortDesc:  Build DD block tables using the id of the block.
 *
 *	Description:
 *
 *	Inputs:
 *		block_handle - handle of the block.
 *		block_id - id of the of the block to load.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Zero for success and non-zero for failure.
 *
 *	Author:
 *		Kent Anderson
 *
 **********************************************************************/

static int
ddi_id_build_dd_blk_tbls(
ENV_INFO		*env_info,
BLOCK_HANDLE	block_handle,
ITEM_ID			block_id)
{
	ROD_HANDLE					rod_handle = 0;

	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir = nullptr;
	nsEDDEngine::BLK_TBL_ELEM		*blk_tbl_elem = nullptr;
	int							 blk_tbl_offset;
	DIR_SPECIFIER_REFERENCE		 ddi_load_tables_request;
	int							 r_code=0;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!pConnectionMgr->valid_block_handle(block_handle)) {
		return(DDI_INVALID_BLOCK_HANDLE);
	}

	if (block_id == 0) {
		return(DDI_DD_BLOCK_REF_NOT_FOUND);
	}

	/*
	 *	Get the pointer to the DD device tables, and check if
	 *	they exist.
	 */

	r_code = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, 	(void **)&flat_device_dir);

	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_dir == 0) {
		return(DDI_DEVICE_TABLES_NOT_FOUND);
	}

	/*
	 *	Search for the particular block item ID in the Block Table.
	 */

	blk_tbl_elem = (nsEDDEngine::BLK_TBL_ELEM *) bsearch(
			(char *)&block_id,
			(char *)flat_device_dir->blk_tbl.list,
			(unsigned int)flat_device_dir->blk_tbl.count,
			sizeof(nsEDDEngine::BLK_TBL_ELEM),
			(CMP_FN_PTR)compare_bt_elem);

	if (blk_tbl_elem == 0) {
		return(DDI_BLOCK_TABLES_NOT_FOUND);
	}

	blk_tbl_offset =
			(int) (blk_tbl_elem - flat_device_dir->blk_tbl.list);

	r_code = pConnectionMgr->set_abt_dd_blk_tbl_offset(block_handle,blk_tbl_offset);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}
	/*
	 *	Check the usage of the DD block.  If not used yet build the
	 *	block's tables, otherwise just increment the usage and store 
	 *	the Block Table offset.
	 */

	if (blk_tbl_elem->usage == 0) {

		/*
		 *	Point to the DD handle, and check to make sure it is valid.
		 */

		r_code = pConnectionMgr->get_abt_dd_handle(block_handle, &rod_handle);
		if(r_code != SUCCESS) {
			return(r_code) ;
		}

		if (!pConnectionMgr->valid_dd_handle(rod_handle)) {
			return(DDI_INVALID_DD_HANDLE);
		}

		/*
		 *	Set up the load tables request structure.
		 */

		(void) memcpy((char *)&ddi_load_tables_request.ref,
				(char *)&blk_tbl_elem->blk_dir_dd_ref,
				sizeof(nsEDDEngine::DD_REFERENCE));

		ddi_load_tables_request.rod_handle = rod_handle;

		// We knoe what the block_handle and now we set it in env_info so that we can use it in ddi_load_block_tables
		env_info->block_handle = block_handle;
		env_info->handle_type = ENV_INFO::BlockHandle;

		/*
		 *	Load the block tables.
		 */

		r_code = ddi_load_block_tables(env_info, &ddi_load_tables_request,
				&blk_tbl_elem->flat_block_dir, flat_device_dir);
		if (r_code != DDS_SUCCESS) {
			return(r_code);
		}

	}

	blk_tbl_elem->usage ++;
	r_code = pConnectionMgr->set_abt_param_count(block_handle, flat_device_dir->blk_tbl.list[blk_tbl_offset].flat_block_dir.param_tbl.count);
	return(r_code);
}



/***********************************************************************
 *
 *	Name:  ddi_build_dd_blk_tbls
 *
 *	ShortDesc:  Build DD block tables.
 *
 *	Description:
 *		The ddi_build_dd_blk_tbls function takes in a block handle
 *		and does what is necessary to build the block tables (used
 *		by DDS) for a particular DD block.  Upon successful return,
 *		the Active Block Table element of the block handle will
 *		contain a valid Block Table offset, and the Block Table
 *		element of the DD device tables will contain pointers to
 *		the block tables.
 *
 *	Inputs:
 *		block_handle - handle of the block.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_BLOCK_HANDLE.
 *		DDI_DD_BLOCK_REF_NOT_FOUND.
 *		DDI_BLOCK_TABLES_NOT_FOUND.
 *		DDI_INVALID_DD_HANDLE.
 *		DDI_DEVICE_TABLES_NOT_FOUND.
 *		Return values from ddi_load_block_tables function.
 *		Return values from ddi_id_build_dd_blk_tbls function.
 *		Return values from ddi_offset_build_dd_blk_tbls function.
 *
 *	Author:
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int
ddi_build_dd_blk_tbls(	ENV_INFO *env_info,
						BLOCK_HANDLE	block_handle)
{
	ITEM_ID						block_id;
	int							r_code;
	int							block_offset ;
#ifdef DEBUG
	DEVICE_TYPE_HANDLE			device_type_handle ;
#endif
	/*
	 *	Check to make sure that the block handle is valid.
	 */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	if (!pConnectionMgr->valid_block_handle(block_handle)) {
		return(DDI_INVALID_BLOCK_HANDLE);
	}

	/*
	 *	Check if the block tables have already been built.
	 */

	r_code = pConnectionMgr->get_abt_dd_blk_id(block_handle, &block_id);
	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	r_code = pConnectionMgr->get_abt_dd_blk_tbl_offset(block_handle, &block_offset) ;
	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	if ((block_offset >= 0) && (block_id == 0)) 
	{

		/*
		 * The block_offset is filled in, but the block id is
		 * not so load the DD for the block using the block offset
		 */

		r_code = ddi_offset_build_dd_blk_tbls(env_info, block_handle, block_offset) ;

	}
	else if ((block_offset < 0) &&
			(block_id > 0)) {

		/*
		 * The block id is filled in, but the block offset is
		 * not so load the DD for the block using the block id.
		 */

		r_code = ddi_id_build_dd_blk_tbls(env_info, block_handle, block_id) ;

	}
	else if ((block_offset >= 0) &&
		(block_id > 0)) {

		/*
		 * The block offset is filled in, and the block id is
		 * filled in so the DD for the block must already be
		 * loaded.
		 */

		return(DDS_SUCCESS);

	}
	else {

		/*
		 * The block offset is not filled in, and the block id is
		 * filled in so the DD for the block cannot be loaded.
		 */

		return(DDI_DD_BLOCK_REF_NOT_FOUND);

	}

	if (r_code != DDS_SUCCESS) {
		return(r_code) ;
	}

#ifdef	DEBUG
	r_code = pConnectionMgr->get_abt_adtt_offset(block_handle, &device_type_handle);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}
	dds_check_table(env_info, device_type_handle);
#endif

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ddi_remove_dd_blk_tbls
 *
 *	ShortDesc:  Remove DD block tables.
 *
 *	Description:
 *		The ddi_remove_dd_blk_tbls function takes in a block handle
 *		and removes the block tables (used by DDS) for a particular
 *		DD block (if it will no longer be needed).  Upon successful
 *		return, the Active Block Table element of the block handle
 *		will contain a negative Block Table offset, and the Block
 *		Table element of the DD device tables will contain null
 *		pointers to the block tables.
 *
 *	Inputs:
 *		block_handle - handle of the block.
 *
 *	Outputs:
 *		None.
 *		
 *	Returns:
 *		DDS_SUCCESS.
 *		DDI_INVALID_BLOCK_HANDLE.
 *		DDI_DEVICE_TABLES_NOT_FOUND.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
ddi_remove_dd_blk_tbls(ENV_INFO *env_info, BLOCK_HANDLE	block_handle)
{
	int						 blk_tbl_offset;
	nsEDDEngine::FLAT_DEVICE_DIR			*flat_device_directory;
	int						r_code ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!pConnectionMgr->valid_block_handle(block_handle)) {
		return(DDI_INVALID_BLOCK_HANDLE);
	}

	/*
	 *	Get the Block Table offset and check if the block
	 *	tables are already gone.
	 */

	r_code = pConnectionMgr->get_abt_dd_blk_tbl_offset(block_handle, &blk_tbl_offset);
	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	if (blk_tbl_offset < 0) {
		return(DDS_SUCCESS);
	}

	/*
	 *	Get the pointer to the DD device tables, and check if
	 *	they exist.
	 */

	r_code = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_directory);
	if(r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_directory == 0) {
		return(DDI_DEVICE_TABLES_NOT_FOUND);
	}

	/*
	 *	Check the usage of the DD block.  If it is used more than
	 *	once just decrement the usage, otherwise remove the block
	 *	tables.
	 */

	if(!flat_device_directory->blk_tbl.list)
		return (DDS_SUCCESS);

	if (flat_device_directory->
			blk_tbl.list[blk_tbl_offset].usage > 1) {

		flat_device_directory->blk_tbl.list[blk_tbl_offset].usage --;
	}
	else {

		ddi_clean_block_dir(&flat_device_directory->
				blk_tbl.list[blk_tbl_offset].flat_block_dir);

		flat_device_directory->
				blk_tbl.list[blk_tbl_offset].usage = 0;

		r_code = pConnectionMgr->set_abt_dd_blk_tbl_offset(block_handle,-1);
		if (r_code != SUCCESS) {
			return(r_code) ;
		}			
	}

	return(DDS_SUCCESS);
}


