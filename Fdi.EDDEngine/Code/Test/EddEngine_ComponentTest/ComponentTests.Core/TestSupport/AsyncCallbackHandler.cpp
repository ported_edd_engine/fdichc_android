#include "TestSupport/AsyncCallbackHandler.h"

using namespace nsEDDEngine;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {

            void AsyncCallbackHandler::AsyncCallback( nsEDDEngine::IAsyncResult* ar )
            {
                if (_asyncCallback != nullptr)
                {
                    _asyncCallback(ar);
                }
            }

            AsyncCallbackHandler::AsyncCallbackHandler()
                : _asyncCallback(nullptr)
            {
            }

            AsyncCallbackHandler::AsyncCallbackHandler( UnmanagedAsyncCallback asyncCallback )
                : _asyncCallback(asyncCallback)
            {
            }

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests
