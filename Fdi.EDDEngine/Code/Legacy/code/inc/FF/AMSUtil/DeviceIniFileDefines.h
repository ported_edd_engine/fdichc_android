#pragma once

// Sections
#define DEVICE_INI_OPTIONS_SECTION _T("Options")
#define DEVICE_INI_LAYOUT_SECTION _T("LayoutSource")
#define DEVICE_INI_NODOWNLOAD_SECTION _T("no_download")
#define DEVICE_INI_GAUGE_OPTIONS_SECTION _T("GAUGEOPTIONS")
#define DEVICE_INI_DYNAMIC_REFRESH_RATE _T("DynamicRefresh")
#define DEVICE_INI_METHOD_OPTIONS _T("MethodInterpreter")


// Keys
#define DEVICE_INI_USE_NAV_UI_KEY _T("NavigationBar")
#define DEVICE_INI_NOIMAGEINEDDL_KEY _T("NoImageInEDDL")
#define DEVICE_INI_BIT_ENUM_STYLE_KEY _T("BIT_ENUM_STYLE")
#define DEVICE_INI_COMPARE_KEY _T("Compare")
#define DEVICE_INI_CONFIGURE_KEY _T("Configure")
#define DEVICE_INI_PROCESS_VARIABLES_KEY _T("ProcessVariables")
#define DEVICE_INI_DEVICE_DIAGNOSTICS_KEY _T("Diagnostics")
#define DEVICE_INI_EDDL_KEY _T("EDDL")
#define DEVICE_INI_NODOWNLOAD_KEY_FORMAT _T("Param%d")
#define DEVICE_INI_COMPARE_MENU_KEY _T("CompareMenu")
#define DEVICE_INI_CONFIGURE_MENU_KEY _T("ConfigureMenu")
#define DEVICE_INI_PROCESS_VARIABLES_MENU_KEY _T("ProcessVariablesMenu")
#define DEVICE_INI_DEVICE_DIAGNOSTICS_MENU_KEY _T("DeviceDiagnosticsMenu")
#define DEVICE_INI_EDDL_VERTICAL_PARAMETER_KEY _T("EDDLVerticalParameter")
#define DEVICE_INI_EDDL_NUMBER_OF_TICKS_KEY _T("NUM_SCALE_TICKS")
#define DEVICE_INI_EDDL_GAUGE_STYLE_KEY _T("GAUGE_STYLE")
#define DEVICE_INI_LANDING_ZONE_KEY _T("LandingZone")
#define DEVICE_INI_OPTIMIZED_DEVICE_KEY _T("OptimizedDevice")
#define DEVICE_INI_RED_TREEITEMS_TABS_KEY _T("DeviceDiagnosticsRedding")
#define DEVICE_INI_GREEN_TREEITEMS_TABS_KEY _T("CompareGreening")
#define DEVICE_INI_HIGH_LATENCY _T("HighLatency")
#define DEVICE_INI_LOW_LATENCY _T("LowLatency")
#define DEVICE_INI_SHOW_IMAGE_LABELS _T("ShowImageLabels")
#define DEVICE_INI_AUTO_SAVE_VALUES _T("AutoSaveValues")


// Values
#define DEVICE_INI_YES_VALUE _T("Yes")
#define DEVICE_INI_NO_VALUE _T("No")
#define DEVICE_INI_ORIGINAL_VALUE _T("Original")
#define DEVICE_INI_RESOURCE_VALUE _T("Resource")
#define DEVICE_INI_EDDL_VALUE _T("EDDL")
#define DEVICE_INI_HANDHELD_VALUE _T("HandHeld")
#define DEVICE_INI_NONCROSSBLOCK_VALUE _T("NonCrossblock")