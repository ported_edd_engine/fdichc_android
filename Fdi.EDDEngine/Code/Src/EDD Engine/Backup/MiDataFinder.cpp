
#include "stdafx.h"
#include "ServerProjects/rtn_code.h"
#include <Ole/EDDLMethInterp/pathtype.h>
#include "MiDataFinder.h"
#include "enumlist.h"
#include "ddbdefs.h"

using namespace nsEDDEngine;


const wchar_t *timeScaleNames[]=
	{ 
		L"",
		L"s",
		L"min",
		L"h"
	};


//
// Constructor
//
CMiDataFinder::CMiDataFinder(  )
{
}

//
// destructor
//
CMiDataFinder::~CMiDataFinder()
{

}

// SetEDDEngineImpl : Sets EDDEngineImpl instance
void CMiDataFinder::SetEDDEngineImpl( CEDDEngineImpl* pEDDEngineImpl)
{
	m_pEDDEngineImpl = pEDDEngineImpl;
	if (m_pEDDEngineImpl && ((m_pEDDEngineImpl->GetProtocol() == FF) || (m_pEDDEngineImpl->GetProtocol() == ISA100)))
	{
		loadFFDeviceErrorMap();
	}
}

// ResolveNameToID : for a given item name find the associated itemid.
int CMiDataFinder::ResolveNameToID( LPCTSTR szItemName, ITEM_ID *plItemId )
{

	// call to IEDDEngine GetItemIdFromSymbolName function to get item id for given symbol name.
	int nRetVal = m_pEDDEngineImpl->GetItemIdFromSymbolName((wchar_t*)szItemName, plItemId);

	return nRetVal;

}

//ResolveRefToID : For a given index find the associated item ID in item array and collection.
//If the item is not item array or collection, this function returns zero.
ITEM_ID CMiDataFinder::ResolveRefToID( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, long lIndex, const wchar_t *lang_code )
{
	ITEM_ID lRetVal = 0;
	long itemKind = 0;

	itemKind = GetKindFromID( iBlockInstance, lItemId );
	if( itemKind == PT_IARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);
		AttributeNameSet attribute_list = elements;

		// call to GetItemBase function to get generic item for given item id
		int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
			if( flat_item_arry )
			{
				for (int i = 0; i < flat_item_arry->elements.count; i++)
				{
					if (flat_item_arry->elements.list[i].index == (unsigned long)(lIndex))
					{
						lRetVal = flat_item_arry->elements.list[i].ref.id;
						break;
					}
				}
			}
		}
	}
	else if( itemKind == PT_COLLECT )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_COLLECTION);
		AttributeNameSet attribute_list = FLAT_COLLECTION::All_Attrs;

		// call to GetItemBase function to get generic item for given item id
		int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_COLLECTION *flat_collect = dynamic_cast<FLAT_COLLECTION *>(generic_item.item);

			if( flat_collect )
			{
				//look for an item ID that matches the member name
				for( int i = 0; i < flat_collect->op_members.count; i++ )
				{
					if (flat_collect->op_members.list[i].name == (unsigned long)lIndex)
					{
						lRetVal = flat_collect->op_members.list[i].ref.desc_id;
						break;
					}
				}
			}
		}
	}
	else
	{
		lRetVal = 0;
	}

	return lRetVal;
}


int CMiDataFinder::GetValue( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, long lProperty, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	switch(lProperty)
	{
	case PP_ItemLabel:	//used for item array only, i.e. iT_ItemArray or vT_Index
		nRetVal = GetArrayElementLabel( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code);
		break;
	case PP_EditFormat:
		nRetVal = GetEditFormatFromItemId( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_DisplayFormat:
		nRetVal = GetDisplayFormatFromItemId( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Class:
		nRetVal = GetClassFromItemId( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Kind:
		nRetVal = GetKindFromItemId( iBlockInstance, lItemID, pvtValue );
		break;
	case PP_Name:
		nRetVal = GetNameFromItemID( lItemID, pvtValue );
		break;
	case PP_ItemID:
		nRetVal = GetItemID( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_MemberID:
		CV_VT(pvtValue, ValueTagType::CVT_I4);
		CV_I4(pvtValue) = lMemberID;
		nRetVal = DDS_SUCCESS;
		break;
	case PP_Definition:
		nRetVal = GetMethodDefinition( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_Type:
		nRetVal = GetParameterType( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_MethodParams:
		nRetVal = GetMethodParams( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_Handling:
		nRetVal = GetParameterHandling( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Validity:
		nRetVal = GetParameterValidity( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Label:
		nRetVal = GetLabel( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_ScalingFactor: //The Method interpreter expects data from the server to be scaled. 
		nRetVal = GetScalingFactor( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Size:
		nRetVal = GetParameterSize( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_DDL:
		nRetVal = GetDDLString( pvtValue );
		break;
	case PP_Help:
		nRetVal = GetHelp( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_DefaultValue:
		nRetVal = GetDefaultValue( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_ENUMS_AS_XML:
		nRetVal = GetEnumXMLdata( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Count: 
		nRetVal = GetElementCount( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_MinMaxCount: 
		nRetVal = GetMinMaxCount( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_MinValue1: 
		nRetVal = GetMinValue( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_MaxValue1: 
		nRetVal = GetMaxValue( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_IndexItemArray:	//get item ID of array of collection
		nRetVal = GetIndexItemArrayId( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_Units:
		nRetVal = GetUnits( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_NumberOfElements:
		nRetVal = GetNumberOfElements( iBlockInstance, pValueSpec, lItemID, pvtValue, lang_code );
		break;
	case PP_AxisScaling:
		nRetVal = GetAxisScaling( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Capacity:
		nRetVal = GetCapacity( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_IndexedValueList:
		nRetVal = GetIndexedValueList(iBlockInstance, pValueSpec, lItemID,lMemberID, pvtValue,lang_code );
		break;
	case PP_XAxisItemID:
		nRetVal = GetXAxisItemID(iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_YAxisItemID:
		nRetVal = GetYAxisItemID(iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	case PP_Length:
		nRetVal = GetLength(iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
		break;
	default:
		//	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Unsupported property type=%d", lProperty);
		nRetVal = VARIABLE_VALUE_NEEDED;
		break;
	}

	return nRetVal;
}


// GetElementCount: Get the element count for a given item id.
int CMiDataFinder::GetElementCount( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;

	itemKind = GetKindFromID( iBlockInstance, lItemID );
	if( itemKind == PT_IARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);
		AttributeNameSet attribute_list = elements;

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
			if( flat_item_arry )
			{
				CV_VT(pvtValue, ValueTagType::CVT_I4);
				CV_I4(pvtValue) = flat_item_arry->elements.count;
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if( itemKind == PT_FILE )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_FILE);
		AttributeNameSet attribute_list = members;

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_FILE *flat_file = dynamic_cast<FLAT_FILE *>(generic_item.item);
			if( flat_file )
			{
				CV_VT(pvtValue, ValueTagType::CVT_I4);
				CV_I4(pvtValue) = flat_file->members.count;
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}


// GetMinMaxCount: Get the minimum and maximum count for a given item id.
int CMiDataFinder::GetMinMaxCount( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	AttributeNameSet attribute_list = min_value | max_value;

	long lKind = GetKindFromID( iBlockInstance, lItemID );
	switch(lKind)
	{
	case PT_VAR:
		{

			FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				if (flat_var->min_val.count > flat_var->max_val.count)
				{
					CV_UI4(pvtValue) = flat_var->min_val.count;	//take largest count among min/max counts
				}
				else
				{
					CV_UI4(pvtValue) = flat_var->max_val.count;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
		break;
	case PT_AXIS: 
		{
			//getting min/max COUNT for AXIS
			FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_AXIS* flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);

				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				//Is min value specified or min reference specified?
				if (flat_axis-> min_axis.eType != nsEDDEngine::EXPR::EXPR_TYPE_NONE )
				{
					//yes, min value is specified
					CV_UI4(pvtValue) = 1;   
				}
				else if ( ((flat_axis-> min_axis_ref.op_ref_type == nsEDDEngine::STANDARD_TYPE) && (flat_axis->min_axis_ref.op_info.type != ITYPE_NO_VALUE))
					|| ((flat_axis-> min_axis_ref.op_ref_type == nsEDDEngine::COMPLEX_TYPE) && (flat_axis->min_axis_ref.op_info_list.count != 0)) )
				{
					//yes, min reference is specified
					CV_UI4(pvtValue) = 1;   
				}
				else
				{
					CV_UI4(pvtValue) = 0;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
		break;
	default:
		if (lKind < 0)
		{
			//error
			nRetVal = lKind;
		}
		break;
	}
	return nRetVal;
}


// GetMinValue1: Get the minimum value 1 for a given item id.
int CMiDataFinder::GetMinValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = min_value;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, lItemID );
	pvtValue->clear();
	if( itemKind == PT_VAR )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			if( flat_var->min_val.list )
			{
				switch( flat_var->min_val.list[lMemberID].eType )
				{
				case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
					{
						CV_VT(pvtValue, ValueTagType::CVT_I4);
						CV_I4(pvtValue) =(int) flat_var->min_val.list[lMemberID].val.i;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = (int)flat_var->min_val.list[lMemberID].val.u;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R4);
						CV_R4(pvtValue) = flat_var->min_val.list[lMemberID].val.f;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R8);
						CV_R8(pvtValue) = flat_var->min_val.list[lMemberID].val.d;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
					{
					    CV_VT(pvtValue, ValueTagType::CVT_WSTR);
						CV_WSTR(pvtValue) = (wchar_t*)flat_var->min_val.list[lMemberID].val.s.c_str();
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_BINARY: 
					{
						CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
						CV_BITSTR(pvtValue, flat_var->min_val.list[lMemberID].val.b.ptr(), flat_var->min_val.list[lMemberID].val.b.length());
					}
					break;
				default:
					nRetVal = DDI_TAB_BAD_DDID;
					break;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if( itemKind == PT_AXIS )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_AXIS* flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);

			if( flat_axis->min_axis.size > 0 )
			{
				switch( flat_axis->min_axis.eType )
				{
				case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
					{
						CV_VT(pvtValue, ValueTagType::CVT_I4);
						CV_I4(pvtValue) =(int) flat_axis->min_axis.val.i;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) =(int) flat_axis->min_axis.val.u;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R4);
						CV_R4(pvtValue) = flat_axis->min_axis.val.f;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R8);
						CV_R8(pvtValue) = flat_axis->min_axis.val.d;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
					{
						CV_VT(pvtValue, ValueTagType::CVT_WSTR);
						CV_WSTR(pvtValue) = flat_axis->min_axis.val.s.c_str();
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_BINARY: 
					{
						CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
						CV_BITSTR(pvtValue, flat_axis->min_axis.val.b.ptr(), flat_axis->min_axis.val.b.length());
					}
					break;
				default:
					nRetVal = DDI_TAB_BAD_DDID;
					break;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}


// GetMaxValue1: Get the maximum value 1 for a given item id.
int CMiDataFinder::GetMaxValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = max_value;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, lItemID );
	pvtValue->clear();
	if( itemKind == PT_VAR )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			if( flat_var->max_val.list )
			{
				switch( flat_var->max_val.list[lMemberID].eType )
				{
				case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
					{
						CV_VT(pvtValue, CValueVarient::CVT_I4);
						CV_I4(pvtValue) =(int) flat_var->max_val.list[lMemberID].val.i;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
					{
						CV_VT(pvtValue, CValueVarient::CVT_UI4);
						CV_UI4(pvtValue) = (int)flat_var->max_val.list[lMemberID].val.u;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
					{
						CV_VT(pvtValue, CValueVarient::CVT_R4);
						CV_R4(pvtValue) = flat_var->max_val.list[lMemberID].val.f;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
					{
						CV_VT(pvtValue, CValueVarient::CVT_R8);
						CV_R8(pvtValue) = flat_var->max_val.list[lMemberID].val.d;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
					{
						CV_VT(pvtValue, CValueVarient::CVT_WSTR);
						CV_WSTR(pvtValue) = flat_var->max_val.list[lMemberID].val.s.c_str();
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_BINARY:
					{
						CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
						CV_BITSTR(pvtValue, flat_var->max_val.list[lMemberID].val.b.ptr(), flat_var->max_val.list[lMemberID].val.b.length());
					}
					break;
				default:
					nRetVal = DDI_TAB_BAD_DDID;
					break;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if( itemKind == PT_AXIS )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_AXIS* flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);

			if( flat_axis->max_axis.size > 0 )
			{
				switch( flat_axis->max_axis.eType )
				{
				case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
					{
						CV_VT(pvtValue, ValueTagType::CVT_I4);
						CV_I4(pvtValue) = (int)flat_axis->max_axis.val.i;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = (int)flat_axis->max_axis.val.u;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R4);
						CV_R4(pvtValue) = flat_axis->max_axis.val.f;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
					{
						CV_VT(pvtValue, ValueTagType::CVT_R8);
						CV_R8(pvtValue) = flat_axis->max_axis.val.d;
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
					{
						CV_VT(pvtValue, ValueTagType::CVT_WSTR);
						CV_WSTR(pvtValue) = flat_axis->max_axis.val.s.c_str();
					}
					break;
				case nsEDDEngine::EXPR::EXPR_TYPE_BINARY:
					{
						CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
						CV_BITSTR(pvtValue, flat_axis->max_axis.val.b.ptr(), flat_axis->max_axis.val.b.length());
					}
					break;
				default:
					nRetVal = DDI_TAB_BAD_DDID;
					break;
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}


// GetIndexItemArrayId: Get the indexed item array ID for a given item id.
int CMiDataFinder::GetIndexItemArrayId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	AttributeNameSet attribute_list = indexed;
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		CV_VT(pvtValue, ValueTagType::CVT_I4);
		CV_I4(pvtValue) = flat_var->indexed;
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}


//GetDDLString: Returns DDL string from header information
int CMiDataFinder::GetDDLString( CValueVarient *pvtValue )
{
	EDDFileHeader header;

	// Call to IEDDEngine GetEDDFileHeader funtion to get header information
	m_pEDDEngineImpl->GetEDDFileHeader(&header);

	CStdString sDDL;
	// Construction of DDL String using device_id in header information
	sDDL.Format(_T("%08x%04x%04x%04x"),header.device_id.manufacturer,
		header.device_id.device_type, header.device_id.device_revision,
		header.device_id.dd_revision);
	CV_VT(pvtValue, ValueTagType::CVT_WSTR);
	CV_WSTR(pvtValue) = sDDL;

	return DDS_SUCCESS;
}

//
//
//GetNameFromItemID : Gets name for given item id
int CMiDataFinder::GetNameFromItemID( ITEM_ID lItemID, CValueVarient *pvtValue )
{
	wchar_t itemName[128] = {0};

	// call to IEDDEngine interface function GetSymbolNameFromItemId to get the name
	int nRetVal = m_pEDDEngineImpl->GetSymbolNameFromItemId(lItemID, itemName, sizeof(itemName)/sizeof(itemName[0]));

	if(nRetVal == DDS_SUCCESS)
	{
		CV_VT(pvtValue, ValueTagType::CVT_WSTR);
		CV_WSTR(pvtValue) = itemName;
		nRetVal = DDS_SUCCESS;
	}


	return nRetVal;
}


// GetMethodDefinition: Get the method defintion for given item id.
int CMiDataFinder::GetMethodDefinition( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
	AttributeNameSet attribute_list = definition;

	// call to GetItemBase function to get generic item for given item id
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);
		if( flat_method )
		{
			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			if( flat_method->def.size > 0 )
			{
				CV_WSTR(pvtValue) = flat_method->def.data;
			}
			else
			{
				CV_WSTR(pvtValue) = _T("");
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}

	return nRetVal;
}

// GetMethodParams : Get the parameter values
int CMiDataFinder::GetMethodParams( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
	AttributeNameSet attribute_list = method_parameters;

	// call to GetItemBase function to get generic item for given item id
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);
		if( flat_method )
		{
			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			if( flat_method->params.length() > 0 )
			{
				CV_WSTR(pvtValue) = flat_method->params.c_str();
			}
			else
			{
				CV_WSTR(pvtValue) = L"";
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}

	return nRetVal;
}


// GetDefaultValue : Get default value
int CMiDataFinder::GetDefaultValue( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	AttributeNameSet attribute_list = default_value; 
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, lMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		switch( flat_var->default_value.eType )
		{
		case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
			{
				CV_VT(pvtValue, ValueTagType::CVT_I4);
				CV_I4(pvtValue) = (int)flat_var->default_value.val.i;
			}
			break;
		case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
			{
				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				CV_UI4(pvtValue) = (int)flat_var->default_value.val.u;
			}
			break;
		case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
			{
				CV_VT(pvtValue, ValueTagType::CVT_R4);
				CV_R4(pvtValue) = flat_var->default_value.val.f;
			}
			break;
		case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
			{
				CV_VT(pvtValue, ValueTagType::CVT_R8);
				CV_R8(pvtValue) = flat_var->default_value.val.d;
			}
			break;
		case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
			{
				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				CV_WSTR(pvtValue) = flat_var->default_value.val.s.c_str();
			}
			break;
		case nsEDDEngine::EXPR::EXPR_TYPE_BINARY:
			{
				CV_VT(pvtValue, ValueTagType::CVT_BITSTR);
				CV_BITSTR(pvtValue, flat_var->default_value.val.b.ptr(), flat_var->default_value.val.b.length());
			}
			break;
		}
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}


//GetLabel:  Get label for given item id.
int CMiDataFinder::GetLabel( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = label;

	long itemKind = GetKindFromID( iBlockInstance, lItemID );
	switch (itemKind)
	{
		case PT_VAR:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				if(flat_var->label.length() > 0)
				{
					// return label string 
					CV_WSTR(pvtValue) = (wchar_t*)flat_var->label.c_str();
				}
				else
				{
					CV_WSTR(pvtValue) = L"";
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
		break;
		case PT_ARRAY:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_ARRAY *flat_arry = (FLAT_ARRAY *)generic_item.item;

				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				if(flat_arry->label.length() > 0)
				{
					// return label string 
					CV_WSTR(pvtValue) = flat_arry->label.c_str();
				}
				else
				{
					CV_WSTR(pvtValue) = L"";
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
		break;
		case PT_REC:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_RECORD *flat_record = (FLAT_RECORD *)generic_item.item;

				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				if(flat_record->label.length() > 0)
				{
					// return label string 
					CV_WSTR(pvtValue) = flat_record->label.c_str();
				}
				else
				{
					CV_WSTR(pvtValue) = L"";
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
		break;
		case PT_COLLECT:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_COLLECTION);
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_COLLECTION *flat_collect = dynamic_cast<FLAT_COLLECTION *>(generic_item.item);

				if( flat_collect )
				{
					if(flat_collect->label.length() > 0)
					{
						// return label string 
						CV_WSTR(pvtValue) = flat_collect->label.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;
		case PT_IARRAY:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);

			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
				if( flat_item_arry )
				{
					pvtValue->vTagType = ValueTagType::CVT_WSTR;
					if(flat_item_arry->label.length() > 0)
					{
						// return label string 
						CV_WSTR(pvtValue) = flat_item_arry->label.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
				
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;
		case PT_METHOD:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);

			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_METHOD *flat_method = dynamic_cast<FLAT_METHOD *>(generic_item.item);
				if( flat_method )
				{
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					if(flat_method->label.length() > 0)
					{
						// return label string 
						CV_WSTR(pvtValue) = flat_method->label.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
				
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;
		case PT_AXIS:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);

			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_AXIS *flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);
				if( flat_axis )
				{
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					if(flat_axis->label.length() > 0)
					{
						// return label string 
						CV_WSTR(pvtValue) = flat_axis->label.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
				
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;
		case PT_FILE:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_FILE);

			nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_FILE *flat_file = dynamic_cast<FLAT_FILE *>(generic_item.item);
				if( flat_file )
				{
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					if(flat_file->label.length() > 0)
					{
						// return label string 
						CV_WSTR(pvtValue) = flat_file->label.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
				
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;
		
		default:
			if (itemKind < 0)
			{
				//error
				nRetVal = itemKind;
			}
		break;
	}

	return nRetVal;
}

//GetHelp:  Get Help for given item id.
int CMiDataFinder::GetHelp( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = help;

	long itemKind = GetKindFromID( iBlockInstance, lItemID );
	switch (itemKind)
	{
	case  PT_VAR:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			//return help string.
			if (flat_var->help.length() > 0)
			{
				CV_WSTR(pvtValue) = flat_var->help.c_str();
			}
			else
			{
				CV_WSTR(pvtValue) = L"";
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	break;
	case PT_METHOD:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_METHOD* flat_method = dynamic_cast<FLAT_METHOD *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			//return help string.
			if (flat_method->help.length() > 0)
			{
				CV_WSTR(pvtValue) = flat_method->help.c_str();
			}
			else
			{
				CV_WSTR(pvtValue) = L"";
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	break;
	case PT_ARRAY:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ARRAY *flat_arry = (FLAT_ARRAY *)generic_item.item;

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			if (flat_arry->help.length() > 0)
			{
				// return help string 
				CV_WSTR(pvtValue) = flat_arry->help.c_str();
			}
			else
			{
				CV_WSTR(pvtValue) = L"";
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	break;
	case PT_REC:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_RECORD *flat_record = (FLAT_RECORD *)generic_item.item;

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			if (flat_record->help.length() > 0)
			{
				// return help string 
				CV_WSTR(pvtValue) = flat_record->help.c_str();
			}
			else
			{
				CV_WSTR(pvtValue) = L"";
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	break;
	case PT_COLLECT:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_COLLECTION);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_COLLECTION *flat_collect = dynamic_cast<FLAT_COLLECTION *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			if (flat_collect)
			{
				//return help string.
				if (flat_collect->help.length() > 0)
				{
					CV_WSTR(pvtValue) = flat_collect->help.c_str();
				}
				else
				{
					CV_WSTR(pvtValue) = L"";
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	break;
	case PT_IARRAY:
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);

		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
			if (flat_item_arry)
			{
				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				if (flat_item_arry->help.length() > 0)
				{
					// return label string 
					CV_WSTR(pvtValue) = flat_item_arry->help.c_str();
				}
				else
				{
					CV_WSTR(pvtValue) = L"";
				}

			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	break;
	default:
		if (itemKind < 0)
		{
			//error
			nRetVal = itemKind;
		}
		break;
	}

	return nRetVal;
}


//GetParameterType : retunrs variable type for given item id.
int CMiDataFinder::GetParameterType( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	long lKind = GetKindFromID( iBlockInstance, ulItemID );
	switch(lKind)
	{
	case PT_REC:
	case PT_ARRAY:
	case PT_DDLIST:
	case PT_FILE:
	case PT_IARRAY:
	case PT_COLLECT:
		{
			nRetVal = GetItemID( iBlockInstance, pValueSpec, ulItemID, ulMemberID, pvtValue, lang_code );
			if (nRetVal == SUCCESS)
			{
				ITEM_ID recItemID = CV_UI4(pvtValue);
				CV_VT(pvtValue, ValueTagType::CVT_EMPTY);
 				nRetVal = GetParameterType( iBlockInstance, pValueSpec, recItemID, 0, pvtValue, lang_code );
			}
		}
		break;

	case PT_VAR:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
			AttributeNameSet attribute_list = variable_type;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_VAR *flat_var = dynamic_cast< FLAT_VAR *>(generic_item.item);
				if( flat_var )
				{
					CV_VT(pvtValue, ValueTagType::CVT_I4);
					CV_I4(pvtValue) = flat_var->type_size.type;
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;

	case PT_METHOD:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
			AttributeNameSet attribute_list = method_type;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);
				if( flat_method )
				{
					CV_VT(pvtValue, ValueTagType::CVT_I4);
					CV_I4(pvtValue) = flat_method->type; // retunr method type
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;

	case PT_AXIS:
		{
			//getting AXIS min/max value type
			FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
			AttributeNameSet attribute_list = max_value;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_AXIS *flat_axis = dynamic_cast< FLAT_AXIS *>(generic_item.item);
				if( flat_axis )
				{
					CV_VT(pvtValue, ValueTagType::CVT_I4);

					//Is min value specified or min reference specified?
					if (flat_axis-> max_axis.eType != nsEDDEngine::EXPR::EXPR_TYPE_NONE )
					{
						//yes, min value is specified
						switch(flat_axis->max_axis.eType)
						{
						case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
							{
								CV_I4(pvtValue) = VT_FLOAT;
							}
							break;
						case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
							{
								CV_I4(pvtValue) = VT_DOUBLE;
							}
							break;
						case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
							{
								CV_I4(pvtValue) = VT_UNSIGNED;
							}
							break;
						case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
							{
								CV_I4(pvtValue) = VT_INTEGER;
							}
							break;
						case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
							{
								CV_I4(pvtValue) = VT_ASCII;
							}
							break;
						// no default:
						}
					}
					else if ((flat_axis-> max_axis_ref.op_ref_type == nsEDDEngine::STANDARD_TYPE) && (flat_axis->max_axis_ref.op_info.type != ITYPE_NO_VALUE))
					{
						//yes, min stardard reference is specified
						ITEM_ID ref_item_id = flat_axis->max_axis_ref.op_info.id;
						ITEM_ID ref_member_id = flat_axis->max_axis_ref.op_info.member;
						nRetVal = GetParameterType(iBlockInstance, pValueSpec, ref_item_id, ref_member_id, pvtValue, lang_code );
					}
					else if ((flat_axis-> max_axis_ref.op_ref_type == nsEDDEngine::COMPLEX_TYPE) && (flat_axis->max_axis_ref.op_info_list.count != 0))
					{
						//yes, min complex reference is specified
						ITEM_ID ref_item_id = flat_axis->max_axis_ref.op_info_list.list[flat_axis->max_axis_ref.op_info_list.count - 1].id;
						ITEM_ID ref_member_id = flat_axis->max_axis_ref.op_info_list.list[flat_axis->max_axis_ref.op_info_list.count - 1].member;
						nRetVal = GetParameterType(iBlockInstance, pValueSpec, ref_item_id, ref_member_id, pvtValue, lang_code );
					}
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}		
		break;

	default:
		nRetVal = DDI_TAB_BAD_DDID;
		break;
	}
	return nRetVal;
}


// GetParameterSize: get variable size for given item id.
int CMiDataFinder::GetParameterSize( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	long lKind = GetKindFromID( iBlockInstance, ulItemID );
	switch(lKind)
	{
	case PT_REC:
	case PT_ARRAY:
	case PT_DDLIST:
	case PT_FILE:
	case PT_IARRAY:
	case PT_COLLECT:
		{
			nRetVal = GetItemID( iBlockInstance, pValueSpec, ulItemID, ulMemberID, pvtValue, lang_code );
			if (nRetVal == SUCCESS)
			{
				ITEM_ID recItemID = CV_UI4(pvtValue);
				CV_VT(pvtValue, ValueTagType::CVT_EMPTY);
 				nRetVal = GetParameterSize( iBlockInstance, pValueSpec, recItemID, 0, pvtValue, lang_code );
			}
		}
		break;

	case PT_VAR:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
			AttributeNameSet attribute_list = variable_type;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_VAR *flat_var = dynamic_cast< FLAT_VAR *>(generic_item.item);
				if( flat_var )
				{
					CV_VT(pvtValue, ValueTagType::CVT_I4);
					CV_I4(pvtValue) = flat_var->type_size.size;
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}
		break;

	case PT_AXIS:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
			AttributeNameSet attribute_list = max_value | min_value;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_AXIS *flat_axis = dynamic_cast< FLAT_AXIS *>(generic_item.item);
				if( flat_axis )
				{
					CV_VT(pvtValue, ValueTagType::CVT_UI4);

					//Is min value specified or min reference specified?
					if (flat_axis-> max_axis.eType != nsEDDEngine::EXPR::EXPR_TYPE_NONE )
					{
						//yes, min value is specified
						if (flat_axis->max_axis.size > flat_axis->min_axis.size)
						{
							CV_UI4(pvtValue) = flat_axis->max_axis.size;
						}
						else
						{
							CV_UI4(pvtValue) = flat_axis->min_axis.size;
						}
					}
					else if ((flat_axis-> max_axis_ref.op_ref_type == nsEDDEngine::STANDARD_TYPE) && (flat_axis->max_axis_ref.op_info.type != ITYPE_NO_VALUE))
					{
						//yes, min stardard reference is specified
						ITEM_ID ref_item_id = flat_axis->max_axis_ref.op_info.id;
						ITEM_ID ref_member_id = flat_axis->max_axis_ref.op_info.member;
						nRetVal = GetParameterSize(iBlockInstance, pValueSpec, ref_item_id, ref_member_id, pvtValue, lang_code );
					}
					else if ((flat_axis-> max_axis_ref.op_ref_type == nsEDDEngine::COMPLEX_TYPE) && (flat_axis->max_axis_ref.op_info_list.count != 0))
					{
						//yes, min complex reference is specified
						ITEM_ID ref_item_id = flat_axis->max_axis_ref.op_info_list.list[flat_axis->max_axis_ref.op_info_list.count - 1].id;
						ITEM_ID ref_member_id = flat_axis->max_axis_ref.op_info_list.list[flat_axis->max_axis_ref.op_info_list.count - 1].member;
						nRetVal = GetParameterSize(iBlockInstance, pValueSpec, ref_item_id, ref_member_id, pvtValue, lang_code );
					}
				}
				else
				{
					nRetVal = DDI_TAB_BAD_DDID;
				}
			}
		}		
		break;

	default:
		nRetVal = DDI_TAB_BAD_DDID;
		break;
	}
	return nRetVal;
}


//GetParameterHandling: Returns parameter handling for given item id.
int CMiDataFinder::GetParameterHandling( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = GetItemID( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
	if (nRetVal == DDS_SUCCESS)
	{
		AttributeNameSet attribute_list = handling;
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		ITEM_ID itemID = CV_UI4(pvtValue);

		nRetVal = GetItemBase(iBlockInstance, pValueSpec, itemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_I4);
			CV_I4(pvtValue) = (int)flat_var->handling;
		}
		else
		{
			pvtValue->clear();
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}

	return nRetVal;
}

//GetParameterHandling: Returns parameter handling for given item id.
int CMiDataFinder::GetParameterValidity( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = DDS_SUCCESS;

	
	nRetVal = GetItemID( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
	if (nRetVal == DDS_SUCCESS)
	{
		AttributeNameSet attribute_list = validity;
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		ITEM_ID itemID = CV_UI4(pvtValue);

		nRetVal = GetItemBase(iBlockInstance, pValueSpec, itemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_BOOL);
			CV_BOOL(pvtValue) = (flat_var->valid != 0) ? TRUE : FALSE; /* valid: short 0 == FALSE, 1 == TRUE */
		}
		else
		{
			pvtValue->clear();
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	

	return nRetVal;
}


// GetItemType when provided a block instance and an item id
// Returns ITYPE_NO_VALUE, if not found
nsEDDEngine::ITEM_TYPE CMiDataFinder::GetItemType(int iBlockInstance, ITEM_ID lItemID)
{
	FDI_ITEM_SPECIFIER item_specifier = {FDI_ITEM_ID, lItemID, 0};
	nsEDDEngine::ITEM_TYPE			item_type = ITYPE_NO_VALUE;

	int rc = m_pEDDEngineImpl->GetItemType(iBlockInstance, &item_specifier, &item_type);
	if (rc != SUCCESS)
	{
		item_type = ITYPE_NO_VALUE;
	}

	return item_type;
}

// GetItemTypeAndItemId Returns the Item Type and Item Id of the specified Item
int CMiDataFinder::GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType, ITEM_ID* pItemId)
{
	return m_pEDDEngineImpl->GetItemTypeAndItemId(iBlockInstance,pItemSpec, pItemType, pItemId);
}



// GetKindFromID : Get the kind based on itemid (used internally)
// If the kind (item type) is not found, the returned value is error code.
long CMiDataFinder::GetKindFromID( int iBlockInstance, ITEM_ID lItemID )
{
	long lRetVal = DDI_TAB_BAD_DDID;


	FDI_ITEM_SPECIFIER item_specifier = {FDI_ITEM_ID, lItemID, 0};
	nsEDDEngine::ITEM_TYPE			item_type;

	// call to IEDDEngine GetItemType function to get type for given item id
	int iStatus = m_pEDDEngineImpl->GetItemType(iBlockInstance, &item_specifier, &item_type);
	if(iStatus == DDS_SUCCESS)
	{
		switch(item_type)
		{
			case ITYPE_VARIABLE:
				lRetVal = PT_VAR;
				break;
			case ITYPE_METHOD:
				lRetVal = PT_METHOD;
				break;
			case ITYPE_ITEM_ARRAY:
				lRetVal = PT_IARRAY;
				break;
			case ITYPE_COLLECTION:
				lRetVal = PT_COLLECT;
				break;
			case ITYPE_COMMAND:
				lRetVal = PT_COMMAND;
				break;
			case ITYPE_LIST:
				lRetVal = PT_DDLIST;	//?PT_DDLIST_VAR, PT_DDLIST_VAR, PT_DDLIST_RECORD, PT_DDLIST_ARRAY
				break;
			case ITYPE_RECORD:
				lRetVal = PT_REC;
				break;
			case ITYPE_ARRAY:
				lRetVal = PT_ARRAY;
				break;
			case ITYPE_MENU:
				lRetVal = PT_MENU;
				break;
			case ITYPE_FILE:
				lRetVal = PT_FILE;
				break;
			case ITYPE_AXIS:
				lRetVal = PT_AXIS;
				break;
			case ITYPE_GRAPH:
				lRetVal = PT_GRAPH;
				break;
			case ITYPE_WAVEFORM:
				lRetVal = PT_WAVE_FORM;
				break;
			case ITYPE_CHART:
				lRetVal = PT_CHART;
				break;
			case ITYPE_SOURCE:
				lRetVal = PT_SOURCE;
				break;
			case ITYPE_VAR_LIST:
				lRetVal = PT_PLIST;
				break;
			default:
				break;
		}
	}
	else
	{
		lRetVal = iStatus;
	}

	return lRetVal;
}


//GetKindFromItemId 
int CMiDataFinder::GetKindFromItemId( int iBlockInstance, ITEM_ID lItemID, CValueVarient *pvtValue )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	long lKind = GetKindFromID( iBlockInstance, lItemID );

	if( lKind > -1 )
	{
		CV_VT(pvtValue, ValueTagType::CVT_I4);
		CV_I4(pvtValue) = lKind;
		nRetVal = DDS_SUCCESS;
	}
	else
	{
		nRetVal = lKind;
	}

	return nRetVal;
}

//GetEnumXMLdata : dumps enum information into xml format for given item id.
int CMiDataFinder::GetEnumXMLdata( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code)
{
	int nRetVal = METH_INTERNAL_ERROR;

	AttributeNameSet attribute_list = enumerations;
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, lMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		EnumList* pList = new EnumList(lItemID);

		// Add enumeration values if they exist
		if(flat_var->enums.count > 0)
		{
			for( int i = 0; i < flat_var->enums.count; i++ )
			{
				if(!pList->AddEnumElement(flat_var->enums.list[i].val.val.u, (wchar_t*)flat_var->enums.list[i].desc.c_str(), (wchar_t*)flat_var->enums.list[i].help.c_str()))
				{
					nRetVal = METH_INTERNAL_ERROR;
					break;
				}	
			}
		}
		if(nRetVal == DDS_SUCCESS)
		{
			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			CV_WSTR(pvtValue) = pList->GetList();
		}

		if(pList)
		{
			delete pList;
			pList = NULL;
		}
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}

//GetArrayElementLabel: For a given index find the associated name in item array.
int CMiDataFinder::GetArrayElementLabel( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, long lIndex, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = METH_INTERNAL_ERROR;
	long itemKind = 0;

	itemKind = GetKindFromID( iBlockInstance, lItemID );

	if( itemKind == PT_IARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);
		AttributeNameSet attribute_list = elements;

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
			if( flat_item_arry )
			{
				for (int i = 0; i < flat_item_arry->elements.count; i++)
				{
					if (flat_item_arry->elements.list[i].index == (unsigned long)(lIndex))
					{
						CV_VT(pvtValue, ValueTagType::CVT_WSTR);
						if (flat_item_arry->elements.list[i].desc.length() > 0)
						{
							CV_WSTR(pvtValue) = flat_item_arry->elements.list[i].desc.c_str();
						}
						else
						{
							CV_WSTR(pvtValue) = L"";
						}
						break;
					}
				}
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

//GetClassFromItemId : Gets the class for given item id.
int CMiDataFinder::GetClassFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = GetItemID( iBlockInstance, pValueSpec, lItemID, lMemberID, pvtValue, lang_code );
	if (nRetVal == DDS_SUCCESS)
	{
		AttributeNameSet attribute_list = class_attr;
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		ITEM_ID itemID = CV_UI4(pvtValue);

		nRetVal = GetItemBase(iBlockInstance, pValueSpec, itemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_I4);
			CV_I4(pvtValue) = (int)flat_var->class_attr;
		}
		else
		{
			pvtValue->clear();
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}

	return nRetVal;
}

// GetEditFormatFromItemId : This funtions gets Edit format property.
int CMiDataFinder::GetEditFormatFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	AttributeNameSet attribute_list = edit_format; 
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, lMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		CV_VT(pvtValue, ValueTagType::CVT_WSTR);
		if(flat_var->edit.length() > 0)
		{
			CV_WSTR(pvtValue) = flat_var->edit.c_str();
		}
		else
		{
			CV_WSTR(pvtValue) = L"";
		}
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}

int CMiDataFinder::GetDisplayFormatFromItemId( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	AttributeNameSet attribute_list = display_format; 
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		CV_VT(pvtValue, ValueTagType::CVT_WSTR);
		if(flat_var->display.length() > 0)
		{
			CV_WSTR(pvtValue) = flat_var->display.c_str();
		}
		else
		{
			CV_WSTR(pvtValue) = L"";
		}
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}

/* GetResponseCodeString : gets response code string based on given command number and response code.
 * If the response string is not found due to command number or response code or DD, 
 * this function returns error code ENUM_NOT_FOUND. Otherwise, it returns DDS_SUCCESS(0)
 */
int CMiDataFinder::GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	ITEM_ID item_id = {0};


	if (protocol == nsEDDEngine::HART)
	{
		// Call to IEDDEngine interface function to get item id based on command number
		nRetVal = m_pEDDEngineImpl->GetCmdIdFromNumber(iBlockInstance, nCommandNumber, &item_id);
	}
	else if (protocol == nsEDDEngine::PROFIBUS)
	{
		item_id = (ITEM_ID)nCommandNumber;		//Profibus command ID
		nRetVal = DDS_SUCCESS;
	}

	if(nRetVal == DDS_SUCCESS) 
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_COMMAND);
		AttributeNameSet attribute_list = response_codes;

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, item_id, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_COMMAND *flat_command = dynamic_cast< FLAT_COMMAND *>(generic_item.item);
			if( flat_command )
			{
				//get item for ITYPE_RESP_CODES for given reference
				FDI_GENERIC_ITEM generic_item2(ITYPE_RESP_CODES);
				RESPONSE_CODE_LIST *pResponseCodeList = nullptr;

				// if response code type is RESP_CODE_TYPE_LIST
				if(flat_command->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST)
				{
					// Assign response code list 
					pResponseCodeList = &(flat_command->resp_codes.response_codes.resp_code_list);
				}
				// if response code type is RESP_CODE_TYPE_REF
				else if(flat_command->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF)
				{
					// call to GetItemBase function to get generic item for given item id
					nRetVal = GetItemBase(iBlockInstance, pValueSpec, flat_command->resp_codes.response_codes.resp_code_ref, 0, &attribute_list, &generic_item2, lang_code);
					if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
					{
						FLAT_RESP_CODE *flat_resp_code = dynamic_cast< FLAT_RESP_CODE *>(generic_item2.item);
						if( flat_resp_code )
						{
							pResponseCodeList = &(flat_resp_code->member);
						}
					}

				}

				nRetVal = ENUM_NOT_FOUND;
				if (pResponseCodeList != nullptr)
				{
					for(int i = 0; i < pResponseCodeList->count; i++)
					{
						if(pResponseCodeList->list[i].val == (unsigned long) nResponseCode)
						{
							// assign response code string associated with given response code.
							CV_VT(pvtValue, ValueTagType::CVT_WSTR);
							CV_WSTR(pvtValue) = pResponseCodeList->list[i].desc.c_str();
							nRetVal = DDS_SUCCESS;
							break;
						}
					}
				}
			}
			else
			{
				nRetVal = ENUM_NOT_FOUND;
			}
		}
		else
		{
			nRetVal = ENUM_NOT_FOUND;
		}
	}
	return nRetVal;
}

//GetVarInfo : returns FLAT_VAR for given item id and member id.
int CMiDataFinder::GetVarInfo(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, nsEDDEngine::FLAT_VAR** pVar, const wchar_t *lang_code)
{
	AttributeNameSet attribute_list = FLAT_VAR::All_Attrs;
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, lMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		*pVar = dynamic_cast<FLAT_VAR *>(generic_item.item);
		generic_item.item = nullptr;
	}

	return nRetVal;
}


//This function process item ID and member ID into FDI_ITEM_SPECIFIER variable. Then this function calls IEDDEngine GetItem function to get generic item
int CMiDataFinder::GetItemBase(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, nsEDDEngine::AttributeNameSet *pAttrList, nsEDDEngine::FDI_GENERIC_ITEM* pGenericItem, const wchar_t *lang_code)
{
	int iSubIndex = 0;
	FDI_ITEM_SPECIFIER item_spec;

	//convert the member ID into a SubIndex in terms of variable table
	if (ulMemberID != 0)
	{
		int ddsCode = ResolveSubindex ( iBlockInstance, ulItemID, ulMemberID, &iSubIndex);
		if (ddsCode != DDS_SUCCESS)
		{
			return DDI_TAB_BAD_DDID;
		}
		item_spec.eType = FDI_ITEM_ID_SI;		//doesn't work if lMemberID == 0
	}
	else
	{
		item_spec.eType = FDI_ITEM_ID;
	}
	item_spec.item.id = ulItemID;
	item_spec.subindex = iSubIndex;

	// call to IEDDEngine GetItem function to get generic item for given item id and member id
	int nRetVal = m_pEDDEngineImpl->GetItem(iBlockInstance, pValueSpec, &item_spec, pAttrList, lang_code, pGenericItem);

	return nRetVal;
}


int CMiDataFinder::GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	return m_pEDDEngineImpl->GetDictionaryString(ulIndex, pString, iStringLen, lang_code) ;
}

int CMiDataFinder::GetDictionaryString( wchar_t* wsDictName,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	return m_pEDDEngineImpl->GetDictionaryString(wsDictName, pString, iStringLen, lang_code) ;
}


int CMiDataFinder::GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	return m_pEDDEngineImpl->GetDevSpecString(ulIndex, pString, iStringLen, lang_code) ;
}


//GetUnits:  Get unit string for given item id.
int CMiDataFinder::GetUnits( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = constant_unit | time_scale;


	long itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_VAR )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			CV_WSTR(pvtValue) = L"";
			if(flat_var->constant_unit.length() > 0)
			{
				// return constant unit string
				
				CV_WSTR(pvtValue) = flat_var->constant_unit.c_str();
			}
			else if ((flat_var->time_scale > 0) && (VT_TIME_VALUE == flat_var->type_size.type))
			{
				// return time_scale string
				
				switch (flat_var->time_scale)		//this algorithm only works for legacy HART. Better algorithm shall be used after nsEDDEngine\Flats.h is updated
				{
				case FDI_TIME_SCALE_SECONDS:
					CV_WSTR(pvtValue) =  timeScaleNames[1];
					break;
				case FDI_TIME_SCALE_MINUTES:
					CV_WSTR(pvtValue) = timeScaleNames[2];
					break;
				case FDI_TIME_SCALE_HOURS:
					CV_WSTR(pvtValue) = timeScaleNames[3];
					break;
				}
			}
			else
			{
				//this is not constant unit or time_scale
				//get UNIT variable item ID
				int iSubIndex = 0;
				nsEDDEngine::FDI_PARAM_SPECIFIER	param_spec;

				//convert the member ID into a SubIndex in terms of variable table
				if (ulMemberID != 0)
				{
					ResolveSubindex ( iBlockInstance, ulItemID, ulMemberID, &iSubIndex);
					param_spec.eType = FDI_PS_ITEM_ID_SI;		//doesn't work if lMemberID == 0
				}
				else
				{
					param_spec.eType = FDI_PS_ITEM_ID;
				}
				param_spec.id = ulItemID;
				param_spec.subindex = iSubIndex;

				ITEM_ID unitItemId = 0;	//initial value
				nRetVal = m_pEDDEngineImpl->GetParamUnitRelItemId(iBlockInstance, pValueSpec, &param_spec, &unitItemId);
				if ((nRetVal == DDS_SUCCESS) && (unitItemId > 0))
				{
					//get unit variable
					attribute_list.clear();
					attribute_list = relation_watch_variable;
					FDI_GENERIC_ITEM generic_item2(ITYPE_UNIT);
					nRetVal = GetItemBase(iBlockInstance, pValueSpec, unitItemId, 0, &attribute_list, &generic_item2, lang_code);

					if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
					{
						FLAT_UNIT* flat_unit = dynamic_cast<FLAT_UNIT *>(generic_item2.item);

						convertOPRefTrailToParamSpec(iBlockInstance, &flat_unit->var, &param_spec);

						nsConsumer::EVAL_VAR_VALUE unitValue;
						nRetVal = (m_pEDDEngineImpl->GetParamCache())->GetParamValue(iBlockInstance, pValueSpec, &(param_spec), &unitValue);

						if (nRetVal == DDS_SUCCESS)
						{
							switch (unitValue.type)
							{
							case VT_ASCII:
							case VT_PACKED_ASCII:
							{
								// return unit varialbe (string) unit string
								
								CV_WSTR(pvtValue) = unitValue.val.s.c_str();
							}
								break;
							case VT_ENUMERATED:
							{
								//get unit variable (enum) unit string
								attribute_list.clear();
								attribute_list = enumerations;
								FDI_GENERIC_ITEM generic_item3(ITYPE_VARIABLE);
								nRetVal = GetItemBase(iBlockInstance, pValueSpec, param_spec.id, 0, &attribute_list, &generic_item3, lang_code);

								if ((nRetVal == DDS_SUCCESS) && generic_item3.item)
								{
									FLAT_VAR* flat_var3 = dynamic_cast<FLAT_VAR *>(generic_item3.item);

									if(flat_var3->enums.count > 0)
									{
										for( int i = 0; i < flat_var3->enums.count; i++ )
										{
											unsigned long long ullEnumValue = 0;
											switch (flat_var3->enums.list[i].val.eType)
											{
											case EXPR::EXPR_TYPE_UNSIGNED:
												ullEnumValue = flat_var3->enums.list[i].val.val.u;
												break;
											case EXPR::EXPR_TYPE_INTEGER:
												ullEnumValue = (unsigned long long)flat_var3->enums.list[i].val.val.i;
												break;
											}
											if(ullEnumValue == unitValue.val.u)
											{
												
												CV_WSTR(pvtValue) = flat_var3->enums.list[i].desc.c_str();
												break;
											}	
										}
									}
								}
							}
							break;

							default:
							break;
							}
						}
					}
				}
			}
		}
	}
	else if( itemKind == PT_AXIS )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_AXIS* flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);

			CV_VT(pvtValue, ValueTagType::CVT_WSTR);
			CV_WSTR(pvtValue) = L"";
			if(flat_axis->constant_unit.length() > 0)
			{
				// return constant unit string
				
				CV_WSTR(pvtValue) = flat_axis->constant_unit.c_str();
			}
			else
			{
				//this is not constant unit
				//get UNIT variable item ID

				ITEM_ID unitItemId = 0;	//initial value
				nRetVal = m_pEDDEngineImpl->GetAxisUnitRelItemId(iBlockInstance, pValueSpec, ulItemID, &unitItemId);
				if ((nRetVal == DDS_SUCCESS) && (unitItemId > 0))
				{
					//get unit variable
					attribute_list.clear();
					attribute_list = relation_watch_variable;
					FDI_GENERIC_ITEM generic_item2(ITYPE_UNIT);
					nRetVal = GetItemBase(iBlockInstance, pValueSpec, unitItemId, 0, &attribute_list, &generic_item2, lang_code);

					if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
					{
						FLAT_UNIT* flat_unit = dynamic_cast<FLAT_UNIT *>(generic_item2.item);

						nsEDDEngine::FDI_PARAM_SPECIFIER	param_spec;
						convertOPRefTrailToParamSpec(iBlockInstance, &flat_unit->var, &param_spec);

						nsConsumer::EVAL_VAR_VALUE unitValue;
						nRetVal = (m_pEDDEngineImpl->GetParamCache())->GetParamValue(iBlockInstance, pValueSpec, &(param_spec), &unitValue);

						if (nRetVal == DDS_SUCCESS)
						{
							switch (unitValue.type)
							{
							case VT_ASCII:
							case VT_PACKED_ASCII:
							{
								// return unit varialbe (string) unit string
								
								CV_WSTR(pvtValue) =  unitValue.val.s.c_str();
							}
							break;
							case VT_ENUMERATED:
							{
								//get unit variable (enum) unit string
								attribute_list.clear();
								attribute_list = enumerations;
								FDI_GENERIC_ITEM generic_item3(ITYPE_VARIABLE);
								nRetVal = GetItemBase(iBlockInstance, pValueSpec, param_spec.id, 0, &attribute_list, &generic_item3, lang_code);

								if ((nRetVal == DDS_SUCCESS) && generic_item3.item)
								{
									FLAT_VAR* flat_var3 = dynamic_cast<FLAT_VAR *>(generic_item3.item);

									if(flat_var3->enums.count > 0)
									{
										for( int i = 0; i < flat_var3->enums.count; i++ )
										{
											unsigned long long ullEnumValue = 0;
											switch (flat_var3->enums.list[i].val.eType)
											{
											case EXPR::EXPR_TYPE_UNSIGNED:
												ullEnumValue = flat_var3->enums.list[i].val.val.u;
												break;
											case EXPR::EXPR_TYPE_INTEGER:
												ullEnumValue = (unsigned long long)flat_var3->enums.list[i].val.val.i;
												break;
											}
											if(ullEnumValue == unitValue.val.u)
											{
												
												CV_WSTR(pvtValue) = flat_var3->enums.list[i].desc.c_str();
												break;
											}	
										}
									}
								}
							}
							break;

							default:
							break;
							}
						}
					}
				}
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex)
{
	long itemKind = GetKindFromID( iBlockInstance, ItemId );
	if ( itemKind == PT_REC )
	{
		return m_pEDDEngineImpl->ResolveSubindex(iBlockInstance, ItemId, MemberId, iSubIndex);
	}
	else if ( MemberId != 0)	//other itemKind including PT_DDLIST
	{
		*iSubIndex = MemberId;
		return SUCCESS;
	}
	else
	{
		return DDI_TAB_BAD_DDID;
	}
}

//This function looks up a MemberId which cooresponds to the given RECORD ItemId and SubIndex
//For other variable types, this function returns error code.
int CMiDataFinder::LookupMemberId (nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, SUBINDEX ulSubIndex, const wchar_t *lang_code,
								   /* out */ITEM_ID* pulMemberId)
{
	int nRetVal = DDI_TAB_BAD_DDID;

	long itemKind = GetKindFromID( iBlockInstance, ulItemId );

	if (itemKind == PT_REC)
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
		AttributeNameSet attribute_list = members;

		// call to GetItemBase function to get generic item for given file item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_RECORD* flat_rec = dynamic_cast<FLAT_RECORD *>(generic_item.item);

			if ( (ulSubIndex > 0) && (ulSubIndex <= flat_rec->members.count) )
			{
				//note that iSubindex is 1 based value.
				*pulMemberId = flat_rec->members.list[ulSubIndex - 1].name;

				nRetVal = DDS_SUCCESS;
			}
		}
	}

	return nRetVal;
}


int CMiDataFinder::GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence)
{	
	return m_pEDDEngineImpl->GetBlockInstanceByObjIndex( iObjectIndex, iOccurrence);
}


int CMiDataFinder::GetBlockInstanceByTag(ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence)
{
	return m_pEDDEngineImpl->GetBlockInstanceByTag( iItemId, pTag, iOccurrence );
}


int CMiDataFinder::GetBlockInstanceCount( ITEM_ID iItemId, int* piCount)
{
	return m_pEDDEngineImpl->GetBlockInstanceCount( iItemId, piCount );
}


int CMiDataFinder::ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance)
{
	return m_pEDDEngineImpl->ConvertToBlockInstance( ulItemId, iOccurrence, piBlockInstance );
}


int CMiDataFinder::GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, ITEM_ID /*lMemberId*/, int nResponseCode, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = response_codes;
	RESPONSE_CODE_LIST *pResponseCodeList = nullptr;
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	//get item for ITYPE_RESP_CODES for given reference
	FDI_GENERIC_ITEM generic_item2(ITYPE_RESP_CODES);

	long itemKind = GetKindFromID( iBlockInstance, lItemId );

	//get RESPONSE_CODE_LIST pointer
	if ( itemKind == PT_VAR )
	{
		generic_item.item_type = ITYPE_VARIABLE;
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			// if response code type is RESP_CODE_TYPE_LIST
			if(flat_var->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST)
			{
				// Assign response code list 
				pResponseCodeList = &(flat_var->resp_codes.response_codes.resp_code_list);
			}
			// if response code type is RESP_CODE_TYPE_REF
			else if(flat_var->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF)
			{
				// call to GetItemBase function to get generic item for given item id
				nRetVal = GetItemBase(iBlockInstance, pValueSpec, flat_var->resp_codes.response_codes.resp_code_ref, 0, &attribute_list, &generic_item2, lang_code);

				if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
				{
					FLAT_RESP_CODE *flat_resp_code = dynamic_cast< FLAT_RESP_CODE *>(generic_item2.item);
					if( flat_resp_code )
					{
						pResponseCodeList = &(flat_resp_code->member);
					}
				}
			}
		}
	}
	else if (itemKind == PT_ARRAY)
	{
		generic_item.item_type = ITYPE_ARRAY;
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ARRAY *flat_arry = (FLAT_ARRAY *)generic_item.item;

			// if response code type is RESP_CODE_TYPE_LIST
			if(flat_arry->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST)
			{
				// Assign response code list 
				pResponseCodeList = &(flat_arry->resp_codes.response_codes.resp_code_list);
			}
			// if response code type is RESP_CODE_TYPE_REF
			else if(flat_arry->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF)
			{
				// call to GetItemBase function to get generic item for given item id
				nRetVal = GetItemBase(iBlockInstance, pValueSpec, flat_arry->resp_codes.response_codes.resp_code_ref, 0, &attribute_list, &generic_item2, lang_code);

				if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
				{
					FLAT_RESP_CODE *flat_resp_code = dynamic_cast< FLAT_RESP_CODE *>(generic_item2.item);
					if( flat_resp_code )
					{
						pResponseCodeList = &(flat_resp_code->member);
					}
				}
			}
		}
	}
	else if (itemKind == PT_REC)
	{
		generic_item.item_type = ITYPE_RECORD;
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemId, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_RECORD *flat_record = (FLAT_RECORD *)generic_item.item;

			// if response code type is RESP_CODE_TYPE_LIST
			if(flat_record->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST)
			{
				// Assign response code list 
				pResponseCodeList = &(flat_record->resp_codes.response_codes.resp_code_list);
			}
			// if response code type is RESP_CODE_TYPE_REF
			else if(flat_record->resp_codes.eType == nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF)
			{
				// call to GetItemBase function to get generic item for given item id
				nRetVal = GetItemBase(iBlockInstance, pValueSpec, flat_record->resp_codes.response_codes.resp_code_ref, 0, &attribute_list, &generic_item2, lang_code);

				if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
				{
					FLAT_RESP_CODE *flat_resp_code = dynamic_cast< FLAT_RESP_CODE *>(generic_item2.item);
					if( flat_resp_code )
					{
						pResponseCodeList = &(flat_resp_code->member);
					}
				}
			}
		}
	}

	if (nRetVal == DDS_SUCCESS) 
	{
		nRetVal = DDI_TAB_BAD_DDID;

		if (pResponseCodeList != nullptr)
		{
			for(int i = 0; i < pResponseCodeList->count; i++)
			{
				if(pResponseCodeList->list[i].val == (unsigned long) nResponseCode)
				{
					// assign response code string associated with given response code.
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					CV_WSTR(pvtValue) = pResponseCodeList->list[i].desc.c_str();
					nRetVal = DDS_SUCCESS;
				}
			}
		}
		
		if ((nRetVal != DDS_SUCCESS) && ((m_pEDDEngineImpl->GetProtocol() == FF) || (m_pEDDEngineImpl->GetProtocol() == ISA100)))
		{
			nRetVal = GetFFDeviceCodeString((unsigned long)nResponseCode, pvtValue, lang_code);
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

wchar_t* CMiDataFinder::GetErrorString(int iErrorNum)
{
	return m_pEDDEngineImpl->GetErrorString(iErrorNum);
}

// GetNumberOfElements: Get the number of element in array for a given item id.
int CMiDataFinder::GetNumberOfElements( int iBlockInstance, void* pValueSpec, ITEM_ID lItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;

	itemKind = GetKindFromID( iBlockInstance, lItemID );
	if( itemKind == PT_ARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
		AttributeNameSet attribute_list = number_of_elements;	//for array use

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, lItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ARRAY *flat_arry = dynamic_cast<FLAT_ARRAY *>(generic_item.item);
			if( flat_arry )
			{
				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				CV_UI4(pvtValue) = flat_arry->num_of_elements;
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}


ITEM_ID CMiDataFinder::resolveBlockRef( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulMemberId, ResolveType eResolveType, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;

	ITEM_ID ulRetItemId = 0;

	AttributeNameSet attribute_list;

	switch(eResolveType )
	{
	case RESOLVE_LOCAL_REF:
		attribute_list = local_parameters;
		break;
	case RESOLVE_PARAM_LIST_REF:
		attribute_list = parameter_lists;
		break;
	case RESOLVE_PARAM_REF:
		attribute_list = parameters | local_parameters;
		break;
	case RESOLVE_BLOCK_REF:
		attribute_list = characteristics;
		break;
	default:
		return 0;	// Not a legal eResolveType, so no ITEM_ID could be looked up.
	}

	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_BLOCK, 0, 0};

	FDI_GENERIC_ITEM generic_item(ITYPE_BLOCK);

	// call to IEDDEngine GetItem function to get generic item for given item id
	nRetVal = m_pEDDEngineImpl->GetItem(iBlockInstance, pValueSpec, &item_spec, &attribute_list, lang_code, &generic_item);
	if (nRetVal == DDS_SUCCESS) 
	{
		FLAT_BLOCK *flat_block = dynamic_cast< FLAT_BLOCK *>(generic_item.item);

		if( flat_block )
		{
			MEMBER_LIST* member_list = nullptr;

			if(eResolveType == RESOLVE_BLOCK_REF)
			{
				attribute_list = members;
				FDI_GENERIC_ITEM generic_item2(ITYPE_RECORD);

				// call to GetItemBase function to get generic item for given item id
				nRetVal = GetItemBase(iBlockInstance, pValueSpec, flat_block->characteristic, 0, &attribute_list, &generic_item2, lang_code);

				if ((nRetVal == DDS_SUCCESS) && generic_item2.item)
				{
					FLAT_RECORD *flat_record = dynamic_cast< FLAT_RECORD *>(generic_item2.item);

					if( flat_record )
					{
						member_list = &flat_record->members;

						for(int i = 0; i < member_list->count; i++)
						{
							if(member_list->list[i].name == ulMemberId)
							{
								ulRetItemId = member_list->list[i].ref.id;
								break;
							}
						}
					}
				}
			}
			else
			{
				switch(eResolveType )
				{
				case RESOLVE_LOCAL_REF:
					member_list = &flat_block->local_param;
					break;
				case RESOLVE_PARAM_LIST_REF:
					member_list = &flat_block->param_lists;
					break;
				case RESOLVE_PARAM_REF:
					member_list = &flat_block->param;
					break;

				default:
					break;
				}

				bool bFound = false;
				for(int i = 0; i < member_list->count; i++)
				{
					if(member_list->list[i].name == ulMemberId)
					{
						ulRetItemId = member_list->list[i].ref.id;
						bFound = true;
						break;
					}
				}

				if ( !bFound && (eResolveType == RESOLVE_PARAM_REF) && ((m_pEDDEngineImpl->GetProtocol() == FF) || (m_pEDDEngineImpl->GetProtocol() == ISA100)))
				{
					//try local parameter again
					member_list = &flat_block->local_param;
					for(int i = 0; i < member_list->count; i++)
					{
						if(member_list->list[i].name == ulMemberId)
						{
							ulRetItemId = member_list->list[i].ref.id;
							break;
						}
					}
				}
			}
		}
	}

	return ulRetItemId;
}

//GetTypeReferenceID: Get List Type Reference ID
int CMiDataFinder::GetTypeReferenceID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = type_definition;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if ( itemKind == PT_DDLIST )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_LIST);

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_LIST *flat_list = dynamic_cast<FLAT_LIST *>(generic_item.item);
			if( flat_list )
			{
				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				CV_UI4(pvtValue) = flat_list->type;
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if ( itemKind == PT_ARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ARRAY *flat_arry = dynamic_cast<FLAT_ARRAY *>(generic_item.item);
			if( flat_arry )
			{
				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				CV_UI4(pvtValue) = flat_arry->type;
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}


//If conversion succeeds, this function returns true. Otherwise, return false.
bool CMiDataFinder::ConvertEXPRtoVARIANT(nsEDDEngine::EXPR *pSrc, CValueVarient *pvtDest)
{
	bool bRetVal = true;	//success

	switch( pSrc->eType )
	{
	case nsEDDEngine::EXPR::EXPR_TYPE_INTEGER:
		{
			switch (pSrc->size)
			{
			case 1:
				CV_VT(pvtDest, ValueTagType::CVT_I1);
				CV_I1(pvtDest) = (char)pSrc->val.i;
				break;
			case 2:
				CV_VT(pvtDest, ValueTagType::CVT_I2);
				CV_I2(pvtDest) = (short)pSrc->val.i;
				break;
			case 3:
			case 4:
				CV_VT(pvtDest, ValueTagType::CVT_I4);
				CV_I4(pvtDest) = (long)pSrc->val.i;
				break;
			default:
				CV_VT(pvtDest, ValueTagType::CVT_I8);
				CV_I8(pvtDest) = pSrc->val.i;
				break;
			}
		}
		break;
	case nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED:
		{
			switch (pSrc->size)
			{
			case 1:
				CV_VT(pvtDest, ValueTagType::CVT_UI1);
				CV_UI1(pvtDest) = (unsigned char)pSrc->val.u;
				break;
			case 2:
				CV_VT(pvtDest, ValueTagType::CVT_UI2);
				CV_UI2(pvtDest) = (unsigned short)pSrc->val.u;
				break;
			case 3:
			case 4:
				CV_VT(pvtDest, ValueTagType::CVT_UI4);
				CV_UI4(pvtDest) = (unsigned long)pSrc->val.u;
				break;
			default:
				CV_VT(pvtDest, ValueTagType::CVT_UI8);
				CV_UI8(pvtDest) = pSrc->val.u;
				break;
			}
		}
		break;
	case nsEDDEngine::EXPR::EXPR_TYPE_FLOAT:
		{
			CV_VT(pvtDest, ValueTagType::CVT_R4);
			CV_R4(pvtDest) = pSrc->val.f;
		}
		break;
	case nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE:
		{
			CV_VT(pvtDest, ValueTagType::CVT_R8);
			CV_R8(pvtDest) = pSrc->val.d;
		}
		break;
	case nsEDDEngine::EXPR::EXPR_TYPE_STRING:
		{
			CV_VT(pvtDest, ValueTagType::CVT_WSTR);
			CV_WSTR(pvtDest) = (wchar_t*)pSrc->val.s.c_str();
		}
		break;
	case nsEDDEngine::EXPR::EXPR_TYPE_BINARY:  
		{
			CV_VT(pvtDest, ValueTagType::CVT_BITSTR);
			CV_BITSTR(pvtDest, pSrc->val.b.ptr(), pSrc->val.b.length());		
		}
		break;
	default:
		bRetVal = false;	//something wrong
		break;
	}

	return bRetVal;
}


//GetScalingFactor : Get the scaling factor for the gievn item id.
int CMiDataFinder::GetScalingFactor( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	AttributeNameSet attribute_list = scaling_factor;

	// call to GetItemBase function to get generic item for given item id
	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		if ( !ConvertEXPRtoVARIANT(&flat_var->scaling_factor, pvtValue) )
		{
			nRetVal = DDL_BAD_VALUE_TYPE;
		}
	}

	return nRetVal;
}

int CMiDataFinder::GetItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	switch (itemKind)
	{
		case PT_VAR:
		{
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = ulItemID;
			nRetVal = DDS_SUCCESS;
		}
		break;
		case PT_IARRAY:
		case PT_COLLECT:
		{
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = ResolveRefToID( iBlockInstance, pValueSpec, ulItemID, ulMemberID, lang_code );
			nRetVal = DDS_SUCCESS;		
				
		}
		break;
		case PT_FILE:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_FILE);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_FILE* flat_file = dynamic_cast<FLAT_FILE *>(generic_item.item);

				for( int i = 0; i < flat_file->members.count; i++ )
				{
					if (flat_file->members.list[i].name == (unsigned long)ulMemberID)
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = flat_file->members.list[i].ref.id;
						break;
					}
				}
			}
		}
		break;
		case PT_REC:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_RECORD);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_RECORD* flat_rec = dynamic_cast<FLAT_RECORD *>(generic_item.item);

				for( int i = 0; i < flat_rec->members.count; i++ )
				{
					if (flat_rec->members.list[i].name == (unsigned long)ulMemberID)
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = flat_rec->members.list[i].ref.id;
						break;
					}
				}
			}
		}
		break;
		case PT_GRAPH:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_GRAPH);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_GRAPH* flat_graph = dynamic_cast<FLAT_GRAPH *>(generic_item.item);

				for( int i = 0; i < flat_graph->members.count; i++ )
				{
					if (flat_graph->members.list[i].name == (unsigned long)ulMemberID)
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = flat_graph->members.list[i].ref.id;
						break;
					}
				}
			}
		}
		break;
		case PT_CHART:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_CHART);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_CHART* flat_chart = dynamic_cast<FLAT_CHART *>(generic_item.item);

				for( int i = 0; i < flat_chart->members.count; i++ )
				{
					if (flat_chart->members.list[i].name == (unsigned long)ulMemberID)
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = flat_chart->members.list[i].ref.id;
						break;
					}
				}
			}
		}
		break;
		case PT_ARRAY:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_ARRAY);
			AttributeNameSet attribute_list = type_definition;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_ARRAY* flat_valArr = dynamic_cast<FLAT_ARRAY *>(generic_item.item);

				CV_VT(pvtValue, ValueTagType::CVT_UI4);
				CV_UI4(pvtValue) = flat_valArr->type;
			}
		}
		break;
		case PT_PLIST:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_VAR_LIST);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given file item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_VAR_LIST* flat_var_list = dynamic_cast<FLAT_VAR_LIST *>(generic_item.item);

				for( int i = 0; i < flat_var_list->members.count; i++ )
				{
					if (flat_var_list->members.list[i].name == (unsigned long)ulMemberID)
					{
						CV_VT(pvtValue, ValueTagType::CVT_UI4);
						CV_UI4(pvtValue) = flat_var_list->members.list[i].ref.id;
						break;
					}
				}
			}
		}
		break;
		case PT_SOURCE:
		{
			FDI_GENERIC_ITEM generic_item(ITYPE_SOURCE);
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given item id
			nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
			if ((nRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_SOURCE *flat_source = dynamic_cast<FLAT_SOURCE *>(generic_item.item);

				if( flat_source )
				{
					//look for an item ID that matches the member name
					for( int i = 0; i < flat_source->op_members.count; i++ )
					{
						if (flat_source->op_members.list[i].name == (unsigned long)ulMemberID)
						{
							CV_VT(pvtValue, ValueTagType::CVT_UI4);
							CV_UI4(pvtValue) = flat_source->op_members.list[i].ref.desc_id;
							break;
						}
					}
				}
			}
		}
		break;
		case PT_DDLIST:
		{
			nRetVal = GetTypeReferenceID( iBlockInstance, pValueSpec, ulItemID, pvtValue, lang_code );
		}
		break;
		default:
			if (itemKind < 0)
			{
				//error
				nRetVal = itemKind;
			}
			else
			{
				nRetVal = DDI_INVALID_ITEM_TYPE;
			}
			break;
	}

	if ((nRetVal == DDS_SUCCESS ) && (CV_UI4(pvtValue) == 0))
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}

int CMiDataFinder::GetEnumVarString(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *pvtValue, const wchar_t *lang_code)
{
	int nRetVal = VARIABLE_VALUE_NEEDED;


	long itemKind = GetKindFromID( iBlockInstance, ulItemId );
	if ( itemKind == PT_VAR )
	{
		AttributeNameSet attribute_list = enumerations;
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemId, 0, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			nRetVal = DDI_TAB_BAD_DDID;
			for( int i = 0; i < flat_var->enums.count; i++ )
			{
				if(flat_var->enums.list[i].val.val.u == ulEnumValue)
				{
					CV_VT(pvtValue, ValueTagType::CVT_WSTR);
					if (flat_var->enums.list[i].desc.length())
					{
						CV_WSTR(pvtValue) = flat_var->enums.list[i].desc.c_str();
					}
					else
					{
						CV_WSTR(pvtValue) = L"";
					}
					nRetVal = DDS_SUCCESS;
					break;
				}
			}
		}
		else
		{
			nRetVal = DDI_TAB_BAD_DDID;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetAxisScaling( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_AXIS )
	{
		pvtValue->clear();
		AttributeNameSet attribute_list = scaling;
		FDI_GENERIC_ITEM generic_item(ITYPE_AXIS);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_AXIS* flat_axis = dynamic_cast<FLAT_AXIS *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_I1);
			CV_I1(pvtValue) = (char)flat_axis->scaling;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetCapacity( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_DDLIST )
	{
		pvtValue->clear();
		AttributeNameSet attribute_list = capacity;
		FDI_GENERIC_ITEM generic_item(ITYPE_LIST);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_LIST *flat_list = dynamic_cast<FLAT_LIST *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = flat_list->capacity;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetIndexedValueList( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID /*ulMemberID*/, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = METH_INTERNAL_ERROR;
	long itemKind = 0;

	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	
	if( itemKind == PT_IARRAY )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_ITEM_ARRAY);
		AttributeNameSet attribute_list = elements;

		// call to GetItemBase function to get generic item for given item id
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, 0, &attribute_list, &generic_item, lang_code);
		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item.item);
			if( flat_item_arry )
			{
				CV_VT(pvtValue, ValueTagType::CVT_WSTR);
				wchar_t index_value[100]={0}; 
				wchar_t* buffer = NULL;
				size_t memSize = 100;
				size_t bufferSize = 0;
				wchar_t* combine_index = (wchar_t *) malloc(sizeof(wchar_t)*memSize);

				if (combine_index == NULL)
				{
					return DDI_MEMORY_ERROR;
				}

				combine_index[0] = L'\0';

				for (int i = 0; i < flat_item_arry->elements.count; i++)
				{
					//Index of the elements are concatenated into a single string
					PS_Itow(flat_item_arry->elements.list[i].index, index_value, 10);

					//add comma
					PS_Wcscat(index_value, 100, L",");
					size_t elementSize = wcslen (index_value);
					// -1 Added for null termination
					if ((memSize  - bufferSize -1) < (elementSize) )
					{
						buffer = (wchar_t*) malloc(sizeof(wchar_t)*memSize);
						PS_Wcscpy(buffer, memSize, combine_index);

						//re-allocate 100 more memory
						memSize += (sizeof(wchar_t)*100);
						combine_index = (wchar_t*) realloc(combine_index, sizeof(wchar_t)*memSize);

						if (combine_index == NULL)
						{
							//free original memory
							free (buffer);
							break;
						}

						//copy the original content to new memory locatoin
						PS_Wcscpy(combine_index, memSize, buffer);
						//free original memory
						free (buffer);
					}

					PS_Wcscat(combine_index, memSize, index_value );
					bufferSize = wcslen (combine_index);
				}

				//over-write terminal character to the last comma
				combine_index[bufferSize - 1] = L'\0';
				CV_WSTR(pvtValue) = combine_index;
				if (combine_index != NULL)
				{
					free(combine_index);
				}	
			}
			else
			{
				nRetVal = DDI_TAB_BAD_DDID;
			}
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetXAxisItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	long itemKind = 0;


	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_GRAPH )
	{
		pvtValue->clear();
		AttributeNameSet attribute_list = x_axis;
		FDI_GENERIC_ITEM generic_item(ITYPE_GRAPH);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_GRAPH *flat_graph = dynamic_cast<FLAT_GRAPH *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = flat_graph->x_axis;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetYAxisItemID( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = y_axis;
	long itemKind = 0;


	pvtValue->clear();
	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_SOURCE )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_SOURCE);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_SOURCE *flat_source = dynamic_cast<FLAT_SOURCE *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = flat_source->y_axis;
		}
	}
	else if( itemKind == PT_WAVE_FORM )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_WAVEFORM);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_WAVEFORM *flat_wf = dynamic_cast<FLAT_WAVEFORM *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_UI4);
			CV_UI4(pvtValue) = flat_wf->y_axis;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

int CMiDataFinder::GetLength( int iBlockInstance, void* pValueSpec, ITEM_ID ulItemID, ITEM_ID ulMemberID, CValueVarient *pvtValue, const wchar_t *lang_code )
{
	int nRetVal = VARIABLE_VALUE_NEEDED;
	AttributeNameSet attribute_list = length;
	long itemKind = 0;


	pvtValue->clear();
	itemKind = GetKindFromID( iBlockInstance, ulItemID );
	if( itemKind == PT_CHART )
	{
		FDI_GENERIC_ITEM generic_item(ITYPE_CHART);
		nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemID, ulMemberID, &attribute_list, &generic_item, lang_code);

		if ((nRetVal == DDS_SUCCESS) && generic_item.item)
		{
			FLAT_CHART *flat_chart = dynamic_cast<FLAT_CHART *>(generic_item.item);
			CV_VT(pvtValue, ValueTagType::CVT_UI8);
			CV_UI8(pvtValue) = flat_chart->length;
		}
	}
	else if (itemKind < 0)
	{
		//error
		nRetVal = itemKind;
	}

	return nRetVal;
}

void CMiDataFinder::convertOPRefTrailToParamSpec(const int iBlockInstance, const nsEDDEngine::OP_REF_TRAIL *pSrcValue, nsEDDEngine::FDI_PARAM_SPECIFIER *pDstValue)
{
	int lSubIndex = 0;

	switch (pSrcValue->op_ref_type)
	{
	case nsEDDEngine::STANDARD_TYPE:
		{
			if (pSrcValue->op_info.member > 0)
			{
				//find subindex if member id is not zero
				int ddsCode = ResolveSubindex ( iBlockInstance, pSrcValue->op_info.id, pSrcValue->op_info.member, &lSubIndex);

				//Adjust value array and list index to 0 based for HART and Profibus
				if (ddsCode == 0)
				{
					//find the item type
					long lKind = GetKindFromID( iBlockInstance, pSrcValue->op_info.id );
					if ( ((PT_ARRAY == lKind) || (PT_DDLIST == lKind)) && ((FF != m_pEDDEngineImpl->GetProtocol()) && (ISA100 != m_pEDDEngineImpl->GetProtocol())))
					{
						lSubIndex--;		//index 0 based
					}
				}

				pDstValue->eType = FDI_PS_ITEM_ID_SI;          //doesn't work if ulMemberId == 0
			}
			else
			{
				pDstValue->eType = FDI_PS_ITEM_ID;
			}

			pDstValue->id = pSrcValue->op_info.id;
			pDstValue->subindex = lSubIndex;
		}
		break;
	case nsEDDEngine::COMPLEX_TYPE:
		{
			pDstValue->eType = FDI_PS_ITEM_ID_COMPLEX;

			pDstValue->RefList.count = pSrcValue->op_info_list.count;

			if (pDstValue->RefList.count > 0)
			{
				pDstValue->RefList.list = new OP_REF_INFO[pDstValue->RefList.count];
				for (int i = 0; i < pDstValue->RefList.count; i++)
				{
					pDstValue->RefList.list[i].id = pSrcValue->op_info_list.list[i].id;

					//Adjust value array and list index to 0 based for HART and Profibus
					//find the item type
					long lKind = GetKindFromID( iBlockInstance, pSrcValue->op_info_list.list[i].id );
					if ( ((PT_ARRAY == lKind) || (PT_DDLIST == lKind)) && ((FF != m_pEDDEngineImpl->GetProtocol()) && (ISA100 != m_pEDDEngineImpl->GetProtocol())))
					{
						lSubIndex = pSrcValue->op_info_list.list[i].member - 1;		//index 0 based
					}
					else
					{
						lSubIndex = pSrcValue->op_info_list.list[i].member;
					}
					pDstValue->RefList.list[i].member = lSubIndex;

					pSrcValue->op_info_list.list[i].type = pDstValue->RefList.list[i].type;
				}
			}
		}
		break;
	}

}

int CMiDataFinder::GetActionList( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, nsEDDEngine::ACTION_LIST *pOutput, const wchar_t *lang_code)
{
	AttributeNameSet attribute_list = refresh_actions;
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);

	int nRetVal = GetItemBase(iBlockInstance, pValueSpec, ulItemId, 0, &attribute_list, &generic_item, lang_code);

	if ((nRetVal == DDS_SUCCESS) && generic_item.item)
	{
		FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

		*pOutput = flat_var->refresh_act;
	}
	else
	{
		nRetVal = DDI_TAB_BAD_DDID;
	}

	return nRetVal;
}


/***********************************************************************
 *
 *	Name:  GetItemsBetweenReferenceItems
 *
 *	Description:
 *	Get item type array, item ID array, item member ID array, number of array elements
 *	which are reference items between a starting item and a ending item.
 *  The output array element items from array index 0 to the top are ones 
 *  from the closest reference to the ending item to the furthest reference to the ending item.
 *  The top output array element item is actually referenced by the beginning item.
 *	This function is used for FF.
 *
 *	Inputs:
 *			iBlockInstance:		Block instance
 *			pValueSpec:			Value specifier
 *			lang_code:			language code
 *			ulRefItemId			The ending item ID
 *			usMaxArrayElem:		Maxmium of item array elements, i.e. array size
 *
 *  Outputs:
 *			peItemType[]:		Item type array. But the given first element is the starting item
 *			pulItemId[]:		Item ID array. But the given first element is the starting item
 *			pulMemberId[]:		Item member ID array
 *			pArrayElemNum:		Actual number of array elements
 *
 *	Returns:
 *			SUCCESS on success. VARIABLE_VALUE_NEEDED, DDS_OUT_OF_MEMORY, ENUM_NOT_FOUND on failure.
 *
 *  Example:
 *		resolving reference: "fl_input_collection.IN_COL_COL.BLK_COL_REC"
 *		inputs: 
 *					ulRefItemId is item ID of "dl_input_rec".
 *					usMaxArrayElem is 10
 *					peItemType[0] is COLLECTION.
 *					pulItemId[0] is item ID of "fl_input_collection".
 *		outputs: 
 *					peItemType[0] is COLLECTION
 *					pulItemId[0] is item ID of "fl_blk_parm_collection"
 *					pulMemberId[0] is member ID of "BLK_COL_REC"
 *					pulMemberId[1] is member ID of "IN_COL_COL"
 *					pArrayElemNum is 2
 *
 **********************************************************************/

int CMiDataFinder::GetItemsBetweenReferenceItems( /*in*/nsEDDEngine::BLOCK_INSTANCE iBlockInstance, /*in*/void* pValueSpec,  /*in*/const wchar_t *lang_code,
												  /*in*/ITEM_ID ulRefItemId,
												  /*in*/const unsigned short usMaxArrayElem,
												  /*in, out*/nsEDDEngine::ITEM_TYPE peItemType[], /*in, out*/ITEM_ID pulItemId[], /*in, out*/ITEM_ID pulMemberId[], 
												  /*out*/unsigned short* pArrayElemNum )
{
	int iRetVal = VARIABLE_VALUE_NEEDED;
	bool bFound = false;

	FDI_GENERIC_ITEM generic_item(peItemType[*pArrayElemNum]);
	switch(peItemType[*pArrayElemNum])
	{
	case ITYPE_COLLECTION:
		{
			AttributeNameSet attribute_list = members;

			// call to GetItemBase function to get generic item for given item id
			iRetVal = GetItemBase(iBlockInstance, pValueSpec, pulItemId[*pArrayElemNum], 0, &attribute_list, &generic_item, lang_code);
			if ((iRetVal == DDS_SUCCESS) && generic_item.item)
			{
				FLAT_COLLECTION *flat_collect = dynamic_cast<FLAT_COLLECTION *>(generic_item.item);

				if( flat_collect )
				{
					//look for an item ID that matches the member name
					if (flat_collect->op_members.count > 0)
					{
						for( int i = 0; i < flat_collect->op_members.count; i++ )
						{
							if (flat_collect->op_members.list[i].ref.op_info.id == ulRefItemId)
							{
								pulMemberId[*pArrayElemNum] = flat_collect->op_members.list[i].name;
								bFound = true;
								*pArrayElemNum += 1;
								if (*pArrayElemNum >= usMaxArrayElem)
								{
									iRetVal = DDS_OUT_OF_MEMORY;
								}
								break;
							}
						}

						if (!bFound)
						{
							//item ID is not found yet. now look for an item ID that matches the member name in next level
							for( int i = 0; i < flat_collect->op_members.count; i++ )
							{
								//pre-set upper level type and item ID
								peItemType[*pArrayElemNum] = flat_collect->op_members.list[i].ref.op_info.type;
								pulItemId[*pArrayElemNum] = flat_collect->op_members.list[i].ref.op_info.id;
								//upper level member ID will be set in the following function call
								iRetVal = GetItemsBetweenReferenceItems(iBlockInstance, pValueSpec, lang_code,
												ulRefItemId, 
												usMaxArrayElem,
												&(peItemType[*pArrayElemNum]), &(pulItemId[*pArrayElemNum]), &(pulMemberId[*pArrayElemNum]),
												pArrayElemNum);
								if (iRetVal == DDS_SUCCESS)
								{
									//Now set current level member ID
									pulMemberId[*pArrayElemNum] = flat_collect->op_members.list[i].name;
									bFound = true;
									*pArrayElemNum += 1;
									if (*pArrayElemNum >= usMaxArrayElem)
									{
										iRetVal = DDS_OUT_OF_MEMORY;
									}
									break;
								}
							}
						}
					}
				}
			}
		}
		break;
	default:
		break;
	}

	if (!bFound)
	{
		iRetVal = ENUM_NOT_FOUND;
	}

	return iRetVal;
}


//The following section is #defines for FF device parameter errorType and functions to handle FF device error code associated string
//error class. see FF-870 (Fieldbus Message Spec), section 6.1 "Parameter Error Type" and 6.1.2 "Meaning of Error Class and Error Code"
#define	DEV_VFD_ST								1
#define	DEV_APP_REF								2
#define	DEV_DEF									3
#define	DEV_RSRC								4
#define	DEV_SERV								5
#define	DEV_ACCS								6
#define	DEV_OD									7
#define	DEV_OTHER								8	//FUNCTION BLOCK SERVICE ERROR

//error code. see FF-870 (Fieldbus Message Spec), section 10.1.4.4 "Error Class"
#define DEV_VFD_STATE_OTHER						0

#define DEV_APP_REF_OTHER						0
#define DEV_APP_R_A_UNREACH						1

#define DEV_DEF_OTHER							0
#define DEV_DEF_OBJ_UNDEF						1
#define DEV_DEF_OBJ_ATTR_INCST					2
#define DEV_DEF_NAME_A_EXIST					3

#define DEV_RSRC_OTHER							0
#define DEV_RSRC_MEM_UNAVAIL     				1

#define DEV_SERV_OTHER							0
#define DEV_SERV_OBJ_ST_CONFL					1
#define DEV_SERV_PDU_SIZE						2
#define DEV_SERV_OBJ_CONS_CONFL					3
#define DEV_SERV_PARAM_INCST					4
#define DEV_SERV_ILL_PARAM						5

#define DEV_ACCS_OTHER							0
#define DEV_ACCS_OBJ_INV						1
#define DEV_ACCS_HW_FAULT         				2
#define DEV_ACCS_OBJ_ACCS_D						3
#define DEV_ACCS_INV_ADDR						4
#define DEV_ACCS_OBJ_ATTR_INCST 				5
#define DEV_ACCS_OBJ_ACCS_UNS		 			6
#define DEV_ACCS_OBJ_N_EXIST      				7
#define DEV_ACCS_TYPE_CONFL						8
#define DEV_ACCS_NM_ACCS_UNS			 		9
#define DEV_ACCS_TO_ELEM_UNS			 		10

#define DEV_OD_OTHER							0
#define DEV_OD_NM_LEN_OVERFLOW    				1
#define DEV_OD_OVERFLOW                 		2
#define DEV_OD_WRITE_PROT      					3
#define DEV_OD_EXT_LEN_OVERFLOW			 		4
#define DEV_OD_DESC_LEN_OVERFLOW 				5
#define DEV_OD_OPER_PROB          				6

#define DEV_OTHER_OTHER							0

//additional code for function block service error
//See FF-890 (Function Block Part 1), Section 4.14.2 "Function Block Services and Protocol", Table 23 "Service Error Reason Codes"
#define FB_S_E_OTHER							0
#define FB_S_PARAM_CHK							1
#define FB_S_E_P_LIMIT 							2
#define FB_S_W_M_REQ		 					3
#define FB_S_WR_PROH	    					4
#define FB_S_D_V_N_WR			 				5
#define FB_S_INS_N_SUP		 					6
#define FB_S_DE_N_SUP	    					7
#define FB_S_ALM_ACK			 				8
//See FF-902 (Transducer Block Common Structures), Section 4.6 "Reason Codes", Table 2 "Reason Codes"
#define FB_S_DLT_V_SET      					16
#define FB_S_S_2_N_VAL	    					17
#define FB_S_P_N_IMPL 							18
#define FB_S_INC_FORM          					19
#define FB_S_INV_DATE							20
#define FB_S_P_C_N_PERM			 				21
#define FB_S_PROC_O_O_R      					22
#define FB_S_A_Z_FAIL		 					23
#define FB_S_DEV_BUSY          					24
#define FB_S_UPD_FAIL          					25
#define FB_S_INV_CONF   						26
#define FB_S_EXC_CORR         					27
#define FB_S_CAL_FAIL			 				28
#define FB_S_CAL_PROG  							29

//device error code is a composite of error information
//  The following error information is returned from a FB device
//  MSB = Error Class - for more info see FMS FF-870 spec 10.1.4.4
//  2nd MSB = Error Code - for more info see FMS FF-870 spec 10.1.4.4
//  2nd LSB = MSB of Additional Code - for more info see FMS FF-890 spec table 23 and FF-902 table 2
//  LSB = LSB of Additional Code

#define ERRCD(error_class, error_code, addl_code) ( (error_class << 24) | (error_code << 16) | addl_code )

void CMiDataFinder::loadFFDeviceErrorMap()
{
	//initialize m_mDevErrCodeString variable
	m_mDevErrCodeString[ERRCD(DEV_VFD_ST, DEV_VFD_STATE_OTHER, 0)]			= _T("|en|Device VFD Error: Other Error");

	m_mDevErrCodeString[ERRCD(DEV_APP_REF, DEV_APP_REF_OTHER, 0)]			= _T("|en|Device App Ref Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_APP_REF, DEV_APP_R_A_UNREACH, 0)]			= _T("|en|Device App Ref Error: Application Unreachable");

	m_mDevErrCodeString[ERRCD(DEV_DEF, DEV_DEF_OTHER, 0)]					= _T("|en|Device Definition Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_DEF, DEV_DEF_OBJ_UNDEF, 0)]				= _T("|en|Device Definition Error: Object Undefined");
	m_mDevErrCodeString[ERRCD(DEV_DEF, DEV_DEF_OBJ_ATTR_INCST, 0)]			= _T("|en|Device Definition Error: Inconsistent Object Attributes");
	m_mDevErrCodeString[ERRCD(DEV_DEF, DEV_DEF_NAME_A_EXIST, 0)]			= _T("|en|Device Definition Error: Name Already Exists");

	m_mDevErrCodeString[ERRCD(DEV_RSRC, DEV_RSRC_OTHER, 0)]					= _T("|en|Device Resource Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_RSRC, DEV_RSRC_MEM_UNAVAIL, 0)]			= _T("|en|Device Resource Error: Memory Unavailable");

	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_OTHER, 0)]					= _T("|en|Device Service Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_OBJ_ST_CONFL, 0)]			= _T("|en|Device Service Error: Object/State Conflict");
	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_PDU_SIZE, 0)]				= _T("|en|Device Service Error: PDU Size Error");
	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_OBJ_CONS_CONFL, 0)]		= _T("|en|Device Service Error: Object Constraint Conflict");
	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_PARAM_INCST, 0)]			= _T("|en|Device Service Error: Inconsistent Parameter");
	m_mDevErrCodeString[ERRCD(DEV_SERV, DEV_SERV_ILL_PARAM, 0)]				= _T("|en|Device Service Error: Illegal Parameter");

	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OTHER, 0)]					= _T("|en|Device Access Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OBJ_INV, 0)]				= _T("|en|Device Access Error: Object Invalidated");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_HW_FAULT, 0)]				= _T("|en|Device Access Error: Hardware Fault");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OBJ_ACCS_D, 0)]			= _T("|en|Device Access Error: Object Access Denied");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_INV_ADDR, 0)]				= _T("|en|Device Access Error: Invalid Address");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OBJ_ATTR_INCST, 0)]		= _T("|en|Device Access Error: Object Attribute Inconsistent");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OBJ_ACCS_UNS, 0)]			= _T("|en|Device Access Error: Object Access Unsupported");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_OBJ_N_EXIST, 0)]			= _T("|en|Device Access Error: Object Non-Existant");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_TYPE_CONFL, 0)]			= _T("|en|Device Access Error: Type Conflict");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_NM_ACCS_UNS, 0)]			= _T("|en|Device Access Error: Named Access Unsupported");
	m_mDevErrCodeString[ERRCD(DEV_ACCS, DEV_ACCS_TO_ELEM_UNS, 0)]			= _T("|en|Device Access Error: Access To Element Unsupported");

	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_OTHER, 0)]						= _T("|en|Device OD Error: Other Error");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_NM_LEN_OVERFLOW, 0)]			= _T("|en|Device OD Error: Name Length Overflow");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_OVERFLOW, 0)]					= _T("|en|Device OD Error: OD Overflow");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_WRITE_PROT, 0)]				= _T("|en|Device OD Error: OD Write Protected");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_EXT_LEN_OVERFLOW, 0)]			= _T("|en|Device OD Error: Extension Length Overflow");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_DESC_LEN_OVERFLOW, 0)]			= _T("|en|Device OD Error: OD Description Length Overflow");
	m_mDevErrCodeString[ERRCD(DEV_OD, DEV_OD_OPER_PROB, 0)]					= _T("|en|Device OD Error: Operational Problem");

	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_E_OTHER)]	= _T("|en|Device Other Error: Other Function Block Service Error");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_PARAM_CHK)]	= _T("|en|Function Block Service Error: Parameter Check");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_E_P_LIMIT)]	= _T("|en|Function Block Service Error: Exceeds Parameter Limits");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_W_M_REQ)]	= _T("|en|Function Block Service Error: Wrong Mode for Request");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_WR_PROH)]	= _T("|en|Function Block Service Error: Write is Prohibited by Write Lock Switch");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_D_V_N_WR)]	= _T("|en|Function Block Service Error: Data Value is Never Writable");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_INS_N_SUP)]	= _T("|en|Function Block Service Error: Instantiate Not Supported/Unsuccessful");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_DE_N_SUP)]	= _T("|en|Function Block Service Error: Delete Not Supported/Unsuccessful");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_ALM_ACK)]	= _T("|en|Function Block Service Error: Alarm Already Acknowledged");

	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_DLT_V_SET)]	= _T("|en|Function Block Service Error: Default Value Set");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_S_2_N_VAL)]	= _T("|en|Function Block Service Error: Set to Nearest Possible Value");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_P_N_IMPL)]	= _T("|en|Function Block Service Error: Parameter Not Implemented");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_INC_FORM)]	= _T("|en|Function Block Service Error: Incorrect Format");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_INV_DATE)]	= _T("|en|Function Block Service Error: Invalid Date");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_P_C_N_PERM)] = _T("|en|Function Block Service Error: Parameter Change Not Permitted When AI Block is in This Mode");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_PROC_O_O_R)]	= _T("|en|Function Block Service Error: Applied Process Out Of Range");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_A_Z_FAIL)]	= _T("|en|Function Block Service Error: Automatic Zero Failure");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_DEV_BUSY)]	= _T("|en|Function Block Service Error: Device Busy");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_UPD_FAIL)]	= _T("|en|Function Block Service Error: Update Failed");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_INV_CONF)]	= _T("|en|Function Block Service Error: Invalid Configuration Request");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_EXC_CORR)]	= _T("|en|Function Block Service Error: Excess Correction");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_CAL_FAIL)]	= _T("|en|Function Block Service Error: Calibration Failed");
	m_mDevErrCodeString[ERRCD(DEV_OTHER, DEV_OTHER_OTHER, FB_S_CAL_PROG)]	= _T("|en|Function Block Service Error: Calibration In Progress");

}

int CMiDataFinder::GetFFDeviceCodeString(unsigned long ulPackedCode, CValueVarient *pvtValue, const wchar_t *lang_code)
{
	int nRetVal = DDL_DEV_SPEC_STRING_NOT_FOUND;

	if ((m_pEDDEngineImpl->GetProtocol() != FF) && (m_pEDDEngineImpl->GetProtocol() != ISA100))
	{
		return nRetVal;
	}

	// assign response code string associated with given response code.
	DEVICE_ERROR_MAP::iterator stringIter = m_mDevErrCodeString.find(ulPackedCode);
	if (stringIter != m_mDevErrCodeString.end())
	{
		//string translation per language
		size_t stringSize = wcslen(stringIter->second);
		wchar_t *errString = new wchar_t[stringSize+1];
		m_pEDDEngineImpl->GetStringTranslation(stringIter->second, lang_code, errString, (int)stringSize+1);

		CV_VT(pvtValue, ValueTagType::CVT_WSTR);
		CV_WSTR(pvtValue) = errString;
		delete [] errString;
		nRetVal = DDS_SUCCESS;
	}

	return nRetVal;
}
