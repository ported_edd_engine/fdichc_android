#ifndef lint
static char SCCSID[] = "@(#)cmtblacc.c	30.5  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cmtblacc.c - Connection Manager Tables Module
 *
 *	This file contains all access functions pertaining to the Connection
 *	(Active) Tables.
 */
#include "stdinc.h"

#include "cm_lib.h"

#include "panic.h"
#include "malloc.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"


int CConnectionMgr::set_abt_tag(BLOCK_HANDLE bh, char *tag)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	if( strlen( active_blk_tbl.list[bh]->tag) < strlen(tag) ) {
		active_blk_tbl.list[bh]->tag = (char*)realloc(active_blk_tbl.list[bh]->tag, strlen(tag) +1);
		if ( active_blk_tbl.list[bh]->tag == NULL ) return FAILURE;
	}

	(void)strcpy(active_blk_tbl.list[bh]->tag, tag) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_op_index(BLOCK_HANDLE bh, OBJECT_INDEX *oi)
{

	DEVICE_HANDLE	device_handle ;
	NET_TYPE		net_type ;
	BOOLEAN			bCheck;

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	device_handle = ABT_ADT_OFFSET(bh) ;

	ASSERT_RET(valid_device_handle(device_handle), CM_BAD_DEVICE_HANDLE) ;
	get_adt_net_type(device_handle, &net_type);
	//net_type = ADT_NET_TYPE(device_handle) ;
	bCheck = ((net_type == NT_OFF_LINE) 
		|| (net_type == NT_A) || (net_type == NT_A_SIM)
		);

	ASSERT_RET(bCheck, CM_NO_COMM) ;
	ASSERT_RET(oi, CM_BAD_POINTER) ;

	*oi = active_blk_tbl.list[bh]->op_index ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_op_index(BLOCK_HANDLE bh, OBJECT_INDEX oi)
{

	DEVICE_HANDLE	device_handle ;
	NET_TYPE		net_type ;
	BOOLEAN			bCheck;

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	device_handle = ABT_ADT_OFFSET(bh) ;
	get_adt_net_type(device_handle, &net_type);
	//net_type = ADT_NET_TYPE(device_handle) ;

	bCheck = ((net_type == NT_OFF_LINE) 
		|| (net_type == NT_A) || (net_type == NT_A_SIM)
		);

	active_blk_tbl.list[bh]->op_index = oi ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_param_count(BLOCK_HANDLE bh, unsigned int *pc)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(pc, CM_BAD_POINTER) ;
	*pc = active_blk_tbl.list[bh]->param_count ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_param_count(BLOCK_HANDLE bh, unsigned int pc)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->param_count = pc ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_blk_id(BLOCK_HANDLE bh, ITEM_ID *dbi)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dbi, CM_BAD_POINTER) ;
	*dbi = active_blk_tbl.list[bh]->dd_blk_id ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_blk_id (BLOCK_HANDLE bh, UINT32 dbi)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->dd_blk_id = dbi ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_blk_tbl_offset(BLOCK_HANDLE bh, int *dbto)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dbto, CM_BAD_POINTER) ;
	*dbto = active_blk_tbl.list[bh]->dd_blk_tbl_offset ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_blk_tbl_offset(BLOCK_HANDLE bh, int dbto)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto ;
	return(CM_SUCCESS) ;
}



int CConnectionMgr::get_abt_app_info(BLOCK_HANDLE bh, void **ai)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(ai, CM_BAD_POINTER) ;
	*ai = active_blk_tbl.list[bh]->app_info ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_app_info(BLOCK_HANDLE bh, void *ai)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->app_info = ai ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_adt_offset(BLOCK_HANDLE bh, DEVICE_HANDLE *dh)
{
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dh, CM_BAD_POINTER) ;
	*dh = active_blk_tbl.list[bh]->active_dev_tbl_offset ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_adt_offset(BLOCK_HANDLE bh, DEVICE_HANDLE adto)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->active_dev_tbl_offset = adto ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE *dth)
{

	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(dth, CM_BAD_POINTER) ;
	device_handle = ABT_ADT_OFFSET(bh) ;
	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE) ;
	}
	*dth = ADT_ADTT_OFFSET(device_handle) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto)
{

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	SET_ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh),adtto) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_device_id(BLOCK_HANDLE bh, DD_DEVICE_ID *ddi) 
{

	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(ddi, CM_BAD_POINTER) ;
	device_handle = ABT_ADT_OFFSET(bh) ;
	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE) ;
	}
	(void)memcpy(ddi, ADT_DD_DEVICE_ID(device_handle), sizeof(DD_DEVICE_ID)) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_device_id(BLOCK_HANDLE bh, DD_DEVICE_ID *ddi) 
{

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	SET_ADT_DD_DEVICE_ID(ABT_ADT_OFFSET(bh), ddi) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_handle(BLOCK_HANDLE bh, ROD_HANDLE *ddh)
{

	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(ddh, CM_BAD_POINTER) ;
	device_handle = ABT_ADT_OFFSET(bh) ;
	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	*ddh = ADT_DD_HANDLE(device_handle);
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_handle(BLOCK_HANDLE bh, ROD_HANDLE ddh) 
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	SET_ADT_DD_HANDLE(ABT_ADT_OFFSET(bh),ddh) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dd_dev_tbls(BLOCK_HANDLE bh, void **ddt)
{

	DEVICE_HANDLE	device_handle ;
	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	device_handle = ABT_ADT_OFFSET(bh) ;
	if (!valid_device_handle(device_handle)) { 
			return(CM_BAD_DEVICE_HANDLE) ;
	}
	*ddt = ADT_DD_DEV_TBLS(device_handle) ;	
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dd_dev_tbls(BLOCK_HANDLE bh, void *ddt) 
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	g_DeviceTypeMgr.set_adtt_dd_dev_tbls(ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh)),ddt);
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_dev_family(BLOCK_HANDLE bh, int *df)
{

	DEVICE_HANDLE	device_handle ;

	if (!valid_block_handle(bh)) { 
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(df, CM_BAD_POINTER) ;
	device_handle = ABT_ADT_OFFSET(bh) ;
	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	get_nt_dev_family(ADT_NETWORK(device_handle), df);
	//*df = ADT_DEV_FAMILY(device_handle) ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_dev_family(BLOCK_HANDLE bh, int df)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	set_nt_dev_family(ADT_NETWORK(ABT_ADT_OFFSET(bh)),df);
	//SET_ADT_DEV_FAMILY(ABT_ADT_OFFSET(bh),df) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_count()
{
	return(active_dev_tbl.count) ;
}


int CConnectionMgr::get_adt_station_address(DEVICE_HANDLE dh, UINT16 *sa)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(sa, CM_BAD_POINTER) ;
	*sa = active_dev_tbl.list[dh]->station_address ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_station_address(DEVICE_HANDLE dh, UINT16 sa)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->station_address = sa ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_a_opod_cref(DEVICE_HANDLE dh, CREF *oo)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;
	ASSERT_RET(oo, CM_BAD_POINTER) ;

	*oo = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_a_opod_cref(DEVICE_HANDLE dh, CREF oc)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;

	active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.opod_cref = oc ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_a_ddod_cref(DEVICE_HANDLE dh, CREF *ddo)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;
	ASSERT_RET(ddo, CM_BAD_POINTER) ;
	*ddo = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_a_ddod_cref(DEVICE_HANDLE dh, CREF dc)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;

	active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.ddod_cref = dc ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_a_mib_cref(DEVICE_HANDLE dh, CREF *mi)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;
	ASSERT_RET(mi, CM_BAD_POINTER) ;

	*mi = active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_a_mib_cref(DEVICE_HANDLE dh, CREF mc)
{

	NET_TYPE		net_type ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}

	get_adt_net_type(dh, &net_type);
	//net_type = ADT_NET_TYPE(dh) ;

	ASSERT_RET((net_type == NT_A) || (net_type == NT_A_SIM) ||
			(net_type == NT_OFF_LINE), CM_NO_COMM) ;

	active_dev_tbl.list[dh]->adt_net_spec.adt_spec_a.mib_cref = mc ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_dd_device_id(DEVICE_HANDLE dh, DD_DEVICE_ID *ddi)
{

	DEVICE_TYPE_HANDLE	device_type_handle ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(ddi, CM_BAD_POINTER) ;
	device_type_handle = ADT_ADTT_OFFSET(dh) ;
	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) { 
			return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	(void)memcpy((char *)ddi,(char *)g_DeviceTypeMgr.ADTT_DD_DEVICE_ID(device_type_handle),
			sizeof(DD_DEVICE_ID)) ;
	return(CM_SUCCESS) ;
}

int CConnectionMgr::set_adt_dd_device_id(DEVICE_HANDLE dh, DD_DEVICE_ID *ddi)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	g_DeviceTypeMgr.SET_ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh),ddi) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_dd_handle(DEVICE_HANDLE dh, ROD_HANDLE *ddh)
{

	DEVICE_TYPE_HANDLE	device_type_handle ;

	if (!valid_device_handle(dh))
	{
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(ddh, CM_BAD_POINTER) ;
	device_type_handle = ADT_ADTT_OFFSET(dh) ;
	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) 
	{ 
			return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}

	*ddh = g_DeviceTypeMgr.ADTT_DD_HANDLE(device_type_handle);

	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_dd_handle(DEVICE_HANDLE dh, ROD_HANDLE ddh)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	g_DeviceTypeMgr.SET_ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh),ddh) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_dd_dev_tbls(DEVICE_HANDLE dh, void **ddt)
{

	DEVICE_TYPE_HANDLE	device_type_handle ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(ddt, CM_BAD_POINTER) ;
	device_type_handle = ADT_ADTT_OFFSET(dh) ;
	*ddt = g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(device_type_handle) ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_dd_dev_tbls(DEVICE_HANDLE dh, void *ddt)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	g_DeviceTypeMgr.set_adtt_dd_dev_tbls(ADT_ADTT_OFFSET(dh), ddt) ;
	//SET_ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh), ddt) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_net_type(DEVICE_HANDLE dh, NET_TYPE *nt)
{

	NETWORK_HANDLE network_handle ;

	if (!valid_device_handle(dh)) { 
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(nt, CM_BAD_POINTER) ;
	network_handle = ADT_NETWORK(dh) ;
	if (!valid_network_handle(network_handle)) { 
			return(CM_BAD_NETWORK_HANDLE) ;
	}
	get_nt_net_type(network_handle, nt);
	//*nt = NT_NET_TYPE(network_handle) ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_net_type(DEVICE_HANDLE dh, NET_TYPE nt)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	set_nt_net_type(ADT_NETWORK(dh),nt) ;
	//SET_NT_NET_TYPE(ADT_NETWORK(dh),nt) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_net_route(DEVICE_HANDLE dh, NET_ROUTE *nr)
{

	NETWORK_HANDLE	network_handle ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(nr, CM_BAD_POINTER) ;
	network_handle =  ADT_NETWORK(dh) ;
	if (!valid_network_handle(network_handle)) { 
			return(CM_BAD_NETWORK_HANDLE) ;
	}
	get_nt_net_route(network_handle, nr);
	/*(void)memcpy((char *)nr, (char *)NT_NET_ROUTE(network_handle),
		sizeof(NET_ROUTE)) ;*/
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_net_route(DEVICE_HANDLE dh, NET_ROUTE *nr)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	set_nt_net_route(ADT_NETWORK(dh), nr) ;
	//SET_NT_NET_ROUTE(ADT_NETWORK(dh), nr) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_dev_family(DEVICE_HANDLE dh, int *df)
{

	NETWORK_HANDLE	network_handle ;

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(df, CM_BAD_POINTER) ;
	network_handle = ADT_NETWORK(dh) ;
	if (!valid_network_handle(network_handle)) { 
			return(CM_BAD_NETWORK_HANDLE) ;
	}
	get_nt_dev_family(network_handle, df);
	//*df = NT_DEV_FAMILY(network_handle) ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_dev_family(DEVICE_HANDLE dh, int df)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	set_nt_dev_family(ADT_NETWORK(dh), df) ;
	//SET_NT_DEV_FAMILY(ADT_NETWORK(dh), df) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_abt_usage(BLOCK_HANDLE bh, int *usage)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(usage, CM_BAD_POINTER) ;
	*usage = active_blk_tbl.list[bh]->usage ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_abt_usage(BLOCK_HANDLE bh, int usage)
{

	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	active_blk_tbl.list[bh]->usage = usage ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_network(DEVICE_HANDLE dh, NETWORK_HANDLE *nh)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(nh, CM_BAD_POINTER) ;
	*nh = active_dev_tbl.list[dh]->network ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_network(DEVICE_HANDLE dh, NETWORK_HANDLE nh)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->network = nh ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_oprod_handle(DEVICE_HANDLE dh, ROD_HANDLE *rh)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(rh, CM_BAD_POINTER) ;
	*rh = active_dev_tbl.list[dh]->oprod_handle ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_oprod_handle(DEVICE_HANDLE dh, ROD_HANDLE oh)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->oprod_handle = oh ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_app_info(DEVICE_HANDLE dh, void **ai)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(ai, CM_BAD_POINTER) ;
	*ai = active_dev_tbl.list[dh]->app_info ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_app_info(DEVICE_HANDLE dh, void *ai)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->app_info = ai ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_adtt_offset(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE *dth)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(dth, CM_BAD_POINTER) ;
	*dth = active_dev_tbl.list[dh]->active_dev_type_tbl_offset ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_adtt_offset(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_adt_usage(DEVICE_HANDLE dh, int *usage)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	ASSERT_RET(usage, CM_BAD_POINTER) ;
	*usage = active_dev_tbl.list[dh]->usage ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_adt_usage(DEVICE_HANDLE dh, int usage)
{

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
	active_dev_tbl.list[dh]->usage = usage ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_count ()
{
	return(active_dev_type_tbl.count) ;
}


int CDeviceTypeMgr::get_adtt_dd_device_id(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi)
{

	if (!valid_device_type_handle(dth)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}
	ASSERT_RET(ddi, CM_BAD_POINTER) ;
	(void)memcpy((char *)ddi, 
			(char *)&active_dev_type_tbl.list[dth]->dd_device_id, 
			sizeof(DD_DEVICE_ID)) ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_dd_device_id(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	(void)memcpy((char *)&(active_dev_type_tbl.list[dth]->dd_device_id), 
			(char *)ddi,sizeof(DD_DEVICE_ID)) ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_dd_handle(DEVICE_TYPE_HANDLE dth, ROD_HANDLE *ddh)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(ddh, CM_BAD_POINTER) ;

	*ddh = active_dev_type_tbl.list[dth]->rod_handle;

	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_dd_handle(DEVICE_TYPE_HANDLE dth, ROD_HANDLE rod_handle)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->rod_handle = rod_handle;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_dd_dev_tbls(DEVICE_TYPE_HANDLE dth, void **ddt)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(ddt, CM_BAD_POINTER) ;
	*ddt = active_dev_type_tbl.list[dth]->dd_dev_tbls ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_dd_dev_tbls(DEVICE_TYPE_HANDLE dth, void *ddt)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->dd_dev_tbls = ddt ;
/*
	printf("0x%08X\n", ddt);
*/
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_sym_info(DEVICE_TYPE_HANDLE dth, SYMINFO **si)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(si, CM_BAD_POINTER) ;
	*si =  active_dev_type_tbl.list[dth]->syminfo ;
	return (CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_sym_info(DEVICE_TYPE_HANDLE dth, SYMINFO *si)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->syminfo = si ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::get_adtt_usage(DEVICE_TYPE_HANDLE dth, int *usage)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	ASSERT_RET(usage, CM_BAD_POINTER) ;
	*usage = active_dev_type_tbl.list[dth]->usage ;
	return(CM_SUCCESS) ;
}


int CDeviceTypeMgr::set_adtt_usage(DEVICE_TYPE_HANDLE dth, int usage)
{

	if (!valid_device_type_handle(dth)) { 
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}
	active_dev_type_tbl.list[dth]->usage = usage ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_nt_net_type(NETWORK_HANDLE nh, NET_TYPE *nt)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	ASSERT_RET(nt, CM_BAD_POINTER) ;
	*nt = network_tbl.list[nh].network_type ;
	return (CM_SUCCESS) ;
}


int CConnectionMgr::set_nt_net_type(NETWORK_HANDLE nh, NET_TYPE nt)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	network_tbl.list[nh].network_type = nt ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_nt_net_route(NETWORK_HANDLE nh, NET_ROUTE *nr)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	ASSERT_RET(nr, CM_BAD_POINTER) ;
	(void)memcpy((char *)nr, (char *)&(network_tbl.list[nh].network_route), 
			sizeof(NET_ROUTE)) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_nt_net_route(NETWORK_HANDLE nh, NET_ROUTE *nr)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	(void)memcpy((char *)&(network_tbl.list[nh].network_route),
			(char *)nr, sizeof(NET_ROUTE)) ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_nt_dev_family(NETWORK_HANDLE nh, int *df)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	ASSERT_RET(df, CM_BAD_POINTER) ;
	*df = network_tbl.list[nh].device_family ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_nt_dev_family(NETWORK_HANDLE nh, int df)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	network_tbl.list[nh].device_family = df ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::get_nt_cfg_file_id(NETWORK_HANDLE nh, int *cfi)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	ASSERT_RET(cfi, CM_BAD_POINTER) ;
	*cfi = network_tbl.list[nh].config_file_id;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::set_nt_cfg_file_id(NETWORK_HANDLE nh, int cfi)
{

	if (!valid_network_handle(nh)) {
		return(CM_BAD_NETWORK_HANDLE) ;
	}
	network_tbl.list[nh].config_file_id = cfi ;
	return(CM_SUCCESS) ;
}


int CConnectionMgr::valid_block_handle(BLOCK_HANDLE bh)
{
	return	((bh >= 0) && (bh < active_blk_tbl.count) 
			&& (active_blk_tbl.list[bh])) ;
}


int CConnectionMgr::valid_device_handle(DEVICE_HANDLE dh)
{
	return ((dh >= 0) && (dh < active_dev_tbl.count)
			&& (active_dev_tbl.list[dh])) ;
}


int CDeviceTypeMgr::valid_device_type_handle(DEVICE_TYPE_HANDLE dth)
{
	return ((dth >= 0) && (dth < active_dev_type_tbl.count) 
			&& (active_dev_type_tbl.list[dth])) ;
}


int CConnectionMgr::valid_network_handle(NETWORK_HANDLE nh)
{
	return((nh >= 0) && (nh < network_tbl.count)) ;
}


int CRodMgr::valid_rod_handle(ROD_HANDLE rh)
{
	return ((rh >= 0) && (rh < rod_tbl.count) 
			&& (rod_tbl.list[rh])) ;
}

int CDeviceTypeMgr::valid_rod_handle(ROD_HANDLE rh)
{
	return m_RodMgr.valid_rod_handle(rh);
}

int CConnectionMgr::valid_dd_handle(ROD_HANDLE rod_handle)
{
	return (rod_handle >= 0);
}
