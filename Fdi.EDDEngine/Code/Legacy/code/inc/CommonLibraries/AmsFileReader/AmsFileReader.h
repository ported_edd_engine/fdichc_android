#pragma once
//#include "PlatformCommon.h"
enum AmsFileReaderStatus
{
	FILE_READER_SUCCESS,
	FILE_READER_INVALID_FILE,
	FILE_READER_INVALID_PARAMETERS,
	FILE_READER_FILE_EMPTY,
	FILE_READER_READ_ERROR,
	FILE_READER_END_OF_FILE,
	FILE_READER_FAILURE
};

enum FileEncoding
{
	FILE_ENCODING_UNKNOWN,
	FILE_ENCODING_ANSI,
	FILE_ENCODING_UTF8,
	FILE_ENCODING_UTF16LE,
	FILE_ENCODING_UTF16BE
};

class AmsFileReader
{
public:
	AmsFileReader();
	~AmsFileReader(void);

	AmsFileReaderStatus OpenFile(LPCTSTR fileName);
	void CloseFile();
	FileEncoding GetFileEncoding();
	AmsFileReaderStatus GetString(CStdString& string);
	AmsFileReaderStatus GetFileAsBuffer(wchar_t** fileBuffer, size_t* fileCharacterCount);
	bool EndOfFile();

	void GetErrorInfo(CStdString& sErrorInfo);

private:
	void DetermineFileEncoding();
	bool IsCorrectBomForFile(BYTE* bomValues, size_t bomSize);
	bool IsFileEncodedAsUTF8();
	bool IsFileEncodedUTF16LE();
	bool IsFileEncodedUTF16BE();
	size_t GetBOMSize(FileEncoding fe);
	size_t GetFileDataSize();
	void AdvancePastBom();

	CStdString m_sfileName;
	FILE* m_pFilePtr;
	FileEncoding m_Encoding;
	CStdString m_sErrorInfo;
};
