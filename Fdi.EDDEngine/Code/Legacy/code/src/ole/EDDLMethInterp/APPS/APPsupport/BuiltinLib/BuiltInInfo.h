////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "BuiltInInfo.h"
//
// Author: Mike DeCoster
//
// Creation Date: 5-2-07
//
// Purpose: Encapsulates all meta info concerning a built in function
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _BUILTININFO_H__
#define _BUILTININFO_H__

#pragma warning(disable: 4996) //WHS (EmersonProcess) Disable the VS2005 security warning 

//TODO: Remove once integrated into project
#include "typedefs.h"
#include "ParserDeclarations.h"
#include <string>


////////////////////////////////////////////////////////////////////////////////////////////////////
// Global Pre-Existing enums
////////////////////////////////////////////////////////////////////////////////////////////////////


//TODO: Remove definition from ParserDeclarations.h
#ifndef MAX_NUMBER_OF_FUNCTION_PARAMETERS	
#define MAX_NUMBER_OF_FUNCTION_PARAMETERS 10
#endif

enum BUILTIN_NAME
{
	BUILTIN_delay							=0
	,	BUILTIN_DELAY						=1
	,	BUILTIN_DELAY_TIME					=2
	,	BUILTIN_BUILD_MESSAGE				=3
	,	BUILTIN_PUT_MESSAGE
	,	BUILTIN_put_message
	,	BUILTIN_ACKNOWLEDGE
	,	BUILTIN_acknowledge
	,	BUILTIN__get_dev_var_value
	,	BUILTIN__get_local_var_value
	,	BUILTIN__display_xmtr_status
	,	BUILTIN_display_response_status
	,	BUILTIN_DISPLAY
	,	BUILTIN_display
	,	BUILTIN_SELECT_FROM_LIST
	,	BUILTIN_select_from_list
	,	BUILTIN_vassign
	,	BUILTIN__vassign
	,	BUILTIN__dassign
	,	BUILTIN__fassign
	,	BUILTIN__lassign
	,	BUILTIN__iassign
	,	BUILTIN__fvar_value
	,	BUILTIN__ivar_value
	,	BUILTIN__lvar_value
	,	BUILTIN_sassign
	,	BUILTIN__sassign
	,	BUILTIN_save_values
	,	BUILTIN_get_more_status
	,	BUILTIN__get_status_code_string
	,	BUILTIN_get_enum_string
	,	BUILTIN__get_enum_string
	,	BUILTIN_get_dictionary_string
	,	BUILTIN__get_dictionary_string
	,	BUILTIN_dictionary_string
	,	BUILTIN__dictionary_string
	,	BUILTIN_resolve_array_ref
	,	BUILTIN_resolve_array_ref2
	,	BUILTIN_resolve_record_ref
	,	BUILTIN_resolve_record_ref2
	,	BUILTIN_resolve_param_ref
	,   BUILTIN_resolve_local_ref
	,   BUILTIN_HT_resolve_local_ref
	,	BUILTIN_rspcode_string
	,	BUILTIN_get_rspcode_string_by_id
	,	BUILTIN__get_rspcode_string_by_id
	,	BUILTIN__set_comm_status
	,	BUILTIN__set_device_status
	,	BUILTIN__set_resp_code
	,	BUILTIN__set_all_resp_code
	,	BUILTIN__set_no_device
	,	BUILTIN_IGNORE_NO_DEVICE
	,	BUILTIN_SET_NUMBER_OF_RETRIES
	,	BUILTIN__set_xmtr_comm_status
	,	BUILTIN__set_xmtr_device_status
	,	BUILTIN__set_xmtr_resp_code
	,	BUILTIN__set_xmtr_all_resp_code
	,	BUILTIN__set_xmtr_no_device
	,	BUILTIN__set_xmtr_all_data
	,	BUILTIN__set_xmtr_data
	,	BUILTIN_abort
	,	BUILTIN_process_abort
	,	BUILTIN__add_abort_method
	,	BUILTIN__remove_abort_method
	,	BUILTIN_remove_all_abort
	,	BUILTIN_push_abort_method
	,	BUILTIN__push_abort_method
	,	BUILTIN_pop_abort_method
	,	BUILTIN_NaN_value
    ,   BUILTIN_FDI_NaN_value
	,	BUILTIN_isetval
	,	BUILTIN_lsetval
	,	BUILTIN_fsetval
	,	BUILTIN_dsetval
	,	BUILTIN_igetvalue
	,	BUILTIN_igetval
	,	BUILTIN_fgetval
	,	BUILTIN_sgetval
	,	BUILTIN_ssetval
	,	BUILTIN_send
	,	BUILTIN_send_command
	,	BUILTIN_send_command_trans
	,	BUILTIN_send_trans
	,	BUILTIN_ext_send_command
	,	BUILTIN_ext_send_command_trans
	,	BUILTIN_tsend_command
	,	BUILTIN_tsend_command_trans
	,	BUILTIN_abs
	,	BUILTIN_acos
	,	BUILTIN_asin
	,	BUILTIN_atan
	,	BUILTIN_cbrt
	,	BUILTIN_ceil
	,	BUILTIN_cos
	,	BUILTIN_cosh
	,	BUILTIN_exp
	,	BUILTIN_floor
	,	BUILTIN_fmod
#ifdef XMTR
	,	BUILTIN_frand
#endif
	,	BUILTIN_log
	,	BUILTIN_log10
	,	BUILTIN_log2
	,	BUILTIN_pow
	,	BUILTIN_round
	,	BUILTIN_sin
	,	BUILTIN_sinh
	,	BUILTIN_sqrt
	,	BUILTIN_tan
	,	BUILTIN_tanh
	,	BUILTIN_trunc
	,	BUILTIN_atof
	,	BUILTIN_atoi
	,	BUILTIN_itoa
	,	BUILTIN_ByteToDouble
	,	BUILTIN_ByteToFloat
	,	BUILTIN_ByteToLong
	,	BUILTIN_ByteToShort
	,	BUILTIN_DoubleToByte
	,	BUILTIN_FloatToByte
	,	BUILTIN_LongToByte
	,	BUILTIN_ShortToByte
	,	BUILTIN_YearMonthDay_to_Date
	,	BUILTIN_Date_to_Year
	,	BUILTIN_Date_to_Month
	,	BUILTIN_Date_to_DayOfMonth
	,	BUILTIN_DATE_to_days
	,	BUILTIN_days_to_DATE
	,	BUILTIN_From_DATE_AND_TIME_VALUE
	,	BUILTIN_From_TIME_VALUE
	,	BUILTIN_GetCurrentDate
	,	BUILTIN_GetCurrentTime
	,	BUILTIN_GetCurrentDateAndTime
	,	BUILTIN_To_Date_and_Time
	,	BUILTIN_strstr
	,	BUILTIN_strupr
	,	BUILTIN_strlwr
	,	BUILTIN_strlen
	,	BUILTIN_strcmp
	,	BUILTIN_strtrim
	,	BUILTIN_strmid
	,	BUILTIN_strleft
	,	BUILTIN_strright
	,	BUILTIN_discard_on_exit
	,   BUILTIN_ListInsert				
	,   BUILTIN__ListInsert				
	,	BUILTIN_ListDeleteElementAt
	,	BUILTIN__ListDeleteElementAt
	,   BUILTIN__MenuDisplay
	,   BUILTIN_remove_all_abort_methods
	,   BUILTIN_DiffTime
	,   BUILTIN_AddTime
	,   BUILTIN_Make_Time
	,   BUILTIN_To_Time
	,   BUILTIN_Date_To_Time
	,   BUILTIN_To_Date
	,   BUILTIN_Time_To_Date
	,   BUILTIN_seconds_to_TIME_VALUE
	,   BUILTIN_seconds_to_TIME_VALUE8
	,   BUILTIN_TIME_VALUE_to_seconds
	,   BUILTIN_TIME_VALUE_to_Hour
	,   BUILTIN_TIME_VALUE_to_Minute
	,   BUILTIN_TIME_VALUE_to_Second
	,   BUILTIN_DATE_AND_TIME_VALUE_to_string
	,   BUILTIN_DATE_to_string
	,   BUILTIN_TIME_VALUE_to_string
	,   BUILTIN_timet_to_string
	,   BUILTIN_timet_to_TIME_VALUE
	,   BUILTIN_timet_to_TIME_VALUE8
	,   BUILTIN_To_TIME_VALUE
	,   BUILTIN_To_TIME_VALUE8
	,   BUILTIN_fpclassify
	,   BUILTIN_nanf
	,   BUILTIN_nan
	,   BUILTIN_literal_string
	,   BUILTIN_GET_DEV_VAR_VALUE
	,	BUILTIN_get_dev_var_value
	,	BUILTIN_GET_LOCAL_VAR_VALUE
	,	BUILTIN_get_local_var_value
	,   BUILTIN_MenuDisplay
	,	BUILTIN_Menu
	,	BUILTIN_get_stddict_string
	,	BUILTIN_add_abort_method
	,	BUILTIN_dassign
	,   BUILTIN_fassign
	,	BUILTIN_float_value
	,	BUILTIN_fvar_value
	,	BUILTIN_iassign
	,	BUILTIN_int_value
	,	BUILTIN_ivar_value
	,	BUILTIN_lassign
	,	BUILTIN_long_value
	,	BUILTIN_lvar_value
	,	BUILTIN_remove_abort_method
	//,	BUILTIN_vassign
	,	BUILTIN_assign_var
	,	BUILTIN_ReadCommand
	,	BUILTIN_WriteCommand
	,	BUILTIN_ReadWriteCommand
	,	BUILTIN_UNDEFINED			// uninitialized
	,	BUILTIN_LANG_INTRINSIC		// part of language, not a builtin, nescessary to build a complete mapping
	,	BUILTIN_assign_double
	,	BUILTIN_assign_float
	,	BUILTIN_assign_int
	,	BUILTIN_delayfor
	,	BUILTIN_delayfor2
	,	BUILTIN_DICT_ID
	,	BUILTIN_display_dynamics
	,	BUILTIN_display_dynamics2
	,	BUILTIN_display_message
	,	BUILTIN_display_message2
	,	BUILTIN_edit_device_value
	,	BUILTIN_edit_device_value2
	,	BUILTIN_edit_local_value
	,	BUILTIN_edit_local_value2
	,	BUILTIN_ftoa
	,	BUILTIN_get_double
	,	BUILTIN_get_double_value
	,	BUILTIN_get_double_value2
	,	BUILTIN_get_signed
	,	BUILTIN_get_signed_value
	,	BUILTIN_get_signed_value2
	,	BUILTIN_get_string
	,	BUILTIN_assign
	,	BUILTIN_get_date_value
	,	BUILTIN_get_date_value2
	,	BUILTIN_get_acknowledgement
	,	BUILTIN_get_acknowledgement2
	,	BUILTIN_get_string_value
	,	BUILTIN_get_string_value2
	,	BUILTIN_get_unsigned
	,	BUILTIN_get_unsigned_value
	,	BUILTIN_get_unsigned_value2
	,	BUILTIN_is_NaN
	,	BUILTIN_method_abort
	,	BUILTIN_read_value
	,	BUILTIN_save_on_exit
	,	BUILTIN_select_from_menu
	,	BUILTIN_select_from_menu2
	,	BUILTIN_send_all_values
	,	BUILTIN_send_on_exit
	,	BUILTIN_send_value
	,	BUILTIN__WARNING
	,	BUILTIN__TRACE
	,	BUILTIN__ERROR
	,	BUILTIN__itoa
	,	BUILTIN_get_response_code_string
	,   BUILTIN_display_response_code
	,	BUILTIN_get_dds_error
	,	BUILTIN_display_builtin_error
	,	BUILTIN_display_comm_error
	,	BUILTIN_get_comm_error
	,	BUILTIN_get_comm_error_string
	,	BUILTIN_get_response_code
	,	BUILTIN_put_double
	,	BUILTIN_put_string
	,	BUILTIN_put_signed
	,	BUILTIN_put_unsigned
	,	BUILTIN_put_date
	,	BUILTIN_get_date
	,	BUILTIN_abort_on_comm_error
	,	BUILTIN_retry_on_comm_error
	,	BUILTIN_abort_on_response_code
	,	BUILTIN_retry_on_response_code
	,	BUILTIN_fail_on_comm_error
	,	BUILTIN_fail_on_response_code
	,	BUILTIN_abort_on_all_comm_errors
	,	BUILTIN_abort_on_all_response_codes
	,	BUILTIN_fail_on_all_comm_errors
	,	BUILTIN_fail_on_all_response_codes
	,	BUILTIN_retry_on_all_comm_errors
	,	BUILTIN_retry_on_all_response_codes
	,	BUILTIN_ret_double_value
	,	BUILTIN_ret_signed_value
	,	BUILTIN_ret_unsigned_value
	,	BUILTIN_put_date_value
    ,   BUILTIN_put_double_value
    ,   BUILTIN_put_float_value
    ,   BUILTIN_put_signed_value
    ,   BUILTIN_put_unsigned_value
    ,   BUILTIN_put_string_value
	,	BUILTIN_put_date_value2
	,   BUILTIN_put_double_value2
    ,   BUILTIN_put_float_value2
    ,   BUILTIN_put_signed_value2
    ,   BUILTIN_put_unsigned_value2
    ,   BUILTIN_put_string_value2
    ,   BUILTIN_get_double_lelem
    ,   BUILTIN_get_float_lelem
    ,   BUILTIN_get_string_lelem
    ,   BUILTIN_get_signed_lelem
    ,   BUILTIN_get_unsigned_lelem
    ,   BUILTIN_get_date_lelem
	,	BUILTIN_resolve_list_ref
	,	BUILTIN_FF_resolve_param_ref
	,	BUILTIN_resolve_param_list_ref
	,	BUILTIN_resolve_block_ref
	,	BUILTIN_get_resolve_status
	,	BUILTIN_get_status_string
	,	BUILTIN_resolve_local_ref2
	,	BUILTIN_resolve_param_ref2
	,	BUILTIN_send_value2
	,	BUILTIN_read_value2
	,	BUILTIN_assign2
	,	BUILTIN_get_block_instance_count
	,	BUILTIN_get_block_instance_by_tag
	,	BUILTIN_get_block_instance_by_object_index
	,   BUILTIN_get_date_lelem2
	,   BUILTIN_get_double_lelem2
	,   BUILTIN_get_float_lelem2
	,   BUILTIN_get_signed_lelem2
	,   BUILTIN_get_string_lelem2
	,   BUILTIN_get_unsigned_lelem2
	,	BUILTIN_ret_double_value2
	,	BUILTIN_ret_signed_value2
	,	BUILTIN_ret_unsigned_value2
	,   BUILTIN_ListInsert2	
	,	BUILTIN_drand
	,   BUILTIN_dseed
	,	BUILTIN_LOG_MESSAGE
	,	BUILTIN_isOffline
	,   BUILTIN_ListDeleteElementAt2
	,	BUILTIN_resolve_block_ref2
	,	BUILTIN_GET_DD_REVISION
	,	BUILTIN_GET_DEVICE_REVISION
	,   BUILTIN_GET_DEVICE_TYPE
	,	BUILTIN_GET_MANUFACTURER
	,	BUILTIN_GET_TICK_COUNT
	,	BUILTIN_get_variable_string
	,	BUILTIN__get_variable_string
	,	BUILTIN_display_bitenum
	,	BUILTIN__display_bitenum
	,	BUILTIN_VARID
	,	BUILTIN__VARID
	,	BUILTIN_GET_SEL_DOUBLE
	,	BUILTIN_GET_SEL_DOUBLE2
	,	BUILTIN_GET_SEL_STRING
	,	BUILTIN_RESOLVE_SELECTOR_REF /* Builtin resolve_selector_ref() is obsolete for FDI. But keep it for legacy FF. */
	,	BUILTIN_LINE_NUMBER
};

////////////////////////////////////////////////////////////////////////////////////////////////////
class CBuiltInInfo
{
public:
	CBuiltInInfo();
	CBuiltInInfo(const CBuiltInInfo& rs);
	virtual ~CBuiltInInfo();
	CBuiltInInfo& operator=(const CBuiltInInfo& rs);
	virtual void Clear();

	void DebugDump(FILE* pFile);

	////////////////////////////////////////////////////////////////////////////////////////////////
	virtual void Set(const CBuiltInInfo& rs);
    void Set(
                BUILTIN_NAME		enBuiltIn,
                const char*			strName,
                RUL_TOKEN_TYPE		enTokenType,
                RUL_TOKEN_SUBTYPE	enTokenSubType,
                RUL_TOKEN_TYPE		enReturnType,
                int                 uNumberOfParams,
                RUL_TOKEN_TYPE		enParam1 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam2 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam3 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam4 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam5 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam6 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam7 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam8 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam9 = RUL_TYPE_NONE,
                RUL_TOKEN_TYPE		enParam10= RUL_TYPE_NONE);

	void SetName(const char* szStr)			{ m_strName = szStr; }
	void SetType(RUL_TOKEN_TYPE tt)			{ m_enTokenType = tt; }
	void SetSubType(RUL_TOKEN_SUBTYPE ts)	{ m_enTokenSubType = ts; }
	void SetReturnType(RUL_TOKEN_TYPE rt)	{ m_enReturnType = rt; }
        void SetNumOfParams(_UINT32 uNum)		{ m_uNumberOfParams = uNum; }

        void SetParams(RUL_TOKEN_TYPE* pTokenTypes, _UINT32 uNumParams)
	{ memcpy(m_penParameterType,pTokenTypes,sizeof(RUL_TOKEN_TYPE)*uNumParams); m_uNumberOfParams = uNumParams; }

        void SetParam(UINT32 uIndex, RUL_TOKEN_TYPE tt)
	{
		if (uIndex >= MAX_NUMBER_OF_FUNCTION_PARAMETERS)
			return;
		m_penParameterType[uIndex] = tt;
		if (uIndex > m_uNumberOfParams)
			m_uNumberOfParams = uIndex;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	const string&		GetName()					{ return m_strName; }  // note - returns a reference
	const string		GetName_copy()	const		{ return m_strName; }  // note - creates a copy
	RUL_TOKEN_TYPE		GetTokenType() const		{ return m_enTokenType; }
	RUL_TOKEN_SUBTYPE	GetTokenSubtype() const		{ return m_enTokenSubType; }
	RUL_TOKEN_TYPE		GetReturnType() const		{ return m_enReturnType; }
	RUL_TOKEN_TYPE		GetTokenReturnType() const	{ return m_enReturnType; }
        UINT32				GetNumberOfParams() const	{ return m_uNumberOfParams; }
	const RUL_TOKEN_TYPE* GetParamTypes() const		{ return m_penParameterType; }
	BUILTIN_NAME		GetBuiltInEn() const		{ return m_enBuiltIn; }
	RUL_TOKEN_TYPE		GetParam(unsigned int i) const
	{
		if (i > m_uNumberOfParams || i > MAX_NUMBER_OF_FUNCTION_PARAMETERS)
			return RUL_TYPE_NONE;
		return m_penParameterType[i];
	}

protected:
	BUILTIN_NAME		m_enBuiltIn;
	string				m_strName;
	RUL_TOKEN_TYPE		m_enTokenType;
	RUL_TOKEN_SUBTYPE	m_enTokenSubType;
	RUL_TOKEN_TYPE		m_enReturnType;
        UINT32				m_uNumberOfParams;
	RUL_TOKEN_TYPE		m_penParameterType[MAX_NUMBER_OF_FUNCTION_PARAMETERS];
};

#endif
