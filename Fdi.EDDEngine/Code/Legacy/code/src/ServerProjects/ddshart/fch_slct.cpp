/**
 *	@(#) $Id: fch_slct.c,v 1.2 1996/04/18 14:43:56 davcomf Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all of the functions for fetch select.  This
 *	is the "front end" of fetch and basically serves to obtain the
 *	proper DD handles and DD references (object indices or file
 *	offsets) and routing the calls to either ROD fetch or flat fetch.
 *
 *	A call to ROD fetch means that the requested data is contained
 *	in a Remote Object Dictionary (ROD) in binary form and will need
 *	to be evaluated.  A call to flat fetch means that the requested
 *	data is contained in a flat file and is already evaluated (except
 *	for any data that is dynamic).
 * 
 *	Currently, the flat fetch interface is only implemented with stub
 *	functions.  These stub functions are included in this file for
 *	the time being.
 */


#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif

#define	LESS_THAN					-1
#define	GREATER_THAN				 1
#define	EQUAL						 0


/*	
 *	Note:	The following will be moved into a common .h file when the
 *			flat file system is more clearly defined.
 */

#define	DEVICE_DIRECTORY_OFFSET		 0


/**********************************************************************
 *                                                                    *
 *                      Flat Fetch Functions                          *
 *                                                                    *
 **********************************************************************/

/***********************************************************************
 *
 *	Name:  fch_flat_item_spad_size
 *	ShortDesc:  Get the required scratch pad size needed to retrieve
 *				an item's attribute data from a flat file.
 *
 *	Description:
 *		NOTE:  The fch_flat_item_spad_size function below is
 *		currently just a stub.  The real function's basic purpose
 *		(when ready) will be to take a flat handle and file offset
 *		and get the required scratch pad size needed to retrieve the
 *		item's attribute data from a flat file.  The details of how
 *		this will be implemented and the exact inputs & outputs will
 *		be determined at a later time.
 *		
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_flat_item_spad_size(
FLAT_HANDLE		 flat_handle,
FILE_OFFSET		 file_offset,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
ITEM_TYPE		 item_type)
{
	/*
	 *	For now, reference each parameter and return a code
	 *	indicating that the item was not found.
	 */

	if (flat_handle) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (file_offset) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (required_sp_size) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (request_mask) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (item_type) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	return(FETCH_ITEM_NOT_FOUND);
}


/***********************************************************************
 *
 *	Name:  fch_flat_item
 *	ShortDesc: Fetch attribute data for an item from a flat file.
 *
 *	Description:
 *		NOTE:  The fch_flat_item function below is currently just
 *		a stub.  The real function's basic purpose (when ready) will
 *		be to take a flat handle and file offset and retrieve an
 *		item's attribute data from a flat file.  The details of how
 *		this will be implemented and the exact inputs & outputs will
 *		be determined at a later time.
 *		
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_flat_item(
FLAT_HANDLE		 flat_handle,
FILE_OFFSET		 file_offset,
SCRATCH_PAD		*scratch_pad,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
void			*flat,
ITEM_TYPE		 item_type)
{
	/*
	 *	For now, reference each parameter and return a code
	 *	indicating that the item was not found.
	 */

	if (flat_handle) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (file_offset) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (scratch_pad) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (required_sp_size) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (request_mask) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (flat) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (item_type) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	return(FETCH_ITEM_NOT_FOUND);
}


/***********************************************************************
 *
 *	Name:  fch_flat_dir_spad_size
 *	ShortDesc:  Get the required scratch pad size needed to retrieve
 *				a directory's table data from a flat file.
 *
 *	Description:
 *		NOTE:  The fch_flat_dir_spad_size function below is
 *		currently just a stub.  The real function's basic purpose
 *		(when ready) will be to take a flat handle and file offset
 *		and get the required scratch pad size needed to retrieve the
 *		directory's table data from a flat file.  The details of how
 *		this will be implemented and the exact inputs & outputs will
 *		be determined at a later time.
 *		
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_flat_dir_spad_size(
FLAT_HANDLE		 flat_handle,
FILE_OFFSET		 file_offset,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
unsigned short	 dir_type)
{
	/*
	 *	For now, reference each parameter and return a code
	 *	indicating that the item was not found.
	 */

	if (flat_handle) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (file_offset) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (required_sp_size) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (request_mask) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (dir_type) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	return(FETCH_ITEM_NOT_FOUND);
}


/***********************************************************************
 *
 *	Name:  fch_flat_device_dir
 *	ShortDesc: Fetch table data for a device directory from a flat file.
 *
 *	Description:
 *		NOTE:  The fch_flat_device_dir function below is currently just
 *		a stub.  The real function's basic purpose (when ready) will
 *		be to take a flat handle and file offset and retrieve a
 *		directory's table data from a flat file.  The details of how
 *		this will be implemented and the exact inputs & outputs will
 *		be determined at a later time.
 *		
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_flat_device_dir(
FLAT_HANDLE				 flat_handle,
FLAT_DEVICE_DIR			*device_dir)
{
	/*
	 *	For now, reference each parameter and return a code
	 *	indicating that the item was not found.
	 */

	if (flat_handle) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (device_dir) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	return(FETCH_ITEM_NOT_FOUND);
}


/***********************************************************************
 *
 *	Name:  fch_flat_block_dir
 *	ShortDesc: Fetch table data for a block directory from a flat file.
 *
 *	Description:
 *		NOTE:  The fch_flat_block_dir function below is currently just
 *		a stub.  The real function's basic purpose (when ready) will
 *		be to take a flat handle and file offset and retrieve a
 *		directory's table data from a flat file.  The details of how
 *		this will be implemented and the exact inputs & outputs will
 *		be determined at a later time.
 *		
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_flat_block_dir(
FLAT_HANDLE				 flat_handle,
FILE_OFFSET				 file_offset,
FLAT_BLOCK_DIR			*block_dir)
{

	/*
	 *	For now, reference each parameter and return a code
	 *	indicating that the item was not found.
	 */

	if (flat_handle) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (file_offset) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	if (block_dir) {
		return(FETCH_ITEM_NOT_FOUND);
	}
	return(FETCH_ITEM_NOT_FOUND);
}


/**********************************************************************
 *                                                                    *
 *                    Fetch Select Functions                          *
 *                                                                    *
 **********************************************************************/

/***********************************************************************
 *
 *	Name:  compare_item_id
 *	ShortDesc:  Compare item ID's of two items in the Item Table.
 *
 *	Description:
 *		The compare_item_id function takes in two pointers to Item
 *		Table element structures in an Item Table structure and
 *		compares their item ID's.  The value returned is reflective
 *		of the comparison of the two item ID's.  This function is
 *		used in conjunction with the bsearch function (for searching
 *		the Item Table).
 *
 *	Inputs:
 *		item_1 - pointer to an Item Table element structure in an
 *				 Item Table structure.
 *		item_2 - pointer to another Item Table element structure in
 *				 an Item Table structure.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		LESS_THAN.
 *		GREATER_THAN.
 *		EQUAL.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
compare_item_id(
ITEM_TBL_ELEM	*item_1,
ITEM_TBL_ELEM	*item_2)
{
	/*
	 *	Compare the two item ID's and return the reflective value.
	 */

	if (item_1->item_id < item_2->item_id) {
		return(LESS_THAN);
	}
	else if (item_1->item_id > item_2->item_id) {
		return(GREATER_THAN);
	}
	else {
		return(EQUAL);
	}
}


/***********************************************************************
 *
 *	Name:  get_item_dd_ref
 *	ShortDesc:  Get the DD reference for an item.
 *
 *	Description:
 *		The get_item_dd_ref function takes a device handle, and
 *		uses the Connection Tables to obtain the corresponding Device
 *		Directory Tables.  It then searches the Item Table to find
 *		the particular item ID, and assigns its DD reference.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the item.
 *		item_id - ID of the item.
 *
 *	Outputs:
 *		dd_reference - DD reference of the item.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_TABLES_NOT_FOUND.
 *		FETCH_ITEM_NOT_FOUND.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_item_dd_ref(
ENV_INFO			*env_info,
DEVICE_HANDLE		 device_handle,
ITEM_ID				 item_id,
DD_REFERENCE		*dd_reference)
{
	FLAT_DEVICE_DIR		*flat_device_dir;
	ITEM_TBL_ELEM		*item_tbl_elem;
	int					r_code ;

#ifdef ISPTEST
	TEST_FAIL(GET_ITEM_DD_REF);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 *	Get the pointer to the Device Directory Tables.
	 */

	r_code = pConnMgr->get_adt_dd_dev_tbls(device_handle, 
			(void **)&flat_device_dir);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_dir == 0) {
		return(FETCH_TABLES_NOT_FOUND);
	}

	/*
	 *	Search for the particular item ID in the Item Table.
	 */

	item_tbl_elem = (ITEM_TBL_ELEM *) bsearch(
		(char *)&item_id,
		(char *)flat_device_dir->item_tbl.list,
		(unsigned int)flat_device_dir->item_tbl.count,
		sizeof(ITEM_TBL_ELEM),
		(CMP_FN_PTR)compare_item_id);

	if (item_tbl_elem == 0) {
		return(FETCH_ITEM_NOT_FOUND);
	}

	/*
	 *	Assign the DD reference of the item.
	 */

	*dd_reference = item_tbl_elem->dd_ref;

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_item_spad_size
 *	ShortDesc:  Get the scratch pad size needed to fetch the requested
 *				attributes of an item.
 *
 *	Description:
 *		The fch_item_spad_size function takes a device handle
 *		and obtains the corresponding DD handle.  It then converts
 *		an item ID into a DD reference (object index or file offset).
 *		It then uses either ROD fetch or flat fetch (depending upon
 *		the type of DD handle) to get the required scratch pad size
 *		needed to fetch the requested attributes of the item.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the item.
 *		item_id - ID of the item.
 *		request_mask - mask that identifies the specific attributes
 *					   to be fetched.
 *		item_type - type of item to be fetched.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the attributes
 *						   that were requested.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from get_item_dd_ref function.
 *		Return values from fch_rod_item_spad_size function.
 *		Return values from fch_flat_item_spad_size function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/
//not used in legacy engine
int
fch_item_spad_size(
ENV_INFO	*env_info,
DEVICE_HANDLE	 device_handle,
ITEM_ID			 item_id,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
ITEM_TYPE		 item_type)
{
	ROD_HANDLE			rod_handle = 0;
	DD_REFERENCE		dd_reference;
	int					r_code;

#ifdef ISPTEST
	TEST_FAIL(FCH_ITEM_SCRATCHPAD_SIZE);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Get the DD reference from the device handle and item ID.
	 */

	r_code = get_item_dd_ref( env_info,
		device_handle,
		item_id,
		&dd_reference);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info,
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	r_code = fch_rod_item_spad_size(
		env_info,
		rod_handle,
		dd_reference.object_index,
		required_sp_size,
		request_mask,
		item_type);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_item
 *	ShortDesc:  Fetch attribute data for an item.
 *
 *	Description:
 *		The fch_item function takes a device handle and obtains
 *		the corresponding DD handle.  It then converts an item ID
 *		into a DD reference (object index or file offset).  It then
 *		uses either ROD fetch or flat fetch (depending upon the type
 *		of DD handle) to fetch the attribute data for the specified
 *		item.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the item.
 *		item_id - ID of the item.
 *		scratch_pad - scratch pad memory structure that fetch will
 *					  use for various data storage.
 *		request_mask - mask that identifies the specific attributes
 *					   to be fetched.
 *		flat - item flat structure.
 *		item_type - type of item to be fetched.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the remaining
 *						   attributes that were requested.
 *		flat - item flat structure containing attribute data.
 *
 *	Returns: 
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from get_item_dd_ref function.
 *		Return values from fch_rod_item function.
 *		Return values from fch_flat_item function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

int
fch_item(
ENV_INFO		*env_info,
DEVICE_HANDLE	 device_handle,
ITEM_ID			 item_id,
SCRATCH_PAD		*scratch_pad,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
void			*flat,
ITEM_TYPE		 item_type)
{
	ROD_HANDLE			rod_handle = 0;
	DD_REFERENCE		dd_reference;
	int					r_code;

#ifdef ISPTEST
	TEST_FAIL(FETCH_ITEM);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	
	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Get the DD reference from the device handle and item ID.
	 */

	r_code = get_item_dd_ref( env_info,
		device_handle,
		item_id,
		&dd_reference);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}
	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info, 
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}
	r_code = fch_rod_item(
		env_info,
		rod_handle,
		dd_reference.object_index,
		scratch_pad,
		required_sp_size,
		request_mask,
		flat,
		item_type);
	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  compare_block_id
 *	ShortDesc:  Compare block item ID's of two blocks in the
 *				Block Table.
 *
 *	Description:
 *		The compare_block_id function takes in two pointers to Block
 *		Table element structures in an Block Table structure and
 *		compares their item ID's.  The value returned is reflective
 *		of the comparison of the two item ID's.  This function is
 *		used in conjunction with the bsearch function (for searching
 *		the Block Table).
 *
 *	Inputs:
 *		block_1 - pointer to a Block Table element structure in a
 *				  Block Table structure.
 *		block_2 - pointer to another Block Table element structure in
 *				  Block Table structure.
 *
 *	Outputs:
 *		None
 *
 *	Returns:
 *		LESS_THAN.
 *		GREATER_THAN.
 *		EQUAL.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
compare_block_id(
BLK_TBL_ELEM	*block_1,
BLK_TBL_ELEM	*block_2)
{
	/*
	 *	Compare the two block item ID's and return the reflective value.
	 */

	if (block_1->blk_id < block_2->blk_id) {
		return(LESS_THAN);
	}
	else if (block_1->blk_id > block_2->blk_id) {
		return(GREATER_THAN);
	}
	else {
		return(EQUAL);
	}
}


/***********************************************************************
 *
 *	Name:  get_blk_dir_dd_ref
 *	ShortDesc:  Get the DD reference for a block directory.
 *
 *	Description:
 *		The get_blk_dir_dd_ref function takes a device handle,
 *		and uses the Connection Tables to obtain the corresponding
 *		Device Directory Tables.  It then searches the Block
 *		Table to find the particular block item ID, and assigns its DD
 *		reference.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the block
 *						directory.
 *		block_id - item ID of the block.
 *
 *	Outputs:
 *		dd_reference - DD reference of the block directory.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_TABLES_NOT_FOUND.
 *		FETCH_DIRECTORY_NOT_FOUND.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/

static int
get_blk_dir_dd_ref(
ENV_INFO			*env_info,
DEVICE_HANDLE		 device_handle,
ITEM_ID				 block_id,
DD_REFERENCE		*dd_reference)
{
	FLAT_DEVICE_DIR			*flat_device_dir;
	BLK_TBL_ELEM			*blk_tbl_elem;
	int						r_code ;

	/*
	 *	Get the pointer to the Device Directory Tables.
	 */

#ifdef ISPTEST
	TEST_FAIL(GET_BLK_DIR_DD_REF);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	r_code = pConnMgr->get_adt_dd_dev_tbls(device_handle, 
			(void **)&flat_device_dir);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (flat_device_dir == 0) {
		return(FETCH_TABLES_NOT_FOUND);
	}

	/*
	 *	Search for the particular block item ID in the Block Table.
	 */

	blk_tbl_elem = (BLK_TBL_ELEM *) bsearch(
		(char *)&block_id,
		(char *)flat_device_dir->blk_tbl.list,
		(unsigned int)flat_device_dir->blk_tbl.count,
		sizeof(BLK_TBL_ELEM),
		(CMP_FN_PTR)compare_block_id);

	if (blk_tbl_elem == 0) {
		return(FETCH_DIRECTORY_NOT_FOUND);
	}

	/*
	 *	Assign the DD reference of the block directory.
	 */

	*dd_reference = blk_tbl_elem->blk_dir_dd_ref;

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_block_dir_spad_size
 *	ShortDesc:  Get the scratch pad size needed to fetch the requested
 *				tables of a block directory.
 *
 *	Description:
 *		The fch_block_dir_spad_size function takes a device
 *		handle and obtains the corresponding DD handle.  It then
 *		converts a block item ID into a DD reference (object index or
 *		file offset).  It then uses either ROD fetch or flat fetch
 *		(depending upon the type of DD handle) to get the required
 *		scratch pad size needed to fetch the requested tables of
 *		the block directory.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the block
 *						directory.
 *		block_id - item ID of the block.
 *		request_mask - mask that identifies the specific tables to
 *					   be fetched.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the tables that
 *						   were requested.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from get_blk_dir_dd_ref function.
 *		Return values from fch_rod_dir_spad_size function.
 *		Return values from fch_flat_dir_spad_size function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/
//not used in legacy engine
int
fch_block_dir_spad_size(
ENV_INFO		*env_info,
DEVICE_HANDLE	 device_handle,
ITEM_ID			 block_id,
unsigned long	*required_sp_size,
unsigned long	 request_mask)
{
	ROD_HANDLE			rod_handle = 0;
	DD_REFERENCE		dd_reference;
	int					r_code;

#ifdef ISPTEST
	TEST_FAIL(FCH_BLOCK_DIR_SCRATCHPAD_SIZE);
#endif
	
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Get the DD reference from the device handle and
	 *	block item ID.
	 */

	r_code = get_blk_dir_dd_ref(
		env_info,
		device_handle,
		block_id,
		&dd_reference);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info,
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	r_code = fch_rod_dir_spad_size(
			env_info,
			rod_handle,
			dd_reference.object_index,
			required_sp_size,
			request_mask,
			(unsigned short)BLOCK_DIR_TYPE);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_block_dir
 *	ShortDesc:  Fetch table data for a block directory.
 *
 *	Description:
 *		The fch_block_dir function takes a device handle and
 *		obtains the corresponding DD handle.  It then converts a
 *		block item ID into a DD reference (object index or file offset).
 *		It then uses either ROD fetch or flat fetch (depending upon
 *		the type of DD handle) to fetch the table data for the
 *		specified block directory.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the block
 *						directory.
 *		block_id - item ID of the block.
 *		scratch_pad - scratch pad memory structure that fetch will
 *					  use for various data storage.
 *		request_mask - mask that identifies the specific tables to
 *					   be fetched.
 *		binary - block directory binary structure.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the remaining
 *						   attributes that were requested.
 *		binary - block directory binary structure containing table
 *				 table data in binary form.  (If ROD fetch was used).
 *		flat - block directory flat structure containing table data
 *			   in evaluated form.  (If flat fetch was used).
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from get_blk_dir_dd_ref function.
 *		Return values from fch_rod_block_dir function.
 *		Return values from fch_flat_block_dir function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/
//not used in legacy engine
int
fch_block_dir(
ENV_INFO		*env_info,
DEVICE_HANDLE	 device_handle,
ITEM_ID			 block_id,
SCRATCH_PAD		*scratch_pad,
unsigned long	*required_sp_size,
unsigned long	 request_mask,
BIN_BLOCK_DIR	*binary)
{
	ROD_HANDLE			rod_handle = 0;
	DD_REFERENCE		dd_reference;
	int					r_code;

#ifdef ISPTEST
	TEST_FAIL(FETCH_BLOCK_DIR);
#endif


	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Get the DD reference from the device handle and
	 *	block item ID.
	 */

	r_code = get_blk_dir_dd_ref(
		env_info,
		device_handle,
		block_id,
		&dd_reference);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}
	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info, 
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	r_code = fch_rod_block_dir(
		env_info,
		rod_handle,
		dd_reference.object_index,
		scratch_pad,
		required_sp_size,
		request_mask,
		binary);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_device_dir_spad_size
 *	ShortDesc:  Get the scratch pad size needed to fetch the requested
 *				tables of a device directory.
 *
 *	Description:
 *		The fch_device_dir_spad_size function takes a device
 *		handle and obtains the corresponding DD handle.  It then uses
 *		either ROD fetch or flat fetch (depending upon the type of DD
 *		handle) to get the required scratch pad size needed to fetch
 *		the requested tables of the device directory.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the device
 *						directory.
 *		request_mask - mask that identifies the specific tables to
 *					   be fetched.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the tables that
 *						   were requested.
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from fch_rod_dir_spad_size function.
 *		Return values from fch_flat_dir_spad_size function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/
//not used in legacy engine
int
fch_device_dir_spad_size(
ENV_INFO			*env_info,
DEVICE_HANDLE		 device_handle,
unsigned long		*required_sp_size,
unsigned long		 request_mask)
{
	ROD_HANDLE			 rod_handle = 0;
	int					 r_code;

#ifdef ISPTEST
	TEST_FAIL(FCH_DEVICE_DIR_SCRATCHPAD_SIZE);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle);
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info,
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	r_code = fch_rod_dir_spad_size(
		env_info,
		rod_handle,
		(OBJECT_INDEX)DEVICE_DIRECTORY_INDEX,
		required_sp_size,
		request_mask,
		(unsigned short)DEVICE_DIR_TYPE);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  fch_device_dir
 *	ShortDesc:  fetch table data for a device directory.
 *
 *	Description:
 *		The fch_device_dir function takes a device handle and
 *		obtains the corresponding DD handle.  It then uses either
 *		ROD fetch or flat fetch (depending upon the type of DD handle)
 *		to fetch the table data for the device directory.
 *
 *	Inputs:
 *		device_handle - handle of the device that contains the device
 *						directory.
 *		scratch_pad - scratch pad memory structure that fetch will
 *					  use for various data storage.
 *		required_sp_size - required scratch pad size variable.
 *		request_mask - mask that identifies the specific tables to
 *					   be fetched.
 *		binary - device directory binary structure.
 *
 *	Outputs:
 *		required_sp_size - required scratch pad size showing the
 *						   memory needed to fetch the remaining
 *						   attributes that were requested.
 *		binary - device directory binary structure containing table
 *				 data in binary form.  (If ROD fetch was used).
 *		flat - device directory flat structure containing table data
 *			   in evaluated form.  (If flat fetch was used).
 *
 *	Returns:
 *		DDS_SUCCESS.
 *		FETCH_INVALID_DEVICE_HANDLE.
 *		FETCH_BAD_DD_DEVICE_LOAD.
 *		FETCH_INVALID_DD_HANDLE_TYPE.
 *		Return values from fch_rod_device_dir function.
 *		Return values from fch_flat_device_dir function.
 *
 *	Author:
 *		Jon Reuter
 *
 **********************************************************************/
//not used in legacy engine
int
fch_device_dir(
ENV_INFO 			*env_info,
DEVICE_HANDLE		 device_handle,
SCRATCH_PAD			*scratch_pad,
unsigned long		*required_sp_size,
unsigned long		 request_mask,
BIN_DEVICE_DIR		*binary)
{
	ROD_HANDLE			 rod_handle = 0;
	int					 r_code;

#ifdef ISPTEST
	TEST_FAIL(FETCH_DEVICE_DIR);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();


	/*
	 *	Validate the device handle.
	 */

	if (!pConnMgr->valid_device_handle(device_handle)) {
		return(FETCH_INVALID_DEVICE_HANDLE);
	}

	/*
	 *	Copy and validate the DD handle.  If the DD handle is not
	 *	valid, request to load the DD for the device.
	 */

	r_code = pConnMgr->get_adt_dd_handle(device_handle, &rod_handle) ;
	if (r_code != SUCCESS) {
		return(r_code) ;
	}

	if (!g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
		r_code = pConnMgr->ds_dd_device_load(env_info,
				device_handle,
				&rod_handle);
		if (r_code) {
			return(FETCH_BAD_DD_DEVICE_LOAD);
		}
	}

	r_code = fch_rod_device_dir( env_info,
		rod_handle,
		scratch_pad,
		required_sp_size,
		request_mask,
		binary);

	if (r_code != DDS_SUCCESS) {
		return(r_code);
	}

	return(DDS_SUCCESS);
}


