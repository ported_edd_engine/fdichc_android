/******************************************************************************
 * $Header: AMSMiscUtilExport.h: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:55 AM$
 *
 * Copyright 1999 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:	Include containing import/export macro for AMSServerUtil
 *													   
 * $Workfile: AMSMiscUtilExport.h$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#if !defined(AMSMISCUTIL_EXPORT_H)
#define AMSMISCUTIL_EXPORT_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#if defined(BUILD_AMSMISCUTIL)
  #define AMSMISCUTIL_EXPORT __declspec( dllexport )
#else
  #define AMSMISCUTIL_EXPORT __declspec( dllimport )
#endif // !defined(BUILD_AMSMISCUTIL)

#endif // !defined(AMSMISCUTIL_EXPORT_H)
