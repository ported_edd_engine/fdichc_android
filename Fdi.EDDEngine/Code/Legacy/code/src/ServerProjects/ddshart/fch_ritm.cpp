/**
 *	@(#) $Id: fch_ritm.c,v 1.1 1995/05/02 20:44:43 donmarq Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	This file contains the functions for the ROD Item Fetch
 */

/* #includes */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"


#pragma warning (disable : 4131)

#ifdef ISPTEST
#include "tst_fail.h"
#endif

/* #defines */

#define	MAX_LENGTH_MASK		~(ULONG_MAX >> LENGTH_SHIFT)
#define	LENGTH_ENCODE_MASK	~LENGTH_MASK

/* typedefs */

/* global variables */


/* local procedure declarations */

static int      get_rod_item
P((ENV_INFO *, ROD_HANDLE, OBJECT_INDEX, SCRATCH_PAD *, unsigned long,
		unsigned long *, void *, unsigned short));

typedef int (*attach_attr_fn_type)(unsigned char*, unsigned long, SCRATCH_PAD*, void*, unsigned short);

/*********************************************************************
 *
 *	Name:	get_extn_hdr
 *
 *	ShortDesc: Get non-attribute fields of object extension
 *
 *	Description:
 *		Retrieves the Item Type, Item Id, and Item Mask for an object
 *		extension and calculates the number of bytes for these fields
 *
 *	Inputs:
 *		obj_extn_ptr -	pointer to start of object extension
 *		itype -			type of item.  An error is returned if the
 *						the value obtained for item_type does not
 *						match this
 *
 *	Outputs:
 *		obj_extn_ptr -	points to first attribute in object extension
 *      item_info -     pointer to structure containing the items
 *                      preceding the attributes in the object
 *                      extension
 *		extn_length -	pointer to length of object extension
 *		item_type -		pointer to value for Item Type
 *		item_subtype -	pointer to value for Item Subtype
 *		item_id -		pointer to value for Item ID
 *		item_mask -		pointer to value for Item Mask
 *		hdr_length -	number of bytes in object extension header
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_ITEM_TYPE_MISMATCH
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_extn_hdr(
ROD_HANDLE      rhandle,
unsigned char **obj_extn_ptr,
unsigned char  *extn_length,
unsigned char  *item_type,
unsigned char  *item_subtype,
unsigned long  *item_id,
unsigned long  *item_mask,
unsigned char  *hdr_length,
ITEM_TYPE       itype)
{
	unsigned char  *local_extn_ptr;	/* points to start of extension header */
	unsigned long   obj_item_id;	/* temporary variable to unpack item ID */
	unsigned long   obj_item_mask;	/* temporary variable to unpack item
					 * mask */
	unsigned char   mask_size;	/* number of octets of item mask
					 * (depends on item type) */
	int             i;

#ifdef ISPTEST
	TEST_FAIL(GET_EXTN_HDR);
#endif

	/*
	 * Extract the extension length, item type, item sub-type, and item
	 * ID fields. Compare the item type in the object extension with
	 * the requested item type and return if these do not match. If the
	 * object is external, skip the item mask extraction since there
	 * isn't one.
	 */

	local_extn_ptr = *obj_extn_ptr;

	*extn_length = *(local_extn_ptr++);
	if (!*extn_length) {
		return(FETCH_NO_OBJ_EXTN);
	}
	*item_type = *(local_extn_ptr++);
	if (itype != (unsigned short) *item_type) {
		return (FETCH_ITEM_TYPE_MISMATCH);
	}
	*item_subtype = *(local_extn_ptr++);

	/*
	 * Extract 4 bytes for the Item ID
	 */

	obj_item_id = 0L;
	for (i = 0; i < 4; i++) {
		obj_item_id = (obj_item_id << 8) |
			(unsigned long) *(local_extn_ptr++);
	}
	*item_id = obj_item_id;

	/*
	 * Extract the appropriate item mask for the item type and set the
	 * pointer to the first attribute
	 */

	const unsigned char maskSizes[] = { MskSzArr };

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);
	if (tok6or8)
	{
		mask_size = maskSizes[itype];
	}
	else
	{
		switch (itype) {

		case VARIABLE_ITYPE:
			mask_size = VAR_ATTR_MASK_SIZE;
			break;
		case BLOCK_ITYPE:
			mask_size = BLOCK_ATTR_MASK_SIZE;
			break;
		default:
			mask_size = MIN_ATTR_MASK_SIZE;

		}		
	}
	
	obj_item_mask = 0L;
	if (obj_item_id == 0L) {/* No mask for External objects */
		*hdr_length = (unsigned char) ATTR_MASK_OFFSET;
		*item_mask = 0L;
	}
	else {
		for (i = 0; i < mask_size; i++) {
			obj_item_mask = (obj_item_mask << 8) |
				(unsigned long) *(local_extn_ptr++);
		}
		*hdr_length =
			(unsigned char) (ATTR_MASK_OFFSET + mask_size);
		*item_mask = obj_item_mask;
	}

	/*
	 * Advance the object extension pointer to the first attribute field
	 */

	*obj_extn_ptr = local_extn_ptr;

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name:	count_bits
 *
 *	ShortDesc: 	Calculates number of bits set
 *
 *	Description:
 *		Sums the number of bits set in an unsigned long field.
 *
 *	Inputs:
 *		bfield -	unsigned long value
 *
 *	Outputs:
 *
 *	Returns:
 *		bit_cnt -	total number of bits set
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static unsigned short
count_bits(
unsigned long   bfield)
{
	unsigned short  bit_cnt;

	for (bit_cnt = 0; bfield != 0; bfield >>= 1) {
		if (bfield & 1L) {
			bit_cnt++;
		}
	}
	return (bit_cnt);
}

/*********************************************************************
 *
 *	Name: parse_attribute_id
 *
 *	ShortDesc: Get values from Attribute Identifier
 *
 *	Description:
 *		Parse the Attribute Identifier portion of the item attribute
 *		into the RI, tag and length values
 *
 *	Inputs:
 *		attr_ptr -		pointer to first byte of Attribute ID
 *
 *	Outputs:
 *		attr_ptr -		pointer to first byte after Attribute ID
 *		attr_RI -		pointer to value of attribute reference
 *						indicator
 *		attr_tag -		pointer to value of attribute tag
 *		attr_length -	pointer to value of attribute data length
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_ATTR_LENGTH_OVERFLOW
 *		FETCH_ATTR_ZERO_LENGTH
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
parse_attribute_id(
unsigned char **attr_ptr,
unsigned short *attr_RI,
unsigned short *attr_tag,
unsigned long  *attr_len)
{
	unsigned char  *local_attr_ptr;	/* points to current attribute in
					 * object extension */
	unsigned short  temp;	/* used to unpack attribute ID field */
	unsigned long   calc_length;	/* used to unpack attribute length */

	/*
	 * Check for valid parameters
	 */

	ASSERT_RET(attr_ptr && attr_RI && attr_tag && attr_len,
		FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(PARSE_ATTRIBUTE_ID);
#endif

	/*
	 * Extract the RI and tag from the Attribute Identifier (octet #1).
	 * Check the validity of the input parameters before using any of the
	 * values or references.
	 */

	local_attr_ptr = *attr_ptr;
	ASSERT_RET(local_attr_ptr,
		FETCH_INVALID_PARAM);

	/*
	 * The attribute RI is in the MS 2 bits of the Attribute Identifier
	 * field while the attribute tag is in the LS 6 bits.
	 */

	temp = (unsigned short) *local_attr_ptr++;
#ifndef SUN
#pragma warning (disable : 4244)
#endif
	*attr_tag = temp & ATTR_TAG_MASK;
	*attr_RI = (temp >> REF_INDICATOR_SHIFT) & REF_INDICATOR_MASK;
#ifndef SUN
#pragma warning (default : 4244)
#endif

	/*
	 * If bit 8 of the length byte is set, the length is encoded into the
	 * LS 7 bits of this byte and subsequent bytes.  Bit 8 of the last byte
	 * of the encoded sequence is 0.  The upper 7 bits of the calculated
	 * length are monitored during unpacking and a length overflow error is
	 * returned if these are nonzero.
	 */

	calc_length = 0L;
	do {
		if (calc_length & MAX_LENGTH_MASK) {
			return (FETCH_ATTR_LENGTH_OVERFLOW);
		}
		calc_length = (calc_length << LENGTH_SHIFT) |
			(unsigned long) (LENGTH_MASK & *local_attr_ptr);
	} while (LENGTH_ENCODE_MASK & *local_attr_ptr++);

	*attr_len = calc_length;
	if (!calc_length) {
		return (FETCH_ATTR_ZERO_LENGTH);
	}

	*attr_ptr = local_attr_ptr;
	return (SUCCESS);

}

/*********************************************************************
 *
 *	Name: align_depbin
 *
 *	ShortDesc: Locates DEPBIN structure on correct address boundary
 *
 *	Description:
 *		This function computes the next available location in the
 *		scratchpad to place a DEPBIN structure.  The address of the
 *		structure must be on the correct boundary, so an offset is
 *		calculated if the next available scratchpad memory location
 *		is not on such a boundary
 *
 *	Inputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and any external objects
 *						and data referenced by the object.
 *
 *	Outputs:
 *		db_align_ptr -	address-aligned pointer to scratchpad
 *						location for DEPBIN structure
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
align_depbin(
SCRATCH_PAD    *scrpad,
DEPBIN        **db_align_ptr)
{
	unsigned long   sp_aligned_offset;	/* offset necessary to align
						 * DEPBIN structure in the
						 * scratchpad */
	unsigned long   sp_unaligned;	/* used to compute offset */
	unsigned long   db_size;/* amount of scratchpad memory reserved,
				 * including offset */

	/*
	 * Calculate the offset required to align a DEPBIN structure on the
	 * correct address boundary in the scratchpad
	 */

	sp_unaligned = scrpad->used % sizeof(DEPBIN *);
	sp_aligned_offset = (!sp_unaligned) ?
		0L : (sizeof(DEPBIN *) - sp_unaligned);

	/*
	 * Calculate the amount of scratchpad required for the DEPBIN structure
	 * plus any offset for address alignment.  Return if not enough
	 * scratchpad is available.
	 */

	db_size = ((unsigned long) sizeof(DEPBIN)) + sp_aligned_offset;
	if ((scrpad->size - scrpad->used) < db_size) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}

	/*
	 * Return the pointer to the address-aligned location in the scratchpad
	 * for the DEPBIN structure.  The amount of memory used in the
	 * scratchpad is adjusted accordingly.
	 */

	*db_align_ptr =
		(DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
	scrpad->used += db_size;

	/*
	 * Force the DEPBIN structure to all 0's
	 */

	memset((char *) *db_align_ptr, 0, sizeof(DEPBIN));

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name:	attach_var_data
 *
 *	ShortDesc:	Attach binary (attribute) to flat structure
 *
 *	Description: Posts the pointer for an item attribute to
 *				 its corresponding DEPBIN pointer in the flat
 *				 structure.
 *
 *	Inputs:
 *		attr_data_ptr - pointer to attribute data in the
 *						scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
attach_var_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_VAR       *var_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail;
	unsigned long   sp_aligned_offset;
	unsigned long   db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointers */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	var_flat = (FLAT_VAR *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer arrays.
	 * These must be reserved on the scratchpad before the DEPBIN
	 * structures for each attribute can be created. Return if there is not
	 * enough scratchpad memory to reserve the array.  For the Variables
	 * flat structure only, the sub-structures var_flat->misc and
	 * var_flat->actions must already exist.
	 */

	switch (tag) {

	case VAR_UNIT_ID:
	case VAR_READ_TIME_OUT_ID:
	case VAR_WRITE_TIME_OUT_ID:
	case VAR_VALID_ID:
	case VAR_MIN_VAL_ID:
	case VAR_MAX_VAL_ID:
	case VAR_SCALE_ID:
	case VAR_DEBUG_ID:
	case VAR_DEFAULT_VALUE_ID:
	case VAR_TIME_FORMAT_ID:									// timj 4jan07 added
	case VAR_TIME_SCALE_ID:
		ASSERT_RET(var_flat->misc, FETCH_NULL_POINTER);
		if (!var_flat->misc->depbin) {
			sp_avail = scrpad->size - scrpad->used;
			sp_unaligned = scrpad->used % sizeof(VAR_MISC_DEPBIN *);
			sp_aligned_offset = (!sp_unaligned) ?
				0L : (sizeof(VAR_MISC_DEPBIN *) - sp_unaligned);
			db_array_size =
				((unsigned long) sizeof(VAR_MISC_DEPBIN)) + sp_aligned_offset;
			if (sp_avail < db_array_size) {
				return (FETCH_INSUFFICIENT_SCRATCHPAD);
			}

			/*
			 * Reserve the DEPBIN pointer array on the scratchpad
			 * and assign the pointer to the flat structure.
			 */

			var_flat->misc->depbin =
				(VAR_MISC_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
			scrpad->used += db_array_size;

			/*
			 * Force the DEPBIN pointer array to all 0's
			 */

			memset((char *) var_flat->misc->depbin, 0, sizeof(VAR_MISC_DEPBIN));
		}
		break;

	case VAR_PRE_READ_ACT_ID:
	case VAR_POST_READ_ACT_ID:
	case VAR_PRE_WRITE_ACT_ID:
	case VAR_POST_WRITE_ACT_ID:
    case VAR_REFRESH_ACT_ID:
	case VAR_PRE_EDIT_ACT_ID:
	case VAR_POST_EDIT_ACT_ID:
		ASSERT_RET(var_flat->actions, FETCH_NULL_POINTER);
		if (!var_flat->actions->depbin) {
			sp_avail = scrpad->size - scrpad->used;
			sp_unaligned = scrpad->used % sizeof(VAR_ACTIONS_DEPBIN *);
			sp_aligned_offset = (!sp_unaligned) ?
				0L : (sizeof(VAR_ACTIONS_DEPBIN *) - sp_unaligned);
			db_array_size =
				((unsigned long) sizeof(VAR_ACTIONS_DEPBIN)) + sp_aligned_offset;
			if (sp_avail < db_array_size) {
				return (FETCH_INSUFFICIENT_SCRATCHPAD);
			}

			/*
			 * Reserve the DEPBIN pointer array on the scratchpad
			 * and assign the pointer to the flat structure.
			 */

			var_flat->actions->depbin =
				(VAR_ACTIONS_DEPBIN *)(scrpad->pad + scrpad->used + sp_aligned_offset);
			scrpad->used += db_array_size;

			/*
			 * Force the DEPBIN pointer array to all 0's
			 */

			memset((char *) var_flat->actions->depbin, 0, sizeof(VAR_ACTIONS_DEPBIN));
		}
		break;

	default:
		if (!(var_flat->depbin)) {
			sp_avail = scrpad->size - scrpad->used;
			sp_unaligned = scrpad->used % sizeof(VAR_DEPBIN *);
			sp_aligned_offset = (!sp_unaligned) ?
				0L : (sizeof(VAR_DEPBIN *) - sp_unaligned);
			db_array_size =
				((unsigned long) sizeof(VAR_DEPBIN)) + sp_aligned_offset;
			if (sp_avail < db_array_size) {
				return (FETCH_INSUFFICIENT_SCRATCHPAD);
			}

			/*
			 * Reserve the DEPBIN pointer array on the scratchpad
			 * and assign the pointer to the flat structure.
			 */

			var_flat->depbin =
				(VAR_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
			scrpad->used += db_array_size;

			/*
			 * Force the DEPBIN pointer array to all 0's
			 */

			memset((char *) var_flat->depbin, 0, sizeof(VAR_DEPBIN));
		}

	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	switch (tag) {

	case VAR_CLASS_ID:
		depbin_ptr = &var_flat->depbin->db_class;
		break;
	case VAR_HANDLING_ID:
		depbin_ptr = &var_flat->depbin->db_handling;
		break;
	case VAR_UNIT_ID:
		depbin_ptr = &var_flat->misc->depbin->db_unit;
		break;
	case VAR_LABEL_ID:
		depbin_ptr = &var_flat->depbin->db_label;
		break;
	case VAR_HELP_ID:
		depbin_ptr = &var_flat->depbin->db_help;
		break;
	case VAR_READ_TIME_OUT_ID:
		depbin_ptr = &var_flat->misc->depbin->db_read_time_out;
		break;
	case VAR_WRITE_TIME_OUT_ID:
		depbin_ptr = &var_flat->misc->depbin->db_write_time_out;
		break;
	case VAR_VALID_ID:
		depbin_ptr = &var_flat->misc->depbin->db_valid;
		break;
	case VAR_PRE_READ_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_pre_read_act;
		break;
	case VAR_POST_READ_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_post_read_act;
		break;
	case VAR_PRE_WRITE_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_pre_write_act;
		break;
	case VAR_POST_WRITE_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_post_write_act;
		break;
	case VAR_REFRESH_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_refresh_act;
		break;
	case VAR_PRE_EDIT_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_pre_edit_act;
		break;
	case VAR_POST_EDIT_ACT_ID:
		depbin_ptr = &var_flat->actions->depbin->db_post_edit_act;
		break;
	case VAR_RESP_CODES_ID:
		depbin_ptr = &var_flat->depbin->db_resp_codes;
		break;
	case VAR_TYPE_SIZE_ID:
		depbin_ptr = &var_flat->depbin->db_type_size;
		break;
	case VAR_DISPLAY_ID:
		depbin_ptr = &var_flat->depbin->db_display;
		break;
	case VAR_EDIT_ID:
		depbin_ptr = &var_flat->depbin->db_edit;
		break;
	case VAR_MIN_VAL_ID:
		depbin_ptr = &var_flat->misc->depbin->db_min_val;
		break;
	case VAR_MAX_VAL_ID:
		depbin_ptr = &var_flat->misc->depbin->db_max_val;
		break;
	case VAR_SCALE_ID:
		depbin_ptr = &var_flat->misc->depbin->db_scale;
		break;
	case VAR_ENUMS_ID:
		depbin_ptr = &var_flat->depbin->db_enums;
		break;
	case VAR_INDEX_ITEM_ARRAY_ID:
		depbin_ptr = &var_flat->depbin->db_index_item_array;
		break;
	case VAR_DEBUG_ID:
		depbin_ptr = &var_flat->misc->depbin->db_debug_info;
		break;
	case VAR_DEFAULT_VALUE_ID:
		depbin_ptr = &var_flat->depbin->db_default_value;
		break;
	case VAR_TIME_FORMAT_ID:
		depbin_ptr = &var_flat->misc->depbin->db_time_format;
		break;
	case VAR_TIME_SCALE_ID:
		depbin_ptr = &var_flat->misc->depbin->db_time_scale;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}			/* end switch */

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the .bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	var_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:	attach_block_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_block_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_BLOCK     *block_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	block_flat = (FLAT_BLOCK *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!block_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(BLOCK_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(BLOCK_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(BLOCK_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		block_flat->depbin =
			(BLOCK_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) block_flat->depbin, 0, sizeof(BLOCK_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	switch (tag) {

	case BLOCK_CHARACTERISTIC_ID:
		depbin_ptr = &block_flat->depbin->db_characteristic;
		break;
	case BLOCK_LABEL_ID:
		depbin_ptr = &block_flat->depbin->db_label;
		break;
	case BLOCK_HELP_ID:
		depbin_ptr = &block_flat->depbin->db_help;
		break;
	case BLOCK_PARAM_ID:
		depbin_ptr = &block_flat->depbin->db_param;
		break;
	case BLOCK_MENU_ID:
		depbin_ptr = &block_flat->depbin->db_menu;
		break;
	case BLOCK_EDIT_DISP_ID:
		depbin_ptr = &block_flat->depbin->db_edit_disp;
		break;
	case BLOCK_METHOD_ID:
		depbin_ptr = &block_flat->depbin->db_method;
		break;
	case BLOCK_UNIT_ID:
		depbin_ptr = &block_flat->depbin->db_unit;
		break;
	case BLOCK_REFRESH_ID:
		depbin_ptr = &block_flat->depbin->db_refresh;
		break;
	case BLOCK_WAO_ID:
		depbin_ptr = &block_flat->depbin->db_wao;
		break;
	case BLOCK_COLLECT_ID:
		depbin_ptr = &block_flat->depbin->db_collect;
		break;
	case BLOCK_ITEM_ARRAY_ID:
		depbin_ptr = &block_flat->depbin->db_item_array;
		break;
	case BLOCK_PARAM_LIST_ID:
		depbin_ptr = &block_flat->depbin->db_param_list;
		break;
	case BLOCK_DEBUG_ID:
		depbin_ptr = &block_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}			/* end switch */

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	block_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_menu_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_menu_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_MENU      *menu_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	menu_flat = (FLAT_MENU *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!menu_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(MENU_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(MENU_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(MENU_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		menu_flat->depbin =
			(MENU_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) menu_flat->depbin, 0, sizeof(MENU_DEPBIN));
	}


	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case MENU_LABEL_ID:
		depbin_ptr = &menu_flat->depbin->db_label;
		break;
	case MENU_HELP_ID:
		depbin_ptr = &menu_flat->depbin->db_help;
		break;
	case MENU_ITEMS_ID:
		depbin_ptr = &menu_flat->depbin->db_items;
		break;

	case MENU_VALID_ID:
		depbin_ptr = &menu_flat->depbin->db_valid;
		break;

	case MENU_STYLE_ID:
		depbin_ptr = &menu_flat->depbin->db_style;
		break;

	case MENU_DEBUG_ID:
		depbin_ptr = &menu_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	menu_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_edit_disp_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_edit_disp_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_EDIT_DISPLAY *edit_disp_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	edit_disp_flat = (FLAT_EDIT_DISPLAY *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!edit_disp_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(EDIT_DISPLAY_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(EDIT_DISPLAY_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(EDIT_DISPLAY_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		edit_disp_flat->depbin =
			(EDIT_DISPLAY_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) edit_disp_flat->depbin, 0, sizeof(EDIT_DISPLAY_DEPBIN));
	}


	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case EDIT_DISPLAY_LABEL_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_label;
		break;
	case EDIT_DISPLAY_HELP_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_help;
		break;
	case EDIT_DISPLAY_VALID_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_valid;
		break;
	case EDIT_DISPLAY_EDIT_ITEMS_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_edit_items;
		break;
	case EDIT_DISPLAY_DISP_ITEMS_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_disp_items;
		break;
	case EDIT_DISPLAY_PRE_EDIT_ACT_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_pre_edit_act;
		break;
	case EDIT_DISPLAY_POST_EDIT_ACT_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_post_edit_act;
		break;
	case EDIT_DISPLAY_DEBUG_ID:
		depbin_ptr = &edit_disp_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	edit_disp_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_method_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_method_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_METHOD    *method_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	method_flat = (FLAT_METHOD *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!method_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(METHOD_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(METHOD_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(METHOD_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		method_flat->depbin =
			(METHOD_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) method_flat->depbin, 0, sizeof(METHOD_DEPBIN));
	}


	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case METHOD_CLASS_ID:
		depbin_ptr = &method_flat->depbin->db_class;
		break;
	case METHOD_LABEL_ID:
		depbin_ptr = &method_flat->depbin->db_label;
		break;
	case METHOD_HELP_ID:
		depbin_ptr = &method_flat->depbin->db_help;
		break;
	case METHOD_DEF_ID:
		depbin_ptr = &method_flat->depbin->db_def;
		break;
	case METHOD_VALID_ID:
		depbin_ptr = &method_flat->depbin->db_valid;
		break;
	case METHOD_SCOPE_ID:
		depbin_ptr = &method_flat->depbin->db_scope;
		break;
	case METHOD_TYPE_ID:
		depbin_ptr = &method_flat->depbin->db_type;
		break;
	case METHOD_PARAMS_ID:
		depbin_ptr = &method_flat->depbin->db_params;
		break;
	case METHOD_DEBUG_ID:
		depbin_ptr = &method_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	method_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_refresh_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_refresh_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_REFRESH   *refresh_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	refresh_flat = (FLAT_REFRESH *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!refresh_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(REFRESH_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(REFRESH_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(REFRESH_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		refresh_flat->depbin =
			(REFRESH_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) refresh_flat->depbin, 0, sizeof(REFRESH_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case REFRESH_ITEMS_ID:
		depbin_ptr = &refresh_flat->depbin->db_items;
		break;
	case REFRESH_DEBUG_ID:
		depbin_ptr = &refresh_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	refresh_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_unit_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_unit_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_UNIT      *unit_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	unit_flat = (FLAT_UNIT *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!unit_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(UNIT_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(UNIT_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(UNIT_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		unit_flat->depbin =
			(UNIT_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) unit_flat->depbin, 0, sizeof(UNIT_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case UNIT_ITEMS_ID:
		depbin_ptr = &unit_flat->depbin->db_items;
		break;
	case UNIT_DEBUG_ID:
		depbin_ptr = &unit_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	unit_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_wao_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_wao_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_WAO       *wao_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	wao_flat = (FLAT_WAO *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!wao_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(WAO_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(WAO_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(WAO_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		wao_flat->depbin =
			(WAO_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) wao_flat->depbin, 0, sizeof(WAO_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case WAO_ITEMS_ID:
		depbin_ptr = &wao_flat->depbin->db_items;
		break;
	case WAO_DEBUG_ID:
		depbin_ptr = &wao_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	wao_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_item_array_data
 *
 *  ShortDesc:	Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_item_array_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_ITEM_ARRAY *item_array_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	item_array_flat = (FLAT_ITEM_ARRAY *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!item_array_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(ITEM_ARRAY_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(ITEM_ARRAY_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(ITEM_ARRAY_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		item_array_flat->depbin =
			(ITEM_ARRAY_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) item_array_flat->depbin, 0, sizeof(ITEM_ARRAY_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case ITEM_ARRAY_ELEMENTS_ID:
		depbin_ptr = &item_array_flat->depbin->db_elements;
		break;
	case ITEM_ARRAY_LABEL_ID:
		depbin_ptr = &item_array_flat->depbin->db_label;
		break;
	case ITEM_ARRAY_HELP_ID:
		depbin_ptr = &item_array_flat->depbin->db_help;
		break;
	case ITEM_ARRAY_VALIDITY_ID:
		depbin_ptr = &item_array_flat->depbin->db_valid;
		break;
	case ITEM_ARRAY_DEBUG_ID:
		depbin_ptr = &item_array_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	item_array_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_collection_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_collection_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_COLLECTION *collection_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	collection_flat = (FLAT_COLLECTION *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!collection_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(COLLECTION_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(COLLECTION_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(COLLECTION_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		collection_flat->depbin =
			(COLLECTION_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) collection_flat->depbin, 0, sizeof(COLLECTION_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case COLLECTION_MEMBERS_ID:
		depbin_ptr = &collection_flat->depbin->db_members;
		break;
	case COLLECTION_LABEL_ID:
		depbin_ptr = &collection_flat->depbin->db_label;
		break;
	case COLLECTION_HELP_ID:
		depbin_ptr = &collection_flat->depbin->db_help;
		break;
	case COLLECTION_VALIDITY_ID:
		depbin_ptr = &collection_flat->depbin->db_valid;
		break;
	case COLLECTION_DEBUG_ID:
		depbin_ptr = &collection_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	collection_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_program_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_program_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_PROGRAM   *program_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	program_flat = (FLAT_PROGRAM *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!program_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(PROGRAM_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(PROGRAM_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(PROGRAM_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		program_flat->depbin =
			(PROGRAM_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) program_flat->depbin, 0, sizeof(PROGRAM_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case PROGRAM_ARGS_ID:
		depbin_ptr = &program_flat->depbin->db_args;
		break;
	case PROGRAM_RESP_CODES_ID:
		depbin_ptr = &program_flat->depbin->db_resp_codes;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	program_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_record_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_record_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_RECORD    *record_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	record_flat = (FLAT_RECORD *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!record_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(RECORD_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(RECORD_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(RECORD_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		record_flat->depbin =
			(RECORD_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) record_flat->depbin, 0, sizeof(RECORD_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case RECORD_MEMBERS_ID:
		depbin_ptr = &record_flat->depbin->db_members;
		break;
	case RECORD_LABEL_ID:
		depbin_ptr = &record_flat->depbin->db_label;
		break;
	case RECORD_HELP_ID:
		depbin_ptr = &record_flat->depbin->db_help;
		break;
	case RECORD_RESP_CODES_ID:
		depbin_ptr = &record_flat->depbin->db_resp_codes;
		break;
	case RECORD_DEBUG_ID:
		depbin_ptr = &record_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	record_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_array_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_array_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_ARRAY     *array_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	array_flat = (FLAT_ARRAY *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!array_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(ARRAY_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(ARRAY_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(ARRAY_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		array_flat->depbin =
			(ARRAY_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) array_flat->depbin, 0, sizeof(ARRAY_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case ARRAY_LABEL_ID:
		depbin_ptr = &array_flat->depbin->db_label;
		break;
	case ARRAY_HELP_ID:
		depbin_ptr = &array_flat->depbin->db_help;
		break;
	case ARRAY_TYPE_ID:
		depbin_ptr = &array_flat->depbin->db_type;
		break;
	case ARRAY_NUM_OF_ELEMENTS_ID:
		depbin_ptr = &array_flat->depbin->db_num_of_elements;
		break;
	case ARRAY_RESP_CODES_ID:
		depbin_ptr = &array_flat->depbin->db_resp_codes;
		break;
	case ARRAY_VALID_ID:
		depbin_ptr = &array_flat->depbin->db_valid;
		break;
	case ARRAY_DEBUG_ID:
		depbin_ptr = &array_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	array_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_var_list_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_var_list_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_VAR_LIST  *var_list_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	var_list_flat = (FLAT_VAR_LIST *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!var_list_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(VAR_LIST_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(VAR_LIST_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(VAR_LIST_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		var_list_flat->depbin =
			(VAR_LIST_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) var_list_flat->depbin, 0, sizeof(VAR_LIST_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.
	 */

	switch (tag) {

	case VAR_LIST_MEMBERS_ID:
		depbin_ptr = &var_list_flat->depbin->db_members;
		break;
	case VAR_LIST_LABEL_ID:
		depbin_ptr = &var_list_flat->depbin->db_label;
		break;
	case VAR_LIST_HELP_ID:
		depbin_ptr = &var_list_flat->depbin->db_help;
		break;
	case VAR_LIST_RESP_CODES_ID:
		depbin_ptr = &var_list_flat->depbin->db_resp_codes;
		break;
	case VAR_LIST_DEBUG_ID:
		depbin_ptr = &var_list_flat->depbin->db_debug_info;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	var_list_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_resp_codes_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_resp_codes_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_RESP_CODE *resp_codes_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	resp_codes_flat = (FLAT_RESP_CODE *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!resp_codes_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(RESP_CODE_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(RESP_CODE_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(RESP_CODE_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		resp_codes_flat->depbin =
			(RESP_CODE_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) resp_codes_flat->depbin, 0, sizeof(RESP_CODE_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	switch (tag) {

	case RESP_CODE_MEMBER_ID:
		depbin_ptr = &resp_codes_flat->depbin->db_member;
		break;
	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	resp_codes_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_command_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      Chris Gustafson
 *
 *********************************************************************/

static int
attach_command_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_COMMAND *command_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	command_flat = (FLAT_COMMAND *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!command_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(COMMAND_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(COMMAND_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(COMMAND_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		command_flat->depbin =
			(COMMAND_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) command_flat->depbin, 0, sizeof(COMMAND_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	switch (tag) {

	case COMMAND_NUMBER_ID:
		depbin_ptr = &command_flat->depbin->db_number;
		break;

	case COMMAND_OPER_ID:
		depbin_ptr = &command_flat->depbin->db_oper;
		break;

	case COMMAND_TRANS_ID:
		depbin_ptr = &command_flat->depbin->db_trans;
		break;

	case COMMAND_RESP_CODES_ID:
		depbin_ptr = &command_flat->depbin->db_resp_codes;
		break;

	case COMMAND_DEBUG_ID:
		depbin_ptr = &command_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	command_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *  Name:   attach_domain_data
 *
 *  ShortDesc:  Attach binary (attribute) to flat structure
 *
 *  Description: Posts the pointer for an item attribute to
 *               its corresponding DEPBIN pointer in the flat
 *               structure.
 *
 *  Inputs:
 *      attr_data_ptr - pointer to attribute data in the
 *                      scratchpad
 *		data_len -		length of the attribute data
 *      tag - 			the attribute type
 *
 *  Outputs:
 *      flats -			pointer to union of flat structures for the
 *                  	items with DEPBIN pointer attached
 *
 *  Returns:
 *      SUCCESS
 *		FETCH_INVALID_ATTRIBUTE
 *
 *  Author:
 *      David Bradsher
 *
 *********************************************************************/

static int
attach_domain_data(
unsigned char  *attr_data_ptr,
unsigned long   data_len,
SCRATCH_PAD    *scrpad,
void           *flats,
unsigned short  tag)
{
	FLAT_DOMAIN    *domain_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	domain_flat = (FLAT_DOMAIN *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!domain_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(DOMAIN_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(DOMAIN_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(DOMAIN_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		domain_flat->depbin =
			(DOMAIN_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		memset((char *) domain_flat->depbin, 0, sizeof(DOMAIN_DEPBIN));
	}


	/*
	 * Select the type of attribute and attach the address in the
	 * scratchpad to the corresponding DEPBIN pointer in the flat
	 * structure.  If the DEPBIN structure pointer is null, reserve the
	 * space for it on the scratchpad and set the pointer in the DEPBIN
	 * array.
	 */

	switch (tag) {

	case DOMAIN_HANDLING_ID:
		depbin_ptr = &domain_flat->depbin->db_handling;
		break;

	case DOMAIN_RESP_CODES_ID:
		depbin_ptr = &domain_flat->depbin->db_resp_codes;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);

	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	domain_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}


static int
attach_axis_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_AXIS    *axis_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	axis_flat = (FLAT_AXIS *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!axis_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(AXIS_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(AXIS_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(AXIS_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		axis_flat->depbin =
			(AXIS_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) axis_flat->depbin, 0, sizeof(AXIS_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case AXIS_LABEL_ID:
		depbin_ptr = &axis_flat->depbin->db_label;
		break;

	case AXIS_HELP_ID:
		depbin_ptr = &axis_flat->depbin->db_help;
		break;

	case AXIS_MINVAL_ID:
		depbin_ptr = &axis_flat->depbin->db_min_value;
		break;

	case AXIS_MAXVAL_ID:
		depbin_ptr = &axis_flat->depbin->db_max_value;
		break;
		
	case AXIS_MAX_VAL_REF_ID:
		depbin_ptr = &axis_flat->depbin->db_max_value;
		break;

	case AXIS_MIN_VAL_REF_ID:
		depbin_ptr = &axis_flat->depbin->db_min_value;
		break;

	case AXIS_SCALING_ID:
		depbin_ptr = &axis_flat->depbin->db_scaling;
		break;

	case AXIS_CONSTUNIT_ID:
		depbin_ptr = &axis_flat->depbin->db_constant_unit;
		break;

	case AXIS_VALID_ID:
		depbin_ptr = &axis_flat->depbin->db_valid;
		break;

	case AXIS_DEBUG_ID:
		depbin_ptr = &axis_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	axis_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_chart_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_CHART    *chart_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	chart_flat = (FLAT_CHART *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!chart_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(CHART_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(CHART_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(CHART_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		chart_flat->depbin =
			(CHART_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) chart_flat->depbin, 0, sizeof(CHART_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case CHART_LABEL_ID:
		depbin_ptr = &chart_flat->depbin->db_label;
		break;

	case CHART_HELP_ID:
		depbin_ptr = &chart_flat->depbin->db_help;
		break;

	case CHART_MEMBERS_ID:
		depbin_ptr = &chart_flat->depbin->db_members;
		break;

	case CHART_CYCLETIME_ID:
		depbin_ptr = &chart_flat->depbin->db_cycle_time;
		break;

	case CHART_HEIGHT_ID:
		depbin_ptr = &chart_flat->depbin->db_height;
		break;

	case CHART_WIDTH_ID:
		depbin_ptr = &chart_flat->depbin->db_width;
		break;

	case CHART_LENGTH_ID:
		depbin_ptr = &chart_flat->depbin->db_length;
		break;

	case CHART_TYPE_ID:
		depbin_ptr = &chart_flat->depbin->db_type;
		break;

	case CHART_VALID_ID:
		depbin_ptr = &chart_flat->depbin->db_valid;
		break;

	case CHART_DEBUG_ID:
		depbin_ptr = &chart_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	chart_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_file_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_FILE    *file_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	file_flat = (FLAT_FILE *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!file_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(FILE_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(FILE_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(FILE_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		file_flat->depbin =
			(FILE_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) file_flat->depbin, 0, sizeof(FILE_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case FILE_LABEL_ID:
		depbin_ptr = &file_flat->depbin->db_label;
		break;

	case FILE_HELP_ID:
		depbin_ptr = &file_flat->depbin->db_help;
		break;

	case FILE_MEMBERS_ID:
		depbin_ptr = &file_flat->depbin->db_members;
		break;
	case FILE_DEBUG_ID:
		depbin_ptr = &file_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	file_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_graph_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_GRAPH    *graph_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	graph_flat = (FLAT_GRAPH *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!graph_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(GRAPH_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(GRAPH_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(GRAPH_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		graph_flat->depbin =
			(GRAPH_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) graph_flat->depbin, 0, sizeof(GRAPH_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case GRAPH_LABEL_ID:
		depbin_ptr = &graph_flat->depbin->db_label;
		break;

	case GRAPH_HELP_ID:
		depbin_ptr = &graph_flat->depbin->db_help;
		break;

	case GRAPH_MEMBERS_ID:
		depbin_ptr = &graph_flat->depbin->db_members;
		break;

	case GRAPH_HEIGHT_ID:
		depbin_ptr = &graph_flat->depbin->db_height;
		break;

	case GRAPH_WIDTH_ID:
		depbin_ptr = &graph_flat->depbin->db_width;
		break;

	case GRAPH_XAXIS_ID:
		depbin_ptr = &graph_flat->depbin->db_x_axis;
		break;

	case GRAPH_VALID_ID:
		depbin_ptr = &graph_flat->depbin->db_valid;
		break;

	case GRAPH_CYCLETIME_ID:
		depbin_ptr = &graph_flat->depbin->db_cycle_time;
		break;

	case GRAPH_DEBUG_ID:
		depbin_ptr = &graph_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	graph_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}


static int
attach_grid_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_GRID    *grid_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	grid_flat = (FLAT_GRID *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!grid_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(GRID_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(GRID_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(GRID_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		grid_flat->depbin =
			(GRID_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) grid_flat->depbin, 0, sizeof(GRID_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case GRID_LABEL_ID:
		depbin_ptr = &grid_flat->depbin->db_label;
		break;

	case GRID_HELP_ID:
		depbin_ptr = &grid_flat->depbin->db_help;
		break;

	case GRID_MEMBERS_ID:
		depbin_ptr = &grid_flat->depbin->db_vectors;
		break;

	case GRID_HEIGHT_ID:
		depbin_ptr = &grid_flat->depbin->db_height;
		break;

	case GRID_WIDTH_ID:
		depbin_ptr = &grid_flat->depbin->db_width;
		break;

	case GRID_ORIENT_ID:
		depbin_ptr = &grid_flat->depbin->db_orientation;
		break;

	case GRID_HANDLING_ID:
		depbin_ptr = &grid_flat->depbin->db_handling;
		break;

	case GRID_VALID_ID:
		depbin_ptr = &grid_flat->depbin->db_valid;
		break;

	case GRID_DEBUG_ID:
		depbin_ptr = &grid_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	grid_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_image_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_IMAGE    *image_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	image_flat = (FLAT_IMAGE *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!image_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(IMAGE_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(IMAGE_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(IMAGE_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		image_flat->depbin =
			(IMAGE_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) image_flat->depbin, 0, sizeof(IMAGE_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case IMAGE_LABEL_ID:
		depbin_ptr = &image_flat->depbin->db_label;
		break;

	case IMAGE_HELP_ID:
		depbin_ptr = &image_flat->depbin->db_help;
		break;

	case IMAGE_PATH_ID:
		depbin_ptr = &image_flat->depbin->db_path;
		break;

	case IMAGE_LINK_ID:
		depbin_ptr = &image_flat->depbin->db_link;
		break;

	case IMAGE_VALID_ID:
		depbin_ptr = &image_flat->depbin->db_valid;
		break;

	case IMAGE_DEBUG_ID:
		depbin_ptr = &image_flat->depbin->db_debug_info;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	image_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_list_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_LIST    *list_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	list_flat = (FLAT_LIST *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!list_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(LIST_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(LIST_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(LIST_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		list_flat->depbin =
			(LIST_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) list_flat->depbin, 0, sizeof(LIST_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case LIST_LABEL_ID:
		depbin_ptr = &list_flat->depbin->db_label;
		break;

	case LIST_HELP_ID:
		depbin_ptr = &list_flat->depbin->db_help;
		break;

	case LIST_TYPE_ID:
		depbin_ptr = &list_flat->depbin->db_type;
		break;

	case LIST_COUNT_ID:
		depbin_ptr = &list_flat->depbin->db_count;
		break;
		
	case LIST_COUNT_REF_ID:
		depbin_ptr = &list_flat->depbin->db_count;
		break;

	case LIST_CAPACITY_ID:
		depbin_ptr = &list_flat->depbin->db_capacity;
		break;

	case LIST_VALID_ID:
		depbin_ptr = &list_flat->depbin->db_valid;
		break;

	case LIST_DEBUG_ID:
		depbin_ptr = &list_flat->depbin->db_debug_info;
		break;
	
	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	list_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_source_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_SOURCE    *source_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	source_flat = (FLAT_SOURCE *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!source_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(SOURCE_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(SOURCE_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(SOURCE_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		source_flat->depbin =
			(SOURCE_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) source_flat->depbin, 0, sizeof(SOURCE_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case SOURCE_LABEL_ID:
		depbin_ptr = &source_flat->depbin->db_label;
		break;

	case SOURCE_HELP_ID:
		depbin_ptr = &source_flat->depbin->db_help;
		break;

	case SOURCE_EMPHASIS_ID:
		depbin_ptr = &source_flat->depbin->db_emphasis;
		break;

	case SOURCE_LINETYPE_ID:
		depbin_ptr = &source_flat->depbin->db_line_type;
		break;

	case SOURCE_LINECOLOR_ID:
		depbin_ptr = &source_flat->depbin->db_line_color;
		break;

    case SOURCE_MEMBERS_ID:
		depbin_ptr = &source_flat->depbin->db_members;
		break;

	case SOURCE_YAXIS_ID:
		depbin_ptr = &source_flat->depbin->db_y_axis;
		break;

	case SOURCE_VALID_ID:
		depbin_ptr = &source_flat->depbin->db_valid;
		break;

	case SOURCE_DEBUG_ID:
		depbin_ptr = &source_flat->depbin->db_debug_info;
		break;

	case SOURCE_INIT_ACTIONS_ID:
		depbin_ptr = &source_flat->depbin->db_init_actions;
		break;

	case SOURCE_RFRSH_ACTIONS_ID:
		depbin_ptr = &source_flat->depbin->db_refresh_actions;
		break;

	case SOURCE_EXIT_ACTIONS_ID:
		depbin_ptr = &source_flat->depbin->db_exit_actions;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	source_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

static int
attach_waveform_data(unsigned char *attr_data_ptr, unsigned long data_len,
	SCRATCH_PAD *scrpad, void *flats, unsigned short tag)
{

	FLAT_WAVEFORM    *waveform_flat;
	DEPBIN        **depbin_ptr;
	unsigned long   sp_avail, sp_aligned_offset, db_array_size;
	unsigned long   sp_unaligned;
	int             rcode;

	depbin_ptr = (DEPBIN **) 0L;	/* Initialize the DEPBIN pointer */

	/*
	 * Assign the appropriate flat structure pointer
	 */

	waveform_flat = (FLAT_WAVEFORM *) flats;

	/*
	 * Check the flat structure for existence of the DEPBIN pointer array.
	 * This must be reserved on the scratchpad before the DEPBIN structures
	 * for each attribute can be created. Return if there is not enough
	 * scratchpad memory to reserve the array.
	 */

	if (!waveform_flat->depbin) {
		sp_avail = scrpad->size - scrpad->used;
		sp_unaligned = scrpad->used % sizeof(WAVEFORM_DEPBIN *);
		sp_aligned_offset = (!sp_unaligned) ?
			0L : (sizeof(WAVEFORM_DEPBIN *) - sp_unaligned);
		db_array_size =
			((unsigned long) sizeof(WAVEFORM_DEPBIN)) + sp_aligned_offset;
		if (sp_avail < db_array_size) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}

		/*
		 * Reserve the DEPBIN pointer array on the scratchpad and
		 * assign the pointer to the flat structure.
		 */

		waveform_flat->depbin =
			(WAVEFORM_DEPBIN *) (scrpad->pad + scrpad->used + sp_aligned_offset);
		scrpad->used += db_array_size;

		/*
		 * Force the DEPBIN pointer array to all 0's
		 */

		(void)memset((char *) waveform_flat->depbin, 0, sizeof(WAVEFORM_DEPBIN));
	}

	/*
	 * Select the type of attribute and attach the address in the scratchpad
	 * to the corresponding DEPBIN pointer in the flat structure.  If the
	 * DEPBIN structure pointer is null, reserve the space for it on the
	 * scratchpad and set the pointer in the DEPBIN array.
	 */

	switch (tag) {

	case WAVEFORM_LABEL_ID:
		depbin_ptr = &waveform_flat->depbin->db_label;
		break;

	case WAVEFORM_HELP_ID:
		depbin_ptr = &waveform_flat->depbin->db_help;
		break;

	case WAVEFORM_EMPHASIS_ID:
		depbin_ptr = &waveform_flat->depbin->db_emphasis;
		break;

	case WAVEFORM_LINETYPE_ID:
		depbin_ptr = &waveform_flat->depbin->db_line_type;
		break;

	case WAVEFORM_LINECOLOR_ID:
		depbin_ptr = &waveform_flat->depbin->db_line_color;
		break;

	case WAVEFORM_INIT_ACTIONS_ID:
		depbin_ptr = &waveform_flat->depbin->db_init_actions;
		break;

	case WAVEFORM_RFRSH_ACTIONS_ID:
		depbin_ptr = &waveform_flat->depbin->db_refresh_actions;
		break;

	case WAVEFORM_EXIT_ACTIONS_ID:
		depbin_ptr = &waveform_flat->depbin->db_exit_actions;
		break;

	case WAVEFORM_TYPE_ID:
		depbin_ptr = &waveform_flat->depbin->db_type;
		break;

	case WAVEFORM_X_INITIAL_ID:
		depbin_ptr = &waveform_flat->depbin->db_x_initial;
		break;

	case WAVEFORM_X_INCREMENT_ID:
		depbin_ptr = &waveform_flat->depbin->db_x_increment;
		break;

	case WAVEFORM_POINT_COUNT_ID:
		depbin_ptr = &waveform_flat->depbin->db_number_of_points;
		break;

	case WAVEFORM_Y_VALUES_ID:
		depbin_ptr = &waveform_flat->depbin->db_y_values;
		break;

	case WAVEFORM_X_VALUES_ID:
		depbin_ptr = &waveform_flat->depbin->db_x_values;
		break;

	case WAVEFORM_KEYPTS_Y_ID:
		depbin_ptr = &waveform_flat->depbin->db_key_y_values;
		break;

	case WAVEFORM_KEYPTS_X_ID:
		depbin_ptr = &waveform_flat->depbin->db_key_x_values;
		break;

	case WAVEFORM_YAXIS_ID:
		depbin_ptr = &waveform_flat->depbin->db_y_axis;
		break;

	case WAVEFORM_HANDLING_ID:
		depbin_ptr = &waveform_flat->depbin->db_handling;
		break;

	case WAVEFORM_DEBUG_ID:
		depbin_ptr = &waveform_flat->depbin->db_debug_info;
		break;

	case WAVEFORM_VALID_ID:
		depbin_ptr = &waveform_flat->depbin->db_valid;
		break;

	default:
		return (FETCH_INVALID_ATTRIBUTE);
	}

	/*
	 * Attach the data and the data length to the DEPBIN structure. It the
	 * structure does not yet exist, reserve it on the scratchpad first.
	 */

	if (!(*depbin_ptr)) {

		/*
		 * Reserve the DEPBIN structure on the scratchpad and assign
		 * the pointer to the DEPBIN pointer array field.
		 */

		rcode = align_depbin(scrpad, depbin_ptr);
		if (rcode != SUCCESS) {
			return (rcode);
		}
	}
	(*depbin_ptr)->bin_chunk = attr_data_ptr;
	(*depbin_ptr)->bin_size = data_len;

	/*
	 * Set the bin_hooked bit in the flat structure for the appropriate
	 * attribute
	 */

	waveform_flat->masks.bin_hooked |= (1L << tag);

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name: 	get_item_attr
 *
 *	ShortDesc: 	Get attributes for Domain item
 *
 *	Description:
 *		Determines the data type for each requested attribute in the
 *		object extension and passes the pointer to the data to the
 *		attribute attachment routine.
 *
 *  Inputs:
 *      rhandle -       handle for the ROD containing the object
 *      obj -           pointer to the object structure, returned by
						get_rod_object(), containing pointers to the
						object extension and local data area
 *      req_mask -		attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type).  A bit
 *						for a requested attribute is reset after the
 *						binary has been attached.
 *		obj_item_mask -	the item mask field for the current object
 *						extension or, in the case of an externally
 *						referenced object, the item mask of the object
 *						extension which references it.
 *		extn_attr_length  -	length (in octets) of the attribute data.
 *		obj_ext_ptr -	pointer to the first attribute in the object
 *						extension
 *		itype -			the type of the requested item
 *		attach_attr_fn -	a pointer to the appropriate attribute
 *						attach functions for the item type
 *		item_bin_hooked -	the field in the item's flat structure
 *						whose bits indicate which attributes have
 *						already been attached.
 *
 *  Outputs:
 *      scrpad -        pointer to a structure whose members point
 *                      to a scratchpad memory area containing
 *                      the object requested and any external objects
 *                      and data referenced by the object.
 *		req_mask -     	remaining attributes requested for the Item. As
 *                      an attribute is attached, the bit in the mask
 *                      corresponding to the attribute is reset.
 *      flats -    		pointer to the Item attributes in the
 *						scratchpad memory (contents of structure
 *						depend on the Item type)
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INVALID_RI
 *		FETCH_ATTRIBUTE_NO_MASK_BIT
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_item_attr(
ENV_INFO		*env_info,
ROD_HANDLE      rhandle,
OBJECT         *obj,
SCRATCH_PAD    *scrpad,
unsigned long  *req_mask,
unsigned long   obj_item_mask,
void           *flats,
unsigned char   extn_attr_length,
unsigned char  *obj_ext_ptr,
ITEM_TYPE       itype,
unsigned long   item_bin_hooked,
attach_attr_fn_type attach_attr_fn)
{
	unsigned char  *local_data_ptr;
	unsigned char  *obj_attr_ptr;	/* pointer to attributes in object
					 * extension */
	unsigned short  curr_attr_RI;	/* RI for current attribute */
	unsigned short  curr_attr_tag;	/* tag for current attribute */
	unsigned long   curr_attr_length;	/* data length for current
						 * attribute */
	unsigned long   local_req_mask;	/* request mask for base or external
					 * objects */
	unsigned long   attr_mask_bit;	/* bit in item mask corresponding to
					 * current attribute */
	unsigned long   extern_attr_mask;	/* used for recursive call for
						 * External All objects */
	unsigned long   sp_avail;	/* available scratchpad memory */
	unsigned short  extern_obj_index;	/* attribute data field for
						 * object index of External
						 * object */
	unsigned long  attr_offset;	/* attribute data field for offset of
					 * data in local data area */
	int             rcode;

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);
	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(obj_ext_ptr, FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(GET_ITEM_ATTR);
#endif

	/*
	 * Point to the first attribute in the object extension and begin
	 * extracting the attribute data
	 */

	local_req_mask = *req_mask;
	obj_attr_ptr = obj_ext_ptr;

	while ((obj_attr_ptr < (obj_ext_ptr + extn_attr_length)) &&
		local_req_mask) {

		/*
		 * Retrieve the Attribute Identifier information from the
		 * leading attribute bytes.  The object extension pointer will
		 * point to the first byte after the Attribute ID, which will
		 * be Immediate data or will reference Local or External data.
		 */

		rcode = parse_attribute_id(&obj_attr_ptr, &curr_attr_RI,
			&curr_attr_tag, &curr_attr_length);

		if (rcode != SUCCESS) {
			return (rcode);
		}

		/*
		 * Confirm that the current attribute being is matched by a set
		 * bit in the Item Mask.  The mask or the attribute tag is
		 * incorrect if there is not a match.
		 */

		attr_mask_bit = (unsigned long) (1L << curr_attr_tag);
		if (!(obj_item_mask & attr_mask_bit)) {
			return (FETCH_ATTRIBUTE_NO_MASK_BIT);
		}

		/*
		 * Use the Tag field from the Attribute Identifier to determine
		 * if the attribute was requested or not (not applicable to
		 * External All data type).  For all data types except External
		 * All, skip to the next attribute in the extension if not
		 * requested.  Also, skip forward if the attribute has already
		 * been attached.
		 */

		sp_avail = scrpad->size - scrpad->used;
		switch (curr_attr_RI) {

		case RI_IMMEDIATE:
			if (local_req_mask & attr_mask_bit) {
				if (!(item_bin_hooked & attr_mask_bit)) {

					rcode = (*attach_attr_fn) (obj_attr_ptr, curr_attr_length,
						scrpad, flats, curr_attr_tag);

					if (rcode != SUCCESS) {
						goto err_exit;
					}
					else {
						local_req_mask &= ~attr_mask_bit;	/* clear bit */
					}
				}
				else {
					local_req_mask &= ~attr_mask_bit;	/* clear bit */
				}
			}
			obj_attr_ptr += curr_attr_length;

			/*
			 * Check for invalid length that would advance the
			 * object extension pointer past the end of the object
			 * extension
			 */

			if (obj_attr_ptr > (obj_ext_ptr + extn_attr_length)) {
				return (FETCH_INVALID_ATTR_LENGTH);
			}
			break;

		case RI_LOCAL:
			if (local_req_mask & attr_mask_bit) {
				if (!(item_bin_hooked & attr_mask_bit)) {
					if (curr_attr_length > sp_avail) {
						rcode = FETCH_INSUFFICIENT_SCRATCHPAD;
						goto err_exit;
					}
					else {
						local_data_ptr = scrpad->pad + scrpad->used;
#ifndef SUN
#pragma warning (disable : 4244)
#endif
						if (tok6or8)
						{
							attr_offset = 0L;
							do {
								if (attr_offset & MAX_LENGTH_MASK) {
									return (FETCH_ATTR_LENGTH_OVERFLOW);
								}
								attr_offset = (attr_offset << LENGTH_SHIFT) |
									(unsigned long) (LENGTH_MASK & *obj_attr_ptr);
							} while (LENGTH_ENCODE_MASK & *obj_attr_ptr++);
						}
						else
						{
							attr_offset =
								(((unsigned short) obj_attr_ptr[0]) << 8) |
								obj_attr_ptr[1];
							obj_attr_ptr += LOCAL_REF_SIZE;
						}


#ifndef SUN
#pragma warning (default : 4244)
#endif

						/*
						 * Get the local data from the
						 * object structure. Return if
						 * an error occurred.
						 */

						rcode = get_local_data(obj, attr_offset,
							curr_attr_length, scrpad);

						if (rcode != SUCCESS) {
							goto err_exit;
						}

						rcode = (*attach_attr_fn) (local_data_ptr, curr_attr_length,
							scrpad, flats, curr_attr_tag);

						if (rcode != SUCCESS) {
							goto err_exit;
						}
						else {
							local_req_mask &= ~attr_mask_bit;	/* clear bit */
						}
					}
				}
				else {
					local_req_mask &= ~attr_mask_bit;	/* clear bit */
					if (tok6or8)
					{
						attr_offset = 0L;
						do {
							if (attr_offset & MAX_LENGTH_MASK) {
								return (FETCH_ATTR_LENGTH_OVERFLOW);
							}
							attr_offset = (attr_offset << LENGTH_SHIFT) |
								(unsigned long) (LENGTH_MASK & *obj_attr_ptr);
						} while (LENGTH_ENCODE_MASK & *obj_attr_ptr++);
					}
					else
					{
						obj_attr_ptr += LOCAL_REF_SIZE;
					}
				}
			}
			else
			{
				if (tok6or8)
				{
					attr_offset = 0L;
					do {
						if (attr_offset & MAX_LENGTH_MASK) {
							return (FETCH_ATTR_LENGTH_OVERFLOW);
						}
						attr_offset = (attr_offset << LENGTH_SHIFT) |
							(unsigned long) (LENGTH_MASK & *obj_attr_ptr);
					} while (LENGTH_ENCODE_MASK & *obj_attr_ptr++);
				}
				else
				{
					obj_attr_ptr += LOCAL_REF_SIZE;
				}
			}
			break;

		case RI_EXTERNAL_SINGLE:
			if (local_req_mask & attr_mask_bit) {
				if (!(item_bin_hooked & attr_mask_bit)) {
					if (curr_attr_length > sp_avail) {
						*req_mask = local_req_mask;
						return (FETCH_INSUFFICIENT_SCRATCHPAD);
					}
					else {

						/*
						 * Set a request mask with a
						 * single attribute
						 */

						extern_attr_mask = attr_mask_bit;

						/*
						 * Make a recursive call to
						 * get_rod_item() to load the
						 * External object and attach
						 * the attribute(s).
						 */

#ifndef SUN
#pragma warning (disable : 4244)
#endif
						extern_obj_index =
							(((unsigned short) obj_attr_ptr[0]) << 8) |
							obj_attr_ptr[1];
#ifndef SUN
#pragma warning (default : 4244)
#endif
						rcode = get_rod_item(env_info, rhandle, extern_obj_index,
							scrpad, obj_item_mask, &extern_attr_mask,
							flats, itype);

						/*
						 * Continue if there was no
						 * returned error code
						 */

						if (rcode != SUCCESS) {
							goto err_exit;
						}
					}

				}
				local_req_mask &= ~attr_mask_bit;
			}
			obj_attr_ptr += EXTERNAL_REF_SIZE;
			break;

		case RI_EXTERNAL_ALL:

			if (curr_attr_length > sp_avail) {
				*req_mask = local_req_mask;
				return (FETCH_INSUFFICIENT_SCRATCHPAD);
			}
			else {
				extern_attr_mask = local_req_mask;

				/*
				 * Make a recursive call to get_rod_item() to
				 * load the External object and attach the
				 * attribute(s).
				 */

#ifndef SUN
#pragma warning (disable : 4244)
#endif
				extern_obj_index =
					(((unsigned short) obj_attr_ptr[0]) << 8) |
					obj_attr_ptr[1];
#ifndef SUN
#pragma warning (default : 4244)
#endif
				rcode = get_rod_item(env_info, rhandle, extern_obj_index,
					scrpad, obj_item_mask, &extern_attr_mask,
					flats, itype);

				/*
				 * Ignore FETCH_ATTRIBUTE_NOT_FOUND errors in
				 * this context since the request mask may
				 * point to attributes not contained in the
				 * external object.
				 */

				if ((rcode != SUCCESS) &&
					(rcode != FETCH_ATTRIBUTE_NOT_FOUND)) {
					goto err_exit;
				}

				/*
				 * Reduce attribute count by number of
				 * attributes attached from External object
				 */

				local_req_mask &= ~extern_attr_mask;	/* clear attached bits */
			}

			obj_attr_ptr += EXTERNAL_REF_SIZE;
			break;

		default:
			return (FETCH_INVALID_RI);
		}		/* end switch */

	}			/* end while */

	/*
	 * Return the local request mask which will indicate the attributes
	 * that were requested but have not been attached.
	 */

	*req_mask = local_req_mask;
	return (SUCCESS);

err_exit:
	*req_mask = local_req_mask;
	return (rcode);
}

/*********************************************************************
 *
 *	Name:	get_rod_item
 *
 *	ShortDesc:	Get ROD object prior to processing attributes
 *
 *	Description:
 *		This routine is called by the exposed interface as well as by
 *		the item fetch routines which have attributes with External
 *		Single or External All data types.  It calls the ROD manager
 *		to load an object (extension) into the scratchpad.  If the load
 *		is successful, it confirms that the object extension has the
 *		correct item ID and item type, then sets a pointer to the
 *		first attribute in the extension in order to process the
 *		requested item attributes.
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		data_item_mask -	a copy of the Item Mask contained in an
 *						object which references an external object.
 *						Set to 0 if called from the exposed interface
 *						(fch_rod_item()).
 *		attr_req_mask -	attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type)
 *		itype -			identifies the type of Item requested
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and any external objects
 *						and data referenced by the object.
 *		attr_req_mask -	remaining attributes requested for the Item. As
 *                      an attribute is attached, the bit in the mask
 *                      corresponding to the attribute is reset.
 *		flats -			pointer to the flat structure containing the
 *						pointers to the Item attributes in the
 *						scratchpad memory (contents of structure
 *						depend on the Item type)
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_ITEM_TYPE_MISMATCH
 *		FETCH_INVALID_ATTRIBUTE
 *		FETCH_INVALID_RI
 *		FETCH_EMPTY_ITEM_MASK
 *		FETCH_ATTRIBUTE_NOT_FOUND
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

static int
get_rod_item(
ENV_INFO	   *env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
SCRATCH_PAD    *scrpad,
unsigned long   data_item_mask,
unsigned long  *attr_req_mask,
void           *flats,
ITEM_TYPE       itype)
{
	OBJECT         *rod_obj;/* points to ROD object structure, returned
				 * from get_rod_obj(), containing pointers to
				 * the object extension and local data area */
	unsigned char  *obj_extn_ptr;	/* points to start of object extension */
	unsigned char   obj_extn_length;	/* length of object extension */
	unsigned char   obj_item_type;	/* item type contained in object
					 * extension */
	unsigned char	obj_item_subtype;	/* item subtype contained in
					 * object extension */
	unsigned char   obj_hdr_length;	/* length of header portion (length,
					 * type, item ID, & item mask) of
					 * object extension */
	unsigned long   obj_item_mask;	/* item mask contained in object
					 * extension */
	unsigned long   obj_item_id;	/* item ID contained in object
					 * extension */
	unsigned char   attributes_length;	/* length of attributes portion
						 * of object extension */
	unsigned long   sp_avail;	/* available memory in scratchpad */
	unsigned long   local_sp_req;
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
					 * object extension if application
					 * scratchpad is too small */
	unsigned char   pad_buf[MAX_OBJ_EXTN_LEN];	/* holds object
							 * extension to allow
							 * determination of
							 * scratchpad size
							 * (copied to
							 * application
							 * scratchpad if space
							 * permits) */
	int             rcode;

#ifdef ISPTEST
	TEST_FAIL(GET_ROD_ITEM);
#endif

	/*
	 * Check the available scratchpad size.  If there is not enough for a
	 * maximum-length object extension, use the local buffer.  If the
	 * actual size requirement for the object extension is small enough to
	 * fit in the available scratchpad memory, it can be copied in from the
	 * local buffer once it has been loaded from the ROD and the length
	 * determined.
	 */

	sp_avail = scrpad->size - scrpad->used;
	local_sp_req = MAX_OBJ_EXTN_LEN;
	if (sp_avail < local_sp_req) {

		local_scrpad.pad = pad_buf;
		local_scrpad.size = sizeof(pad_buf);
		local_scrpad.used = 0L;
	}
	else {
		local_scrpad.pad = scrpad->pad;
		local_scrpad.size = scrpad->size;
		local_scrpad.used = scrpad->used;

		ASSERT_RET(local_scrpad.pad || !local_scrpad.size,
			FETCH_INVALID_PARAM);
	}

	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = local_scrpad.pad + local_scrpad.used;

	/*
	 * Retrieve the item object.  Return with the resulting error code if
	 * not successful.
	 */

	rcode = get_rod_object(env_info, rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length, item type, item ID, and item mask
	 * fields
	 */

	rcode = get_extn_hdr(rhandle, &obj_extn_ptr, &obj_extn_length,
		&obj_item_type, &obj_item_subtype, &obj_item_id,
		&obj_item_mask, &obj_hdr_length, itype);

	if (rcode != SUCCESS) {
		return (rcode);
	}

#ifndef SUN
#pragma warning (disable : 4244)
#endif
	attributes_length = (obj_extn_length + EXTEN_LENGTH_SIZE) -
		obj_hdr_length;
#ifndef SUN
#pragma warning (default : 4244)
#endif

	/*
	 * Compare the actual object length to the available size.  If the
	 * object was copied into the local buffer and the scratchpad size is
	 * large enough, copy the local buffer into the scratchpad.  If the
	 * scratchpad is still too small, return with a
	 * FETCH_INSUFFICIENT_SCRATCHPAD return code.
	 */

	if (sp_avail < (unsigned long)(obj_extn_length + EXTEN_LENGTH_SIZE)) {
		return (FETCH_INSUFFICIENT_SCRATCHPAD);
	}
	else {
		if (local_scrpad.pad == pad_buf) {
			local_sp_req = local_scrpad.used;	/* Get actual amount
								 * used */

			/*
			 * Application scratchpad has sufficient size to hold
			 * the object extension.  Use it instead of the local
			 * buffer and copy the extension from the local buffer.
			 */

			local_scrpad.pad = scrpad->pad;
			local_scrpad.size = scrpad->size;
			local_scrpad.used = scrpad->used;

			ASSERT_RET(local_scrpad.pad || !local_scrpad.size,
				FETCH_INVALID_PARAM);

			/*
			 * Copy the array (containing the object extension)
			 * into the application scratchpad, beginning at the
			 * first available location.
			 */

			memcpy((char *) (local_scrpad.pad + local_scrpad.used),
				(char *) pad_buf,
				(int) local_sp_req);

			/*
			 * Reset the pointer to the first attribute
			 */

			obj_extn_ptr = local_scrpad.pad + local_scrpad.used +
				obj_hdr_length;

			/*
			 * Increment the amount of scratchpad used by the
			 * actual size of the object extension
			 */

			local_scrpad.used += local_sp_req;
		}
	}

	/*
	 * If the object being retrieved is External, use the data_item_mask
	 * parameter that was passed in place of the value (obj_item_mask)
	 * returned from get_extn_hdr().  Return with an error code if the
	 * item mask is empty (base object only).
	 */

	if (!obj_item_id) {
		if (!data_item_mask) {

			/*
			 * Goes here if External object was retrieved as a Base
			 * object (item ID will be 0 and so will data_item_mask
			 * parameter)
			 */

			return (FETCH_INVALID_ITEM_ID);
		}
		obj_item_mask = data_item_mask;
	}
	else {
		if (!obj_item_mask) {	/* No attributes in object? */
			return (FETCH_EMPTY_ITEM_MASK);
		}
	}

	/*
	 * If the binary does not exist for any of the requested
     * attributes, don`t even think about trying to get it... really
     * 
     */

	*attr_req_mask &= obj_item_mask;

	/*
	 * Assign the pointer to the appropriate flat structure for the item
	 * type requested.  Set the bin_exists and item ID fields in the
	 * flat structure if they are not already set (will already be set
	 * if the current object is External, i.e., obj_item_id is 0).
	 */


	switch (itype) {

	case VARIABLE_ITYPE:
		if (obj_item_id) {
			((FLAT_VAR *) flats)->masks.bin_exists =
					obj_item_mask & VAR_ATTR_MASKS;
			((FLAT_VAR *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= VAR_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, VARIABLE_ITYPE,
			((FLAT_VAR *) flats)->masks.bin_hooked,
			attach_var_data);
		break;

	case MENU_ITYPE:
		if (obj_item_id) {
			((FLAT_MENU *) flats)->masks.bin_exists =
					obj_item_mask & MENU_ATTR_MASKS;
			((FLAT_MENU *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= MENU_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, MENU_ITYPE,
			((FLAT_MENU *) flats)->masks.bin_hooked,
			attach_menu_data);
		break;

	case EDIT_DISP_ITYPE:
		if (obj_item_id) {
			((FLAT_EDIT_DISPLAY *) flats)->masks.bin_exists =
					obj_item_mask & EDIT_DISP_ATTR_MASKS;
			((FLAT_EDIT_DISPLAY *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= EDIT_DISP_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, EDIT_DISP_ITYPE,
			((FLAT_EDIT_DISPLAY *) flats)->masks.bin_hooked,
			attach_edit_disp_data);
		break;

	case METHOD_ITYPE:
		if (obj_item_id) {
			((FLAT_METHOD *) flats)->masks.bin_exists =
					obj_item_mask & METHOD_ATTR_MASKS;
			((FLAT_METHOD *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= METHOD_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, METHOD_ITYPE,
			((FLAT_METHOD *) flats)->masks.bin_hooked,
			attach_method_data);
		break;

	case REFRESH_ITYPE:
		if (obj_item_id) {
			((FLAT_REFRESH *) flats)->masks.bin_exists =
					obj_item_mask & REFRESH_ATTR_MASKS;
			((FLAT_REFRESH *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= REFRESH_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, REFRESH_ITYPE,
			((FLAT_REFRESH *) flats)->masks.bin_hooked,
			attach_refresh_data);
		break;

	case UNIT_ITYPE:
		if (obj_item_id) {
			((FLAT_UNIT *) flats)->masks.bin_exists =
					obj_item_mask & UNIT_ATTR_MASKS;
			((FLAT_UNIT *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= UNIT_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, UNIT_ITYPE,
			((FLAT_UNIT *) flats)->masks.bin_hooked,
			attach_unit_data);
		break;

	case WAO_ITYPE:
		if (obj_item_id) {
			((FLAT_WAO *) flats)->masks.bin_exists =
					obj_item_mask & WAO_ATTR_MASKS;
			((FLAT_WAO *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= WAO_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, WAO_ITYPE,
			((FLAT_WAO *) flats)->masks.bin_hooked,
			attach_wao_data);
		break;

	case ITEM_ARRAY_ITYPE:
		if (obj_item_id) {
			((FLAT_ITEM_ARRAY *) flats)->masks.bin_exists =
					obj_item_mask & ITEM_ARRAY_ATTR_MASKS;
			((FLAT_ITEM_ARRAY *) flats)->id = obj_item_id;
			((FLAT_ITEM_ARRAY *) flats)->subtype =
					(ITEM_TYPE) obj_item_subtype;
		}
		*attr_req_mask &= ITEM_ARRAY_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, ITEM_ARRAY_ITYPE,
			((FLAT_ITEM_ARRAY *) flats)->masks.bin_hooked,
			attach_item_array_data);
		break;

	case COLLECTION_ITYPE:
		if (obj_item_id) {
			((FLAT_COLLECTION *) flats)->masks.bin_exists =
					obj_item_mask & COLLECTION_ATTR_MASKS;
			((FLAT_COLLECTION *) flats)->id = obj_item_id;
			((FLAT_COLLECTION *) flats)->subtype =
					(ITEM_TYPE) obj_item_subtype;
		}
		*attr_req_mask &= COLLECTION_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, COLLECTION_ITYPE,
			((FLAT_COLLECTION *) flats)->masks.bin_hooked,
			attach_collection_data);
		break;

	case BLOCK_ITYPE:
		if (obj_item_id) {
			((FLAT_BLOCK *) flats)->masks.bin_exists =
					obj_item_mask & BLOCK_ATTR_MASKS;
			((FLAT_BLOCK *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= BLOCK_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, BLOCK_ITYPE,
			((FLAT_BLOCK *) flats)->masks.bin_hooked,
			attach_block_data);
		break;

	case PROGRAM_ITYPE:
		if (obj_item_id) {
			((FLAT_PROGRAM *) flats)->masks.bin_exists =
					obj_item_mask & PROGRAM_ATTR_MASKS;
			((FLAT_PROGRAM *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= PROGRAM_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, PROGRAM_ITYPE,
			((FLAT_PROGRAM *) flats)->masks.bin_hooked,
			attach_program_data);
		break;

	case RECORD_ITYPE:
		if (obj_item_id) {
			((FLAT_RECORD *) flats)->masks.bin_exists =
					obj_item_mask & RECORD_ATTR_MASKS;
			((FLAT_RECORD *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= RECORD_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, RECORD_ITYPE,
			((FLAT_RECORD *) flats)->masks.bin_hooked,
			attach_record_data);
		break;

	case ARRAY_ITYPE:
		if (obj_item_id) {
			((FLAT_ARRAY *) flats)->masks.bin_exists =
					obj_item_mask & ARRAY_ATTR_MASKS;
			((FLAT_ARRAY *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= ARRAY_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, ARRAY_ITYPE,
			((FLAT_ARRAY *) flats)->masks.bin_hooked,
			attach_array_data);
		break;

	case VAR_LIST_ITYPE:
		if (obj_item_id) {
			((FLAT_VAR_LIST *) flats)->masks.bin_exists =
					obj_item_mask & VAR_LIST_ATTR_MASKS;
			((FLAT_VAR_LIST *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= VAR_LIST_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, VAR_LIST_ITYPE,
			((FLAT_VAR_LIST *) flats)->masks.bin_hooked,
			attach_var_list_data);
		break;

	case RESP_CODES_ITYPE:
		if (obj_item_id) {
			((FLAT_RESP_CODE *) flats)->masks.bin_exists =
					obj_item_mask & RESP_CODE_ATTR_MASKS;
			((FLAT_RESP_CODE *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= RESP_CODE_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, RESP_CODES_ITYPE,
			((FLAT_RESP_CODE *) flats)->masks.bin_hooked,
			attach_resp_codes_data);
		break;

	case COMMAND_ITYPE:
		if (obj_item_id) {
			((FLAT_COMMAND *) flats)->masks.bin_exists =
					obj_item_mask & COMMAND_ATTR_MASKS;
			((FLAT_COMMAND *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= COMMAND_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, COMMAND_ITYPE,
			((FLAT_COMMAND *) flats)->masks.bin_hooked,
			attach_command_data);
		break;

	case DOMAIN_ITYPE:
		if (obj_item_id) {
			((FLAT_DOMAIN *) flats)->masks.bin_exists =
					obj_item_mask & DOMAIN_ATTR_MASKS;
			((FLAT_DOMAIN *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= DOMAIN_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, DOMAIN_ITYPE,
			((FLAT_DOMAIN *) flats)->masks.bin_hooked,
			attach_domain_data);
		break;

	case AXIS_ITYPE:
		if (obj_item_id) {
			((FLAT_AXIS *) flats)->masks.bin_exists =
					obj_item_mask & AXIS_ATTR_MASKS;
			((FLAT_AXIS *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= AXIS_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, AXIS_ITYPE,
			((FLAT_AXIS *) flats)->masks.bin_hooked, attach_axis_data);
		break;

	case CHART_ITYPE:
		if (obj_item_id) {
			((FLAT_CHART *) flats)->masks.bin_exists =
					obj_item_mask & CHART_ATTR_MASKS;
			((FLAT_CHART *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= CHART_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, CHART_ITYPE,
			((FLAT_CHART *) flats)->masks.bin_hooked, attach_chart_data);
		break;

	case FILE_ITYPE:
		if (obj_item_id) {
			((FLAT_FILE *) flats)->masks.bin_exists =
					obj_item_mask & FILE_ATTR_MASKS;
			((FLAT_FILE *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= FILE_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, FILE_ITYPE,
			((FLAT_FILE *) flats)->masks.bin_hooked, attach_file_data);
		break;

	case GRAPH_ITYPE:
		if (obj_item_id) {
			((FLAT_GRAPH *) flats)->masks.bin_exists =
					obj_item_mask & GRAPH_ATTR_MASKS;
			((FLAT_GRAPH *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= GRAPH_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, GRAPH_ITYPE,
			((FLAT_GRAPH *) flats)->masks.bin_hooked, attach_graph_data);
		break;

	case GRID_ITYPE:
		if (obj_item_id) {
			((FLAT_GRID *) flats)->masks.bin_exists =
					obj_item_mask & GRID_ATTR_MASKS;
			((FLAT_GRID *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= GRID_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, GRID_ITYPE,
			((FLAT_GRID *) flats)->masks.bin_hooked, attach_grid_data);
		break;

	case IMAGE_ITYPE:
		if (obj_item_id) {
			((FLAT_IMAGE *) flats)->masks.bin_exists =
					obj_item_mask & IMAGE_ATTR_MASKS;
			((FLAT_IMAGE *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= IMAGE_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, IMAGE_ITYPE,
			((FLAT_IMAGE *) flats)->masks.bin_hooked, attach_image_data);
		break;

	case LIST_ITYPE:
		if (obj_item_id) {
			((FLAT_LIST *) flats)->masks.bin_exists =
					obj_item_mask & LIST_ATTR_MASKS;
			((FLAT_LIST *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= LIST_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, LIST_ITYPE,
			((FLAT_LIST *) flats)->masks.bin_hooked, attach_list_data);
		break;

	case SOURCE_ITYPE:
		if (obj_item_id) {
			((FLAT_SOURCE *) flats)->masks.bin_exists =
					obj_item_mask & SOURCE_ATTR_MASKS;
			((FLAT_SOURCE *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= SOURCE_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, SOURCE_ITYPE,
			((FLAT_SOURCE *) flats)->masks.bin_hooked, attach_source_data);
		break;

	case WAVEFORM_ITYPE:
		if (obj_item_id) {
			((FLAT_WAVEFORM *) flats)->masks.bin_exists =
					obj_item_mask & WAVEFORM_ATTR_MASKS;
			((FLAT_WAVEFORM *) flats)->id = obj_item_id;
		}
		*attr_req_mask &= WAVEFORM_ATTR_MASKS;
		rcode = get_item_attr(env_info, rhandle, rod_obj, &local_scrpad,
			attr_req_mask, obj_item_mask, flats,
			attributes_length, obj_extn_ptr, WAVEFORM_ITYPE,
			((FLAT_WAVEFORM *) flats)->masks.bin_hooked, attach_waveform_data);
		break;

	default:
		CRASH_RET(FETCH_INVALID_ITEM_TYPE);
	}			/* end switch */

	/*
	 * Update the scratchpad memory used parameter
	 */

	scrpad->used = local_scrpad.used;

	return (rcode);
}

/*********************************************************************
 *
 *	Name:	fch_rod_item_spad_size
 *
 *	ShortDesc: Compute needed scratchpad memory size for fetch
 *
 *	Description:
 *		This function returns the amount of scratchpad memory required
 *		for a ROD Item Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		request_mask -	attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type)
 *		itype -			identifies the type of Item requested
 *
 *	Outputs:
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_ATTRIBUTE_NO_MASK_BIT
 *		FETCH_INVALID_RI
 *		FETCH_INVALID_ITEM_TYPE
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

#define MAX_ALIGN_OFFSET (sizeof(DEPBIN *)-1)	/* maximum offset required to
						 * place structures in the
						 * scratchpad on a correctly
						 * aligned address boundary */
int
fch_rod_item_spad_size(
ENV_INFO		*env_info, 
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
unsigned long  *sp_addlsize,
unsigned long   request_mask,
ITEM_TYPE       itype)
{
	int             rcode;
	OBJECT         *rod_obj;/* points to ROD object structure, returned
				 * from get_rod_obj(), containing pointers to
				 * the object extension and local data area */
	unsigned short  num_requests;	/* number of requested attributes */
	unsigned short  num_depbins;	/* number of DEPBIN structs needed */
	unsigned char  *obj_extn_ptr;	/* points to start of object extension */
	unsigned char  *obj_attr_ptr;	/* pointer to attributes in object
					 * extension */
	unsigned char   obj_extn_length;	/* length of object extension */
	unsigned char   obj_item_type;	/* item type contained in object
					 * extension */
	unsigned char	obj_item_subtype;	/* item subtype contained in
					 * object extension */
	unsigned char   obj_hdr_length;	/* length of header portion (length,
					 * type, item ID, & item mask) of
					 * object extension */
	unsigned long   obj_item_mask;	/* item mask contained in object
					 * extension */
	unsigned long   obj_item_id;	/* item ID contained in object
					 * extension */
	unsigned char   attributes_length;	/* length of attributes portion
						 * of object extension */
	unsigned long   curr_attr_length;	/* data length for current
						 * attribute */
	unsigned long   attr_mask_bit;	/* bit in item mask corresponding to
					 * current attribute */
	unsigned short  curr_attr_RI;	/* RI for current attribute */
	unsigned short  curr_attr_tag;	/* tag for current attribute */
	SCRATCH_PAD     local_scrpad;	/* allows ROD object fetch to load
					 * object extension */
	unsigned char   pad_buf[MAX_OBJ_EXTN_LEN];	/* holds object
							 * extension to allow
							 * calculation of
							 * scratchpad size */
	unsigned long  attr_offset;	/* attribute data field for offset of
					 * data in local data area */

	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(sp_addlsize, FETCH_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(ROD_FCH_ITEM_SCRATCHPAD_SIZE);
#endif

	/*
	 * Set the remaining size parameter to 0, if not already set.
	 */

	*sp_addlsize = 0L;

	/*
	 * Calculate the scratchpad overhead required for the DEPBIN
	 * structures and DEPBIN pointer array(s) for the type of item.
	 * Extra bytes are factored into to the calculations to allow
	 * proper address alignment for these structures.
	 */

	switch (itype) {

	case VARIABLE_ITYPE:

	/**
	 *	If this item is a variable, we must turn on the TYPE bit in the 
	 *	request mask.  This is required for var_type subattributes.
	 **/

		request_mask |= VAR_TYPE_SIZE;

		request_mask &= VAR_ATTR_MASKS;
		if (request_mask & VAR_MAIN_MASKS) {
			*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(VAR_DEPBIN));
		}
		if (request_mask & VAR_ACT_MASKS) {
			*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(VAR_ACTIONS_DEPBIN));
		}
		if (request_mask & VAR_MISC_MASKS) {
			*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(VAR_MISC_DEPBIN));
		}
		break;

	case MENU_ITYPE:
		request_mask &= MENU_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(MENU_DEPBIN));
		break;

	case EDIT_DISP_ITYPE:
		request_mask &= EDIT_DISP_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(EDIT_DISPLAY_DEPBIN));
		break;

	case METHOD_ITYPE:
		request_mask &= METHOD_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(METHOD_DEPBIN));
		break;

	case REFRESH_ITYPE:
		request_mask &= REFRESH_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(REFRESH_DEPBIN));
		break;

	case UNIT_ITYPE:
		request_mask &= UNIT_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(UNIT_DEPBIN));
		break;

	case WAO_ITYPE:
		request_mask &= WAO_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(WAO_DEPBIN));
		break;

	case ITEM_ARRAY_ITYPE:
		request_mask &= ITEM_ARRAY_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(ITEM_ARRAY_DEPBIN));
		break;

	case COLLECTION_ITYPE:
		request_mask &= COLLECTION_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(COLLECTION_DEPBIN));
		break;

	case BLOCK_ITYPE:
		request_mask &= BLOCK_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(BLOCK_DEPBIN));
		break;

	case PROGRAM_ITYPE:
		request_mask &= PROGRAM_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(PROGRAM_DEPBIN));
		break;

	case RECORD_ITYPE:
		request_mask &= RECORD_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(RECORD_DEPBIN));
		break;

	case ARRAY_ITYPE:
		request_mask &= ARRAY_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(ARRAY_DEPBIN));
		break;

	case VAR_LIST_ITYPE:
		request_mask &= VAR_LIST_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(VAR_LIST_DEPBIN));
		break;

	case RESP_CODES_ITYPE:
		request_mask &= RESP_CODE_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(RESP_CODE_DEPBIN));
		break;

	case COMMAND_ITYPE:
		request_mask &= COMMAND_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(COMMAND_DEPBIN));
		break;

	case DOMAIN_ITYPE:
		request_mask &= DOMAIN_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(DOMAIN_DEPBIN));
		break;

	case AXIS_ITYPE:
		request_mask &= AXIS_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(AXIS_DEPBIN));
		break;

	case CHART_ITYPE:
		request_mask &= CHART_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(CHART_DEPBIN));
		break;

	case FILE_ITYPE:
		request_mask &= FILE_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(FILE_DEPBIN));
		break;

	case GRAPH_ITYPE:
		request_mask &= GRAPH_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(GRAPH_DEPBIN));
		break;

	case GRID_ITYPE:
		request_mask &= GRID_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(GRID_DEPBIN));
		break;

	case IMAGE_ITYPE:
		request_mask &= IMAGE_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(IMAGE_DEPBIN));
		break;

	case LIST_ITYPE:
		request_mask &= LIST_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(LIST_DEPBIN));
		break;

	case SOURCE_ITYPE:
		request_mask &= SOURCE_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(SOURCE_DEPBIN));
		break;

	case WAVEFORM_ITYPE:
		request_mask &= WAVEFORM_ATTR_MASKS;
		*sp_addlsize += (MAX_ALIGN_OFFSET + sizeof(WAVEFORM_DEPBIN));
		break;

	default:
		CRASH_RET(FETCH_INVALID_ITEM_TYPE);
	}			/* end switch */

	/*
	 * Set up a temporary scratchpad and load the object
	 */

	local_scrpad.pad = pad_buf;
	local_scrpad.size = sizeof(pad_buf);
	local_scrpad.used = 0L;


	/*
	 * Get the initial location of the object extension in the scratchpad
	 * prior to the ROD object fetch.
	 */

	obj_extn_ptr = local_scrpad.pad;

	rcode = get_rod_object(env_info, rhandle, obj_index, &rod_obj, &local_scrpad);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * Retrieve the extension length, item type, item ID, and item mask
	 * fields
	 */

	rcode = get_extn_hdr(rhandle, &obj_extn_ptr, &obj_extn_length,
		&obj_item_type, &obj_item_subtype,
		&obj_item_id, &obj_item_mask, &obj_hdr_length, itype);

	if (rcode != SUCCESS) {
		return (rcode);
	}

	/*
	 * The minimum scratchpad size requirement is the length of the object
	 * extension
	 */

	*sp_addlsize += (obj_extn_length + EXTEN_LENGTH_SIZE);
#ifndef SUN
#pragma warning (disable : 4244)
#endif
	attributes_length = (obj_extn_length + EXTEN_LENGTH_SIZE) - obj_hdr_length;
#ifndef SUN
#pragma warning (default : 4244)
#endif

	/*
	 * Get the number of requested attributes
	 */

	num_depbins = num_requests = count_bits(request_mask & obj_item_mask);

	/*
	 * Point to the first attribute in the object extension and begin
	 * sizing the attribute data
	 */

	obj_attr_ptr = obj_extn_ptr;

	while ((obj_attr_ptr < obj_extn_ptr + attributes_length) &&
		(num_requests) > 0) {

		/*
		 * Retrieve the Attribute Identifier information from the
		 * leading attribute bytes.  The object extension pointer will
		 * point to the first byte after the Attribute ID, which will
		 * be Immediate data or will reference Local or External data.
		 */

		rcode = parse_attribute_id(&obj_attr_ptr, &curr_attr_RI,
			&curr_attr_tag, &curr_attr_length);

		if (rcode != SUCCESS) {
			return (rcode);
		}

		/*
		 * Confirm that the current attribute being is matched by a set
		 * bit in the Item Mask.  The mask or the attribute tag is
		 * incorrect if there is not a match.
		 */

		attr_mask_bit = (unsigned long) (1L << curr_attr_tag);
		if (!(obj_item_mask & attr_mask_bit)) {
			return (FETCH_ATTRIBUTE_NO_MASK_BIT);
		}

		/*
		 * Use the Tag field from the Attribute Identifier to determine
		 * if the attribute was requested or not (not applicable to
		 * External All data type).  For all data types except External
		 * All, skip to the next attribute in the extension if not
		 * requested.  Also, skip forward if the attribute has already
		 * been attached.
		 */


		switch (curr_attr_RI) {

		case RI_IMMEDIATE:	/* Size already included in extn length */
			obj_attr_ptr += curr_attr_length;
			if (request_mask & attr_mask_bit) {
				num_requests--;
			}
			break;

		case RI_LOCAL:
			if (tok6or8)
			{
				attr_offset = 0L;
				do {
					if (attr_offset & MAX_LENGTH_MASK) {
						return (FETCH_ATTR_LENGTH_OVERFLOW);
					}
					attr_offset = (attr_offset << LENGTH_SHIFT) |
						(unsigned long) (LENGTH_MASK & *obj_attr_ptr);
				} while (LENGTH_ENCODE_MASK & *obj_attr_ptr++);
			}
			else
			{
				obj_attr_ptr += LOCAL_REF_SIZE;
			}
			
			if (request_mask & attr_mask_bit) {
				*sp_addlsize += curr_attr_length;
				num_requests--;
			}
			break;

		case RI_EXTERNAL_SINGLE:
			obj_attr_ptr += EXTERNAL_REF_SIZE;
			if (request_mask & attr_mask_bit) {
				*sp_addlsize += curr_attr_length;
				num_requests--;
			}
			break;

		case RI_EXTERNAL_ALL:
			obj_attr_ptr += EXTERNAL_REF_SIZE;
			*sp_addlsize += curr_attr_length;

			break;

		default:
			return (FETCH_INVALID_RI);
		}		/* end switch */

	}			/* end while */

	/*
	 * Calculate the scratchpad overhead required for the DEPBIN
	 * structures.
	 */

	*sp_addlsize += (unsigned long)
			(num_depbins * (MAX_ALIGN_OFFSET + sizeof(DEPBIN)));

	return (SUCCESS);
}

/*********************************************************************
 *
 *	Name:	fch_rod_item
 *
 *	ShortDesc: ROD item fetch interface
 *
 *	Description:
 *		This function provides the external interface for a ROD Item
 *		Fetch
 *
 *	Inputs:
 *		rhandle -		handle for the ROD containing the object
 *		obj_index -		index of the object
 *		request_mask -	attributes requested for the Item, indicated
 *						by individual bits set for each attribute
 *						(attributes depend on the Item type)
 *		itype -			identifies the type of Item requested
 *
 *	Outputs:
 *		scrpad -		pointer to a structure whose members point
 *						to a scratchpad memory area containing
 *						the object requested and any external objects
 *						and data referenced by the object.
 *		sp_addlsize -	pointer to the value which, if nonzero,
 *						indicates the request was unsuccessful due to
 *						insufficient scratchpad memory.  The value
 *						returned is the minimum size needed to complete
 *						the request.
 *		flat_item -		pointer to the flat structure containing the
 *						pointers to the Item attributes in the
 *						scratchpad memory (contents of structure depend
 *						on the Item type)
 *
 *	Returns:
 *		SUCCESS
 *		FETCH_INVALID_PARAM
 *		FETCH_INSUFFICIENT_SCRATCHPAD
 *
 *	Author:
 *		David Bradsher
 *
 *********************************************************************/

int
fch_rod_item(
ENV_INFO	   *env_info,
ROD_HANDLE      rhandle,
OBJECT_INDEX    obj_index,
SCRATCH_PAD    *scrpad,
unsigned long  *sp_addlsize,
unsigned long   request_mask,
void           *flat_item,
ITEM_TYPE       itype)
{
	int             rcode;

	/*
	 * Check the validity of the pointer parameters
	 */

	ASSERT_RET(scrpad && sp_addlsize && flat_item,
		FETCH_INVALID_PARAM);

	/**
	 *	If this item is a variable, we must turn on the TYPE bit in the 
	 *	request mask.  This is required for var_type subattributes.
	 **/

	if (itype == VARIABLE_ITYPE) {
		request_mask |= VAR_TYPE_SIZE;
	}

	/*
	 * Make sure the remaining size parameter is set to 0.  If there is
	 * insufficient memory in the scratchpad for the requested attributes,
	 * this value will contain the scratchpad required for a subsequent
	 * item request.
	 */

	*sp_addlsize = 0L;

#ifdef DEBUG
	dds_item_flat_check(flat_item, itype);
#endif
	rcode = get_rod_item(env_info, rhandle, obj_index, scrpad, (unsigned long) 0L,
		&request_mask, flat_item, itype);
#ifdef DEBUG
	dds_item_flat_check(flat_item, itype);
#endif

	if (rcode == FETCH_INSUFFICIENT_SCRATCHPAD) {
		rcode = fch_rod_item_spad_size(env_info, rhandle, obj_index,
			sp_addlsize, request_mask, itype);
		if (rcode == SUCCESS) {
			return (FETCH_INSUFFICIENT_SCRATCHPAD);
		}
	}
	return (rcode);
}


/*********************************************************************
*
*	Name:	calc_item_ddod_object_data_size
*
*	ShortDesc: Calculates the space needed in the DDOD Object Data area
*
*	Description:
*		Read the Object extension for a DD Item Object and add up
*       the amount of DDOD Object Data that is needed for this Object.
*       This calculation is needed because some DD Item Objects require more than the 0xFFFF domain.size 
*       that is allowed in the Binary file format for .fm8. Therefore, it must be
*       calculated here and the domain.size adjusted by the caller.
*
*	Inputs:
*		rhandle -		handle for the ROD containing the object
*		pExtension -	pointer to the extension part of this object
*		itemType -		identifies the type of Item requested
*
*	Outputs:
*		pTotal -		pointer to the total number of bytes needed
*
*	Returns:
*		SUCCESS
*		other error codes
*
*	Author:
*		Mike Dieter
*
*********************************************************************/

int calc_item_ddod_object_data_size(ROD_HANDLE rhandle,
                                    UINT8* pExtension,
                                    ITEM_TYPE itemType,
                                    UINT32* pTotal)
{
    *pTotal = 0;    /* initialize the output */

    const bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rhandle);

    /*
    * Retrieve the extension header information
    */

    unsigned char* obj_extn_ptr     = pExtension; /* points to start of object extension */
    unsigned char obj_extn_length   = 0; /* length of object extension */
    unsigned char obj_item_type     = 0; /* <unused> item type contained in object extension */
    unsigned char obj_item_subtype  = 0; /* <unused> item subtype contained in object extension */
    unsigned long obj_item_id       = 0; /* <unused> item ID contained in object extension */
    unsigned long obj_item_mask     = 0; /* <unused> item mask contained in object extension */
    unsigned char obj_hdr_length    = 0; /* length of header portion (length, type, item ID, & item mask) of object extension */

    int rcode = get_extn_hdr(rhandle, &obj_extn_ptr, &obj_extn_length,
                             &obj_item_type, &obj_item_subtype,
                             &obj_item_id, &obj_item_mask, &obj_hdr_length, itemType);

    /* on return, the obj_extn_ptr has been advanced to point at the attribute list */

    if (rcode != SUCCESS)
    {
        return (rcode);
    }

    const unsigned char attributes_length = (obj_extn_length + EXTEN_LENGTH_SIZE) - obj_hdr_length;

    /*
    * Point to the first attribute in the object extension and begin
    * sizing the attribute data
    */

    unsigned char* obj_attr_ptr = obj_extn_ptr; /* pointer to attributes in object extension */

    while (obj_attr_ptr < obj_extn_ptr + attributes_length) /* loop through all the attributes */
    {
        /*
        * Retrieve the Attribute Identifier information from the
        * leading attribute bytes.  The object extension pointer will
        * point to the first byte after the Attribute ID, which will
        * be Immediate data or will reference Local or External data.
        */

        unsigned short curr_attr_RI = 0;    /* RI for current attribute */
        unsigned short curr_attr_tag = 0;   /* <unused> tag for current attribute */
        unsigned long curr_attr_length = 0; /* data length for current attribute */

        rcode = parse_attribute_id(&obj_attr_ptr, &curr_attr_RI,
                                   &curr_attr_tag, &curr_attr_length);

        if (rcode != SUCCESS)
        {
            return (rcode);
        }

        // Use the Reference Indicator to determine whether we need to add the DDOD Object Data length
        // For all RI, advance the obj_attr_ptr to the next attribute.
        switch (curr_attr_RI)
        {
        case RI_IMMEDIATE: /* Size not a part of the DDOD Object Data */
            obj_attr_ptr += curr_attr_length; // Only advance the pointer past the immediate data
            break;

        case RI_LOCAL:
            *pTotal += curr_attr_length; // Accumulate the DDOD Object Data length

            // Advance the pointer by skipping over the local-address-offset
            if (tok6or8)
            {
                unsigned long attr_offset = 0L;
                do
                {
                    if (attr_offset & MAX_LENGTH_MASK)
                    {
                        return (FETCH_ATTR_LENGTH_OVERFLOW);
                    }
                    attr_offset = (attr_offset << LENGTH_SHIFT) |
                        (unsigned long)(LENGTH_MASK & *obj_attr_ptr);
                }
                while (LENGTH_ENCODE_MASK & *obj_attr_ptr++);
            }
            else
            {
                obj_attr_ptr += LOCAL_REF_SIZE;
            }

            break;

        case RI_EXTERNAL_SINGLE:
        case RI_EXTERNAL_ALL:
            obj_attr_ptr += EXTERNAL_REF_SIZE; // Only advance the pointer
            break;

        default:
            return (FETCH_INVALID_RI);
        } /* end switch */
    } /* end while */

    return rcode;
}
