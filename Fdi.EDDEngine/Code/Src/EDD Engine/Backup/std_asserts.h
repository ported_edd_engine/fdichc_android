////////////////////////////////////////////////////////////////////////
////	Simple universal definitions.
////////////////////////////////////////////////////////////////////////

#ifndef STD_ASSERTS_H
#define STD_ASSERTS_H

/*
 *	Assert macros
 */

extern void panic (const char*, unsigned, const char *,...);

// ASSERT and CRASH Definitions
#ifdef DEBUG

#  define ASSERT_DBG(cond)	\
        if(!(cond)) panic(__FILE__, __LINE__, "Assertion failed: \"%s\"", _T(#cond))
#  define ASSERT_RET(cond,param) \
     if(!(cond)) panic(__FILE__, __LINE__, "Condition not true: passed value = %d\n", (param))
#  define CRASH_RET(param) \
     panic(__FILE__, __LINE__, "Crash: passed value = %d\n", (param))

#  define CRASH_DBG() \
	BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Crash")

#else

#  define ASSERT_DBG(cond)
#  define ASSERT_RET(cond,param)	if(!(cond)) return (param)
#  define CRASH_RET(param)		return (param)
#  define CRASH_DBG()

#endif				/* DEBUG */


#endif				/* STD_ASSERTS_H */
