#pragma once

#include "nsEDDEngine/IItemInfo.h"
#include "nsEDDEngine/ITableInfo.h"
#include "nsEDDEngine/IConvenience.h"
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IEDDEngineLogger.h"
#include "nsEDDEngine/ProtocolType.h"
#include "nsEDDEngine/FFBlockInfo.h"
#include "pugixml.hpp"
#include "stdstring.h"


class CEDDEngineImpl :  public nsEDDEngine::IItemInfo,
                        public nsEDDEngine::ITableInfo,
                        public nsEDDEngine::IConvenience
{
protected:
	CStdString  m_szFilename;
	nsConsumer::IParamCache *m_pIParamCache;
	nsEDDEngine::ProtocolType m_Protocol;
	nsEDDEngine::DDSType m_DdsType;

public:
	nsConsumer::IEDDEngineLogger *m_pIEDDEngineLogger;
	CEDDEngineImpl(const wchar_t *sEDDBinaryFilename, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger,  nsEDDEngine::DDSType eDDSType,
		 nsEDDEngine::EDDEngineFactoryError* /*eErrorCode*/)
	{
		m_szFilename = sEDDBinaryFilename;
		m_pIEDDEngineLogger = pIEDDEngineLogger;
		m_Protocol = (nsEDDEngine::ProtocolType)0;
		m_pIParamCache = nullptr;
		m_DdsType = eDDSType;
	};
	virtual ~CEDDEngineImpl() { };
	
    // IEDDEngine implementation
	virtual void GetEDDFileHeader(nsEDDEngine::EDDFileHeader* /*pEDDFileHeader*/) = 0;
	virtual int AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo *pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount ) = 0;
	// IItemInfo implementation
	virtual int GetItem( int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::FDI_ITEM_SPECIFIER* /*pItemSpec*/, nsEDDEngine::AttributeNameSet* /*pAttributeNameSet*/, const wchar_t* /*lang_code*/, nsEDDEngine::FDI_GENERIC_ITEM* /*pGenericItem*/ ) = 0;

	//ITableInfo implementation
	virtual int GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR** /*pDeviceDir*/) = 0;

	// IConvenience implementation
	// Gets the Unit Relation ItemId for the specified parameter
	virtual int GetParamUnitRelItemId(int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::FDI_PARAM_SPECIFIER * /*pParamSpec*/, ITEM_ID * /*pUnitItemId*/) = 0;
	virtual int GetStringTranslation(const wchar_t* /*string*/, const wchar_t* /*lang_code*/, wchar_t* /*outbuf*/, int /*outbuf_size*/) = 0;
	virtual int GetParamType(int /*iBlockInstance*/, nsEDDEngine::FDI_PARAM_SPECIFIER* /*pParamSpec*/, nsEDDEngine::TYPE_SIZE* /*pTypeSize*/) = 0;
	virtual int GetSymbolNameFromItemId(ITEM_ID /*item_id*/, wchar_t* /*item_name*/, int /*outbuf_size*/) = 0; 
	virtual	int GetItemIdFromSymbolName(wchar_t* /*pItemName*/, ITEM_ID* /*pItemId*/) = 0;
    virtual int GetItemTypeAndItemId(int /*iBlockInstance*/, nsEDDEngine::FDI_ITEM_SPECIFIER* /*pItemSpec*/, nsEDDEngine::ITEM_TYPE* /*pItemType*/, ITEM_ID* /*pItemId*/) = 0;
    virtual int GetItemType(int /*iBlockInstance*/, nsEDDEngine::FDI_ITEM_SPECIFIER* /*pItemSpec*/, nsEDDEngine::ITEM_TYPE* /*pItemType*/) = 0;
	// Converts an EDD Engine Error code to a string
    virtual wchar_t *GetErrorString(int /*iErrorNum*/) = 0;

    // Command Convenience functions

    // Retrieves the List of Commands that can be used to read/write this parameter
    virtual int GetCommandList(int /*iBlockInstance*/, nsEDDEngine::FDI_PARAM_SPECIFIER * /*pParamSpec*/, nsEDDEngine::CommandType /*eCmdType*/, nsEDDEngine::FDI_COMMAND_LIST * /*pCommandList*/) = 0;
    // Retrieves the ItemId of the Command indicated by the Command Number (HART only)
    virtual int GetCmdIdFromNumber(int /*iBlockInstance*/, ulong /*ulCmdNumber*/, ITEM_ID * /*pCmdItemId*/) = 0;

	virtual int GetDictionaryString( unsigned long /*ulIndex*/,  wchar_t* /*pString*/, int /*iStringLen*/, const wchar_t* /*lang_code*/ ) = 0;
	virtual int GetDictionaryString( wchar_t* /*wsDictName*/,  wchar_t* /*pString*/, int /*iStringLen*/, const wchar_t* /*lang_code*/ ) = 0;
	virtual int GetDevSpecString ( unsigned long /*ulIndex*/,  wchar_t* /*pString*/, int /*iStringLen*/, const wchar_t * /*lang_code*/ ) = 0;
	virtual	int ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE /*iBlockInstance*/, ITEM_ID /*ItemId*/, ITEM_ID /*MemberId*/, int* /*iSubIndex*/) = 0;
		
	virtual int GetBlockInstanceByObjIndex(int /*iObjectIndex*/, int* /*iOccurrence*/) = 0;
	virtual int GetBlockInstanceByTag( ITEM_ID /*iItemId*/, wchar_t* /*pTag*/, int* /*iOccurrence*/) = 0;
	virtual int GetBlockInstanceCount( ITEM_ID /*iItemId*/, int* /*piCount*/) = 0;
	virtual int ConvertToBlockInstance(unsigned long /*ulItemId*/, int /*iOccurrence*/, nsEDDEngine::BLOCK_INSTANCE* /*piBlockInstance*/) = 0;
#ifdef _DEBUG
	virtual int DisplayDebugInfo() = 0;
#endif


    // Implmentations:  Called from projects that this project links to
    	
	void SetParamCache(nsConsumer::IParamCache *pIParamCache)
	{
		m_pIParamCache = pIParamCache;
	}

	nsConsumer::IParamCache* GetParamCache()
	{
		return m_pIParamCache;
	}

    void SetProtocol(nsEDDEngine::ProtocolType type)
    {
        m_Protocol = type;
    }

    nsEDDEngine::ProtocolType GetProtocol()
    {
        return m_Protocol;
    }
	
    //*****************************************************************************************//
    //
    //   Name: GetFieldFromXML
    //	 
    //	 Description: 
    //		Get the named field from the config XML string
    //		
    //	 Inputs:
    //		const wchar_t* fieldName - The field name to extract the text for
    //      const wchar_t* configXML - The XML text string to extract it from
    //		wchar_t *      fieldBuff   - Contains The requested field string or NULL if not found
    //                                 - Empty String on failure
    //      int            fieldLen    - Max characters that can fit in fieldBuff
    //
    //	 Returns:
    //      true if success, false if any failures
    //
    //	 Author:
    //		Mark Sandmann
    //*****************************************************************************************//
    bool GetFieldFromXML(const wchar_t* fieldName, const wchar_t* configXML,
                                        wchar_t* fieldBuff, unsigned int fieldLen)
    {
        bool returnValue = false;

        fieldBuff[0] = 0;   // default fieldBuff to an empty string

		pugi::xml_document xmlDoc;
		pugi::xml_node rootNode; 
		pugi::xml_node childNode;

		pugi::xml_parse_result result = xmlDoc.load_string(configXML, pugi::parse_trim_pcdata);
		if (!result) 
		{
			return returnValue;
		}

		rootNode = xmlDoc.first_child();
		if (rootNode == NULL)
		{
			return returnValue;
		}

		childNode = rootNode.child(fieldName);
		if (childNode == NULL)
		{
			return returnValue;
		}

		std::wstring contentTemp = childNode.text().get();
		if (contentTemp.length()  <= fieldLen)
		{
			wcscpy(fieldBuff, contentTemp.c_str());
			returnValue = true;
		}
		return returnValue;
    }

    
    //*****************************************************************************************//
    //
    //   Name: GetFieldFromXMLByID
    //	 
    //	 Description: 
    //		Get the named field with the specified ID from the config XML string
    //		
    //	 Inputs:
    //		const wchar_t* fieldName - The field name to extract the text for
    //      const wchar_t* id        - FieldName ID to extract the text for
    //      const wchar_t* configXML - The XML text string to extract it from
    //		wchar_t *      fieldBuff   - Contains The requested field string or NULL if not found
    //                                 - Empty String on failure
    //      int            fieldLen    - Max characters that can fit in fieldBuff
    //
    //	 Returns:
    //      true if success, false if any failures
    //
    //	 Author:
    //		Mark Sandmann
    //*****************************************************************************************//
    bool GetFieldFromXMLByID(const wchar_t* fieldName, const INT32 id, const wchar_t* configXML,
                                        wchar_t* fieldBuff, unsigned int fieldLen)
    {
        bool returnValue = false;

        fieldBuff[0] = 0;   // default fieldBuff to an empty string

		pugi::xml_document xmlDoc;
		pugi::xml_node rootNode;

		pugi::xml_parse_result result = xmlDoc.load_string(configXML, pugi::parse_trim_pcdata);
		if (!result)
		{
			return returnValue;
		}

		rootNode = xmlDoc.first_child();
		if (rootNode == NULL)
		{
			return returnValue;
		}


		for (pugi::xml_node childNode : rootNode.children())
		{
			if (wcscmp(childNode.name(), fieldName) == 0)
			{
				pugi::xml_attribute idAttr = childNode.attribute(L"ID");
				if (idAttr != NULL)
				{
					if (wcstol(idAttr.value(), NULL, 16) == id)
					{
						std::wstring contentTemp = childNode.text().get();
						if (contentTemp.length() <= fieldLen)
						{
							wcscpy(fieldBuff, contentTemp.c_str());
							returnValue = true;
							break;
						}
					}

				}
			}

		}
        return returnValue;
    }
};
