#pragma once

enum HeaderEDDProfile {
	Profile_None		= 0,
	Profile_HART		= 1,
	Profile_FF			= 2,
	Profile_ProfiBus	= 3,
	Profile_ProfiNet	= 4,
	Profile_ISA100		= 5,
	Profile_GPE			= 6,
	Profile_CommServer	= 7
};

typedef struct tag_DDOD_HEADER {
	UINT32		magic_number;
	UINT32		header_size;
	UINT32		meta_data_size;
	UINT32		item_objects_size;
	UINT32		manufacturer;
	UINT16		device_type;
	UINT8		device_revision;
	UINT8		dd_revision;
	UINT8		major_revision;
	UINT8		minor_revision;
	HeaderEDDProfile edd_profile;		
	UINT8		tool_release;
	UINT32		signature;
	UINT32		reserved3;
	UINT32		reserved4;
    wchar_t		filepath[512];
} DDOD_HEADER;

