/*
 *	@(#) $Id: ddi_lang.cpp,v 1.2 2012/08/12 20:45:44 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 *
 *  This file contains all of the functions for DDI language
 */


#include "stdafx.h"
#include "rtn_code.h"
#include "ddi_lib.h"


#define	DEF_LANG_CTRY		L"|en|"	/*	Of the form "|LL|" or "|LL CC|", where
									 *	LL = language code and
									 *	CC = country code (optional)
									 */

#define COUNTRY_CODE_MARK 	L'|'

wchar_t g_dds_language_code[32] = DEF_LANG_CTRY;

/*********************************************************************
 *
 *	Name: ddi_get_string_translation
 *	ShortDesc: return the requested translation of the string
 *
 *	Description:
 *		ddi_get_string_translation will take the given string, which is
 *		composed of phrases, and using the language/country code, extract 
 *		and copy the correct string into the output buffer
 *
 *	Inputs:
 *		string:			a pointer to the string to search for the
 *						correct translation
 *		lang_cntry:		a pointer to the language/country code string
 *		outbuf_size:	the size of the output buffer, in characters
 *
 *	Outputs:
 *		outbuf:			a pointer to the output buffer with the correct
 *						translation loaded
 *
 *	Returns:
 *		DDS_SUCCESS, DDI_INSUFFICIENT_BUFFER
 *
 *	Author: Vern Reinhardt
 *
 **********************************************************************/

int
ddi_get_string_translation(
const wchar_t *string, const wchar_t *lang_cntry, wchar_t *outbuf, int outbuf_size)
{
	/*
	 *	If the input string is a null string (which is legal), we can
	 *	skip all the string processing and return the output string,
	 *	which we set to a null string.
	 */
	if ((string == NULL) || (string[0] == L'\0')) 
	{
		outbuf[0] = L'\0';
		return(DDS_SUCCESS);
	}

    /*
     * If the input country_code is "||" we return the untranslated string
     */
    if (wcscmp(L"||", lang_cntry) == 0)
    {
        /*
	     *	Check the length of the output buffer.  If the phrase to be output
	     *	is longer than the output buffer, return an error code.  Otherwise,
	     *	copy the phrase in the holding buffer into the output buffer.
	     */
	    if ((size_t) outbuf_size < (wcslen(string) + 1))
        {
		    outbuf[0] = '\0';
		    return DDI_INSUFFICIENT_BUFFER;
	    }
        else
        {
		    (void)wcscpy(outbuf, string);
	    }
        return(DDS_SUCCESS);
    }

	/*
	 *	If the input country_code is full-size (i.e., seven character),
	 *	extract the language code from the language/country code.
	 *	Otherwise, make the language-only code a null string.
	 */
    wchar_t	lang_only[5]{0};	/* language-only pulled from language/country code */

	if (wcslen(lang_cntry) == 7) {
		(void)wcsncpy(lang_only, lang_cntry, (size_t)3);
		lang_only[3] = COUNTRY_CODE_MARK;
		lang_only[4] = '\0';
	} else {
		lang_only[0] = '\0';
	}

	/*
	 *	Check to see if the input string	 begins with a COUNTRY_CODE_MARK.
	 *	If it does not, set the first-phrase pointer, then enter the loop.
	 */
	wchar_t	*first_phrp = NULL;		/* first phrase pointer */

	if (string[0] != COUNTRY_CODE_MARK) {
		first_phrp = outbuf;
	}

	/*
	 *	The Loop:
	 *		On a character-by-character basis, check for any of the
	 *	possible language or language/country codes, or escape sequences.
	 *	Look for the specified language/country code in the input string
	 *	in this order:
	 *
	 *		- the complete language/country code
	 *		- the language-only code (new style)
	 *		- the language-only code (old style)
	 *
	 *	If one of the language/country codes matches, and the corresponding
	 *	phrase pointer is not yet set, save the address of that phrase.
	 *	In any case that a substring in the form of a language/country code is
	 *	found, even if it's not one we're looking for, insert an end-of-string
	 *	character in the output buffer, then move the input string pointer
	 *	beyond the language/country code.  If no language/country code is
	 *	found, look for escape sequences.  Do this this until the original
	 *	string's end-of-string is encountered.
	 */

	wchar_t	*lang_cntry_phrp = NULL;	/* language + country phrase pointer */
	wchar_t	*lang_only_phrp = NULL;		/* language-only phrase pointer */
	wchar_t	*new_def_phrp = NULL;		/* new-style default phrase pointer */
	wchar_t	*old_def_phrp = NULL;		/* old-style language-only phrase pointer */

	int code_length;		/* length of language/country code, in characters */
	const wchar_t *ci;		/* input character pointer */
	wchar_t *co;			/* output character pointer */

	for (co = outbuf, ci = string; *ci; ci++) {

	/*
	 *	Look for the complete language/country code.
	 */
		if((ci[0] == COUNTRY_CODE_MARK) && (unsigned)(ci[1] + 1) > 256)
		{
//			ASSERT(0); // FD_FDI decode country code issue
			outbuf[0] = L'\0';
			return(DDS_SUCCESS);
		}

		if ((ci[0] == COUNTRY_CODE_MARK) && iswalpha(ci[1]) &&
				iswalpha(ci[2]) && (ci[3] == ' ') && iswalpha(ci[4])
				&& iswalpha(ci[5]) && (ci[6] == COUNTRY_CODE_MARK)) {
			code_length = 7;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) {
				lang_cntry_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
					(wcsncmp(ci, g_dds_language_code, code_length) == 0)) {
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = '\0';
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (new style).
	 */
		} else if ((ci[0] == COUNTRY_CODE_MARK) && iswalpha(ci[1]) &&
				iswalpha(ci[2]) && (ci[3] == COUNTRY_CODE_MARK)) {
			code_length = 4;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) {
				lang_cntry_phrp = co + 1;
			}

			if ((lang_only_phrp == 0) && (lang_only[0] != '\0') &&
					(wcsncmp(ci, lang_only, code_length) == 0)) {
				lang_only_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
					(wcsncmp(ci, g_dds_language_code, code_length) == 0)) {
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = '\0';
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (old style); default only.
	 */
		} else if ((ci[0] == COUNTRY_CODE_MARK) && isdigit(ci[1]) &&
				isdigit(ci[2]) && isdigit(ci[3])) {
			code_length = 4;

			if ((old_def_phrp == 0) &&
					(wcsncmp(ci, L"|001", code_length) == 0)) {
				old_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = '\0';
			ci += (code_length - 1);

	/*
	 *	If the escape sequence character (\) is encountered, convert
	 *	the following character as required.  These are the escape
	 *	sequences required by the DDL Spec.
	 */

		}
		else if (*ci == '\\') 
		{
			//convert two characters of escape sequences into a single character. see 61804-3 doc section A.2.3
			switch (*(ci + 1)) {

				case 'a':
					*co++ = '\a';
					ci++;
					break;

				case 'f':
					*co++ = '\f';
					ci++;
					break;

				case 'n':
					*co++ = '\n';
					ci++;
					break;

				case 'r':
					*co++ = '\r';
					ci++;
					break;

				case 't':
					*co++ = '\t';
					ci++;
					break;

				case 'v':
					*co++ = '\v';
					ci++;
					break;

				case '\'':
				case '"':
				case '|':
				case '?':
					*co++ = *(ci + 1);
					ci++;
					break;

				default:
					*co++ = *ci;		//don't forget *ci
					*co++ = *(ci + 1);
					ci++;
					break;
			}

	/*
	 *	This is the 'normal' case; this character has no special
	 *	significance, so just copy it to the output pointer.
	 */
		} else {
			*co++ = *ci;
		}
	}
	*co++ = '\0';

	/*
	 *	We may have found a phrase to output.  Copy the highest priority
	 *	phrase into the holding buffer.  Priority is determined in this 
	 *	order, depending upon which string pointers have been assigned 
	 *	non-null values:
	 *
	 *		- the phrase specified by the complete language/country code,
	 *		- the phrase specified by just the language in the
	 *		  language/country code,
	 *		- the phrase specified by the new-style default
	 *		  language/country code,
	 *		- the phrase specified by the old-style default
	 *		  language/country code,
	 *		- the first phrase encountered in the input string.
	 */

	wchar_t	*out_phrp = NULL;		/* output phrase pointer */

	if (lang_cntry_phrp) {
		out_phrp = lang_cntry_phrp;
	} else if (lang_only_phrp) {
		out_phrp = lang_only_phrp;
	} else if (new_def_phrp) {
		out_phrp = new_def_phrp;
	} else if (old_def_phrp) {
		out_phrp = old_def_phrp;
	} else {
		out_phrp = first_phrp;
	}

	/*
	 *	Check the length of the output buffer.  If the phrase to be output
	 *	is longer than the output buffer, return an error code.  Otherwise,
	 *	copy the phrase in the holding buffer into the output buffer.
	 */
	if (!out_phrp || ((size_t) outbuf_size < wcslen(out_phrp))) {
		outbuf[0] = '\0';
		return DDI_INSUFFICIENT_BUFFER;
	} else {
		(void)wcscpy(outbuf, out_phrp);
	}

	return DDS_SUCCESS;
}



/*
 * ddi_set_language_code
 * Set the global language code used inside DDS for image loading.
 * Example codes are "|en|", "|de|", etc.
 */
int ddi_set_language_code(wchar_t *new_code)
{
	size_t buf_size = sizeof(g_dds_language_code)/2;

    if (wcslen(new_code) >= (buf_size - 2))
	{
        return DDI_INSUFFICIENT_BUFFER;
	}

    /* Make sure we keep the bars in the string */
    if (new_code[0] == '|')
	{
        wcscpy(g_dds_language_code, new_code);
	}
    else
    {
        wcscpy(g_dds_language_code, L"|");
        wcscat(g_dds_language_code, new_code);
        wcscat(g_dds_language_code, L"|");
    }
    return DDS_SUCCESS;
}
