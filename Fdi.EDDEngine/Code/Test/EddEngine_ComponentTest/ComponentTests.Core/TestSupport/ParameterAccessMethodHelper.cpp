
#include "TestSupport/ParameterAccessMethodHelper.h"
#include "TestSupport/AsyncCallbackHandler.h"
#include <gcroot.h>

using namespace System::Runtime::InteropServices;
using namespace nsEDDEngine;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            ParameterAccessMethodHelper::ParameterAccessMethodHelper(System::String^ eddFileName)
            {
                _engine = gcnew EddEngineController(eddFileName);
                _builtinSupportSimulator = new BuiltinSupportSimulator();
                _wasAsyncCalled = false;
                _methodResult = METH_FAILED;
                _engine->Initialize(nullptr, _builtinSupportSimulator);
            }

            ParameterAccessMethodHelper::~ParameterAccessMethodHelper()
            {
                delete _engine;
                delete _builtinSupportSimulator;
            }
            
            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::LoadEdd(System::String^ eddFile)
            {
                return gcnew ParameterAccessMethodHelper(eddFile);
            }

            MockRecorder* ParameterAccessMethodHelper::GetBuildinRecorder()
            {
                return _builtinSupportSimulator->Recorder;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterGetUnscaledValue(nsConsumer::EVAL_VAR_VALUE* returnValue)
            {
                return RegisterGetUnscaledValue(returnValue, new nsConsumer::PC_ErrorCode(nsConsumer::PC_SUCCESS_EC));
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterGetUnscaledValue(nsConsumer::EVAL_VAR_VALUE* returnValue, nsConsumer::PC_ErrorCode* errorCode)
            {
                _builtinSupportSimulator->Recorder->Record(L"GetUnscaledValue.pValue", returnValue);
                _builtinSupportSimulator->Recorder->Record(L"GetUnscaledValue", errorCode);

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterSetUnscaledValue()
            {
                return RegisterSetUnscaledValue(new nsConsumer::PC_ErrorCode(nsConsumer::PC_SUCCESS_EC));
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterSetUnscaledValue(nsConsumer::PC_ErrorCode* errorCode)
            {
                _builtinSupportSimulator->Recorder->Record(L"SetUnscaledValue", errorCode);

                return this;
            }
            
            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterActionValueBegin(nsConsumer::EVAL_VAR_VALUE* actionValue)
            {
                _builtinSupportSimulator->Recorder->Record(L"ActionValue.Begin", new nsConsumer::EVAL_VAR_VALUE(*actionValue));

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterGetParamValue(nsConsumer::EVAL_VAR_VALUE* returnValue)
            {
                return RegisterGetParamValue(returnValue, new nsConsumer::PC_ErrorCode(nsConsumer::PC_SUCCESS_EC));
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterGetParamValue(nsConsumer::EVAL_VAR_VALUE* returnValue, nsConsumer::PC_ErrorCode* errorCode)
            {
                _builtinSupportSimulator->Recorder->Record(L"GetParamValue.pValue", returnValue);
                _builtinSupportSimulator->Recorder->Record(L"GetParamValue", errorCode);

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterSetParamValue()
            {
                return RegisterSetParamValue(new nsConsumer::PC_ErrorCode(nsConsumer::PC_SUCCESS_EC));
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterSetParamValue(nsConsumer::PC_ErrorCode* errorCode)
            {
                _builtinSupportSimulator->Recorder->Record(L"SetParamValue", errorCode);

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterOnMethodExiting()
            {
                return RegisterOnMethodExiting(true);
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::RegisterOnMethodExiting(bool returnCode)
            {
                _builtinSupportSimulator->Recorder->Record(L"OnMethodExiting", new bool(returnCode));

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::ExecuteScalingMethodSynchronously(System::String^ methodName)
            {
                nsEDDEngine::FLAT_METHOD* method = MethodHelper::GetMethod(_engine, methodName);

                nsConsumer::EVAL_VAR_VALUE actionValue = *((nsConsumer::EVAL_VAR_VALUE*)
                    _builtinSupportSimulator->Recorder->ReplayReturnValue(L"ActionValue.Begin"));

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
				methodAction.action.meth_definition.size = wcslen(method->def.data);
				methodAction.action.meth_definition.data = method->def.data;
                nsEDDEngine::IAsyncResult* asyncRunningMethod =  _engine->Instance->BeginVariableActionMethod(
                    iBlockInstance,
                    pValueSpec,
                    &actionValue,
                    &methodAction,
                    pCallback,
                    asyncState,
                    languageCode);

                nsConsumer::EVAL_VAR_VALUE returnedValue;
                _methodResult = _engine->Instance->EndVariableActionMethod(&asyncRunningMethod, &returnedValue);

                _builtinSupportSimulator->Recorder->Record(L"ActionValue.End", new nsConsumer::EVAL_VAR_VALUE(returnedValue));
				//delete method;
				//this is not methodAction's data, please don't delete it
				methodAction.action.meth_definition.data = nullptr;

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::ExecuteScalingMethodAsynchronously(System::String^ methodName)
            {
                nsEDDEngine::FLAT_METHOD* method = MethodHelper::GetMethod(_engine, methodName);

                nsConsumer::EVAL_VAR_VALUE actionValue = *((nsConsumer::EVAL_VAR_VALUE*)
                    _builtinSupportSimulator->Recorder->ReplayReturnValue(L"ActionValue.Begin"));
                
                ScalingCallback^ callback = gcnew ScalingCallback(ParameterAccessMethodHelper::MyScalingMethodCallback);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                UnmanagedAsyncCallback asyncCallback = (UnmanagedAsyncCallback) Marshal::GetFunctionPointerForDelegate(callback).ToPointer();
                nsEDDEngine::IAsyncCallbackHandler* pCallback = new AsyncCallbackHandler(asyncCallback);
                void* asyncState = new gcroot<ParameterAccessMethodHelper^>(this);
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
				methodAction.action.meth_definition.size = wcslen(method->def.data);
				methodAction.action.meth_definition.data = method->def.data;
                _engine->Instance->BeginVariableActionMethod(
                    iBlockInstance,
                    pValueSpec,
                    &actionValue,
                    &methodAction,
                    pCallback,
                    asyncState,
                    languageCode);

				//this is not methodAction's data, please don't delete it
				methodAction.action.meth_definition.data = nullptr;

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::ExecuteMethodSynchronously(System::String^ methodName)
            {
                nsEDDEngine::FLAT_METHOD* method = MethodHelper::GetMethod(_engine, methodName);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                nsEDDEngine::IAsyncCallbackHandler* pCallback = nullptr;
                void* asyncState = 0;
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
				methodAction.action.meth_definition.size = wcslen(method->def.data);
				methodAction.action.meth_definition.data = method->def.data;
                nsEDDEngine::IAsyncResult* asyncRunningMethod =  _engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

                _methodResult = _engine->Instance->EndMethod( &asyncRunningMethod );

 				//this is not methodAction's data, please don't delete it
				methodAction.action.meth_definition.data = nullptr;

                return this;
            }

            ParameterAccessMethodHelper^ ParameterAccessMethodHelper::ExecuteMethodAsynchronously(System::String^ methodName)
            {
                nsEDDEngine::FLAT_METHOD* method = MethodHelper::GetMethod(_engine, methodName);

                ScalingCallback^ callback = gcnew ScalingCallback(ParameterAccessMethodHelper::MyMethodCallback);

                int iBlockInstance = 0;
                void* pValueSpec = 0;
                UnmanagedAsyncCallback asyncCallback = (UnmanagedAsyncCallback) Marshal::GetFunctionPointerForDelegate(callback).ToPointer();
                nsEDDEngine::IAsyncCallbackHandler* pCallback = new AsyncCallbackHandler(asyncCallback);
                void* asyncState = new gcroot<ParameterAccessMethodHelper^>(this);
                wchar_t* languageCode = L"en";
				ACTION methodAction; 
				methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
				methodAction.action.meth_definition.size = wcslen(method->def.data);
				methodAction.action.meth_definition.data = method->def.data;
                _engine->Instance->BeginMethod(
                    iBlockInstance, 
                    pValueSpec, 
                    &methodAction, 
                    pCallback, 
                    asyncState,
                    languageCode);

				//this is not methodAction's data, please don't delete it
				methodAction.action.meth_definition.data = nullptr;

                return this;
            }

            void ParameterAccessMethodHelper::MyMethodCallback(nsEDDEngine::IAsyncResult* asyncResult)
            {
                ParameterAccessMethodHelper^ helper = *reinterpret_cast<gcroot<ParameterAccessMethodHelper^>*>(asyncResult->get_AsyncState());
                helper->_wasAsyncCalled = true;
                helper->_engine->Instance->EndMethod(&asyncResult);
            }

            void ParameterAccessMethodHelper::MyScalingMethodCallback(nsEDDEngine::IAsyncResult* asyncResult)
            {
                ParameterAccessMethodHelper^ helper = *reinterpret_cast<gcroot<ParameterAccessMethodHelper^>*>(asyncResult->get_AsyncState());
                helper->_wasAsyncCalled = true;
                nsConsumer::EVAL_VAR_VALUE returnedValue;
                helper->_engine->Instance->EndVariableActionMethod(&asyncResult, &returnedValue);
                helper->_builtinSupportSimulator->Recorder->Record(L"ActionValue.End", new nsConsumer::EVAL_VAR_VALUE(returnedValue));
            }

            bool ParameterAccessMethodHelper::WasAsyncCalled()
            {
                return _wasAsyncCalled;
            }

            nsEDDEngine::Mth_ErrorCode ParameterAccessMethodHelper::GetMethodResult()
            {
                return _methodResult;
            }

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests
