#ifndef _EDDENGINE_CONTROLLER_H_
#define _EDDENGINE_CONTROLLER_H_

typedef struct tagVARIANT VARIANT;

#include "windows.h"
#include "nsEDDEngine/IEDDEngine.h"
#include "nsEDDEngine/Table.h"
#include "nsEDDEngine/Ddldefs.h"
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IEDDEngineLogger.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            public ref class EddEngineController
            {
            public:
                EddEngineController(System::String^ ddFile);
                ~EddEngineController();
                !EddEngineController();
                
                property nsEDDEngine::IEDDEngine *Instance
                {
                    nsEDDEngine::IEDDEngine *get();
                }
                
                void Initialize();
                void Initialize(nsConsumer::IParamCache *pIParamCache);
                void Initialize(nsConsumer::IParamCache *pIParamCache, nsConsumer::IBuiltinSupport *pIBuiltinSupport);

                nsEDDEngine::FDI_GENERIC_ITEM* GetVariableItem(ITEM_ID itemId);
                nsEDDEngine::FDI_GENERIC_ITEM* GetGenericItem(ITEM_ID itemId);
                nsEDDEngine::FDI_GENERIC_ITEM* GetGenericItem(ITEM_ID itemId, nsEDDEngine::ITEM_TYPE type);
                nsEDDEngine::FDI_GENERIC_ITEM* GetGenericItem(ITEM_ID itemId, nsEDDEngine::ITEM_TYPE type, nsEDDEngine::AttributeNameSet attributes);
                nsEDDEngine::FDI_GENERIC_ITEM* GetGenericItem(
                    ITEM_ID itemId, 
                    nsEDDEngine::ITEM_TYPE type, 
                    nsEDDEngine::AttributeNameSet attributes, 
                    int iBlockInstance);

                void FillParamSpecifier(System::String^ paramName, nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier);

                static nsEDDEngine::AttributeNameSet GetAllAttributes(nsEDDEngine::ITEM_TYPE itemType);

            private:
                System::String^ _eddFileName;
                nsEDDEngine::IEDDEngine *_Instance;
                nsConsumer::IParamCache *_ParamCache;
            };

        } // namespace TestsSupport
    } // namespace Core
} // namespace ComponentTests

#endif