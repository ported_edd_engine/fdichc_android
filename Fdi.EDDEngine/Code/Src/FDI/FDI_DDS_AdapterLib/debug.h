#ifndef _DEBUG_H_
#define _DEBUG_H_


////force debugging output regardless of the command line?
////(see <debuglog.h> for a more detailed explanation)
//#define HARDWIRED_DEBUGGING

//Don't allow hardwired debugging to get compiled into release mode.
#ifndef _DEBUG
#ifdef HARDWIRED_DEBUGGING
#pragma message( "HARDWIRED_DEBUGGING turned off." )
#endif
#undef HARDWIRED_DEBUGGING
#endif

extern int g_debugSSIMessages;
extern int g_debugSSIReadFlush;
extern int g_debugClientMethods;
extern int g_debugStartupCalls;
extern int g_debugShutdownCalls;
extern int g_debugSSIMethods;
extern int g_debugSocketMethods;
extern int g_debugRequests;
extern int g_debugCRequests;
extern int g_debugBlockerMethods;
extern int g_debugParamCache;
extern int g_debugCCmTblManipulation;
extern int g_debugClientSideMessages;
extern int g_debugConversationMethods;
extern int g_debugFlagCatchers;
extern int g_debugCEnumMethods;
extern int g_debugFMSServerMethods;
extern int g_debugEnumMethods;
extern int g_debugEventCreation;
extern int g_debugEventProcessing;
extern int g_debugNavigationMethods;
extern int g_debugMain;
extern int g_debugDbClient;
extern int g_debugCDbCommInterface;
extern int g_debugDeltaVPath;
extern int g_debugMisc;

#endif