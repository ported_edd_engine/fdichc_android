// EDD_Engine_Windows.h : main header file for the EDD_Engine_Windows DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CEDD_Engine_WindowsApp
// See EDD_Engine_Windows.cpp for the implementation of this class
//

class CEDD_Engine_WindowsApp : public CWinApp
{
public:
	CEDD_Engine_WindowsApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
