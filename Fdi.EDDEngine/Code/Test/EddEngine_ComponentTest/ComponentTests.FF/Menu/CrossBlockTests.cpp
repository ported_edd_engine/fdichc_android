#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_a_menu_with_cross_block_FF
        {
        public:
            static EddEngineController^ engine;
            static  FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_block_item_ID;
            static ITEM_ID* test_menu_item_ID;
            static FLAT_VAR* test_var;
            static ITEM_ID* test_var_item_ID;
            static FLAT_MENU* test_menu;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0006\\TestCase0006.ff6");
                engine->Initialize();

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // get the ItemIds for the test_int
                test_var_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_int", test_var_item_ID);

                // get the ItemIds for the test_menu
                test_menu_item_ID = new ITEM_ID();
                int error =  engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", test_menu_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    iBlockInstance);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_var_item_ID;
                delete test_block_item_ID;
                delete test_menu_item_ID;
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_three_items_in_the_menu()
            {
                Assert::AreEqual(3u, UInt32(test_menu->items.count));
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_item_id_for_the_second_element_in_the_list()
            {
                Assert::AreEqual(*test_var_item_ID, test_menu->items.list[1].ref.op_info.id);
            }
        };

        [TestClass]
        public ref class When_loading_a_menu_with_cross_block_menu_reference_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* device_root_menu_item_ID;
            static ITEM_ID* main_test_menu_item_ID;
            static ITEM_ID* test_menu_cross_item_ID;
            static ITEM_ID* test_block_cross_menu_item_ID;
            static ITEM_ID* test_ascii_item_ID;
            static FDI_GENERIC_ITEM* genericItem_test_menu_cross;
            static FDI_GENERIC_ITEM* genericItem_main_test_menu;
            static FDI_GENERIC_ITEM* genericItem_device_root_menu;
            static FLAT_MENU* test_menu_cross;
            static FLAT_MENU* main_test_menu;
            static FLAT_MENU* device_root_menu;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase000E\\TestCase000E.ff6");                
                engine->Initialize();

                // get the ItemIds 
                device_root_menu_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"device_root_menu", device_root_menu_item_ID);

                main_test_menu_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", main_test_menu_item_ID);

                test_menu_cross_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_menu_cross", test_menu_cross_item_ID);

                test_block_cross_menu_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block_cross_menu", test_block_cross_menu_item_ID);

                test_ascii_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_ascii", test_ascii_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_cross_menu_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the menus
                genericItem_test_menu_cross = engine->GetGenericItem(
                    *test_menu_cross_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    iBlockInstance);

                test_menu_cross = dynamic_cast<FLAT_MENU*>(genericItem_test_menu_cross->item);

                genericItem_main_test_menu = engine->GetGenericItem(
                    *main_test_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    iBlockInstance);

                main_test_menu = dynamic_cast<FLAT_MENU*>(genericItem_main_test_menu->item);

                genericItem_main_test_menu = engine->GetGenericItem(
                    *main_test_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    iBlockInstance);

                main_test_menu = dynamic_cast<FLAT_MENU*>(genericItem_main_test_menu->item);

                genericItem_device_root_menu = engine->GetGenericItem(
                    *device_root_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    iBlockInstance);

                device_root_menu = dynamic_cast<FLAT_MENU*>(genericItem_device_root_menu->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete device_root_menu_item_ID;
                delete main_test_menu_item_ID;
                delete test_menu_cross_item_ID;
                delete test_block_cross_menu_item_ID;
                delete test_ascii_item_ID;
                delete genericItem_test_menu_cross;
                delete genericItem_main_test_menu;
                delete genericItem_device_root_menu;
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_item_id_for_the_test_menu_cross()
            {
                Assert::AreEqual(*test_menu_cross_item_ID, test_menu_cross->id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_item_id_for_the_main_test_menu()
            {
                Assert::AreEqual(*main_test_menu_item_ID, main_test_menu->id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_item_id_for_the_device_root_menu()
            {
                Assert::AreEqual(*device_root_menu_item_ID, device_root_menu->id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_number_of_menu_items_in_the_device_root_menu()
            {
                Assert::AreEqual(2u, (UINT32)device_root_menu->items.count); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_number_of_menu_items_in_the_main_test_menu()
            {
                Assert::AreEqual(1u, (UINT32)main_test_menu->items.count); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_correct_number_of_menu_items_in_the_test_menu_cross()
            {
                Assert::AreEqual(1u, (UINT32)test_menu_cross->items.count); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_main_test_menu_as_first_member_in_the_device_root_menu()
            {                                                          
                Assert::AreEqual(*main_test_menu_item_ID, device_root_menu->items.list[0].ref.op_info.id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_test_menu_cross_as_second_member_in_the_device_root_menu()
            {
                Assert::AreEqual(*test_menu_cross_item_ID, device_root_menu->items.list[1].ref.op_info.id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_test_ascii_as_first_member_in_the_test_menu_cross()
            {
                Assert::AreEqual(*test_ascii_item_ID, test_menu_cross->items.list[0].ref.op_info.id); 
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_the_test_menu_cross_as_first_member_in_the_main_test_menu()
            {
                Assert::AreEqual(*test_menu_cross_item_ID, main_test_menu->items.list[0].ref.op_info.id); 
            }
        };

        [TestClass]
        public ref class When_loading_a_menu_with_records_with_local_parameters
        {
        public: 
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FDI_GENERIC_ITEM* genericItem_test_record_local;
            static FDI_GENERIC_ITEM* genericItem_test_var;
            static FDI_GENERIC_ITEM* genericItem_test_double;
            static FLAT_MENU* test_menu;
            static FLAT_RECORD* test_record;
            static ITEM_ID* test_record_item_ID;
            static FLAT_RECORD* test_record_local;
            static ITEM_ID* test_record_local_item_ID;
            static FDI_GENERIC_ITEM* genericItem_record;

            static ITEM_ID* test_var_1_item_ID;
            static FLAT_VAR* test_var_1;

            static ITEM_ID* test_double_item_ID;
            static FLAT_VAR* test_double_var;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase000c\\TestCase000c.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_block
                ITEM_ID* test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // get the ItemIds for the test_block_2
                ITEM_ID* test_block_2_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block_2", test_block_2_item_ID);

                // get the ItemIds for the test_menu
                ITEM_ID* test_menu_item_ID = new ITEM_ID();
                int error =  engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", test_menu_item_ID);

                // get the ItemIds for the test_var_1
                test_var_1_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_1_item_ID);

                // get the ItemIds for the test_double
                test_double_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_double", test_double_item_ID);

                // get the ItemIds for the test_record
                test_record_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_record", test_record_item_ID);

                // get the ItemIds for the test_record_local
                test_record_local_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_record_local", test_record_local_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo[2];

                pFieldbusBlockInfo[0].m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo[0].m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo[0].m_usObjectIndex = 1000;

                pFieldbusBlockInfo[1].m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo[1].m_CharRecord.m_ulDDItemId = *test_block_2_item_ID;
                pFieldbusBlockInfo[1].m_usObjectIndex = 2000;

                int arrBlockInstance[2]; // Make sure this array is as big as the pFieldbusBlockInfo array
                const int iCount = 2;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, arrBlockInstance, iCount);
                
                // get the test_menu
                genericItem = engine->GetGenericItem(
                    *test_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    0);		// Use the default Device level iBlockInstance number

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);
                
                // get the test_record
                genericItem_record = engine->GetGenericItem(
                    *test_record_item_ID,
                    nsEDDEngine::ITYPE_RECORD,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_RECORD),
                    arrBlockInstance[0]);

                test_record = dynamic_cast<FLAT_RECORD*>(genericItem_record->item);

                // get the test_record_local
                genericItem_test_record_local = engine->GetGenericItem(
                    *test_record_local_item_ID,
                    nsEDDEngine::ITYPE_RECORD,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_RECORD),
                    arrBlockInstance[0]);

                test_record_local = dynamic_cast<FLAT_RECORD*>(genericItem_test_record_local->item);

                // get the test_var_1
                genericItem_test_var = engine->GetGenericItem(
                    *test_var_1_item_ID,
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    arrBlockInstance[0]);

                test_var_1 = dynamic_cast<FLAT_VAR*>(genericItem_test_var->item);

                // get the test_double
                genericItem_test_double = engine->GetGenericItem(
                    *test_double_item_ID,
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    arrBlockInstance[0]);

                test_double_var = dynamic_cast<FLAT_VAR*>(genericItem_test_double->item);

                delete [] pFieldbusBlockInfo;
                delete test_block_item_ID;
                delete test_block_2_item_ID;
                delete test_menu_item_ID;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete genericItem_test_record_local;
                delete genericItem_test_var;
                delete genericItem_test_double;
                delete genericItem_record;
				delete test_var_1_item_ID;
				delete test_double_item_ID;
				delete test_record_item_ID;
				delete test_record_local_item_ID;
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_return_a_generic_item_without_errors()
            {
                Assert::AreEqual((ushort) 0, genericItem->errors.count);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_two_elements_in_the_item_list()
            {
                Assert::AreEqual((ushort) 4, test_menu->items.count);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as_first_element_in_the_item_list_the_test_record_local()
            {
                Assert::AreEqual(test_record_local->id, test_menu->items.list[0].ref.op_info.id);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as_second_element_in_the_item_list_the_record_which_contains_the_test_var()
            {
                Assert::AreEqual(test_record_local->id, test_menu->items.list[1].ref.op_info.id);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as_third_element_in_the_item_list_the_record()
            {
                Assert::AreEqual(test_record->id, test_menu->items.list[2].ref.op_info.id);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as_forth_element_in_the_item_list_the_record_which_contains_test_double()
            {
                Assert::AreEqual(test_record->id, test_menu->items.list[3].ref.op_info.id);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as__the_subindex_one_for_the_second_element_in_the_item_list()
            {
                Assert::AreEqual(1u, test_menu->items.list[1].ref.op_info.member);
            }

            [TestMethod, TestCategory("CrossBlockTest")]
            void It_should_have_as_the_subindex_two_for_the_third_element_in_the_item_list()
            {
                Assert::AreEqual(2u, test_menu->items.list[2].ref.op_info.member);
            }
        };

    } // namespace FF
} // namespace ComponentTests
