#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T14:05:48
#
#-------------------------------------------------

QT       -= gui

TARGET = MIEngineInterface
TEMPLATE = lib
CONFIG += staticlib
#CONFIG +=plugin
DEFINES += MIENGINEINTERFACE_LIBRARY \
           UNICODE
QMAKESPEC=linux-g++-32
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=gnu++11


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += MiEngine.cpp \
    stdafx.CPP

HEADERS += ../../../../inc/Ole/EDDLMethInterp/AsyncResult.h \
    ../../../../inc/Ole/EDDLMethInterp/ManagedMiInterfaces.h \
    ../../../../inc/Ole/EDDLMethInterp/MiEngine.h \
    ../../../../inc/Ole/EDDLMethInterp/std.h \
    stdafx.H \

unix {
    target.path = /usr/lib/
    INSTALLS += target
}

DESTDIR = ../../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u
include($$DESTDIR/../../../Src/SuppressWarning.pri)

INCLUDEPATH += $$PWD/../COMMON/
INCLUDEPATH += $$PWD/../../../../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../../inc/
INCLUDEPATH += $$PWD/../../../../../../Inc/
INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Linux
}

INCLUDEPATH += $$PWD/../APPS/APPsupport/Interpreter/
INCLUDEPATH += $$PWD/../


INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../APPS/APPsupport/DevServices/ -lDevServices
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../APPS/APPsupport/DevServices/ -lDevServicesd

INCLUDEPATH += $$PWD/../APPS/APPsupport/DevServices
DEPENDPATH += $$PWD/../APPS/APPsupport/DevServices
