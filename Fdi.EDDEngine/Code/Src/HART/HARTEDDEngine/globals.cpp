

#include "stdafx.h"

#include "DDSSupport.h"



 //Param Cache

int app_func_get_param_value (ENV_INFO *env_info, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value)
{
	int rc = DDL_SUCCESS;
	ParamMap::iterator it;
	if(env_info->pParamMap)
	{
		it = env_info->pParamMap->find(*op_ref);

		if (it != env_info->pParamMap->end())	// Set param_value, if it is found
		{
			*param_value = it->second;
		}

		else
		{
			IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
			rc = pDDSSupport->DDS_GetParamValue( env_info, op_ref, param_value );

			if (rc == DDL_SUCCESS) 
			{
				// This stores param_value using op_ref as key.
				(*(env_info->pParamMap))[*op_ref] = *param_value;
			}
		}
	}
	else
	{	
		IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
		rc = pDDSSupport->DDS_GetParamValue( env_info, op_ref, param_value );

		if (rc == DDL_SUCCESS) 
		{
			env_info->pParamMap = new ParamMap();

			// This stores param_value using op_ref as key.
			(*(env_info->pParamMap))[*op_ref] = *param_value;
		}
	}

	return rc;
}


