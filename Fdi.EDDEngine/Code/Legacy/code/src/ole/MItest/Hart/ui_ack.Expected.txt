<Method>
{ 
  int iR1, iR2, iR3, iR4, iR5, iR6, iR7;
  int iaV1[1], iV1, iV2;
  char cmd_status[3];

  
  iR1 = ACKNOWLEDGE("This is built-in ACKNOWLEDGE");
  iaV1[0] = 16448;
  iR2 = acknowledge("This is built-in acknowledge with value %[4.2f]{0}", iaV1);

  iV1 = 16476;
  iV2 = 0x01;
  iR3 = _display_xmtr_status(iV1, iV2);

  iR4 = DISPLAY("This is built-in DISPLAY");

  iV1 = 0x01;
  iR5 = send(iV1, cmd_status);
  iR6 = display_response_status(iV1, cmd_status[0]);
  iR6 = display_response_status(iV1, cmd_status[1]);
  iR6 = display_response_status(iV1, cmd_status[2]);
}
</Method>
<Function>
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_CHARPTR
    Param 1 Value: This is built-in ACKNOWLEDGE
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: acknowledge
  Number Of Input Parameters: 2
    Param 1 Type: RUL_CHARPTR
    Param 1 Value: This is built-in acknowledge with value %[4.2f]{0}
    Param 2 Type: RUL_SAFEARRAY
    Param 2 Array[0]: 16448
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function acknowledge
Function Name: _display_xmtr_status
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 16476
    Param 2 Type: RUL_INT
    Param 2 Value: 1
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _display_xmtr_status
Function Name: DISPLAY
  Number Of Input Parameters: 1
    Param 1 Type: RUL_CHARPTR
    Param 1 Value: This is built-in DISPLAY
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function DISPLAY
Function Name: send
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 1
    Param 2 Type: RUL_SAFEARRAY
    Param 2 Array: 
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function send
Function Name: display_response_status
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 1
    Param 2 Type: RUL_CHAR
    Param 2 Value:  
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function display_response_status
Function Name: display_response_status
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 1
    Param 2 Type: RUL_CHAR
    Param 2 Value:  
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function display_response_status
Function Name: display_response_status
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 1
    Param 2 Type: RUL_CHAR
    Param 2 Value: 
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function display_response_status
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><iR1>0,</iR1><iR2>0,</iR2><iR3>0,</iR3><iR4>0,</iR4><iR5>0,</iR5><iR6>0,</iR6><iR7>0,</iR7><iaV1>[0]16448,</iaV1><iV1>1,</iV1><iV2>1,</iV2><cmd_status>[0][1][2],[3]</cmd_status></SymbolTable>
