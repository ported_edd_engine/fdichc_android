
// Flats.h

#pragma once

#include <map>
#include <typeinfo>
#include "nsEDDEngine/Attrs.h"

namespace nsEDDEngine
{

	//
	//	FLAT_MASKS - Each AttributeNameSet contains a set of AttributeName enum values which are true
	//					for that set.
	//
	//				Example: if there is a binary definition for the "style" attribute for this Item,
	//							AttributeName::style will be in the "bin_exists" set.
	//						 Similiarly, if the "scaling_factor" attribute for this Item is dynamic,
	//							AttributeName::scaling_factor will be in the "dynamic" set.
	//
	//				You can mimic setting, clearing and checking bits in a bitmask by using a std::set
	//					with the "insert", "erase", and "find" operations.

	class _INTERFACE FLAT_MASKS {

	public:

		AttributeNameSet	bin_exists;	// Attributes in this set have a definition in the binary file,
		//  in other words, they appeared explicitly in the EDDL source.

		AttributeNameSet	bin_hooked;	// Attributes in this set have the binary chunk hooked up to
		//  the DEPBIN struct corresponding to that attribute.

		AttributeNameSet	attr_avail;	// Attributes in this set have a value available in the FLAT_*
		//  corresponding to that attribute. This value may have come
		//  from the EDDL source or a default value was used.

		AttributeNameSet	dynamic;	// Attributes in this set are dynamic. That is, in order to 
		//  determine their value, a value from the Host Parameter Cache
		//  was needed. This happens when an expression or a conditional
		//  keyword is used to define this attribute.
		FLAT_MASKS() { };
		virtual ~FLAT_MASKS() {};

		//copy constructor
		FLAT_MASKS(const FLAT_MASKS &flat_masks);
		
		//Overloaded Assignment Operator 
		FLAT_MASKS& operator= (const FLAT_MASKS &flat_masks);
	};

	class DEPBIN;

	struct AttributeItem
	{
		const char *type;
		DEPBIN *depbin;		
		DEPBIN **depbin_storage;
		void *attribute;
	};


#define	DEPBIN_MAP(id,db,attr) depbin_map[id].depbin=db; depbin_map[id].depbin_storage=&db; depbin_map[id].attribute=&attr; depbin_map[id].type=typeid(attr).name();

	typedef struct tag_ATTRIBUTE_DATA
	{
		int tag;
		int size;
		int default_id;
		int protocol;
	} ATTRIBUTE_DATA;

	struct AttributeDataItem
	{
		const char *type;
		ATTRIBUTE_DATA attribute_data;
		int exists;
		int errors;
		int oks;
	};


#define	ATTRIBUTE_MAP(id,datatype,tag,size,defaultid, prot) attribute_map[id].attribute_data.tag=tag; attribute_map[id].attribute_data.protocol=prot; attribute_map[id].attribute_data.size=size; attribute_map[id].attribute_data.defaultid=defaultid; attribute_map[id].type=typeid(datatype).name();attribute_map[id].exists=0;attribute_map[id].oks=0;attribute_map[id].errors=0;

	class _INTERFACE ItemBase
	{
	private:
		void AssignItemBase(const ItemBase &item_base);

	public:
		ITEM_ID		id;				// Item id
		STRING		symbol_name;	// Symbol Name
		ITEM_TYPE	item_type;		// Type of Item defined in ITEM_TYPE enum.
		FLAT_MASKS  masks;			// FLAT_MASK object for item.

        char _dummy[50]{0};

		bool IsDynamic(AttributeName eAttrName);
		bool isAvailable(AttributeName eAttrName);

	private:			// Declare a private default constructor, but don't provide a body.
		ItemBase();		// This will trigger a compile or link error, if anyone ever tries to use it.

	public:
		ItemBase(ITEM_TYPE iType)	// This should be the only constructor used.
		{
			// masks has its own constructor
			id = 0;
			item_type = iType;
		};

		virtual ~ItemBase();

		//copy constructor
		ItemBase(const ItemBase &item_base);
		
		//Overloaded Assignment Operator 
		ItemBase& operator= (const ItemBase &item_base);

		virtual const char *GetTypeName() = 0;

		// Method, which when implemented in the inheriting class, creates a copy of the object.
		virtual ItemBase* Replicate() = 0;

		void FreeDepbins();
		virtual void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) = 0;

		enum FDI_Protocol
		{
			ProtocolNONE		= 0,	// None
			ProtocolFF			= 0x01,	// FieldBus
			ProtocolHART		= 0x02,	// HART
			ProtocolPB			= 0x04, // Profibus
			ProtocolAll			= 0x07	// All
		};

		static void GetAttributeDataMap(std::map<AttributeName, AttributeDataItem>& attribute_map)
		{
			int tag=0;
			int size=0;
			int default_id=0;
			int type=0;
			type=type;

			ATTRIBUTE_MAP(item_information			, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(label						, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(help						, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(validity					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(members					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(handling					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(response_codes			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(height					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(post_edit_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(pre_edit_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(refresh_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(relation_update_list		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(relation_watch_list		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(relation_watch_variable	, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(width						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(access					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(class_attr				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(constant_unit				, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(cycle_time				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(emphasis					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(exit_actions				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(identity					, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(init_actions				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(line_color				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(line_type					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(max_value					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(min_value					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(command_number			, type, tag, size, default_id			, ProtocolHART | ProtocolPB);
			ATTRIBUTE_MAP(post_read_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(post_write_actions		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(pre_read_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(pre_write_actions			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(type_definition			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(y_axis					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(appinstance				, type, tag, size, default_id			, ProtocolNONE);
			ATTRIBUTE_MAP(axis_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(block_b					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(block_b_type				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(capacity					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(characteristics			, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(chart_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(chart_type				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(charts					, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(collection_items			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(block_number				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(count						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(default_value				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(default_values			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(definition				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(display_format			, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(display_items				, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(edit_display_items		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(edit_format				, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(edit_items				, type, tag, size, default_id			, ProtocolFF | ProtocolHART);
			ATTRIBUTE_MAP(elements					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(enumerations				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(file_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(first						, type, tag, size, default_id			, ProtocolNONE);
			ATTRIBUTE_MAP(graph_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(graphs					, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(grid_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(grids						, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(image_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(image_table_index			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(index						, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(indexed					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(initial_value				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(items						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(keypoints_x_values		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(keypoints_y_values		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(last						, type, tag, size, default_id			, ProtocolNONE);
			ATTRIBUTE_MAP(length					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(link						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(list_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(lists						, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(local_parameters			, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(menu_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(menus						, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(method_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(method_parameters			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(method_type				, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(methods					, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(number_of_elements		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(number_of_points			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(on_update_actions			, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(operation					, type, tag, size, default_id			, ProtocolHART | ProtocolPB);
			ATTRIBUTE_MAP(orientation				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(parameter_lists			, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(parameters				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(post_user_actions			, type, tag, size, default_id			, ProtocolNONE);
			ATTRIBUTE_MAP(post_rqst_actions			, type, tag, size, default_id			, ProtocolNONE);
			ATTRIBUTE_MAP(uuid						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(reference_array_items		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(refresh_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(scaling					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(scaling_factor			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(slot						, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(source_items				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(style						, STRING, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(sub_slot					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(time_format				, STRING, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(time_scale				, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(transaction				, type, tag, size, default_id			, ProtocolHART | ProtocolPB);
			ATTRIBUTE_MAP(unit_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(variable_type				, TYPE_SIZE, tag, size, default_id		, ProtocolAll);
			ATTRIBUTE_MAP(vectors					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(waveform_items			, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(waveform_type				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(write_as_one_items		, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(component_byte_order		, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(x_axis					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(x_increment				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(x_initial					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(x_values					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(y_values					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(view_min					, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(view_max					, type, tag, size, default_id			, ProtocolHART);
			ATTRIBUTE_MAP(reserved117				, type, tag, size, default_id			, ProtocolAll); // min_axis
			ATTRIBUTE_MAP(reserved118				, type, tag, size, default_id			, ProtocolAll); // max_axis
			ATTRIBUTE_MAP(reserved119				, type, tag, size, default_id			, ProtocolPB);  // default_reference
			ATTRIBUTE_MAP(addressing				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(can_delete				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(check_configuration		, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(classification			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(component_parent			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(component_path			, STRING, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(component_relations		, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(components				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(declaration				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(detect					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(device_revision			, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(device_type				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(edd						, STRING, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(manufacturer				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(maximum_number			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(minimum_number			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(protocol					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(redundancy				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(relation_type				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(component_connect_point	, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(scan						, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(scan_list					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(private_attr				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(visibility				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(product_uri				, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(api						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(header					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(write_mode				, type, tag, size, default_id			, ProtocolFF);
			ATTRIBUTE_MAP(plugin_items				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(files						, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(plugins					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(initial_values			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(post_rqstreceive_actions	, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(shared					, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(variable_status			, type, tag, size, default_id			, ProtocolPB);
			ATTRIBUTE_MAP(count_ref					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(min_value_ref				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(max_value_ref				, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(image_id					, type, tag, size, default_id			, ProtocolAll);
			ATTRIBUTE_MAP(_end_						, type, tag, size, default_id			, ProtocolNONE);
		}
	};

	//
	// FDI_GENERIC_ITEM
	//

	class _INTERFACE RETURN_INFO {
	public:
		AttributeName   bad_attr;	// Bit indicating which attribute is bad
		int             rc;			// Return code to be passed back
		OP_REF          var_needed;	// Used to store op_ref if unable to get value from parameter value service.

		RETURN_INFO() 
		{
			bad_attr = _end_;
			rc = 0;
			// var_needed has its own constructor
		}
		~RETURN_INFO() {};

		//copy constructor
		RETURN_INFO(const RETURN_INFO &return_info);
		
		//Overloaded Assignment Operator 
		RETURN_INFO& operator= (const RETURN_INFO &return_info);
	};

	const int MAX_RETURN_LIST_SIZE = 32;	// Max number of elements which can be returned in a RETURN_INFO list

	class _INTERFACE RETURN_LIST {
	public:
		unsigned short  count;				// number of items in the RETURN_INFO list
		RETURN_INFO list[MAX_RETURN_LIST_SIZE];	// list of return information
		RETURN_LIST()
		{
			count = 0;
		}

		~RETURN_LIST() {};

		//copy constructor
		RETURN_LIST(const RETURN_LIST &return_list);
		
		//Overloaded Assignment Operator 
		RETURN_LIST& operator= (const RETURN_LIST &return_list);
	};


	class _INTERFACE FDI_GENERIC_ITEM
	{
	private:
		//AssignGenericItem is used to assign all the attributes of GENERIC_ITEM
		void AssignGenericItem(const FDI_GENERIC_ITEM &generic_item);
	public:

		ITEM_TYPE   item_type;	// the type of item

		ItemBase    *item;	    // a pointer to the item (FLAT_* data structure)

		RETURN_LIST errors;		// list of errors encountered during the last evaluation

		FDI_GENERIC_ITEM(ITEM_TYPE type)
		{
			item_type = type;
			item = nullptr;
			//errors has its own constructor
		}

		~FDI_GENERIC_ITEM() 
		{
			delete item;
			item = nullptr;
		}

		//copy constructor
		FDI_GENERIC_ITEM(const FDI_GENERIC_ITEM &generic_item);
		
		//Overloaded Assignment Operator 
		FDI_GENERIC_ITEM& operator= (const FDI_GENERIC_ITEM &generic_item);
	};


class _INTERFACE DEPBIN {
         public:
                 OP_REF_LIST     dep;
                 unsigned long   bin_size;
                 unsigned char  *bin_chunk;
                 bool            allocated;

                 // Default constructor
                 // Used by Hart_ConvDEPBIN, FF_ConvDEPBIN as the default constructor for the DEPBIN members
                 DEPBIN()
                 {
                          // dep has its own constructor
                          bin_size = 0;
                          bin_chunk        = nullptr;
                          allocated        = false;

                 };               
                 
                 // Constructor:
                 //       Create new depbin either copying the contents of chunk or reusing the pointer of chunk
                 //  depending on the value of copy_chunk.
                 DEPBIN(unsigned char *chunk, unsigned long size, bool copy_chunk)
                 {
                          // dep has its own constructor
                          this->bin_size   = size;
                          this->bin_chunk = nullptr;
                          this->allocated = false;

                          if(size>0)
                          {
                                  if(copy_chunk)
                                  {
                                           this->bin_chunk  = new unsigned char[size];
                                           memcpy(this->bin_chunk, chunk, size);
                                           allocated=true;
                                  }
                                  else
                                  {
                                           this->bin_chunk=chunk;
                                  }
                          }

                 };


                 void CopyTo(void *old_depbin);
                 static void CopyFromWithDelete(DEPBIN *pSource, DEPBIN **pDestination);

				 // Destructor will only delete the bin_chunk if it was allocated for
				 // this instance.  Otherwise the memory belongs to another class instance.
                 virtual ~DEPBIN()
                 {
					if(this->bin_chunk  != nullptr && this->allocated)
					{
						delete [] this->bin_chunk;
						this->bin_chunk = nullptr;
					}        
                 };

                 //copy constructor
                 DEPBIN(const DEPBIN &depbin);
                 
                 //Overloaded Assignment Operator 
                 DEPBIN& operator= (const DEPBIN &depbin);

         private:

                 //AssignDepbin function to assign all the values
                 void AssignDepbin(const DEPBIN &depbin);

         };



	/*
	*	FLAT structures
	*/


	/*
	* VARIABLE item
	*/

	class _INTERFACE VAR_DEPBIN 
	{
	private:
		//AssignVarDepbin function to assign all the values
		void AssignVarDepbin(const VAR_DEPBIN &var_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN         *db_class;				// CLASS aatribute value.
		DEPBIN         *db_handling;			// HANDLING attribute value.
		DEPBIN         *db_constant_unit;		// CONSTANT_UNIT attribute value.
		DEPBIN         *db_label;				// LABEL attribute value.
		DEPBIN         *db_help;				// HELP attribute value.

		DEPBIN         *db_valid;				// VALIDITY attribute value.

		DEPBIN         *db_pre_read_act;		// PRE_READ_ACTIONS attribute value.
		DEPBIN         *db_post_read_act;		// POST_READ_ACTIONS attribute value.
		DEPBIN         *db_pre_write_act;		// PRE_WRITE_ACTIONS attribute value.
		DEPBIN         *db_post_write_act;		// POST_WRITE_ACTIONS attribute value.
		DEPBIN         *db_pre_edit_act;		// PRE_EDIT_ACTIONS attribute value.
		DEPBIN         *db_post_edit_act;		// POST_EDIT_ACTIONS attribute value.

		DEPBIN         *db_type_size;
		DEPBIN         *db_display;				// DISPLAY_FORMAT attribute value.
		DEPBIN         *db_edit;				// EDIT_FORMAT attribute value.

		DEPBIN         *db_min_val;				// MIN_VLAUE attribute value.
		DEPBIN         *db_max_val;				// MAX_VLAUE attribute value.

		DEPBIN         *db_enums;

		DEPBIN         *db_default_value;		// DEFAULT_VALUE attribute value. 
		DEPBIN         *db_refresh_act;			// REFRESH_ACTIONS attribute value.

		DEPBIN		   *db_indexed;
		DEPBIN		   *db_initial_value;		// INITIAL_VALUE attribute value.
		DEPBIN		   *db_post_user_action;
		DEPBIN		   *db_post_rqst_actions;
		DEPBIN		   *db_private_attr;		// PRIVATE attribute value.
		DEPBIN		   *db_visibility;			// VISIBILITY attribute value.

		DEPBIN			*db_height;				// HEIGHT attribute value.
		DEPBIN			*db_width;				// WIDTH attribute value.
		DEPBIN			*db_time_scale;			// TIME_SCALE attribute value.
		DEPBIN			*db_time_format;		// TIME_FORMAT attribute value.
		DEPBIN			*db_scaling_factor;		// SCALING_FACTOR attribute value.
		DEPBIN			*db_response_codes;		// RESPONSE_CODES attribute value.
		DEPBIN			*db_write_mode;			// WRITE_MODE attribute value.


		VAR_DEPBIN() : db_class(0), db_handling(0), db_constant_unit(0), db_label(0), db_help(0), db_valid(0), db_pre_read_act(0), db_post_read_act(0),
			db_pre_write_act(0), db_post_write_act(0), db_pre_edit_act(0), db_post_edit_act(0), db_type_size(0), db_display(0)
			, db_edit(0), db_min_val(0), db_max_val(0), db_enums(0), db_default_value(0), db_refresh_act(0)
			,  db_indexed(0), db_initial_value(0)
			, db_post_user_action(0), db_post_rqst_actions(0), db_private_attr(0), db_visibility(0)
			, db_height(0), db_width(0), db_time_scale(0), db_time_format(0), db_scaling_factor(0), db_response_codes(0), db_item_information(0), db_write_mode(0)
			 {};

		virtual ~VAR_DEPBIN();

		//copy constructor
		VAR_DEPBIN(const VAR_DEPBIN &var_depbin);
		
		//Overloaded Assignment Operator 
		VAR_DEPBIN& operator= (const VAR_DEPBIN &var_depbin);
	};

	class _INTERFACE FLAT_VAR : public ItemBase
	{
	private:
		//AssignFlatVar is used to assign all the attributes of FLAT_VAR
		void AssignFlatVar(const FLAT_VAR &flat_var);
	public:

		ITEM_INFORMATION item_info;

		ClassType       class_attr;			// CLASS attribute value.
		Handling        handling;			// HANDLING attribute value.
		STRING          label;				// LABEL attribute value.
		STRING          help;				// HELP attribute value.

		unsigned long   read_time_out;		// READ_TIME_OUT attribute value.
		unsigned long   write_time_out;		// WRITE_TIME_OUT attribute value.
		Boolean         valid;				// VALIDITY attribute value.

		ACTION_LIST	    pre_read_act;		// PRE_READ_ACTIONS attribute value.
		ACTION_LIST	    post_read_act;		// POST_READ_ACTIONS attribute value.
		ACTION_LIST	    pre_write_act;		// PRE_WRITE_ACTIONS attribute value.
		ACTION_LIST	    post_write_act;		// POST_WRITE_ACTIONS attribute value.
		ACTION_LIST	    pre_edit_act;		// PRE_EDIT_ACTIONS attribute value.
		ACTION_LIST	    post_edit_act;		// POST_EDIT_ACTIONS attribute value.

		RESPONSE_CODES	resp_codes;			// RESPONSE_CODES attribute value.
		TYPE_SIZE       type_size;			// contains variable type and size.
		STRING          display;			// DISPLAY_FORMAT attribute value.
		STRING          edit;				// EDIT_FORMAT attribute value.

		RANGE_DATA_LIST min_val;			// MIN_VLAUE attribute value.
		RANGE_DATA_LIST max_val;			// MAX_VLAUE attribute value.

		EXPR            scaling_factor;		// SCALING_FACTOR attribute value.
		ENUM_VALUE_LIST enums;				// 

		EXPR			default_value;		// DEFAULT_VALUE attribute value. 
		ACTION_LIST	    refresh_act;		// REFRESH_ACTIONS attribute value.
		STRING			style;				// STYLE attribute value.
		STRING			constant_unit;		// CONSTANT_UNIT attribute value.

		DisplaySize		height;				// HEIGHT attribute value.
		DisplaySize		width;				// WIDTH attribute value.

		STRING			time_format;		// TIME_FORMAT attribute value.
		TimeScale		time_scale;			// TIME_SCALE attribute value.

		//		New attributes added as per specs.
		ITEM_ID 	indexed;				// INDEXED attribute value.
		EXPR			initial_value;		// INITIAL_VALUE attribute value.
		ACTION_LIST		post_user_action;	// 
		ACTION_LIST		post_rqst_actions;	

		Boolean			private_attr;		// PRIVATE attribute value.
		Boolean			visibility;			// VISIBILITY attribute value.

		WriteMode		write_mode;			// WRITE_MODE attribute value.
		///////////////////////////////////////////////

		VAR_DEPBIN     *depbin;

		//copy constructor
		FLAT_VAR(const FLAT_VAR &flat_var);
		
		//Overloaded Assignment Operator 
		FLAT_VAR& operator= (const FLAT_VAR &flat_var);

		//create a table mapping the object to the attribute
		//	map(attr,item,type, depbin) -> decode & depbin
		//	decodeMap(type,function)
		//	attributeMap[attr]
		FLAT_VAR(): ItemBase(ITYPE_VARIABLE)
		{
			class_attr		 = CT_NONE;
			handling		 = (Handling)0;
			read_time_out	 = 0;
			write_time_out	 = 0;
			valid			 = True;
			time_scale		 = nsEDDEngine::FDI_TIME_SCALE_NONE;
			height			 = DISPSIZE_XXX_SMALL;
			width			 = DISPSIZE_X_SMALL;
			//write_mode		 = 0;

			// scaling_factor has its own contructor
			// default_value has its own contructor


			indexed = 0;
			// initial_value has its own contructor
			// post_user_action has its own contructor
			// post_rqst_actions has its own contructor
			private_attr = False;
			visibility	 = True;
			write_mode = (nsEDDEngine::WriteMode)0;
			depbin = new VAR_DEPBIN(); 
		};

		virtual ~FLAT_VAR();

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_VAR(*this); };

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
		//	int null=0;

			DEPBIN_MAP(nsEDDEngine::item_information		, depbin->db_item_information,		item_info);	
			DEPBIN_MAP(nsEDDEngine::label					, depbin->db_label,					label);				
			DEPBIN_MAP(nsEDDEngine::help 					, depbin->db_help,					help);				
			DEPBIN_MAP(nsEDDEngine::validity				, depbin->db_valid,					valid);				
			DEPBIN_MAP(nsEDDEngine::variable_type			, depbin->db_type_size,				type_size);			
			DEPBIN_MAP(nsEDDEngine::handling 				, depbin->db_handling,				handling);			
			DEPBIN_MAP(nsEDDEngine::response_codes			, depbin->db_response_codes,		resp_codes);		
			DEPBIN_MAP(nsEDDEngine::post_edit_actions		, depbin->db_post_edit_act,			post_edit_act);		
			DEPBIN_MAP(nsEDDEngine::pre_edit_actions		, depbin->db_pre_edit_act,			pre_edit_act);		
			DEPBIN_MAP(nsEDDEngine::refresh_actions			, depbin->db_refresh_act,			refresh_act);			
			DEPBIN_MAP(nsEDDEngine::class_attr 				, depbin->db_class,					class_attr);				
			DEPBIN_MAP(nsEDDEngine::constant_unit			, depbin->db_constant_unit,			constant_unit);				
			DEPBIN_MAP(nsEDDEngine::max_value				, depbin->db_max_val,				max_val);				
			DEPBIN_MAP(nsEDDEngine::min_value				, depbin->db_min_val,				min_val);				
			DEPBIN_MAP(nsEDDEngine::post_read_actions		, depbin->db_post_read_act,			post_read_act);		
			DEPBIN_MAP(nsEDDEngine::post_write_actions		, depbin->db_post_write_act,		post_write_act);		
			DEPBIN_MAP(nsEDDEngine::pre_read_actions		, depbin->db_pre_read_act,			pre_read_act);		
			DEPBIN_MAP(nsEDDEngine::pre_write_actions		, depbin->db_pre_write_act,			pre_write_act);		
			DEPBIN_MAP(nsEDDEngine::indexed     			, depbin->db_indexed,				indexed);				
			DEPBIN_MAP(nsEDDEngine::default_value			, depbin->db_default_value,			default_value);		
			DEPBIN_MAP(nsEDDEngine::display_format			, depbin->db_display,				display);				
			DEPBIN_MAP(nsEDDEngine::edit_format				, depbin->db_edit,					edit);				
			DEPBIN_MAP(nsEDDEngine::enumerations			, depbin->db_enums,					enums);				
			DEPBIN_MAP(nsEDDEngine::initial_value			, depbin->db_initial_value,			initial_value);		
			DEPBIN_MAP(nsEDDEngine::post_user_actions		, depbin->db_post_user_action,		post_user_action);	
			DEPBIN_MAP(nsEDDEngine::post_rqst_actions		,	depbin->db_post_rqst_actions,	post_rqst_actions);	
			DEPBIN_MAP(nsEDDEngine::scaling_factor			, depbin->db_scaling_factor,		scaling_factor);		
			DEPBIN_MAP(nsEDDEngine::time_format				, depbin->db_time_format,			time_format);			
			DEPBIN_MAP(nsEDDEngine::time_scale				, depbin->db_time_scale,			time_scale);			
			DEPBIN_MAP(nsEDDEngine::height					, depbin->db_height,				height);				
			DEPBIN_MAP(nsEDDEngine::width					, depbin->db_width,					width);				
			DEPBIN_MAP(nsEDDEngine::private_attr			, depbin->db_private_attr,			private_attr);				

			DEPBIN_MAP(nsEDDEngine::write_mode				, depbin->db_write_mode,			write_mode);				
			DEPBIN_MAP(nsEDDEngine::visibility				, depbin->db_visibility,			visibility);				
		}

		static AttributeNameSet const All_Attrs;
	};

	/*
	*	Axis Item
	*/

	class _INTERFACE AXIS_DEPBIN {
	private:
		//AssignAxisDepbin function to assign all the attributes of AXIS_DEPBIN
		void AssignAxisDepbin(const AXIS_DEPBIN &axis_depbin);

	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_min_axis;
		DEPBIN      *db_max_axis;
		DEPBIN      *db_scaling;
		DEPBIN      *db_constant_unit;

		AXIS_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_min_axis(0), db_max_axis(0),
			db_scaling(0), db_constant_unit(0) {};

		//copy constructor
		AXIS_DEPBIN(const AXIS_DEPBIN &axis_depbin);
		
		//Overloaded Assignment Operator 
		AXIS_DEPBIN& operator= (const AXIS_DEPBIN &axis_depbin);

		virtual ~AXIS_DEPBIN();
	};

	class _INTERFACE FLAT_AXIS : public ItemBase
	{
		//AssignFlatAxis is used to assign all the attributes of FLAT_AXIS
		void AssignFlatAxis(const FLAT_AXIS &flat_axis);
	public:
		ITEM_INFORMATION item_info;
		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.

		EXPR            min_axis;		// MIN_VALUE attribute value.
		OP_REF			min_axis_ref;	// MIN_VALUE variable reference
		EXPR            max_axis;		// MAX_VALUE attribute value.
		OP_REF			max_axis_ref;	// MAX_VALUE variable reference

		Scaling			scaling;		// SCALING attribute value.
		STRING          constant_unit;	// CONSTANT_UNIT attribute value.

		AXIS_DEPBIN     *depbin;

		FLAT_AXIS():ItemBase(ITYPE_AXIS)
		{
			// min_axis has its own contructor
			// max_axis has its own contructor
			scaling = (Scaling)0;
			depbin = new AXIS_DEPBIN();
		}

		virtual ~FLAT_AXIS();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}
		
		virtual ItemBase* Replicate() { return new FLAT_AXIS(*this); };

		//copy constructor
		FLAT_AXIS(const FLAT_AXIS &flat_axis);
		
		//Overloaded Assignment Operator 
		FLAT_AXIS& operator= (const FLAT_AXIS &flat_axis);


		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{

			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::min_value				, depbin->db_min_axis			, min_axis);
			DEPBIN_MAP( nsEDDEngine::max_value				, depbin->db_max_axis			, max_axis);
			DEPBIN_MAP( nsEDDEngine::scaling				, depbin->db_scaling			, scaling);
			DEPBIN_MAP( nsEDDEngine::constant_unit			, depbin->db_constant_unit		, constant_unit);
			DEPBIN_MAP( nsEDDEngine::max_value_ref			, depbin->db_max_axis			, max_axis_ref);
			DEPBIN_MAP( nsEDDEngine::min_value_ref			, depbin->db_min_axis			, min_axis_ref);
		}
	};

	/*
	*	CHART Item
	*/

	class _INTERFACE CHART_DEPBIN {
	private:
		//AssignChartDepbin is used to assign all the attributes of CHART_DEPBIN
		void AssignChartDepbin(const CHART_DEPBIN &chart_depbin);
	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_members;
		DEPBIN      *db_cycle_time;
		DEPBIN      *db_height;
		DEPBIN      *db_width;
		DEPBIN      *db_length;
		DEPBIN      *db_type;
		DEPBIN      *db_valid;

		DEPBIN		*db_visibility;

		CHART_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_members(0), db_cycle_time(0), db_height(0), 
			db_width(0), db_length(0), db_type(0), db_valid(0), db_visibility(0) {};

		//copy constructor
		CHART_DEPBIN(const CHART_DEPBIN &chart_depbin);
		
		//Overloaded Assignment Operator 
		CHART_DEPBIN& operator= (const CHART_DEPBIN &chart_depbin);

		virtual ~CHART_DEPBIN();
	};

	class _INTERFACE FLAT_CHART : public ItemBase
	{
	private:
		//AssignFlatChart is used to assign all the attributes of FLAT_CHART
		void AssignFlatChart(const FLAT_CHART &flat_chart);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;		// HELP attribute value.
		STRING          label;		// LABEL attribute value.
		MEMBER_LIST     members;	// MEMBERS attribute value.
		DisplaySize     height;		// HEIGHT attribute value.
		DisplaySize     width;		// WIDTH attribute value.
		ulong			cycle_time; // CYCLE_TIME attribute value.
		ulonglong		length;		// LENGTH attribute value.
		ChartType       type;		// TYPE attribute value.	
		Boolean         valid;		// VALIDITY attribute value.


		Boolean			visibility;	// VISIBILITY attribute value.

		CHART_DEPBIN    *depbin;

		FLAT_CHART():ItemBase(ITYPE_CHART)
		{
			// members has its own constructor
			height		= DISPSIZE_MEDIUM; // The default height and width for Charts is MEDIUM
			width		= DISPSIZE_MEDIUM;
			cycle_time	= DEFAULT_REFRESH_CYCLE_TIME;
			length		= 0;
			type		= (ChartType)0;
			valid		= True;

			visibility	= True;

			depbin = new CHART_DEPBIN();
		}
		virtual ~FLAT_CHART();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}
		
		virtual ItemBase* Replicate() { return new FLAT_CHART(*this); };

		//copy constructor
		FLAT_CHART(const FLAT_CHART &flat_chart);
		
		//Overloaded Assignment Operator 
		FLAT_CHART& operator= (const FLAT_CHART &flat_chart);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::members				, depbin->db_members			, members);
			DEPBIN_MAP( nsEDDEngine::cycle_time				, depbin->db_cycle_time			, cycle_time);
			DEPBIN_MAP( nsEDDEngine::height					, depbin->db_height				, height);
			DEPBIN_MAP( nsEDDEngine::width					, depbin->db_width				, width);
			DEPBIN_MAP( nsEDDEngine::length					, depbin->db_length				, length);
			DEPBIN_MAP( nsEDDEngine::chart_type				, depbin->db_type				, type);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);

			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
		}
	};


	/*
	*	FILE Item
	*/

	class _INTERFACE FILE_DEPBIN {
	private:
		//AssignFileDepbin is used to assign all the attributes of FILE_DEPBIN
	void AssignFileDepbin(const FILE_DEPBIN &file_depbin);
	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_members;
		DEPBIN      *db_handling;

		DEPBIN      *db_identity;
		DEPBIN      *db_on_update_actions;
		DEPBIN		*db_shared;

		FILE_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_members(0), db_handling(0), 
			db_identity(0), db_on_update_actions(0), db_shared(0) {};
		virtual ~FILE_DEPBIN();
		//copy constructor
		FILE_DEPBIN(const FILE_DEPBIN &file_depbin);
		
		//Overloaded Assignment Operator 
		FILE_DEPBIN& operator= (const FILE_DEPBIN &file_depbin);
	};

	class _INTERFACE FLAT_FILE : public ItemBase
	{
		//AssignFlatFile is used to assign all the attributes of FLAT_FILE
		void AssignFlatFile(const FLAT_FILE &flat_file);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;				// HELP attribute value.		
		STRING          label;				// LABEL attribute value.
		MEMBER_LIST     members;			// MEMBERS attribute value.
		Handling		handling;			// HANDLING attribute value.

		STRING			identity;			// IDENTITY attribute value.
		ACTION_LIST		on_update_actions;	// ON_UPDATE_ACTIONS attribute value.
		Boolean			shared;				// SHARED attribute value.

		FILE_DEPBIN     *depbin;

		FLAT_FILE():ItemBase(ITYPE_FILE)
		{
			//member has its own constructor
			handling = (Handling)0;
			shared = False;
			depbin = new FILE_DEPBIN();
		}

		virtual ~FLAT_FILE();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_FILE(*this); };

		//copy constructor
		FLAT_FILE(const FLAT_FILE &flat_file);
		
		//Overloaded Assignment Operator 
		FLAT_FILE& operator= (const FLAT_FILE &flat_file);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::members				, depbin->db_members			, members);
			DEPBIN_MAP( nsEDDEngine::handling				, depbin->db_handling			, handling);
			DEPBIN_MAP( nsEDDEngine::identity				, depbin->db_identity			, identity);
			DEPBIN_MAP( nsEDDEngine::on_update_actions		, depbin->db_on_update_actions	, on_update_actions);
			DEPBIN_MAP( nsEDDEngine::shared					, depbin->db_shared				, shared);
		}
	};


	/*
	*	GRAPH Item
	*/

	class _INTERFACE GRAPH_DEPBIN {
	private:
	   //AssignGraphDepbin is used to assign all the attributes of GRAPH_DEPBIN
	   void AssignGraphDepbin(const GRAPH_DEPBIN &graph_depbin);
	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_members;
		DEPBIN      *db_cycle_time;
		DEPBIN      *db_height;
		DEPBIN      *db_width;
		DEPBIN      *db_x_axis;
		DEPBIN      *db_valid;

		DEPBIN      *db_visibility;

		GRAPH_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_members(0), db_cycle_time(0), db_height(0), 
			db_width(0), db_x_axis(0), db_valid(0), db_visibility(0) {};
		//copy constructor
		GRAPH_DEPBIN(const GRAPH_DEPBIN &graph_depbin);
		
		//Overloaded Assignment Operator 
		GRAPH_DEPBIN& operator= (const GRAPH_DEPBIN &graph_depbin);

		virtual ~GRAPH_DEPBIN();
	};

	class _INTERFACE FLAT_GRAPH : public ItemBase
	{
		//AssignFlatGraph is used to assign all the attributes of FLAT_GRAPH
		void AssignFlatGraph(const FLAT_GRAPH &flat_graph);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;		// HELP attribute value.
		STRING          label;		// LABEL attribute value.
		MEMBER_LIST     members;	// MEMBERS attribute value.
		DisplaySize     height;		// HEIGHT attribute value.
		DisplaySize     width;		// WIDTH attribute value.
		ITEM_ID         x_axis;		// X_AXIS attribute value.
		Boolean         valid;		// VALIDITY attribute value.
		ulong           cycle_time;	// CYCLE_TIME attribute value.

		Boolean			visibility;	// VISIBILITY attribute value.

		GRAPH_DEPBIN    *depbin;

		FLAT_GRAPH():ItemBase(ITYPE_GRAPH)
		{
			// members has its own constructor
			height		= DISPSIZE_MEDIUM; // The default height and width for Graphs is MEDIUM
			width		= DISPSIZE_MEDIUM;
			x_axis		= 0;	
			valid		= True;
			cycle_time	= 0;

			visibility	= True;

			depbin = new GRAPH_DEPBIN();
		}

		virtual ~FLAT_GRAPH();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_GRAPH(*this); };

		//copy constructor
		FLAT_GRAPH(const FLAT_GRAPH &flat_graph);
		
		//Overloaded Assignment Operator 
		FLAT_GRAPH& operator= (const FLAT_GRAPH &flat_graph);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP(nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::members			, depbin->db_members			, members);
			DEPBIN_MAP( nsEDDEngine::cycle_time			, depbin->db_cycle_time			, cycle_time);
			DEPBIN_MAP( nsEDDEngine::height				, depbin->db_height				, height);
			DEPBIN_MAP( nsEDDEngine::width				, depbin->db_width				, width);
			DEPBIN_MAP( nsEDDEngine::x_axis				, depbin->db_x_axis				, x_axis);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility);
		}
	};

	/*
	*	GRID Item
	*/

	class _INTERFACE GRID_DEPBIN {
	private:
		//AssignGridDepbin is used to assign all the attributes of FLAT_CHART
		void AssignGridDepbin(const GRID_DEPBIN &grid_depbin);

	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_vectors;
		DEPBIN      *db_height;
		DEPBIN      *db_width;
		DEPBIN      *db_handling;
		DEPBIN      *db_valid;
		DEPBIN      *db_orientation;

		DEPBIN      *db_visibility;

		GRID_DEPBIN(): db_item_information(0), db_help(0), db_label(0), db_vectors(0), db_height(0), db_width(0), 
			db_handling(0), db_valid(0), db_orientation(0), db_visibility(0) {};
		//copy constructor
		GRID_DEPBIN(const GRID_DEPBIN &Grid_depbin);
		
		//Overloaded Assignment Operator 
		GRID_DEPBIN& operator= (const GRID_DEPBIN &grid_depbin);

		virtual ~GRID_DEPBIN();
	};

	class _INTERFACE FLAT_GRID : public ItemBase
	{
		//AssignFlatGrid is used to assign all the attributes of FLAT_GRID
		void AssignFlatGrid(const FLAT_GRID &flat_grid);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;		// HELP attribute value.
		STRING          label;		// LABEL attribute value.
		VECTOR_LIST     vectors;	// VECTORS attribute value.
		DisplaySize     height;		// HEIGHT attribute value.
		DisplaySize     width;		// WIDTH attribute value.
		Handling        handling;	// HANDLING attribute value.
		Boolean         valid;		// VALIDITY attribute value.
		Orientation     orientation; // ORIENTATION attribute value.

		Boolean			visibility;	// VISIBILITY attribute value.	

		GRID_DEPBIN     *depbin;

		FLAT_GRID():ItemBase(ITYPE_GRID)
		{
			// vectors has its own constructor
			height		= DISPSIZE_MEDIUM; // The default height and width for Grid is MEDIUM
			width		= DISPSIZE_MEDIUM;
			handling	= (Handling)0;
			valid		= True;
			orientation = (Orientation)0;

			visibility	= True;

			depbin = new GRID_DEPBIN();
		}

		virtual ~FLAT_GRID();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_GRID(*this); };

		//copy constructor
		FLAT_GRID(const FLAT_GRID &flat_grid);
		
		//Overloaded Assignment Operator 
		FLAT_GRID& operator= (const FLAT_GRID &flat_grid);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::vectors				, depbin->db_vectors			, vectors);
			DEPBIN_MAP( nsEDDEngine::height					, depbin->db_height				, height);
			DEPBIN_MAP( nsEDDEngine::width					, depbin->db_width				, width);
			DEPBIN_MAP( nsEDDEngine::handling				, depbin->db_handling			, handling);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::orientation			, depbin->db_orientation		, orientation);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
		}
	};

	/*
	* LIST item
	*/

	class _INTERFACE LIST_DEPBIN {
	private:
		//AssignListDepbin is used to assign all the attributes of LIST_DEPBIN
		void AssignListDepbin(const LIST_DEPBIN &list_depbin);
	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_valid;
		DEPBIN      *db_type;
		DEPBIN      *db_capacity;
		DEPBIN      *db_count;

		DEPBIN      *db_private_attr;
		DEPBIN      *db_visibility;

		LIST_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_valid(0), db_type(0), db_capacity(0), db_count(0), db_private_attr(0), db_visibility(0) {};
		//copy constructor
		LIST_DEPBIN(const LIST_DEPBIN &list_depbin);
		
		//Overloaded Assignment Operator 
		LIST_DEPBIN& operator= (const LIST_DEPBIN &list_depbin);
		virtual ~LIST_DEPBIN();
	};

	class _INTERFACE FLAT_LIST : public ItemBase
	{
		//AssignFlatList is used to assign all the attributes of FLAT_LIST
		void AssignFlatList(const FLAT_LIST &flat_list);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;		// HELP attribute value.
		STRING          label;		// LABEL attribute value.
		Boolean         valid;		// VALIDITY attribute value.
		ITEM_ID         type;		// TYPE attribute value.
		ulong           capacity;	// CAPACITY attribute value.
		ulong           count;		// COUNT attribute value.
		OP_REF			count_ref;	// COUNT attribute variable reference.

		Boolean			private_attr;	// PRIVATE attribute value.	
		Boolean			visibility;		// VISIBILITY attribute value.

		LIST_DEPBIN     *depbin;

		FLAT_LIST():ItemBase(ITYPE_LIST)
		{
			valid		= True;
			type		= 0;
			capacity	= 0;
			count		= 0;

			private_attr = False;
			visibility	 = True;
			depbin = new LIST_DEPBIN();
		}

		virtual ~FLAT_LIST();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_LIST(*this); };

		//copy constructor
		FLAT_LIST(const FLAT_LIST &flat_list);
		
		//Overloaded Assignment Operator 
		FLAT_LIST& operator= (const FLAT_LIST &flat_list);
		
		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	,item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::type_definition		, depbin->db_type				, type);
			DEPBIN_MAP( nsEDDEngine::capacity				, depbin->db_capacity			, capacity);
			DEPBIN_MAP( nsEDDEngine::count					, depbin->db_count				, count);
			DEPBIN_MAP( nsEDDEngine::private_attr			, depbin->db_private_attr		, private_attr);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
			DEPBIN_MAP( nsEDDEngine::count_ref				, depbin->db_count				, count_ref);
		}
	};


	/*
	* SOURCE item
	*/

	class _INTERFACE SOURCE_DEPBIN {
	private:
		//AssignSourceDepbin is used to assign all the attributes of SOURCE_DEPBIN
		void AssignSourceDepbin(const SOURCE_DEPBIN &source_depbin);

	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;
		DEPBIN      *db_label;
		DEPBIN      *db_members;
		DEPBIN      *db_emphasis;
		DEPBIN      *db_y_axis;
		DEPBIN      *db_line_type;
		DEPBIN      *db_line_color;
		DEPBIN      *db_init_actions;
		DEPBIN      *db_refresh_actions;
		DEPBIN      *db_exit_actions;
		DEPBIN      *db_valid;

		DEPBIN		*db_visibility;

		SOURCE_DEPBIN() :db_item_information(0), db_help(0), db_label(0), db_members(0), db_emphasis(0), db_y_axis(0), db_line_type(0), 
			db_line_color(0), db_init_actions(0), db_refresh_actions(0), db_exit_actions(0), db_valid(0), db_visibility(0) {};
		//copy constructor
		SOURCE_DEPBIN(const SOURCE_DEPBIN &source_depbin);
		
		//Overloaded Assignment Operator 
		SOURCE_DEPBIN& operator= (const SOURCE_DEPBIN &source_depbin);
		virtual ~SOURCE_DEPBIN();
	};

	class _INTERFACE FLAT_SOURCE : public ItemBase
	{
		//AssignFlatSource is used to assign all the attributes of FLAT_SOURCE
		void AssignFlatSource(const FLAT_SOURCE &flat_source);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.
		OP_MEMBER_LIST  op_members;		// MEMBERS attribute value.
		Boolean         emphasis;		// EMPHASIS attribute value.
		ITEM_ID         y_axis;			// Y_AXIS attribute value.			
		LineType        line_type;		// LINE_TYPE attribute value.		
		ulong           line_color;		// LINE_COLOR attribute value.
		ACTION_LIST	    init_actions;	// INIT_ACTIONS attribute value.
		ACTION_LIST	    refresh_actions; // REFRESH_ACTIONS attribute value.
		ACTION_LIST	    exit_actions;	//	EXIT ACTIONS attribute value.
		Boolean         valid;			// VALIDITY attribute value.

		Boolean			visibility;		// VISIBILITY attribute value.

		SOURCE_DEPBIN   *depbin;

		FLAT_SOURCE():ItemBase(ITYPE_SOURCE)
		{
			//members has its own constructor
			emphasis = False;
			y_axis = 0;
			line_type = FDI_DATA0_LINETYPE;
			line_color = LINE_COLOR_DEFAULT;
			// init_actions, refresh_actions, exit_actions
			valid = True;

			visibility = True;

			depbin = new SOURCE_DEPBIN();

		}

		virtual ~FLAT_SOURCE();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_SOURCE(*this); };

		//copy constructor
		FLAT_SOURCE(const FLAT_SOURCE &flat_source);
		
		//Overloaded Assignment Operator 
		FLAT_SOURCE& operator= (const FLAT_SOURCE &flat_source);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::members				, depbin->db_members			, op_members);
			DEPBIN_MAP( nsEDDEngine::emphasis				, depbin->db_emphasis			, emphasis);
			DEPBIN_MAP( nsEDDEngine::y_axis					, depbin->db_y_axis				, y_axis);
			DEPBIN_MAP( nsEDDEngine::line_type				, depbin->db_line_type			, line_type);
			DEPBIN_MAP( nsEDDEngine::line_color				, depbin->db_line_color			, line_color);
			DEPBIN_MAP( nsEDDEngine::init_actions			, depbin->db_init_actions		, init_actions);
			DEPBIN_MAP( nsEDDEngine::refresh_actions		, depbin->db_refresh_actions	, refresh_actions);
			DEPBIN_MAP( nsEDDEngine::exit_actions			, depbin->db_exit_actions		, exit_actions);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
		}
	};

	/*
	* WAVEFORM item
	*/

	class _INTERFACE WAVEFORM_DEPBIN {
	private:
		//AssignWaveformDepbin is used to assign all the attributes of  WAVEFORM_DEPBIN
		void AssignWaveformDepbin(const WAVEFORM_DEPBIN &waveform_depbin);
	public:
		DEPBIN      *db_item_information;

		DEPBIN      *db_help;			// HELP attribute value.
		DEPBIN      *db_label;			// LABEL attribute value.
		DEPBIN      *db_valid;			// VALIDITY attribute value.
		DEPBIN      *db_emphasis;
		DEPBIN      *db_line_type;		// LINE_TYPE attribute value.
		DEPBIN      *db_handling;		// HANDLING attribute value.
		DEPBIN      *db_init_actions;
		DEPBIN      *db_refresh_actions;
		DEPBIN      *db_exit_actions;
		DEPBIN      *db_type;			
		DEPBIN      *db_x_initial;
		DEPBIN      *db_x_increment;
		DEPBIN      *db_number_of_points;
		DEPBIN      *db_x_values;
		DEPBIN      *db_y_values;
		DEPBIN      *db_key_x_values;
		DEPBIN      *db_key_y_values;
		DEPBIN      *db_line_color;
		DEPBIN      *db_y_axis;
		DEPBIN		*db_waveform_type;

		DEPBIN		*db_visibility; 

		WAVEFORM_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_valid(0), db_emphasis(0), db_line_type(0),
			db_handling(0), db_init_actions(0), db_refresh_actions(0), db_exit_actions(0), db_type(0), 
			db_x_initial(0), db_x_increment(0), db_number_of_points(0), db_x_values(0), db_y_values(0), 
			db_key_x_values(0), db_key_y_values(0), db_line_color(0), db_y_axis(0), db_visibility(0), db_waveform_type(0) {};
		//copy constructor
		WAVEFORM_DEPBIN(const WAVEFORM_DEPBIN &waveform_depbin);
		
		//Overloaded Assignment Operator 
		WAVEFORM_DEPBIN& operator= (const WAVEFORM_DEPBIN &waveform_depbin);

		virtual ~WAVEFORM_DEPBIN();
	};

	class _INTERFACE FLAT_WAVEFORM : public ItemBase
	{
		//AssignFlatWaveform is used to assign all the attributes of FLAT_WAVEFORM
		void AssignFlatWaveform(const FLAT_WAVEFORM &flat_waveform);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;				// HELP attribute value.
		STRING          label;				// LABEL attribute value.
		Boolean         valid;				// VALIDITY attribute value.
		Boolean         emphasis;
		LineType        line_type;			// LINE_TYPE attribute value.
		Handling        handling;			// HANDLING attribute value.
		ACTION_LIST	    init_actions;		// INIT_ACTIONS attribute value.
		ACTION_LIST	    refresh_actions;	// REFRESH_ACTIONS attribute value.
		ACTION_LIST	    exit_actions;		// EXIT ACTIONS attribute value.
		EXPR            x_initial;			// X_INITIAL attribute value.
		EXPR            x_increment;		// X_INCREMENT attribute value.
		ulong           number_of_points;	// NUMBER_OF_POINTS attribute value.
		DATA_ITEM_LIST  x_values;			// X_VALUES attribute value.
		DATA_ITEM_LIST  y_values;			// Y_VALUES attribute value.
		DATA_ITEM_LIST  key_x_values;		// KEY_POINTS attribute value.
		DATA_ITEM_LIST  key_y_values;		// KEY_POINTS attribute value.
		ulong           line_color;			// LINE_COLOR attribute value.
		ITEM_ID			y_axis;				// Y_AXIS attribute value.
		WaveformType	waveform_type;		// TYPE attribute value.

		Boolean			visibility;			// VISIBILITY attribute value.


		WAVEFORM_DEPBIN     *depbin;

        char _dummy[3]{0};

		FLAT_WAVEFORM():ItemBase(ITYPE_WAVEFORM)
		{
			valid = True;
			emphasis = False;
			line_type = FDI_DATA0_LINETYPE;
			handling = (Handling)0;
			waveform_type = (WaveformType)0;
			// x_initial has its own contructor
			// x_increment has its own contructor
			number_of_points = 0;
			line_color = LINE_COLOR_DEFAULT;
			y_axis = 0;

			visibility	= True;
			depbin = new WAVEFORM_DEPBIN();

		}

		virtual ~FLAT_WAVEFORM();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_WAVEFORM(*this); };

		//copy constructor
		FLAT_WAVEFORM(const FLAT_WAVEFORM &flat_waveform);
		
		//Overloaded Assignment Operator 
		FLAT_WAVEFORM& operator= (const FLAT_WAVEFORM &flat_waveform);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
		DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
		DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
		DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
		DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
		DEPBIN_MAP( nsEDDEngine::emphasis				, depbin->db_emphasis			, emphasis);
		DEPBIN_MAP( nsEDDEngine::line_type				, depbin->db_line_type			, line_type);
		DEPBIN_MAP( nsEDDEngine::handling				, depbin->db_handling			, handling);
		DEPBIN_MAP( nsEDDEngine::init_actions			, depbin->db_init_actions		, init_actions);
		DEPBIN_MAP( nsEDDEngine::refresh_actions		, depbin->db_refresh_actions	, refresh_actions);
		DEPBIN_MAP( nsEDDEngine::exit_actions			, depbin->db_exit_actions		, exit_actions);
//		DEPBIN_MAP( nsEDDEngine::type_definition,	depbin->db_type,				type);
		DEPBIN_MAP( nsEDDEngine::x_initial				, depbin->db_x_initial			, x_initial);
		DEPBIN_MAP( nsEDDEngine::x_increment			, depbin->db_x_increment		, x_increment);
		DEPBIN_MAP( nsEDDEngine::number_of_points		, depbin->db_number_of_points	, number_of_points);
		DEPBIN_MAP( nsEDDEngine::x_values				, depbin->db_x_values			, x_values);
		DEPBIN_MAP( nsEDDEngine::y_values				, depbin->db_y_values			, y_values);
		DEPBIN_MAP( nsEDDEngine::keypoints_x_values		, depbin->db_key_x_values		, key_x_values);
		DEPBIN_MAP( nsEDDEngine::keypoints_y_values		, depbin->db_key_y_values		, key_y_values);
		DEPBIN_MAP( nsEDDEngine::line_color				, depbin->db_line_color			, line_color);
		DEPBIN_MAP( nsEDDEngine::y_axis					, depbin->db_y_axis				, y_axis);
		DEPBIN_MAP( nsEDDEngine::waveform_type			, depbin->db_waveform_type		, waveform_type);
		DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility); 
		}
	};

	/*
	* IMAGE item
	*/

	class _INTERFACE IMAGE_DEPBIN {
	private:
		//AssignImageDepbin is used to assign all the attributes of IMAGE_DEPBIN
		void AssignImageDepbin(const IMAGE_DEPBIN &image_depbin);

	public:
		DEPBIN			*db_item_information;

		DEPBIN          *db_help;
		DEPBIN          *db_label;
		DEPBIN          *db_path;
		DEPBIN          *db_link;
		DEPBIN          *db_valid;

		DEPBIN          *db_image_table_index;
		DEPBIN          *db_visibility;

		IMAGE_DEPBIN() : db_item_information(0), db_help(0), db_label(0), db_path(0), db_link(0), db_valid(0), db_image_table_index(0), db_visibility(0) {};

		//copy constructor
		IMAGE_DEPBIN(const IMAGE_DEPBIN &image_depbin);
		
		//Overloaded Assignment Operator 
		IMAGE_DEPBIN& operator= (const IMAGE_DEPBIN &image_depbin);

		virtual ~IMAGE_DEPBIN();
	};

	class _INTERFACE FLAT_IMAGE : public ItemBase
	{
		//AssignFlatImage is used to assign all the attributes of FLAT_IMAGE
		void AssignFlatImage(const FLAT_IMAGE &flat_image);
	public:
		ITEM_INFORMATION item_info;

		STRING          label;				// LABEL attribute value.
		STRING          help;				// HELP attribute value.
		DESC_REF		link;				// LINK attribute value.
		Boolean         valid;				// VALIDITY attribute value.
		BINARY_IMAGE	entry;

		ulong			image_table_index;
		STRING			image_id;	
		Boolean			visibility;			// VISIBILITY attribute value.

		IMAGE_DEPBIN    *depbin;

		FLAT_IMAGE():ItemBase(ITYPE_IMAGE)
		{
			//link has its own contructor
			valid = True;
			//entry has its own constructor

			image_table_index = 0;
			visibility		  = True;	


			depbin = new IMAGE_DEPBIN();
		};

		virtual ~FLAT_IMAGE();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_IMAGE(*this); };

		//copy constructor
		FLAT_IMAGE(const FLAT_IMAGE &flat_image);
		
		//Overloaded Assignment Operator 
		FLAT_IMAGE& operator= (const FLAT_IMAGE &flat_image);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP(nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);

			DEPBIN_MAP( nsEDDEngine::link				, depbin->db_link				, link);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);

			DEPBIN_MAP( nsEDDEngine::image_table_index	, depbin->db_image_table_index	, image_table_index);
			DEPBIN_MAP( nsEDDEngine::image_id			, depbin->db_image_table_index	, image_id);
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility);
		}

	};

	/*
	* MENU item
	*/

	class _INTERFACE MENU_DEPBIN {
	private:
		//AssignMenuDepbin is used to assign all the attributes of MENU_DEPBIN
		void AssignMenuDepbin(const MENU_DEPBIN &menu_depbin);

	public:

		DEPBIN         *db_item_information;

		DEPBIN         *db_label;
		DEPBIN         *db_help;
		DEPBIN         *db_items;
		DEPBIN         *db_valid;
		DEPBIN         *db_style;
		DEPBIN         *db_access;

		DEPBIN         *db_pre_edit_act;
		DEPBIN         *db_post_edit_act;
		DEPBIN         *db_pre_read_act;
		DEPBIN         *db_post_read_act;
		DEPBIN         *db_pre_write_act;
		DEPBIN         *db_post_write_act;

		DEPBIN         *db_exit_actions;
		DEPBIN         *db_init_actions;

		DEPBIN		   *db_visibility;

		MENU_DEPBIN() : db_item_information(0), db_label(0), db_help(0),  db_items(0), db_valid(0), db_style(0),
			db_access(0), db_pre_edit_act(0),  db_post_edit_act(0), db_pre_read_act(0), 
			db_post_read_act(0), db_pre_write_act(0), db_post_write_act(0), db_visibility(0),
			db_exit_actions(0), db_init_actions(0)
		{};

		//copy constructor
		MENU_DEPBIN(const MENU_DEPBIN &menu_depbin);
		
		//Overloaded Assignment Operator 
		MENU_DEPBIN& operator= (const MENU_DEPBIN &menu_depbin);

		virtual ~MENU_DEPBIN();

	};


	class _INTERFACE FLAT_MENU : public ItemBase
	{
		//AssignFlatMenu is used to assign all the attributes of FLAT_MENU
		void AssignFlatMenu(const FLAT_MENU &flat_menu);
	public:
		ITEM_INFORMATION item_info;

		STRING          label;			// LABEL attribute value.
		STRING          help;			// HELP attribute value.
		Boolean			valid;			// VALIDITY attribute value.
		MenuStyle		style;			// STYLE attribute value.
		MENU_ITEM_LIST  items;			// ITEMS attribute value.
		Access			access;			// ACCESS attribute value.
		ACTION_LIST	    pre_edit_act;	// PRE_EDIT_ACTIONS attribute value.
		ACTION_LIST	    post_edit_act;	// POST_EDIT_ACTIONS attribute value.
		ACTION_LIST	    pre_read_act;	// PRE_READ_ACTIONS attribute value.
		ACTION_LIST	    post_read_act;	// POST_READ_ACTIONS attribute value.
		ACTION_LIST	    pre_write_act;	// PRE_WRITE_ACTIONS attribute value.
		ACTION_LIST	    post_write_act;	// POST_WRITE_ACTIONS attribute value.

		ACTION_LIST	    exit_actions;	// EXIT_ACTIONS attribute value.
		ACTION_LIST	    init_actions;	// INIT_ACTIONS attribute value.

		Boolean			visibility;		// VISIBILITY attribute value.

		MENU_DEPBIN    *depbin;

		FLAT_MENU():ItemBase(ITYPE_MENU)
		{
			valid = True;
			style = FDI_NO_STYLE_TYPE;
			//items has its own constructor
			access = UNKNOWN_ACCESS;

			visibility = True;

			depbin = new MENU_DEPBIN();
		}

		virtual ~FLAT_MENU();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_MENU(*this); };

		//copy constructor
		FLAT_MENU(const FLAT_MENU &flat_menu);
		
		//Overloaded Assignment Operator 
		FLAT_MENU& operator= (const FLAT_MENU &flat_menu);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::items				, depbin->db_items				, items);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::style				, depbin->db_style				, style);
			DEPBIN_MAP( nsEDDEngine::access				, depbin->db_access				, access);
			DEPBIN_MAP( nsEDDEngine::pre_edit_actions	, depbin->db_pre_edit_act		, pre_edit_act);
			DEPBIN_MAP( nsEDDEngine::post_edit_actions	, depbin->db_post_edit_act		, post_edit_act);
			DEPBIN_MAP( nsEDDEngine::pre_read_actions	, depbin->db_pre_read_act		, pre_read_act);
			DEPBIN_MAP( nsEDDEngine::post_read_actions	, depbin->db_post_read_act		, post_read_act);
			DEPBIN_MAP( nsEDDEngine::pre_write_actions	, depbin->db_pre_write_act		, pre_write_act);
			DEPBIN_MAP( nsEDDEngine::post_write_actions	, depbin->db_post_write_act		, post_write_act);
			DEPBIN_MAP( nsEDDEngine::visibility			,depbin->db_visibility,			visibility);
			DEPBIN_MAP( nsEDDEngine::exit_actions		,depbin->db_exit_actions,		exit_actions);
			DEPBIN_MAP( nsEDDEngine::init_actions		,depbin->db_init_actions,		init_actions);
		}

	};


	/*
	* EDIT_DISPLAY item
	*/

	class _INTERFACE EDIT_DISPLAY_DEPBIN {
	private:
		//EditDisplaydepbin is used to assign all the attributes of EDIT_DISPLAY_DEPBIN
		void AssignEditDisplaydepbin(const EDIT_DISPLAY_DEPBIN &edit_display_depbin);
	public:
		DEPBIN         *db_item_information;

		DEPBIN         *db_disp_items;
		DEPBIN         *db_edit_items;
		DEPBIN         *db_label;
		DEPBIN         *db_help;
		DEPBIN         *db_valid;
		DEPBIN         *db_pre_edit_act;
		DEPBIN         *db_post_edit_act;

		DEPBIN		   *db_visibility;


		EDIT_DISPLAY_DEPBIN() : db_item_information(0), db_disp_items(0), db_edit_items(0),  db_label(0), db_help(0), db_valid(0),
			db_pre_edit_act(0), db_post_edit_act(0), db_visibility(0) {};
		//copy constructor
		EDIT_DISPLAY_DEPBIN(const EDIT_DISPLAY_DEPBIN &edit_display_depbin);
		
		//Overloaded Assignment Operator 
		EDIT_DISPLAY_DEPBIN& operator= (const EDIT_DISPLAY_DEPBIN &edit_display_depbin);

		virtual ~EDIT_DISPLAY_DEPBIN();
	};

	class _INTERFACE FLAT_EDIT_DISPLAY : public ItemBase
	{
		//AssignFlatEditDisplay is used to assign all the attributes of FLAT_EDIT_DISPLAY
		void AssignFlatEditDisplay(const FLAT_EDIT_DISPLAY &flat_edit_display);
	public:
		ITEM_INFORMATION item_info;

		OP_REF_TRAIL_LIST disp_items;	// DISPLAY_ITEMS attribute value.
		OP_REF_TRAIL_LIST edit_items;	// EDIT_ITEMS attribute value.
		STRING          label;			// LABEL attribute value.
		STRING			help;			// HELP attribute value.
		Boolean			valid;			// VALIDITY attribute value.
		ACTION_LIST		pre_edit_act;	// PRE_EDIT_ACTIONS attribute value.
		ACTION_LIST		post_edit_act;	// POST_EDIT_ACTIONS attribute value.

		Boolean			visibility;		// VISIBILITY attribute value.

		EDIT_DISPLAY_DEPBIN *depbin;

		FLAT_EDIT_DISPLAY():ItemBase(ITYPE_EDIT_DISP)
		{
			valid = True;

			visibility = True;

			depbin = new EDIT_DISPLAY_DEPBIN();
		};

		virtual ~FLAT_EDIT_DISPLAY();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_EDIT_DISPLAY(*this); };

		//copy constructor
		FLAT_EDIT_DISPLAY(const FLAT_EDIT_DISPLAY &flat_edit_display);
		
		//Overloaded Assignment Operator 
		FLAT_EDIT_DISPLAY& operator= (const FLAT_EDIT_DISPLAY &flat_edit_display);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::display_items		, depbin->db_disp_items			, disp_items);
			DEPBIN_MAP( nsEDDEngine::edit_items			, depbin->db_edit_items			, edit_items);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::pre_edit_actions	, depbin->db_pre_edit_act		, pre_edit_act);
			DEPBIN_MAP( nsEDDEngine::post_edit_actions	, depbin->db_post_edit_act		, post_edit_act);
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility);
		}
	};


	/*
	* METHOD item
	*/

	class _INTERFACE METHOD_DEPBIN {
	private:
		//AssignMethodDepbin is used to assign all the attributes of  METHOD_DEPBIN
		void AssignMethodDepbin(const METHOD_DEPBIN &method_depbin);
	public:
			
		DEPBIN         *db_item_information;

		DEPBIN         *db_class;
		DEPBIN         *db_def;
		DEPBIN         *db_help;
		DEPBIN         *db_label;
		DEPBIN         *db_valid;
		DEPBIN         *db_type;
		DEPBIN         *db_params;
		DEPBIN         *db_access;

		DEPBIN         *db_private_attr;
		DEPBIN         *db_visibility;

		METHOD_DEPBIN() : db_item_information(0), db_class(0), db_def(0), db_help(0), db_label(0), db_valid(0), db_type(0),
			db_params(0), db_access(0), db_private_attr(0), db_visibility(0) {};

		//copy constructor
		METHOD_DEPBIN(const METHOD_DEPBIN &method_depbin);
		
		//Overloaded Assignment Operator 
		METHOD_DEPBIN& operator= (const METHOD_DEPBIN &method_depbin);

		virtual ~METHOD_DEPBIN();
	};

	class _INTERFACE FLAT_METHOD : public ItemBase
	{
		//AssignFlatMethod is used to assign all the attributes of FLAT_METHOD
		void AssignFlatMethod(const FLAT_METHOD &flat_method);
	public:
		ITEM_INFORMATION item_info;

		ClassType       class_attr;		// CLASS attribute value. 
		DEFINITION      def;			// DEFINITION attribute value.
		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value. 
		Boolean         valid;			// VALIDITY attribute value.

		MethodType		type;			// TYPE attribute value.
		STRING			params;
		Access			access;			// ACCESS attribute value.

		Boolean			private_attr;	// PRIVATE attribute value.
		Boolean			visibility;		// VISIBILITY attribute value.

		METHOD_DEPBIN  *depbin;

		FLAT_METHOD():ItemBase(ITYPE_METHOD)
		{
			class_attr = CT_NONE;
			// def has its own constructor
			valid	= True;
			type	= MT_VOID;
			access = ONLINE_ACCESS;

			private_attr = False;
			visibility	 = True;

			depbin = new METHOD_DEPBIN();
		}

		virtual ~FLAT_METHOD();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_METHOD(*this); };

		//copy constructor
		FLAT_METHOD(const FLAT_METHOD &flat_method);
		
		//Overloaded Assignment Operator 
		FLAT_METHOD& operator= (const FLAT_METHOD &flat_method);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::class_attr				, depbin->db_class				, class_attr);
			DEPBIN_MAP( nsEDDEngine::definition				, depbin->db_def				, def);
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::method_type			, depbin->db_type				, type);
			DEPBIN_MAP( nsEDDEngine::method_parameters		, depbin->db_params				, params);
			DEPBIN_MAP( nsEDDEngine::access					, depbin->db_access				, access);
			DEPBIN_MAP( nsEDDEngine::private_attr			, depbin->db_private_attr		, private_attr);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
		}
	};

	/*
	* REFRESH item
	*/

	class _INTERFACE REFRESH_DEPBIN {
	private:
		//AssignRefreshDepbin is used to assign all the attributes of REFRESH_DEPBIN
		void AssignRefreshDepbin(const REFRESH_DEPBIN &refresh_depbin);
	public:

		DEPBIN         *db_item_information;

		DEPBIN         *db_depend_items;
		DEPBIN         *db_update_items;

		REFRESH_DEPBIN() : db_item_information(0), db_depend_items(0), db_update_items(0) {};
		//copy constructor
		REFRESH_DEPBIN(const REFRESH_DEPBIN &refresh_depbin);
		
		//Overloaded Assignment Operator 
		REFRESH_DEPBIN& operator= (const REFRESH_DEPBIN &refresh_depbin);

		virtual ~REFRESH_DEPBIN();
	};

	class _INTERFACE FLAT_REFRESH : public ItemBase
	{
		//AssignFlatRefresh is used to assign all the attributes of FLAT_REFRESH
		void AssignFlatRefresh(const FLAT_REFRESH &flat_refresh);
	public:
		ITEM_INFORMATION item_info;

		OP_REF_TRAIL_LIST depend_items;	// cause-parameter attribute value.
		OP_REF_TRAIL_LIST update_items; // effected-parameter attribute value.
		
		REFRESH_DEPBIN *depbin;

		FLAT_REFRESH():ItemBase(ITYPE_REFRESH)
		{
			depbin = new REFRESH_DEPBIN();
		}

		virtual ~FLAT_REFRESH();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_REFRESH(*this); };

		//copy constructor
		FLAT_REFRESH(const FLAT_REFRESH &flat_refresh);
		
		//Overloaded Assignment Operator 
		FLAT_REFRESH& operator= (const FLAT_REFRESH &flat_refresh);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP(nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::relation_watch_list	, depbin->db_depend_items		, depend_items);
			DEPBIN_MAP( nsEDDEngine::relation_update_list	, depbin->db_update_items		, update_items);
	
		}
	};


	/*
	* UNIT item
	*/

	class _INTERFACE UNIT_DEPBIN {
	private:
		//AssignUnitDepbin is used to assign all the attributes of UNIT_DEPBIN
		void AssignUnitDepbin(const UNIT_DEPBIN &unit_depbin);

	public:
		DEPBIN         *db_item_information;

		DEPBIN         *db_var;			
		DEPBIN         *db_var_units;	

		UNIT_DEPBIN() : db_item_information(0), db_var(0), db_var_units(0) {};

		//copy constructor
		UNIT_DEPBIN(const UNIT_DEPBIN &unit_depbin);
		
		//Overloaded Assignment Operator 
		UNIT_DEPBIN& operator= (const UNIT_DEPBIN &unit_depbin);
		virtual ~UNIT_DEPBIN();

	};

	class _INTERFACE FLAT_UNIT : public ItemBase
	{
		//AssignFlatUnit is used to assign all the attributes of FLAT_UNIT
		void AssignFlatUnit(const FLAT_UNIT &flat_unit);
	public:
		ITEM_INFORMATION item_info;

		OP_REF_TRAIL		var;		// cause-parameter attribute value.
		OP_REF_TRAIL_LIST	var_units;	// effected-parameter attribute value.

		UNIT_DEPBIN    *depbin;

		FLAT_UNIT():ItemBase(ITYPE_UNIT)
		{
			depbin = new UNIT_DEPBIN();
		}

		virtual ~FLAT_UNIT();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_UNIT(*this); };

		//copy constructor
		FLAT_UNIT(const FLAT_UNIT &flat_unit);
		
		//Overloaded Assignment Operator 
		FLAT_UNIT& operator= (const FLAT_UNIT &flat_unit);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information			, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::relation_watch_variable	, depbin->db_var				, var);
			DEPBIN_MAP( nsEDDEngine::relation_update_list		, depbin->db_var_units			, var_units);
	
		}
	};


	/*
	* WRITE AS ONE item
	*/

	class _INTERFACE WAO_DEPBIN {
	private:
		//AssignWaoDepbin is used to assign all the attributes of WAO_DEPBIN
		void AssignWaoDepbin(const WAO_DEPBIN &wao_depbin);
	public:
		DEPBIN         *db_item_information;
	
		DEPBIN         *db_items;
		WAO_DEPBIN() : db_item_information(0), db_items(0) {};
		//copy constructor
		WAO_DEPBIN(const WAO_DEPBIN &wao_depbin);
		
		//Overloaded Assignment Operator 
		WAO_DEPBIN& operator= (const WAO_DEPBIN &wao_depbin);
		virtual ~WAO_DEPBIN();
	};

	class _INTERFACE FLAT_WAO : public ItemBase
	{
		//AssignFlatWao is used to assign all the attributes of FLAT_WAO
		void AssignFlatWao(const FLAT_WAO &flat_wao);
	public:
		ITEM_INFORMATION item_info;

		OP_REF_TRAIL_LIST items;

		WAO_DEPBIN     *depbin;

		FLAT_WAO():ItemBase(ITYPE_WAO)
		{
			depbin = new WAO_DEPBIN();
		}

		virtual ~FLAT_WAO();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_WAO(*this); };

		//copy constructor
		FLAT_WAO(const FLAT_WAO &flat_wao);
		
		//Overloaded Assignment Operator 
		FLAT_WAO& operator= (const FLAT_WAO &flat_wao);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::relation_update_list	, depbin->db_items				, items);
		}
	};

	/*
	* ITEM_ARRAY item
	*/

	class _INTERFACE ITEM_ARRAY_DEPBIN {
	private:
		//AssignItemArrayDepbin is used to assign all the attributes of ITEM_ARRAY_DEPBIN
		void AssignItemArrayDepbin(const ITEM_ARRAY_DEPBIN &flat_item_array);
	public:
		DEPBIN         *db_item_information;

		DEPBIN         *db_elements;		
		DEPBIN         *db_help;		
		DEPBIN         *db_label;		
		DEPBIN         *db_valid;		

		DEPBIN         *db_visibility;	
		DEPBIN		   *db_private_attr;

		ITEM_ARRAY_DEPBIN() : db_item_information(0), db_elements(0), db_help(0), db_label(0), db_valid(0), db_private_attr(0), db_visibility(0) {};

		//copy constructor
		ITEM_ARRAY_DEPBIN(const ITEM_ARRAY_DEPBIN &item_array_depbin);
		
		//Overloaded Assignment Operator 
		ITEM_ARRAY_DEPBIN& operator= (const ITEM_ARRAY_DEPBIN &item_array_depbin);


		virtual ~ITEM_ARRAY_DEPBIN();

	};

	class _INTERFACE FLAT_ITEM_ARRAY : public ItemBase
	{
		//AssignFlatItemArray is used to assign all the attributes of FLAT_ITEM_ARRAY
		void AssignFlatItemArray(const FLAT_ITEM_ARRAY &flat_item_array);
	public:
		ITEM_INFORMATION item_info;

		ITEM_TYPE		subtype;			// TYPE attribute value.

		ITEM_ARRAY_ELEMENT_LIST elements;	// ELEMENTS attribute value.
		STRING          help;				// HELP attribute value.
		STRING          label;				// LABEL attribute value.
		Boolean			valid;				// VALIDITY attribute value.

		Boolean			visibility;			// VISIBILITY attribute value.
		Boolean			private_attr;		// PRIVATE attribute value.

		ITEM_ARRAY_DEPBIN *depbin;

		FLAT_ITEM_ARRAY():ItemBase(ITYPE_ITEM_ARRAY)
		{
			subtype = ITYPE_NO_VALUE;
			// elements has its own constructor
			valid =	True;

			visibility = True;
			private_attr = False;

			depbin = new ITEM_ARRAY_DEPBIN();
		}

		virtual ~FLAT_ITEM_ARRAY();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_ITEM_ARRAY(*this); };

		//copy constructor
		FLAT_ITEM_ARRAY(const FLAT_ITEM_ARRAY &flat_item_array);
		
		//Overloaded Assignment Operator 
		FLAT_ITEM_ARRAY& operator= (const FLAT_ITEM_ARRAY &flat_item_array);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::elements				, depbin->db_elements			, elements);
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
			DEPBIN_MAP( nsEDDEngine::private_attr			, depbin->db_private_attr		, private_attr);
		}

	};


	/*
	* ARRAY item
	*/

	class _INTERFACE ARRAY_DEPBIN {
	private:
		//AssignArrayDepbin is used to assign all the attributes of ARRAY_DEPBIN
		void AssignArrayDepbin(const ARRAY_DEPBIN &array_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_num_of_elements;
		DEPBIN			*db_help;
		DEPBIN			*db_label;
		DEPBIN			*db_valid;
		DEPBIN			*db_type;
		DEPBIN			*db_resp_codes;

		DEPBIN			*db_private_attr;
		DEPBIN			*db_visibility;
		DEPBIN			*db_write_mode;

		ARRAY_DEPBIN() : db_item_information(0), db_num_of_elements(0), db_help(0), db_label(0), db_valid(0), db_type(0), 
			db_resp_codes(0), db_private_attr(0), db_visibility(0), db_write_mode(0)  {};
		//copy constructor
		ARRAY_DEPBIN(const ARRAY_DEPBIN &array_depbin);
		
		//Overloaded Assignment Operator 
		ARRAY_DEPBIN& operator= (const ARRAY_DEPBIN &array_depbin);
		virtual ~ARRAY_DEPBIN();
	};  



	class _INTERFACE FLAT_ARRAY : public ItemBase
	{
		//AssignFlatArray is used to assign all the attributes of FLAT_ARRAY
		void AssignFlatArray(const FLAT_ARRAY &flat_array);
	public:
		ITEM_INFORMATION item_info;

		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.
		Boolean			valid;			// VALIDITY attribute value.
		ITEM_ID         type;			// TYPE attribute value.
		ulong			num_of_elements; // NUMBER_OF_ELEMENTS attribute value.
		RESPONSE_CODES  resp_codes;		// RESPONSE_CODES attribute value.

		Boolean			private_attr;	// PRIVATE attribute value.
		Boolean			visibility;		// VISIBILITY attribute value.
		WriteMode		write_mode;		// WRITE_MODE attribute value.

		ARRAY_DEPBIN	*depbin;

		FLAT_ARRAY():ItemBase(ITYPE_ARRAY) 
		{
			num_of_elements	= 0;
			valid	= True;
			type	= 0;
			//write_mode =0;
			// resp_codes has its own constructor

			private_attr	= False;
			visibility		= True;
			write_mode = (WriteMode)0;

			depbin = new ARRAY_DEPBIN();
		};

		virtual ~FLAT_ARRAY();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_ARRAY(*this); };

		//copy constructor
		FLAT_ARRAY(const FLAT_ARRAY &flat_array);
		
		//Overloaded Assignment Operator 
		FLAT_ARRAY& operator= (const FLAT_ARRAY &flat_array);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::validity				, depbin->db_valid				,valid);
			DEPBIN_MAP( nsEDDEngine::type_definition		, depbin->db_type				, type);
			DEPBIN_MAP( nsEDDEngine::number_of_elements		, depbin->db_num_of_elements	, num_of_elements);
			DEPBIN_MAP( nsEDDEngine::response_codes			, depbin->db_resp_codes			, resp_codes);
			DEPBIN_MAP( nsEDDEngine::private_attr			, depbin->db_private_attr		, private_attr);
			DEPBIN_MAP( nsEDDEngine::visibility				, depbin->db_visibility			, visibility);
			DEPBIN_MAP( nsEDDEngine::write_mode				, depbin->db_write_mode			, write_mode);
		}

	};


	/*
	* COLLECTION item
	*/

	class _INTERFACE COLLECTION_DEPBIN
	{
	private:
		//AssignCollectionDepbin is used to assign all the attributes of COLLECTION_DEPBIN
		void AssignCollectionDepbin(const COLLECTION_DEPBIN &collection_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_members;
		DEPBIN			*db_help;
		DEPBIN			*db_label;
		DEPBIN			*db_valid;

		DEPBIN			*db_visibility;
		DEPBIN		    *db_private_attr;

		COLLECTION_DEPBIN() : db_item_information(0), db_members(0), db_help(0), db_label(0), db_valid(0), db_private_attr(0), db_visibility(0) {};
		//copy constructor
		COLLECTION_DEPBIN(const COLLECTION_DEPBIN &collection_depbin);
		
		//Overloaded Assignment Operator 
		COLLECTION_DEPBIN& operator= (const COLLECTION_DEPBIN &collection_depbin);
		virtual ~COLLECTION_DEPBIN();
	};




	class _INTERFACE FLAT_COLLECTION : public ItemBase
	{
		//AssignFlatCollection is used to assign all the attributes of FLAT_COLLECTION
		void AssignFlatCollection(const FLAT_COLLECTION &flat_collection);
	public:
		ITEM_INFORMATION item_info;

		ITEM_TYPE		subtype;		// TYPE attribute value.

		OP_MEMBER_LIST  op_members;		// MEMBERS attribute value.
		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.
		Boolean         valid;			// VALIDITY attribute value.

		Boolean			visibility;		// VISIBILITY attribute value.
		Boolean			private_attr;	// PRIVATE attribute value.

		COLLECTION_DEPBIN *depbin;

		FLAT_COLLECTION():ItemBase(ITYPE_COLLECTION)
		{
			subtype = ITYPE_NO_VALUE;
			// members has its own constructor
			valid = True;

			visibility = True;
			private_attr = False;

			depbin = new COLLECTION_DEPBIN();
		};

		virtual ~FLAT_COLLECTION();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COLLECTION(*this); };

		//copy constructor
		FLAT_COLLECTION(const FLAT_COLLECTION &flat_collection);
		
		//Overloaded Assignment Operator 
		FLAT_COLLECTION& operator= (const FLAT_COLLECTION &flat_collection);

		static AttributeNameSet const All_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility);	
			DEPBIN_MAP( nsEDDEngine::private_attr		, depbin->db_private_attr		, private_attr);				
			DEPBIN_MAP( nsEDDEngine::members			, depbin->db_members			, op_members);
		}
	};


	/*
	* RECORD item
	*/

	class _INTERFACE RECORD_DEPBIN 
	{
	private:
		//AssignRecordDepbin is used to assign all the attributes of RECORD_DEPBIN
		void AssignRecordDepbin(const RECORD_DEPBIN &record_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_members;
		DEPBIN			*db_help;
		DEPBIN			*db_label;
		DEPBIN			*db_valid;
		DEPBIN			*db_resp_codes;
		DEPBIN			*db_private_attr;
		DEPBIN			*db_visibility;
		DEPBIN			*db_write_mode;

		RECORD_DEPBIN() : db_item_information(0), db_members(0), db_help(0), db_label(0), db_valid(0), db_resp_codes(0), 
			db_private_attr(0), db_visibility(0), db_write_mode(0)  {};
		//copy constructor
		RECORD_DEPBIN(const RECORD_DEPBIN &record_depbin);
		
		//Overloaded Assignment Operator 
		RECORD_DEPBIN& operator= (const RECORD_DEPBIN &record_depbin);
		virtual ~RECORD_DEPBIN();
	};

	class _INTERFACE FLAT_RECORD : public ItemBase
	{
		//AssignFlatRecord is used to assign all the attributes of FLAT_RECORD
		void AssignFlatRecord(const FLAT_RECORD &flat_record);
	public:
		ITEM_INFORMATION item_info;

		MEMBER_LIST     members;		// MEMBERS attribute value.
		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.
		Boolean         valid;			// VALIDITY attribute value.
		RESPONSE_CODES	resp_codes;		// RESPONSE_CODES attribute value.
		WriteMode		write_mode;		// WRITE_MODE attribute value.

		Boolean			private_attr;	// PRIVATE attribute value.
		Boolean			visibility;		// VISIBILITY attribute value.

		RECORD_DEPBIN  *depbin;

		FLAT_RECORD():ItemBase(ITYPE_RECORD)
		{
			valid			= True;
			private_attr	= False;
			visibility		= True;
			write_mode = (nsEDDEngine::WriteMode)0;
			depbin = new RECORD_DEPBIN();
		}

		virtual ~FLAT_RECORD();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_RECORD(*this); };

		//copy constructor
		FLAT_RECORD(const FLAT_RECORD &flat_record);
		
		//Overloaded Assignment Operator 
		FLAT_RECORD& operator= (const FLAT_RECORD &flat_record);

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::members			, depbin->db_members			, members);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);				
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);				
			DEPBIN_MAP( nsEDDEngine::response_codes		, depbin->db_resp_codes			, resp_codes);		
			DEPBIN_MAP( nsEDDEngine::private_attr		, depbin->db_private_attr		, private_attr);	
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility );		
			DEPBIN_MAP(nsEDDEngine::write_mode			, depbin->db_write_mode			, write_mode );		
		}

		static AttributeNameSet const All_Attrs;
	};


	/*
	* BLOCK item
	*/

	class _INTERFACE BLOCK_DEPBIN {
	private:
		//AssignBlockDepbinis used to assign all the attributes of BLOCK_DEPBIN
		void AssignBlockDepbin(const BLOCK_DEPBIN &block_depbin);
	public:

		DEPBIN         *db_item_information;

		DEPBIN         *db_label;
		DEPBIN         *db_help;
		DEPBIN         *db_param;

		DEPBIN         *db_characteristic;

		DEPBIN         *db_item_array;
		DEPBIN         *db_collect;
		DEPBIN         *db_menu;
		DEPBIN         *db_edit_disp;

		DEPBIN         *db_method;
		DEPBIN         *db_unit;
		DEPBIN         *db_refresh;
		DEPBIN         *db_wao;

		DEPBIN         *db_chart;
		DEPBIN         *db_graph;
		DEPBIN         *db_grid;
		DEPBIN         *db_image;

		DEPBIN         *db_axis;
		DEPBIN         *db_source;
		DEPBIN         *db_waveform;
		DEPBIN         *db_file;
		DEPBIN         *db_list;

		DEPBIN         *db_charts;
		DEPBIN         *db_graphs;
		DEPBIN         *db_grids;
		DEPBIN         *db_lists;
		DEPBIN         *db_local_param;
		DEPBIN         *db_menus;
		DEPBIN         *db_methods;
		DEPBIN         *db_param_list;

		DEPBIN			*db_plugin_items;
		DEPBIN			*db_files;
		DEPBIN			*db_plugins;

		BLOCK_DEPBIN() : db_item_information(0), db_characteristic(0), db_help(0), db_label(0), db_param(0),
			db_item_array(0), db_collect(0), db_menu(0), db_edit_disp(0),db_method(0), 
			db_unit(0), db_refresh(0), db_wao(0), db_chart(0), db_graph(0), db_grid(0), 
			db_image(0), db_axis(0), db_source(0), db_waveform(0), db_file(0), db_list(0), 
			db_charts(0), db_graphs(0), db_grids(0), db_lists(0), db_local_param(0), db_menus(0),
			db_methods(0), db_param_list(0),
			db_plugin_items(0),db_files(0),db_plugins(0) {};
		//copy constructor
		BLOCK_DEPBIN(const BLOCK_DEPBIN &block_depbin);
		
		//Overloaded Assignment Operator 
		BLOCK_DEPBIN& operator= (const BLOCK_DEPBIN &block_depbin);
		virtual ~BLOCK_DEPBIN();
	};


	class _INTERFACE FLAT_BLOCK : public ItemBase
	{
		//AssignFlatBlock is used to assign all the attributes of FLAT_BLOCK
		void AssignFlatBlock(const FLAT_BLOCK &flat_block);
	public:
		ITEM_INFORMATION item_info;

		ITEM_ID         characteristic;	// CHARACTERISTICS attribute value.
		STRING          help;			// HELP attribute value.
		STRING          label;			// LABEL attribute value.
		MEMBER_LIST     param;			// PARAMETERS attribute value.

		ITEM_ID_LIST    item_array;	// REFERENCE_ARRAY_ITEMS attribute value.
		ITEM_ID_LIST    collect;	// COLLECTION_ITEMS attribute value.
		ITEM_ID_LIST    menu;		// MENU_ITEMS attribute value.
		ITEM_ID_LIST    edit_disp;	// EDIT_DISPLAY_ITEMS attribute value.

		ITEM_ID_LIST    method;		// METHOD_ITEMS attribute value.
		ITEM_ID_LIST    unit;		// UNIT_ITEMS attribute value.
		ITEM_ID_LIST    refresh;	// REFRESH_ITEMS attribute value.
		ITEM_ID_LIST    wao;		// WRITE_AS_ONE_ITEMS attribute value.
		ITEM_ID_LIST    plugin_items; // PLUGIN_ITEMS attribute value.

		ITEM_ID_LIST    chart;		// CHART_ITEMS attribute value.
		ITEM_ID_LIST    graph;		// GRAPH_ITEMS attribute value.
		ITEM_ID_LIST    grid;		// GRID_ITEMS attribute value.
		ITEM_ID_LIST    image;		// IMAGE_ITEMS attribute value.

		ITEM_ID_LIST    axis;		// AXIS_ITEMS attribute value.
		ITEM_ID_LIST    source;		// SOURCE_ITEMS attribute value.
		ITEM_ID_LIST    waveform;	// WAVEFORM_ITEMS attribute value.
		ITEM_ID_LIST    file;		// FILE_ITEMS attribute value.
		ITEM_ID_LIST    list;		// LIST_ITEMS attribute value.

		//////////////////////////////////////////
		MEMBER_LIST		charts;		// CHARTS attribute value.
		MEMBER_LIST		graphs;		// GRAPHS attribute value.
		MEMBER_LIST		grids;		// GRIDS attribute value.
		MEMBER_LIST		lists;		// LISTS attribute value.
		MEMBER_LIST		local_param;// LOCAL_PARAMETERS attribute value.
		MEMBER_LIST		menus;		// MENUS attribute value.
		MEMBER_LIST		methods;	// METHODS attribute value.
		MEMBER_LIST		param_lists;// PARAMETER_LIST attribute value.

		MEMBER_LIST		files;		// FILES attribute value.
		MEMBER_LIST		plugins;	// PLUGINS attribute value.

		////////////////////////////////////////

		BLOCK_DEPBIN   *depbin;

        char _dummy[10]{0};

		FLAT_BLOCK():ItemBase(ITYPE_BLOCK)
		{
			characteristic	= 0;

			depbin = new BLOCK_DEPBIN();
		}

		virtual ~FLAT_BLOCK();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_BLOCK(*this); };

		//copy constructor
		FLAT_BLOCK(const FLAT_BLOCK &flat_block);
		
		//Overloaded Assignment Operator 
		FLAT_BLOCK& operator= (const FLAT_BLOCK &flat_block);

		static AttributeNameSet const All_Attrs;
		static AttributeNameSet const Every_Attrs;

		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::parameters				, depbin->db_param				, param);
			DEPBIN_MAP( nsEDDEngine::axis_items				, depbin->db_axis				, axis);			
			DEPBIN_MAP( nsEDDEngine::characteristics		, depbin->db_characteristic		, characteristic);		
			DEPBIN_MAP( nsEDDEngine::chart_items			, depbin->db_chart				, chart);		
			DEPBIN_MAP( nsEDDEngine::charts					, depbin->db_charts				, charts);		
			DEPBIN_MAP( nsEDDEngine::collection_items		, depbin->db_collect			, collect);	
			DEPBIN_MAP( nsEDDEngine::edit_display_items		, depbin->db_edit_disp			, edit_disp); 	
			DEPBIN_MAP( nsEDDEngine::file_items				, depbin->db_file				, file); 		
			DEPBIN_MAP( nsEDDEngine::graph_items			, depbin->db_graph				, graph);		
			DEPBIN_MAP( nsEDDEngine::graphs					, depbin->db_graphs				, graphs); 	
			DEPBIN_MAP( nsEDDEngine::grid_items				, depbin->db_grid				, grid); 		
			DEPBIN_MAP( nsEDDEngine::grids					, depbin->db_grids				, grids); 		
			DEPBIN_MAP( nsEDDEngine::reference_array_items	, depbin->db_item_array			, item_array);	
			DEPBIN_MAP( nsEDDEngine::menus					, depbin->db_menus				, menus); 		
			DEPBIN_MAP( nsEDDEngine::menu_items				, depbin->db_menu				, menu); 		
			DEPBIN_MAP( nsEDDEngine::methods				, depbin->db_methods			, methods);	
			DEPBIN_MAP( nsEDDEngine::method_items			, depbin->db_method				, method);		
			DEPBIN_MAP( nsEDDEngine::unit_items				, depbin->db_unit				, unit); 		
			DEPBIN_MAP( nsEDDEngine::refresh_items			, depbin->db_refresh			, refresh); 	
			DEPBIN_MAP( nsEDDEngine::write_as_one_items		, depbin->db_wao				, wao); 		
			DEPBIN_MAP( nsEDDEngine::image_items			, depbin->db_image				, image);		
			DEPBIN_MAP( nsEDDEngine::source_items			, depbin->db_source				, source); 	
			DEPBIN_MAP( nsEDDEngine::waveform_items			, depbin->db_waveform			, waveform);	
			DEPBIN_MAP( nsEDDEngine::list_items				, depbin->db_list				, list);		
			DEPBIN_MAP( nsEDDEngine::lists					, depbin->db_lists				, lists);		
			DEPBIN_MAP( nsEDDEngine::local_parameters		, depbin->db_local_param		, local_param);		
			DEPBIN_MAP( nsEDDEngine::parameter_lists		, depbin->db_param_list			, param_lists); 		
			DEPBIN_MAP(nsEDDEngine::plugin_items		,depbin->db_plugin_items,	plugin_items); 		
			DEPBIN_MAP(nsEDDEngine::files				,depbin->db_files,			files); 		
			DEPBIN_MAP(nsEDDEngine::plugins				,depbin->db_plugins,		plugins); 		

		}
	};

	/*
	* RESPONSE CODE item
	*/

	class _INTERFACE RESP_CODE_DEPBIN {
	private:
		//AssignRespCodeDepbin is used to assign all the attributes of RESP_CODE_DEPBIN
		void AssignRespCodeDepbin(const RESP_CODE_DEPBIN &resp_code_depbin);
	public:
		DEPBIN         *db_item_information;

		DEPBIN         *db_member;

		RESP_CODE_DEPBIN() : db_item_information(0), db_member(0) {};
		//copy constructor
		RESP_CODE_DEPBIN(const RESP_CODE_DEPBIN &resp_code_depbin);
		
		//Overloaded Assignment Operator 
		RESP_CODE_DEPBIN& operator= (const RESP_CODE_DEPBIN &resp_code_depbin);
		virtual ~RESP_CODE_DEPBIN();

	};

	class _INTERFACE FLAT_RESP_CODE : public ItemBase
	{
		//AssignFlatRespCode is used to assign all the attributes of FLAT_RESP_CODE
		void AssignFlatRespCode(const FLAT_RESP_CODE &flat_resp_code);
	public:
		ITEM_INFORMATION item_info;

		RESPONSE_CODE_LIST member;	// MEMBERS attribute value.

		RESP_CODE_DEPBIN *depbin;

		FLAT_RESP_CODE():ItemBase(ITYPE_RESP_CODES)
		{
			//member has its own constructor
			depbin = new RESP_CODE_DEPBIN();
		};

		virtual ~FLAT_RESP_CODE();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_RESP_CODE(*this); };

		//copy constructor
		FLAT_RESP_CODE(const FLAT_RESP_CODE &flat_resp_code);
		
		//Overloaded Assignment Operator 
		FLAT_RESP_CODE& operator= (const FLAT_RESP_CODE &flat_resp_code);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::response_codes		, depbin->db_member				, member);
		}
	};


	/*
	* COMMAND item
	*/

	class _INTERFACE COMMAND_DEPBIN	{
	private:
		//AssignFlatCommand is used to assign all the attributes of COMMAND_DEPBIN
		void AssignFlatCommandDepbin(const COMMAND_DEPBIN &command_depbin);
	public:
		DEPBIN         *db_item_information;

		DEPBIN		   *db_number;
		DEPBIN         *db_oper;		
		DEPBIN         *db_trans;
		DEPBIN         *db_resp_codes;
		DEPBIN         *db_index;
		DEPBIN         *db_slot;
		DEPBIN         *db_sub_slot;
		DEPBIN		   *db_block_b;
		DEPBIN		   *db_connection;

		DEPBIN         *db_api;
		DEPBIN         *db_header;

		COMMAND_DEPBIN() : db_item_information(0), db_number(0), db_oper(0), db_trans(0), db_resp_codes(0), db_index(0), db_slot(0), db_sub_slot(0), db_block_b(0), db_connection(0), db_api(0), db_header(0) {};
		//copy constructor
		COMMAND_DEPBIN(const COMMAND_DEPBIN &command_depbin);
		
		//Overloaded Assignment Operator 
		COMMAND_DEPBIN& operator= (const COMMAND_DEPBIN &command_depbin);
		virtual ~COMMAND_DEPBIN();
	};

	class _INTERFACE FLAT_COMMAND : public ItemBase
	{
		//AssignFlatCommand is used to assign all the attributes of FLAT_COMMAND
		void AssignFlatCommand(const FLAT_COMMAND &flat_command);
	public:
		ITEM_INFORMATION item_info;

		ulong			number;		// NUMBER attribute value.
		CommandOperation oper;		// OPERATION attribute value.
		TRANSACTION_LIST trans;		// TRANSACTION attribute value.
		RESPONSE_CODES	resp_codes;	// RESPONSE_CODES attribute value.
		ulong			index;		// INDEX attribute value.
		ulong			slot;		// SLOT attribute value.
		ulong			sub_slot;	// SUB_SLOT attribute value.
		ITEM_ID		    block_b;	// BLOCK_B attribute value.
		ITEM_ID		    connection;	// 

		ulong           api;		// API attribute value.
		STRING			header;		// HEADER attribute value.

		COMMAND_DEPBIN *depbin;

		FLAT_COMMAND():ItemBase(ITYPE_COMMAND)
		{
			number		= 0;
			oper		= (CommandOperation)0;
			// trans and resp_codes have their own contructors
			index		= 0;
			slot		= 0;
			sub_slot	= 0;
			block_b		= 0;
			connection	= 0;
			api			= 0;

			depbin = new COMMAND_DEPBIN();
		};

		virtual ~FLAT_COMMAND();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COMMAND(*this); };

		//copy constructor
		FLAT_COMMAND(const FLAT_COMMAND &flat_command);
		
		//Overloaded Assignment Operator 
		FLAT_COMMAND& operator= (const FLAT_COMMAND &flat_command);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information		, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::command_number			, depbin->db_number				, number);
			DEPBIN_MAP( nsEDDEngine::operation				, depbin->db_oper				, oper);
			DEPBIN_MAP( nsEDDEngine::transaction			, depbin->db_trans				, trans);
			DEPBIN_MAP( nsEDDEngine::response_codes			, depbin->db_resp_codes			, resp_codes);
			DEPBIN_MAP( nsEDDEngine::index					, depbin->db_index				, index);
			DEPBIN_MAP( nsEDDEngine::slot					, depbin->db_slot				, slot);
			DEPBIN_MAP( nsEDDEngine::sub_slot				, depbin->db_sub_slot			, sub_slot);
			DEPBIN_MAP( nsEDDEngine::block_b				, depbin->db_block_b			, block_b);
			DEPBIN_MAP( nsEDDEngine::api					, depbin->db_api				, api);
			DEPBIN_MAP( nsEDDEngine::header					, depbin->db_header				, header);
		}
	};

	/*
	* BLOCK_B item
	*/

	class _INTERFACE BLOCK_B_DEPBIN {
	private:
		//AssignBlockBDepbin is used to assign all the attributes of BLOCK_B_DEPBIN
		void AssignBlockBDepbin(const BLOCK_B_DEPBIN &block_b_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN          *db_number;
		DEPBIN			*db_type;

		BLOCK_B_DEPBIN() : db_item_information(0), db_number(0), db_type(0) {};
		//copy constructor
		BLOCK_B_DEPBIN(const BLOCK_B_DEPBIN &block_b_depbin);
		
		//Overloaded Assignment Operator 
		BLOCK_B_DEPBIN& operator= (const BLOCK_B_DEPBIN &block_b_depbin);
		virtual ~BLOCK_B_DEPBIN();

	};

	class _INTERFACE FLAT_BLOCK_B : public ItemBase
	{
		//AssignFlatBlockB is used to assign all the attributes of FLAT_BLOCK_B
		void AssignFlatBlockB(const FLAT_BLOCK_B &flat_block_b);
	public:
		ITEM_INFORMATION item_info;

		ulong           number;	// NUMBER attribute value.
		BlockBType      type;	// TYPE attribute value.

		BLOCK_B_DEPBIN *depbin;

		FLAT_BLOCK_B():ItemBase(ITYPE_BLOCK_B)
		{
			number	= 0;
			type	= UNKNOWN_BLOCK_TYPE;
			depbin = new BLOCK_B_DEPBIN();
		}

		virtual ~FLAT_BLOCK_B();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_BLOCK_B(*this); };

		//copy constructor
		FLAT_BLOCK_B(const FLAT_BLOCK_B &flat_block_b);
		
		//Overloaded Assignment Operator 
		FLAT_BLOCK_B& operator= (const FLAT_BLOCK_B &flat_block_b);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::block_number		, depbin->db_number				, number);
			DEPBIN_MAP( nsEDDEngine::block_b_type		, depbin->db_type				, type);
		}
	};


	/*
	* VARIABLE   LIST item
	*/

	class _INTERFACE VAR_LIST_DEPBIN
	{
	private:
		//AssignVarListDepbin is used to assign all the attributes of VAR_LIST_DEPBIN
		void AssignVarListDepbin(const VAR_LIST_DEPBIN &var_list_depbin);
	public:
		DEPBIN			*db_item_information;
		DEPBIN         *db_members;
		DEPBIN         *db_help;
		DEPBIN         *db_label;
		DEPBIN         *db_resp_codes;

		VAR_LIST_DEPBIN() : db_item_information(0), db_members(0), db_help(0), db_label(0), db_resp_codes(0) {};
		//copy constructor
		VAR_LIST_DEPBIN(const VAR_LIST_DEPBIN &var_list_depbin);
		
		//Overloaded Assignment Operator 
		VAR_LIST_DEPBIN& operator= (const VAR_LIST_DEPBIN &var_list_depbin);
		virtual ~VAR_LIST_DEPBIN();
	};

	class _INTERFACE FLAT_VAR_LIST : public ItemBase
	{
		//AssignFlatVarList is used to assign all the attributes of FLAT_VAR_LIST
		void AssignFlatVarList(const FLAT_VAR_LIST &flat_var_list);
	public:
		ITEM_INFORMATION item_info;
		
		MEMBER_LIST     members;	// MEMBERS attribute value.
		STRING          help;		// HELP attribute value.
		STRING          label;		// LABEL attribute value.
		ITEM_ID         resp_codes;	// RESPONSE_CODES attribute value.
		VAR_LIST_DEPBIN *depbin;


		FLAT_VAR_LIST():ItemBase(ITYPE_VAR_LIST)
		{
			resp_codes = 0;
			depbin = new VAR_LIST_DEPBIN();
		}

		virtual ~FLAT_VAR_LIST();
		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_VAR_LIST(*this); };

		//copy constructor
		FLAT_VAR_LIST(const FLAT_VAR_LIST &flat_var_list);
		
		//Overloaded Assignment Operator 
		FLAT_VAR_LIST& operator= (const FLAT_VAR_LIST &flat_var_list);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::members			, depbin->db_members			, members);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::response_codes		, depbin->db_resp_codes			, resp_codes);
		}
};    

////////////////////////////////////////////////////////////

	class _INTERFACE MEMBER_DEPBIN
	{
	private:
		//AssignMemberDepbin is used to assign all the attributes of MEMBER_DEPBIN
		void AssignMemberDepbin(const MEMBER_DEPBIN &member_depbin);
	public:
		DEPBIN			*db_item_information;

		MEMBER_DEPBIN() : db_item_information(0) {};
		//copy constructor
		MEMBER_DEPBIN(const MEMBER_DEPBIN &flat_member);
		
		//Overloaded Assignment Operator 
		MEMBER_DEPBIN& operator= (const MEMBER_DEPBIN &flat_member);
		virtual ~MEMBER_DEPBIN(){}
	};

	class _INTERFACE FLAT_MEMBER : public ItemBase
	{
		//AssignFlatMember is used to assign all the attributes of FLAT_MEMBER
		void AssignFlatMember(const FLAT_MEMBER &flat_member);
	public:
		ITEM_INFORMATION item_info;
		
		MEMBER_DEPBIN *depbin;

		FLAT_MEMBER():ItemBase(ITYPE_MEMBER)
		{
			depbin = new MEMBER_DEPBIN();
		}

		virtual ~FLAT_MEMBER()
		{
			FreeDepbins();
			delete depbin;
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_MEMBER(*this); };

		//copy constructor
		FLAT_MEMBER(const FLAT_MEMBER &flat_member);
		
		//Overloaded Assignment Operator 
		FLAT_MEMBER& operator= (const FLAT_MEMBER &flat_member);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			depbin_map.clear();

			DEPBIN_MAP(nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
		}
	};   
 
	class _INTERFACE BLOB_DEPBIN
	{
	private:
		//AssignBlob is used to assign all the attributes of BLOB_DEPBIN
		void AssignBlobDepbin(const BLOB_DEPBIN &blob_depbin);
	public:
		DEPBIN			*db_item_information;
		DEPBIN			*db_label;
		DEPBIN			*db_help;
		DEPBIN			*db_handling;
		DEPBIN			*db_identity;
		DEPBIN			*db_count;

		BLOB_DEPBIN() : db_item_information(0), db_label(0), db_help(0), db_handling(0), db_identity(0), db_count(0)   {};
		//copy constructor
		BLOB_DEPBIN(const BLOB_DEPBIN &blob_depbin);
		
		//Overloaded Assignment Operator 
		BLOB_DEPBIN& operator= (const BLOB_DEPBIN &blob_depbin);

		virtual ~BLOB_DEPBIN(){}
	};

	class _INTERFACE FLAT_BLOB : public ItemBase
	{
		//AssignFlatBlob is used to assign all the attributes of FLAT_BLOB
		void AssignFlatBlob(const FLAT_BLOB &flat_blob);
	public:
		ITEM_INFORMATION item_info;
	
		BLOB_DEPBIN *depbin;

		STRING label;		// LABEL attribute value.
		STRING help;		// HELP attribute value.
		Handling handling;	// HANDLING attribute value.
		STRING identity;	// IDENTITY attribute value.
		ulong  count;

		FLAT_BLOB():ItemBase(ITYPE_BLOB)
		{
			depbin = new BLOB_DEPBIN();

			count=0;
		}

		virtual ~FLAT_BLOB()
		{
			FreeDepbins();
			delete depbin;
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		//copy constructor
		FLAT_BLOB(const FLAT_BLOB &flat_blob);

		virtual ItemBase* Replicate() { return new FLAT_BLOB(*this); };
		
		//Overloaded Assignment Operator 
		FLAT_BLOB& operator= (const FLAT_BLOB &flat_blob);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::handling			, depbin->db_handling			, handling);
			DEPBIN_MAP( nsEDDEngine::identity			, depbin->db_identity			, identity);
			DEPBIN_MAP( nsEDDEngine::count				, depbin->db_count				, count);
		}
	};   


	class _INTERFACE PLUGIN_DEPBIN
	{
	private:
		//AssignPluginDepbin is used to assign all the attributes of PLUGIN_DEPBIN
		void AssignPluginDepbin(const PLUGIN_DEPBIN &plugin_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_label;
		DEPBIN			*db_help;
		DEPBIN			*db_uuid;
		DEPBIN			*db_valid;
		DEPBIN			*db_visibility;

		PLUGIN_DEPBIN() : db_item_information(0), db_label(0), db_help(0), db_uuid(0), db_valid(0), db_visibility(0)  {}
		//copy constructor
		PLUGIN_DEPBIN(const PLUGIN_DEPBIN &plugin_depbin);
		
		//Overloaded Assignment Operator 
		PLUGIN_DEPBIN& operator= (const PLUGIN_DEPBIN &plugin_depbin);
		virtual ~PLUGIN_DEPBIN(){}
	};

	class _INTERFACE FLAT_PLUGIN : public ItemBase
	{
		//AssignFlatPlugin is used to assign all the attributes of FLAT_PLUGIN
		void AssignFlatPlugin(const FLAT_PLUGIN &flat_plugin);
	public:
		ITEM_INFORMATION item_info;
		
		PLUGIN_DEPBIN *depbin;

		STRING label;			// LABEL attribute value.
		STRING help;			// HELP attribute value.
		GUID uuid;				// UUID attribute value.
		Boolean         valid;	// VALIDITY attribute value.
		Boolean			visibility; // VISIBILITY attribute value.

		FLAT_PLUGIN():ItemBase(ITYPE_PLUGIN)
		{
			valid	 = True;
			visibility	 = True;
			depbin = new PLUGIN_DEPBIN();
		}

		virtual ~FLAT_PLUGIN() 
		{ 			

			FreeDepbins();
			delete depbin; 
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_PLUGIN(*this); };

		//copy constructor
		FLAT_PLUGIN(const FLAT_PLUGIN &flat_plugin);
		
		//Overloaded Assignment Operator 
		FLAT_PLUGIN& operator= (const FLAT_PLUGIN &flat_plugin);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP( nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::uuid				, depbin->db_uuid				, uuid);
			DEPBIN_MAP( nsEDDEngine::validity			, depbin->db_valid				, valid);
			DEPBIN_MAP( nsEDDEngine::visibility			, depbin->db_visibility			, visibility);

		}
	};    

		
	class _INTERFACE TEMPLATE_DEPBIN
	{
	private:
		//AssignTemplateDepbin is used to assign all the attributes of TEMPLATE_DEPBIN
		void AssignTemplateDepbin(const TEMPLATE_DEPBIN &template_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_label;
		DEPBIN			*db_help;
		DEPBIN			*db_default_values;

		DEPBIN			*db_validity;

		TEMPLATE_DEPBIN() : db_item_information(0), db_label(0), db_help(0), db_default_values(0), db_validity(0){};
		//copy constructor
		TEMPLATE_DEPBIN(const TEMPLATE_DEPBIN &template_depbin);
		
		//Overloaded Assignment Operator 
		TEMPLATE_DEPBIN& operator= (const TEMPLATE_DEPBIN &template_depbin);
		virtual ~TEMPLATE_DEPBIN(){}
	};

	class _INTERFACE FLAT_TEMPLATE : public ItemBase
	{
		//AssignFlatTemplate is used to assign all the attributes of FLAT_TEMPLATE
		void AssignFlatTemplate(const FLAT_TEMPLATE &flat_template);
	public:
		ITEM_INFORMATION item_info;
		
		TEMPLATE_DEPBIN *depbin;

		STRING label;	// LABEL attribute value.
		STRING help;	// HELP attribute value.
		DEFAULT_VALUES_LIST default_values;	// DEFAULT_VALUE attribute value.
		Boolean				valid;	// VALIDITY attribute value.

		FLAT_TEMPLATE():ItemBase(ITYPE_TEMPLATE)
		{
			valid = True;
			depbin = new TEMPLATE_DEPBIN();
		}

		virtual ~FLAT_TEMPLATE() 
		{ 			

			FreeDepbins();
			delete depbin; 
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_TEMPLATE(*this); };

		//copy constructor
		FLAT_TEMPLATE(const FLAT_TEMPLATE &flat_template);
		
		//Overloaded Assignment Operator 
		FLAT_TEMPLATE& operator= (const FLAT_TEMPLATE &flat_template);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			DEPBIN_MAP(nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label				, depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				, depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::default_values		, depbin->db_default_values		, default_values);
			DEPBIN_MAP(nsEDDEngine::validity			, depbin->db_validity			, valid);
		}
};   

	
	class _INTERFACE COMPONENT_DEPBIN
	{
	private:
		//AssignComponentDepbin is used to assign all the attributes of COMPONENT_DEPBIN
		void AssignComponentDepbin(const COMPONENT_DEPBIN &component_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_label					;					
		DEPBIN			*db_help					;
		DEPBIN			*db_can_delete				;
		DEPBIN			*db_check_configuration		;
		DEPBIN			*db_classification			;
		DEPBIN			*db_component_byte_order	;
		DEPBIN			*db_component_connect_point	;
		DEPBIN			*db_component_parent		;
		DEPBIN			*db_component_path			;
		DEPBIN			*db_component_relations		;
		DEPBIN			*db_declaration				;
		DEPBIN			*db_detect					;
		DEPBIN			*db_edd						;
		DEPBIN			*db_product_uri				;
		DEPBIN			*db_initial_values			;
		DEPBIN			*db_protocol				;
		DEPBIN			*db_redundancy				;
		DEPBIN			*db_scan					;
		DEPBIN			*db_scan_list				;

		COMPONENT_DEPBIN() :	db_item_information			(0),
								db_label					(0),
								db_help						(0),
								db_can_delete				(0),
								db_check_configuration		(0),
								db_classification			(0),
								db_component_byte_order		(0),
								db_component_connect_point	(0),
								db_component_parent			(0),
								db_component_path			(0),
								db_component_relations		(0),
								db_declaration				(0),
								db_detect					(0),
								db_edd						(0),
								db_product_uri				(0),
								db_initial_values			(0),
								db_protocol					(0),
								db_redundancy				(0),
								db_scan						(0),
								db_scan_list				(0)
		{};

		//copy constructor
		COMPONENT_DEPBIN(const COMPONENT_DEPBIN &component_depbin);
		
		//Overloaded Assignment Operator 
		COMPONENT_DEPBIN& operator= (const COMPONENT_DEPBIN &component_depbin);
		virtual ~COMPONENT_DEPBIN(){}
	};

	class _INTERFACE FLAT_COMPONENT : public ItemBase
	{
		//AssignFlatComponent is used to assign all the attributes of FLAT_COMPONENT
		void AssignFlatComponent(const FLAT_COMPONENT &flat_component);
	public:
		ITEM_INFORMATION item_info;

		STRING label;		// LABEL attribute value.			
		STRING help;		// HELP attribute value.

		Boolean				can_delete;				// CAN_DELETE attribute value.			
		DESC_REF			check_configuration;	// CHECK_CONFIGURATION attribute value.		
		Classification		classification;			// CLASSIFICATION attribute value.		
		ByteOrder			component_byte_order;	// BYTE_ORDER attribute value.	
		DESC_REF			component_connect_point;// CONNECTION_POINT attribute value.		
		DESC_REF			component_parent;		// COMPONENT_PARENT attribute value.		
		STRING				component_path;			// COMPONENT_PATH attribute value.		
		OP_REF_TRAIL_LIST	component_relations;	// COMPONENT_RELATIONS attribute value.		
		ulong				declaration;			// DECLARATION attribute value.		
		DESC_REF			detect;					// DETECT attribute value.			
		STRING				edd;					// EDD attribute value.	
		STRING				product_uri;			// PRODUCT_URI attribute value.	
		DEFAULT_VALUES_LIST	initial_values;			// INITIAL_VALUES attribute value.		
		Protocol			protocol;				// PROTOCOL attribute value.	
		EXPR				redundancy;				// REDUNDANCY attribute value.	
		DESC_REF			scan;					// SCAN attribute value.	
		DESC_REF			scan_list;				// SCAN_LIST attribute value.	
		
		COMPONENT_DEPBIN *depbin;

		FLAT_COMPONENT():ItemBase(ITYPE_COMPONENT)
		{
			can_delete = True;
			classification = classification_none;
			component_byte_order = No_Order;
			declaration = 0;
			protocol = protocol_none;
			depbin = new COMPONENT_DEPBIN();
		}

		virtual ~FLAT_COMPONENT();
		

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COMPONENT(*this); };

		//copy constructor
		FLAT_COMPONENT(const FLAT_COMPONENT &flat_component);
		
		//Overloaded Assignment Operator 
		FLAT_COMPONENT& operator= (const FLAT_COMPONENT &flat_component);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			depbin_map.clear();

			DEPBIN_MAP(nsEDDEngine::item_information		, depbin->db_item_information			, item_info);	
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label						, label);					
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help						, help);
			DEPBIN_MAP( nsEDDEngine::can_delete				, depbin->db_can_delete					, can_delete);
			DEPBIN_MAP( nsEDDEngine::check_configuration	, depbin->db_check_configuration		, check_configuration);
			DEPBIN_MAP( nsEDDEngine::classification			, depbin->db_classification				, classification);
			DEPBIN_MAP( nsEDDEngine::component_byte_order	, depbin->db_component_byte_order		, component_byte_order);
			DEPBIN_MAP( nsEDDEngine::component_connect_point, depbin->db_component_connect_point	, component_connect_point);
			DEPBIN_MAP( nsEDDEngine::component_parent		, depbin->db_component_parent			, component_parent);
			DEPBIN_MAP( nsEDDEngine::component_path			, depbin->db_component_path				, component_path);
			DEPBIN_MAP( nsEDDEngine::component_relations	, depbin->db_component_relations		, component_relations);
			DEPBIN_MAP( nsEDDEngine::declaration			, depbin->db_declaration				, declaration);
			DEPBIN_MAP( nsEDDEngine::detect					, depbin->db_detect						, detect);
			DEPBIN_MAP( nsEDDEngine::edd					, depbin->db_edd						, edd);
			DEPBIN_MAP( nsEDDEngine::product_uri			, depbin->db_product_uri				, product_uri);
			DEPBIN_MAP( nsEDDEngine::initial_values			, depbin->db_initial_values				, initial_values);
			DEPBIN_MAP( nsEDDEngine::protocol				, depbin->db_protocol					, protocol);
			DEPBIN_MAP( nsEDDEngine::redundancy				, depbin->db_redundancy					, redundancy);
			DEPBIN_MAP( nsEDDEngine::scan					, depbin->db_scan						, scan);
			DEPBIN_MAP( nsEDDEngine::scan_list				, depbin->db_scan_list					, scan_list);
		}
	};   

	
	class _INTERFACE COMPONENT_FOLDER_DEPBIN
	{
	private:
		//AssignComponentFolderDepbin is used to assign all the attributes of COMPONENT_FOLDER_DEPBIN
		void AssignComponentFolderDepbin(const COMPONENT_FOLDER_DEPBIN &component_folder_depbin);
	public:
		DEPBIN			*db_item_information	;
		DEPBIN			*db_label				;
		DEPBIN			*db_help				;
		DEPBIN			*db_classification		;
		DEPBIN			*db_component_parent	;
		DEPBIN			*db_component_path		;
		DEPBIN			*db_protocol			;

		COMPONENT_FOLDER_DEPBIN() : db_item_information(0),
									db_label			(0),
									db_help				(0),
									db_classification	(0),
									db_component_parent	(0),
									db_component_path	(0),
									db_protocol			(0)									
		{};

		//copy constructor
		COMPONENT_FOLDER_DEPBIN(const COMPONENT_FOLDER_DEPBIN &component_folder_depbin);
		
		//Overloaded Assignment Operator 
		COMPONENT_FOLDER_DEPBIN& operator= (const COMPONENT_FOLDER_DEPBIN &component_folder_depbin);

		virtual ~COMPONENT_FOLDER_DEPBIN(){}
	};

	class _INTERFACE FLAT_COMPONENT_FOLDER : public ItemBase
	{
		//AssignFlatComponentFolder is used to assign all the attributes of FLAT_COMPONENT_FOLDER
		void AssignFlatComponentFolder(const FLAT_COMPONENT_FOLDER &flat_component_folder);
	public:
		ITEM_INFORMATION item_info;			
		
		STRING label;	// LABEL attribute value.							
		STRING help;	// HELP attribute value.							

		Classification classification;	// CLASSIFICATION attribute value.			

		DESC_REF component_parent;		// COMPONENT_PARENT attribute value.					
		STRING component_path;			// COMPONENT_PATH attribute value.						

		Protocol protocol;				// PROTOCOL attribute value.							

		COMPONENT_FOLDER_DEPBIN *depbin;

		FLAT_COMPONENT_FOLDER():ItemBase(ITYPE_COMPONENT_FOLDER)
		{
			classification = classification_none;
			protocol = protocol_none;
			depbin = new COMPONENT_FOLDER_DEPBIN();
		}

		virtual ~FLAT_COMPONENT_FOLDER() 
		{ 			
			FreeDepbins();
			delete depbin; 
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COMPONENT_FOLDER(*this); };

		//copy constructor
		FLAT_COMPONENT_FOLDER(const FLAT_COMPONENT_FOLDER &flat_component_folder);
		
		//Overloaded Assignment Operator 
		FLAT_COMPONENT_FOLDER& operator= (const FLAT_COMPONENT_FOLDER &flat_component_folder);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			depbin_map.clear();

			DEPBIN_MAP(nsEDDEngine::item_information		, depbin->db_item_information		, item_info);	
			DEPBIN_MAP( nsEDDEngine::label					, depbin->db_label					, label);
			DEPBIN_MAP( nsEDDEngine::help					, depbin->db_help					, help);
			DEPBIN_MAP( nsEDDEngine::classification			, depbin->db_classification			, classification);
			DEPBIN_MAP( nsEDDEngine::component_parent		, depbin->db_component_parent		, component_parent);
			DEPBIN_MAP( nsEDDEngine::component_path			, depbin->db_component_path			, component_path);
			DEPBIN_MAP( nsEDDEngine::protocol				, depbin->db_protocol				, protocol);


		}
	}; 

	// In EFF COMPONENT_REFERENCE is known as "Component_Descriptor". Plesae see section 5.7.27 in FCG_TS12041.doc
	class _INTERFACE COMPONENT_REFERENCE_DEPBIN
	{
	private:
		//AssignComponentDescriptorDepbin is used to assign all the attributes of COMPONENT_REFERENCE_DEPBIN
		void AssignComponentReferenceDepbin(const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_classification		;
		DEPBIN			*db_component_parent	;
		DEPBIN			*db_component_path		;
		DEPBIN			*db_protocol			;
		DEPBIN			*db_manufacturer		;
		DEPBIN			*db_device_type			;
		DEPBIN			*db_device_revision		;

		COMPONENT_REFERENCE_DEPBIN() : db_item_information (0),
										db_classification	(0),
										db_component_parent	(0),
										db_component_path	(0),
										db_protocol			(0),
										db_manufacturer		(0),
										db_device_type		(0),
										db_device_revision	(0)
		{};

		//copy constructor
		COMPONENT_REFERENCE_DEPBIN(const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin);
		
		//Overloaded Assignment Operator 
		COMPONENT_REFERENCE_DEPBIN& operator= (const COMPONENT_REFERENCE_DEPBIN &component_reference_depbin);
		virtual ~COMPONENT_REFERENCE_DEPBIN(){}
	};

	// In EFF COMPONENT_REFERENCE is known as "Component_Descriptor". Plesae see section 5.7.27 in FCG_TS12041.doc
	class _INTERFACE FLAT_COMPONENT_REFERENCE : public ItemBase
	{
		//AssignFlatComponentDescriptor is used to assign all the attributes of FLAT_COMPONENT_REFERENCE
		void AssignFlatComponentReference(const FLAT_COMPONENT_REFERENCE &flat_component_reference);
	public:
		ITEM_INFORMATION item_info;			
		
		Classification classification;	// CLASSIFICATION attribute value.			

		DESC_REF component_parent;		// COMPONENT_PARENT attribute value.			
		STRING component_path;			// COMPONENT_PATH attribute value.				

		Protocol protocol;				// PROTOCOL attribute value.					

		ulong manufacturer;				// MANUFACTURER attribute value.					
		ulong device_type;				// DEVICE_TYPE attribute value.					
		ulong device_revision;			// DEVICE_REVISION attribute value.				

		COMPONENT_REFERENCE_DEPBIN *depbin;

		FLAT_COMPONENT_REFERENCE():ItemBase(ITYPE_COMPONENT_REFERENCE)
		{
			classification = classification_none;
			protocol = protocol_none;
			manufacturer = 0;
			device_type = 0;
			device_revision = 0;
			depbin = new COMPONENT_REFERENCE_DEPBIN();
		}

		virtual ~FLAT_COMPONENT_REFERENCE() 
		{ 			
			FreeDepbins();
			delete depbin; 
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COMPONENT_REFERENCE(*this); };

		//copy constructor
		FLAT_COMPONENT_REFERENCE(const FLAT_COMPONENT_REFERENCE &flat_component_reference);
		
		//Overloaded Assignment Operator 
		FLAT_COMPONENT_REFERENCE& operator= (const FLAT_COMPONENT_REFERENCE &flat_component_reference);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			depbin_map.clear();

			DEPBIN_MAP(nsEDDEngine::item_information	, depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::classification		, depbin->db_classification		, classification);
			DEPBIN_MAP( nsEDDEngine::component_parent	, depbin->db_component_parent	, component_parent);
			DEPBIN_MAP( nsEDDEngine::component_path		, depbin->db_component_path		, component_path);
			DEPBIN_MAP( nsEDDEngine::protocol			, depbin->db_protocol			, protocol);
			DEPBIN_MAP( nsEDDEngine::manufacturer		, depbin->db_manufacturer		, manufacturer);
			DEPBIN_MAP( nsEDDEngine::device_type		, depbin->db_device_type		, device_type);
			DEPBIN_MAP( nsEDDEngine::device_revision	, depbin->db_device_revision	, device_revision);
		}
	};   

	
	class _INTERFACE COMPONENT_RELATION_DEPBIN
	{
	private:
		//AssignComponentRelationDepbin is used to assign all the attributes of FLAT_COMPONENT_RELATION
		void AssignComponentRelationDepbin(const COMPONENT_RELATION_DEPBIN &component_relation_depbin);
	public:
		DEPBIN			*db_item_information;

		DEPBIN			*db_label				;
		DEPBIN			*db_help				;
		DEPBIN			*db_maximum_number		;
		DEPBIN			*db_minimum_number		;
		DEPBIN			*db_addressing			;
		DEPBIN			*db_relation_type		;
		DEPBIN			*db_components			;

		COMPONENT_RELATION_DEPBIN() : db_item_information(0),
					db_label				(0),
					db_help					(0),
					db_maximum_number		(0),
					db_minimum_number		(0),
					db_addressing			(0),
					db_relation_type		(0),
					db_components			(0)
		{};
		//copy constructor
		COMPONENT_RELATION_DEPBIN(const COMPONENT_RELATION_DEPBIN &component_relation_depbin);
		
		//Overloaded Assignment Operator 
		COMPONENT_RELATION_DEPBIN& operator= (const COMPONENT_RELATION_DEPBIN &component_relation_depbin);
		virtual ~COMPONENT_RELATION_DEPBIN(){}
	};

	class _INTERFACE FLAT_COMPONENT_RELATION : public ItemBase
	{
		//AssignFlatComponentRelation is used to assign all the attributes of FLAT_COMPONENT_RELATION
		void AssignFlatComponentRelation(const FLAT_COMPONENT_RELATION &flat_component_relation);
	public:
		ITEM_INFORMATION item_info;			
	
		STRING label;		// LABEL attribute value.						
		STRING help;		// HELP attribute value.			

		ulong maximum_number;	// MAXIMUM_NUMBER attribute value.					
		ulong minimum_number;	// MINIMUM_NUMBER attribute value.					

		OP_REF_TRAIL_LIST addressing;		// ADDRESSING attribute value.			

		RelationType relation_type;			// RELATION_TYPE attribute value.			
		COMPONENT_SPECIFIER_LIST components;// COMPONENTS attribute value.		

		COMPONENT_RELATION_DEPBIN *depbin;

		FLAT_COMPONENT_RELATION():ItemBase(ITYPE_COMPONENT_RELATION)
		{
			maximum_number = 0;
			minimum_number = 0;
			relation_type = RELATIONTYPE_NONE;
			depbin = new COMPONENT_RELATION_DEPBIN();
		}

		virtual ~FLAT_COMPONENT_RELATION() 
		{ 			
			FreeDepbins();
			delete depbin; 
			depbin = nullptr;
		}

		virtual const char *GetTypeName()	{	return typeid(this).name();	}

		virtual ItemBase* Replicate() { return new FLAT_COMPONENT_RELATION(*this); };

		//copy constructor
		FLAT_COMPONENT_RELATION(const FLAT_COMPONENT_RELATION &flat_component_relation);
		
		//Overloaded Assignment Operator 
		FLAT_COMPONENT_RELATION& operator= (const FLAT_COMPONENT_RELATION &flat_component_relation);

		static AttributeNameSet const All_Attrs;
		void GetDepbinMap(std::map<AttributeName, AttributeItem>& depbin_map) 
		{
			depbin_map.clear();

			DEPBIN_MAP(nsEDDEngine::item_information	,depbin->db_item_information	, item_info);	
			DEPBIN_MAP( nsEDDEngine::label				,depbin->db_label				, label);
			DEPBIN_MAP( nsEDDEngine::help				,depbin->db_help				, help);
			DEPBIN_MAP( nsEDDEngine::maximum_number		,depbin->db_maximum_number		, maximum_number);
			DEPBIN_MAP( nsEDDEngine::minimum_number		,depbin->db_minimum_number		, minimum_number);
			DEPBIN_MAP( nsEDDEngine::addressing			,depbin->db_addressing			, addressing);
			DEPBIN_MAP( nsEDDEngine::relation_type		,depbin->db_relation_type		, relation_type);
			DEPBIN_MAP( nsEDDEngine::components			,depbin->db_components			, components);
		}
	};   
}
