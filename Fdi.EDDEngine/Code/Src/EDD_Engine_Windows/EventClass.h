#pragma once

#include <string>

class WindowsEventClass
{
private:

	void* m_handle;

public:
	// Constructors
	WindowsEventClass();	
	virtual ~WindowsEventClass();	
	BOOL Create();
	BOOL Close();
	BOOL Set();
	BOOL Wait();
	void Reset();	
	void* GetHandle();
};
