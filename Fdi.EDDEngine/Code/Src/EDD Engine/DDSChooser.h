#pragma once

#include "EDDEngineImpl.h"

class CDDSChooser
{
public:
	static CEDDEngineImpl* ChooseDDSLib( const wchar_t *sEDDBinaryFilename, 
		const wchar_t* pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode);

private:
	static int GetFileFormat( const wchar_t *sEDDBinaryFilename, UINT32 * manufacturer, UINT16	* device_type, UINT8 * device_revision);
    static UINT32 GetInteger( UINT8 *bin_ptr, int bin_size);
	static void LoadLegacyHartDeviceInformation(UINT8 * header_buffer,UINT32 * manufacturer, UINT16	* device_type, UINT8 * device_revision);
};