#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T13:03:42
#
#-------------------------------------------------

QT       -= gui

TARGET = FDI_DDS_Lib
TEMPLATE = lib
CONFIG += staticlib
#CONFIG +=plugin
DEFINES += FDI_DDS_LIB_LIBRARY \
            UNICODE \
QMAKESPEC=linux-g++-32

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=gnu++11

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += conversions.cpp \
    ddi_cmi.cpp \
    ddi_conv.cpp \
    ddi_dir.cpp \
    ddi_item.cpp \
    ddi_lang.cpp \
    ddi_tab.cpp \
    dds_chk.cpp \
    dds_err.cpp \
    dds_tab.cpp \
    evl_base.cpp \
    evl_cln.cpp \
    evl_cond.cpp \
    evl_dir.cpp \
    evl_enum.cpp \
    evl_expr.cpp \
    evl_iary.cpp \
    evl_item.cpp \
	evl_component.cpp \
    evl_mem.cpp \
    evl_menu.cpp \
    evl_ref.cpp \
    evl_rel.cpp \
    evl_resp.cpp \
    evl_rslv.cpp \
    evl_str.cpp \
    evl_tran.cpp \
    evl_type.cpp \
    fch_rdir.cpp \
    fch_ritm.cpp \
    fch_rodf.cpp \
    fch_slct.cpp \
    ManualUnitLookup.cpp \
    stdafx.cpp \


HEADERS += ../inc/rtn_code.h\
    ../inc/rtn_def.h \
    attrs.h \
    conversions.h \
    ddi_item.h \
    ddi_lib.h \
    ddi_tab.h \
    ddldefs.h \
    dds_chk.h \
    dds_tab.h \
    dds_upcl.h \
    env_info.h \
    evl_lib.h \
    evl_loc.h \
    evl_ret.h \
    fch_lib.h \
    flats.h \
    HARTBreaker.h \
    ManualUnitLookup.h \
    od_defs.h \
    std.h \
    stdafx.h \
    table.h \
    tags_sa.h \
    tst_fail.h \

unix {
    target.path = /usr/lib/
    INSTALLS += target
}


DESTDIR = $$PWD/../lib

CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}

include($$DESTDIR/../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../inc \
INCLUDEPATH += $$PWD/../FDI_DDS_AdapterLib \
INCLUDEPATH += $$PWD/../../../Interfaces/EDD_Engine_Interfaces \
INCLUDEPATH += $$PWD/../../../Inc \
INCLUDEPATH += $$PWD/../../EDD_Engine_Common \

android {
    INCLUDEPATH += $$PWD/../../EDD_Engine_Android \
}
else {
    INCLUDEPATH += $$PWD/../../EDD_Engine_Linux \
}

INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR

