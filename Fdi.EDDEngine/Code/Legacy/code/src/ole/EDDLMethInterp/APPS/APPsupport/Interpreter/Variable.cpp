// Variable.cpp //

#include "stdafx.h"
#include "Variable.h"


CVariable::CVariable()
{
}

CVariable::CVariable(
			CToken* pToken)
	:CToken(*pToken)
{
	RUL_TOKEN_TYPE Type = pToken->GetType();
	RUL_TOKEN_SUBTYPE SubType = pToken->GetSubType();

	if (Type == RUL_SIMPLE_VARIABLE)
	{
		switch(SubType)
		{
			case RUL_CHAR_DECL:
					m_Value = (char)0;
					break;
			case RUL_LONG_LONG_DECL:
					m_Value = (INT64)0;
					break;
			case RUL_UNSIGNED_LONG_LONG_DECL:
					m_Value = (UINT64)0;
					break;
					// Walt EPM 08sep08 - added
			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					m_Value = (wchar_t)0;
					break;
			case RUL_SHORT_INTEGER_DECL:
					m_Value = (short)0;
					break;
			case RUL_UNSIGNED_INTEGER_DECL:
					m_Value = (unsigned int)0;
					break;
					// Walt EPM 08sep08 - end added
			case RUL_INTEGER_DECL:
			case RUL_LONG_DECL:
			case RUL_TIMET_DECL:
					m_Value = (long)0;
					break;
			case RUL_BOOLEAN_DECL:
					m_Value = (bool)false;
					break;
			case RUL_REAL_DECL:
					m_Value = (float)0.0;
					break;
			case RUL_DOUBLE_DECL:
					m_Value = (double)0.0;//WS:EPM 10aug07
					break;
			case RUL_STRING_DECL:
					m_Value = (char*)"";
					break;
			case RUL_UNSIGNED_CHAR_DECL:
					m_Value = (unsigned char)' ';//WHS EP June17-2008 have changed this to make sure that it works for all data types
					break;
			case RUL_DD_STRING_DECL:	//this shall be a INTER_SAFEARRAY
					break;

		}
	}
}

CVariable::CVariable(
			const _CHAR* szLexeme,
			RUL_TOKEN_TYPE Type,
			RUL_TOKEN_SUBTYPE SubType) 
	:CToken(szLexeme,Type,SubType,-1)
{
}

CVariable::~CVariable()
{
}

void CVariable::Identify(
		_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,m_pszLexeme);
	strcat(szData,">");

	if(GetCompoundData())
	{
		strcat(szData,GetCompoundData()->m_szName);
		strcat(szData,",");
		strcat(szData,GetCompoundData()->m_szAttribute);
	} 

	strcat(szData,"</");
	strcat(szData,m_pszLexeme);
	strcat(szData,">");
}

INTER_VARIANT& CVariable::GetValue()
{
	return m_Value;
}

//Anil August 26 2005 For handling DD variable and Expression
void CVariable::SetVarType(RUL_TOKEN_TYPE Type,RUL_TOKEN_SUBTYPE SubType)
{
	
	if (Type == RUL_SIMPLE_VARIABLE)
	{
		switch(SubType)
		{
			case RUL_CHAR_DECL:
					m_Value = (char)' ';
					break;
			case RUL_LONG_LONG_DECL:
					m_Value = (INT64)0;
					break;
			case RUL_UNSIGNED_LONG_LONG_DECL:
					m_Value = (UINT64)0;
					break;
					// Walt EPM 08sep08 - added
			case RUL_UNSIGNED_SHORT_INTEGER_DECL:
					m_Value = (unsigned short)0;
					break;
			case RUL_SHORT_INTEGER_DECL:
					m_Value = (short)0;
					break;
			case RUL_UNSIGNED_INTEGER_DECL:
					m_Value = (unsigned int)0;
					break;
					// Walt EPM 08sep08 - end added
			case RUL_INTEGER_DECL:
			case RUL_LONG_DECL:
			case RUL_TIMET_DECL:
					m_Value = (long)0;
					break;
			case RUL_BOOLEAN_DECL:
					m_Value = (bool)false;
					break;
			case RUL_REAL_DECL:
					m_Value=(float)0.0;
					break;
			case RUL_DOUBLE_DECL:
					m_Value=(double)0.0;//WS:EPM 10aug07
					break;
			case RUL_STRING_DECL:
					m_Value=(char*)"";
					break;
			case RUL_DD_STRING_DECL:	//this shall be a INTER_SAFEARRAY
					m_Value = (wchar_t*)_T("");
					break;
			case RUL_UNSIGNED_CHAR_DECL:
					m_Value=(unsigned char)' ';//WHS EP June17-2008 have changed this to make sure that it works for all data types
					break;

		}
	}
	
	
}
