#pragma once

#include "nsConsumer\IEDDEngineLogger.h"

using namespace nsConsumer;

class CEDDEngineLogger : public nsConsumer::IEDDEngineLogger
{
	
public:
	CEDDEngineLogger();
	~CEDDEngineLogger(void);

	bool ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory);
    void Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory);
	void CloseLogFile();
	void OpenLogFile();
private:
	FILE* m_pEddEngineLogFile;
};