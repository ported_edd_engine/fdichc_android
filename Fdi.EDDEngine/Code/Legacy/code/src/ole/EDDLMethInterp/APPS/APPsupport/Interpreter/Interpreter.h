#ifndef INTERPRETER_H
#define INTERPRETER_H

#pragma warning (disable : 4786)//Anil August 26 2005
#include "Parser.h"
#include "ddbVar.h"

class CBuiltIn; //WHS May 24 2007 BUILTIN SUBCLASSING
class MEE; //Vibhor 010705: Added

class CInterpreter
{
private:
	/* Flag to indicate if source code is assigned or not */
	bool	m_bInitialized;

	/* Status of the interpreter */
	INTERPRETER_STATUS	m_intStatus;

	/* Pointer to the source as passed by the Client */
	char	*m_pchSourceCode;

	/* The pointer to the parser object */
	CParser  *m_pParser;

	MEE		 *m_pMEE; //Vibhor 010705 Added : Required for access to "Global" (DD) Data

public:
	CInterpreter();
	~CInterpreter();

	INTERPRETER_STATUS ExecuteCode
							(
#ifdef STANDALONE_PARSER
								CBuiltInLib  *pBuiltInLibParam
#else
								CBuiltIn *pBuiltInLibParam //WHS May 24 2007 BUILTIN SUBCLASSING
#endif
								, char *pchSource
								, char *pchSourceName
								, char *pchCodeData
								, char *&pchSymbolDump
								, MEE  *pMEE			//Vibhor 010705: Added
							);

	int GetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
					, hCitemBase**     ppDevObjVar = NULL
				);

	bool SetVariableValue
				(
					char *pchVariableName
					, INTER_VARIANT &varValue
				);

	bool SetVariableValue
				(
					CExpression *pExp
					, INTER_VARIANT &varValue
				);

	void FindInLocalSymbolTable (/*in*/char *pchVariableName, /*out*/CVariable **ppVar, /*out*/ bool *pbIsMethodLocal);
	void GetLocalVariables(wchar_t **pDebugInfoXml);

	//Anil Octobet 5 2005 for handling Method Calling Method
	INTERPRETER_STATUS ExecuteCode
							(
#ifdef STANDALONE_PARSER
								CBuiltInLib  *pBuiltInLibParam
#else
								CBuiltIn *pBuiltInLibParam //WHS May 24 2007 BUILTIN SUBCLASSING
#endif
								, char *pchSource
								, char *pchSourceName
								, char *pchCodeData
								, char *&pchSymbolDump
								, MEE  *pMEE
								, METHOD_ARG_INFO_VECTOR* vectMethArg
								, vector<INTER_VARIANT>* vectInterVar
								);

				
};

#endif /*INTERPRETER_H*/
