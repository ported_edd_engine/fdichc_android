/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)rmalloc.h	30.4  30  21 Nov 1996
 *
 *	rmalloc.h
 */

#ifndef RMALLOC_H
#define RMALLOC_H

#include "std.h"
#include <stddef.h>


/*
 *	memory allocation hidden bits
 */

#define DYNAMIC_HEAP	1L

/* These are the control structures for all the memory allocation stuff */
typedef struct {
	uintptr_t		ma_nextblk;		/* Address of next block, plus busy bit */
} RBLKHEAD;

typedef struct r_submalloc RSUBHEAP;
struct r_submalloc {
	RSUBHEAP	*ma_nextsub;	/* Next subheap in chain */
	RBLKHEAD	*ma_top;		/* The top of our allocation area */
	RBLKHEAD	*ma_bot;		/* The bottom */
	RBLKHEAD	*ma_last;		/* Last block allocated */
	size_t		ma_free;		/* The amount of memory free */
	size_t		ma_usage;		/* The number of bytes used */
	size_t		ma_osize;		/* Original number of byes configured */
	size_t		ma_nblks;		/* The number of blocks used + free */
	size_t		ma_maxused;		/* The max number of bytes used */
	size_t		ma_maxblks;		/* The max number of blocks */
	RBLKHEAD	ma_pseudotop;	/* Pseudo first heap pointer/flags */
};

typedef struct f_malloc {
	RSUBHEAP	*ma_firstsub;	/* First subheap chained in */
	RSUBHEAP	*ma_lastsub;	/* Last subheap chained in */
	RSUBHEAP	*ma_currsub;	/* Currently active subheap */
	size_t		ma_subsize;		/* Size for additional subheaps */
} RHEAP;

#define SUBHEAP_OVERHEAD		(sizeof(RSUBHEAP) + (2*sizeof(RBLKHEAD)))
#define HEAP_OVERHEAD			(sizeof(RHEAP) + SUBHEAP_OVERHEAD)

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern void *rmalloc P((RHEAP * heap, size_t nb));
extern void *rcalloc P((RHEAP * heap, size_t nelem, size_t elsize));
extern void *rrealloc P((RHEAP * heap, char *p, size_t nb));
extern void rfreeheap P((RHEAP *heap));
extern void rfree P((RHEAP * heap, char *p));
extern void rheapinit P((RHEAP * heap, size_t size));
extern void rcreateheap P((RHEAP *heap, size_t initsize, size_t addlsize));
extern int rmemberof P((RHEAP * heap,char *p));
extern void rdump P((RHEAP * heap, int verbose));


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif		/* RMALLOC_H */
