#ifndef ___BUILTINS_PARENT_H_
#define ___BUILTINS_PARENT_H_

#pragma warning(disable: 4996) //WHS (EmersonProcess) Disable the VS2005 security warning 

#include <nsEDDEngine/ProtocolType.h>
using nsEDDEngine::ProtocolType;

#pragma warning (disable : 4786)
#include <map>
#include <vector>

#include "RTN_CODE.H"
#include "BI_CODES.H"
#include "ddbdefs.h"
#include "ddbGeneral.h"

#include "BuiltInInfo.h"
#include "DDLDEFS.H"
//#include "PlatformCommon.h"
#include "stdstring.h"

#define BYTENUM(x)          ((int)((x)/8))
#define BITNUM(x)           ((x)-(BYTENUM(x)*8))

#define DEFAULT_TRANSACTION_NUMBER (0) 

//using namespace std;
class INTER_VARIANT;
class FunctionExpression;
class hCVar;// extern

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version

// stevev 10aug07 - added a bit-enum to indicate what and how dynamics should be handled
//		currently, only DISPLAY updates dynamics, ACKNOWLEDGE updates special (%#) indicated dynamic

//DDLInfoType enum is used by Profibus builtins GET_DD_REVISION, GET_DEVICE_REVISION, GET_DEVICE_TYPE and GET_MANUFACTURER
enum DDLInfoType
{
	GET_DD_REVISION		= 0,
	GET_DEVICE_REVISION	= 1,
	GET_DEVICE_TYPE		= 2,
	GET_MANUFACTURER	= 3,
};

//log type is used by HART and Profibus builtins LOG_MESSAGE, _ERROR, _WARNING, and _TRACE
enum logType
{
	LOG_ERROR_TYPE		= 0,
	LOG_WARNING_TYPE	= 1,
	LOG_TRACE_TYPE		= 2,
};

typedef struct strctDuration {
	
	double dSeconds;
	int iMinutes;
	int iHours;
	int iDays;

	strctDuration (){
		dSeconds = 0;
		iMinutes = 0;
		iHours=0;
		iDays = 0;
	}

} _DURATION;

/* error used in InvokeFunction() */
#define METHOD_ABORTED					(0xf3)	//abort builtin or UI builtin cancelled

#define BUILTIN_SUCCESS					0
#define BUILTIN_ABORTED					1
#define BUILTIN_COMM_ABORTED			2		//equivalent to BSEC_ABORTED and BSEC_FAIL_COMM plus masked
#define BUILTIN_RETRY_FAILED			3		//communication fails after retries.
#define BUILTIN_VAR_ABORTED				4		//variable access aborted due to false validity
#define BUILTIN_UI_CANCELLED			5
#define BUILTIN_COMM_NO_DEVICE			6		//equivalent to BSEC_NO_DEVICE
#define BUILTIN_COMM_FAIL_RESPONSE		7		//equivalent to BSEC_FAIL_RESPONSE

#define NORMAL_CMD		0
#define MORE_STATUS_CMD	1

typedef unsigned long ULONG;

class hCitemBase;
class hCddbDevice;
class CInterpreter;
class CValueVarient;
class OneMeth;//MEE;

enum SM_ABORT_OR_RETRY
{
	ABORT = 0,
	RETRY = 1
};

class CBuiltIn
{
protected:	// MTD 052407: changed access to protected to allow children access
	hCddbDevice *m_pDevice;
	CInterpreter *m_pInterpreter;
	OneMeth *m_pMeth;
	
	unsigned long ulResolveStatus;
	wchar_t *bltinErrorString[19];
	wchar_t *commErrorString[5];


	int SEND_COMMAND(
			
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int  iCmdType
			, bool bMoreDataFlag
			, int& iMoreInfoSize
			);

	/* This function returns true meaning command 48 abort or returns false meaning command 48 not abort if the last argument is set to ABORT.
	 * This function returns true meaning command 48 retry or returns false meaning command 48 not retry if the last argument is set to RETRY. */
	bool isCOMMAND48abortORretry(uchar* pchMoreDataStatus, uchar* pchMoreDataInfo, int iMoreInfoSize, SM_ABORT_OR_RETRY eCheck);
	
	int ProfibusSendCommand (unsigned long commandId, long *responseCode);

	void PackedASCIIToASCII (
								unsigned char *pbyPackedASCII
								, unsigned short wPackedASCIISize
								, char *pchASCII
							);
	void ASCIIToPackedASCII (
								char *pchASCII
		  					    , unsigned char *pbyPackedASCIIOutput
   						        , unsigned short *pwPackedASCIISize
	   					    );
/*Vibhor 081204: Start of Code*/
	//Adding this function to execute the actions on waveforms. Should only be called from PlotBuiltins
	int ExecuteWaveActions(actionType_t actionType,vector <hCitemBase*> wavePtrList);
/*Vibhor 081204: End of Code*/

/*Arun 170505 Start of code */
//	int is_leap_year(int year);
/*End of code*/

public:	
	CBuiltIn()
	: m_pDevice(NULL),m_pInterpreter(NULL),m_pMeth(0),ulResolveStatus(0)
	{
		wchar_t *builinErrorString[19] = {L"Success",
										L"Out of memory",
										L"Cannot find variable",
										L"Specified Item or Member ID does not exist",
										L"No Value to send to device",
										L"Wrong variable type for buitlin",
										L"Item does not have response codes",
										L"Invalid method ID",
										L"Supplied buffer for starting string is too small",
										L"Cannot read the specified variable",
										L"bad prompt string",
										L"No valid string for current language",
										L"A DDS error occurred",
										L"Response code received from the device caused builtin to fail",
										L"Communication error caused builtins to fail",
										L"Builtin function not yet implemented",
										L"Wrong item type for builtin",
										L"A CLASS LOCAL variable has not been intitialized before being used",
										L"Specified block does not exist within the device"
										};
		wchar_t *communicateErrorString[5] = {L"The Builtin completed successfully",
											L"An error occurred during the execution of the Builtin",
											L"The user aborted the task during the execution of the Builtin",
											L"There was no response from the device during the execution of the Builtin",
											L"A communication error occurred during the execution of the Builtin"
											};

		for (int i=0; i<19; i++)
		{
			bltinErrorString[i] = builinErrorString[i];
		}
		for (int i=0; i<5; i++)
		{
			commErrorString[i] = communicateErrorString[i];
		}
	}

	virtual ~CBuiltIn()
	{
	}
	
	///////////////////////////////////////////////////////////////////////////
	// ConstructChild					05/11/07	Mike DeCoster
	// 
	// allocates a new CBuiltIn derived class for the appropriate protocol
	//
	// Return:		CBuiltIn*		a newly constructed child class
	///////////////////////////////////////////////////////////////////////////
	static CBuiltIn* ConstructChild(ProtocolType type);

	///////////////////////////////////////////////////////////////////////////
	// BuildMapper						05/11/07	Mike DeCoster
	//
	// All child classes must implement this method to setup the CBuiltInMapper
	// class
	///////////////////////////////////////////////////////////////////////////
	virtual void BuildMapper() = 0;

	///////////////////////////////////////////////////////////////////////////
	// DebugLogParams
	//
	// Logs details of the method call to a text file
	///////////////////////////////////////////////////////////////////////////
	void DebugLogParams(
					char *pchFunctionName
					, int iNumberOfParameters
					, INTER_VARIANT *pVarParameters
					, INTER_VARIANT *pVarReturnValue
					, int	*pBuiltinReturnCode
					);

	void PreDebugLogParams(
				char *pchFunctionName
				, int iNumberOfParameters
				, INTER_VARIANT *pVarParameters
				, INTER_VARIANT *pVarReturnValue
				, int	*pBuiltinReturnCode
				);
	void PostDebugLogParams(
				char *pchFunctionName
				, int iNumberOfParameters
				, INTER_VARIANT *pVarParameters
				, INTER_VARIANT *pVarReturnValue
				, int	*pBuiltinReturnCode
				);
	///////////////////////////////////////////////////////////////////////////
	// DecodeVariantType
	//
	// map to a string name for the enum type 
	///////////////////////////////////////////////////////////////////////////
	const wchar_t* DecodeVariantType(VARIANT_TYPE type);


	hCddbDevice* getDevicePointer(){ return m_pDevice;}
	OneMeth* getOneMethPointer(){ return m_pMeth;}
	
	//public abort builtins
	virtual int abort(tchar *message);
	virtual int process_abort();

	//operations
	virtual bool Initialise(hCddbDevice *pDevice,CInterpreter *pInterpreter,OneMeth/*MEE*/ *pMeth)
	{
		if ((pDevice == NULL) || (pInterpreter == NULL) || (pMeth == NULL))
		{
			return false;
		}
		m_pDevice = pDevice;
		m_pInterpreter = pInterpreter;
		m_pMeth = pMeth;

		return true;
	}
	
	virtual bool InvokeFunction(
					char *pchFunctionName
					, int iNumberOfParameters
					, INTER_VARIANT *pVarParameters
					, INTER_VARIANT *pVarReturnValue
					, int	*pBuiltinReturnCode
					, FunctionExpression* pFuncExp = 0
					);

	/* Methods to convert a Variant array to a normal array */
	bool GetLongArray
					(
						INTER_VARIANT &varValue
						, long *plongArray
						, int  &iArraySize
					);

	bool GetULongArray
					(
						INTER_VARIANT &varValue
						, unsigned long *plongArray
						, int  &iArraySize
					);

	bool GetCharArray
					(
						INTER_VARIANT &varValue
						, char *pchArray
						, int  &iArraySize
					);

	bool SetCharArray
					(
						INTER_VARIANT &varValue
						, char *pchArray
						, int  aSize = 0 // stevev 30may07-0 is use-string length, non zero, use aSize
					);

	bool GetWCharArray
					(
						INTER_VARIANT &varValue
						, tchar *pchArray
						, int  &iArraySize
					);

	bool SetWCharArray
					(
						INTER_VARIANT &varValue
						, tchar *pchArray
						, int  aSize = 0 // stevev 30may07-0 is use-string length, non zero, use aSize
					);
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
/* <START>	List of built ins............ <START>	*/
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
protected:




/***** Start Delay builtins *****/
virtual long delayfor2
			(
				long iTimeInSeconds
				, tchar *pchDisplayString	/* DD_STRING */
				, unsigned long *pulBlkId
				, unsigned long *pulBlkNum
				, unsigned long *pulItemId
				, unsigned long *pulMemberId
				, long lNumberOfItemIds
			);

virtual int DELAY_TIME
		(
			int iTimeInSeconds
		);
/* End Delay builtins */


/***** Start Abort builtins *****/
virtual int _add_abort_method(long lMethodId);

virtual int _remove_abort_method(long lMethodId);

virtual void remove_all_abort();

/*Arun 190505 Start of code */
virtual int push_abort_method(long lMethodId);

virtual int pop_abort_method();

/*End of code*/

/* End Abort builtins */

/***** Start Display Message, Value, and Menu builtins *****/

/*Arun 190505 Start of code*/

virtual tchar* BUILD_MESSAGE
		(
			tchar* dest,
			int destStrSize,
			tchar* message
		);

/*End of code*/

virtual int PUT_MESSAGE
		(
			tchar *pchDisplayString
		);

virtual int put_message
		(
			tchar *pchDisplayString
			, long *plItemIds
			, unsigned long *plMemberIds
			, int	iNumberOfItemIds
		);

virtual long display_message2
		(
			tchar *pchDisplayString
			, unsigned long *pulBlkIds
			, unsigned long *pulBlkNums
			, unsigned long *pulItemIds
			, unsigned long *pulMemberIds
			, long	lNumberOfItemIds
		);

virtual int ACKNOWLEDGE
		(
			tchar *pchDisplayString
		);

virtual int acknowledge
		(
			tchar *pchDisplayString
			, long *plItemIds
			, unsigned long *plMemberIds
			, int	iNumberOfItemIds
		);

virtual long get_acknowledgement2 (tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums,
											unsigned long *pulItemIds, unsigned long *pulMemberIds, long lNumberOfItemIds );

virtual long display_dynamics2 (tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums,
											unsigned long *pulItemIds, unsigned long *pulMemberIds, long lNumberOfItemIds );

virtual long edit_device_value2(tchar *pchString, unsigned long *pULongBlkIds, unsigned long *pULongBlkNums,
								unsigned long *pULongItemIds, unsigned long *pULongMemberIds,
								long lNumberOfItemIds,  unsigned long ulBlkId,  unsigned long ulBlkNum,
								unsigned long ulItemId,  unsigned long ulMemberId);

virtual int edit_local_value2				
		(
			tchar *pchDisplayString, unsigned long *pulBlkIds, unsigned long *pulBlkNums
			, unsigned long *pulItemIds
			, unsigned long *pulMemberIds
			, long	lNumberOfItemIds
			, char *pchVariableName
		);

int get_tm(struct tm *pstTM, long date);
int get_tm_from_date_and_time(struct tm *pstTM, UINT64 dateTime);
int get_tm_from_time_value(struct tm *pstTM, INT64 time_value);
int get_tm_from_time(struct tm *pstTM, unsigned long long time_value);
int split_duration(UINT64 time_value, _DURATION &duration);
void convertUllTo8Char(unsigned long long const ullSrcVal, unsigned char *pDestChar, long length);	//this conversion is Endian independent
void convert8CharToUll(const unsigned char *pSrcChar, long length, unsigned long long &ullDestVal);	//this conversion is Endian independent

virtual int _display_xmtr_status
		(
			long lItemId
			, int  iStatusValue
		);

virtual int display_response_status
		(
			long lCommandNumber
			, int  iStatusValue
		);

virtual int display
		(
			tchar *pchDisplayString
			, long *plItemIds
			, int	iNumberOfItemIds
		);
	
virtual int select_from_list
		(
			tchar *pchDisplayString
			, long *plItemIds
			, int	iNumberOfItemIds
			, tchar *pchList
		);

/* End Abort Display Message, Value, and Menu builtins */

/***** Start Variable Access Builtins (Non-scaling) builtins *****/
/* Assign one item id to another */
virtual int _vassign
		(
			long lItemId1
			, long lItemId
		);

virtual int _dassign
		(
			long lItemId
			, double dValue
		);

virtual int _fassign
		(
			long lItemId
			, float fValue
		);

virtual int _iassign
		(
			long lItemId
                        , long long iValue
		);

virtual int _lassign
		(
			long lItemId
			, long long lValue
		);

virtual int _fvar_value
		(
			float& fOut,
			long lItemId
		);
	
virtual int _ivar_value
		(
                        INT64& llOut,
			long lItemId
		);
	
//sassign will assign the specified value to the DD VARIABLE.  
//The variable must be valid, and must reference a variable of type ASCII or PACKED_ASCII. If the new value is too small, the string variable will be padded with spaces. 
//If the new value is too large, the string will be truncated to fit.
//this builtin is NOT supported by FDI spec, but used in Hart
virtual int sassign(long lItemId, char* new_value);

virtual	ITEM_ID VerifyItemIdNGetProperty(const ITEM_ID ulBlockItemId, const unsigned long ulOccurrence, 
										 const ITEM_ID ulItemId, ITEM_ID ulMemberId, CStdString sProperty, /*out*/CValueVarient *pvtProperty);
virtual int GetPARAM_REF(const ITEM_ID ulBlockItemId, const unsigned long ulOccurrence, ITEM_ID ulItemId, ITEM_ID ulMemberId, /*out*/PARAM_REF *pOpRef);

virtual int AccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType);

virtual long send_on_exit();

virtual long save_on_exit();
	
//Added By Anil July 01 2005 --starts here
//Discards any DD Variable changes made in a method when the method exits. 
virtual int discard_on_exit();

//Added By Anil July 01 2005 --Ends here

/* End Variable Access Builtins (Non-scaling) builtins */


virtual unsigned long  resolve_group_reference(unsigned long dwItemId, unsigned long dwIndex, itemType_t typeCheck);



/***** Start Communications builtins *****/
virtual int get_more_status
		(
			  uchar *pchResponseCode
			, uchar *pchMoreStatusCode
			, int& moreInfoSize
		);
	
virtual int _get_status_code_string
		(
			long lItemId
			, int iStatusCode
			, tchar *pchStatusString
			, int iStatusStringLength
		);

/* End Communications builtins */

/***** Start Name to ID Translation builtins *****/

/*Arun 190505 Start of code*/

virtual int get_enum_string
		(
			long lItemId
			, int variable_value
			, tchar* status_string
			, int status_string_length
		);

/*End of code*/

virtual int _get_dictionary_string
		(
			long lItemId
			, tchar *pchDictionaryString
			, int iMaxStringLength
		);

// stevev 29jan08
int literal_string
		(
			long lItemId
			, tchar **pchDictionaryString			
		);

virtual unsigned long resolve_array_ref2	//for FF
		(
			unsigned long ulBlockId
			, unsigned long ulBlockInst
			, unsigned long ulItemId
			, unsigned long ulIndex
		);

virtual unsigned long resolve_record_ref2
		(
			unsigned long ulBlockId
			, unsigned long ulBlockInstance
			, unsigned long ulItemId
			, unsigned long ulIndex
		);

virtual long resolve_param_ref
		(
			long lItemId
		);

virtual int rspcode_string
		(
			int iCommandNumber
			, int iResponseCode
			, tchar *pchResponseCodeString
			, int iResponseCodeStringLength
			, nsEDDEngine::ProtocolType protocol
		);

/* End Name to ID Translation builtins */

/***** Start Scaling ABORT, IGNORE, RETRY builtins *****/
virtual int _set_comm_status
		(
			int iCommStatus
			, int iAbortIgnoreRetry
		);

virtual int _set_device_status
		(
			int iDeviceStatus
			, int iAbortIgnoreRetry
		);

virtual int _set_resp_code
		(
			int iResponseCode
			, int iAbortIgnoreRetry
		);

virtual int _set_all_resp_code
		(
			int iAbortIgnoreRetry
		);

virtual int _set_no_device
		(
			int iAbortIgnoreRetry
		);

virtual int SET_NUMBER_OF_RETRIES
		(
			int iNumberOfRetries
		);

/* End Scaling ABORT, IGNORE, RETRY builtins */

/***** Start XMTR ABORT, IGNORE, RETRY builtins *****/
virtual int _set_xmtr_comm_status
		(
			int iCommStatus
			, int iAbortIgnoreRetry
		);

virtual int _set_xmtr_device_status
		(
			int iDeviceStatus
			, int iAbortIgnoreRetry
		);

virtual int _set_xmtr_resp_code
		(
			int iResponseCode
			, int iAbortIgnoreRetry
		);

virtual int _set_xmtr_all_resp_code
		(
			int iAbortIgnoreRetry
		);

virtual int _set_xmtr_no_device
		(
			int iAbortIgnoreRetry
		);

virtual int _set_xmtr_all_data
		(
			int iAbortIgnoreRetry
		);

virtual int _set_xmtr_data
		(
			int iByteCode
			, int iBitMask
			, int iAbortIgnoreRetry

		);

/* End XMTR ABORT, IGNORE, RETRY builtins */


/***** Start Floating point builtins *****/

//float NaN_value();
virtual unsigned int NaN_value();

/* End Floating point builtins */

/***** Start Pre and Post actions builtins *****/
virtual int isetval(/*in, out*/long long& llValue);

virtual int lsetval(/*in, out*/int& lValue);

virtual int fsetval(/*in, out*/double& dValue);

virtual int fgetval(/*out*/double& dValue);

virtual int igetval(/*out*/INT64& llValue);

virtual int put_double(double dValue);
virtual long put_signed(long lValue);
virtual long put_unsigned(UINT32 uiValue);
virtual int put_date(const unsigned char* dateBuf, long size);

virtual long get_double(double *pdVal);
virtual long get_signed(long* lval);
virtual long get_unsigned(unsigned long* lval);
virtual long get_date(unsigned char *dateBuf, long *len);

virtual long put_string_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long id, unsigned long member_id, wchar_t* strval);
virtual long put_signed_value(unsigned long id, unsigned long member_id, long value);
virtual long put_unsigned_value(unsigned long id, unsigned long member_id, unsigned long value);
virtual long put_bitstring_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, _BYTE_STRING *pBitStr);

/*Arun 190505 Start of code*/

virtual int get_action_string
		(
			/*in*/FunctionExpression* pFuncExp
			, /*in*/int length
			, /*out*/INTER_VARIANT* pivString
		);

virtual int put_action_string
		(
			/*in*/INTER_VARIANT* pivValue
			, /*in*/int length
			, /*out*/wchar_t* outString
		);

virtual long assign(unsigned long uldst_BlockItemId, unsigned long uldst_Occurrence, unsigned long uldst_ItemId, unsigned long uldst_MemberId, unsigned long ulsrc_BlockItemId, unsigned long ulsrc_Occurrence, unsigned long ulsrc_ItemId, unsigned long ulsrc_MemberId);
virtual long get_date_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, unsigned char *stringval, long *len);
virtual long get_signed_value(unsigned long ulItemId, unsigned long ulMemberId, long *lValue);
virtual long get_unsigned_value(unsigned long ulItemId, unsigned long ulMemberId, unsigned long *ulValue);
virtual long get_string_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, wchar_t *stringval, long *length);
virtual long get_bitstring_value2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, _BYTE_STRING *pBitStr);

//genernal get/put variable value
virtual long get_variable_value(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue);
virtual long put_variable_value(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue);

/*End of code*/

/* End Pre and Post actions builtins */

/***** Start Command dispatching builtins *****/
virtual int send
		(
			int iCommandNumber
			, uchar *pchResponseStatus
		);

virtual int send_command
		(
			int iCommandNumber
		);

virtual int send_command_trans
		(
			int iCommandNumber
			, int iTransNumber
		);

virtual int send_trans
		(
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
		);

virtual int ext_send_command
		(
			int iCommandNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int& moreInfoSize
		);

virtual int ext_send_command_trans
		(
			int iCommandNumber
			, int iTransNumber
			, uchar *pchResponseStatus
			, uchar *pchMoreDataStatus
			, uchar *pchMoreDataInfo
			, int&  moreInfoSize
		);

virtual int tsend_command
		(
			int iCommandNumber
		);

virtual int tsend_command_trans
		(
			int iCommandNumber
			, int iTransNumber
		);

/* End Command dispatching builtins */
/* <END>  List of built ins................  <END> */


	/*	Functions that use the device object */
	virtual long bltin_format_string2 (/* out */ tchar *out_buf, /* in */ int max_length, /* in */ tchar *passed_prompt, 
						  /* in */ unsigned long *blk_ids, /* in */ unsigned long *blk_nums, 
						  /* in */ unsigned long *glob_var_ids, /* in */ unsigned long *glob_var_memberids, /* in */ long nCnt_Ids, /* in, out */ bool *bDyanmicVarvalChanged);
	virtual long doFormat (tchar* retStr, int rsLen, tchar* formatStr, int formatLen, INTER_VARIANT	*pSrcValue, hCVar *pVar, bool isDisplay);
									  //unsigned long blk_id, unsigned long blk_num, 
									  //unsigned long glob_var_id, unsigned long glob_var_memberid);

/*Arun 110505 Start of code*/

/****************Math support builtins (EDDL)*********************/

virtual double abs
		(
			double x
		);

virtual double acos
		(	
			double x
		);

virtual double asin
		(
			double x
		);

virtual double atan
		(
			double x
		);

virtual double cbrt
		(
			double x
		);

virtual double ceil
		(
			double x
		);
virtual double cos
		(	
			double x
		);

virtual double cosh
		(	
			double x
		);

virtual double exp
		(
			double x
		);

virtual double floor
		(
			double x
		);

virtual double fmod
		(
			double x,
			double y
		);
#ifdef XMTR
virtual double frand();
#endif
virtual double log
		(
			double x
		);

virtual double log10
		(
			double x
		);

virtual double log2
		(
			double x
		);

virtual double pow
		(	
			double x,
			double y
		);

virtual double round
		(
			double x
		);

virtual double sin
		(	
			double x
		);

virtual double sinh
		(
			double x
		);

virtual double sqrt
		(
			double x
		);

virtual double tan
		(
			double x
		);

virtual double tanh
		(
			double x
		);

virtual double trunc
		(
			double x
		);

virtual double drand ();
virtual void dseed (double x);

/****************End of Math support builtins (EDDL)*********************/

virtual double atof
		(
			char* string
		);

virtual int atoi
		(
			char* string
		);

virtual wchar_t* itoa
		(
			int value,
			wchar_t* string,
			int radix
		);


virtual wchar_t* ftoa
		(
			float value
		);

virtual double ByteToDouble (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4, 
	unsigned char byte5, unsigned char byte6, unsigned char byte7, unsigned char byte8);
virtual int DoubleToByte (double d,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4, 
	unsigned char *byte5, unsigned char *byte6, unsigned char *byte7, unsigned char *byte8);

virtual float ByteToFloat (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4); 
virtual int FloatToByte (float f,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4); 

virtual long ByteToLong (
	unsigned char byte1, unsigned char byte2, unsigned char byte3, unsigned char byte4); 
virtual int LongToByte (int l,
	unsigned char *byte1, unsigned char *byte2, unsigned char *byte3, unsigned char *byte4); 

virtual short ByteToShort (unsigned char byte1, unsigned char byte2);
virtual int ShortToByte (short s, unsigned char *byte1, unsigned char *byte2);


/*****************************End of Math Builtins (eDDL)************************/

/*End of code*/


/* Arun 160505 Start of code */

/****************************Date Time Builtins (eDDL) ***************************/

//virtual long YearMonthDay_to_Date - WS:EPM-Not a builtin 25jun07
//		(
//			int year,
//			int month,
//			int dayOfMonth
//		);

virtual int Date_to_Year
		(
			long days
		);

virtual int Date_to_Month
		(
			long days
		);

virtual int Date_to_DayOfMonth
		(
			long days
		);

virtual long DATE_to_days
		(
			long date1, long date0
		);

virtual long days_to_DATE
		(
			long days, long date0
		);

virtual long From_DATE_AND_TIME_VALUE
		(
			long date, unsigned long time_value
		);

virtual long From_TIME_VALUE
		(
			unsigned long time_value
		);
virtual long From_TIME_VALUE
		(
			unsigned long long time_value
		);

virtual long GetCurrentDate();

virtual long GetMICurrentTime();	//avoid using Microsoft function GetCurrentTime()

virtual long GetCurrentDateAndTime();

long get_utc_offset_inSec();

virtual UINT64 To_Date_and_Time(int days, int hour, int minute, int second, int millisecond);
			
/****************************End of Date Time Builtins (eDDL) ********************/

/****************************Start of DD_STRING  Builtins  (eDDL) ********************/

#define MAX_METHOD_LIST			20

//Find a substring.
virtual wchar_t* STRSTR(wchar_t*  string_var,wchar_t* substring_to_find);

//Convert a string to upper case.
//The strupr function converts, in place, each lowercase letter in string_var to uppercase.
virtual wchar_t* STRUPR(wchar_t* string_var);

//Convert a string to lower case.
//The STRLWR function converts, in place, each uppercase letter in string_var to lowercase.
virtual wchar_t* STRLWR(wchar_t* string_var);

//This function returns the number of characters in a string variable.
virtual int STRLEN(wchar_t* string_var);

//This function compares two strings lexicographically, and returns the relation as described below:
virtual int STRCMP(wchar_t* string_var1, wchar_t* string_var2);

//Remove any leading and trailing space characters from a string and return the new string.
virtual wchar_t* STRTRIM(wchar_t* string_var);

//Get a substring from a string specifying starting position and length.
virtual wchar_t* STRMID(wchar_t* string_var,int start, int len);

/*Vibhor 200905: Start of Code*/

virtual long ListInsert2(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId); //Insert an item in the list

virtual double get_double_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id);

virtual float get_float_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id);

virtual long get_string_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id, wchar_t* strval, int* length);

virtual long get_signed_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id);

virtual unsigned long get_unsigned_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id);

virtual long get_date_lelem(unsigned long list_id, unsigned long index, unsigned long element_id, unsigned long subelement_id, unsigned char* data, int* size);


/*Vibhor 200905: End of Code*/

//Anil September 26 2005 added MenuDisplay
virtual int _MenuDisplay(long lMenuId, tchar *pchOptionList, long* lselection);
virtual long select_from_menu				
		(
			tchar *pchDisplayString
			, unsigned long *plItemIds
			, unsigned long *plMemberIds
			, long	iNumberOfItemIds
			, tchar *pchOptionList, long* lselection
		);

virtual long select_from_menu2 (  tchar *pchDisplayString
								, unsigned long *pulBlkIds
								, unsigned long *pulBlkNums
								, unsigned long *pulItemIds
								, unsigned long *pulMemberIds
								, long	lNumberOfItemIds
								, tchar *pchOptionList, long* lselection );

virtual double DiffTime(long time_t1,long time_t0);

virtual long AddTime(long time_t, double lseconds);//WS:EPM - second param type to match spec 25jun07

virtual long Make_Time(int year,int month , int dayofmonth, int hour, int minute, int second , int isDST);

virtual long To_Time(long date, int hour, int minute, int second, int isDST);

virtual long Date_To_Time (long date);

virtual long To_Date(int Year, int month, int DayOfMonth);

virtual long Time_To_Date(long time_t);

virtual unsigned long seconds_to_TIME_VALUE(double seconds);
virtual unsigned long long seconds_to_TIME_VALUE8(double seconds);
virtual double TIME_VALUE_to_seconds (double time_value);

virtual int TIME_VALUE_to_Hour (double time_value);
virtual int TIME_VALUE_to_Minute (double time_value);
virtual int TIME_VALUE_to_Second (double time_value);

virtual int DATE_AND_TIME_VALUE_to_string (wchar_t* string, wchar_t* format, long date, unsigned long time_value);
virtual int DATE_to_string (wchar_t* date_str, wchar_t* format, long date);
virtual int TIME_VALUE_to_string (wchar_t* time_value_str, wchar_t* format, unsigned long time_value);
virtual int TIME_VALUE_to_string (wchar_t* time_value_str, wchar_t* format, unsigned long long time_value);
virtual int timet_to_string (wchar_t* timet_str, wchar_t* format, long timet_value);

virtual unsigned long timet_to_TIME_VALUE (long timet_value);
virtual unsigned long long timet_to_TIME_VALUE8 (long timet_value);
virtual unsigned long To_TIME_VALUE (int hours, int minutes, int seconds);
virtual unsigned long long To_TIME_VALUE8 (int year, int month, int day, int hour, int minute, int second);

virtual int fpclassify(double x);

virtual double nan(const char* nan_string);
virtual float nanf(const char* nan_string);

private:
	//An utility Function to get the Language Code
	void GetLanguageCode(char* szString, char* szLanguageCode, bool* bLangCodepresent);
	void GetLanguageCode(wchar_t* szString, wchar_t* szLanguageCode, bool* bLangCodepresent);

/* stevev 30may07 insert common code routines */
	bool GetStringParam(tchar* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber);
	bool GetByteStringParam(uchar* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber);
	bool GetCharStringParam(char* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber);

	bool GetUnsignedCharArrayParam(unsigned char* retString, int retStringLen,INTER_VARIANT *pParamArray, int paramNumber);

	bool SetStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, 
												    int paramNumber, tchar* paramString, bool bExpandString = false);
	// ByteString is NOT required to be null terminated and is L bytes long
	bool SetByteStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, 
																 int paramNumber, _BYTE_STRING& bsS);
	// CharString is used to null terminator or L length, which ever comes first
	bool SetCharStringParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, 
													 int paramNumber, char* paramString, int L);
	
	bool SetCharArrayParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, int paramNumber, unsigned char* param, int L);

	bool SetByteArrParam(FunctionExpression* pFuncExp, INTER_VARIANT *pParamArray, 
													 int paramNumber, char* paramString, int L);
	virtual bool 
		OutputParameterValue( FunctionExpression* pFuncExp, int nParamNumber, 
															 INTER_VARIANT &NewVarValue, int L);
	virtual bool OutputLiteralString( FunctionExpression* pFuncExp, int nParamNumber, 
												 INTER_VARIANT *pParam, wchar_t* pParamString );
/* stevev 30may07 - end */

	void FormatDouble(double dValue, char * strReturn);

/****************************End of DD_STRING  Builtins (eDDL) **********************************/
	virtual bool doFormat(tchar* formatStr, int formatLen, INTER_VARIANT& ivValue, tchar* retStr, int rsLen, hCVar *pVar);

	BI_ErrorCode ConvertToBICode(int errorCode);
	BLTIN_ErrorCode ConvertToBLTINCode(int errorCode);

	virtual int get_response_code_string
		(
			int iItemId
			, int iMemberId
			, int iRsponseCode
			, tchar *pchResponseCodeString
			, int iResponseCodeStringLength
		);
	virtual int get_dds_error(tchar* pchErrorString, int iMaxLength);

	virtual long display_builtin_error(long errorCode);
	virtual long display_comm_error(unsigned long errorCode);
	virtual unsigned long get_comm_error();
	virtual long get_comm_error_string(unsigned long ulErrorCode, tchar* pchErrorString, long lMaxStringLength);


    virtual long is_NaN(double dReturnValue);
    virtual long NaN_value(double *value);

	virtual long get_response_code(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId);
	virtual long AccessDeviceValue(/* int */ unsigned long ulBlockItemId, /* int */ unsigned long iOccurrence, /* int */ unsigned long ulItemId, /* int */ unsigned long ulMemberId, /* int */ AccessType iAccessType);
	virtual long set_ff_comm_status(unsigned long ulCommStatus, int iAbortIgnoreRetry);
	virtual long set_ff_resp_code(unsigned long ulResponseCode, int iAbortIgnoreRetry);
	virtual int set_ff_all_comm_status(int iAbortIgnoreRetry);
	virtual int set_ff_all_resp_code(int iAbortIgnoreRetry);
	virtual ITEM_ID resolve_block_ref(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType);
	virtual unsigned long resolve_list_ref(unsigned long  ulItemId);
	virtual long put_date_value(unsigned long ulItemId, unsigned long ulMemberId, unsigned char* pchVal, long lLength);
	virtual unsigned long get_resolve_status();
	virtual long get_status_string( unsigned long ulItemId, unsigned long ulMemberId, unsigned long ulStatusCode, tchar *pchStatusString, long lMaxLength);
	virtual long AccessTypeValue2(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, CValueVarient *pvtValue, AccessType eAccessType);
	virtual long get_block_instance_count(unsigned long ulBlockId, unsigned long *pulCount);
	virtual int get_block_instance_by_object_index(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex);
	virtual int get_block_instance_by_block_tag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex);
	
	virtual long get_date_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, unsigned char *pchData, int* iSize);
	virtual double get_double_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId);
	virtual float get_float_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId);
	virtual long get_signed_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId);
	virtual long get_string_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, tchar *pchData, int* iSize);
	virtual unsigned long get_unsigned_lelem2(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId);
	virtual void LOG_MESSAGE(int priorityVal, wchar_t *msg);
	virtual int isOffline();
	virtual int ListDeleteElementAt2(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, int iCount);

	virtual int GET_DDL(DDLInfoType ddlType);
	virtual int get_variable_string(unsigned long ulItemId, wchar_t *pValueDisp, unsigned int iValueDispLen);
	virtual int display_bitenum (unsigned long ulItemId);

	/*FF builtins */
	virtual double get_sel_double2(unsigned long ulItemId, int iIndex, unsigned long ulEmbededId, unsigned long ulSelector, unsigned long ulAdditional);
	virtual long get_sel_string(char* pchString, unsigned long ulItemId, unsigned long ulSelector, unsigned long ulAdditional, long *length);

	// FF Builtin
	// Builtin resolve_selector_ref() is obsolete for FDI. But keep it for legacy FF.
	virtual unsigned long resolve_selector_ref(unsigned long ulItem_Id, unsigned long ulIndex, unsigned long ulSelector);

	virtual void MethodDebugMessage(unsigned long ulLineNo);
};

#endif /*HART_BUILTINS_H*/
