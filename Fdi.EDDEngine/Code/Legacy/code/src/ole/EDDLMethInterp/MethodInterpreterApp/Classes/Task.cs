using System.Collections.Generic;

namespace MethodInterpreterApp.Classes
{
    public class Task
    {
        private List<WorkItem> _workitems;

        public List<WorkItem> WorkItems
        {
            get { return _workitems; }
            set { _workitems = value; }
        }
    }
}
