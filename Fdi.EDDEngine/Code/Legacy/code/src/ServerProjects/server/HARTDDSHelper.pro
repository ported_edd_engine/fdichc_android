#-------------------------------------------------
#
# Project created by QtCreator 2017-11-30T17:31:30
#
#-------------------------------------------------

QT       -= gui

TARGET = HARTDDSHelper
TEMPLATE = lib
CONFIG += staticlib
#CONFIG +=plugin
DEFINES += HARTDDSHELPER_LIBRARY \
           UNICODE
#QMAKE_CFLAGS += -std= c++11 -Wno-narrowing
QMAKESPEC=linux-g++-32
QMAKE_CXXFLAGS += -std=gnu++11
QMAKE_CXXFLAGS += -std=c++11 -Wno-narrowing #todo
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/AmsFileReader.cpp \
   src/cm_comm.cpp \
   src/cm_dds.cpp \
   src/cm_init.cpp \
   src/cm_rod.cpp \
   src/cm_tbl.cpp \
   src/cmtblacc.cpp \
   src/ConnectionMgr.cpp \
   src/dev_type.cpp \
   src/DeviceTypeMgr.cpp \
   src/ff_sup.cpp \
   src/rmalloc.cpp \
   src/sm_strfn.cpp \
   src/StandardDictionary.cpp \
   ../NtcSpecies/EddEngineLog.cpp \
   ../NtcSpecies/NtcSpeciesLog.cpp \

HEADERS += ../../../inc/amsversion.h\
    ../../../inc/CommonLibraries/AmsFileReader/AmsFileReader.h\
    ../../../inc/Inf/EddEngineLog.h \
    ../../../inc/Inf/NtcSpecies/BssProgLog.h \
    ../../../inc/Inf/NtcSpecies/ntcassert.h \
    ../../../inc/Inf/NtcSpecies/Ntcassert2.h \
    ../../../inc/ServerProjects/DeviceIdent.h \
    ../../../inc/ServerProjects/fmscrypt.h \
    ../../../inc/ServerProjects/hartbreaker.h \
    ../../../inc/ServerProjects/rtn_code.h \
    ../../../inc/ServerProjects/ServerRes.h \
    ../../../inc/ServerProjects/std.h \
    ../../../inc/ServerProjects/tx_hart.h \
    interp/stdinc.h \
   src/cm_dds.h \
   src/cm_loc.h \
   src/cm_rod.h \
   src/cm_struc.h \
   src/ConnectionMgr.h \
   src/dev_type.h \
   src/DeviceTypeMgr.h \
   src/langspec.h \
   src/map_dict.h \
   src/rmalloc.h \
   src/RodMgr.h \
   src/StandardDictionary.h \
   src/sym.h \
   src/sysproto.h \

unix {
    target.path = /usr/lib
    INSTALLS += target
}
DESTDIR = ../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/src/
INCLUDEPATH += $$PWD/../../../../../Inc/
INCLUDEPATH += $$PWD/../../../inc/ole/
INCLUDEPATH += $$PWD/../../../inc/ServerProjects/
INCLUDEPATH += $$PWD/../../../inc/ServerProjects/DDS/
INCLUDEPATH += $$PWD/../../../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../inc/
INCLUDEPATH += $$PWD/hrt_cstk/
INCLUDEPATH += $$PWD/interp/
INCLUDEPATH += $$PWD/../hartpt/share/
INCLUDEPATH += $$PWD/../hartpt/common/
INCLUDEPATH += $$PWD/DeviceConnectorService/
INCLUDEPATH += $$PWD/../../../inc/ServerProjects/server/DeviceConnectorService/
INCLUDEPATH += $$PWD/DeviceConnectorService/ConnectionManager/
INCLUDEPATH += $$PWD/../../../inc/dbwraps/
INCLUDEPATH += $$PWD/../../../inc/workqueue/
INCLUDEPATH += $$PWD/../../../inc/ServerProjects/hartpt/
INCLUDEPATH += $$PWD/../../idl/
INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Android
    LIBS += -L$$DESTDIR -lEDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Linux
    unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Linux
}

#unix:!macx: LIBS += -L$$PWD/../../../../../build/debug/ -lFDI_DDS_AdapterLib
#unix:!macx: LIBS += -L$$PWD/../../../../../build/debug/ -lddsHart

INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR

unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common

DEPENDPATH += $$DESTDIR/EDD_Engine_Common

unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a

