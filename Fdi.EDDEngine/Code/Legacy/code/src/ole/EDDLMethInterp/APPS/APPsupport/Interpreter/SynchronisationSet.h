
#include "ParserDeclarations.h"
#include <memory>


enum PRODUCTION:int
{
	EXPRESSION=0,
	DECLARATION=1,
	ASSIGNMENT=2,
	IF_SELECTION=3,
	ITERATION=4,
	COMPOUND_STMT=5,
	STMT_LIST=6
};
#define PRODUCTION_COUNT	7


struct FOLLOW_ELEMENT
{
	FOLLOW_ELEMENT(	RUL_TOKEN_TYPE	rhsType,
							   RUL_TOKEN_SUBTYPE	rhsSubType);
	RUL_TOKEN_TYPE		Type;
	RUL_TOKEN_SUBTYPE	SubType;
};

typedef vector<FOLLOW_ELEMENT>	FOLLOW_VECTOR;

struct FOLLOWS
{
	PRODUCTION			production;
	FOLLOW_VECTOR	set;
};

//Singleton class;
class FOLLOW_SET
{
public:
	static bool	IsPresent(
		PRODUCTION production,
		RUL_TOKEN_TYPE Type,
		RUL_TOKEN_SUBTYPE SubType);

private:
	FOLLOW_SET();
	static FOLLOWS		follows[PRODUCTION_COUNT];
};
