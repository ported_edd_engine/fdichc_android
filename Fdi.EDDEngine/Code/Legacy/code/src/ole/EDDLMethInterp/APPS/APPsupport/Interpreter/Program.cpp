// Program.cpp //

#include "stdafx.h"
#include "Program.h"
#include "Declarations.h"
#include "StatementList.h"
#include "GrammarNodeVisitor.h"
#include "MEE.h"
#include "ErrorDefinitions.h"


CProgram::CProgram()
{
	m_pDeclarations = 0;
	m_pStmtList = 0;
}

CProgram::~CProgram()
{
	DELETE_PTR(m_pDeclarations);
	DELETE_PTR(m_pStmtList);
}
//CJK DP GETS INTO HERE REDO from CPARSEr execute
_INT32 CProgram::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	_INT32 nReturn =  pVisitor->visitProgram(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2

	return nReturn;
}

_INT32 CProgram::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	_INT32 i32Ret = 0;
	CToken* pToken=0;
	try
	{
		//check if process is cancelled
		MEE *pMEE = plexAnal->GetMEEInterface();
		if (pMEE && (pMEE->IsMethodCancelled()))
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}

		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			||(pToken->GetSubType() != RUL_LBRACK))
		{
			DELETE_PTR(pToken);//clean up memory even on errors
			throw(C_UM_ERROR_MISSINGTOKEN);
		}
		DELETE_PTR(pToken);

		//mark method parser starting
		plexAnal->m_nIndentIndex++;

		m_pDeclarations	= new CDeclarations;
		i32Ret = m_pDeclarations->CreateParseSubTree(
			plexAnal,
			pSymbolTable,
			pvecErrors);

		m_pStmtList = new CStatementList;
		i32Ret = m_pStmtList->CreateParseSubTree(
			plexAnal,
			pSymbolTable,
			pvecErrors);

		if (PARSE_FAIL == i32Ret)
		{
			if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
				|| !pToken
				||(pToken->GetSubType() != RUL_RBRACK))
			{
				
				DELETE_PTR(pToken);//clean up memory, even on errors
				throw(C_UM_ERROR_MISSINGTOKEN);
				return i32Ret;
			}
			else
			{
				DELETE_PTR(pToken);
				i32Ret = PARSE_SUCCESS;			
			}
		}

		//check if process is cancelled
		if (pMEE && (pMEE->IsMethodCancelled()))
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}

	}
	catch(_INT32 error)
	{
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}
	return i32Ret;
}

void CProgram::Identify(
		_CHAR* szData)
{
	strcat(szData,"<Program>");
	m_pDeclarations->Identify(szData);
	m_pStmtList->Identify(szData);
	strcat(szData,"</Program>");
}

CDeclarations* CProgram::GetDeclarations()
{
	return m_pDeclarations;
}

CStatementList* CProgram::GetStatementList()
{
	return m_pStmtList;
}

_INT32 CProgram::GetLineNumber()
{
	if (m_pStmtList)
	{
		return m_pStmtList->GetLineNumber();
	}
	else
	{
		return 0;
	}
}
