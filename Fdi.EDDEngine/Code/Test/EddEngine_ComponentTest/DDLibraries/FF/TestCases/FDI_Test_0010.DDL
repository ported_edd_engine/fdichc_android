


MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0010, DEVICE_REVISION 1, DD_REVISION 1

/****************************************************************************** */
/* Menu structure                                                               */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    ITEMS
   {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU main_test_menu
{
    LABEL "Main test menu";
    ITEMS    
    {
        test_block[0].PARAM.BITENUM_TEST,
        test_block[0].PARAM.BITENUM_TEST [0x03],
        test_block[0].PARAM.BITENUM_TEST [0xFC]
    }
}

/****************************************************************************** */
/* Block definitions                                                            */
/****************************************************************************** */

BLOCK test_block
{
    CHARACTERISTICS test_block_characteristics ;
    LABEL "Test Block" ;
    HELP "Help Test Block" ;
        
    PARAMETERS
    {
        BITENUM_TEST, test_var_1;
    }
    
}

RECORD test_block_characteristics
{
    LABEL           "Label test block character" ;
    HELP             "help test block character";
    MEMBERS
    {
        TEST_VAR_1_CHARACTERISTICS, test_var_1;
    }
}

VARIABLE test_var_1
{
    LABEL "Test Variable 1";
    HELP "A bit enumerated variable for bitmask test";
    TYPE BIT_ENUMERATED (1)
    {
        {0x01, "Enable option 1"},
        {0x02, "Enable option 2"},
        {0x04, "Enable option 3"},
        {0x08, "Enable option 4"},
        {0x10, "Enable option 5"},
        {0x20, "Enable option 6"},
        {0x40, "Enable option 7"},
        {0x80, "Enable option 8"}
    }
    CLASS CONTAINED;
}

