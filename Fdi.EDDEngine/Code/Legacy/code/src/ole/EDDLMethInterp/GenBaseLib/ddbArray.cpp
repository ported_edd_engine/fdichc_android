/*************************************************************************************************
 *
 * $Workfile: ddbArray.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2004, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		08/30/04	sjv	started creation
 */
// #define MEMSIZE_CHK 1

#include "stdafx.h"
#include "ddbArray.h"
#include "ddbGrpItmDesc.h"
#include "ddbDevice.h"
#include <CMethodInterpreterDevice.h>
#include "logging.h"
#include <3rdParty/panic.h>
#include "varientTag.h"

#ifdef MEMSIZE_CHK
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

#include "..\..\SDC625\stdafx.h"

#if 0
#include <crtdbg.h>
#include <winnt.h>
#include <afx.h>
#endif
#undef TRUE
#define TRUE "use true"
#undef FALSE
#define FALSE "use false"
#endif

//
//
//
hCarray::hCarray(DevInfcHandle_t h, aCitemBase* paItemBase) :  hCitemBase(h,paItemBase)
{
	//get value array count
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	CValueVarient vtValue{};
	int iRetVal = pDevice->ReadParameterData2( &vtValue, _T("NUMBEROFELEMENTS"), 0, 0, this->itemId, 0 );
	if( iRetVal == SUCCESS ) 
	{
		
		m_ulCount = CV_UI4(&vtValue);
	}
	else
	{
		m_ulCount = 0;
	}
}
		 
RETURNCODE hCarray::getAllByIndex(UINT32 indexValue, HreferenceList_t& returnedItemRefs)
{
	RETURNCODE rc = FAILURE;
	return rc;
}

RETURNCODE hCarray::getAllindexValues(vector<UINT32>& allValidindexes)
{
	return FAILURE;
}

RETURNCODE hCarray::Label(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadValueArrayData( itemId, _T("Label"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

RETURNCODE hCarray::Help(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadValueArrayData( itemId, _T("Help"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}

//Note: The input argument indexValue is a 0 based index of value array.
RETURNCODE hCarray::getByIndex(UINT32 indexValue,hCgroupItemDescriptor** ppGID,bool suppress)
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;

	if ( ppGID == NULL )
	{	
		return rc;	   
	}
	else
	{	pGid   = new hCgroupItemDescriptor(devHndl());
		if ( *ppGID != NULL )	
		{
			delete *ppGID;
		}
		*ppGID = pGid;
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		//Value array index is always 1 based in the MI data process.
		//Since the input index is 0 based, add 1 to the index here.
		hCreference locRef( devHndl() );
		locRef.setRef( itemId, indexValue + 1, iT_Variable, false, rT_Item_id);
		(*pGid) = locRef;
		pGid->setIndex(indexValue + 1);
		rc = SUCCESS;
	}
	
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

hCitemBase* hCarray::operator[](int indexValue)
{
	hCitemBase* pRetVal = NULL;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		(void)pDevice->getElementOrMemberPointer(itemId, indexValue, &pRetVal);
	}
	return pRetVal;
}

RETURNCODE hCarray::destroy(void)
{ 
	return SUCCESS;
}
	


