#pragma once

//#include <Dictionary.h>
#include <CMethodInterpreterDevice.h>

class CMethodInterpreterDevice;



class CServerDictionary : public CDictionary
{

	private:
		CMethodInterpreterDevice *m_pDevice;
	public:
		CServerDictionary();//HART
		CServerDictionary( CMethodInterpreterDevice *pDevice );//Profi
		virtual ~CServerDictionary();

	public:	
		// This version to be used by DD_Parser
		virtual int get_dictionary_string( unsigned long index, STRING *str );
		virtual int get_dictionary_string(char* chStringKey , STRING *str);
		// This one to be used by Builtin Library & SDC
		virtual int get_dictionary_string( unsigned long index, wchar_t* str, int iStringLen );
		virtual int get_dictionary_string(wchar_t* chStringKey ,  wchar_t* str, int iStringLen );
		virtual int get_lit_string( unsigned long index, wchar_t* str, long lStringLen );
};