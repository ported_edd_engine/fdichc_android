#ifndef lint
static char SCCSID[] = "@(#)ddi_dir.c	30.5  30  21 Apr 1997";
#endif /* lint */

/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */
/**
 *		Copyright 1995 - HART Communication Foundation
 *		All rights reserved.
 */

#ifdef SUN
#include <memory.h>     /* K&R */
#else
#include <string.h>     /* ANSI */
#endif /* SUN */
#include "stdinc.h"
#include "std.h"
#include "APP_XMAL.H"
#include "evl_lib.h"
#include "DDI_TAB.H"
#include "fch_lib.h"
#include "panic.h"
#include "FF/FFEDDEngine/DDSSupport.h"
/*
 *	DEVICE_DIRECTORY_OFFSET really does not belong here.
 *	It will be moved when the FLAT table interface is implemented.
 */

#define DEVICE_DIRECTORY_OFFSET	0
#define OP_REF_LIST_INCR	20	/* Increment size for OP_REF_LIST array */
/*********************************************************************
 *  Name: 	ddi_get_device_dd_handle
 *  ShortDesc: 	obtains dd_handle and dd_reference for device directory requests.
 *
 *  Description:
 *		This routine is responsible for obtaining dd_handle and dd_reference, from
 *		the ddi tables.  DD_HANDLE and DD_REF are needed to get directory information
 *		from EVAL and FETCH.
 *
 *  Inputs:
 *      req - ptr to a DDI_DEVICE_DIR_REQUEST.
 *
 *  Outputs:
 *		dd_handle - ptr to a DD_HANDLE.
 *      ref	- ptr to a DD_REFERENCE.
 *
 *  Returns:
 *      DDS_SUCCESS,
 * 		DDI_INVALID_DEV_TYPE_HANDLE;
 *		DDI_INVALID_DEVICE_HANDLE;
 * 		DDI_INVALID_REQUEST_TYPE;
 *		DDI_BAD_DD_DEVICE_LOAD;
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
static int
ddi_get_device_dd_handle(ENV_INFO *env_info, DDI_DEVICE_DIR_REQUEST *req, ROD_HANDLE *rod_handle,
	DD_REFERENCE *ref)
{

	int		rc = DDS_SUCCESS;	/* return code */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 * If DD_REFERENCE is not passed in, this routine knows where to get
	 * DD_REF from
	 */

	switch (req->type) {
		case DD_DD_HANDLE:

			/*
			 * DEVICE_DIR request by DD_REFERENCE and DD_HANDLE
			 */

			*rod_handle = req->spec.ref.rod_handle;

			ref->object_index = req->spec.ref.ref.object_index;

			rc = DDS_SUCCESS;
			break;

		case DD_DT_HANDLE:

			/*
			 * DEVICE_DIR request by DEVICE_TYPE_HANDLE device_type_handle
			 * = index into ACTIVE_DEVICE_TYPE_TABLE
			 */

			if (!g_DeviceTypeMgr.valid_device_type_handle(req->spec.device_type_handle)) {
				return DDI_INVALID_DEV_TYPE_HANDLE;
			}

			rc = g_DeviceTypeMgr.get_adtt_dd_handle(req->spec.device_type_handle, 
					rod_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			ref->object_index = (OBJECT_INDEX) DEVICE_DIRECTORY_INDEX;

			rc = DDS_SUCCESS;
			break;
	
		case DD_DEVICE_HANDLE:

			/*
			 * DEVICE_DIR request by DEVICE_HANDLE device_handle is an
			 * index into the "ACTIVE_DEVICE_TABLE"
			 */

			if (!pConnMgr->valid_device_handle(req->spec.device_handle)) {
				return DDI_INVALID_DEVICE_HANDLE;
			}

			rc = pConnMgr->get_adt_dd_handle(req->spec.device_handle, 
					rod_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (!pConnMgr->valid_dd_handle(*rod_handle)) {

				/*
				 * This device is currently not in the tables. Load the
				 * device tables. ds_dd_device_load() returns dd_handle)
				 */

				rc = pConnMgr->ds_dd_device_load(env_info, req->spec.device_handle, rod_handle);
				if (rc) {
					return DDI_BAD_DD_DEVICE_LOAD;
				}
			}

			/*
			 * dd is loaded.
			 */

			ref->object_index = (OBJECT_INDEX) DEVICE_DIRECTORY_INDEX;

			rc = DDS_SUCCESS;
			break;

		default:
			rc = DDI_INVALID_REQUEST_TYPE;
			break;
	}

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_get_block_dd_handle
 *  ShortDesc: 	obtains dd_handle and dd_reference for block directory requests.
 *
 *  Description:
 *		This routine is responsible for obtaining dd_handle and dd_reference, from
 *		the ddi tables.  DD_HANDLE and DD_REF are needed to get directory information
 *		from EVAL and FETCH.
 *
 *  Inputs:
 *      req - ptr to a DDI_BLOCK_DIR_REQUEST.
 *
 *  Outputs:
 *		dd_handle - ptr to a DD_HANDLE.
 *      ref	- ptr to a DD_REFERENCE.
 *
 *  Returns:
 *      DDS_SUCCESS,
 * 		DDI_INVALID_DEV_TYPE_HANDLE;
 * 		DDI_INVALID_BLOCK_HANDLE;
 *		DDI_INVALID_REQUEST_TYPE;
 *		DDI_BAD_DD_BLOCK_LOAD;
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
static int
ddi_get_block_dd_handle(ENV_INFO *env_info, DDI_BLOCK_DIR_REQUEST *req, ROD_HANDLE *rod_handle,
	DD_REFERENCE *ref)
{

	int             rc = DDS_SUCCESS;	/* return code */
	int				blk_tbl_offset ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();
	/*
	 * If DD_REFERENCE is not passed in, this routine knows where to get
	 * DD_REF from
	 */

	switch( req->type ) {
		case BD_DD_HANDLE:

			/*
			 * BLOCK_DIR request by DD_REFERENCE and DD_HANDLE
			 */

			*rod_handle = req->spec.ref.rod_handle;

			ref->object_index = req->spec.ref.ref.object_index;
	
			rc = DDS_SUCCESS;
			break;
	
		case BD_BLOCKNAME:

			/*
			 * BLOCK_DIR request by DEVICE_TYPE_HANDLE BLOCKNAME
			 */

			if (!g_DeviceTypeMgr.valid_device_type_handle(req->spec.block.dt_handle)) {
				return DDI_INVALID_DEV_TYPE_HANDLE;
			}

			rc = g_DeviceTypeMgr.get_adtt_dd_handle(req->spec.block.dt_handle, 
					rod_handle) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			/*
			 * pass in a device_type_handle and a blockname and get back a
			 * dd_ref.
			 */

			rc = tr_gdr_device_type_handle_blockname(env_info,
				req->spec.block.dt_handle, req->spec.block.blockname, ref);

			break;
	
		case BD_BLOCK_HANDLE:

			/*
			 * BLOCK request by BLOCK_HANDLE
			 */

			if (!pConnMgr->valid_block_handle(req->spec.block_handle)) {
				return DDI_INVALID_BLOCK_HANDLE;
			}

			/*
			 * If the Block_name_table_offset for this block_handle is
			 * invalid (ie < 0 ), The block table information has not been
			 * loaded from the dd.  Load the block table info.  We should
			 * now have a valid dd_handle.
			 */

			rc = pConnMgr->get_abt_dd_blk_tbl_offset(req->spec.block_handle,
					&blk_tbl_offset) ;
			if (rc != SUCCESS) {
				return(rc) ;
			}

			if (blk_tbl_offset < 0) {
				rc = pConnMgr->ds_dd_block_load(env_info, req->spec.block_handle, rod_handle);
				if (rc) {
					return DDI_BAD_DD_BLOCK_LOAD;
				}
			}
			else {
				rc = pConnMgr->get_abt_dd_handle(req->spec.block_handle, 
						rod_handle) ;
				if (rc != SUCCESS) {
					return(rc) ;
				}
			}

			/*
			 * dd is loaded. get dd_ref using block_handle.
			 */

			rc = tr_gdr_block_handle(env_info, req->spec.block_handle, ref);
			break;

		default:
			rc = DDI_INVALID_REQUEST_TYPE;
			break;
	}

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_device_dir_request
 *  ShortDesc: 	Returns the desired device directories
 *
 *  Description:
 *		  This routine is a DDI interface routine which allows ddi users to get
 *		values from the device tables.
 *		  If this is a ROD request, a call to  fetch is made to get the desired
 *		chunks of device directory binary.  Eval is then called to evaluate the
 *		binary chunks.
 *		  If this is a FLAT request, the device tables are already evaluated.
 *		A call to fetch is made to get the desired device tables.
 *
 *  Inputs:
 *		dir_req - ptr to a DDI_DEVICE_DIR_REQUEST
 *
 *  Outputs:
 *		dir_resp - ptr to FLAT_DEVICE_DIR structure containing the desired
 *			device table values.
 *
 *  Returns:
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_device_dir_request(ENV_INFO *env_info, DDI_DEVICE_DIR_REQUEST *dir_req, FLAT_DEVICE_DIR *dir_resp)
{

	int             rc = 0;			/* return code */
	ROD_HANDLE      rod_handle = 0;	/* remote object dictionary handle */

	DD_REFERENCE    dd_ref;		/* device_reference */
	SCRATCH_PAD     s_pad;		/* storage area for binary chunks */
	BIN_DEVICE_DIR  device_dir_bin;	/* list of ptrs to binary chunks */
	unsigned long   s_pad_size;	/* size to make scratch_pad */
	unsigned short  dr_type;	/* directory request type */

	ASSERT_RET(dir_req && dir_resp, DDI_INVALID_PARAM);


	s_pad.size = 0;
	s_pad.used = 0;
	s_pad.pad = NULL;
	(void)memset((char *) &device_dir_bin, 0, sizeof(BIN_DEVICE_DIR));
	s_pad_size = 0;
	dr_type = DEVICE_DIR_TYPE;

	rc = ddi_get_device_dd_handle(env_info, dir_req, &rod_handle, &dd_ref);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	/*
		get binary chunks from fetch.
		call eval to evaluate binary chunks.

		Send fetch_request to determine the size of scratchpad needed.
	*/

	rc = fch_rod_dir_spad_size( env_info,  rod_handle,
		dd_ref.object_index, &s_pad_size, dir_req->mask, dr_type);
	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	s_pad.pad = (unsigned char *)xmalloc((size_t) s_pad_size);
	if (s_pad.pad == NULL) 
	{
		return DDL_MEMORY_ERROR;
	}
	s_pad.size = s_pad_size;

	/*
		* Send second fetch_request to get the chunks of binary to be
		* evaluated (ie. get a value). Fetch people say s_pad_size and
		* s_pad->used must be zeroed out.
		*/

	s_pad_size = 0;
	s_pad.used = 0;
	rc = fch_rod_device_dir(env_info,  rod_handle,
		&s_pad, &s_pad_size, dir_req->mask, &device_dir_bin);

	/*
		* If s_pad_size > 0, we should not be returning DDS_SUCCESS.  I am
		* assuming rod_fetch_dir() will not be returning DDS_SUCCESS in this
		* instance.  Therefore, I am not checking the size of s_pad_size
		* explicitly.
		*/

	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	rc = eval_dir_device_tables(dir_resp, &device_dir_bin, dir_req->mask);
	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	/*
		* Free the memory malloc'd for scratch_pad.
		*/

	if (s_pad.pad) 
	{
		xfree((void **)&s_pad.pad);
	}
	
	return rc;
}


/*********************************************************************
 *  Name: 	ddi_block_dir_request
 *  ShortDesc: 	Returns the desired block directories
 *
 *  Description:
 *		This routine is a DDI interface routine which allows ddi users to get
 *		values from the block tables.  This function calls fetch to get the
 *		desired chunks of block directory binary.  It then calls eval to get
 *		the values of these block tables desired by the user.
 *
 *  Inputs:
 *		dir_req - ptr to a DDI_BLOCK_DIR_REQUEST
 *
 *  Outputs:
 *		dir_resp - ptr to FLAT_BLOCK_DIR structure containing the desired
 *			block table values.
 *
 *  Returns:
 *		return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_block_dir_request(ENV_INFO *env_info,DDI_BLOCK_DIR_REQUEST *dir_req, FLAT_BLOCK_DIR *dir_resp)
{

	int             rc;			/* return code */
	ROD_HANDLE      rod_handle;	/* remote object dictionary handle */
	DD_REFERENCE    dd_ref;		/* device_reference */
	SCRATCH_PAD     s_pad;		/* storage area for binary chunks */
	BIN_BLOCK_DIR   block_dir_bin;	/* list of ptrs to binary chunks */
	unsigned long   s_pad_size;	/* size to make scratch_pad */
	unsigned short  dr_type;	/* directory request type */

	ASSERT_RET(dir_req && dir_resp, DDI_INVALID_PARAM);


	s_pad.size = 0;
	s_pad.used = 0;
	s_pad.pad = NULL;
	(void)memset((char *) &block_dir_bin, 0, sizeof(BIN_BLOCK_DIR));
	s_pad_size = 0;
	dr_type = BLOCK_DIR_TYPE;

	rc = ddi_get_block_dd_handle(env_info, dir_req, &rod_handle, &dd_ref);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	/*
	 * If we have a ROD type handle, get binary chunks from fetch, and call
	 * eval to evaluate binary chunks.  If we have a FLAT type handle (the
	 * tables are already evaluated), get table information from fetch, and
	 * do NOT call eval.
	 */


	/*
		* Send fetch_request to determine the size of scratchpad needed.
		*/

	rc = fch_rod_dir_spad_size(env_info,  rod_handle,
		dd_ref.object_index, &s_pad_size, dir_req->mask, dr_type);
	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	s_pad.pad = (unsigned char *)xmalloc((size_t) s_pad_size);
	if (s_pad.pad == NULL) {
		return DDL_MEMORY_ERROR;
	}
	s_pad.size = s_pad_size;

	/*
		* Send second fetch_request to get the chunks of binary to be
		* evaluated (ie. get a value). Fetch people say s_pad_size and
		* s_pad->used must be zeroed out.
		*/

	s_pad_size = 0;
	s_pad.used = 0;
	rc = fch_rod_block_dir(env_info, rod_handle,
		dd_ref.object_index, &s_pad, &s_pad_size, dir_req->mask, &block_dir_bin);

	/*
		* If s_pad_size > 0, we should not be returning DDS_SUCCESS.  I am
		* assuming rod_fetch_dir() will not be returning DDS_SUCCESS in this
		* instance.  Therefore, I am not checking the size of s_pad_size
		* explicitly.
		*/

	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	rc = eval_dir_block_tables(dir_resp, &block_dir_bin, dir_req->mask);
	if (rc != DDS_SUCCESS) 
	{
		return rc;
	}

	/*
		* Free the memory malloc'd for scratch_pad.
		*/

	if (s_pad.pad) 
	{
		xfree((void **)&s_pad.pad);
	}

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_load_device_tables
 *  ShortDesc: 	Loads the device tables.
 *
 *  Description:
 *     	This routine is a DDI interface routine responsible for
 *		generating device tables.
 *
 *  Inputs:
 *		table_req - ptr to a DIR_SPECIFIER_REFERENCE structure which contains
 *			the dd_handle and the dd_reference of the desired device tables.
 *
 *  Outputs:
 *		table_resp - ptr to a FLAT_DEVICE_DIR structure which contains the
 *			values from the desired device tables.
 *
 *  Returns:
 *      return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/

#define DEVICE_TABLES_FF    (BLK_TBL_MASK | ITEM_TBL_MASK)
#define DEVICE_TABLES_HCF   (BLK_TBL_MASK | ITEM_TBL_MASK | CMD_NUM_ID_TBL_MASK)

int
ddi_load_device_tables(ENV_INFO *env_info, DIR_SPECIFIER_REFERENCE *table_req, FLAT_DEVICE_DIR *table_resp)
{

	int						rc;			/* return code */
	DDI_DEVICE_DIR_REQUEST	dir_req;	/* list of requested device tables */
	UINT8					tok_type;

	ASSERT_RET(table_req && table_resp, DDI_INVALID_PARAM);

	/*
	 * The BLOCK NAME TABLE, ITEM TABLE, and COMMAND NUMBER TO ITEM ID
     * TABLE will be loaded if any of the other device tables are to
	 * be loaded, the user must call ddi_device_dir_request() with the
	 * appropriate masks set.
	 */

	dir_req.type = DD_DD_HANDLE;
	dir_req.spec.ref.rod_handle = table_req->rod_handle;
	dir_req.spec.ref.ref.object_index = table_req->ref.object_index;

	rc = get_tok_type(env_info, dir_req.spec.ref.rod_handle, &tok_type);
	if (rc != SUCCESS) 
	{
		return(rc);
	}

	if (tok_type == FF_TOK_TYPE) 
	{
		dir_req.mask = DEVICE_TABLES_FF;
	}
	else 
	{
		dir_req.mask = DEVICE_TABLES_HCF;
	}

	rc = ddi_device_dir_request(env_info, &dir_req, table_resp);

	return rc;
}


/*********************************************************************
 *  Name: 	ddi_load_block_tables
 *  ShortDesc: 	Loads the block tables.
 *
 *  Description:
 *     	This routine is a DDI interface routine responsible for generating
 *		all of the block tables automatically.
 *
 *  Inputs:
 *		table_req - ptr to a DIR_SPECIFIER_REFERENCE structure which contains
 *			the dd_handle and the dd_reference of the desired block tables.
 *
 *  Outputs:
 *		table_resp - ptr to a FLAT_BLOCK_DIR structure which contains the
 *			values from the desired block tables.
 *
 *  Returns:
 *      return codes from other DDS functions.
 *
 *  Author:	steve beyerl
 *********************************************************************/
int
ddi_load_block_tables(ENV_INFO *env_info, DIR_SPECIFIER_REFERENCE *table_req, FLAT_BLOCK_DIR *table_resp)
{

	int						rc;			/* return code */
	DDI_BLOCK_DIR_REQUEST	dir_req;	/* list of requested block tables */
	UINT8					tok_type;

	ASSERT_RET(table_req && table_resp, DDI_INVALID_PARAM);

	dir_req.type = BD_DD_HANDLE;

	dir_req.spec.ref.rod_handle = table_req->rod_handle;
	dir_req.spec.ref.ref.object_index = table_req->ref.object_index;

	rc = get_tok_type(env_info, dir_req.spec.ref.rod_handle, &tok_type);
	if (rc != SUCCESS) 
	{
		return(rc);
	}
	if (tok_type == FF_TOK_TYPE) 
	{
		dir_req.mask = BLOCK_TBL_MASKS_FF;
	}
	else 
	{
		dir_req.mask = BLOCK_TBL_MASKS_HCF;
	}
	
	rc = ddi_block_dir_request(env_info, &dir_req, table_resp);

	if(rc == DDS_SUCCESS)
	{
		ddi_build_dominant_tbl(env_info, table_resp);
	}
		
	return rc;
}

void ddi_build_dominant_tbl(ENV_INFO *env_info, FLAT_BLOCK_DIR *table_resp)
{
	OP_DESC_LIST op_desc_list = {0};
	op_desc_list.count = 0;
	int rc = DDS_SUCCESS;
	table_resp->dominant_tbl = new DOMINANT_TBL();

	for(int i = 0; i <  table_resp->blk_item_tbl.count; i++)
	{

		DDI_BLOCK_SPECIFIER block_spec = {0};
		block_spec.type = DDI_BLOCK_HANDLE;
		block_spec.block.handle = env_info->block_handle;

		DDI_ITEM_SPECIFIER item_spec = {0};
		item_spec.item.id = table_resp->blk_item_tbl.list[i].blk_item_id;
		item_spec.type = DDI_ITEM_ID;
		ITEM_TYPE item_type = 0;


		rc = ddi_get_type(env_info, &block_spec, &item_spec, &item_type);

		if(rc != DDS_SUCCESS)
		{
			continue;
		}

		DDI_PARAM_SPECIFIER param_spec = {0};

		if (	(item_type == VARIABLE_ITYPE)	// Process each entire parameter first
			||	(item_type == RECORD_ITYPE)
			||	(item_type == ARRAY_ITYPE) )
		{
			param_spec.type = DDI_PS_ITEM_ID;
			param_spec.item.id = table_resp->blk_item_tbl.list[i].blk_item_id;
			param_spec.subindex = 0; 

			rc = ddi_get_update_items(env_info, &block_spec, &param_spec, &op_desc_list);   

			if(rc == DDS_SUCCESS)
			{
				ddi_load_dominant_tables(&op_desc_list, &param_spec, item_type, table_resp->dominant_tbl);
			}
		}

		if(item_type == RECORD_ITYPE)			// If a Record, process each member
		{
			param_spec.type = DDI_PS_ITEM_ID_SI;
			param_spec.item.id = table_resp->blk_item_tbl.list[i].blk_item_id;
			int iParamTblindex = table_resp->blk_item_name_tbl.list[table_resp->blk_item_tbl.list[i].blk_item_name_tbl_offset].param_tbl_offset;

			if(iParamTblindex != -1)
			{
				int iParamMemCount = table_resp->param_tbl.list[iParamTblindex].param_mem_count;

				for(int j = 1; j <= iParamMemCount; j++)
				{
					param_spec.subindex = j;

					rc = ddi_get_update_items(env_info, &block_spec, &param_spec, &op_desc_list); 

					if(rc == DDS_SUCCESS)
					{
						ddi_load_dominant_tables(&op_desc_list, &param_spec, item_type, table_resp->dominant_tbl);
					}
				}
			}
		}
		else if(item_type == ARRAY_ITYPE)		// If an Array, process each element
		{
			param_spec.type = DDI_PS_ITEM_ID_SI;
			param_spec.item.id = table_resp->blk_item_tbl.list[i].blk_item_id;
			int iParamTblindex = table_resp->blk_item_name_tbl.list[table_resp->blk_item_tbl.list[i].blk_item_name_tbl_offset].param_tbl_offset;

			if(iParamTblindex != -1)
			{
				int iParamMemCount = table_resp->param_tbl.list[iParamTblindex].array_elem_count;

				for(int j = 1; j <= iParamMemCount; j++)
				{
					param_spec.subindex = j;

					rc = ddi_get_update_items(env_info, &block_spec, &param_spec, &op_desc_list);  

					if(rc == DDS_SUCCESS)
					{
						ddi_load_dominant_tables(&op_desc_list, &param_spec, item_type, table_resp->dominant_tbl);
					}
				}
			}
		}
	}

	free(op_desc_list.list);
	op_desc_list.list = nullptr;
}

void ddi_load_dominant_tables( OP_DESC_LIST *op_desc_list, DDI_PARAM_SPECIFIER *param_spec, ITEM_TYPE item_type, DOMINANT_TBL *dominant_tbl)
{
	if(dominant_tbl != NULL)
	{
		for(int i = 0; i < op_desc_list->count; i++)
		{

			unsigned long long key = op_desc_list->list[i].op_ref.id;
			
			DOMINANT_TBL::iterator it;

			if(op_desc_list->list[i].op_ref.subindex != 0)
			{
				key |= ((unsigned long long)op_desc_list->list[i].op_ref.subindex) << 32;
			}
			
			it = dominant_tbl->find(key);

			if ( it == dominant_tbl->end( ) )
			{
				OP_REF_LIST *pLocalOpRefList = (OP_REF_LIST*)calloc(1,sizeof(OP_REF_LIST));

				pLocalOpRefList->list = (OP_REF*)calloc(5, sizeof(OP_REF));

				pLocalOpRefList->count = 0;
				pLocalOpRefList->limit = 5;

				std::pair< DOMINANT_TBL::iterator, bool > pr;

				pr = dominant_tbl->insert(std::pair<unsigned long long , OP_REF_LIST*> (key, pLocalOpRefList));

				it = (pr.first);
			}

			OP_REF_LIST *pOpRefList = it->second;

			bool bIsFound = false;

			// foreach element in the list, check to see if it is equal to the param_spec
			// if so, continue to the next loop iteration
			for(int j = 0; j < pOpRefList->count; j++)
			{
				if (pOpRefList->list[j].id == param_spec->item.id)	// Look first for a match in the Item.id
				{
					bIsFound = true;	// We have found this item.id in the list, if a DDI_PS_ITEM_ID

					// However, if we are a DDI_PS_ITEM_ID_SI, we must also match the subindex. If subindex isn't the same, we don't have a match
					if ((param_spec->type == DDI_PS_ITEM_ID_SI) && (pOpRefList->list[j].subindex != param_spec->subindex))
					{
						bIsFound = false;
					}

					if (bIsFound)	// If we really found the right one, break out of the loop
					{
						break;
					}
				}	
			}

			if(bIsFound != true)
			{
				if(pOpRefList->count == pOpRefList->limit)
				{
					pOpRefList->limit += OP_REF_LIST_INCR;

					pOpRefList->list = (OP_REF *) realloc((void *) pOpRefList->list, (size_t) (pOpRefList->limit * sizeof(OP_REF)));

					memset((char *) &pOpRefList->list[pOpRefList->count], 0, (size_t) (OP_REF_LIST_INCR * sizeof(OP_REF)));
				}
				// If we got here, insert at count, then incr count
				pOpRefList->list[pOpRefList->count].id = param_spec->item.id;
				pOpRefList->list[pOpRefList->count].subindex = 0;
				pOpRefList->list[pOpRefList->count].type = item_type;

				if(param_spec->type == DDI_PS_ITEM_ID_SI)
				{
					pOpRefList->list[pOpRefList->count].subindex = param_spec->subindex;
				}
				pOpRefList->count++;
			}
		}
	}
}

/*********************************************************************
 *  Name: 	ddi_clean_block_dir
 *  ShortDesc: 	Frees the block tables
 *
 *  Description:
 *     	This routine is a DDI interface routine which allows ddi users to
 *		delete the block tables.
 *
 *  Inputs:
 *      block_dir - ptr to a FLAT_BLOCK_DIR structure, which contains the
 *			block tables to be deleted.
 *
 *  Outputs:
 *      none
 *
 *  Returns:
 *      void
 *
 *  Author:	steve beyerl
 *********************************************************************/
void
ddi_clean_block_dir(FLAT_BLOCK_DIR *block_dir)
{

	if (!block_dir) {
		return;
	}

	/*
	 * Call EVAL to free the block table.
	 */

	eval_clean_block_dir(block_dir);
}


/*********************************************************************
 *  Name: 	ddi_clean_device_dir
 *  ShortDesc: 	Frees device tables
 *
 *  Description:
 *     	This routine is a DDI interface routine which allows ddi users to
 *		delete the device tables.  Because the BLOCK_NAME_TABLE is to be 
 *		deleted, the block tables are also deleted.
 *
 *  Inputs:
 *      device_dir - ptr to a FLAT_DEVICE_DIR structure, which contains the
 *			device tables to be deleted.
 *
 *  Outputs:
 *      none
 *
 *  Returns:
 *      void
 *
 *  Author:	steve beyerl
 *********************************************************************/
void
ddi_clean_device_dir(FLAT_DEVICE_DIR *device_dir)
{

	int             i;	/* loop variable */
	int             count;	/* number of elements in BLOCK_NAME_TABLE */
	BLK_TBL_ELEM   *bt_ptr;	/* temp ptr to the BLOCK_NAME_TABLE */

	if (!device_dir) {
		return;
	}

	count = (int) device_dir->blk_tbl.count;
	bt_ptr = device_dir->blk_tbl.list;

	/*
	 * Loop to remove all block tables from the BLOCK_NAME_TABLE
	 */

	for (i = 0; i < count; i++) {
		eval_clean_block_dir(&bt_ptr[i].flat_block_dir);
	}

	/*
	 * Call EVAL to free the device table.
	 */

	eval_clean_device_dir(device_dir);
}
