#pragma once
using namespace nsEDDEngine;
#include <Ole/EDDLMethInterp/ManagedMiInterfaces.h>
#include "nsEDDEngine/Attrs.h"
#include "nsEDDEngine/IMethods.h"
#include <nsEDDEngine/ProtocolType.h>
#include <nsConsumer/IBuiltinSupport.h>
#include <nsConsumer/IEDDEngineLogger.h>
#include "../APPS/APPsupport/BuiltinLib/RTN_CODE.H"
#ifdef _METHOD_INTERP_ONLY_
#include <Profibus/amspbserver/MiDataFinder.h>
#endif //_METHOD_INTERP_ONLY_
//#include <afxmt.h>			//for CCriticalSection
#include "EDD Engine/IMiDataFinder.h"
#include <Ole/EDDLMethInterp/std.h>
#include <mutex>
#include <condition_variable>

#ifndef _WIN32 
#include "varient.h"
typedef unsigned char boolean;
#endif

class CUICallbackState;

namespace nsMiEngine
{
	enum enumMiEngineState { READY=0, 
							 WAITING_ON_MI=1,
							 WAITING_ON_SERVER_EXECUTE=2,
							 WAITING_ON_ENGINE_GET=3, 
							 WAITING_ON_ENGINE_SET=4,
							 WAITING_ON_ENGINE_EXECUTE=5,
							 WAITING_ON_ENGINE_MICOMPLETE=6};

}


class CMiEngine : /*public EVENT,*/ public IManagedMiEngineSig
{
	public:
		CMiEngine( BLOCK_INSTANCE iBlockInstance, void* pValueSpec, nsConsumer::EVAL_VAR_VALUE *pActionValue, ACTION* pAction, nsConsumer::IBuiltinSupport *pIBuiltinSupport, nsConsumer::IEDDEngineLogger *pEDDEngineLogger, ProtocolType protocol, const wchar_t* sLanguageCode, IMiDataFinder *pDataFinder);
		~CMiEngine();
		int ExecuteMethod();
		void Cancel();		//used to cancel CMiEngine process

		IAsyncResult* pAsyncResult;			//used to call Execute() and Terminate() respectively at the beginning and end of mehtod exec. thread
		void LockMethodCompleteEvent();
		void UnlockMethodCompleteEvent();
		void ResetMethodCompleteEvent();

	private:	// IManagedMiEngineSig
		BLTIN_ErrorCode convertParamRefToOpRef(const BLOCK_INSTANCE iBlockInstance, const PARAM_REF *pSrcValue, nsEDDEngine::OP_REF *pDstValue);
		BLTIN_ErrorCode convertParamRefToParamSpec(const BLOCK_INSTANCE iBlockInstance, const PARAM_REF *pSrcValue, nsEDDEngine::FDI_PARAM_SPECIFIER *pDstValue);
		BLTIN_ErrorCode convertParamRefToOpRefTrail(const BLOCK_INSTANCE iBlockInstance, PARAM_REF *pSrcValue, /*out*/nsEDDEngine::OP_REF_TRAIL *pDstValue);
		BLTIN_ErrorCode convertParamItemIDNType(const BLOCK_INSTANCE iBlockInstance,
									 const ITEM_ID ulSrcItemId, const ITEM_ID ulSrcMemberId, const itemType_t SrcType,
									 /*out*/ITEM_ID *pulDstItemId, /*out*/ITEM_ID *pulDstMemberId, /*out*/nsEDDEngine::ITEM_TYPE *pDstType);
		BLTIN_ErrorCode GetDescItemTypeAndItemId(const BLOCK_INSTANCE iBlockInstance,
									  const ITEM_ID ulSrcItemId, const ITEM_ID ulSrcMemberId, const nsEDDEngine::ITEM_TYPE eSrcItemType,  
									  /*out*/ITEM_ID *pulDstItemId, /*out*/ITEM_TYPE *peDstItemType);
		static void convertParamRefTypeToOpRefType(const itemType_t srcValue, nsEDDEngine::ITEM_TYPE *pDstValue);
		static BLTIN_ErrorCode convertEvalToVariant(nsConsumer::EVAL_VAR_VALUE *pevalValue, CValueVarient *vtValue);
		static void convertCValueVarientToEval(/* in */ CValueVarient *pvtSrcValue, /* out */ nsConsumer::EVAL_VAR_VALUE *evalDstValue);
		BLTIN_ErrorCode convertVariantToEval(/* in */ CValueVarient *pvtSrcValue, /* in */ variableType_t eValType, /* in */ long lSize, /* out */ nsConsumer::EVAL_VAR_VALUE *evalDstValue);
		static BLTIN_ErrorCode GetUINT64FromCValueVarient(/* in */ CValueVarient *pSrcValue, /* out */ UINT64 *pDstValue, /* out */ boolean *pIsSigned);
		static BLTIN_ErrorCode GetINT64FromCValueVarient(/* in */ CValueVarient *pSrcValue, /* out */ INT64 *pDstValue);
		static BLTIN_ErrorCode ConvertPCCodeToBuiltinCode(nsConsumer::PC_ErrorCode pcCode);
		static int ConvertBSCodeToErrorCode(nsConsumer::BS_ErrorCode bsCode);
		static void convertVariableType(const variableType_t srcValue, nsEDDEngine::VariableType *pDstValue);
		void preLogInterfaceAccess(wchar_t* funcName, wchar_t* message, nsEDDEngine::FDI_PARAM_SPECIFIER *paramSpec = NULL);
		void appendLogInterfaceAccess(wchar_t* message1, wchar_t* message2);
		void postLogInterfaceAccess(const wchar_t* funcName, nsConsumer::EVAL_VAR_VALUE *evalValue = NULL);

		int engineGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen );
		void OnMethodComplete(nsEDDEngine::Mth_ErrorCode iMethodCompleteReturnCode);
		bool waitForMiEngineComplete();
		int OnCommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize);
		void OnCommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity);

		int OnAccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType);

		int OnListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount);
		int OnUIExecute(ACTION_UI_DATA& stUIData);
		nsConsumer::BS_ErrorCode OnUIExecuteText(ACTION_UI_DATA& stUIData);
		nsConsumer::BS_ErrorCode OnUIExecuteEditDeviceParam(ACTION_UI_DATA& stUIData);
		nsConsumer::BS_ErrorCode OnUIExecuteEditLocal(ACTION_UI_DATA& stUIData);
		nsConsumer::BS_ErrorCode OnUIExecuteSelection(ACTION_UI_DATA& stUIData);
		int OnUIMethRspExecute(CValueVarient& vtRsp);
		static void UICallback (nsConsumer::EVAL_VAR_VALUE *pvUserUIInput, nsConsumer::BS_ErrorCode eStatus, void *state);
		int OnRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol);
		int OnDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen);
		int OnLitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen);
		int OnResponseCodeStringExecute(ITEM_ID iItemId, ITEM_ID iMemberId, int iRsponseCode, CValueVarient *pvtValue);
		int OnDdsErrorExecute(wchar_t* pErrorString);
		unsigned long OnCommErrorExecute();
		int OnSendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType);
		int OnResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId);
		int engineAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef);
		ITEM_ID OnBlockRefExecute(unsigned long ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType);
		int engineAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *vtValue, variableType_t eValType, AccessType eAccessType, long lSize);
		int engineAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType);
		int engineWriteMethRtnVar(/*out*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal);
		BLTIN_ErrorCode ConvertDdsCodeToBuiltinCode(int ddsCode);
		int OnBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount);
		int OnBlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex);
		int OnBlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex);
		int OnGetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data);
		int OnGetListParamSpecifier(/*in*/unsigned long ulListId, /*in*/int iIndex,											//List item IDs
									/*in*/unsigned long ulEmbListId, /*in*/int iEmbListIndex,								//embedded list item IDs
									/*in*/unsigned long ulElementId, /*in*/unsigned long ulSubElementId,					//element item IDs ultimately referenced by the list
									/*in*/ITEM_TYPE peItemType[], /*in*/ITEM_ID pulItemID[], /*in*/ITEM_ID pulMemberID[],	//type and item ID arrays of middle elements between the list and the element
									/*in*/unsigned short usArrElemCount,													//number of middle elements in the above arrays								
									/*out*/FDI_PARAM_SPECIFIER* pParamSpec);												//output
		void engineLogMessage(int priorityVal, wchar_t *msg);
		void engineMethodDebugMessage(unsigned long lineNumber,
			/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml);
		int engineIsOffline();
		long engineSendCommand(unsigned long commandId, long *commandError);
		int engineAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType);
		int engineGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString);
		virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0);


	protected:
        /*LRESULT*/ int OnMethodInterpreterComplete();
		int GetValueByItemID2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, long lPropertyType, CValueVarient *pvtValue );
		int engineGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput);
		int ResolveItemNameToID( CStdString sItemName, unsigned long *memberID );
		void ChangeState(nsMiEngine::enumMiEngineState newState);
		nsMiEngine::enumMiEngineState GetState();

	private:
		BLOCK_INSTANCE m_iBlockInstance;
		void* m_pValueSpec;
		CUICallbackState* m_pCurrentUICallbackState;
		nsConsumer::EVAL_VAR_VALUE m_evvMethodInputValue;
		nsConsumer::EVAL_VAR_VALUE m_evvMethodReturnValue;
		ITEM_ID m_MethodID;
		ACTION m_Action;
		ProtocolType m_protocol;
        std::vector<WORD> m_waSavedValues;
		nsMiEngine::enumMiEngineState m_currentState;
		IManagedServerMiSig* m_ManagedEngine;
		nsEDDEngine::Mth_ErrorCode m_iMethodCompleteReturnCode;
		IMiDataFinder* m_pDataFinder;
		std::mutex m_CriticalSection;
		nsConsumer::IBuiltinSupport *m_pIBuiltinSupport;
		nsConsumer::IEDDEngineLogger *m_pIEDDEngineLogger;
		int m_iDdsErrorCode;

		//The following variables are created for UI builtins
		CValueVarient m_vUIInput;		//for UI builtin user selection, and user input value in all kinds of data types
		nsConsumer::BS_ErrorCode m_eUIStatus; //for UI builtin response: Okay/Cancel/Error
		std::mutex mi_uiBltinCriticalSection;
		unsigned long ulUICount;

		std::mutex m_methodCompleteWaitLock;
		static bool m_bmethodCompleted;
		std::condition_variable m_methodCompleteCondVariable;
};


class CUICallbackState
{
	public:
		CUICallbackState(CMiEngine *pMiEngine) {m_pMiEngine = pMiEngine; m_bProcessed = false;};
		~CUICallbackState() {};

		bool m_bProcessed;
		CMiEngine *m_pMiEngine;

private:
		CUICallbackState();	//provent from using compiler default constructor
};
