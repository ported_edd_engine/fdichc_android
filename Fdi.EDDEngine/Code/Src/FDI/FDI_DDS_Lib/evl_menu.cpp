/**
 *	@(#) $Id: evl_menu.cpp,v 1.2 2012/09/24 23:54:37 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  MENU_ITEM_LIST and MENU_ITEM.
 */


#include "stdafx.h"
#include "evl_loc.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif


/*********************************************************************
 *
 *	Name: ddl_free_menuitems_list
 *	ShortDesc: Free the list of MENU_ITEMs
 *
 *	Description:
 *		ddl_free_menuitems_list will check the MENU_ITEM_LIST pointer and
 *		the list, if the are both not equal to NULL it will free or
 *		clean the list, depending on dest_flag, and set the count and
 *		limit equal to zero.
 *
 *	Inputs:
 *		menu: pointer to the MENU_ITEM_LIST structure.
 *		dest_flag: flag specifying "cleaning" or "freeing" of lists.
 *
 *	Outputs:
 *		menu: pointer to the list of MENU_ITEM_LIST with an empty
 *		list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_menuitems_list(
MENU_ITEM_LIST *menu,
uchar           dest_flag)
{

	int             i;
	MENU_ITEM      *temp_list;

	if (menu == NULL) {
		return;
	}

	if (menu->list == NULL) {

		ASSERT_DBG(!menu->count && !menu->limit);
		menu->limit = 0;
	}
	else {

		temp_list = menu->list;
		for (i = 0; i < menu->count; temp_list++, i++) {
			ddl_free_op_ref_trail(&temp_list->ref);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of MENU_ITEMs
			 */

			free((void *) menu->list);
			menu->list = NULL;
			menu->limit = 0;
		}
	}

	menu->count = 0;
}


/*********************************************************************
 *
 *	Name: ddl_shrink_menuitems_list
 *	ShortDesc: Shrink the list of MENU_ITEMs
 *
 *	Description:
 *		ddl_shrink_menuitems_list reallocs the list of MENU_ITEMs to contain
 *		only the MENU_ITEMs being used
 *
 *	Inputs:
 *		menu: pointer to the MENU_ITEM_LIST structure
 *
 *	Outputs:
 *		menu: pointer to the resized MENU_ITEM_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static int
ddl_shrink_menuitems_list(
MENU_ITEM_LIST *menu)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_MENUITEMS_LIST);
#endif

	if (menu == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (menu->list == NULL) {
		ASSERT_DBG(!menu->count && !menu->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (menu->count == menu->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (menu->count == 0) {
		ddl_free_menuitems_list(menu, FREE_ATTR);
	}
	else {
		menu->limit = menu->count;

		menu->list = (MENU_ITEM *) realloc((void *) menu->list,
			(size_t) (menu->limit * sizeof(MENU_ITEM)));

		if (menu->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddl_parse_menuitems
 *
 *	ShortDesc: Parse a list of MENU_ITEMs
 *
 *	Description:
 *		ddl_parse_menuitems parses the binary data and loads a MENU_ITEM_LIST
 *		structure if the pointer to the MENU_ITEM_LIST is not NULL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		menu - pointer to an MENU_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		menu - pointer to a MENU_ITEM_LIST structure containing the result
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		error returns ddl_parse_item_id(), ddl_parse_bitstring()
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static
int
ddl_parse_menuitems(
unsigned char **chunkp,
DDL_UINT       *size,
MENU_ITEM_LIST *menu,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int             rc;	/* return code */
	DDL_UINT        temp_uint;	/* temp storage for a parsed bitstring */
	MENU_ITEM      *temp_menu_item;	/* temp ptr to the list of menu items */
	DDL_UINT        tag;				/* stores the binary REFERENCE type number */

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_MENUITEMS);
#endif


	while (*size > 0) {

		/*
		 * Parse a series of MENU_ITEMs.  If we need more room in the
		 * array of structures, malloc more room.  Then parse the next
		 * MENU_ITEM structure.
		 */

		if (menu) {
			if (menu->count == menu->limit) {

				menu->limit += MENU_INC;
				temp_menu_item = (MENU_ITEM *) realloc((void *) menu->list,
					(size_t) menu->limit * sizeof(MENU_ITEM));

				if (temp_menu_item == NULL) {
					menu->limit = menu->count;
					ddl_free_menuitems_list(menu, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_menu_item[menu->count], 0,
					(MENU_INC * sizeof(MENU_ITEM)));

				menu->list = temp_menu_item;

			}

			temp_menu_item = &menu->list[menu->count++];

			/*
			 * Parse the data reference and the flag bits for the
			 * qualifiers.
			 */
			DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

			if(tag!=MENU_TAG)
			{
				return DDL_ENCODING_ERROR;
			}

			rc = ddl_parse_op_ref_trail(chunkp, size, &temp_menu_item->ref,
				depinfo, env_info, var_needed);
			
			if (rc != DDL_SUCCESS)
			{
				if (rc == CM_BLOCK_NOT_FOUND || rc == DDL_INVALID_REFERENCE)
				{
					// If it's invalid block or reference, 
					// just skip the reference and conntinue parsing other menu references. 
					// But �ddl_parse_bitstring()� will still be called, but the results will be thrown away just like the reference.
					menu->count--;
				}
				else
				{
					// If it's other error than CM_BLOCK_NOT_FOUND,
					// its better to pass it to the caller.
					return rc;
				}
			}

			rc = ddl_parse_bitstring(chunkp, size, &temp_uint);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			temp_menu_item->qual = (unsigned short) temp_uint;
		}
		else {

			/*
			 * Because a value is not desired, and speed is
			 * important, ddl_parse_item_id() is called instead of
			 * ddl_parse_op_ref_trail().
			 */

			rc = ddl_parse_item_id(chunkp, size, (ITEM_ID *) NULL, depinfo,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			rc = ddl_parse_bitstring(chunkp, size, (DDL_UINT *) NULL);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_menuitems_choice
 *	ShortDesc: Choose the correct list of menu items from a binary.
 *
 *	Description:
 *		ddl_menuitems_choice will parse the binary for a list of menu items,
 *		according to the current conditionals (if any).  The value of
 *		the menu items is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		items - pointer to an MENU_ITEM_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		items - pointer to an MENU_ITEM_LIST structure containing the result
 *		depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_menuitems().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static int
ddl_menuitems_choice(
unsigned char **chunkp,
DDL_UINT       *size,
MENU_ITEM_LIST *items,
nsEDDEngine::OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp ptr to an element of the chunk
					 * list */

#ifdef DDSTEST
	TEST_FAIL(DDL_MENUITEMS_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		MENU_ITEM_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}


	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (items && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (items == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_menuitems(&(chunk_ptr->chunk), &(chunk_ptr->size),
				items, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (items && data_valid) {
			*data_valid = TRUE;	/* items has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}


/*******************************************************************
*
* Name: eval_attr_menuitems
*
* ShortDesc: evaluate a menu items attribute
*
* Description:
*	The eval_attr_menuitems function evaluates a list of menu items.
*	The buffer pointed to by chunk should contain the binary of the list
*	of menu items. Size specifies the size of the binary chunk.
*	If items is not a null pointer, the menu items are returned in items.
*	If depinfo is not a null pointer, dependency information about the
*	menu items is returned in depinfo.
*
*  Inputs:
*      chunkp - pointer to the address of the binary
*      size - pointer to the size of the binary
*      menu - pointer to a MENU_ITEM_LIST structure where the result will
*              be stored.  If this is NULL, no result is computed
*              or stored.
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
*              information will be stored.  If this is NULL, no
*              dependency information will be stored.
*      env_info - environment information
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
*  Outputs:
*      chunkp - pointer to the address following this binary
*      size - pointer to the size of the binary following this one
*      menu - pointer to a MENU_ITEM_LIST structure containing the result
*      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
*              dependency information.
*      data_valid - pointer to an int which, if set to 1, indicates
*              that there is a valid value in expr
*      var_needed - pointer to a OP_REF structure. If the value
*              of a variable is needed, but is not available,
*              "var_needed" info is returned to the application.
*
*
* Returns:
*	DDL_SUCCESS
*	DDL_DEFAULT_ATTR
*	return codes from:
*		ddl_menuitems_choice()
*		ddl_shrink_depinfo()
*
* Author: steve beyerl
****************************************************************/

int
eval_attr_menuitems(
uchar          *chunk,
DDL_UINT        size,
void		   *voidP,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed,
void *)
{
	MENU_ITEM_LIST* menu = static_cast<MENU_ITEM_LIST*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* indicates the validity of the data in menu */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_MENUITEMS);
#endif

	valid = 0;

	if (menu) {
		ddl_free_menuitems_list(menu, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;

	/*
	 * Parse the binary.
	 */

	rc = ddl_menuitems_choice(&chunk, &size, menu, depinfo, &valid,
		env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_menuitems_list(menu, FREE_ATTR);
		return rc;
	}

	/*
	 * Condense the menu list and the dependency list.
	 */

	rc = ddl_shrink_depinfo(depinfo);

	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_menuitems_list(menu);
	}

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_menuitems_list(menu, FREE_ATTR);
		return rc;
	}

	if (menu && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddl_parse_role
 *
 *	ShortDesc: Parse a list of roles
 *
 *	Description:
 *		ddl_parse_role parses the binary data and loads a STRING
 *		structure if the pointer to the STRING is not NULL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		role - pointer to an STRING structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		role - pointer to a STRING structure containing the result
 *      depinfo - pointer to a nsEDDEngine::OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		error returns ddl_parse_item_id(), ddl_parse_bitstring()
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static inline
int
ddl_parse_role(
unsigned char **chunkp,
DDL_UINT       *size,
STRING			*role,
nsEDDEngine::OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
nsEDDEngine::OP_REF         *var_needed)
{

	int rc = DDL_SUCCESS;	/* return code */

	while (*size > 0) {

		if (role) {
			// Parse out a new role string
			STRING string = {0};

			rc = ddl_parse_string(chunkp, size, &string, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (role->str == NULL)	// The string has not yet been allocated
			{
				role->str = (wchar_t*)malloc((string.len + 2) * sizeof(wchar_t));
				role->str[0] = NULL;	// Initialize to empty string by null terminating
			}
			else
			{						// Allocate room for original string, new string and " & "
				role->str = (wchar_t*)realloc(role->str, (role->len + string.len + 5) * sizeof(wchar_t));
			}

			if (role->str[0] != NULL)	// Append " & ", if the string is non-zero
			{
				wcscat(role->str, L" & ");
			}

			wcscat(role->str, string.str);		// Append the new string

			ddl_free_string(&string);			// Free the new string

			role->len = (unsigned short)wcslen(role->str);		// Set the other string members
			role->flags = STRING::FREE_STRING;
		}
		else {
			// Value is not desired, just parse past the string
			rc = ddl_parse_string(chunkp, size, (STRING *) NULL, depinfo,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}

