////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "BuiltInMapper.h"
//
// Author: Mike DeCoster
//
// Creation Date: 5-2-07
//
// Purpose: Handles a list of info about built-ins
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __BUILT_IN_MAPPER__H_
#define __BUILT_IN_MAPPER__H_

#include <nsEDDEngine/ProtocolType.h>
#include <mutex>
using nsEDDEngine::ProtocolType;
#include "BuiltInInfo.h"
#include <vector>
#include "BuiltIn.h"


class CBuiltInMapper
{
public:
	~CBuiltInMapper();
	void Clear();

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Singleton methods
	////////////////////////////////////////////////////////////////////////////////////////////////
	static CBuiltInMapper* GetInstance();
	static void DestroyInstance();
	void StartUp(ProtocolType type);
	void Shutdown();


	////////////////////////////////////////////////////////////////////////////////////////////////
	// GetSize
	//
	// Returns the number of elements on the list based on current protocol type
	//
	// Return: _UINT32		num ele's on list
	////////////////////////////////////////////////////////////////////////////////////////////////
        UINT32 GetSize() const {
		switch (m_enStartedProtocol)
		{
		case nsEDDEngine::HART:
                        return (UINT32)m_lsInfo_hart.size();
			break;
		case nsEDDEngine::FF:
                        return (UINT32)m_lsInfo_fieldbus.size();
			break;
		case nsEDDEngine::PROFIBUS:
                        return (UINT32)m_lsInfo_profibus.size();
			break;
		default:
                        return (UINT32)0;
			break;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Add
	//
	// Adds a new built in info object onto the back of the list
	//
	// In:		info			the info object to add to the list
	////////////////////////////////////////////////////////////////////////////////////////////////
	void Add(const CBuiltInInfo& info);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// operator[]
	//
	// Returns a reference to an element on the list
	//
	// In:		uIndex			the index of the element to get a copy of
	//
	// Return:	CBuiltInInfo	the element from the list
	////////////////////////////////////////////////////////////////////////////////////////////////
	CBuiltInInfo& operator[](unsigned int uIndex);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// operator[]
	//
	// Returns a reference of the first element on the list that matches szName
	//
	// In:		szName					the name of the element
	//
	// Return:	const CBuiltInInfo*		the element from the list
	////////////////////////////////////////////////////////////////////////////////////////////////
	CBuiltInInfo* operator[](const char* szName);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Get
	//
	// Returns a ptr to the first element in the list with an enum matching enName
	//
	// In:		enName					the enum of the element you want
	//
	// Return:	CBuiltInInfo*			the info you want or null if not found
	////////////////////////////////////////////////////////////////////////////////////////////////
	CBuiltInInfo* Get(BUILTIN_NAME enName);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// DebugDump
	//
	// Dumps the list to "BuiltInMap.txt" in working directory
	////////////////////////////////////////////////////////////////////////////////////////////////
	void DebugDump();

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Accessors and Mutators:
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool IsStarted() {
		bool bStarted = false;
		switch (m_enStartedProtocol)
		{
		case nsEDDEngine::HART:
			bStarted = m_bStarted_hart;
			break;
		case nsEDDEngine::FF:
			bStarted = m_bStarted_fieldbus;
			break;
		case nsEDDEngine::PROFIBUS:
			bStarted = m_bStarted_profibus;
			break;
		default:
			break;
		}

		return bStarted;
	}

	void LockBIMapper(ProtocolType type);
	void ReleaseBIMapper();

private:

	static std::mutex m_mutex;							//protecting m_pInst
	static CBuiltInMapper* m_pInst;
	ProtocolType m_enStartedProtocol;					//protected my m_IsInfo_cs

	bool m_bStarted_hart;								//protected my m_IsInfo_cs
	bool m_bStarted_fieldbus;							//protected my m_IsInfo_cs
	bool m_bStarted_profibus;							//protected my m_IsInfo_cs

	CBuiltInMapper();
	CBuiltInMapper(CBuiltInMapper& rs);					// not used
	CBuiltInMapper& operator=(CBuiltInMapper& rs);		// not used

	std::vector<CBuiltInInfo> m_lsInfo_hart;			//protected my m_IsInfo_cs
	std::vector<CBuiltInInfo> m_lsInfo_fieldbus;		//protected my m_IsInfo_cs
	std::vector<CBuiltInInfo> m_lsInfo_profibus;		//protected my m_IsInfo_cs

	std::mutex m_IsInfo_cs;
};

#endif
