/*
 * @(#) $Id: dds_tab.h,v 1.5 2012/09/06 22:28:01 rgretta Exp $
 */

#ifndef DDS_TAB_H
#define DDS_TAB_H

#include "table.h"
#include "env_info.h"

#include "nsEDDEngine/Table.h"

#ifdef __cplusplus
    extern "C" {
#endif

/*
 * Macros used in dds_tab
 */

#define CHAR_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex) (subindex-1)
#define CHAR_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset) (table_offset+1)


#define	CHECK_CHAR_MEMBER_OFFSET(cmt, offset)(((offset >= CMT_COUNT(cmt)) || \
									(offset < 0)) ? \
									(DDI_TAB_BAD_CHAR_OFFSET) \
									: (DDS_SUCCESS))


#define PARAM_MEM_SUBINDEX_TO_TABLE_OFFSET(subindex) (subindex-1)
#define PARAM_MEM_TABLE_OFFSET_TO_SUBINDEX(table_offset) (table_offset+1)

#define	CHECK_PARAM_MEMBER_OFFSET(pt_elem,offset)(((offset >= PTE_PM_COUNT(pt_elem)) && \
									(offset >= PTE_AE_COUNT(pt_elem)) || \
									(offset < 0)) ? \
									(DDI_TAB_BAD_PARAM_MEMBER) \
									: (DDS_SUCCESS))

#define CHECK_PARAM_OFFSET(pt,offset)(((offset >= PT_COUNT(pt)) || \
								(offset < 0)) ? \
								(DDI_TAB_BAD_PARAM_OFFSET) \
								: (DDS_SUCCESS))

#define CHECK_PARAM_LIST_OFFSET(plt, offset)(((offset >= PLT_COUNT(plt)) || \
								(offset < 0)) ? \
								(DDI_TAB_BAD_PARAM_LIST_OFFSET) \
								: (DDS_SUCCESS))

#define	CHECK_CHAR_MEMBER_OFFSET(cmt, offset)(((offset >= CMT_COUNT(cmt)) || \
									(offset < 0)) ? \
									(DDI_TAB_BAD_CHAR_OFFSET) \
									: (DDS_SUCCESS))

#define REQ_TABLE_CHK(x,p)((x > 0) && (p != NULL))

#define OPT_TABLE_CHK(x,p)(((x > 0) && (p != NULL)) || \
						((x <= 0) && (p == NULL)))

/*	
 * 	function prototypes needed for conversion to ddid 
 */

extern int	tr_param_si_to_ite P((nsEDDEngine::ITEM_TBL *,nsEDDEngine::BLK_TBL_ELEM *,int,SUBINDEX, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_resolve_name_to_id_type P((ENV_INFO *, BLOCK_HANDLE,nsEDDEngine::BLK_TBL_ELEM *,ITEM_ID,ITEM_ID *, ITEM_TYPE *, bool check_for_param)) ;

extern int	tr_id_si_to_ite P((nsEDDEngine::ITEM_TBL *,nsEDDEngine::BLK_TBL_ELEM *,ITEM_ID,SUBINDEX, nsEDDEngine::ITEM_TBL_ELEM **)) ;

/*	
 * 	function prototypes needed for conversion to subindex 
 */

extern int	tr_resolve_ddid_mem_to_si P((nsEDDEngine::BLK_TBL_ELEM *,ITEM_ID,ITEM_ID,SUBINDEX *, ENV_INFO *, DESC_REF *)) ;

extern int	tr_resolve_lid_pname_to_id_type P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::BLK_TBL_ELEM *, ITEM_ID,
											ITEM_ID,ITEM_ID *, ITEM_TYPE *));

/*	
 * 	function prototypes needed for conversion to find table elements 
 */

extern int	find_bint_elem P((nsEDDEngine::BLK_ITEM_NAME_TBL *,ITEM_ID,nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM **)) ;
extern int	find_bit_elem P((nsEDDEngine::BLK_ITEM_TBL *,ITEM_ID,nsEDDEngine::BLK_ITEM_TBL_ELEM **)) ;
extern int	find_pmnt_elem P((nsEDDEngine::PARAM_MEM_NAME_TBL *,nsEDDEngine::PARAM_TBL_ELEM *,ITEM_ID,nsEDDEngine::PARAM_MEM_NAME_TBL_ELEM **));


/*
 * Function prototype for converting the block handle into the block
 * table element.
 */

extern int block_handle_to_bt_elem P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::BLK_TBL_ELEM **));


/*
 * Funciton prototype needed for the table checker
 */

extern void	dds_check_table P((ENV_INFO *, DEVICE_TYPE_HANDLE)) ;

extern int	tr_characteristics_to_ite P((nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *, SUBINDEX,
											nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_resolve_char_rec_to_ddid P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::BLK_TBL_ELEM *, ITEM_ID *)) ;
	

#ifdef __cplusplus
    }
#endif

#endif				/* DDS_TAB_H */

