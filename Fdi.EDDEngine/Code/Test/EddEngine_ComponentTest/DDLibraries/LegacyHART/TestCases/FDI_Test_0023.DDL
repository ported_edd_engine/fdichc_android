/*** FDI DeviceModelServices EDD TestCase ***/
#include "Macros.h"
#include "Methods.h"

// Manufacturer 0xFD = "Special"
MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0023, DEVICE_REVISION 1, DD_REVISION 1

/****************************************************************************** */
/*    Common Tables and Univeral Commands (mandatory)                           */
/* **************************************************************************** */
/* */                                                                           
/* */  IMPORT STANDARD _TABLES, DEVICE_REVISION 6, DD_REVISION 1                
/* */  {                                                                        
/* */      EVERYTHING;                                                          
/* */  }                                                                        
/* */                                                                           
/* */  IMPORT STANDARD _UNIVERSAL, DEVICE_REVISION 5, DD_REVISION 1             
/* */  {                                                                        
/* */      EVERYTHING;
/* */  }                                                                        
/* */

/****************************************************************************** */
/* Dynamic variables (mandatory)                                                */
/* **************************************************************************** */
/* */  
/* */  
/* */  ARRAY OF VARIABLE dynamic_variables
/* */  {
/* */      ELEMENTS
/* */      {
/* */          PRIMARY, primary_value;
/* */      }
/* */  }
/* */




VARIABLE primary_value
{
    LABEL "Primary value as float";
    TYPE FLOAT;
    CLASS DEVICE & DYNAMIC;
}


/****************************************************************************** */
/* Root menus                                                                   */
/****************************************************************************** */

MENU device_root_menu
{
    LABEL "Device Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

MENU root_menu
{
    LABEL "Root Menu";
    STYLE MENU;
    ITEMS
    {
        main_test_menu
    }
}

MENU offline_root_menu
{
    LABEL "Offline Root Menu";
    ITEMS
    {
        main_test_menu
    }
}

/****************************************************************************** */
/* Test structure                                                               */
/****************************************************************************** */

MENU main_test_menu
{
    LABEL "Main test menu";
    STYLE WINDOW;
    ITEMS
    {
        value_array_menu
    }
}

MENU value_array_menu
{
    LABEL "value array menu";
    ITEMS
    {
        array_of_values[1]
    }
}

/****************************************************************************** */
/* variables                                                                    */
/* **************************************************************************** */
ARRAY array_of_values
{
    LABEL "Test label";
    HELP "Test help";
    TYPE myvariable1;
    NUMBER_OF_ELEMENTS 100;
}

VARIABLE myvariable1
{
    LABEL "float variable 1";
    TYPE FLOAT;
    CLASS LOCAL;
}
