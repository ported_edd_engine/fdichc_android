#ifndef ITERATIONFOR_H
#define ITERATIONFOR_H

#include "Statement.h"
#include "Expression.h"
#include "BreakStatement.h"
#include "ReturnStatement.h"
#include "ContinueStatement.h"
#include "AssignmentStatement.h"

class CSymbolTable;
class CIterationForStatement : public CStatement  
{
public:
	CIterationForStatement();
	virtual ~CIterationForStatement();

//	Identify self
	virtual void Identify(
		_CHAR* szData);

//	Allow Visitors to do different operations on the node.
	virtual _INT32 Execute(
		CGrammarNodeVisitor*	pVisitor,
		CSymbolTable*			pSymbolTable,
		INTER_VARIANT*		pvar=0,
		ERROR_VEC*				pvecErrors=0,
		RUL_TOKEN_SUBTYPE	AssignType = RUL_ASSIGN);//Anil August 26 2005 to Fix a[exp1] += exp2

//	Create as much of the parse tree as possible.
	virtual _INT32 CreateParseSubTree(
		CLexicalAnalyzer*	plexAnal, 
		CSymbolTable*	pSymbolTable,
		ERROR_VEC*		pvecErrors=0);

//This returns the last line in which this node has a presence...
	virtual _INT32 GetLineNumber();

	CAssignmentStatement*	GetExpressionStatement();
	CExpression*			GetExpression();
	CStatement*		GetInitializationStatement();
	CExpression*	GetInitializationExpression();
	CStatement*		GetIncrementStatement();
	CExpression*	GetIncrementExpression();

	CStatement*		GetStatement();

	GRAMMAR_NODE_TYPE GetIncrementNodeType()
	{
		return incrementNodeType;
	}

	GRAMMAR_NODE_TYPE GetExpressionNodeType()
	{
		return expressionNodeType;
	}

protected:
	CAssignmentStatement*	 m_pExpressionStatement;	//check statement
	CExpression* m_pExpression;							//check expression
	CAssignmentStatement* m_pInitializationStatement;	//init statement
	CExpression* m_pInitializationExpression;			//init expression
	CAssignmentStatement* m_pIncrementStatement;		//increment statement
	CExpression* m_pIncrementExpression;				//increment expression

	CStatement* m_pStatement;							//for loop statement

	GRAMMAR_NODE_TYPE incrementNodeType;
	GRAMMAR_NODE_TYPE expressionNodeType;
};

#endif
