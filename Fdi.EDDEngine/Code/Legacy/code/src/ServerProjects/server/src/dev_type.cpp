
////////////////////////////////////////////////////////////////////////
////	Device Type Load/Unload Functions.
////
////	@(#) $Id: dev_type.cpp,v 1.11 1996/06/11 13:16:15 joefish Exp $
////////////////////////////////////////////////////////////////////////

#include "stdinc.h"

#include "dev_type.h"
#include "HART/HARTEDDEngine/DDSSupport.h"
#include "DeviceTypeMgr.h"
////////////////////////////////////////////////////////////////////////
////
////    Load the Device Type Information
////
////////////////////////////////////////////////////////////////////////

int load_dev_type_info (ENV_INFO * env_info, DEVICE_TYPE_HANDLE dth) 
{
	DD_DEVICE_ID*	dd_device_id ;
	int				rc ;
	SYMINFO			*syminfo ;
	DDI_DEVICE_DIR_REQUEST ddi_dir_req;
	FLAT_DEVICE_DIR	*flat_device_dir;

	// get the DD Device ID for loading the symbol file

	dd_device_id = g_DeviceTypeMgr.get_adtt_dd_device_id ( dth );
	if (!dd_device_id) {
		return(CM_DEVICE_TYPE_NOT_FOUND) ;
	}

 	rc = g_DeviceTypeMgr.get_adtt_syminfo(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;
	
	if (syminfo != NULL)
	{
		return SUCCESS ;
	}

	syminfo = new SYMINFO ;

	if (syminfo == NULL) 
	{
		return (CM_NO_MEMORY) ;
	}

	//read sym file

	rc = read_sym_file(env_info, dd_device_id, syminfo);
	if (rc != SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"read_sym_file failed" );
		return(rc) ;
	}

	/*
	 * Save the symbol information
	 */

	rc = g_DeviceTypeMgr.set_adtt_syminfo(dth, syminfo) ;
	if (rc != SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"set_adtt_syminfo failed" );
		return(rc) ;
	}

	rc = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(dth, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"get_adtt_dd_dev_tbls failed" );
		return(rc) ;
	}

	memset(&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
	ddi_dir_req.type = DD_DT_HANDLE;

	ROD_HANDLE rod_handle = 0;
	rc = g_DeviceTypeMgr.get_adtt_dd_handle (dth, &rod_handle);
	if (rc != CM_SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"get_adtt_dd_handle failed" );
		return(rc) ;
	}
	ddi_dir_req.spec.ref.rod_handle = rod_handle;

	//To support fms format
	ddi_dir_req.mask = STRING_TBL_MASK;
			
	bool tok6or8 = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(ddi_dir_req.spec.ref.rod_handle);
	if (tok6or8)
	{
		ddi_dir_req.mask |= IMAGE_TBL_MASK;
	}

	ddi_dir_req.spec.device_type_handle =  dth;
	
	rc = ddi_device_dir_request(env_info, &ddi_dir_req, flat_device_dir);
	if (rc != SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"ddi_device_dir_request failed" );
		return (rc);
	}

	return(SUCCESS) ;
}	

////////////////////////////////////////////////////////////////////////
////
////    Remove the Device Type Information
////
////////////////////////////////////////////////////////////////////////

int remove_dev_type_info (ENV_INFO * /* env_info */, DEVICE_TYPE_HANDLE dth) 
{
	SYMINFO	*syminfo ;
	int		rc ;

	rc = g_DeviceTypeMgr.get_adtt_syminfo(dth, &syminfo) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	if (syminfo) {
		free_symtbl(syminfo);
		delete(syminfo);
	}

	rc = g_DeviceTypeMgr.set_adtt_syminfo(dth, NULL) ;

	ASSERT_RET(rc == SUCCESS,rc) ;

	return(SUCCESS) ;
}	
	
