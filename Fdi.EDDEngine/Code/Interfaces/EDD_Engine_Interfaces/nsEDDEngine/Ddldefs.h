
#pragma once

#include "nsEDDEngine/AttributeName.h"
//
//	DDLDEFS.h - defines constants that are found in the FDI Binary File Format Specification
//	FDI-2041.doc
//

#pragma warning ( disable : 4005 )

typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned long  ulong;
typedef unsigned long long ulonglong;

typedef	unsigned short OBJECT_INDEX;
typedef	unsigned long  SUBINDEX;

typedef unsigned long	ITEM_ID;
typedef unsigned long	DDL_UINT;	/* Unparsed integer from binary */
typedef	unsigned long	OFFSET;


namespace nsEDDEngine
{


// For FF only
#define DDL_HUGE_INTEGER ULONG_MAX  /* defines the maximum size of an */
									/* unsigned integer               */
#define INVALID_ITEM_ID  0L

/*
 * Major and minor DDOD Binary revision numbers.
 * These are used by the Tokenizer and DDS to form a Version number:
 *
 * 		<major ddod revision>.<minor ddod revision>.<software revision>
 *
 * The major and minor binary revision numbers are assigned here. 
 * The software revision numbers are assigned independently by the
 * Tokenizer and DDS in separate header files.
 */

/*
 * Value assigned to unused table offsets in the Block Directories.
 */
#define UNUSED_OFFSET   ((OFFSET)~0)

	//
	//	Item Types.
	//
	// This enumeration gets it values from the �DD_Item_Type� enumeration in section 5.5.1 of the FDI-2041 spec.
	// It is used as an attribute in the ItemBase, FDI_GENERIC_ITEM, FLAT_ITEM_ARRAY and FLAT_COLLECTION classes

	//	WARNING- If you change this list in ANY way, you MUST
	//	change the resolve_id_table in evl_rslv.cpp also! These defines
	//	index into an array, so they should be consecutive.
	//
	enum ITEM_TYPE
	{
		ITYPE_NO_VALUE			= 0,
		ITYPE_VARIABLE 	       	= 1,
		ITYPE_COMMAND           = 2,
		ITYPE_MENU              = 3,
		ITYPE_EDIT_DISP         = 4,
		ITYPE_METHOD            = 5,
		ITYPE_REFRESH           = 6,
		ITYPE_UNIT              = 7,
		ITYPE_WAO 	       		= 8,
		ITYPE_ITEM_ARRAY        = 9,
		ITYPE_COLLECTION        = 10,
		ITYPE_BLOCK_B			= 11,
		ITYPE_BLOCK             = 12,
//#define PROGRAM_ITYPE           13 //Not used in FDI
		ITYPE_RECORD            = 14,
		ITYPE_ARRAY             = 15,
		ITYPE_VAR_LIST          = 16,
		ITYPE_RESP_CODES        = 17,
//#define DOMAIN_ITYPE            18  //Not used for FDI
		ITYPE_MEMBER            = 19,
		ITYPE_FILE				= 20,
		ITYPE_CHART				= 21,
		ITYPE_GRAPH				= 22,
		ITYPE_AXIS				= 23,
		ITYPE_WAVEFORM			= 24,
		ITYPE_SOURCE			= 25,
		ITYPE_LIST				= 26,
		ITYPE_GRID				= 27,
		ITYPE_IMAGE				= 28,
		ITYPE_BLOB				= 29,
		ITYPE_PLUGIN			= 30,
		ITYPE_TEMPLATE			= 31,
		ITYPE_RESERVED			= 32,
		ITYPE_COMPONENT			= 33,
		ITYPE_COMPONENT_FOLDER	= 34,
		ITYPE_COMPONENT_REFERENCE	= 35,
		ITYPE_COMPONENT_RELATION	= 36,
		

		ITYPE_SEPARATOR			= 40, //used by DDS
		ITYPE_ROWBREAK			= 41, //used by DDS
		ITYPE_COLUMNBREAK		= 42, //used by DDS
		ITYPE_ENUM_BIT			= 43, //used by DDS
		ITYPE_STRING_LITERAL	= 44, //used by DDS
		ITYPE_CONST_INTEGER		= 45, //used by DDS
		ITYPE_CONST_FLOAT		= 46, //used by DDS
        ITYPE_CONST_UNSIGNED    = 47, //used by DDS
        ITYPE_CONST_DOUBLE      = 48, //used by DDS
		ITYPE_SELECTOR			= 49, //used by DDS
		ITYPE_LOCAL_PARAM		= 50, //used by DDS
		ITYPE_METH_ARGS			= 51, //used by DDS
		ITYPE_ATTR				= 52, //used by DDS
        ITYPE_BLOCK_XREF        = 53, //used by DDS
		ITYPE_MAX				= 54,	/* must be last in list */
	};

/*
 *	Special object type values
 */

#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130

 /* these additional ITYPES are used by resolve to build resolve trails */

#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202

#define AMORPHOUS_ARRAY_ITYPE   250

	//	variables Types.
	//
	// This enumeration gets it values from the �Variable_Type� sequence in section 5.8.32 of the FDI-2041 spec.
	// It is used as an attribute in the TYPE_SIZE class.
	enum VariableType
	{
		VT_DDS_TYPE_UNUSED	= 0,
		VT_INTEGER 			= 2,
		VT_UNSIGNED 		= 3,
		VT_FLOAT 			= 4,
		VT_DOUBLE 			= 5,
		VT_ENUMERATED 		= 6,
		VT_BIT_ENUMERATED 	= 7,
		VT_INDEX 			= 8,
		VT_ASCII 			= 9,
		VT_PACKED_ASCII 	= 10,
		VT_PASSWORD 		= 11,
		VT_BITSTRING 		= 12,
		VT_EDD_DATE			= 13,
		VT_TIME 			= 14,
		VT_DATE_AND_TIME 	= 15,
		VT_DURATION 		= 16,
		VT_EUC				= 17,
		VT_OCTETSTRING		= 18,
		VT_VISIBLESTRING	= 19,
		VT_TIME_VALUE		= 20,
		VT_OBJECT_REFERENCE	= 21,
		VT_BOOLEAN			= 22
	};

/*
 * Types of members.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the mem_type in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define	COLLECTION_MEM		1
#define	PARAM_MEM			2
#define	PARAM_LIST_MEM		3
#define	VAR_LIST_MEM		4
#define	RECORD_MEM			5
#define	CHARACTERISTIC_MEM  6	/* must be last item in list */

#define MEMBER_STRINGS  {\
	"",					/*required since items start at 1*/\
	"collection",\
	"parameter",\
	"parameter-list",\
	"variable-list",\
	"record",\
	"file",\
	"chart",\
	"source",\
	"graph",\
	"characteristic",\
	NULL}		/*array must be NULL terminated for searches*/
#define NO_ATTRTAG		0xffff

#define MEMBER2ATTRTAG { NO_ATTRTAG, COLLECTION_MEMBERS_ID, NO_ATTRTAG, NO_ATTRTAG,\
		VAR_LIST_MEMBERS_ID, RECORD_MEMBERS_ID, FILE_MEMBERS_ID, CHART_MEMBERS_ID, \
		SOURCE_MEMBERS_ID, GRAPH_MEMBERS_ID, BLOCK_CHARACTERISTIC_ID, NO_ATTRTAG}
/*
 * Classes of variables and methods.
 */
#pragma warning ( push )
#pragma warning ( disable : 4480 )		// Disable complaint about using an underlying type in an enum

	//	Class Types.
	//
	// This enumeration gets it values from the �Class_Specifier� sequence in section 5.8.7 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_VAR & FLAT_METHOD classes.
	enum ClassType : unsigned long long
	{
		CT_NONE				=	  0x000000,	
		CT_DIAGNOSTIC		=	  0x000001,
		CT_DYNAMIC			=	  0x000002,
		CT_SERVICE			=	  0x000004,
		CT_CORRECTION		=	  0x000008,
		CT_COMPUTATION		=	  0x000010,
		CT_ANALOG_INPUT		=	  0x000020,		
		CT_ANALOG_OUTPUT	=	  0x000040,
		CT_HART				=	  0x000080,
		CT_LOCAL_DISPLAY	=	  0x000100,
		CT_FREQUENCY_INPUT	=	  0x000200,	
		CT_DISCRETE_INPUT	=	  0x000400,	
		CT_DEVICE			=	  0x000800,
		CT_LOCAL_deprecated =	  0x001000,
		CT_INPUT			=	  0x002000,		// This is INPUT_BLOCK_CLASS in Legacy HART
		CT_OPERATE			=	  0x004000,		
		CT_ALARM			=	  0x008000,		
		CT_ANALOG_CHANNEL	=	  0x010000,		
		CT_CONTAINED		=	  0x020000,		
		CT_DIGITAL_INPUT	=	  0x040000,		 
		CT_DIGITAL_OUTPUT	=	  0x080000,		 
		CT_DISCRETE_OUTPUT	=	0x00100000,		 
		CT_FREQUENCY_OUTPUT	=	0x00200000,		
		CT_TUNE				=	0x00400000,		
		CT_OUTPUT			=	0x00800000,		
		CT_DISCRETE			=	0x01000000,		
		CT_BACKGROUND		=	0x02000000,		 
		CT_FREQUENCY		=	0x04000000,		
		CT_MODE				=	0x08000000,		 
		CT_RANGE			=	0x10000000,		 
		CT_OPTIONAL			=	0x20000000,		
		CT_TEMPORARY		=	0x40000000,		 
		CT_FACTORY			=	0x80000000,
		CT_LOCAL_A			=  0x100000000,
		CT_LOCAL_B			=  0x200000000,
		CT_CONSTANT			=  0x400000000,
		CT_IS_CONFIG		= 0x4000000000,		
		CT_SPECIALIST		= 0x8000000000		
		
	};

#pragma warning ( pop )

	//	Handling Specifiers.
	//
	// This enumeration gets it values from the �Handling_Specifier� sequence in section 5.8.14 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_VAR, FLAT_FILE, FLAT_GRID, FLAT_WAVEFORM & FLAT_BLOB classes.
	enum Handling
	{
		FDI_READ_HANDLING	= 0x01,
		FDI_WRITE_HANDLING	= 0x02
	};

	//	Mode Specifiers.
	//
	// This enumeration gets it values from the �Mode_BitEnum� sequence in section 5.8.42 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_VAR, FLAT_ARRAY & FLAT_RECORD classes.
	enum WriteMode
	{
		remote_output	= 0x01,
		remote_cascade	= 0x02,
		cascade			= 0x04,
		automatic		= 0x08,
		manual			= 0x10,
		locked			= 0x20,
		initialization	= 0x40,
		out_of_service	= 0x80
	};

	//	Time Scale
	// It is used as an attribute in the FLAT_VAR
	enum TimeScale
	{
        FDI_TIME_SCALE_NONE       = 0,
        FDI_TIME_SCALE_SECONDS    = 1,
        FDI_TIME_SCALE_MINUTES    = 2,
        FDI_TIME_SCALE_HOURS      = 3
	};

	//	Protocol Types.
	//
	// This enumeration gets it values from the �Protocol� sequence in section 5.8.35 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_COMPONENT, FLAT_COMPONENT_FOLDER & FLAT_COMPONENT_REFERENCE classes.
	enum Protocol : unsigned char// size 1
	{
		protocol_none	=	127, // This is used if atribute value is not specified.
		ff				= 0,
		hart			= 1,
		profibus_dp		= 2,
		profibus_pa		= 3,
		profinet		= 4,
		isa100			= 5
	};

	//	Relation Type.
	//
	// This enumeration gets it values from the �Relation_Type� sequence in section 5.8.37 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_COMPONENT_RELATION class.
	enum RelationType : unsigned char // size 1
	{
		RELATIONTYPE_NONE	= 127, // This is used if atribute value is not specified.
		CHILD_COMPONENT		= 0,
		PARENT_COMPONENT	= 1,
		SIBLING_COMPONENT	= 2,
		NEXT_COMPONENT		= 3,
		PREV_COMPONENT		= 4
	};

	//	ByteOrder.
	//
	// This enumeration gets it values from the �Byte_Order� sequence in section 5.8.40 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_COMPONENT class.
	enum ByteOrder : unsigned char // size 1
	{
		No_Order		= 0,
		Big_Endian		= 1,
		Little_Endian	= 2
	};

	//	Classification.
	//
	// This enumeration gets it values from the �Classification� enumeration in section 5.8.36 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_COMPONENT, FLAT_COMPONENT_FOLDER & FLAT_COMPONENT_REFERENCE classes.
	enum Classification : unsigned char// size 1
	{
		classification_none						= 127, // This is used if atribute value is not specified.
		actuator								= 0,									
		actuator_electro_pneumatic				= 1,				
		actuator_electric						= 2,				
		actuator_hydraulic 						= 3,				
		converter 								= 4,				
		controller								= 5,				
		discrete_in 							= 6,				
		discrete_out							= 7,				
		electrical_distribution					= 8,				
		electrical_distribution_power_monitoring= 9,
		frequency_converter 					= 10,
		indicator 								= 11,
		network 								= 12,
		network_communication_service_provider 	= 13,
		network_component 						= 14,	
		network_component_wirelessadapter 		= 15,	
		network_connection_point 				= 16,	
		remoteio 								= 17,	
		recorder 								= 18,	
		sensor									= 19,	
		sensor_acoustic 						= 20,	
		sensor_analytic 						= 21,	
		sensor_analytic_gas 					= 22,	
		sensor_analytic_liquid 					= 23,	
		sensor_concentration 					= 24,	
		sensor_density 							= 25,	
		sensor_density_radiometric 				= 26,	
		sensor_flow								= 27,	
		sensor_flow_coriolis 					= 28,	
		sensor_flow_electro_magnetic 			= 29,	
		sensor_flow_mechanical 					= 30,	
		sensor_flow_radiometric 				= 31,	
		sensor_flow_thermal 					= 32,	
		sensor_flow_ultrasonic 					= 33,	
		sensor_flow_vortex_counter 				= 34,	
		sensor_level 							= 35,	
		sensor_level_buoyancy 					= 36,	
		sensor_level_capacitive 				= 37,	
		sensor_level_echo 						= 38,	
		sensor_level_hydrostatic 				= 39,	
		sensor_level_radiometric 				= 40,	
		sensor_multivariable 					= 41,	
		sensor_position 						= 42,	
		sensor_pressure 						= 43,	
		sensor_temperature 						= 44,	
		semiconductor 							= 45,	
		switchgear 								= 46,	
		universal 								= 47
	};


	//	BaseClass.
	//
	// This enumeration gets it values from the "Base_Class" enumeration in the
    // �Status_Class� sequence in section 5.8.12 of the FDI-2041 spec.
	// It is used as an attribute in the STATUS_CLASS class.
	enum BaseClass
	{
		FDI_HARDWARE_STATUS				=	0,
		FDI_SOFTWARE_STATUS				=	1,
		FDI_PROCESS_STATUS				=	2,
		FDI_MODE_STATUS					=	3,
		FDI_DATA_STATUS					=	4,
		FDI_MISC_STATUS					=	5,
		FDI_EVENT_STATUS				=	6,
		FDI_STATE_STATUS				=	7,
		FDI_SELF_CORRECTING_STATUS		=	8,
		FDI_CORRECTABLE_STATUS			=	9,
		FDI_UNCORRECTABLE_STATUS		=	10,
		FDI_SUMMARY_STATUS				=	11,
		FDI_DETAIL_STATUS				=	12,
		FDI_MORE_STATUS					=	13,
		FDI_COMM_ERROR_STATUS			=	14,
		FDI_IGNORE_IN_HANDHELD      	=	15,
		FDI_BAD_OUTPUT_STATUS			=	16,
		FDI_IGNORE_IN_HOST      		=	17,
		FDI_ERROR_STATUS				=	18,
		FDI_WARNING_STATUS				=	19,
		FDI_INFO_STATUS					=	20,
		FDI_DV_STATUS					=	21,
		FDI_TV_STATUS					=   22,
		FDI_AO_STATUS					=	23,
		FDI_ALL_STATUS					=	24,
        
        FDI_UNDEFINED_STATUS            =   25, // Initial value should always be overwritten
        FDI_BASE_CLASS_ITEM_MAX       =   26, // Maximum number of items in the enum
                                                // Used when creating a list of resonable and sufficient length
	};

	 

    

	//	OutputClass.
	//
	// This enumeration gets it values from the �Output_Class� sequence in section 5.8.12 of the FDI-2041 spec.
	// It is used as an attribute in the OUTPUT_STATUS class.
	enum ModeAndReliability
	{
		FDI_MR_AUTO_GOOD	        =	0,
		FDI_MR_AUTO_BAD	        =	1,
		FDI_MR_MANUAL_GOOD	        =	3,
		FDI_MR_MANUAL_BAD	        =	4,

        FDI_MR_UNUSED           =   5, // Initial value may or may not be overwritten
	};


/*
 * Variable Object Reference values
 */

#define OBJ_REF_ROOT		0
#define OBJ_REF_PARENT		1
#define OBJ_REF_CHILD		2
#define OBJ_REF_SELF		3
#define OBJ_REF_NEXT		4
#define OBJ_REF_PREV		5
#define OBJ_REF_FIRST		6
#define OBJ_REF_LAST		7


/*
 * COMMAND operations.
 */
	enum CommandOperation
	{
		FDI_COMMAND_OPERATION	= 0,
		FDI_READ_OPERATION		= 1,
		FDI_WRITE_OPERATION		= 2,
		FDI_DATA_EX_OPERATION	= 3	// New for PROFIBUS
	};

/*
 * Data item types.
 */
	enum DataItemType
	{
		FDI_DATA_CONSTANT			= 0,	// iconst
		FDI_DATA_REFERENCE 			= 1,	// ref
		FDI_DATA_REF_FLAGS			= 2,	// ref with DATA_ITEM.flags
		FDI_DATA_REF_WIDTH			= 3,	// ref with DATA_ITEM.width
		FDI_DATA_REF_FLAGS_WIDTH	= 4,	// ref with DATA_ITEM.flags & width
		FDI_DATA_FLOATING 			= 5,	// fconst
		FDI_DATA_STRING				= 6,	// str
		FDI_DATA_UNSIGNED			= 7,	// uconst
		FDI_DATA_DOUBLE				= 8		// dconst
	};

/* 
 * Response code types.
 */
	enum ResponseCodeType
	{
		FDI_SUCCESS_RSPCODE 			= 0,
		FDI_MISC_WARNING_RSPCODE 		= 1,
		FDI_DATA_ENTRY_WARNING_RSPCODE 	= 2,
		FDI_DATA_ENTRY_ERROR_RSPCODE 	= 3,
		FDI_MODE_ERROR_RSPCODE 			= 4,
		FDI_PROCESS_ERROR_RSPCODE 		= 5,
		FDI_MISC_ERROR_RSPCODE 			= 6
	};


/*
 * Command data item flags.
 */

#define INFO_DATA_ITEM 					0x0001
#define INDEX_DATA_ITEM 				0x0002

/*
 * data item flags.
 */

#define WIDTH_PRESENT 					0x8000

	//	MenuItemQual.
	//
	// This enumeration gets it values from the �Menu_Item� sequence in section 5.8.19 of the FDI-2041 spec.
	// It is used as an attribute in the MENU_ITEM class.
	enum MenuItemQual
	{
		FDI_READ_ONLY_ITEM		= 0x01,
		FDI_DISPLAY_VALUE_ITEM	= 0x02,
		FDI_REVIEW_ITEM			= 0x04,
		FDI_NO_LABEL_ITEM		= 0x08,
		FDI_NO_UNIT_ITEM		= 0x10,
		FDI_INLINE_ITEM			= 0x20,
		FDI_HIDDEN_ITEM			= 0x40,
		FDI_ALIGN_RIGHT			= 0x80,
		FDI_ALIGN_LEFT			= 0x100
	};


	//	MenuStyle.
	//
	// This enumeration gets it values from the �Style� enumeration in section 5.8.30 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_MENU class.
	enum MenuStyle
	{
		FDI_WINDOW_STYLE_TYPE      = 0,
        FDI_DIALOG_STYLE_TYPE      = 1,
		FDI_PAGE_STYLE_TYPE        = 2,
		FDI_GROUP_STYLE_TYPE       = 3,
        FDI_MENU_STYLE_TYPE        = 4,
        FDI_TABLE_STYLE_TYPE       = 5,
        FDI_NO_STYLE_TYPE          = 6
	};

	//
	//	Boolean
	//
	enum Boolean
	{
		False	= 0,
		True	= 1
	};

/*
 * Menu ENTRY type
 *		New for PROFIBUS
 */
#define ROOT_ENTRY_TYPE			1

/*
 * Menu PURPOSE types
 *		New for PROFIBUS
 */
#define CATALOG_PURPOSE_TYPE		0x0001
#define DIAGNOSE_PURPOSE_TYPE		0x0002
#define LOAD_TO_APP_PURPOSE_TYPE	0x0004
#define LOAD_TO_DEV_PURPOSE_TYPE	0x0008
#define MENU_PURPOSE_TYPE			0x0010
#define PROCESS_VALUE_PURPOSE_TYPE	0x0020
#define SIMULATION_PURPOSE_TYPE		0x0040
#define TABLE_PURPOSE_TYPE			0x0080


	//	Access.
	//
	// This enumeration gets it values from the �Access_Specifier� sequence in section 5.8.1 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_MENU & FLAT_METHOD classes.
	enum Access
	{
		ONLINE_ACCESS	= 0,
		OFFLINE_ACCESS	= 1,
		UNKNOWN_ACCESS  = 2
	};

/*
 * Method type flags
 */
#define SCALING_TYPE_METHOD			0x00000001
#define USER_TYPE_METHOD			0x00000010

	//	MethodType.
	//
	// This enumeration gets it values from the �Method_Type� enumeration in section 5.8.21 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_METHOD class.
	enum MethodType{

		MT_VOID		= 0,
		MT_INT_8	= 1,	/* signed char  */
		MT_INT_16	= 2, 	/* short        */
		MT_INT_32	= 3,	/* int & long   */
		MT_FLOAT	= 4,	/* FLOAT    */
		MT_DOUBLE	= 5,	/* DOUBLE_FLOAT */
		MT_UINT_8 	= 6,
		MT_UINT_16 	= 7,
		MT_UINT_32	= 8,
		MT_INT_64 	= 9,/* currently unsupported */
		MT_UINT_64	= 10,	/* currently unsupported */
		MT_DDSTRING	= 11,
		MT_DD_ITEM  = 12
	};


/*
 * Method parameter details
 */

#define ARRAYFLAG   	1	/* isArray - is declared '[]' or '[' chain-expr ']' */
#define REFECFLAG		2	/* isReference - ( has a '&' in front of it */
#define CONSTFLAG		4	/* isConst - for posterity */


	//	ChartType.
	//
	// This enumeration gets it values from the �Chart_Type� sequence in section 5.8.6 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_CHART class.
	enum ChartType
	{
		CTYPE_STRIP		= 0,
		CTYPE_SWEEP		= 1,
		CTYPE_SCOPE		= 2,
		CTYPE_HORIZ_BAR	= 3,
		CTYPE_VERT_BAR	= 4,
		CTYPE_GAUGE		= 5
	};


	//	DisplaySize .
	//
	// This enumeration gets it values from the �Display_Size� enumeration in section 5.8.28 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_VAR, FLAT_CHART, FLAT_GRAPH & FLAT_GRID classes.
	enum DisplaySize
	{
		DISPSIZE_XX_SMALL	= 0,
		DISPSIZE_X_SMALL	= 1,
		DISPSIZE_SMALL		= 2,
		DISPSIZE_MEDIUM		= 3,
		DISPSIZE_LARGE		= 4,
		DISPSIZE_X_LARGE	= 5,
		DISPSIZE_XX_LARGE	= 6,
		DISPSIZE_XXX_SMALL	= 7
	};


	//	WaveformType.
	//
	// This enumeration gets it values from the �Waveform_Type� enumeration in section 5.8.34 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_WAVEFORM class.
	enum WaveformType
	{
		FDI_XY_WAVEFORM_TYPE	= 0,
		FDI_YT_WAVEFORM_TYPE	= 1,
		FDI_HORZ_WAVEFORM_TYPE	= 2,
		FDI_VERT_WAVEFORM_TYPE	= 3
	};


	// LineType.
	//
	// This enumeration gets it values from the �Line_Type� sequence in section 5.8.16 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_SOURCE and FLAT_WAVEFORM classes
	enum LineType
	{
		FDI_LOWLOW_LINETYPE			= 2,
		FDI_LOW_LINETYPE			= 3,
		FDI_HIGH_LINETYPE			= 4,
		FDI_HIGHHIGH_LINETYPE		= 5,
		FDI_TRANSPARENT_LINETYPE	= 6,
		FDI_DATA0_LINETYPE			= 16,
		FDI_DATA1_LINETYPE			= 17,
		FDI_DATA2_LINETYPE			= 18,
		FDI_DATA3_LINETYPE			= 19,
		FDI_DATA4_LINETYPE			= 20,
		FDI_DATA5_LINETYPE			= 21,
		FDI_DATA6_LINETYPE			= 22,
		FDI_DATA7_LINETYPE			= 23,
		FDI_DATA8_LINETYPE			= 24,
		FDI_DATA9_LINETYPE			= 25
	};


	// Orientation.
	//
	// This enumeration gets it values from the �Orientation� sequence in section 5.8.23 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_GRID class.
	enum Orientation
	{
		FDI_ORIENT_HORIZ	= 0,
		FDI_ORIENT_VERT		= 1
	};


	// Scaling.
	//
	// This enumeration gets it values from the �Axis_Scaling� sequence in section 5.8.3 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_AXIS class.
	enum Scaling
	{
		FDI_LINEAR_SCALE	= 0,
		FDI_LOG_SCALE		= 1
	};

/*
 * HALFPOINT types.
 */

#define X_VALUE				1
#define Y_VALUE				2

	// BlockBType.
	//
	// This enumeration gets it values from the �Block_B_Type� sequence in section 5.8.4 of the FDI-2041 spec.
	// It is used as an attribute in the FLAT_BLOCK_B class.
	enum BlockBType
	{
		TRANSDUCER_BLOCK_TYPE	= 0,
		PHYSICAL_BLOCK_TYPE		= 1,
		FUNCTION_BLOCK_TYPE		= 2,
		UNKNOWN_BLOCK_TYPE		= 3
	};


/* SOURCE constants */
#define LINE_COLOR_DEFAULT  0xffffffff


///////////////////////////////////////////////////////////////////////////////

/*
 * Table Identifiers for DDOD Directory Objects.
 * These are also used as bit positions for the
 * Directory Table Masks defined below.
 */

#define	BLOCK_NAME_SIZE		32	/* This is the max size of a block name */

/* DEVICE DIRECTORY tables */
#ifdef _OLD_CODE
	#define	BLK_TBL_ID			 			0
	#define	ITEM_TBL_ID					 	1
	#define	STRING_TBL_ID				 	2
	#define	DICT_REF_TBL_ID				 	3
	#define	LOCAL_VAR_TBL_ID				4
	#define IMAGE_TBL_ID					5
	#define DICT_STRING_TBL_ID				6
	#define SYMBOL_TBL_ID					7
	#define	MAX_DEVICE_TBL_ID			 	8

	/* BLOCK DIRECTORY tables */

	#define	BLK_ITEM_TBL_ID				 	0
	#define	BLK_ITEM_NAME_TBL_ID		 	1
	#define PARAM_TBL_ID				 	2
	#define	PARAM_MEM_TBL_ID			 	3
	#define	PARAM_MEM_NAME_TBL_ID			4
	#define PARAM_ELEM_TBL_ID				5
	#define	CHAR_MEM_TBL_ID				 	6	// In PROFIBUS Binary, but not used
	#define CHAR_MEM_NAME_TBL_ID			7	// In PROFIBUS Binary, but not used
	#define	REL_TBL_ID					 	8
	#define UPDATE_TBL_ID					9
	#define COMMAND_TBL_ID					10
	#define CRIT_PARAM_TBL_ID				11
	#define	MAX_BLOCK_TBL_ID				12	/* must be last in list */
#endif

// Default refresh action cycle time
#define DEFAULT_REFRESH_CYCLE_TIME		1000


/* BLOCK DIRECTORY table masks */

#define	BLK_ITEM_TBL_MASK				(1<<BLK_ITEM_TBL_ID)
#define	BLK_ITEM_NAME_TBL_MASK			(1<<BLK_ITEM_NAME_TBL_ID)
#define PARAM_TBL_MASK					(1<<PARAM_TBL_ID)
#define	PARAM_MEM_TBL_MASK				(1<<PARAM_MEM_TBL_ID)
#define PARAM_MEM_NAME_TBL_MASK			(1<<PARAM_MEM_NAME_TBL_ID)
#define PARAM_ELEM_TBL_MASK				(1<<PARAM_ELEM_TBL_ID)
#define	CHAR_MEM_TBL_MASK				(1<<CHAR_MEM_TBL_ID)
#define CHAR_MEM_NAME_TBL_MASK			(1<<CHAR_MEM_NAME_TBL_ID)
#define	REL_TBL_MASK					(1<<REL_TBL_ID)
#define	UPDATE_TBL_MASK					(1<<UPDATE_TBL_ID)
#define COMMAND_TBL_MASK				(1<<COMMAND_TBL_ID)
#define CRIT_PARAM_TBL_MASK				(1<<CRIT_PARAM_TBL_ID)

#define PARAM_LIST_TBL_MASK					(1<<PARAM_LIST_TBL_ID)
#define PARAM_LIST_MEM_TBL_MASK				(1<<PARAM_LIST_MEM_TBL_ID)
#define PARAM_LIST_MEM_NAME_TBL_MASK		(1<<PARAM_LIST_MEM_NAME_TBL_ID)


#define BLOCK_TBL_MASKS			BLK_ITEM_TBL_MASK | \
								BLK_ITEM_NAME_TBL_MASK | \
								PARAM_TBL_MASK | \
								PARAM_MEM_TBL_MASK | \
								PARAM_MEM_NAME_TBL_MASK | \
								PARAM_ELEM_TBL_MASK | \
								CHAR_MEM_TBL_MASK | \
								CHAR_MEM_NAME_TBL_MASK | \
								REL_TBL_MASK | \
								UPDATE_TBL_MASK | \
								COMMAND_TBL_MASK | \
								CRIT_PARAM_TBL_MASK | \
								PARAM_LIST_TBL_MASK | \
								PARAM_LIST_MEM_TBL_MASK | \
								PARAM_LIST_MEM_NAME_TBL_MASK


}

