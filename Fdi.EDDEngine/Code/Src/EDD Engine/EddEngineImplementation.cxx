#include "stdafx.h"

#include <vector>

#include "EddEngineImplementation.h"


	int EddEngineImplementation::Open(const wchar_t* filename)
	{
		const wchar_t* dictionaryPath = L"<CONFIG><HARTPath>.</HARTPath><FFPath>.</FFPath></CONFIG>";

		nsEDDEngine::EDDEngineFactoryError errorCode = nsEDDEngine::EDDE_SUCCESS; 

		paramCache = new ParamCache;

		engine.reset(nsEDDEngine::EDDEngineFactory::CreateDeviceInstance(filename, paramCache, nullptr, nullptr, dictionaryPath, &errorCode));

		if (errorCode == nsEDDEngine::EDDE_SUCCESS)
		{
			paramCache->SetEddEngine(engine.get());

			nsEDDEngine::EDDFileHeader header;
			engine->GetEDDFileHeader(&header);
			if (header.edd_profile == nsEDDEngine::PROFILE_FF)
				addFFBlocks();
		}
		else
		{
			engine.reset(nullptr);
		}

		return (int)errorCode;
	}

	void EddEngineImplementation::GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader)
	{
		engine->GetEDDFileHeader(pEDDFileHeader);
	}

  int EddEngineImplementation::GetItem(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::AttributeNameSet* pAttributeNameSet, const wchar_t* lang_code, nsEDDEngine::FDI_GENERIC_ITEM* pGenericItem)
  {
		return engine->GetItem(iBlockInstance, 0, pItemSpec, pAttributeNameSet, lang_code, pGenericItem);
	}

	nsEDDEngine::FLAT_DEVICE_DIR* EddEngineImplementation::GetDeviceDir()
	{
		nsEDDEngine::FLAT_DEVICE_DIR* myDir;
		auto ret = engine->GetDeviceDir(&myDir);
		if (ret != 0)
			return nullptr;
		return myDir;
	}


	const wchar_t* EddEngineImplementation::GetSymbolNameFromItemId(ITEM_ID item_id)
	{
		int ret = 0;
		int buffersize = 50;

		wchar_t* buffer = new wchar_t[buffersize];

		for (;;)
		{
			ret = engine->GetSymbolNameFromItemId(item_id, buffer, buffersize);
			if (ret == (-1700 - 3)) // resize the buffer
			{
				delete [] buffer;
				buffersize += 100;
				buffer = new wchar_t[buffersize];
			}
			else if (ret == 0)
			{
				// success
				break;
			}
			else
			{
				delete [] buffer;
				return nullptr;
			}

		}

		return buffer;
	}

	void EddEngineImplementation::addFFBlocks()
	{
		nsEDDEngine::FLAT_DEVICE_DIR* dir;
		int returnCode = engine->GetDeviceDir(&dir);

		// we use the item table and not the block table, because I don't know if the block table works before adding all the blocks...

		// first, get number of blocks
		int numberOfBlocks = 0;
		for (int n = 0; n < dir->item_tbl.count; ++n)
		{
			if (dir->item_tbl.list[n].item_type == nsEDDEngine::ITYPE_BLOCK)
				++numberOfBlocks;
		}

		if (numberOfBlocks > 0)
		{
			// second, collect all blocks in a vector
			std::vector<nsEDDEngine::CFieldbusBlockInfo> blockInfo(numberOfBlocks);

			int x = 0;
			for (int i = 0; i != dir->item_tbl.count; ++i)
			{
				if (dir->item_tbl.list[i].item_type != nsEDDEngine::ITYPE_BLOCK)
					continue;

				// I don't know where to get the block type from => assume function block => capability file
				blockInfo[x].m_BlockType = nsEDDEngine::FUNCTION_BLOCK;

				// I don't know what this is => uses multiples of 1000 => capability file
				blockInfo[x].m_usObjectIndex = (i + 1) * 1000;

				// the item id
				blockInfo[x].m_CharRecord.m_ulDDItemId = dir->item_tbl.list[i].item_id;

				++x;
			}

			// now, add all the blocks
			std::vector<int> instances(numberOfBlocks);
			engine->AddFFBlocks(&blockInfo[0], &instances[0], numberOfBlocks);
		}
}

