/**
 *              Device Description Services Rel. 4.2
 *              Copyright 1994-1996 - Fieldbus Foundation
 *              All rights reserved.
 */

/*
 *      @(#)int_lib.h	30.3  30  14 Nov 1996
 *
 *      int_lib.h - method interpreter exposed interface
 */

#ifndef INT_LIB_H
#define INT_LIB_H

#include "comm.h"
#include "env_info.h"

#ifdef __cplusplus
    extern "C" {
#endif

extern long mi_exec_user_meth P((ENV_INFO, ITEM_ID));
extern long mi_exec_edit_meth P((ENV_INFO, ITEM_ID, P_REF *));
extern long mi_exec_scaling_meth P((ENV_INFO *, ITEM_ID));

#ifdef __cplusplus
    }
#endif

#endif /* INT_LIB_H */
