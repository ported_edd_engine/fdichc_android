#include "StdAfx.h"
#include "EDDEngineLogger.h"


CEDDEngineLogger::CEDDEngineLogger()
{	
	m_pMethodLogFile = nullptr;
	m_pMIErrorLogFile = nullptr;
	//m_eProtocolType = eProtocolType;
//	m_bIsActionMethod = bIsActionMethod;
//	m_bIsLogFileCreated = false;
}

CEDDEngineLogger::~CEDDEngineLogger(void)
{

}

void CEDDEngineLogger::OpenLogFile(ProtocolType eProtocolType, bool bIsActionMethod)
{
	//create methodLog.txt FILE pointer
	wchar_t path[MAX_PATH];
	wchar_t filename[MAX_PATH];
	if ((_wgetcwd(path, MAX_PATH) != NULL))
	{
		if (wcsstr(path, _T("ole")) == NULL)
		{
			//Not running unit test
			if (wcsstr(path, _T("\\Legacy\\code")) != NULL)
			{
				//Method runs from Emerson method interpreter GUI in both main source branch and release branch
				wcscpy_s(filename, _T("..\\src\\ole\\MItest\\"));
			}
		}
		else
		{
			//Unit test runs from Emerson method interpreter
			wcscpy_s(filename, _T("..\\..\\..\\..\\MItest\\"));
		}

		switch (eProtocolType)
		{
		case nsEDDEngine::HART:
			wcscat_s(filename, _T("Hart\\"));
			break;
		case nsEDDEngine::PROFIBUS:
		case nsEDDEngine::PROFIBUS_PN:
		case nsEDDEngine::GPE:
		case nsEDDEngine::COMSERVER:
			wcscat_s(filename, _T("Profibus\\"));
			break;
		case nsEDDEngine::FF:
		case nsEDDEngine::ISA100:
			wcscat_s(filename, _T("Fieldbus\\"));
			break;
		default:
			return;
			break;
		}

		if (bIsActionMethod == true)
		{
			wcscat_s(filename, _T("actionLog.txt"));
		}
		else
		{
			wcscat_s(filename, _T("methodLog.txt"));
		}
		//Opens an empty file for writing. If the given file exists, its contents are destroyed
		if (_wfopen_s(&m_pMethodLogFile, filename, L"wt, ccs=UTF-8"))
		{
			//Problem opening the log file
			m_pMethodLogFile = nullptr;
		}

		

		if(_wfopen_s(&m_pMIErrorLogFile, L"C:\\MILog.txt", L"a+")) 
		{
			m_pMIErrorLogFile = nullptr;
		}
		//m_bIsLogFileCreated = true;
	}
}

void CEDDEngineLogger::CloseLogFile()
{
	if(m_pMethodLogFile)
	{
		fclose(m_pMethodLogFile);
		m_pMethodLogFile = nullptr;
	}
	
	if(m_pMIErrorLogFile)
	{
		fclose(m_pMIErrorLogFile);
		m_pMIErrorLogFile = nullptr;
	}
}

bool CEDDEngineLogger::ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory)
{
	(void)eLogSeverity;
	bool rc = false;

	if (0 == wcscmp(sCategory, L"MI_ExecutionLog") || (0 == wcscmp(sCategory, L"MI")))
	{
		rc = true;
	}
	

	return rc;
}
void CEDDEngineLogger::Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory)
{
	if (ShouldLog(eLogSeverity, sCategory))
	{
		if(0 == wcscmp(sCategory, L"MI_ExecutionLog")) 
		{
			if (m_pMethodLogFile != NULL)
			{
				fwprintf_s(m_pMethodLogFile, L"%s", sMessage);
				fflush(m_pMethodLogFile);
			}
		}
		if(0 == wcscmp(sCategory, L"MI")) 
		{
			if (m_pMIErrorLogFile != NULL)
			{
			wchar_t buffer[100] = {0};
			int writecnt;

			_wstrdate_s(buffer);					// Output Date
			writecnt = fwprintf_s(m_pMIErrorLogFile, L"%s ", buffer);
			_wstrtime_s(buffer);
			writecnt = fwprintf_s(m_pMIErrorLogFile, L"%s: ", buffer);	// Output Time
			writecnt = fwprintf_s(m_pMIErrorLogFile, L"%s", sMessage);		// Output string

			fflush(m_pMIErrorLogFile);
			}
		}
	}
}

