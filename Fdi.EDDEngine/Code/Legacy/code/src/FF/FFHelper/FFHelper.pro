#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T13:16:12
#
#-------------------------------------------------
QT       -= gui

TARGET = FFHelper
TEMPLATE = lib
CONFIG += staticlib
QMAKESPEC=linux-g++-32
QMAKE_CXXFLAGS += -std=gnu++11
    QMAKE_CC = $$QMAKE_CXX
    QMAKE_CFLAGS  = $$QMAKE_CXXFLAGS
QMAKE_CXXFLAGS += -Wno-unused-parameter -Wno-unknown-pragmas
QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter

DEFINES += FFHELPER_LIBRARY
DEFINES += UNICODE
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += ../../ServerProjects/NtcSpecies/NtcSpeciesLog.cpp \
    app_pnic.c \
    cmtblacc.c \
    cm_comm.c \
    cm_dds.c \
    cm_init.c \
    cm_rod.c \
    cm_tbl.c \
    ConnectionMgr.cpp \
    dds_cm.c \
    DeviceTypeMgr.cpp \
    dev_type.cpp \
    dict.cpp \
    EddEngineLog.cpp \
    ff_sup.c \
    pc_lib.c \
    rmalloc.c \
    sm_strfn.c \

HEADERS += ../../../inc/FF/AmsGenFiles/std.h \
    ../../../inc/FF/AMSServer/bi_codes.h \
    ../../../inc/FF/FFHelper/cm_lib.h \
    ../../../inc/FF/FFHelper/cm_rod.h \
    ../../../inc/FF/FFHelper/comm.h \
    ../../../inc/FF/FFHelper/ConnectionMgr.h \
    ../../../inc/FF/FFHelper/DDOD_Header.h \
    ../../../inc/FF/FFHelper/DeviceTypeMgr.h \
    ../../../inc/FF/FFHelper/dev_type.h \
    ../../../inc/FF/FFHelper/EddEngineLog.h \
    ../../../inc/FF/FFHelper/int_diff.h \
    ../../../inc/FF/FFHelper/int_lib.h \
    ../../../inc/FF/FFHelper/int_meth.h \
    ../../../inc/FF/FFHelper/panic.h \
    ../../../inc/FF/FFHelper/pc_lib.h \
    ../../../inc/FF/FFHelper/pc_loc.h \
    ../../../inc/FF/FFHelper/rmalloc.h \
    ../../../inc/FF/FFHelper/RodMgr.h \
    ../../../inc/FF/FFHelper/stdinc.h \
    ../../../inc/FF/FFHelper/sysparam.h \
    ../../../inc/FF/FFHelper/tst_cmn.h \
    ../../../inc/Inf/NtcSpecies/BssProgLog.h \
    ../../../inc/Inf/NtcSpecies/ntcassert.h \
    ../../../inc/Inf/NtcSpecies/Ntcassert2.h \
    dict.h \

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DESTDIR = ../../../lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../../../inc/FF/AmsGenFiles
INCLUDEPATH += $$PWD/../../../inc/FF/AMSDds
INCLUDEPATH += $$PWD/../../../inc/FF/FFHelper
INCLUDEPATH += $$PWD/../../../inc/FF
INCLUDEPATH += $$PWD/../../../inc/
INCLUDEPATH += $$PWD/../../../../../Interfaces/EDD_Engine_Interfaces
INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Android
}
else {
    INCLUDEPATH += $$PWD/../../../../../Src/EDD_Engine_Linux
}

unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Common
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR
unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Common.a
