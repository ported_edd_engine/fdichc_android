/*
 *	@(#) $Id: ddi_conv.cpp,v 1.2 2012/09/26 22:28:43 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 *
 *  This file contains all of the functions for DDI convenience
 */


#include "stdafx.h"
#include "ddi_lib.h"
#include "ddi_tab.h"
#include "dds_tab.h"
#ifdef DDSTEST
#include "tst_fail.h"
#endif
#include "FDI/FDIEDDEngine/DDSSupport.h"
#include "ManualUnitLookup.h"

#define SYM_response_code_ID									(ITEM_ID) 150
#define SYM_device_status_ID									(ITEM_ID) 151
#define SYM_comm_status_ID										(ITEM_ID) 152

extern int get_ddtype(ENV_INFO *env_info);

/*********************************************************************
 *
 *	Name: conv_block_spec
 *	ShortDesc: convert the block_specifier
 *
 *	Description:
 *		ddi_conv_specifiers converts the block_specifier into a
 *		block_handle if the type is DDI_BLOCK_TAG.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *
 *	Outputs:
 *		block_handle:	the block_handle
 *
 *	Returns:DDS_SUCCESS, DDI_INVALID_PARAM
 *		returns from other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
conv_block_spec(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
BLOCK_HANDLE   *block_handle)
{
	int             rc;	/* return code */

	switch (block_spec->type) {

	case DDI_BLOCK_TAG:
		rc = tr_block_tag_to_block_handle(env_info, block_spec->block.tag,
			block_handle);
		if (rc != DDS_SUCCESS) {

			return rc;
		}
		return DDS_SUCCESS;

	case DDI_BLOCK_HANDLE:
		*block_handle = block_spec->block.handle;
		return DDS_SUCCESS;

	default:
		return DDI_INVALID_TYPE;

	}
}


/*********************************************************************
 *
 *	Name: convert_param_spec
 *	ShortDesc: convert the param_specifer structure into a bint offset
 *
 *	Description:
 *		convert_param_spec takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the Block Item Name Table offset
 *
 *	Inputs:
 *		block_handle:	the block handle
 *		param_spec:	    a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		bint_offset:	a pointer to the block item name table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_param_spec(
BLOCK_HANDLE         /*block_handle*/,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
nsEDDEngine::BLK_TBL_ELEM        *bt_elem,
int                 *bint_offset)
{

	int             rc;	/* return code */


	/*
	 * Convert the param specifier to a block item name table offset
	 */

	switch (param_spec->eType) {

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID:
	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_SI:
		rc = tr_id_to_bint_offset(bt_elem, param_spec->id, bint_offset);
		break;

	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET:
	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET_SI:
		rc = tr_param_to_bint_offset(bt_elem, param_spec->param,
			bint_offset);
		break;

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX:
		rc = DDI_INVALID_PARAM;
		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: conv_block_and_param_spec
 *
 *	Description:
 *       convert a block specifier to a block handle and a
 *       parameter specifier to a block item name table offset
 *
 *	Inputs:
 *		block_spec: a pointer to the block specifier
 *      param_spec: a pointer to the parameter specifier
 *
 *	Outputs:
 *		block_handle:	a block handle
 *		bt_elem:		a pointer to a pointer of block table element
 *		bint_offset:	an index into the Block Item Name Table
 *
 *	Returns:
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
conv_block_and_param_spec(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
BLOCK_HANDLE   *block_handle,
nsEDDEngine::BLK_TBL_ELEM   **bt_elem,
int            *bint_offset)
{
	int             rc;	/* return code */

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_spec(env_info, block_spec, block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, *block_handle, bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = convert_param_spec(*block_handle, param_spec, *bt_elem, bint_offset);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_param_spec fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnectionMgr->ds_dd_block_load(env_info, *block_handle);
			if (!rc) {

				rc = convert_param_spec(*block_handle,
					param_spec, *bt_elem, bint_offset);

				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}

static int conv_param_spec_to_itct_offset( 
	ENV_INFO	*env_info,
	nsEDDEngine::OP_REF_INFO_LIST *OpRefInfoList,
	bool	bItemIdOnly,
	int *cmd_table_offset,
    bool *pbModifiedIndex)
{	
	int rc = 0;
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

    *pbModifiedIndex = false;   // Start out assuming that we don't need to modify the index

	// Get pointer to the Flat Device Dir
	nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir = nullptr ;

	rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}

	//convert RefList to RESOLVED_REFERENCE
	nsEDDEngine::RESOLVED_REFERENCE resolved_ref;
	
	
	// Determine how many RESOLVED_REFERENCE entries there will be
	if (bItemIdOnly || (OpRefInfoList->list[0].type == VARIABLE_ITYPE) )
	{
		resolved_ref.size = 1;
	}
	else
	{
		resolved_ref.size = OpRefInfoList->count + 1;
	}


	unsigned long* tempResolvedRef = new unsigned long[resolved_ref.size];

	// If we need to simplify the Resolved Ref, start at the beginning.
	int iOpRefIdx = -1;

	do
	{
		// Fill the RESOLVED_REFERENCE from my OP_REF_INFO_LIST
		tempResolvedRef[0] = OpRefInfoList->list[0].id;

		for(int j=1; j < resolved_ref.size; j++)
		{
			tempResolvedRef[j] = OpRefInfoList->list[j-1].member;
		}

		resolved_ref.Set( tempResolvedRef, resolved_ref.size );

		nsEDDEngine::COMPLEX_ITEM_TO_CMD_TBL *pComplexItemToCommandTbl = &flat_device_dir->complex_item_to_cmd_tbl;

		rc = DDI_NO_COMMANDS_FOR_PARAM;
		int tableCount = pComplexItemToCommandTbl->count;
		for( int nIndex = 0; nIndex < tableCount; nIndex++)
		{
			if( pComplexItemToCommandTbl->list[nIndex].resolved_ref == resolved_ref )
			{
				*cmd_table_offset = pComplexItemToCommandTbl->list[nIndex].item_to_command_table_offset;
				rc = SUCCESS;
				break;
			}
		}

		if(rc == SUCCESS)
		{
			break;
		}

		// Didn't find it. Try looking for a List index and zero it
		for ( iOpRefIdx++; iOpRefIdx < OpRefInfoList->count; iOpRefIdx++)
		{
            if (    (OpRefInfoList->list[iOpRefIdx].type == LIST_ITYPE)
                ||  (OpRefInfoList->list[iOpRefIdx].type == ARRAY_ITYPE))
            {
				if (OpRefInfoList->list[iOpRefIdx].member != 0)	// If we aren't looking
				{													// for the 0th member,
					OpRefInfoList->list[iOpRefIdx].member = 0;		// reset to do so and retry.
                    *pbModifiedIndex = true;                        // We had to modify the index, so we need to tell the caller
					break;
				}
			}
		}

	} while (iOpRefIdx < OpRefInfoList->count);

	delete [] tempResolvedRef;
	tempResolvedRef = nullptr;

	return rc;
}



/*********************************************************************
 *
 *	Name: convert_param_spec_to_bint_offset
 *	ShortDesc:  convert the param specifier structure into an bint
 *				offset
 *
 *	Description:
 *		convert_param_spec_to_bint_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the named item table offset
 *
 *	Inputs:
 *              block_handle    the block handle
 *
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER
 *						structure
 *		bt_elem:	the current block table element
 *
 *	Outputs:
 *		bint_offset:	a pointer to the Block Item Name Table offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from bint_offset table
 *  request functions
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

static
int
convert_param_spec_to_bint_offset(
ENV_INFO	*env_info,
BLOCK_HANDLE block_handle,
DDI_PARAM_SPECIFIER *param_spec,
nsEDDEngine::BLK_TBL_ELEM   *bt_elem,
int            *bint_offset)
{

  int             rc;	/* return code */

  switch (param_spec->type) {

  case DDI_PS_ITEM_ID:
    rc = tr_id_to_bint_offset(bt_elem, param_spec->item.id,bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME:
    rc = tr_name_to_bint_offset(bt_elem, param_spec->item.name,bint_offset);
    break;
    
  case DDI_PS_PARAM_OFFSET:
    rc = tr_param_to_bint_offset(bt_elem, param_spec->item.param,bint_offset);
    break;
    
  case DDI_PS_OP_INDEX:
    rc = tr_op_index_to_bint_offset(env_info, block_handle, bt_elem, param_spec->item.op_index, bint_offset);
      break;

  case DDI_PS_ITEM_ID_SI:
    rc = tr_id_si_to_bint_offset(bt_elem, param_spec->subindex,param_spec->item.id, bint_offset);
    break;
    
  case DDI_PS_PARAM_NAME_SI:
    rc = tr_name_si_to_bint_offset(bt_elem, param_spec->subindex,param_spec->item.name, bint_offset);
    break;
    
  case DDI_PS_OP_INDEX_SI:
    rc = tr_op_index_si_to_bint_offset(env_info, block_handle, bt_elem, param_spec->item.op_index, bint_offset,
				    param_spec->subindex);
    break;

  case DDI_PS_PARAM_OFFSET_SI:
    rc = tr_param_si_to_bint_offset(bt_elem, param_spec->subindex, param_spec->item.param, bint_offset);
    break;

  default:
    rc = DDI_INVALID_REQUEST_TYPE;
  }
  
  return rc;
}


/*********************************************************************
 *
 *	Name: get_rt_offset
 *	ShortDesc: convert a bint_offset or bint_offset_si into a relation table offset
 *
 *	Description:
 *		get_rt_offset takes a DDI_PARAM_SPECIFIER structure
 *		and calls the appropriate conversion function to
 *              convert the bint_offset to a relation table offset.
 *
 *	Inputs:
 *		bint_offset:	the bint offset
 *		param_spec:	a pointer to the DDI_PARAM_SPECIFIER structure
 *		bt_elem:		the current block handle
 *
 *	Outputs:
 *		rt_offset:		a pointer to the Relation Table
 *						offset
 *
 *	Returns: DDI_INVALID_REQUEST_TYPE and returns from
 *		other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
get_rt_offset(
int            bint_offset,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
nsEDDEngine::BLK_TBL_ELEM   *bt_elem,
int            *rt_offset)
{

	int             rc;	/* return code */

	/*
	 * Convert the request to a Relation Offset
	 */

	switch (param_spec->eType) {

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID:	
	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET:
		rc = tr_bint_offset_to_rt_offset(bt_elem, bint_offset,
			rt_offset);
		break;


	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_SI:	
	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET_SI:
		rc = tr_bint_offset_si_to_rt_offset(bt_elem,
			param_spec->subindex, bint_offset, rt_offset);
		break;


	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX:
		rc = DDI_INVALID_PARAM;
		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
		break;

	}
	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_check_enum_var_value
 *	ShortDesc: check for the value in the FLAT_VAR
 *
 *	Description:
 *		ddi_check_enum_var_value searches the enum_value_list for
 *		the given value if the FLAT_VAR is of type ENUMERATED
 *		 or BIT_ENUMERATED
 *
 *	Inputs:
 *		var:	a pointer to the flat variable to search
 *		value:	a pointer to the value to look for
 *
 *	Outputs:
 *		none
 *
 *	Returns: DDI_LEGAL_ENUM_VAR_VALUE, DDI_ILLEGAL_ENUM_VAR_VALUE
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_check_enum_var_value(
::nsEDDEngine::FLAT_VAR       * /*var*/,
unsigned long  * /*value*/)
{
	ASSERT(0);
	return DDI_LEGAL_ENUM_VAR_VALUE;
}


/*********************************************************************
 *
 *	Name: convert_item_spec_to_item_type
 *	ShortDesc: convert the item_spec structure into an item_type
 *
 *	Description:
 *		convert_item_spec_to_item_type takes an DDI_ITEM_SPECIFIER structure
 *		and calls the appropriate table request funtion to
 *		get the item_type.
 *
 *	Inputs:
 *		block_handle:	the current block handle
 *		item_spec:	a pointer to the DDI_ITEM_SPECIFIER structure
 *		bt_elem:	the Block Table element
 *
 *	Outputs:
 *		item_type:		a pointer to the item type
 *		item_id 		a pointer to the item id
 *
 *	Returns:DDI_INVALID_REQUEST_TYPE and returns from
 *  	other DDS functions
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
convert_item_spec_to_item_type(
ENV_INFO		*env_info,
nsEDDEngine::FDI_ITEM_SPECIFIER *item_spec,
nsEDDEngine::BLK_TBL_ELEM   *bt_elem,
ITEM_TYPE      *item_type,
ITEM_ID		   *item_id)
{

	int             rc;			/* return code */
	nsEDDEngine::ITEM_TBL       	*it;		/* item table */
	nsEDDEngine::ITEM_TBL_ELEM  	*it_elem;	/* item table element */
	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

#ifdef DDSTEST
	TEST_FAIL(CONVERT_ITEM_SPEC);
#endif

	switch (env_info->handle_type)
	{
	case ENV_INFO::BlockHandle:
		rc = pConnectionMgr->get_abt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir) ;
		break;
	case ENV_INFO::DeviceHandle:
		rc = pConnectionMgr->get_adt_dd_dev_tbls(env_info->block_handle, (void **)&flat_device_dir) ;
		break;
	default:
		return DDI_INVALID_DD_HANDLE_TYPE;
	}

	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	switch (item_spec->eType) {

	case nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID:
	  rc = tr_id_to_ite(env_info, it, item_spec->item.id, &it_elem);
		break;

	case nsEDDEngine::ItemSpecifierType::FDI_ITEM_PARAM:
	  rc = tr_param_to_ite(it, bt_elem, item_spec->item.param,
			       &it_elem);
	  break;
		
	case nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID_SI:
	  rc = tr_id_si_to_ite(it, bt_elem, item_spec->item.id,
			       item_spec->subindex, &it_elem);
	  break;
		
	case nsEDDEngine::ItemSpecifierType::FDI_ITEM_PARAM_SI:
	  rc = tr_param_si_to_ite(it, bt_elem,
				  item_spec->item.param, item_spec->subindex,
				  &it_elem);
	  break;
	case nsEDDEngine::ItemSpecifierType::FDI_ITEM_BLOCK:
	  *item_type = BLOCK_ITYPE;
	  return DDS_SUCCESS;
	  
	default:
		return DDI_INVALID_REQUEST_TYPE;
	}

	if (rc != DDS_SUCCESS) {
		return rc;
	}

	*item_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);
	*item_id = ITE_ITEM_ID(it_elem) ;

	return rc;
}

/*********************************************************************
 *
 *	Name: compare_cmd_num
 *	ShortDesc: compare command numbers
 *
 *	Description:
 *		compare command numbers
 *
 *	Inputs:
 *		cmd_1:	command 
 *		cmd_1:	command
 *
 *	Returns:
 *		-1 if command 1 < command 2
 *		1 if command 1 > command 2
 *		0 if command 1 == command 2
 *
 *
 **********************************************************************/
static int
compare_cmd_num(
nsEDDEngine::COMMAND_NUMBER_TBL_ELEM	*cmd_1,
nsEDDEngine::COMMAND_NUMBER_TBL_ELEM	*cmd_2)
{
	/*
	 *	Compare the two Subindexes and return the reflective value.
	 */

	if (cmd_1->command_number <	cmd_2->command_number) 
	{
		return(-1);
	}
	else if (cmd_1->command_number > cmd_2->command_number) 
	{
		return(1);
	}
	else 
	{
		return(0);
	}
}


/*********************************************************************
 *
 *	Name: ddi_get_cmd_id
 *	ShortDesc: get the item id of a command
 *
 *	Description:
 *		ddi_get_cmd_id() will return the item id
 *      of a command which corresponds to the given
 *      
 *
 *	Inputs:
 *		block_spec : a pointer to the block specifier
 *      cmd_number : the command number
 *
 *	Outputs:
 *      cmd_id : a pointer to the item id of the command
 *
 *	Returns: DDI_SUCCESS, DDI_COMMAND_NOT_FOUND
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_cmd_id(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
ulong				 cmd_number,
ITEM_ID  			*cmd_id)
{
	int						rc;				  /* return code */
	BLOCK_HANDLE    		block_handle;     /* Block Handle */
	nsEDDEngine::COMMAND_NUMBER_TBL		*cmd_numbers_tbl; /* Command number to item id table pointer */
	nsEDDEngine::COMMAND_NUMBER_TBL_ELEM		*command;         /* Pointer at element of cmd_num_id_tbl */
	nsEDDEngine::FLAT_DEVICE_DIR			*flat_device_dir ;
	nsEDDEngine::ITEM_TBL *it;
	ASSERT_RET(block_spec && cmd_id, DDI_INVALID_PARAM);
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	rc = conv_block_spec(env_info, block_spec, &block_handle);  /* get a block handle */
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	cmd_numbers_tbl = &flat_device_dir->command_number_tbl;

	if (! cmd_numbers_tbl->count) 
	{
		return DDI_COMMAND_NOT_FOUND;
	}


	command = (nsEDDEngine::COMMAND_NUMBER_TBL_ELEM *) bsearch(
		(char *)&cmd_number,
		(char *)cmd_numbers_tbl->list,
		(unsigned int) cmd_numbers_tbl->count,
		sizeof(nsEDDEngine::COMMAND_NUMBER_TBL_ELEM),
		(CMP_FN_PTR)compare_cmd_num);

	if(! command) 
	{
		return DDI_COMMAND_NOT_FOUND;
	}

	it = &flat_device_dir->item_tbl;
	if( !it->count )
	{
		return DDI_DEVICE_TABLES_NOT_FOUND;
	}

	*cmd_id = it->list[command->item_info_index].item_id;
	
	return DDS_SUCCESS;
}


/*****************************************************************
 *
 *	Name: ddi_get_type_and_item_id
 *	ShortDesc: get the type and the item id of the item
 *
 *	Description:
 *		ddi_get_type will return the item_type and the item id
 *		of the requested type.  
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		item_spec:		a pointer to the item specifier
 *
 *	Outputs:
 *		item_type:		a pointer to the item type of item requested
 *		item_id:		a pointer to the item id of item requested
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 *****************************************************************/

int
ddi_get_type_and_item_id(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_ITEM_SPECIFIER *item_spec,
ITEM_TYPE      *item_type,
ITEM_ID        *item_id)
{

	int rc = DDS_SUCCESS;	/* return code */
	nsEDDEngine::BLK_TBL_ELEM *bt_elem = nullptr;/* Block Table element pointer */

	ASSERT_RET(block_spec && item_spec && item_type, DDI_INVALID_PARAM);

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	switch (env_info->handle_type)
	{
	case ENV_INFO::BlockHandle:
		{
			BLOCK_HANDLE    block_handle;	/* Block Handle */

			rc = conv_block_spec(env_info, block_spec, &block_handle);
			if (rc != DDS_SUCCESS) {

				return rc;
			}

			rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
			if (rc != DDS_SUCCESS) {

				return rc;
			}
		}
		break;
	case ENV_INFO::DeviceHandle:
		bt_elem = nullptr;
		break;
	default:
		return  DDI_INVALID_DD_HANDLE_TYPE;
	}

	rc = convert_item_spec_to_item_type(env_info, item_spec,
		bt_elem, item_type, item_id);
	if (rc != DDS_SUCCESS) {

		if (rc == DDI_TAB_NO_BLOCK) {

			/*
			 * If convert_item_spec_to_item_type fails because of no block,
			 * request a load for the dd
			 */

			rc = pConnectionMgr->ds_dd_block_load(env_info, env_info->block_handle);
			if (!rc) {

				rc = convert_item_spec_to_item_type(env_info, item_spec,
					bt_elem, item_type, item_id);
				if (rc != DDS_SUCCESS) {

					return rc;
				}
			}
			else {

				return DDI_BAD_DD_BLOCK_LOAD;
			}
		}
		else {

			return rc;
		}
	}

	return rc;
}

/*****************************************************************
 *
 *	Name: ddi_find_unit_ids
 *	ShortDesc: find uint ids
 *
 *	Description:
 *		find uint ids
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		id_to_find:		id to find
 *
 *	Outputs:
 *		item_id_of_unit_rel:		unit rel
 *		item_id_of_unit_var:		unit var
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *
 *****************************************************************/

int
ddi_find_unit_ids(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID id_to_find,
	ITEM_ID *item_id_of_unit_rel, ITEM_ID *item_id_of_unit_var)
{
	
	nsEDDEngine::BLK_TBL_ELEM *bt_elem = nullptr;		// Block Table element pointer

	int rc = block_handle_to_bt_elem(env_info, env_info->block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	nsEDDEngine::FDI_ITEM_SPECIFIER blk_item_spec;

	blk_item_spec.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
    blk_item_spec.item.id = bt_elem->blk_id;
	blk_item_spec.subindex = 0;

    nsEDDEngine::AttributeNameSet req_mask = nsEDDEngine::unit_items;
	DDI_GENERIC_ITEM gi = {0};

    rc = ddi_get_item(block_spec, &blk_item_spec, env_info, req_mask, &gi); 
    if (rc) {
        return rc;
	}

	rc = DDI_TAB_NO_UNIT;	// Init to No Unit, in case we skip over the loop completely

    nsEDDEngine::FLAT_BLOCK *flat = (nsEDDEngine::FLAT_BLOCK *)gi.item;
    for (int i=0; i < flat->unit.count; i++)
    {
        nsEDDEngine::FDI_ITEM_SPECIFIER is_unit;
		is_unit.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
        is_unit.item.id = flat->unit.list[i];
		is_unit.subindex = 0;

		nsEDDEngine::AttributeNameSet mask = nsEDDEngine::relation_update_list;
		DDI_GENERIC_ITEM gi_unit = {0};

        rc = ddi_get_item(block_spec, &is_unit, env_info, mask, &gi_unit);
        if (rc) {            
            delete flat;
            return rc;
        }

		bool bFound = false;
        nsEDDEngine::FLAT_UNIT *unit = (nsEDDEngine::FLAT_UNIT *)gi_unit.item;

		nsEDDEngine::FDI_PARAM_SPECIFIER param_to_find;
		param_to_find.eType = nsEDDEngine::FDI_PS_ITEM_ID;
		param_to_find.id = id_to_find;


		if ( ManualUnitLookup::IsMyUnit(unit, &param_to_find) )
		{								// We found our unit relation
			if (item_id_of_unit_rel)
				*item_id_of_unit_rel = unit->id;
			if (item_id_of_unit_var)
				*item_id_of_unit_var = unit->var.desc_id;

			bFound = true;
			rc = DDS_SUCCESS;
		}
		else
		{
			rc = DDI_TAB_NO_UNIT;
		}

		delete unit;

		if (bFound)
		{
			break;
		}
    }

    delete flat;
    return rc;
}


/*****************************************************************
 *
 *	Name: ddi_get_type
 *	ShortDesc: get the type of the item
 *
 *	Description:
 *		ddi_get_type will return the item_type of the
 *		requested type
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		item_spec:		a pointer to the item specifier
 *
 *	Outputs:
 *		item_type:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *			Returns from other DDS functions.
 *
 *	Author: Steve Beyerl
 *
 *****************************************************************/

int
ddi_get_type(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_ITEM_SPECIFIER *item_spec,
ITEM_TYPE      *item_type)
{

	int		rc;		// return code
	ITEM_ID	item_id;  // item id of requested item

	rc = ddi_get_type_and_item_id(env_info, block_spec, item_spec, 
			item_type, &item_id);

	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_get_unit
 *	ShortDesc: get the item_id of the unit
 *
 *	Description:
 *		ddi_get_unit will return the item_id of the unit relation
 *		that contains the unit variable of the requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the parameter specifier
 *
 *	Outputs:
 *		unit_item_id:	a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_unit(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER	*block_spec,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
ITEM_ID				*unit_item_id)
{
	ASSERT_RET(env_info && block_spec && param_spec && unit_item_id, DDI_INVALID_PARAM);
	
	int rc = DDS_SUCCESS;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	BLOCK_HANDLE block_handle = 0;		//  Get the Block Handle from the block_spec
	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	nsEDDEngine::BLK_TBL_ELEM *bt_elem = nullptr;		// Block Table element pointer

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);	// Get the Block Table Elem
	if (rc != DDS_SUCCESS) {
		return rc;
	}

	nsEDDEngine::REL_TBL *rt = BTE_RT(bt_elem);		// Get the Relation Table
	ITEM_ID unitFromTbl = 0;

	if (param_spec->eType != nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX)
	{
		int				bint_offset = 0;		// Block Item Name Table offset

		rc = convert_param_spec(block_handle, param_spec, bt_elem, &bint_offset);

		if (rc != DDS_SUCCESS) {
			return rc;
		}

		int rt_offset = 0;		// relation table offset
		rc = get_rt_offset(bint_offset, param_spec, bt_elem, &rt_offset);

		if (rc != DDS_SUCCESS) {
			return rc;
		}

		if (rt_offset == -1) {
			return DDI_TAB_NO_UNIT;
		}

		// relation table element
		nsEDDEngine::REL_TBL_ELEM *rt_elem = RTE(rt, rt_offset);

		// Item Table offset for this unit relation
		int unit_it_offset = RTE_UNIT_IT_OFFSET(rt_elem);

		if (unit_it_offset == -1) {
			return DDI_TAB_NO_UNIT;
		}

		nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir = nullptr;
		rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
		if (rc != SUCCESS) {
			return(rc) ;
		}

		// Item Table
		nsEDDEngine::ITEM_TBL *it = &flat_device_dir->item_tbl;
	
		nsEDDEngine::ITEM_TBL_ELEM *it_elem = ITE(it, unit_it_offset);	// Get the UNIT ITEM_ID from the Relation Table
		unitFromTbl = ITE_ITEM_ID(it_elem);
	}
	else	// param_spec is FDI_PS_ITEM_ID_COMPLEX
	{
		unitFromTbl = 0;	// Not set for Complex references
	}


	// If we don't have the UnitLookup class yet, create it.
	if (rt->UnitLookup == nullptr)
	{
		rt->UnitLookup = new ManualUnitLookup();

		rc = rt->UnitLookup->Init(env_info, block_spec);
		if (rc != SUCCESS) {
			return(rc) ;
		}
	}

	// Find the Unit relation that best matches this param_spec
	ITEM_ID unit_rel = 0;

	rc = rt->UnitLookup->FindUnitRel(env_info, block_spec, unitFromTbl, param_spec, &unit_rel);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	*unit_item_id = unit_rel;

	return DDS_SUCCESS;
}

/*********************************************************************
 *
 *	Name: ddi_get_wao
 *	ShortDesc: get the item_id of the wao
 *
 *	Description:
 *		ddi_get_wao will return the item_id of the wao of the
 *		requested item
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:		a pointer to the param specifier
 *
 *	Outputs:
 *		item_id:		a pointer the item id of the unit of item
 *						requested
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_wao(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
ITEM_ID        *item_id)
{
	int             rc;	/* return code */
	nsEDDEngine::BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	nsEDDEngine::ITEM_TBL       *it;	/* item table */
	nsEDDEngine::ITEM_TBL_ELEM  *it_elem;/* item table element */
	BLOCK_HANDLE    block_handle;	/* a Block Handle */
	int             rt_offset;	/* relation table offset */
	int             bint_offset;	/* bint offset */
	nsEDDEngine::REL_TBL_ELEM   *rt_elem;/* relation table element */
	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	ASSERT_RET(block_spec && param_spec && item_id, DDI_INVALID_PARAM);

	rc = conv_block_and_param_spec(env_info, block_spec, 
				       param_spec,
				       &block_handle,
				       &bt_elem,
				       &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rt_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	/*
	 * Use the Relation Table Offset to form the item id of the Write As
	 * One Relation.
	 */

	if (rt_offset == -1) {
		return DDI_TAB_NO_WAO;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rt_offset);

	if (RTE_WAO_IT_OFFSET(rt_elem) == -1) {
		return DDI_TAB_NO_WAO;
	}

	rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl;
	it_elem = ITE(it, RTE_WAO_IT_OFFSET(rt_elem));
	*item_id = ITE_ITEM_ID(it_elem);
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_update_items
 *	ShortDesc: get the items of a relation
 *
 *	Description:
 *		ddi_get_update_items will return the list of item_ids associated
 *      with the right side of a relation.  This only affects wao and
 *      refresh relations.
 *
 *	Inputs:
 *		block_spec:		a pointer to block specifier
 *		param_spec:	a pointer to the relation request
 *
 *	Outputs:
 *		op_desc_list:	a pointer to the requested list of operational
 *						references (and descriptive references).
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_update_items(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
OP_DESC_LIST   *op_desc_list)
{
	int             rc;	/* return code */
	nsEDDEngine::BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	nsEDDEngine::REL_TBL_ELEM   *rt_elem;/* relation table element */
	int             count;	/* number of relation items */
	int             rel_tbl_offset;	/* relation table offset */
	OP_DESC        *update_ptr;	/* update list pointer */
	int             bint_offset;    /* bint offset */
	int             ut_offset;	/* Update table offset */
	nsEDDEngine::UPDATE_TBL     *update_tbl;	/* Update table */
	int             inc;	/* loop variable */
	BLOCK_HANDLE    block_handle;	/* A Block Handle */
	//nsEDDEngine::ITEM_TBL_ELEM  *op_it_elemp;	/* Operational reference */
	nsEDDEngine::ITEM_TBL_ELEM  *desc_it_elemp;	/* Descriptive reference */
	nsEDDEngine::ITEM_TBL       *it;
	nsEDDEngine::FLAT_DEVICE_DIR	*flat_device_dir ;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	ASSERT_RET(block_spec && param_spec && op_desc_list, DDI_INVALID_PARAM);


	rc = conv_block_and_param_spec(env_info, block_spec, 
				       param_spec,
				       &block_handle,
				       &bt_elem,
				       &bint_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = get_rt_offset(bint_offset,param_spec,bt_elem,&rel_tbl_offset);

	if (rc != DDS_SUCCESS) {

		return rc;
	}


	if (rel_tbl_offset == -1) {
		return DDI_TAB_NO_UPDATE;
	}

	rt_elem = RTE(BTE_RT(bt_elem), rel_tbl_offset);

	count = RTE_U_COUNT(rt_elem);
	ut_offset = RTE_UT_OFFSET(rt_elem);

	if ((ut_offset == -1) || (count == 0)) {
		return DDI_TAB_NO_UPDATE;
	}

	
	update_tbl = BTE_UT(bt_elem);

	update_ptr = (OP_DESC *) realloc((void *) op_desc_list->list,
		(size_t) count * sizeof(OP_DESC));

	if (update_ptr == NULL) {
		return DDI_MEMORY_ERROR;
	}

	op_desc_list->list = update_ptr;
	op_desc_list->count = (unsigned short) count;

	rc  = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}
	it = &flat_device_dir->item_tbl ;

	//we are only handling ResolvedReferences whose size is two or less. In other words,
	//we aren't handling COMPLEX references. We may have to come back later and fix this to handle COMPLEX references, 

	for (inc = 0; inc < count; inc++, update_ptr++) {

		int update_tbl_index = ut_offset + inc;
		desc_it_elemp = ITE(it,
			UTE_DESC_IT_OFFSET(UTE(update_tbl, (update_tbl_index))));
		update_ptr->desc_ref.id = ITE_ITEM_ID(desc_it_elemp);
		update_ptr->desc_ref.type = (ITEM_TYPE)ITE_ITEM_TYPE(desc_it_elemp);


		update_ptr->op_ref.op_ref_type = nsEDDEngine::STANDARD_TYPE;

		update_ptr->op_ref.op_info.id = (ITEM_ID)update_tbl->list[update_tbl_index].to_be_updated.pResolvedRef[0];
		update_ptr->op_ref.op_info.member = 0;
		//update_ptr->op_ref.op_info.type		Set below

		update_ptr->op_ref.op_info_list.count = 0;
		update_ptr->op_ref.op_info_list.list = nullptr;
		update_ptr->op_ref.block_instance = 0;

		// Find item_type
		nsEDDEngine::FDI_ITEM_SPECIFIER item_spec = {nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID, 0, 0};
		
		item_spec.item.id = (ITEM_ID)update_tbl->list[update_tbl_index].to_be_updated.pResolvedRef[0];
		ITEM_TYPE item_type = 0;

		rc = ddi_get_type(env_info, block_spec, &item_spec, &item_type);
		if(rc == DDS_SUCCESS)
		{
			update_ptr->op_ref.op_info.type = (nsEDDEngine::ITEM_TYPE)item_type;	// These are the same

			// Set op_info.member, if needed
			if(item_type == ARRAY_ITYPE)
			{
				if(update_tbl->list[ut_offset + inc].to_be_updated.size > 1)
				{
					update_ptr->op_ref.op_info.member = update_tbl->list[update_tbl_index].to_be_updated.pResolvedRef[1];
				}
			}
			else if(item_type == RECORD_ITYPE)
			{
				if(update_tbl->list[ut_offset + inc].to_be_updated.size > 1)
				{
					update_ptr->op_ref.op_info.member = update_tbl->list[update_tbl_index].to_be_updated.pResolvedRef[1];
				}
			}
		}
	}
	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_clean_update_item_list
 *	ShortDesc: free the relation item id list
 *
 *	Description:
 *		ddi_clean_update_item_list() will free the relation item id list
 *		 malloced by ddi_get_update_items() call
 *
 *	Inputs:
 *		none
 *
 *	Outputs:
 *		update_list:		a pointer to the freed list
 *
 *	Returns: none
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

void
ddi_clean_update_item_list(OP_DESC_LIST   *update_list)
{

	if (!update_list) {
		return;
	}

	free((void *) update_list->list);

	update_list->count = 0;

	return;
}

void param_spec_to_op_ref_info_list(	nsEDDEngine::OP_REF_INFO_LIST *pOpRefInfoList,
								ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *pBlockSpec,
								const nsEDDEngine::FDI_PARAM_SPECIFIER *pFdiParamSpec)
{
	int rs = DDS_SUCCESS;

	switch(pFdiParamSpec->eType)
	{
	case nsEDDEngine::FDI_PS_ITEM_ID:
	case nsEDDEngine::FDI_PS_ITEM_ID_SI:
		{
			
			pOpRefInfoList->count = 1;
			pOpRefInfoList->list = new nsEDDEngine::OP_REF_INFO[pOpRefInfoList->count];

			pOpRefInfoList->list[0].id		= pFdiParamSpec->id;
			pOpRefInfoList->list[0].member	= pFdiParamSpec->subindex;

			// type
			nsEDDEngine::FDI_ITEM_SPECIFIER is;
			is.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
			is.item.id = pFdiParamSpec->id;

			ITEM_TYPE item_type = 0;

			rs = ddi_get_type(env_info, pBlockSpec, &is, &item_type);
			pOpRefInfoList->list[0].type = (nsEDDEngine::ITEM_TYPE)item_type;

		}
		break;

	case nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX:
		{
			// Figure out how many entries you need
			unsigned short cnt = pFdiParamSpec->RefList.count;
			pOpRefInfoList->count = cnt;
			pOpRefInfoList->list = new nsEDDEngine::OP_REF_INFO[pOpRefInfoList->count];
			

			// Loop to copy
			for (int i=0; i < cnt; i++)
			{
				pOpRefInfoList->list[i].id		= pFdiParamSpec->RefList.list[i].id;
				pOpRefInfoList->list[i].type	= pFdiParamSpec->RefList.list[i].type;
				pOpRefInfoList->list[i].member	= pFdiParamSpec->RefList.list[i].member;
			}
		}
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET:
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE,
			L"param_spec_to_op_ref_info_list: cannot convert ParamSpecifierType: FDI_PS_PARAM_OFFSET");
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET_SI:
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE,
			L"param_spec_to_op_ref_info_list: cannot convert ParamSpecifierType: FDI_PS_PARAM_OFFSET_SI" );
		break;
	default:
		BssProgLog( __FILE__, __LINE__, BssError, BSS_FAILURE,
			L"param_spec_to_op_ref_info_list: cannot convert ParamSpecifierType: %d", pFdiParamSpec->eType );
	}
}

/**************************************
 * 
 *  adjust_command_index() - adjusts the value of the COMMAND_INDEX to be the value of the param_spec subindex
 *
 *  This is called by ddi_get_ptoc() if the param_spec subindex was modified in order to get the proper cmd_table_index.
 *  See call to conv_param_spec_to_itct_offset() in ddi_get_ptoc()
 *
 **************************************/

static void adjust_command_index(
    ENV_INFO			*env_info,
    DDI_BLOCK_SPECIFIER *block_spec,
    nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
    nsEDDEngine::COMMAND_INDEX *pCommandIndex)
{
    if (param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID)    // We should only be adjusting the COMMAND_INDEX if we have subindices
    {
        return;
    }

    // Look up this index VARIABLE definition to see if it is indexing the list or array in the param_spec

    nsEDDEngine::FDI_ITEM_SPECIFIER itemSpec = { nsEDDEngine::FDI_ITEM_ID, pCommandIndex->id, 0 };
    nsEDDEngine::AttributeNameSet attributeNameSet = nsEDDEngine::variable_type | nsEDDEngine::indexed;
    DDI_GENERIC_ITEM generic_item = { 0 };

    int r_code = ddi_get_item(block_spec, &itemSpec, env_info, attributeNameSet, &generic_item);

    if (r_code == DDS_SUCCESS)
    {
        ::nsEDDEngine::FLAT_VAR *pFlatVar = (::nsEDDEngine::FLAT_VAR *)generic_item.item;

        // Check to see if this index variable is an index for the list or value array that was passed in
        if (pFlatVar->type_size.type == nsEDDEngine::VT_INDEX)
        {
            if (param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI) // Use the id and subindex
            {
                if (pFlatVar->indexed == param_spec->id)
                {
                    // If this variable is an index for the list or array referenced in the param_spec,
                    // then it is safe to replace the index value with the subindex passed in.
                    pCommandIndex->value = param_spec->subindex;
                }
            }
            else if (param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX) // Look through the RefList
            {
                for (int i = 0; i < param_spec->RefList.count; i++)
                {
                    nsEDDEngine::OP_REF_INFO *pOpRefInfo = &param_spec->RefList.list[i];

                    if (pFlatVar->indexed == pOpRefInfo->id)
                    {
                        pCommandIndex->value = pOpRefInfo->member;
                        break;  // Once we have found the list or array, we can exit.
                    }
                }
            }

        }

        delete pFlatVar;
    }
}

/*********************************************************************
 *
 *	Name: ddi_get_ptoc
 *	ShortDesc: Gets a list of commands that either read or write the
 *             given DDL parameter
 *
 *	Description:
 *		ddi_get_ptoc
 *
 *	Inputs:
 *		block_spec:	A pointer to the structure which specifies
 *                  the block which contains the parameter for
 *                  which the list of commands is being requested.
 *
 *		param_spec:	A pointer to the structure which specifies the
 *					parameter for which the list of commands is being
 *					requested.
 *
 *      cmd_type:	Specifies that either READ or WRITE commands are
 *                  being requested.
 *
 *
 *	Outputs:
 *		cmd_list:	a pointer to the requested list of READ or WRITE
 *                  commands which contain the given parameter.
 *
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
ddi_get_ptoc(
	ENV_INFO			*env_info, 
	DDI_BLOCK_SPECIFIER *block_spec,
	nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
	nsEDDEngine::CommandType eCmdType,
	nsEDDEngine::FDI_COMMAND_LIST * pCommandList)
{

	ASSERT_RET(block_spec && param_spec && pCommandList, DDI_INVALID_PARAM);

	pCommandList->Dispose();	// Make sure that any old command elems are cleaned out

	ASSERT_RET(!pCommandList->list, DDI_INVALID_PARAM);

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();


	BLOCK_HANDLE	block_handle	= 0;	/* Handle for the block */
	nsEDDEngine::BLK_TBL_ELEM	*bt_elem		= NULL;	/* Block Table Element pointer */
	int				bint_offset		= 0;	/* Block Item Name Table offset */
	int cmd_table_offset = -1;
    bool bModifiedIndex = false;        // Assume that we haven't modified the index during command table lookup
	int rc = 0;

	if( param_spec->eType == nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX || param_spec->eType == nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_SI )
	{
		nsEDDEngine::OP_REF_INFO_LIST OpRefInfoList;
		param_spec_to_op_ref_info_list(&OpRefInfoList, env_info, block_spec, param_spec);
		
		rc = conv_param_spec_to_itct_offset(env_info, &OpRefInfoList, (param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID),
            &cmd_table_offset, &bModifiedIndex);
	}
	else
	{
		rc = conv_block_and_param_spec(env_info, block_spec, param_spec,
								&block_handle, &bt_elem, &bint_offset);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		if (bint_offset == -1) {
			return DDI_NO_COMMANDS_FOR_PARAM;
		}
	
		// Get the offset into the ITEM_TO_COMMAND_TBL
		cmd_table_offset = BINTE_ITCT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
	}

	if (cmd_table_offset == -1)
	{
		return (eCmdType == nsEDDEngine::FDI_READ_COMMAND) ? DDI_NO_READ_COMMANDS_FOR_PARAM : DDI_NO_WRITE_COMMANDS_FOR_PARAM;
	}

	// Get pointer to the Flat Device Dir
	nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir = nullptr ;

	rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle, (void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}

	// Get pointer to the Item To Command Table
	nsEDDEngine::ITEM_TO_COMMAND_TBL *pItemToCommandTbl = &flat_device_dir->item_to_command_tbl;

	if (cmd_table_offset >= pItemToCommandTbl->count)
	{
		return DDL_ENCODING_ERROR;
	}

	// Get the correct element of the Item To Command Table
	nsEDDEngine::ITEM_TO_COMMAND_TBL_ELEM *pItemToCommandTblElem = &pItemToCommandTbl->list[cmd_table_offset];

	// Look up either the Read Command list or the Write Command List
	int num_cmds = 0;
	nsEDDEngine::COMMAND_TBL_ELEM *pCommandTblElem = nullptr;

	switch (eCmdType) {

		case nsEDDEngine::FDI_READ_COMMAND:
			num_cmds = pItemToCommandTblElem->number_of_read_commands;
			pCommandTblElem = pItemToCommandTblElem->read_command_list;
			break;

		case nsEDDEngine::FDI_WRITE_COMMAND:
			num_cmds = pItemToCommandTblElem->number_of_write_commands;
			pCommandTblElem = pItemToCommandTblElem->write_command_list;
			break;

		default:
			return DDI_INVALID_PARAM;
	}
	
	if (num_cmds == 0)	// No commands defined for this CmdType
	{
		return (eCmdType == nsEDDEngine::FDI_READ_COMMAND) ? DDI_NO_READ_COMMANDS_FOR_PARAM : DDI_NO_WRITE_COMMANDS_FOR_PARAM;
	}

	// Create the empty list of Command Elements
	nsEDDEngine::COMMAND_ELEM *pCommandElem = new nsEDDEngine::COMMAND_ELEM[num_cmds];
	if (pCommandElem == NULL) {
		return DDI_MEMORY_ERROR;
	}

	// Hook the list up to the FDI_COMMAND_LIST that was passed in
	pCommandList->list = pCommandElem;
	pCommandList->count = num_cmds;

	// Fill the FDI_COMMAND_LIST with the commands found in the Command Table Element
	nsEDDEngine::ITEM_TBL* it = &flat_device_dir->item_tbl;

	for ( int i=0; i < num_cmds; i++, pCommandElem++, pCommandTblElem++)
	{
		// Look up the Command's ITEM_ID
		nsEDDEngine::ITEM_TBL_ELEM* it_elem = ITE(it, pCommandTblElem->command_item_info_index);
		ITEM_ID cmd_id = ITE_ITEM_ID(it_elem);

		// Copy the rest of the info from the Ite
		pCommandElem->command_id		= cmd_id;
		pCommandElem->command_number	= pCommandTblElem->command_number;
		pCommandElem->transaction		= pCommandTblElem->transaction_number;
		pCommandElem->weight			= (ushort)pCommandTblElem->weight;
		pCommandElem->count				= pCommandTblElem->count;

		if (pCommandElem->count) {
			
			// Create a list of Command Indexes
			pCommandElem->index_list = new nsEDDEngine::COMMAND_INDEX [pCommandElem->count];
			if (pCommandElem->index_list == NULL)
			{
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"ddi_get_ptoc: failed to malloc memory");
			}

			// Copy in the id and value entries
			nsEDDEngine::COMMAND_INDEX *index_ptr = pCommandElem->index_list;
			nsEDDEngine::COMMAND_INDEX *index_tbl_ptr = pCommandTblElem->index_list;

			for (int count=0; count < pCommandElem->count; count++, index_ptr++, index_tbl_ptr++) {

				index_ptr->id = index_tbl_ptr->id;
				index_ptr->value = index_tbl_ptr->value;

                // If this param_spec was modified to find the cmd_table_index, adjust the index_ptr->value to the right value.
                if (bModifiedIndex)
                {
                    adjust_command_index(env_info, block_spec, param_spec, index_ptr);
                }
			}
		}
		else {
			// If count is zero, index_list should also be NULL
			pCommandElem->index_list = NULL;
		}
	}

	return DDS_SUCCESS;
}


/*********************************************************************
 *
 *	Name: ddi_get_var_type
 *	ShortDesc: get the variable type and size
 *
 *	Description:
 *		ddi_get_var_type will return the type and size of
 *		the given variable
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier structure
 *		param_spec:		a pointer to the parameter specifier structure
 *
 *	Outputs:
 *		type_size:		the loaded type/size structure
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
ddi_get_var_type(
ENV_INFO			*env_info, 
DDI_BLOCK_SPECIFIER *block_spec,
nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec,
TYPE_SIZE           *type_size)
{
	nsEDDEngine::BLK_TBL_ELEM   *bt_elem;      /* Block Table element pointer */
	BLOCK_HANDLE    block_handle; /* Block Handle */
	int				param_offset; /* Param Table offset */
	int				bint_offset;  /* Block Item Name Table offset */
	ulong			rec_mem_count;/* Temp count for record member count */
	int				arr_ele_count;/* Temp count for array element count */
	int				offset;       /* Temp count offset */
	int             rc;	          /* return code */


	ASSERT_RET(block_spec && param_spec && type_size, DDI_INVALID_PARAM);

	// Get the profile.
	int ddtype = get_ddtype(env_info);
	
	// If profile is HART, then check for the communication parameters. 
	if(ddtype == ::nsEDDEngine::PROFILE_HART)
	{
		// If item id is 151 i.e. communication paramater "device_status" or 152 i.e. "comm_status"
		if( (param_spec->id == SYM_device_status_ID) || (param_spec->id == SYM_comm_status_ID) )
		{
			type_size->type = BIT_ENUMERATED; // type is BIT_ENUMERATED for "device_status" & "comm_status"
			type_size->size = 1; 

			return DDS_SUCCESS;
		}
		// If item id is 150 i.e. "response_code".
		else if(param_spec->id == SYM_response_code_ID)
		{
			type_size->type = ENUMERATED; // type is ENUMERATED 
			type_size->size = 1;

			return DDS_SUCCESS;
		}
	}


	rc = conv_block_and_param_spec(env_info, block_spec, param_spec,
								&block_handle, &bt_elem, &bint_offset);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	// If profile is FF, then check for a characteristics record, which occurs when the bint_offset is 0
	if((ddtype == ::nsEDDEngine::PROFILE_FF) || (ddtype == ::nsEDDEngine::PROFILE_ISA100))
	{
		if (bint_offset == 0) 
		{
			// Handle characteristics individually
			rec_mem_count = CMT_COUNT(BTE_CMT(bt_elem));

			if ( ((int)param_spec->subindex > rec_mem_count) || (param_spec->subindex < 1)) 
			{
				return DDI_TAB_BAD_SUBINDEX;
			}

			type_size->type = (ushort) CMTE_CM_TYPE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));
			type_size->size = CMTE_CM_SIZE(CMTE(BTE_CMT(bt_elem), param_spec->subindex-1));

			return DDS_SUCCESS;
		}
	}

	param_offset = BINTE_PT_OFFSET(BINTE(BTE_BINT(bt_elem), bint_offset));
	if (param_offset == -1) {

		return DDI_TAB_BAD_PARAM_OFFSET;
	}

	rec_mem_count  = PTE_PM_COUNT(PTE(BTE_PT(bt_elem), param_offset));
	arr_ele_count  = PTE_PE_MX_COUNT(PTE(BTE_PT(bt_elem), param_offset));


	switch (param_spec->eType) {

	/*
	 * get type and size of parameter without subindex
	 */

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID:
	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET:

		if((rec_mem_count) || (arr_ele_count)) {

			return DDI_INVALID_REQUEST_TYPE;		
		}

		type_size->type = (ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
		type_size->size = PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
		break;

	/*
	 * get type and size of parameter with subindex
	 */

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_SI:
	case nsEDDEngine::ParamSpecifierType::FDI_PS_PARAM_OFFSET_SI:

		if (rec_mem_count) {

			offset = PTE_PMT_OFFSET(PTE(BTE_PT(bt_elem), param_offset));

			if ((param_spec->subindex > rec_mem_count) || (param_spec->subindex < 1)) {

				return DDI_TAB_BAD_SUBINDEX;
			}

			type_size->type = (ushort) PMTE_PM_TYPE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));
			type_size->size = PMTE_PM_SIZE(PMTE(BTE_PMT(bt_elem), (offset + param_spec->subindex - 1)));
		}
		else {


			if (arr_ele_count) {

				type_size->type = (ushort) PTE_AE_VAR_TYPE(PTE(BTE_PT(bt_elem), param_offset));
				type_size->size = PTE_AE_VAR_SIZE(PTE(BTE_PT(bt_elem), param_offset));
			}
			else {

				return DDI_INVALID_REQUEST_TYPE;
			}
		}
		break;

	case nsEDDEngine::ParamSpecifierType::FDI_PS_ITEM_ID_COMPLEX:
		rc = DDI_INVALID_PARAM;
		break;

	default:
		rc = DDI_INVALID_REQUEST_TYPE;
	}


	return rc;
}


/*********************************************************************
 *
 *	Name: ddi_get_item_id
 *	ShortDesc: get the item id
 *
 *	Description:
 *		ddi_get_item_id will return the item id for the parameter
 *		name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		param_name:		parameter name for which you want the item_id
 *
 *	Outputs:
 *		item_id:		the item_id of the requested item
 *
 *	Returns:
 *		Returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

//not used in FDI Engine
int
ddi_get_item_id(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
ITEM_ID		param_name,
ITEM_ID    *item_id)
{

	nsEDDEngine::BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	int             rc;	/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */
	ITEM_TYPE       item_type;	/* Throw away item type */

	ASSERT_RET(block_spec && item_id, DDI_INVALID_PARAM);

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = tr_resolve_name_to_id_type(env_info, block_handle, bt_elem,
			param_name, item_id, &item_type,false);

	return rc;

}


/*********************************************************************
 *
 *	Name: ddi_get_subindex
 *	ShortDesc: get the subindex of the member of the item
 *
 *	Description:
 *		ddi_get_subindex() will return the subindex of the
 *		item based on the inputs of item_id and member name
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		item_id:		the item_id of the item to which the member belongs
 *		mem_name:		the item_id of the member
 *
 *	Outputs:
 *		subindex:		the subindex of the member
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/


int
ddi_get_subindex(
ENV_INFO		*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
ITEM_ID         item_id,
ITEM_ID         mem_name,
SUBINDEX       *subindex)
{

	nsEDDEngine::BLK_TBL_ELEM   *bt_elem;/* Block Table element pointer */
	int             rc;	/* return code */
	BLOCK_HANDLE    block_handle;	/* Block Handle */

	ASSERT_RET(block_spec && item_id && mem_name && subindex, DDI_INVALID_PARAM);

	rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != DDS_SUCCESS) {

		return rc;
	}

	rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
	if (rc != DDS_SUCCESS) {

		return rc;
	}
	
	rc = tr_resolve_ddid_mem_to_si(bt_elem, item_id, mem_name, subindex, env_info, NULL);

	return rc;

}

/*********************************************************************
 *
 *	Name: param_spec_to_item_spec
 *
 *	ShortDesc: convert a DDI_PARAM_SPECIFIER into a DDI_ITEM_SPECIFIER
 *
 *	Description: 
 *              param_spec_to_item_spec() will convert a DDI_PARAM_SPECIFIER
 *              structure into a DDI_ITEM_SPECIFIER.  This is safe since a parameter
 *              is an item.
 *
 *	Inputs:
 *		param_spec:    a pointer to the param specifier
 *
 *	Outputs:
 *              item_spec:     a pointer to the item specifier        
 *
 *	Returns:
 *		returns DDI_INVALID_REQUEST_TYPE or DDS_SUCCESS
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
param_spec_to_item_spec(
DDI_PARAM_SPECIFIER *param_spec,
DDI_ITEM_SPECIFIER *item_spec)
{

  item_spec->subindex = param_spec->subindex;

  switch(param_spec->type) {
  case DDI_PS_ITEM_ID:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID;
    break;
  case DDI_PS_PARAM_NAME:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME;
    break;
  case DDI_PS_PARAM_OFFSET:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM;
    break;
  case DDI_PS_OP_INDEX:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX;
    break;
  case DDI_PS_ITEM_ID_SI:
    item_spec->item.id = param_spec->item.id;
    item_spec->type = DDI_ITEM_ID_SI;
    break;
  case DDI_PS_OP_INDEX_SI:
    item_spec->item.op_index = param_spec->item.op_index;
    item_spec->type = DDI_ITEM_OP_INDEX_SI;
    break;
  case DDI_PS_PARAM_NAME_SI:
    item_spec->item.name = param_spec->item.name;
    item_spec->type = DDI_ITEM_NAME_SI;
    break;
  case DDI_PS_PARAM_OFFSET_SI:
    item_spec->item.param = param_spec->item.param;
    item_spec->type = DDI_ITEM_PARAM_SI;
    break;
  default:
    return DDI_INVALID_REQUEST_TYPE;
    
  }
  return DDS_SUCCESS;
}



/*********************************************************************
 *
 *	Name: ddi_get_param_info
 *	ShortDesc: Get information about a parameter
 *
 *	Description:
 *		ddi_get_param_info() accepts a DDI_PARAM_SPECIFIER
 *		and fills in a PARAM_INFO structure with the parameter's
 *              information.
 *     
 *              The idea behind this function is that a user knows one piece
 *              of information about a parameter ( either the item id, 
 *              param name, param offset or object index ) and the user
 *              is interested in either some or all of the other information        
 *              about the parameter. 
 *
 *	Inputs:
 *		block_spec:		a pointer to the block specifier
 *		param_spec:             a pointer to the param specifier
 *
 *	Outputs:
 *		param_info:		a pointer to the parameter information
 *
 *	Returns:
 *		returns from other DDS functions.
 *
 *	Author: Nathan Dodge
 *
 **********************************************************************/

int
ddi_get_param_info(
ENV_INFO			*env_info,
DDI_BLOCK_SPECIFIER *block_spec,
DDI_PARAM_SPECIFIER *param_spec,
DDI_PARAM_INFO      *param_info)
{
  int                rc;                 /* return code */
  nsEDDEngine::BLK_TBL_ELEM       *bt_elem;           /* Block Table element pointer */
  BLOCK_HANDLE       block_handle;       /* Block Handle */
  int                bint_offset;       /* Block item Name Table Offset */
  nsEDDEngine::BLK_ITEM_NAME_TBL_ELEM *bint_elem;     /* blk item name tb elem */
  nsEDDEngine::FLAT_DEVICE_DIR       *flat_device_dir ;
  nsEDDEngine::ITEM_TBL              *it;            /* item table */
  nsEDDEngine::ITEM_TBL_ELEM         *it_elem;       /* item table element */

  ASSERT_RET(block_spec && param_spec && param_info, DDI_INVALID_PARAM);
  
  IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
  CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

  rc = conv_block_spec(env_info, block_spec, &block_handle);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
  if (rc != DDS_SUCCESS) {
    return rc;
  }

  /*
   * Get the Block Item Name Table Offset of the Parameter
   */

  rc = convert_param_spec_to_bint_offset(env_info, block_handle,param_spec, bt_elem,&bint_offset);
  if (rc != DDS_SUCCESS) {
    
    if (rc == DDI_TAB_NO_BLOCK) {
      
      /*
       * If convert_param_spec_to_bint_offset() fails because of no block,
       * request a load for the dd
       */
      
      rc = pConnectionMgr->ds_dd_block_load(env_info, block_handle);
      if (!rc) {
        
        rc = convert_param_spec_to_bint_offset(env_info, block_handle,param_spec, bt_elem,&bint_offset);
        if (rc != DDS_SUCCESS) {
          
          return rc;
        }
      }
      else {
        
        return DDI_BAD_DD_BLOCK_LOAD;
      }
    }
    else {
      
      return rc;
    }
  }
  
  bint_elem = BINTE(BTE_BINT(bt_elem),bint_offset);

  /* 
   * Fill in param offset
   */
  
  param_info->param_offset = BINTE_PT_OFFSET(bint_elem);

  /* 
   * Fill in item id
   */
  
  rc = pConnectionMgr->get_abt_dd_dev_tbls(block_handle,(void **)&flat_device_dir) ;
  if (rc != SUCCESS) {
    return(rc) ;
  }
  it = &flat_device_dir->item_tbl;
  it_elem = ITE(it,BINTE_IT_OFFSET(bint_elem));
  param_info->item_id = ITE_ITEM_ID(it_elem);

  /* 
   * Fill in param name 
   */

  param_info->param_name = BINTE_BLK_ITEM_NAME(bint_elem);

  /* 
   * Fill in op index  
   */

  rc = tr_param_offset_to_op_index(env_info, block_handle,
                BINTE_PT_OFFSET(bint_elem), &param_info->op_index);

  /* 
   * Fill in item type
   */
  
  param_info->item_type = (ITEM_TYPE)ITE_ITEM_TYPE(it_elem);

  return rc;
}


