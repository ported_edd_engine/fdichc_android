/*
 *	@(#) $Id: ff_sup.cpp,v 1.2 2012/10/04 23:50:56 rgretta Exp $
 *
 *	ff_sup.c - Profibus Foundation Support Module
 */


#include "STDINC.H"


#include "cm_struc.h"
#include "evl_loc.h"
#include <ServerRes.h>
#include "FDI/FDIEDDEngine/DDSSupport.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//Take a error code defined in rtn_def.h and return a general error string
//that is from ServerLoc.dll
wchar_t* dds_error_user_string(int err, wchar_t *out_buf, int out_len)
{
	CStdString sFormat;
    //::LoadString(0, IDS_GENERAL_SERVER_USER_ERROR, sFormat.GetBuffer(out_len-1), out_len-1);
    sFormat.TryLoad((void *)IDS_GENERAL_SERVER_USER_ERROR);
    sFormat.ReleaseBuffer();

	CStdString sError;
	sError.Format(sFormat, err);

	PS_Wcscpy(out_buf, out_len-1, sError);
            
	return out_buf;
}

#pragma region Device_Specific_Strings
/*********************************************************************
 *
 *  Name: get_string_tbl_handle
 *
 ********************************************************************/
static int
get_string_tbl_handle(BLOCK_HANDLE block_handle, ENV_INFO* env_info, nsEDDEngine::STRING_TBL **string_tbl_handle)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	nsEDDEngine::FLAT_DEVICE_DIR		*flat_device_dir = 0;

	int rs = pConnectionMgr->get_abt_dd_dev_tbls (block_handle, (void **)(&flat_device_dir));

	*string_tbl_handle = &flat_device_dir->string_tbl;

	return(rs);
}


/*********************************************************************
 *
 *  Name: app_func_get_dev_spec_string
 *
 *  ShortDesc:  Upcall to retreive a device specific string.
 *
 *  Description: Returns device specific string which has been specified as a device specific
 *               string reference.  The device specific string table to use is 
 *				 specified by the block_handle passed in.  There is one
 *				 device specific string table per device type.
 *
 *  Inputs:
 *		env_info - contains block_handle which is used to access device
 *					specific string table.
 *		dev_str_info - information about the string.
 *
 *  Outputs:
 *		str - pointer to a STRING structure to be filled by the application.
 *
 *  Returns:
 *		DDL_DEV_SPEC_STRING_NOT_FOUND;
 *		DDL_SUCCESS;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
int app_func_get_dev_spec_string(
							ENV_INFO *env_info, 
							DEV_STRING_INFO *dev_str_info,
							STRING *str )
{
	int nRetVal = DDL_DEV_SPEC_STRING_NOT_FOUND;
	nsEDDEngine::STRING_TBL		*string_tbl_handle=NULL;

	//
	// Find the pointer to the device specific table corresponding to block handle
	//
	int rs = get_string_tbl_handle(env_info->block_handle, env_info, &string_tbl_handle);
	if (rs != SUCCESS) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"\n\napp_func_get_dev_spec_string: Device specific table not found" );
	}
	else if (dev_str_info->id >= (unsigned long) string_tbl_handle->count) 
	{
		//
		// If string not found
		//
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"\n\napp_func_get_dev_spec_string: Device specific string of id %d not found", dev_str_info->id );
	}
	else 
	{
		//
		// Retreive the device specific string information.
		//
		// Get the translation you want
		// If the strings are the same
		//		Use the one from the string table and DONT_FREE_STRING
		// else the translation was different
		//		Use the translation and FREE_STRING
		//

		wchar_t *table_str = (wchar_t*)string_tbl_handle->list[dev_str_info->id].c_str();
		if( table_str )
		{
			int buf_len = wcslen(table_str);
			wchar_t *buf = (wchar_t *) malloc ((buf_len+1) * sizeof(wchar_t));

			rs = ddi_get_string_translation( table_str, env_info->lang_code, buf, buf_len );
			if ( rs != DDS_SUCCESS ) 
			{
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"\n\napp_func_get_dev_spec_string: String translation failed" );
			}
			else if( wcscmp(buf, table_str) == 0) 
			{												// If the strings are the same,
				str->flags = STRING::DONT_FREE_STRING;		// point directly at the table.
				str->str = table_str;
				str->len = (unsigned short) wcslen(str->str);
				free(buf);
				nRetVal = DDL_SUCCESS;
			}
			else 
			{
				str->flags = STRING::FREE_STRING;
				str->str = buf;
				str->len = (unsigned short) wcslen(str->str);
				nRetVal = DDL_SUCCESS;
			}
		}
	}
	return nRetVal;
}

#pragma endregion Device_Specific_Strings

#pragma region Standard_Dict_Strings

/*********************************************************************
 *
 *  Name: get_dict_string_tbl_handle
 *
 ********************************************************************/
static int
get_dict_string_tbl_handle_x(BLOCK_HANDLE block_handle, ENV_INFO* env_info, nsEDDEngine::DICT_REF_TBL **dict_string_tbl_handle)
{
	//ASSERT(0);
	//dict_string_tbl_handle=NULL;
	//return DDL_SUCCESS;
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	::nsEDDEngine::FLAT_DEVICE_DIR		*flat_device_dir = 0;

	int rs = pConnectionMgr->get_abt_dd_dev_tbls (block_handle, (void **)(&flat_device_dir));
	if(rs)
		return rs;

	*dict_string_tbl_handle = &flat_device_dir->dict_ref_tbl;

	return(rs);
}

static nsEDDEngine::DICT_TBL_ELEM *
get_dict_string_tbl_lookup(BLOCK_HANDLE block_handle, ENV_INFO* env_info, int id)
{
	nsEDDEngine::DICT_REF_TBL *dict_string_tbl;
	if(get_dict_string_tbl_handle_x(block_handle, env_info, &dict_string_tbl))
		return NULL;

	if(!dict_string_tbl)
		return NULL;

	for(int i=0;i<dict_string_tbl->count;i++)
	{
		if(id==dict_string_tbl->list[i].dictionary_id)
			return &dict_string_tbl->list[i];
	}

	return NULL;
}

static nsEDDEngine::DICT_TBL_ELEM *
get_dict_string_tbl_lookup_name(BLOCK_HANDLE block_handle, ENV_INFO* env_info, const wchar_t *str_name)
{
	nsEDDEngine::DICT_REF_TBL *dict_string_tbl;
	if(get_dict_string_tbl_handle_x(block_handle, env_info, &dict_string_tbl))
		return NULL;

	for(int i=0;i<dict_string_tbl->count;i++)
	{
		wchar_t *table_str_name = (wchar_t*)dict_string_tbl->list[i].name.c_str();
		if( wcscmp(str_name, table_str_name) == 0)
		{
			return &dict_string_tbl->list[i];
		}
	}

	return NULL;
}

int get_symbol_tbl_handle_x(BLOCK_HANDLE block_handle, ENV_INFO* env_info, nsEDDEngine::SYMBOL_TBL **symbol_tbl_handle)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	::nsEDDEngine::FLAT_DEVICE_DIR		*flat_device_dir = 0;

	int rs = pConnectionMgr->get_abt_dd_dev_tbls (block_handle, (void **)(&flat_device_dir));
	if(rs)
		return rs;

	*symbol_tbl_handle = &flat_device_dir->symbol_tbl;

	return(rs);
}

nsEDDEngine::SYMBOL_TBL_ELEM *
get_symbol_tbl_lookup(BLOCK_HANDLE block_handle, ENV_INFO* env_info, int id)
{
	nsEDDEngine::SYMBOL_TBL *symbol_tbl=NULL;
	if(get_symbol_tbl_handle_x(block_handle, env_info, &symbol_tbl))
		return NULL;

	if(!symbol_tbl)
		return NULL;

	for(int i=0;i<symbol_tbl->count;i++)
	{
		if(id==symbol_tbl->list[i].symbol_id)
			return &symbol_tbl->list[i];
	}

	return NULL;
}

/*********************************************************************
 *
 *  Name: app_func_get_dict_string
 *
 *  ShortDesc:  Upcall to retreive a device specific string.
 *
 *  Description: Returns device specific string which has been specified as a device specific
 *               string reference.  The device specific string table to use is 
 *				 specified by the block_handle passed in.  There is one
 *				 device specific string table per device type.
 *
 *  Inputs:
 *		env_info - contains block_handle which is used to access device
 *					specific string table.
 *		dev_str_info - information about the string.
 *
 *  Outputs:
 *		str - pointer to a STRING structure to be filled by the application.
 *
 *  Returns:
 *		DDL_DEV_SPEC_STRING_NOT_FOUND;
 *		DDL_SUCCESS;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
int app_func_get_dict_string(
							ENV_INFO *env_info, 
							DDL_UINT dict_string_index,
							STRING *str )
{
	int nRetVal = DDL_DEV_SPEC_STRING_NOT_FOUND;
	// If this is a Standard Dictionary Default String

	if ((dict_string_index >> 16) == DICT_DFLT_SEC)
	{	
		switch(dict_string_index)
		{
		case DEFAULT_STD_DICT_STRING:
		case DEFAULT_DEV_SPEC_STRING:
		case DEFAULT_STD_DICT_HELP:
		case DEFAULT_STD_DICT_LABEL:
		case DEFAULT_STD_DICT_DESC:
			str->flags = STRING::DONT_FREE_STRING;
			str->str = L"";
			str->len = 0;
			break;
		case DEFAULT_STD_DICT_DISP_INT:
		case DEFAULT_STD_DICT_EDIT_INT:
			str->flags = STRING::DONT_FREE_STRING;
			str->str = L"d";
			str->len = 1;
			break;
		case DEFAULT_STD_DICT_DISP_UINT:
		case DEFAULT_STD_DICT_EDIT_UINT:
			str->flags = STRING::DONT_FREE_STRING;
			str->str = L"u";
			str->len = 1;
			break;
		case DEFAULT_STD_DICT_DISP_FLOAT:
		case DEFAULT_STD_DICT_DISP_DOUBLE:
		case DEFAULT_STD_DICT_EDIT_FLOAT:
		case DEFAULT_STD_DICT_EDIT_DOUBLE:
			str->flags = STRING::DONT_FREE_STRING;
			str->str = L"f";
			str->len = 1;
			break;
		case DEFAULT_STD_DICT_UNDEFINED:
			str->flags = STRING::DONT_FREE_STRING;
			str->str = L"Undefined";
			str->len = 9;
			break;
		default:
			return DDL_DEV_SPEC_STRING_NOT_FOUND;
		}

		return DDL_SUCCESS;
	}

	else// Normal string, get it from the dictionary string table
	{

		int rs=0;
		//
		// Find the pointer to the dictionary string table corresponding to block handle
		//
		nsEDDEngine::DICT_TBL_ELEM *entry = get_dict_string_tbl_lookup(env_info->block_handle, env_info, dict_string_index);
		if (!entry) 
		{	
			// If string not found
			//
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"\n\napp_func_get_dict_string: Dictionary String of id %d not found", dict_string_index );
		}
		else 
		{
			//
			// Retreive the device specific string information.
			//
			// Get the translation you want
			// If the strings are the same
			//		Use the one from the string table and DONT_FREE_STRING
			// else the translation was different
			//		Use the translation and FREE_STRING
			//

			wchar_t *table_str = (wchar_t*)entry->dictionary_entry.c_str();
			if( table_str )
			{
				int buf_len = wcslen(table_str);
				wchar_t *buf = (wchar_t *) malloc ((buf_len+1) * sizeof(wchar_t));

				rs = ddi_get_string_translation( table_str, env_info->lang_code, buf, buf_len );
				if ( rs != DDS_SUCCESS ) 
				{
					EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"\n\napp_func_get_dict_string: String translation failed" );
					free(buf);
				}
				else if( wcscmp(buf, table_str) == 0) 
				{												// If the strings are the same,
					str->flags = STRING::DONT_FREE_STRING;		// point directly at the table.
					str->str = table_str;
					str->len = (unsigned short) wcslen(str->str);
					free(buf);
					nRetVal = DDL_SUCCESS;
				}
				else 
				{
					str->flags = STRING::FREE_STRING;
					str->str = buf;
					str->len = (unsigned short) wcslen(str->str);
					nRetVal = DDL_SUCCESS;
				}
			}
		}
	}
	return nRetVal;
}

int app_func_get_dict_string_by_name(ENV_INFO *env_info, const wchar_t *str_name, STRING *str_value )
{
	int nRetVal = DDL_DEV_SPEC_STRING_NOT_FOUND;

	// Find the pointer to the dictionary string table corresponding to block handle
	::nsEDDEngine::DICT_TBL_ELEM *entry = get_dict_string_tbl_lookup_name(env_info->block_handle, env_info, str_name);
	if (!entry) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"Dictionary String table not found");
	}
	else 
	{
		//
		// Retreive the device specific string information.
		//
		// Get the translation you want
		// If the strings are the same
		//		Use the one from the string table and DONT_FREE_STRING
		// else the translation was different
		//		Use the translation and FREE_STRING
		//
		wchar_t *table_str = (wchar_t*)entry->dictionary_entry.c_str();

		if( table_str )
		{
			int buf_len = wcslen(table_str);
			wchar_t *buf = (wchar_t *) malloc ((buf_len+1) * sizeof(wchar_t));

			int rs = ddi_get_string_translation( table_str, env_info->lang_code, buf, buf_len );
			if ( rs != DDS_SUCCESS ) 
			{
				EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"FDI_DDS", L"String translation faile");
			}
			else if( wcscmp(buf, table_str) == 0) 
			{												// If the strings are the same,
				str_value->flags = STRING::DONT_FREE_STRING;		// point directly at the table.
				str_value->str = table_str;
				str_value->len = (unsigned short) wcslen(str_value->str);
				free(buf);
				nRetVal = DDL_SUCCESS;
			}
			else 
			{
				str_value->flags = STRING::FREE_STRING;
				str_value->str = buf;
				str_value->len = (unsigned short) wcslen(str_value->str);
				nRetVal = DDL_SUCCESS;
			}
		}
	}

	return nRetVal;
}

#pragma endregion Standard_Dict_Strings


