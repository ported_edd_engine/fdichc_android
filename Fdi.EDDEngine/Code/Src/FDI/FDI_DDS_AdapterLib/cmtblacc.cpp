/******************************************************************************
 * $Header: cmtblacc.cpp: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:54 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description: Connection Manager Tables module
 *				This file contains all access functions pertaining 
 *				to the Connection Manager Active Tables.
 *
 * $Workfile: cmtblacc.cpp$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/


#include "STDINC.H"

#include "cm_loc.h"
#include <malloc.h>
#include "cmTblManipulation.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/*
 * Functions for accessing Active Device Type Information
 */


int CDeviceTypeMgr::get_adtt_dd_device_id (DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID *ddi)
{
	if (!valid_device_type_handle(dth)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	ASSERT_RET(ddi, CM_BAD_POINTER);
	
	memcpy((char *)ddi, 
			(char *)ADTT_DD_DEVICE_ID(dth), 
			sizeof(DD_DEVICE_ID));

	return(CM_SUCCESS);
}


int CDeviceTypeMgr::get_adtt_symbol_info (DEVICE_TYPE_HANDLE dth, SYMINFO **pSymInfo)
{
	if (!valid_device_type_handle(dth)) 
	{ 
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}
	ASSERT_RET(pSymInfo, CM_BAD_POINTER);
	*pSymInfo =  active_dev_type_tbl.list[dth]->pSymInfo;
	return (CM_SUCCESS);
}


int CDeviceTypeMgr::set_adtt_symbol_info (DEVICE_TYPE_HANDLE dth, SYMINFO *pSymInfo)
{
	if (!valid_device_type_handle(dth)) 
	{ 
		return(CM_BAD_DEVICE_TYPE_HANDLE);
	}
	active_dev_type_tbl.list[dth]->pSymInfo = pSymInfo;
	return(CM_SUCCESS);
}




