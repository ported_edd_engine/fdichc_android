////////////////////////////////////////////////////////////////////////
//
//  $Workfile: cmTblManipulation.h$
// 
//  $Revision: 1$  
// 
//  $Date: Friday, August 9, 2019 8:00:54 AM$
// 
//  Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
// 
//  CCmTblManipulation.h : CCmTblManipulation class declaration file
//
////////////////////////////////////////////////////////////////////////

#ifndef _CM_TBL_GENERIC
#define _CM_TBL_GENERIC

#include "cm_struc.h"

#define LOG_CM_EVENTS FALSE

class CCmTblManipulation
{
public:
	static BLOCK_HANDLE BlockSearch(DEVICE_HANDLE deviceHandle, int objectIndex);
	static BLOCK_HANDLE BlockSearch(const char* tag, DEVICE_HANDLE deviceHandle);
	static DEVICE_TYPE_HANDLE DeviceTypeSearch(const DD_DEVICE_ID *ddDeviceId);
		
	static int NewActiveBlockTableEntry(BLOCK_HANDLE* blockHandle);
	static int NewActiveDeviceTableEntry(DEVICE_HANDLE* deviceHandle);
	static int NewActiveDeviceTypeTableEntry(DEVICE_TYPE_HANDLE* deviceTypeHandle);
			
	static void DeleteActiveBlockTableEntry(BLOCK_HANDLE blockHandle);
	static void DeleteActiveDeviceTableEntry(DEVICE_HANDLE deviceHandle);
	static void DeleteActiveDeviceTypeTableEntry(DEVICE_TYPE_HANDLE deviceTypeHandle);

	static int BlockClose(BLOCK_HANDLE blockHandle, int force);
	static int DeviceClose(DEVICE_HANDLE deviceHandle);
	static int DeviceTypeClose(DEVICE_TYPE_HANDLE deviceTypeHandle);

};

#endif