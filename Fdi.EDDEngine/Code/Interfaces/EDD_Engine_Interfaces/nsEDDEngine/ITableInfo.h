#pragma once

#include "nsEDDEngine/Table.h"

namespace nsEDDEngine
{
	// The ITableInfo interface provides a way to retrieve the static tables 
	// that contain information accumulated from various sources in the EDD
	class ITableInfo
	{
	public:
		virtual ~ITableInfo(void) {};

		// Returns a pointer to the Device Directory Tables
		virtual int GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir) = 0;

		// Returns a pointer to the Critical Prama Tables
		virtual int GetCriticalParams(int iBlockInstance, nsEDDEngine::CRIT_PARAM_TBL **pCriticalParamTbl) = 0;

		virtual int GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, ITEM_ID *pWAO_Item, ITEM_ID *pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList = NULL) = 0;

	};

}
