#include "stdafx.h"

#include "LegacyFF.h"
#include "LegacyFFFlats.h"
#include "LegacyFFFactory.h"

#include "../src/FF/FFHelper/dict.h"	// Move dict.h to inc/FF/FFHelper

CLegacyFF:: CLegacyFF(const wchar_t *szFilename, const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
	: CEDDEngineImpl(szFilename, pIEDDEngineLogger, nsEDDEngine::LegacyFF_DDS, pErrorCode)
{
	
	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;
	
	int r_code = 0;

	m_device_handle = -1;
	memset(&m_header, 0, sizeof(m_header));
	m_Flat_Device_Dir = NULL;
    wchar_t xmlReleaseDir[MAX_PATH] = {0};

	SetProtocol(nsEDDEngine::FF);

	//
	//Set up the dictionary path and make sure we are reading from the correct file
	//
    if (!CEDDEngineImpl::GetFieldFromXML(L"FFPath", pConfigXML, xmlReleaseDir, MAX_PATH - 1))
	{
		*pErrorCode = nsEDDEngine::CONFIG_FILE_ERROR;
		return;
	}


	//
	// Initialize the DDS tables.
	//
	m_connection_mgr.init_dds_tbls(xmlReleaseDir);


	//
	// Load FF dictionary table(s)
	//

	BOOL bRet = g_StdDictTable.LoadDict(xmlReleaseDir);

	if (bRet != TRUE)
	{
		*pErrorCode = nsEDDEngine::DICTIONARY_FILE_ERROR;
		return;
	}

	//
	// Load DD and open the device and the block
	//

	ENV_INFO env_info(this, 0, nsEDDEngine::DeviceLevel_BI, L"");
	r_code = m_connection_mgr.open_device(&env_info, szFilename, L"DummyDeviceName", &m_device_handle, &m_header);

	if(r_code != DDS_SUCCESS)
	{
		*pErrorCode = nsEDDEngine::LOAD_DD_AND_BLOCK_OPEN_ERROR;
		return;
	}
}

CLegacyFF::~CLegacyFF()
{
		//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;
	
	// make sure to clean FDI FLAT_DEVICE_DIR first and then DDS FLAT_DEVICE_DIR
	Clean_FLAT_DEVICE_DIR(m_Flat_Device_Dir);
	free(m_Flat_Device_Dir);
	m_Flat_Device_Dir = nullptr;

	for (int i = 0; i < m_connection_mgr.get_active_blk_tbl_count() ; i++)
	{
		if ( m_connection_mgr.valid_block_handle( i ) )
		{
			nsEDDEngine::BLOCK_INSTANCE block_instance;
			if (!BlockHandleToBlockInst(i, &block_instance))
			{
				ENV_INFO env_info(this, 0, block_instance, L"");
				m_connection_mgr.ct_block_close(&env_info, i);
			}
		}
	}


	if ( m_connection_mgr.valid_device_handle( m_device_handle ) )
	{
		ENV_INFO env_info(this, 0, nsEDDEngine::DeviceLevel_BI, L"");
		m_connection_mgr.ct_device_close( &env_info, m_device_handle );
	}



	m_connection_mgr.cleanup_dds_tbls();
}

int CLegacyFF::GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID * pUnitItemId)
{

	// Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert pParamSpec
	DDI_PARAM_SPECIFIER param_spec = {0};
	int rc = Init_DDI_PARAM_SPECIFIER(&param_spec, pParamSpec);

	if (rc == DDS_SUCCESS)
	{
		// Create a DDS TYPE_SIZE
		ITEM_ID_LIST item_id_list = {0};
		ENV_INFO env_info(this, pValueSpec, iBlockInstance, L"");
		rc = ddi_get_unit(&env_info, &block_spec,&param_spec, pUnitItemId);
		if(rc == DDS_SUCCESS)
		{
			if( item_id_list.count > 0 )
			{
				*pUnitItemId = item_id_list.list[0];
			}
		}
	}

    return rc;
}

int CLegacyFF::GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID* pUnitItemId)
{
	ENV_INFO env_info(this, pValueSpec, iBlockInstance, L"");


	// Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	    
	int rc = ddi_find_unit_ids(&env_info, &block_spec, AxisItemId, pUnitItemId, NULL);
    return rc;
}

// Retrieves the List of Commands that can be used to read/write this parameter
int CLegacyFF::GetCommandList(int /*iBlockInstance*/, nsEDDEngine::FDI_PARAM_SPECIFIER * /*pParamSpec*/, nsEDDEngine::CommandType /*eCmdType*/, nsEDDEngine::FDI_COMMAND_LIST * /*pCommandList*/)
{
	// Not applicable for FF
    return 0;
}

// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
int CLegacyFF::GetCmdIdFromNumber(int /*iBlockInstance*/, ulong /*ulCmdNumber*/, ITEM_ID* /*pCmdItemId*/)
{
    // Not applicable for FF
    return 0;
}

// Converts an EDD Engine Error code to a string
wchar_t *CLegacyFF::GetErrorString(int iErrorNum)
{
    int iMaxStringLength = 100;

	static wchar_t str[100] = _T("");
	
	return  dds_error_string_ff(iErrorNum, str, iMaxStringLength);
}

// GetStringTranslation() : Gets the specified language from multi-language string.
int CLegacyFF::GetStringTranslation(const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size)
{
//	return ddi_get_string_translation(string,(wchar_t*)lang_code,outbuf, outbuf_size);
	return ddi_get_string_translation(	const_cast<wchar_t*>(string),
										const_cast<wchar_t*>(lang_code),
										outbuf, outbuf_size);
}


CConnectionMgr* CLegacyFF::GetConnectionManager()
{
	return &m_connection_mgr;
}

int CLegacyFF::GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle )
{
	int rc = ConvertToBlockInstance( ulItemId, iOccurrence, piBlockInstance );

	if (rc == SUCCESS)
	{
		*piBlockHandle = BlockInstToBlockHandle( *piBlockInstance );
	}
	else
	{	// Set output parameters to invalid values
		*piBlockInstance = -1;
		*piBlockHandle = -1;
	}

	return rc;
}

void CLegacyFF::DDS_Log(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory)
{
	if (m_pIEDDEngineLogger)
	{
		m_pIEDDEngineLogger->Log(sMessage, eLogSeverity, sCategory);
	}
}

int CLegacyFF::GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType)
{
	ENV_INFO env_info(this, nullptr, iBlockInstance, L"");
    // Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert pParamSpec
	DDI_ITEM_SPECIFIER item_spec = {0};
	Init_DDI_ITEM_SPECIFIER(&item_spec, pItemSpec);

	// Create a DDS TYPE_SIZE
	ITEM_TYPE ItemType = {0};

    int rc = ddi_get_type(&env_info, &block_spec,&item_spec,&ItemType);
    if(DDS_SUCCESS == rc)
    {
        *pItemType = Set_ITEM_TYPE(ItemType);
    }
    return rc;
}

int CLegacyFF::GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE *pItemType, ITEM_ID *pItemId)
{
    // Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert pParamSpec
	DDI_ITEM_SPECIFIER item_spec = {0};
	Init_DDI_ITEM_SPECIFIER(&item_spec, pItemSpec);
	
	// Create a DDS TYPE_SIZE
	ITEM_TYPE ItemType = {0}; 
    // Create a DDS TYPE_SIZE
	ITEM_ID ItemID = {0};
	
	ENV_INFO env_info(this, 0, iBlockInstance, L"");
	// Make the call into FF DDS
	//                                  [in]         [in]         [out]
	int rc = ddi_get_type_and_id(&env_info, &block_spec, &item_spec, &ItemType, &ItemID);

	// Convert to pItemType
	if(rc == DDS_SUCCESS)
	{
		*pItemType = Set_ITEM_TYPE(ItemType);
        *pItemId = ItemID;
	}

	return rc;
}

//GetParamType() : Gets the type and size of the specified paramaeter
int CLegacyFF::GetParamType(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, nsEDDEngine::TYPE_SIZE *pTypeSize)
{
	// Convert iBlockInstance
	DDI_BLOCK_SPECIFIER block_spec = {0};
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert pParamSpec
	DDI_PARAM_SPECIFIER param_spec = {0};
	int rc = Init_DDI_PARAM_SPECIFIER(&param_spec, pParamSpec);
	
	if (rc == DDS_SUCCESS)
	{
		// Create a DDS TYPE_SIZE
		TYPE_SIZE type_size = {0}; 
		ENV_INFO env_info(this, 0, iBlockInstance, L"");
		// Make the call into FF DDS
		//                         [in]         [in]         [out]
		rc = ddi_get_var_type(&env_info, &block_spec, &param_spec, &type_size);
	
		// Convert to pTypeSize
		if(rc == DDS_SUCCESS)
		{
			Fill_EDD_TYPE_SIZE(pTypeSize, type_size);
		}
	}

	return rc;
	
}

//GetSymbolNameFromItemId(): Returns symbol name from sym info.
int CLegacyFF::GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size)
{
	SYMINFO* syminfo = nullptr;

 	int rc = g_DeviceTypeMgr.get_adtt_sym_info(m_connection_mgr.ADT_ADTT_OFFSET(m_device_handle), &syminfo);
	if(rc == DDS_SUCCESS)
	{
		rc = server_item_id_to_name(syminfo, item_id, item_name, outbuf_size);
	}

	return rc;
}
 
//GetItemIdFromSymbolName(): Returns item id from sym info.
int CLegacyFF::GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* IptemId)
{
	SYMINFO* syminfo = nullptr;

 	int rc = g_DeviceTypeMgr.get_adtt_sym_info(m_connection_mgr.ADT_ADTT_OFFSET(m_device_handle), &syminfo);

	if(rc == DDS_SUCCESS)
	{
		rc = server_item_name_to_id(syminfo, pItemName, IptemId);
	}

	return rc;
}

// GetItem : Fills in an item data structure with the requested attributes
int CLegacyFF::GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec, nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, nsEDDEngine::FDI_GENERIC_ITEM * pGenericItem )
{
	ENV_INFO				env_info(this, pValueSpec, iBlockInstance, lang_code);

	DDI_ITEM_SPECIFIER		item_spec = {0};
	DDI_BLOCK_SPECIFIER		block_spec = {0};
	DDI_GENERIC_ITEM        generic_item = {0};
	
	(void)memset((char *)&generic_item, 0, sizeof(DDI_GENERIC_ITEM));
	// Initializes DDI_BLOCK_SPECIFIER
	Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);
	// Convert to DDI_ITEM_SPECIFIER from FDI_ITEM_SPECIFIER
	Init_DDI_ITEM_SPECIFIER(&item_spec, pItemSpec);
	// Convert to DDI_GENERIC_ITEM from FDI_GENERIC_ITEM
	Init_DDI_GENERIC_ITEM(&generic_item, pGenericItem);
	
	int rc = -1;
	DDI_ATTR_REQUEST mask = 0;

	Fill_Bitmask( mask, *pAttributeNameSet,  pGenericItem->item_type);

	rc = ddi_get_item( &block_spec, &item_spec, &env_info, mask, &generic_item );

	if( (rc == DDS_SUCCESS) || (rc == DDL_DEFAULT_ATTR) || (rc == DDL_CHECK_RETURN_LIST) )
	{
		Fill_FDI_GENERIC_ITEM(iBlockInstance, pGenericItem, generic_item);
		// Only set rc from ddi_clean_item if is is non-zero
		// otherwise keep the DDL_DEFAULT_ATTTR or DDL_CHECK_RETURN_LIST from before
		int temp_rc = ddi_clean_item(&generic_item);
		if (temp_rc != DDS_SUCCESS)
		{
			rc = temp_rc;
		}
	}

	return rc;
}


// GetDeviceDir() : Gives pointer to device directory tables i.e. FLAT_DEVICE_DIR
int CLegacyFF::GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir)
{
	CLockDeviceTypeMgr dtMgrLock;

	int r_code = -1;
	if(m_Flat_Device_Dir == NULL)
	{	
		m_Flat_Device_Dir = (nsEDDEngine::FLAT_DEVICE_DIR*)calloc(1,sizeof(nsEDDEngine::FLAT_DEVICE_DIR));
		FLAT_DEVICE_DIR *pDdiFlatDeviceDir = NULL; 
		DDI_DEVICE_DIR_REQUEST		ddi_dir_req;

		r_code = m_connection_mgr.get_adt_dd_dev_tbls( m_device_handle, (void**)&pDdiFlatDeviceDir );
		ENV_INFO				env_info(this, 0, nsEDDEngine::DeviceLevel_BI, NULL);
		if ( r_code == CM_SUCCESS )
		{
			if (!(pDdiFlatDeviceDir->attr_avail & STRING_TBL_MASK))
			{
				(void) memset((char  *)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
				ddi_dir_req.type = DD_DT_HANDLE;
				ddi_dir_req.mask = STRING_TBL_MASK;
				r_code = m_connection_mgr.get_adt_adtt_offset( m_device_handle, &ddi_dir_req.spec.device_type_handle ); 
				
				if(r_code == DDS_SUCCESS)
				{
					r_code = ddi_device_dir_request( &env_info, &ddi_dir_req, pDdiFlatDeviceDir );
				}
			}
		}
		else
		{
			EddEngineLog(&env_info, __FILE__, __LINE__, BssError, L"LegacyFF", L"Device Tables not set... returned with error code %d", r_code );
		}

		if(r_code == DDS_SUCCESS)
		{
			Fill_FLAT_DEVICE_DIR(m_Flat_Device_Dir,pDdiFlatDeviceDir);
			*pDeviceDir = m_Flat_Device_Dir;
		}
		
		//return r_code;
	}
	else
	{
		*pDeviceDir = m_Flat_Device_Dir;
		r_code = 0;
	}

	return r_code;
}

//GetEDDFileHeader : 
void CLegacyFF::GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader)
{
	pEDDFileHeader->magic_number				= m_header.magic_number;
	pEDDFileHeader->header_size					= m_header.header_size;
	pEDDFileHeader->metadata_size				= m_header.data_size;
	pEDDFileHeader->item_objects_size			= m_header.objects_size;
	pEDDFileHeader->device_id.manufacturer		= m_header.manufacturer;
	pEDDFileHeader->device_id.device_type		= m_header.device_type;
	pEDDFileHeader->device_id.device_revision	= m_header.device_revision;
	pEDDFileHeader->device_id.dd_revision		= m_header.dd_revision;
//	pEDDFileHeader->major_rev					= m_header.lit80_major_rev;
//	pEDDFileHeader->minor_rev					= m_header.lit80_mminor_rev;
	pEDDFileHeader->edd_profile					= nsEDDEngine::EDD_Profile::PROFILE_FF;
//	pEDDFileHeader->reserved2					= m_header.reserved2;
//	pEDDFileHeader->signature					= m_header
	pEDDFileHeader->reserved3					= m_header.reserved3;
	pEDDFileHeader->reserved4					= m_header.reserved4;
	pEDDFileHeader->layout						= nsEDDEngine::LayoutType::COLUMNWIDTH_EQUAL;
}
BLOCK_HANDLE CLegacyFF::BlockInstToBlockHandle(nsEDDEngine::BLOCK_INSTANCE iBlockInstance) const
{
	BLOCK_HANDLE bh = -1;

	std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>::const_iterator cIter;

	cIter = m_mapBlockInst.find(iBlockInstance);

	if (cIter == m_mapBlockInst.cend())
	{
		assert(cIter != m_mapBlockInst.cend());	// MHD decide what to do here on error
	}
	else
	{
		bh = cIter->second;
	}

	return bh;
}


int CLegacyFF::BlockHandleToBlockInst(BLOCK_HANDLE bh, nsEDDEngine::BLOCK_INSTANCE *block_instance)
{
	int rc = DDS_SUCCESS;
	bool bFound = false;
	std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>::const_iterator cIter;

	for (cIter = m_mapBlockInst.cbegin(); cIter != m_mapBlockInst.cend(); cIter++)
	{
		if (cIter->second == bh)
		{
			*block_instance = cIter->first;
			bFound = true;
			break;
		}
	}
	if (!bFound)
	{
		rc = CM_BLOCK_NOT_FOUND;
	}
	return rc;
}

// Init_DDI_BLOCK_SPECIFIER() - Initializes a DDI_BLOCK_SPECIFIER from Block Instance
void CLegacyFF::Init_DDI_BLOCK_SPECIFIER( DDI_BLOCK_SPECIFIER *pBlockSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInst)
{
	(void)memset((char *)pBlockSpec, 0, sizeof(*pBlockSpec));

	if (iBlockInst == nsEDDEngine::DeviceLevel_BI)		// Rule to determine device level
	{
		pBlockSpec->type = DDI_DEVICE_HANDLE;
		pBlockSpec->block.handle = m_device_handle;		// This is now a device handle
	}
	else	// FF Block
	{
		pBlockSpec->type = DDI_BLOCK_HANDLE;
		pBlockSpec->block.handle = BlockInstToBlockHandle(iBlockInst);
	}
}


ENV_INFO::ENV_INFO ( const ENV_INFO &src )
{
    block_handle  = src.block_handle;
    app_info      = src.app_info;
    value_spec    = src.value_spec;
	handle_type   = src.handle_type;
	if(src.lang_code != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(lang_code, maxSize, src.lang_code, _TRUNCATE);
	}
}


ENV_INFO::ENV_INFO( const CLegacyFF *pLegacyFF, void* pValueSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInst, const wchar_t *iLangCode  )
{
	if (iBlockInst == nsEDDEngine::DeviceLevel_BI)		// Rule to determine device level
	{
		handle_type = ENV_INFO::DeviceHandle;
		block_handle = pLegacyFF->GetDeviceHandle();	// This is now a device handle
	}
	else	// an FF Block
	{
		handle_type = ENV_INFO::BlockHandle;
		block_handle = pLegacyFF->BlockInstToBlockHandle( iBlockInst );
	}

	app_info = (IDDSSupport*)pLegacyFF;
	value_spec = pValueSpec;
	if(iLangCode != NULL)
	{
		const size_t maxSize = 10; // Max size for lang code is 10
		PS_Wcsncpy(lang_code, maxSize, iLangCode, _TRUNCATE);
	}
}


//Init_DDI_ITEM_SPECIFIER() - Initializes a DDI_ITEM_SPECIFIER from an FDI_ITEM_SPECIFIER
void CLegacyFF::Init_DDI_ITEM_SPECIFIER( DDI_ITEM_SPECIFIER *pItemSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pFdiItemSpec)
{
	switch(pFdiItemSpec->eType)
	{
	case nsEDDEngine::FDI_ITEM_ID :
		pItemSpec->type = DDI_ITEM_ID;
		pItemSpec->item.id = pFdiItemSpec->item.id;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_ID_SI :
		pItemSpec->type = DDI_ITEM_ID_SI;
		pItemSpec->item.id = pFdiItemSpec->item.id;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	case nsEDDEngine::FDI_ITEM_PARAM :
		pItemSpec->type = DDI_ITEM_PARAM;
		pItemSpec->item.param = pFdiItemSpec->item.param;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_PARAM_SI :
		pItemSpec->type = DDI_ITEM_PARAM_SI;
		pItemSpec->item.param = pFdiItemSpec->item.param;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	case nsEDDEngine::FDI_ITEM_BLOCK :
		pItemSpec->type = DDI_ITEM_BLOCK;
		pItemSpec->item.id = 0;
		pItemSpec->subindex = 0;
		break;
	case nsEDDEngine::FDI_ITEM_CHARACTERISTICS :
		pItemSpec->type = DDI_ITEM_CHARACTERISTICS;
		pItemSpec->item.id = 0;
		pItemSpec->subindex = pFdiItemSpec->subindex;
		break;
	default:
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Init_DDI_ITEM_SPECIFIER() cannot convert ItemSpecifierType: %d", pFdiItemSpec->eType );
	}

}

// Init_DDI_PARAM_SPECIFIER() - Initializes a DDI_PARAM_SPECIFIER from an FDI_PARAM_SPECIFIER
//                                                      	[in/out]			                     [in]
int CLegacyFF::Init_DDI_PARAM_SPECIFIER( DDI_PARAM_SPECIFIER *pDdiParamSpec, const nsEDDEngine::FDI_PARAM_SPECIFIER *pFdiParamSpec)
{
	int rc = DDS_SUCCESS;

	switch(pFdiParamSpec->eType)
	{
	case nsEDDEngine::FDI_PS_ITEM_ID:
		pDdiParamSpec->type = DDI_PS_ITEM_ID;
		pDdiParamSpec->subindex = 0;
		pDdiParamSpec->item.id = pFdiParamSpec->id;
		break;
	case nsEDDEngine::FDI_PS_ITEM_ID_SI:
		pDdiParamSpec->type = DDI_PS_ITEM_ID_SI;
		pDdiParamSpec->subindex = pFdiParamSpec->subindex;
		pDdiParamSpec->item.id = pFdiParamSpec->id;
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET:
		pDdiParamSpec->type = DDI_PS_PARAM_OFFSET;
		pDdiParamSpec->subindex = 0;
		pDdiParamSpec->item.param = pFdiParamSpec->param;
		break;
	case nsEDDEngine::FDI_PS_PARAM_OFFSET_SI:
		pDdiParamSpec->type = DDI_PS_PARAM_OFFSET_SI;
		pDdiParamSpec->subindex = pFdiParamSpec->subindex;
		pDdiParamSpec->item.param = pFdiParamSpec->param;
		break;
	case nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX:
		rc = DDI_INVALID_PARAM;		// There are no Complex references in Legacy FF
		break;
	default:
		rc = DDI_INVALID_PARAM;
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Init_DDI_PARAM_SPECIFIER() cannot convert ParamSpecifierType: %d", pFdiParamSpec->eType );
	}

	return rc;
}

//Init_DDI_GENERIC_ITEM() - Initializes a DDI_GENERIC_ITEM from an FDI_GENERIC_ITEM
void CLegacyFF::Init_DDI_GENERIC_ITEM( DDI_GENERIC_ITEM *pDdiGenericItem, nsEDDEngine::FDI_GENERIC_ITEM * pFdiGenericItem)
{
	// RETURN_LIST i.e.Errors is output parameter only.
	//pDdiGenericItem->errors.count = pFdiGenericItem->errors.count;
	
	pDdiGenericItem->item = pFdiGenericItem->item;
	pDdiGenericItem->item_type = Set_DDI_ITEM_TYPE(pFdiGenericItem->item_type);

}


//Fill_FDI_GENERIC_ITEM() - Fills in an FDI_GENERIC_ITEM from DDI_GENERIC_ITEM
void CLegacyFF::Fill_FDI_GENERIC_ITEM(int iBlockInstance, nsEDDEngine::FDI_GENERIC_ITEM *pFdiGenericItem, DDI_GENERIC_ITEM pDdiGenericItem)
{	
	if(pDdiGenericItem.errors.count != 0)
	{
		for(int i = 0; i < pDdiGenericItem.errors.count; i++)
		{
            nsEDDEngine::AttributeNameSet ans; 
			pFdiGenericItem->errors.count						= pDdiGenericItem.errors.count;
            Fill_AttributeNameSet(ans, pDdiGenericItem.errors.list[i].bad_attr, Set_ITEM_TYPE(pDdiGenericItem.item_type));
            if(ans.empty())
            {
                // Error condition
                pFdiGenericItem->errors.list[i].bad_attr        = nsEDDEngine::_end_;
            }
            else
            {
			    pFdiGenericItem->errors.list[i].bad_attr        = ans.getFirstItem();
            }
			pFdiGenericItem->errors.list[i].rc					= pDdiGenericItem.errors.list[i].rc;

			pFdiGenericItem->errors.list[i].var_needed.block_instance = pDdiGenericItem.errors.list[i].var_needed.block_instance;
			pFdiGenericItem->errors.list[i].var_needed.op_info.id		= pDdiGenericItem.errors.list[i].var_needed.id;
			pFdiGenericItem->errors.list[i].var_needed.op_info.member	= pDdiGenericItem.errors.list[i].var_needed.subindex;

			pFdiGenericItem->errors.list[i].var_needed.op_info.type		= Set_ITEM_TYPE(pDdiGenericItem.errors.list[i].var_needed.type);
		}
	}

	switch(pDdiGenericItem.item_type)
	{
	case VARIABLE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_VARIABLE;
			FF_ConvFLAT_VAR *pFlatVar = new FF_ConvFLAT_VAR( (::FLAT_VAR*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatVar;
			pFdiGenericItem->item = dynamic_cast< nsEDDEngine::FLAT_VAR *>(pFlatVar);
			break;
		}
	case ITEM_ARRAY_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_ITEM_ARRAY;
			FF_ConvFLAT_ITEM_ARRAY *pFlatItemArray = new FF_ConvFLAT_ITEM_ARRAY( (FLAT_ITEM_ARRAY*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatItemArray;
			break;
		}
	case COLLECTION_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_COLLECTION;
			FF_ConvFLAT_COLLECTION *pFlatCollection = new FF_ConvFLAT_COLLECTION( (FLAT_COLLECTION*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatCollection;
			break;
		}
	case ARRAY_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_ARRAY;
			FF_ConvFLAT_ARRAY *pArray = new FF_ConvFLAT_ARRAY( (FLAT_ARRAY*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pArray;
			break;
		}
	case RESP_CODES_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_RESP_CODES;
			FF_ConvFLAT_RESP_CODE *pFlatRespCode = new FF_ConvFLAT_RESP_CODE( (FLAT_RESP_CODE*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatRespCode;
			break;
		}
	case LIST_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_LIST;
			FF_ConvFLAT_LIST *pFlatList = new FF_ConvFLAT_LIST( (FLAT_LIST*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatList;
			break;
		}
	case FILE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_FILE;
			FF_ConvFLAT_FILE *pFlatFile = new FF_ConvFLAT_FILE( (FLAT_FILE*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatFile;
			break;
		}
	case CHART_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_CHART;
			FF_ConvFLAT_CHART *pFlatChart = new FF_ConvFLAT_CHART( (FLAT_CHART*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatChart;
			break;
		}
	case GRAPH_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_GRAPH;
			FF_ConvFLAT_GRAPH *pFlatGraph = new FF_ConvFLAT_GRAPH( (FLAT_GRAPH*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatGraph;
			break;
		}
	case AXIS_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_AXIS;
			FF_ConvFLAT_AXIS *pFlatAxis = new FF_ConvFLAT_AXIS( (FLAT_AXIS*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatAxis;
			break;
		}
	case WAVEFORM_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_WAVEFORM;
			FF_ConvFLAT_WAVEFORM *pFlatWaveform = new FF_ConvFLAT_WAVEFORM((FLAT_WAVEFORM*)pDdiGenericItem.item, this );			
			pFdiGenericItem->item = pFlatWaveform;
			break;
		}
	case SOURCE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_SOURCE;
			FF_ConvFLAT_SOURCE *pFlatSource = new FF_ConvFLAT_SOURCE((FLAT_SOURCE*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatSource;
			break;
		}
	case GRID_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_GRID;
			FF_ConvFLAT_GRID *pFlatGrid = new FF_ConvFLAT_GRID((FLAT_GRID*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatGrid;
			break;
		}
	case IMAGE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_IMAGE;
			FF_ConvFLAT_IMAGE *pFlatImage = new FF_ConvFLAT_IMAGE((FLAT_IMAGE*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatImage;
			break;
		}
	case METHOD_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_METHOD;
			FF_ConvFLAT_METHOD *pFlatMethod = new FF_ConvFLAT_METHOD((FLAT_METHOD*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatMethod;
			break;
		}
	case MENU_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_MENU;
			FF_ConvFLAT_MENU *pFlatMenu = new FF_ConvFLAT_MENU((FLAT_MENU*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatMenu;
			break;
		}
	case EDIT_DISP_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_EDIT_DISP;
			FF_ConvFLAT_EDIT_DISPLAY *pFlatEditDisplay = new FF_ConvFLAT_EDIT_DISPLAY((FLAT_EDIT_DISPLAY*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatEditDisplay;
			break;
		}
	case REFRESH_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_REFRESH;
			FF_ConvFLAT_REFRESH *pFlatRefresh = new FF_ConvFLAT_REFRESH((FLAT_REFRESH*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatRefresh;
			break;
		}
	case UNIT_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_UNIT;
			FF_ConvFLAT_UNIT *pFlatUnit = new FF_ConvFLAT_UNIT((FLAT_UNIT*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatUnit;
			break;
		}
	case WAO_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_WAO;
			FF_ConvFLAT_WAO *pFlatWao = new FF_ConvFLAT_WAO((FLAT_WAO*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatWao;
			break;
		}
	case BLOCK_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_BLOCK;
			FF_ConvFLAT_BLOCK *pFlatBlock = new FF_ConvFLAT_BLOCK((FLAT_BLOCK*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatBlock;
			break;
		}
	case RECORD_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_RECORD;
			FF_ConvFLAT_RECORD *pFlatRecord = new FF_ConvFLAT_RECORD((FLAT_RECORD*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatRecord;
			break;
		}
	case VAR_LIST_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_VAR_LIST;
			FF_ConvFLAT_VAR_LIST *pFlatVarList = new FF_ConvFLAT_VAR_LIST((FLAT_VAR_LIST*)pDdiGenericItem.item, this );
			pFdiGenericItem->item = pFlatVarList;
			break;
		}
	case TEMPLATE_ITYPE:
		{
			pFdiGenericItem->item_type = nsEDDEngine::ITYPE_TEMPLATE;
			FF_ConvFLAT_TEMPLATE *pFlatTemplate = new FF_ConvFLAT_TEMPLATE(iBlockInstance, (FLAT_TEMPLATE*)pDdiGenericItem.item, this);
			pFdiGenericItem->item = pFlatTemplate;
			break;
		}
	default:
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Fill_FDI_GENERIC_ITEM() cannot convert ITEM_TYPE: %d", pDdiGenericItem.item_type );
	}	
	
	// This code is use to assign symbol name to the item because the Legacy FF DDS is not handling the symbol names.
    wchar_t strTemp[MAX_PATH+1]{0};			// Put the name into a large buffer first
	int rs = GetSymbolNameFromItemId(pFdiGenericItem->item->id, strTemp, MAX_PATH);
	pFdiGenericItem->item->symbol_name = (rs == DDL_SUCCESS) ? strTemp : L"" ;	// copy it in, if successful

}


//Set_ITEM_TYPE(): Retunrs FDI item type 
nsEDDEngine::ITEM_TYPE CLegacyFF::Set_ITEM_TYPE(ITEM_TYPE itemType)
{
	nsEDDEngine::ITEM_TYPE fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;

	switch(itemType)
	{
	case RESERVED_ITYPE1:
		fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;
		break;
	case VARIABLE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VARIABLE;
		break;
	case COMMAND_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COMMAND;
		break;
	case MENU_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MENU;
		break;
	case EDIT_DISP_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_EDIT_DISP;
		break;
	case METHOD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_METHOD;
		break;
	case REFRESH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_REFRESH;
		break;
	case UNIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_UNIT;
		break;
	case WAO_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAO;
		break;
	case ITEM_ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ITEM_ARRAY;
		break;
	case COLLECTION_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLLECTION;
		break;
	case RESERVED_ITYPE2:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK_B;
		break;
	case BLOCK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_BLOCK;
		break;
#if 0 // PROGRAM_ITYPE Not used in FDI
 	case PROGRAM_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_PROGRAM;
		break;
#endif
	case RECORD_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RECORD;
		break;
	case ARRAY_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ARRAY;
		break;
	case VAR_LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_VAR_LIST;
		break;
	case RESP_CODES_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_RESP_CODES;
		break;
#if 0 // DOMAIN_ITYPE Not used in FDI
 	case DOMAIN_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_DOMAIN;
		break;
#endif
	case MEMBER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_MEMBER;
		break;
	case AXIS_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_AXIS;
		break;
	case CHART_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CHART;
		break;
	case FILE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_FILE;
		break;
	case GRAPH_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRAPH;
		break;
	case LIST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_LIST;
		break;
	case SOURCE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SOURCE;
		break;
	case WAVEFORM_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_WAVEFORM;
		break;
	case GRID_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_GRID;
		break;
	case IMAGE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_IMAGE;
		break;
	case TEMPLATE_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_TEMPLATE;
		break;
#if 0 // These item types are not supported by legacy FF
	
	case 30:
		fdiItemType = nsEDDEngine::ITYPE_PLUGIN;
		break;
	case 31:
		fdiItemType = nsEDDEngine::ITYPE_TEMPLATE;
		break;
	case 32:
		fdiItemType = nsEDDEngine::ITYPE_RESERVED;
		break;
	case 33:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT;
		break;
	case 34:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_FOLDER;
		break;
	case 35:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_REFERENCE;
		break;
	case 36:
		fdiItemType = nsEDDEngine::ITYPE_COMPONENT_RELATION;
		break;
	case SEPARATOR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_SEPARATOR;
		break;
	case ROWBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ROWBREAK;
		break;
	case COLUMNBREAK_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_COLUMNBREAK;
		break;
#endif
	case ENUM_BIT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ENUM_BIT;
		break;
#if 0 // These item types are not supported by legacy FF
	case STRING_LITERAL_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_STRING_LITERAL;
		break;
	case CONST_INTEGER_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_INTEGER;
		break;
	case CONST_FLOAT_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST_FLOAT;
		break;
	case CONST_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_CONST;
		break;
	case 48:
		fdiItemType = nsEDDEngine::ITYPE_SELECTOR;
		break;
	case 49:
		fdiItemType = nsEDDEngine::ITYPE_LOCAL_PARAM;
		break;
	case 50:
		fdiItemType = nsEDDEngine::ITYPE_METH_ARGS;
		break;
	case ATTR_ITYPE:
		fdiItemType = nsEDDEngine::ITYPE_ATTR;
		break;
#endif	

		// MHD These need to be fixed, but for now can return the unassigned value
	case 200:		// #define PARAM_ITYPE             200
	case 201:		// #define PARAM_LIST_ITYPE        201
	case 202:		// #define BLOCK_CHAR_ITYPE        202
	case 203:		// #define LOCAL_PARAM_ITYPE       203
	case 204:		// #define BLOCK_XREF_ITYPE        204  /* Cross-block reference */

		fdiItemType = nsEDDEngine::ITYPE_NO_VALUE;
		break;

	default:
		EddEngineLog(this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Set_ITEM_TYPE() cannot convert ITEM_TYPE: %d", itemType );
	}
	return fdiItemType;
}


//Set_DDI_ITEM_TYPE(): Retunrs DDI item type 
ITEM_TYPE CLegacyFF::Set_DDI_ITEM_TYPE(nsEDDEngine::ITEM_TYPE itemType)
{
	ITEM_TYPE DdiItemType = RESERVED_ITYPE1;

	switch(itemType)
	{
	case nsEDDEngine::ITYPE_NO_VALUE:
		DdiItemType = RESERVED_ITYPE1;
		break;
	case nsEDDEngine::ITYPE_VARIABLE:
		DdiItemType = VARIABLE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COMMAND:
		DdiItemType = COMMAND_ITYPE;
		break;
	case nsEDDEngine::ITYPE_MENU:
		DdiItemType = MENU_ITYPE;
		break;
	case nsEDDEngine::ITYPE_EDIT_DISP:
		DdiItemType = EDIT_DISP_ITYPE;
		break;
	case nsEDDEngine::ITYPE_METHOD:
		DdiItemType = METHOD_ITYPE;
		break;
	case nsEDDEngine::ITYPE_REFRESH:
		DdiItemType = REFRESH_ITYPE;
		break;
	case nsEDDEngine::ITYPE_UNIT:
		DdiItemType = UNIT_ITYPE;
		break;
	case nsEDDEngine::ITYPE_WAO:
		DdiItemType = WAO_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ITEM_ARRAY:
		DdiItemType = ITEM_ARRAY_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COLLECTION:
		DdiItemType = COLLECTION_ITYPE;
		break;
	case nsEDDEngine::ITYPE_BLOCK_B:
		DdiItemType = RESERVED_ITYPE2;
		break;
	case nsEDDEngine::ITYPE_BLOCK:
		DdiItemType = BLOCK_ITYPE;
		break;
	case nsEDDEngine::ITYPE_RECORD:
		DdiItemType = RECORD_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ARRAY:
		DdiItemType = ARRAY_ITYPE;
		break;
	case nsEDDEngine::ITYPE_VAR_LIST:
		DdiItemType = VAR_LIST_ITYPE;
		break;
	case nsEDDEngine::ITYPE_RESP_CODES:
		DdiItemType = RESP_CODES_ITYPE;
		break;
	case nsEDDEngine::ITYPE_MEMBER:
		DdiItemType = MEMBER_ITYPE;
		break;
	case nsEDDEngine::ITYPE_FILE:
		DdiItemType = FILE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CHART:
		DdiItemType = CHART_ITYPE;
		break;
	case nsEDDEngine::ITYPE_GRAPH:
		DdiItemType = GRAPH_ITYPE;
		break;
	case nsEDDEngine::ITYPE_AXIS:
		DdiItemType = AXIS_ITYPE;
		break;
	case nsEDDEngine::ITYPE_WAVEFORM:
		DdiItemType = WAVEFORM_ITYPE;
		break;
	case nsEDDEngine::ITYPE_SOURCE:
		DdiItemType = SOURCE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_LIST:
		DdiItemType = LIST_ITYPE;
		break;
	case nsEDDEngine::ITYPE_GRID:
		DdiItemType = GRID_ITYPE;
		break;
	case nsEDDEngine::ITYPE_IMAGE:
		DdiItemType = IMAGE_ITYPE;
		break;
	case nsEDDEngine::ITYPE_TEMPLATE:
		DdiItemType = TEMPLATE_ITYPE;
		break;
#if 0	// below types are not present in FF Legacy
	case nsEDDEngine::ITYPE_SEPARATOR:
		DdiItemType = SEPARATOR_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ROWBREAK:
		DdiItemType = ROWBREAK_ITYPE;
		break;
	case nsEDDEngine::ITYPE_COLUMNBREAK:
		DdiItemType = COLUMNBREAK_ITYPE;
		break;
#endif
	case nsEDDEngine::ITYPE_ENUM_BIT:
		DdiItemType = ENUM_BIT_ITYPE;
		break;
#if 0	// below types are not present in FF Legacy
	case nsEDDEngine::ITYPE_STRING_LITERAL:
		DdiItemType = STRING_LITERAL_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CONST_INTEGER:
		DdiItemType = CONST_INTEGER_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CONST_FLOAT:
		DdiItemType = CONST_FLOAT_ITYPE;
		break;
	case nsEDDEngine::ITYPE_CONST:
		DdiItemType = CONST_ITYPE;
		break;
	case nsEDDEngine::ITYPE_ATTR:
		DdiItemType = ATTR_ITYPE;
		break;
#endif	

	default:
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Set_DDI_ITEM_TYPE() cannot convert nsEDDEngine::ITEM_TYPE: %d", itemType );
	}
	return DdiItemType;
}


// SetClassType() - converts unsigned long value to FDI ClassType enum value
nsEDDEngine::ClassType CLegacyFF::SetClassType(ulong class_attr)
{
	nsEDDEngine::ClassType eTempType = nsEDDEngine::CT_NONE;

	if(class_attr & DIAGNOSTIC_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DIAGNOSTIC; 
	}
	if(class_attr & DYNAMIC_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DYNAMIC; 
	}
	if(class_attr & SERVICE_CLASS)
	{
		eTempType |= nsEDDEngine::CT_SERVICE; 
	}
	if(class_attr & CORRECTION_CLASS)
	{
		eTempType |= nsEDDEngine::CT_CORRECTION; 
	}
	if(class_attr & COMPUTATION_CLASS)
	{
		eTempType |= nsEDDEngine::CT_COMPUTATION; 
	}
	if(class_attr & INPUT_BLOCK_CLASS) // INPUT_BLOCK_CLASS is not present in FDI. We are assuming  CT_ANALOG_INPUT as INPUT_BLOCK_CLASS
	{
		eTempType |= nsEDDEngine::CT_ANALOG_INPUT; 
	}
	if(class_attr & ANALOG_OUTPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_ANALOG_OUTPUT; 
	}
	if(class_attr & HART_CLASS)
	{
		eTempType |= nsEDDEngine::CT_HART; 
	}
	if(class_attr & LOCAL_DISPLAY_CLASS)
	{
		eTempType |= nsEDDEngine::CT_LOCAL_DISPLAY; 
	}
	if(class_attr & FREQUENCY_CLASS)					// There are 3 Frequency classes in FDI, Frequency(CT_FREQUENCY), Frequency input(CT_FREQUENCY_INPUT)     
	{													// and Frequency output(CT_FREQUENCY_OUTPUT).    
		eTempType |= nsEDDEngine::CT_FREQUENCY;			// As per new HART class definitions Frequency(CT_FREQUENCY) is considered. 
	}													
	
	if(class_attr & DISCRETE_CLASS)						// There are 3 Discrete classes in FDI, Discrete(CT_DISCRETE), Discrete input(CT_DISCRETE_INPUT)
	{													// and Discrete output(CT_DISCRETE_OUTPUT).
		eTempType |= nsEDDEngine::CT_DISCRETE;			// As per new HART class definitions descrete(CT_DISCRETE) is considered. 
	}													
	if(class_attr & DEVICE_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DEVICE; 
	}
	if(class_attr & LOCAL_CLASS)
	{
		eTempType |= nsEDDEngine::CT_LOCAL_A;
	}
	if(class_attr & INPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_INPUT; 
	}
	if(class_attr & OUTPUT_CLASS)				
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_OUTPUT;	//  DDS value is 0x004000
	}											//	FDI value is 0x00800000
	if(class_attr & CONTAINED_CLASS)
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_CONTAINED; //  DDS value is 0x008000
	}											//	FDI value is 0x020000
	if(class_attr & OPERATE_CLASS)				
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_OPERATE;	//  DDS value is 0x010000 			
	}											//	FDI value is 0x004000
	if(class_attr & ALARM_CLASS)				
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_ALARM;		//  DDS value is 0x020000 			
	}											//	FDI value is 0x008000
	if(class_attr & TUNE_CLASS)
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_TUNE;		//  DDS value is 0x100000
	}											//	FDI value is 0x00400000	
	if(class_attr & FACTORY_CLASS)
	{											//	Values are different in DDS and FDI
		eTempType |= nsEDDEngine::CT_FACTORY;	//  DDS value is 0x040000
	}											//	FDI value is 0x80000000
	// below types are not available in legacy FF
#if 0 
	if(class_attr & LOOP_IN_MANUAL_CLASS)		// LOOP_IN_MANUAL_CLASS is not available in FDI
	{
		//eTempType |= nsEDDEngine::CT_DIAGNOSTIC;  
	}

	if(class_attr & ANALOG_INPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_ANALOG_INPUT; 
	}
	if(class_attr & DIGITAL_INPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DIGITAL_INPUT; 
	}
	if(class_attr & DIGITAL_OUTPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DIGITAL_OUTPUT; 
	}
	if(class_attr & DISCRETE_INPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DISCRETE_INPUT; 
	}
	if(class_attr & DISCRETE_OUTPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_DISCRETE_OUTPUT; 
	}
	if(class_attr & FREQUENCY_INPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_FREQUENCY_INPUT; 
	}
	if(class_attr & FREQUENCY_OUTPUT_CLASS)
	{
		eTempType |= nsEDDEngine::CT_FREQUENCY_OUTPUT; 
	}
#endif

	return eTempType;
}

nsEDDEngine::Handling CLegacyFF::SetHandling(unsigned long hadling)
{
	nsEDDEngine::Handling eHandling = (nsEDDEngine::Handling)0;

	if(hadling & READ_HANDLING)
	{
		eHandling |= nsEDDEngine::FDI_READ_HANDLING;
	}
	if(hadling & WRITE_HANDLING)
	{
		eHandling |= nsEDDEngine::FDI_WRITE_HANDLING;
	}

	return eHandling;
}

nsEDDEngine::Boolean CLegacyFF::SetBoolean(unsigned long bVal)
{
	nsEDDEngine::Boolean eValid = nsEDDEngine::False;

	if(bVal == 0)
	{
		eValid = nsEDDEngine::False;
	}
	else 
	{
		eValid = nsEDDEngine::True;
	}

	return eValid;
}

unsigned int CLegacyFF::Set_DDI_COMMAND_TYPE(nsEDDEngine::CommandType commandType)
{
    unsigned int cmd_type = 0;
    switch(commandType)
    {
    case nsEDDEngine::FDI_READ_COMMAND:
        cmd_type = DDI_READ_COMMAND;
        break;
    case nsEDDEngine::FDI_WRITE_COMMAND:
        cmd_type = DDI_WRITE_COMMAND;
        break;
	default:
		EddEngineLog(this,  __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Set_DDI_COMMAND_TYPE() cannot convert nsEDDEngine::CommandType: %d", commandType );
    }
    return cmd_type; 
}

nsEDDEngine::DisplaySize CLegacyFF::SetDisplaySize(unsigned long dispsize)
{
	nsEDDEngine::DisplaySize eSize =  (nsEDDEngine::DisplaySize)0;
	
	switch(dispsize)
	{
	case DISPLAY_SIZE_XX_SMALL:
		eSize = nsEDDEngine::DISPSIZE_XX_SMALL;
		break;
	case DISPLAY_SIZE_X_SMALL:
		eSize = nsEDDEngine::DISPSIZE_X_SMALL;
		break;
	case DISPLAY_SIZE_SMALL:
		eSize = nsEDDEngine::DISPSIZE_SMALL;
		break;
	case DISPLAY_SIZE_MEDIUM:
		eSize = nsEDDEngine::DISPSIZE_MEDIUM;
		break;
	case DISPLAY_SIZE_LARGE:
		eSize = nsEDDEngine::DISPSIZE_LARGE;
		break;
	case DISPLAY_SIZE_X_LARGE:
		eSize = nsEDDEngine::DISPSIZE_X_LARGE;
		break;
	case DISPLAY_SIZE_XX_LARGE:
		eSize = nsEDDEngine::DISPSIZE_XX_LARGE;
		break;
	default:
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::SetDisplaySize() cannot convert display size: %d", dispsize );
	}

	return eSize;
}

nsEDDEngine::LineType CLegacyFF::SetLineType(unsigned long linetype)
{
	nsEDDEngine::LineType eLineType = (nsEDDEngine::LineType)0;

	switch(linetype)
	{
	case LINE_TYPE_LOW_LOW_LIMIT:
		eLineType = nsEDDEngine::FDI_LOWLOW_LINETYPE;
		break;
	case LINE_TYPE_LOW_LIMIT:
		eLineType = nsEDDEngine::FDI_LOW_LINETYPE;
		break;
	case LINE_TYPE_HIGH_LIMIT:
		eLineType = nsEDDEngine::FDI_HIGH_LINETYPE;
		break;
	case LINE_TYPE_HIGH_HIGH_LIMIT:
		eLineType = nsEDDEngine::FDI_HIGHHIGH_LINETYPE;
		break;
	case LINE_TYPE_TRANSPARENT:
		eLineType = nsEDDEngine::FDI_TRANSPARENT_LINETYPE;
		break;
	case LINE_TYPE_DATA_N:
		eLineType = nsEDDEngine::FDI_DATA0_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 1:
		eLineType = nsEDDEngine::FDI_DATA1_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 2:
		eLineType = nsEDDEngine::FDI_DATA2_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 3:
		eLineType = nsEDDEngine::FDI_DATA3_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 4:
		eLineType = nsEDDEngine::FDI_DATA4_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 5:
		eLineType = nsEDDEngine::FDI_DATA5_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 6:
		eLineType = nsEDDEngine::FDI_DATA6_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 7:
		eLineType = nsEDDEngine::FDI_DATA7_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 8:
		eLineType = nsEDDEngine::FDI_DATA8_LINETYPE;
		break;
	case LINE_TYPE_DATA_N + 9:
		eLineType = nsEDDEngine::FDI_DATA9_LINETYPE;
		break;
	default:
		EddEngineLog( this,  __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::SetLineType() cannot convert line type: %d", linetype );
	}

	return eLineType;
}

int CLegacyFF::DDS_GetParamValue(BLOCK_HANDLE bh, void* pValueSpec, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value)
{
	int rc = PC_FAIL;

	//Initialize FDI data structures
	nsConsumer::EVAL_VAR_VALUE fdi_param_value;

	nsEDDEngine::FDI_PARAM_SPECIFIER	param_spec;
	param_spec.id = op_ref->id;
	param_spec.subindex = op_ref->subindex;

	switch (op_ref->type)
	{
	case VARIABLE_ITYPE:
		param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
		break;
	case RECORD_ITYPE:
	case ARRAY_ITYPE:
		param_spec.eType = nsEDDEngine::FDI_PS_ITEM_ID_SI;
		break;
	default:
		return PC_BAD_PARAM_REQUEST;
	}
	nsEDDEngine::BLOCK_INSTANCE iBlockInstance = -1;
	rc = BlockHandleToBlockInst(bh, &iBlockInstance);

	if (rc == DDS_SUCCESS)
	{
		nsConsumer::PC_ErrorCode ec = nsConsumer::PC_INVALID_EC;
		if (m_pIParamCache)
		{
			// Call Consumer GetParamValue with FDI data structures
			ec = m_pIParamCache->GetParamValue(iBlockInstance, pValueSpec, &param_spec, &fdi_param_value);
		}
		if (ec == nsConsumer::PC_SUCCESS_EC)
		{
			// Fill DDS EVAL_VAR_VALUE with FDI EVAL_VAR_VALUE
			Fill_DDI_EVAL_VAR_VALUE(param_value, &fdi_param_value);
		}

		switch (ec)		// Convert the PC_ErrorCode into our own return codes
		{
		case nsConsumer::PC_SUCCESS_EC:		rc = PC_SUCCESS;		break;
		case nsConsumer::PC_BUSY_EC:		rc = PC_BUSY;			break;
		case nsConsumer::PC_INVALID_EC:		rc = PC_VALUE_NOT_SET;	break;
		case nsConsumer::PC_CIRC_DEPEND_EC:	rc = L7_CIRCULAR_DEPENDENCY_DETECTED;	break;
		default:
		case nsConsumer::PC_OTHER_EC:		rc = PC_INTERNAL_ERROR;	break;
		}

	}
	return rc;
}



void CLegacyFF::Fill_DDI_EVAL_VAR_VALUE(::EVAL_VAR_VALUE *pDdiEvalVarValue, nsConsumer::EVAL_VAR_VALUE *pFdiEvalVarValue )
{
	pDdiEvalVarValue->size = pFdiEvalVarValue->size;
	
	switch(pFdiEvalVarValue->type)
	{
	case nsEDDEngine::VT_DDS_TYPE_UNUSED:
		pDdiEvalVarValue->type = DDSUNUSED;
		break;
	case nsEDDEngine::VT_INTEGER:
		pDdiEvalVarValue->type = INTEGER;
		pDdiEvalVarValue->val.i =  (long)pFdiEvalVarValue->val.i;
		break;
	case nsEDDEngine::VT_UNSIGNED:
		pDdiEvalVarValue->type = UNSIGNED;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_FLOAT:
		pDdiEvalVarValue->type = FLOAT;
		pDdiEvalVarValue->val.f =  pFdiEvalVarValue->val.f;
		break;
	case nsEDDEngine::VT_DOUBLE:
		pDdiEvalVarValue->type = DOUBLE;
		pDdiEvalVarValue->val.d =  pFdiEvalVarValue->val.d;
		break;
	case nsEDDEngine::VT_ENUMERATED:
		pDdiEvalVarValue->type = ENUMERATED;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_BIT_ENUMERATED:
		pDdiEvalVarValue->type = BIT_ENUMERATED;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_INDEX:
		pDdiEvalVarValue->type = INDEX;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_ASCII:
		pDdiEvalVarValue->type = ASCII;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_PACKED_ASCII:
		pDdiEvalVarValue->type = PACKED_ASCII;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_PASSWORD:
		pDdiEvalVarValue->type = PASSWORD;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_EDD_DATE:
		pDdiEvalVarValue->type = HART_DATE_FORMAT;
		pDdiEvalVarValue->val.i =  (long)pFdiEvalVarValue->val.i;
		break;
	case nsEDDEngine::VT_TIME:
		pDdiEvalVarValue->type = TIME;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_DATE_AND_TIME:
		pDdiEvalVarValue->type = DATE_AND_TIME;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_DURATION:
		pDdiEvalVarValue->type = DURATION;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_EUC:
		pDdiEvalVarValue->type = EUC;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_VISIBLESTRING:
		pDdiEvalVarValue->type = VISIBLESTRING;
		Fill_DDS_STRING(&pDdiEvalVarValue->val.s, &pFdiEvalVarValue->val.s);
		break;
	case nsEDDEngine::VT_TIME_VALUE:
		pDdiEvalVarValue->type = TIME_VALUE;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_BOOLEAN:
		pDdiEvalVarValue->type =  BOOLEAN_T;
		pDdiEvalVarValue->val.u =  (unsigned long)pFdiEvalVarValue->val.u;
		break;
	case nsEDDEngine::VT_OCTETSTRING: 	
	case nsEDDEngine::VT_BITSTRING: 	
	default:
		EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Fill_DDI_EVAL_VAR_VALUE() cannot convert VariableType: %d", pFdiEvalVarValue->type );
	}
}

void CLegacyFF::Fill_DDS_STRING(STRING *pDdiString, nsEDDEngine::STRING *pFdiString)
{
	pDdiString->len = (unsigned short)pFdiString->length();
	pDdiString->flags = FREE_STRING;
	pDdiString->str = (wchar_t*)calloc(1,sizeof(wchar_t)*(pFdiString->length() + 1));
	PS_Wcscpy(pDdiString->str, pFdiString->length() + 1, pFdiString->c_str());  
}


int CLegacyFF::GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info(this, 0, nsEDDEngine::DeviceLevel_BI, lang_code);
	
	STRING string = {0};
	
	int rc = app_func_get_dict_string(&env_info, ulIndex, &string);
	
	if (rc == DDL_SUCCESS) 
	{
		if (string.len < iStringLen)
		{
			(void)PS_Wcscpy(pString, iStringLen, string.str);
		} 
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}

	if(string.flags == FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}


int CLegacyFF::GetDevSpecString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code )
{
	ENV_INFO env_info(this, 0, nsEDDEngine::DeviceLevel_BI, lang_code);
	
	STRING string = {0};

	DEV_STRING_INFO dev_string_Info = {0};

	dev_string_Info.id = ulIndex;
	
	int rc = app_func_get_dev_spec_string(&env_info, &dev_string_Info, &string );

	if (rc == DDL_SUCCESS) 
	{
		if (string.len < iStringLen)
		{
			(void)PS_Wcscpy(pString, iStringLen, string.str);	
		} 
		else
		{
			rc = DDI_INSUFFICIENT_BUFFER;
		}
	}
	
	if(string.flags == FREE_STRING)
	{
		free((void*)string.str);
	}
	
	return rc;
}


int CLegacyFF::ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex)
{
	int rs;
	
	SUBINDEX sub_index = {0}; 

	nsEDDEngine::FDI_ITEM_SPECIFIER      item_spec;
	nsEDDEngine::ITEM_TYPE               item_type;

	item_spec.eType			= nsEDDEngine::FDI_ITEM_ID;
	item_spec.item.id       = ItemId;
	item_spec.subindex      = 0;            /* not used just set for completeness */


	rs = GetItemType(iBlockInstance, &item_spec, &item_type);
	
	if (rs == DDS_SUCCESS)
	{
		switch (item_type) 
		{

		case ARRAY_ITYPE:
			if (MemberId == 0)
			{
				rs = DDI_TAB_BAD_SUBINDEX;
			}
			else
			{
				*iSubIndex = MemberId;
			}
			break;

		case RECORD_ITYPE:
			{
				// Convert iBlockInstance
				DDI_BLOCK_SPECIFIER block_spec = {0};
				Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

				if (MemberId != 0)
				{				
					ENV_INFO env_info(this, 0, iBlockInstance, L"");
					rs = ddi_get_subindex (&env_info, &block_spec, ItemId, MemberId, &sub_index);

					if(rs == DDS_SUCCESS)
					{
						*iSubIndex = sub_index;
					}
				}
			}
			break;

		default:
			rs = DDI_INVALID_ITEM_TYPE;
			break;

		}
	}

	return rs;
}

// CrossBlock functions copied from FDI DDS

//////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceCount
//
// Description: returns the number of blocks that are instantiated
//						 in the device.
//
//
//////////////////////////////////////////////////////////////////////////
int CLegacyFF::GetBlockInstanceCount( ITEM_ID iItemId, int* piCount)
{
	int iCnt = 0;

	if( iItemId == 0 )
	{
		*piCount = m_connection_mgr.get_active_blk_tbl_count();
	}
	else // Iterate through the entire active block table to find the count of the specified type of block with iItemId
	{
		for ( int i = 0; i < m_connection_mgr.get_active_blk_tbl_count(); i++ )
		{
			if( m_connection_mgr.ABT_DD_BLK_ID(i) == iItemId )
			{
				iCnt++;
			}
		}

		*piCount = iCnt;
	}
	return SUCCESS;
}

int CLegacyFF::CompareObjectIndex( nsEDDEngine::CCrossblockInfo* pElement1 , nsEDDEngine::CCrossblockInfo* pElement2 )
{
	
	if( pElement1->object_index < pElement2->object_index )
	{
		return -1;
	}
	else if( pElement1->object_index > pElement2->object_index )
	{
		return 1;
	}
	return 0;
}


int CLegacyFF::AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo *pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount )
{	
	CLockDeviceTypeMgr dtMgrLock;
	
	int rc = SUCCESS;

	for( int i = 0; i < iCount; i++ )
	{
		const nsEDDEngine::CFieldbusBlockInfo* pCurBlock = &pFieldbusBlockInfo[i];

		BLOCK_HANDLE iBlockHandle = m_connection_mgr.ct_block_search( pCurBlock->m_usObjectIndex );
		if( m_connection_mgr.valid_block_handle( iBlockHandle ) )
		{
			rc = BlockHandleToBlockInst( iBlockHandle, &arrBlockInstance[i]);

			if (rc != SUCCESS) {
				break;
			}
		}
		else //need to add to Active Block Table
		{
			ENV_INFO env_info(this, 0, nsEDDEngine::DeviceLevel_BI, L"");

			rc = m_connection_mgr.open_blk(	&env_info, L"DummyDeviceName",
											pCurBlock->m_CharRecord.m_ulDDItemId,
											pCurBlock->m_usObjectIndex,
											&iBlockHandle,
											&m_header);
			if (rc != SUCCESS) {
				break;
			}
		
			m_connection_mgr.SET_ABT_CHAR_RECORD( iBlockHandle, pCurBlock->m_CharRecord );
			m_connection_mgr.SET_ABT_BLOCK_TYPE(  iBlockHandle, pCurBlock->m_BlockType );
			m_connection_mgr.SET_ABT_A_OP_INDEX(  iBlockHandle, pCurBlock->m_usObjectIndex );
			m_connection_mgr.SET_ABT_TAG(  iBlockHandle, PS_Strdup(pCurBlock->m_CharRecord.m_pTag) );

			nsEDDEngine::BLOCK_INSTANCE iBlockInst = m_mapBlockInst.size()+1; // Calc the new iBlockInst number (1-n)

			m_mapBlockInst[iBlockInst] = iBlockHandle;	// Associate this iBlockInst with the Block handle
			arrBlockInstance[i] = iBlockInst;			// Store it in the output param
		}
	}

	return rc;
}


///////////////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceByObjectInstance
//
// Description: Obtains the OCCURRENCE number of the block instance at the 
//						 specified object index.
//
///////////////////////////////////////////////////////////////////////////////////
int CLegacyFF::GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence)
{

	int rc = CM_BLOCK_NOT_FOUND;
	ITEM_ID item_id = 0;
	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connection_mgr.get_active_blk_tbl_count()];
	int k = 0;

	// Loop through the active block table to find the instance in the table that matches 
	// the given Object Index and record its ITEM_ID to be used later.
	for( int i = 0; i < m_connection_mgr.get_active_blk_tbl_count(); i++ )
	{
		OBJECT_INDEX oi;
		m_connection_mgr.get_abt_op_index( i, &oi );
		if( oi == iObjectIndex )
		{
			item_id = m_connection_mgr.ABT_CR_ITEM_ID(i);
			rc = SUCCESS;
			break;
		}
	}

	if( rc == SUCCESS )
	{
		// Loop through the active block table in order to find all instances of the block instance
		// and store each instances object index
		for( int j = 0; j < m_connection_mgr.get_active_blk_tbl_count(); j++ )
		{
			if( m_connection_mgr.ABT_CR_ITEM_ID(j) == item_id )
			{
				rc = m_connection_mgr.get_abt_op_index( j, &(crossblockInfo[k].object_index) );
				rc = SUCCESS;
				k++;
			}
		}

		if( rc == SUCCESS )
		{
			//Sort the array by object index in order to get the correct occurrence number for the object index we are looking for.
			qsort(crossblockInfo, k , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);
			
			// Look through the array of blocks to find our matching object index of the block we are looking for and 
			// record the occurrence number
			for( int l = 0; l < k; l++ )
			{
				if( crossblockInfo[l].object_index == iObjectIndex )
				{
					*iOccurrence = l;
					rc = SUCCESS;
					break;
				}
			}
		}
	}

	delete crossblockInfo;
	return rc;
}

///////////////////////////////////////////////////////////////////////////////////
// Name: GetBlockInstanceByTag
//
// Description: Obtains the OCCURRENCE number of the block instance at the 
//						 specified tag.
//
///////////////////////////////////////////////////////////////////////////////////
int CLegacyFF::GetBlockInstanceByTag(ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence)
{
	
	int rc = CM_BLOCK_NOT_FOUND;
	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connection_mgr.get_active_blk_tbl_count()];
	int k = 0;
	size_t tagSize = wcslen(pTag) +1;
	const size_t  ntagSize = 256;
    char tempTag[ntagSize]{0};
	ITEM_ID item_id = 0;

	PS_Wcstombs( 0, tempTag, tagSize, pTag, _TRUNCATE );

	if( iItemId == 0 )
	{
		// Find the item id of the specified block by the tag
		for( int i = 0; i < m_connection_mgr.get_active_blk_tbl_count(); i++ )
		{
			if(m_connection_mgr.ABT_TAG(i) == (tempTag)) 
			{
				item_id = m_connection_mgr.ABT_CR_ITEM_ID(i);
				rc = SUCCESS;
				break;
			}
		}
	}
	else
	{
		item_id = iItemId;
		rc = SUCCESS;
	}

	if( rc == SUCCESS )
	{
		// get all the blocks that correspond to the item id of the specified block and store their information
		for( int j = 0; j < m_connection_mgr.get_active_blk_tbl_count(); j++ )
		{
			if( m_connection_mgr.ABT_CR_ITEM_ID(j) == item_id )
			{
				rc = m_connection_mgr.get_abt_op_index( j, &(crossblockInfo[k].object_index) );
				memcpy(crossblockInfo[k].tag, m_connection_mgr.ABT_CR_TAG(j), sizeof(crossblockInfo[k].tag));
				rc = SUCCESS;
				k++;
			}
		}

		if( rc == SUCCESS )
		{
			// sort the array of blocks by their block index
			qsort(crossblockInfo, k , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);

			// iterate through the array to find the corresponding occurrence number for the specified block
			for( int l = 0; l < k; l++ )
			{
				if( iItemId == 0 )
				{
					if(!strcmp(crossblockInfo[l].tag, tempTag)) 
					{
						*iOccurrence = l;
						rc = SUCCESS;
						break;
					}
				}
				else
				{
					if( !strcmp(crossblockInfo[l].tag, tempTag) && ( item_id == iItemId ) )
					{
						*iOccurrence = l;
						rc = SUCCESS;
						break;
					}
				}
			}
		}
	}

	delete crossblockInfo;
	return rc;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: ConvertToBlockInstance
//
// Description: Returns a Block Instance of a specified block type and its Occurrence number
//						 
//
//
////////////////////////////////////////////////////////////////////////////////////////////////////////
int CLegacyFF::ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance)
{		

	int rc = CM_BLOCK_NOT_FOUND;

	nsEDDEngine::CCrossblockInfo* crossblockInfo = new nsEDDEngine::CCrossblockInfo[m_connection_mgr.get_active_blk_tbl_count()];
	
	int j = 0;
	//iterate through the active block table to find all blocks of the specified type and store in an array
	for( int i = 0; i < m_connection_mgr.get_active_blk_tbl_count(); i++ )
	{
		if( m_connection_mgr.ABT_CR_ITEM_ID(i) == ulItemId )
		{
			m_connection_mgr.get_abt_op_index(i, &(crossblockInfo[j].object_index) ); 
			crossblockInfo[j].block_handle = i;
			j++;
		}
	}
	
	// Sort the block info array by the block object index
	qsort(crossblockInfo, j , sizeof(nsEDDEngine::CCrossblockInfo), (int(*) (const void*, const void*))CompareObjectIndex);
	
	if( (iOccurrence >= 0 ) && (iOccurrence < j ) )
	{
		rc = BlockHandleToBlockInst(crossblockInfo[iOccurrence].block_handle, piBlockInstance);	// Convert from handle to instance
		//rc = SUCCESS;
	}

	delete crossblockInfo;
	return rc;
}


int  CLegacyFF::GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, ITEM_ID* pWAO_Item, ITEM_ID* pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList)
{
	// Get the legacy flat_device_dir 
	int r_code = -1;
	FLAT_DEVICE_DIR *pDdiFlatDeviceDir = NULL; 
	ENV_INFO env_info(this, pValueSpec, iBlockInstance, L"");

	//Initialize output Vars to empty
	if ( pWAO_Item != nullptr)
	{
		*pWAO_Item = 0;
	}

	if ( pUNIT_Item != nullptr)
	{
		*pUNIT_Item = 0;
	}

	if((pUpdateList != nullptr) && (pUpdateList->list != nullptr))
	{
		pUpdateList->~OP_REF_LIST();
	}

	if((pDominantList != nullptr) && (pDominantList->list != nullptr))
	{
		pDominantList->~OP_REF_LIST();
	}

	// invalid instances for this call.  Encountered primarily in regression testing
	if (iBlockInstance == nsEDDEngine::DeviceLevel_BI)
	{
		return 0;
	}

	// Get the data that was requested

	r_code = m_connection_mgr.get_adt_dd_dev_tbls( m_device_handle, (void**)&pDdiFlatDeviceDir );

	if ( r_code == CM_SUCCESS )
	{
		if (!(pDdiFlatDeviceDir->attr_avail & STRING_TBL_MASK))
		{
			DDI_DEVICE_DIR_REQUEST ddi_dir_req = {0};

			ddi_dir_req.type = DD_DT_HANDLE;
			ddi_dir_req.mask = STRING_TBL_MASK;
			r_code = m_connection_mgr.get_adt_adtt_offset( m_device_handle, &ddi_dir_req.spec.device_type_handle ); 

			if(r_code == DDS_SUCCESS)
			{
				r_code = ddi_device_dir_request( &env_info, &ddi_dir_req, pDdiFlatDeviceDir );
			}
		}
	}
	else 
	{
		EddEngineLog( &env_info, __FILE__, __LINE__, BssError, L"LegacyFF", L"Device Tables not set... returned with error code %d", r_code);
	}

	if( r_code == CM_SUCCESS )
	{
		// Init a block_spec
		DDI_BLOCK_SPECIFIER block_spec = {0};
		Init_DDI_BLOCK_SPECIFIER(&block_spec, iBlockInstance);

		// Init a param_spec
		DDI_PARAM_SPECIFIER param_spec = {0};
		r_code = Init_DDI_PARAM_SPECIFIER(&param_spec, pParamSpec);

		if (r_code != DDS_SUCCESS)
		{
			return r_code;
		}


		if ( pWAO_Item != nullptr)
		{
			r_code = ddi_get_wao(&env_info, &block_spec, &param_spec, pWAO_Item);

			if (r_code == DDI_TAB_NO_WAO)	// This is an okay return. It just means that we don't have any WAOs
			{								// In this case, *pWAO_Item will remain as 0.
				r_code = DDS_SUCCESS;
			}
		} // pWAO_Item

		if ( pUNIT_Item != nullptr)
		{
			r_code = ddi_get_unit(&env_info, &block_spec, &param_spec, pUNIT_Item);

			if (r_code == DDI_TAB_NO_UNIT)	// This is an okay return. It just means that we don't have any UNITs
			{								// In this case, *pUNIT_Item will remain as 0.
				r_code = DDS_SUCCESS;
			}
		} // pUNIT_Item

		if ( pUpdateList != nullptr)
		{
			OP_DESC_LIST op_desc_list = {0};
			op_desc_list.count = 0;

			r_code = ddi_get_update_items(&env_info, &block_spec, &param_spec, &op_desc_list);

			if(r_code == DDS_SUCCESS)
			{
				pUpdateList->list = (nsEDDEngine::OP_REF *) malloc((size_t) (op_desc_list.count * sizeof(nsEDDEngine::OP_REF)));

				pUpdateList->limit = (unsigned short)op_desc_list.count;
				pUpdateList->count = (unsigned short)op_desc_list.count;

				for (int i = 0; i < op_desc_list.count ; i++)
				{
					nsEDDEngine::OP_REF *pOpRef = &pUpdateList->list[i];

					// Set the dependent item id to UPDATE item
					pOpRef->op_ref_type =  nsEDDEngine::STANDARD_TYPE;
					pOpRef->block_instance = iBlockInstance;
					pOpRef->op_info.id = op_desc_list.list[i].op_ref.id;
					pOpRef->op_info.member = op_desc_list.list[i].op_ref.subindex;
					pOpRef->op_info.type = Set_ITEM_TYPE(op_desc_list.list[i].op_ref.type);
					pOpRef->op_info_list.count = 0;
					pOpRef->op_info_list.list = nullptr;

				}

			}
			else if (r_code == DDI_TAB_NO_UPDATE) // This is an okay return. It just means that we don't have any UPDATES
			{									  // In this case, *pUpdateList will remain as 0.
				r_code = DDS_SUCCESS;
			}

			free(op_desc_list.list);
			op_desc_list.list = nullptr;
		} // pUpdateList

		if (pDominantList != nullptr)
		{
			// Find the block table index
			int iBlockTableOffset =  m_connection_mgr.ABT_DD_BLK_TBL_OFFSET(env_info.block_handle);

			// this is a common starting point for table references.  Use to keep code readable
			FLAT_BLOCK_DIR* pFlatBlockDir = &pDdiFlatDeviceDir->blk_tbl.list[iBlockTableOffset].flat_block_dir;


			unsigned long long key = pParamSpec->id;

			if(pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI)
			{
				key |= ((unsigned long long)pParamSpec->subindex) << 32;
			}

			DOMINANT_TBL::iterator it;

			it = pFlatBlockDir->dominant_tbl->find(key);

			if(it != pFlatBlockDir->dominant_tbl->end())
			{
				OP_REF_LIST *opList = it->second;

				pDominantList->count = opList->count;
				pDominantList->limit = opList->limit;
				pDominantList->list = (nsEDDEngine::OP_REF *) malloc((size_t) (opList->count * sizeof(nsEDDEngine::OP_REF)));
				
				
				for(int i = 0; i < pDominantList->count ; i++)
				{
					Fill_OP_REF(&pDominantList->list[i], &opList->list[i]);
				}
			}
		} // pDominantList

	}

	return r_code;
}

CEDDEngineImpl* LegacyFFFactory::CreateLegacyFFDDS( const wchar_t *sEDDBinaryFilename, 
	const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode)
{
	CEDDEngineImpl *pImpl = (CEDDEngineImpl*)new CLegacyFF(sEDDBinaryFilename, pConfigXML, pIEDDEngineLogger,
        pErrorCode);

	if (*pErrorCode != nsEDDEngine::EDDE_SUCCESS)	// If there is an error in the constructor, delete and return null
	{
		delete pImpl;
		pImpl = nullptr;
	}

	return pImpl;
}

