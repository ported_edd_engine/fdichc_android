#include "stdafx.h"
#include "LegacyFF.h"




// Fill_STRING() - Fills in FDI STRING from DDS STRING
void CLegacyFF::Fill_STRING(nsEDDEngine::STRING *pFdiString, const STRING *pDdiString)
{
	if (pDdiString->flags == FREE_STRING)
	{
		*pFdiString = pDdiString->str;
	}
	else
	{
		pFdiString->assign(pDdiString->str,false);
	}
}

// Fill_BINARY() - Fills in FDI BINARY from DDS BINARY
void CLegacyFF::Fill_BINARY(nsEDDEngine::BINARY *pFdiBinary, const BINARY *pDdiBinary)
{
	pFdiBinary->assign(pDdiBinary->ptr, pDdiBinary->len);
}

// Fill_EDD_TYPE_SIZE() - Fills in an FDI TYPE_SIZE from DDS TYPE_SIZE
void CLegacyFF::Fill_EDD_TYPE_SIZE (nsEDDEngine::TYPE_SIZE *pTypeSize, TYPE_SIZE type_size)
{
	pTypeSize->size = type_size.size;
	switch (type_size.type)
	{
	case DDSUNUSED:									//0
		pTypeSize->type = nsEDDEngine::VT_DDS_TYPE_UNUSED;
		break;
	case INTEGER:									//2				
		pTypeSize->type = nsEDDEngine::VT_INTEGER;
		break;
	case UNSIGNED:									//3
		pTypeSize->type = nsEDDEngine::VT_UNSIGNED;
		break;
    case FLOATG_PT:										//4
		pTypeSize->type = nsEDDEngine::VT_FLOAT;
		break;
    case DOUBLEG_PT:									//5
		pTypeSize->type = nsEDDEngine::VT_DOUBLE;
		break;
	case ENUMERATED:								//6
		pTypeSize->type = nsEDDEngine::VT_ENUMERATED;
		break;
	case BIT_ENUMERATED:								//7
		pTypeSize->type = nsEDDEngine::VT_BIT_ENUMERATED;
		break;
	case INDEX:											//8
		pTypeSize->type = nsEDDEngine::VT_INDEX;
		break;
	case ASCII:											//9
		pTypeSize->type = nsEDDEngine::VT_ASCII;
		break;
	case PACKED_ASCII:									//10
		pTypeSize->type = nsEDDEngine::VT_PACKED_ASCII;
		break;
	case PASSWORD:										//11
		pTypeSize->type = nsEDDEngine::VT_PASSWORD;
		break;
	case BITSTRING:										//12
		pTypeSize->type = nsEDDEngine::VT_BITSTRING;
		break;
	case HART_DATE_FORMAT:									//13
		pTypeSize->type = nsEDDEngine::VT_EDD_DATE;
		break;
	case TIME:												//14
		pTypeSize->type = nsEDDEngine::VT_TIME;
		break;
	case DATE_AND_TIME:										//15
		pTypeSize->type = nsEDDEngine::VT_DATE_AND_TIME;
		break;
	case DURATION:											//16
		pTypeSize->type = nsEDDEngine::VT_DURATION;
		break;
	case EUC:												//17
		pTypeSize->type = nsEDDEngine::VT_EUC;
		break;
	case OCTETSTRING:										//18
		pTypeSize->type = nsEDDEngine::VT_OCTETSTRING;
		break;
	case VISIBLESTRING:										//19
		pTypeSize->type = nsEDDEngine::VT_VISIBLESTRING;
		break;
	case TIME_VALUE:										//20
		pTypeSize->type = nsEDDEngine::VT_TIME_VALUE;
		break;
	case BOOLEAN_T:
		pTypeSize->type = nsEDDEngine::VT_BOOLEAN;
		break;
	default:
		EddEngineLog( this,  __FILE__, __LINE__, BssError, L"LegacyFF",
			L"CLegacyFF::Fill_EDD_TYPE_SIZE() cannot convert Type: %d", type_size.type );
	}
}


// Fill_RESPONSE_CODES() - Fills in an FDI RESPONSE_CODE_LIST from DDS RESPONSE_CODE_LIST
void CLegacyFF::Fill_RESPONSE_CODES(nsEDDEngine::RESPONSE_CODE_LIST* pFdiRespCode, RESPONSE_CODE_LIST* pDdiRespCodeList)
{
	pFdiRespCode->list = nullptr;
	if(pDdiRespCodeList->count != 0)
	{
		pFdiRespCode->count = pDdiRespCodeList->count;
		pFdiRespCode->limit = pDdiRespCodeList->limit;
		pFdiRespCode->list = new nsEDDEngine::RESPONSE_CODE[pDdiRespCodeList->count];
		//pFdiRespCode->list = (nsEDDEngine::RESPONSE_CODE*)calloc(1, sizeof(nsEDDEngine::RESPONSE_CODE)*pDdiRespCodeList->count);
		for(int i = 0; i < pDdiRespCodeList->count; i++)
		{
			if(pDdiRespCodeList->list[i].evaled & RS_DESC_EVALED)
			{
				pFdiRespCode->list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_DESC_EVALED; 
				Fill_STRING(&pFdiRespCode->list[i].desc, &pDdiRespCodeList->list[i].desc);
			}

			if(pDdiRespCodeList->list[i].evaled & RS_HELP_EVALED)
			{
				pFdiRespCode->list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_HELP_EVALED; 
				Fill_STRING(&pFdiRespCode->list[i].help, &pDdiRespCodeList->list[i].help);
			}

			if(pDdiRespCodeList->list[i].evaled & RS_TYPE_EVALED)
			{
				pFdiRespCode->list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_TYPE_EVALED; 
				switch(pDdiRespCodeList->list[i].type)
				{
				case SUCCESS_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_SUCCESS_RSPCODE;
					break;
				case MISC_WARNING_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_MISC_WARNING_RSPCODE;
					break;
				case DATA_ENTRY_WARNING_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_DATA_ENTRY_WARNING_RSPCODE;
					break;
				case DATA_ENTRY_ERROR_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_DATA_ENTRY_ERROR_RSPCODE;
					break;
				case MODE_ERROR_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_MODE_ERROR_RSPCODE;
					break;
				case PROCESS_ERROR_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_PROCESS_ERROR_RSPCODE;
					break;
				case MISC_ERROR_RSPCODE:
					pFdiRespCode->list[i].type = nsEDDEngine::FDI_MISC_ERROR_RSPCODE;
					break;
				default:
					EddEngineLog(this, __FILE__, __LINE__, BssError, L"LegacyFF",
						L"CLegacyFF::Fill_RESPONSE_CODES() cannot convert Responsecode Type: %d", pDdiRespCodeList->list[i].type );
				}
			}

			if(pDdiRespCodeList->list[i].evaled & RS_VAL_EVALED)
			{
				pFdiRespCode->list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_VAL_EVALED; 
				pFdiRespCode->list[i].val = pDdiRespCodeList->list[i].val;
			}	
		}

	}	
}

// Fill_OP_REF() - Fills in an FDI OP_REF from DDS OP_REF
void CLegacyFF::Fill_OP_REF(nsEDDEngine::OP_REF* pFdiOpRef, OP_REF* pDdiOpRef)
{
	pFdiOpRef->block_instance		= 0;
	pFdiOpRef->op_ref_type			= nsEDDEngine::OpRefType::STANDARD_TYPE;
	pFdiOpRef->op_info.id			= pDdiOpRef->id;
	pFdiOpRef->op_info.member		= pDdiOpRef->subindex;
	pFdiOpRef->op_info.type			= Set_ITEM_TYPE(pDdiOpRef->type);			
	pFdiOpRef->op_info_list.count	= 0;
	pFdiOpRef->op_info_list.list	= nullptr;
}

// Fill_OP_REF_TRAIL() - Fills in an FDI OP_REF_TRAIL from DDS OP_REF_TRAIL
void CLegacyFF::Fill_OP_REF_TRAIL(nsEDDEngine::OP_REF_TRAIL* pFdiOpRefTrail, OP_REF_TRAIL* pDdiOpRefTrail)
{
	pFdiOpRefTrail->Init();

	
	ConvertToBlockInstance( pDdiOpRefTrail->op_block_id, pDdiOpRefTrail->op_block_instance, &pFdiOpRefTrail->block_instance );
	
	pFdiOpRefTrail->desc_id				= pDdiOpRefTrail->desc_id;
	pFdiOpRefTrail->desc_type			= Set_ITEM_TYPE(pDdiOpRefTrail->desc_type);
	pFdiOpRefTrail->desc_bit_mask		= pDdiOpRefTrail->bit_mask; 

    if( pDdiOpRefTrail->container_id == 0 )
	{
		pFdiOpRefTrail->op_ref_type		= nsEDDEngine::OpRefType::STANDARD_TYPE;
		pFdiOpRefTrail->op_info.id		= pDdiOpRefTrail->op_id;
		pFdiOpRefTrail->op_info.member	= pDdiOpRefTrail->op_subindex;
		
		pFdiOpRefTrail->op_info.type	= Set_ITEM_TYPE(pDdiOpRefTrail->op_type);
	}
	else
	{
		pFdiOpRefTrail->op_ref_type			= nsEDDEngine::OpRefType::COMPLEX_TYPE;
		pFdiOpRefTrail->op_info_list.count	= 2; // one for op_* info and one for container_* info
        pFdiOpRefTrail->op_info_list.list	= new nsEDDEngine::OP_REF_INFO[pFdiOpRefTrail->op_info_list.count];

		pFdiOpRefTrail->op_info_list.list[0].id		= pDdiOpRefTrail->container_id;
		pFdiOpRefTrail->op_info_list.list[0].member	= pDdiOpRefTrail->container_index;
		pFdiOpRefTrail->op_info_list.list[0].type	= Set_ITEM_TYPE(pDdiOpRefTrail->container_type);

		pFdiOpRefTrail->op_info_list.list[1].id		= pDdiOpRefTrail->op_id;
		pFdiOpRefTrail->op_info_list.list[1].member	= pDdiOpRefTrail->op_subindex;
		pFdiOpRefTrail->op_info_list.list[1].type	= Set_ITEM_TYPE(pDdiOpRefTrail->op_type);
	}
	
	if(pDdiOpRefTrail->trail_count != 0)
	{
		pFdiOpRefTrail->trail_count	= pDdiOpRefTrail->trail_count;
		pFdiOpRefTrail->trail_limit	= pDdiOpRefTrail->trail_count;

		pFdiOpRefTrail->trail	= new nsEDDEngine::RESOLVE_INFO[pDdiOpRefTrail->trail_count];

		for(int i = 0; i < pDdiOpRefTrail->trail_count; i++)
		{
			pFdiOpRefTrail->trail[i].element	= pDdiOpRefTrail->trail[i].element;
			pFdiOpRefTrail->trail[i].id			= pDdiOpRefTrail->trail[i].id;
			pFdiOpRefTrail->trail[i].type		= Set_ITEM_TYPE(pDdiOpRefTrail->trail[i].type);
		}
	}

}


//Fill_EXPR - Fills in an FDI EXPR from DDS EXPR.
void CLegacyFF::Fill_EXPR(nsEDDEngine::EXPR* pFdiExpr, EXPR* pDdiExpr)
{
	pFdiExpr->size			= pDdiExpr->size;
	
	switch(pDdiExpr->type)
	{
	case INTEGER:
		pFdiExpr->val.i = pDdiExpr->val.i;
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_INTEGER;
		break;
	case UNSIGNED:
		pFdiExpr->val.u = pDdiExpr->val.u;
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED;
		break;
    case FLOATG_PT:
		pFdiExpr->val.f = pDdiExpr->val.f;
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_FLOAT;
		break;
    case DOUBLEG_PT:
		pFdiExpr->val.d = pDdiExpr->val.d;
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_DOUBLE;
		break;
	case ASCII:
		Fill_STRING(&pFdiExpr->val.s, &pDdiExpr->val.s);
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_STRING;
		break;
	case BITSTRING:
		Fill_BINARY(&pFdiExpr->val.b, &pDdiExpr->val.b);
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_BINARY;
		break;
	default:
		pFdiExpr->eType = nsEDDEngine::EXPR::EXPR_TYPE_NONE;
		break;
	}

}



//Fill_DATA_ITEM_LIST(): Fills in FDI DATA_ITEM_LIST from DDS DATA_ITEM_LIST
void CLegacyFF::Fill_DATA_ITEM_LIST(nsEDDEngine::DATA_ITEM_LIST* pFdiDataList, DATA_ITEM_LIST* pDdiDataList)
{
	if(pDdiDataList->count != 0)
	{
		pFdiDataList->count = pDdiDataList->count;
		pFdiDataList->limit = pDdiDataList->count;

		pFdiDataList->list = new nsEDDEngine::DATA_ITEM[pDdiDataList->count];

		for(int i = 0; i < pDdiDataList->count; i++)
		{

			switch(pDdiDataList->list[i].type)
			{
			case DATA_CONSTANT:
				pFdiDataList->list[i].type	= nsEDDEngine::FDI_DATA_CONSTANT;
				pFdiDataList->list[i].data.iconst = pDdiDataList->list[i].data.iconst; 
				break;

			case DATA_FLOATING:
				pFdiDataList->list[i].type	= nsEDDEngine::FDI_DATA_FLOATING;
				pFdiDataList->list[i].data.fconst = pDdiDataList->list[i].data.fconst; 
				break;

			case DATA_REFERENCE:
				pFdiDataList->list[i].type	= nsEDDEngine::FDI_DATA_REFERENCE;
				Fill_OP_REF_TRAIL(&pFdiDataList->list[i].data.ref, &pDdiDataList->list[i].data.ref);
				break;

			default:
				EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
					L"CLegacyFF::Fill_DATA_ITEM_LIST() cannot convert DATA_ type: %d", pDdiDataList->list[i].type );
			}

			pFdiDataList->list[i].flags				= pDdiDataList->list[i].flags;
			pFdiDataList->list[i].width				= pDdiDataList->list[i].width;

			// Below element is not available in DDS DATA_ITEM_LIST
			//pFdiDataList->list[i].data_item_mask				= pDdiDataList->list[i];
						
		}
	}
}


//Fill_RANGE_DATA_LIST(): Fills in FDI RANGE_DATA_LIST from DDS RANGE_DATA_LIST
void CLegacyFF::Fill_RANGE_DATA_LIST(nsEDDEngine::RANGE_DATA_LIST* pFdiRangeDataList, RANGE_DATA_LIST* pDdiRangeDataList)
{
	if(pDdiRangeDataList->count != 0)
	{
		pFdiRangeDataList->count = pDdiRangeDataList->count;
		pFdiRangeDataList->limit = pDdiRangeDataList->limit;

		//pFdiRangeDataList->list = (nsEDDEngine::EXPR*)calloc(1,sizeof(nsEDDEngine::EXPR)*pDdiRangeDataList->count);
		pFdiRangeDataList->list = new nsEDDEngine::EXPR[pDdiRangeDataList->count];
		
		for(int i = 0; i < pDdiRangeDataList->count; i++)
		{
			Fill_EXPR(&pFdiRangeDataList->list[i], &pDdiRangeDataList->list[i]);
		}

	}

}


//Fill_ACTION_LIST(): Fills in FDI ACTION_LIST from DDS ITEM_ID_LIST
void CLegacyFF::Fill_ACTION_LIST(nsEDDEngine::ACTION_LIST* pFdiActionList, ITEM_ID_LIST* pDdiItemIdList)
{
	if(pDdiItemIdList != nullptr)
	{
		if(pDdiItemIdList->count != 0)
		{
			pFdiActionList->count = pDdiItemIdList->count;
			pFdiActionList->limit = pDdiItemIdList->count;
			//pFdiActionList->list =  (nsEDDEngine::ACTION*)calloc(1,sizeof(nsEDDEngine::ACTION)*pDdiItemIdList->count);
			pFdiActionList->list =  new nsEDDEngine::ACTION[pDdiItemIdList->count];

			for(int i = 0; i < pDdiItemIdList->count; i++)
			{
				pFdiActionList->list[i].eType = nsEDDEngine::ACTION::ACTION_TYPE_REFERENCE;
				pFdiActionList->list[i].action.meth_ref = pDdiItemIdList->list[i];
			}
		}
	}
}


// Fill_ENUM_VALUE_LIST(): Fills in FDI ENUM_VALUE_LIST from DDS ENUM_VALUE_LIST
void CLegacyFF::Fill_ENUM_VALUE_LIST(nsEDDEngine::ENUM_VALUE_LIST* pFdiEnumValuList, ENUM_VALUE_LIST* pDdiEnumValuList)
{
	pFdiEnumValuList->list = nullptr;
	if(pDdiEnumValuList != NULL)
	{
		if(pDdiEnumValuList->count != 0)
		{
			pFdiEnumValuList->count = pDdiEnumValuList->count;
			pFdiEnumValuList->limit = pDdiEnumValuList->count;

			//pFdiEnumValuList->list = (nsEDDEngine::ENUM_VALUE*)calloc(1, sizeof(nsEDDEngine::ENUM_VALUE)*pDdiEnumValuList->count);
			pFdiEnumValuList->list = new nsEDDEngine::ENUM_VALUE[pDdiEnumValuList->count];

			for(int i = 0; i < pDdiEnumValuList->count; i++)
			{
				if(pDdiEnumValuList->list[i].evaled & ENUM_ACTIONS_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_ACTIONS_EVALED; 
					pFdiEnumValuList->list[i].actions = pDdiEnumValuList->list[i].actions;
				}
				if(pDdiEnumValuList->list[i].evaled & ENUM_CLASS_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_CLASS_EVALED; 
					pFdiEnumValuList->list[i].func_class = CLegacyFF::SetClassType(pDdiEnumValuList->list[i].func_class);
				}
				if(pDdiEnumValuList->list[i].evaled & ENUM_DESC_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_DESC_EVALED; 
					Fill_STRING(&pFdiEnumValuList->list[i].desc, &pDdiEnumValuList->list[i].desc);
				}
				if(pDdiEnumValuList->list[i].evaled & ENUM_HELP_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_HELP_EVALED; 
					Fill_STRING(&pFdiEnumValuList->list[i].help, &pDdiEnumValuList->list[i].help);
				}
				if(pDdiEnumValuList->list[i].evaled & ENUM_STATUS_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_STATUS_EVALED; 
					Fill_ENUM_STATUS_CLASS(&pFdiEnumValuList->list[i].status, &pDdiEnumValuList->list[i].status);
				}
				if(pDdiEnumValuList->list[i].evaled & ENUM_VAL_EVALED)
				{
					pFdiEnumValuList->list[i].evaled |= nsEDDEngine::ENUM_VALUE::FDI_ENUM_VAL_EVALED;

					pFdiEnumValuList->list[i].val.val.u = pDdiEnumValuList->list[i].val;
					pFdiEnumValuList->list[i].val.eType = nsEDDEngine::EXPR::EXPR_TYPE_UNSIGNED;
					pFdiEnumValuList->list[i].val.size = 4;
				}
			}
		}
	}

}

// Add an enum status class entry to the already allocated buffer
// Verify that it fits first.
static void AddEnumStatus(nsEDDEngine::STATUS_CLASS_LIST* pFdiBitEnumStatus, nsEDDEngine::BaseClass baseClass,
                         nsEDDEngine::ModeAndReliability modeAndReliability, unsigned short whichOutput)
{
    if (pFdiBitEnumStatus->count >= pFdiBitEnumStatus->limit)
    {
        return;
    }
    pFdiBitEnumStatus->list[pFdiBitEnumStatus->count].base_class = baseClass;
    pFdiBitEnumStatus->list[pFdiBitEnumStatus->count].output_class.mode_and_reliability = modeAndReliability;
    pFdiBitEnumStatus->list[pFdiBitEnumStatus->count].output_class.which_output = whichOutput;
    pFdiBitEnumStatus->count++; // Advance count indicating that this instance is complete
}

//Fill_ENUM_STATUS_CLASS(): Fills in FDI BIT_ENUM_STATUS from DDS BIT_ENUM_STATUS
void CLegacyFF::Fill_ENUM_STATUS_CLASS(nsEDDEngine::STATUS_CLASS_LIST* pFdiBitEnumStatus, BIT_ENUM_STATUS* pDdiBitEnumStatus)
{
    // Create a maximum size list to fill in,  Reduce the size after all data has been set
    // Maximum size is pretty small so incremental reallocs would be a waste of processor power.
	pFdiBitEnumStatus->count = 0;
	pFdiBitEnumStatus->limit = nsEDDEngine::FDI_BASE_CLASS_ITEM_MAX;
    pFdiBitEnumStatus->list = new nsEDDEngine::STATUS_CLASS_ELEM[nsEDDEngine::FDI_BASE_CLASS_ITEM_MAX];

    // Add one enum class entry for each bit set in the status_class.
	if(pDdiBitEnumStatus->status_class & HARDWARE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_HARDWARE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & SOFTWARE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_SOFTWARE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & PROCESS_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_PROCESS_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & MODE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_MODE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & DATA_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_DATA_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & MISC_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_MISC_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & EVENT_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_EVENT_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & STATE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_STATE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & SELF_CORRECTING_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_SELF_CORRECTING_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & CORRECTABLE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_CORRECTABLE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & UNCORRECTABLE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_UNCORRECTABLE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & SUMMARY_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_SUMMARY_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & DETAIL_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_DETAIL_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & MORE_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_MORE_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & COMM_ERROR_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_COMM_ERROR_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & IGNORE_IN_TEMPORARY_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_IGNORE_IN_HANDHELD, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
	if(pDdiBitEnumStatus->status_class & BAD_OUTPUT_STATUS)
	{
		AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_BAD_OUTPUT_STATUS, nsEDDEngine::FDI_MR_UNUSED, 0);
	}
    //
    // The following items found in HART are not in FF: IGNORE_IN_HOST_STATUS, ERROR_STATUS, WARNING_STATUS, INFO_STATUS
    //

    // oclass.list items are added to the enum class in the same manner only these contain
    // mode and reliability and which fields.
	for(int i = 0; i < pDdiBitEnumStatus->oclasses.count; i++)
	{
        // Each of these should have a valid mode and reliability field.
        // Determine what it should be before continuing.
        nsEDDEngine::ModeAndReliability mode_and_reliability = nsEDDEngine::FDI_MR_MANUAL_GOOD;
        switch (pDdiBitEnumStatus->oclasses.list[i].oclass)
        {
        case 0:
            mode_and_reliability = nsEDDEngine::FDI_MR_AUTO_GOOD;
            break;
        case OC_MANUAL:
            mode_and_reliability = nsEDDEngine::FDI_MR_MANUAL_GOOD;
            break;
        case OC_BAD:
            mode_and_reliability = nsEDDEngine::FDI_MR_AUTO_BAD;
            break;
        case OC_MANUAL | OC_BAD:
            mode_and_reliability = nsEDDEngine::FDI_MR_MANUAL_BAD;
            break;
		default:
			EddEngineLog(this, __FILE__, __LINE__, BssError, L"LegacyFF",
				L"CLegacyFF::Fill_ENUM_STATUS_CLASS() cannot convert Output class: %d", pDdiBitEnumStatus->oclasses.list[i].oclass );
        }

        // Now based on the kind add an enum class entry with the proper value.
        switch(pDdiBitEnumStatus->oclasses.list[i].kind)
		{
		case OC_DV:
		    AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_DV_STATUS,
                        mode_and_reliability,
                        pDdiBitEnumStatus->oclasses.list[i].which);
			break;
		case OC_TV:
		    AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_TV_STATUS,
                        mode_and_reliability,
                        pDdiBitEnumStatus->oclasses.list[i].which);
			break;
		case OC_AO:
		    AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_AO_STATUS,
                        mode_and_reliability,
                        pDdiBitEnumStatus->oclasses.list[i].which);
			break;
		case OC_ALL:
		    AddEnumStatus(pFdiBitEnumStatus, nsEDDEngine::FDI_ALL_STATUS,
                        mode_and_reliability,
                        pDdiBitEnumStatus->oclasses.list[i].which);
			break;
		default:
			EddEngineLog( this, __FILE__, __LINE__, BssError, L"LegacyFF",
				L"CLegacyFF::Fill_ENUM_STATUS_CLASS() cannot convert Output kind: %d", pDdiBitEnumStatus->oclasses.list[i].kind );
		}

	}

    // All finished adding stuff now we need to reduce the size of the data to what
    // is actually used.
   
    nsEDDEngine::STATUS_CLASS_ELEM  *temp_list = pFdiBitEnumStatus->list;
	pFdiBitEnumStatus->list = new nsEDDEngine::STATUS_CLASS_ELEM[pFdiBitEnumStatus->count];
    for (int i = 0 ; i < pFdiBitEnumStatus->count ; i++)
    {
        pFdiBitEnumStatus->list[i] = temp_list[i];
    }
    delete [] temp_list;
}


void  CLegacyFF::Fill_MEMBER_LIST(nsEDDEngine::MEMBER_LIST* pFdiMemberList, MEMBER_LIST* pDdiMemberList)
{
	if(pDdiMemberList->count != 0)
	{
		pFdiMemberList->count = pDdiMemberList->count;
		pFdiMemberList->limit = pDdiMemberList->count;

		pFdiMemberList->list = new nsEDDEngine::MEMBER[pDdiMemberList->count];

		for(int i = 0; i < pDdiMemberList->count; i++)
		{
			if(pDdiMemberList->list[i].evaled & MEM_DESC_EVALED)
			{
				pFdiMemberList->list[i].evaled |= nsEDDEngine::MEMBER::FDI_MEM_DESC_EVALED; 
				Fill_STRING(&pFdiMemberList->list[i].desc, &pDdiMemberList->list[i].desc);
			}

			if(pDdiMemberList->list[i].evaled & MEM_HELP_EVALED)
			{
				pFdiMemberList->list[i].evaled |= nsEDDEngine::MEMBER::FDI_MEM_HELP_EVALED; 
				Fill_STRING(&pFdiMemberList->list[i].help, &pDdiMemberList->list[i].help);
			}

			if(pDdiMemberList->list[i].evaled & MEM_NAME_EVALED)
			{
				pFdiMemberList->list[i].evaled |= nsEDDEngine::MEMBER::FDI_MEM_NAME_EVALED; 
				pFdiMemberList->list[i].name = pDdiMemberList->list[i].name;
			}

			if(pDdiMemberList->list[i].evaled & MEM_REF_EVALED)
			{
				pFdiMemberList->list[i].evaled |= nsEDDEngine::MEMBER::FDI_MEM_REF_EVALED;
				pFdiMemberList->list[i].ref.id		=  pDdiMemberList->list[i].ref.id;
				pFdiMemberList->list[i].ref.type  = Set_ITEM_TYPE(pDdiMemberList->list[i].ref.type);
			}
		}
	}
}


void  CLegacyFF::Fill_OP_MEMBER_LIST(nsEDDEngine::OP_MEMBER_LIST* pFdiOpMemberList, OP_MEMBER_LIST* pDdiOpMemberList)
{
	if(pDdiOpMemberList->count != 0)
	{
		pFdiOpMemberList->count = pDdiOpMemberList->count;
		pFdiOpMemberList->limit = pDdiOpMemberList->count;

		pFdiOpMemberList->list = new nsEDDEngine::OP_MEMBER[pDdiOpMemberList->count];

		for(int i = 0; i < pDdiOpMemberList->count; i++)
		{
			if(pDdiOpMemberList->list[i].evaled & MEM_DESC_EVALED)
			{
				pFdiOpMemberList->list[i].evaled |= nsEDDEngine::OP_MEMBER::FDI_OP_MEM_DESC_EVALED; 
				Fill_STRING(&pFdiOpMemberList->list[i].desc, &pDdiOpMemberList->list[i].desc);
			}

			if(pDdiOpMemberList->list[i].evaled & MEM_HELP_EVALED)
			{
				pFdiOpMemberList->list[i].evaled |= nsEDDEngine::OP_MEMBER::FDI_OP_MEM_HELP_EVALED; 
				Fill_STRING(&pFdiOpMemberList->list[i].help, &pDdiOpMemberList->list[i].help);
			}

			if(pDdiOpMemberList->list[i].evaled & MEM_NAME_EVALED)
			{
				pFdiOpMemberList->list[i].evaled |= nsEDDEngine::OP_MEMBER::FDI_OP_MEM_NAME_EVALED; 
				pFdiOpMemberList->list[i].name = pDdiOpMemberList->list[i].name;
			}

			if(pDdiOpMemberList->list[i].evaled & MEM_REF_EVALED)
			{
				pFdiOpMemberList->list[i].evaled |= nsEDDEngine::OP_MEMBER::FDI_OP_MEM_REF_EVALED; 
				Fill_OP_REF_TRAIL(&pFdiOpMemberList->list[i].ref, &pDdiOpMemberList->list[i].ref);
			}
		}
	}
}

void CLegacyFF::Fill_OP_REF_TRAIL_LIST(nsEDDEngine::OP_REF_TRAIL_LIST* pFdiOpRefTrailList, OP_REF_TRAIL_LIST* pDdiOpRefTrailList)
{

	if(pDdiOpRefTrailList->count != 0)
	{
		pFdiOpRefTrailList->count	= pDdiOpRefTrailList->count;
		pFdiOpRefTrailList->limit	= pDdiOpRefTrailList->count;
		pFdiOpRefTrailList->list	= new nsEDDEngine::OP_REF_TRAIL[pDdiOpRefTrailList->count];

		for(int i = 0; i < pDdiOpRefTrailList->count; i++)
		{
			Fill_OP_REF_TRAIL(&pFdiOpRefTrailList->list[i], &pDdiOpRefTrailList->list[i]);
		}
	}
}

void CLegacyFF::Fill_ITEM_ID_LIST( nsEDDEngine::ITEM_ID_LIST *pFdiItemIdList, ITEM_ID_LIST *pDdiItemIdList)
{
	if(pDdiItemIdList->count != 0)
	{
		pFdiItemIdList->count = pDdiItemIdList->count;
		pFdiItemIdList->limit = pDdiItemIdList->count;

		pFdiItemIdList->list  = new ITEM_ID[pDdiItemIdList->count];

		for(int i = 0; i < pDdiItemIdList->count; i++)
		{
			pFdiItemIdList->list[i] = pDdiItemIdList->list[i];
		}
	}
}

void CLegacyFF::Fill_VECTOR_LIST( nsEDDEngine::VECTOR_LIST *pFdiVectorList, VECTOR_LIST *pDdiVectorList )
{
	if(pDdiVectorList->count != 0)
	{
		pFdiVectorList->count	= pDdiVectorList->count;
		pFdiVectorList->limit	= pDdiVectorList->count;
		pFdiVectorList->vectors	= new nsEDDEngine::VECTOR[pDdiVectorList->count];
		
		for(int i = 0; i < pDdiVectorList->count; i++)
		{
			// Count needs to be adjusted as description string is encoded as vector string.
			unsigned short count = pDdiVectorList->vectors[i].count - 1;
			pFdiVectorList->vectors[i].count = count;
			pFdiVectorList->vectors[i].limit = count;
			pFdiVectorList->vectors[i].list = new nsEDDEngine::VECTOR_ITEM[count];

			// First vector item will be considered as vector "description"
			if (pDdiVectorList->vectors[i].list[0].type == VECTOR_STRING)
			{
				Fill_STRING(&pFdiVectorList->vectors[i].description, &pDdiVectorList->vectors[i].list[0].vector.str);
			}
			else
			{
				pFdiVectorList->vectors[i].description.assign((wchar_t*)"");
				//pFdiVectorList->vectors[i].description.assign((wchar_t*)"", false);
			}

			for(int j = 0; j < count; j++)
			{
				// Skip the first item as we have considered it as vactor "description".
				switch (pDdiVectorList->vectors[i].list[j+1].type)
				{

				case VECTOR_CONSTANT:
					pFdiVectorList->vectors[i].list[j].type = nsEDDEngine::FDI_DATA_CONSTANT;
					pFdiVectorList->vectors[i].list[j].vector.iconst = pDdiVectorList->vectors[i].list[j+1].vector.iconst;
					break;
				case VECTOR_FLOATING:
					pFdiVectorList->vectors[i].list[j].type = nsEDDEngine::FDI_DATA_FLOATING;
					pFdiVectorList->vectors[i].list[j].vector.fconst = pDdiVectorList->vectors[i].list[j+1].vector.fconst;
					break;
				case VECTOR_REFERENCE:
					pFdiVectorList->vectors[i].list[j].type = nsEDDEngine::FDI_DATA_REFERENCE;
					Fill_OP_REF_TRAIL(&pFdiVectorList->vectors[i].list[j].vector.ref, &pDdiVectorList->vectors[i].list[j+1].vector.ref);
					break;
				case VECTOR_STRING:
					pFdiVectorList->vectors[i].list[j].type = nsEDDEngine::FDI_DATA_STRING;
					Fill_STRING(&pFdiVectorList->vectors[i].list[j].vector.str, &pDdiVectorList->vectors[i].list[j+1].vector.str);
					break;
				default:
					EddEngineLog(this, __FILE__, __LINE__, BssError, L"LegacyFF",
						L"CLegacyFF::Fill_VECTOR_LIST() cannot convert Vector item type: %d", pDdiVectorList->vectors[i].list[j+1].type);
				}
			}
		}
	}
}

void CLegacyFF::Fill_DEFAULT_VALUES_LIST(nsEDDEngine::DEFAULT_VALUES_LIST *pFdiDefaultValueList, DEFAULT_VALUE_LIST *pDdiDefaultValueList)
{
	if (pDdiDefaultValueList->count != 0)
	{
		pFdiDefaultValueList->count = pDdiDefaultValueList->count;
		pFdiDefaultValueList->limit = pDdiDefaultValueList->count;
		pFdiDefaultValueList->list = new nsEDDEngine::DEFAULT_VALUE_ITEM[pDdiDefaultValueList->count];

		for (int i = 0; i < pDdiDefaultValueList->count; i++)
		{
			Fill_OP_REF_TRAIL(&pFdiDefaultValueList->list[i].ref, &pDdiDefaultValueList->list[i].ref);
			Fill_EXPR(&pFdiDefaultValueList->list[i].value, &pDdiDefaultValueList->list[i].value);
		}
	}
}
