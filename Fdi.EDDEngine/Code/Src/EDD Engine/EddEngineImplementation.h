#pragma once

#include <string>
#include <map>
#include <memory>

#include "MyParamCache.h"

#include "nsEDDEngine/Common.h"
#include "nsEDDEngine/EDDEngineFactory.h"
#include "nsEDDEngine/Table.h"
#include "nsEDDEngine/FFBlockInfo.h"
#include "nsEDDEngine/IItemInfo.h"
#include "nsEDDEngine/IEDDEngine.h"
#include "nsEDDEngine/IConvenience.h"
#include "nsEDDEngine/Flats.h"
#include "nsConsumer/IParamCache.h"
#include "nsConsumer/IBuiltinSupport.h"

class __declspec(dllexport) EddEngineImplementation
{
public:
	int Open(const wchar_t* filename);

	void GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader);


  int GetItem(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::AttributeNameSet* pAttributeNameSet, const wchar_t* lang_code, nsEDDEngine::FDI_GENERIC_ITEM* pGenericItem);

	nsEDDEngine::FLAT_DEVICE_DIR* GetDeviceDir();

	const wchar_t* GetSymbolNameFromItemId(ITEM_ID item_id);

	const nsEDDEngine::IEDDEngine* Engine()
	{
		return engine.get();
	}


private:

	void addFFBlocks();

	std::unique_ptr<nsEDDEngine::IEDDEngine> engine;
	ParamCache* paramCache;
};
