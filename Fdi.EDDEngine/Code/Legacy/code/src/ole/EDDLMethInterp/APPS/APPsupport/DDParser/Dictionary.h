#ifndef DICTIONARY_H
#define DICTIONARY_H
//#include "PlatformCommon.h"
#include "DDlConditional.h"// includes--"Attributes.h"
#include <string>
#include <map>

#define DEF__LANG__CTRY		_T("|en|")	/* Of the form "|LL|" or "|LL CC|" */
#define DEF_LANG_CTRY		"|en|"	/* Of the form "|LL|" or "|LL CC|" */

#define COUNTRY_CODE_MARK 	_T('|')


class CDictionary
{
	public:	
		
		virtual int get_dictionary_string(char* chStringKey , STRING *str) = 0;
		// This version to be used by DD_Parser
		virtual int get_dictionary_string( unsigned long index, STRING *str )=0;
	
		// This one to be used by Builtin Library & SDC
		virtual int get_dictionary_string(wchar_t* chStringKey ,  wchar_t* str, int iStringLen )= 0;
		virtual int get_dictionary_string( unsigned long index, wchar_t* str, int iStringLen )=0;
		virtual int get_lit_string( unsigned long index, wchar_t* str, long lStringLen ) = 0;

 		int get_string_translation(wchar_t *string,wchar_t *outbuf,int outbuf_size, const wchar_t *lang_cntry);
};



#endif/*DICTIONARY_H*/
