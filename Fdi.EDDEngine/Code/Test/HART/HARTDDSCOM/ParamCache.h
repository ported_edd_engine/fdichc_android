#pragma once

#include "nsConsumer\IParamCache.h"
#include "nsEDDEngine\IConvenience.h"
#include "nsEDDEngine\IItemInfo.h"

using namespace nsEDDEngine;
using namespace nsConsumer;


class CParamCache : public nsConsumer::IParamCache
{
	IConvenience *m_IConvenience;
	IItemInfo *m_IItemInfo;

	ITEM_ID m_itemProcessing;		// Keep track of the ITEM_ID for the param we are currently processing
	SUBINDEX m_subindexProcessing;	// This is used to minimize infinite loops.

public:
	CParamCache();
	~CParamCache(void);
	void AssignIConvenience( IConvenience* pIConvenience );
	void AssignItemInfo( IItemInfo *pItemInfo );

	nsConsumer::PC_ErrorCode GetParamValue(int iBlockInstance, void* pValueSpec, FDI_PARAM_SPECIFIER *pParamSpec, EVAL_VAR_VALUE * pParamValue);

    virtual PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ EVAL_VAR_VALUE * pParamValue);

private:
	unsigned long GetDefaultIndex(int iBlockInstance, void * pValueSpec, FDI_PARAM_SPECIFIER *pParamSpec);

};

