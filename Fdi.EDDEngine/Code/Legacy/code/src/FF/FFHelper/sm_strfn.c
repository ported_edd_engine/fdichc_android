
/******************************************************************************
 * $Header: sm_strfn.c: Revision: 1: Author: dgoff: Date: Friday, August 9, 2019 8:00:22 AM$
 *
 * Copyright 1997 Fisher-Rosemount Systems, Inc. - All rights reserved
 *
 * Description:
 *													   
 * $Workfile: sm_strfn.c$
 *
 * $Revision: 1$  
 *
 *****************************************************************************/

#include "stdinc.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include "panic.h"
#include "cm_lib.h"





//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemId. This function is passed to qsort
//////////////////////////////////////////////////////////////////////////////

int  symidtbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	////////////////////////////////////////////////////////////////////////////
	//
	// Optimized: 6/6/96 (Gabe Avila)
	//

	if ((*elem1).item_id < (*elem2).item_id)
		return -1;
	else if ((*elem1).item_id > (*elem2).item_id)
		return 1;
	else
		return 0;

}




//////////////////////////////////////////////////////////////////////////////
//	Symtbl is sorted by ItemId. This function is passes to qsort
//////////////////////////////////////////////////////////////////////////////

int  symnametbl_compare(SYMTBL* elem1, SYMTBL* elem2)
{
	////////////////////////////////////////////////////////////////////////////
	//
	// Optimized: 6/6/96 (Gabe Avila)
	//

	if (strcmp((*elem1).item_name_ptr, (*elem2).item_name_ptr) < 0)
		return -1;
	else if (strcmp((*elem1).item_name_ptr, (*elem2).item_name_ptr) > 0)
		return 1;
	else
		return 0;

}

int change_file_ext(const wchar_t* dd_path, wchar_t* ext, wchar_t* filename)
{
	wcscpy(filename, dd_path);		// Copy *.fm? filename

	wchar_t* pExt = wcsrchr(filename, L'.');		// Find extension position

	int iFilenameSize = (pExt - filename);				// Calculate size of filename minus extension

	PS_Wcscpy(pExt, _MAX_PATH - iFilenameSize , ext);		// Copy in the new extension

	return 0;
}

#define MAX_SYM_LINE_LEN	512	// Really big so it is never reached.
/*********************************************************************************
	read_sym_file
	read sym file and stores the dd-id's and corresponding names in memory
	last update: 2008.10.21 (michfra)
    revision history:
	   2008.10.21 (michfra) SCR28216 Resolution
	   Added checking for revisions of sy5 files aside from the usual sym checking
    other notes:
	   It is assumed that the correct number of files is present in the TEMP
	   folder before this application is run.
**********************************************************************************/
int 
read_sym_file(const wchar_t* release_path, /*DD_DEVICE_ID *dd_device_id,*/ SYMINFO* syminfo)
{
	wchar_t			symfile_sym_name[_MAX_PATH] = {0};
	wchar_t			symfile_sy5_name[_MAX_PATH] = {0};
	wchar_t			*symfile_name = NULL;
    char	        line[MAX_SYM_LINE_LEN + 1]{0};
	ITEM_ID			iItemId = 0;
	int				item_name_size;
	int				recno;
	SYMTBL*			temp_symtbl;
	SYMTBL*			temp_symnametbl;
	char			szSep[] = " \t\n";		// Valid token separators


#ifdef _DEBUG
	int i;
#endif
	
	// Choose the correct symbol file name. Try .sy5 first, then .sym

	change_file_ext(release_path, L".sy5", symfile_sy5_name);


    DWORD attrib = PS_GetFileAttributes(symfile_sy5_name);
    if (attrib != INVALID_FILE_ATTRIBUTES)
	{
		symfile_name = symfile_sy5_name;   
    }
	else
	{
		change_file_ext(release_path, L".sym", symfile_sym_name);

        DWORD attribute = PS_GetFileAttributes(symfile_sym_name);
		if (attribute != INVALID_FILE_ATTRIBUTES)
  		{
			symfile_name = symfile_sym_name;   
		}
		else
		{
			return (SM_NO_SYMFILE_FOUND);
		}
	}

	/*
	 * Open the symbol file
	 */
    FILE *symfile = PS_wfopen(symfile_name, L"r");

	if (symfile == NULL)
	{
		return SM_SYMFILE_OPEN_ERR;
	}

	// Count the number of lines in the file. This is the number of symbols to load.
	int tblsize = 0;
	while (!feof (symfile))
	{
		fgets (line, sizeof(line)-1, symfile);
		tblsize ++;	
	}

	if (tblsize == 0)		// If none, then there was an error
	{
		fclose (symfile);
		return SM_SYMFILE_OPEN_ERR;
	}
	

	// Create SYMTBL arrays to hold the symbol information

	temp_symtbl = (SYMTBL*)malloc( tblsize * sizeof(SYMTBL) );
	if (temp_symtbl == NULL) {
		panic("read_sym_file: malloc for temp_symtbl failed \n");
	}
	memset(temp_symtbl, 0, sizeof(temp_symtbl));

	temp_symnametbl = (SYMTBL*)malloc( tblsize * sizeof(SYMTBL) );
	if (temp_symnametbl == NULL) {
		panic("read_sym_file: malloc for temp_symnametbl failed \n");
	}
	memset(temp_symnametbl, 0, sizeof(temp_symnametbl));


	rewind (symfile);	// Rewind to the beginning of the file to parse each line.

	recno = -1;
	(void) fgets (line, sizeof(line)-1, symfile);
	for (; !feof (symfile); fgets(line, sizeof(line)-1, symfile) ) {
		char *pItemId = strtok (line, szSep);			// Position at Column 1 (but ignore)
		char *pItemName = strtok ((char *)NULL, szSep);	// Get ItemName from Column 2
		char *pCol3 = strtok ((char *)NULL, szSep);		// Position at Column 3
		char *pCol4 = strtok ((char *)NULL, szSep);		// Position at Column 4

		if (pCol4 == NULL)
		{
			pItemId = pCol3;	// If there is no Column 4, the ItemId is in Column 3
		}
		else
		{
			pItemId = pCol4;	// If there is a Column 4, the ItemId is found there
		}
		
		if(pItemId == nullptr || pItemName == nullptr)
		{
			free(temp_symtbl);
			free(temp_symnametbl);
			return SM_SYMFILE_OPEN_ERR;
		}

		// Insert into the tables
		recno++;

		// First insert the ItemName
		item_name_size = strlen(pItemName) + 1;
	
		temp_symtbl[recno].item_name_ptr = (char*) malloc(item_name_size);
		if (temp_symtbl[recno].item_name_ptr == NULL) {
			panic("read_sym_file: malloc for temp_symtbl.item_name_ptr failed \n");
		}
		strcpy(temp_symtbl[recno].item_name_ptr, pItemName);

		temp_symnametbl[recno].item_name_ptr = (char*) malloc(item_name_size);
		if (temp_symnametbl[recno].item_name_ptr == NULL) {
			panic("read_sym_file: malloc for temp_symnametbl.item_name_ptr failed \n");
		}
		strcpy(temp_symnametbl[recno].item_name_ptr, pItemName);

		// Now insert the ItemId

		iItemId = strtoul(pItemId, NULL, 16);

		temp_symtbl[recno].item_id = iItemId;
		temp_symnametbl[recno].item_id = iItemId;
	}

	/*
	 * Close the symbol file
	 */
	(void) fclose (symfile);

	int iSymCnt = recno+1;

	//sort by DDID
	qsort(temp_symtbl, iSymCnt, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symidtbl_compare);

	syminfo->symidtbl = temp_symtbl;

	//sort by DDstring
	qsort(temp_symnametbl, iSymCnt, sizeof(SYMTBL), 
			(int (*) (const void*,const void*))symnametbl_compare);
	
	syminfo->symnametbl = temp_symnametbl;
	
	syminfo->numelem = iSymCnt;

#ifdef _DEBUG
// code for dumping copied from pc_lib.cpp.

	change_file_ext(symfile_name, L".dbg", symfile_name);
	
	symfile = _wfopen (symfile_name, L"w");
    if (symfile != NULL)
    {
	    fprintf(symfile,  "*********************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY ID      \n");
	    fprintf(symfile,  "*********************************\n");
	    for (i = 0; i < iSymCnt; i++) {
		    fprintf(symfile, "%10X            %s\n", temp_symtbl[i].item_id, 
				    temp_symtbl[i].item_name_ptr);
	    }
	    fprintf(symfile,  "\n");
	    fprintf(symfile,  "=================================\n\n");

	    fprintf(symfile,  "*********************************\n");
	    fprintf(symfile,  "* SYMBOL TABLE SORTED BY NAME     \n");
	    fprintf(symfile,  "*********************************\n");
	    for (i = 0; i < iSymCnt; i++) {
		    fprintf(symfile, "%10X            %s\n", temp_symnametbl[i].item_id, 
				    temp_symnametbl[i].item_name_ptr);
	    }
	    (void) fclose (symfile);
    }

#endif

	return SUCCESS;

}


void free_symtbl(SYMINFO* syminfo)
{
	SYMTBL* symtbl;
	int		num;
	int		i, j;

	num = syminfo->numelem;
	symtbl = syminfo->symidtbl;

	// free memory pointed to by item_name_ptr before freeing the table itself
	for (i = 0; i < num; i++) {
		(void)free((void*)(symtbl[i].item_name_ptr));
	}

	free((void*)symtbl);
	syminfo->symidtbl = 0;

	symtbl = syminfo->symnametbl;

	// free memory pointed to by item_name_ptr before freeing the table itself
	for (j = 0; j < num; j++) {
		(void)free((void*)(symtbl[j].item_name_ptr));
	}

	free((void*)symtbl);
	syminfo->symnametbl = 0;

	syminfo->numelem = 0;

}


////////////////////////////////////////////////////////////////////////////
//
// The following optimizations have passed all RTEST's but are not being 
// compiled yet (per Jon) for extra cutiousness: 6/6/96 (Gabe Avila)
//
// NOTE:	I Benchmarked these optimized functions and they performed 
//			faster than Microsoft's bsearch()
//

//
// Converts item_id to item-name. 
//

// Original code

int 
server_item_id_to_name(SYMINFO* syminfo, ITEM_ID item_id, wchar_t* item_name, int outbuf_size)
{
	int	low;
	int med;
	int	high;
	SYMTBL* symtbl;
	BOOLEAN	flag;

	flag = FALSE;
	low = 0;
	high = syminfo->numelem;
	symtbl = syminfo->symidtbl;

	while (low < high) {
	
		med = (low + high) / 2;
		// the next check is to stop this code going into infinite loop for
		// invalid numbers. It might have to be modified
		if (med == low) {
			if (!flag)
				flag = TRUE;
			else
				return SM_ID_NOT_FOUND;
		}

		if (item_id < symtbl[med].item_id)

			high = med;

		else if (item_id > symtbl[med].item_id)

			low = med;

		else {

			if ((size_t) outbuf_size <= strlen(symtbl[med].item_name_ptr))
			{
				item_name[0] = '\0';
				return DDI_INSUFFICIENT_BUFFER;
			} 
			else
			{
				mbstowcs(item_name, symtbl[med].item_name_ptr, strlen(symtbl[med].item_name_ptr)+1 );
			}
			
			
			return SUCCESS;
			break;
		}

	}

	return SM_ID_NOT_FOUND;
}



//
// Converts item-name to item_id.
//

// Original code
int 
server_item_name_to_id(SYMINFO* syminfo, wchar_t* item_name, ITEM_ID *item_id)
{
	int	low;
	int med;
	int	high;
	SYMTBL* symtbl;
	BOOLEAN	flag;

	flag = FALSE;
	low = 0;
	high = syminfo->numelem;
	symtbl = syminfo->symnametbl;
	char chItemName[_MAX_PATH] = {0};
	wcstombs(chItemName, item_name, wcslen(item_name));
	while (low < high) {
	
		med = (low + high) / 2;
		/* the next check is to stop this code going into infinite loop for
		 	invalid numbers. It might have to be modified */
		if (med == low) {
			if (!flag)
				flag = TRUE;
		 	else
				return SM_NAME_NOT_FOUND;
		}

		if (strcmp(chItemName, symtbl[med].item_name_ptr) < 0) 

			high = med;

		else 	if (strcmp(chItemName, symtbl[med].item_name_ptr) > 0) 

			low = med;

		else {

			*item_id = symtbl[med].item_id;
			return SUCCESS;
		}

	}

	return SM_NAME_NOT_FOUND;
}


// Optimized code
/*
int 
server_item_name_to_id(SYMINFO* syminfo, char* item_name, ITEM_ID *item_id)
{
	int comp;
	int med;
	int	low = 0;
	int	high = syminfo->numelem;
	SYMTBL* symtbl = syminfo->symnametbl;

	while(low <= high) 
	{
		med = (low + high) / 2;
		comp = strcmp(item_name, symtbl[med].item_name_ptr);
		if(comp > 0) 
			low = med + 1;
		else if(comp < 0) 
			high = med - 1;
		else 
		{
			*item_id = symtbl[med].item_id;
			return SUCCESS;
		}
	}

	return SM_NAME_NOT_FOUND;
}
*/

//
////////////////////////////////////////////////////////////////////////////

