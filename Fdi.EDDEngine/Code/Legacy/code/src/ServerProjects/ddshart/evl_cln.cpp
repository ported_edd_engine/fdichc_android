/*
 *	@(#) $Id: evl_cln.c,v 1.2 1996/01/04 20:12:22 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *  This file contains item destruct routines.
 *
 */

#include "stdinc.h"

#pragma warning (disable : 4131)


/*********************************************************************
 *
 *	Name: eval_clean_block
 *	ShortDesc: Free the FLAT_BLOCK structure
 *
 *	Description:
 *		eval_clean_block will check the attr_avail and dynamic
 *		flags in the FLAT_BLOCK structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_block: pointer to the FLAT_BLOCK structure
 *
 *	Outputs:
 *		flat_block: pointer to the empty FLAT_BLOCK structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_block(
FLAT_BLOCK     *flat_block)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	BLOCK_DEPBIN   *temp_depbin;

	if (flat_block == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_block, BLOCK_ITYPE);
#endif

	temp_attr_avail = flat_block->masks.attr_avail;
	temp_dynamic = flat_block->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_block->characteristic = 0;

	if (temp_attr_avail & BLOCK_HELP) {
		ddl_free_string(&flat_block->help);
	}

	if (temp_attr_avail & BLOCK_LABEL) {
		ddl_free_string(&flat_block->label);
	}

	if (temp_attr_avail & BLOCK_PARAM) {
		ddl_free_members_list(&flat_block->param, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_PARAM_LIST) {
		ddl_free_members_list(&flat_block->param_list, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_ITEM_ARRAY) {
		ddl_free_reflist(&flat_block->item_array, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_COLLECT) {
		ddl_free_reflist(&flat_block->collect, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_MENU) {
		ddl_free_reflist(&flat_block->menu, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_EDIT_DISP) {
		ddl_free_reflist(&flat_block->edit_disp, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_METHOD) {
		ddl_free_reflist(&flat_block->method, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_UNIT) {
		ddl_free_reflist(&flat_block->unit, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_REFRESH) {
		ddl_free_reflist(&flat_block->refresh, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_WAO) {
		ddl_free_reflist(&flat_block->wao, FREE_ATTR);
	}

	if (temp_attr_avail & BLOCK_DEBUG)
	{
		ddl_free_string(&flat_block->symbol_name);
	}

	flat_block->masks.attr_avail = 0;

	if (flat_block->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_block->depbin;

	if (temp_dynamic & BLOCK_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & BLOCK_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	flat_block->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_block, BLOCK_ITYPE);
#endif

}

/*********************************************************************
 *
 *	Name: eval_clean_var
 *	ShortDesc: Free the FLAT_VAR structure
 *
 *	Description:
 *		eval_clean_var will check the attr_avail and dynamic
 *		flags in the FLAT_VAR structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_var: pointer to the FLAT_VAR structure
 *
 *	Outputs:
 *		flat_var: pointer to the empty FLAT_VAR structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_var(
FLAT_VAR       *flat_var)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	VAR_DEPBIN     *temp_depbin;
	FLAT_VAR_ACTIONS *temp_actions;
	FLAT_VAR_MISC  *temp_misc;
	VAR_ACTIONS_DEPBIN *temp_act_depbin;
	VAR_MISC_DEPBIN *temp_misc_depbin;

	if (flat_var == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_var, VARIABLE_ITYPE);
#endif

	temp_attr_avail = flat_var->masks.attr_avail;
	temp_dynamic = flat_var->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_var->class_attr = 0;
	flat_var->handling = 0;
	flat_var->type_size.type = 0;
	flat_var->type_size.size = 0;
	flat_var->index_item_array = 0;
	flat_var->resp_codes = 0;

	if (temp_attr_avail & VAR_HELP) {
		ddl_free_string(&flat_var->help);
	}

	if (temp_attr_avail & VAR_LABEL) {
		ddl_free_string(&flat_var->label);
	}

	if (temp_attr_avail & VAR_DISPLAY) {
		ddl_free_string(&flat_var->display);
	}

	if (temp_attr_avail & VAR_EDIT) {
		ddl_free_string(&flat_var->edit);
	}

	if (temp_attr_avail & VAR_ENUMS) {
		ddl_free_enum_list(&flat_var->enums, FREE_ATTR);
	}

	if (temp_attr_avail & VAR_DEFAULT_VALUE) {
		ddl_free_expr(&flat_var->default_value);
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	if (flat_var->depbin != NULL) {

		temp_depbin = flat_var->depbin;

		if (temp_dynamic & VAR_CLASS) {
			ddl_free_depinfo(&temp_depbin->db_class->dep, FREE_ATTR);
		}
		if (temp_dynamic & VAR_HANDLING) {
			ddl_free_depinfo(&temp_depbin->db_handling->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_HELP) {
			ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_LABEL) {
			ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_DISPLAY) {
			ddl_free_depinfo(&temp_depbin->db_display->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_EDIT) {
			ddl_free_depinfo(&temp_depbin->db_edit->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_ENUMS) {
			ddl_free_depinfo(&temp_depbin->db_enums->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_TYPE_SIZE) {
			ddl_free_depinfo(&temp_depbin->db_type_size->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_INDEX_ITEM_ARRAY) {
			ddl_free_depinfo(&temp_depbin->db_index_item_array->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_DEFAULT_VALUE) {
			ddl_free_depinfo(&temp_depbin->db_default_value->dep, FREE_ATTR);
		}

		if (temp_dynamic & VAR_RESP_CODES) {
			ddl_free_depinfo(&temp_depbin->db_resp_codes->dep, FREE_ATTR);
		}
	}

	if (flat_var->actions != NULL) {

		temp_actions = flat_var->actions;

		/*
		 * Free actions attribute structures
		 */

		if (temp_attr_avail & VAR_PRE_EDIT_ACT) {
			ddl_free_reflist(&temp_actions->pre_edit_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_POST_EDIT_ACT) {
			ddl_free_reflist(&temp_actions->post_edit_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_PRE_READ_ACT) {
			ddl_free_reflist(&temp_actions->pre_read_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_POST_READ_ACT) {
			ddl_free_reflist(&temp_actions->post_read_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_PRE_WRITE_ACT) {
			ddl_free_reflist(&temp_actions->pre_write_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_POST_WRITE_ACT) {
			ddl_free_reflist(&temp_actions->post_write_act, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_REFRESH_ACT) {
			ddl_free_reflist(&temp_actions->refresh_act, FREE_ATTR);
		}

		if (temp_actions->depbin != NULL) {

			temp_act_depbin = temp_actions->depbin;

			/*
			 * Free actions dependancy info for attributes which
			 * allow conditional expressions. If ddl_free_depinfo()
			 * is not called for a specific attribute, it is
			 * because the attribute does not allow conditional
			 * expressions.
			 */

			if (temp_dynamic & VAR_PRE_EDIT_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_pre_edit_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_POST_EDIT_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_post_edit_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_PRE_READ_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_pre_read_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_POST_READ_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_post_read_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_PRE_WRITE_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_pre_write_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_POST_WRITE_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_post_write_act->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_REFRESH_ACT) {
				ddl_free_depinfo(&temp_act_depbin->db_refresh_act->dep, FREE_ATTR);
			}
		}
	}

	if (flat_var->misc != NULL) {

		temp_misc = flat_var->misc;

		/*
		 * Free misc attribute structures
		 */

		temp_misc->read_time_out = 0;
		temp_misc->write_time_out = 0;
		temp_misc->scale.type = 0;
		temp_misc->scale.size = 0;
		temp_misc->scale.val.d = 0;
		temp_misc->valid = 0;
		temp_misc->time_scale = 0;

		if (temp_attr_avail & VAR_UNIT) {
			ddl_free_string(&temp_misc->unit);
		}

		if (temp_attr_avail & VAR_TIME_FORMAT) {
			ddl_free_string(&temp_misc->time_format);
		}

		if (temp_attr_avail & VAR_MIN_VAL) {
			ddl_free_range_list(&temp_misc->min_val, FREE_ATTR);
		}

		if (temp_attr_avail & VAR_MAX_VAL) {
			ddl_free_range_list(&temp_misc->max_val, FREE_ATTR);
		}
		
		if( temp_attr_avail & VAR_DEBUG ){
			ddl_free_string(&temp_misc->symbol_name);
		}

		if (temp_misc->depbin != NULL) {

			temp_misc_depbin = temp_misc->depbin;

			/*
			 * Free misc dependancy info for attributes which allow
			 * conditional expressions. If ddl_free_depinfo() is
			 * not called for a specific attribute, it is because
			 * the attribute does not allow conditional
			 * expressions.
			 */

			if (temp_dynamic & VAR_UNIT) {
				ddl_free_depinfo(&temp_misc_depbin->db_unit->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_READ_TIME_OUT) {
				ddl_free_depinfo(&temp_misc_depbin->db_read_time_out->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_WRITE_TIME_OUT) {
				ddl_free_depinfo(&temp_misc_depbin->db_write_time_out->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_MIN_VAL) {
				ddl_free_depinfo(&temp_misc_depbin->db_min_val->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_MAX_VAL) {
				ddl_free_depinfo(&temp_misc_depbin->db_max_val->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_SCALE) {
				ddl_free_depinfo(&temp_misc_depbin->db_scale->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_VALID) {
				ddl_free_depinfo(&temp_misc_depbin->db_valid->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_TIME_SCALE) {
				ddl_free_depinfo(&temp_misc_depbin->db_time_scale->dep, FREE_ATTR);
			}

			if (temp_dynamic & VAR_TIME_FORMAT) {
				ddl_free_depinfo(&temp_misc_depbin->db_time_format->dep, FREE_ATTR);
			}
		}
	}

	flat_var->masks.attr_avail = 0;
	flat_var->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_var, VARIABLE_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_program
 *	ShortDesc: Free the FLAT_PROGRAM structure
 *
 *	Description:
 *		eval_clean_program will check the attr_avail and dynamic
 *		flags in the FLAT_PROGRAM structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_program: pointer to the FLAT_PROGRAM structure
 *
 *	Outputs:
 *		flat_program: pointer to the empty FLAT_PROGRAM structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_program(
FLAT_PROGRAM   *flat_program)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	PROGRAM_DEPBIN *temp_depbin;

	if (flat_program == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_program, PROGRAM_ITYPE);
#endif

	temp_attr_avail = flat_program->masks.attr_avail;
	temp_dynamic = flat_program->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_program->resp_codes = 0;

	if (temp_attr_avail & PROGRAM_ARGS) {
		ddl_free_dataitems(&flat_program->args, FREE_ATTR);
	}

	flat_program->masks.attr_avail = 0;

	if (flat_program->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_program->depbin;

	if (temp_dynamic & PROGRAM_ARGS) {
		ddl_free_depinfo(&temp_depbin->db_args->dep, FREE_ATTR);
	}

	flat_program->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_program, PROGRAM_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_menu
 *	ShortDesc: Free the FLAT_MENU structure
 *
 *	Description:
 *		eval_clean_menu will check the attr_avail and dynamic
 *		flags in the FLAT_MENU structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_menu: pointer to the FLAT_MENU structure
 *
 *	Outputs:
 *		flat_menu: pointer to the empty FLAT_MENU structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_menu(
FLAT_MENU      *flat_menu)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	MENU_DEPBIN    *temp_depbin;

	if (flat_menu == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_menu, MENU_ITYPE);
#endif

	temp_attr_avail = flat_menu->masks.attr_avail;
	temp_dynamic = flat_menu->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_menu->valid = 0;
	flat_menu->style = 0;

	if (temp_attr_avail & MENU_LABEL) {
		ddl_free_string(&flat_menu->label);
	}

	if (temp_attr_avail & MENU_HELP) {
		ddl_free_string(&flat_menu->help);
	}

	if (temp_attr_avail & MENU_ITEMS) {
		ddl_free_menuitems_list(&flat_menu->items, FREE_ATTR);
	}

	if (temp_attr_avail & MENU_DEBUG) {
		ddl_free_string(&flat_menu->symbol_name);
	}

	flat_menu->masks.attr_avail = 0;

	if (flat_menu->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_menu->depbin;

	if (temp_dynamic & MENU_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	if (temp_dynamic & MENU_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & MENU_ITEMS) {
		ddl_free_depinfo(&temp_depbin->db_items->dep, FREE_ATTR);
	}

  	if (temp_dynamic & MENU_STYLE) {
		ddl_free_depinfo(&temp_depbin->db_style->dep, FREE_ATTR);
	}
	if (temp_dynamic & MENU_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_menu->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_menu, MENU_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_edit_display
 *	ShortDesc: Free the FLAT_EDIT_DISPLAY structure
 *
 *	Description:
 *		eval_clean_edit_display will check the attr_avail and dynamic
 *		flags in the FLAT_EDIT_DISPLAY structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_edit_display: pointer to the FLAT_EDIT_DISPLAY structure
 *
 *	Outputs:
 *		flat_edit_display: pointer to the empty FLAT_EDIT_DISPLAY structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_edit_display(
FLAT_EDIT_DISPLAY *flat_edit_display)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	EDIT_DISPLAY_DEPBIN *temp_depbin;

	if (flat_edit_display == NULL) 
	{
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_edit_display, EDIT_DISP_ITYPE);
#endif

	temp_attr_avail = flat_edit_display->masks.attr_avail;
	temp_dynamic = flat_edit_display->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & EDIT_DISPLAY_DISP_ITEMS) 
	{
		ddl_free_op_ref_trail_list(&flat_edit_display->disp_items, FREE_ATTR);
	}

	if (temp_attr_avail & EDIT_DISPLAY_EDIT_ITEMS) 
	{
		ddl_free_op_ref_trail_list(&flat_edit_display->edit_items, FREE_ATTR);
	}

	if (temp_attr_avail & EDIT_DISPLAY_LABEL) 
	{
		ddl_free_string(&flat_edit_display->label);
	}

	if (temp_attr_avail & EDIT_DISPLAY_PRE_EDIT_ACT) 
	{
		ddl_free_reflist(&flat_edit_display->pre_edit_act, FREE_ATTR);
	}

	if (temp_attr_avail & EDIT_DISPLAY_HELP)
	{
		ddl_free_string(&flat_edit_display->help);
	}

	if (temp_attr_avail & EDIT_DISPLAY_POST_EDIT_ACT) 
	{
		ddl_free_reflist(&flat_edit_display->post_edit_act, FREE_ATTR);
	}

	if(temp_attr_avail & EDIT_DISPLAY_DEBUG)
	{
		ddl_free_string(&flat_edit_display->symbol_name);
	}

	flat_edit_display->masks.attr_avail = 0;

	if (flat_edit_display->depbin == NULL) 
	{
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_edit_display->depbin;

	if (temp_dynamic & EDIT_DISPLAY_DISP_ITEMS) 
	{
		ddl_free_depinfo(&temp_depbin->db_disp_items->dep, FREE_ATTR);
	}

	if (temp_dynamic & EDIT_DISPLAY_EDIT_ITEMS) 
	{
		ddl_free_depinfo(&temp_depbin->db_edit_items->dep, FREE_ATTR);
	}

	if (temp_dynamic & EDIT_DISPLAY_LABEL) 
	{
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	if ( temp_dynamic & EDIT_DISPLAY_HELP )
	{
		ddl_free_depinfo( &temp_depbin->db_help->dep, FREE_ATTR );
	}

	if (temp_dynamic & EDIT_DISPLAY_PRE_EDIT_ACT) 
	{
		ddl_free_depinfo(&temp_depbin->db_pre_edit_act->dep, FREE_ATTR);
	}

	if (temp_dynamic & EDIT_DISPLAY_POST_EDIT_ACT) 
	{
		ddl_free_depinfo(&temp_depbin->db_post_edit_act->dep, FREE_ATTR);
	}

	if (temp_dynamic & EDIT_DISPLAY_VALID) 
	{
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_edit_display->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_edit_display, EDIT_DISP_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_method
 *	ShortDesc: Free the FLAT_METHOD structure
 *
 *	Description:
 *		eval_clean_method will check the attr_avail and dynamic
 *		flags in the FLAT_METHOD structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_method: pointer to the FLAT_METHOD structure
 *
 *	Outputs:
 *		flat_method: pointer to the empty FLAT_METHOD structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_method(
FLAT_METHOD    *flat_method)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	METHOD_DEPBIN  *temp_depbin;

	if (flat_method == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_method, METHOD_ITYPE);
#endif

	temp_attr_avail = flat_method->masks.attr_avail;
	temp_dynamic = flat_method->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_method->class_attr = 0;
	flat_method->valid = 0;
	flat_method->scope = 0;

	if (temp_attr_avail & METHOD_DEF) {
		if (flat_method->def.data) {
			free((void *) flat_method->def.data);
		}
		flat_method->def.data = NULL;
		flat_method->def.size = 0;
	}

	if (temp_attr_avail & METHOD_HELP) {
		ddl_free_string(&flat_method->help);
	}

	if (temp_attr_avail & METHOD_LABEL) {
		ddl_free_string(&flat_method->label);
	}

	if (temp_attr_avail & METHOD_PARAMS) {
		ddl_free_string(&flat_method->params);
	}

	if ( temp_attr_avail & METHOD_DEBUG )
	{
		ddl_free_string( &flat_method->symbol_name );
	}

	flat_method->masks.attr_avail = 0;

	if (flat_method->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_method->depbin;

	if (temp_dynamic & METHOD_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_CLASS) {
		ddl_free_depinfo(&temp_depbin->db_class->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_DEF) {
		ddl_free_depinfo(&temp_depbin->db_def->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_TYPE) {
		ddl_free_depinfo(&temp_depbin->db_type->dep, FREE_ATTR);
	}

	if (temp_dynamic & METHOD_PARAMS) {
		ddl_free_depinfo(&temp_depbin->db_params->dep, FREE_ATTR);
	}

	flat_method->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_method, METHOD_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_refresh
 *	ShortDesc: Free the FLAT_REFRESH structure
 *
 *	Description:
 *		eval_clean_refresh will check the attr_avail and dynamic
 *		flags in the FLAT_REFRESH structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_refresh: pointer to the FLAT_REFRESH structure
 *
 *	Outputs:
 *		flat_refresh: pointer to the empty FLAT_REFRESH structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_refresh(
FLAT_REFRESH   *flat_refresh)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	REFRESH_DEPBIN *temp_depbin;

	if (flat_refresh == NULL) {
		return;
	}

	temp_attr_avail = flat_refresh->masks.attr_avail;
	temp_dynamic = flat_refresh->masks.dynamic;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_refresh, REFRESH_ITYPE);
#endif

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & REFRESH_ITEMS) {
		ddl_free_refresh(&flat_refresh->items, FREE_ATTR);
	}

	if (temp_attr_avail & REFRESH_DEBUG)
	{
		ddl_free_string(&flat_refresh->symbol_name);
	}

	flat_refresh->masks.attr_avail = 0;

	if (flat_refresh->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_refresh->depbin;

	if (temp_dynamic & REFRESH_ITEMS) {
		ddl_free_depinfo(&temp_depbin->db_items->dep, FREE_ATTR);
	}

	flat_refresh->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_refresh, REFRESH_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_unit
 *	ShortDesc: Free the FLAT_UNIT structure
 *
 *	Description:
 *		eval_clean_unit will check the attr_avail and dynamic
 *		flags in the FLAT_UNIT structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_unit: pointer to the FLAT_UNIT structure
 *
 *	Outputs:
 *		flat_unit: pointer to the empty FLAT_UNIT structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_unit(
FLAT_UNIT      *flat_unit)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	UNIT_DEPBIN    *temp_depbin;

	if (flat_unit == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_unit, UNIT_ITYPE);
#endif

	temp_attr_avail = flat_unit->masks.attr_avail;
	temp_dynamic = flat_unit->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & UNIT_ITEMS) {
		ddl_free_unit(&flat_unit->items, FREE_ATTR);
	}
	
	if( temp_attr_avail & UNIT_DEBUG)
	{
		ddl_free_string(&flat_unit->symbol_name);
	}
	flat_unit->masks.attr_avail = 0;

	if (flat_unit->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_unit->depbin;

	if (temp_dynamic & UNIT_ITEMS) {
		ddl_free_depinfo(&temp_depbin->db_items->dep, FREE_ATTR);
	}

	flat_unit->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_unit, UNIT_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_wao
 *	ShortDesc: Free the FLAT_WAO structure
 *
 *	Description:
 *		eval_clean_wao will check the attr_avail and dynamic
 *		flags in the FLAT_WAO structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_wao: pointer to the FLAT_WAO structure
 *
 *	Outputs:
 *		flat_wao: pointer to the empty FLAT_WAO structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_wao(
FLAT_WAO       *flat_wao)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	WAO_DEPBIN     *temp_depbin;

	if (flat_wao == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_wao, WAO_ITYPE);
#endif

	temp_attr_avail = flat_wao->masks.attr_avail;
	temp_dynamic = flat_wao->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & WAO_ITEMS) {
		ddl_free_op_ref_trail_list(&flat_wao->items, FREE_ATTR);
	}
	if(temp_attr_avail & WAO_DEBUG)
	{
		ddl_free_string(&flat_wao->symbol_name);
	}

	flat_wao->masks.attr_avail = 0;

	if (flat_wao->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_wao->depbin;

	if (temp_dynamic & WAO_ITEMS) {
		ddl_free_depinfo(&temp_depbin->db_items->dep, FREE_ATTR);
	}

	flat_wao->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_wao, WAO_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_itemarray
 *	ShortDesc: Free the FLAT_ITEM_ARRAY structure
 *
 *	Description:
 *		eval_clean_itemarray will check the attr_avail and dynamic
 *		flags in the FLAT_ITEM_ARRAY structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_item_array: pointer to the FLAT_ITEM_ARRAY structure
 *
 *	Outputs:
 *		flat_item_array: pointer to the empty FLAT_ITEM_ARRAY structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_itemarray(
FLAT_ITEM_ARRAY *flat_item_array)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	ITEM_ARRAY_DEPBIN *temp_depbin;

	if (flat_item_array == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_item_array, ITEM_ARRAY_ITYPE);
#endif

	temp_attr_avail = flat_item_array->masks.attr_avail;
	temp_dynamic = flat_item_array->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & ITEM_ARRAY_ELEMENTS) {
		ddl_free_itemarray_list(&flat_item_array->elements, FREE_ATTR);
	}

	if (temp_attr_avail & ITEM_ARRAY_HELP) {
		ddl_free_string(&flat_item_array->help);
	}

	if (temp_attr_avail & ITEM_ARRAY_LABEL) {
		ddl_free_string(&flat_item_array->label);
	}

	if (temp_attr_avail & ITEM_ARRAY_DEBUG)
	{
		ddl_free_string(&flat_item_array->symbol_name);
	}

	flat_item_array->masks.attr_avail = 0;

	if (flat_item_array->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_item_array->depbin;

	if (temp_dynamic & ITEM_ARRAY_ELEMENTS) {
		ddl_free_depinfo(&temp_depbin->db_elements->dep, FREE_ATTR);
	}

	if (temp_dynamic & ITEM_ARRAY_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & ITEM_ARRAY_VALIDITY) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	if (temp_dynamic & ITEM_ARRAY_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	flat_item_array->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_item_array, ITEM_ARRAY_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_array
 *	ShortDesc: Free the FLAT_ARRAY structure
 *
 *	Description:
 *		eval_clean_array will check the attr_avail and dynamic
 *		flags in the FLAT_ARRAY structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_array: pointer to the FLAT_ARRAY structure
 *
 *	Outputs:
 *		flat_array: pointer to the empty FLAT_ARRAY structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_array(
FLAT_ARRAY     *flat_array)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	ARRAY_DEPBIN   *temp_depbin;

	if (flat_array == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_array, ARRAY_ITYPE);
#endif

	temp_attr_avail = flat_array->masks.attr_avail;
	temp_dynamic = flat_array->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_array->num_of_elements = 0;
	flat_array->type = 0;
	flat_array->resp_codes = 0;

	if (temp_attr_avail & ARRAY_HELP) {
		ddl_free_string(&flat_array->help);
	}

	if (temp_attr_avail & ARRAY_LABEL) {
		ddl_free_string(&flat_array->label);
	}

	if ( temp_attr_avail & ARRAY_DEBUG)
	{
		ddl_free_string(&flat_array->symbol_name);
	}

	flat_array->masks.attr_avail = 0;

	if (flat_array->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_array->depbin;

	if (temp_dynamic & ARRAY_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & ARRAY_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	if (temp_dynamic & ARRAY_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_array->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_array, ARRAY_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_collection
 *	ShortDesc: Free the FLAT_COLLECTION structure
 *
 *	Description:
 *		eval_clean_collection will check the attr_avail and dynamic
 *		flags in the FLAT_COLLECTION structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_collection: pointer to the FLAT_COLLECTION structure
 *
 *	Outputs:
 *		flat_collection: pointer to the empty FLAT_COLLECTION structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_collection(
FLAT_COLLECTION *flat_collection)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	COLLECTION_DEPBIN *temp_depbin;

	if (flat_collection == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_collection, COLLECTION_ITYPE);
#endif

	temp_attr_avail = flat_collection->masks.attr_avail;
	temp_dynamic = flat_collection->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & COLLECTION_MEMBERS) {
		ddl_free_op_members_list(&flat_collection->members, FREE_ATTR);
	}

	if (temp_attr_avail & COLLECTION_HELP) {
		ddl_free_string(&flat_collection->help);
	}

	if (temp_attr_avail & COLLECTION_LABEL) {
		ddl_free_string(&flat_collection->label);
	}

	if (temp_attr_avail & COLLECTION_DEBUG)
	{
		ddl_free_string(&flat_collection->symbol_name);
	}

	flat_collection->masks.attr_avail = 0;

	if (flat_collection->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_collection->depbin;

	if (temp_dynamic & COLLECTION_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}

	if (temp_dynamic & COLLECTION_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & COLLECTION_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
				
	if (temp_dynamic & COLLECTION_VALIDITY) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_collection->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_collection, COLLECTION_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_record
 *	ShortDesc: Free the FLAT_RECORD structure
 *
 *	Description:
 *		eval_clean_record will check the attr_avail and dynamic
 *		flags in the FLAT_RECORD structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_record: pointer to the FLAT_RECORD structure
 *
 *	Outputs:
 *		flat_record: pointer to the empty FLAT_RECORD structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_record(
FLAT_RECORD    *flat_record)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	RECORD_DEPBIN  *temp_depbin;

	if (flat_record == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_record, RECORD_ITYPE);
#endif

	temp_attr_avail = flat_record->masks.attr_avail;
	temp_dynamic = flat_record->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_record->resp_codes = 0;

	if (temp_attr_avail & RECORD_MEMBERS) {
		ddl_free_members_list(&flat_record->members, FREE_ATTR);
	}

	if (temp_attr_avail & RECORD_HELP) {
		ddl_free_string(&flat_record->help);
	}

	if (temp_attr_avail & RECORD_LABEL) {
		ddl_free_string(&flat_record->label);
	}

	if (temp_attr_avail & RECORD_DEBUG)
	{
		ddl_free_string(&flat_record->symbol_name);
	}

	flat_record->masks.attr_avail = 0;

	if (flat_record->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_record->depbin;

	if (temp_dynamic & RECORD_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}

	if (temp_dynamic & RECORD_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & RECORD_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	flat_record->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_record, RECORD_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_var_list
 *	ShortDesc: Free the FLAT_VAR_LIST structure
 *
 *	Description:
 *		eval_clean_var_list will check the attr_avail and dynamic
 *		flags in the FLAT_VAR_LIST structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_var_list: pointer to the FLAT_VAR_LIST structure
 *
 *	Outputs:
 *		flat_var_list: pointer to the empty FLAT_VAR_LIST structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_var_list(
FLAT_VAR_LIST  *flat_var_list)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	VAR_LIST_DEPBIN *temp_depbin;

	if (flat_var_list == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_var_list, VAR_LIST_ITYPE);
#endif

	temp_attr_avail = flat_var_list->masks.attr_avail;
	temp_dynamic = flat_var_list->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_var_list->resp_codes = 0;

	if (temp_attr_avail & VAR_LIST_MEMBERS) {
		ddl_free_members_list(&flat_var_list->members, FREE_ATTR);
	}

	if (temp_attr_avail & VAR_LIST_HELP) {
		ddl_free_string(&flat_var_list->help);
	}

	if (temp_attr_avail & VAR_LIST_LABEL) {
		ddl_free_string(&flat_var_list->label);
	}

	if (temp_attr_avail & VAR_LIST_DEBUG)
	{
		ddl_free_string(&flat_var_list->symbol_name);
	}

	flat_var_list->masks.attr_avail = 0;

	if (flat_var_list->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_var_list->depbin;

	if (temp_dynamic & VAR_LIST_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}

	if (temp_dynamic & VAR_LIST_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	flat_var_list->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_var_list, VAR_LIST_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_resp_code
 *	ShortDesc: Free the FLAT_RESP_CODE structure
 *
 *	Description:
 *		eval_clean_resp_code will check the attr_avail and dynamic
 *		flags in the FLAT_RESP_CODE structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_resp_code: pointer to the FLAT_RESP_CODE structure
 *
 *	Outputs:
 *		flat_resp_code: pointer to the empty FLAT_RESP_CODE structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_resp_code(
FLAT_RESP_CODE *flat_resp_code)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	RESP_CODE_DEPBIN *temp_depbin;

	if (flat_resp_code == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_resp_code, RESP_CODES_ITYPE);
#endif

	temp_attr_avail = flat_resp_code->masks.attr_avail;
	temp_dynamic = flat_resp_code->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & RESP_CODE_MEMBER) {
		ddl_free_resp_codes(&flat_resp_code->member, FREE_ATTR);
	}

	flat_resp_code->masks.attr_avail = 0;

	if (flat_resp_code->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_resp_code->depbin;

	if (temp_dynamic & RESP_CODE_MEMBER) {
		ddl_free_depinfo(&temp_depbin->db_member->dep, FREE_ATTR);
	}

	flat_resp_code->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_resp_code, RESP_CODES_ITYPE);
#endif

}


/*********************************************************************
 *
 *	Name: eval_clean_domain
 *	ShortDesc: Free the FLAT_DOMAIN structure
 *
 *	Description:
 *		eval_clean_domain will check the attr_avail and dynamic
 *		flags in the FLAT_DOMAIN structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_domain: pointer to the FLAT_DOMAIN structure
 *
 *	Outputs:
 *		flat_domain: pointer to the empty FLAT_DOMAIN structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_domain(
FLAT_DOMAIN    *flat_domain)
{

	ulong           temp_dynamic;

	DOMAIN_DEPBIN  *temp_depbin;

	if (flat_domain == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_domain, DOMAIN_ITYPE);
#endif

	temp_dynamic = flat_domain->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_domain->handling = 0;
	flat_domain->resp_codes = 0;

	flat_domain->masks.attr_avail = 0;

	if (flat_domain->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_domain->depbin;

	if (temp_dynamic & DOMAIN_HANDLING) {
		ddl_free_depinfo(&temp_depbin->db_handling->dep, FREE_ATTR);
	}

	flat_domain->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_domain, DOMAIN_ITYPE);
#endif

}


#ifdef HART

/*********************************************************************
 *
 *	Name: eval_clean_command
 *	ShortDesc: Free the FLAT_COMMAND structure
 *
 *	Description:
 *		eval_clean_command will check the attr_avail and dynamic
 *		flags in the FLAT_COMMAND structure to see which attributes
 *		exist and which attributes have dependancy information.
 *		Appropriate routines will be called to free the attribute
 *		structures allocated by eval, simple fields will be	set to zero.
 *
 *	Inputs:
 *		flat_command: pointer to the FLAT_COMMAND structure
 *
 *	Outputs:
 *		flat_command: pointer to the empty FLAT_COMMAND structure.
 *
 *	Returns:
 *		int
 *
 *	Author:
 *		Dave Raskin
 *
 *********************************************************************/
void
eval_clean_command(
FLAT_COMMAND   *flat_command)
{

	ulong           temp_attr_avail;
	ulong           temp_dynamic;

	COMMAND_DEPBIN *temp_depbin;

	if (flat_command == NULL) {
		return;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) flat_command, COMMAND_ITYPE);
#endif

	temp_attr_avail = flat_command->masks.attr_avail;
	temp_dynamic = flat_command->masks.dynamic;

	/*
	 * Free attribute structures
	 */

	flat_command->number = 0;
	flat_command->oper = 0;

	if (temp_attr_avail & COMMAND_TRANS) {
		ddl_free_trans_list(&flat_command->trans, FREE_ATTR);
	}

	if (temp_attr_avail & COMMAND_RESP_CODES) {
		ddl_free_resp_codes(&flat_command->resp_codes, FREE_ATTR);
	}

	if(temp_attr_avail & COMMAND_DEBUG)
	{
		ddl_free_string(&flat_command->symbol_name);
	}

	flat_command->masks.attr_avail = 0;

	if (flat_command->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_command->depbin;

	if (temp_dynamic & COMMAND_NUMBER) {
		ddl_free_depinfo(&temp_depbin->db_number->dep, FREE_ATTR);
	}

	if (temp_dynamic & COMMAND_OPER) {
		ddl_free_depinfo(&temp_depbin->db_oper->dep, FREE_ATTR);
	}

	if (temp_dynamic & COMMAND_TRANS) {
		ddl_free_depinfo(&temp_depbin->db_trans->dep, FREE_ATTR);
	}

	if (temp_dynamic & COMMAND_RESP_CODES) {
		ddl_free_depinfo(&temp_depbin->db_resp_codes->dep, FREE_ATTR);
	}

	flat_command->masks.dynamic = 0;

#ifdef DEBUG
	dds_item_flat_check((void *) flat_command, COMMAND_ITYPE);
#endif

}

#endif


void eval_clean_axis(FLAT_AXIS *flat_axis)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	AXIS_DEPBIN  *temp_depbin;

	if (flat_axis == NULL) {
		return;
	}

	temp_dynamic = flat_axis->masks.dynamic;
    temp_attr_avail = flat_axis->masks.attr_avail;

	if (temp_attr_avail & AXIS_HELP) {
		ddl_free_string(&flat_axis->help);
	}

	if (temp_attr_avail & AXIS_LABEL) {
		ddl_free_string(&flat_axis->label);
	}

	if (temp_attr_avail & AXIS_CONSTUNIT) {
		ddl_free_string(&flat_axis->constant_unit);
	}

	if (temp_attr_avail & AXIS_DEBUG)
	{
		ddl_free_string(&flat_axis->symbol_name);
	}

	flat_axis->masks.attr_avail = 0;

	if (flat_axis->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_axis->depbin;

	if (temp_dynamic & AXIS_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_CONSTUNIT) {
		ddl_free_depinfo(&temp_depbin->db_constant_unit->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_MINVAL) {
		ddl_free_depinfo(&temp_depbin->db_min_value->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_MAXVAL) {
		ddl_free_depinfo(&temp_depbin->db_max_value->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_MAX_VAL_REF) {
		ddl_free_depinfo(&temp_depbin->db_max_value->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_MIN_VAL_REF) {
		ddl_free_depinfo(&temp_depbin->db_min_value->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_SCALING) {
		ddl_free_depinfo(&temp_depbin->db_scaling->dep, FREE_ATTR);
	}
	if (temp_dynamic & AXIS_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_axis->masks.dynamic = 0;

}

void eval_clean_chart(FLAT_CHART *flat_chart)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	CHART_DEPBIN  *temp_depbin;

	if (flat_chart == NULL) {
		return;
	}

	temp_dynamic = flat_chart->masks.dynamic;
    temp_attr_avail = flat_chart->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
    if (temp_attr_avail & CHART_MEMBERS) {
		ddl_free_members_list(&flat_chart->members, FREE_ATTR);
	}

	if (temp_attr_avail & CHART_HELP) {
		ddl_free_string(&flat_chart->help);
	}

	if (temp_attr_avail & CHART_LABEL) {
		ddl_free_string(&flat_chart->label);
	}

	if (temp_attr_avail & CHART_DEBUG)
	{
		ddl_free_string(&flat_chart->symbol_name);
	}

	flat_chart->masks.attr_avail = 0;

	if (flat_chart->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_chart->depbin;

	if (temp_dynamic & CHART_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}
	if (temp_dynamic & CHART_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & CHART_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & CHART_CYCLETIME) {
		ddl_free_depinfo(&temp_depbin->db_cycle_time->dep, FREE_ATTR);
	}
	if (temp_dynamic & CHART_LENGTH) {
		ddl_free_depinfo(&temp_depbin->db_length->dep, FREE_ATTR);
	}
	if (temp_dynamic & CHART_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_chart->masks.dynamic = 0;
}

void eval_clean_file(FLAT_FILE *flat_file)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	FILE_DEPBIN  *temp_depbin;

	if (flat_file == NULL) {
		return;
	}

	temp_dynamic = flat_file->masks.dynamic;
    temp_attr_avail = flat_file->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
    if (temp_attr_avail & FILE_MEMBERS) {
		ddl_free_members_list(&flat_file->members, FREE_ATTR);
	}

	if (temp_attr_avail & FILE_HELP) {
		ddl_free_string(&flat_file->help);
	}

	if (temp_attr_avail & FILE_LABEL) {
		ddl_free_string(&flat_file->label);
	}

	if (temp_attr_avail & FILE_DEBUG)
	{
		ddl_free_string(&flat_file->symbol_name);
	}

	flat_file->masks.attr_avail = 0;

	if (flat_file->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_file->depbin;

	if (temp_dynamic & FILE_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}
	if (temp_dynamic & FILE_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & FILE_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}

	flat_file->masks.dynamic = 0;
}

void eval_clean_graph(FLAT_GRAPH *flat_graph)
{
	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	GRAPH_DEPBIN  *temp_depbin;

	if (flat_graph == NULL) {
		return;
	}

	temp_dynamic = flat_graph->masks.dynamic;
    temp_attr_avail = flat_graph->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
    if (temp_attr_avail & GRAPH_MEMBERS) {
		ddl_free_members_list(&flat_graph->members, FREE_ATTR);
	}

	if (temp_attr_avail & GRAPH_HELP) {
		ddl_free_string(&flat_graph->help);
	}

	if (temp_attr_avail & GRAPH_LABEL) {
		ddl_free_string(&flat_graph->label);
	}

	if (temp_attr_avail & GRAPH_DEBUG)
	{
		ddl_free_string(&flat_graph->symbol_name);
	}

	flat_graph->masks.attr_avail = 0;

	if (flat_graph->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_graph->depbin;

	if (temp_dynamic & GRAPH_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRAPH_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRAPH_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRAPH_XAXIS) {
		ddl_free_depinfo(&temp_depbin->db_x_axis->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRAPH_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRAPH_CYCLETIME) {
		ddl_free_depinfo(&temp_depbin->db_cycle_time->dep, FREE_ATTR);
	}

    flat_graph->masks.dynamic = 0;
}


void eval_clean_grid(FLAT_GRID *flat_grid)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	GRID_DEPBIN  *temp_depbin;

	if (flat_grid == NULL) {
		return;
	}

	temp_dynamic = flat_grid->masks.dynamic;
    temp_attr_avail = flat_grid->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
    if (temp_attr_avail & GRID_MEMBERS) {
		ddl_free_vectors_list(&flat_grid->vectors, FREE_ATTR);
	}

	if (temp_attr_avail & GRID_HELP) {
		ddl_free_string(&flat_grid->help);
	}

	if (temp_attr_avail & GRID_LABEL) {
		ddl_free_string(&flat_grid->label);
	}

	if (temp_attr_avail & GRID_DEBUG)
	{
		ddl_free_string(&flat_grid->symbol_name);
	}

	flat_grid->masks.attr_avail = 0;

	if (flat_grid->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_grid->depbin;

	if (temp_dynamic & GRID_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_vectors->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRID_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRID_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRID_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}
	if (temp_dynamic & GRID_HANDLING) {
		ddl_free_depinfo(&temp_depbin->db_handling->dep, FREE_ATTR);
	}

    flat_grid->masks.dynamic = 0;
}


void eval_clean_image(FLAT_IMAGE *flat_image)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	IMAGE_DEPBIN	*temp_depbin;

	if (flat_image == NULL) {
		return;
	}

	temp_dynamic = flat_image->masks.dynamic;
    temp_attr_avail = flat_image->masks.attr_avail;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & IMAGE_HELP) {
		ddl_free_string(&flat_image->help);
	}

	if (temp_attr_avail & IMAGE_LABEL) {
		ddl_free_string(&flat_image->label);
	}

	if (temp_attr_avail & IMAGE_DEBUG)
	{
		ddl_free_string(&flat_image->symbol_name);
	}

	flat_image->masks.attr_avail = 0;

	if (flat_image->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_image->depbin;

	if (temp_dynamic & IMAGE_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & IMAGE_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & IMAGE_PATH) {
		ddl_free_depinfo(&temp_depbin->db_path->dep, FREE_ATTR);
	}
	if (temp_dynamic & IMAGE_BINARY) {
		ddl_free_depinfo(&temp_depbin->db_path->dep, FREE_ATTR);
	}
	if (temp_dynamic & IMAGE_LINK) {
		ddl_free_depinfo(&temp_depbin->db_link->dep, FREE_ATTR);
	}
	if (temp_dynamic & IMAGE_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}
    flat_image->masks.dynamic = 0;
}

void eval_clean_list(FLAT_LIST *flat_list)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	LIST_DEPBIN  *temp_depbin;

	if (flat_list == NULL) {
		return;
	}

	temp_dynamic = flat_list->masks.dynamic;
    temp_attr_avail = flat_list->masks.attr_avail;

	/*
	 * Free attribute structures
	 */

	if (temp_attr_avail & LIST_HELP) {
		ddl_free_string(&flat_list->help);
	}

	if (temp_attr_avail & LIST_LABEL) {
		ddl_free_string(&flat_list->label);
	}

	if(temp_attr_avail & LIST_DEBUG)
	{
		ddl_free_string(&flat_list->symbol_name);
	}

	flat_list->masks.attr_avail = 0;

	if (flat_list->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	temp_depbin = flat_list->depbin;

	if (temp_dynamic & LIST_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_TYPE) {
		ddl_free_depinfo(&temp_depbin->db_type->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_CAPACITY) {
		ddl_free_depinfo(&temp_depbin->db_capacity->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_COUNT) {
		ddl_free_depinfo(&temp_depbin->db_count->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_COUNT_REF) {
		ddl_free_depinfo(&temp_depbin->db_count->dep, FREE_ATTR);
	}
	if (temp_dynamic & LIST_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_list->masks.dynamic = 0;
}

void eval_clean_source(FLAT_SOURCE *flat_source)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	SOURCE_DEPBIN  *temp_depbin;

	if (flat_source == NULL) {
		return;
	}

	temp_dynamic = flat_source->masks.dynamic;
    temp_attr_avail = flat_source->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
    if (temp_attr_avail & SOURCE_MEMBERS) {
		ddl_free_op_members_list(&flat_source->members, FREE_ATTR);
	}

	if (temp_attr_avail & SOURCE_HELP) {
		ddl_free_string(&flat_source->help);
	}

	if (temp_attr_avail & SOURCE_LABEL) {
		ddl_free_string(&flat_source->label);
	}

	if (temp_attr_avail & SOURCE_DEBUG)
	{
		ddl_free_string(&flat_source->symbol_name);
	}

	if (temp_attr_avail & SOURCE_INIT_ACTIONS) {
		ddl_free_reflist(&flat_source->init_actions, FREE_ATTR);
	}

	if (temp_attr_avail & SOURCE_RFRSH_ACTIONS) {
		ddl_free_reflist(&flat_source->refresh_actions, FREE_ATTR);
	}

	if (temp_attr_avail & SOURCE_EXIT_ACTIONS) {
		ddl_free_reflist(&flat_source->exit_actions, FREE_ATTR);
	}

	flat_source->masks.attr_avail = 0;

	if (flat_source->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_source->depbin;

	if (temp_dynamic & SOURCE_MEMBERS) {
		ddl_free_depinfo(&temp_depbin->db_members->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_EMPHASIS) {
		ddl_free_depinfo(&temp_depbin->db_emphasis->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_YAXIS) {
		ddl_free_depinfo(&temp_depbin->db_y_axis->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_LINETYPE) {
		ddl_free_depinfo(&temp_depbin->db_line_type->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_LINECOLOR) {
		ddl_free_depinfo(&temp_depbin->db_line_color->dep, FREE_ATTR);
	}
	if (temp_dynamic & SOURCE_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}
	flat_source->masks.dynamic = 0;

}

void eval_clean_waveform(FLAT_WAVEFORM *flat_waveform)
{

	ulong           temp_dynamic;
    ulong           temp_attr_avail;
	WAVEFORM_DEPBIN  *temp_depbin;

	if (flat_waveform == NULL) {
		return;
	}

	temp_dynamic = flat_waveform->masks.dynamic;
    temp_attr_avail = flat_waveform->masks.attr_avail;

	/*
	 * Free attribute structures
	 */
	if (temp_attr_avail & WAVEFORM_HELP) {
		ddl_free_string(&flat_waveform->help);
	}

	if (temp_attr_avail & WAVEFORM_LABEL) {
		ddl_free_string(&flat_waveform->label);
	}

	if (temp_attr_avail & WAVEFORM_INIT_ACTIONS) {
		ddl_free_reflist(&flat_waveform->init_actions, FREE_ATTR);
	}

	if (temp_attr_avail & WAVEFORM_RFRSH_ACTIONS) {
		ddl_free_reflist(&flat_waveform->refresh_actions, FREE_ATTR);
	}

	if (temp_attr_avail & WAVEFORM_EXIT_ACTIONS) {
		ddl_free_reflist(&flat_waveform->exit_actions, FREE_ATTR);
	}
	
    if (temp_attr_avail & WAVEFORM_X_VALUES) {
		ddl_free_dataitems(&flat_waveform->x_values, FREE_ATTR);
	}

    if (temp_attr_avail & WAVEFORM_Y_VALUES) {
		ddl_free_dataitems(&flat_waveform->y_values, FREE_ATTR);
	}

    if (temp_attr_avail & WAVEFORM_KEYPTS_X) {
		ddl_free_dataitems(&flat_waveform->key_x_values, FREE_ATTR);
	}

    if (temp_attr_avail & WAVEFORM_KEYPTS_Y) {
		ddl_free_dataitems(&flat_waveform->key_y_values, FREE_ATTR);
	}

	if (temp_attr_avail & WAVEFORM_DEBUG) {
		ddl_free_string(&flat_waveform->symbol_name);
	}


	flat_waveform->masks.attr_avail = 0;

	if (flat_waveform->depbin == NULL) {
		ASSERT_DBG(temp_dynamic == 0);
		return;
	}

	/*
	 * Free dependancy info for attributes which allow conditional
	 * expressions. If ddl_free_depinfo() is not called for a specific
	 * attribute, it is because the attribute does not allow conditional
	 * expressions.
	 */

	temp_depbin = flat_waveform->depbin;

	if (temp_dynamic & WAVEFORM_X_VALUES) {
		ddl_free_depinfo(&temp_depbin->db_x_values->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_Y_VALUES) {
		ddl_free_depinfo(&temp_depbin->db_y_values->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_KEYPTS_X) {
		ddl_free_depinfo(&temp_depbin->db_key_x_values->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_KEYPTS_Y) {
		ddl_free_depinfo(&temp_depbin->db_key_y_values->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_HELP) {
		ddl_free_depinfo(&temp_depbin->db_help->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_LABEL) {
		ddl_free_depinfo(&temp_depbin->db_label->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_EMPHASIS) {
		ddl_free_depinfo(&temp_depbin->db_emphasis->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_LINETYPE) {
		ddl_free_depinfo(&temp_depbin->db_line_type->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_LINECOLOR) {
		ddl_free_depinfo(&temp_depbin->db_line_color->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_HANDLING) {
		ddl_free_depinfo(&temp_depbin->db_handling->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_X_INITIAL) {
		ddl_free_depinfo(&temp_depbin->db_x_initial->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_X_INCREMENT) {
		ddl_free_depinfo(&temp_depbin->db_x_increment->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_POINT_COUNT) {
		ddl_free_depinfo(&temp_depbin->db_number_of_points->dep, FREE_ATTR);
	}
	if (temp_dynamic & WAVEFORM_VALID) {
		ddl_free_depinfo(&temp_depbin->db_valid->dep, FREE_ATTR);
	}

	flat_waveform->masks.dynamic = 0;

}


/*********************************************************************
 *
 *	Name: eval_clean_item
 *	ShortDesc: Clean the FLAT structure associated with a FLAT item
 *
 *	Description:
 *		eval_clean_item() is a distributor function for all
 *		"eval_clean_*" routines.
 *
 *	Inputs:
 *		item_type:		type of item to be evaluated (ie. var, ... ).
 *
 *	Outputs:
 *		item:			the cleaned FLAT structure
 *
 *	Returns:
 *		void
 *
 *	Author: steve beyerl
 *
 **********************************************************************/
void
eval_clean_item(
void           *item,
ITEM_TYPE       item_type)
{

	switch (item_type) {
	case VARIABLE_ITYPE:
		eval_clean_var((FLAT_VAR *) item);
		break;
#ifdef HART
	case COMMAND_ITYPE:
		eval_clean_command((FLAT_COMMAND *) item);
		break;
#endif
	case MENU_ITYPE:
		eval_clean_menu((FLAT_MENU *) item);
		break;
	case EDIT_DISP_ITYPE:
		eval_clean_edit_display((FLAT_EDIT_DISPLAY *) item);
		break;
	case METHOD_ITYPE:
		eval_clean_method((FLAT_METHOD *) item);
		break;
	case REFRESH_ITYPE:
		eval_clean_refresh((FLAT_REFRESH *) item);
		break;
	case UNIT_ITYPE:
		eval_clean_unit((FLAT_UNIT *) item);
		break;
	case WAO_ITYPE:
		eval_clean_wao((FLAT_WAO *) item);
		break;
	case ITEM_ARRAY_ITYPE:
		eval_clean_itemarray((FLAT_ITEM_ARRAY *) item);
		break;
	case COLLECTION_ITYPE:
		eval_clean_collection((FLAT_COLLECTION *) item);
		break;
	case BLOCK_ITYPE:
		eval_clean_block((FLAT_BLOCK *) item);
		break;
	case PROGRAM_ITYPE:
		eval_clean_program((FLAT_PROGRAM *) item);
		break;
	case RECORD_ITYPE:
		eval_clean_record((FLAT_RECORD *) item);
		break;
	case ARRAY_ITYPE:
		eval_clean_array((FLAT_ARRAY *) item);
		break;
	case VAR_LIST_ITYPE:
		eval_clean_var_list((FLAT_VAR_LIST *) item);
		break;
	case RESP_CODES_ITYPE:
		eval_clean_resp_code((FLAT_RESP_CODE *) item);
		break;
	case DOMAIN_ITYPE:
		eval_clean_domain((FLAT_DOMAIN *) item);
		break;
	case AXIS_ITYPE:
		eval_clean_axis((FLAT_AXIS *) item);
		break;
	case SOURCE_ITYPE:
		eval_clean_source((FLAT_SOURCE *) item);
		break;
	case CHART_ITYPE:
		eval_clean_chart((FLAT_CHART *) item);
		break;
	case WAVEFORM_ITYPE:
		eval_clean_waveform((FLAT_WAVEFORM *) item);
		break;
	case GRAPH_ITYPE:
		eval_clean_graph((FLAT_GRAPH *) item);
		break;
	case IMAGE_ITYPE:
		eval_clean_image((FLAT_IMAGE *) item);
		break;
	case GRID_ITYPE:
		eval_clean_grid((FLAT_GRID *) item);
		break;
	case LIST_ITYPE:
		eval_clean_list((FLAT_LIST *) item);
		break;
	case FILE_ITYPE:
		eval_clean_file((FLAT_FILE *) item);
		break;
	default:
		break;
	}
}
