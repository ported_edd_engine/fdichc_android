#include "StdAfx.h"
#include <propvarutil.h>	//for InitVariantFromBuffer()
#include "Consumer.h"
#include "nsEDDEngine\Common.h"
#include <Inf\NtcSpecies\BssProgLog.h>
#include "nsEDDEngine\Ddldefs.h"
//#include "APIs.h"


#define IS_MEMBER_ID( id_num ) ((id_num & 0x40000000) ? true : false)

namespace nsConsumer
{


	Consumer::Consumer(void)
	{
		// m_ParamCache -- it has its own constructor
		// Initialize the critical section one time only.
		InitializeCriticalSection(&m_uiBltinCriticalSection);
		m_pUICallback = nullptr;
		m_state = nullptr;
		pUIConsumer = nullptr;
	}


	Consumer::~Consumer(void)
	{
		if (pUIConsumer != nullptr)
		{
			delete pUIConsumer;
		}
		// Release resources used by the critical section object.
		DeleteCriticalSection(&m_uiBltinCriticalSection);
	}

	//
	// Comm Builtins Support
	//		Route to Communications component
	// FF
	BS_ErrorCode Consumer::ReadValue( int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec )
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"ReadValue: The value has been read from device.");
		if (((pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID) || (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI))
			&& IS_MEMBER_ID(pParamSpec->id))
		{
			return BSEC_OTHER;
		}
		return BSEC_SUCCESS;	//BLTIN_SUCCESS;
	}

	BS_ErrorCode Consumer::WriteValue( int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec )
	{
		BS_ErrorCode iRetVal = BSEC_SUCCESS;

		if (((pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID) || (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI))
			&& IS_MEMBER_ID(pParamSpec->id))
		{
			return BSEC_FAIL_COMM;
		}

		//test
		switch(pParamSpec->id)
		{
		case 0x20451:	//FF record r_ds146
			if (pParamSpec->subindex > 0)
			{
				iRetVal = BSEC_NO_DEVICE;
			}
			break;
		case 0x20452:	//FF record r_ds147
			if (pParamSpec->subindex > 0)
			{
				//FF v_vis_string64
				iRetVal = BSEC_FAIL_COMM;
			}
			break;
		}

		return iRetVal;
	}

	BS_ErrorCode Consumer::GetResponseCode( void* /*pValueSpec*/,
			INT8 *pError_Class,
			INT8 *pError_Code,
			INT16 *pAddl_code,
			ITEM_ID *err_id, SUBINDEX *err_subindex )
	{
		*pError_Class = 8;			//function block service unknown error
		*pError_Code = 0;			//function block service unknown error
		*pAddl_code = 0;			//function block service unknown error
		*err_id = 0x80020126UL;		//This is a RECORD __mode_blk just for testing purpose
		*err_subindex = 1;			//subindex is 1 based, TARGET member

		return BSEC_SUCCESS;
	}

	BS_ErrorCode Consumer::GetCommError( void* /*pValueSpec*/, unsigned long * pError )
	{
		*pError = 0x1800UL; // This is just for testing purpose.
		return BSEC_SUCCESS;
	}

	// HART
	// This routine sends specific commands to the device or monitor and
	// returns the command execution status in cmd_status[3].
	BS_ErrorCode Consumer::SendCmdTrans(void* /*pValueSpec*/, int cmd_num, int trans, unsigned char *cmd_status )
	{
		//For test purposes, we are treating the cmd_num argument as the first byte of the return status.
		//and trans as the second byte.  This way, we can simulate any response that a device might give.
		
		BS_ErrorCode returnedVal = BSEC_SUCCESS;

		cmd_status[0] = 0x00;//one byte integer: command response code
		cmd_status[1] = 0x00;//bit enumeration: communication error
		cmd_status[2] = 0x00;//bit enumeration: field device status
		
		if (cmd_num == 0x1FF)//Special cmd_num to return a BSEC_NO_DEVICE condition
		{
			returnedVal = BSEC_NO_DEVICE;
		}
		else if (cmd_num == 134)//Special cmd_num to match existing tests for HART
		{
			cmd_status[2] = 0x10;// Field device status -- 0x10 == DS_MORESTATUSAVAIL -- command 48 is required
		}
		else if (cmd_num == 135)//Special cmd_num to match existing tests for HART
		{
			cmd_status[2] = 0x80;// Field device status -- 0x80 == DS_DEVMALFUNCTION -- command 48 is required based on HCF_SPEC-127
		}
		else if (cmd_num > 0xFF)//Special cmd_num to return a BSEC_OTHER condition
		{
			returnedVal = BSEC_OTHER;
		}
		else if (cmd_num & 0x80)//If the high bit is set, this is considered a communication error.
		{
			cmd_status[1] = (unsigned char)cmd_num; //Communication error
		}
		else					//High bit is not set, fill in response and device status bytes.
		{
			cmd_status[0] = (unsigned char)cmd_num; //Response code 
			cmd_status[2] = (unsigned char)trans;	//Device Status
		}
		return returnedVal;
	}

	//  Maximum of more_data_info_size is 64.
	BS_ErrorCode Consumer::GetMoreStatus(void* /*pValueSpec*/, unsigned char *more_data_status, unsigned char *more_data_info, int* more_data_info_size )	// Sends Cmd 48
	{
		BS_ErrorCode returnedVal = BSEC_SUCCESS;

		more_data_status[0] = 0x00;	//one byte integer: command response code
		more_data_status[1] = 0x00;	//bit enumeration: communication error
		more_data_status[2] = 0x00;	//bit enumeration: field device status

		more_data_info[0] = 0x00;	//here set for test purpose
		more_data_info[1] = 0x10;	//test in method_XMTR_ABORT_ON_DATA
		more_data_info[2] = 0x00;
		more_data_info[3] = 0x00;
		*more_data_info_size = 4;

		return returnedVal;
	}

		
	// PB
	// PB SendCommand() returns only BSEC_SUCCESS, BSEC_ABORTED, and BSEC_NO_DEVICE.
	BS_ErrorCode Consumer::SendCommand( void* /*pValueSpec*/, ITEM_ID command_id, long *pErrorValue)
	{
		*pErrorValue = 0;
        BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SendCommand: commandId = %d, command error = %i.", command_id, *pErrorValue);
		switch (command_id)
		{
		case 0x45:	//PB testCommand2
			return BSEC_NO_DEVICE;		//testing no device mask
			break;
		default:
			return BSEC_SUCCESS;
			break;
		}
	}


	int Consumer::isOffline( void* /*pValueSpec*/)
	{
        BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"isOffline: Online.");
		return 0;
	}


	//
	// Debug Builtins Support
	//int Consumer::_BPSupport( char* /*MethodName*/, char* /*FileName*/, unsigned int /*LineNumber*/, /* inout */ LOCAL_VARIABLE_LIST* /*LocalVariableList*/ )
	//{
	//	return 0;
	//}

	void Consumer::LogMessage( void* /*pValueSpec*/, int iPriority, wchar_t *message) // used for _ERROR, _WARNING, _TRACE and LOG_MESSAGE
	{
        BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"LogMessage: iPriority = %i, message = %ls.", iPriority, message);
	}

	void Consumer::OnMethodDebugInfo(int /*iBlockInstance*/, void* /*pValueSpec*/, unsigned long /*lineNumber*/, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** /*modifiedMethodDebugXml*/)
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"MethodDebugInfo: message = %ls.", currentMethodDebugXml);
	}

	//
	// List Cache Builtins Support
    PC_ErrorCode Consumer::GetListElement( int iBlockInstance, ITEM_ID list_id, int index, ITEM_ID element_id, ITEM_ID member_id, EVAL_VAR_VALUE * pValue )
    {
		PC_ErrorCode ec = PC_SUCCESS_EC;

		//find if the parameter has been set before
		ULONGLONG key = (((ULONGLONG)list_id << 32) | index);
		PARAMETER_CACHE::iterator valueIterator = m_ParamCache.find(key);
		if (valueIterator != m_ParamCache.end())
		{
			//yes, read it
			*pValue = valueIterator->second;
			return ec;
		}

        switch (list_id)
        {
        case 131959: // FF- fl_input_var_list.fl_input_var: local class
            pValue->type = nsEDDEngine::VT_FLOAT;
			pValue->val.f = (float)3.55;
			pValue->size = 4;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %f.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.f);
            break;
        case 132225: // FF- dl_input_var_list.dl_input_var: local class
		    pValue->type = nsEDDEngine::VT_DOUBLE;
			pValue->val.d = (double)5.55;
			pValue->size = 8;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %d.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.d);
            break;
        case 132226: // FF- i_input_var_list.i_input_var: local class
			pValue->type = nsEDDEngine::VT_INTEGER;
			pValue->val.i = (long)75;
			pValue->size = 4;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %i.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.i);
            break;
        case 132227: // FF- ui_input_var_list.ui_input_var: local class
            pValue->type = nsEDDEngine::VT_UNSIGNED;
            pValue->val.u = (unsigned long)75;
            pValue->size = 4;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %u.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.u);
            break;
        case 132228: // FF- ascii_string32_input_var_list
            pValue->type = nsEDDEngine::VT_ASCII;
			pValue->val.s.assign(L"Imhere",false);;
			pValue->size = (unsigned short)pValue->val.s.length();
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %s.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.s.c_str());
            break;
        case 132229: // FF- date_input_var_list
            pValue->type = nsEDDEngine::VT_EDD_DATE;
            pValue->val.i = 0x0c0570;	//date: 6/12/2012
            pValue->size = 4;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %s.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.s.c_str());
            break;
		case 16700: // HART- xList2.menu_display_test_float_var1 float: local class
            pValue->type = nsEDDEngine::VT_FLOAT;
			pValue->val.f = (float)6.3;
			pValue->size = 4;
            BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %f.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.f);
            break;
		default:
			{
				nsEDDEngine::FDI_PARAM_SPECIFIER stdParamSpec;
				stdParamSpec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
				stdParamSpec.id = element_id;
				stdParamSpec.subindex = 0;			//assuming this is a simple variable

				nsEDDEngine::TYPE_SIZE TypeSize;
				int nRetVal = pEDDEngineInstance->GetParamType(iBlockInstance, &stdParamSpec, &TypeSize);
	
				if (0 == nRetVal)
				{
					switch(TypeSize.type)
					{
					case nsEDDEngine::VT_INTEGER:
						pValue->type = nsEDDEngine::VT_INTEGER;
						pValue->size = 1;
						pValue->val.i = (long)-75;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %i.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.i);
						break;

 					case nsEDDEngine::VT_UNSIGNED:
						pValue->type = nsEDDEngine::VT_UNSIGNED;
						pValue->val.u = (unsigned long)75;
						pValue->size = 4;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %u.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.u);
						break;

					case nsEDDEngine::VT_TIME_VALUE:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned long)0xFFFF;
						pValue->size = 4;
						break;

					case nsEDDEngine::VT_ENUMERATED:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned char)0x01;
						pValue->size = 1;
						break;
					case nsEDDEngine::VT_BIT_ENUMERATED:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned char)0x84;
						pValue->size = 1;
						break;

					case nsEDDEngine::VT_FLOAT:
						pValue->type = nsEDDEngine::VT_FLOAT;
						pValue->val.f = (float)3.55;
						pValue->size = 4;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %f.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.f);
						break;

					case nsEDDEngine::VT_DOUBLE:
						pValue->type = nsEDDEngine::VT_DOUBLE;
						pValue->val.d = (double)5.55;
						pValue->size = 8;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %d.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.d);
						break;

					case nsEDDEngine::VT_ASCII:
					case nsEDDEngine::VT_VISIBLESTRING:
						pValue->type = TypeSize.type;
						pValue->val.s.assign(L"Imhere",false);;
						pValue->size = (unsigned short)pValue->val.s.length();
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %s.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.s.c_str());
						break;

					case nsEDDEngine::VT_PACKED_ASCII:
						pValue->type = TypeSize.type;
						pValue->val.s.assign(L"PACKED_ASCII",false);
						pValue->size = 26;
						break;

					case nsEDDEngine::VT_PASSWORD:
						pValue->type = TypeSize.type;
						pValue->val.s.assign(L"password1",false);
						pValue->size = 18;
						break;
					case nsEDDEngine::VT_BITSTRING:
					case nsEDDEngine::VT_OCTETSTRING:
						{
							pValue->type = TypeSize.type;
							uchar usConst[] = {0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40};
							pValue->val.b.assign(usConst, 7);
							pValue->size = (short)pValue->val.b.length();
						}
						break;
					case nsEDDEngine::VT_EUC:
						pValue->type = TypeSize.type;
						pValue->val.s.assign(L"eeeeeeee",false);;
						pValue->size = (short)pValue->val.s.length();
						break;
					case nsEDDEngine::VT_INDEX:
						pValue->type = TypeSize.type;
						pValue->val.u = (long)1;
						pValue->size = 4;
						break;

					case nsEDDEngine::VT_DURATION:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned char)0xcc;
						pValue->size = 8;
						break;
					case nsEDDEngine::VT_TIME:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned char)0xcc;
						pValue->size = 8;
						break;

					case nsEDDEngine::VT_EDD_DATE:
						pValue->type = nsEDDEngine::VT_EDD_DATE;
						pValue->val.i = 0x0c0570;	//date: 6/12/2012
						pValue->size = 4;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %s.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.s.c_str());
						break;

					case nsEDDEngine::VT_DATE_AND_TIME:
						pValue->type = TypeSize.type;
						pValue->val.u = (unsigned long long)0x010275;
						pValue->size = 8;
						BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement: list = %i, list index = %i, element id = %i, member id = %i and item = %u.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.u);
						break;

					default:
						BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"GetListElement: Value for list Item ID %i is unknown at this time.", list_id);
						ec = PC_OTHER_EC;
						break;
					}
				}
				else
				{
					BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"GetListElement: Value for list element Item ID %i probably is defined as local class.", element_id);
					ec = PC_OTHER_EC;
				}
			}
			break;
        }

		if (ec == PC_SUCCESS_EC)
		{
			m_ParamCache[key] = *pValue;
		}

		return ec;
	}

	PC_ErrorCode Consumer::GetListElement2( int iBlockInstance, 
											ITEM_ID list_id, int index, 
											ITEM_ID /*embedded_list_id*/, int /*embedded_list_index*/,
											ITEM_ID element_id, ITEM_ID member_id, 
											ITEM_ID var_item_id, ITEM_ID /*var_member_id*/, 
											EVAL_VAR_VALUE * pValue )
	{
		PC_ErrorCode ec = PC_SUCCESS_EC;

		//find if the parameter has been set before
		ULONGLONG key = (((ULONGLONG)list_id << 32) | index);
		PARAMETER_CACHE::iterator valueIterator = m_ParamCache.find(key);
		if (valueIterator != m_ParamCache.end())
		{
			//yes, read it
			*pValue = valueIterator->second;
			return ec;
		}

		nsEDDEngine::FDI_PARAM_SPECIFIER stdParamSpec;
		stdParamSpec.eType = nsEDDEngine::FDI_PS_ITEM_ID;
		stdParamSpec.id = var_item_id;
		stdParamSpec.subindex = 0;		//assuming this is a simple variable

		nsEDDEngine::TYPE_SIZE TypeSize;
		int nRetVal = pEDDEngineInstance->GetParamType(iBlockInstance, &stdParamSpec, &TypeSize);
	
		if (0 == nRetVal)
		{
			switch(TypeSize.type)
			{
			case nsEDDEngine::VT_INTEGER:
				pValue->type = nsEDDEngine::VT_INTEGER;
				pValue->size = 1;
				pValue->val.i = (long)-75;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %i.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.i);
				break;

 			case nsEDDEngine::VT_UNSIGNED:
				pValue->type = nsEDDEngine::VT_UNSIGNED;
				pValue->val.u = (unsigned long)75;
				pValue->size = 4;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %u.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.u);
				break;

			case nsEDDEngine::VT_TIME_VALUE:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned long)0xFFFF;
				pValue->size = 4;
				break;

			case nsEDDEngine::VT_ENUMERATED:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned char)0x01;
				pValue->size = 1;
				break;
			case nsEDDEngine::VT_BIT_ENUMERATED:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned char)0x84;
				pValue->size = 1;
				break;

			case nsEDDEngine::VT_FLOAT:
				pValue->type = nsEDDEngine::VT_FLOAT;
				pValue->val.f = (float)3.55;
				pValue->size = 4;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %f.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.f);
				break;

			case nsEDDEngine::VT_DOUBLE:
				pValue->type = nsEDDEngine::VT_DOUBLE;
				pValue->val.d = (double)5.55;
				pValue->size = 8;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %d.", (int)list_id, index, (int) element_id, (int) member_id, (int)pValue->val.d);
				break;

			case nsEDDEngine::VT_ASCII:
			case nsEDDEngine::VT_VISIBLESTRING:
				pValue->type = TypeSize.type;
				pValue->val.s.assign(L"Imhere",false);;
				pValue->size = (unsigned short)pValue->val.s.length();
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %s.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.s.c_str());
				break;

			case nsEDDEngine::VT_PACKED_ASCII:
				pValue->type = TypeSize.type;
				pValue->val.s.assign(L"PACKED_ASCII",false);
				pValue->size = 26;
				break;

			case nsEDDEngine::VT_PASSWORD:
				pValue->type = TypeSize.type;
				pValue->val.s.assign(L"password1",false);
				pValue->size = 18;
				break;
			case nsEDDEngine::VT_BITSTRING:
			case nsEDDEngine::VT_OCTETSTRING:
				{
					pValue->type = TypeSize.type;
					uchar usConst[] = {0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40};
					pValue->val.b.assign(usConst, 7);
					pValue->size = (short)pValue->val.b.length();
				}
				break;
			case nsEDDEngine::VT_EUC:
				pValue->type = TypeSize.type;
				pValue->val.s.assign(L"eeeeeeee",false);;
				pValue->size = (short)pValue->val.s.length();
				break;
			case nsEDDEngine::VT_INDEX:
				pValue->type = TypeSize.type;
				pValue->val.u = (long)1;
				pValue->size = 4;
				break;

			case nsEDDEngine::VT_DURATION:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned char)0xcc;
				pValue->size = 8;
				break;
			case nsEDDEngine::VT_TIME:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned char)0xcc;
				pValue->size = 8;
				break;

			case nsEDDEngine::VT_EDD_DATE:
				pValue->type = nsEDDEngine::VT_EDD_DATE;
				pValue->val.i = 0x0c0570;	//date: 6/12/2012
				pValue->size = 4;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %i.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.i);
				break;

			case nsEDDEngine::VT_DATE_AND_TIME:
				pValue->type = TypeSize.type;
				pValue->val.u = (unsigned long long)0x010275;
				pValue->size = 8;
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetListElement2: list = %i, list index = %i, element id = %i, member id = %i and item = %u.", (int)list_id, index, (int) element_id, (int) member_id, pValue->val.u);
				break;

			default:
				BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"GetListElement2: Value for list Item ID %i is unknown at this time.", list_id);
				ec = PC_OTHER_EC;
				break;
			}
		}
		else
		{
			BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"GetListElement2: Value for list element Item ID %i probably is defined as local class.", element_id);
			ec = PC_OTHER_EC;
		}

		if (ec == PC_SUCCESS_EC)
		{
			m_ParamCache[key] = *pValue;
		}

		return PC_SUCCESS_EC;
	}

	BS_ErrorCode Consumer:: ListDeleteElementAt( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int start_index, int /*delete_count*/ )
	{
		BS_ErrorCode retValue = BSEC_OTHER;
		VARIANT vtValue;
		VariantClear(&vtValue);

		//decrease count
		EVAL_VAR_VALUE evvValue;
		nsEDDEngine::AttributeName attrKey = nsEDDEngine::AttributeName::count;
		ATTR_VALUE::iterator attrIterator = m_AttrValueList.find(attrKey);
		if (attrIterator != m_AttrValueList.end())
		{
			//found, read it
			evvValue = attrIterator->second;

			if (evvValue.val.u > 0)
			{
				//decreased by 1
				evvValue.val.u --;
				retValue = BSEC_SUCCESS;
			}
		}
		else
		{
			//not found. read it from DD or initialize it with zero
			evvValue.type = nsEDDEngine::VT_UNSIGNED;
			evvValue.size = 4;

			retValue = GetListElementCount(iBlockInstance, pValueSpec, pListOpRef, &vtValue);
			if (retValue == BSEC_SUCCESS)
			{
				if (V_UI4(&vtValue) > 0)
				{
					evvValue.val.u = V_UI4(&vtValue) - 1;
				}
				else
				{
					retValue = BSEC_OTHER;
				}
			}
		}

		if ( retValue == BSEC_SUCCESS )
		{
			//if the count is greater than or equal to 0, do it
			(void)SetDynamicAttribute(iBlockInstance, pValueSpec, pListOpRef, nsEDDEngine::AttributeName::count, &evvValue);
		}

		//erase list element value
		ITEM_ID ulListId = 0;
		if (pListOpRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			ulListId = pListOpRef->op_info.id;
		}
		else
		{
			ulListId = pListOpRef->op_info_list.list[pListOpRef->op_info_list.count - 1].id;
		}
		ULONGLONG listKey = (((ULONGLONG)ulListId << 32) | start_index);
		PARAMETER_CACHE::iterator valueIterator = m_ParamCache.find(listKey);
		if (valueIterator != m_ParamCache.end())
		{
			//yes, erase the list element value
			m_ParamCache.erase(listKey);
		}

		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"ListDeleteElementAt: list = %i and list index = %i.", pListOpRef->op_info.id, start_index);
		return retValue;
	}

	BS_ErrorCode Consumer::ListInsert( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef )
	{
		BS_ErrorCode retValue = BSEC_OTHER;
		VARIANT vtValue;
		VariantClear(&vtValue);

		//increase count
		EVAL_VAR_VALUE evvValue;
		nsEDDEngine::AttributeName attrKey = nsEDDEngine::AttributeName::count;
		ATTR_VALUE::iterator attrIterator = m_AttrValueList.find(attrKey);
		if (attrIterator != m_AttrValueList.end())
		{
			//found, read it
			evvValue = attrIterator->second;
			//increased by 1
			evvValue.val.u ++;
		}
		else
		{
			//not found. read it from DD or initialize it with zero
			evvValue.type = nsEDDEngine::VT_UNSIGNED;
			evvValue.size = 4;

			retValue = GetListElementCount(iBlockInstance, pValueSpec, pListOpRef, &vtValue);
			if (retValue == BSEC_SUCCESS)
			{
				evvValue.val.u = V_UI4(&vtValue) + 1;
			}
			else
			{
				evvValue.val.u = 1;
			}
		}

		//get list CAPACITY
		retValue = GetListCapacity(iBlockInstance, pValueSpec, pListOpRef, &vtValue);

		if ( (retValue == BSEC_SUCCESS) && ((V_UI4(&vtValue) == 0) || (evvValue.val.u <= V_UI4(&vtValue))) )
		{
			//if no capacity is specified or the count is less than or equal to capacity, do it
			(void)SetDynamicAttribute(iBlockInstance, pValueSpec, pListOpRef, nsEDDEngine::AttributeName::count, &evvValue);
		}
		else
		{
			retValue = BSEC_OTHER;
		}

		//save list element value
		ITEM_ID ulListId = 0;
		if (pListOpRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			ulListId = pListOpRef->op_info.id;
		}
		else
		{
			ulListId = pListOpRef->op_info_list.list[pListOpRef->op_info_list.count - 1].id;
		}
		ULONGLONG listElemKey = (((ULONGLONG)ulListId << 32) | insert_index);

		ITEM_ID ulValueId = 0;
		if (pInsertedOpRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			ulValueId = pInsertedOpRef->op_info.id;
		}
		else
		{
			ulValueId = pInsertedOpRef->op_info_list.list[pInsertedOpRef->op_info_list.count - 1].id;
		}

		//looking for saved value in parameter cache
		for (PARAMETER_CACHE::iterator valueIterator = m_ParamCache.begin(); valueIterator != m_ParamCache.end(); valueIterator++)
		{
			ULONGLONG oldKey = valueIterator->first;
			if (ulValueId == (oldKey >> 32))
			{
				//yes, get inserted value. save it as list element value
				m_ParamCache[listElemKey] = valueIterator->second;
				break;
			}
		}

		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"ListInsert: list = %i, list index = %i and item = %i.", pListOpRef->op_info.id, insert_index, pInsertedOpRef->op_info.id);
		return retValue;
	}

	//get list capacity
	BS_ErrorCode Consumer::GetListCapacity( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, VARIANT* pvtCapacity )
	{
		nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_LIST);
		nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::capacity;
		nsEDDEngine::FDI_ITEM_SPECIFIER ItemSpec = {nsEDDEngine::FDI_ITEM_ID, 0, 0};
		if (pListOpRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			ItemSpec.item.id = pListOpRef->op_info.id;
		}
		else
		{
			ItemSpec.item.id = pListOpRef->op_info_list.list[pListOpRef->op_info_list.count - 1].id;
		}

		int nRetVal = pEDDEngineInstance->GetItem(iBlockInstance, pValueSpec, &ItemSpec, &attribute_list, L"|en|", &generic_item);
		if ((nRetVal == 0) && generic_item.item)
		{
			nsEDDEngine::FLAT_LIST *flat_list = dynamic_cast<nsEDDEngine::FLAT_LIST *>(generic_item.item);
			V_VT(pvtCapacity) = VT_UI4;
			V_UI4(pvtCapacity) = flat_list->capacity;
			return BSEC_SUCCESS;
		}
		else
		{
			return BSEC_OTHER;
		}
	}

	//get number of list elements
	BS_ErrorCode Consumer::GetListElementCount( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, VARIANT* pvtCount )
	{
		nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_LIST);
		nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::count;
		nsEDDEngine::FDI_ITEM_SPECIFIER ItemSpec = {nsEDDEngine::FDI_ITEM_ID, 0, 0};
		if (pListOpRef->op_ref_type == nsEDDEngine::STANDARD_TYPE)
		{
			ItemSpec.item.id = pListOpRef->op_info.id;
		}
		else
		{
			ItemSpec.item.id = pListOpRef->op_info_list.list[pListOpRef->op_info_list.count - 1].id;
		}

		int nRetVal = pEDDEngineInstance->GetItem(iBlockInstance, pValueSpec, &ItemSpec, &attribute_list, L"|en|", &generic_item);
		if ((nRetVal == 0) && generic_item.item)
		{
			nsEDDEngine::FLAT_LIST *flat_list = dynamic_cast<nsEDDEngine::FLAT_LIST *>(generic_item.item);
			V_VT(pvtCount) = VT_UI4;
			V_UI4(pvtCount) = flat_list->count;
			return BSEC_SUCCESS;
		}
		else
		{
			return BSEC_OTHER;
		}
	}

	//This is a temporary function to solve complex reference while DDS doesn't recognize complex reference, collection, file data types for now
	static void getStdParamSpecPointer(nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec, nsEDDEngine::FDI_PARAM_SPECIFIER* pDestParamSpec)
	{
		ITEM_ID idPrimaryID = 0;	// Find out what the primary Item Id is
		ITEM_ID idPrimaryMemberID = 0;	// Find out what the primary Member Id is

		if (   (pSrcParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID)
			|| (pSrcParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI) )
		{
			idPrimaryID = pSrcParamSpec->id;
			idPrimaryMemberID = pSrcParamSpec->subindex;

			pDestParamSpec->eType = pSrcParamSpec->eType;
			pDestParamSpec->id = idPrimaryID;
			pDestParamSpec->subindex = idPrimaryMemberID;
		}
		else if (pSrcParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX)
		{
			if  ( (pSrcParamSpec->RefList.count > 1)
				  && ( (pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 2].type == nsEDDEngine::ITYPE_ARRAY)		//good for array variable as parameter in method with parameter
			           || (pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 2].type == nsEDDEngine::ITYPE_LIST) )
				)
			{
				//for value array or list item, return array or list IDs instead of simple variable ID
				idPrimaryID = pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 2].id;
				idPrimaryMemberID = pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 2].member;

				pDestParamSpec->eType = nsEDDEngine::FDI_PS_ITEM_ID_SI;
			}
			else
			{
				idPrimaryID = pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 1].id;
				idPrimaryMemberID = pSrcParamSpec->RefList.list[pSrcParamSpec->RefList.count - 1].member;	//shall be zero
				pDestParamSpec->eType = nsEDDEngine::FDI_PS_ITEM_ID;
			}
			pDestParamSpec->id = idPrimaryID;
			pDestParamSpec->subindex = idPrimaryMemberID;
		}

		return;
	}

	//
	// Param Cache Builtins Support
	//		Route to Param Cache
	
	unsigned long Consumer::GetDefaultIndex(int iBlockInstance, void * pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec)
	{
	
		nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_VARIABLE);
		nsEDDEngine::FDI_ITEM_SPECIFIER item_spec = {nsEDDEngine::FDI_ITEM_ID, pParamSpec->id, 0};
		nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::indexed;

		int r_code = pEDDEngineInstance->GetItem( iBlockInstance, pValueSpec, &item_spec, &attribute_list, L"|en|", &generic_item);
		if (r_code == 0)
		{
			nsEDDEngine::FLAT_VAR *pFlatVar = dynamic_cast<nsEDDEngine::FLAT_VAR*>(generic_item.item);

			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec2 = {nsEDDEngine::FDI_ITEM_ID, pFlatVar->indexed, 0};

			nsEDDEngine::ITEM_TYPE ItemType = nsEDDEngine::ITYPE_NO_VALUE;
			pEDDEngineInstance->GetItemType(iBlockInstance, &item_spec2, &ItemType); // Get the type of the thing the index is on

			if (ItemType == nsEDDEngine::ITYPE_ITEM_ARRAY)
			{
				nsEDDEngine::FDI_GENERIC_ITEM generic_item2(nsEDDEngine::ITYPE_ITEM_ARRAY);
				nsEDDEngine::AttributeNameSet attribute_list2 = nsEDDEngine::elements;

				r_code = pEDDEngineInstance->GetItem( iBlockInstance, pValueSpec, &item_spec2, &attribute_list2, L"|en|", &generic_item2);
				if (r_code == 0)
				{
					nsEDDEngine::FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<nsEDDEngine::FLAT_ITEM_ARRAY *>(generic_item2.item);
					if (flat_item_arry != NULL)	
					{
						return flat_item_arry->elements.list[0].index;	// good return
					}
				}
			}
		}
		return 0;	// if not Item Array
	}

	PC_ErrorCode Consumer::GetParamValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in/out */ EVAL_VAR_VALUE *pValue )
	{
		PC_ErrorCode ec = PC_SUCCESS_EC;
		int nRetVal = 0;

		//check if item is list first. If the answer is yes, call a different function to process the list item.
		nsEDDEngine::ITEM_TYPE ItemType = nsEDDEngine::ITYPE_NO_VALUE;
		if ( pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX )
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER ItemSpec = {nsEDDEngine::FDI_ITEM_ID, pParamSpec->RefList.list[0].id, 0};
			nRetVal = pEDDEngineInstance->GetItemType(iBlockInstance, &ItemSpec, &ItemType);
		}
		else if ( (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID) || (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI) )
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER ItemSpec = {nsEDDEngine::FDI_ITEM_ID, pParamSpec->id, 0};
			nRetVal = pEDDEngineInstance->GetItemType(iBlockInstance, &ItemSpec, &ItemType);
		}
		if ( nRetVal != 0 )
		{
			return PC_OTHER_EC;
		}

		if (ItemType == nsEDDEngine::ITYPE_LIST)	// If this is a list process them elsewhere
		{
			switch(pParamSpec->RefList.count)
			{
			case 0:
			{
				ec = GetListElement	( iBlockInstance,
										pParamSpec->id, pParamSpec->subindex, 
										0, 0,
										pValue);
			}
			break;
			case 1:
			{
				ec = GetListElement	( iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member, 
										0, 0,
										pValue);
			}
			break;
			case 2:
			{
				ec = GetListElement	( iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member, 
										pParamSpec->RefList.list[1].id, pParamSpec->RefList.list[1].member,
										pValue);
			}
			break;
			case 3:
			{
				ec = GetListElement2 (	iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member,
										0,	0,
										pParamSpec->RefList.list[1].id, pParamSpec->RefList.list[1].member,
										pParamSpec->RefList.list[2].id, pParamSpec->RefList.list[2].member,
										pValue );
			}
			break;
			case 4:
			{
				//assuming count is 4 for test purpose only
				ec = GetListElement2 (	iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member,
										pParamSpec->RefList.list[1].id, pParamSpec->RefList.list[1].member,
										pParamSpec->RefList.list[2].id, pParamSpec->RefList.list[2].member,
										pParamSpec->RefList.list[3].id, pParamSpec->RefList.list[3].member,
										pValue );
			}
			break;
			case 5:
			{
				//assuming count is 5 for test purpose only
				ec = GetListElement2 (	iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member,
										pParamSpec->RefList.list[1].id, pParamSpec->RefList.list[1].member,
										pParamSpec->RefList.list[3].id, pParamSpec->RefList.list[3].member,
										pParamSpec->RefList.list[4].id, pParamSpec->RefList.list[4].member,
										pValue );
			}
			break;
			case 6:
			{
				//assuming count is 6 for test purpose only
				ec = GetListElement2 (	iBlockInstance,
										pParamSpec->RefList.list[0].id, pParamSpec->RefList.list[0].member,
										pParamSpec->RefList.list[1].id, pParamSpec->RefList.list[1].member,
										pParamSpec->RefList.list[4].id, pParamSpec->RefList.list[4].member,
										pParamSpec->RefList.list[5].id, pParamSpec->RefList.list[5].member,
										pValue );
			}
			break;
			default:
				ec = PC_OTHER_EC;
			break;
			}

			return ec;
		}


		nsEDDEngine::FDI_PARAM_SPECIFIER stdParamSpec;
		getStdParamSpecPointer(pParamSpec, &stdParamSpec);

		ITEM_ID idPrimaryID = stdParamSpec.id;				// Find out what the primary Item Id is
		ITEM_ID idPrimaryMemberID = stdParamSpec.subindex;	// Find out what the primary Member Id is

		//find if the parameter has been set before
		ULONGLONG key = (((ULONGLONG)idPrimaryID << 32) | idPrimaryMemberID);
		PARAMETER_CACHE::iterator valueIterator = m_ParamCache.find(key);
		if (valueIterator != m_ParamCache.end())
		{
			//yes, read it
			*pValue = valueIterator->second;
			return ec;
		}

		//The parameter hasn't been set before. Set it now.
		switch(idPrimaryID)
		{
		case 16823:	//legacy HART: input_float
			nRetVal = -1;		//equivalent to REFRESH_ACTION abort
			break;
		case 16432:	//method_local_date2
			pValue->type = nsEDDEngine::VT_EDD_DATE;
			pValue->val.i = (long)0x1c0358;
			pValue->size = 4;		//<= legacy HART date special
			break;

		default:
			{
				nsEDDEngine::TYPE_SIZE TypeSize;
				nRetVal = pEDDEngineInstance->GetParamType(iBlockInstance, &stdParamSpec, &TypeSize);
	
				switch(TypeSize.type)
				{
				case nsEDDEngine::VT_INTEGER:
					pValue->type = TypeSize.type;
					if (TypeSize.size < 4)
					{
						pValue->val.i = (long)50;
						pValue->size = 1;
					}
					else if (TypeSize.size == 4)
					{
						pValue->val.i = (long)75;
						pValue->size = 4;
					}
					else
					{
						pValue->val.i = (long long)0x7FFFFFFFFFFFFFFF;
						pValue->size = 8;
					}
					break;
				case nsEDDEngine::VT_UNSIGNED:
					pValue->type = TypeSize.type;
					if (TypeSize.size == 1)
					{
						pValue->val.u = (unsigned char)0xcc;
						pValue->size = 1;
					}
					else if (TypeSize.size == 2)
					{
						pValue->val.u = (unsigned long)0xcccc;
						pValue->size = 2;
					}
					else
					{
						pValue->val.u = (unsigned long)0x7ccccccc;
						pValue->size = 4;			
					}
					break;
				case nsEDDEngine::VT_DURATION:
					pValue->type = TypeSize.type;
					pValue->val.u = (unsigned char)0xcc;
					pValue->size = 8;
					break;
				case nsEDDEngine::VT_TIME:
					pValue->type = TypeSize.type;
					pValue->val.u = (unsigned char)0xcc;
					pValue->size = 8;
					break;
				case nsEDDEngine::VT_FLOAT:
					pValue->type = TypeSize.type;
					pValue->val.f = (float)3.55;
					pValue->size = 4;
					break;
				case nsEDDEngine::VT_DOUBLE:
					pValue->type = TypeSize.type;
					pValue->val.d = (double)8.8;
					pValue->size = 8;			
					break;
				case nsEDDEngine::VT_EDD_DATE:	//HART date and method_local_date
					pValue->type = TypeSize.type;
					pValue->val.i = (long)0x1d035a;
					pValue->size = 4;
					break;
				case nsEDDEngine::VT_DATE_AND_TIME:	//FF a_date and v_date
					pValue->type = TypeSize.type;
					pValue->val.u = (unsigned long long)0x010275;
					pValue->size = 8;
					break;
				case nsEDDEngine::VT_TIME_VALUE:
					pValue->type = TypeSize.type;
					if (TypeSize.size <= 4)
					{
						pValue->val.u = (unsigned long)0xFFFF;
						pValue->size = 4;
					}
					else
					{
						pValue->val.i = (long long)0xFFFF;
						pValue->size = 8;
					}
					break;
				case nsEDDEngine::VT_ENUMERATED:
					//pValue->type = TypeSize.type;
					//pValue->val.u = (unsigned char)0x01;
					//pValue->size = 1;
					//break;
				case nsEDDEngine::VT_BIT_ENUMERATED:
					pValue->type = TypeSize.type;
					//pValue->val.u = (unsigned char)0x84;
					pValue->size = 1;
					//break;

					{
						// Set up the call to get the enumeration list from this variable
						nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_VARIABLE);
						nsEDDEngine::FDI_ITEM_SPECIFIER item_spec = {nsEDDEngine::FDI_ITEM_ID, pParamSpec->id, 0};

						if (pParamSpec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI)
						{
							item_spec.eType = nsEDDEngine::FDI_ITEM_ID_SI;
							item_spec.subindex = pParamSpec->subindex;
						}

						nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::enumerations;
						nRetVal = pEDDEngineInstance->GetItem(iBlockInstance, pValueSpec, &item_spec, &attribute_list, _T("en"), &generic_item);

						pValue->val.u = 0; // just default it to zero
						if (nRetVal == 0)
						{
							nsEDDEngine::FLAT_VAR *flat_var = dynamic_cast<nsEDDEngine::FLAT_VAR*>(generic_item.item);
							if (flat_var != NULL)
							{
								if (flat_var->enums.count > 0)
								{
									pValue->val.u = flat_var->enums.list[0].val.val.u;		// Get the first value in the list.
								}
							}
						}
					}
					break;
				case nsEDDEngine::VT_ASCII:
				case nsEDDEngine::VT_VISIBLESTRING:
					pValue->type = TypeSize.type;
					pValue->val.s.assign(L"Imhere",false);
					pValue->size = 14;
					break;
				case nsEDDEngine::VT_PACKED_ASCII:
					pValue->type = TypeSize.type;
					pValue->val.s.assign(L"PACKED_ASCII",false);
					pValue->size = 26;
					break;
				case nsEDDEngine::VT_PASSWORD:
					pValue->type = TypeSize.type;
					pValue->val.s.assign(L"password1",false);
					pValue->size = 18;
					break;
				case nsEDDEngine::VT_BITSTRING:
				case nsEDDEngine::VT_OCTETSTRING:
					{
						pValue->type = TypeSize.type;
						uchar usConst[] = {0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40};
						pValue->val.b.assign(usConst, 7);
						pValue->size = (short)pValue->val.b.length();
					}
					break;
				case nsEDDEngine::VT_EUC:
					pValue->type = TypeSize.type;
					pValue->val.s.assign(L"eeeeeeee",false);;
					pValue->size = (short)pValue->val.s.length();
					break;
				case nsEDDEngine::VT_INDEX:
					pValue->type = TypeSize.type;
					pValue->val.u = (unsigned long long)GetDefaultIndex(iBlockInstance, pValueSpec, pParamSpec);
					pValue->size = 4;
					break;
				default:
					BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"GetParamValue: Value for Item ID %i is unknown at this time.", idPrimaryID);
					ec = PC_OTHER_EC;
					break;
				}
			}
			break;
		}
		if (0 != nRetVal)
		{
			ec = PC_OTHER_EC;
		}

		if (ec == PC_SUCCESS_EC)
		{
			m_ParamCache[key] = *pValue;
		}

		return ec;
	}

    
	PC_ErrorCode Consumer::GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF* pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ EVAL_VAR_VALUE *pParamValue)
    {
		//find if the attribute has been set before
		nsEDDEngine::AttributeName attrKey = attributeName;
		ATTR_VALUE::iterator attrIterator = m_AttrValueList.find(attrKey);
		if (attrIterator != m_AttrValueList.end())
		{
			//found, read it
			*pParamValue = attrIterator->second;
			return PC_ErrorCode::PC_SUCCESS_EC;
		}

		switch (attrKey)
		{
		case nsEDDEngine::AttributeName::variable_status:
			pParamValue->type = nsEDDEngine::VT_INTEGER;
			pParamValue->val.i = nsEDDEngine::VARIABLE_STATUS_NOT_ACCEPTED;
			pParamValue->size = sizeof(int);
			break;
		case nsEDDEngine::AttributeName::count:
			{
				pParamValue->type = nsEDDEngine::VT_UNSIGNED;
				pParamValue->size = sizeof(int);

				//read it from DD or initialize it with zero
				VARIANT vtValue;
				VariantClear(&vtValue);
				int retValue = GetListElementCount(iBlockInstance, pValueSpec, (nsEDDEngine::OP_REF*)pOpRef, &vtValue);
				if (retValue == BSEC_SUCCESS)
				{
					pParamValue->val.u = V_UI4(&vtValue);
				}
				else
				{
					pParamValue->val.u = 0;
				}
			}
			break;
		case nsEDDEngine::AttributeName::scaling_factor:
			pParamValue->type = nsEDDEngine::VT_DOUBLE;
			pParamValue->val.d = 0.1;
			pParamValue->size = sizeof(double);
			break;
		default:
			pParamValue->type = nsEDDEngine::VT_INTEGER;
			pParamValue->val.i = 1;
			pParamValue->size = sizeof(int);
			break;
		}
        
		return PC_ErrorCode::PC_SUCCESS_EC;
    }


	PC_ErrorCode Consumer::SetDynamicAttribute( int /*iBlockInstance*/, void* /*pValueSpec*/, const nsEDDEngine::OP_REF * /*pOpRef*/,
            const nsEDDEngine::AttributeName attributeName, const EVAL_VAR_VALUE *pParamValue)
    {
		nsEDDEngine::AttributeName attrKey = attributeName;
 		m_AttrValueList[attrKey] = *pParamValue;
		
		return PC_ErrorCode::PC_SUCCESS_EC;
    }


	PC_ErrorCode Consumer::SetParamValue( int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ EVAL_VAR_VALUE *pValue )
	{
		nsEDDEngine::FDI_PARAM_SPECIFIER stdParamSpec;
		getStdParamSpecPointer(pParamSpec, &stdParamSpec);

		ULONGLONG key = (((ULONGLONG)stdParamSpec.id << 32) | stdParamSpec.subindex);
		m_ParamCache[key] = *pValue;

		switch (pValue->type)
		{
		case nsEDDEngine::VT_ASCII:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: ASCII, Item ID: %u, Value: %.2900ls", pParamSpec->id, pValue->val.s.c_str());
			break;
		case nsEDDEngine::VT_PACKED_ASCII:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: PACKED_ASCII, Item ID: %u, Value: %.2900ls", pParamSpec->id, pValue->val.s.c_str());
			break;
		case nsEDDEngine::VT_PASSWORD:
		case nsEDDEngine::VT_EUC:
		case nsEDDEngine::VT_VISIBLESTRING:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: PASSWORD, Item ID: %u, Value: %.2900ls", pParamSpec->id, pValue->val.s.c_str());
			break;
		case nsEDDEngine::VT_BITSTRING:
		case nsEDDEngine::VT_OCTETSTRING:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: BITSTRING, Item ID: %u, Value[0]: %hhu, length: %d", pParamSpec->id, pValue->val.b.ptr()[0], pValue->val.b.length());
			break;
		case nsEDDEngine::VT_INTEGER:
		case nsEDDEngine::VT_BOOLEAN:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: INTEGER, Item ID: %u, Value: %lli", pParamSpec->id, pValue->val.i);
			break;
		case nsEDDEngine::VT_UNSIGNED:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: UNSIGNED, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.u);
			break;
		case nsEDDEngine::VT_DATE_AND_TIME:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: DATE_AND_TIME, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.u);
			break;
		case nsEDDEngine::VT_EDD_DATE:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: EDD_DATE, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.i);
			break;
		case nsEDDEngine::VT_TIME_VALUE:
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DURATION:
			if (pValue->size >= 8)
			{
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: INTEGER, Item ID: %u, Value: %lli", pParamSpec->id, pValue->val.i);
			}
			else	//should be 4
			{
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: TIME_VALUE, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.u);
			}
			break;
		case nsEDDEngine::VT_ENUMERATED:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: 64 ENUMERATED, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.u);
			break;
		case nsEDDEngine::VT_BIT_ENUMERATED:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: 64 bit ENUMERATED, Item ID: %u, Value: %llu", pParamSpec->id, pValue->val.u);
			break;
		case nsEDDEngine::VT_INDEX:
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: INDEX, Item ID: %u, Value: %lli", pParamSpec->id, pValue->val.i);
			break;
		case nsEDDEngine::VT_FLOAT:	//4 byte real
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: FLOAT, Item ID: %u, Value: %f", pParamSpec->id, pValue->val.f);
			break;
		case nsEDDEngine::VT_DOUBLE:	//8 byte real
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"SetParamValue: Type: DOUBLE, Item ID: %u, Value: %Lf", pParamSpec->id, pValue->val.d);
			break;
			
		default:
			BssProgLog(__FILE__, __LINE__, BssError, E_INVALIDARG, L"SetParamValue: Data type for Item ID %u is unknown at this time.", pParamSpec->id);
			return PC_OTHER_EC;
			break;
		}
		return PC_SUCCESS_EC;
	}

	PC_ErrorCode Consumer::AssignParamValue( void* /*pValueSpec*/, int /*iDestBlockInstance*/, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, 
												int /*iSrcBlockInstance*/, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec )
	{
		nsEDDEngine::FDI_PARAM_SPECIFIER stdTempParamSpec;
		getStdParamSpecPointer(pSrcParamSpec, &stdTempParamSpec);

		//find if the source parameter has been set before
		EVAL_VAR_VALUE evalValue;
		ULONGLONG key = (((ULONGLONG)stdTempParamSpec.id << 32) | stdTempParamSpec.subindex);
		PARAMETER_CACHE::iterator valueIterator = m_ParamCache.find(key);
		if (valueIterator != m_ParamCache.end())
		{
			//yes, read it
			evalValue = valueIterator->second;
		}

		//assign the source parameter to destination parameter. If the source is not found in parameter cache, assign empty value to destination for test purpose onely
		getStdParamSpecPointer(pDestParamSpec, &stdTempParamSpec);
		key = (((ULONGLONG)stdTempParamSpec.id << 32) | stdTempParamSpec.subindex);
		m_ParamCache[key] = evalValue;

		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"AssignParamValue: Dest Item ID: %i, Dest subindex: %i; Src Item ID: %i, Src subindex: %i", pDestParamSpec->id, pDestParamSpec->subindex, pSrcParamSpec->id, pSrcParamSpec->subindex);
		return PC_SUCCESS_EC;
	}

	void Consumer::OnMethodExiting ( int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::ChangedParamActivity eActivity )
	{
		switch (eActivity)
		{
		case nsEDDEngine::Discard:
			//discard the changed values on method exit
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"OnMethodExiting: The changed values are discarded.");
			break;
		case nsEDDEngine::Save:
			//save the changed values to parameter cache
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"OnMethodExiting: The changed values are saved to parameter cache.");
			break;
		case nsEDDEngine::Send:
			//send the changed values to device
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"OnMethodExiting: The changed values are sent to device.");
			break;
		default:
			break;
		}
	}

	BS_ErrorCode Consumer::ProcessChangedValues(int /*iBlockInstance*/, void* /*pValueSpec*/, nsEDDEngine::ChangedParamActivity eActivity)
	{
		BS_ErrorCode iRetVal = BSEC_SUCCESS;
	
		switch (eActivity)
		{
		case nsEDDEngine::Save:
			//save the changed values to parameter cache
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"ProcessChangedValues: The changed values are saved to parameter cache.");
			break;
		case nsEDDEngine::Send:
			//send the changed values to device
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"ProcessChangedValues: The changed values are sent to device.");
			break;
		default:
			BssProgLog(__FILE__, __LINE__, BssError, BSS_INVALID_PARAMETER, L"ProcessChangedValues: Failed.");
			iRetVal = BSEC_OTHER;
			break;
		}

		return iRetVal;
	}


	//
	//  UI Builtins Support
	//		Route to UI
	BS_ErrorCode Consumer::AbortRequest(void * /*pValueSpec*/, wchar_t* pPrompt, AckCallback pCallback, void* state )		// Block, returns after AbortResponse
	{
		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		m_pUICallback = pCallback;
		m_state = state;
		getUIConsumerPtr()->m_ulRequestType = WINNONE;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);

		return BSEC_SUCCESS;
	}

	BS_ErrorCode Consumer::AcknowledgementRequest(void * /*pValueSpec*/,  wchar_t* pPrompt, AckCallback pCallback, void* state )		// Async; after receiving the AcknowledgementResponse,
	{
		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		if (pPrompt)
		{
			wcsncpy_s (getUIConsumerPtr()->m_wsUIMsg, 1024, pPrompt, _TRUNCATE);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		m_pUICallback = pCallback;
		m_state = state;
		getUIConsumerPtr()->m_ulRequestType = WINNONE;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);
		return BSEC_SUCCESS;
	}

												// the callback function is called with a scope (VT_ERROR: E_ABORT) to indicate whether it was cancelled or not.
	// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest until aborted or lSecondsToWait expires
	BS_ErrorCode Consumer::DelayMessageRequest(void * /*pValueSpec*/,  unsigned long lSecondsToWait, wchar_t* pPrompt, AckCallback pCallback, void* state )
	{
		wchar_t remainingTime[100];
		_itow_s((int)lSecondsToWait, remainingTime, 10);

		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		wcscat_s (getUIConsumerPtr()->m_wsUIMsg, _T("\nThe delay time is "));
		wcscat_s (getUIConsumerPtr()->m_wsUIMsg, remainingTime);
		wcscat_s (getUIConsumerPtr()->m_wsUIMsg, _T(" seconds."));
		m_pUICallback = pCallback;
		m_state = state;
		getUIConsumerPtr()->m_ulRequestType = WINNONE;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);
		return BSEC_SUCCESS;
	}

	//This function is called by two kinds of UI builtins: one for displaying prompt without user acknowledgement;
	// another one for updating prompt and waiting for user acknowledgement
	BS_ErrorCode Consumer::InfoRequest(void * /*pValueSpec*/,  wchar_t* pPrompt, bool /* bPromptUpdate */ )	// Async, Must return immediately, so dynamic update is possible
	{
		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		if (!isUICallbackWaiting())
		{
			//This function is called for displaying prompt without user acknowledgement
			createUIConsumerInst();
			getUIConsumerPtr()->m_ulRequestType = WINNONE;
		}

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);
		return BSEC_SUCCESS;
	}


	//For UI builtin test only. It is copied from the same named function in MiEngine.cpp
	bool Consumer::convertVariantToEval(VARIANT *pvtValue, nsConsumer::EVAL_VAR_VALUE *evalValue)
	{
		//copy from MiEngine.cpp
		bool rc = true;

		switch (V_VT(pvtValue))
		{
	case VT_BSTR:
		evalValue->type = nsEDDEngine::VT_ASCII;
		evalValue->val.s = V_BSTR(pvtValue);	//pointer assignment
		if (V_BSTR(pvtValue) != NULL)
		{
			evalValue->size = (short)evalValue->val.s.length();
		}
		else
		{
			evalValue->size = 0;
		}
		break;
	case VT_INT:
		evalValue->type = nsEDDEngine::VT_INTEGER;
		evalValue->val.i = V_INT(pvtValue);
		evalValue->size = 4;
		break;
	case VT_I1:
		evalValue->type = nsEDDEngine::VT_INTEGER;
		evalValue->val.i = V_I1(pvtValue);
		evalValue->size = 1;
		break;
	case VT_I2:
		evalValue->type = nsEDDEngine::VT_INTEGER;
		evalValue->val.i = V_I2(pvtValue);
		evalValue->size = 2;
		break;
	case VT_I4:
		evalValue->type = nsEDDEngine::VT_INTEGER;
		evalValue->val.i = V_I4(pvtValue);
		evalValue->size = 4;
		break;
	case VT_I8:	//signed 64-bit int
		evalValue->type = nsEDDEngine::VT_INTEGER;	//or VT_TIME_VALUE(8) or VT_EDD_DATE
		evalValue->val.i = V_I8(pvtValue);
		evalValue->size = 8;
		break;
	case VT_BOOL:
	case VT_UI1:
		evalValue->type = nsEDDEngine::VT_UNSIGNED;
		evalValue->val.u = (unsigned long long)V_UI1(pvtValue);	//BYTE
		evalValue->size = 1;
		break;
	case VT_UI2:
		evalValue->type = nsEDDEngine::VT_UNSIGNED;
		evalValue->val.u = (unsigned long long)V_UI2(pvtValue);
		evalValue->size = 2;
		break;
	case VT_UINT:	//unsigned 32-bit int
		evalValue->type = nsEDDEngine::VT_UNSIGNED;	//or other types such as VT_ENUMERATED etc, see IParamCache.h
		evalValue->val.u = V_UINT(pvtValue);
		evalValue->size = 4;
		break;
	case VT_UI4:	//unsigned 32-bit int
		evalValue->type = nsEDDEngine::VT_UNSIGNED;	//or other types such as VT_ENUMERATED etc, see IParamCache.h
		evalValue->val.u = V_UI4(pvtValue);
		evalValue->size = 4;
		break;
	case VT_UI8:	//unsigned 64-bit int
		evalValue->type = nsEDDEngine::VT_UNSIGNED;	//or other types such as VT_TIME etc, see IParamCache.h
		evalValue->val.u = V_UI8(pvtValue);
		evalValue->size = 8;
		break;
	case VT_R4:	//4 byte real
		evalValue->type = nsEDDEngine::VT_FLOAT;
		evalValue->val.f = V_R4(pvtValue);
		evalValue->size = 4;
		break;
	case VT_R8:	//8 byte real
		evalValue->type = nsEDDEngine::VT_DOUBLE;
		evalValue->val.d = V_R8(pvtValue);
		evalValue->size = 8;
		break;
	default:
		rc = false;
		break;
		}

		return (rc);
	}

	//For UI builtin test only. It is copied from the same named function in MiEngine.cpp
	bool Consumer::convertEvalToVariant(const nsConsumer::EVAL_VAR_VALUE *pevalValue, VARIANT *vtValue)
	{
		bool rc = true;

		switch (pevalValue->type)
		{
		case nsEDDEngine::VT_ASCII:
		case nsEDDEngine::VT_PASSWORD:
		case nsEDDEngine::VT_PACKED_ASCII:
		case nsEDDEngine::VT_EUC:
		case nsEDDEngine::VT_VISIBLESTRING:
			V_VT(vtValue) = VT_BSTR;
			V_BSTR(vtValue) = SysAllocString( T2OLE( (wchar_t*)pevalValue->val.s.c_str()));
			break;
		case nsEDDEngine::VT_BITSTRING:	// unsigned char[]
		case nsEDDEngine::VT_OCTETSTRING:
			InitVariantFromBuffer(pevalValue->val.b.ptr(), pevalValue->val.b.length(), vtValue);
			break;
		case nsEDDEngine::VT_TIME_VALUE:
			if (pevalValue->size <= 4)
			{
				V_VT(vtValue) = VT_UI4;
				V_UI4(vtValue) = (ULONG)pevalValue->val.u;
			}
			else
			{
				V_VT(vtValue) = VT_I8;
				V_I8(vtValue) = pevalValue->val.i;
			}
			break;
		case nsEDDEngine::VT_UNSIGNED:
		case nsEDDEngine::VT_ENUMERATED:
		case nsEDDEngine::VT_BIT_ENUMERATED:
		case nsEDDEngine::VT_INDEX:
		case nsEDDEngine::VT_BOOLEAN:
		case nsEDDEngine::VT_DATE_AND_TIME:
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DURATION:
		case nsEDDEngine::VT_OBJECT_REFERENCE:
			if (pevalValue->size == 1)
			{
				V_VT(vtValue) = VT_UI1;
				V_UI1(vtValue) = (unsigned char)pevalValue->val.u;
			}
			else if (pevalValue->size == 2)
			{
				V_VT(vtValue) = VT_UI2;
				V_UI2(vtValue) = (unsigned short)pevalValue->val.u;
			}
			else if (pevalValue->size <= 4)
			{
				V_VT(vtValue) = VT_UI4;
				V_UI4(vtValue) = (ULONG)pevalValue->val.u;
			}
			else
			{
				V_VT(vtValue) = VT_UI8;
				V_UI8(vtValue) = pevalValue->val.u;
			}
			break;
		case nsEDDEngine::VT_FLOAT:	//4 byte real
			V_VT(vtValue) = VT_R4;
			V_R4(vtValue) = pevalValue->val.f;
			break;
		case nsEDDEngine::VT_DOUBLE:	//8 byte real
			V_VT(vtValue) = VT_R8;
			V_R8(vtValue) = pevalValue->val.d;
			break;
		case nsEDDEngine::VT_EDD_DATE:
		case nsEDDEngine::VT_INTEGER:
			if (pevalValue->size == 1)
			{
				V_VT(vtValue) = VT_I1;
				V_I1(vtValue) = (char)pevalValue->val.i;
			}
			else if (pevalValue->size == 2)
			{
				V_VT(vtValue) = VT_I2;
				V_I2(vtValue) = (short)pevalValue->val.i;
			}
			else if (pevalValue->size <= 4)
			{
				V_VT(vtValue) = VT_I4;
				V_I4(vtValue) = (LONG)pevalValue->val.i;
			}
			else
			{
				V_VT(vtValue) = VT_I8;
				V_I8(vtValue) = pevalValue->val.i;
			}
			break;
		default:
			rc = false;
			BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Wrong data type");
			break;
		}

		return (rc);
	}


	void Consumer::uiBuiltinCallback(BOOLEAN isOkayBtn)
	{
		//The following code is for test only. FDI EDD engine consumer would do it in a seperated thread from those UI *Request() thread
		if ((m_pUICallback != nullptr) && (m_state != nullptr))
		{
			EVAL_VAR_VALUE tempEval;
			EVAL_VAR_VALUE *pEvalVarValue = nullptr;	//Create a null evalVarValue to test to make sure that the UICallBack can properly handle the null.
			BS_ErrorCode eStatus = BSEC_ABORTED;        //initial Cancel button hit
			BOOLEAN bRet = isOkayBtn;				//copy from user button entry

			if ( (bRet)
	    		 && (((getUIConsumerPtr()->m_vUIUserInput.vt != VT_EMPTY) && (getUIConsumerPtr()->m_ulRequestType == WINEDIT)) 
				     || (getUIConsumerPtr()->m_ulRequestType == WINSELECT))
			   )
			{
				if (getUIConsumerPtr()->m_ulRequestType == WINSELECT)
				{
					//insert the user selection into vUIEntry
					VariantClear(&(getUIConsumerPtr()->m_vUIUserInput));
					getUIConsumerPtr()->m_vUIUserInput.vt = VT_UI4;
					getUIConsumerPtr()->m_vUIUserInput.ulVal = getUIConsumerPtr()->m_ulCurrentSel;				//indexed parameter index, not the value
				}
				
				if (convertVariantToEval(&(getUIConsumerPtr()->m_vUIUserInput), &tempEval))
				{
					eStatus = BSEC_SUCCESS;
				}
				else
				{
					eStatus = BSEC_ABORTED;	//equivalent to cancel button hit
				}
				pEvalVarValue = &tempEval;
			}
			else if (bRet)
			{
				eStatus = BSEC_SUCCESS;
			}
			else
			{
				eStatus = BSEC_ABORTED;		//Cancell button hit
			}

			if (m_pUICallback)
			{
				(*m_pUICallback)(pEvalVarValue, eStatus, m_state);
			}

			//for next time use
			m_pUICallback = nullptr;
			m_state = nullptr;
		}

		deleteUIConsumerInst();
		//done of test thread
	}

	//This function is used to get local variable value
	// pValue is InitialValue, which is set to final value during call
	BS_ErrorCode Consumer::InputRequest(void * /*pValueSpec*/,  wchar_t* pPrompt, /* in */ const EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, AckCallback pCallback, void* state )
	{
		BS_ErrorCode retVal = BSEC_SUCCESS;


		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance if it is not already created
		createUIConsumerInst();

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		m_pUICallback = pCallback;
		m_state = state;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);


		if ( (pVar != nullptr) &&
			 ((pVar->type_size.type == nsEDDEngine::VT_ENUMERATED)
			  || (pVar->type_size.type == nsEDDEngine::VT_BIT_ENUMERATED)
			  || (pVar->type_size.type == nsEDDEngine::VT_INDEX))
		   )
		{
			//never come here because there is no local variable that is ENUM or BIT-ENUM or INDEX

			BSTR sUISelect[32];
			unsigned long c = 0;
#if 0
			token = wcstok_s(pValue->val.s.str, seps, &next_token);
			while (token != NULL)
			{
				sUISelect[c] = SysAllocString(token);
				token = wcstok_s(NULL, seps, &next_token);
				c++;
			}
#endif
			// Specify the bounds:
			// -- dim. count = 1
			// -- element count = 32 for each dimension
			// -- low bound = 0 for each dimension
			SAFEARRAYBOUND mySafeArrayBounds[1] = {c, 0};
 
			// Create mySafeArray
			SAFEARRAY *mySafeArray = NULL;
			mySafeArray = SafeArrayCreate(VT_BSTR /* var. type: BSTR */ ,
				1 /* dim. count */ , mySafeArrayBounds);
 
			if (mySafeArray != NULL)
			{
				//add UI selection enumerations into SAFEARRAY variable
				long lIndex;
				for (lIndex = 0; lIndex < (long)c; lIndex++)
				{
					SafeArrayPutElement(mySafeArray, &lIndex, (void*) sUISelect[lIndex]);
				}

				//add SAFEARRAY variable into VARIANT variable
				EnterCriticalSection(&m_uiBltinCriticalSection);

				getUIConsumerPtr()->m_vUIOptionList.vt = VT_ARRAY|VT_BSTR;
				getUIConsumerPtr()->m_vUIOptionList.parray = mySafeArray;

				getUIConsumerPtr()->m_ulCurrentSel = 0;		//zero based
				getUIConsumerPtr()->m_ulRequestType = WINSELECT;
	
				LeaveCriticalSection(&m_uiBltinCriticalSection);
			}
			else
			{
				retVal = BSEC_OTHER;
			}
		}
		else
		{
			//other EDIT or HARTDATE initial data
			EnterCriticalSection(&m_uiBltinCriticalSection);

			getUIConsumerPtr()->m_ulRequestType = WINEDIT;
	
			//save required return type for input request
			convertEvalToVariant(pValue, &(getUIConsumerPtr()->m_vUIUserInput));

			LeaveCriticalSection(&m_uiBltinCriticalSection);
		}
		
		return (retVal);
	}

	//This function is used to get device variable value
	// We only get the parameter reference (OP_REF_TRAIL), so there isn't much to do here.
	BS_ErrorCode Consumer::ParameterInputRequest(int iBlockInstance, void * /*pValueSpec*/,  wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, AckCallback pCallback, void* state )
	{
		BS_ErrorCode retVal = BSEC_SUCCESS;

		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		m_pUICallback = pCallback;
		m_state = state;

		CString str;
		str.Format(L"iBlockInstance = %d, ItemId = %u, MemberId = %u", iBlockInstance, pParameter->op_info.id, pParameter->op_info.member);

		getUIConsumerPtr()->m_vUIUserInput.vt = VT_BSTR;
		getUIConsumerPtr()->m_vUIUserInput.bstrVal = SysAllocString(str);
		getUIConsumerPtr()->m_ulRequestType = WINEDIT;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);

		return (retVal);
	}

	BS_ErrorCode Consumer::SelectionRequest(void * /*pValueSpec*/,  wchar_t* pPrompt, wchar_t* pOptions, AckCallback pCallback, void* state)
	{
		BS_ErrorCode retVal = BSEC_SUCCESS;


		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		if (pPrompt)
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, pPrompt);
		}
		else
		{
			wcscpy_s (getUIConsumerPtr()->m_wsUIMsg, _T(""));
		}
		m_pUICallback = pCallback;
		m_state = state;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);

		//retrieve selections from MI
		BSTR sUISelect[32];
		wchar_t seps[] = L";";
		wchar_t* token = NULL;
		wchar_t* next_token = NULL;
		unsigned long c = 0;
		token = wcstok_s(pOptions, seps, &next_token);
		while (token != NULL)
		{
			sUISelect[c] = SysAllocString(token);
			token = wcstok_s(NULL, seps, &next_token);
			c++;
		}

		// Specify the bounds:
		// -- dim. count = 1
		// -- element count = 32 for each dimension
		// -- low bound = 0 for each dimension
		SAFEARRAYBOUND mySafeArrayBounds[1] = {c, 0};
 
		// Create mySafeArray
		SAFEARRAY *mySafeArray = NULL;
		mySafeArray = SafeArrayCreate(VT_BSTR /* var. type: BSTR */ ,
			1 /* dim. count */ , mySafeArrayBounds);
 
		if (mySafeArray != NULL)
		{
			//add UI selection enumerations into SAFEARRAY variable
			long lIndex;
			for (lIndex = 0; lIndex < (long)c; lIndex++)
			{
				SafeArrayPutElement(mySafeArray, &lIndex, (void*) sUISelect[lIndex]);
			}

			//add SAFEARRAY variable into VARIANT variable
			EnterCriticalSection(&m_uiBltinCriticalSection);

			getUIConsumerPtr()->m_vUIOptionList.vt = VT_ARRAY|VT_BSTR;
			getUIConsumerPtr()->m_vUIOptionList.parray = mySafeArray;

			getUIConsumerPtr()->m_ulCurrentSel = 0;		//zero based
			getUIConsumerPtr()->m_ulRequestType = WINSELECT;

			LeaveCriticalSection(&m_uiBltinCriticalSection);
		}
		else
		{
			retVal = BSEC_OTHER;
		}

		return (retVal);
	}

	BS_ErrorCode Consumer::UIDRequest ( int /*iBlockInstance*/, void * /*pValueSpec*/, ITEM_ID /*menu*/, wchar_t* pOptions, AckCallback pCallback, void* state)
	{
		BS_ErrorCode retVal = BSEC_SUCCESS;


		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);

		//create UIConsumer instance
		createUIConsumerInst();

		getUIConsumerPtr()->m_wsUIMsg[0] = _T('\0');
		m_pUICallback = pCallback;
		m_state = state;

		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);

		//retrieve selections from MI
		BSTR sUISelect[32];
		wchar_t seps[] = L";";
		wchar_t* token = NULL;
		wchar_t* next_token = NULL;
		unsigned long c = 0;
		token = wcstok_s(pOptions, seps, &next_token);
		while (token != NULL)
		{
			sUISelect[c] = SysAllocString(token);
			token = wcstok_s(NULL, seps, &next_token);
			c++;
		}

		// Specify the bounds:
		// -- dim. count = 1
		// -- element count = 32 for each dimension
		// -- low bound = 0 for each dimension
		SAFEARRAYBOUND mySafeArrayBounds[1] = {c, 0};
 
		// Create mySafeArray
		SAFEARRAY *mySafeArray = NULL;
		mySafeArray = SafeArrayCreate(VT_BSTR /* var. type: BSTR */ ,
			1 /* dim. count */ , mySafeArrayBounds);
 
		if (mySafeArray != NULL)
		{
			//add UI selection enumerations into SAFEARRAY variable
			long lIndex;
			for (lIndex = 0; lIndex < (long)c; lIndex++)
			{
				SafeArrayPutElement(mySafeArray, &lIndex, (void*) sUISelect[lIndex]);
			}

			//add SAFEARRAY variable into VARIANT variable
			EnterCriticalSection(&m_uiBltinCriticalSection);

			getUIConsumerPtr()->m_vUIOptionList.vt = VT_ARRAY|VT_BSTR;
			getUIConsumerPtr()->m_vUIOptionList.parray = mySafeArray;

			getUIConsumerPtr()->m_ulCurrentSel = 0;		//zero based
			getUIConsumerPtr()->m_ulRequestType = WINSELECT;

			LeaveCriticalSection(&m_uiBltinCriticalSection);
		}
		
		return (retVal);
	}


	void Consumer::EnterUIBuiltinSection()
	{
		// Request ownership of the critical section.
		EnterCriticalSection(&m_uiBltinCriticalSection);
	}

	void Consumer::LeaveUIBuiltinSection()
	{
		// Release ownership of the critical section.
		LeaveCriticalSection(&m_uiBltinCriticalSection);
	}

	bool Consumer::isUIBuiltinRunning()
	{
		return (pUIConsumer != nullptr);
	}
	
	bool Consumer::isUICallbackWaiting()
	{
		return (m_pUICallback != nullptr);
	}

	wchar_t* UIConsumer::getUIMessage()
	{
		size_t ilen = wcslen(m_wsUIMsg);

		wchar_t *promptMsg = new wchar_t[ilen+1];
		wcsncpy_s(promptMsg, ilen+1, m_wsUIMsg, _TRUNCATE);
		return (promptMsg);
	}

	enumUIRequestType UIConsumer::getUIRequestType()
	{
		return (m_ulRequestType);
	}

	void UIConsumer::setUICurrentSel(unsigned long selection)
	{
		m_ulCurrentSel = selection;
	}

	unsigned long UIConsumer::getUICurrentSel()
	{
		return (m_ulCurrentSel);
	}


	//This function converts user input string to a proper variable value based on variable type stored in m_vUIUserInput.vt
	void UIConsumer::setUserEnteredText(VARIANT* entry)				//entry is the output of input box. Therefore its value is always BSTR
	{
		//convert entry BSTR to required data type in m_vUIUserText
		switch (m_vUIUserInput.vt)
		{
		case VT_I1:
			{
				//this type is equivalent to local char type in DD. User expects to display character
				wchar_t *wstr = V_BSTR(entry);
				char temp = *reinterpret_cast<char *>(wstr);
				V_I1(&m_vUIUserInput) = (char)temp;
			}
			break;
		case VT_UI1:
			//this type is equivalent to local unsigned char type in DD. User expects to display numeric value
		default:
			VariantChangeType(&m_vUIUserInput, entry, 0, m_vUIUserInput.vt);
			break;
		}
	}

	void UIConsumer::getUIOptionList(VARIANT* entry)				//entry is the output of input box. Therefore its value is always BSTR
	{
		VariantCopy(entry, &m_vUIOptionList);
	}

	//This function always creates a new UIConsumer instance
	void Consumer::createUIConsumerInst()
	{
		deleteUIConsumerInst();
		pUIConsumer = new UIConsumer();
	}
	
	void Consumer::deleteUIConsumerInst()
	{
		if (pUIConsumer != nullptr)
		{
			delete pUIConsumer; 
			pUIConsumer = nullptr;
		}
	}

	
	UIConsumer::UIConsumer(void)
	{
		m_ulCurrentSel = 0;
		m_ulRequestType = WINNONE;
		VariantInit(&m_vUIOptionList);
		VariantInit(&m_vUIUserInput);
	}


	UIConsumer::~UIConsumer(void)
	{
		m_ulCurrentSel = 0;
		m_ulRequestType = WINNONE;
		VariantClear(&m_vUIOptionList);
		VariantClear(&m_vUIUserInput);
	}

}
