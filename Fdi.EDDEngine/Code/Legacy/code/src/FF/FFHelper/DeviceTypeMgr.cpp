#include "stdinc.h"
#include "DeviceTypeMgr.h"

CDeviceTypeMgr g_DeviceTypeMgr;

CLockDeviceTypeMgr::CLockDeviceTypeMgr()
{
	g_DeviceTypeMgr.m_cs.lock();
}

CLockDeviceTypeMgr::~CLockDeviceTypeMgr() 
{
	g_DeviceTypeMgr.m_cs.unlock();
}

CDeviceTypeMgr::CDeviceTypeMgr()   
{
	
	active_dev_type_tbl.count = 0;
	active_dev_type_tbl.list = (ACTIVE_DEV_TYPE_TBL_ELEM **) malloc(1);

}

CDeviceTypeMgr::~CDeviceTypeMgr()
{
	active_dev_type_tbl.count = 0;
	free (active_dev_type_tbl.list);
	active_dev_type_tbl.list = nullptr;	
	
	
}