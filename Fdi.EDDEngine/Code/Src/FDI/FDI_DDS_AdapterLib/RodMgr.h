#pragma once

#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"

//FDI Rod Manager

class CRodMgr
{
public:
	CRodMgr();
	~CRodMgr();

	int VALID_ROD_HANDLE(ROD_HANDLE rh);
	int rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
	unsigned long rod_get_version(ROD_HANDLE rhandle);
	int ds_ddod_file_load(ENV_INFO *env_info,/* fd_fdi */wchar_t *filename, ROD_HANDLE *rod_handle);
	int rod_close(ROD_HANDLE rod_handle);
	int ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc);
private:	
		
	char *rod_malloc(int size);
	char *rod_calloc(int number, int size);
	char *rod_realloc(char *pointer, int size);
	void rod_free(char *pointer);
	int rod_member(char *pointer);
	void rod_object_free(OBJECT *object);
	int rod_object_heap_check(OBJECT *object);
	int rod_set_header(ROD_HANDLE rhandle, DDOD_HEADER header);
	int rod_set_version(ROD_HANDLE rhandle, unsigned long version);
	ROD_HANDLE	rod_new_rod(wchar_t * filename);
	ROD_HANDLE rod_open(DDEF_DESC *ddef_desc,ROD_HANDLE rod_handle);
	int rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT *object); // fd_fdi
	int rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
	int ds_get_object(FILE *file, OBJECT *obj, ROD_HANDLE *rod_handle);
	

	ROD_TBL				rod_tbl;
	RHEAP				rod_heap;
};