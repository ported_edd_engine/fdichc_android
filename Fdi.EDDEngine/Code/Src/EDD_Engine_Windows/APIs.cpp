#include "stdafx.h"
#include "APIs.h"
#include <cassert>
#include <crtdbg.h>


DWORD PS_GetLastError(void)
{
	return GetLastError();
}

BOOL PS_InitializeSecurityDescriptor(void* pSecurityDescriptor, DWORD dwRevision)
{
	return InitializeSecurityDescriptor(pSecurityDescriptor, dwRevision);
}

int PS_CrtDbgReport(int reportType, const char * filename, int linenumber, const char * moduleName, const char * format)
{
	int retCode = 0;
#ifdef _DEBUG
	retCode = _CrtDbgReport( reportType, filename, linenumber,  moduleName, format);
#endif
	return retCode;   
}

void PS_CrtDbgBreak()
{
	_CrtDbgBreak();	
}

BOOL PS_IsDebuggerPresent(void)
{
	return IsDebuggerPresent();
}

errno_t PS_GetErrno(int* err)
{
	return _get_errno(err);
}

void PS_Sleep(unsigned long lMilliSeconds)
{
	Sleep(lMilliSeconds);
}

UINT PS_GetSystemDirectory(wchar_t* lpBuffer, UINT uSize)
{
	return GetWindowsDirectoryW(lpBuffer, uSize);
}

void * PS_LocalAlloc(unsigned int uFlags, size_t uBytes)
{
	return LocalAlloc(uFlags,uBytes);
}

void* PS_LocalFree(void * ptr)
{
	return LocalFree(ptr);
}

void* PS_MapViewOfFile(void * hFileMappingObject, int fileAccess, DWORD dwFileOffsetHigh, DWORD dwFileOffsetLow, unsigned long dwNumberOfBytesToMap)
{
	DWORD dwDesiredAccess = FILE_MAP_ALL_ACCESS;
	
	if (fileAccess == PI_FILE_MAP_READ_ONLY)
	{
		dwDesiredAccess = FILE_MAP_READ;
	}
	else if (fileAccess == PI_FILE_MAP_READ_WRITE)
	{
		dwDesiredAccess = FILE_MAP_WRITE;
	}
	
	return MapViewOfFile(hFileMappingObject, dwDesiredAccess, dwFileOffsetHigh, dwFileOffsetLow, dwNumberOfBytesToMap);	
}

void * PS_CreateFileMapping(void* hFile, BOOL bSetSecurityAttribute, int nDesiredAccess, DWORD dwMaximumSizeHigh, DWORD dwMaximumSizeLow, const wchar_t* lpName)
{
	PSECURITY_ATTRIBUTES lpSecurityAttrs = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	HANDLE handle = INVALID_HANDLE_VALUE;
	DWORD dwDesiredAccess = GetFileMapAccess(nDesiredAccess);

	if (dwDesiredAccess == PAGE_NOACCESS)
	{
		return NULL;
	}

	if (bSetSecurityAttribute)
	{
		lpSecurityAttrs = (PSECURITY_ATTRIBUTES)LocalAlloc(LPTR, sizeof(SECURITY_ATTRIBUTES));
		pSD = (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH);

		assert(lpSecurityAttrs);
		assert(pSD);

		if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION))
		{
			LocalFree(pSD);
			LocalFree(lpSecurityAttrs);
			return NULL;
		}

		if (!SetSecurityDescriptorDacl(pSD, TRUE, (PACL)NULL, FALSE))
		{
			LocalFree(pSD);
			LocalFree(lpSecurityAttrs);
			return NULL;
		}

		// Created a security attribute which has a NULL discretionary ACL assigned
		// to the security descriptor, allowing all access to the file-mapping 
		// object. This is done since we are now running our AMS COM servers under 
		// a common user account which may be different than who the client is 
		// running as. If we don't do this and pass in NULL for the second 
		// parameter of the CreateFileMapping() method call, the file-mapping
		// object gets a default security descriptor and we will most likely 
		// encounter an access denied error when the client process attempts to 
		// open the file.
		lpSecurityAttrs->nLength = sizeof(lpSecurityAttrs);
		lpSecurityAttrs->lpSecurityDescriptor = pSD;
		lpSecurityAttrs->bInheritHandle = FALSE;

		if (hFile == NULL)
		{
			hFile = INVALID_HANDLE_VALUE;
		}
	}	

	handle = CreateFileMappingW(hFile, lpSecurityAttrs, GetFileMapAccess(nDesiredAccess), dwMaximumSizeHigh, dwMaximumSizeLow, lpName);

	if (lpSecurityAttrs != NULL)
	{
		LocalFree(((PSECURITY_ATTRIBUTES)lpSecurityAttrs)->lpSecurityDescriptor);
		LocalFree(lpSecurityAttrs);
	}

	return handle;
}

DWORD GetFileMapAccess(int nFileAccess)
{
	DWORD dwDesiredAccess = PAGE_NOACCESS;

	if (nFileAccess == PI_PAGE_READ_ONLY)
	{
		dwDesiredAccess = PAGE_READONLY;
	}
	else if (nFileAccess == PI_PAGE_READ_WRITE)
	{
		dwDesiredAccess = PAGE_READWRITE;
	}
	else if (nFileAccess == PI_PAGE_WRITE_COPY)
	{
		dwDesiredAccess = PAGE_WRITECOPY;
	}

	return dwDesiredAccess;
}

void* PS_OpenFileMapping(int nDesiredAccess, BOOL bInheritHandle, const wchar_t* lpName)
{
	DWORD dwDesiredAccess = GetFileMapAccess(nDesiredAccess);

	if (dwDesiredAccess == PAGE_NOACCESS)
		return NULL;

	return OpenFileMappingW(dwDesiredAccess, bInheritHandle, lpName);
}

BOOL PS_UnmapViewOfFile(const void* lpBaseAddress)
{
	return UnmapViewOfFile(lpBaseAddress);
}

BOOL PS_CloseHandle(void* hObject)
{
	return CloseHandle(hObject);
}

DWORD PS_GetTicketCount()
{
	return GetTickCount();
}

DWORD PS_GetFileAttributes(const wchar_t* lpFileName)
{
	return GetFileAttributes(lpFileName);
}

BOOL PS_TRACE(const wchar_t *format, ...)
{
#ifdef _DEBUG
	wchar_t buffer[1000];

	va_list argptr;
	va_start(argptr, format);
	wvsprintf(buffer, format, argptr);
	va_end(argptr);

	OutputDebugString(buffer);
#endif
	return true;
}

BOOL PS_CreateDirectory(const wchar_t *filename, void* fileAttributes)
{
	return ::CreateDirectory(filename, NULL);
}

DWORD PS_GetCurrentProcessId()
{
	return GetCurrentProcessId();
}

void PS_DoDebugBreak(bool bEnableDebugBreak, const wchar_t* mylpString)
{
	if (bEnableDebugBreak)
	{
		if (PS_IsDebuggerPresent())
		{
			if (mylpString != NULL)
			{
				// We're running under the debugger. There's no need to call DebugBreak
				// placed in the two __try/__catch blocks below, because the outer DebugBreak will
				// force a first-chance exception handled in the debugger.
				OutputDebugString(mylpString);
			}

			//DebugBreak();
			return;
		}
		else // Lets start a debugger
		{

		}
		__try
		{
			__try
			{
				DebugBreak();
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{

			}

		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			// If we got here.
			// Debugger is already attached to our process.
			// Return to let the outer DebugBreak be called.
		}


	}
	else
	{
		if (PS_IsDebuggerPresent())
		{
			if (mylpString != NULL)
			{
				// We're running under the debugger. There's no need to call DebugBreak
				// placed in the two __try/__catch blocks below, because the outer DebugBreak will
				// force a first-chance exception handled in the debugger.
				OutputDebugString(mylpString);
			}

			//DebugBreak();
			return;
		}

		__try
		{
			__try
			{
				DebugBreak();
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
				// You can place the ExitProcess here to emulate  
				// work of the __except block from BaseStartProcess
				// ExitProcess( 0 );
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			// If we got here
			// Debugger is already attached to our process.
			// Return to let the outer DebugBreak be called.
		}
	}
}

FILE * PS_wfopen(const wchar_t *filename, const wchar_t* mode)
{
	return _wfopen(filename, mode);
}

errno_t PS_wfopen_s(FILE** fileptr, const wchar_t *filename, const wchar_t* mode)
{
	return _wfopen_s(fileptr, filename, mode);	
}

int  PS_MultiByteToWideChar(
	unsigned int CodePage,
	unsigned long dwFlags,
	const char* lpMultiByteStr,
	int cbMultiByte,
	wchar_t* lpWideCharStr,
	int cchWideChar	
	)
{	
	return ::MultiByteToWideChar(CodePage, dwFlags, lpMultiByteStr, cbMultiByte, lpWideCharStr, cchWideChar);
}

int PS_WideCharToMultiByte(
	unsigned int CodePage,
	unsigned long dwFlags,
	const wchar_t* lpWideCharStr,
	int cchWideChar,
	char* lpMultiByteStr,
	int cbMultiByte,
	void* lpDefaultChar,
	void* lpUsedDefaultChar
)
{
	return ::WideCharToMultiByte(CodePage, dwFlags, lpWideCharStr, cchWideChar, (LPSTR)lpMultiByteStr, cbMultiByte,
		(LPCCH)lpDefaultChar, (LPBOOL)lpUsedDefaultChar);
}

void  PS_OutputDebugString(const wchar_t* lpOutputString)
{
	OutputDebugStringW(lpOutputString);
}

void  PS_GetFileExtension(const wchar_t* pFileName, wchar_t* pFileExt)
{
	_wsplitpath_s(pFileName, NULL, 0, NULL, 0, NULL, 0, pFileExt, (size_t)_MAX_EXT);
}

