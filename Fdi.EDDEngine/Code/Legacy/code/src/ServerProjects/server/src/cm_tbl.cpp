
/**
 *	@(#) $Id: cm_tbl.cpp,v 1.78 1996/06/27 22:19:14 joefish Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_tbl.c - Connection Manager Tables Module
 *
 *	This file contains all functions pertaining to the Connection
 *	(Active) Tables.
 */


#include "stdinc.h"

#include "dev_type.h"

#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"
/*
 *	Function pointers for DDS Tables.
 */

static int (* const remove_dd_dev_tbl_fn_ptr) P((ENV_INFO *, DEVICE_HANDLE)) = ddi_remove_dd_dev_tbls;
static int (* const remove_dd_blk_tbl_fn_ptr) P((ENV_INFO *, BLOCK_HANDLE)) = ddi_remove_dd_blk_tbls;


/***********************************************************************
 *
 *	Name:  ct_block_search
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				a tag.
 *
 *	Description:
 *		The ct_block_time_search function takes a block tag and searches
 *		through the valid (allocated) Active Block Table elements in
 *		the Active Block Table list, looking for the element that
 *		matches the tag.  If found, the corresponding block handle
 *		will be returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		tag - unique identifier of a block.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		block_handle for success and -1 for failure
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/
//CAY called by ddi_tab.cpp  tr_block_tag_to_block_handle
BLOCK_HANDLE CConnectionMgr::ct_block_search(LPTSTR tag)
{
	BLOCK_HANDLE	block_handle;

	ASSERT_RET(tag, CM_BAD_POINTER) ;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (block_handle = 0; block_handle < active_blk_tbl.count;
			block_handle ++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the tag of the element and its time.
			 */

            if (!wcscmp(get_abt_tag(block_handle),tag)) {
				return(block_handle);
			}
		}
	}

	return(-1);
}

/***********************************************************************
 *
 *	Name:  ct_block_time_search
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				a tag.
 *
 *	Description:
 *		The ct_block_time_search function takes a block tag and searches
 *		through the valid (allocated) Active Block Table elements in
 *		the Active Block Table list, looking for the element that
 *		matches the tag.  If found, the corresponding block handle
 *		will be returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		tag - unique identifier of a block.
 *		network - that the tag is connected to
 *
 *	Outputs:
 *		block_handle.
 *
 *	Returns:
 *		Zero for success and non-zero for failure.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_block_time_search(LPTSTR tag, BLK_LIST *blk_list)
{
	BLOCK_HANDLE	block_handle;
	BLK_LIST_ELEM	*bl_elemp ;

	ASSERT_RET(tag && blk_list, CM_BAD_POINTER) ;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	blk_list->count = 0 ;
	
	for (block_handle = 0, bl_elemp = &blk_list->list[0] ; block_handle < active_blk_tbl.count;
			block_handle ++, bl_elemp++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the tag of the element and its time.
			 */

            if (!wcscmp(get_abt_tag(block_handle),tag)) {

				blk_list->count++ ;
				bl_elemp->block_handle = block_handle ;
				bl_elemp->status = CM_SUCCESS ;
			}
		}
	}

	if (blk_list->count == 0) {
		return(CM_BLOCK_NOT_FOUND);
	}

 	return(CM_SUCCESS);
}




/***********************************************************************
 *
 *	Name:  ct_device_type_search
 *
 *	ShortDesc:  Search for a device type in the Active Device Type
 *				Table using a DD device ID.
 *
 *	Description:
 *		The ct_device_type_search function takes a DD device ID and
 *		searches through the valid (allocated) Active Device Type
 *		Table elements in the Active Device Type Table list, looking
 *		for the element that matches the DD device ID.  If found, the
 *		corresponding device type handle will be returned.  Otherwise
 *		an error will be returned.
 *
 *	Inputs:
 *		dd_device_id - unique identifier of a device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device type handle of the matching Active Device Type
 *		Table element if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
 
DEVICE_TYPE_HANDLE CDeviceTypeMgr::ct_device_type_search(DD_DEVICE_ID *dd_device_id)
{
	DEVICE_TYPE_HANDLE	device_type_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (device_type_handle = 0; device_type_handle <
			active_dev_type_tbl.count; device_type_handle ++) {

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_type_tbl.list[device_type_handle]) {

			/*
			 *	Check the DD device ID of the element.
			 */
			
			if ((get_adtt_dd_device_id( device_type_handle)->ddid_manufacturer == dd_device_id->ddid_manufacturer) &&
					(get_adtt_dd_device_id( device_type_handle)->ddid_device_type ==dd_device_id->ddid_device_type) &&
					(get_adtt_dd_device_id( device_type_handle)->ddid_device_rev == dd_device_id->ddid_device_rev) &&
					(wcscmp(get_adtt_dd_device_id( device_type_handle)->dd_path, dd_device_id->dd_path) ==0)
					)
			{
				return(device_type_handle);
			}
		}
	}

	return(-1);
}


/***********************************************************************
 *
 *	Name:  ct_new_block
 *
 *	ShortDesc:  Create a new Active Block Table element.
 *
 *	Description:
 *		The ` function creates a new Active Block Table
 *		element either in the first unused (deallocated) element
 *		space or at the end of the existing Active Block Table list.
 *		The corresponding block handle will be returned unless there
 *		is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		block_handle - handle of the new Active Block Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_new_block(BLOCK_HANDLE *block_handle)
{
	BLOCK_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (temp_handle = 0; temp_handle < active_blk_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_blk_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Block Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_blk_tbl.count) {

		/*
		 *	Extend the Active Block Table.
		 */

		active_blk_tbl.list =
				(ACTIVE_BLK_TBL_ELEM **) realloc(
				(char *)active_blk_tbl.list,
				(unsigned int)(sizeof(ACTIVE_BLK_TBL_ELEM *) *
				(active_blk_tbl.count + 1)));

		if (!active_blk_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_blk_tbl.count ++;

		temp_handle = active_blk_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Block Table element where the block
	 *	handle is.
	 */

	active_blk_tbl.list[temp_handle] = new ACTIVE_BLK_TBL_ELEM;

	if (!active_blk_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}
	
	
	set_abt_adt_offset(temp_handle, -1) ;
	set_abt_tag(temp_handle, NULL) ;
	set_abt_operation(temp_handle, CM_IDLE_OP) ;
	memset(&active_blk_tbl.list[temp_handle]->abt_net_spec, 0,
			sizeof(ABT_NET_SPEC)) ;

// 	memset(&active_blk_tbl.list[temp_handle]->block_message, 0,
//			sizeof(COMM_REQ)) ;

	set_abt_param_count(temp_handle,(unsigned)-1);
	set_abt_dd_blk_id(temp_handle,0);
	set_abt_dd_blk_tbl_offset(temp_handle,-1);
	set_abt_app_info(temp_handle,NULL);

	//set_abt_comm_detail(temp_handle, );
  	set_abt_comm_status(temp_handle, 0) ;
	set_abt_db_handle(temp_handle, 0) ;
	//InitializeAbtGenericRefCount(temp_handle);
	
	*block_handle = temp_handle;
	
	return(CM_SUCCESS);
}


void CDeviceTypeMgr::SetDevManufacturerTblOffsetInDevTypeTbl(DEVICE_TYPE_HANDLE dth, DEVICE_MANUFACTURER_HANDLE admto)
{
	active_dev_type_tbl.list[dth]->active_dev_manufacturer_tbl_offset = admto;
}

/***********************************************************************
 *
 *	Name:  ct_new_device
 *
 *	ShortDesc:  Create a new Active Device Table element.
 *
 *	Description:
 *		The ct_new_device function creates a new Active Device Table
 *		element either in the first unused (deallocated) element space
 *		or at the end of the existing Active Device Table list.  The
 *		corresponding device handle will be returned unless there is
 *		an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_handle - handle of the new Active Device Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_new_device(DEVICE_HANDLE *device_handle)
{
	DEVICE_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Device Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_tbl.count) {

		/*
		 *	Extend the Active Device Table.
		 */

		active_dev_tbl.list =
				(ACTIVE_DEV_TBL_ELEM **) realloc(
				active_dev_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TBL_ELEM *) *
				(active_dev_tbl.count + 1)));

		if (!active_dev_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_tbl.count ++;

		temp_handle = active_dev_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Table element where the device
	 *	handle is.
	 */

	active_dev_tbl.list[temp_handle] = new ACTIVE_DEV_TBL_ELEM;

	if (!active_dev_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_handle = temp_handle;

	set_adt_network(*device_handle, -1) ;
	set_adt_adtt_offset(*device_handle, -1) ;
	set_adt_station_address(*device_handle, 0xffff) ;
	set_adt_address_type(*device_handle, 0) ;
	set_adt_operation(*device_handle, CM_IDLE_OP) ;
	set_adt_app_info(*device_handle, NULL) ;
	//set_adt_comm_detail(*device_handle, ) ;
  	set_adt_comm_status(*device_handle, 0) ;
	set_adt_device_tag(*device_handle, NULL) ;
	set_adt_desc(*device_handle, NULL) ;
	set_adt_date(*device_handle, NULL) ;
	set_adt_identified(*device_handle, DEVICE_NOT_IDENTIFIED) ;
	set_adt_usage(*device_handle, 0) ;
 	set_adt_time(temp_handle, 0.0) ;
	set_adt_view(temp_handle, 'h');

	/*
	 * Make sure that the source starts in a good state.
	 */


	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_new_device_type
 *
 *	ShortDesc:  Create a new Active Device Type table element.
 *
 *	Description:
 *		The ct_new_device_type function creates a new Active Device
 *		Type Table element either in the first unused (deallocated)
 *		element space or at the end of the existing Active Device
 *		Type Table list.  The corresponding device type table handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_type_handle - handle of the new Active Device Type
 *							 Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CDeviceTypeMgr::ct_new_device_type(DEVICE_TYPE_HANDLE *device_type_handle)
{
	DEVICE_TYPE_HANDLE	temp_handle;
	ROD_HANDLE			rod_handle = 0;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_type_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_type_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Type Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_type_tbl.count) {

		/*
		 *	Extend the Active Device Type Table.
		 */

		active_dev_type_tbl.list =
				(ACTIVE_DEV_TYPE_TBL_ELEM **) realloc(
				active_dev_type_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TYPE_TBL_ELEM *) *
				(active_dev_type_tbl.count + 1)));

		if (!active_dev_type_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_type_tbl.count ++;

		temp_handle = active_dev_type_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Type Table element where the
	 *	device type handle is.
	 */

	active_dev_type_tbl.list[temp_handle] =	new ACTIVE_DEV_TYPE_TBL_ELEM ;

	if (!active_dev_type_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_type_handle = temp_handle;

	rod_handle = -1 ;

	set_adtt_dd_handle(*device_type_handle, &rod_handle) ;
	set_adtt_dd_dev_tbls(*device_type_handle,0) ;
	set_adtt_syminfo(*device_type_handle,0) ;
	set_adtt_usage(*device_type_handle,1) ;
	set_adtt_compatibility(*device_type_handle, DD_NOT_LOADED) ;
	SetDevManufacturerTblOffsetInDevTypeTbl(*device_type_handle, -1);

	return(CM_SUCCESS);
}

wchar_t *	ActiveDevManufacturerTblElem::
GetMfgDictPath()
{
	return m_MfgDictPath;
}

void ActiveDevManufacturerTblElem::
SetMfgDictPath(const wchar_t* pDictPath)
{
    PS_Wcscpy( m_MfgDictPath, MAX_PATH, pDictPath );
}

DD_MANUFACTURER_ID*	ActiveDevManufacturerTblElem::
GetDeviceManufacturerID()
{
	return &m_Manufacturer_id;
}

bool ActiveDevManufacturerTblElem::
SetDeviceManufacturerID(DD_MANUFACTURER_ID *dmi)
{
	bool retval = false;
	if (dmi !=NULL)
	{
		m_Manufacturer_id.ddid_manufacturer = dmi->ddid_manufacturer;
        PS_Tcscpy(m_Manufacturer_id.base_dd_path, dmi->base_dd_path);

		retval = true;
	}
	return retval;
}

void ActiveDevManufacturerTblElem::
SetDctUsage(int u)
{	
	m_iDct_usage = u;
}

int ActiveDevManufacturerTblElem::
GetDctUsage()
{
	return m_iDct_usage;
}

void ActiveDevManufacturerTblElem::
SetDictionaryTableCreated(bool created)
{
	m_bDct_tbl_created = created;
}

bool ActiveDevManufacturerTblElem::
GetDictionaryTableCreated()
{
	return m_bDct_tbl_created;
}

DictionaryTable* ActiveDevManufacturerTblElem::
GetDictionaryTable()
{
	return m_pDictionary_tbl;
}

void ActiveDevManufacturerTblElem::
SetDictionaryTable(DictionaryTable* dict_tbl)
{
	m_pDictionary_tbl = dict_tbl;
}

ActiveDevManufacturerTblElem::
ActiveDevManufacturerTblElem()
{
	m_bDct_tbl_created =false;
	m_iDct_usage =0;
	m_pDictionary_tbl =NULL;	
    m_MfgDictPath[0] = 0;   // initialize to an empty string
}

ActiveDevManufacturerTblElem::
~ActiveDevManufacturerTblElem()
{
	delete m_pDictionary_tbl;

}

/***********************************************************************
 *
 *	Name: ActiveDevManufacturerTbl::RemoveElement
 *
 *	ShortDesc:  Remove a specific member of the Active Device Manufacturer
 *				Table.
 *
 *	Description:
 *		The RemoveElement function removes a specific element
 *		of the Active Device Manufacturer Table.  Note that the element will
 *		be deallocated but the Active Device Manufacturer Table list will not
 *		be reduced.
 *
 *	Inputs:
 *		device_manufacturer_handle - handle (offset) of the specific Active
 *							 Device Manufacturer Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Ying Xu
 *
 **********************************************************************/

void ActiveDevManufacturerTbl::
RemoveElement(DEVICE_MANUFACTURER_HANDLE device_manufacturer_handle)
{
	/*
	 *	Free the Active Device Manufacturer Table element and null the
	 *	element pointer.
	 */
	if (DeviceManufacturerHandleIsValid(device_manufacturer_handle))
	{
		delete list[device_manufacturer_handle] ;
		list[device_manufacturer_handle] = 0;
	}
}


/***********************************************************************
 *
 *	Name:  ActiveDevManufacturerTbl::SearchDeviceManufacturer
 *
 *	ShortDesc:  Search for a device manufacturer in the Active Device Manufacturer
 *				Table using a DD manufacturer ID.
 *
 *	Description:
 *		The SearchDeviceManufacturer function takes a DD manufacturer ID and
 *		searches through the valid (allocated) Active Device Manufacturer
 *		Table elements in the Active Device Manufacturer Table list, looking
 *		for the element that matches the DD manufacturer ID.  If found, the
 *		corresponding device manufacturer handle will be returned.  Otherwise
 *		an error will be returned.
 *
 *	Inputs:
 *		dd_manufacturer_id - unique identifier of a device manufacturer.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device manufacturer handle of the matching Active Device Manufacturer
 *		Table element if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Ying Xu
 *
 **********************************************************************/
 
DEVICE_MANUFACTURER_HANDLE ActiveDevManufacturerTbl:: 
SearchDeviceManufacturer(DD_MANUFACTURER_ID *dd_manufacturer_id)
{
	DEVICE_MANUFACTURER_HANDLE	device_manufacturer_handle;

	/*
	 *	Go through each element in the Active Device Manufacturer Table.
	 */

	for (device_manufacturer_handle = 0; device_manufacturer_handle <
			count; device_manufacturer_handle ++)
	{

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (list[device_manufacturer_handle])
		{

			/*
			 *	Check the DD manufacturer ID of the element.
			 */

			if ((list[device_manufacturer_handle]->GetDeviceManufacturerID()->ddid_manufacturer == dd_manufacturer_id->ddid_manufacturer) &&
				(wcscmp(list[device_manufacturer_handle]->GetDeviceManufacturerID()->base_dd_path,dd_manufacturer_id->base_dd_path)==0)) 
			{
				return(device_manufacturer_handle);
			}
		}
	}

	return(-1);
}

ActiveDevManufacturerTblElem * ActiveDevManufacturerTbl::
GetElement(DEVICE_MANUFACTURER_HANDLE dmh)
{
	if (DeviceManufacturerHandleIsValid(dmh))
	{
		return list[dmh];
	}
	else
	{
		return nullptr;
	}
}

bool ActiveDevManufacturerTbl::
DeviceManufacturerHandleIsValid(DEVICE_MANUFACTURER_HANDLE dmh)
{
	return ((dmh >= 0) && (dmh < count) && list[dmh]);
}


ActiveDevManufacturerTbl::
~ActiveDevManufacturerTbl()
{
	if (list != NULL)
	{
		free(list);
		list = NULL;
	}
	count = 0;
}

ActiveDevManufacturerTbl::
ActiveDevManufacturerTbl()
{
	count = 0;
	list = NULL;
}


/***********************************************************************
 *
 *	Name:  ActiveDevManufacturerTbl::AddElement
 *
 *	ShortDesc:  Create a new Active Device Manufacturer table element.
 *
 *	Description:
 *		The AddElement function creates a new Active Device
 *		Manufacturer Table element either in the first unused (deallocated)
 *		element space or at the end of the existing Active Device
 *		Manufacturer Table list.  The corresponding deviceManufacturer table handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_Manufacturer_handle - handle of the new Active Device Manufacturer
 *							 Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Ying Xu
 *		
 *
 **********************************************************************/

int
ActiveDevManufacturerTbl::
AddElement(DEVICE_MANUFACTURER_HANDLE *device_manufacturer_handle,
						   DD_MANUFACTURER_ID *dd_manufacturer_id)
{
	DEVICE_MANUFACTURER_HANDLE	temp_handle = 0;

	/*
	 *	Go through each element in the Active Device Manufacturer Table.
	 */

	for (temp_handle = 0; temp_handle < count;
			temp_handle ++)
	{

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!list[temp_handle])
		{
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Manufacturer Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= count)
	{

		/*
		 *	Extend the Active Device Manufacturer Table.
		 */
		ActiveDevManufacturerTblElem ** oldlist = list;
		list =
				(ActiveDevManufacturerTblElem **) realloc(
				list,
				(unsigned int)(sizeof(ActiveDevManufacturerTblElem *) *
				(count + 1)));

		if (!list)
		{
			free (oldlist);
			return(CM_NO_MEMORY);
		}
		count ++;

		temp_handle = count - 1;
	}

	/*
	 *	Allocate a new Active Device Manufacturer Table element where the
	 *	device manufacturer handle is.
	 */

	list[temp_handle] =	new ActiveDevManufacturerTblElem ;

	if (!list[temp_handle])
	{
		return(CM_NO_MEMORY);
	}

	*device_manufacturer_handle = temp_handle;
	
	GetElement(*device_manufacturer_handle)->SetDctUsage(1) ;
	if (!list[*device_manufacturer_handle]->SetDeviceManufacturerID(dd_manufacturer_id))
	{
		return (CM_NO_MEMORY);
	}

	GetElement(*device_manufacturer_handle)->SetDictionaryTableCreated(false);
	GetElement(*device_manufacturer_handle)->SetDictionaryTable(nullptr);

	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  ct_remove_block
 *
 *	ShortDesc:  Remove a specific element of the Active Block Table.
 *
 *	Description:
 *		The ct_remove_block function removes a specific element of
 *		the Active Block Table.  Note that the element will be
 *		deallocated but the Active Block Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		block_handle - handle (offset) of the specific Active Block
 *					   Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ct_remove_block(BLOCK_HANDLE block_handle)
{
	/*
	 *	Check if a tag exits.
	 */


	if (get_abt_tag(block_handle)) {
		free(get_abt_tag(block_handle));
	}
		
	/*
	 *	Free the Active Block Table element and null the element
	 *	pointer.
	 */

	if (active_blk_tbl.list[block_handle])
	{
		delete active_blk_tbl.list[block_handle];
		active_blk_tbl.list[block_handle] = 0;
	}
}


/***********************************************************************
 *
 *	Name:  ct_remove_device
 *
 *	ShortDesc:  Remove a specific member of the Active Device Table.
 *
 *	Description:
 *		The ct_remove_device function removes a specific element of
 *		the Active Device Table.  Note that the element will be
 *		deallocated but the Active Device Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		device_handle - handle (offset) of the specific Active Device
 *						Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter 
 *		Kent Anderson
 *
 **********************************************************************/

void CConnectionMgr::ct_remove_device (DEVICE_HANDLE device_handle)
{	
	
	/*
	 * Free the device tag
	 */

	if (get_adt_device_tag(device_handle)) {
		free(get_adt_device_tag(device_handle)) ;
	}

	/*
	 * Free the device date
	 */
	
	if (get_adt_date(device_handle)) {
		free(get_adt_date(device_handle)) ;
	}

	/*
	 * Free the device descriptor
	 */

	if (get_adt_desc(device_handle)) {
		free(get_adt_desc(device_handle)) ;
	}

	/*
	 *	Free the Active Device Table element and null the element
	 *	pointer.
	 */

	if (active_dev_tbl.list[device_handle])
	{
		delete active_dev_tbl.list[device_handle];
		active_dev_tbl.list[device_handle] = 0;
	}
}


/***********************************************************************
 *
 *	Name:  ct_remove_device_type
 *
 *	ShortDesc:  Remove a specific member of the Active Device Type
 *				Table.
 *
 *	Description:
 *		The ct_remove_device_type function removes a specific element
 *		of the Active Device Type Table.  Note that the element will
 *		be deallocated but the Active Device Type Table list will not
 *		be reduced.
 *
 *	Inputs:
 *		device_type_handle - handle (offset) of the specific Active
 *							 Device Type Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CDeviceTypeMgr::ct_remove_device_type(DEVICE_TYPE_HANDLE device_type_handle)
{
	/*
	 *	Free the Active Device Type Table element and null the
	 *	element pointer.
	 */

	delete active_dev_type_tbl.list[device_type_handle] ;
	active_dev_type_tbl.list[device_type_handle] = 0;
}



/***********************************************************************
 *
 *	Name:  ct_simple_hart_open
 *
 *	ShortDesc:  Open a session with a tagged block.
 *
 *	Description:
 *		The ct_simple_hart_open function takes a block tag and opens a new
 *		session with a block that matches the tag.  A system-wide tag
 *		search may be used if the block was not found in the Active
 *		Block Table.  If successful, the corresponding block handle 
 *		will be returned.  If necessary, the a new active block will be
 *		allocated and filled.  Otherwise, an error code will be 
 *		returned.
 *
 *	Inputs:
 *		simple_open - contains: tag, mfrid, mfr_dev_type, and xmtr_spec_rev
 *
 *	Outputs:
 *		blk_list - the corresponding list of block handles for the tag
 *
 *	Returns:
 *		Zero for success and non-zero for failure
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_simple_hart_open(ENV_INFO *envInfo, wchar_t *dd_file_path, SIMPLE_OPEN *simple_open, BLK_LIST *blk_list)
 {
	HART_IDENTIFY_CNF	find_cnf ;
	int					r_code = -2; // CAY init to unknown error
	int					i ;
	DEVICE_HANDLE		device_handle=-1 ;

	ASSERT_RET(simple_open && simple_open->tag && blk_list, CM_BAD_POINTER) ;

	/* Open block tag */

	envInfo->block_handle = -1 ;

	r_code = ct_block_open(simple_open->tag,envInfo, blk_list);
	

	if (r_code != CM_UPDATE_PENDING) {
		return(r_code) ;
	}


	/*
	 * Note, this is really a kludge to get a block
	 * open without actually doing any comm.
	 */

	for (i = 0; i < blk_list->count ; i++) {

	 	/*
		 * Now, we'll simulate the successful
		 * response of a hart device.
		 */

		r_code = get_abt_adt_offset(blk_list->list[i].block_handle,&device_handle) ;
		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}

		find_cnf.mfrid = simple_open->mfrid ;
		find_cnf.mfr_dev_type = simple_open->mfr_dev_type ;
		find_cnf.req_preambles = 1 ;

		//	universal cmd rev may or may not be specified
		//  (ie. the device simulator needs to fill in the 
		//	hart rev to load a generic dd if the desired dd
		//  is not available.
		if (simple_open->hart_rev > 0) 
		{
			find_cnf.uni_cmd_rev = simple_open->hart_rev;
		}
		else 
		{
			find_cnf.uni_cmd_rev = 1 ;
		}

		find_cnf.xmtr_spec_rev = simple_open->xmtr_spec_rev ;
		find_cnf.soft_rev = 1 ;
		find_cnf.hard_rev = 1 ;
		find_cnf.flags = 1 ;
		find_cnf.device_id[0] = simple_open->device_id[0] ;
		find_cnf.device_id[1] = simple_open->device_id[1] ;
		find_cnf.device_id[2] = simple_open->device_id[2] ;

		//CAY restore the call to this function 
		r_code =  set_abt_hart_find_cnf(dd_file_path, blk_list->list[i].block_handle,&find_cnf) ;

		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}
	}
		
	r_code = ct_block_open(simple_open->tag,envInfo, blk_list);

	if ((r_code != CM_UPDATE_PENDING) && (r_code != CM_SUCCESS)) 
	{
		return(r_code) ;
	}
	
	/*
	 * load the DD because this is a simple open
	 */

	DD_DEVICE_ID* dd_device_id = get_adt_dd_device_id( device_handle );
	
	if( dd_device_id )
	{
		dd_device_id->bUseBasePathUnmodified = simple_open->bUseBasePathUnmodified;
	}
	for (i = 0; i < blk_list->count ; i++) 
	{
		r_code = ds_dd_load(envInfo, blk_list->list[i].block_handle, 1) ;
		if (r_code != CM_SUCCESS) 
		{
			return(r_code) ;
		}
	}

	return(r_code);
}

/***********************************************************************
 *
 *	Name:  ct_block_open
 *
 *	ShortDesc:  Open a session with a tagged block.
 *
 *	Description:
 *		The ct_block_open function takes a block tag and opens a new
 *		session with a block that matches the tag.  A system-wide tag
 *		search may be used if the block was not found in the Active
 *		Block Table.  If successful, the corresponding block handle 
 *		will be returned.  If necessary, the a new active block will be
 *		allocated and filled.  Otherwise, an error code will be 
 *		returned.
 *
 *	Inputs:
 *		tag - tag of the block to be opened.
 *		env_info - information from calling node (i.e. historic time, return PATH etc.)
 *
 *	Outputs:
 *		block_list - the corresponding list of block handles for the tag
 *
 *	Returns:
 *		Zero for success and non-zero for failure
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_block_open(LPTSTR tag, ENV_INFO *env_info, BLK_LIST *block_list)
{
	int					r_code = -2; // CAY init to unknown error
	BLK_LIST_ELEM		*bl_elemp ;
	BLOCK_HANDLE		bh ;
	DEVICE_HANDLE		dh ;
	//NETWORK_HANDLE		nh ;
	int					i ;
	LPTSTR				str = NULL ;

	ASSERT_RET(tag && env_info && env_info->app_info,CM_BAD_POINTER);
	 
	/*
	 *	Search the Active Block Table for the tag.
	 */

	r_code = ct_block_time_search(tag, block_list);
 
	/*
	 *	Check if any blocks were found.
	 */

	if (block_list->count > 0 && r_code == CM_SUCCESS) {
		
		r_code = CM_SUCCESS ;

		for (i = 0, bl_elemp = &block_list->list[0] ; i < block_list->count ; i++, bl_elemp++) {
   
		 
			bh = bl_elemp->block_handle ;

			CM_OP temp_op;
			get_abt_operation(bh, &temp_op);
	  		if ( temp_op == CM_HAVE_VALUE_OP) {
						
				/*
				 * Append the detail string with any errors we might
				 * have.
				 */

				if (str && wcslen(str)) {

					bl_elemp->status = CM_COMM_ERR ;
					r_code = CM_COMM_ERR ;
				}
				else {
				   	bl_elemp->status = CM_SUCCESS ;
				}

				/*
				 * Clear the operation so we're sure that the caller can get
				 * a new one.
				 */

			   set_abt_operation(bh, CM_IDLE_OP) ;
			}
			else if (temp_op != CM_IDLE_OP) {
				/*
				 * Do some stuff so the caller knows when the
				 * block is open.
				 */


				bl_elemp->status= CM_UPDATE_PENDING ;
				r_code = CM_UPDATE_PENDING ;
			}
		}
		return (r_code) ;
	
	}
	else 
	{
			/*
			* make a new block
			*/

		r_code = ct_new_block(&bh) ;
		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}

		r_code = ct_new_device(&dh) ;
		if (r_code != CM_SUCCESS) {
			return(r_code) ;
		}

		/*
			* Connect the new block and the new device,
			* and connect the device to the network
			*/

		set_abt_adt_offset(bh, dh) ;

        set_abt_tag(bh, wcsdup(tag)) ;
		//set_adt_network(dh, nh);  //CAY no networks 

		/*
			* add the block to the block list
			*/
		block_list->count = 1;
		block_list->list[0].block_handle = bh ;
		block_list->list[0].status = CM_UPDATE_PENDING ;
		
	}

	/*
	 *	Perform a tag search on all attached networks
	 * (set the tag in env_info so we can search for it
	 * later).
	 */

	r_code = cm_find_block(tag, env_info, block_list, CM_OPEN_BLOCK_OP);
	

	return(r_code);
}



DEVICE_MANUFACTURER_HANDLE CDeviceTypeMgr::GetDevManufacturerTblOffsetFromDevTypeTbl(DEVICE_TYPE_HANDLE dth) const
{
	DEVICE_MANUFACTURER_HANDLE dmh = -1;
	if (valid_device_type_handle(dth))
	{
		dmh = active_dev_type_tbl.list[dth]->active_dev_manufacturer_tbl_offset;
	}
	return dmh;
}
void CDeviceTypeMgr::DecrementOrDeallocateManufacturer(DEVICE_MANUFACTURER_HANDLE device_manufacturer_handle)
{
	if (active_dev_manufacturer_tbl.DeviceManufacturerHandleIsValid(device_manufacturer_handle))
	{
		if (active_dev_manufacturer_tbl.GetElement(device_manufacturer_handle)->GetDctUsage() > 1) 
		{

			/*
				*	Decrement the device manufacturer's usage.
				*/

			active_dev_manufacturer_tbl.GetElement(device_manufacturer_handle)->SetDctUsage(
					active_dev_manufacturer_tbl.GetElement(device_manufacturer_handle)->GetDctUsage() - 1);
		}

		else 
		{

			/*
				*	Dealocate the device manufacturer.
				*/
			active_dev_manufacturer_tbl.RemoveElement(device_manufacturer_handle);
		}
	}
}

/***********************************************************************
 *
 *	Name:  ct_block_close
 *
 *	ShortDesc:  Close a previously openned session with a block.
 *
 *	Description:
 *		The ct_block_close function takes a block handle and closes
 *		a previously opened session with a block.  The function checks
 *		for the corresponding Active Block, Device, Device Manufacturer and Device Type
 *		elements that are no longer used, and deallocates them.
 *
 *	Inputs:
 *		block_handle - handle of the block to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_BLOCK_CLOSE.
 *		Return values from rod_close function.
 *		Return values from cr_terminate_comm function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
int CConnectionMgr::ct_block_close(ENV_INFO *envInfo, BLOCK_HANDLE block_handle)  
{
	DEVICE_HANDLE				device_handle;
	DEVICE_TYPE_HANDLE			device_type_handle = 0;
	DEVICE_MANUFACTURER_HANDLE	device_manufacturer_handle ;
	int							block_deallocate = 0;
	int							device_deallocate = 0;
	int							device_type_deallocate = 0;
	int							remove_block_dd_info = 0;
	ENV_INFO					env_info ;

	if (!valid_block_handle(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	/*
	 * The block may have closed because we killed off the
	 * block app info so let's check.
	 */

  	if (!valid_block_handle(block_handle)) {
		return(SUCCESS) ;
	}

	/*
	 * We need to deallocate the block because
	 * removing the app info didn't affect the
	 * block.
	 */

	block_deallocate = 1 ;
	ITEM_ID blk_id = 0;
	int blk_tbl_offset;
	get_abt_dd_blk_tbl_offset( block_handle, &blk_tbl_offset);
	get_abt_dd_blk_id( block_handle, &blk_id );
	if (blk_tbl_offset >= 0 &&
		blk_id != 0) {
		remove_block_dd_info = 1 ;
	}

	/*
	 *	Check if the device has multiple sessions.
	 */
	get_abt_adt_offset( block_handle, &device_handle ); 
	if (valid_device_handle(device_handle)) {
		if (get_adt_usage(device_handle) > 1) {
	
			/*
			 *	Decrement the device's usage.
			 */

			set_adt_usage(device_handle,
					get_adt_usage(device_handle) - 1);

		}
		else {

			/*
			 *	Set the flag to deallocate the device.
			 */
	    
			device_deallocate = 1;

			/*
			 *	Check if the device type has multiple sessions.
			 */
		
			device_type_handle = get_adt_adtt_offset(device_handle);
			if (g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) {
				if (g_DeviceTypeMgr.get_adtt_usage(device_type_handle) > 1) {

					/*
					 *	Decrement the device type's usage.
					 */

					g_DeviceTypeMgr.set_adtt_usage(device_type_handle,
							g_DeviceTypeMgr.get_adtt_usage(device_type_handle) - 1);
				}

				else {

					/*
					 *	Set the flag to deallocate the device type.
					 */

					 device_type_deallocate = 1;

					 /*
					 *	Check if the device manufacturer has multiple sessions.
					 */
		
					device_manufacturer_handle = g_DeviceTypeMgr.GetDevManufacturerTblOffsetFromDevTypeTbl(device_type_handle);
					g_DeviceTypeMgr.DecrementOrDeallocateManufacturer(device_manufacturer_handle);
					
				}
			}
		}
	}


	/*
	 *	Check if the block is to be deallocated.
	 */

	if (block_deallocate) {

		/*
		 * The block may have closed because we killed off the
		 * block app info so let's check.
		 */

	  	if (!valid_block_handle(block_handle)) {
			return(SUCCESS) ;
		}

   		/*
		 *	Remove any data connected to the Active Block Table and
		 *	deallocate the block.
		 */

		if (remove_dd_blk_tbl_fn_ptr != 0 && remove_block_dd_info != 0) {
            int	r_code = SUCCESS;  //Restrict to local scope for following call
			r_code = (*remove_dd_blk_tbl_fn_ptr)(envInfo, block_handle);
			ASSERT_RET(((r_code == DDI_DEVICE_TABLES_NOT_FOUND) ||
					(r_code == DDI_BLOCK_TABLES_NOT_FOUND) ||
					(r_code == SUCCESS)) ,CM_BAD_BLOCK_CLOSE);
		}

			
		ct_remove_block(block_handle);
	}

	/*
	 *	Check if the device is to be deallocated.
	 */

	if (device_deallocate) {

		env_info.block_handle = block_handle ;
// The following line was commented out by Conrad
//		memset(&comm_info, 0, sizeof(COMM_INFO)) ; 
		
		
		ct_remove_device(device_handle);
	}

	/*
	 *	Check if the device type is to be deallocated.
	 */

	if (device_type_deallocate) {

		/*
		 *	Remove the DD of the device type, remove any data
		 *	connected to the Active Device Type Table, and
		 *	deallocate the device type.
		 */
		ROD_HANDLE rod_handle = 0;
		g_DeviceTypeMgr.get_adtt_dd_handle( device_type_handle, &rod_handle );
		if (g_DeviceTypeMgr.valid_rod_handle(rod_handle)) {
			int	r_code = SUCCESS;  //Restrict to local scope for following call
			r_code = g_DeviceTypeMgr.rod_close(rod_handle);
			ASSERT_RET((r_code == CM_SUCCESS),r_code);
		}

		if (remove_dd_dev_tbl_fn_ptr != 0) {
            int	r_code = SUCCESS;  //Restrict to local scope for following call
			r_code = (*remove_dd_dev_tbl_fn_ptr)(envInfo, device_type_handle);
			ASSERT_RET((r_code != DDI_DEVICE_TABLES_NOT_FOUND),CM_BAD_BLOCK_CLOSE);
			void* ddt;
			g_DeviceTypeMgr.get_adtt_dd_dev_tbls( device_type_handle, (void**)&ddt );
			ASSERT_DBG(!ddt);
		}
		
        {   // brackets for enforcing local scope only (not associated with any conditional)
            int	r_code = SUCCESS;  //Restrict to local scope for following call
		    r_code = remove_dev_type_info(envInfo, device_type_handle);
		    ASSERT_RET((!r_code),CM_BAD_BLOCK_CLOSE);
        }
		SYMINFO* syminfo = nullptr;
		g_DeviceTypeMgr.get_adtt_syminfo( device_type_handle, &syminfo );
		ASSERT_DBG(!syminfo);
		
		g_DeviceTypeMgr.ct_remove_device_type(device_type_handle);
	}

	return(CM_SUCCESS);
}

