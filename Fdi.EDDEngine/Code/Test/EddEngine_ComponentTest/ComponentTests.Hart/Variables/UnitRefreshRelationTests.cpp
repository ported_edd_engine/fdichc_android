#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass]
        public ref class When_requesting_a_unit_relation_for_an_axis_which_is_part_of_a_unit_relation_fmA
        {
        public:
            static int errorCode;
            static unsigned result;
            static unsigned unit_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001e.fmA");
                engine->Initialize(nullptr);

                //initialize unit item ID
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_unit_relation", &mySpecifier);
                unit_item_id = mySpecifier.id;

                //get the unit id
                engine->FillParamSpecifier("test_axis_with_unit", &mySpecifier);
                ITEM_ID* resultId = new ITEM_ID();
                void* pValueSpec = 0;
                errorCode = engine->Instance->GetAxisUnitRelItemId(0, pValueSpec, mySpecifier.id, resultId);
                result = *resultId;

                //cleanup
                delete engine;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_0_as_error()
            {
                Assert::AreEqual(0, errorCode);
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_unit_relation_s_item_id()
            {
                Assert::AreEqual(unit_item_id, result);
            }
        };

        [TestClass]
        public ref class When_requesting_a_unit_relation_for_an_axis_which_is_not_part_of_a_unit_relation_fmA
        {
        public:
            static int errorCode;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001e.fmA");
                engine->Initialize(nullptr);

                //get item id
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_axis_without_unit", &mySpecifier);

                //get the unit id
                int iBlockInstance = 0;
                ITEM_ID* result = new ITEM_ID();
				void* pValueSpec = 0;
                errorCode = engine->Instance->GetAxisUnitRelItemId(0, pValueSpec, mySpecifier.id, result);

                //cleanup
                delete engine;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_minus_1735_no_unit_as_error()
            {
                Assert::AreEqual(-1735, errorCode);
            }
        };

        [TestClass]
        public ref class When_receiving_a_unit_relation_for_a_parameter_which_is_part_of_a_unit_relation_fmA
        {
        public:
            static int errorCode;
            static unsigned result;
            static unsigned unit_item_id;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001e.fmA");
                engine->Initialize(nullptr);

                //initialize unit item ID
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_unit_relation", &mySpecifier);
                unit_item_id = mySpecifier.id;

                //get the unit id
                engine->FillParamSpecifier("test_double", &mySpecifier);
                ITEM_ID resultId;
                void* pValueSpec = 0;
                errorCode = engine->Instance->GetParamUnitRelItemId(0, pValueSpec, &mySpecifier, &resultId);
                result = resultId;

                //cleanup
                delete engine;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_0_as_error()
            {
                Assert::AreEqual(0, errorCode);
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_unit_relation_s_item_id()
            {
                Assert::AreEqual(unit_item_id, result);
            }
        };

        [TestClass]
        public ref class When_receiving_a_unit_relation_for_a_parameter_which_is_not_part_of_a_unit_relation_fmA
        {
        public:
            static int errorCode;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001E.fmA");
                engine->Initialize(nullptr);

                //get item id
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_float", &mySpecifier);

                //get the unit id
                int iBlockInstance = 0;
                ITEM_ID result;
                void* pValueSpec = 0;
                errorCode= engine->Instance->GetParamUnitRelItemId(iBlockInstance, pValueSpec, &mySpecifier, &result);

                //cleanup
                delete engine;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_return_the_minus_1735_no_unit_as_error()
            {
                Assert::AreEqual(-1735, errorCode);
            }
        };

        [TestClass]
        public ref class When_evaluating_a_unit_relation_fmA
        {
        public:
            static unsigned id_test_unit_relation;
            static unsigned id_test_unit;
            static unsigned id_test_double;
            static unsigned id_test_double2;
            static unsigned id_test_axis;
            static List<UInt32>^ dependent_vars = gcnew List<UInt32>;
            static unsigned dominant_var;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001E.fmA");
                engine->Initialize(nullptr);

                //get item ids
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_unit_relation", &mySpecifier);
                id_test_unit_relation = mySpecifier.id;
                engine->FillParamSpecifier("test_unit", &mySpecifier);
                id_test_unit = mySpecifier.id;
                engine->FillParamSpecifier("test_double", &mySpecifier);
                id_test_double = mySpecifier.id;
                engine->FillParamSpecifier("test_double_2", &mySpecifier);
                id_test_double2 = mySpecifier.id;
                engine->FillParamSpecifier("test_axis_with_unit", &mySpecifier);
                id_test_axis = mySpecifier.id;

                //Get unit relation
                FDI_GENERIC_ITEM* unitItem;
                unitItem = engine->GetGenericItem(id_test_unit_relation, ITYPE_UNIT, FLAT_UNIT::All_Attrs, 0);
                FLAT_UNIT* flat_unit = dynamic_cast<FLAT_UNIT*>(unitItem->item);

                dominant_var = flat_unit->var.op_info.id;
                for(int index = 0; index < flat_unit->var_units.count; index++)
                {
                    dependent_vars->Add(flat_unit->var_units.list[index].op_info.id);
                }

                //cleanup
                delete engine;
                delete unitItem;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_contain_test_unit_s_item_id_as_unit_variable()
            {
                Assert::AreEqual(id_test_unit, dominant_var);
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_contain_the_correct_dependent_variable_ids()
            {
                Assert::AreEqual(3, dependent_vars->Count);
                CollectionAssert::Contains(dependent_vars, id_test_double);
                CollectionAssert::Contains(dependent_vars, id_test_double2);
                CollectionAssert::Contains(dependent_vars, id_test_axis);
            }
        };

        [TestClass]
        public ref class When_evaluating_a_refresh_relation_fmA
        {
        public:
            static unsigned id_test_float;
            static unsigned id_test_double;
            static unsigned id_test_int;
            static unsigned id_test_uint;
            static List<UInt32>^ dominant_vars = gcnew List<UInt32>;
            static List<UInt32>^ dependent_vars = gcnew List<UInt32>;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                //init
                EddEngineController^ engine = gcnew EddEngineController("Hart\\TestCase001E.fmA");
                engine->Initialize(nullptr);

                //get item ids
                FDI_PARAM_SPECIFIER mySpecifier;
                engine->FillParamSpecifier("test_refresh_relation", &mySpecifier);
                ITEM_ID id_test_refresh_relation = mySpecifier.id;
                engine->FillParamSpecifier("test_float", &mySpecifier);
                id_test_float = mySpecifier.id;
                engine->FillParamSpecifier("test_double", &mySpecifier);
                id_test_double = mySpecifier.id;
                engine->FillParamSpecifier("test_int", &mySpecifier);
                id_test_int = mySpecifier.id;
                engine->FillParamSpecifier("test_uint", &mySpecifier);
                id_test_uint = mySpecifier.id;

                //Get refresh relation
                FDI_GENERIC_ITEM* refreshItem;
                refreshItem = engine->GetGenericItem(id_test_refresh_relation, ITYPE_REFRESH, FLAT_REFRESH::All_Attrs, 0);
                FLAT_REFRESH* flat_refresh = dynamic_cast<FLAT_REFRESH*>(refreshItem->item);

                for(int index = 0; index < flat_refresh->update_items.count; index++)
                {
                    dependent_vars->Add(flat_refresh->update_items.list[index].op_info.id);
                }

                for(int index = 0; index < flat_refresh->depend_items.count; index++)
                {
                    dominant_vars->Add(flat_refresh->depend_items.list[index].op_info.id);
                }

                //cleanup
                delete engine;
                delete refreshItem;
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_contain_the_correct_dominant_variable_ids()
            {
                Assert::AreEqual(2, dominant_vars->Count);
                CollectionAssert::Contains(dominant_vars, id_test_double);
                CollectionAssert::Contains(dominant_vars, id_test_float);
            }

            [TestMethod, TestCategory("UnitRelationTests")]
            void It_should_contain_the_correct_dependent_variable_ids()
            {
                Assert::AreEqual(2, dependent_vars->Count);
                CollectionAssert::Contains(dependent_vars, id_test_int);
                CollectionAssert::Contains(dependent_vars, id_test_uint);
            }
        };

    } // namespace Hart
} // namespace ComponentTests
