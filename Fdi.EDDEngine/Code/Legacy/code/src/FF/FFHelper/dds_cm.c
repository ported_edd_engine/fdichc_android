/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	dds_cm.c - DDS/Connection Manager DD Support Module
 *
 */
#include "stdinc.h"

#include <stdlib.h>
#include <sys/stat.h>

#include "cm_lib.h"

#include <AMSDds/tags_sa.h>	
#include "panic.h"
#include "table.h"
#include "DDI_LIB.H"
#include "DDOD_Header.h"
#include "dev_type.h"
#include "ConnectionMgr.h"
#include "RodMgr.h"
#include "DeviceTypeMgr.h"

#define			EXT_FFO L"ffo"
#define			EXT_FF5 L"ff5"


extern char *strdup();


static int	(*build_dd_dev_tbl_fn_ptr) P((ENV_INFO *, DEVICE_HANDLE)) = 0;
static int	(*build_dd_blk_tbl_fn_ptr) P((ENV_INFO *, BLOCK_HANDLE)) = 0;
static int	(*build_blk_app_info_fn_ptr) P((BLOCK_HANDLE)) = 0;
 

/***********************************************************************
 *
 *	Name:  ds_init_dds_tbl_fn_ptrs
 *
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The ds_init_dds_tbl_fn_ptrs function links the pointers of
 *		the DDS functions that the Connection Manager calls when
 *		any DDS tables need to be built.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ds_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)
{
	/*
	 *	Link the pointers of the functions that build DD device
	 *	and DD block tables (DDS Tables).
	 */

	build_dd_dev_tbl_fn_ptr = dds_tbl_fn_ptrs->build_dd_dev_tbl_fn_ptr;
	build_dd_blk_tbl_fn_ptr = dds_tbl_fn_ptrs->build_dd_blk_tbl_fn_ptr;
}

/***********************************************************************
 *
 *	Name:  cm_get_integer
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static UINT32
cm_get_integer(UINT8 *bin_ptr, int bin_size)

{
	UINT32	value = 0;
	int		byte_num;

	/*
	 *	Go through each byte and total up the integer value.
	 */

	for (byte_num = 0; byte_num < bin_size; byte_num ++) {

		value <<= 8;
		/* Codecenter thinks values which equal 0xbf have not been set*/
		/*SUPPRESS 113*/
		value += (UINT32)bin_ptr[byte_num];
	}

	return(value);
}


/***********************************************************************
 *
 *	Name:  ds_get_header
 *
 *	ShortDesc:  Get the DDOD file header from the DDOD file.
 *
 *	Description:
 *		The ds_get_header function takes a DDOD file and gets all of
 *		the header information.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		header - the DDOD file header information. 
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int ds_get_header(FILE *file, DDOD_HEADER *header)
{

	UINT8			header_buffer[HEADER_SIZE];
	int				r_code;

	/*
	 *	Read the DDOD Header into the buffer.
	 */

	r_code = fread((char *)header_buffer, 1, HEADER_SIZE, file);
	if (r_code != HEADER_SIZE) {
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the DDOD Header, and perform verification
	 *	where applicable.
     */

	header->magic_number = cm_get_integer(
			&header_buffer[MAGIC_NUMBER_OFFSET], MAGIC_NUMBER_SIZE);

	if (header->magic_number != 0x7F3F5F77L) {
		return(CM_FILE_ERROR);
	}

	header->header_size = cm_get_integer(
			&header_buffer[HEADER_SIZE_OFFSET], HEADER_SIZE_SIZE);

	header->objects_size = cm_get_integer(
			&header_buffer[OBJECTS_SIZE_OFFSET], OBJECTS_SIZE_SIZE);

	header->data_size = cm_get_integer(
			&header_buffer[DATA_SIZE_OFFSET], DATA_SIZE_SIZE);

	header->manufacturer = cm_get_integer(
			&header_buffer[MANUFACTURER_OFFSET], MANUFACTURER_SIZE);

	header->device_type = (UINT16) cm_get_integer(
			&header_buffer[DEVICE_TYPE_OFFSET], DEVICE_TYPE_SIZE);

	header->device_revision = (UINT8) cm_get_integer(
			&header_buffer[DEVICE_REV_OFFSET], DEVICE_REV_SIZE);

	header->dd_revision = (UINT8) cm_get_integer(
			&header_buffer[DD_REV_OFFSET], DD_REV_SIZE);

	header->reserved1 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED1_OFFSET], RESERVED1_SIZE);

	header->reserved2 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED2_OFFSET], RESERVED2_SIZE);

	header->reserved3 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED3_OFFSET], RESERVED3_SIZE);

	header->reserved4 = (UINT32) cm_get_integer(
			&header_buffer[RESERVED4_OFFSET], RESERVED4_SIZE);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_od_description
 *
 *	ShortDesc:  Get the OD Description from the DDOD file.
 *
 *	Description:
 *		The ds_get_od_description function takes a DDOD file and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file - the DDOD file.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_od_description(FILE *file, OD_DESCRIPTION_SPECIFIC *od_description)
{

    UINT8		od_description_buffer[ODES_SIZE];
    int			r_code;

	/*
	 *	Read the OD Description into the buffer.
	 */

    r_code = fread((char *)od_description_buffer, 1, ODES_SIZE, file);
    if (r_code != ODES_SIZE) {
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the OD Description.
     */

	od_description->ram_rom_flag = (UINT8) cm_get_integer(
			&od_description_buffer[ROM_RAM_FLAG_OFFSET], ROM_RAM_FLAG_SIZE);

	od_description->name_length = (UINT8) cm_get_integer(
			&od_description_buffer[NAME_LENGTH_OFFSET], NAME_LENGTH_SIZE);

	od_description->access_protection_flag = (UINT8) cm_get_integer(
			&od_description_buffer[ACCSS_PROTECT_OFFSET], ACCSS_PROTECT_SIZE);

	od_description->version = (UINT16) cm_get_integer(
			&od_description_buffer[VERSION_OD_OFFSET], VERSION_OD_SIZE);

	od_description->stod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[STOD_LENGTH_OFFSET], STOD_LENGTH_SIZE);

	od_description->sod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_SOD_OFFSET], FIRST_IN_SOD_SIZE);

	od_description->sod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[SOD_LENGTH_OFFSET], SOD_LENGTH_SIZE);

	od_description->dvod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DVOD_OFFSET], FIRST_IN_DVOD_SIZE);

	od_description->dvod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[DVOD_LENGTH_OFFSET], DVOD_LENGTH_SIZE);

	od_description->dpod_first_index = (OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DPOD_OFFSET], FIRST_IN_DPOD_SIZE);

	od_description->dpod_object_count = (UINT16) cm_get_integer(
			&od_description_buffer[DPOD_LENGTH_OFFSET], DPOD_LENGTH_SIZE);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_object
 *
 *	ShortDesc:  Get an object from the DDOD file.
 *
 *	Description:
 *		The ds_get_object function takes a DDOD file and ROD handle
 *		and gets an object from the file and stores it into the
 *		corresponding ROD.
 *
 *	Inputs:
 *		file - the DDOD file.
 *		rod_handle - the handle of the ROD in which the object will
 *					 be stored.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::ds_get_object(FILE *file, ROD_HANDLE rod_handle)
{

	UINT8		 object_buffer[FIXED_SIZE];
	OBJECT		*object;
	UINT8		 extension_length_buffer[EXTEN_LENGTH_SIZE];
	UINT8		 extension_length;
	int			 r_code;

	/*
	 *	Allocate space in the ROD heap for an object, and for a
	 *	domain object's specific information.
	 */

	object = (OBJECT *)rod_calloc(1, sizeof(OBJECT));
	if (!object) {
		return(CM_NO_ROD_MEMORY);
	}

	object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object->specific) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Read the fixed fields of the (domain) object into the buffer.
	 */

    r_code = fread((char *)object_buffer, 1, FIXED_SIZE, file);
    if (r_code != FIXED_SIZE) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get each field in the (domain) object, and perform
	 *	verification where applicable.
	 */

	object->index = (OBJECT_INDEX) cm_get_integer(
			&object_buffer[OBJECT_INDEX_OFFSET], OBJECT_INDEX_SIZE);

	object->code = (UINT8) cm_get_integer(
			&object_buffer[OBJECT_CODE_OFFSET], OBJECT_CODE_SIZE);

	if (object->code != OC_DOMAIN) {
		return(CM_FILE_ERROR);
	}


	object->access.password = (UINT8) cm_get_integer(
			&object_buffer[PASSWORD_OFFSET], PASSWORD_SIZE);

	object->access.groups = (UINT8) cm_get_integer(
			&object_buffer[ACCESS_GROUP_OFFSET], ACCESS_GROUP_SIZE);

	object->access.rights = (UINT16) cm_get_integer(
			&object_buffer[ACCESS_RIGHTS_OFFSET], ACCESS_RIGHTS_SIZE);

	/* 
	 * If password or group contains a value other than 0
	 * the DD is in the new large format. These 2 fields are not used
	 * so instead we are using them as storage for the enlaged DDO information.
	 */

	if(object->access.rights==0x80FD)
	{
		object->specific->domain.size = (UINT32) cm_get_integer(
				&object_buffer[MAX_OCTETS_OFFSET], MAX_OCTETS_SIZE2);
	}
	else
	{
		object->specific->domain.size = (UINT16) cm_get_integer(
				&object_buffer[MAX_OCTETS_OFFSET], MAX_OCTETS_SIZE);
	}

	object->specific->domain.local_address = (UINT32) cm_get_integer(
			&object_buffer[LOCAL_ADDRESS_OFFSET], LOCAL_ADDRESS_SIZE);

	object->specific->domain.state = (UINT8) cm_get_integer(
			&object_buffer[DOMAIN_STATE_OFFSET], DOMAIN_STATE_SIZE);

	object->specific->domain.upload_state = (UINT8) cm_get_integer(
			&object_buffer[UPLOAD_STATE_OFFSET], UPLOAD_STATE_SIZE);

	object->specific->domain.usage = (UINT8) cm_get_integer(
			&object_buffer[COUNTER_OFFSET], COUNTER_SIZE);

	/*
	 *	Read the extension length field of the (domain) object
	 *	into the buffer.
	 */

    r_code = fread((char *)extension_length_buffer, 1, EXTEN_LENGTH_SIZE, file);
    if (r_code != EXTEN_LENGTH_SIZE) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get the extension length field and allocate space in the
	 *	ROD heap for the (domain) object's extension.
	 */

	extension_length = (UINT8) cm_get_integer(
		&extension_length_buffer[EXTEN_LENGTH_OFFSET], EXTEN_LENGTH_SIZE);

	object->extension = (UINT8 *) rod_malloc(
			(int)(EXTEN_LENGTH_SIZE + extension_length));
	if (!object->extension) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the first field in the extension to the length, and
	 *	read in the remaining part of the extension.
	 */

	object->extension[EXTEN_LENGTH_OFFSET] = extension_length;

	r_code = fread((char *)(object->extension + 1), 1,
			(int)extension_length, file);
	if (r_code != extension_length) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Put the (domain) object into the ROD.
	 */

	r_code = rod_put(rod_handle, object->index, object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_object_value
 *
 *	ShortDesc:  Get an object's value.
 *
 *	Description:
 *		The ds_get_object_value function takes a DDOD file, DDOD file
 *		header, and object and gets the objects value.  It will attach
 *		the object's value to the object (both being in the ROD).
 *
 *	Inputs:
 *		file - the DDOD file.
 *		header - the header of the DDOD file.
 *		object - the object whose value will be retrieved.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::ds_get_object_value(FILE *file, DDOD_HEADER *header, OBJECT *object)
{
	UINT32		 offset;
	UINT8		*domain_data;
	int			 r_code;

	/*
	 *	Check if there is no object to get a value for, or no
	 *	domain data to get.
	 */

	if (!object) {
		return(CM_SUCCESS);
	}
	if (!object->specific) {
		return(CM_BAD_OBJECT);
	}
	if (!object->specific->domain.size) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}
	if (object->specific->domain.local_address == 0xFFFFFFFF) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}

	/*
	 *	Calculate the offset into the DDOD file where the domain
	 *	data is located.
	 */

	offset = object->specific->domain.local_address +
			header->header_size + header->objects_size;

	/*
	 *	Allocate space in the ROD heap for the domain data.
	 */

	domain_data = (UINT8 *)rod_malloc((int)object->specific->domain.size);
	if (!domain_data) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the file position and read in the domain data.
	 */

	r_code = fseek(file, (long)offset, 0);
	if (r_code < 0) {
		return(CM_FILE_ERROR);
	}
	r_code = fread((char *)domain_data, 1,
			(int)object->specific->domain.size, file);
	
	if (r_code != (int)object->specific->domain.size) {
		return(CM_FILE_ERROR);
	}

	/*
	 *	Connect the domain data to the ROD object.
	 */

	object->value = domain_data;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_ddod_file_load
 *
 *	ShortDesc:  Load a DDOD file.
 *
 *	Description:
 *		The ds_ddod_file_load function takes a DDOD file name and loads
 *		the corresponding DDOD file into a ROD.
 *
 *	Inputs:
 *		filename - the name of the DDOD file that is to be loaded.
 *
 *	Outputs:
 *		rod_handle - the handle of the ROD in which the DDOD was
 *					 loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::ds_ddod_file_load(wchar_t *filename, ROD_HANDLE *rod_handle)
{

	//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;

	FILE						*file;
	DDOD_HEADER					 header;
	OBJECT						*object_0;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 object_num;
	int							 r_code;

	/*
	 *	Open the DDOD file and get the Header information.
	 */

	file = PS_wfopen(filename, L"rb");
	if (!file) {
		return(CM_BAD_FILE_OPEN);
	}
/*
	printf("DDOD open: %s\n", filename);
*/
	r_code = ds_get_header(file, &header);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Allocate space in the ROD heap for an object, and set it up
	 *	as an OD description object.
	 */

	object_0 = (OBJECT *)rod_calloc(1, sizeof(OBJECT));
	if (r_code) {
		return(CM_NO_ROD_MEMORY);
	}

	object_0->index = 0;
	object_0->code = OC_OD_DESCRIPTION;

	/*
	 *	Allocate space in the ROD heap for an OD description object's
	 *	specific information, and set it up to describe the DDROD.
	 */

	object_0->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object_0->specific) {
		return(CM_NO_ROD_MEMORY);
	}
	od_description = (OD_DESCRIPTION_SPECIFIC *)object_0->specific;

	r_code = ds_get_od_description(file, od_description);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Open up the DDROD with object 0.
	 */

	*rod_handle = rod_new_rod((LPWSTR)filename);

	if (*rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);

	}
	
	*rod_handle = rod_open(object_0,*rod_handle);
	if (*rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);
	}

	/*
	 *	Load all of the objects in the S-OD.  Note that these will
	 *	all be domain objects.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object(file, *rod_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Load all of the object's values in the S-OD.  Note that these
	 *	will all be domain data.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object_value(file, &header, 
				rod_tbl.list[*rod_handle]->sod[object_num]);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	r_code = fclose(file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}

	return(CM_SUCCESS);
}

int CDeviceTypeMgr::ds_ddod_file_load(wchar_t *filename, ROD_HANDLE *rod_handle)
{
	return m_RodMgr.ds_ddod_file_load(filename,rod_handle);
}


/***********************************************************************
 *
 *	Name:  ds_ddod_load
 *
 *	ShortDesc:  Load a DDOD.
 *
 *	Description:
 *		The ds_ddod_load function takes a device handle and search
 *		mode, and loads the corresponding DDOD into a ROD.  Only DDOD
 *		file loading is supported (currently there is no support for
 *		obtaining a DDOD over a network).
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_FILE_NOT_FOUND.
 *		Return values from ddod_file_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
/*************************** MODIFICATION LOG **************************
Date			Author		Modifictaion
22-Aug-2012		Irfan		Function now uses binary file name provided in
							DD_DEVICE_ID to locate and load DD file.
***********************************************************************/

int CConnectionMgr::ds_ddod_load(DEVICE_HANDLE device_handle, ROD_HANDLE *rod_handle)
{
	ROD_HANDLE			 temp_rod_handle = 0;

	DD_DEVICE_ID		*dd_device_id;
	int					 r_code;

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	dd_device_id = (DD_DEVICE_ID *)ADT_DD_DEVICE_ID(device_handle);


	/*
	 *	Load the DDOD file into a ROD.
	 */
	r_code = g_DeviceTypeMgr.ds_ddod_file_load(dd_device_id->dd_path, &temp_rod_handle); 

	/*
	 *	Put the DD Handle into the Active Device Type Table.
	 */

	SET_ADT_DD_HANDLE(device_handle, temp_rod_handle);
	*rod_handle = temp_rod_handle;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_device_load
 *
 *	ShortDesc:  Load a DD for a device.
 *
 *	Description:
 *		The ds_dd_device_load function takes a device handle and loads
 *		the corresponding DD for the device.  After the DD is loaded,
 *		the DD device tables will be built.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_DEV_LOAD.
 *		Return values from ds_ddod_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_device_load(ENV_INFO *env_info, DEVICE_HANDLE device_handle, ROD_HANDLE *rod_handle)
{

	int		r_code;
	DDI_DEVICE_DIR_REQUEST ddi_dir_req;
	FLAT_DEVICE_DIR	*flat_device_dir;

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	/*
	 *	Load a DDOD. Find the most recent DD rev
	 */

	r_code = ds_ddod_load(device_handle,  rod_handle); 
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Build the DD device tables.
	 */

	if (build_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*build_dd_dev_tbl_fn_ptr)(env_info, ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(r_code);
		}

		memset((char*)&ddi_dir_req, 0, sizeof(DDI_DEVICE_DIR_REQUEST));
		ddi_dir_req.type = DD_DT_HANDLE;
		ddi_dir_req.mask = STRING_TBL_MASK;

		r_code = g_DeviceTypeMgr.get_adtt_dd_dev_tbls(ADT_ADTT_OFFSET(device_handle), (void **)&flat_device_dir) ;
		if (r_code != SUCCESS)
		{
			return(r_code) ;
		}

		r_code = ddi_device_dir_request(env_info, &ddi_dir_req, flat_device_dir);
		if (r_code != SUCCESS)
		{
			return (r_code);
		}
	}

	/*
	 *	Build any device type specific Application information.
	 */
	//DEVICE_TYPE_HANDLE *dth;
	
	r_code = load_sym_info(env_info, ADT_ADTT_OFFSET(device_handle));
	
	if (r_code) 
	{	
		return(CM_BAD_DD_DEV_LOAD);
	}
	

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_block_load
 *
 *	ShortDesc:  Load a DD for a block.
 *
 *	Description:
 *		The ds_dd_block_load function takes a block handle and loads
 *		the corresponding DD for the block.  Loading a DD for a block
 *		consists of first loading the DD for the device (if not already
 *		done), and then building the DD block tables.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose DD is to be
 *					   loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_NO_DD_BLK_REF.
 *		CM_BAD_DD_BLK_LOAD.
 *		Return values from ds_dd_device_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_block_load(ENV_INFO *env_info, BLOCK_HANDLE block_handle, ROD_HANDLE *rod_handle)
{

	int			r_code;

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!valid_block_handle(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that there is a DD Block ID.
	 */

	ITEM_ID temp_id = 0;
	get_abt_dd_blk_id( block_handle, &temp_id );
	if (temp_id == 0) 
	{
		return(CM_NO_DD_BLK_REF);
	}

	/*
	 *	Check if the DD has been loaded for the device.
	 */
	ROD_HANDLE temp_rod_handle;
	get_abt_dd_handle( block_handle, &temp_rod_handle );
	if (!valid_dd_handle(temp_rod_handle)) 
	{
		DEVICE_HANDLE dev_handle;
		get_abt_adt_offset( block_handle, &dev_handle );

		r_code = ds_dd_device_load(env_info, dev_handle,
				rod_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Build the DD block tables.
	 */

	if (build_dd_blk_tbl_fn_ptr != 0) {
		r_code = (*build_dd_blk_tbl_fn_ptr)(env_info,block_handle);
		if (r_code) {
			return(r_code);
		}
	}
	
	/*
	 *	Build any block specific Application information.
	 */

	if (build_blk_app_info_fn_ptr != 0) {
		r_code = (*build_blk_app_info_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}

	get_abt_dd_handle( block_handle, &temp_rod_handle );

	*rod_handle = temp_rod_handle;
	return CM_SUCCESS;
}


/***********************************************************************
 *
 *	Name:  ds_dd_device_load_ex
 *
 *	ShortDesc:  Load a DD for a device (explicit DD revision used).
 *
 *	Description:
 *		The ds_dd_device_load_ex function takes a device handle and
 *		loads the corresponding DD for the device.  After the DD is
 *		loaded, the DD device tables will be built.
 *
 *		NOTE:	The DD that will be loaded is determined by the entire
 *				(4 number) Device ID in the Active Device Type Table.
 *				That is, no search will be performed for the latest DD.
 *
 *	Inputs:
 *		device_handle - the handle of the device whose DD is to be
 *						loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_DEVICE_HANDLE.
 *		CM_BAD_DD_DEV_LOAD.
 *		Return values from ds_ddod_load function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_device_load_ex(ENV_INFO * env_info, DEVICE_HANDLE device_handle, ROD_HANDLE *rod_handle) 
{

	int		r_code;

	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	/*
	 *	Load a DDOD.  Load the DD with the specified DD rev and file extension.
	 */

	r_code = ds_ddod_load(device_handle, rod_handle); 
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Build the DD device tables.
	 */

	if (build_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*build_dd_dev_tbl_fn_ptr)(env_info, ADT_ADTT_OFFSET(device_handle));
		if (r_code) {
			return(CM_BAD_DD_DEV_LOAD);
		}
	}

	/*
	 *	Build any device type specific Application information.
	 */
	

	
	r_code = load_sym_info(env_info, ADT_ADTT_OFFSET(device_handle));
	
	if (r_code) 
	{	
		return(CM_BAD_DD_DEV_LOAD);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_dd_block_load_ex
 *
 *	ShortDesc:  Load a DD for a block (explicit DD revision used).
 *
 *	Description:
 *		The ds_dd_block_load_ex function takes a block handle and loads
 *		the corresponding DD for the block.  Loading a DD for a block
 *		consists of first loading the DD for the device (if not already
 *		done), and then building the DD block tables.
 *
 *		NOTE:	The DD that will be loaded is determined by the entire
 *				(4 number) Device ID in the Active Device Type Table.
 *				That is, no search will be performed for the latest DD.
 *
 *	Inputs:
 *		block_handle - the handle of the block whose DD is to be
 *					   loaded.
 *
 *	Outputs:
 *		dd_handle - the handle of the DD that was loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_NO_DD_BLK_REF.
 *		CM_BAD_DD_BLK_LOAD.
 *		Return values from ds_dd_device_load_ex function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ds_dd_block_load_ex(ENV_INFO *env_info, BLOCK_HANDLE block_handle, ROD_HANDLE *rod_handle)
{

	int			r_code;

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!valid_block_handle(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that there is a DD Block ID.
	 */

	if (ABT_DD_BLK_ID(block_handle) == 0) {
		return(CM_NO_DD_BLK_REF);
	}

	/*
	 *	Check if the DD has been loaded for the device.
	 */

	if (!valid_dd_handle((ABT_DD_HANDLE(block_handle)))) {
		r_code = ds_dd_device_load_ex(env_info, ABT_ADT_OFFSET(block_handle), rod_handle); 
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Build the DD block tables.
	 */

	if (build_dd_blk_tbl_fn_ptr != 0) {
		r_code = (*build_dd_blk_tbl_fn_ptr)(env_info, block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}
	
	/*
	 *	Build any block specific Application information.
	 */

	if (build_blk_app_info_fn_ptr != 0) {
		r_code = (*build_blk_app_info_fn_ptr)(block_handle);
		if (r_code) {
			return(CM_BAD_DD_BLK_LOAD);
		}
	}


	*rod_handle = ABT_DD_HANDLE(block_handle);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  rod_get
 *
 *	ShortDesc:  Get an object from a ROD.
 *
 *	Description:
 *		The rod_get function takes a ROD handle and object index and
 *		gets the corresponding object.  The (requested) object output
 *		is a pointer to an object.  This object and any of its
 *		allocated contents reside in the ROD.
 *
 *	Inputs:
 *		rod_handle - the handle of the ROD that the object exists in.
 *		object_index - the index of the requested object.
 *
 *	Outputs:
 *		object - the requested object.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_INVALID_ROD_HANDLE.
 *		CM_NO_OBJECT.
 *		Return values from rod_object_lookup function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)

{
	int		r_code;

	/*
	 *	Check to make sure that the ROD handle is valid.
	 */

	if (!valid_rod_handle(rod_handle)) {
		return(CM_INVALID_ROD_HANDLE);
	}

	/*
	 *	Point to the object.
	 */

	r_code = rod_object_lookup(rod_handle, object_index, object);
	if (r_code != CM_SUCCESS) {
		return (r_code);
	}

	/*
	 *	Check to make sure that the object exists.
	 */

	if (!*object) {
		return (CM_NO_OBJECT);
	}

    /* Check the object version and update the ROD version if necessary */
    if ((*object)->access.rights == 0x80FD)
        rod_set_version(rod_handle, 0x0500);


	return(CM_SUCCESS);
}

int CDeviceTypeMgr::rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object)
{
	return m_RodMgr.rod_get(rod_handle,object_index,object);
}
