// dllmain.h : Declaration of module class.

class CEDDEngineCOMModule : public ATL::CAtlDllModuleT< CEDDEngineCOMModule >
{
public :
	DECLARE_LIBID(LIBID_EDDEngineCOMLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_EDDENGINECOM, "{A75E2100-7EE4-4EA6-8EF3-F1C45BFE8813}")
};

extern class CEDDEngineCOMModule _AtlModule;
