#include <mutex>
#include <list>
#include<memory>

//Forward Declaration
class CDeviceList;

class CProtectedDeviceList
{
	public:
		~CProtectedDeviceList(void);
		static CProtectedDeviceList* Instance();
		std::list<CDeviceList *>* LockList();
		void ReleaseList();

    private:
		
		std::mutex m_cs;
		CProtectedDeviceList();
		std::list<CDeviceList *>* m_list;
		static std::mutex m_mutex;			//protecting m_pInstance
		static std::unique_ptr<CProtectedDeviceList>  m_pInstance;

};
