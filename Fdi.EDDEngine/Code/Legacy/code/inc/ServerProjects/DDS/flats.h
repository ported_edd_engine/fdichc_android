/*
 * @(#) $Id: flats.h,v 1.1 1995/05/02 20:44:55 donmarq Exp $
 */

#ifndef FLATS_H
#define FLATS_H

#include "attrs.h"


/*
 *	FLAT structures
 */


typedef struct {

	ulong           bin_exists;	/* There is binary for this attribute */

	ulong           bin_hooked;	/* Binary is attached for this
					 * attribute */

	ulong           attr_avail;	/* The attribute has been evaluated	 */

	ulong           dynamic;/* The attribute is dynamic */

}               FLAT_MASKS;



/*
 * VARIABLE item
 */

typedef struct {

	DEPBIN         *db_pre_edit_act;
	DEPBIN         *db_post_edit_act;

	DEPBIN         *db_pre_read_act;
	DEPBIN         *db_post_read_act;

	DEPBIN         *db_pre_write_act;
	DEPBIN         *db_post_write_act;

    DEPBIN         *db_refresh_act;

}               VAR_ACTIONS_DEPBIN;

typedef struct {

	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;

	ITEM_ID_LIST    pre_read_act;
	ITEM_ID_LIST    post_read_act;

	ITEM_ID_LIST    pre_write_act;
	ITEM_ID_LIST    post_write_act;

	ITEM_ID_LIST    refresh_act;

	VAR_ACTIONS_DEPBIN *depbin;

}               FLAT_VAR_ACTIONS;


typedef struct {

	DEPBIN         *db_unit;
	DEPBIN         *db_read_time_out;
	DEPBIN         *db_write_time_out;
	DEPBIN         *db_min_val;
	DEPBIN         *db_max_val;
	DEPBIN         *db_scale;
	DEPBIN         *db_valid;
	DEPBIN		   *db_debug_info;
	DEPBIN         *db_time_format;
	DEPBIN         *db_time_scale;
}               VAR_MISC_DEPBIN;

typedef struct {

	STRING          unit;	/**/
	unsigned long   read_time_out;	/**/
	unsigned long   write_time_out;	/**/
	RANGE_DATA_LIST min_val;/**/
	RANGE_DATA_LIST max_val;/**/
	EXPR            scale;	/* scale factor */
	ulong           valid;	/* validity */
	STRING			symbol_name; /* symbol name of item */
	STRING			time_format;
	ulong			time_scale;

	VAR_MISC_DEPBIN *depbin;

}               FLAT_VAR_MISC;

typedef struct {

	DEPBIN         *db_class;
	DEPBIN         *db_handling;
	DEPBIN         *db_help;
	DEPBIN         *db_label;
	DEPBIN         *db_type_size;
	DEPBIN         *db_display;
	DEPBIN         *db_edit;
	DEPBIN         *db_enums;
	DEPBIN         *db_index_item_array;
	DEPBIN         *db_resp_codes;
	DEPBIN         *db_default_value;

}               VAR_DEPBIN;



typedef struct {


	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	ulong           class_attr;
	ulong           handling;
	STRING          help;
	STRING          label;
	TYPE_SIZE       type_size;
	STRING          display;
	STRING          edit;
	ENUM_VALUE_LIST enums;
	ITEM_ID         index_item_array;
	ITEM_ID         resp_codes;
	EXPR            default_value;


	VAR_DEPBIN     *depbin;

	FLAT_VAR_ACTIONS *actions;

	FLAT_VAR_MISC  *misc;


}               FLAT_VAR;



typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_min_value;
    DEPBIN      *db_max_value;
    DEPBIN      *db_scaling;
    DEPBIN      *db_constant_unit;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               AXIS_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
    EXPR            min_value;
	OP_REF			min_value_ref;	// min_value == min_axis
    EXPR            max_value;
	OP_REF			max_value_ref;	// min_value = min_axis
	ulong			scaling;
	STRING          constant_unit;
	ulong           valid;	/* validity */
	STRING			symbol_name;
    AXIS_DEPBIN     *depbin;
}               FLAT_AXIS;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_cycle_time;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_length;
    DEPBIN      *db_type;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               CHART_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    ulong           height;
    ulong           width;
    ulong           cycle_time;
    ulonglong       length;
    ulong           type;
	ulong           valid;	/* validity */
	STRING			symbol_name;
    CHART_DEPBIN    *depbin;
}  FLAT_CHART;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
	DEPBIN		*db_debug_info;
}               FILE_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
	STRING			symbol_name;
    FILE_DEPBIN     *depbin;
}  FLAT_FILE;


typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_cycle_time;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_x_axis;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               GRAPH_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    ulong           height;
    ulong           width;
    ITEM_ID         x_axis;
	ulong           valid;	/* validity */
    ulong           cycle_time;
	STRING			symbol_name;
    GRAPH_DEPBIN    *depbin;
}  FLAT_GRAPH;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_vectors;
    DEPBIN      *db_height;
    DEPBIN      *db_width;
    DEPBIN      *db_handling;
    DEPBIN      *db_valid;
    DEPBIN      *db_orientation;
	DEPBIN		*db_debug_info;
}               GRID_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	VECTOR_LIST     vectors;
    ulong           height;
    ulong           width;
    ulong			handling;
    ulong			valid;
    ulong			orientation;
	STRING			symbol_name;
    GRID_DEPBIN     *depbin;
}  FLAT_GRID;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_type;
    DEPBIN      *db_capacity;
    DEPBIN      *db_count;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               LIST_DEPBIN;
typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
    ITEM_ID         type;
    ulong           capacity;
    ulong           count;
	OP_REF			count_ref;
	ulong           valid;	/* validity */
	STRING			symbol_name;
    LIST_DEPBIN     *depbin;
}  FLAT_LIST;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_members;
    DEPBIN      *db_emphasis;
    DEPBIN      *db_y_axis;
    DEPBIN      *db_line_type;
    DEPBIN      *db_line_color;
	DEPBIN      *db_init_actions;
    DEPBIN      *db_refresh_actions;
	DEPBIN      *db_exit_actions;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               SOURCE_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	OP_MEMBER_LIST  members;
    ulong           emphasis;
    ITEM_ID         y_axis;
    ulong           line_type;
    ulong           line_color;
	ITEM_ID_LIST    init_actions;
	ITEM_ID_LIST    refresh_actions;
	ITEM_ID_LIST    exit_actions;
	ulong           valid;	/* validity */
	STRING			symbol_name;
    SOURCE_DEPBIN   *depbin;
}  FLAT_SOURCE;

typedef struct {
    DEPBIN      *db_help;
    DEPBIN      *db_label;
    DEPBIN      *db_emphasis;
    DEPBIN      *db_line_type;
    DEPBIN      *db_handling;
    DEPBIN      *db_init_actions;
    DEPBIN      *db_refresh_actions;
	DEPBIN      *db_exit_actions;
    DEPBIN      *db_type;
    DEPBIN      *db_x_initial;
    DEPBIN      *db_x_increment;
    DEPBIN      *db_number_of_points;
    DEPBIN      *db_x_values;
    DEPBIN      *db_y_values;
    DEPBIN      *db_key_x_values;
    DEPBIN      *db_key_y_values;
    DEPBIN      *db_line_color;
    DEPBIN      *db_y_axis;
	DEPBIN      *db_valid;
	DEPBIN		*db_debug_info;
}               WAVEFORM_DEPBIN;
typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
	STRING          help;
	STRING          label;
	MEMBER_LIST     members;
    ulong           emphasis;
    ulong           line_type;
    ulong           handling;
	ITEM_ID_LIST    init_actions;
	ITEM_ID_LIST    refresh_actions;
	ITEM_ID_LIST    exit_actions;
    ulong           type;
    EXPR            x_initial;
    EXPR            x_increment;
    ulong           number_of_points;
   	DATA_ITEM_LIST  x_values;
    DATA_ITEM_LIST  y_values;
	DATA_ITEM_LIST  key_x_values;
    DATA_ITEM_LIST  key_y_values;
    ulong           line_color;
	ITEM_ID			y_axis;
	ulong           valid;	/* validity */
	STRING			symbol_name;
    WAVEFORM_DEPBIN     *depbin;
}  FLAT_WAVEFORM;

typedef struct {
    DEPBIN          *db_help;
    DEPBIN          *db_label;
	DEPBIN          *db_path;
	DEPBIN          *db_link;
	DEPBIN          *db_valid;
	DEPBIN			*db_debug_info;
}               IMAGE_DEPBIN;

typedef struct {
    ITEM_ID         id;
    FLAT_MASKS      masks;
    STRING          label;
    STRING          help;
    ulong			path;
	BINARY_IMAGE	entry;
    ITEM_ID			link;
	ulong           valid;
	STRING			symbol_name;
    IMAGE_DEPBIN    *depbin;
}               FLAT_IMAGE;

/*
 * PROGRAM item
 */

typedef struct {
	DEPBIN         *db_args;
	DEPBIN         *db_resp_codes;

}               PROGRAM_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	DATA_ITEM_LIST  args;
	ITEM_ID         resp_codes;

	PROGRAM_DEPBIN *depbin;

}               FLAT_PROGRAM;

/*
 * MENU item
 */

typedef struct {

	DEPBIN         *db_label;
	DEPBIN         *db_help;
	DEPBIN         *db_items;
	DEPBIN         *db_valid;
	DEPBIN         *db_style;
	DEPBIN		   *db_debug_info;

}               MENU_DEPBIN;


typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	STRING          label;
	STRING          help;
	ulong			valid;
	ulong			style;
	MENU_ITEM_LIST  items;
	STRING			symbol_name;

	MENU_DEPBIN    *depbin;

}               FLAT_MENU;


/*
 * EDIT_DISPLAY item
 */

typedef struct {

	DEPBIN			*db_disp_items;
	DEPBIN			*db_edit_items;
	DEPBIN			*db_label;
	DEPBIN			*db_help;
	DEPBIN			*db_valid;
	DEPBIN			*db_pre_edit_act;
	DEPBIN			*db_post_edit_act;
	DEPBIN			*db_debug_info;

}               EDIT_DISPLAY_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	OP_REF_TRAIL_LIST disp_items;
	OP_REF_TRAIL_LIST edit_items;
	STRING          label;
	STRING			help;
	ulong			valid;
	ITEM_ID_LIST    pre_edit_act;
	ITEM_ID_LIST    post_edit_act;
	STRING			symbol_name;
	EDIT_DISPLAY_DEPBIN *depbin;

}               FLAT_EDIT_DISPLAY;


/*
 * METHOD item
 */

typedef struct {


	DEPBIN			*db_class;
	DEPBIN			*db_def;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_valid;
	DEPBIN			*db_scope;
	DEPBIN			*db_type;
	DEPBIN			*db_params;
	DEPBIN			*db_debug_info;

}               METHOD_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	ulong           class_attr ;  
	DEFINITION      def;
	STRING          help;
	STRING          label;	 
	ulong           valid;
	ulong			scope;
	ulong			type;
	STRING			params;
	STRING			symbol_name;

	METHOD_DEPBIN  *depbin;

}               FLAT_METHOD;

/*
 * REFRESH item
 */

typedef struct {

	DEPBIN          *db_items;
	DEPBIN			*db_debug_info;

}               REFRESH_DEPBIN;

typedef struct {

	ITEM_ID				id;	/* Item ID for this item */
	FLAT_MASKS			masks;
	REFRESH_RELATION	items;
	STRING				symbol_name;

	REFRESH_DEPBIN *depbin;

}               FLAT_REFRESH;


/*
 * UNIT item
 */

typedef struct {

	DEPBIN			*db_items;
	DEPBIN			*db_debug_info;

}               UNIT_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	UNIT_RELATION   items;
	STRING			symbol_name;
	UNIT_DEPBIN		*depbin;

}               FLAT_UNIT;


/*
 * WRITE AS ONE item
 */

typedef struct {

	DEPBIN			*db_items;
	DEPBIN			*db_debug_info;

}               WAO_DEPBIN;

typedef struct {

	ITEM_ID				id;	/* Item ID for this item */
	FLAT_MASKS			masks;
	OP_REF_TRAIL_LIST	items;
	STRING				symbol_name;
	WAO_DEPBIN			*depbin;

}               FLAT_WAO;



/*
 * ITEM_ARRAY item
 */

typedef struct {

	DEPBIN			*db_elements;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_valid;
	DEPBIN			*db_debug_info;

}               ITEM_ARRAY_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
	ITEM_ARRAY_ELEMENT_LIST elements;
	STRING          help;
	STRING          label;
	ulong			valid;
	STRING			symbol_name;

	ITEM_ARRAY_DEPBIN *depbin;

}               FLAT_ITEM_ARRAY;

/*
 * ARRAY item
 */

typedef struct {

	DEPBIN			*db_num_of_elements;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_type;
	DEPBIN			*db_resp_codes;
	DEPBIN			*db_valid;
	DEPBIN			*db_debug_info;

}               ARRAY_DEPBIN;


typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ulong           num_of_elements;
	STRING          help;
	STRING          label;
	ITEM_ID         type;
	ITEM_ID         resp_codes;
	ulong			valid;
	STRING			symbol_name;

	ARRAY_DEPBIN   *depbin;

}               FLAT_ARRAY;

/*
 * COLLECTION item
 */

typedef struct {

	DEPBIN			*db_members;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_valid;
	DEPBIN			*db_debug_info;

}               COLLECTION_DEPBIN;


typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_TYPE		subtype;
	OP_MEMBER_LIST	members;
	STRING          help;
	STRING          label;
	ulong           valid;	/* validity */
	STRING			symbol_name;

	COLLECTION_DEPBIN *depbin;

}               FLAT_COLLECTION;

/*
 * RECORD item
 */

typedef struct {

	DEPBIN			*db_members;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_resp_codes;
	DEPBIN			*db_debug_info;

}               RECORD_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
	STRING			symbol_name;

	RECORD_DEPBIN  *depbin;

}               FLAT_RECORD;

/*
 * VARIABLE	LIST item
 */

typedef struct {

	DEPBIN			*db_members;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_resp_codes;
	DEPBIN			*db_debug_info;
}               VAR_LIST_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	MEMBER_LIST     members;
	STRING          help;
	STRING          label;
	ITEM_ID         resp_codes;
	STRING			symbol_name;

	VAR_LIST_DEPBIN *depbin;

}               FLAT_VAR_LIST;


/*
 * BLOCK item
 */

typedef struct {

	DEPBIN			*db_characteristic;
	DEPBIN			*db_help;
	DEPBIN			*db_label;
	DEPBIN			*db_param;
	DEPBIN			*db_param_list;
	DEPBIN			*db_debug_info;
	DEPBIN			*db_item_array;
	DEPBIN			*db_collect;
	DEPBIN			*db_menu;
	DEPBIN			*db_edit_disp;

	DEPBIN			*db_method;
	DEPBIN			*db_unit;
	DEPBIN			*db_refresh;
	DEPBIN			*db_wao;
	DEPBIN			*db_axis;
	DEPBIN			*db_chart;
	DEPBIN			*db_file;
	DEPBIN			*db_graph;
	DEPBIN			*db_list;
	DEPBIN			*db_source;
	DEPBIN			*db_waveform;

}               BLOCK_DEPBIN;


typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS      masks;
	ITEM_ID         characteristic;
	STRING          help;
	STRING          label;
	MEMBER_LIST     param;
	MEMBER_LIST     param_list;
	STRING			symbol_name;

	ITEM_ID_LIST    item_array;
	ITEM_ID_LIST    collect;
	ITEM_ID_LIST    menu;
	ITEM_ID_LIST    edit_disp;

	ITEM_ID_LIST    method;
	ITEM_ID_LIST    unit;
	ITEM_ID_LIST    refresh;
	ITEM_ID_LIST    wao;
	BLOCK_DEPBIN   *depbin;
}               FLAT_BLOCK;

/*
 * RESPONSE CODE item
 */

typedef struct {

	DEPBIN         *db_member;

}               RESP_CODE_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	RESPONSE_CODE_LIST member;

	RESP_CODE_DEPBIN *depbin;

}               FLAT_RESP_CODE;

/*
 * DOMAIN item
 */

typedef struct {

	DEPBIN         *db_handling;
	DEPBIN         *db_resp_codes;

}               DOMAIN_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */

	FLAT_MASKS      masks;

	ulong           handling;
	ITEM_ID         resp_codes;

	DOMAIN_DEPBIN  *depbin;

}               FLAT_DOMAIN;




/*
 * COMMAND item
 */

typedef struct {

	DEPBIN			*db_number;
	DEPBIN			*db_oper;
	DEPBIN			*db_trans;
	DEPBIN			*db_resp_codes;
	DEPBIN			*db_debug_info;
}               COMMAND_DEPBIN;

typedef struct {

	ITEM_ID         id;	/* Item ID for this item */
	FLAT_MASKS			masks;
	ulong				number;
	ulong				oper;
	TRANSACTION_LIST	trans;
	RESPONSE_CODE_LIST	resp_codes;
	STRING				symbol_name;
	COMMAND_DEPBIN *depbin;

}               FLAT_COMMAND;




#endif
