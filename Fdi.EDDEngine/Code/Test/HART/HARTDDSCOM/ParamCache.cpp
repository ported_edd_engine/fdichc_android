#include "StdAfx.h"
#include "ParamCache.h"


CParamCache::CParamCache()
{	
	m_itemProcessing = 0;
	m_subindexProcessing = 0;
}

CParamCache::~CParamCache(void)
{

}
void CParamCache::AssignIConvenience( IConvenience *pIConvenience )
{
	m_IConvenience = pIConvenience;
}

void CParamCache::AssignItemInfo( IItemInfo *pIItemInfo )
{
	m_IItemInfo = pIItemInfo;
}

unsigned long CParamCache::GetDefaultIndex(int iBlockInstance, void * pValueSpec, FDI_PARAM_SPECIFIER *pParamSpec)
{
	
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, pParamSpec->id, 0};
	AttributeNameSet attribute_list = indexed;

	int r_code = m_IItemInfo->GetItem( iBlockInstance, pValueSpec, &item_spec, &attribute_list, L"|en|", &generic_item);
	if (r_code == 0)
	{
		FLAT_VAR *pFlatVar = dynamic_cast<FLAT_VAR*>(generic_item.item);

		FDI_ITEM_SPECIFIER item_spec2 = {FDI_ITEM_ID, pFlatVar->indexed, 0};

		ITEM_TYPE ItemType = ITYPE_NO_VALUE;
		m_IConvenience->GetItemType(iBlockInstance, &item_spec2, &ItemType); // Get the type of the thing the index is on

		if (ItemType == ITYPE_ITEM_ARRAY)
		{
			FDI_GENERIC_ITEM generic_item2(ITYPE_ITEM_ARRAY);
			AttributeNameSet attribute_list2 = elements;

			r_code = m_IItemInfo->GetItem( iBlockInstance, pValueSpec, &item_spec2, &attribute_list2, L"|en|", &generic_item2);
			if (r_code == 0)
			{
				FLAT_ITEM_ARRAY *flat_item_arry = dynamic_cast<FLAT_ITEM_ARRAY *>(generic_item2.item);
				if (flat_item_arry != NULL)	
				{
					return flat_item_arry->elements.list[0].index;	// good return
				}
			}
		}
	}
	return 0;	// if not Item Array
}

PC_ErrorCode CParamCache::GetParamValue(int iBlockInstance, void* pValueSpec, FDI_PARAM_SPECIFIER *pParamSpec, EVAL_VAR_VALUE * pParamValue)
{
	PC_ErrorCode ec = PC_SUCCESS_EC;

	if (pParamValue == (EVAL_VAR_VALUE *) NULL) 
	{
		return PC_OTHER_EC;
	}

	/*
	* Get the variable type
	*/

	if (pParamSpec->eType == FDI_PS_ITEM_ID_COMPLEX)
	{
		return PC_OTHER_EC;
	}

	TYPE_SIZE TypeSize;

	int rcode = m_IConvenience->GetParamType(iBlockInstance, pParamSpec, &TypeSize );
	
	if (rcode != 0)
	{
		return PC_OTHER_EC;
	}

	/* default initial param value based on type */

	pParamValue->type = TypeSize.type;
	pParamValue->size = TypeSize.size;

	switch (pParamValue->type)
	{
	case VT_INTEGER:
		pParamValue->val.i = 1;		// Default is 1
		break;

	
	case VT_ENUMERATED:
	case VT_BIT_ENUMERATED:
		{
			bool bDefaultFound = false;

			// if the pParamSpec is not the same as what we are already processing, then go ahead and get the enum list
			if( ! ( (m_itemProcessing == pParamSpec->id) && (m_subindexProcessing == pParamSpec->subindex) ) )
			{
				ITEM_ID prevItemProcessing = m_itemProcessing;	// Save the current ITEM_ID
				SUBINDEX prevSubindexProcessing = m_subindexProcessing;

				m_itemProcessing = pParamSpec->id;				// Set the new ITEM_ID to the param->id
				m_subindexProcessing = pParamSpec->subindex;

				// Set up the call to get the enumeration list from this variable
				FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
				FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, pParamSpec->id, 0};

				if (pParamSpec->eType == FDI_PS_ITEM_ID_SI)
				{
					item_spec.eType = FDI_ITEM_ID_SI;
					item_spec.subindex = pParamSpec->subindex;
				}

				AttributeNameSet attribute_list = nsEDDEngine::enumerations;

				rcode = m_IItemInfo->GetItem(iBlockInstance, pValueSpec, &item_spec, &attribute_list, _T("en"), &generic_item);
				if (rcode == 0)
				{
					FLAT_VAR *flat_var = dynamic_cast<FLAT_VAR*>(generic_item.item);
					if (flat_var != NULL)
					{
						if (flat_var->enums.count > 0)
						{
							pParamValue->val.u = flat_var->enums.list[0].val.val.u;		// Get the first value in the list.
							bDefaultFound = true;
						}
					}
				}

				m_itemProcessing = prevItemProcessing;	// Restore to the previous value
				m_subindexProcessing = prevSubindexProcessing;
			}
			
			if (bDefaultFound == false)
			{
				pParamValue->val.u = 0; // just default it to zero and get out of the loop that has occured
			}
			break;
		}

	case VT_UNSIGNED:
		pParamValue->val.u = 0;
		break;

	case VT_INDEX:
		pParamValue->val.u = GetDefaultIndex(iBlockInstance, pValueSpec,pParamSpec);
		break;

	case VT_FLOAT:
		pParamValue->val.f = 1.0;		// Default is 1.0
		break;

	case VT_DOUBLE:
		pParamValue->val.d = 1.0;		// Default is 1.0
		break;

	case VT_ASCII:
	case VT_PACKED_ASCII:
	case VT_PASSWORD:
	case VT_EUC:  
    case VT_VISIBLESTRING:
		pParamValue->val.s.assign((wchar_t*)"",false);
		break;
	case VT_BITSTRING:
	case VT_OCTETSTRING:
	{
		uchar usConst[] = { 0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40 };
		pParamValue->val.b.assign(usConst, 7);
	}
	break;
	case VT_EDD_DATE:
	case VT_DATE_AND_TIME:
	case VT_DURATION:
		break;

	default:
		TRACE(L"\n*** error (CParamCache::GetParamValue) : Unsupported type %d\n",pParamValue->type);
	//	ASSERT(0);
		ec = PC_OTHER_EC;
	}

	return ec;
}


PC_ErrorCode CParamCache::GetDynamicAttribute( int /*iBlockInstance*/, void* /*pValueSpec*/, const nsEDDEngine::OP_REF* /*pOpRef*/,
            const nsEDDEngine::AttributeName /*attributeName*/, /* out */ EVAL_VAR_VALUE * pParamValue)
{
    pParamValue->type = VT_INTEGER;
    pParamValue->val.i = 1;
    pParamValue->size = sizeof(int);

    return PC_ErrorCode::PC_SUCCESS_EC;
}



