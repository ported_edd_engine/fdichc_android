/*************************************************************************************************
 *
 * $Workfile: HARTsupport.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		structures and enums to support the HART protocol
 *	
 * #include "HARTsupport.h"
 */

#ifndef _HARTSUPPORT_H
#define _HARTSUPPORT_H
#ifdef INC_DEBUG
#pragma message("In HARTsupport.h") 
#endif

#ifdef INC_DEBUG
#pragma message("    Finished Includes::HARTsupport.h") 
#endif


// COMMUNICATION ERROR - Hi Bit is SET in first byte, use | to tie'em together; second byte is zero
#define DEV_PARITYERR		0xC0	// hi bit and bit 6
#define DEV_OVERUNERR		0xA0	// hi bit and bit 5
#define DEV_FRAMINGERR		0x90	// hi bit and bit 4
#define DEV_CHKSUMERR		0x88	// hi bit and bit 3
#define DEV_BIT2RSVD		0x84	// hi bit and bit 2 - reserved
#define DEV_RXBUFFOVER		0x82	// hi bit and bit 1
#define DEV_BIT0RSVD		0x81	// hi bit and bit 0 - reserved

// COMMAMD RESPONSE - Hi Bit is CLEAR in first byte, use them as an integer; There could be more integers.
#define CR_NO_SPECIFIC_ERROR		0
#define CR_INVALID_SELECTION		2
#define CR_PARAM_TOO_LARGE			3
#define CR_PARAM_TOO_SMALL			4
#define CR_DATA_TOO_FEW				5
#define CR_DEV_COMMAND_ERROR		6
#define CR_WRITE_PROTECT_MODE		7
#define CR_ACCESS_RESTRICTED		16
#define CR_DEVICE_BUSY				32
#define CR_COMMAND_NOT_IMPL			64

// DEVICE STATUS - Second byte after a command response code <non-commumication error>
#define DS_DEVMALFUNCTION	0x80	// bit 7
#define DS_CONFIGCHANGED	0x40	// bit 6
#define DS_COLDSTART		0x20	// bit 5
#define DS_MORESTATUSAVAIL	0x10	// bit 4
#define DS_OUTCURRENTFIXED	0x08	// bit 3
#define DS_ANALGOUTSATURATE	0x04	// bit 2
#define DS_NONPRIMOUTLIMIT	0x02	// bit 1
#define DS_PRIMVAROUTLIMIT	0x01	// bit 0
//#define DS_NO_STATUS		0x00


#endif // _HARTSUPPORT_H