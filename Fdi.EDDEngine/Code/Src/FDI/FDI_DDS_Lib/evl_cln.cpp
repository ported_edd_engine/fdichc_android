/*
 *	@(#) $Id: evl_cln.cpp,v 1.3 2012/09/03 22:13:07 rgretta Exp $
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 *  This file contains item destruct routines.
 *
 */

#include "stdafx.h"
#include "evl_loc.h"

/*********************************************************************
 *
 *	Name: eval_clean_item
 *	ShortDesc: Clean the FLAT structure associated with a FLAT item
 *
 *	Description:
 *		eval_clean_item() is a distributor function for all
 *		"eval_clean_*" routines.
 *
 *	Inputs:
 *		item_type:		type of item to be evaluated (ie. var, ... ).
 *
 *	Outputs:
 *		item:			the cleaned FLAT structure
 *
 *	Returns:
 *		void
 *
 *	Author: steve beyerl
 *
 **********************************************************************/
void
eval_clean_item(nsEDDEngine::ItemBase  * /* item */, ITEM_TYPE /* itype */)
{
	// regj remove
}
