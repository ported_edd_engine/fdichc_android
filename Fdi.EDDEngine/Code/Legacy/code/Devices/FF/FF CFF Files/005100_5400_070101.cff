//=======================
//    File Header
//=======================
[File Header]

Description = "Fisher Controls AO & PID rev 7 Digital Valve Controller"
FileType = CapabilitiesFile
FileDate = 2000,10,3
CffVersion = 1,5

//=======================
//    Device Header
//=======================
[Device Header]

DeviceName = "Fisher DVC 5000f"
DeviceClass = BASIC

CommGroup = Class3
CommClass = Class31
CommSubClass = Class3Publisher+Class3Subscriber

[Device VFD 1]                         // Management VFD

VendorName = "National Instruments - Fisher"
ModelName = "DVC 5000f"
Revision = "FF 1.3 FCS rel 2"
VersionOD = 0
ProfileNumber = 0x4d47

[Device VFD 2]                         // FB VFD

VendorName = "Fisher Controls"
ModelName = "DVC5000f AO/PID"
Revision = "ACT9 NI 2.3CTS"
VersionOD = 0x1
ProfileNumber = 0x0000

//=======================
//   NM section
//=======================
[NM OD Directory]

DirectoryRevisionNumber = 0
NumberOfDirectoryObjects = 1
TotalNumberOfDirectoryEntries = 8
DirectoryIndexOfFirstCompositeListReference = 8
NumberOfCompositeListReferences = 1
StackMgtOdIndex = 433
NumberOfObjectsInStackManagement = 1
VcrListOdIndex = 998
NumberOfObjectsInVcrList = 22
DlmeBasicOdIndex = 434
NumberOfObjectsInDllBasic = 2
DlmeLinkMasterOdIndex = 436
NumberOfObjectsInDllLme = 8
LinkScheduleListOdIndex = 444
NumberOfObjectsInDllLinkSchedule = 10
DlmeBridgeOdIndex = 0
NumberOfObjectsInDllBridge = 0
PlmeBasicOdIndex = 454
NumberOfObjectsInPhyLme = 3
ListOfMmeReferences = 9
NumberOfMmeComposites = 0

//=======================
//   NM Restrictions
//=======================
[NM Restrictions]

//=======================
// Basic Info
//=======================
MaximumResponseDelaySlotTime = 20
MinimumInterPduDelay = 10
Slottime = 5

//=======================
// Basic Characteristics
//=======================
Version = 1
BasicStatisticsSupportedFlag = FALSE
DlOperatFunctionalClass = BASIC
DlDeviceConformance = 0x00000000

//=======================
//  Physical Layer
//=======================
PowerRequired = 26               // in milliamps
ChannelStatisticsSupported = 0x0
MediumAndDataRatesSupported = 0x4900000000000000
IecVersion = 0x0001
NumberOfchannels = 1
PowerMode = BUS_POWERED
InterfaceMode = HALF_DUPLEX

//=======================
// Basic Characteristics
//=======================

//=======================
// Stack Capabilities
//=======================

FasArTypesAndRolesSupported = QUB_SERVER+QUU_SOURCE+BNU_PUBLISHER+\
                              BNU_SUBSCRIBER+QUB_CLIENT+QUB_PEER+\
                              QUU_SINK
                              
MaxDlsapAddressSupported = 0x00FF
MaxDlcepAddressSupported = 0x0FFF

DlcepDeliveryFeaturesSupported = S_CLASSICAL+S_DISORDERED+\
                                 S_ORDERED+S_UNORDERED+\
                                 R_CLASSICAL+R_DISORDERED+\
                                 R_ORDERED+R_UNORDERED

AgentFunctionsSupported = INDIVIDUAL_VCR_ENTRY_DOWNLOADABLE+VCR_LIST_DOWNLOADABLE

FmsFeaturesSupported = GET_OD+READ+WRITE+INFO_REPORT+EVENT+\
                       ACK_EVENT+ALTER_EVENT+GEN_DOWNLOAD, \
                       GET_OD+READ+WRITE+INFO_REPORT+EVENT+\
                       ACK_EVENT+ALTER_EVENT+GEN_DOWNLOAD

//=======================
// VCR Capabilities
//=======================
MaxEntries = 20
NumPermanentEntries = 1
DynamicsSupportedFlag = FALSE
StatisticsSupported = 0x0

MaximumNumberOfClientVcrs = 0
MaximumNumberOfServerVcrs = 19
MaximumNumberOfSourceVcrs = 19
MaximumNumberOfSinkVcrs = 19
MaximumNumberOfSubscriberVcrs = 19
MaximumNumberOfPublisherVcrs = 19

//==================================
// VCR Restrictions
//==================================
[NM VCR Usage 1]
FasArTypeAndRole = QUB+SERVER+NOBYPASS
FasDllLocalAddr = 0xF8
FasDllConfiguredRemoteAddr = FREE
FasDllSDAP = NONSCHEDULED+DISORDERED+AUTH_SOURCE+TIMEAVAILABLE
FasDllMaxConfirmDelayOnConnect = 65000
FasDllMaxConfirmDelayOnData = 65000
FasDllMaxDlsduSize = 128
FasDllResidualActivitySupported = TRUE
FasDllTimelinessClass = NONE+FALSE+UPDATE
FasDllPublisherTimeWindowSize = 0
FasDllPublisherSynchronizingDlcep = 0
FasDllSubscriberTimeWindowSize = 0
FasDllSubscriberSynchronizingDlcep = 0
FmsVfdID = 300
FmsMaxOutstandingServicesCalling = 0
FmsMaxOutstandingServicesCalled = 1
FmsFeaturesSupported = NONE,READ+WRITE+INFO_REPORT+EVENT+\
                       ACK_EVENT+ALTER_EVENT+GEN_DOWNLOAD
//=======================
//   SM section
//=======================
[SM OD Directory]
DirectoryRevisionNumber = 0
NumberOfDirectoryObjects = 1
TotalNumberOfDirectoryEntries = 5
DirectoryIndexOfFirstCompositeListReference = 0
NumberOfCompositeListReference = 0
SmAgentStartingOdIndex = 402
NumberOfSmAgentObjects = 4
SyncAndSchedulingStartingOdIndex = 406
NumberOfSyncAndSchedulingObjects = 9
AddressAssignmentStartingOdIndex = 414
NumberOfAddressAssignmentObjects = 3
VfdListStartingOdIndex = 428
NumberOfVfdListObjects = 2
FbScheduleStartingOdIndex = 417
NumberOfFbScheduleObjects = 11

[SM VFD 1]
VFD_REF = 0x012C
VFD_TAG = "MIB"

[SM VFD 2]
VFD_REF = 0x0001
VFD_TAG = "FBAP"                            

[SM Capability]
SM_SUPPORT = SET_PDTAG_AGENT+SET_ADDR_AGENT+CLR_ADDR_AGENT+IDENTIFY_AGENT\
            +LOC_FB_AGENT+FMS_SERVER+TIME_SLAVE+SCHEDULE_FB

//=======================
//   Application section
//=======================
[VFD 2 OD Directory]
DirectoryRevisionNumber = 1
NumberOfDirectoryObjects = 1
TotalNumberOfDirectoryEntries = 12
DirectoryIndexOfFirstCompositeListReference = 17
NumberOfCompositeListReference = 3

OdIndexForStartingActionObject = 0
NumberOfActionObjects = 0

ODIndexOfTheStartingLinkObjectInTheVfd=665
NumberOfLinkObjectsInTheVfd=15

OdIndexOfTheStartingAlertObjectInTheVfd=680
NumberOfAlertObjectsInTheVfd=3

OdIndexOfTheStartingTrendObjectInTheVfd=683
NumberOfTrendObjectsInTheVfd=24

OdIndexOfTheStartingDomainObjectInTheVfd=0
NumberOfDomainObjectsInTheVfd=0

DirectoryIndexForTheResourceBlock=23
NumberOfResourceBlocksInTheVfd=1

DirectoryIndexForTheFirstTransducerBlockPointer = 25
NumberOfTransducerBlocksInTheVfd = 1

DirectoryIndexForTheFirstFunctionBlockPointer = 27
NumberOfFunctionBlocksInTheVfd = 2

[VFD 2 Channels]
Channel1 = "Valve Control"

[VFD 2 Resource Block]
BLOCK_INDEX = 409
BLOCK_TYPE = RESOURCE
DD_ITEM = 0x80020310
PROFILE = 267
PROFILE_REVISION = 1
NUM_OF_PARMS = 41
VIEWS_INDEX = 707
NUMBER_VIEW_3 = 1
NUMBER_VIEW_4 = 1

[VFD 2 Resource Block Defaults]
MANUFAC_ID = 0x5100
DEV_TYPE = 0x5400
DEV_REV = 7
DD_REV = 1
DD_RESOURCE = ""
HARD_TYPES = 0x4000

FEATURES = 0x7400
CYCLE_TYPE = 0x8000
MIN_CYCLE_T = 3200
MEMORY_SIZE = 0
NV_CYCLE_T = 5760000
MAX_NOTIFY = 15

[VFD 2 Transducer Block 1]
BLOCK_INDEX = 450
BLOCK_TYPE = APVV_Transducer_Block
DD_ITEM = 0x80020530
PROFILE = 0x10d
PROFILE_REVISION = 1
NUM_OF_PARMS = 109
VIEWS_INDEX = 711
NUMBER_VIEW_3 = 1
NUMBER_VIEW_4 = 10

[VFD 2 Transducer Block 1 Defaults]

[VFD 2 Function Block 1]
BLOCK_INDEX = 559
BLOCK_TYPE = AO
DD_ITEM = 0x800201F0
PROFILE = 258
PROFILE_REVISION = 1
EXECUTION_TIME = 1920
NUM_OF_PARMS = 31
VIEWS_INDEX = 724
NUMBER_VIEW_3 = 1
NUMBER_VIEW_4 = 1

[VFD 2 Function Block 1 Defaults]
STRATEGY = 0
ALERT_KEY = 0

SP = 128, 0
SIMULATE = 0,0,0,0,1
PV_SCALE = 100,0,1342,2
XD_SCALE = 100,0,1342,2
IO_OPTS = 0x0000
STATUS_OPTS = 0x0000
CAS_IN = 11,0
SP_RATE_DN = 1.#INF
SP_RATE_UP = 1.#INF
SP_HI_LIM = 100
SP_LO_LIM = 0
CHANNEL = 0
FSTATE_TIME = 0
FSTATE_VAL = 0
RCAS_IN = 27,0
SHED_OPT = 1

[VFD 2 Function Block 2]
BLOCK_INDEX = 590
BLOCK_TYPE = PID
DD_ITEM = 0x800202B0
PROFILE = 264
PROFILE_REVISION = 257
EXECUTION_TIME = 3840
NUM_OF_PARMS = 75
VIEWS_INDEX = 728
NUMBER_VIEW_3 = 1
NUMBER_VIEW_4 = 1

[VFD 2 Function Block 2 Defaults]
STRATEGY = 0
ALERT_KEY = 0

SP = 128,0
PV_SCALE = 100,0,57,2
OUT_SCALE = 100,0,57,2
CONTROL_OPTS = 0x0000
STATUS_OPTS = 0x0000
PV_FTIME = 0
BYPASS = 0x00
CAS_IN = 11,0
SP_RATE_DN=1.#INF
SP_RATE_UP=1.#INF
SP_HI_LIM=100
SP_LO_LIM=0
GAIN = 1
RESET = 1000
BAL_TIME = 0
RATE = 0
BKCAL_IN = 11,0
OUT_HI_LIM = 100
OUT_LO_LIM = 0
BKCAL_HYS = 0.5
BKCAL_OUT = 28,0
RCAS_IN = 27,0
ROUT_IN = 27,0
SHED_OPT = 1
RCAS_OUT = 28,0
TRK_SCALE = 100,0,57,2
TRK_IN_D = 11,0
TRK_VAL = 11,0
FF_VAL = 11,0
FF_SCALE = 100,0,57,2
FF_GAIN = 0
ALARM_SUM = 0x0100,0x0100,0x0100,0x0000
ACK_OPTION = 0x0000
ALARM_HYS = 0.5
HI_HI_PRI = 0
HI_HI_LIM = 1.#INF
HI_PRI = 0
HI_LIM = 1.#INF
LO_PRI = 0
LO_LIM = -1.#INF
LO_LO_PRI = 0
LO_LO_LIM = -1.#INF
DV_HI_PRI = 0
DV_HI_LIM = 1.#INF
DV_LO_PRI = 0
DV_LO_LIM = -1.#INF
