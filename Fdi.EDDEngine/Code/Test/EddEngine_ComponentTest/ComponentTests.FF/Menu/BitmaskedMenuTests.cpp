#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_a_DD_for_FF_which_contains_bitmasked_menu_items
        {
        private:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_MENU* test_menu;
            static ITEM_ID expectedItemId;

        public:
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0010\\TestCase0010.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                ITEM_ID* test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // get the ItemIds for the test_var_1
                ITEM_ID* test_var_1_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_var_1", test_var_1_item_ID);
                expectedItemId = *test_var_1_item_ID;
                delete test_var_1_item_ID;

                // get the ItemIds for the test_menu
                ITEM_ID* test_menu_item_ID = new ITEM_ID();
                int error =  engine->Instance->GetItemIdFromSymbolName(L"main_test_menu", test_menu_item_ID);

                //  create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_menu_item_ID,
                    nsEDDEngine::ITYPE_MENU,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_MENU),
                    0);

                test_menu = dynamic_cast<FLAT_MENU*>(genericItem->item);

                delete pFieldbusBlockInfo;
                delete test_menu_item_ID;
                delete test_block_item_ID;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void EDDEngine_should_return_a_FLAT_MENU()
            {
                Assert::IsFalse(nullptr == test_menu);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_menu_should_have_three_items()
            {
                Assert::AreEqual((UInt16)3, test_menu->items.count);
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_first_item_is_an_unbitmasked_ITYPE_VARIABLE()
            {
                Assert::AreEqual((unsigned long) 0, test_menu->items.list[0].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_VARIABLE, (int)(test_menu->items.list[0].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_second_item_is_an_ITYPE_VARIABLE_with_bitmask_of_03()
            {
                Assert::AreEqual(0x03u, test_menu->items.list[1].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_ENUM_BIT, (int)(test_menu->items.list[1].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmasks")]
            void The_third_item_is_an_ITYPE_VARIABLE_with_bitmask_FC()
            {
                Assert::AreEqual(0xFCu, test_menu->items.list[2].ref.desc_bit_mask);
                Assert::AreEqual((int)ITYPE_VARIABLE, (int)(test_menu->items.list[0].ref.desc_type));
            }

            [TestMethod, TestCategory("MenuBitmask")]
            void All_three_items_reference_test_bitEnum()
            {
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[0].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[1].ref.desc_id));
                Assert::AreEqual((unsigned long)(expectedItemId), (unsigned long)(test_menu->items.list[2].ref.desc_id));
            }
        };

    } // namespace FF
} // namespace ComponentTests
