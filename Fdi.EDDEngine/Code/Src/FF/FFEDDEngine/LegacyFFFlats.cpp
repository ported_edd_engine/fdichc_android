#include "stdafx.h"
#include "LegacyFF.h"
#include "LegacyFFFlats.h"

#define HOOK_DEPBIN(dds_ptr, db, legacyff_Ptr)                     \
    if (dds_ptr->db != NULL)                         \
    {                                                \
        this->db = new FF_ConvDEPBIN(dds_ptr->db, legacyff_Ptr); \
    }


//FF_ConvFLAT_FILE() - Fills in an FDI FLAT_FILE from DDS FLAT_FILE
FF_ConvFLAT_FILE::FF_ConvFLAT_FILE( ::FLAT_FILE *pFlatFile, CLegacyFF *pLegacyFF )
{
	id = pFlatFile->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatFile->masks, nsEDDEngine::ITYPE_FILE);

	CLegacyFF::Fill_STRING(&help, &pFlatFile->help);
	CLegacyFF::Fill_STRING(&label, &pFlatFile->label);

	pLegacyFF->Fill_MEMBER_LIST(&members, &pFlatFile->members);

	if(pFlatFile->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvFILE_DEPBIN( pFlatFile->depbin, pLegacyFF );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_FILE::attr_map[] = {
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::members,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//FF_ConvFILE_DEPBIN() - Fills in an FDI FILE_DEPBIN from DDS FILE_DEPBIN
FF_ConvFILE_DEPBIN::FF_ConvFILE_DEPBIN( ::FILE_DEPBIN *pFileDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pFileDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pFileDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pFileDepbin, db_members, pLegacyFF)
}


//FF_ConvFLAT_LIST(): Fills in FDI FLAT_LIST from DDS FLAT_LIST.
FF_ConvFLAT_LIST::FF_ConvFLAT_LIST( ::FLAT_LIST *pFlatList, CLegacyFF* pLegacyFF )
{
	id = pFlatList->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatList->masks, nsEDDEngine::ITYPE_LIST);

	CLegacyFF::Fill_STRING(&help, &pFlatList->help);

	CLegacyFF::Fill_STRING(&label, &pFlatList->label);

	valid = CLegacyFF::SetBoolean(pFlatList->valid);

	type = pFlatList->type;

	capacity = pFlatList->capacity;
	count = pFlatList->count;

	pLegacyFF->Fill_OP_REF(&this->count_ref, &pFlatList->count_ref);

	if(pFlatList->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvLIST_DEPBIN( pFlatList->depbin, pLegacyFF );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_LIST::attr_map[] = {
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::type_definition,
	nsEDDEngine::AttributeName::count,
	nsEDDEngine::AttributeName::capacity,
	nsEDDEngine::AttributeName::validity,
	nsEDDEngine::AttributeName::count_ref,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//FF_ConvLIST_DEPBIN(): Fills in FDI LIST_DEPBIN from DDS LIST_DEPBIN.
FF_ConvLIST_DEPBIN::FF_ConvLIST_DEPBIN( ::LIST_DEPBIN* pListDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pListDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pListDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pListDepbin, db_type, pLegacyFF)
	HOOK_DEPBIN( pListDepbin, db_capacity, pLegacyFF)
	HOOK_DEPBIN( pListDepbin, db_count, pLegacyFF)
	HOOK_DEPBIN( pListDepbin, db_valid, pLegacyFF)
}


//FF_ConvFLAT_RESP_CODE(): Fills in an FDI FLAT_RESP_CODE from DDS FLAT_RESP_CODE.
FF_ConvFLAT_RESP_CODE::FF_ConvFLAT_RESP_CODE( ::FLAT_RESP_CODE* pFlatRespCode, CLegacyFF *pLegacyFF )
{
	id = pFlatRespCode->id;
	
	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatRespCode->masks, nsEDDEngine::ITYPE_RESP_CODES);

	pLegacyFF->Fill_RESPONSE_CODES(&member, &pFlatRespCode->member);	

	if(pFlatRespCode->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvRESP_CODE_DEPBIN( pFlatRespCode->depbin, pLegacyFF );
	}
}

const nsEDDEngine::AttributeName FF_ConvFLAT_RESP_CODE::attr_map[] = {
	nsEDDEngine::AttributeName::response_codes,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//FF_ConvRESP_CODE_DEPBIN(): Fills in an FDI RESP_CODE_DEPBIN from DDS RESP_CODE_DEPBIN.
FF_ConvRESP_CODE_DEPBIN::FF_ConvRESP_CODE_DEPBIN( ::RESP_CODE_DEPBIN *pRespCodeDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pRespCodeDepbin, db_member, pLegacyFF )
}


// FF_ConvFLAT_COLLECTION() - Fills in an FDI FLAT_COLLECTION table from DDS FLAT_COLLECTION table.
FF_ConvFLAT_COLLECTION::FF_ConvFLAT_COLLECTION( ::FLAT_COLLECTION *pFlatCollection, CLegacyFF *pLegacyFF )
{
	id = pFlatCollection->id;
	
	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatCollection->masks, nsEDDEngine::ITYPE_COLLECTION);

	subtype =  pLegacyFF->Set_ITEM_TYPE(pFlatCollection->subtype);

	pLegacyFF->Fill_OP_MEMBER_LIST(&op_members, &pFlatCollection->members);

	CLegacyFF::Fill_STRING(&help, &pFlatCollection->help);
	
	CLegacyFF::Fill_STRING(&label, &pFlatCollection->label);

	valid = CLegacyFF::SetBoolean(pFlatCollection->valid);

	if(pFlatCollection->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvCOLLECTION_DEPBIN( pFlatCollection->depbin, pLegacyFF);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_COLLECTION::attr_map[] = {
	nsEDDEngine::AttributeName::members,
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::validity,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

// FF_ConvCOLLECTION_DEPBIN(): Fills in an FDI COLLECTION_DEPBIN from DDS COLLECTION_DEPBIN
FF_ConvCOLLECTION_DEPBIN::FF_ConvCOLLECTION_DEPBIN( ::COLLECTION_DEPBIN *pCollectionDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pCollectionDepbin, db_members, pLegacyFF )
	HOOK_DEPBIN( pCollectionDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pCollectionDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pCollectionDepbin, db_valid, pLegacyFF)
}


//FF_ConvFLAT_ITEM_ARRAY() - Fills in an FDI FLAT_ITEM_ARRAY from DDS FLAT_ITEM_ARRAY
FF_ConvFLAT_ITEM_ARRAY::FF_ConvFLAT_ITEM_ARRAY( ::FLAT_ITEM_ARRAY *pFlatItemArray, CLegacyFF *pLegacyFF)
{
	id = pFlatItemArray->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatItemArray->masks, nsEDDEngine::ITYPE_ITEM_ARRAY);

	subtype  = pLegacyFF->Set_ITEM_TYPE(pFlatItemArray->subtype);

	if(pFlatItemArray->elements.count != 0)
	{
		elements.count	= pFlatItemArray->elements.count;
		elements.limit	= pFlatItemArray->elements.count;
		elements.list	= new nsEDDEngine::ITEM_ARRAY_ELEMENT[pFlatItemArray->elements.count];

		for(int i = 0; i < pFlatItemArray->elements.count; i++)
		{
			
			if(pFlatItemArray->elements.list[i].evaled & IA_DESC_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_DESC_EVALED; 
				CLegacyFF::Fill_STRING(&elements.list[i].desc, &pFlatItemArray->elements.list[i].desc);
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_HELP_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_HELP_EVALED; 
				CLegacyFF::Fill_STRING(&elements.list[i].help, &pFlatItemArray->elements.list[i].help);
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_INDEX_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_INDEX_EVALED; 
				elements.list[i].index		=  pFlatItemArray->elements.list[i].index;
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_REF_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_REF_EVALED; 
				elements.list[i].ref.id		=  pFlatItemArray->elements.list[i].ref.id;

				elements.list[i].ref.type  = pLegacyFF->Set_ITEM_TYPE(pFlatItemArray->elements.list[i].ref.type);
			}
			
		}
	}

	CLegacyFF::Fill_STRING(&help, &pFlatItemArray->help);

	CLegacyFF::Fill_STRING(&label, &pFlatItemArray->label);

    valid = CLegacyFF::SetBoolean(pFlatItemArray->valid);

	if(pFlatItemArray->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvITEM_ARRAY_DEPBIN( pFlatItemArray->depbin, pLegacyFF);
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_ITEM_ARRAY::attr_map[] = {
		nsEDDEngine::AttributeName::elements,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//FF_ConvITEM_ARRAY_DEPBIN() - Fills in an FDI ITEM_ARRAY_DEPBIN from DDS ITEM_ARRAY_DEPBIN
FF_ConvITEM_ARRAY_DEPBIN::FF_ConvITEM_ARRAY_DEPBIN( ::ITEM_ARRAY_DEPBIN * pItemArrayDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pItemArrayDepbin, db_elements, pLegacyFF)
	HOOK_DEPBIN( pItemArrayDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pItemArrayDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pItemArrayDepbin, db_valid, pLegacyFF)
}


// FF_ConvFLAT_VAR() - Fills in an FDI FLAT_VAR from DDS FLAT_VAR
FF_ConvFLAT_VAR::FF_ConvFLAT_VAR( ::FLAT_VAR* pFlatVar, CLegacyFF *pLegacyFF )
{
	id = pFlatVar->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatVar->masks, nsEDDEngine::ITYPE_VARIABLE);

	// The following masks are filled so that the defaults can be retrieved even though
	// the fields are not available in legacy FF
	// The constructor FLAT_VAR(): ItemBase(ITYPE_VARIABLE) fills the values with defaults.
	masks.attr_avail.insert(nsEDDEngine::AttributeName::height);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::width);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::visibility);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::private_attr);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::time_scale);

	class_attr = CLegacyFF::SetClassType(pFlatVar->class_attr);

	handling = CLegacyFF::SetHandling(pFlatVar->handling);

	CLegacyFF::Fill_STRING(&help, &pFlatVar->help);
	CLegacyFF::Fill_STRING(&label, &pFlatVar->label);

	pLegacyFF->Fill_EDD_TYPE_SIZE(&type_size, pFlatVar->type_size); 

	if (type_size.type == nsEDDEngine::VT_OCTETSTRING)
	{
		// This is special case for FF "__tag_desc" (ItemId="2147615104(__tag_desc)") . 
		// Since "__tag_desc" is always understood to be a printable string, 
		// the EDD Engine should default this DISPLAY/EDIT_FORMAT to "s" so that the consumer will know
		if (pFlatVar->id == 2147615104)
		{
			display.assign(L"s");
			edit.assign(L"s");
		}
		else
		{
			// As per -3 spec 9.1.15.11.2 When the DISPLAY_FORMAT attribute is not specified, a default format of �#x� shall be used for FF
			if (pFlatVar->display.len == 0)
			{
				display.assign(L"X");
			}
			else
			{
				CLegacyFF::Fill_STRING(&display, &pFlatVar->display);
			}
			// As per -3 Spec 9.1.15.11.3 if EDIT_FORMAT is not specified, the DISPLAY_FORMAT should be used. 
			if (pFlatVar->edit.len == 0)
			{
				edit = display;
			}
			else
			{
				CLegacyFF::Fill_STRING(&edit, &pFlatVar->edit);
			}
		}

		masks.attr_avail.insert(nsEDDEngine::AttributeName::display_format);
		masks.attr_avail.insert(nsEDDEngine::AttributeName::edit_format);
	}
	else
	{
		CLegacyFF::Fill_STRING(&display, &pFlatVar->display);
		CLegacyFF::Fill_STRING(&edit, &pFlatVar->edit);
	}

	pLegacyFF->Fill_ENUM_VALUE_LIST(&enums, &pFlatVar->enums);

	indexed = pFlatVar->index_item_array;

	if ( pFlatVar->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatVar->resp_codes;
	}

	//CLegacyFF::Fill_EXPR(&default_value, &pFlatVar->default_value);

	if(pFlatVar->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvVAR_DEPBIN( pFlatVar, pLegacyFF );
	}

	if(pFlatVar->actions != NULL)
	{
		CLegacyFF::Fill_ACTION_LIST(&post_edit_act, &pFlatVar->actions->post_edit_act);

		CLegacyFF::Fill_ACTION_LIST(&post_read_act, &pFlatVar->actions->post_read_act);

		CLegacyFF::Fill_ACTION_LIST(&post_write_act, &pFlatVar->actions->post_write_act);

		CLegacyFF::Fill_ACTION_LIST(&pre_edit_act, &pFlatVar->actions->pre_edit_act);

		CLegacyFF::Fill_ACTION_LIST(&pre_read_act, &pFlatVar->actions->pre_read_act);

		CLegacyFF::Fill_ACTION_LIST(&pre_write_act, &pFlatVar->actions->pre_write_act);

		CLegacyFF::Fill_ACTION_LIST(&refresh_act, &pFlatVar->actions->refresh_act);
	}

	if(pFlatVar->misc != NULL) 
	{
		CLegacyFF::Fill_STRING(&constant_unit, &pFlatVar->misc->unit);

		valid = CLegacyFF::SetBoolean(pFlatVar->misc->valid);

		CLegacyFF::Fill_RANGE_DATA_LIST(&min_val, &pFlatVar->misc->min_val);

		CLegacyFF::Fill_RANGE_DATA_LIST(&max_val, &pFlatVar->misc->max_val);	

		CLegacyFF::Fill_EXPR(&scaling_factor, &pFlatVar->misc->scale);

//		CLegacyFF::Fill_STRING(&time_format, &pFlatVar->misc->time_format);

//		time_scale = pFlatVar->misc->time_scale;

//		write_mode = pFlatVar->misc->write_mode;
	}


	// Below attributes are not available in DDS FLAT_VAR
	

	//Fill_EXPR(pFdiFlatVar->initial_val, pFlatVar-> initial_val);

	//Fill_STRING(&pFdiFlatVar->style, &pFlatVar->misc->);
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_VAR::attr_map[] = {
		nsEDDEngine::AttributeName::class_attr,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::constant_unit,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		(nsEDDEngine::AttributeName) -1,	// READ_TIME_OUT
		(nsEDDEngine::AttributeName) -1,	// WRITE_TIME_OUT
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::pre_read_actions,
		nsEDDEngine::AttributeName::post_read_actions,
		nsEDDEngine::AttributeName::pre_write_actions,
		nsEDDEngine::AttributeName::post_write_actions,
		nsEDDEngine::AttributeName::pre_edit_actions,
		nsEDDEngine::AttributeName::post_edit_actions,
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::variable_type,
		nsEDDEngine::AttributeName::display_format,
		nsEDDEngine::AttributeName::edit_format,
		nsEDDEngine::AttributeName::min_value,
		nsEDDEngine::AttributeName::max_value,
		nsEDDEngine::AttributeName::scaling_factor,
		nsEDDEngine::AttributeName::enumerations,
		nsEDDEngine::AttributeName::indexed,
		nsEDDEngine::AttributeName::refresh_actions,
		nsEDDEngine::AttributeName::write_mode,
		
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//FF_ConvVAR_DEPBIN() - Fills in an FDI VAR_DEPBIN from DDS VAR_DEPBIN
FF_ConvVAR_DEPBIN::FF_ConvVAR_DEPBIN( ::FLAT_VAR *pFlatVar, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pFlatVar->depbin, db_class, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_handling, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_type_size, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_display, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_edit, pLegacyFF )
	HOOK_DEPBIN( pFlatVar->depbin, db_enums, pLegacyFF )

	if( pFlatVar->depbin->db_index_item_array != NULL )
	{
		db_indexed = new FF_ConvDEPBIN( pFlatVar->depbin->db_index_item_array, pLegacyFF );
	}

	if( pFlatVar->depbin->db_resp_codes != NULL )
	{
		db_response_codes = new FF_ConvDEPBIN( pFlatVar->depbin->db_resp_codes, pLegacyFF );
	}
	
	// action depbins
	if((pFlatVar->actions != NULL) && (pFlatVar->actions->depbin != NULL))
	{
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_edit_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_edit_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_read_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_read_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_write_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_write_act, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_refresh_act, pLegacyFF )
	}

	// misc depbins
	if((pFlatVar->misc != NULL) && pFlatVar->misc->depbin != NULL)
	{
		if( pFlatVar->misc->depbin->db_unit != NULL )
		{
			db_constant_unit = new FF_ConvDEPBIN( pFlatVar->misc->depbin->db_unit, pLegacyFF );
		}
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_min_val, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_max_val, pLegacyFF )
		if( pFlatVar->misc->depbin->db_scale != NULL )
		{
			db_scaling_factor = new FF_ConvDEPBIN( pFlatVar->misc->depbin->db_scale, pLegacyFF );
		}
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_valid, pLegacyFF )
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_write_mode, pLegacyFF )
	}
}


//FF_ConvFLAT_ARRAY() - Fills in an FDI FLAT_ARRAY from DDS FLAT_ARRAY
FF_ConvFLAT_ARRAY::FF_ConvFLAT_ARRAY( ::FLAT_ARRAY *pFlatArray, CLegacyFF *pLegacyFF )
{
	id = pFlatArray->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatArray->masks, nsEDDEngine::ITYPE_ARRAY);

	num_of_elements = pFlatArray->num_of_elements;

	CLegacyFF::Fill_STRING(&help, &pFlatArray->help);
	CLegacyFF::Fill_STRING(&label, &pFlatArray->label);

	valid = CLegacyFF::SetBoolean(pFlatArray->valid);
	
	type = pFlatArray->type;

	if ( pFlatArray->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatArray->resp_codes;
	}

	write_mode = (nsEDDEngine::WriteMode)pFlatArray->write_mode;

	if(pFlatArray->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvARRAY_DEPBIN( pFlatArray->depbin, pLegacyFF);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_ARRAY::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::type_definition,
		nsEDDEngine::AttributeName::number_of_elements,
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::write_mode,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//FF_ConvARRAY_DEPBIN() - Fills in an FDI ARRAY_DEPBIN from DDS ARRAY_DEPBIN
FF_ConvARRAY_DEPBIN::FF_ConvARRAY_DEPBIN( ::ARRAY_DEPBIN *pArrayDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pArrayDepbin, db_num_of_elements, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_valid, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_type, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_resp_codes, pLegacyFF )
	HOOK_DEPBIN( pArrayDepbin, db_write_mode, pLegacyFF )
}


//FF_ConvFLAT_CHART() - Fills in an FDI FLAT_CHART from DDS FLAT_CHART
FF_ConvFLAT_CHART::FF_ConvFLAT_CHART( ::FLAT_CHART *pFlatChart, CLegacyFF *pLegacyFF )
{
	id = pFlatChart->id;
	
	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatChart->masks, nsEDDEngine::ITYPE_CHART);

	CLegacyFF::Fill_STRING(&help, &pFlatChart->help);
	CLegacyFF::Fill_STRING(&label, &pFlatChart->label);
	
	pLegacyFF->Fill_MEMBER_LIST(&members, &pFlatChart->members);

	height	=  pLegacyFF->SetDisplaySize(pFlatChart->height);
	width	=  pLegacyFF->SetDisplaySize(pFlatChart->width);

	cycle_time	= pFlatChart->cycle_time;
	length		= pFlatChart->length;

	switch(pFlatChart->type)
	{
	case CHART_TYPE_GAUGE:
		type = nsEDDEngine::CTYPE_GAUGE;
		break;
	case CHART_TYPE_HORIZONTAL_BAR:
		type = nsEDDEngine::CTYPE_HORIZ_BAR;
		break;
	case CHART_TYPE_SCOPE:
		type = nsEDDEngine::CTYPE_SCOPE;
		break;
	case CHART_TYPE_STRIP:
		type = nsEDDEngine::CTYPE_STRIP;
		break;
	case CHART_TYPE_SWEEP:
		type = nsEDDEngine::CTYPE_SWEEP;
		break;
	case CHART_TYPE_VERTICAL_BAR:
		type = nsEDDEngine::CTYPE_VERT_BAR;
		break;
	default:
		EddEngineLog( pLegacyFF, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"FF_ConvFLAT_CHART::FF_ConvFLAT_CHART() cannot convert Chart type: %d", pFlatChart->type );
	}


	valid		= CLegacyFF::SetBoolean(pFlatChart->valid);
	
	if(pFlatChart->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvCHART_DEPBIN( pFlatChart->depbin, pLegacyFF );
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_CHART::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::cycle_time,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::length,
		nsEDDEngine::AttributeName::chart_type,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//FF_ConvARRAY_DEPBIN() - Fills in an FDI ARRAY_DEPBIN from DDS ARRAY_DEPBIN
FF_ConvCHART_DEPBIN::FF_ConvCHART_DEPBIN( ::CHART_DEPBIN *pChartDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pChartDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_members, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_cycle_time, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_height, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_width, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_length, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_type, pLegacyFF )
	HOOK_DEPBIN( pChartDepbin, db_valid, pLegacyFF )
}

FF_ConvFLAT_GRAPH::FF_ConvFLAT_GRAPH( ::FLAT_GRAPH *pFlatGraph, CLegacyFF *pLegacyFF )
{
	id = pFlatGraph->id;
	
	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatGraph->masks, nsEDDEngine::ITYPE_GRAPH);

	CLegacyFF::Fill_STRING(&help, &pFlatGraph->help);
	CLegacyFF::Fill_STRING(&label, &pFlatGraph->label);
	
	pLegacyFF->Fill_MEMBER_LIST(&members, &pFlatGraph->members);

	height	=  pLegacyFF->SetDisplaySize(pFlatGraph->height);
	width	=  pLegacyFF->SetDisplaySize(pFlatGraph->width);

	x_axis	=	pFlatGraph->x_axis;
	valid		= CLegacyFF::SetBoolean(pFlatGraph->valid);
	cycle_time	= pFlatGraph->cycle_time;

	
	if(pFlatGraph->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvGRAPH_DEPBIN( pFlatGraph->depbin, pLegacyFF );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_GRAPH::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::x_axis,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::cycle_time,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvGRAPH_DEPBIN::FF_ConvGRAPH_DEPBIN( ::GRAPH_DEPBIN *pGraphDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN(pGraphDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_members, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_cycle_time, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_height, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_width, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_x_axis, pLegacyFF)
	HOOK_DEPBIN(pGraphDepbin, db_valid, pLegacyFF)
}


FF_ConvFLAT_AXIS::FF_ConvFLAT_AXIS( ::FLAT_AXIS *pFlatAxis, CLegacyFF *pLegacyFF)
{
	id = pFlatAxis->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatAxis->masks, nsEDDEngine::ITYPE_AXIS);

	CLegacyFF::Fill_STRING(&help, &pFlatAxis->help);
	CLegacyFF::Fill_STRING(&label, &pFlatAxis->label);

	CLegacyFF::Fill_EXPR(&min_axis, &pFlatAxis->min_value);
	pLegacyFF->Fill_OP_REF(&this->min_axis_ref, &pFlatAxis->min_value_ref);

	CLegacyFF::Fill_EXPR(&max_axis, &pFlatAxis->max_value);
	pLegacyFF->Fill_OP_REF(&this->max_axis_ref, &pFlatAxis->max_value_ref);

	switch(pFlatAxis->scaling)
	{
	case AXIS_SCALING_LINEAR:
		scaling = nsEDDEngine::FDI_LINEAR_SCALE;
		break;
	case AXIS_SCALING_LOGARITHMIC:
		scaling = nsEDDEngine::FDI_LOG_SCALE;
		break;
	default:
		EddEngineLog(pLegacyFF,  __FILE__, __LINE__, BssError, L"LegacyFF",
			L"FF_ConvFLAT_AXIS::FF_ConvFLAT_AXIS() cannot convert AXIS Scale: %d", pFlatAxis->scaling );
	}

	CLegacyFF::Fill_STRING(&constant_unit, &pFlatAxis->constant_unit);

	if(pFlatAxis->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvAXIS_DEPBIN( pFlatAxis->depbin, pLegacyFF );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_AXIS::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::min_value,
		nsEDDEngine::AttributeName::max_value,
		nsEDDEngine::AttributeName::scaling,
		nsEDDEngine::AttributeName::constant_unit,
		nsEDDEngine::AttributeName::max_value_ref,
		nsEDDEngine::AttributeName::min_value_ref,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvAXIS_DEPBIN::FF_ConvAXIS_DEPBIN( ::AXIS_DEPBIN *pAxisDepbin, CLegacyFF* pLegacyFF)
{
	HOOK_DEPBIN( pAxisDepbin, db_help, pLegacyFF );
	HOOK_DEPBIN( pAxisDepbin, db_label, pLegacyFF );
	if( pAxisDepbin->db_min_value != NULL )
	{
		this->db_min_axis = new FF_ConvDEPBIN( pAxisDepbin->db_min_value, pLegacyFF );
	}
	if( pAxisDepbin->db_max_value != NULL )
	{
		this->db_max_axis = new FF_ConvDEPBIN( pAxisDepbin->db_max_value, pLegacyFF );
	}
	HOOK_DEPBIN( pAxisDepbin, db_scaling, pLegacyFF );
	HOOK_DEPBIN( pAxisDepbin, db_constant_unit, pLegacyFF );
}


FF_ConvFLAT_WAVEFORM::FF_ConvFLAT_WAVEFORM( ::FLAT_WAVEFORM *pFlatWaveform, CLegacyFF *pLegacyFF )
{
	id = pFlatWaveform->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatWaveform->masks, nsEDDEngine::ITYPE_WAVEFORM);

	CLegacyFF::Fill_STRING(&help, &pFlatWaveform->help);
	CLegacyFF::Fill_STRING(&label, &pFlatWaveform->label);

	valid = CLegacyFF::SetBoolean(pFlatWaveform->valid);
	emphasis = CLegacyFF::SetBoolean(pFlatWaveform->emphasis);
	line_type = pLegacyFF->SetLineType(pFlatWaveform->line_type);

	handling = CLegacyFF::SetHandling(pFlatWaveform->handling);

	CLegacyFF::Fill_ACTION_LIST(&init_actions, &pFlatWaveform->init_actions);
	CLegacyFF::Fill_ACTION_LIST(&refresh_actions, &pFlatWaveform->refresh_actions);
	CLegacyFF::Fill_ACTION_LIST(&exit_actions, &pFlatWaveform->exit_actions);

	switch(pFlatWaveform->type)
	{
	case WAVEFORM_TYPE_YT:
		waveform_type = nsEDDEngine::FDI_YT_WAVEFORM_TYPE;
		break;
	case WAVEFORM_TYPE_XY:
		waveform_type = nsEDDEngine::FDI_XY_WAVEFORM_TYPE;
		break;
	case WAVEFORM_TYPE_HORIZONTAL:
		waveform_type = nsEDDEngine::FDI_HORZ_WAVEFORM_TYPE;
		break;
	case WAVEFORM_TYPE_VERTICAL:
		waveform_type = nsEDDEngine::FDI_VERT_WAVEFORM_TYPE;
		break;
	default:
		EddEngineLog( pLegacyFF, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"FF_ConvFLAT_WAVEFORM::FF_ConvFLAT_WAVEFORM() cannot convert WAVEFORM type: %d", pFlatWaveform->type );
	}
	
	CLegacyFF::Fill_EXPR(&x_initial, &pFlatWaveform->x_initial);
	CLegacyFF::Fill_EXPR(&x_increment, &pFlatWaveform->x_increment);
	
	number_of_points = pFlatWaveform->number_of_points;

	pLegacyFF->Fill_DATA_ITEM_LIST(&x_values, &pFlatWaveform->x_values);
	pLegacyFF->Fill_DATA_ITEM_LIST(&y_values, &pFlatWaveform->y_values);
	pLegacyFF->Fill_DATA_ITEM_LIST(&key_x_values, &pFlatWaveform->key_x_values);
	pLegacyFF->Fill_DATA_ITEM_LIST(&key_y_values, &pFlatWaveform->key_y_values);

	line_color	= pFlatWaveform->line_color;
	y_axis		= pFlatWaveform->y_axis;

	if(pFlatWaveform->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvWAVEFORM_DEPBIN( pFlatWaveform->depbin, pLegacyFF );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_WAVEFORM::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::emphasis,
		nsEDDEngine::AttributeName::line_type,
		(nsEDDEngine::AttributeName) -1,	// Not Used	
		nsEDDEngine::AttributeName::init_actions,
		nsEDDEngine::AttributeName::refresh_actions,
		nsEDDEngine::AttributeName::waveform_type,
		nsEDDEngine::AttributeName::x_initial,
		nsEDDEngine::AttributeName::x_increment,
		nsEDDEngine::AttributeName::number_of_points,
		nsEDDEngine::AttributeName::y_values,
		nsEDDEngine::AttributeName::x_values,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::line_color,
		nsEDDEngine::AttributeName::keypoints_y_values,
		nsEDDEngine::AttributeName::keypoints_x_values,
		nsEDDEngine::AttributeName::y_axis,
		nsEDDEngine::AttributeName::exit_actions,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvWAVEFORM_DEPBIN::FF_ConvWAVEFORM_DEPBIN( ::WAVEFORM_DEPBIN *pWaveformDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pWaveformDepbin, db_help, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_label, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_valid, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_emphasis, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_line_type, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_handling, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_init_actions, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_refresh_actions, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_exit_actions, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_type, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_x_initial, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_x_increment, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_number_of_points, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_x_values, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_y_values, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_key_x_values, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_key_y_values, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_line_color, pLegacyFF )
    HOOK_DEPBIN( pWaveformDepbin, db_y_axis, pLegacyFF )
}


FF_ConvFLAT_SOURCE::FF_ConvFLAT_SOURCE( ::FLAT_SOURCE *pFlatSource, CLegacyFF *pLegacyFF )
{
	id = pFlatSource->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatSource->masks, nsEDDEngine::ITYPE_SOURCE);

	CLegacyFF::Fill_STRING(&help, &pFlatSource->help);
	CLegacyFF::Fill_STRING(&label, &pFlatSource->label);

	pLegacyFF->Fill_OP_MEMBER_LIST(&op_members, &pFlatSource->members);
	
	emphasis	= CLegacyFF::SetBoolean(pFlatSource->emphasis);
	y_axis		= pFlatSource->y_axis;
	line_type	= pLegacyFF->SetLineType(pFlatSource->line_type);
	line_color	= pFlatSource->line_color;

	CLegacyFF::Fill_ACTION_LIST(&init_actions, &pFlatSource->init_actions);
	CLegacyFF::Fill_ACTION_LIST(&refresh_actions, &pFlatSource->refresh_actions);
	CLegacyFF::Fill_ACTION_LIST(&exit_actions, &pFlatSource->exit_actions);

	valid	= CLegacyFF::SetBoolean(pFlatSource->valid);

	if(pFlatSource->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvSOURCE_DEPBIN( pFlatSource->depbin, pLegacyFF );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_SOURCE::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::emphasis,
		nsEDDEngine::AttributeName::line_type,
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::y_axis,
		nsEDDEngine::AttributeName::line_color,
		nsEDDEngine::AttributeName::init_actions,
		nsEDDEngine::AttributeName::refresh_actions,
		nsEDDEngine::AttributeName::exit_actions,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvSOURCE_DEPBIN::FF_ConvSOURCE_DEPBIN( ::SOURCE_DEPBIN *pSourceDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pSourceDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_members, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_emphasis, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_y_axis, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_line_type, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_line_color, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_init_actions, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_refresh_actions, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_exit_actions, pLegacyFF)
	HOOK_DEPBIN( pSourceDepbin, db_valid, pLegacyFF)
}


FF_ConvFLAT_GRID::FF_ConvFLAT_GRID( ::FLAT_GRID *pFlatGrid, CLegacyFF *pLegacyFF )
{
	id = pFlatGrid->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatGrid->masks, nsEDDEngine::ITYPE_GRID);

	CLegacyFF::Fill_STRING(&help, &pFlatGrid->help);
	CLegacyFF::Fill_STRING(&label, &pFlatGrid->label);

	pLegacyFF->Fill_VECTOR_LIST(&vectors, &pFlatGrid->vectors);

	height		= pLegacyFF->SetDisplaySize(pFlatGrid->height);
	width		= pLegacyFF->SetDisplaySize(pFlatGrid->width);
	handling	= CLegacyFF::SetHandling(pFlatGrid->handling);
	valid		= CLegacyFF::SetBoolean(pFlatGrid->validity);
	switch(pFlatGrid->orientation)
	{

	case GRID_ORIENTATION_VERTICAL:
		orientation = nsEDDEngine::FDI_ORIENT_VERT;
		break;
	case GRID_ORIENTATION_HORIZONTAL:
		orientation = nsEDDEngine::FDI_ORIENT_HORIZ;
		break;
	default:
		EddEngineLog( pLegacyFF, __FILE__, __LINE__, BssError, L"LegacyFF",
			L"FF_ConvFLAT_GRID::FF_ConvFLAT_GRID() cannot convert GRID orientation: %d", pFlatGrid->orientation );
	}

	if(pFlatGrid->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvGRID_DEPBIN( pFlatGrid->depbin, pLegacyFF );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_GRID::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::vectors,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::orientation,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvGRID_DEPBIN::FF_ConvGRID_DEPBIN( ::GRID_DEPBIN *pGridDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pGridDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_vectors, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_height, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_width, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_handling, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_valid, pLegacyFF)
	HOOK_DEPBIN( pGridDepbin, db_orientation, pLegacyFF)
}



FF_ConvFLAT_IMAGE::FF_ConvFLAT_IMAGE( ::FLAT_IMAGE * pFlatImage, CLegacyFF *pLegacyFF)
{
	id = pFlatImage->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatImage->masks, nsEDDEngine::ITYPE_IMAGE);

	CLegacyFF::Fill_STRING(&help, &pFlatImage->help);
	CLegacyFF::Fill_STRING(&label, &pFlatImage->label);

	link.id	= pFlatImage->link.id;
	link.type = pLegacyFF->Set_ITEM_TYPE(pFlatImage->link.type);

	valid	= CLegacyFF::SetBoolean(pFlatImage->valid);

	CLegacyFF::Fill_STRING(&image_id, &pFlatImage->path);

	if(pFlatImage->entry.length != 0)
	{
		entry.data = new(unsigned char[pFlatImage->entry.length]);
		entry.length = pFlatImage->entry.length;
		memcpy(entry.data,
			pFlatImage->entry.data,
			entry.length);
	}

	if(pFlatImage->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvIMAGE_DEPBIN( pFlatImage->depbin, pLegacyFF );
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_IMAGE::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::image_id,   // IMAGE_PATH
		nsEDDEngine::AttributeName::link,
		nsEDDEngine::AttributeName::validity,  
		nsEDDEngine::AttributeName::image_table_index, // IMAGE_BINARY
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvIMAGE_DEPBIN::FF_ConvIMAGE_DEPBIN( ::IMAGE_DEPBIN *pImageDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pImageDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pImageDepbin, db_label, pLegacyFF)

	if( pImageDepbin->db_path != NULL )
	{
		this->db_image_table_index = new FF_ConvDEPBIN( pImageDepbin->db_path, pLegacyFF );
	}

	HOOK_DEPBIN( pImageDepbin, db_link, pLegacyFF)
	HOOK_DEPBIN( pImageDepbin, db_valid, pLegacyFF)
}


FF_ConvFLAT_METHOD::FF_ConvFLAT_METHOD( ::FLAT_METHOD *pFlatMethod, CLegacyFF *pLegacyFF )
{
	id = pFlatMethod->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatMethod->masks, nsEDDEngine::ITYPE_METHOD);

	class_attr = CLegacyFF::SetClassType(pFlatMethod->class_attr);
	def.size	=	pFlatMethod->def.size;

	if(pFlatMethod->def.data != 0)
	{
		LPCWSTR wChar = pFlatMethod->def.data;	// Legacy FF method definition is already wchar_t

		def.data	= new wchar_t[pFlatMethod->def.size + 1];
		PS_Wcscpy(def.data, pFlatMethod->def.size + 1, wChar);
	}
	 
	CLegacyFF::Fill_STRING(&help, &pFlatMethod->help);
	CLegacyFF::Fill_STRING(&label, &pFlatMethod->label);
	valid = CLegacyFF::SetBoolean(pFlatMethod->valid);
	
#if 0	// Diff from HART
	switch(pFlatMethod->type)
	{
	case TYPE_VOID:
		type = nsEDDEngine::MT_VOID;
		break;
	case TYPE_INT__8:
		type = nsEDDEngine::MT_INT_8;
		break;
	case TYPE_INT_16:
		type = nsEDDEngine::MT_INT_16;
		break;
	case TYPE_INT_32:
		type = nsEDDEngine::MT_INT_32;
		break;
	case TYPE_INT_64:
		type = nsEDDEngine::MT_INT_64;
		break;
	case TYPE_FLOAT:
		type = nsEDDEngine::MT_FLOAT;
		break;
	case TYPE_DOUBLE:
		type = nsEDDEngine::MT_DOUBLE;
		break;
	case TYPE_UINT__8:
		type = nsEDDEngine::MT_UINT_8;
		break;
	case TYPE_UINT_16:
		type = nsEDDEngine::MT_UINT_16;
		break;
	case TYPE_UINT_32:
		type = nsEDDEngine::MT_UINT_32;
		break;
	case TYPE_UINT_64:
		type = nsEDDEngine::MT_UINT_64;
		break;
	case TYPE_DDSTRING:
		type = nsEDDEngine::MT_DDSTRING;
		break;
	case TYPE_DD_ITEM:
		type = nsEDDEngine::MT_DD_ITEM;
		break;
	default:
		break;
	}
	
	
	CLegacyFF::Fill_STRING(&params, &pFlatMethod->params);
#endif	// Diff from HART	

	//access = pFlatMethod-> 

	if(pFlatMethod->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvMETHOD_DEPBIN( pFlatMethod->depbin, pLegacyFF );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_METHOD::attr_map[] = {
		nsEDDEngine::AttributeName::class_attr,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::definition,
		nsEDDEngine::AttributeName::validity,
		(nsEDDEngine::AttributeName) -1,					//METHOD_SCOPE_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvMETHOD_DEPBIN::FF_ConvMETHOD_DEPBIN( ::METHOD_DEPBIN *pMethodDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pMethodDepbin, db_class, pLegacyFF)
	HOOK_DEPBIN( pMethodDepbin, db_def, pLegacyFF)
	HOOK_DEPBIN( pMethodDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN( pMethodDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pMethodDepbin, db_valid, pLegacyFF)
}



FF_ConvFLAT_MENU::FF_ConvFLAT_MENU( ::FLAT_MENU *pFlatMenu, CLegacyFF *pLegacyFF )
{
	id = pFlatMenu->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatMenu->masks, nsEDDEngine::ITYPE_MENU);

//	CLegacyFF::Fill_STRING(&help, &pFlatMenu->help);
	CLegacyFF::Fill_STRING(&label, &pFlatMenu->label);

	valid = CLegacyFF::SetBoolean( pFlatMenu->valid );

	switch( pFlatMenu->style )
	{
	case MENU_STYLE_DIALOG:
		style = nsEDDEngine::FDI_DIALOG_STYLE_TYPE;
		break;
	case MENU_STYLE_WINDOW:
		style = nsEDDEngine::FDI_WINDOW_STYLE_TYPE;
		break;
	case MENU_STYLE_PAGE:
		style = nsEDDEngine::FDI_PAGE_STYLE_TYPE;
		break;
	case MENU_STYLE_GROUP:
		style = nsEDDEngine::FDI_GROUP_STYLE_TYPE;
		break;
	case MENU_STYLE_MENU:
		style = nsEDDEngine::FDI_MENU_STYLE_TYPE;
		break;
	case MENU_STYLE_TABLE:
		style = nsEDDEngine::FDI_TABLE_STYLE_TYPE;
		break;
	default:
		style = nsEDDEngine::FDI_NO_STYLE_TYPE;
		break;
	}

	if(pFlatMenu->items.count != 0)
	{
		items.count = pFlatMenu->items.count;
		items.limit = pFlatMenu->items.count;

		items.list = new nsEDDEngine::MENU_ITEM[pFlatMenu->items.count];
		
		for(int i = 0; i < pFlatMenu->items.count; i++)
		{
			pLegacyFF->Fill_OP_REF_TRAIL(&items.list[i].ref, &pFlatMenu->items.list[i].ref);

			if(pFlatMenu->items.list[i].qual & READ_ONLY_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_READ_ONLY_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & DISPLAY_VALUE_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_DISPLAY_VALUE_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & REVIEW_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_REVIEW_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & HIDDEN_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_HIDDEN_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & NO_LABEL_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_NO_LABEL_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & NO_UNIT_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_NO_UNIT_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & INLINE_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_INLINE_ITEM;
			}

			if(pFlatMenu->items.list[i].qual & SEPARATOR_ITEM)
			{
				items.list[i].ref.op_ref_type = nsEDDEngine::STANDARD_TYPE;
				items.list[i].ref.op_info.type =  nsEDDEngine::ITYPE_COLUMNBREAK;
				items.list[i].ref.op_info.member = 0;
				
				items.list[i].ref.desc_id = 0;
				items.list[i].ref.desc_type =  nsEDDEngine::ITYPE_COLUMNBREAK;
			}

			if(pFlatMenu->items.list[i].qual & STRING_ITEM)
			{
				items.list[i].ref.op_ref_type = nsEDDEngine::STANDARD_TYPE;
				items.list[i].ref.op_info.type =  nsEDDEngine::ITYPE_STRING_LITERAL;
				items.list[i].ref.op_info.member = 0;
				
				items.list[i].ref.expr.val.s = pFlatMenu->items.list[i].user_string.str;
				items.list[i].ref.expr.eType = nsEDDEngine::EXPR::EXPR_TYPE_STRING;
				items.list[i].ref.desc_type =  nsEDDEngine::ITYPE_STRING_LITERAL;
			}

			if(pFlatMenu->items.list[i].qual & ROWBREAK_ITEM)
			{
				items.list[i].ref.op_ref_type = nsEDDEngine::STANDARD_TYPE;
				items.list[i].ref.op_info.type =  nsEDDEngine::ITYPE_ROWBREAK;
				items.list[i].ref.op_info.member = 0;
				
				items.list[i].ref.desc_id = 0;
				items.list[i].ref.desc_type =  nsEDDEngine::ITYPE_ROWBREAK;
			}

			if(pFlatMenu->items.list[i].qual & COLUMNBREAK_ITEM)
			{
				items.list[i].ref.op_ref_type = nsEDDEngine::STANDARD_TYPE;
				items.list[i].ref.op_info.type =  nsEDDEngine::ITYPE_COLUMNBREAK;
				items.list[i].ref.op_info.member = 0;
				
				items.list[i].ref.desc_id = 0;
				items.list[i].ref.desc_type =  nsEDDEngine::ITYPE_COLUMNBREAK;
			}
		}
	}

	//access is not available in DDS
	// pre_edit_act, post_edit_act, pre_read_act, post_read_act, pre_write_act
	// and post_write_act are not available in DDS
	//CLegacyFF::Fill_ACTION_LIST(&pre_edit_act, &pFlatMenu->)
		
	if(pFlatMenu->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvMENU_DEPBIN( pFlatMenu->depbin, pLegacyFF );
	}

}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_MENU::attr_map[] = {
		
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::items,
		nsEDDEngine::AttributeName::style,
		(nsEDDEngine::AttributeName) -1,		//MENU_STYLE_STRING_ID
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvMENU_DEPBIN::FF_ConvMENU_DEPBIN( ::MENU_DEPBIN *pMenuDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN( pMenuDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pMenuDepbin, db_items, pLegacyFF )
	HOOK_DEPBIN( pMenuDepbin, db_valid, pLegacyFF )
	HOOK_DEPBIN( pMenuDepbin, db_style, pLegacyFF )
}




FF_ConvFLAT_EDIT_DISPLAY::FF_ConvFLAT_EDIT_DISPLAY( ::FLAT_EDIT_DISPLAY *pFlatEditDisplay, CLegacyFF *pLegacyFF )
{
	id = pFlatEditDisplay->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatEditDisplay->masks, nsEDDEngine::ITYPE_EDIT_DISP);

	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&disp_items, &pFlatEditDisplay->disp_items);
	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&edit_items, &pFlatEditDisplay->edit_items);

//	CLegacyFF::Fill_STRING(&help, &pFlatEditDisplay->help);
	CLegacyFF::Fill_STRING(&label, &pFlatEditDisplay->label);

//	valid	= CLegacyFF::SetBoolean(pFlatEditDisplay->valid);

	CLegacyFF::Fill_ACTION_LIST(&pre_edit_act, &pFlatEditDisplay->pre_edit_act);
	CLegacyFF::Fill_ACTION_LIST(&post_edit_act, &pFlatEditDisplay->post_edit_act);

	if(pFlatEditDisplay->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvEDIT_DISPLAY_DEPBIN( pFlatEditDisplay->depbin, pLegacyFF );
	}


}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_EDIT_DISPLAY::attr_map[] = {
		
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::edit_items,
		nsEDDEngine::AttributeName::display_items,
		nsEDDEngine::AttributeName::pre_edit_actions,
		nsEDDEngine::AttributeName::post_edit_actions,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvEDIT_DISPLAY_DEPBIN::FF_ConvEDIT_DISPLAY_DEPBIN( ::EDIT_DISPLAY_DEPBIN *pEditDisplayDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pEditDisplayDepbin, db_disp_items, pLegacyFF)
	HOOK_DEPBIN( pEditDisplayDepbin, db_edit_items, pLegacyFF)
	HOOK_DEPBIN( pEditDisplayDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN( pEditDisplayDepbin, db_pre_edit_act, pLegacyFF)
	HOOK_DEPBIN( pEditDisplayDepbin, db_post_edit_act, pLegacyFF)
}



FF_ConvFLAT_REFRESH::FF_ConvFLAT_REFRESH( ::FLAT_REFRESH *pFlatRefresh, CLegacyFF *pLegacyFF )
{

	id = pFlatRefresh->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatRefresh->masks, nsEDDEngine::ITYPE_REFRESH);

	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&depend_items, &pFlatRefresh->items.depend_items);
	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&update_items, &pFlatRefresh->items.update_items);

	if(pFlatRefresh->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvREFRESH_DEPBIN( pFlatRefresh->depbin, pLegacyFF  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_REFRESH::attr_map[] = {
		
		// Because there is no direct mapping for REFRESH, we have to hard code this.
		// See CLegacyFF::Fill_Bitmask() and CLegacyFF::Fill_AttributeNameSet() for more detail
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvREFRESH_DEPBIN::FF_ConvREFRESH_DEPBIN( ::REFRESH_DEPBIN *pRefreshDepbin, CLegacyFF *pLegacyFF)
{
	// can't use HOOK_DEPBIN macro as depbin names are different. 
	if(pRefreshDepbin->db_items != NULL)
	{
		this->db_depend_items = new FF_ConvDEPBIN(pRefreshDepbin->db_items, pLegacyFF);
		this->db_update_items = new FF_ConvDEPBIN(pRefreshDepbin->db_items, pLegacyFF);
	}

}

FF_ConvFLAT_UNIT::FF_ConvFLAT_UNIT( ::FLAT_UNIT *pFlatUnit, CLegacyFF *pLegacyFF )
{
	id = pFlatUnit->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatUnit->masks, nsEDDEngine::ITYPE_UNIT);

	pLegacyFF->Fill_OP_REF_TRAIL(&var, &pFlatUnit->items.var); 
	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&var_units, &pFlatUnit->items.var_units);
	
	if(pFlatUnit->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvUNIT_DEPBIN( pFlatUnit->depbin, pLegacyFF  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_UNIT::attr_map[] = {
		
		// Because there is no direct mapping for UNIT, we have to hard code this.
		// See CLegacyFF::Fill_Bitmask() and CLegacyFF::Fill_AttributeNameSet() for more detail
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvUNIT_DEPBIN::FF_ConvUNIT_DEPBIN( ::UNIT_DEPBIN *pUnitDepbin, CLegacyFF *pLegacyFF )
{
	// can't use HOOK_DEPBIN macro as depbin names are different. 
	if(pUnitDepbin->db_items != NULL)
	{
		this->db_var		= new FF_ConvDEPBIN(pUnitDepbin->db_items, pLegacyFF);
		this->db_var_units	= new FF_ConvDEPBIN(pUnitDepbin->db_items, pLegacyFF);
	}
}

FF_ConvFLAT_WAO::FF_ConvFLAT_WAO( ::FLAT_WAO *pFlatWao, CLegacyFF *pLegacyFF )
{
	id = pFlatWao->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatWao->masks, nsEDDEngine::ITYPE_WAO);

	pLegacyFF->Fill_OP_REF_TRAIL_LIST(&items, &pFlatWao->items);
	
	if(pFlatWao->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvWAO_DEPBIN( pFlatWao->depbin, pLegacyFF );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_WAO::attr_map[] = {
		
		nsEDDEngine::AttributeName::relation_update_list,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvWAO_DEPBIN::FF_ConvWAO_DEPBIN( ::WAO_DEPBIN *pWaoDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pWaoDepbin, db_items, pLegacyFF)
}


FF_ConvFLAT_BLOCK::FF_ConvFLAT_BLOCK( ::FLAT_BLOCK *pFlatBlock, CLegacyFF *pLegacyFF)
{
	id = pFlatBlock->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatBlock->masks, nsEDDEngine::ITYPE_BLOCK);

	characteristic = pFlatBlock->characteristic;

	CLegacyFF::Fill_STRING(&help, &pFlatBlock->help);
	CLegacyFF::Fill_STRING(&label, &pFlatBlock->label);

	pLegacyFF->Fill_MEMBER_LIST(&param, &pFlatBlock->param);
	CLegacyFF::Fill_ITEM_ID_LIST(&item_array, &pFlatBlock->item_array);
	CLegacyFF::Fill_ITEM_ID_LIST(&collect, &pFlatBlock->collect);
	CLegacyFF::Fill_ITEM_ID_LIST(&menu, &pFlatBlock->menu);
	CLegacyFF::Fill_ITEM_ID_LIST(&edit_disp, &pFlatBlock->edit_disp);
	CLegacyFF::Fill_ITEM_ID_LIST(&method, &pFlatBlock->method);
	CLegacyFF::Fill_ITEM_ID_LIST(&unit, &pFlatBlock->unit);
	CLegacyFF::Fill_ITEM_ID_LIST(&refresh, &pFlatBlock->refresh);
	CLegacyFF::Fill_ITEM_ID_LIST(&wao, &pFlatBlock->wao);

	CLegacyFF::Fill_ITEM_ID_LIST(&chart, &pFlatBlock->chart);
	CLegacyFF::Fill_ITEM_ID_LIST(&graph, &pFlatBlock->graph);
	CLegacyFF::Fill_ITEM_ID_LIST(&grid, &pFlatBlock->grid);
	CLegacyFF::Fill_ITEM_ID_LIST(&image, &pFlatBlock->image);
	CLegacyFF::Fill_ITEM_ID_LIST(&source, &pFlatBlock->source);
	CLegacyFF::Fill_ITEM_ID_LIST(&axis, &pFlatBlock->axis);
	CLegacyFF::Fill_ITEM_ID_LIST(&waveform, &pFlatBlock->waveform);
	CLegacyFF::Fill_ITEM_ID_LIST(&file, &pFlatBlock->file);
	CLegacyFF::Fill_ITEM_ID_LIST(&list, &pFlatBlock->list);

	pLegacyFF->Fill_MEMBER_LIST(&charts, &pFlatBlock->chart_members);
	pLegacyFF->Fill_MEMBER_LIST(&graphs, &pFlatBlock->graph_members);
	pLegacyFF->Fill_MEMBER_LIST(&grids, &pFlatBlock->grid_members);
	pLegacyFF->Fill_MEMBER_LIST(&lists, &pFlatBlock->list_members);
	pLegacyFF->Fill_MEMBER_LIST(&local_param, &pFlatBlock->local_param);
	pLegacyFF->Fill_MEMBER_LIST(&menus, &pFlatBlock->menu_members);
	pLegacyFF->Fill_MEMBER_LIST(&methods, &pFlatBlock->method_members);
	pLegacyFF->Fill_MEMBER_LIST(&param_lists, &pFlatBlock->param_list);

	pLegacyFF->Fill_MEMBER_LIST(&files, &pFlatBlock->file_members);

	if(pFlatBlock->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvBLOCK_DEPBIN( pFlatBlock->depbin, pLegacyFF  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_BLOCK::attr_map[] = {
		
		nsEDDEngine::AttributeName::characteristics,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::parameters,
		nsEDDEngine::AttributeName::menu_items,
		nsEDDEngine::AttributeName::edit_display_items,
		nsEDDEngine::AttributeName::method_items,
		nsEDDEngine::AttributeName::refresh_items,
		nsEDDEngine::AttributeName::unit_items,
		nsEDDEngine::AttributeName::write_as_one_items,				//BLOCK_COLLECT_ID
		nsEDDEngine::AttributeName::collection_items,				//BLOCK_ITEM_ARRAY_ID
		nsEDDEngine::AttributeName::reference_array_items,
		nsEDDEngine::AttributeName::parameter_lists,
		nsEDDEngine::AttributeName::axis_items,
		nsEDDEngine::AttributeName::chart_items,
		nsEDDEngine::AttributeName::file_items,
		nsEDDEngine::AttributeName::graph_items,
		nsEDDEngine::AttributeName::list_items,
		nsEDDEngine::AttributeName::source_items,
		nsEDDEngine::AttributeName::waveform_items,
		nsEDDEngine::AttributeName::local_parameters,
		nsEDDEngine::AttributeName::grid_items,
		nsEDDEngine::AttributeName::image_items,
		nsEDDEngine::AttributeName::charts,
		nsEDDEngine::AttributeName::files,
		nsEDDEngine::AttributeName::graphs,
		nsEDDEngine::AttributeName::grids,
		nsEDDEngine::AttributeName::menus,
		nsEDDEngine::AttributeName::methods,
		nsEDDEngine::AttributeName::lists,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


FF_ConvBLOCK_DEPBIN::FF_ConvBLOCK_DEPBIN( ::BLOCK_DEPBIN *pBlockDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pBlockDepbin, db_characteristic, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_param, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_param_list, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_item_array, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_collect, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_menu, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_edit_disp, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_method, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_unit, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_refresh, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_wao, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_axis, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_chart, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_file, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_graph, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_list, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_source, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_waveform, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_local_param, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_grid, pLegacyFF )
	HOOK_DEPBIN( pBlockDepbin, db_image, pLegacyFF )

	// can't use HOOK_DEPBIN macro as depbin names are different. 
	if(pBlockDepbin->db_chart_members != NULL)
	{
		this->db_charts		= new FF_ConvDEPBIN(pBlockDepbin->db_chart_members, pLegacyFF);
	}

	if(pBlockDepbin->db_file_members != NULL)
	{
		this->db_files		= new FF_ConvDEPBIN(pBlockDepbin->db_file_members, pLegacyFF);
	}

	if(pBlockDepbin->db_graph_members != NULL)
	{
		this->db_graphs		= new FF_ConvDEPBIN(pBlockDepbin->db_graph_members, pLegacyFF);
	}
	
	if(pBlockDepbin->db_grid_members != NULL)
	{
		this->db_grids		= new FF_ConvDEPBIN(pBlockDepbin->db_grid_members, pLegacyFF);
	}

	if(pBlockDepbin->db_menu_members != NULL)
	{
		this->db_menus		= new FF_ConvDEPBIN(pBlockDepbin->db_menu_members, pLegacyFF);
	}

	if(pBlockDepbin->db_method_members != NULL)
	{
		this->db_methods		= new FF_ConvDEPBIN(pBlockDepbin->db_method_members, pLegacyFF);
	}

	if(pBlockDepbin->db_list_members != NULL)
	{
		this->db_lists		= new FF_ConvDEPBIN(pBlockDepbin->db_list_members, pLegacyFF);
	}
}


FF_ConvFLAT_RECORD::FF_ConvFLAT_RECORD( ::FLAT_RECORD *pFlatRecord, CLegacyFF *pLegacyFF )
{
	id = pFlatRecord->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatRecord->masks, nsEDDEngine::ITYPE_RECORD);

	pLegacyFF->Fill_MEMBER_LIST(&members, &pFlatRecord->members);

	CLegacyFF::Fill_STRING(&help, &pFlatRecord->help);

	CLegacyFF::Fill_STRING(&label, &pFlatRecord->label);
	
	if ( pFlatRecord->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatRecord->resp_codes;
	}

	valid = CLegacyFF::SetBoolean(pFlatRecord->valid);

	//write_mode = pFlatRecord->write_mode;

	//private_attr and visibility are not available in Legacy FF.
	//Default values are assigned. 

	private_attr = CLegacyFF::SetBoolean(0); 

	visibility = CLegacyFF::SetBoolean(1);
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_RECORD::attr_map[] = {
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::validity,				//RECORD_VALID_ID
		nsEDDEngine::AttributeName::write_mode,				//RECORD_WRITE_MODE_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvRECORD_DEPBIN::FF_ConvRECORD_DEPBIN( ::RECORD_DEPBIN *pRecordDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pRecordDepbin, db_members, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_resp_codes, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_valid, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_write_mode, pLegacyFF )
}

FF_ConvFLAT_VAR_LIST::FF_ConvFLAT_VAR_LIST( ::FLAT_VAR_LIST *pFlatVarList, CLegacyFF *pLegacyFF )
{
	id = pFlatVarList->id;
	
	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatVarList->masks, nsEDDEngine::ITYPE_VAR_LIST);

	pLegacyFF->Fill_MEMBER_LIST(&members, &pFlatVarList->members);

	CLegacyFF::Fill_STRING(&help, &pFlatVarList->help);

	CLegacyFF::Fill_STRING(&label, &pFlatVarList->label);
	
	resp_codes = pFlatVarList->resp_codes;

	if(pFlatVarList->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new FF_ConvVAR_LIST_DEPBIN( pFlatVarList->depbin, pLegacyFF  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_VAR_LIST::attr_map[] = {
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

FF_ConvVAR_LIST_DEPBIN::FF_ConvVAR_LIST_DEPBIN( ::VAR_LIST_DEPBIN *pRecordDepbin, CLegacyFF *pLegacyFF )
{
	HOOK_DEPBIN( pRecordDepbin, db_members, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_help, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_label, pLegacyFF )
	HOOK_DEPBIN( pRecordDepbin, db_resp_codes, pLegacyFF )
}

void FF_ConvFLAT_TEMPLATE::PostProcessFlatTemplate(int iBlockInstance, nsEDDEngine::FLAT_TEMPLATE* pFlatTemplate, CLegacyFF *pLegacyFF)
{
	nsEDDEngine::AttributeNameSet attribute_list = nsEDDEngine::variable_type;

	for (int i = 0; i < pFlatTemplate->default_values.count; i++)
	{
		if (pFlatTemplate->default_values.list[i].ref.desc_type == nsEDDEngine::ITYPE_VARIABLE)
		{
			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec;

			item_spec.eType = nsEDDEngine::FDI_ITEM_ID;
			item_spec.item.id = pFlatTemplate->default_values.list[i].ref.desc_id;
			item_spec.subindex = 0;

			nsEDDEngine::FDI_GENERIC_ITEM generic_item(nsEDDEngine::ITYPE_VARIABLE);

			int r_code = pLegacyFF->GetItem(iBlockInstance, 0, &item_spec, &attribute_list, L"|en|", &generic_item);

			if (r_code == SUCCESS)
			{

				nsEDDEngine::FLAT_VAR *flat_var = dynamic_cast<nsEDDEngine::FLAT_VAR * >(generic_item.item);

				pFlatTemplate->default_values.list[i].value.SetExprAsPerVariableType(flat_var->type_size);

			}
			// else There is nothing we can do if GetItem fails.
		}
		// else There is nothing we can do if it's not a variable.

	}
}

FF_ConvFLAT_TEMPLATE::FF_ConvFLAT_TEMPLATE(int iBlockInstance, ::FLAT_TEMPLATE *pFlatTemplate, CLegacyFF *pLegacyFF)
{
	id = pFlatTemplate->id;

	CLegacyFF::Fill_FLAT_MASK(&masks, &pFlatTemplate->masks, nsEDDEngine::ITYPE_TEMPLATE);

	CLegacyFF::Fill_STRING(&help, &pFlatTemplate->help);

	CLegacyFF::Fill_STRING(&label, &pFlatTemplate->label);

	valid = CLegacyFF::SetBoolean(pFlatTemplate->valid);
	
	pLegacyFF->Fill_DEFAULT_VALUES_LIST(&default_values, &pFlatTemplate->default_values);

	PostProcessFlatTemplate(iBlockInstance, this, pLegacyFF);

	if (pFlatTemplate->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin = new FF_ConvTEMPLATE_DEPBIN(pFlatTemplate->depbin, pLegacyFF);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName FF_ConvFLAT_TEMPLATE::attr_map[] = {
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::validity,
	nsEDDEngine::AttributeName::default_values,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
};

FF_ConvTEMPLATE_DEPBIN::FF_ConvTEMPLATE_DEPBIN(::TEMPLATE_DEPBIN *pTemplateDepbin, CLegacyFF *pLegacyFF)
{
	HOOK_DEPBIN(pTemplateDepbin, db_help, pLegacyFF)
	HOOK_DEPBIN(pTemplateDepbin, db_label, pLegacyFF)
	HOOK_DEPBIN(pTemplateDepbin, db_default_values, pLegacyFF)
	HOOK_DEPBIN(pTemplateDepbin, db_validity, pLegacyFF)
}

// FF_ConvDEPBIN() - Fills in FDI DEPBIN from DDS DEPBIN
FF_ConvDEPBIN::FF_ConvDEPBIN( ::DEPBIN *pDepbin, CLegacyFF* pLegacyFF)
{
	this->bin_chunk = new unsigned char[pDepbin->bin_size];
	memcpy(this->bin_chunk, pDepbin->bin_chunk, pDepbin->bin_size);
	this->bin_size  = pDepbin->bin_size;

	this->allocated = true;		// Mark that this has been allocated

	if(pDepbin->dep.count != 0)
	{
		this->dep.count = pDepbin->dep.count;
		this->dep.limit = pDepbin->dep.count;

		this->dep.list  = (nsEDDEngine::OP_REF*)malloc(sizeof(nsEDDEngine::OP_REF) * pDepbin->dep.count); 

		for(int i = 0; i < pDepbin->dep.count; i++)
		{
			this->dep.list[i].op_info.id		= pDepbin->dep.list[i].id;
			this->dep.list[i].op_info.member	= pDepbin->dep.list[i].subindex;
			this->dep.list[i].op_info.type		= pLegacyFF->Set_ITEM_TYPE(pDepbin->dep.list[i].type);			
			this->dep.list[i].op_info_list.count = 0;
			this->dep.list[i].op_info_list.list  = nullptr;
		}
	}
}

// Fill_FLAT_MASK() - Fills in FDI FLAT_MASKS from DDS FLAT_MASKS
void CLegacyFF::Fill_FLAT_MASK(nsEDDEngine::FLAT_MASKS* pFdiFlatMask, FLAT_MASKS* pDdiFlatMask, nsEDDEngine::ITEM_TYPE itemType)
{

	Fill_AttributeNameSet(pFdiFlatMask->attr_avail, pDdiFlatMask->attr_avail, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->bin_exists, pDdiFlatMask->bin_exists, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->bin_hooked, pDdiFlatMask->bin_hooked, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->dynamic, pDdiFlatMask->dynamic, itemType);
	/*pFdiFlatMask->attr_avail = pDdiFlatMask->attr_avail;
	pFdiFlatMask->bin_exists = pDdiFlatMask->bin_exists;
	pFdiFlatMask->bin_hooked = pDdiFlatMask->bin_hooked;
	pFdiFlatMask->dynamic = pDdiFlatMask->dynamic;*/
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName* attr_maps[] = {
	nullptr,								// ITYPE_NO_VALUE	= 0,
	FF_ConvFLAT_VAR::attr_map,			// ITYPE_VARIABLE	= 1,
	nullptr,								// ITYPE_COMMAND	= 2,
	FF_ConvFLAT_MENU::attr_map,			// ITYPE_MENU		= 3,
	FF_ConvFLAT_EDIT_DISPLAY::attr_map,	// ITYPE_EDIT_DISP	= 4,
	FF_ConvFLAT_METHOD::attr_map,			// ITYPE_METHOD		= 5,
	FF_ConvFLAT_REFRESH::attr_map,		// ITYPE_REFRESH	= 6,
	FF_ConvFLAT_UNIT::attr_map,			// ITYPE_UNIT		= 7,
	FF_ConvFLAT_WAO::attr_map,			// ITYPE_WAO		= 8,
	FF_ConvFLAT_ITEM_ARRAY::attr_map,		// ITYPE_ITEM_ARRAY	= 9,
	FF_ConvFLAT_COLLECTION::attr_map,		// ITYPE_COLLECTION	= 10,
	nullptr,								// ITYPE_BLOCK_B	= 11,
	FF_ConvFLAT_BLOCK::attr_map,			// ITYPE_BLOCK		= 12,
	nullptr,								// PROGRAM_ITYPE	= 13 //Not used in FDI
	FF_ConvFLAT_RECORD::attr_map,			// ITYPE_RECORD		= 14,
	FF_ConvFLAT_ARRAY::attr_map,			// ITYPE_ARRAY		= 15,
	FF_ConvFLAT_VAR_LIST::attr_map,			// ITYPE_VAR_LIST	= 16,
	FF_ConvFLAT_RESP_CODE::attr_map,		// ITYPE_RESP_CODES	= 17,
	nullptr,								// DOMAIN_ITYPE		= 18  //Not used for FDI
	nullptr,								// ITYPE_MEMBER		= 19,
	FF_ConvFLAT_FILE::attr_map,			// ITYPE_FILE		= 20,
	FF_ConvFLAT_CHART::attr_map,			// ITYPE_CHART		= 21,
	FF_ConvFLAT_GRAPH::attr_map,			// ITYPE_GRAPH		= 22,
	FF_ConvFLAT_AXIS::attr_map,			// ITYPE_AXIS		= 23,
	FF_ConvFLAT_WAVEFORM::attr_map,		// ITYPE_WAVEFORM	= 24,
	FF_ConvFLAT_SOURCE::attr_map,			// ITYPE_SOURCE		= 25,
	FF_ConvFLAT_LIST::attr_map,			// ITYPE_LIST		= 26,
	FF_ConvFLAT_GRID::attr_map,			// ITYPE_GRID		= 27,
	FF_ConvFLAT_IMAGE::attr_map,			// ITYPE_IMAGE		= 28,
	nullptr,								// ITYPE_BLOB		= 29,
	nullptr,								// ITYPE_PLUGIN		= 30,
	FF_ConvFLAT_TEMPLATE::attr_map,			// ITYPE_TEMPLATE	= 31,
	nullptr,								// ITYPE_RESERVED	= 32,
	nullptr,								// ITYPE_COMPONENT	= 33,
	nullptr,								// ITYPE_COMPONENT_FOLDER		= 34,
	nullptr,								// ITYPE_COMPONENT_REFERENCE	= 35,
	nullptr,								// ITYPE_COMPONENT_RELATION		= 36,
	};




void CLegacyFF::Fill_Bitmask( unsigned long& bitmask, const nsEDDEngine::AttributeNameSet& ans, nsEDDEngine::ITEM_TYPE itype)
{
	bitmask = 0;

	if(itype == nsEDDEngine::ITYPE_REFRESH)
	{
		// If either of these attributes are found, set a single bit in the DDS bitmask
		if (	(ans.isMember(nsEDDEngine::relation_watch_list))
			||	(ans.isMember(nsEDDEngine::relation_update_list))
			)
		{
			bitmask |= REFRESH_ITEMS;
		}
	}
	else if(itype == nsEDDEngine::ITYPE_UNIT)
	{
		// If either of these attributes are found, set a single bit in the DDS bitmask
		if (	(ans.isMember(nsEDDEngine::relation_watch_variable))
			||	(ans.isMember(nsEDDEngine::relation_update_list))
			)
		{
			bitmask |= UNIT_ITEMS;
		}
	}
	else
	{
		ntcassert2 (itype < (sizeof(attr_maps) / sizeof(attr_maps[0])) );

		const nsEDDEngine::AttributeName *attr_map = attr_maps[itype];

		ntcassert2(attr_map != nullptr);


		// Look for the AttributeName in our attr_map
		for( int i = 0; attr_map[i] != nsEDDEngine::AttributeName::_end_ ; i++)
		{
            if (attr_map[i] != (nsEDDEngine::AttributeName)-1)
            {
			    if (ans.isMember(attr_map[i]))	// If found, set the corresponding bit, and go to next
			    {
				    bitmask |= (1 << i);
			    }
            }
		}

	}
}



void CLegacyFF::Fill_AttributeNameSet( nsEDDEngine::AttributeNameSet& ans, unsigned long bitmask, nsEDDEngine::ITEM_TYPE itype )
{
	ans.clear();

	if(itype == nsEDDEngine::ITYPE_REFRESH)
	{
		if (bitmask & REFRESH_ITEMS )	// Is this bit set?
		{
			ans.insert(nsEDDEngine::relation_watch_list);
			ans.insert(nsEDDEngine::relation_update_list);
		}
	}
	else if(itype == nsEDDEngine::ITYPE_UNIT)
	{
		if (bitmask & UNIT_ITEMS )	// Is this bit set?
		{
			ans.insert(nsEDDEngine::relation_watch_variable);
			ans.insert(nsEDDEngine::relation_update_list);
		}
	}
	else
	{
		ntcassert2 (itype < (sizeof(attr_maps) / sizeof(attr_maps[0])) );

		const nsEDDEngine::AttributeName *attr_map = attr_maps[itype];

		ntcassert2(attr_map != nullptr);

		// For each entry in attr_map, if the bit is set, add the corresponding AttributeName to the set
		for( int i = 0; bitmask && (attr_map[i] != nsEDDEngine::AttributeName::_end_) ; i++)
		{
			unsigned long cur_mask = 1 << i;

			if (bitmask & cur_mask )	// Is this bit set?
			{
				if(attr_map[i] != -1)
				{
					ans.insert(attr_map[i]);
				}
				bitmask &= ~cur_mask;	// Delete the bit
			}
		}
	}
}





















