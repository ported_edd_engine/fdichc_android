﻿<Method>
{
 int  x;

 x = igetval();
 x = x + 2;
 isetval(x);
 save_values();
}
</Method>
<Function>
Function Name: igetval
  Number Of Input Parameters: 0
  Returned Param Type: RUL_LONGLONG
  Returned Param Value: 75
End of Function igetval
Function Name: isetval
  Number Of Input Parameters: 1
    Param 1 Type: RUL_INT
    Param 1 Value: 77
  Returned Param Type: RUL_LONGLONG
  Returned Param Value: 77
End of Function isetval
IBuiltinSupport ProcessChangedValues() is being called
IBuiltinSupport ProcessChangedValues with returned value 0 () is returned
Function Name: save_values
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function save_values
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><x>77,</x></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
