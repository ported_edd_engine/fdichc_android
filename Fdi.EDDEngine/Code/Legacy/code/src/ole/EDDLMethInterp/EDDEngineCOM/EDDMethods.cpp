// EDDMethods.cpp : Implementation of CEDDMethods

#include "stdafx.h"
#include "nsEDDEngine/ProtocolType.h"
#include "nsEDDEngine/EDDEngineFactory.h"
#include "nsEDDEngine/Ddldefs.h"
#include "EDDMethods.h"					//IEDDEngine.h, IMethods.h
#include <Inf\NtcSpecies\BssProgLog.h>
#include "EDDEngineLogger.h"
//#include "APIs.h"

#include <io.h>
#include <iostream>
#include <fstream>


using namespace nsEDDEngine;

#define CONFIG_XML_MAX 5000

//global variable for calling EndMethod()
nsEDDEngine::IEDDEngine *pGlobalDeviceInstance;

//static variables to CEDDMethods class
CIAsyncList CEDDMethods::lClientAsyncResult;
CRITICAL_SECTION CEDDMethods::cs_EDDMethCriticalSection;

wchar_t *sLanguageCode = L"|en|";


// CEDDMethods

STDMETHODIMP CEDDMethods::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* const arr[] = 
	{
		&IID_IEDDMethods
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}


void CEDDMethods::AsyncCallback(nsEDDEngine::IAsyncResult* ar)
{
	POSITION pos;
	Mth_ErrorCode retVal = METH_ABORTED;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);
	
	//find out the right pointer and remove it from the list befire it has been deleted in EndMethod()
	if ((pos = lClientAsyncResult.Find(ar)) != NULL)	//hunt for ar pointer
	{
		//if method execution is completing, call callback and EndMethod() here
		if (m_bIsActionMethod)
		{
			retVal = pGlobalDeviceInstance->EndVariableActionMethod(&ar, &returnedValue);
		}
		else
		{
			retVal = pGlobalDeviceInstance->EndMethod(&ar, &returnedValue);
		}
		pEDDEngineLogger->CloseLogFile();
		//remove this pointer from lClientAsyncResult
		lClientAsyncResult.RemoveAt(pos);
	}
	else
	{
		ASSERT(false);
	}

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	if (retVal != METH_SUCCESS)
	{
		//aborted or cancelled or stopped due to DD error
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"AsyncCallback(): MI Engine is aborted or cancelled or stopped due to DD error.");
	}
	else
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"AsyncCallback(): MI Engine is completed.");
	}
}


STDMETHODIMP CEDDMethods::BeginMethodInterp(BSTR sMethodName, BOOLEAN bUseCallback, INT* error)
{
	HRESULT hr = E_FAIL;
	IAsyncResult *pLocalIAsyncResult = nullptr;


	OutputDebugStringW(L"\n'Begin Method' button is pressed");

	if (pDeviceInstance == nullptr)
	{
		return S_OK;	//device instance hasn't created and DD hasn't loaded yet
	}

	wchar_t buf[1000];
	wsprintf(buf, L"\nExecuting method -- %s\n", sMethodName);
	OutputDebugStringW (buf);

	//sMethodName is formed as "method id", "method name"("method argument string")
	//Get method id
	wchar_t seps[] = L",";
	wchar_t* sRemainingContext = NULL;
	wchar_t* sMethID = wcstok_s(sMethodName, seps, &sRemainingContext);
	ITEM_ID ulMethID = wcstoul(sMethID, NULL, 10);

	ACTION methodAction;

	if (ulMethID != 0)
	{
		//check if this is a method with parameter or a method without parameter, then fill out the variable methodAction.
		FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
		FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, ulMethID, 0};
		AttributeNameSet attribute_list = method_parameters;

		// call to IEDDEngine GetItem function to get generic item for given item id
		hr = pDeviceInstance->GetItem(m_iBlockInstance, 0, &item_spec, &attribute_list, sLanguageCode, &generic_item);
		if (hr == S_OK) 
		{
			FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);
			if( flat_method )
			{
				if (flat_method->params.length() > 0)
				{
					//this method has input argument
					wchar_t* sRealMethodName = sRemainingContext;
					wchar_t* sTemp = wcsrchr(sRealMethodName, L'(');	//find (
					sTemp[0] = L'\0';									//replace ( with \0
					wchar_t* sMethodArg = sTemp + 1;
					sTemp = wcsrchr(sMethodArg, L')');
					sTemp[0] = L'\0';

					methodAction.eType = ACTION::ACTION_TYPE_REF_WITH_ARGS;
					methodAction.action.meth_ref_args.desc_id = ulMethID;

					methodAction.action.meth_ref_args.expr.eType = EXPR::EXPR_TYPE_STRING;
					methodAction.action.meth_ref_args.expr.size = (unsigned short)wcslen(sMethodArg);
					if (methodAction.action.meth_ref_args.expr.size > 0)
					{
						methodAction.action.meth_ref_args.expr.val.s.assign( sMethodArg, true);
					}
					else
					{
						wsprintf(buf, L"\nPlease enter argument for method %s\n", sRealMethodName);
						OutputDebugStringW (buf);
						hr = E_FAIL;
					}
				}
				else
				{
					//this method has no input argument
					methodAction.eType = ACTION::ACTION_TYPE_REFERENCE;
					methodAction.action.meth_ref = ulMethID;
				}
			}
		}

		if (hr != S_OK)
		{
			*error = 1;
			OutputDebugStringW(L"\nIn function BeginMethodInterp(), failed to run GetItem()");
			return hr;
		}
	}
	else
	{
		//method definition
		methodAction.eType = ACTION::ACTION_TYPE_DEFINITION;
		methodAction.action.meth_definition.size = wcslen(sRemainingContext);
		methodAction.action.meth_definition.data = new wchar_t[methodAction.action.meth_definition.size + 1];
		wcscpy_s(methodAction.action.meth_definition.data, methodAction.action.meth_definition.size + 1, sRemainingContext);
		
		hr = S_OK;
	}

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	try
	{
		m_bIsActionMethod = false;
		pEDDEngineLogger->OpenLogFile((nsEDDEngine::ProtocolType)m_protocol, m_bIsActionMethod);
		if (bUseCallback)
		{
			m_bWithCallbackMethod = true;
			pLocalIAsyncResult = pDeviceInstance->BeginMethod(m_iBlockInstance, (int)0, &methodAction, (IAsyncCallbackHandler *)this, NULL, sLanguageCode);
		}
		else
		{
			m_bWithCallbackMethod = false;
			pLocalIAsyncResult = pDeviceInstance->BeginMethod(m_iBlockInstance, (int)0, &methodAction, (IAsyncCallbackHandler *)nullptr, NULL, sLanguageCode);
		}
	}
	catch (...)
	{
		*error = 1;
		hr = E_FAIL;
		BssProgLog(__FILE__, __LINE__, BssError, E_OUTOFMEMORY, L"BeginMethodInterp: failed to run BeginMethod(): returned pointer = %i.", pLocalIAsyncResult);
		OutputDebugStringW(L"\nIn function BeginMethodInterp(), catch an expection from running BeginMethod()");
	}

	if (pLocalIAsyncResult != nullptr)
	{
		lClientAsyncResult.AddTail(pLocalIAsyncResult);

		*error = 0;	//DDS_SUCCESS;
		hr = S_OK;
	}

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	ATLASSERT(hr == S_OK);

	return hr;
}


STDMETHODIMP CEDDMethods::BeginVarActionMethodInterp(BSTR sVarItemID, INT iActionIndex, BOOLEAN bUseCallback, INT* error)
{
	HRESULT hr = E_FAIL;
	IAsyncResult *pLocalIAsyncResult = nullptr;


	OutputDebugStringW(L"'Begin Variable Action Method' button is pressed\n");

	if (pDeviceInstance == nullptr)
	{
		return S_OK;	//device instance hasn't created and DD hasn't loaded yet
	}

	//get variable item ID from a string
	ULONG ulVarItemID;
	//Convert string to numeric value as item ID
	wchar_t *stopstring = nullptr;
	if ( (sVarItemID[0] == L'0') && ((sVarItemID[1] == L'x') || (sVarItemID[1] == L'X')) )
	{
		ulVarItemID = wcstoul((const wchar_t *)sVarItemID, &stopstring, 16);
	}
	else
	{
		ulVarItemID = wcstoul((const wchar_t *)sVarItemID, &stopstring, 10);
	}

	if (ulVarItemID == 0)
	{
		return S_OK;	//variable item ID hasn't entered yet
	}

	wchar_t buf[1000];
	wsprintf(buf, L"\nExecuting Variable Action method -- %u\n", ulVarItemID);
	OutputDebugStringW (buf);

	//Get variable
	FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, ulVarItemID, 0};
	AttributeNameSet attribute_list = post_read_actions | post_write_actions | pre_read_actions | pre_write_actions | post_edit_actions | pre_edit_actions | variable_type | refresh_actions;

	// call to IEDDEngine GetItem function to get generic item for given item id
	*error = pDeviceInstance->GetItem(m_iBlockInstance, 0, &item_spec, &attribute_list, sLanguageCode, &generic_item);
	if( *error != 0)
	{
		return S_FALSE;
	}
	
	FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);
	if( flat_var == nullptr)
	{
		return S_FALSE;
	}

	//Get variable action
	ACTION methodAction;

	int iActionCount = flat_var->pre_read_act.count;
	int iIndex = iActionIndex;
	int iActionCount1 = iActionCount + flat_var->post_read_act.count;
	int iIndex1 = iActionIndex - iActionCount;
	int iActionCount2 = iActionCount1 + flat_var->pre_write_act.count;
	int iIndex2 = iActionIndex - iActionCount1;
	int iActionCount3 = iActionCount2 + flat_var->post_write_act.count;
	int iIndex3 = iActionIndex - iActionCount2;
	int iActionCount4 = iActionCount3 + flat_var->pre_edit_act.count;
	int iIndex4 = iActionIndex - iActionCount3;
	int iActionCount5 = iActionCount4 + flat_var->post_edit_act.count;
	int iIndex5 = iActionIndex - iActionCount4;
	int iActionCount6 = iActionCount5 + flat_var->refresh_act.count;
	int iIndex6 = iActionIndex - iActionCount5;

	//looking for action within the varaible action lists
	if (iActionIndex < iActionCount)
	{
		methodAction = flat_var->pre_read_act.list[iIndex];
	}
	else if (iActionIndex < iActionCount1) 
	{
		methodAction = flat_var->post_read_act.list[iIndex1];
	}
	else if (iActionIndex < iActionCount2) 
	{
		methodAction = flat_var->pre_write_act.list[iIndex2];
	}
	else if (iActionIndex < iActionCount3) 
	{
		methodAction = flat_var->post_write_act.list[iIndex3];
	}
	else if (iActionIndex < iActionCount4) 
	{
		methodAction = flat_var->pre_edit_act.list[iIndex4];
	}
	else if (iActionIndex < iActionCount5) 
	{
		methodAction = flat_var->post_edit_act.list[iIndex5];
	}
	else if (iActionIndex < iActionCount6)
	{
		methodAction = flat_var->refresh_act.list[iIndex6];
	}

	//Set variable value randomly
	nsConsumer::EVAL_VAR_VALUE value;

	switch(flat_var->type_size.type)
	{
	case VT_ASCII:
	case VT_PASSWORD:
	case VT_PACKED_ASCII:
	case VT_EUC:
	case VT_VISIBLESTRING:
		value.type = flat_var->type_size.type;
		value.val.s.assign(L"This is a string test",false);;
		value.size = (unsigned short) value.val.s.length();
		break;
	case VT_BITSTRING:	// unsigned char[]
	case VT_OCTETSTRING:
		{
			value.type = flat_var->type_size.type;
			value.size = 8;

			uchar usConst[] = {0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40, 0x80};
			value.val.b.assign(usConst, value.size);
		}
		break;
	case VT_TIME_VALUE:
		value.type = flat_var->type_size.type;
		if (flat_var->type_size.size <= 4)
		{
			value.val.u = (unsigned long)175;
			value.size = 4;
		}
		else
		{
			value.val.i = (long long)179;
			value.size = 8;
		}
		break;
	case VT_UNSIGNED:
	case VT_ENUMERATED:
	case VT_BIT_ENUMERATED:
	case VT_INDEX:
	case VT_BOOLEAN:
	case VT_DATE_AND_TIME:
	case VT_TIME:
	case VT_DURATION:
	case VT_OBJECT_REFERENCE:
		value.type = flat_var->type_size.type;
		value.val.u = (unsigned long)175;
		value.size = flat_var->type_size.size;
		break;
		break;
	case VT_FLOAT:	//4 byte real
		value.type = flat_var->type_size.type;
		value.val.f = (float)55.7;
		value.size = 4;
		break;
	case VT_DOUBLE:	//8 byte real
		value.type = flat_var->type_size.type;
		value.val.d = 55.76543;
		value.size = 8;
		break;
	case VT_EDD_DATE:
		value.type = nsEDDEngine::VT_EDD_DATE;
		value.val.i = (unsigned long)0x010275;
		value.size = 4;
		break;
	case VT_INTEGER:
		value.type = flat_var->type_size.type;
		value.val.i = (long)75;
		value.size = 4;
		break;
	default:
		*error = 1;
	}

	try
	{
		m_bIsActionMethod = true;
		pEDDEngineLogger->OpenLogFile((nsEDDEngine::ProtocolType)m_protocol, m_bIsActionMethod);
		if (bUseCallback)
		{
			m_bWithCallbackMethod = true;
			pLocalIAsyncResult = pDeviceInstance->BeginVariableActionMethod(m_iBlockInstance, (int)0, &value, &methodAction, (IAsyncCallbackHandler *)this, NULL, sLanguageCode);
		}
		else
		{
			m_bWithCallbackMethod = false;
			pLocalIAsyncResult = pDeviceInstance->BeginVariableActionMethod(m_iBlockInstance, (int)0, &value, &methodAction, (IAsyncCallbackHandler *)nullptr, NULL, sLanguageCode);
		}

		//Immediately add the new pointer into pointer list for other function like AsyncCallback() use
		// Request ownership of the critical section.
		EnterCriticalSection(&cs_EDDMethCriticalSection);

		if (pLocalIAsyncResult != nullptr)
		{
			lClientAsyncResult.AddTail(pLocalIAsyncResult);

			*error = 0;	//DDS_SUCCESS;
			hr = S_OK;
		}

		// Release ownership of the critical section.
		LeaveCriticalSection(&cs_EDDMethCriticalSection);
	}
	catch (...)
	{
		hr = E_FAIL;
		BssProgLog(__FILE__, __LINE__, BssError, E_OUTOFMEMORY, L"BeginVarActionMethodInterp: failed to run BeginVariableActionMethod(): returned pointer = %i.", pLocalIAsyncResult);
	}

	ATLASSERT(hr == S_OK);

	return hr;
}


// This routine is responsible for calling EndMethod and deleting the pointer to IAsyncResult when "End Method" button is pressed.
// Argument error is zero if EndMethod() returns true; Argument error is non-zero if EndMethod() returns false.
STDMETHODIMP CEDDMethods::EndMethodInterp(INT* error)
{
	HRESULT hr = S_OK;
	BOOL isAsyncResultListEmpty = true;
	IAsyncResult *pLocalIAsyncResult = nullptr;

	OutputDebugStringW(L"\n'End Method' button is pressed\n");

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();
	if (!isAsyncResultListEmpty)
	{
		pLocalIAsyncResult = lClientAsyncResult.GetHead();
	}

	if ((pDeviceInstance != nullptr) && (pLocalIAsyncResult != nullptr) && !isAsyncResultListEmpty)
	{
		Mth_ErrorCode retVal = METH_ABORTED;

		if (m_bIsActionMethod)
		{
			retVal = pDeviceInstance->EndVariableActionMethod(&pLocalIAsyncResult, &returnedValue);
		}
		else
		{
			retVal = pDeviceInstance->EndMethod(&pLocalIAsyncResult, &returnedValue);
		}
		pEDDEngineLogger->CloseLogFile();
		hr = retVal;

		lClientAsyncResult.RemoveHead();
	}

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	*error = (INT)hr;
	return hr;
}


// This routine is responsible for calling CancelMethod and deleting the pointer to IAsyncResult
// when "Cancel Method" button is pressed.
STDMETHODIMP CEDDMethods::CancelMethodInterp(void)
{
	HRESULT hr = S_OK;
	BOOL isAsyncResultListEmpty = true;
	IAsyncResult *pLocalIAsyncResult = nullptr;

	OutputDebugStringW(L"\n'Cancel Method' button is pressed\n");

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();
	if (!isAsyncResultListEmpty)
	{
		pLocalIAsyncResult = lClientAsyncResult.GetHead();
	}

	if ((pDeviceInstance != nullptr) && (pLocalIAsyncResult != nullptr) && !isAsyncResultListEmpty)
	{
		pDeviceInstance->CancelMethod(pLocalIAsyncResult);

		if (!m_bWithCallbackMethod)
		{
			if (m_bIsActionMethod)
			{
				hr = pDeviceInstance->EndVariableActionMethod(&pLocalIAsyncResult, &returnedValue);
			}
			else
			{
				hr = pDeviceInstance->EndMethod(&pLocalIAsyncResult, &returnedValue);
			}
			pEDDEngineLogger->CloseLogFile();
			lClientAsyncResult.RemoveHead();
		}
	}
 
	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	return hr;
}


// This function is to check whether the oldest method execution is completed
STDMETHODIMP CEDDMethods::isMethodCompleted(BOOLEAN* result)
{
	IAsyncResult *pLocalIAsyncResult = nullptr;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	if (!lClientAsyncResult.IsEmpty())
	{
		pLocalIAsyncResult = lClientAsyncResult.GetHead();

		*result = pLocalIAsyncResult->get_IsCompleted();
	}
	else
	{
		*result = 1;	//yes
	}
	
	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	return S_OK;
}

//assuming the initial method state is NULL, see the function call to BeginMethod()
STDMETHODIMP CEDDMethods::validMethodState(BOOLEAN* result)
{
	*result = 0;	//MI state is the same as the initial value
	BOOL isAsyncResultListEmpty = true;
	IAsyncResult *pLocalIAsyncResult = nullptr;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();
	if (!isAsyncResultListEmpty)
	{
		pLocalIAsyncResult = lClientAsyncResult.GetHead();
	}

	if ((pDeviceInstance != nullptr) && (pLocalIAsyncResult != nullptr) && !isAsyncResultListEmpty)
	{
		if (pLocalIAsyncResult->get_AsyncState() != NULL)	//initial value is NULL
		{
			*result = 1;	//MI state is different from the initial value
		}
	}

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	return S_OK;
}

//This function waits for method to be completed if there is no callback
//If there is a callback, please use functin waitForTestToComplete() to verify the test is done.
STDMETHODIMP CEDDMethods::waitMethodCompleted()
{
	HRESULT hr = S_OK;
	BOOL isAsyncResultListEmpty = true;
	IAsyncResult *pLocalIAsyncResult = nullptr;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();
	if (!isAsyncResultListEmpty)
	{
		pLocalIAsyncResult = lClientAsyncResult.GetHead();
	}

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);
	
	if (pLocalIAsyncResult != nullptr)
	{
		HANDLE WaitEvent = (void *)(pLocalIAsyncResult->get_AsyncWaitHandle());
		DWORD dwRetVal = WaitForSingleObject(WaitEvent, INFINITE);

		if (dwRetVal != WAIT_OBJECT_0)
		{
			hr = E_FAIL;;	//method is never completed
		}
		//else method execution is completed if there is no callback.
	}
	//else method execution is completed

	return hr;
}

//Wait for list lClientAsyncResult to be empty
STDMETHODIMP CEDDMethods::waitForTestToComplete()
{
	HRESULT hr = E_FAIL;
	BOOL isAsyncResultListEmpty = true;

	while (hr != S_OK)
	{
		// Request ownership of the critical section.
		EnterCriticalSection(&cs_EDDMethCriticalSection);

		isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();

		// Release ownership of the critical section.
		LeaveCriticalSection(&cs_EDDMethCriticalSection);

		if (!isAsyncResultListEmpty)
		{
			Sleep(100);		//.1 second
		}
		else
		{
			hr = S_OK;
		}
	}

	return hr;
}

//UI COM functions	

//first argument: UI prompt text
//second argument: option list for selection
//3rd argument: UI window type -- 0 means no user input required; 1 means user input test reqired; 2 means user selection required
STDMETHODIMP CEDDMethods::GetUIPromptNOption(/*out*/ BSTR* wsText, /*out*/ VARIANT* vOption, /*out*/ unsigned long* winType)
{
	HRESULT hr = S_OK;
	BOOL isAsyncResultListEmpty = true;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	if ((pDeviceInstance != nullptr) && !isAsyncResultListEmpty)
	{
		m_pMIconsumer->EnterUIBuiltinSection();

		if (m_pMIconsumer->isUIBuiltinRunning())
		{
			//retrieve prompt string
			wchar_t *pPromt = (m_pMIconsumer->getUIConsumerPtr())->getUIMessage();
			*wsText = SysAllocString(pPromt);
			delete [] pPromt;

			//retrieve window type
			*winType = (m_pMIconsumer->getUIConsumerPtr())->getUIRequestType();
			if (*winType == nsConsumer::WINSELECT)
			{
				//retrieve selection option string
				(m_pMIconsumer->getUIConsumerPtr())->getUIOptionList(vOption);
			}
		}

		m_pMIconsumer->LeaveUIBuiltinSection();
	}

	return hr;
}


//first argument: user entered text
//second argument: selection value. 0 means the first selction in the option list, 1 means the second selection in the option list, ..., etc
//3rd argument: UI-user hit button: true == okay; false == cancel
STDMETHODIMP CEDDMethods::SetUIUserInputNButton(/*in*/ VARIANT* vUserText, /*in*/ unsigned long currentSel, /*in*/ BOOLEAN bUserButton)
{
	HRESULT hr = S_OK;
	BOOL isAsyncResultListEmpty = true;

	// Request ownership of the critical section.
	EnterCriticalSection(&cs_EDDMethCriticalSection);

	isAsyncResultListEmpty = lClientAsyncResult.IsEmpty();

	// Release ownership of the critical section.
	LeaveCriticalSection(&cs_EDDMethCriticalSection);

	if ((pDeviceInstance != nullptr) && !isAsyncResultListEmpty)
	{
		m_pMIconsumer->EnterUIBuiltinSection();

		if (m_pMIconsumer->isUICallbackWaiting())
		{
			nsConsumer::enumUIRequestType winType = (m_pMIconsumer->getUIConsumerPtr())->getUIRequestType();
			switch (winType)
			{
			case nsConsumer::WINEDIT:
				//User input is retrieved here
				if (vUserText->vt == VT_BSTR)
				{
					//It's edit input request. Receive user input
					size_t len = wcslen(vUserText->bstrVal);
					if (len > 0)
					{
						//receive user entered text
						(m_pMIconsumer->getUIConsumerPtr())->setUserEnteredText(vUserText);
					}
				}
				break;
			case nsConsumer::WINSELECT:
				if ((m_pMIconsumer->getUIConsumerPtr())->getUICurrentSel() != currentSel)
				{
					//It's bit enum input request or selection/UID request. Receive current selection from user interface
					//receive user entry
					(m_pMIconsumer->getUIConsumerPtr())->setUICurrentSel(currentSel);
				}
				break;
			}
			
			//call callback function and set button info. This function call also deletes UIConsumer instance
			m_pMIconsumer->uiBuiltinCallback(bUserButton);
		}

		m_pMIconsumer->LeaveUIBuiltinSection();
	}

	return hr;
}

int CEDDMethods::GetFFBlockInstance( wchar_t* pBlockName, int* piBlockInstance )
{
	ITEM_ID iBlockItemId = 0;	// device level, otherwise Convert the requested Block name to an ItemId
	int rc = 0;
	
	if (pBlockName[0] != _T('\0'))
	{
		rc = pDeviceInstance->GetItemIdFromSymbolName(pBlockName, &iBlockItemId);
	}

	if (rc == 0)
	{
		int iMyBlockIndex = -1;		// Index into the FieldbusBlockInfo which contains my block

		//
		// Set up the FF Block Tables like the Fieldbus Diagnostics boys did
		//
		FLAT_DEVICE_DIR	*flat_device_dir = nullptr;

		rc = pDeviceInstance->GetDeviceDir(&flat_device_dir);

		int iBlockCount = flat_device_dir->blk_tbl.count;

		// Create CFildbusBlockInfo array as an input to the AddFFBlocks() call
		nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo[iBlockCount];
		memset(pFieldbusBlockInfo, 0, sizeof(nsEDDEngine::CFieldbusBlockInfo) * iBlockCount);

		// Create piBlockInstances array for output from the AddFFBlocks() call
		int* piBlockInstances = new int[iBlockCount];
		memset(piBlockInstances, 0, sizeof(int) * iBlockCount);

		unsigned short object_index = 1000;						// For test purposes, start the first block at 1000
		wchar_t wsBlockName[BLOCK_TAG_SIZE*3]={0};
		size_t convertedChars = 0;

		for( int i = 0; i < iBlockCount; i++ )
		{
			ITEM_ID curBlockItemId = flat_device_dir->blk_tbl.list[i].blk_id;

			// Fill in the FieldbusBlockInfo array
			pFieldbusBlockInfo[i].m_BlockType = UNKNOWN_BLOCK;	// For test purposes, don't designate what kind of block
			
			//set block tag name and block ID
			int status = pDeviceInstance->GetSymbolNameFromItemId(curBlockItemId, wsBlockName, BLOCK_TAG_SIZE*3);
			if (status != 0)
			{
				break;
			}
			wcstombs_s(&convertedChars, pFieldbusBlockInfo[i].m_CharRecord.m_pTag, BLOCK_TAG_SIZE+1, wsBlockName, _TRUNCATE);
			pFieldbusBlockInfo[i].m_CharRecord.m_ulDDItemId = curBlockItemId;
			
			pFieldbusBlockInfo[i].m_usObjectIndex = object_index;
			object_index += 100;								// For test purposes, assume that blocks are at 100 boundries

			// If this is my block, save the index
			if (iBlockItemId == curBlockItemId)
			{
				iMyBlockIndex = i;
			}
		}

		// Make call to Add these FF Blocks to the EDD Engine instance
		rc = pDeviceInstance->AddFFBlocks( /*in*/ pFieldbusBlockInfo, /*out*/ piBlockInstances, /*in*/ iBlockCount );
	
		if ((rc == 0) && (iMyBlockIndex != -1))	// If everything alright, get my Block Instance number
		{
			*piBlockInstance = piBlockInstances[iMyBlockIndex];
		}
		else if (rc == 0)
		{
			*piBlockInstance = 0;
		}

		delete [] pFieldbusBlockInfo;
		delete [] piBlockInstances;
	}
	else
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"GetFFBlockInstance: after calling GetItemIdFromSymbolName(), the return code is %i", rc);
	}

	return rc;
}

STDMETHODIMP CEDDMethods::CreateDeviceInstanceNLoadDD(BSTR sDDName, SHORT protocol, BSTR sFFBlockName, VARIANT* vMethodList, INT* error)
{
	HRESULT hr = S_OK;
	m_protocol = protocol;
    wchar_t configXML[ CONFIG_XML_MAX ]; // arbitrary max string length

    if (!ReadConfigXML(configXML, CONFIG_XML_MAX -1))
    {
        ASSERT(0);
    }

	//create an instance of EDD Engine
	if (pDeviceInstance != nullptr)	//shall be re-used. do it in FDI consumer
	{
		delete pDeviceInstance;
	}

	//create instances of consumer
	if (m_pMIconsumer == nullptr)
	{
		m_pMIconsumer = new nsConsumer::Consumer();
	}
	else
	{
		return E_FAIL;	//stop continueing
	}

	pEDDEngineLogger = new CEDDEngineLogger();
	nsConsumer::IEDDEngineLogger* m_IEDDEngineLogger = (nsConsumer::IEDDEngineLogger*)pEDDEngineLogger;

	m_iBlockInstance = 0;

	*error = 0;

	nsEDDEngine::EDDEngineFactoryError eError = nsEDDEngine::EDDE_SUCCESS;
	pDeviceInstance = EDDEngineFactory::CreateDeviceInstance(sDDName, (nsConsumer::IParamCache *)m_pMIconsumer, (nsConsumer::IBuiltinSupport *)m_pMIconsumer, m_IEDDEngineLogger, configXML, &eError);
    //global device instance for calling EndMethod()
	pGlobalDeviceInstance = pDeviceInstance;
	//for Consumer instance use
	m_pMIconsumer->setEDDEngineInstance(pDeviceInstance);
	
    if ((eError != nsEDDEngine::EDDE_SUCCESS) || (pDeviceInstance == nullptr))
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"CreateDeviceInstanceNLoadDD: CreateDeviceInstance() returns error: %i", *error);
		hr = E_FAIL;
	}
	else
	{
		FLAT_DEVICE_DIR	*flat_device_dir;

		*error = pDeviceInstance->GetDeviceDir(&flat_device_dir);

		if( *error == 0)
		{
			if ((protocol == ProtocolType::FF) || (protocol == ProtocolType::ISA100))
			{
				// Get the Block Instance number for the FF Test Block
				*error = GetFFBlockInstance( sFFBlockName, &m_iBlockInstance );

				if (*error != 0)
				{
					BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"CreateDeviceInstanceNLoadDD: GetFFBlockInstance() returns error: %i", *error);
					return E_FAIL;
				}
			}

			ITEM_TBL		*item_table = &flat_device_dir->item_tbl;
			ITEM_TBL_ELEM	*item_element;
			ITEM_ID			item_id = 0;
			ITEM_TYPE		item_type = ITYPE_NO_VALUE;
			unsigned long	method_count = 0;
			BSTR			*pMethods = new BSTR[item_table->count];
			wchar_t			methIDName[1024];
				

			for( int nIndex = 0; nIndex < item_table->count ; nIndex++ )
			{
				item_element = &item_table->list[nIndex];
				item_id = item_element->item_id;
				item_type = item_element->item_type;

				if (item_type == ITYPE_METHOD)
				{
					wchar_t methodName[900]={0};
					int methodLen = 900;
					int status = pDeviceInstance->GetSymbolNameFromItemId(item_id, methodName, methodLen);
					if (status == 0)
					{
						wchar_t methodParams[900] = _T("");
						getMethodParams(item_id, 900, &methodParams[0]);

						if (wcslen(methodParams) > 0)
						{
							wsprintf(methIDName, L"%u, %s(%s)", item_id, methodName, methodParams);
						}
						else
						{
							wsprintf(methIDName, L"%u, %s", item_id, methodName);
						}

						pMethods[method_count] = SysAllocString(methIDName);
						method_count++;
					}
				}
			}			
			
			// Specify the method list bounds:
			// -- dim. count = 1
			// -- element count = 32 for each dimension
			// -- low bound = 0 for each dimension
			SAFEARRAYBOUND mySafeArrayBounds[1] = {method_count, 0};
 
			// Create mySafeArray
			SAFEARRAY *mySafeArray = NULL;
			mySafeArray = SafeArrayCreate(VT_BSTR /* var. type: BSTR */ ,
				1 /* dim. count */ , mySafeArrayBounds);

			if (mySafeArray != NULL)
			{
				//add UI selection enumerations into SAFEARRAY variable
				long lIndex;
				for (lIndex = 0; lIndex < (long)method_count; lIndex++)
				{
					SafeArrayPutElement(mySafeArray, &lIndex, (void*) pMethods[lIndex]);
					::SysFreeString(pMethods[lIndex]);
				}
				
				VariantClear(vMethodList);
				vMethodList->vt = VT_ARRAY|VT_BSTR;
				vMethodList->parray = mySafeArray;
			}			
			else
			{
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"CreateDeviceInstanceNLoadDD: SafeArrayCreate() returns NULL");
				hr = E_FAIL;
			}

			delete[] pMethods;
		}
		else
		{
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"CreateDeviceInstanceNLoadDD: GetDeviceDir() returns error: %i", *error);
			hr = E_FAIL;
		}
	}

	return hr;
}

STDMETHODIMP CEDDMethods::LoadActionList(BSTR sItemID, VARIANT* vMethodList, INT* error)
{
	//HRESULT hr = E_FAIL;	//initial failure
	
	*error = 1;				//initial failure
	
	//Convert string to numeric value as item ID
	wchar_t *stopstring = nullptr;
	unsigned long ulItemID = 0;
	if ( (sItemID[0] == L'0') && ((sItemID[1] == L'x') || (sItemID[1] == L'X')) )
	{
		ulItemID = wcstoul((const wchar_t *)sItemID, &stopstring, 16);
	}
	else
	{
		ulItemID = wcstoul((const wchar_t *)sItemID, &stopstring, 10);
	}

	if (ulItemID != 0)
	{
		//Conversion suceeds. Then get action list
		FDI_GENERIC_ITEM generic_item(ITYPE_VARIABLE);
		FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, ulItemID, 0};
		AttributeNameSet attribute_list = post_read_actions | post_write_actions | pre_read_actions | pre_write_actions | post_edit_actions | pre_edit_actions | refresh_actions;

		// call to IEDDEngine GetItem function to get generic item for given item id
		*error = pDeviceInstance->GetItem(m_iBlockInstance, 0, &item_spec, &attribute_list, sLanguageCode, &generic_item);
		if( *error == 0)
		{
			FLAT_VAR* flat_var = dynamic_cast<FLAT_VAR *>(generic_item.item);

			int iTotalCount = flat_var->post_edit_act.count + flat_var->pre_edit_act.count;
			iTotalCount += flat_var->post_write_act.count + flat_var->pre_write_act.count;
			iTotalCount += flat_var->post_read_act.count + flat_var->pre_read_act.count + flat_var->refresh_act.count;

			unsigned long	method_count = 0;
			BSTR			*pMethods = new BSTR[iTotalCount];
			
			//get REFRESH_ACTIONS list in FLAT_VAR order
			for (int nIndex = 0; nIndex < flat_var->refresh_act.count; nIndex++)
			{
				wchar_t methodName[1024] = { 0 };

				switch (flat_var->refresh_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
				{
					int methodLen = 900;
					wchar_t wcsName[900] = { 0 };
					*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->refresh_act.list[nIndex].action.meth_ref, wcsName, methodLen);
					wsprintf(methodName, L"PRERA, %u, %s", flat_var->refresh_act.list[nIndex].action.meth_ref, wcsName);
				}
				break;
				case ACTION::ACTION_TYPE_DEFINITION:
				{
					wsprintf(methodName, L"PRERA, %u, %s", 0, flat_var->refresh_act.list[nIndex].action.meth_definition.data);
				}
				break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
				{
					int methodLen = 900;
					wchar_t wcsName[900] = { 0 };
					*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->refresh_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
					wsprintf(methodName, L"PRERA, %u, %s", flat_var->refresh_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
				}
				break;
				default:
					*error = 1;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}

			//get PRE_READ_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->pre_read_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->pre_read_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_read_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"PRERA, %u, %s", flat_var->pre_read_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"PRERA, %u, %s", 0, flat_var->pre_read_act.list[nIndex].action.meth_definition.data);
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_read_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"PRERA, %u, %s", flat_var->pre_read_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				default:
					*error = 1;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			//get POST_READ_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->post_read_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->post_read_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_read_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"POSTRA, %u, %s", flat_var->post_read_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"POSTRA, %u, %s", 0, flat_var->post_read_act.list[nIndex].action.meth_definition.data);
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_read_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"POSTRA, %u, %s", flat_var->post_read_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				default:
					*error = 1;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			//get PRE_WRITE_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->pre_write_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->pre_write_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_write_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"PREWA, %u, %s", flat_var->pre_write_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"PREWA, %u, %s", 0, flat_var->pre_write_act.list[nIndex].action.meth_definition.data);	//copy 50 characters
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_write_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"PREWA, %u, %s", flat_var->pre_write_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			//get POST_WRITE_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->post_write_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->post_write_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_write_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"POSTWA, %u, %s", flat_var->post_write_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"POSTWA, %u, %s", 0, flat_var->post_write_act.list[nIndex].action.meth_definition.data);	//copy 50 characters
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_write_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"POSTWA, %u, %s", flat_var->post_write_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			//get PRE_EDIT_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->pre_edit_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->pre_edit_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_edit_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"PREEA, %u, %s", flat_var->pre_edit_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"PREEA, %u, %s", 0, flat_var->pre_edit_act.list[nIndex].action.meth_definition.data);	//copy 50 characters
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->pre_edit_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"PREEA, %u, %s", flat_var->pre_edit_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			//get POST_EDIT_ACTIONS list in FLAT_VAR order
			for( int nIndex = 0; nIndex < flat_var->post_edit_act.count; nIndex++ )
			{
				wchar_t methodName[1024]={0};

				switch(flat_var->post_edit_act.list[nIndex].eType)
				{
				case ACTION::ACTION_TYPE_REFERENCE:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_edit_act.list[nIndex].action.meth_ref, wcsName, methodLen);
						wsprintf(methodName, L"POSTEA, %u, %s", flat_var->post_edit_act.list[nIndex].action.meth_ref, wcsName);
					}
					break;
				case ACTION::ACTION_TYPE_DEFINITION:
					{
						wsprintf(methodName, L"POSTEA, %u, %s", 0, flat_var->post_edit_act.list[nIndex].action.meth_definition.data);
					}
					break;
				case ACTION::ACTION_TYPE_REF_WITH_ARGS:
					{
						int methodLen = 900;
						wchar_t wcsName[900]={0};
						*error = pDeviceInstance->GetSymbolNameFromItemId(flat_var->post_edit_act.list[nIndex].action.meth_ref_args.desc_id, wcsName, methodLen);
						wsprintf(methodName, L"POSTEA, %u, %s", flat_var->post_edit_act.list[nIndex].action.meth_ref_args.desc_id, wcsName);
					}
					break;
				default:
					*error = 1;
				}

				pMethods[method_count] = SysAllocString(methodName);
				method_count++;
			}			
			
			// Specify the method list bounds:
			// -- dim. count = 1
			// -- element count = 32 for each dimension
			// -- low bound = 0 for each dimension
			SAFEARRAYBOUND mySafeArrayBounds[1] = {method_count, 0};
 
			// Create mySafeArray
			SAFEARRAY *mySafeArray = NULL;
			mySafeArray = SafeArrayCreate(VT_BSTR /* var. type: BSTR */ ,
				1 /* dim. count */ , mySafeArrayBounds);

			if (mySafeArray != NULL)
			{
				//add UI selection enumerations into SAFEARRAY variable
				long lIndex;
				for (lIndex = 0; lIndex < (long)method_count; lIndex++)
				{
					SafeArrayPutElement(mySafeArray, &lIndex, (void*) pMethods[lIndex]);
					::SysFreeString(pMethods[lIndex]);
				}
				
				VariantClear(vMethodList);
				vMethodList->vt = VT_ARRAY|VT_BSTR;
				vMethodList->parray = mySafeArray;
			}			
			else
			{
				BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"LoadActionList: SafeArrayCreate() returns NULL");
				*error = E_FAIL;
			}

			delete[] pMethods;
		}
		else
		{
			BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"LoadActionList: GetItem() returns error: %i", *error);
			*error = E_FAIL;
		}
	}
	else
	{
		BssProgLog(__FILE__, __LINE__, BssInformation, BSS_SUCCESS, L"LoadActionList: no method ID found in error: %i", *error);
		*error = E_FAIL;
	}

	return *error;
}


//*****************************************************************************************//
//
//   Name: ReadConfigXML
//	 
//	 Description: 
//		This function creates the configuration XML string for passing
//      on to the edd engine.  It gets it from the EddEngineConfig.xml
//      file in c:\ProgramData\FDI  or C:\Documents and Settings\All Users\Application Data\FDI
//      depending on the OS.
//		
//	 Inputs:
//		ConfigXML - The configuration as an xml string.
//                - empty string on failure
//      ConfigXMLLen - max length of the configuration string.
//
//
//	 Returns:
//      true if it worked or false on an error opening the file.
//      This function in no way warrants that the XML in the file
//      is properly formed.  That is handled in the EDD Engine Code.
//
//   Date:
//      3 July, 2013
//	 Author:
//		Mark Sandmann
//*****************************************************************************************//
 
int CEDDMethods::ReadConfigXML(wchar_t* configXML, int configXMLLen)
{
    int returnValue = false;    // default to failed

    // String buffer for holding the path.
    static TCHAR strPath[ MAX_PATH ];

    int configXMLIndex = 0;
    configXML[0] = 0;   // terminate configuration

    // Get the special folder path from the OS Environment Variable
    // Windows 7 = c:\Program Data
    // Windows XP = C:\Documents and Settings\user\Application Data
    SHGetSpecialFolderPath( 0,       // Hwnd
                            strPath, // String buffer.
                            CSIDL_COMMON_APPDATA, // CSLID of folder
                            FALSE ); // Create if doesn't exists?

    // Add in the \FDI\EddEngineConfig.xml part of the path.
    wcscat_s(strPath, MAX_PATH, L"\\FDI\\EddEngineConfig.xml");

    char tempChar;
    std::ifstream myfile (strPath);
    if (myfile.is_open())
    {
        returnValue = true;
        while ( myfile.good() && configXMLIndex < configXMLLen)
        {
            // Get the next character
            tempChar = (char)myfile.get();
            // ignore CR/LF/EOF characters
            if(tempChar != '\n' && tempChar != '\r' && tempChar != '\377')
                configXML[configXMLIndex++] = tempChar;
        }
    }

    // terminate config string.
    configXML[configXMLIndex++] = 0;
 
    return returnValue;
}

void CEDDMethods::getMethodParams(/* in */ unsigned long ulMethID, /* in */ size_t uiLen, /* out */ wchar_t *pMethodParams)
{
	if (pMethodParams == NULL)
	{
		return;
	}

	//check if this is a method with parameter or a method without parameter.
	FDI_GENERIC_ITEM generic_item(ITYPE_METHOD);
	FDI_ITEM_SPECIFIER item_spec = {FDI_ITEM_ID, ulMethID, 0};
	AttributeNameSet attribute_list = method_parameters;

	// call to IEDDEngine GetItem function to get generic item for given item id
	int hr = pDeviceInstance->GetItem(m_iBlockInstance, 0, &item_spec, &attribute_list, sLanguageCode, &generic_item);
	if (hr == S_OK) 
	{
		FLAT_METHOD *flat_method = dynamic_cast< FLAT_METHOD *>(generic_item.item);
		if( flat_method )
		{
			if (flat_method->params.length() > 0)
			{
				//this method has input argument
				wchar_t seps[] = L";";
				wchar_t* sToken = (wchar_t*)flat_method->params.c_str();
				wchar_t* sRemainingContext = NULL;
				wchar_t* sParamType = NULL;
				wchar_t* sParamFlag = NULL;
				wchar_t* sParamName = NULL;
				wchar_t wcSpace[2] = L"";

				pMethodParams[0] = L'\0';
				do
				{
					sParamType = wcstok_s(sToken, seps, &sRemainingContext);
					sToken = sRemainingContext;
					sParamFlag = wcstok_s(sToken, seps, &sRemainingContext);
					sToken = sRemainingContext;
					sParamName = wcstok_s(sToken, seps, &sRemainingContext);
					sToken = sRemainingContext;

					//find the parameter type name
					wcscat_s(pMethodParams, uiLen, wcSpace);
					MethodType iParamType = (MethodType)_wtoi( sParamType );
					switch(iParamType)
					{
					case MT_INT_8:
						wcscat_s(pMethodParams, uiLen, L"char ");
						break;
					case MT_INT_16:
						wcscat_s(pMethodParams, uiLen, L"short ");
						break;
					case MT_INT_32:
						wcscat_s(pMethodParams, uiLen, L"int ");
						break;
					case MT_FLOAT:
						wcscat_s(pMethodParams, uiLen, L"float ");
						break;
					case MT_DOUBLE:
						wcscat_s(pMethodParams, uiLen, L"double ");
						break;
					case MT_UINT_8:
						wcscat_s(pMethodParams, uiLen, L"unsigned char ");
						break;
					case MT_UINT_16:
						wcscat_s(pMethodParams, uiLen, L"unsigned short ");
						break;
					case MT_UINT_32:
						wcscat_s(pMethodParams, uiLen, L"unsigned int ");
						break;
					case MT_INT_64:
						wcscat_s(pMethodParams, uiLen, L"long long ");
						break;
					case MT_UINT_64:
						wcscat_s(pMethodParams, uiLen, L"unsigned long long ");
						break;
					case MT_DDSTRING:
						wcscat_s(pMethodParams, uiLen, L"DD_STRING ");
						break;
					case MT_DD_ITEM:
						wcscat_s(pMethodParams, uiLen, L"ref ");
						break;
					}
					wcscat_s(pMethodParams, uiLen, sParamName);
					wcSpace[0] = L',';
					wcSpace[1] = L'\0';
				} while (sRemainingContext[0] != _T('\0'));
			}
		}
	}

}
