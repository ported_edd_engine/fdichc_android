

#include "stdafx.h"
#include "ddbAxis.h"
#include "logging.h"
#include <CMethodInterpreterDevice.h>
#include <Inf/NtcSpecies/BssProgLog.h>
#include <ServerProjects/CommonToolkitStrings.h>
#include "varientTag.h"
#include "float.h"
#include<limits.h>


hCaxis::hCaxis(DevInfcHandle_t h, aCitemBase* paItemBase)
	: hCgrpItmBasedClass(h, paItemBase)
{
}


RETURNCODE hCaxis::Help(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadValueArrayData( itemId, _T("Help"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}


RETURNCODE hCaxis::Label(wstring& retStr)
{ 
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadValueArrayData( itemId, _T("Label"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_WSTR )
			{
				retStr = CV_WSTR(&vtType);
			}
		}
	}
	return SUCCESS;
}


hCattrBase*   hCaxis::newHCattr(aCattrBase* pACattr)
{
	return NULL;
}

//
//
//
RETURNCODE hCaxis::getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress )
{
	RETURNCODE rc = FAILURE;
	hCgroupItemDescriptor* pGid = NULL;
	CStdString sName = indexName.c_str();

	if ( ppGID == NULL )
	{	return rc;	   
	}
	else
	{	
		pGid   = new hCgroupItemDescriptor( devHndl() );
		if ( *ppGID != NULL )
		{
			delete *ppGID;
		}
		*ppGID = pGid;
		rc = FAILURE; // set it to delete pGid if no attribute found
	}

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtItemID{};
		CStdString sMoniker;
		CStdString sIndexName = indexName.c_str();
		sMoniker.Format(_T("Member!%s!ItemID"), sIndexName );
		if( pDevice->ReadValueArrayData( itemId, sMoniker, &vtItemID ) ) 
		{
			if( vtItemID.vTagType == ValueTagType::CVT_I4 )
			{
				hCreference locRef( devHndl() );
				locRef.setRef( vtItemID.vValue.iIntConst, 0, iT_Variable, false, rT_Item_id) ;
				(*pGid) = locRef;
				
				CValueVarient vtMemberID{};
				sMoniker.Format(_T("Member!%s!MemberId"), sIndexName );
				if( pDevice->ReadValueArrayData( itemId, sMoniker, &vtMemberID ) ) 
				pGid->setIndex(vtMemberID.vValue.iIntConst);
				rc = SUCCESS;
			}
		}
	}
	if (rc != SUCCESS)
	{
		if(pGid)	//Vibhor: 200904
		{
			RAZE(pGid);		
		}
		*ppGID = NULL;
	}
	return rc;
}

CValueVarient hCaxis::getAttrValue(unsigned attrType, int which)
{
	CValueVarient rV{};

	axisAttrType_t xat = (axisAttrType_t) attrType;

	switch (xat)
	{
	case axisAttrMinVal:
		{
		}
		break;
	case axisAttrMaxVal:
		{
		}
		break;
	default:
		{
			LOGIT(CERR_LOG,"ERROR: program error %s line %d.\n",__FILE__,__LINE__);
		}
		break;
	}// endswitch
	return rV;
}

//
//
// copied from ddbVar.cpp
bool hCaxis::ReadParameterData(CValueVarient *pOutput, wchar_t *sValue )
{
	bool bRetVal = false;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		int iRetVal = pDevice->ReadParameterData2(pOutput, sValue, 0, 0, this->itemId, 0 );

		if (iRetVal == BLTIN_SUCCESS)
		{
			bRetVal = true;
		}
	}

	return bRetVal;
}

//
//
// copied from ddbVar.cpp
RETURNCODE hCaxis::getMinMaxList(hCRangeList& retList)
{
	RETURNCODE rc = SUCCESS;
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	CValueVarient vtMaxValue{};
	CValueVarient vtMinValue{};

	retList.clear();
	CValueVarient vtType{};
	CValueVarient vtSize{};
	ReadParameterData( &vtSize, DCI_PROPERTY_SIZE );
	ReadParameterData( &vtType, DCI_PROPERTY_TYPE );

	CValueVarient vtMinCount{};
	ReadParameterData( &vtMinCount, DCI_PROPERTY_MIN_COUNT );

	hCRangeItem tItm;

	if( (int)vtMinCount > 0 )
	{
		int iRetVal_min = pDevice->ReadParameterData2( &vtMinValue, DCI_PROPERTY_MIN_VALUE, 0, 0, this->itemId, 0 );
		if (iRetVal_min == BLTIN_SUCCESS)
		{
				//min value
				tItm.minVal = vtMinValue;
		}
		int iRetVal_max = pDevice->ReadParameterData2( &vtMaxValue, DCI_PROPERTY_MAX_VALUE, 0, 0, this->itemId, 0 );
		if (iRetVal_max == BLTIN_SUCCESS)
		{
			//max value
			tItm.maxVal = vtMaxValue;			
		}

		retList[0] = tItm;
	}
	else
	{
		switch( (int)vtType )
		{
		case vT_FloatgPt:
			vtMinValue = -FLT_MAX;
			vtMaxValue = FLT_MAX;
			break;

		case vT_Double:
			vtMinValue = -DBL_MAX;
			vtMaxValue = DBL_MAX;
			break;

		case vT_Unsigned:
		case vT_Enumerated:
		case vT_BitEnumerated:
		case vT_Index:
		case vT_Time:
		case vT_DateAndTime:
		case vT_Duration:
			vtMinValue = 0;
			switch ((int)vtSize)
			{
			case 1:
				vtMaxValue = UINT8_MAX;
				break;
			case 2:
				vtMaxValue = UINT16_MAX;
				break;
			case 3:
			case 4:
				vtMaxValue = UINT32_MAX;
				break;
			default:
                vtMaxValue = ULLONG_MAX;
				break;
			}
			break;

		case vT_Boolean:
			vtMinValue = FALSE;
			vtMaxValue = FALSE;
			break;

		case vT_Integer:
		case vT_HartDate:
			switch ((int)vtSize)
			{
			case 1:
				vtMinValue = INT8_MIN;
				vtMaxValue = INT8_MAX;
				break;
			case 2:
				vtMinValue = INT16_MIN;
				vtMaxValue = INT16_MAX;
				break;
			case 3:
			case 4:
				vtMinValue = INT32_MIN;
				vtMaxValue = INT32_MAX;
				break;
			default:
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
				break;
			}
			break;

		case vT_TimeValue:
			if( (int)vtSize == 4)
			{
				vtMinValue = 0;
				vtMaxValue = UINT32_MAX;
			}
			else
			{
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
			}

		default:
			if( (int)vtSize > 4 )
			{
                vtMinValue = LLONG_MIN;
                vtMaxValue = LLONG_MAX;
			}
			else
			{
                //TODO:LINUX:FIXIT
                vtMinValue = INT32_MIN; //LONG_MIN
                vtMaxValue = INT32_MAX; //LONG_MAX
			}
			break;
		}

		//min value
		tItm.minVal = vtMinValue;

		//max value
		tItm.maxVal = vtMaxValue;

		retList[0] = tItm;
	}

	return rc;
}
