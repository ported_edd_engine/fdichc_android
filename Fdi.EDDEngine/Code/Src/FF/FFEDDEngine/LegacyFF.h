#pragma once

#include "EDDEngineImpl.h"
#include "DDSSupport.h"
#include <map>


class CLegacyFF : public CEDDEngineImpl, public IDDSSupport
{
private:
	DEVICE_HANDLE m_device_handle;
	DDOD_HEADER m_header;
	nsEDDEngine::FLAT_DEVICE_DIR *m_Flat_Device_Dir;
	std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>  m_mapBlockInst;
	CConnectionMgr m_connection_mgr;

public:
	CLegacyFF(const wchar_t *sEDDBinaryFilename,
		const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode);
	~CLegacyFF();

	DEVICE_HANDLE GetDeviceHandle() const { return m_device_handle; };
	// Interface conversion functions

    //
    // Unit Convenience functions
    //
    // Gets the Unit Relation ItemId for the specified parameter
    int GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID * pUnitItemId);
    int GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID *pUnitItemId);

    // Command Convenience functions

    // Retrieves the List of Commands that can be used to read/write this parameter
    int GetCommandList(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, nsEDDEngine::CommandType eCmdType, nsEDDEngine::FDI_COMMAND_LIST * pCommandList);

	// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
    int GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID * pCmdItemId);

    //
    // String Convenience functions
    //
    // Converts an EDD Engine Error code to a string
	wchar_t *GetErrorString(int iErrorNum);
	int GetStringTranslation(const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size); 

    //
    // Type Convenience functions
    //
    int GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType);
    int GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE *pItemType, ITEM_ID *pItemId);
    int GetParamType(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, nsEDDEngine::TYPE_SIZE *pTypeSize);
	
    int GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size);
	int GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* IptemId);
	int GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec,nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, nsEDDEngine::FDI_GENERIC_ITEM * pGenericItem );

	int GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir);
	void GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader);

	// Input data structure initialization
	void Init_DDI_BLOCK_SPECIFIER(DDI_BLOCK_SPECIFIER *pBlockSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInstance);

	// Input data structure conversions
	void Init_DDI_ITEM_SPECIFIER(DDI_ITEM_SPECIFIER *pItemSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pFdiItemSpec);
	int Init_DDI_PARAM_SPECIFIER(DDI_PARAM_SPECIFIER *pDdiParamSpec, const nsEDDEngine::FDI_PARAM_SPECIFIER *pFdiParamSpec);
	void Init_DDI_GENERIC_ITEM(DDI_GENERIC_ITEM *pDdiGenericItem, nsEDDEngine::FDI_GENERIC_ITEM * pFdiGenericItem);

	
	// Output data structure conversions
	void Fill_EDD_TYPE_SIZE (nsEDDEngine::TYPE_SIZE *pTypeSize, TYPE_SIZE type_size);
	void Fill_FDI_GENERIC_ITEM(int iBlockInstance, nsEDDEngine::FDI_GENERIC_ITEM *pFdiGenericItem, DDI_GENERIC_ITEM pDdiGenericItem);
	void Fill_FLAT_DEVICE_DIR(nsEDDEngine::FLAT_DEVICE_DIR *pFdiFlatDeviceDir, FLAT_DEVICE_DIR *pDdiFlatDeviceDir);
	void Fill_BLOCK_TABLE(nsEDDEngine::BLK_TBL *pFdiBlockTbl, BLK_TBL * pDdiBlockTbl);
	void Fill_FLAT_BLOCK_DIR(nsEDDEngine::FLAT_BLOCK_DIR *pFdiBlockDir, FLAT_BLOCK_DIR * pDdiBlockDir);
	void Fill_OP_REF_TRAIL(nsEDDEngine::OP_REF_TRAIL* pFdiOpRefTrail, OP_REF_TRAIL* pDdiOpRefTrail);
	void Fill_DATA_ITEM_LIST(nsEDDEngine::DATA_ITEM_LIST* pFdiDataList, DATA_ITEM_LIST* pDdiDataList);
	void Fill_OP_REF_TRAIL_LIST(nsEDDEngine::OP_REF_TRAIL_LIST* pFdiOpRefTrailList, OP_REF_TRAIL_LIST* pDdiOpRefTrailList);
	void Fill_VECTOR_LIST( nsEDDEngine::VECTOR_LIST *pFdiVectorList, VECTOR_LIST *pDdiVectorList );
	static void Fill_STRING(nsEDDEngine::STRING *pFdiString, const STRING *pDdiString);
	void Fill_RESPONSE_CODES(nsEDDEngine::RESPONSE_CODE_LIST* pFdiRespCodeList, RESPONSE_CODE_LIST* pDdiRespCodeList);
	static void Fill_EXPR(nsEDDEngine::EXPR* pFdiExpr, EXPR* pDdiExpr);
	static void Fill_LIST_DEPBIN(nsEDDEngine::LIST_DEPBIN* pFdiListDepBin, LIST_DEPBIN* pDdiListDepBin);
	static void Fill_RANGE_DATA_LIST(nsEDDEngine::RANGE_DATA_LIST* pFdiRangeDataList, RANGE_DATA_LIST* pDdiRangeDataList);
	void Fill_ENUM_VALUE_LIST(nsEDDEngine::ENUM_VALUE_LIST* pFdiEnumValuList, ENUM_VALUE_LIST* pDdiEnumValuList);
	static void Fill_ACTION_LIST(nsEDDEngine::ACTION_LIST* pFdiActionList, ITEM_ID_LIST* pDdiItemIdList);
	static void Fill_FLAT_MASK(nsEDDEngine::FLAT_MASKS* pFdiFlatMask, FLAT_MASKS* pDdiFlatMask, nsEDDEngine::ITEM_TYPE itemType);
	void Fill_ENUM_STATUS_CLASS(nsEDDEngine::STATUS_CLASS_LIST* pFdiBitEnumStatus, BIT_ENUM_STATUS* pDdiBitEnumStatus);
	void Fill_MEMBER_LIST(nsEDDEngine::MEMBER_LIST* pFdiMemberList, MEMBER_LIST* pDdiMemberList);
	void Fill_OP_MEMBER_LIST(nsEDDEngine::OP_MEMBER_LIST* pFdiOpMemberList, OP_MEMBER_LIST* pDdiOpMemberList);
	static void Fill_ITEM_ID_LIST( nsEDDEngine::ITEM_ID_LIST *pFdiItemIdList, ITEM_ID_LIST *pDdiItemIdList);
	void Fill_OP_REF(nsEDDEngine::OP_REF* pFdiOpRef, OP_REF* pDdiOpRef);
	void Fill_DEFAULT_VALUES_LIST(nsEDDEngine::DEFAULT_VALUES_LIST *pFdiDefaultValueList, DEFAULT_VALUE_LIST *pDdiDefaultValueList);
	static void Fill_BINARY(nsEDDEngine::BINARY *pFdiBinary, const BINARY *pDdiBinary);
	// Clean Function
	void Clean_FLAT_DEVICE_DIR(nsEDDEngine::FLAT_DEVICE_DIR *pFlatDeviceDir);
	void Clean_FLAT_BLOCK_DIR(nsEDDEngine::FLAT_BLOCK_DIR *pFlatBlockDir);
	
	// Helper function
	nsEDDEngine::ITEM_TYPE Set_ITEM_TYPE(ITEM_TYPE itemType);
	ITEM_TYPE Set_DDI_ITEM_TYPE(nsEDDEngine::ITEM_TYPE itemType);
    unsigned int Set_DDI_COMMAND_TYPE(nsEDDEngine::CommandType commandType);
	static nsEDDEngine::ClassType SetClassType(ulong class_attr);
	static nsEDDEngine::Handling SetHandling(unsigned long hadling);
	static nsEDDEngine::Boolean SetBoolean(unsigned long bVal);
	static void Fill_Bitmask( unsigned long& bitmask, const nsEDDEngine::AttributeNameSet& ans, nsEDDEngine::ITEM_TYPE itype );
	static void Fill_AttributeNameSet( nsEDDEngine::AttributeNameSet& ans, unsigned long bitmask, nsEDDEngine::ITEM_TYPE itype );
    
    int GetCriticalParams(int /*iBlockInstance*/, nsEDDEngine::CRIT_PARAM_TBL** /*pCriticalParamTbl*/){return 0;};
	int GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, ITEM_ID* pWAO_Item, ITEM_ID* pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList = NULL);

	nsEDDEngine::DisplaySize SetDisplaySize(unsigned long dispsize);
	nsEDDEngine::LineType SetLineType(unsigned long linetype);

	int DDS_GetParamValue( BLOCK_HANDLE bh, void* pValueSpec, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value);
	int GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle );
	void DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory);

	void Fill_DDI_EVAL_VAR_VALUE(::EVAL_VAR_VALUE *pDdiEvalVarValue, nsConsumer::EVAL_VAR_VALUE *pFdiEvalVarValue );
	static void Fill_DDS_STRING(STRING *pDdiString, nsEDDEngine::STRING *pFdiString);
//	DictionaryTable* GetDictExtTable( );
 
	int GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
	int GetDictionaryString( wchar_t* /*wsDictName*/,  wchar_t* /*pString*/, int /*iStringLen*/, const wchar_t* /*lang_code*/ ) {return -1;};
	int GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
	int ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex);



	int GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence);
	int GetBlockInstanceByTag(ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence);
	int GetBlockInstanceCount( ITEM_ID iItemId, int* piCount);
	int ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance);

	int AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount );
	BLOCK_HANDLE BlockInstToBlockHandle(nsEDDEngine::BLOCK_INSTANCE iBlockInstance) const;
	int BlockHandleToBlockInst(BLOCK_HANDLE bh, nsEDDEngine::BLOCK_INSTANCE *block_instance);
	static int CompareObjectIndex(nsEDDEngine::CCrossblockInfo* pElement1 ,nsEDDEngine::CCrossblockInfo* pElement2 );

	CConnectionMgr* GetConnectionManager();

#ifdef _DEBUG
	int DisplayDebugInfo(){return 0;};
#endif
};
