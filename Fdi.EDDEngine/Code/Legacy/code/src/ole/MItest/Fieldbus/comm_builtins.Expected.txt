<Method>
{



 long lV1, lV2, lV3, lV4, lV5, lV6;

 long status;

 unsigned long id, member, code, erro_code, response_code; 

 id = 1000;

 member = 1001;

 erro_code = 1800;

 response_code = 100;

 status = send_value(id, member);



 lV1 = abort_on_comm_error(erro_code);

 lV2 = retry_on_comm_error(erro_code);

 lV3 = fail_on_comm_error(erro_code);



 lV4 = abort_on_response_code(response_code);

 lV5 = retry_on_response_code(response_code);

 lV6 = fail_on_response_code(response_code);



 abort_on_all_comm_errors();

 abort_on_all_response_codes();

 

 retry_on_all_comm_errors();

 retry_on_all_response_codes();



 fail_on_all_comm_errors();

 fail_on_all_response_codes();

}
</Method>
<Function>
Function Name: send_value
  Number Of Input Parameters: 2
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 1000
    Param 2 Type: RUL_UNSIGNED_INT
    Param 2 Value: 1001
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function send_value
Function Name: abort_on_comm_error
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 1800
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function abort_on_comm_error
Function Name: retry_on_comm_error
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 1800
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function retry_on_comm_error
Function Name: fail_on_comm_error
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 1800
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function fail_on_comm_error
Function Name: abort_on_response_code
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 100
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function abort_on_response_code
Function Name: retry_on_response_code
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 100
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function retry_on_response_code
Function Name: fail_on_response_code
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_INT
    Param 1 Value: 100
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function fail_on_response_code
Function Name: abort_on_all_comm_errors
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function abort_on_all_comm_errors
Function Name: abort_on_all_response_codes
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function abort_on_all_response_codes
Function Name: retry_on_all_comm_errors
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function retry_on_all_comm_errors
Function Name: retry_on_all_response_codes
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function retry_on_all_response_codes
Function Name: fail_on_all_comm_errors
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function fail_on_all_comm_errors
Function Name: fail_on_all_response_codes
  Number Of Input Parameters: 0
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function fail_on_all_response_codes
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><lV1>0,</lV1><lV2>0,</lV2><lV3>0,</lV3><lV4>0,</lV4><lV5>0,</lV5><lV6>0,</lV6><status>0,</status><id>1000,</id><member>1001,</member><code>0,</code><erro_code>1800,</erro_code><response_code>100,</response_code></SymbolTable>
