


MANUFACTURER 0x0000FD, DEVICE_TYPE 0x0004, DEVICE_REVISION 1, DD_REVISION 1

BLOCK test_block
{
    CHARACTERISTICS test_block_characteristics ;
    LABEL "Test Block" ;
    HELP "Help Test Block" ;
    
    PARAMETERS
    {
        BLOCK_TEST_UNIT,          test_unit;
        BLOCK_TEST_CONDITIONED_1, test_var_contitioned_1;
        BLOCK_TEST_CONDITIONED_2, test_var_contitioned_2;
        TEST_INPUT_BIT_ENUM,      test_input_bit_enum;
        COND0027B_ENUM_VAR,       cond0027b_enum_var;
        TEST_VAR_1, test_var_1;
    }
    
    LOCAL_PARAMETERS
    {
        BLOCK_TEST_VAR_2, test_var_2;
        BLOCK_TEST_VAR_3, test_var_3;
    }
    
    UNIT_ITEMS
    {
        test_unit_relation
    }
}

RECORD test_block_characteristics
{
    LABEL           "Label test block character" ;
    HELP             "help test block character";
    MEMBERS
    {
        TEST_VAR_1_CHARACTERISTICS, test_var_1;
    }
}

VARIABLE test_var_1
{
    LABEL "Test Variable 1";
    TYPE INTEGER (4);
    CLASS CONTAINED;
}

VARIABLE test_var_2
{
    LABEL "Test Variable 1";
    TYPE INTEGER (4);
    CLASS LOCAL;
}

VARIABLE test_var_3
{
    LABEL "Test Variable 1";
    TYPE INTEGER (4);
    CLASS LOCAL;
}

VARIABLE test_var_contitioned_1
{
    LABEL "COnditioned LABEL not supported?"; /* IF (test_var_1 == 23) { "Condition is true"; } ELSE { "Condition is false"; } */
    TYPE INTEGER (4);
    CLASS CONTAINED;
}

VARIABLE test_var_contitioned_2
{
    LABEL "Test variable with conditioned validity";
    TYPE INTEGER (4);
    CLASS CONTAINED;
    VALIDITY IF (test_var_2 == 23)
    { 
        TRUE;
    }
    ELSE
    {
        FALSE;
    }
}

VARIABLE test_unit
{
    LABEL "Test";
    TYPE ENUMERATED{
        {1, "m"},
        {2, "kpa"}
    }
    HANDLING READ & WRITE;
    CLASS CONTAINED;
}

UNIT test_unit_relation
{
    test_unit: test_var_1
}

VARIABLE test_var_bit_enum
{
    LABEL "BIT_ENUMERATED var";
    CLASS LOCAL;
    TYPE BIT_ENUMERATED (1)
    {
        {0x01, "Enable option 1"},
        {0x02, "Enable option 2"},
        {0x04, "Enable option 3"},
        {0x08, "Enable option 4"},
        {0x10, "Enable option 5"},
        {0x20, "Enable option 6"},
        {0x40, "Enable option 7"},
        {0x80, "Enable option 8"}
    }
}

/* ********************************************
from FF HTK conditional test cases
*/
#define _CONDTB_ENUM_HELP   "These enumerations represent conditional "     \
                            "evaluation values used to test the attributes "\
                            "of variables in this block."
#define _CONDTB_TEST_INPUT_BIT_ENUM           \
        {0x00000001,    "DISPLAY_FORMAT_1"  },\
        {0x00000002,    "DISPLAY_FORMAT_2"  },\
        {0x00000004,    "EDIT_FORMAT_1"     },\
        {0x00000008,    "EDIT_FORMAT_2"     },\
        {0x00000010,    "MIN_VALUE_1"       },\
        {0x00000020,    "MIN_VALUE_2"       },\
        {0x00000040,    "MAX_VALUE_1"       },\
        {0x00000080,    "MAX_VALUE_2"       },\
        {0x00000100,    "ENUMERATED_1"      },\
        {0x00000200,    "ENUMERATED_2"      },\
        {0x00000400,    "BIT_ENUMERATED_1"  },\
        {0x00000800,    "BIT_ENUMERATED_2"  },\
        {0x00001000,    "CONSTANT_UNIT_1"   },\
        {0x00002000,    "CONSTANT_UNIT_2"   },\
        {0x00004000,    "HANDLING_1"        },\
        {0x00008000,    "HANDLING_2"        },\
        {0x00010000,    "VALIDITY_1"        },\
        {0x00020000,    "VALIDITY_2"        }
VARIABLE    test_input_bit_enum
{
    LABEL       "Test Input Bit Enumeration";
    HELP        _CONDTB_ENUM_HELP;
    CLASS       CONTAINED;
    TYPE        BIT_ENUMERATED (4)
    {
        _CONDTB_TEST_INPUT_BIT_ENUM
    }
    HANDLING    READ & WRITE;
}

/* ********************************************
from FF HTK conditional test cases
*/
#define _ENUMERATED_1_BIT        0x00000100
#define _ENUMERATED_2_BIT        0x00000200
#define _TEST_ENUM1         {1, "YES"},\
                            {2, "NO"}

#define _TEST_ENUM2         {1, "YES"}, \
                            {2, "NO"},  \
                            {3, "MAYBE"}
VARIABLE        cond0027b_enum_var
{
    LABEL       "COND0027B Enum Var";
    HELP        "Verify that host properly dereferences enumerated "
                "bits when evaluating conditionals.";
    CLASS       CONTAINED;
    TYPE        ENUMERATED (2)
    {
        IF (test_input_bit_enum[_ENUMERATED_2_BIT])            
        {   _TEST_ENUM2     }
        ELSE                                
        {   _TEST_ENUM1     }
    }
}

