
#include "stdafx.h"
#include "SynchronisationSet.h"



// Template declaration required for linkage
FOLLOWS	FOLLOW_SET::follows[PRODUCTION_COUNT];

FOLLOW_ELEMENT::FOLLOW_ELEMENT(	RUL_TOKEN_TYPE	rhsType,
							   RUL_TOKEN_SUBTYPE	rhsSubType)
{
	Type = rhsType;
	SubType = rhsSubType;
}

FOLLOW_SET::FOLLOW_SET()
{
	//one time initiaiization among multiple threads
	follows[EXPRESSION].production = EXPRESSION;
	follows[EXPRESSION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_SEMICOLON));
	follows[EXPRESSION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_RPAREN));
	follows[EXPRESSION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_LBRACK));
	follows[EXPRESSION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_RBRACK));

	follows[DECLARATION].production = DECLARATION;
	follows[DECLARATION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_COMMA));
	follows[DECLARATION].set.push_back(FOLLOW_ELEMENT(RUL_SYMBOL,RUL_SEMICOLON));
}

bool FOLLOW_SET::IsPresent(
		PRODUCTION production,
		RUL_TOKEN_TYPE Type,
		RUL_TOKEN_SUBTYPE SubType)
{
	static FOLLOW_SET g_follow_set;			//initialize once only

	for(_INT32 i=0; i<PRODUCTION_COUNT;i++)
	{
		if(follows[i].production == production)
		{
			size_t i32SetSize = follows[i].set.size();
			for(size_t j=0;	j<i32SetSize;	j++)
			{
				if((follows[i].set[j].Type == Type)
					&& (follows[i].set[j].SubType == SubType))
				return true;
			}
		}
	}
	return false;
}
