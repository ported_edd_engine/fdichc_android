// Documentation Notes
//  "Used in" progressions start at the highest level (normally FLAT_*) and progresses
//		through references to the class that makes the actual reference
//	"Used in FLAT_XXX" indicates that the class is included in more than one FLAT
//  61804-3 references are often abstract and imply the class is derived from
//		the referenced element and not necessarily associated directly with one of
//		the members

// Attrs.h

#pragma once

#include "nsEDDEngine/Common.h"


namespace nsEDDEngine
{
	enum OpRefType
	{
			STANDARD_TYPE	= 0,
			COMPLEX_TYPE	= 1
	};


	/////
	// class STRING		Instances of STRING are used for communicating a wchar_t (16 bit)
	//					string to/from the EDD Engine.
	// members:
	//			*str	A pointer to an array of type wchar_t.  If this is set then len
	//					should be set to the number of characters in the string.
	//			len		The length of the string pointed to by str.  No assumption of
	//					buffer size should be made beyond that it is at least len + 1
	//					to accommodate the contents of the string.
	//			flags	An enumeration indicating the status of the STRING.
	//					FDI_NOT_SET indicates that wchar_t is not initialized.
	//					FDI_DONT_FREE_STRING indicates that the location pointed to
	//						str is owned by another class and should not be freed as
	//						part of the processing for this instance.  The owner of the
	//						string is obligated to not delete the string as long as all
	//						references to it may be in scope.
	//					FDI_FREE_STRING indicates that the location pointed to by str
	//						is owned by this instance and may be deleted with this instance.
	// methods:
	//			Constructor STRING ()
	//						Create an instance of STRING and initialize to initial values
	//						(str = null, len = 0, flags = FDI_NOT_SET)
	//			Constructor STRING (const wchar_t* s)
	//						Create an instance of STRING and initialize to the values of
	//						the passed in wide char data 
	//			Constructor STRING (const char* s)
	//						Create an instance of STRING and initialize to the values of
	//						the passed in char type data
	//			Constructor STRING (const STRING &pDdiString)
	//						Create an instance of STRING and initialize to the values of
	//						the passed in STRING instance.
	
	//			Destructor ~STRING()
	//						If FDI_FREE_STRING free str
	//						Sets instance to initial values (str = null, len = 0,
	//						flags = FDI_NOT_SET)

	//			overload STRING& operator= (const STRING &pDdiString)
	//						Copies values of STRING and initialize to the value of
	//						the passed in STRING instance.
	//			overload STRING& operator= ( const wchar_t* pDdiString)
	//						Copies values of wchar data and initialize to the value of
	//						the passed in wide char type data
	//			overload STRING& operator= (const char* pDdiString )
	//						Copies values of char data and initialize to the value of
	//						the passed in char type data
	//			overload STRING& assign(const wchar_t* s,bool bCopy=true);
	//						Use to set string contents, but not allocate memory
	//			const wchar_t* c_str() const;	
	//						Returns a pointer to the string
	//			unsigned length() const;		
	//						Returns the length of the string
	//			void AssignString(const STRING &pDdiString)	
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of STRING instance.
	/////

	class _INTERFACE STRING {
	private:
		wchar_t	*str;	//The pointer to the UNICODE string 
		size_t  len;	//The length in characters of the string 
		void AssignString(const STRING &pDdiString); //AssignString is used to assign all the attributes of STRING
		enum Flags{
			FDI_NOT_SET = 0,		//Value when first created
			FDI_DONT_FREE_STRING,   //When you clean this STRING, you don't have free the str pointer because this string points to the STRING_TABLE
			FDI_FREE_STRING			//When you clean this STRING, you must call free on the str pointer, since this string was malloc'ed for use by this STRING
		}	flags;		//Memory allocation flags 
	public:
	

		
		STRING();		//Default constructor
		STRING(const wchar_t* s);	//Constructor with wchar_t type param
		STRING(const char* s);		//Constructor with char type param
		~STRING();		//Destructor
		STRING (const STRING &pDdiString);	//Copy Constructor 

		STRING& operator= (const STRING &pDdiString);	//Assignment Operator for STRING OBJ
		STRING& operator=(const wchar_t* pDdiString);	//Assignment Operator for wchar_t string
		STRING& operator=(const char* pDdiString);		//Assignment Operator for char string

		STRING& assign(const wchar_t* s,bool bCopy=true); //Use to set string contents, but not allocate memory

		const wchar_t* c_str() const;	//Returns a pointer to the string
		unsigned length() const;		//Returns the length of the string
	};	


	class _INTERFACE BINARY
	{
	private:
		uchar		*m_ptr;		// Pointer to byte 
		unsigned	m_length;	// Length for byte 
	
	public:
		BINARY();								// Default constructor
		BINARY(const uchar* s,unsigned length);	// Constructor with uchar type param
		BINARY(const BINARY &pDdiBinary);		// Copy constructor
		~BINARY();								// Destructor
	
		BINARY& operator= (const BINARY &pDdiBinary); 	// Overloaded Assignment Operator for BINARY obj
		BINARY& assign(const uchar* s,unsigned length); // Assignment method
		const uchar* ptr() const;						// Returns a pointer to the binary string
		unsigned length() const;						// Returns the length of the binary string
	};
	
	/////
	// class EXPR_VALUE	Instances of EXPR_VALUE are included in EXPR which contains
	//					the context indicating which member is to used.	
	// members:
	//			f		Float value storage
	//			d		Double value storage
	//			u		Unsigned long integer value storage
	//			i		Signed long integer value storage
	//			s		STRING value storage
	//			b		BINARY value storage
	// methods:
	//			constructor EXPR_VALUE()
	//						Create an instance of EXPR_VALUE with all values set to 0
	//			constructor EXPR_VALUE (const EXPR_VALUE & exprValue);
	//						Create an instance of EXPR_VALUE copying values from ExprValue
	//			overload EXPR_VALUE& operator= (const EXPR_VALUE &exprValue);
	//						Copies values from exprValue;
	//			void AssignExprValue(const EXPR_VALUE &exprValue)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of EXPR_VALUE instance.
	/////					
	class _INTERFACE EXPR_VALUE
	{
	public:
		float           f;	// EXPR_TYPE_FLOAT
		double          d;	// EXPR_TYPE_DOUBLE
		ULONGLONG		u;	// EXPR_TYPE_UNSIGNED
		LONGLONG		i;	// EXPR_TYPE_INTEGER
		STRING			s;	// EXPR_TYPE_STRING
		BINARY			b;	// EXPR_TYPE_BINARY  

		EXPR_VALUE()
		{
			f=0;
			d=0;
			u=0;
			i=0;
			// STRING s has own constructor
			// BINARY b has own constructor
		}
		//Copy Constructor 
		EXPR_VALUE (const EXPR_VALUE & ExprValue);
		//Overloaded Assignment Operator 
		EXPR_VALUE& operator= (const EXPR_VALUE &exprValue);

		~EXPR_VALUE() {}
	private:
		//AssignExprValue is used to assign all the attributes of list EXPR_VALUE
		void AssignExprValue(const EXPR_VALUE &exprValue);

	};

	/*
	* Expressions
	*/
	/////
	// class EXPR		Instances of EXPR are used for communicating values of
	//					variable types to/from the EDD Engine.
	//					Stored Result of the expression specified in FDI-2041 "Expression_Specifier"
	// members:
	//			eType	Context indicating the type of the value.  One of:
	//					EXPR_TYPE_NONE, EXPR_TYPE_FLOAT, EXPR_TYPE_DOUBLE,
	//					EXPR_TYPE_UNSIGNED, EXPR_TYPE_INTEGER, EXPR_TYPE_STRING
	//			size	Size in Bytes of the data type.
	//			val		An instance of EXPR_VALUE that contains the actual value.  The
	//					member of EXPR_VALUE to be used is determined by eType.
	// methods:
	//			constructor EXPR()
	//						Create an instance of EXPR of type EXPR_TYPE_NONE
	//			constructor EXPR (const EXPR & expr);
	//						Create an instance of EXPR copying values from expr
	//			overload EXPR& operator= (const EXPR &expr);
	//						Copies values from expr;
	//			void Init()
	//						Initialize class members to type EXPR_TYPE_NONE
	//			void ConvertTo(ExprValueType newType, unsigned short newSize)
	//						Converts one type of value to another.
	//          void AssignExpr(const EXPR &expr)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of EXPR instance.
	/////				
	class TYPE_SIZE;
	class _INTERFACE EXPR {
	public:
		/*
		* Types of expressions.
		* The permitted type fields are the same as variables types (ddldefs.h)
		*/
		enum ExprValueType {
			EXPR_TYPE_NONE		= 0,
			EXPR_TYPE_FLOAT		= 1,
			EXPR_TYPE_DOUBLE	= 2,
			EXPR_TYPE_UNSIGNED  = 3,
			EXPR_TYPE_INTEGER	= 4,
			EXPR_TYPE_STRING	= 5,
			EXPR_TYPE_BINARY	= 6  
		};

		// Members
		ExprValueType  eType;	/* valid types are defined in ddldefs.h (ie. INTEGER) */
		unsigned int  size;

		EXPR_VALUE val;

		// Constructor
		EXPR()
		{
			Init();
		}
	
		// Destructor
		~EXPR()
		{
			Dispose();
		}

		// Initialization method called primarily by the Constructor
		void Init()
		{
			eType = EXPR_TYPE_NONE;
			size = 0;
		};
		//Copy Constructor 
		EXPR(const EXPR &expr);

		//Overloaded Assignment Operator 
		EXPR& operator= (const EXPR &expr);
		void Dispose();
		void ConvertTo(ExprValueType newType, unsigned int newSize);
		void SetExprAsPerVariableType(const TYPE_SIZE &TypeSize);
	private:
		//AssignExpr is used to assign all the attributes of EXPR
		void AssignExpr(const EXPR &expr);
	};


	/*
	*  RANGE_DATA_LIST
	*/
	/////
	// class RANGE_DATA_LIST	Instances of RANGE_DATA_LIST are used for storing
	//							min/max values in FLAT_VAR. See Flats.h for usage
	//							Used in FLAT_VAR
	// members:
	//			count	Number of EXPR values in list
	//			limit	Size of list array.
	//			list	Array of EXPR values
	// methods:
	//			constructor RANGE_DATA_LIST()
	//						Create an instance of RANGE_DATA_LIST with count, limit = 0 and list = nullptr
	//			constructor RANGE_DATA_LIST (const RANGE_DATA_LIST & range_data_list)
	//						Create an instance of RANGE_DATA_LIST copying values from range_data_list
	//			destructor	~RANGE_DATA_LIST()
	//						Delete contents of list before self destructing
	//			overload RANGE_DATA_LIST& operator= (const RANGE_DATA_LIST &range_data_list)
	//						Copies values from range_data_list;
	//	        void AssignRangeDataList(const RANGE_DATA_LIST &range_data_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RANGE_DATA_LIST instance.
	/////				
	class _INTERFACE RANGE_DATA_LIST {
	public:
		unsigned short  count;
		unsigned short  limit;
		EXPR           *list;

		RANGE_DATA_LIST()
		{
			count = 0;
			limit = 0;
			list  = nullptr;	
		}

		//Copy Constructor 
		RANGE_DATA_LIST(const RANGE_DATA_LIST &range_data_list);
		
		//Overloaded Assignment Operator 
		RANGE_DATA_LIST& operator= (const RANGE_DATA_LIST &range_data_list);

		~RANGE_DATA_LIST();
	private:
		//AssignRangeDataList is used to assign all the attributes of list RANGE_DATA_LIST
		void AssignRangeDataList(const RANGE_DATA_LIST &range_data_list);

	};


	/*
	* Defines for REFERENCE_ITEM types
	*/
	/////
	// class ITEM_ID_LIST	Instances of ITEM_ID_LIST contain a list of ITEM_ID.
	//						Used in FLAT_BLOCK
	// members:
	//			count	Number of ITEM_IDs in list
	//			limit	Size of list array.
	//			list	Array of ITEM_IDs
	// methods:
	//			constructor ITEM_ID_LIST()
	//						Create an instance of ITEM_ID_LIST with count, limit = 0 and list = nullptr
	//			constructor ITEM_ID_LIST (const ITEM_ID_LIST & item_id_list);
	//						Create an instance of ITEM_ID_LIST copying values from item_id_list
	//			destructor	~ITEM_ID_LIST()
	//						Delete contents of list before self destructing
	//			overload ITEM_ID_LIST& operator= (const ITEM_ID_LIST &item_id_list);
	//						Copies values from item_id_list;
	//			void AssignItemIdList(const ITEM_ID_LIST &item_id_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ITEM_ID_LIST instance.
	//
	/////			

	class _INTERFACE ITEM_ID_LIST {
	public:
		// Members
		unsigned short  count;		/* Number of used item IDs */
		unsigned short  limit;		/* Total number of item IDs */
		ITEM_ID        *list;		/* Pointer to list of IDs */

		// Constructor
		ITEM_ID_LIST()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;
		}

		//copy constructor
		ITEM_ID_LIST(const ITEM_ID_LIST &item_id_list);
		
		//Overloaded Assignment Operator 
		ITEM_ID_LIST& operator= (const ITEM_ID_LIST &item_id_list);

		~ITEM_ID_LIST();
	private:
		//AssignItemIdList is used to assign all the attributes of list ITEM_ID_LIST
		void AssignItemIdList(const ITEM_ID_LIST &item_id_list);
	};

	/////
	// class DESC_REF	Descriptive Reference:
	//					Instances of DESC_REF contains an item ID and Item Type.
	//					It is used to communicate to the consumer a reqested item with its type
	//					so the consumer knows what to do with it.  
	// members:
	//			id			ID of the item
	//			ITEM_TYPE	Type of the item
	// methods:
	//			constructor DESC_REF()
	//						Create an instance of DESC_REF with id of 0 and type ITYPE_NO_VALUE
	//			constructor DESC_REF (const DESC_REF & desc_ref);
	//						Create an instance of DESC_REF copying values from desc_ref
	//			overload DESC_REF& operator= (const DESC_REF &desc_ref);
	//						Copies values from desc_ref;
	//			destructor	~DESC_REF()
	//						Delete contents before self destructing
	//			
	/////			
	class _INTERFACE DESC_REF {
	public:

		// Members
		ITEM_ID         id;
		ITEM_TYPE       type;

		// Constructor
		DESC_REF()
		{
			id = 0;
			type = ITYPE_NO_VALUE;
		};

		// Destructor
		~DESC_REF() {};

		//copy constructor
		DESC_REF(const DESC_REF &desc_ref)
		{
			id		= desc_ref.id;
			type	= desc_ref.type;
		}
		
		//Overloaded Assignment Operator 
		DESC_REF& operator= (const DESC_REF &desc_ref)
		{
			id		= desc_ref.id;
			type	= desc_ref.type;

			return *this;
		}
	};
		
	/////
	// class ATTR_INFO		This class provides debug information about the file and line number of a specific attribute
	//						This structure is only filled in when the tokenizer is in debug mode.
	//						Used in FLATS_XXX->ITEM_INFORMATION->ATTR_INFO_LIST
	//						As specified in FID-2041 "Item Information"
	// members:
	//			attr_file_name		 File name of attribute definition file
	//			attr_lineno			 Line number in file where attribute is defined
	// methods:
	//			constructor ATTR_INFO()
	//						Create an instance of ATTR_INFO with attr_lineno of 0
	//			constructor ATTR_INFO (const ATTR_INFO & attr_info);
	//						Create an instance of ATTR_INFO copying values from attr_info
	//			overload ATTR_INFO& operator= (const ATTR_INFO &attr_info);
	//						Copies values from attr_info;
	//			void AssignAttrInfo(const ATTR_INFO &attr_info)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ATTR_INFO instance.
	/////					

	class _INTERFACE ATTR_INFO {
	public:
		// Members
		STRING attr_file_name;
		int attr_lineno;
		AttributeName attr_name;

		// Constructor
		ATTR_INFO()
		{
			attr_lineno=0;
			attr_name = _end_;
		}
		

		// Copy constructor 
		ATTR_INFO(const ATTR_INFO &attr_info);

		//Overloaded assignment operator
		ATTR_INFO& operator= (const ATTR_INFO &attr_info);

		//Destructor
		~ATTR_INFO() {};

	private:
		//AssignAttrInfo is used to assign all the attributes of ATTR_INFO
		void AssignAttrInfo(const ATTR_INFO &attr_info);
	};

	/////
	// class ATTR_INFO_LIST	Instances of ITEM_ID_LIST contain a list of ATTR_INFO
	//						Used in FLATS_XXX->ITEM_INFORMATION
	//						As specified in FDI-2041 "Item Information->Attr_Info"
	// members:
	//			count	Number of ITEM_IDs in list
	//			limit	Size of list array.
	//			list	Array of ITEM_IDs
	// methods:
	//			constructor ATTR_INFO_LIST()
	//						Create an instance of ATTR_INFO_LIST with count, limit = 0 and list = nullptr
	//			constructor ATTR_INFO_LIST (const ATTR_INFO_LIST & attr_info_list);
	//						Create an instance of ATTR_INFO_LIST copying values from attr_info_list
	//			destructor	~ATTR_INFO_LIST()
	//						Delete contents of list before self destructing
	//			overload ATTR_INFO_LIST& operator= (const ATTR_INFO_LIST &attr_info_list);
	//						Copies values from attr_info_list;
	//			void AssignAttrInfoList(const ATTR_INFO_LIST &attr_info_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ATTR_INFO_LIST instance.
	/////				
	class _INTERFACE ATTR_INFO_LIST
	{
	public:
		// Members
		unsigned short  count;
		unsigned short  limit;
		ATTR_INFO       *list;

		// Constructor
		ATTR_INFO_LIST()
		{
			count=0;
			limit=0;
			list=nullptr;
		}

		// Destructor
		~ATTR_INFO_LIST();

		// Copy constructor 
		ATTR_INFO_LIST(const ATTR_INFO_LIST &attr_info_list);

		//Overloaded assignment operator
		ATTR_INFO_LIST& operator= (const ATTR_INFO_LIST &attr_info_list);

	private:
		//AssignAttrInfoList is used to assign all the attributes of ATTR_INFO_LIST
		void AssignAttrInfoList(const ATTR_INFO_LIST &attr_info_list);
	};

	/////
	// class ITEM_INFORMATION	Storage for the information provided in the item_information attribute
	//							This structure is only filled in when the tokenizer is in debug mode.
	//							Used in FLATS_XXX
	//							Specified in FDI-2041 "Item Information->Item_Information"
	// members:
	//			attr_file_name		File name of item definition file
	//			attr_lineno			Line number in file where item is defined
	//			attrs				The list of attributes which are defined for this item
	// methods:
	//			constructor ITEM_INFORMATION()
	//						Create an instance of ITEM_INFORMATION with line_number of 0
	//			constructor ITEM_INFORMATION (const ITEM_INFORMATION & item_info);
	//						Create an instance of ITEM_INFORMATION copying values from item_info
	//			overload ITEM_INFORMATION& operator= (const ITEM_INFORMATION &item_info);
	//						Copies values from item_info;
	//			void AssignItemInformation(const ITEM_INFORMATION &item_info)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ITEM_INFORMATION instance.
	/////					

	class _INTERFACE ITEM_INFORMATION {
	public:
		// Members
		STRING file_name;
		int line_number;

		ATTR_INFO_LIST attrs;

		// Constructor
		ITEM_INFORMATION()
		{
			line_number=0;
		}
		
		// Copy constructor 
		ITEM_INFORMATION(const ITEM_INFORMATION &item_info);

		//Overloaded assignment operator
		ITEM_INFORMATION& operator= (const ITEM_INFORMATION &item_info);

	private:
		//AssignItemInformation is used to assign all the attributes of ITEM_INFORMATION
		void AssignItemInformation(const ITEM_INFORMATION &item_info);
	};
	
	/////
	// class RESOLVE_INFO	Contains information identifying an member of an OP_REF_TRAIL
	//						The trail results from the resolving of a complex reference and
	//						is not explicitly specified in FDI-2014 or 61804-3
	//						Used in FLAT_XXXX->OP_REF_TRAIL_LIST->OP_REF_TRAIL
	// members:
	//			type		Item type of this entry
	//			id			ID of this entry
	//			element		Index into the entry if it is a list/array/etc...	
	// methods:
	//			constructor RESOLVE_INFO()
	//						Create an instance of RESOLVE_INFO where type, id and element = 0
	//			destructor ~RESOLVE_INFO ();
	//						Delete contents of pointer members before self destructing
	/////					
	class _INTERFACE RESOLVE_INFO {
	public:
		//Memeber
		ITEM_TYPE       type;
		ITEM_ID         id;
		unsigned long   element;

		//Constructor
		RESOLVE_INFO() 
		{
			type	= ITYPE_NO_VALUE;
			id		= 0;
			element = 0;

		};

		//Destructor
		~RESOLVE_INFO() {};
	};

	/////
	// class OP_REF_TRAIL	Contains all the information needed for an OP REF trail.
	//						OpRefType is either STANDARD_TYPE (reference is found in the OP_REF_INFO class), 
	//						or COMPLEX_TYPE (reference is found in the OP_REF_INFO_LIST class)
	//						The trail results from the resolving of a complex reference and
	//						is not explicitly specified in FDI-2014 or 61804-3
	//						Used in FLAT_XXXX->OP_REF_TRAIL_LIST
	// members:
	//			op_ref_type			Indicates whether this is a standard or complex reference
	//			op_info				If a standard reference, use this member
	//			op_info_list		If a complex reference, use this member
	//			block_instance		Block instance for FF reference, 0 for non-FF		
	//			desc_id				ID of element at end of trail
	//			desc_type			Ref of element at end of trail
	//			desc_bit_mask		Used for BIT_ENUM variables on menus, set to 0 if unused
	//			trail_count			Number of elements in trail
	//			trail_limit			Size of the buffer containing trail
	//			*trail				Array of elements followed to resolve a complex reference
	//			expr				Result of the resolved complex reference
	// methods:
	//			constructor OP_REF_TRAIL()
	//						Create an instance of OP_REF_TRAIL where all members are initialized to empty states.
	//			constructor OP_REF_TRAIL(const OP_REF_TRAIL &op_ref_trail);
	//						Create an instance of OP_REF_TRAIL copying values from op_ref_trail
	//			destructor	~OP_REF_TRAIL()
	//						Delete contents of pointer members before self destructing
	//			overload OP_REF_TRAIL& operator= (const OP_REF_TRAIL &op_ref_trail);
	//						Copies values from op_ref_trail;
	//			void Init()
	//						Initialize all members to empty values
	//			void AssignOpRefTrail(const OP_REF_TRAIL &op_ref_trail)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_REF_TRAIL instance.
	/////					
	class _INTERFACE OP_REF_TRAIL {
	public:
		// Operational reference
		OpRefType			op_ref_type;
		OP_REF_INFO			op_info;			// OpRefType.STANDARD_TYPE
		OP_REF_INFO_LIST	op_info_list;		// OpRefType.COMPLEX_TYPE

		// For FF CrossBlock
		BLOCK_INSTANCE	block_instance;			

		// Descriptive reference
		ITEM_ID         desc_id;
		ITEM_TYPE		desc_type;

		unsigned long   desc_bit_mask;	/* Used for BIT_ENUM variables on menus, set to 0 if unused */

		unsigned short  trail_count;
		unsigned short  trail_limit;
		RESOLVE_INFO   *trail;
		EXPR			expr;

		// Constructor
		OP_REF_TRAIL()
		{
			Init();
		}
		//copy constructor
		OP_REF_TRAIL(const OP_REF_TRAIL &op_ref_trail);

		//Overloaded Assignment Operator 
		OP_REF_TRAIL& operator= (const OP_REF_TRAIL &op_ref_trail);

		// Destructor
		~OP_REF_TRAIL()
		{
			Dispose();
		}

		// Set all elments to initial (empty) values
		void Init()
		{
			this->op_ref_type		= STANDARD_TYPE;
			this->block_instance	= 0;
			this->desc_id			= 0;
			this->desc_type			= ITYPE_NO_VALUE;
			this->desc_bit_mask		= 0;
			this->trail_count		= 0;
			this->trail_limit		= 0;
			this->trail				= nullptr;
			this->expr.Init();
		}

		// Dispose of trail and expr
		void Dispose();
	private:
		//AssignOpRefTrail is used to assign all the attributes of OP_REF_TRAIL
		void AssignOpRefTrail(const OP_REF_TRAIL &op_ref_trail);
	};

	/////
	// class OP_REF_TRAIL_LIST	Instances of OP_REF_TRAIL_LIST contain a list of OP_REF_TRAIL
	//							The trail results from the resolving of a complex reference and
	//							is not explicitly specified in FDI-2014 or 61804-3
	//							Used in FLAT_XXXX
	//
	// members:
	//			count	Number of OP_REF_TRAIL in list
	//			limit	Size of list array.
	//			list	Array of OP_REF_TRAIL
	// methods:
	//			constructor OP_REF_TRAIL_LIST()
	//						Create an instance of OP_REF_TRAIL_LIST with count, limit = 0 and list = nullptr
	//			constructor OP_REF_TRAIL_LIST (const OP_REF_TRAIL_LIST & op_ref_trail_list);
	//						Create an instance of OP_REF_TRAIL_LIST copying values from op_ref_trail_list
	//			destructor	~OP_REF_TRAIL_LIST()
	//						Delete contents of list before self destructing
	//			overload OP_REF_TRAIL_LIST& operator= (const OP_REF_TRAIL_LIST &op_ref_trail_list);
	//						Copies values from op_ref_trail_list;
	//			void AssignOpRefTrialList(const OP_REF_TRAIL_LIST &op_ref_trail_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_REF_TRAIL_LIST instance.
	//
	/////			

	class _INTERFACE OP_REF_TRAIL_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		OP_REF_TRAIL   *list;

		// Constructor
		OP_REF_TRAIL_LIST()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;
		}

		//copy constructor
		OP_REF_TRAIL_LIST(const OP_REF_TRAIL_LIST &op_ref_trail_list);

		//Overloaded Assignment Operator 
		OP_REF_TRAIL_LIST& operator= (const OP_REF_TRAIL_LIST &op_ref_trail_list);

		// Destructor
		~OP_REF_TRAIL_LIST();
	private:
		//AssignOpRefTrialList is used to assign all the attributes of list OP_REF_TRAIL_LIST
		void AssignOpRefTrialList(const OP_REF_TRAIL_LIST &op_ref_trail_list);


	};

	/////
	// class NUMBER_EXPR	This is class is used in the RANGE_SET to describe the MIN_VALUEn or MAX_VALUEn
	//						Used in FLAT_COMPONENT_RELATION->COMPONENT_SPECIFIER_LIST->COMPONENT_SPECIFIER->RANGE_LIST->RANGE_SET
	//				
	//members:
	//		which			MIN_VALUEn or MAX_VALUEn (n is �which�) plus the value associated with it
	//		EXPR
	// methods:
	//			constructor NUMBER_EXPR()
	//						Create an instance of NUMBER_EXPR
	//			NUMBER_EXPR(const NUMBER_EXPR &number_expr)
	//						Create an instance of NUMBER_EXPR copying values from range_set
	//			overload NUMBER_EXPR& operator= (const NUMBER_EXPR &number_expr);
	//						Copies values from number_expr;
	//			void AssignNumberExpr(const NUMBER_EXPR &number_expr)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of NUMBER_EXPR instance.
	/////				

	class _INTERFACE NUMBER_EXPR {
	public:
		// Members
		ulong which;
		EXPR value;

		// Constructor
		NUMBER_EXPR()
		{
			which = 0;
		}

		// Copy constructor 
		NUMBER_EXPR(const NUMBER_EXPR &number_expr);

		//Overloaded assignment operator
		NUMBER_EXPR& operator= (const NUMBER_EXPR &number_expr);

		// Destructor
		~NUMBER_EXPR() {};
	private:
		//AssignNumberExpr is used to assign all the attributes of NUMBER_EXPR
		void AssignNumberExpr(const NUMBER_EXPR &number_expr);

	};

	/////
	// class RANGE_SET		Storage of the range set.
	//						Used in FLAT_COMPONENT_RELATION->COMPONENT_SPECIFIER_LIST->COMPONENT_SPECIFIER->RANGE_LIST
	//						Specified in 61804-3 "COMPONENT_RELATION->Specific attributes->COMPONENTS"
	//						Specified in FDI-2041 "Components Specifier->Range_Set"
	// members:
	//            max_val                   Maximum value in the range (MAX_VALUE)
	//            min_value                 Minimum value in the range (MIN_VALUE)
	//            max_num                   Maximum value-n in the range (MAX_VALUEn)
	//            min_num                   Minimum value-n in the range (MIN_VALUEn)
	// methods:
	//			constructor RANGE_SET()
	//						Create an instance of RANGE_SET
	//			constructor RANGE_SET (const RANGE_SET & range_set);
	//						Create an instance of RANGE_SET copying values from range_set
	//			overload RANGE_SET& operator= (const RANGE_SET &range_set);
	//						Copies values from range_set;
	//			void AssignRangeset(const RANGE_SET &range_set)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RANGE_SET instance.
	/////					
	class _INTERFACE RANGE_SET {
	public:
		// Members
		NUMBER_EXPR max_num;
		NUMBER_EXPR min_num;

		// Constructor
		// EXPR does it's own initialization
		RANGE_SET()
		{}

		// Copy constructor 
		RANGE_SET(const RANGE_SET &range_set);

		//Overloaded assignment operator
		RANGE_SET& operator= (const RANGE_SET &range_set);

		// Destructor
		~RANGE_SET() {};
	private:
		//AssignRangeset is used to assign all the attributes of RANGE_SET
		void AssignRangeset(const RANGE_SET &range_set);

	};

	/////
	// class RANGE_LIST		Instances of RANGE_LIST contain a list of RANGE_SET
	//						Used in FLAT_COMPONENT_RELATION->COMPONENT_SPECIFIER_LIST->COMPONENT_SPECIFIER
	//						Specified in 61804-3 "COMPONENT_RELATION->Specific attributes->COMPONENTS"
	//						Specified in FDI-2041 "Components Specifier->Range_List"
	//
	// members:
	//			count	Number of RANGE_SETs in list
	//			limit	Size of list array.
	//			list	Array of RANGE_SET
	// methods:
	//			constructor RANGE_LIST()
	//						Create an instance of RANGE_LIST with count, limit = 0 and list = nullptr
	//			constructor RANGE_LIST (const RANGE_LIST & range_list);
	//						Create an instance of RANGE_LIST copying values from range_list
	//			destructor	~RANGE_LIST()
	//						Delete contents of list before self destructing
	//			overload RANGE_LIST& operator= (const RANGE_LIST &range_list);
	//						Copies values from range_list;
	//			void AssignRangeList(const RANGE_LIST &range_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RANGE_LIST instance.
	/////					
	class _INTERFACE RANGE_LIST {
	public:
		// Members
		DESC_REF	var_refrences;

		unsigned short  count;
		unsigned short  limit;
		RANGE_SET       *list;

		// Constructor
		RANGE_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		}

		// Copy constructor 
		RANGE_LIST(const RANGE_LIST &range_list);

		//Overloaded assignment operator
		RANGE_LIST& operator= (const RANGE_LIST &range_list);

		// Destructor
		~RANGE_LIST();


	private:
		//AssignRangeList is used to assign all the attributes of list RANGE_LIST
		void AssignRangeList(const RANGE_LIST &range_list);
	};

	/////
	// class COMPONENT_SPECIFIER	Storage of the Component_Reference
	//								Used in FLAT_COMPONENT_RELATION->COMPONENT_SPECIFIER_LIST
	//								Specified in FDI-2041 "Components Specifier->Component_Reference"
	//								Specified in 61804-3 "COMPONENTS"
	// members:
	//			maximum_number		The maximum allowed number of this component
	//			minimum_number		The minimum allowed number of this component
	//			auto_create			The number of automatically created components.
	//			filter				Result of an expression that filters components
	//			range_list			Used to define restrictions
	// methods:
	//			constructor COMPONENT_SPECIFIER()
	//						Create an instance of COMPONENT_SPECIFIER
	//			constructor COMPONENT_SPECIFIER (const COMPONENT_SPECIFIER & component_specifier);
	//						Create an instance of COMPONENT_SPECIFIER copying values from component_specifier
	//			overload COMPONENT_SPECIFIER& operator= (const COMPONENT_SPECIFIER &component_specifier);
	//						Copies values from component_specifier;
	//			void AssignComponentSpecifier(const COMPONENT_SPECIFIER &component_specifier)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of COMPONENT_SPECIFIER instance.
	/////					
	class _INTERFACE COMPONENT_SPECIFIER
	{
	public:
		//Members
		OP_REF_TRAIL cr_ref;

		EXPR maximum_number;
		EXPR minimum_number;
		EXPR auto_create;
		Boolean filter;

		RANGE_LIST range_list;

		// Constructor
		COMPONENT_SPECIFIER()
		{
			filter = Boolean::False;
		}

		//Copy Constructor 
		COMPONENT_SPECIFIER(const COMPONENT_SPECIFIER &component_specifier);
		//Overloaded Assignment Operator 
		COMPONENT_SPECIFIER& operator= (const COMPONENT_SPECIFIER &component_specifier);

		// Destructor
		~COMPONENT_SPECIFIER();
	private:
		//AssignComponentSpecifier is used to assign all the attributes of list COMPONENT_SPECIFIER
		void AssignComponentSpecifier(const COMPONENT_SPECIFIER &component_specifier);
	};

	/////
	// class COMPONENT_SPECIFIER_LIST	Instances of COMPONENT_SPECIFIER_LIST contain a list of COMPONENT_SPECIFIER
	//									Used in FLAT_COMPONENT_RELATION
	//									Specified in 61804-3 "COMPONENTS"
	//									Specified in FDI-2041 "Components Specifier->Components_Specifier"
	//
	// members:
	//			count	Number of COMPONENT_SPECIFIERs in list
	//			limit	Size of list array.
	//			list	Array of COMPONENT_SPECIFIERs
	// methods:
	//			constructor COMPONENT_SPECIFIER_LIST()
	//						Create an instance of COMPONENT_SPECIFIER_LIST with count, limit = 0 and list = nullptr
	//			constructor COMPONENT_SPECIFIER_LIST (const COMPONENT_SPECIFIER_LIST & component_specifier_list);
	//						Create an instance of COMPONENT_SPECIFIER_LIST copying values from component_specifier_list
	//			destructor	~COMPONENT_SPECIFIER_LIST()
	//						Delete contents of list before self destructing
	//			overload COMPONENT_SPECIFIER_LIST& operator= (const COMPONENT_SPECIFIER_LIST &component_specifier_list);
	//						Copies values from component_specifier_list;
	//			void AssignComponentSpecifierList(const COMPONENT_SPECIFIER_LIST &component_specifier_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of COMPONENT_SPECIFIER_LIST instance.
	/////				
	class _INTERFACE COMPONENT_SPECIFIER_LIST
	{
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		COMPONENT_SPECIFIER       *list;

		// Constructor
		COMPONENT_SPECIFIER_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		}

		//Copy Constructor 
		COMPONENT_SPECIFIER_LIST(const COMPONENT_SPECIFIER_LIST &component_specifier_list);
		//Overloaded Assignment Operator 
		COMPONENT_SPECIFIER_LIST& operator= (const COMPONENT_SPECIFIER_LIST &component_specifier_list);

		// Destructor
		~COMPONENT_SPECIFIER_LIST();


	private:
		//AssignComponentSpecifierList is used to assign all the attributes of list COMPONENT_SPECIFIER_LIST
		void AssignComponentSpecifierList(const COMPONENT_SPECIFIER_LIST &component_specifier_list);
	};

	/////

	// class BINARY_IMAGE	Storage of the Binary image data read in by the tokenizer for for an IMAGE
	//						Used in FLAT_IMAGE
	//						Specified in 61804-3 "IMAGE"
	//						Specified in FDI-2041 "DDEF Image Data"
	// members:
	//			length				The Length of the binary image data
	//			data				The the binary image data
	// methods:
	//			constructor BINARY_IMAGE()
	//						Create an instance of BINARY_IMAGE
	//			constructor BINARY_IMAGE (const BINARY_IMAGE & binary_image);
	//						Create an instance of BINARY_IMAGE copying values from binary_image
	//			destructor ~BINARY_IMAGE()
	//						Delete contents of pointer members before self destructing
	//			overload BINARY_IMAGE& operator= (const BINARY_IMAGE &binary_image);
	//						Copies values from binary_image;
	//			void AssignBinaryImage(const BINARY_IMAGE &binary_image)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of BINARY_IMAGE instance.
	//
	/////				

	class _INTERFACE BINARY_IMAGE {
	public:
		//Memebers
		unsigned long length;
		unsigned char *data;

		// Constructor
		BINARY_IMAGE()
		{
			length = 0;
			data = nullptr;
		}

		// Destructor
		~BINARY_IMAGE();

		//copy constructor
		BINARY_IMAGE(const BINARY_IMAGE &binary_image);

		//Overloaded Assignment Operator 
		BINARY_IMAGE& operator= (const BINARY_IMAGE &binary_image);

		// Dispose Method for deleting data
        void Dispose();

		private:
		//AssignBinaryImage is used to assign all the attributes of list BINARY_IMAGE
		void AssignBinaryImage(const BINARY_IMAGE &binary_image);
	};


	/*
	* Binary and Dependency Info
	*/

	/////
	// struct BININFO		Storage of Binary Info
	//						Used in BIN_BLOCK_DIR.  See that structure for fuller understanding of
	//						the scope of BININFO
	//						This is an implementation specific structure of the storage of elements from FDI-2041
	// members:
	//			size				The Length of the binary data
	//			chunk				The binary image data
	//			
	/////		

	typedef struct tag_BININFO {
		unsigned long   size;
		unsigned char  *chunk;
	}               BININFO;


	/* The masks for ITEM_ARRAY_ELEMENT */
	/////
	// enum ItemArrayElementEvaled	Bit field definitions used to indicate if members of ITEM_ARRAY_ELEMENT
	//								have been evaluated.
	/////

	enum ItemArrayElementEvaled
	{
		FDI_IA_DESC_EVALED	= 0x01,		// Description has been evaluated
		FDI_IA_HELP_EVALED	= 0x02,		// Help has been evaluated
		FDI_IA_INDEX_EVALED	= 0x04,		// Item array index has been evaluated
		FDI_IA_REF_EVALED	= 0x08,		// Item array Descriptive Reference had been evaluated
	};

	/////
	// class ITEM_ARRAY_ELEMENT	Storage of the Binary image data read in by the tokenizer for for an IMAGE
	//							Used in FLAT_ITEM_ARRAY->ITEM_ARRAY_ELEMENT_LIST
	//							Specified in 61804-3 "REFERENCE_ARRAY->ELEMENTS", and 
	//							Specified in FDI-2041 "Array Attributes->Reference_Array_Attributes"
	// members:
	//			evaled		Binary flags indicating if particular elements have been evaluated
	//						See the definition of ItemArrayElementEvaled for the bit definitions.
	//			index		The number by which the item may be referenced (set if evaled contains FDI_IA_INDEX_EVALED)
	//			ref			A reference to a data-item instance (DESC_REF) (set if evaled contains FDI_IA_REF_EVALED)
	//			desc		A short description for the item (STRING) (set if evaled contains FDI_IA_DESC_EVALED)	
	//			help		Help text for the item (STRING) (set if evaled contains FDI_IA_HELP_EVALED)
	// methods:
	//			constructor ITEM_ARRAY_ELEMENT()
	//						Create an instance of ITEM_ARRAY_ELEMENT
	//			constructor ITEM_ARRAY_ELEMENT (const BINARY_IMAGE & item_array_element);
	//						Create an instance of ITEM_ARRAY_ELEMENT copying values from item_array_element
	//			destructor ~ITEM_ARRAY_ELEMENT()
	//						Delete contents of pointer members before self destructing
	//			overload ITEM_ARRAY_ELEMENT& operator= (const ITEM_ARRAY_ELEMENT &item_array_element);
	//						Copies values from item_array_element;
	//			void AssignItemArrayElement(const ITEM_ARRAY_ELEMENT &item_array_element)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ITEM_ARRAY_ELEMENT instance.
	//
	/////	

	class _INTERFACE ITEM_ARRAY_ELEMENT {
	public:
		//Members
		ItemArrayElementEvaled  evaled;
		unsigned long   index;
		DESC_REF        ref;
		STRING          desc;
		STRING          help;

		// Constructor
		ITEM_ARRAY_ELEMENT()
		{
			evaled = (ItemArrayElementEvaled)0;
			index = 0;

		};

		// Destructor
		~ITEM_ARRAY_ELEMENT();

		//Copy Constructor 
		ITEM_ARRAY_ELEMENT(const ITEM_ARRAY_ELEMENT &item_array_element);
		
		//Overloaded Assignment Operator 
		ITEM_ARRAY_ELEMENT& operator= (const ITEM_ARRAY_ELEMENT &item_array_element);

	private:
		//AssignItemArrayElement is used to assign all the attributes of ITEM_ARRAY_ELEMENT
		void AssignItemArrayElement(const ITEM_ARRAY_ELEMENT &item_array_element);
	};

	/////
	// class ITEM_ARRAY_ELEMENT_LIST	Instances of ITEM_ARRAY_ELEMENT_LIST contain a list of ITEM_ARRAY_ELEMENT
	//									Used in FLAT_ITEM_ARRAY
	//									Specified in 61804-3 "REFERENCE_ARRAY->ELEMENTS", and 
	//									Specified in FDI-2041 "Array Attributes->Reference_Array_Attributes"
	// members:
	//			count	Number of ITEM_ARRAY_ELEMENT in list
	//			limit	Size of list array.
	//			list	Array of ITEM_ARRAY_ELEMENT
	// methods:
	//			constructor ITEM_ARRAY_ELEMENT_LIST()
	//						Create an instance of ITEM_ARRAY_ELEMENT_LIST with count, limit = 0 and list = nullptr
	//			constructor ITEM_ARRAY_ELEMENT_LIST (const ITEM_ARRAY_ELEMENT_LIST & item_array_element_list);
	//						Create an instance of ITEM_ARRAY_ELEMENT_LIST copying values from item_array_element_list
	//			destructor	~ITEM_ARRAY_ELEMENT_LIST()
	//						Delete contents of list before self destructing
	//			overload ITEM_ARRAY_ELEMENT_LIST& operator= (const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list);
	//						Copies values from item_array_element_list;
	//			void AssignItemArrayElementList(const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ITEM_ARRAY_ELEMENT_LIST instance.
	//
	/////					
	class _INTERFACE ITEM_ARRAY_ELEMENT_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		ITEM_ARRAY_ELEMENT *list;

		//Constructor
		ITEM_ARRAY_ELEMENT_LIST() 
		{
			count = 0;
			limit = 0;
			list = nullptr;
		};
		
		//Destructor
		~ITEM_ARRAY_ELEMENT_LIST();
		
		//Copy Constructor 
		ITEM_ARRAY_ELEMENT_LIST(const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list);
		
		//Overloaded Assignment Operator 
		ITEM_ARRAY_ELEMENT_LIST& operator= (const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list);

	private:
		//AssignItemArrayElementList is used to assign all the attributes of list ITEM_ARRAY_ELEMENT_LIST
		void AssignItemArrayElementList(const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list);
	};

	/////
	// class MEMBER	Specifies on item in a members list. 
	//						  Used in FLAT_XXXXX->MEMBER_LIST
	//						  Specified in 61804-3 "MEMBERS"
	//						  Specified in FDI-2041 "Members List Specifier ->Member"
	// members:
	//			evaled		Binary flags indicating if particular elements have been evaluated
	//						See the definition of MemberEvaled for the bit definitions.
	//			name		member�s numeric key(set if evaled contains FDI_MEM_NAME_EVALED)
	//			ref			A reference to a data-item instance (DESC_REF) (set if evaled contains FDI_MEM_REF_EVALED)
	//			desc		A short description for the member (STRING) (set if evaled contains FDI_MEM_DESC_EVALED)	
	//			help		Help text for the member (STRING) (set if evaled contains FDI_MEM_HELP_EVALED)
	// methods:
	//			constructor MEMBER()
	//						Create an instance of MEMBER
	//			constructor MEMBER (const MEMBER & member);
	//						Create an instance of MEMBER copying values from item_array_element
	//			destructor ~MEMBER()
	//						Delete contents of pointer members before self destructing
	//			overload MEMBER& operator= (const MEMBER &member);
	//						Copies values from member;
	//			void AssignMember(const MEMBER &member)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of MEMBER instance.
	//
	/////					

	class _INTERFACE MEMBER {
	public:

		enum MemberEvaled
		{
			FDI_MEM_DESC_EVALED		= 0x01,
			FDI_MEM_HELP_EVALED		= 0x02,
			FDI_MEM_NAME_EVALED		= 0x04,
			FDI_MEM_REF_EVALED		= 0x08
		};

		//Members
		MemberEvaled    evaled;
		unsigned long   name;
		DESC_REF        ref;
		STRING          desc;
		STRING          help;

		//Constructor
		MEMBER()
		{
			evaled = (MemberEvaled)0;
			name = 0;
		};
		//Copy Constructor 
		MEMBER(const MEMBER &member);
		
		//Overloaded Assignment Operator 
		MEMBER& operator= (const MEMBER &member);

		//Destructor
		~MEMBER();
	private:
		//AssignMember is used to assign all the attributes of MEMBER
		void AssignMember(const MEMBER &member);

	};

	/////
	// class MEMBER_LIST	Instances of MEMBER_LIST contain a list of MEMBER
	//						Used in FLAT_XXXXX
	//						Specified in 61804-3 "MEMBERS"
	//						Specified in FDI-2041 "Members List Specifier ->Members_List_Specifier"
	// members:
	//			count	Number of MEMBERs in list
	//			limit	Size of list array.
	//			list	Array of MEMBER
	// methods:
	//			constructor MEMBER_LIST()
	//						Create an instance of MEMBER_LIST with count, limit = 0 and list = nullptr
	//			constructor MEMBER_LIST (const MEMBER_LIST & member_list);
	//						Create an instance of MEMBER_LIST copying values from member_list
	//			destructor	~MEMBER_LIST()
	//						Delete contents of list before self destructing
	//			overload MEMBER_LIST& operator= (const MEMBER_LIST &member_list);
	//						Copies values from member_list;
	//			void AssignMemberList(const MEMBER_LIST &member_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of MEMBER_LIST instance.
	//
	/////
	class _INTERFACE MEMBER_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		MEMBER         *list;

		//Constructor
		MEMBER_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		};

		//Copy Constructor 
		MEMBER_LIST(const MEMBER_LIST &member_list);
		
		//Overloaded Assignment Operator 
		MEMBER_LIST& operator= (const MEMBER_LIST &member_list);

		//Destructor
		~MEMBER_LIST();
	private:
		//AssignMemberList is used to assign all the attributes of list MEMBER_LIST
		void AssignMemberList(const MEMBER_LIST &member_list);
	};

	/////
	// class  OP_MEMBER 	Instances of  OP_MEMBER contain OP_REF_TRAIL
	//						Used in FLAT_XXXXX
	//
	//						
	// members:
	//			OpMemberEvaled  evaled;
	//			unsigned long   name;
	//			OP_REF_TRAIL    ref;
	//			STRING          desc;
	//			STRING          help;
	// methods:
	//			constructor OP_MEMBER()
	//						Create an instance of OP_MEMBER with evaled,name = 0
	//			constructor OP_MEMBER(const OP_MEMBER &op_member)
	//						Create an instance of OP_MEMBER copying values from op_member
	//			destructor	~OP_MEMBER()
	//						Delete contents of list before self destructing
	//			overload OP_MEMBER& operator= (const OP_MEMBER &op_member)
	//						Copies values from op_member;
	//			void AssignOpMember(const OP_MEMBER &op_member)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_MEMBER instance.
	//
	/////

	class _INTERFACE OP_MEMBER {
	public:
		enum OpMemberEvaled
		{
			FDI_OP_MEM_DESC_EVALED		= 0x01,
			FDI_OP_MEM_HELP_EVALED		= 0x02,
			FDI_OP_MEM_NAME_EVALED		= 0x04,
			FDI_OP_MEM_REF_EVALED		= 0x08
		};

		//Members
		OpMemberEvaled  evaled;
		unsigned long   name;
		OP_REF_TRAIL    ref;
		STRING          desc;
		STRING          help;

		//Constructor
		OP_MEMBER()
		{
			evaled = (OpMemberEvaled)0;
			name = 0;
			// ref has own constructor
		};
		//Copy Constructor 
		OP_MEMBER(const OP_MEMBER &op_member);
		
		//Overloaded Assignment Operator 
		OP_MEMBER& operator= (const OP_MEMBER &op_member);

		//Destructor
		~OP_MEMBER();
	private:
		//AssignMember is used to assign all the attributes of MEMBER
		void AssignOpMember(const OP_MEMBER &op_member);

	};


	/////
	// class  OP_MEMBER_LIST 	Instances of OP_MEMBER_LIST contain a list of OP_MEMBER
	//							Used in FLAT_XXXXX
	//
	//						
	// members:
	//			unsigned short  count;
	//			unsigned short  limit;
	//			OP_MEMBER       *list;
	// methods:
	//			constructor OP_MEMBER_LIST()
	//						Create an instance of OP_MEMBER_LIST with count, limit = 0 and list = nullptr
	//			constructor OP_MEMBER_LIST(const OP_MEMBER_LIST &op_member_list)
	//						Create an instance of OP_MEMBER_LIST copying values from op_member_list
	//			destructor	~OP_MEMBER_LIST()
	//						Delete contents of list before self destructing
	//			overload OP_MEMBER_LIST& operator= (const OP_MEMBER_LIST &op_member_list)
	//						Copies values from op_member;
	//			void AssignOpMemberList(const OP_MEMBER_LIST &member_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_MEMBER_LIST instance.
	//
	/////
	class _INTERFACE OP_MEMBER_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		OP_MEMBER       *list;

		//Constructor
		OP_MEMBER_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		};

		//Copy Constructor 
		OP_MEMBER_LIST(const OP_MEMBER_LIST &op_member_list);
		
		//Overloaded Assignment Operator 
		OP_MEMBER_LIST& operator= (const OP_MEMBER_LIST &op_member_list);

		//Destructor
		~OP_MEMBER_LIST();
	private:
		//AssignMemberList is used to assign all the attributes of list MEMBER_LIST
		void AssignOpMemberList(const OP_MEMBER_LIST &member_list);
	};


	/*
	* Menu
	*/

	/////
	// class MENU_ITEM		Specifies on item in a menu items list. 
	//						Used in FLAT_MENU->MENU_ITEM_LIST
	//						Specified in 61804-3 "MENU_ITEMS"
	//						Specified in FDI-2041 "Menu Items Specifier->Menu_Item"
	// members:
	//			ref			A reference to a data-item instance (DESC_REF)
	//			qual		Qualifiers bit mask (See MenuItemQual for values)	
	// methods:
	//			constructor MENU_ITEM()
	//						Create an instance of MENU_ITEM
	//			constructor MENU_ITEM (const MENU_ITEM & menu_item);
	//						Create an instance of MENU_ITEM copying values from menu_item
	//			destructor ~MENU_ITEM()
	//						Delete contents of pointer members before self destructing
	//			overload MENU_ITEM& operator= (const MENU_ITEM &menu_item);
	//						Copies values from menu_item;
	//			void AssignMenuItem(const MENU_ITEM &menu_item)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of MENU_ITEM instance.
	/////				
	class _INTERFACE MENU_ITEM {
	public:
		//Members
		OP_REF_TRAIL	ref;
		MenuItemQual	qual;

		//Constructor
		MENU_ITEM()
		{
			ref.Init();
			qual = (MenuItemQual)0;
		};
		//copy constructor
		MENU_ITEM(const MENU_ITEM &menu_item);

		//Overloaded Assignment Operator 
		MENU_ITEM& operator= (const MENU_ITEM &menu_item);

		//Destructor
		~MENU_ITEM();
	private:
		//AssignMenuItem is used to assign all the attributes of MENU_ITEM
		void AssignMenuItem(const MENU_ITEM &menu_item);
				
	};

	/////
	// class MENU_ITEM_LIST	Instances of MENU_ITEM_LIST contain a list of MENU_ITEM
	//						Used in FLAT_MENU
	//						Specified in 61804-3 "MENU_ITEMS"
	//						Specified in FDI-2041 "Menu Items Specifier->Menu_Items_Specifier"
	// members:
	//			count	Number of MENU_ITEMs in list
	//			limit	Size of list array.
	//			list	Array of MENU_ITEM
	// methods:
	//			constructor MENU_ITEM_LIST()
	//						Create an instance of MENU_ITEM_LIST with count, limit = 0 and list = nullptr
	//			constructor MENU_ITEM_LIST (const MENU_ITEM_LIST & menu_item_list);
	//						Create an instance of MENU_ITEM_LIST copying values from menu_item_list
	//			destructor	~MENU_ITEM_LIST()
	//						Delete contents of list before self destructing
	//			overload MENU_ITEM_LIST& operator= (const MENU_ITEM_LIST &menu_item_list);
	//						Copies values from menu_item_list;
	//			void AssignMenuItemList(const MENU_ITEM_LIST &menu_item_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of MENU_ITEM_LIST instance.
	//
	/////
	class _INTERFACE MENU_ITEM_LIST {
	public:

		//Members
		unsigned short  count;
		unsigned short  limit;
		MENU_ITEM      *list;

		//Constructor
		MENU_ITEM_LIST()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;
		};

		//copy constructor
		MENU_ITEM_LIST(const MENU_ITEM_LIST &menu_item_list);

		//Overloaded Assignment Operator 
		MENU_ITEM_LIST& operator= (const MENU_ITEM_LIST &menu_item_list);

		//Destructor
		~MENU_ITEM_LIST();
	private:
		//AssignMenuItemList is used to assign all the attributes of list MENU_ITEM_LIST
		void AssignMenuItemList(const MENU_ITEM_LIST &menu_item_list);

	};

	/*
	* Response Codes
	*/

	/////
	// class RESPONSE_CODE	Specifies on item in a response code list. 
	//						Used in FLAT_RESP_CODE->RESPONSE_CODE_LIST
	//						Used in FLAT_ARRAY, FLAT_COMMAND, FLAT_RECORD ->RESPONSE_CODES->RESPONSE_CODES_VALUE->RESPONSE_CODE_LIST
	//						Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->RESPONSE_CODES->RESPONSE_CODES_VALUE->RESPONSE_CODE_LIST
	//						Specified in 61804-3 "RESPONSE_CODES"
	//						Specified in FDI-2041 "Response Codes Specifier->Response_Code"
	// members:
	//			evaled		Binary flags indicating if particular members have been evaluated
	//						See the definition of ResponseCodeEvaled for the bit definitions.
	//			val			Value of the response codes
	//			desc		A short description for the response code (STRING) (set if evaled contains FDI_MEM_DESC_EVALED)	
	//			help		Help text for the response code (STRING) (set if evaled contains FDI_MEM_HELP_EVALED)
	// methods:
	//			constructor RESPONSE_CODE()
	//						Create an instance of RESPONSE_CODE with all values at zero
	//			constructor RESPONSE_CODE (const RESPONSE_CODE & menu_item);
	//						Create an instance of RESPONSE_CODE copying values from menu_item
	//			destructor ~RESPONSE_CODE()
	//						Delete contents of pointer members before self destructing
	//			overload RESPONSE_CODE& operator= (const RESPONSE_CODE &menu_item);
	//						Copies values from menu_item
	//			void AssignResponseCode(const RESPONSE_CODE &response_code)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RESPONSE_CODE instance.
	/////					
	class _INTERFACE RESPONSE_CODE {
	public:

		/* The masks for RESPONSE_CODE */
		enum ResponseCodeEvaled
		{
			FDI_RS_DESC_EVALED	= 0x01,
			FDI_RS_HELP_EVALED	= 0x02,
			FDI_RS_TYPE_EVALED	= 0x04,
			FDI_RS_VAL_EVALED	= 0x08
		};

		//Members
		ResponseCodeEvaled  evaled;
		unsigned long   val;
		ResponseCodeType type;
		STRING          desc;
		STRING          help;

		//Constructor
		RESPONSE_CODE()
		{
			evaled	= (ResponseCodeEvaled)0;
			val		= 0;
			type	= (ResponseCodeType)0;

		};

		//copy constructor
		RESPONSE_CODE(const RESPONSE_CODE &response_code);
		//Overloaded Assignment Operator 
		RESPONSE_CODE& operator= (const RESPONSE_CODE &response_code);

		//Destructor
		~RESPONSE_CODE();
	private:
		//AssignResponseCode is used to assign all the attributes of RESPONSE_CODE
		void AssignResponseCode(const RESPONSE_CODE &response_code);

	};

	/////
	// class RESPONSE_CODE_LIST	Instances of RESPONSE_CODE_LIST contain a list of RESPONSE_CODE
	//							Used in FLAT_RESP_CODE
	//							Used in FLAT_ARRAY, FLAT_COMMAND, FLAT_RECORD ->RESPONSE_CODES->RESPONSE_CODES_VALUE
	//							Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->RESPONSE_CODES->RESPONSE_CODES_VALUE
	//							Specified in 61804-3 "RESPONSE_CODES"
	//							Specified in FDI-2041 "Response Codes Specifier->Response_Codes_List_Specifier"
	// members:
	//			count	Number of RESPONSE_CODEs in list
	//			limit	Size of list array.
	//			list	Array of RESPONSE_CODE
	// methods:
	//			constructor RESPONSE_CODE_LIST()
	//						Create an instance of RESPONSE_CODE_LIST with count, limit = 0 and list = nullptr	
	//			constructor RESPONSE_CODE_LIST (const RESPONSE_CODE_LIST & response_code_list);
	//						Create an instance of RESPONSE_CODE_LIST copying values from response_code_list
	//			destructor	~RESPONSE_CODE_LIST()
	//						Delete contents of list before self destructing
	//			overload RESPONSE_CODE_LIST& operator= (const RESPONSE_CODE_LIST &response_code_list);
	//						Copies values from response_code_list;
	//			init()
	//						Sets count, limit = 0 and list = nullptr
	//			Dispose()
	//						Frees list and sets count, limit = 0 and list = nullptr
	//			void AssignResponseCodeList(const RESPONSE_CODE_LIST& original_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RESPONSE_CODE_LIST instance.
	/////
	class _INTERFACE RESPONSE_CODE_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		RESPONSE_CODE  *list;

		//Constructor
		RESPONSE_CODE_LIST()
		{
			Init();
		}

		//copy constructor
		RESPONSE_CODE_LIST(const RESPONSE_CODE_LIST &response_code_list);
		//Overloaded Assignment Operator 
		RESPONSE_CODE_LIST& operator= (const RESPONSE_CODE_LIST &response_code_list);

		//Destructor
		~RESPONSE_CODE_LIST()
		{
			Dispose();
		}

		void Init()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;	
		}
		void Dispose();
	private:
		//AssignResponseCodeList is used to assign all the attributes of list RESPONSE_CODE_LIST
		void AssignResponseCodeList(const RESPONSE_CODE_LIST& original_list);

	};

	/////
	// class RESPONSE_CODES_VALUE	Provides the value, either reference or list, for RESPONSE_CODES
	//								Used in FLAT_ARRAY, FLAT_COMMAND, FLAT_RECORD ->RESPONSE_CODES
	//								Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->RESPONSE_CODES
	//								Specified in 61804-3 "RESPONSE_CODES"
	//								Specified in FDI-2041 "Response Codes Specifier->Response_Codes_Specifier"
	// members:
	//						Applicability of resp_code_ref or resp_code_list is dependent on setting of
	//						eType in RESPONSE_CODES
	//			resp_code_ref		A reference to a RESPONSE_CODES instance
	//			resp_code_list		List of response codes
	// methods:
	//			constructor RESPONSE_CODES_VALUE()
	//						Create an instance of RESPONSE_CODES_VALUE with ref at zero
	//			constructor RESPONSE_CODES_VALUE (const RESPONSE_CODES_VALUE & response_code_value);
	//						Create an instance of RESPONSE_CODES_VALUE copying values from response_code_value
	//			overload RESPONSE_CODES_VALUE& operator= (const RESPONSE_CODES_VALUE &response_code_value);
	//						Copies values response_code_value
	//			void AssignResponseCodeValue(const RESPONSE_CODES_VALUE &response_code_value)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RESPONSE_CODES_VALUE instance.
	/////				
	class _INTERFACE RESPONSE_CODES_VALUE
	{
	public:
		//Members
		ITEM_ID				resp_code_ref;
		RESPONSE_CODE_LIST	resp_code_list;

		//Constructor
		RESPONSE_CODES_VALUE()
		{
			resp_code_ref=0;
		}

		//Copy Constructor 
		RESPONSE_CODES_VALUE(const RESPONSE_CODES_VALUE &response_code_value);
		//Overloaded Assignment Operator 
		RESPONSE_CODES_VALUE& operator= (const RESPONSE_CODES_VALUE &response_code_value);

	private:
		//AssignResponseCodeValue is used to assign all the attributes of RESPONSE_CODES_VALUE
		void AssignResponseCodeValue(const RESPONSE_CODES_VALUE &response_code_value);

	};

	/////
	// class RESPONSE_CODES	Specifies Either a response code reference or list. 
	//						Used in FLAT_ARRAY, FLAT_COMMAND, FLAT_RECORD
	//						Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION
	//						Specified in 61804-3 "RESPONSE_CODES"
	//						Specified in FDI-2041 "Response Codes Specifier->Response_Codes_Specifier"
	// members:
	//			eType			Enumeration set to one of RESP_CODE_TYPE_NONE, RESP_CODE_TYPE_REF, or RESP_CODE_TYPE_LIST
	//			response_codes	Instance of RESPONSE_CODES_VALUE containing RESPONSE_CODES as determined by eType
	// methods:
	//			constructor RESPONSE_CODES()
	//						Create an instance of RESPONSE_CODES with eType set to RESP_CODE_TYPE_NONE
	//			constructor RESPONSE_CODES (const RESPONSE_CODES & responsecodes);
	//						Create an instance of RESPONSE_CODES copying values from responsecodes
	//			overload RESPONSE_CODES& operator= (const RESPONSE_CODES &responsecodes);
	//						Copies values from responsecodes;
	//			void AssignResponseCodes(const RESPONSE_CODES &responsecode_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of RESPONSE_CODES instance.
	/////					
	class _INTERFACE RESPONSE_CODES {
	public:
		// RESPONSE_CODES stores either
		//	referenced or embedded response code definitions.
		enum ResponseCodesType {
			RESP_CODE_TYPE_NONE = 0,
			RESP_CODE_TYPE_REF  = 1,
			RESP_CODE_TYPE_LIST = 2
		};

		//Members
		ResponseCodesType eType;

		RESPONSE_CODES_VALUE response_codes;

		//Constructor
		RESPONSE_CODES()
		{
			eType = RESP_CODE_TYPE_NONE;
			response_codes.resp_code_list.Init(); 
		};
		//Copy Constructor 
		RESPONSE_CODES(const RESPONSE_CODES &responsecodes);
		//Overloaded Assignment Operator 
		RESPONSE_CODES& operator= (const RESPONSE_CODES &responsecodes);

		//Destructor
		~RESPONSE_CODES();
	private:
		//AssignResponseCodes is used to assign all the attributes of RESPONSE_CODES
		void AssignResponseCodes(const RESPONSE_CODES &responsecode_list);
	};

	/*
	* Definitions
	*/
	/////
	// class DEFINITION		Specifies the ASCII string that defines the method (a UTF-8 string) 
	//						Used in FLAT_METHOD
	//						FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->ACTION_LIST->ACTION->ACTION_VALUE
	//						Specified in 61804-3 "Common Attributes->DEFINITION"
	//						Specified in FDI-2041 "Method Definition->Method_Definition"
	// members:
	//			size;		Number of bytes in data.
	//			*data;		Data Buffer containing the definition in UTF-8
	// methods:
	//			constructor DEFINITION()
	//						Create an instance of DEFINITION with size = 0 and data = nullptr
	//			constructor DEFINITION (const DEFINITION & responsecodes);
	//						Create an instance of DEFINITION copying values from responsecodes
	//			destructor	~DEFINITION()
	//						Delete contents of pointer members before self destructing
	//			overload DEFINITION& operator= (const RESPONSE_CODES &responsecodes);
	//						Copies values from responsecodes;
	//			init()
	//						Sets size = 0 and data = nullptr
	//			Dispose()
	//						Delete contents of pointer members and set size = 0 and data = nullptr
	//			void AssignDefination(const DEFINITION &definition)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of DEFINITION instance.
	/////					
	class _INTERFACE DEFINITION {
	public:
		unsigned long   size;
		wchar_t        *data;

		//Constructor
		DEFINITION()
		{
			Init();
		}

		//Destructor
		~DEFINITION()
		{
			Dispose();
		}

		void Init()
		{
			size = 0;
			data = nullptr;
		}
		//Copy Constructor 
		DEFINITION(const DEFINITION &definition);
		//Overloaded Assignment Operator 
		DEFINITION& operator= (const DEFINITION &definition);
		void Dispose();
	private:
		//AssignDefination is used to assign all the attributes of DEFINITION
		void AssignDefination(const DEFINITION &definition);
	};


	/////
	// class ACTION_VALUE	Specifies the method call information to be used in ACTIONS
	//						Used in FLAT_XXXX->ACTION_LIST->ACTION
	//						FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->ACTION_LIST->ACTION
	//						Specified in 61804-3 all sections with actions specified as Attributes
	//						Specified in FDI-2041 "Action List Specifier->Action_List_Specifier"
	// members:
	//			meth_ref		Method Reference
	//			meth_ref_args	Method Arguments (in the form of an OP_REF_TRAIL)
	//			meth_definition	Method Type Definition.
	// methods:
	//			constructor ACTION_VALUE()
	//						Create an instance of ACTION_VALUE
	//			constructor ACTION_VALUE (const ACTION_VALUE & action_value);
	//						Create an instance of ACTION copying values from action_value
	//			destructor ~ACTION_VALUE()
	//						Delete contents of pointer members before self destructing
	//			overload ACTION_VALUE& operator= (const ACTION_VALUE &action_value);
	//						Copies values from action_value;
	//			void AssignActionValue(const ACTION_VALUE &action_value)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ACTION_VALUE instance.
	/////				
	class _INTERFACE ACTION_VALUE
	{
	public:
		ITEM_ID			meth_ref;		// ACTION_TYPE_REFERENCE
		OP_REF_TRAIL	meth_ref_args;	// ACTION_TYPE_REF_WITH_ARGS
		DEFINITION		meth_definition;// ACTION_TYPE_DEFINITION

		ACTION_VALUE()
		{
			meth_ref=0;
		}

		//Copy Constructor 
		ACTION_VALUE(const ACTION_VALUE &action_value);
		//Overloaded Assignment Operator 
		ACTION_VALUE& operator= (const ACTION_VALUE &action_value);

		~ACTION_VALUE()
		{
		}

	private:
		//AssignActionValue is used to assign all the attributes of ACTION_VALUE
		void AssignActionValue(const ACTION_VALUE &action_value);

	};

	/*
	* Actions
	*/
	/////
	// class ACTION			Specifies A method to use in the ACTION_LIST 
	//						Used in FLAT_XXXX->ACTION_LIST
	//						FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->ACTION_LIST
	//						Specified in 61804-3 all sections with actions specified as Attributes
	//						Specified in FDI-2041 "Action List Specifier->Action_List_Specifier"
	// members:
	//			eType			Enumeration set to one of ACTION_TYPE_NONE, ACTION_TYPE_REFERENCE,
	//							ACTION_TYPE_REF_WITH_ARGS or ACTION_TYPE_DEFINITION
	//			action			Instance of ACTION_VALUE
	// methods:
	//			constructor ACTION()
	//						Create an instance of ACTION
	//			constructor ACTION (const ACTION & action_or);
	//						Create an instance of ACTION copying values from action_or
	//			destructor ~ACTION()
	//						Delete contents of pointer members before self destructing
	//			overload ACTION& operator= (const ACTION &action_or);
	//						Copies values from action_or;
	//			void AssignAction(const ACTION &action_or)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ACTION instance.
	/////					
	class _INTERFACE ACTION {
	public:

		enum ActionType {
			ACTION_TYPE_NONE			= 0,
			ACTION_TYPE_REFERENCE		= 1,
			ACTION_TYPE_REF_WITH_ARGS	= 2,
			ACTION_TYPE_DEFINITION		= 3
		};

		//Members
		ActionType eType;

		ACTION_VALUE action;

		//Constructor
		ACTION() {};
		//Copy Constructor 
		ACTION(const ACTION &action_or);
		//Overloaded Assignment Operator 
		ACTION& operator= (const ACTION &action_or);

		//Destructor
		~ACTION() {};
	private:
		//AssignAction is used to assign all the attributes of ACTION
		void AssignAction(const ACTION &action_or);

	};

	/////
	// class ACTION_LIST	Instances of ACTION_LIST contain a list of ACTION
	//						Used in FLAT_XXXX
	//						FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION
	//						Specified in 61804-3 all sections with actions specified as Attributes
	//						Specified in FDI-2041 "Action List Specifier->Action_List_Specifier"
	// members:
	//			count	Number of RESPONSE_CODEs in list
	//			limit	Size of list array.
	//			list	Array of RESPONSE_CODE
	// methods:
	//			constructor ACTION_LIST()
	//						Create an instance of ACTION_LIST with count, limit = 0 and list = nullptr	
	//			constructor ACTION_LIST (const ACTION_LIST & action_list);
	//						Create an instance of ACTION_LIST copying values from action_list
	//			destructor	~ACTION_LIST()
	//						Delete contents of list before self destructing
	//			overload ACTION_LIST& operator= (const ACTION_LIST &action_list);
	//						Copies values from action_list;
	//			init()
	//						Sets count, limit = 0 and list = nullptr
	//			Dispose()
	//						Frees list and sets count, limit = 0 and list = nullptr
	//			void AssignActionList(const ACTION_LIST &action_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ACTION_LIST instance.
	/////
	//
	class _INTERFACE ACTION_LIST {
	public:
		//Memebers
		unsigned short	count;
		unsigned short	limit;
		ACTION			*list;

		//Constructor
		ACTION_LIST() 
		{
			count = 0;
			limit = 0;
			list  = nullptr;
 		};

		//Copy Constructor 
		ACTION_LIST(const ACTION_LIST &action_list);
		//Overloaded Assignment Operator 
		ACTION_LIST& operator= (const ACTION_LIST &action_list);

		//Destructor
		~ACTION_LIST();
	private:
		//AssignActionList is used to assign all the attributes of list ACTION_LIST
		void AssignActionList(const ACTION_LIST &action_list);

	};

	/*
	* Variable Types
	*/
	/////
	// class TYPE_SIZE		Specifies a variable's type and size.  It is a combination of the the two
	//						elements in FDI-2041 "Variable Type "
	//						Used in FLAT_VAR
	//						Specified in 61804-3 "VARIABLE->TYPE"
	//						Specified in FDI-2041 "Variable Type->Variable_type" and "Variable Type->Type_Size"
	// members:
	//			Type		Enumeration set to one of the values of Variable Type
	//			size		Size in bytes of the variable
	// methods:
	//			constructor TYPE_SIZE()
	//						Create an instance of TYPE_SIZE
	//			constructor TYPE_SIZE (const TYPE_SIZE & type_size);
	//						Create an instance of ACTION copying values from type_size
	//			destructor ~TYPE_SIZE()
	//						Delete contents of pointer members before self destructing
	//			overload TYPE_SIZE& operator= (const TYPE_SIZE &type_size);
	//						Copies values from type_size;
	//			
	/////
	class _INTERFACE TYPE_SIZE {
	public:
		//Members
		VariableType	type;
		unsigned int	size;

		//Constructor
		TYPE_SIZE()
		{
			type = VT_DDS_TYPE_UNUSED;
			size = 0;
		}

		//Destructor
		~TYPE_SIZE() {};

		//Copy Constructor 
		TYPE_SIZE(const TYPE_SIZE &type_size)
		{
			type = type_size.type;
			size = type_size.size;
			
		}
		//Overloaded Assignment Operator 
		TYPE_SIZE& operator= (const TYPE_SIZE &type_size)
		{
			type = type_size.type;
			size = type_size.size;
			return *this;
		}
	};


	/*
	* Enumerations
	*/
	/////
	// class STATUS_OUTPUT_CLASS	This class Provides the contents of the Output_Class
	//								It is included in STATUS_CLASS
	//								Named STATUS_OUTPUT_CLASS to avoid problems with a #define OUTPUT_CLASS
	//								Used in FLAT_VAR->ENUM_VALUE_LIST->ENUM_VALUE->ENUM_STATUS_CLASS_LIST->STATUS_CLASS_ELEM
	//								Specified in 61804-3 "VARIABLE->TYPE->ENUMERATED"
	//								Specified in FDI-2041 "Enumerations Specifier->Output_Class"
	// members:
	//			mode_and_reliability	mode_and_reliability
	//			which_output			which_output
	// methods:
	//			constructor STATUS_OUTPUT_CLASS()
	//						Create an instance of STATUS_OUTPUT_CLASS with mode_and_reliability set to FDI_MR_UNUSED
	//			destructor ~STATUS_OUTPUT_CLASS()
	//						Delete contents of pointer members before self destructing
	/////
    class _INTERFACE STATUS_OUTPUT_CLASS {
    public:
		//Members
        ModeAndReliability  mode_and_reliability;
		unsigned short  which_output;

		//Constructor
        STATUS_OUTPUT_CLASS()
        {
            mode_and_reliability = FDI_MR_UNUSED;
            which_output = 0;
        }

		//Destructor
		~STATUS_OUTPUT_CLASS() {};
    };

   
	/////
	// class STATUS_CLASS_ELEM	This class provides the contents of the of Status_Class sequence
	//							It is included in STATUS_CLASS_LIST
	//							Used in FLAT_VAR->ENUM_VALUE_LIST->ENUM_VALUE->ENUM_STATUS_CLASS_LIST
	//							Specified in 61804-3 "VARIABLE->TYPE->ENUMERATED"
	//							Specified in FDI-2041 "Enumerations Specifier->Status_Class"
	// members:
    //      base_class		One of type BaseClass defining the type of status this represents
    //      output_class	Defines the details of the status
	// methods:
	//			constructor STATUS_CLASS_ELEM()
	//						Create an instance of STATUS_CLASS_ELEM with base_class set to FDI_UNDEFINED_STATUS
	//			destructor ~STATUS_CLASS_ELEM()
	//						Delete contents of pointer members before self destructing
	/////
	class _INTERFACE STATUS_CLASS_ELEM {
	public:
		//Members
        BaseClass      base_class;     
		STATUS_OUTPUT_CLASS   output_class;

		//Constructor
        STATUS_CLASS_ELEM()
		{
			base_class	= FDI_UNDEFINED_STATUS;
            // output_class had its own initializer.
		}

		//Destructor
		~STATUS_CLASS_ELEM() {};
	};

   
	/////
	// class STATUS_CLASS_LIST	Instances of STATUS_CLASS_LIST contain a list of STATUS_CLASS_ELEM
	//							This class provides the sequence part of Status_Class sequence
	// 							It includes zero or more instances of STATUS_CLASS
	//							Used in FLAT_VAR->ENUM_VALUE_LIST->ENUM_VALUE
	//							Specified in 61804-3 "VARIABLE->TYPE->ENUMERATED"
	//							Specified in FDI-2041 "Enumerations Specifier->Status_class"
	// members:
	//			count	Number of RESPONSE_CODEs in list
	//			limit	Size of list array.
	//			list	Array of STATUS_CLASS_ELEM
	// methods:
	//			constructor STATUS_CLASS_LIST()
	//						Create an instance of STATUS_CLASS_LIST with count, limit = 0 and list = nullptr	
	//			constructor STATUS_CLASS_LIST (const ACTION_LIST & status_class);
	//						Create an instance of STATUS_CLASS_LIST copying values from status_class
	//			destructor	~STATUS_CLASS_LIST()
	//						Delete contents of list before self destructing
	//			overload STATUS_CLASS_LIST& operator= (const STATUS_CLASS_LIST &status_class);
	//						Copies values from status_class;
	//			void AssignStatusClassList(const STATUS_CLASS_LIST &status_class)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of STATUS_CLASS_LIST instance.
	/////
	class _INTERFACE STATUS_CLASS_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		STATUS_CLASS_ELEM  *list;

		//Constructor
		STATUS_CLASS_LIST()
		{
			count = 0;
			limit = 0;
			list  = nullptr;
		}

		//copy constructor
		STATUS_CLASS_LIST(const STATUS_CLASS_LIST &status_class);

		//Overloaded Assignment Operator 
		STATUS_CLASS_LIST& operator= (const STATUS_CLASS_LIST &status_class);

		//Destructor
		~STATUS_CLASS_LIST();
	private:
		//AssignStatusClassList is used to assign all the attributes of STATUS_CLASS
		void AssignStatusClassList(const STATUS_CLASS_LIST &status_class);
	};


	/////
	// class ENUM_VALUE	This class provides contents of the Enumeration Sequence
	//						 Used in FLAT_VAR->ENUM_VALUE_LIST
	//						 Specified in 61804-3 "VARIABLE->TYPE->ENUMERATED"
	//						 Specified in FDI-2041 "Enumerations Specifier->Enumeration"
	// members:
	//		evaled			Bit field containing flags indicating which members have been evaluated
	//						and are valid.  Bit fields are as defined in EnumValueEvaled
    //      val				A constant that specifies the VARIABLE value (long) if EnumValueEvaled == FDI_ENUM_VAL_EVALED
    //      desc			Description of the enumeration (STRING) if EnumValueEvaled == FDI_ENUM_DESC_EVALED
    //      help			Help text for the enumeration (STRING) if EnumValueEvaled == FDI_ENUM_HELP_EVALED
    //      status			Status (STATUS_CLASS_LIST) if EnumValueEvaled == FDI_ENUM_STATUS_EVALED
    //      actions			Actions (ITEM_ID) if EnumValueEvaluated == FDI_ENUM_ACTIONS_EVALED
    //      func_class		Functional Class (ClassType) if EnumValueEvaluated == FDI_ENUM_CLASS_EVALED
	// methods:
	//			constructor ENUM_VALUE()
	//						Create an instance of ENUM_VALUE with all members set to initial values
	//			constructor ENUM_VALUE (const ENUM_VALUE & Enum_Value);
	//						Create an instance of ENUM_VALUE copying values from Enum_Value
	//			destructor	~ENUM_VALUE()
	//						Delete contents of pointer members before self destructing
	//			overload ENUM_VALUE& operator= (const ENUM_VALUE &Enum_Value);
	//						Copies values from Enum_Value;
	//			void AssignEnumValue(const ENUM_VALUE &Enum_Value)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ENUM_VALUE instance.
	/////
	class _INTERFACE ENUM_VALUE {
	public:

		/* The masks for ENUM_VALUE */
		enum EnumValueEvaled
		{
			FDI_ENUM_ACTIONS_EVALED	= 0x0001,
			FDI_ENUM_CLASS_EVALED	= 0x0002,
			FDI_ENUM_DESC_EVALED	= 0x0004,
			FDI_ENUM_HELP_EVALED	= 0x0008,
			FDI_ENUM_STATUS_EVALED	= 0x0010,
			FDI_ENUM_VAL_EVALED		= 0x0020
		};

		EnumValueEvaled         evaled;

		//Members
		EXPR			 val;
		STRING           desc;
		STRING           help;
        STATUS_CLASS_LIST status;
		ClassType		 func_class;	/* functional class */
		ITEM_ID          actions;

		//Constructor
		ENUM_VALUE()
		{
			evaled	= (EnumValueEvaled)0;
			//val		EXPR has own constructor
			func_class = CT_NONE;
			actions = 0;
		}

		//copy constructor
		ENUM_VALUE(const ENUM_VALUE &Enum_Value);
		//Overloaded Assignment Operator 
		ENUM_VALUE& operator= (const ENUM_VALUE &Enum_Value);

		//Destructor
		~ENUM_VALUE();

	private:
		//AssignEnumValue is used to assign all the attributes of ENUM_VALUE
		void AssignEnumValue(const ENUM_VALUE &Enum_Value);
		

	};



	/////
	// class ENUM_VALUE_LIST	Instances of ENUM_VALUE_LIST contain a list of ENUM_VALUE
	//							This class provides the sequence part of ENUMERATION sequence
	//							Used in FLAT_VAR
	//							It includes zero or more instances of ENUM_VALUE
	//							Specified in 61804-3 "VARIABLE->TYPE->ENUMERATED"
	//							Specified in FDI-2041 "Enumerations Specifier->Enumeration"
	// members:
	//			count	Number of RESPONSE_CODEs in list
	//			limit	Size of list array.
	//			list	Array of ENUM_VALUE
	// methods:
	//			constructor ENUM_VALUE_LIST()
	//						Create an instance of ENUM_VALUE_LIST with count, limit = 0 and list = nullptr	
	//			constructor ENUM_VALUE_LIST (const ACTION_LIST & enum_value_list);
	//						Create an instance of ENUM_VALUE_LIST copying values from enum_value_list
	//			destructor	~ENUM_VALUE_LIST()
	//						Delete contents of list before self destructing
	//			overload ENUM_VALUE_LIST& operator= (const ENUM_VALUE_LIST &enum_value_list);
	//						Copies values from enum_value_list;
	//			void AssignEnumValueList(const ENUM_VALUE_LIST &enum_value_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of ENUM_VALUE_LIST instance.
	//
	/////
	class _INTERFACE ENUM_VALUE_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		ENUM_VALUE     *list;

		//Constructor
		ENUM_VALUE_LIST()
		{
			count = 0;
			limit = 0;
			list  = nullptr;
		}

		//Destructor
		~ENUM_VALUE_LIST();

		//copy constructor
		ENUM_VALUE_LIST(const ENUM_VALUE_LIST &enum_value_list);
		//Overloaded Assignment Operator 
		ENUM_VALUE_LIST& operator= (const ENUM_VALUE_LIST &enum_value_list);

	private:
		//AssignEnumValueList is used to assign all the attributes of list ENUM_VALUE_LIST
		void AssignEnumValueList(const ENUM_VALUE_LIST &enum_value_list);
	};

	/*
	* Data Fields
	*/
	/////
	// class DATA_ITEM_VALUE	This class provides contents of a data item
	//							Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->DATA_ITEM_LIST->DATA_ITEM
	//							FLAT_WAVEFORM->DATA_ITEM_LIST->DATA_ITEM
	//							Specified in 61804-3 "WAVEFORM->Specific attributes" and "COMMAND->Specific attributes->TRANSACTION"
	//							Specified in FDI-2041 "Data Field Specifier->Data_Element"
	// members:
	//			ref			Storage for the OP_REF_TRAIL used to get to this value
	//			iconst		Storage for Value if integer
	//			fconst		Storage for Value if float
	//			dconst		Storage for Value if double
	//			uconst		Storage for Value if Unsigned Integer
	// methods:
	//			constructor DATA_ITEM_VALUE()
	//						Create an instance of DATA_ITEM_VALUE with all members set to initial values
	//			constructor DATA_ITEM_VALUE (const DATA_ITEM_VALUE & Data_Item_Value);
	//						Create an instance of DATA_ITEM_VALUE copying values from Data_Item_Value
	//			destructor	~DATA_ITEM_VALUE()
	//						Delete contents of pointer members before self destructing
	//			overload DATA_ITEM_VALUE& operator= (const DATA_ITEM_VALUE &Data_Item_Value);
	//						Copies values from Data_Item_Value;
	//			void AssignDataItemValue(const DATA_ITEM_VALUE &data_item_value)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of DATA_ITEM_VALUE instance.
	//
	/////
	class _INTERFACE DATA_ITEM_VALUE {
	public:
		//Members
		long			iconst;  // FDI_DATA_CONSTANT
		OP_REF_TRAIL    ref;	 // FDI_DATA_REFERENCE
		float			fconst;	 // FDI_DATA_FLOATING
		double			dconst;  // FDI_DATA_DOUBLE
		unsigned long	uconst;  // FDI_DATA_UNSIGNED

		//Constructor
		DATA_ITEM_VALUE()
		{
			iconst = 0;
			fconst = 0;
			uconst = 0;
			dconst = 0;
		}
		//copy constructor
		DATA_ITEM_VALUE(const DATA_ITEM_VALUE &Data_Item_Value);
		//Overloaded Assignment Operator 
		DATA_ITEM_VALUE& operator= (const DATA_ITEM_VALUE &data_item_value);

		//Destructor
		~DATA_ITEM_VALUE() {};

	private:
		//AssignDataItemValue is used to assign all the attributes of DATA_ITEM_VALUE
		void AssignDataItemValue(const DATA_ITEM_VALUE &data_item_value);
	};

	/////
	// class DATA_ITEM		This class provides the information needed for a Data Element
	//						Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION->DATA_ITEM_LIST
	//						FLAT_WAVEFORM->DATA_ITEM_LIST
	//						Specified in 61804-3 "WAVEFORM->Specific attributes" and "COMMAND->Specific attributes->TRANSACTION"
	//						Specified in FDI-2041 "Data Field Specifier->Data_Element"
	// members:
	//			data			DATA_ITEM_VALUE containing the value for this data item
	//			type			Type of value (used in determining which member of DATA_ITEM_VALUE to use)
	//			flags			varref flags as bits in an integer
	//			width			Number of bits that this variable takes up in a communication frame
	//			data_item_mask	Is the mask which indicates where these bits are located in the byte
	// methods:
	//			constructor DATA_ITEM()
	//						Create an instance of DATA_ITEM with all members set to initial values
	//			constructor DATA_ITEM (const DATA_ITEM & data_item);
	//						Create an instance of DATA_ITEM copying values from data_item
	//			destructor	~DATA_ITEM()
	//						Delete contents of pointer members before self destructing
	//			overload DATA_ITEM& operator= (const DATA_ITEM &data_item);
	//						Copies values from data_item;
	//			void AssignDataItem(const DATA_ITEM &data_item)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of DATA_ITEM instance.
	//
	/////
	class _INTERFACE DATA_ITEM {
	public:
		//Members
		DATA_ITEM_VALUE data;
		DataItemType	type;
		unsigned short  flags;
		unsigned short  width;
		unsigned long long  data_item_mask;

		//Constructor
		DATA_ITEM()
		{
			type			= (DataItemType)0;
			flags			= 0;
			width			= 0;
			data_item_mask	= 0;
		}

		//copy constructor
		DATA_ITEM(const DATA_ITEM &data_item);
		//Overloaded Assignment Operator 
		DATA_ITEM& operator= (const DATA_ITEM &data_item);

		//Destructor
		~DATA_ITEM();
	private:
		//AssignDataItem is used to assign all the attributes of DATA_ITEM
		void AssignDataItem(const DATA_ITEM &data_item);
	};

	/////
	// class DATA_ITEM_LIST		Instances of DATA_ITEM_LIST contain a list of DATA_ITEM
	//							Used in FLAT_COMMAND->TRANSACTION_LIST->TRANSACTION
	//							FLAT_WAVEFORM
	//							Specified in 61804-3 "FLAT_WAVEFORM->DATA_ITEM_LIST" and "COMMAND->Specific attributes->TRANSACTION"
	//							Specified in FDI-2041 "Data Field Specifier->Data_Field_Specifier"
	// members:
	//			count	Number of RESPONSE_CODEs in list
	//			limit	Size of list array.
	//			list	Array of RESPONSE_CODE
	// methods:
	//			constructor DATA_ITEM_LIST()
	//						Create an instance of DATA_ITEM_LIST with count, limit = 0 and list = nullptr	
	//			constructor DATA_ITEM_LIST (const ACTION_LIST & data_item_list);
	//						Create an instance of DATA_ITEM_LIST copying values from data_item_list
	//			destructor	~DATA_ITEM_LIST()
	//						Delete contents of list before self destructing
	//			overload DATA_ITEM_LIST& operator= (const DATA_ITEM_LIST &data_item_list);
	//						Copies values from data_item_list;
	//			void AssignDataItemList(const DATA_ITEM_LIST &data_item_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of DATA_ITEM_LIST instance.
	//
	/////
	class _INTERFACE DATA_ITEM_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		DATA_ITEM      *list;

		//Constructor
		DATA_ITEM_LIST()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;
		}

		//copy constructor
		DATA_ITEM_LIST(const DATA_ITEM_LIST &data_item_list);

		//Overloaded Assignment Operator 
		DATA_ITEM_LIST& operator= (const DATA_ITEM_LIST &data_item_list);
		
		//Destructor
		~DATA_ITEM_LIST();
	private:
		//AssignDataItemList is used to assign all the attributes of list DATA_ITEM_LIST
		void AssignDataItemList(const DATA_ITEM_LIST &data_item_list);

	};
		
	/////
	// class VECTOR_ITEM_VALUE	The VECTORS attribute specifies the contents of the GRID, This provides one value
	//							Used in FLAT_GRID->VECTOR_LIST->VECTOR->VECTOR_ITEM
	//							Specified in 61804-3 "GRID->VECTORS"
	//							Specified in FDI-2041 "Reference Specifier->Reference"
	// members:
	//			long			iconst;  // FDI_DATA_CONSTANT
	//			OP_REF_TRAIL    ref;     // FDI_DATA_REFERENCE
	//			float           fconst;  // FDI_DATA_FLOATING
	//			STRING          str;	 // FDI_DATA_STRING
	//			double			dconst;	 // FDI_DATA_DOUBLE
	//			unsigned long	uconst;	 // FDI_DATA_UNSIGNED
	// methods:
	//			constructor VECTOR_ITEM_VALUE()
	//						Create an instance of VECTOR_ITEM_VALUE with all members set to initial values
	//			constructor VECTOR_ITEM_VALUE (const VECTOR_ITEM_VALUE & vector_item_value);
	//						Create an instance of VECTOR_ITEM_VALUE copying values from vector_item_value
	//			destructor	~VECTOR_ITEM_VALUE()
	//						Delete contents of pointer members before self destructing
	//			overload VECTOR_ITEM_VALUE& operator= (const VECTOR_ITEM_VALUE &vector_item_value);
	//						Copies values from vector_item_value;
	//			void AssignVectorItemValue(const VECTOR_ITEM_VALUE &vector_item_value)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of VECTOR_ITEM_VALUE instance.
	//
	/////
	class _INTERFACE VECTOR_ITEM_VALUE
	{
		public: 
		
		//Members
		long			iconst;  // FDI_DATA_CONSTANT
		OP_REF_TRAIL    ref;     // FDI_DATA_REFERENCE
		float           fconst;  // FDI_DATA_FLOATING
		STRING          str;	 // FDI_DATA_STRING
		double			dconst;	 // FDI_DATA_DOUBLE
		unsigned long	uconst;	 // FDI_DATA_UNSIGNED

		//Constructor
		VECTOR_ITEM_VALUE()
		{
			iconst=0;
			fconst=0;
		}

		//copy constructor
		VECTOR_ITEM_VALUE(const VECTOR_ITEM_VALUE &vector_item_value);
		//Overloaded Assignment Operator 
		VECTOR_ITEM_VALUE& operator= (const VECTOR_ITEM_VALUE &vector_item_value);

		//Destructor
		~VECTOR_ITEM_VALUE() {};
		private:
		//AssignVectorItemValue is used to assign all the attributes of VECTOR_ITEM_VALUE
		void AssignVectorItemValue(const VECTOR_ITEM_VALUE &vector_item_value);
	};

	/*
	* Vector Fields
	*/
	/////
	// class VECTOR_ITEM	The VECTORS attribute specifies the contents of the GRID, This provides one type and value
	//						Used in FLAT_GRID->VECTOR_LIST->VECTOR
	//						Specified in 61804-3 "GRID->VECTORS"
	//						Specified in FDI-2041 "Reference Specifier->Reference_Specifier"
	// members:
	//			vector		Value for Vector dependent upon the value of type
	//			type		Type of value stored in vector
	// methods:
	//			constructor VECTOR_ITEM()
	//						Create an instance of VECTOR_ITEM with all members set to initial values
	//			constructor VECTOR_ITEM (const VECTOR_ITEM & vector_item);
	//						Create an instance of VECTOR_ITEM copying values from vector_item
	//			destructor	~VECTOR_ITEM()
	//						Delete contents of pointer members before self destructing
	//			overload VECTOR_ITEM& operator= (const VECTOR_ITEM &vector_item);
	//						Copies values from vector_item;
	//			void AssignVectorItem(const VECTOR_ITEM &vector_item)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of VECTOR_ITEM instance.
	//
	/////
	class _INTERFACE VECTOR_ITEM {
	public:
		//Members
		VECTOR_ITEM_VALUE vector;
		DataItemType  type;

		//Constructor
		VECTOR_ITEM()
		{
			type = (DataItemType)0;
		}

		//copy constructor
		VECTOR_ITEM(const VECTOR_ITEM &vector_item);
		//Overloaded Assignment Operator 
		VECTOR_ITEM& operator= (const VECTOR_ITEM &vector_item);

		//Destructor
		~VECTOR_ITEM();

		private:
		//AssignVectorItem is used to assign all the attributes of VECTOR_ITEM
		void AssignVectorItem(const VECTOR_ITEM &vector_item);

	};

	/////
	// class VECTOR	Defines a vector with a description and a list of VECTOR_ITEMS
	//						Used in FLAT_GRID->VECTOR_LIST
	//						Specified in 61804-3 "GRID->VECTORS"
	//						Specified in FDI-2041 "Vector Specifier->Grid_Member"
	// members:
	//			count	Number of VECTORs in list
	//			limit	Size of list array.
	//			list	Array of VECTOR
	//			description String containing a readable description of the vector
	// methods:
	//			constructor VECTOR()
	//						Create an instance of VECTOR with count, limit = 0 and list = nullptr, description empty
	//			constructor VECTOR (const VECTOR & vector);
	//						Create an instance of VECTOR copying values from vector
	//			destructor	~VECTOR()
	//						Delete contents of pointer members before self destructing
	//			overload VECTOR& operator= (const VECTOR &vector);
	//						Copies values from vector;
	//			void AssignVector(const VECTOR &vector)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of VECTOR instance.
	//
	/////
	class _INTERFACE VECTOR {
	public:
		unsigned short  count;
		unsigned short  limit;
		STRING			description;
		VECTOR_ITEM      *list;

		VECTOR()
		{
			count	= 0;
			limit	= 0;
			//description.Init();
			list	= nullptr;
		}

		//copy constructor
		VECTOR(const VECTOR &vector);
		//Overloaded Assignment Operator 
		VECTOR& operator= (const VECTOR &vector);

		~VECTOR();
	private:
		//AssignVector is used to assign all the attributes of VECTOR
		void AssignVector(const VECTOR &vector);
	};

	/////
	// class VECTOR_LIST	DInstances of VECTOR_LIST contain a list of VECTOR
	//						Used in FLAT_GRID
	//						Specified in 61804-3 "GRID->VECTORS"
	//						Specified in FDI-2041 "Vector Specifier->Vector_Specifier"
	// members:
	//			count	Number of VECTORs in list
	//			limit	Size of list array.
	//			list	Array of VECTOR
	// methods:
	//			constructor VECTOR_LIST()
	//						Create an instance of VECTOR_LIST with count, limit = 0 and list = nullptr, description empty
	//			constructor VECTOR_LIST (const VECTOR_LIST & vector_list);
	//						Create an instance of VECTOR_LIST copying values from vector_list
	//			destructor	~VECTOR_LIST()
	//						Delete contents of pointer members before self destructing
	//			overload VECTOR_LIST& operator= (const VECTOR_LIST &vector_list);
	//						Copies values from vector_list;
	//			void AssignVectorList(const VECTOR_LIST &vector_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of VECTOR_LIST instance.
	/////
	class _INTERFACE VECTOR_LIST {
	public:
		unsigned short  count;
		unsigned short  limit;
		VECTOR      *vectors;

		VECTOR_LIST()
		{
			count	= 0;
			limit	=	0;
			vectors = nullptr;
		}

		//copy constructor
		VECTOR_LIST(const VECTOR_LIST &vector_list);
		//Overloaded Assignment Operator 
		VECTOR_LIST& operator= (const VECTOR_LIST &vector_list);

		~VECTOR_LIST();
	private:
		//AssignVectorList is used to assign all the attributes of list VECTOR_LIST
		void AssignVectorList(const VECTOR_LIST &vector_list);
	};

	/*
	* Transactions
	*/
	/////
	// class TRANSACTION	Transactions specify the data field of the command�s request and reply messages.
	//						Used in FLAT_COMMAND->TRANSACTION_LIST
	//						Specified in 61804-3 "COMMAND->Specific attributes->TRANSACTION"
	//						Specified in FDI-2041 "Transaction List->Transaction_"
	// members:
	//			number		Number of transactions
	//			request		The REQUEST attribute contains a list of data items which are sent to the device
	//			reply		The REPLY attribute contains a list of data items which are received from the device
	//			resp_codes	The RESPONSE_CODES attribute specifies values a device may return as error information
	//			post_actions POST_RQSTRECEIVE_ACTIONS shall only be supported by device simulators
	// methods:
	//			constructor TRANSACTION()
	//						Create an instance of TRANSACTION with all members set to initial values
	//			constructor TRANSACTION (const TRANSACTION & transaction);
	//						Create an instance of TRANSACTION copying values from transaction
	//			destructor	~TRANSACTION()
	//						Delete contents of pointer members before self destructing
	//			overload TRANSACTION& operator= (const TRANSACTION &transaction);
	//						Copies values from transaction;
	//			void AssignTransaction(const TRANSACTION &transaction)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of TRANSACTION instance.
	/////
	class _INTERFACE TRANSACTION {
	public:
		//Members
		unsigned long   number;
		DATA_ITEM_LIST  request;
		DATA_ITEM_LIST  reply;
		RESPONSE_CODES	resp_codes;
		ACTION_LIST		post_actions;

		//Constructor
		TRANSACTION() : number(0) {};
		//Copy Constructor 
		TRANSACTION(const TRANSACTION &transaction);
		//Overloaded Assignment Operator 
		TRANSACTION& operator= (const TRANSACTION &transaction);

		//Destructor
		~TRANSACTION() {};

	private:
		//AssignTransaction is used to assign all the attributes of TRANSACTION
		void AssignTransaction(const TRANSACTION &transaction);
		
	};


	/////
	// class TRANSACTION_LIST	Instances of TRANSACTION_LIST contain a list of TRANSACTION
	//							Used in FLAT_GRID
	//							Specified in 61804-3 "Transaction List->Transaction_List"
	//							Specified in FDI-2041 "COMMAND->Specific attributes->TRANSACTION"
	// members:
	//			count	Number of TRANSACTIONs in list
	//			limit	Size of list array.
	//			list	Array of TRANSACTION
	// methods:
	//			constructor TRANSACTION_LIST()
	//						Create an instance of TRANSACTION_LIST with count, limit = 0 and list = nullptr, description empty
	//			constructor TRANSACTION_LIST (const TRANSACTION_LIST & transaction_list);
	//						Create an instance of TRANSACTION_LIST copying values from transaction_list
	//			destructor	~TRANSACTION_LIST()
	//						Delete contents of pointer members before self destructing
	//			overload TRANSACTION_LIST& operator= (const TRANSACTION_LIST &transaction_list);
	//						Copies values from transaction_list;
	//			void AssignTransactionList(const TRANSACTION_LIST &transaction_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of TRANSACTION_LIST instance.
	/////
	class _INTERFACE TRANSACTION_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		TRANSACTION    *list;

		//Constructor
		TRANSACTION_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		}

		//Copy Constructor 
		TRANSACTION_LIST(const TRANSACTION_LIST &transaction_list);
		//Overloaded Assignment Operator 
		TRANSACTION_LIST& operator= (const TRANSACTION_LIST &transaction_list);

		//Destructor
		~TRANSACTION_LIST();
	private:
		//AssignTransactionList is used to assign all the attributes of list TRANSACTION_LIST
		void AssignTransactionList(const TRANSACTION_LIST &transaction_list);
	};

	/////
	// class DEFAULT_VALUE_ITEM		Transactions specify the data field of the command�s request and reply messages.
	//								Used in FLAT_TEMPLATE->DEFAULT_VALUES_LIST
	//								FLAT_COMPONENT->DEFAULT_VALUES_LIST
	//								Specified in 61804-3 "COMPONENT->Specific attributes->INITIAL_VALUES" and "TEMPLATE->DEFAULT_VALUES"
	//								Specified in FDI-2041 "Default Values List->Default_Values_List"
	// members:
	//			ref			OP_REF_TRAIL for references used in creating the value
	//			value		EXPR instance containing the value of the default
	// methods:
	//			constructor DEFAULT_VALUE_ITEM()
	//						Create an instance of DEFAULT_VALUE_ITEM with all members set to initial values
	//			constructor TRANSACTION (const DEFAULT_VALUE_ITEM & default_value_item);
	//						Create an instance of DEFAULT_VALUE_ITEM copying values from default_value_item
	//			destructor	~DEFAULT_VALUE_ITEM()
	//						Delete contents of pointer members before self destructing
	//			overload DEFAULT_VALUE_ITEM& operator= (const DEFAULT_VALUE_ITEM &default_value_item);
	//						Copies values from default_value_item;
	//			void AssignDefaultValueItem(const DEFAULT_VALUE_ITEM &default_value_item)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of  DEFAULT_VALUE_ITEM instance.
	/////
	class _INTERFACE DEFAULT_VALUE_ITEM	{
	public:
		//Members
		OP_REF_TRAIL    ref;
		EXPR			value;

		//Constructor
		DEFAULT_VALUE_ITEM()
		{
		}

		//Copy Constructor 
		DEFAULT_VALUE_ITEM(const DEFAULT_VALUE_ITEM &default_value_item);
		//Overloaded Assignment Operator 
		DEFAULT_VALUE_ITEM& operator= (const DEFAULT_VALUE_ITEM &default_value_item);

		//Destructor
		~DEFAULT_VALUE_ITEM() {};
	private:
		//AssignDefaultValueItem is used to assign all the attributes of list DEFAULT_VALUES_LIST
		void AssignDefaultValueItem(const DEFAULT_VALUE_ITEM &default_value_item);

	};

	/////
	// class DEFAULT_VALUES_LIST	Instances of DEFAULT_VALUES_LIST contain a list of DEFAULT_VALUE_ITEM
	//								Used in FLAT_TEMPLATE
	//								FLAT_COMPONENT
	//								Specified in 61804-3 "COMPONENT->Specific attributes->INITIAL_VALUES" and "TEMPLATE->DEFAULT_VALUES"
	//								Specified in FDI-2041 "Default Values List->Default_Values_List"
	// members:
	//			count	Number of DEFAULT_VALUESs in list
	//			limit	Size of list array.
	//			list	Array of DEFAULT_VALUES
	// methods:
	//			constructor DEFAULT_VALUES_LIST()
	//						Create an instance of DEFAULT_VALUES_LIST with count, limit = 0 and list = nullptr, description empty
	//			constructor DEFAULT_VALUES_LIST (const DEFAULT_VALUES_LIST & default_value_list);
	//						Create an instance of DEFAULT_VALUES_LIST copying values from default_value_list
	//			destructor	~DEFAULT_VALUES_LIST()
	//						Delete contents of pointer members before self destructing
	//			overload DEFAULT_VALUES_LIST& operator= (const DEFAULT_VALUES_LIST &default_value_list);
	//						Copies values from default_value_list;
	//			void AssignDefaultValueList(const DEFAULT_VALUES_LIST &default_value_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of DEFAULT_VALUES_LIST instance.
	//
	/////
	class _INTERFACE DEFAULT_VALUES_LIST {
	public:
		//Members
		unsigned short count;
		unsigned short limit;

		DEFAULT_VALUE_ITEM *list;

		//Constructor
		DEFAULT_VALUES_LIST()
		{
			count = 0;
			limit = 0;
			list = nullptr;
		}

		//Copy Constructor 
		DEFAULT_VALUES_LIST(const DEFAULT_VALUES_LIST &default_value_list);
		//Overloaded Assignment Operator 
		DEFAULT_VALUES_LIST& operator= (const DEFAULT_VALUES_LIST &default_value_list);

		//Destructor
		~DEFAULT_VALUES_LIST();
	private:
		//AssignDefaultValueList is used to assign all the attributes of list DEFAULT_VALUES_LIST
		void AssignDefaultValueList(const DEFAULT_VALUES_LIST &default_value_list);

	};


	/////
	// class OP_REF			A class containing OP_REF_INFO in either simple (single) or complex (list) form
	//						Used in RETURN_INFO (Flats.h)
	//						DEPBIN(Flats.h)->OP_REF_LIST
	// members:
	//			op_ref_type	Type of Op Ref (STANDARD_TYPE or COMPLEX_TYPE)
	//			op_info		single element of OP_REF_INFO (when type is STANDARD_TYPE)
	//			op_info_list	List of OP_REF_INFO (when type is COMPLEX_TYPE)
	// methods:
	//			constructor OP_REF()
	//						Create an instance of OP_REF with all members set to initial values
	//			constructor OP_REF (const OP_REF & default_value_list);
	//						Create an instance of OP_REF copying values from default_value_list
	//			destructor	~OP_REF()
	//						Delete contents of pointer members before self destructing
	//			overload OP_REF& operator= (const OP_REF &default_value_list);
	//						Copies values from default_value_list;
	//			void AssignOp_Ref(const OP_REF &op_ref)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_REF instance.
	//
	/////
	class _INTERFACE OP_REF {
	public:

		//Members
		OpRefType			op_ref_type;
		OP_REF_INFO			op_info;
		OP_REF_INFO_LIST	op_info_list;
		
		// For FF CrossBlock
		unsigned long		block_instance;	/* crossblock instance, only used if block_id is nonzero */
		
		//Constructor
		OP_REF();
		//Copy Constructor 
		OP_REF(const OP_REF &op_ref);

		//Overloaded Assignment Operator 
		OP_REF& operator= (const OP_REF &op_ref);
		OP_REF& operator= (const OP_REF_TRAIL &op_ref_trail);

		//Destructor
		~OP_REF();
	private:
		//AssignOp_Ref is used to assign all the attributes of OP_REF
		void AssignOp_Ref(const OP_REF &op_ref);
		void AssignOp_Ref(const OP_REF_TRAIL &op_ref_trail);
	};


/////
	// class OP_REF_LIST	Instances of OP_REF_LIST contain a list of OP_REF
	//						Used in DEPBIN (Flats.h)
	//						Specified in 61804-3 "COMPONENT->Specific attributes->INITIAL_VALUES" and "TEMPLATE->DEFAULT_VALUES"
	//						Specified in FDI-2041 "Default Values List->Default_Values_List"
	// members:
	//			count	Number of DEFAULT_VALUESs in list
	//			limit	Size of list array.
	//			list	Array of DEFAULT_VALUES
	// methods:
	//			constructor OP_REF_LIST()
	//						Create an instance of OP_REF_LIST with count, limit = 0 and list = nullptr, description empty
	//			constructor OP_REF_LIST (const OP_REF_LIST & op_ref_list);
	//						Create an instance of OP_REF_LIST copying values from op_ref_list
	//			destructor	~OP_REF_LIST()
	//						Delete contents of pointer members before self destructing
	//			overload OP_REF_LIST& operator= (const OP_REF_LIST &op_ref_list);
	//						Copies values from op_ref_list;
	//			void AssignOpRefList(const OP_REF_LIST &op_ref_list)
	//						Used in "Copy Constructor" and "Operator overloaded" function 
	//						to assign all the attributes of OP_REF_LIST instance.
	//
	/////
	class _INTERFACE OP_REF_LIST {
	public:
		//Members
		unsigned short  count;
		unsigned short  limit;
		OP_REF         *list;

		//Constructor
		OP_REF_LIST()
		{
			count	= 0;
			limit	= 0;
			list	= nullptr;
		}

		//Copy Constructor 
		OP_REF_LIST(const OP_REF_LIST &op_ref_list);

		//Overloaded Assignment Operator 
		OP_REF_LIST& operator= (const OP_REF_LIST &op_ref_list);

		//Destructor
		~OP_REF_LIST();
	private:
		//AssignOpRefList is used to assign all the attributes of list OP_REF_LIST
		void AssignOpRefList(const OP_REF_LIST &op_ref_list);
	};


	// Function template for |= operator overloading.
	template< class x > x& operator|= (x &bitmask, x value)
	{
		bitmask = static_cast<x> (bitmask | value);
		return bitmask;
	}

	// Function template for &= operator overloading.
	template< class x > x& operator &= (x &bitmask, x value)
	{
		bitmask = static_cast<x> (bitmask & value);
		return bitmask;
	}

	/////
	// class RESOLVED_REFERENCE	 It is used to encode complex references in the �Item to Command Table�,
	//							 �Critical Item Table�, and �Update Table�
	//							 It is an array of ItemIds, MemberIds, and Array indexes that uniquely identify a complex reference
	//							 This class is used internally in the EDD Engine
	//							 This class corresponds to the RESOLVED_REFERENCE sequence in section
	//							 �4.1.1 Standard Data Types� of the FDI-2041 spec
	// members:
	//			*pResolvedRef	Refrence item id to be resolved
	//			 size			Size of Refrence to be resolved
	// methods:
	//			constructor RESOLVED_REFERENCE()
	//						Create an instance of RESOLVED_REFERENCE 
	//			destructor	~RESOLVED_REFERENCE()
	//						Delete contents of pointer members before self destructing
	//			Init()
	//						Sets size = 0 and pResolvedRef = nullptr
	//			Dispose()
	//						Delete contents of pointer members and set size = 0 and pResolvedRef = nullptr
	//			Set( unsigned long *pRef, int len )
	//						Used to Set the value
	//			overload operator==(const RESOLVED_REFERENCE &rhs)
	//						Used when comparing two references
	//
	/////
	class _INTERFACE RESOLVED_REFERENCE
	{
	public:
		//Members
		unsigned long *pResolvedRef;
		int size;

		//Constructor
		RESOLVED_REFERENCE();
		//Destructor
		~RESOLVED_REFERENCE();

		void Init();		// Used when the constructor is not automatically called
		void Dispose();		// Used when the destructor is not automatically called

		bool operator==(const RESOLVED_REFERENCE &rhs);	// Used when comparing two references
		//Overloaded Assignment Operator 
		RESOLVED_REFERENCE& operator= (const RESOLVED_REFERENCE &resolved_ref);
		void Set( unsigned long *pRef, int len );	// Used to Set the value
	};

	//enumerations for variable attribute -- variable status
	enum VariableStatusValue
	{
		VARIABLE_STATUS_NONE			= 0,
		VARIABLE_STATUS_INVALID			= 1,
		VARIABLE_STATUS_NOT_ACCEPTED	= 2,
		VARIABLE_STATUS_NOT_SUPPORTED	= 3,
		VARIABLE_STATUS_CHANGED			= 4,
		VARIABLE_STATUS_LOADED			= 5,
		VARIABLE_STATUS_INITIAL			= 6,
		VARIABLE_STATUS_NOT_CONVERTED	= 7
	};
}

// this definition comes straight out of <guiddef.h>
#ifndef GUID_DEFINED
#define GUID_DEFINED
    typedef struct _GUID {
        unsigned long  Data1;
        unsigned short Data2;
        unsigned short Data3;
        unsigned char  Data4[ 8 ];
    } GUID;
#endif

