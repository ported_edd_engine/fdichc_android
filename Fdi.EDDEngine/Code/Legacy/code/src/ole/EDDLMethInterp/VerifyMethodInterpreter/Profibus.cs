﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VerifyMethodInterpreter
{
    [DeploymentItem("$(OutDir)\\FDIEDDEngine.dll"), TestClass]
    public class Profibus : MainWindowVMTest
    {
        private void CallMainVMTest(string strTestName, bool bRunTest)
        {
           RunOneMethodTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.PROFIBUS, strTestName, bRunTest);
        }

        private void CallActionTest(string strTestName)
        {
            RunOneActionTest((short)VerifyMethodInterpreter.MainWindowVMTest.ProtocolType.PROFIBUS, strTestName);
        }

        // common_misc_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_misc_builtins()
        {
            CallMainVMTest("99, common_misc_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_misc_builtins()
        {
            CallMainVMTest("99, common_misc_builtins", false);
        }

        // common_ui_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_ui_builtins()
        {
            CallMainVMTest("100, common_ui_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_ui_builtins()
        {
            CallMainVMTest("100, common_ui_builtins", false);
        }

        // common_math_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_math_builtins()
        {
            CallMainVMTest("101, common_math_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_math_builtins()
        {
            CallMainVMTest("101, common_math_builtins", false);
        }

        // common_string_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_string_builtins()
        {
            CallMainVMTest("103, common_string_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_string_builtinss()
        {
            CallMainVMTest("103, common_string_builtins", false);
        }

        //common_time_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_time_builtins()
        {
            CallMainVMTest("104, common_time_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_time_builtins()
        {
            CallMainVMTest("104, common_time_builtins", false);
        }

        //common_list_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void common_list_builtins()
        {
            CallMainVMTest("105, common_list_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_common_list_builtins()
        {
            CallMainVMTest("105, common_list_builtins", false);
        }

        //convert_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void convert_builtins()
        {
            CallMainVMTest("106, convert_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_convert_builtins()
        {
            CallMainVMTest("106, convert_builtins", false);
        }

        //dictionary_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void dictionary_builtins()
        {
            CallMainVMTest("107, dictionary_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_dictionary_builtins()
        {
            CallMainVMTest("107, dictionary_builtins", false);
        }

        //DDL_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void DDL_builtins()
        {
            CallMainVMTest("108, DDL_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_DDL_builtins()
        {
            CallMainVMTest("108, DDL_builtins", false);
        }

        //Internal_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void Internal_builtins()
        {
            CallMainVMTest("109, Internal_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_Internal_builtins()
        {
            CallMainVMTest("109, Internal_builtins", false);
        }

        //Communication_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void Communication_builtins()
        {
            CallMainVMTest("110, Communication_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_Communication_builtins()
        {
            CallMainVMTest("110, Communication_builtins", false);
        }

        //param_cache_builtins ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void param_cache_builtins()
        {
            CallMainVMTest("111, param_cache_builtins", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_param_cache_builtins()
        {
            CallMainVMTest("111, param_cache_builtins", false);
        }

       //method_reference_name ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_reference_name()
        {
            CallMainVMTest("116, method_reference_name", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_reference_name()
        {
            CallMainVMTest("116, method_reference_name", false);
        }

        //method_conditional_handling_test ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_conditional_handling_test()
        {
            CallMainVMTest("117, method_conditional_handling_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_conditional_handling_test()
        {
            CallMainVMTest("117, method_conditional_handling_test", false);
        }

        //method_file_member ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_file_member()
        {
            CallMainVMTest("121, method_file_member", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_file_member()
        {
            CallMainVMTest("121, method_file_member", false);
        }

        //method_set_tab_entry ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_set_tab_entry()
        {
            CallMainVMTest("122, method_set_tab_entry(3)", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_set_tab_entry()
        {
            CallMainVMTest("122, method_set_tab_entry(3)", false);
        }

        //method_enum_type_string ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_enum_type_string()
        {
            CallMainVMTest("123, method_enum_type_string", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_enum_type_string()
        {
            CallMainVMTest("123, method_enum_type_string", false);
        }

        //hyperbolic_functions ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void hyperbolic_functions()
        {
            CallMainVMTest("127, hyperbolic_functions", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_hyperbolic_functions()
        {
            CallMainVMTest("127, hyperbolic_functions", false);
        }

        //hyperbolic_functions ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_DURATION_test()
        {
            CallMainVMTest("129, method_DURATION_test", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_DURATION_test()
        {
            CallMainVMTest("129, method_DURATION_test", false);
        }

        //test_method_parameter2 ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void test_method_parameter2()
        {
            CallMainVMTest("130, test_method_parameter2", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_test_method_parameter2()
        {
            CallMainVMTest("130, test_method_parameter2", false);
        }

        //method_refresh_actions ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_refresh_actions()
        {
            CallMainVMTest("132, method_refresh_actions", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_refresh_actions()
        {
            CallMainVMTest("132, method_refresh_actions", false);
        }

        //method_calling_method_with_complex_ref ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_calling_method_with_complex_ref()
        {
            CallMainVMTest("137, method_calling_method_with_complex_ref", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_calling_method_with_complex_ref()
        {
            CallMainVMTest("137, method_calling_method_with_complex_ref", false);
        }

        //meth_float_division_by_zero ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_float_division_by_zero()
        {
            CallMainVMTest("138, meth_float_division_by_zero", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_float_division_by_zero()
        {
            CallMainVMTest("138, meth_float_division_by_zero", false);
        }

        //meth_date_and_time_DEFAULT_VALUE ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_date_and_time_DEFAULT_VALUE()
        {
            CallMainVMTest("139, meth_date_and_time_DEFAULT_VALUE", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_date_and_time_DEFAULT_VALUE()
        {
            CallMainVMTest("139, meth_date_and_time_DEFAULT_VALUE", false);
        }

        //method_VARAIABLE_STATUS ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void method_VARAIABLE_STATUS()
        {
            CallMainVMTest("140, method_VARAIABLE_STATUS", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_method_VARAIABLE_STATUS()
        {
            CallMainVMTest("140, method_VARAIABLE_STATUS", false);
        }

        //meth_communication_abort ///////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunTest")]
        public void meth_communication_abort()
        {
            CallMainVMTest("141, meth_communication_abort", true);
        }
        [TestMethod(), TestCategory("MakeExpectedFile")]
        public void make_meth_communication_abort()
        {
            CallMainVMTest("141, meth_communication_abort", false);
        }


        /////////////////////////////////////ACTION METHODS

        //scale_put_get_val//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_put_get_val()
        {
            CallActionTest("143, scale_put_get_val");
        }

        //scale_set_get_double_val//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_set_get_double_val()
        {
            CallActionTest("156, scale_set_get_double_val");
        }

        //scale_method_time_value8//////////////////////////////////////////////////////
        [TestMethod(), TestCategory("RunActionTest")]
        public void scale_method_time_value8()
        {
            CallActionTest("124, scale_method_time_value8");
        }

    }
}
