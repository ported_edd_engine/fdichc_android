#pragma once

#include "nsEDDEngine/IEDDEngine.h"
#include "nsConsumer/IEDDEngineLogger.h"

#if defined (EXPORT_EDDENGINE_API)
	#define __EDDENGINE_API __declspec(dllexport)
#else
	#define __EDDENGINE_API __declspec(dllimport)
#endif

namespace nsEDDEngine
{
	
	class __EDDENGINE_API EDDEngineFactory
	{
	public:

		
		// Factory for an EDDEngine instance
		
        // Format for pConfigXML
        // <?xml version="1.0"?>
        // <CONFIG>
        //    <HARTPath>....\Devices\HART</HARTPath>
        //    <FFPath>.....\Devices\FF</FFPath>
        //    <HARTManufacturerDictPath ID="000026"></HARTManufacturerDictPath>
        // </CONFIG>

		static nsEDDEngine::IEDDEngine * CreateDeviceInstance(const wchar_t *sEDDBinaryFilename,  
			nsConsumer::IParamCache *pIParamCache,
			nsConsumer::IBuiltinSupport *pIBuiltinSupport,
			nsConsumer::IEDDEngineLogger *pIEDDEngineLogger,
			const wchar_t* pConfigXML, 
			EDDEngineFactoryError *pErrorCode);


		static wchar_t* GetEDDEngineErrorString(EDDEngineFactoryError eErrorCode);

	private:
		EDDEngineFactory& operator=(const EDDEngineFactory &rhs);
	};
}
