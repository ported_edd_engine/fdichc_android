﻿<Method>
{
 char  strcmp[20];
 int  strlen;

 strcmp = literal_string(xxx);
 strlen = strlen(literal_string(xxx));
 if (strlen(strcmp) == strlen)
 {
  ACKNOWLEDGE(literal_string(xxx));
 }

}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: PACKED_ASCII
End of Function literal_string
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: PACKED_ASCII
End of Function literal_string
Function Name: strlen
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: PACKED_ASCII
  Returned Param Type: RUL_INT
  Returned Param Value: 12
End of Function strlen
Function Name: strlen
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SAFEARRAY
    Param 1 Array: PACKED_ASCII
  Returned Param Type: RUL_INT
  Returned Param Value: 12
End of Function strlen
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: PASS: tag length = %{strlen}.
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "PASS: tag length = 12."
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: PASS: tag length = %{strlen}.
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><strcmp>[0]P,[1]A,[2]C,[3]K,[4]E,[5]D,[6]_,[7]A,[8]S,[9]C,[10]I,[11]I,[12][13][14][15][16][17][18][19]</strcmp><strlen>12,</strlen></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being discarded"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
