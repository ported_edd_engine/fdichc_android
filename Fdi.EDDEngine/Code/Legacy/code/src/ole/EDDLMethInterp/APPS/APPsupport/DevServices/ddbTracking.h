/*************************************************************************************************
 *
 * $Workfile: ddbTracking.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		Trail generation classes for index resolution in commands
 * #include "ddbTracking.h"
 */

#ifndef _DDBTRACKING_H
#define _DDBTRACKING_H
#ifdef INC_DEBUG
#pragma message("In DDBtracking.h") 
#endif


class hCgroupItemDescriptor;


class hCexpressionTrail
{
public:
	bool        isVar;		// else is constant
	itemID_t	srcID;		// @  isVar::  symbol number of the INDEX variable
	int			srcIdxVal;  // @ !isVar::  constant index/name value
	
	hCexpressionTrail()  {clear();};
	hCexpressionTrail(const hCexpressionTrail& s){*this = s;};

	bool isEmpty(void){ return (isVar==false && srcID==0  && srcIdxVal==0 ); };
	void clear(void)			{isVar=false;   srcID=0x0000;srcIdxVal=0;};
	hCexpressionTrail& operator=(const hCexpressionTrail& s)
				{ isVar=s.isVar;srcID = s.srcID; srcIdxVal=s.srcIdxVal;return *this;};
	void clogSelf(int indent = 0);

};

typedef vector<hCexpressionTrail>   ExprTrailList_t;

/*************************************************************************************************
 * hCvarGroupTrail
 *
 * this is a data class for dealing with nested collections/arrays
 * used for aquiring information through a resolution
 *************************************************************************************************
 */
class hCitemBase;
#define NOT_PART_OF_GROUP  ((unsigned long)(-1))

class hCvarGroupTrail
{
public:
	itemID_t		 itmID;		// symbol number of item(normally a collection or arrayID or Var)
	hCitemBase*		 itmPtr;	// the value from getItemBySymbolNumber(srcID)<we DO NOT own this>

	wstring           lablStr;
	wstring           helpStr;
	wstring           containerDesc;// member | element info
	wstring           containerHelp;
	unsigned long    containerNumber;// aka: index
	
	hCexpressionTrail* pExprTrail;  // copy of expression   - if it exists <we own these pointers
	hCvarGroupTrail* pSourceTrail;	// copy of source trail - if it exists>
	hCvarGroupTrail* pLookupTrail;  // copy of lookup trail - if it exists

	hCvarGroupTrail():pSourceTrail(NULL),pLookupTrail(NULL),pExprTrail(NULL)  { clear(); };
	hCvarGroupTrail(const hCvarGroupTrail& src)
					 :pSourceTrail(NULL),pLookupTrail(NULL),pExprTrail(NULL)  {*this=src;};
	virtual ~hCvarGroupTrail(){clear();};

	/* the following are in ddbCollAndArr.cpp */
	void clear(void);
	bool isEmpty(void);

	hCvarGroupTrail& operator=(const hCvarGroupTrail& src);
	hCvarGroupTrail& operator=( hCgroupItemDescriptor& src);

	void clogSelf(int indent = 0);
};


class hCcommandDescriptor
{
public:
	int				cmdNumber;
	int				transNumb;

	hCcommandDescriptor();
	hCcommandDescriptor(const hCcommandDescriptor& cd);
	void clear(void);
	hCcommandDescriptor& operator=(const hCcommandDescriptor& cd);
};


#endif //_DDBTRACKING_H

/*************************************************************************************************
 *
 *   $History: $
 * 
 *************************************************************************************************
 */
