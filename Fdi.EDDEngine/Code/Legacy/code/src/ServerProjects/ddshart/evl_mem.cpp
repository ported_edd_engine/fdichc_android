/**
 *	@(#) $Id: evl_mem.c,v 1.2 1996/01/04 20:12:35 kimwolk Exp $
 *	Copyright 1992 Rosemount, Inc. - All rights reserved
 *
 *	This file contains all functions relating to the data structure
 *  MEMBER_LIST and MEMBER.
 */

#include "stdinc.h"
#include "HART/HARTEDDEngine/DDSSupport.h"

#pragma warning (disable : 4131)


#ifdef ISPTEST
#include "tst_fail.h"
#endif

void
ddl_free_vector(VECTOR *vector_items, uchar dest_flag)
{

	int             i;
	VECTOR_ITEM      *temp_list;

	if (vector_items == NULL) {
		return;
	}

	if (vector_items->list == NULL) {
		ASSERT_DBG(!vector_items->count && !vector_items->limit);
		vector_items->limit = 0;
	}
	else {
		temp_list = vector_items->list;
		for (i = 0; i < vector_items->count; temp_list++, i++) {
			if (temp_list->type == DATA_REFERENCE) {
				ddl_free_op_ref_trail(&temp_list->vector.ref);
			}
			if (temp_list->type == DATA_STRING) {
				ddl_free_string(&temp_list->vector.str);
			}
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of VECTOR_ITEMs
			 */

			free((void *) vector_items->list);
			vector_items->list = NULL;
			vector_items->limit = 0;
		}
	}

	vector_items->count = 0;
}

/*********************************************************************
 *
 *	Name: ddl_free_vectors_list
 *	ShortDesc: Free the list of VECTORs
 *
 *	Description:
 *		ddl_free_vectors_list will check the VECTOR_LIST pointer and
 *		the list, if the are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero
 *	Inputs:
 *		vectors: pointer to the VECTOR_LIST structure
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		vectors: pointer to the list of VECTOR_LIST with an empty
 *		list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Ying Xu
 *
 *********************************************************************/
void
ddl_free_vectors_list(VECTOR_LIST *vectors, uchar dest_flag)
{

	if (vectors == NULL) {
		return;
	}

	if (vectors->vectors == NULL) {

		ASSERT_DBG(!vectors->count && !vectors->limit);
		vectors->limit = 0;
	}
	else
	{
		VECTOR *temp_vector = vectors->vectors;
		for (int inc = 0; inc < vectors->limit; inc++, temp_vector++) {
			ddl_free_string(&temp_vector->description);
			ddl_free_vector(temp_vector, FREE_ATTR);
		}

		if (dest_flag == FREE_ATTR)
		{
			/*
			* Free the list
			*/

			free((void *) vectors->vectors);
			vectors->vectors = NULL;
			vectors->limit = 0;
		}
	}
	vectors->count = 0;
}

/*********************************************************************
 *
 *	Name: ddl_free_members_list
 *	ShortDesc: Free the list of MEMBERs
 *
 *	Description:
 *		ddl_free_members_list will check the MEMBER_LIST pointer and
 *		the list, if the are both not equal to NULL it will free the list
 *		and set the count and limit equal to zero
 *	Inputs:
 *		members: pointer to the MEMBER_LIST structure
 *		dest_flat: flag which designates whether or not to clean or
 *		free the structure
 *
 *	Outputs:
 *		members: pointer to the list of MEMBER_LIST with an empty
 *		list.
 *
 *	Returns:
 *		void
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

void
ddl_free_members_list(
MEMBER_LIST    *members,
uchar           dest_flag)
{

	int             inc;	/* incrementer for the list */
	MEMBER         *temp_member;	/* temp pointer for the list */


	if (members == NULL) {
		return;
	}

	if (members->list == NULL) {

		ASSERT_DBG(!members->count && !members->limit);
		members->count = 0;
		members->limit = 0;
	}
	else {

		/*
		 * Free STRINGs for each MEMBER
		 */

		temp_member = members->list;
		for (inc = 0; inc < members->limit; inc++, temp_member++) {
			ddl_free_string(&temp_member->help);
			ddl_free_string(&temp_member->desc);
			ddl_free_string(&temp_member->name_string);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of MEMBERs
			 */

			free((void *) members->list);
			members->list = NULL;
			members->limit = 0;
		}
		else {

			/**
			 *	Initializing the list of member values to 0.
			 *	It is OK to initialize "evaled","name","ref","desc",and "help"
			 */

			memset((char *) members->list, 0, (size_t) members->limit * sizeof(*members->list));
		}

		members->count = 0;
	}

	return;
}


/*********************************************************************
 *
 *	Name: ddl_shrink_members_list
 *	ShortDesc: Shrink the list of MEMBERs
 *
 *	Description:
 *		ddl_shrink_members_list reallocs the list of MEMBERs
 *		to contain only the MEMBERs being used
 *
 *	Inputs:
 *		members: pointer to the MEMBER_LIST structure
 *
 *	Outputs:
 *		members: pointer to the resized MEMBER_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/

static
int
ddl_shrink_members_list(
MEMBER_LIST    *members)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_MEMBERS_LIST);
#endif

	if (members == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (members->list == NULL) {
		ASSERT_DBG(!members->count && !members->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (members->count == members->limit) {
		return DDL_SUCCESS;
	}


	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (members->count == 0) {
		ddl_free_members_list(members, FREE_ATTR);
	}
	else {
		members->limit = members->count;

		members->list = (MEMBER *) realloc((void *) members->list,
			(size_t) (members->limit * sizeof(MEMBER)));

		if (members->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}




/*********************************************************************
 *
 *	Name: ddl_parse_members
 *
 *	ShortDesc: Parse a list of MEMBERs
 *
 *	Description:
 *		ddl_parse_members parses the binary data and loads an MEMBER_LIST
 *		structure is the pointer to the MEMBER_LIST is not NULL
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		members - pointer to an MEMBER_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		members - pointer to an MEMBER_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		error returns from DDL_PARSE_INTEGER(),ddl_parse_string()
 *		ddl_parse_item_id(), DDL_PARSE_TAG()
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR, DDL_ENCODING_ERROR
 *
 *	Author:
 *		Chris Gustafson
 *
 *********************************************************************/


static int
ddl_parse_members(
unsigned char **chunkp,
DDL_UINT       *size,
MEMBER_LIST    *members,
OP_MEMBER_LIST *op_members, 
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        len, tag;	/* temp tag and len for parse_tag() */
	MEMBER         *temp_members;	/* temp pointer for the list */
	OP_MEMBER      *temp_op_members;/* temp pointer for the op list */

#ifdef ISPTEST
	TEST_FAIL(DDL_PARSE_MEMBERS);
#endif

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	ROD_HANDLE rod_handle = 0;
	rc = pConnMgr->get_abt_dd_handle(env_info->block_handle, &rod_handle);
	if (rc != CM_SUCCESS)
	{
		return rc;
	}
	bool name_string_encoded = g_DeviceTypeMgr.is_tok_major_rev_6_or_8(rod_handle);

	while (*size > 0) {

		/*
		 * Parse the next item, and make sure it is an MEMBER
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != MEMBER_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;

		if (op_members) {

			/*
			 * Parse a series of OP_MEMBERs.  If we need more room in
			 * the members of structures, malloc more room. Then
			 * parse the next OP_MEMBER structure.
			 */

			if (op_members->count == op_members->limit) {

				op_members->limit += MEMBERS_INC;

				temp_op_members = (OP_MEMBER *) realloc((void *) op_members->list,
					(size_t) (op_members->limit * sizeof(OP_MEMBER)));

				if (temp_op_members == NULL) {
					op_members->limit = op_members->count;
					ddl_free_op_members_list(op_members, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_op_members[op_members->count], 0,
					(size_t) (MEMBERS_INC * sizeof(OP_MEMBER)));

				op_members->list = temp_op_members;
			}

			temp_op_members = &op_members->list[op_members->count++];

			/*
			 * Parse the index, entry reference, description
			 * (desc), and help (if present)
			 */

			DDL_PARSE_INTEGER(chunkp, &len, &temp_op_members->name);

			rc = ddl_parse_op_ref_trail(chunkp, &len, &temp_op_members->ref,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			temp_op_members->evaled |= MEM_NAME_EVALED | MEM_REF_EVALED;

			if (name_string_encoded)
			{
				if (len > 0)
				{
					rc = ddl_parse_string(chunkp, &len, &temp_op_members->name_string,
					depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					temp_op_members->evaled |= MEM_NAME_STR_EVALED;
				}
				else
				{
					return DDL_ENCODING_ERROR;
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_op_members->desc,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_op_members->evaled |= MEM_DESC_EVALED;
			}
			else {

				/*
				 * If a DESCRIPTION string was not found in the
				 * binary, use the default description string
				 * from the standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_DESC,
					&temp_op_members->desc);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_op_members->desc);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_op_members->help,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_op_members->evaled |= MEM_HELP_EVALED;
			}
			else {

				/*
				 * If a HELP string was not found in the
				 * binary, use the default help string from the
				 * standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_HELP,
					&temp_op_members->help);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_op_members->help);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}
		}
		else if (members) {

			/*
			 * Parse a series of MEMBERs.  If we need more room in
			 * the members of structures, malloc more room. Then
			 * parse the next MEMBER structure.
			 */

			if (members->count == members->limit) {

				members->limit += MEMBERS_INC;

				temp_members = (MEMBER *) realloc((void *) members->list,
					(size_t) (members->limit * sizeof(MEMBER)));

				if (temp_members == NULL) {
					members->limit = members->count;
					ddl_free_members_list(members, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}

				memset((char *) &temp_members[members->count], 0,
					(size_t) (MEMBERS_INC * sizeof(MEMBER)));

				members->list = temp_members;
			}

			temp_members = &members->list[members->count++];

			/*
			 * Parse the index, entry reference, description
			 * (desc), and help (if present)
			 */

			DDL_PARSE_INTEGER(chunkp, &len, &temp_members->name);

			rc = ddl_parse_desc_ref(chunkp, &len, &temp_members->ref,
				depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
			temp_members->evaled |= MEM_NAME_EVALED | MEM_REF_EVALED;

			if (name_string_encoded)
			{
				if (len > 0)
				{
					rc = ddl_parse_string(chunkp, &len, &temp_members->name_string,
					depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
					temp_members->evaled |= MEM_NAME_STR_EVALED;
				}
				else
				{
					return DDL_ENCODING_ERROR;
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_members->desc,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_members->evaled |= MEM_DESC_EVALED;
			}
			else {

				/*
				 * If a DESCRIPTION string was not found in the
				 * binary, use the default description string
				 * from the standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_DESC,
					&temp_members->desc);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_members->desc);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, &temp_members->help,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
				temp_members->evaled |= MEM_HELP_EVALED;
			}
			else {

				/*
				 * If a HELP string was not found in the
				 * binary, use the default help string from the
				 * standard dictionary.
				 */

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_HELP,
					&temp_members->help);


				/*
				 * If a string was not found, get the default
				 * error string.
				 */

				if (rc != DDL_SUCCESS) {
					rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
						&temp_members->help);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
			}
		}
		else {

			/*
			 * Member pointer is NULL so parse but don't load
			 */

			DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);

			/*
			 * The next item in the binary is a "desc_ref".
			 * However, a value is not desired, therefore,
			 * ddl_parse_item_id() is being called because it is
			 * faster than ddl_parse_desc_ref().
			 */

			rc = ddl_parse_item_id(chunkp, &len, (ITEM_ID *) NULL, depinfo,
				env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}

			if (name_string_encoded)
			{
				if (len > 0) {
					rc = ddl_parse_string(chunkp, &len, (STRING *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						return rc;
					}
				}
				else
				{
					return DDL_ENCODING_ERROR;
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, (STRING *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}

			if (len > 0) {
				rc = ddl_parse_string(chunkp, &len, (STRING *) NULL,
					depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					return rc;
				}
			}
		}

		/*
		 * If there are any new fields, just ignore them.
		 */

		if (len > 0) {
			*chunkp += len;
		}
	}

	return DDL_SUCCESS;
}





/*********************************************************************
 *
 *	Name: ddl_members_choice
 *	ShortDesc: Choose the correct members from a binary.
 *
 *	Description:
 *		ddl_members_choice will parse the binary for members items,
 *		according to the current conditionals (if any).  The value of
 *		the members items is returned, along with dependency information.
 *		If a value is found, the valid flag is set to 1.
 *
 *	Inputs:
 *		chunkp - pointer to the address of the binary
 *		size - pointer to the size of the binary
 *		list - pointer to an MEMBER_LIST structure where the result will
 *				be stored.  If this is NULL, no result is computed
 *				or stored.
 *		depinfo - pointer to a OP_REF_LIST structure where dependency
 *				information will be stored.  If this is NULL, no
 *				dependency information will be stored.
 *		env_info - environment information
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Outputs:
 *		chunkp - pointer to the address following this binary
 *		size - pointer to the size of the binary following this one
 *		list - pointer to an MEMBER_LIST structure containing the result
 *		depinfo - pointer to a OP_REF_LIST structure containing the
 *				dependency information.
 *		data_valid - pointer to an int which, if set to 1, indicates
 *				that there is a valid value in expr
 *		var_needed - pointer to a OP_REF structure. If the value
 *				of a variable is needed, but is not available,
 *				"var_needed" info is returned to the application.
 *
 *
 *	Returns:
 *		DDL_SUCCESS
 *		error returns from ddl_cond_list() and ddl_parse_members().
 *
 *	Author:
 *		Steve Beyerl
 *
 *********************************************************************/

static
int
ddl_members_choice(
unsigned char **chunkp,
DDL_UINT       *size,
MEMBER_LIST    *list,
OP_REF_LIST    *depinfo,
int            *data_valid,
ENV_INFO       *env_info,
OP_REF         *var_needed,
OP_MEMBER_LIST *op_list)
{
	int             rc;	/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to a list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;	/* temp pointer for the chunk list */

#ifdef ISPTEST
	TEST_FAIL(DDL_MEMBERS_CHOICE);
#endif

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		MEMBER_SEQLIST_TAG, env_info, var_needed);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and either a value is requested.
	 */

	if ((list || op_list) && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_members(&(chunk_ptr->chunk),
				&(chunk_ptr->size), list, op_list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
		if (op_list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}




/********************************************************************
 *
 * Name: eval_attr_members
 *
 * ShortDesc: evaluate a members
 *
 * Description:
 *
 *	The eval_attr_members function evaluates members.
 *	The buffer pointed to by chunk
 *	should contain a members returned from fetch_members,
 *	and size should specify its size.
 *	If memb is not a null pointer, the members
 *	are returned in coll. If depinfo is not
 *	a null pointer, dependency information about the members
 *	is returned in depinfo.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      members - pointer to an MEMBER_LIST where the result will
 *              be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      members - pointer to an MEMBER_LIST structure containing the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 * Returns:
 *	DDL_SUCCESS, DDL_ENCODING_ERROR, DDL_MEMORY_ERROR
 *	and return codes from ddl_members_choice(), ddl_parse_tag_func()
 *	ddl_parse_integer_func(), ddl_parse_desc_ref(), ddl_parse_string()
 *  ddl_shrink_depinfo(), ddl_shrink_members_list()
 *
 ***************************************************************************/


int
eval_attr_members(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	MEMBER_LIST*	members = static_cast<MEMBER_LIST*>(voidP);
	int             rc;	/* return code */
	int             valid;	/* data valid flag */

#ifdef PROCESS_IMPORTS
	DDL_UINT        len;	/* used by parse tag */
	DDL_UINT        tag, members_tag;	/* temp tag values */
	unsigned long   name;	/* used by redefinition */
	int             match_inc;	/* used by redefinition */
	int             match_position;	/* used by redefinition */
	MEMBER         *temp_members;	/* temp pointer for member in list */

#endif


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_MEMBERS);
#endif

	valid = 0;

	ddl_free_members_list(members, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	/*
	 * Parse the members information
	 */

	rc = ddl_members_choice(&chunk, &size, members, depinfo, &valid, env_info, var_needed, nullptr);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * If we are not returning a value , we can skip the additions,
	 * deletions, and redefinitions.
	 */

	if (!members) {
		goto exit;
	}

#ifdef PROCESS_IMPORTS

	while (size) {

		/*
		 * Parse the tag to find out if this is a deletion, addition,
		 * or redefinition.
		 */

		rc = ddl_parse_tag_func(&chunk, &size, &tag, &len);
		if (rc != DDL_SUCCESS) {
			goto err_exit;
		}

		if (len > size) {
			rc = DDL_ENCODING_ERROR;
			goto err_exit;
		}

		/*
		 * Additions and redefinitions, have a tag that must be
		 * MEMBER_TAG
		 */

		if (tag != DELETE_TAG) {
			rc = ddl_parse_tag_func(&chunk, &size, &members_tag, &len);

			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			if (members_tag != MEMBER_TAG) {
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
		size -= len;

		if (members) {

			/*
			 * Find the existing members element that this
			 * alteration is referencing.
			 */

			rc = ddl_parse_integer_func(&chunk, &len, &name);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			match_position = members->count;

			for (match_inc = 0; match_inc < match_position; match_inc++) {
				if (members->list[match_inc].name == name)
					match_position = match_inc;
			}

			temp_members = &members->list[match_position];

			switch (tag) {

			case DELETE_TAG:

				/*
				 * Delete the members element by freeing any
				 * subreferences and then shrink the list and
				 * eliminate the deleted structure.
				 */

				if (match_position < members->count - 1) {

					memcpy((char *) temp_members, (char *) (temp_members + 1),
						(size_t) (members->count - match_position - 1) *
						sizeof *temp_members);

					memset((char *) &members->list[members->count - 1], 0,
						sizeof *members->list);
				}
				else {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}
				members->count--;
				break;

			case REDEFINE_TAG:

				/*
				 * Redefine the members elements by parsing the
				 * new item, description, and help
				 */

				if (match_position < members->count) {
					rc = ddl_parse_desc_ref(&chunk, &len, &temp_members->ref,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}

					if (len > 0) {
						rc = ddl_parse_string(&chunk, &len, &temp_members->desc,
							depinfo, env_info, var_needed);

						if (rc != DDL_SUCCESS) {
							goto err_exit;
						}
					}

					if (len > 0) {
						rc = ddl_parse_string(&chunk, &len, &temp_members->help,
							depinfo, env_info, var_needed);
						if (rc != DDL_SUCCESS) {
							goto err_exit;
						}
					}

					valid = 1;
				}
				else {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}
				break;

			case ADD_TAG:
				if (match_position < members->count) {
					rc = DDL_ENCODING_ERROR;
					goto err_exit;
				}

				/*
				 * Expand the MEMBER_LIST if necessary, and add
				 * the new members element.
				 */

				if (members->count >= members->limit) {
					members->limit += MEMBERS_INC;

					temp_members = (MEMBER *) realloc((void *) members->list,
						(size_t) members->limit * sizeof *temp_members);

					if (!temp_members) {
						rc = DDL_MEMORY_ERROR;
						goto err_exit;
					}

					members->list = temp_members;
					memset((char *) &members->list[members->count], 0,
						(size_t) MEMBERS_INC * sizeof *members->list);
				}

				/*
				 * Parse the members item, description (desc),
				 * and help.
				 */

				temp_members = members->list + members->count;
				members->count++;

				temp_members->name = name;

				rc = ddl_parse_desc_ref(&chunk, &len, &temp_members->ref, depinfo,
					env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, &temp_members->desc,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}


				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, &temp_members->help,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				valid = 1;
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
		else {
			rc = ddl_parse_integer_func(&chunk, &len, (DDL_UINT *) NULL_PTR);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			switch (tag) {

			case DELETE_TAG:
				break;

			case REDEFINE_TAG:
			case ADD_TAG:
				rc = ddl_parse_item_id(&chunk, &len, (ITEM_ID *) NULL, depinfo, env_info, var_needed);
				if (rc != DDL_SUCCESS) {
					goto err_exit;
				}

				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, (STRING *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}

				if (len > 0) {
					rc = ddl_parse_string(&chunk, &len, (STRING *) NULL,
						depinfo, env_info, var_needed);
					if (rc != DDL_SUCCESS) {
						goto err_exit;
					}
				}
				break;

			default:
				rc = DDL_ENCODING_ERROR;
				goto err_exit;
			}
		}
	}
#endif

exit:
	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_members_list(members);
	}

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (members && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	ddl_free_members_list(members, FREE_ATTR);
	return rc;
}


/*
 * The ddl_op_members functions below are the exact same as the ddl_members functions
 * above, with the exception that they return an operation reference (OP_REF) for
 * the member reference, rather than a descriptive reference (DESC_REF).
 * The MEMBER_SEQLIST_TAG is still used in the binary, since the binary for these
 * two types of lists are identical.
 */

void
ddl_free_op_members_list (OP_MEMBER_LIST *members, uchar dest_flag)
{

	int             inc;			/* incrementer for the list */
	OP_MEMBER         *temp_member;	/* temp pointer for the list */


	if (members == NULL) {
		return;
	}

	if (members->list == NULL) {

		ASSERT_DBG(!members->count && !members->limit);
		members->count = 0;
		members->limit = 0;
	}
	else {

		/*
		 * Free STRINGs for each MEMBER
		 */

		temp_member = members->list;
		for (inc = 0; inc < members->count; inc++, temp_member++) {
			ddl_free_string(&temp_member->help);
			ddl_free_string(&temp_member->desc);
			ddl_free_string(&temp_member->name_string);
            ddl_free_op_ref_trail(&temp_member->ref);
		}

		if (dest_flag == FREE_ATTR) {

			/*
			 * Free the list of MEMBERs
			 */

			free((void *) members->list);
			members->list = NULL;
			members->limit = 0;
		}
		else {

			/**
			 *	Initializing the list of member values to 0.
			 *	It is OK to initialize "evaled","name","ref","desc",and "help"
			 */

			(void)memset((char *) members->list, 0,
				(size_t) members->limit * sizeof(*members->list));
		}

		members->count = 0;
	}

	return;
}

/*********************************************************************
 *
 *	Name: ddl_shrink_op_members_list
 *	ShortDesc: Shrink the list of MEMBERs
 *
 *	Description:
 *		ddl_shrink_op_members_list reallocs the list of OP_MEMBERs
 *		to contain only the OP_MEMBERs being used
 *
 *	Inputs:
 *		members: pointer to the OP_MEMBER_LIST structure
 *
 *	Outputs:
 *		members: pointer to the resized OP_MEMBER_LIST structure
 *
 *	Returns:
 *		DDL_SUCCESS,  DDL_MEMORY_ERROR
 *
 *	Author:
 *		copied from ddl_shrink_members_list by Mark Sandmann
 *
 *********************************************************************/

static
int
ddl_shrink_op_members_list(
OP_MEMBER_LIST    *members)
{

#ifdef ISPTEST
	TEST_FAIL(DDL_SHRINK_OP_MEMBERS_LIST);
#endif

	if (members == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are consistent, and return.
	 */

	if (members->list == NULL) {
		ASSERT_DBG(!members->count && !members->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements.  If it is already at size
	 * elements, just return.
	 */

	if (members->count == members->limit) {
		return DDL_SUCCESS;
	}


	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (members->count == 0) {
		ddl_free_op_members_list(members, FREE_ATTR);
	}
	else {
		members->limit = members->count;

		members->list = (OP_MEMBER *) realloc((void *) members->list,
			(size_t) (members->limit * sizeof(OP_MEMBER)));

		if (members->list == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


/********************************************************************
 *
 * Name: eval_attr_op_members
 *
 * ShortDesc: evaluate a members
 *
 * Description:
 *
 *	The eval_attr_op_members function evaluates op_members.
 *	The buffer pointed to by chunk
 *	should contain a members returned from fetch_members,
 *	and size should specify its size.
 *	If memb is not a null pointer, the members
 *	are returned in coll. If depinfo is not
 *	a null pointer, dependency information about the members
 *	is returned in depinfo.
 *
 *  Inputs:
 *      chunkp - pointer to the address of the binary
 *      size - pointer to the size of the binary
 *      members - pointer to an OP_MEMBER_LIST where the result will
 *              be stored.  If this is NULL, no result is computed
 *              or stored.
 *      depinfo - pointer to a OP_REF_LIST structure where dependency
 *              information will be stored.  If this is NULL, no
 *              dependency information will be stored.
 *      env_info - environment information
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 *
 *  Outputs:
 *      chunkp - pointer to the address following this binary
 *      size - pointer to the size of the binary following this one
 *      members - pointer to an OP_MEMBER_LIST structure containing the result
 *      depinfo - pointer to a OP_REF_LIST structure containing the
 *              dependency information.
 *      var_needed - pointer to a OP_REF structure. If the value
 *              of a variable is needed, but is not available,
 *              "var_needed" info is returned to the application.
 * Returns:
 *	DDL_SUCCESS, DDL_ENCODING_ERROR, DDL_MEMORY_ERROR
 *	and return codes from ddl_members_choice(), ddl_parse_tag_func()
 *	ddl_parse_integer_func(), ddl_parse_desc_ref(), ddl_parse_string()
 *  ddl_shrink_depinfo(), ddl_shrink_op_members_list()
 *
 ***************************************************************************/
int
eval_attr_op_members(
unsigned char  *chunk,
DDL_UINT        size,
void		   *voidP,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	OP_MEMBER_LIST*	members = static_cast<OP_MEMBER_LIST*>(voidP);

	int             rc;		/* return code */
	int             valid;	/* data valid flag */


	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef ISPTEST
	TEST_FAIL(EVAL_OP_MEMBERS);
#endif /* ISPTEST */

	valid = 0;

	ddl_free_op_members_list(members, CLEAN_ATTR);

	if (depinfo) {
		depinfo->count = 0;
	}

	var_needed->op_info.id = 0;		// Make sure that the var_needed id is cleared

	/*
	 * Parse the members information
	 */

	rc = ddl_members_choice(&chunk, &size, NULL, depinfo, &valid, env_info, var_needed, 
        members);
	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * If we are not returning a value, we can skip the additions,
	 * deletions, and redefinitions.
	 */

	if (!members) {
		goto exit;
	}


exit:
	rc = ddl_shrink_depinfo(depinfo);
	if (rc == DDL_SUCCESS) {
		rc = ddl_shrink_op_members_list(members);
	}

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	if (members && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;

err_exit:
	ddl_free_depinfo(depinfo, FREE_ATTR);
	ddl_free_op_members_list(members, FREE_ATTR);
	return rc;
}



//????? Delete This
/*********************************************************************
 *
 *	Name: link_members()
 *	ShortDesc: collect dependencies from collection members for the HART linker
 *
 *	Author: Christian Gustafson
 *
 ***********************************************************************/

int
link_members(
unsigned char **chunkp,
DDL_UINT       *size,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	DDL_UINT        len, tag;	/* temp tag and len for parse_tag() */


	while (*size > 0) {

		/*
		 * Parse the next item, and make sure it is an MEMBER
		 */

		DDL_PARSE_TAG(chunkp, size, &tag, &len);

		if (tag != MEMBER_TAG) {
			return DDL_ENCODING_ERROR;
		}

		*size -= len;


		DDL_PARSE_INTEGER(chunkp, &len, (DDL_UINT *) NULL_PTR);

		rc = link_ref(chunkp, &len, depinfo, env_info, var_needed, 0);
		if (rc != DDL_SUCCESS) {
			return rc;
		}

		if (len > 0) {
			rc = link_string(chunkp, &len, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}

		if (len > 0) {
			rc = link_string(chunkp, &len, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				return rc;
			}
		}


		/*
		 * If there are any new fields, just ignore them.
		 */

		if (len > 0) {
			*chunkp += len;
		}
	}

	return DDL_SUCCESS;
}

static int
ddl_parse_vector_item(
unsigned char **chunkp,
DDL_UINT       *size,
VECTOR_ITEM    *vector_item,
OP_REF_LIST    *depinfo,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{
	int             rc;	/* return code */
	OP_REF_TRAIL    ref;	/* temp OP_REF_TRAIL for calling parse_ref()*/

	memset(&ref, 0, sizeof(OP_REF_TRAIL));

	if (vector_item) {

		ref.desc_id = 0;
		ref.desc_type = 0;

		rc = ddl_parse_ref(chunkp, size, &ref, depinfo,
			env_info, var_needed, (unsigned int) RESOLVE_DESC_REF | RESOLVE_OP_REF | RESOLVE_TRAIL);

		if (rc != DDL_SUCCESS) {
			return rc;
		}
		
		if (ref.desc_id == 0)
		{
			if (ref.expr.type == INTEGER)
			{
				vector_item->type = DATA_CONSTANT;
				vector_item->vector.iconst = (long)ref.expr.val.ll;

			}
			else if (ref.expr.type == FLOAT)
			{
				vector_item->type = DATA_FLOATING;
				vector_item->vector.fconst = ref.expr.val.f;
			}
			else if (ref.expr.type == ASCII)
			{
				vector_item->type = DATA_STRING;
				memcpy(&vector_item->vector.str, &ref.expr.val.s, sizeof(STRING));
			}
			else
			{
				ASSERT_DBG(false);
				return DDL_ENCODING_ERROR;
			}
		}
		else
		{
			vector_item->type = DATA_REFERENCE;
			memcpy(&vector_item->vector.ref, &ref, sizeof(OP_REF_TRAIL));

		}
	}
	else {

		rc = ddl_parse_ref(chunkp, size, (OP_REF_TRAIL *) NULL, depinfo,
			env_info, var_needed, 0);
	}

	return rc;
}

static int ddl_shrink_vector_list(VECTOR_LIST *vector_list)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_REFLIST);
#endif /* DDSTEST */

	if (vector_list == NULL) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the sizes are 0 and return.
	 */

	if (vector_list->vectors == NULL) {
		ASSERT_DBG(!vector_list->count && !vector_list->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to size elements. If count is equal to elements,
	 * return.
	 */

	if (vector_list->count == vector_list->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (vector_list->count == 0) {
		ddl_free_vectors_list(vector_list, FREE_ATTR);
	}
	else {
		vector_list->limit = vector_list->count;

		vector_list->vectors = (VECTOR *) realloc((void *) vector_list->vectors,
			(size_t) (vector_list->limit * sizeof(VECTOR)));

		if (vector_list->vectors == NULL) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}

static int
ddl_shrink_vector(VECTOR *items)
{

#ifdef DDSTEST
	TEST_FAIL(DDL_SHRINK_VECTOR);
#endif /* DDSTEST */

	if (!items) {
		return DDL_SUCCESS;
	}

	/*
	 * If there is no list, make sure the counts are consistent, and
	 * return.
	 */

	if (!items->list) {
		ASSERT_DBG(!items->count && !items->limit);
		return DDL_SUCCESS;
	}

	/*
	 * Shrink the list to count elements.  If it is already at count
	 * elements, just return.
	 */

	if (items->count == items->limit) {
		return DDL_SUCCESS;
	}

	/*
	 * If count = 0, free the list. If count != 0, shrink the list
	 */

	if (items->count == 0) {
		ddl_free_vector(items, FREE_ATTR);
	}
	else {
		items->limit = items->count;

		items->list = (VECTOR_ITEM *) realloc((void *) items->list,
			(size_t) (items->limit * sizeof(VECTOR_ITEM)));
		if (!items->list) {
			return DDL_MEMORY_ERROR;
		}
	}

	return DDL_SUCCESS;
}


static int
ddl_parse_vector(unsigned char **chunkp, DDL_UINT *size, VECTOR *vector,
	OP_REF_LIST *depinfo, ENV_INFO *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	VECTOR_ITEM     *temp_vector;/* temporary pointer */
    DDL_UINT        tag;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_VECTOR);
#endif /* DDSTEST */

	/* Parse vector description */
	rc = ddl_parse_string(chunkp, size, &vector->description, depinfo, env_info, var_needed);
	if (rc != DDL_SUCCESS)
	{
		return rc;
	}

	DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);

    
    if (tag != GRID_MEMBERS_TAG)
	{
        return DDL_ENCODING_ERROR;
	}

	if (vector != NULL) {

		/*
		 * parse the binary and load the VECTOR structure
		 */

		while (*size > 0) {

			if (vector->count == vector->limit) {

				/*
				 * reallocate the VECTOR structure for
				 * more VECTORs
				 */

				vector->limit += VECTOR_INCSZ;

				/*
				 * realloc VECTOR array
				 */

				temp_vector = (VECTOR_ITEM *) realloc((void *) vector->list,
					(size_t) (vector->limit * sizeof(VECTOR_ITEM)));

				if (temp_vector == NULL) {

					vector->limit = vector->count;
					ddl_free_vector(vector, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				vector->list = temp_vector;

				/*
				 * Initialize the new VECTORs to zero
				 */
				(void)memset((char *) &vector->list[vector->count], 0,
					(VECTOR_INCSZ * sizeof(VECTOR_ITEM)));
			}


			rc = ddl_parse_vector_item(chunkp, size, &vector->list[vector->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_vector(vector, FREE_ATTR);
				return rc;
			}

			vector->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_vector(vector);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
        unsigned count = 0;
		while (*size > 0) {

			/*
			 * Don't load the VECTOR structure
			 */

			rc = ddl_parse_vector_item(chunkp, size, (VECTOR_ITEM *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
            count++;
		}
	}

	return DDL_SUCCESS;
}

static int
ddl_parse_vectorlist(unsigned char **chunkp, DDL_UINT *size, VECTOR_LIST *vector_list,
	OP_REF_LIST *depinfo, ENV_INFO *env_info, OP_REF *var_needed)
{

	int             rc;	/* return code */
	VECTOR        *temp_id;/* temporary pointer */
	DDL_UINT tag;
	DDL_UINT len;

	ASSERT_DBG(chunkp && *chunkp && size);

#ifdef DDSTEST
	TEST_FAIL(DDL_PARSE_VECTORLIST);
#endif /* DDSTEST */

	/*
	 * Parse a VECTOR_LIST.  If we need more room in the array of structures,
	 * xrealloc more room.  Then call ddl_parse_item_id to load the next VECTOR
	 * structure.
	 */

	if (vector_list != NULL) {

		/*
		 * parse the binary and load the VECTOR_LIST structure
		 */

		while (*size > 0) {

			if (vector_list->count == vector_list->limit) {

				/*
				 * reallocate the VECTOR_LIST structure for
				 * more VECTORs
				 */

				vector_list->limit += VECTOR_LIST_INC;

				/*
				 * realloc VECTOR array
				 */

				temp_id = (VECTOR *) realloc((void *) vector_list->vectors,
					(size_t) (vector_list->limit * sizeof(VECTOR)));

				if (temp_id == NULL) {

					vector_list->limit = vector_list->count;
					ddl_free_vectors_list(vector_list, FREE_ATTR);
					return DDL_MEMORY_ERROR;
				}
				vector_list->vectors = temp_id;

				/*
				 * Initialize the new VECTORs to zero
				 */
				(void)memset((char *) &vector_list->vectors[vector_list->count], 0,
					(VECTOR_LIST_INC * sizeof(VECTOR)));
			}

			len = 0;
			DDL_PARSE_TAG(chunkp, size, &tag, &len);
    
			if (tag != GRID_ELEMENT_TAG)
			{
				return DDL_ENCODING_ERROR;
			}

			*size -= len;

			rc = ddl_parse_vector(chunkp, &len, &vector_list->vectors[vector_list->count], depinfo,
				env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				ddl_free_vectors_list(vector_list, FREE_ATTR);
				return rc;
			}

			vector_list->count++;
		}

		/*
		 * shrink the structure here
		 */

		rc = ddl_shrink_vector_list(vector_list);
		if (rc != DDL_SUCCESS) {
			return DDL_MEMORY_ERROR;
		}
	}
	else {
		while (*size > 0) {

			DDL_PARSE_TAG(chunkp, size, &tag, (DDL_UINT *) NULL_PTR);
    
			if (tag != GRID_ELEMENT_TAG)
			{
				return DDL_ENCODING_ERROR;
			}

			/*
			 * Don't load the VECTOR_LIST structure
			 */

			rc = ddl_parse_vector(chunkp, size, (VECTOR *) NULL, depinfo, env_info, var_needed);

			if (rc != DDL_SUCCESS) {

				return rc;
			}
		}
	}

	return DDL_SUCCESS;
}

static
int
ddl_vectorlist_choice(unsigned char **chunkp, DDL_UINT *size, VECTOR_LIST *list,
	OP_REF_LIST *depinfo, int *data_valid, ENV_INFO *env_info, OP_REF *var_needed)
{

	int             rc;				/* return code */
	CHUNK_LIST      chunk_list_ptr;	/* ptr to list of binaries */
	CHUNK           chunk_list[DEFAULT_CHUNK_LIST_SIZE];	/* list of binaries */
	CHUNK          *chunk_ptr;

#ifdef DDSTEST
	TEST_FAIL(DDL_VECTORLIST_CHOICE);
#endif /* DDSTEST */

	if (data_valid) {
		*data_valid = FALSE;
	}

	/*
	 * create a list to temporarily store chunks of binary.
	 */

	ddl_create_chunk_list(chunk_list_ptr, chunk_list);

	/*
	 * Use ddl_cond_list to get a list of chunks. ddl_cond_list() may
	 * modify chunk_list_ptr.list.
	 */

	rc = ddl_cond_list(chunkp, size, &chunk_list_ptr, depinfo,
		GRID_SEQLIST_TAG, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		goto err_exit;
	}

	/*
	 * A list of chunks was found and a value is requested.
	 */

	if (list && (chunk_list_ptr.size > 0)) {

		/*
		 * If the calling routine is expecting a value, data_valid
		 * cannot be NULL
		 */

		ASSERT_DBG((data_valid != NULL) || (list == NULL));

		chunk_ptr = chunk_list_ptr.list;
		while (chunk_list_ptr.size > 0) {	/* Parse them */
			rc = ddl_parse_vectorlist(&(chunk_ptr->chunk), &(chunk_ptr->size),
				list, depinfo, env_info, var_needed);
			if (rc != DDL_SUCCESS) {
				goto err_exit;
			}

			chunk_ptr++;
			chunk_list_ptr.size--;
		}

		if (list && data_valid) {
			*data_valid = TRUE;	/* list has been modified */
		}
	}

	rc = DDL_SUCCESS;

err_exit:
	/* delete chunk list */
	if (chunk_list_ptr.list != chunk_list) {
		ddl_delete_chunk_list(&chunk_list_ptr);
	}

	return rc;
}

int
eval_attr_vectors(unsigned char *chunk, DDL_UINT size, void *voidP,
	OP_REF_LIST *depinfo, ENV_INFO *env_info, OP_REF *var_needed)
{

	int             rc;		/* return code */
	int             valid;	/* indicates validity of the vector list in "items" */

	ASSERT_RET(chunk && size, DDL_INVALID_PARAM);

#ifdef DDSTEST
	TEST_FAIL(EVAL_VECTORS);
#endif /* DDSTEST */

	VECTOR_LIST*	items = static_cast<VECTOR_LIST*>(voidP);

	valid = 0;
	var_needed->op_info.id = 0;	/* Zero id says that this is not set */

	if (items) {
		ddl_free_vectors_list(items, CLEAN_ATTR);
	}

	if (depinfo) {
		depinfo->count = 0;
	}

	rc = ddl_vectorlist_choice(&chunk, &size, items, depinfo, &valid, env_info, var_needed);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		return rc;
	}

	rc = ddl_shrink_depinfo(depinfo);

	if (rc != DDL_SUCCESS) {
		ddl_free_depinfo(depinfo, FREE_ATTR);
		ddl_free_vectors_list(items, FREE_ATTR);
		return rc;
	}

	if (items && !valid) {
		return DDL_DEFAULT_ATTR;
	}

	return DDL_SUCCESS;
}



