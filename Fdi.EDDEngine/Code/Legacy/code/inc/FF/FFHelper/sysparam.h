/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)sysparam.h	30.3  30  14 Nov 1996
 */

#ifndef SYSPARAM_H
#define SYSPARAM_H

/*
 *	The maximum number of methods that can be simultaneously active.
 *	This number determines the number of entries in an array of
 *	structures.  Any active method must have one entry allocated to it.
 */

#define MAX_METHODS		6

/*
 *	Default abort message.  This message will be displayed
 *	when aborting a method if no other more specific message
 *	is provided.  It may contain country code strings in
 *	the same format as dictionary strings (that is, with a
 *	vertical bar preceeding the country code).
 */

#define DEFAULT_ABORT_MSG	"|en|Aborting Method"

/*
 *	Standard messages.  These are error (or abort) messages that are
 *	displayed when certain error conditions are encountered.  They
 *	may contain country code strings in the same format as dictionary
 *	strings (that is, with a vertical bar preceeding the country code).
 */

#define METHOD_COLLISION_ABORT_MSG	\
	"|en|Method Collision Detected: Aborting Method"
#define INTERNAL_ERROR_ABORT_MSG	\
	"|en|Internal Error Detected: Aborting Method"
#define INVALID_ARRAY_REFERENCE		\
	"|en|Invalid Array Reference"
#define UNKNOWN_ENUMERATOR			\
	"|en|Unknown Enumerator"
#define UNKNOWN_RESP_CODE			\
	"|en|Unknown Response Code %d"
#define UNKNOWN_COMM_ERROR			\
	"|en|Unknown Comm Error Code %d"
#define DEFAULT_TAG_STRING			\
	"|en|ID %d, subindex %d"
#define COMM_ERROR_MSG				\
	"|en|Received communications error %s when accessing %s."
#define RESP_CODE_MSG				\
	"|en|Received response %s when accessing %s."
#define RETRY_MENU					\
	"|en|ABORT the method:|en|FAIL the builtin:|en|RETRY the communication"
#define SHORT_RETRY_MENU					\
	"|en|ABORT the method:|en|FAIL the builtin"
#define ABORT_ACTION				\
	"|en|  Aborting."
#define CHOOSE_PROMPT				\
	"|en|  Choose:"


/*
 *	When a builtin sees a communication error or a response code,
 *	it will either abort the method, fail the builtin, or
 *	retry.  This action is specified using builtins.
 *	The communications retry choice specifies how many times to
 *	retry when seeing a "retry" type return before prompting the
 *	use to see what actions to take.
 */

#define COMM_RETRY_CHOICE		3

/*
 *	Method collision retry choice specifies how many times a method will
 *	retry before aborting when it encounters a collision condition
 *	with another active method.  Method collision suspend time specifies
 *	how many seconds to wait before retrying in the event of a
 *	method collision with retries remaining.  Collision conditions 
 *	refer to two active methods attempting to access the same variable, 
 *	or two active methods attempting to initialize method variable in an
 *	incompatible manner.  A value of 0 for this define effectively
 *	says that a method should abort in the case of a collision.
 */

#define METHOD_COLLISION_RETRY_CHOICE		5
#define METHOD_COLLISION_SUSPEND_TIME		2

#endif /*SYSPARAM_H*/
