#ifndef lint
static char SCCSID[] = "@(#)ff_sup.c	30.4  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	ff_sup.c - Fieldbus Foundation Support Module
 */
#include "stdinc.h"
#include    <stdio.h>

#include    "DDI_LIB.H"
#include	"APP_XMAL.H"
#include "EddEngineLog.h"

#include	"../../../../../Inc/FF/FFEDDEngine/DDSSupport.h"

#include <Inf/NtcSpecies/BssProgLog.h>

#include "DeviceTypeMgr.h"






/*---------------------------------------------------------------------
 ------------------------------ UPCALLS -------------------------------
 ---------------------------------------------------------------------*/

/*********************************************************************
 *
 *  Name: get_string_tbl_handle
 *
 ********************************************************************/
int CConnectionMgr::get_string_tbl_handle(DEVICE_HANDLE device_handle, STRING_TBL **string_tbl_handle)
{

	DEVICE_TYPE_HANDLE device_type_handle = active_dev_tbl.list[device_handle]->active_dev_type_tbl_offset;
	FLAT_DEVICE_DIR *flat_device_dir = (FLAT_DEVICE_DIR*)g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(device_type_handle);

	if (flat_device_dir == 0)
	{
		return(FAILURE);
	}

	*string_tbl_handle = &flat_device_dir->string_tbl;

	return(SUCCESS);
}


/*********************************************************************
 *
 *  Name: app_func_get_dev_spec_string
 *
 *  ShortDesc:  Upcall to retrieve a device specific string.
 *
 *  Description: Returns device specific string which has been specified as a device specific
 *               string reference.  The device specific string table to use is 
 *				 specified by the block_handle passed in.  There is one
 *				 device specific string table per device type.
 *
 *  Inputs:
 *		env_info - contains block_handle which is used to access device
 *					specific string table.
 *		dev_str_info - information about the string.
 *
 *  Outputs:
 *		str - pointer to a STRING structure to be filled by the application.
 *
 *  Returns:
 *		DDL_DEV_SPEC_STRING_NOT_FOUND;
 *		DDL_SUCCESS;
 *
 *  Author: Dave Raskin
 *
 **********************************************************************/
int
app_func_get_dev_spec_string(ENV_INFO *env_info, DEV_STRING_INFO *dev_str_info, STRING *str)
{

	int             rs;
	STRING_TBL		*string_tbl_handle;

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnMgr = pDDSSupport->GetConnectionManager();

	/*
	 * Find the pointer to the device specific table corresponding to block handle
	 */
	DEVICE_HANDLE dh = 0;

	if (env_info->handle_type == ENV_INFO::BlockHandle)
	{
		dh = pConnMgr->ABT_ADT_OFFSET(env_info->block_handle);
	}
	else
	{
		dh = env_info->block_handle; // This is the DeviceHandle
	}

	rs = pConnMgr->get_string_tbl_handle(dh, &string_tbl_handle);
	if (rs != SUCCESS) {
        EddEngineLog(env_info, __FILE__, __LINE__, 1, L"LegacyFF", L"app_func_get_dev_spec_string: Device specific table not found\n", rs );
		return DDL_DEV_SPEC_STRING_NOT_FOUND;
	}

	if (dev_str_info->id >= (unsigned long) string_tbl_handle->count) {

		/*
		 * If string not found
		 */
		
        EddEngineLog( env_info, __FILE__, __LINE__, 1, L"LegacyFF", L"app_func_get_dev_spec_string: Device specific string of id %d not found\n", dev_str_info->id );
		return DDL_DEV_SPEC_STRING_NOT_FOUND;
	}
	else {

		/*
		 * Retrieve the device specific string information.
		 */
        // Translate to the language specified in the env_info->lang_code
        int buf_len = wcslen(string_tbl_handle->list[dev_str_info->id].str);
		wchar_t *buf = new wchar_t[buf_len+1];
        int rc = ddi_get_string_translation( string_tbl_handle->list[dev_str_info->id].str, (wchar_t *)env_info->lang_code, buf, buf_len );

        if (rc == DDS_SUCCESS)
        {
			str->len = (unsigned short)wcslen(buf);
 #ifdef _WIN32
				str->len = (unsigned short)wcslen(buf);
			str->str = (wchar_t *)malloc((str->len + 1) * 2);
#else
			str->str = new wchar_t[buf_len + 1];
#endif
			wcscpy(str->str, buf);
			str->str[str->len]=0;
			str->flags = FREE_STRING; 
        }
        delete [] (buf);
		return rc;
	}
}


