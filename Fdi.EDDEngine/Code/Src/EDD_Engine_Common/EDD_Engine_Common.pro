#-------------------------------------------------
#
# Project created by QtCreator 2017-12-05T17:14:24
#
#-------------------------------------------------

QT       -= gui

TARGET = EDD_Engine_Common
TEMPLATE = lib
#CONFIG +=plugin
CONFIG += staticlib
DEFINES += EDD_ENGINE_COMMON_LIBRARY \
           UNICODE

QMAKESPEC=linux-g++-32
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=gnu++11

include(../SuppressWarning.pri)
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += pugixml.cpp \
#    stdafx.cpp \ ignore this for non window target


HEADERS += PIFileFind.h \
    PISemaphore.h \
    PI_CtypedptrArray.h \
    PlatformCommon.h \
    pugiconfig.hpp \
    pugixml.hpp \
#    stdafx.h \ ignore this for non window target
    stdstring.h \
#    targetver.h \ ignore this for non window target
    TypesCommon.h \
    typedefs.h


unix {
    target.path = /usr/lib/
    INSTALLS += target
}

DESTDIR = ../../Legacy/code/lib
CONFIG(debug, debug|release) {
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    BUILD_DIR = $$PWD/release
}
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

android {
    LIBS += -L$$DESTDIR -lEDD_Engine_Android
    INCLUDEPATH += $$PWD/../EDD_Engine_Android
    DEPENDPATH += $$PWD/../EDD_Engine_Android
    PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Android.a
}
else {
    unix:!macx: LIBS += -L$$DESTDIR -lEDD_Engine_Linux
    INCLUDEPATH += $$PWD/../EDD_Engine_Linux
    DEPENDPATH += $$PWD/../EDD_Engine_Linux
    unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Linux.a
}

