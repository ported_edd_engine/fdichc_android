#include "stdafx.h"
#include "LegacyHart.h"
#include "LegacyHartFlats.h"

#define HOOK_DEPBIN(dds_ptr, db)                     \
    if (dds_ptr->db != NULL)                         \
    {                                                \
        this->db = new Hart_ConvDEPBIN(dds_ptr->db); \
    }


//Hart_ConvFLAT_FILE() - Fills in an FDI FLAT_FILE from DDS FLAT_FILE
Hart_ConvFLAT_FILE::Hart_ConvFLAT_FILE( ::FLAT_FILE *pFlatFile )
{
	id = pFlatFile->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatFile->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatFile->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatFile->masks, nsEDDEngine::ITYPE_FILE);

	CLegacyHart::Fill_STRING(&help, &pFlatFile->help);
	CLegacyHart::Fill_STRING(&label, &pFlatFile->label);

	CLegacyHart::Fill_MEMBER_LIST(&members, &pFlatFile->members);

	if(pFlatFile->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvFILE_DEPBIN( pFlatFile->depbin );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_FILE::attr_map[] = {
	nsEDDEngine::AttributeName::members,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::validity,
	(nsEDDEngine::AttributeName) -1,	//FILE_DEBUG
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//Hart_ConvFILE_DEPBIN() - Fills in an FDI FILE_DEPBIN from DDS FILE_DEPBIN
Hart_ConvFILE_DEPBIN::Hart_ConvFILE_DEPBIN( ::FILE_DEPBIN *pFileDepbin )
{
	HOOK_DEPBIN( pFileDepbin, db_help)
	HOOK_DEPBIN( pFileDepbin, db_label)
	HOOK_DEPBIN( pFileDepbin, db_members)
}


//Hart_ConvFLAT_LIST(): Fills in FDI FLAT_LIST from DDS FLAT_LIST.
Hart_ConvFLAT_LIST::Hart_ConvFLAT_LIST( ::FLAT_LIST *pFlatList )
{
	id = pFlatList->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatList->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatList->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatList->masks, nsEDDEngine::ITYPE_LIST);

	CLegacyHart::Fill_STRING(&help, &pFlatList->help);

	CLegacyHart::Fill_STRING(&label, &pFlatList->label);
		
	valid = CLegacyHart::SetBoolean(pFlatList->valid);

	type = pFlatList->type;

	// Convert the OP_REF count_ref
	CLegacyHart::Fill_OP_REF(&this->count_ref, &pFlatList->count_ref);

	capacity = pFlatList->capacity;
	count = pFlatList->count;

	if(pFlatList->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvLIST_DEPBIN( pFlatList->depbin );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_LIST::attr_map[] = {
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::validity,
	nsEDDEngine::AttributeName::type_definition,
	nsEDDEngine::AttributeName::count,
	nsEDDEngine::AttributeName::capacity,
	(nsEDDEngine::AttributeName) -1,	// DEBUG_ID
	(nsEDDEngine::AttributeName) -1,	// LIST_FIRST_ID
	(nsEDDEngine::AttributeName) -1,	// LIST_LAST_ID
	nsEDDEngine::AttributeName::count_ref,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//Hart_ConvLIST_DEPBIN(): Fills in FDI LIST_DEPBIN from DDS LIST_DEPBIN.
Hart_ConvLIST_DEPBIN::Hart_ConvLIST_DEPBIN( ::LIST_DEPBIN* pListDepbin )
{
	HOOK_DEPBIN( pListDepbin, db_help)
	HOOK_DEPBIN( pListDepbin, db_label)
	HOOK_DEPBIN( pListDepbin, db_type)
	HOOK_DEPBIN( pListDepbin, db_capacity)
	HOOK_DEPBIN( pListDepbin, db_count)
}


//Hart_ConvFLAT_RESP_CODE(): Fills in an FDI FLAT_RESP_CODE from DDS FLAT_RESP_CODE.
Hart_ConvFLAT_RESP_CODE::Hart_ConvFLAT_RESP_CODE( ::FLAT_RESP_CODE* pFlatRespCode )
{
	id = pFlatRespCode->id;
	
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatRespCode->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatRespCode->masks, nsEDDEngine::ITYPE_RESP_CODES);

	CLegacyHart::Fill_RESPONSE_CODES(&member, &pFlatRespCode->member);	

	if(pFlatRespCode->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvRESP_CODE_DEPBIN( pFlatRespCode->depbin );
	}
}

const nsEDDEngine::AttributeName
Hart_ConvFLAT_RESP_CODE::attr_map[] = {
	nsEDDEngine::AttributeName::response_codes,
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//Hart_ConvRESP_CODE_DEPBIN(): Fills in an FDI RESP_CODE_DEPBIN from DDS RESP_CODE_DEPBIN.
Hart_ConvRESP_CODE_DEPBIN::Hart_ConvRESP_CODE_DEPBIN( ::RESP_CODE_DEPBIN *pRespCodeDepbin)
{
	HOOK_DEPBIN( pRespCodeDepbin, db_member )
}


//Hart_ConvFLAT_COMMAND(): Fills in FDI FLAT_COMMAND from DDS FALT_COMMAND
Hart_ConvFLAT_COMMAND::Hart_ConvFLAT_COMMAND( ::FLAT_COMMAND *pFlatCommand )
{
	id = pFlatCommand->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatCommand->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatCommand->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatCommand->masks, nsEDDEngine::ITYPE_COMMAND);
	
	number = pFlatCommand->number;

	switch(pFlatCommand->oper)
	{
	case READ_OPERATION:
		oper = nsEDDEngine::CommandOperation::FDI_READ_OPERATION;
		break;
	case WRITE_OPERATION:
		oper = nsEDDEngine::CommandOperation::FDI_WRITE_OPERATION;
		break;
	case COMMAND_OPERATION:
		oper = nsEDDEngine::CommandOperation::FDI_COMMAND_OPERATION;
		break;
	}
	 
	if(pFlatCommand->trans.count != 0)
	{
		trans.count = pFlatCommand->trans.count;
		trans.limit = pFlatCommand->trans.count;

		trans.list = new nsEDDEngine::TRANSACTION[pFlatCommand->trans.count];

		for(int i = 0; i < pFlatCommand->trans.count; i++)
		{
			trans.list[i].number = pFlatCommand->trans.list[i].number; 

			CLegacyHart::Fill_DATA_ITEM_LIST(&trans.list[i].reply, &pFlatCommand->trans.list[i].reply);
			CLegacyHart::Fill_DATA_ITEM_LIST(&trans.list[i].request, &pFlatCommand->trans.list[i].request);
			
			trans.list[i].resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST;

			CLegacyHart::Fill_RESPONSE_CODES(&trans.list[i].resp_codes.response_codes.resp_code_list, &pFlatCommand->trans.list[i].rcodes);
		}
	}

	resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_LIST;
	
	CLegacyHart::Fill_RESPONSE_CODES(&resp_codes.response_codes.resp_code_list, &pFlatCommand->resp_codes);
	
	// Below members are not available in DDS FLAT_COMMAND

	//pFdiFlatCommand->block_b = pFlatCommand->
	//pFdiFlatCommand->index = pFlatCommand->
	//pFdiFlatCommand->connection = pFlatCommand->
	//pFdiFlatCommand->slot = pFlatCommand->

	if(pFlatCommand->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvCOMMAND_DEPBIN( pFlatCommand->depbin);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_COMMAND::attr_map[] = {
	nsEDDEngine::AttributeName::command_number,
	nsEDDEngine::AttributeName::operation,
	nsEDDEngine::AttributeName::transaction,
	nsEDDEngine::AttributeName::response_codes,
	(nsEDDEngine::AttributeName) -1,	//COMMAND_DEBUG_ID
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//Hart_ConvCOMMAND_DEPBIN(): Fills in an FDI COMMAND_DEPBIN from DDS COMMAND_DEPBIN
Hart_ConvCOMMAND_DEPBIN::Hart_ConvCOMMAND_DEPBIN( ::COMMAND_DEPBIN * pCommandDepbin )
{
	HOOK_DEPBIN( pCommandDepbin, db_number)
	HOOK_DEPBIN( pCommandDepbin, db_oper)
	HOOK_DEPBIN( pCommandDepbin, db_resp_codes)
	HOOK_DEPBIN( pCommandDepbin, db_trans)
}


// Hart_ConvFLAT_COLLECTION() - Fills in an FDI FLAT_COLLECTION table from DDS FLAT_COLLECTION table.
Hart_ConvFLAT_COLLECTION::Hart_ConvFLAT_COLLECTION( ::FLAT_COLLECTION *pFlatCollection )
{
	id = pFlatCollection->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatCollection->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatCollection->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatCollection->masks, nsEDDEngine::ITYPE_COLLECTION);

	subtype =  CLegacyHart::Set_ITEM_TYPE(pFlatCollection->subtype);

	CLegacyHart::Fill_OP_MEMBER_LIST(&op_members, &pFlatCollection->members);

	CLegacyHart::Fill_STRING(&help, &pFlatCollection->help);
	
	CLegacyHart::Fill_STRING(&label, &pFlatCollection->label);

	valid = CLegacyHart::SetBoolean(pFlatCollection->valid);

	if(pFlatCollection->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvCOLLECTION_DEPBIN( pFlatCollection->depbin);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_COLLECTION::attr_map[] = {
	nsEDDEngine::AttributeName::members,
	nsEDDEngine::AttributeName::label,
	nsEDDEngine::AttributeName::help,
	nsEDDEngine::AttributeName::validity,
	(nsEDDEngine::AttributeName) -1,	//COLLECTION_DEBUG_ID
	nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

// Hart_ConvCOLLECTION_DEPBIN(): Fills in an FDI COLLECTION_DEPBIN from DDS COLLECTION_DEPBIN
Hart_ConvCOLLECTION_DEPBIN::Hart_ConvCOLLECTION_DEPBIN( ::COLLECTION_DEPBIN *pCollectionDepbin )
{
	HOOK_DEPBIN( pCollectionDepbin, db_members )
	HOOK_DEPBIN( pCollectionDepbin, db_help )
	HOOK_DEPBIN( pCollectionDepbin, db_label )
}


//Hart_ConvFLAT_ITEM_ARRAY() - Fills in an FDI FLAT_ITEM_ARRAY from DDS FLAT_ITEM_ARRAY
Hart_ConvFLAT_ITEM_ARRAY::Hart_ConvFLAT_ITEM_ARRAY( ::FLAT_ITEM_ARRAY *pFlatItemArray )
{
	id = pFlatItemArray->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatItemArray->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatItemArray->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatItemArray->masks, nsEDDEngine::ITYPE_ITEM_ARRAY);

	subtype  = CLegacyHart::Set_ITEM_TYPE(pFlatItemArray->subtype);

	if(pFlatItemArray->elements.count != 0)
	{
		elements.count	= pFlatItemArray->elements.count;
		elements.limit	= pFlatItemArray->elements.count;
		elements.list	= new nsEDDEngine::ITEM_ARRAY_ELEMENT[pFlatItemArray->elements.count];

		for(int i = 0; i < pFlatItemArray->elements.count; i++)
		{
			
			if(pFlatItemArray->elements.list[i].evaled & IA_DESC_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_DESC_EVALED; 
				CLegacyHart::Fill_STRING(&elements.list[i].desc, &pFlatItemArray->elements.list[i].desc);
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_HELP_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_HELP_EVALED; 
				CLegacyHart::Fill_STRING(&elements.list[i].help, &pFlatItemArray->elements.list[i].help);
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_INDEX_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_INDEX_EVALED; 
				elements.list[i].index		=  pFlatItemArray->elements.list[i].index;
			}

			if(pFlatItemArray->elements.list[i].evaled & IA_REF_EVALED)
			{
				elements.list[i].evaled |= nsEDDEngine::FDI_IA_REF_EVALED; 
				elements.list[i].ref.id		=  pFlatItemArray->elements.list[i].ref.id;

				elements.list[i].ref.type  = CLegacyHart::Set_ITEM_TYPE(pFlatItemArray->elements.list[i].ref.type);
			}
			
		}
	}

	CLegacyHart::Fill_STRING(&help, &pFlatItemArray->help);

	CLegacyHart::Fill_STRING(&label, &pFlatItemArray->label);

    valid = CLegacyHart::SetBoolean(pFlatItemArray->valid);

	if(pFlatItemArray->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvITEM_ARRAY_DEPBIN( pFlatItemArray->depbin);
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_ITEM_ARRAY::attr_map[] = {
		nsEDDEngine::AttributeName::elements,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		(nsEDDEngine::AttributeName) -1,	//ITEM_ARRAY_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//Hart_ConvITEM_ARRAY_DEPBIN() - Fills in an FDI ITEM_ARRAY_DEPBIN from DDS ITEM_ARRAY_DEPBIN
Hart_ConvITEM_ARRAY_DEPBIN::Hart_ConvITEM_ARRAY_DEPBIN( ::ITEM_ARRAY_DEPBIN * pItemArrayDepbin )
{
	HOOK_DEPBIN( pItemArrayDepbin, db_elements)
	HOOK_DEPBIN( pItemArrayDepbin, db_help)
	HOOK_DEPBIN( pItemArrayDepbin, db_label)
	HOOK_DEPBIN( pItemArrayDepbin, db_valid )
}


// Hart_ConvFLAT_VAR() - Fills in an FDI FLAT_VAR from DDS FLAT_VAR
Hart_ConvFLAT_VAR::Hart_ConvFLAT_VAR( ::FLAT_VAR* pFlatVar )
{
	id = pFlatVar->id;

	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatVar->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatVar->masks, nsEDDEngine::ITYPE_VARIABLE);

    // The following masks are filled so that the defaults can be retrieved even though
    // the fields are not available in legacy hart
    // The constructor FLAT_VAR(): ItemBase(ITYPE_VARIABLE) fills the values with defaults.
    masks.attr_avail.insert(nsEDDEngine::AttributeName::height);
    masks.attr_avail.insert(nsEDDEngine::AttributeName::width);
    masks.attr_avail.insert(nsEDDEngine::AttributeName::visibility);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::private_attr);
	masks.attr_avail.insert(nsEDDEngine::AttributeName::time_scale);

	class_attr = CLegacyHart::SetClassType(pFlatVar->class_attr);

	handling = CLegacyHart::SetHandling(pFlatVar->handling);

	CLegacyHart::Fill_STRING(&help, &pFlatVar->help);
	CLegacyHart::Fill_STRING(&label, &pFlatVar->label);

	CLegacyHart::Fill_EDD_TYPE_SIZE(&type_size, pFlatVar->type_size); 

	CLegacyHart::Fill_STRING(&display, &pFlatVar->display);
	CLegacyHart::Fill_STRING(&edit, &pFlatVar->edit);

	CLegacyHart::Fill_ENUM_VALUE_LIST(&enums, &pFlatVar->enums);

	indexed = pFlatVar->index_item_array;

	if ( pFlatVar->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatVar->resp_codes;
	}

	CLegacyHart::Fill_EXPR(&default_value, &pFlatVar->default_value);

	PostProcessDefaultValue( this );

	if(pFlatVar->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvVAR_DEPBIN( pFlatVar );
	}

	if(pFlatVar->actions != NULL)
	{
		CLegacyHart::Fill_ACTION_LIST(&post_edit_act, &pFlatVar->actions->post_edit_act);

		CLegacyHart::Fill_ACTION_LIST(&post_read_act, &pFlatVar->actions->post_read_act);

		CLegacyHart::Fill_ACTION_LIST(&post_write_act, &pFlatVar->actions->post_write_act);

		CLegacyHart::Fill_ACTION_LIST(&refresh_act, &pFlatVar->actions->refresh_act);

		CLegacyHart::Fill_ACTION_LIST(&pre_edit_act, &pFlatVar->actions->pre_edit_act);

		CLegacyHart::Fill_ACTION_LIST(&pre_read_act, &pFlatVar->actions->pre_read_act);

		CLegacyHart::Fill_ACTION_LIST(&pre_write_act, &pFlatVar->actions->pre_write_act);
	}

	if(pFlatVar->misc != NULL) 
	{
		CLegacyHart::Fill_STRING(&constant_unit, &pFlatVar->misc->unit);

		valid = CLegacyHart::SetBoolean(pFlatVar->misc->valid);

		CLegacyHart::Fill_RANGE_DATA_LIST(&min_val, &pFlatVar->misc->min_val);

		CLegacyHart::Fill_RANGE_DATA_LIST(&max_val, &pFlatVar->misc->max_val);	

		CLegacyHart::Fill_EXPR(&scaling_factor, &pFlatVar->misc->scale);

		CLegacyHart::Fill_STRING(&symbol_name, &pFlatVar->misc->symbol_name);

		CLegacyHart::Fill_STRING(&time_format, &pFlatVar->misc->time_format);

		//time_scale = pFlatVar->misc->time_scale;
		switch(pFlatVar->misc->time_scale)
		{
            case VAR_TIME_SCALE_SECONDS:
                 time_scale = nsEDDEngine::FDI_TIME_SCALE_SECONDS;
                 break;
            case VAR_TIME_SCALE_MINUTES:
                 time_scale = nsEDDEngine::FDI_TIME_SCALE_MINUTES;
                 break;
            case VAR_TIME_SCALE_HOURS:
                 time_scale = nsEDDEngine::FDI_TIME_SCALE_HOURS;
                 break;
            default:
                 time_scale = nsEDDEngine::FDI_TIME_SCALE_NONE;
                 break;
         }

	}


	// Below attributes are not available in DDS FLAT_VAR
	//Fill_ACTION_LIST(&pFdiFlatVar->refresh_act, &pFlatVar->);

	//Fill_EXPR(pFdiFlatVar->initial_val, pFlatVar-> initial_val);

	
}

void Hart_ConvFLAT_VAR::PostProcessDefaultValue( nsEDDEngine::FLAT_VAR* pFlatVar )
{
	pFlatVar->default_value.SetExprAsPerVariableType(pFlatVar->type_size);
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_VAR::attr_map[] = {
		nsEDDEngine::AttributeName::class_attr,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::constant_unit,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		(nsEDDEngine::AttributeName) -1,	// READ_TIME_OUT
		(nsEDDEngine::AttributeName) -1,	// WRITE_TIME_OUT
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::pre_read_actions,
		nsEDDEngine::AttributeName::post_read_actions,
		nsEDDEngine::AttributeName::pre_write_actions,
		nsEDDEngine::AttributeName::post_write_actions,
		nsEDDEngine::AttributeName::pre_edit_actions,
		nsEDDEngine::AttributeName::post_edit_actions,
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::variable_type,
		nsEDDEngine::AttributeName::display_format,
		nsEDDEngine::AttributeName::edit_format,
		nsEDDEngine::AttributeName::min_value,
		nsEDDEngine::AttributeName::max_value,
		nsEDDEngine::AttributeName::scaling_factor,
		nsEDDEngine::AttributeName::enumerations,
		nsEDDEngine::AttributeName::indexed,
		nsEDDEngine::AttributeName::default_value,
		nsEDDEngine::AttributeName::refresh_actions,
		(nsEDDEngine::AttributeName) -1,	// VAR_DEBUG_ID
		(nsEDDEngine::AttributeName) -1,	// VAR_POST_RQST_ACT_ID
		nsEDDEngine::AttributeName::post_user_actions,
		nsEDDEngine::AttributeName::time_format,
		nsEDDEngine::AttributeName::time_scale,
		
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//Hart_ConvVAR_DEPBIN() - Fills in an FDI VAR_DEPBIN from DDS VAR_DEPBIN
Hart_ConvVAR_DEPBIN::Hart_ConvVAR_DEPBIN( ::FLAT_VAR *pFlatVar )
{
	HOOK_DEPBIN( pFlatVar->depbin, db_class )
	HOOK_DEPBIN( pFlatVar->depbin, db_handling )
	HOOK_DEPBIN( pFlatVar->depbin, db_help )
	HOOK_DEPBIN( pFlatVar->depbin, db_label )
	HOOK_DEPBIN( pFlatVar->depbin, db_type_size )
	HOOK_DEPBIN( pFlatVar->depbin, db_display )
	HOOK_DEPBIN( pFlatVar->depbin, db_edit )
	HOOK_DEPBIN( pFlatVar->depbin, db_enums )

	if( pFlatVar->depbin->db_index_item_array != NULL )
	{
		db_indexed = new Hart_ConvDEPBIN( pFlatVar->depbin->db_index_item_array );
	}

	if( pFlatVar->depbin->db_resp_codes != NULL )
	{
		db_response_codes = new Hart_ConvDEPBIN( pFlatVar->depbin->db_resp_codes );
	}

	HOOK_DEPBIN( pFlatVar->depbin, db_default_value )

	// action depbins
	if((pFlatVar->actions != NULL) && (pFlatVar->actions->depbin != NULL))
	{
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_edit_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_edit_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_read_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_read_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_pre_write_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_post_write_act )
		HOOK_DEPBIN( pFlatVar->actions->depbin, db_refresh_act )
	}


	// misc depbins
	if((pFlatVar->misc != NULL) && pFlatVar->misc->depbin != NULL)
	{
		if( pFlatVar->misc->depbin->db_unit != NULL )
		{
			db_constant_unit = new Hart_ConvDEPBIN( pFlatVar->misc->depbin->db_unit );
		}
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_min_val )
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_max_val )
		if( pFlatVar->misc->depbin->db_scale != NULL )
		{
			db_scaling_factor = new Hart_ConvDEPBIN( pFlatVar->misc->depbin->db_scale );
		}
		HOOK_DEPBIN( pFlatVar->misc->depbin, db_valid )
	}
}


//Hart_ConvFLAT_ARRAY() - Fills in an FDI FLAT_ARRAY from DDS FLAT_ARRAY
Hart_ConvFLAT_ARRAY::Hart_ConvFLAT_ARRAY( ::FLAT_ARRAY *pFlatArray )
{
	id = pFlatArray->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatArray->symbol_name);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatArray->masks, nsEDDEngine::ITYPE_ARRAY);

	num_of_elements = pFlatArray->num_of_elements;

	CLegacyHart::Fill_STRING(&help, &pFlatArray->help);
	CLegacyHart::Fill_STRING(&label, &pFlatArray->label);

	valid = CLegacyHart::SetBoolean(pFlatArray->valid);
	
	type = pFlatArray->type;

	if ( pFlatArray->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatArray->resp_codes;
	}
	

	if(pFlatArray->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvARRAY_DEPBIN( pFlatArray->depbin);
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_ARRAY::attr_map[] = {
		nsEDDEngine::AttributeName::response_codes,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::type_definition,
		nsEDDEngine::AttributeName::number_of_elements,
		(nsEDDEngine::AttributeName) -1,	//ARRAY_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

//Hart_ConvARRAY_DEPBIN() - Fills in an FDI ARRAY_DEPBIN from DDS ARRAY_DEPBIN
Hart_ConvARRAY_DEPBIN::Hart_ConvARRAY_DEPBIN( ::ARRAY_DEPBIN *pArrayDepbin )
{
	HOOK_DEPBIN( pArrayDepbin, db_num_of_elements )
	HOOK_DEPBIN( pArrayDepbin, db_help )
	HOOK_DEPBIN( pArrayDepbin, db_label )
	HOOK_DEPBIN( pArrayDepbin, db_valid )
	HOOK_DEPBIN( pArrayDepbin, db_type )
	HOOK_DEPBIN( pArrayDepbin, db_resp_codes )
}


//Hart_ConvFLAT_CHART() - Fills in an FDI FLAT_CHART from DDS FLAT_CHART
Hart_ConvFLAT_CHART::Hart_ConvFLAT_CHART( ::FLAT_CHART *pFlatChart )
{
	id = pFlatChart->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatChart->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatChart->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatChart->masks, nsEDDEngine::ITYPE_CHART);

	CLegacyHart::Fill_STRING(&help, &pFlatChart->help);
	CLegacyHart::Fill_STRING(&label, &pFlatChart->label);
	
	CLegacyHart::Fill_MEMBER_LIST(&members, &pFlatChart->members);

	height	=  CLegacyHart::SetDisplaySize(pFlatChart->height);
	width	=  CLegacyHart::SetDisplaySize(pFlatChart->width);

	cycle_time	= pFlatChart->cycle_time;
	length		= pFlatChart->length;

	switch(pFlatChart->type)
	{
	case GAUGE_CTYPE:
		type = nsEDDEngine::CTYPE_GAUGE;
		break;
	case HORIZ_BAR_CTYPE:
		type = nsEDDEngine::CTYPE_HORIZ_BAR;
		break;
	case SCOPE_CTYPE:
		type = nsEDDEngine::CTYPE_SCOPE;
		break;
	case STRIP_CTYPE:
		type = nsEDDEngine::CTYPE_STRIP;
		break;
	case SWEEP_CTYPE:
		type = nsEDDEngine::CTYPE_SWEEP;
		break;
	case VERT_BAR_CTYPE:
		type = nsEDDEngine::CTYPE_VERT_BAR;
		break;
	default:
		break;
	}


	valid		= CLegacyHart::SetBoolean(pFlatChart->valid);
	
	if(pFlatChart->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvCHART_DEPBIN( pFlatChart->depbin );
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_CHART::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::chart_type,
		nsEDDEngine::AttributeName::length,
		nsEDDEngine::AttributeName::cycle_time,
		nsEDDEngine::AttributeName::members,
		(nsEDDEngine::AttributeName) -1,	//CHART_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


//Hart_ConvARRAY_DEPBIN() - Fills in an FDI ARRAY_DEPBIN from DDS ARRAY_DEPBIN
Hart_ConvCHART_DEPBIN::Hart_ConvCHART_DEPBIN( ::CHART_DEPBIN *pChartDepbin)
{
	HOOK_DEPBIN( pChartDepbin, db_help )
	HOOK_DEPBIN( pChartDepbin, db_label )
	HOOK_DEPBIN( pChartDepbin, db_members )
	HOOK_DEPBIN( pChartDepbin, db_cycle_time )
	HOOK_DEPBIN( pChartDepbin, db_height )
	HOOK_DEPBIN( pChartDepbin, db_width )
	HOOK_DEPBIN( pChartDepbin, db_length )
	HOOK_DEPBIN( pChartDepbin, db_type )
	HOOK_DEPBIN( pChartDepbin, db_valid )
}

Hart_ConvFLAT_GRAPH::Hart_ConvFLAT_GRAPH( ::FLAT_GRAPH *pFlatGraph )
{
	id = pFlatGraph->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatGraph->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatGraph->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatGraph->masks, nsEDDEngine::ITYPE_GRAPH);

	CLegacyHart::Fill_STRING(&help, &pFlatGraph->help);
	CLegacyHart::Fill_STRING(&label, &pFlatGraph->label);
	
	CLegacyHart::Fill_MEMBER_LIST(&members, &pFlatGraph->members);

	height	=  CLegacyHart::SetDisplaySize(pFlatGraph->height);
	width	=  CLegacyHart::SetDisplaySize(pFlatGraph->width);

	x_axis	=	pFlatGraph->x_axis;
	valid		= CLegacyHart::SetBoolean(pFlatGraph->valid);
	cycle_time	= pFlatGraph->cycle_time;

	
	if(pFlatGraph->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvGRAPH_DEPBIN( pFlatGraph->depbin );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_GRAPH::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::x_axis,
		nsEDDEngine::AttributeName::members,
		(nsEDDEngine::AttributeName) -1,	//GRAPH_DEBUG_ID
		nsEDDEngine::AttributeName::cycle_time,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvGRAPH_DEPBIN::Hart_ConvGRAPH_DEPBIN( ::GRAPH_DEPBIN *pGraphDepbin)
{
	HOOK_DEPBIN(pGraphDepbin, db_help)
	HOOK_DEPBIN(pGraphDepbin, db_label)
	HOOK_DEPBIN(pGraphDepbin, db_members)
	HOOK_DEPBIN(pGraphDepbin, db_cycle_time)
	HOOK_DEPBIN(pGraphDepbin, db_height)
	HOOK_DEPBIN(pGraphDepbin, db_width)
	HOOK_DEPBIN(pGraphDepbin, db_x_axis)
	HOOK_DEPBIN(pGraphDepbin, db_valid)
}


Hart_ConvFLAT_AXIS::Hart_ConvFLAT_AXIS( ::FLAT_AXIS *pFlatAxis )
{
	id = pFlatAxis->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatAxis->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatAxis->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatAxis->masks, nsEDDEngine::ITYPE_AXIS);

	CLegacyHart::Fill_STRING(&help, &pFlatAxis->help);
	CLegacyHart::Fill_STRING(&label, &pFlatAxis->label);

	CLegacyHart::Fill_EXPR(&min_axis, &pFlatAxis->min_value);
	CLegacyHart::Fill_OP_REF(&this->min_axis_ref, &pFlatAxis->min_value_ref);

	CLegacyHart::Fill_EXPR(&max_axis, &pFlatAxis->max_value);
	CLegacyHart::Fill_OP_REF(&this->max_axis_ref, &pFlatAxis->max_value_ref);

	switch(pFlatAxis->scaling)
	{
	case LINEAR_SCALE:
		scaling = nsEDDEngine::FDI_LINEAR_SCALE;
		break;
	case LOG_SCALE:
		scaling = nsEDDEngine::FDI_LOG_SCALE;
		break;
	default:
		break;
	}

	CLegacyHart::Fill_STRING(&constant_unit, &pFlatAxis->constant_unit);

	if(pFlatAxis->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvAXIS_DEPBIN( pFlatAxis->depbin );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_AXIS::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		(nsEDDEngine::AttributeName) -1,	//AXIS_VALID
		nsEDDEngine::AttributeName::min_value, //min_axis,
		nsEDDEngine::AttributeName::max_value, //max_axis,
		nsEDDEngine::AttributeName::scaling,
		nsEDDEngine::AttributeName::constant_unit,
		(nsEDDEngine::AttributeName) -1,	//AXIS_DEBUG
		(nsEDDEngine::AttributeName) -1,	//AXIS_VIEW_MIN_VAL
		(nsEDDEngine::AttributeName) -1,	//AXIS_VIEW_MAX_VAL
		nsEDDEngine::AttributeName::max_value_ref,
		nsEDDEngine::AttributeName::min_value_ref,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvAXIS_DEPBIN::Hart_ConvAXIS_DEPBIN( ::AXIS_DEPBIN *pAxisDepbin)
{
	HOOK_DEPBIN( pAxisDepbin, db_help );
	HOOK_DEPBIN( pAxisDepbin, db_label );
	if( pAxisDepbin->db_min_value != NULL )
	{
		this->db_min_axis = new Hart_ConvDEPBIN( pAxisDepbin->db_min_value );
	}
	if( pAxisDepbin->db_max_value != NULL )
	{
		this->db_max_axis = new Hart_ConvDEPBIN( pAxisDepbin->db_max_value );
	}
	HOOK_DEPBIN( pAxisDepbin, db_scaling );
	HOOK_DEPBIN( pAxisDepbin, db_constant_unit );
}


Hart_ConvFLAT_WAVEFORM::Hart_ConvFLAT_WAVEFORM( ::FLAT_WAVEFORM *pFlatWaveform )
{
	id = pFlatWaveform->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatWaveform->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatWaveform->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatWaveform->masks, nsEDDEngine::ITYPE_WAVEFORM);

	CLegacyHart::Fill_STRING(&help, &pFlatWaveform->help);
	CLegacyHart::Fill_STRING(&label, &pFlatWaveform->label);

	valid = CLegacyHart::SetBoolean(pFlatWaveform->valid);
	emphasis = CLegacyHart::SetBoolean(pFlatWaveform->emphasis);
	line_type = CLegacyHart::SetLineType(pFlatWaveform->line_type);

	handling = CLegacyHart::SetHandling(pFlatWaveform->handling);

	CLegacyHart::Fill_ACTION_LIST(&init_actions, &pFlatWaveform->init_actions);
	CLegacyHart::Fill_ACTION_LIST(&refresh_actions, &pFlatWaveform->refresh_actions);
	CLegacyHart::Fill_ACTION_LIST(&exit_actions, &pFlatWaveform->exit_actions);

	switch(pFlatWaveform->type)
	{
	case YT_WAVEFORM_TYPE:
		waveform_type = nsEDDEngine::FDI_YT_WAVEFORM_TYPE;
		break;
	case XY_WAVEFORM_TYPE:
		waveform_type = nsEDDEngine::FDI_XY_WAVEFORM_TYPE;
		break;
	case HORZ_WAVEFORM_TYPE:
		waveform_type = nsEDDEngine::FDI_HORZ_WAVEFORM_TYPE;
		break;
	case VERT_WAVEFORM_TYPE:
		waveform_type = nsEDDEngine::FDI_VERT_WAVEFORM_TYPE;
		break;
	default:
		break;
	}
	
	CLegacyHart::Fill_EXPR(&x_initial, &pFlatWaveform->x_initial);
	CLegacyHart::Fill_EXPR(&x_increment, &pFlatWaveform->x_increment);
	
	number_of_points = pFlatWaveform->number_of_points;

	CLegacyHart::Fill_DATA_ITEM_LIST(&x_values, &pFlatWaveform->x_values);
	CLegacyHart::Fill_DATA_ITEM_LIST(&y_values, &pFlatWaveform->y_values);
	CLegacyHart::Fill_DATA_ITEM_LIST(&key_x_values, &pFlatWaveform->key_x_values);
	CLegacyHart::Fill_DATA_ITEM_LIST(&key_y_values, &pFlatWaveform->key_y_values);

	line_color	= pFlatWaveform->line_color;
	y_axis		= pFlatWaveform->y_axis;

	visibility = nsEDDEngine::Boolean::True; // visibility is not present in Legacy HART, so we always set it to the default value which is true.

	if(pFlatWaveform->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvWAVEFORM_DEPBIN( pFlatWaveform->depbin );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_WAVEFORM::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::emphasis,
		nsEDDEngine::AttributeName::line_type,
		nsEDDEngine::AttributeName::line_color,
		nsEDDEngine::AttributeName::y_axis,
		nsEDDEngine::AttributeName::keypoints_x_values,
		nsEDDEngine::AttributeName::keypoints_y_values,
		nsEDDEngine::AttributeName::waveform_type,
		nsEDDEngine::AttributeName::x_values,
		nsEDDEngine::AttributeName::y_values,
		nsEDDEngine::AttributeName::x_initial,
		nsEDDEngine::AttributeName::x_increment,
		nsEDDEngine::AttributeName::number_of_points,
		nsEDDEngine::AttributeName::init_actions,
		nsEDDEngine::AttributeName::refresh_actions,
		nsEDDEngine::AttributeName::exit_actions,
		(nsEDDEngine::AttributeName) -1,	//WAVEFORM_DEBUG
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvWAVEFORM_DEPBIN::Hart_ConvWAVEFORM_DEPBIN( ::WAVEFORM_DEPBIN *pWaveformDepbin)
{
	HOOK_DEPBIN( pWaveformDepbin, db_help )
    HOOK_DEPBIN( pWaveformDepbin, db_label )
    HOOK_DEPBIN( pWaveformDepbin, db_valid )
    HOOK_DEPBIN( pWaveformDepbin, db_emphasis )
    HOOK_DEPBIN( pWaveformDepbin, db_line_type )
    HOOK_DEPBIN( pWaveformDepbin, db_handling )
    HOOK_DEPBIN( pWaveformDepbin, db_init_actions )
    HOOK_DEPBIN( pWaveformDepbin, db_refresh_actions )
    HOOK_DEPBIN( pWaveformDepbin, db_exit_actions )
    HOOK_DEPBIN( pWaveformDepbin, db_type )
    HOOK_DEPBIN( pWaveformDepbin, db_x_initial )
    HOOK_DEPBIN( pWaveformDepbin, db_x_increment )
    HOOK_DEPBIN( pWaveformDepbin, db_number_of_points )
    HOOK_DEPBIN( pWaveformDepbin, db_x_values )
    HOOK_DEPBIN( pWaveformDepbin, db_y_values )
    HOOK_DEPBIN( pWaveformDepbin, db_key_x_values )
    HOOK_DEPBIN( pWaveformDepbin, db_key_y_values )
    HOOK_DEPBIN( pWaveformDepbin, db_line_color )
    HOOK_DEPBIN( pWaveformDepbin, db_y_axis )
}


Hart_ConvFLAT_SOURCE::Hart_ConvFLAT_SOURCE( ::FLAT_SOURCE *pFlatSource )
{
	id = pFlatSource->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatSource->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatSource->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatSource->masks, nsEDDEngine::ITYPE_SOURCE);

	CLegacyHart::Fill_STRING(&help, &pFlatSource->help);
	CLegacyHart::Fill_STRING(&label, &pFlatSource->label);

	CLegacyHart::Fill_OP_MEMBER_LIST(&op_members, &pFlatSource->members);
	
	emphasis	= CLegacyHart::SetBoolean(pFlatSource->emphasis);
	y_axis		= pFlatSource->y_axis;
	line_type	= CLegacyHart::SetLineType(pFlatSource->line_type);
	line_color	= pFlatSource->line_color;

	CLegacyHart::Fill_ACTION_LIST(&init_actions, &pFlatSource->init_actions);
	CLegacyHart::Fill_ACTION_LIST(&refresh_actions, &pFlatSource->refresh_actions);
	CLegacyHart::Fill_ACTION_LIST(&exit_actions, &pFlatSource->exit_actions);

	valid	= CLegacyHart::SetBoolean(pFlatSource->valid);

	if(pFlatSource->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvSOURCE_DEPBIN( pFlatSource->depbin );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_SOURCE::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::emphasis,
		nsEDDEngine::AttributeName::line_type,
		nsEDDEngine::AttributeName::line_color,
		nsEDDEngine::AttributeName::y_axis,
		nsEDDEngine::AttributeName::members,
		(nsEDDEngine::AttributeName) -1,	// SOURCE_DEBUG_ID
		nsEDDEngine::AttributeName::init_actions,
		nsEDDEngine::AttributeName::refresh_actions,
		nsEDDEngine::AttributeName::exit_actions,
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvSOURCE_DEPBIN::Hart_ConvSOURCE_DEPBIN( ::SOURCE_DEPBIN *pSourceDepbin)
{
	HOOK_DEPBIN( pSourceDepbin, db_help)
	HOOK_DEPBIN( pSourceDepbin, db_label)
	HOOK_DEPBIN( pSourceDepbin, db_members)
	HOOK_DEPBIN( pSourceDepbin, db_emphasis)
	HOOK_DEPBIN( pSourceDepbin, db_y_axis)
	HOOK_DEPBIN( pSourceDepbin, db_line_type)
	HOOK_DEPBIN( pSourceDepbin, db_line_color)
	HOOK_DEPBIN( pSourceDepbin, db_init_actions)
	HOOK_DEPBIN( pSourceDepbin, db_refresh_actions)
	HOOK_DEPBIN( pSourceDepbin, db_exit_actions)
	HOOK_DEPBIN( pSourceDepbin, db_valid)
}


Hart_ConvFLAT_GRID::Hart_ConvFLAT_GRID( ::FLAT_GRID *pFlatGrid )
{
	id = pFlatGrid->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatGrid->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatGrid->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatGrid->masks, nsEDDEngine::ITYPE_GRID);

	CLegacyHart::Fill_STRING(&help, &pFlatGrid->help);
	CLegacyHart::Fill_STRING(&label, &pFlatGrid->label);

	CLegacyHart::Fill_VECTOR_LIST(&vectors, &pFlatGrid->vectors);

	height		= CLegacyHart::SetDisplaySize(pFlatGrid->height);
	width		= CLegacyHart::SetDisplaySize(pFlatGrid->width);
	handling	= CLegacyHart::SetHandling(pFlatGrid->handling);
	valid		= CLegacyHart::SetBoolean(pFlatGrid->valid);
	switch(pFlatGrid->orientation)
	{
	case ORIENT_VERT:
		orientation = nsEDDEngine::FDI_ORIENT_VERT;
		break;
	case ORIENT_HORIZ:
		orientation = nsEDDEngine::FDI_ORIENT_HORIZ;
		break;
	default:
		break;
	}

	if(pFlatGrid->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvGRID_DEPBIN( pFlatGrid->depbin );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_GRID::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::height,
		nsEDDEngine::AttributeName::width,
		nsEDDEngine::AttributeName::orientation,
		nsEDDEngine::AttributeName::handling,
		nsEDDEngine::AttributeName::vectors,
		(nsEDDEngine::AttributeName) -1,	//GRID_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvGRID_DEPBIN::Hart_ConvGRID_DEPBIN( ::GRID_DEPBIN *pGridDepbin )
{
	HOOK_DEPBIN( pGridDepbin, db_help)
	HOOK_DEPBIN( pGridDepbin, db_label)
	HOOK_DEPBIN( pGridDepbin, db_vectors)
	HOOK_DEPBIN( pGridDepbin, db_height)
	HOOK_DEPBIN( pGridDepbin, db_width)
	HOOK_DEPBIN( pGridDepbin, db_handling)
	HOOK_DEPBIN( pGridDepbin, db_valid)
	HOOK_DEPBIN( pGridDepbin, db_orientation)
}



Hart_ConvFLAT_IMAGE::Hart_ConvFLAT_IMAGE( ::FLAT_IMAGE * pFlatImage )
{
	id = pFlatImage->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatImage->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatImage->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatImage->masks, nsEDDEngine::ITYPE_IMAGE);

	CLegacyHart::Fill_STRING(&help, &pFlatImage->help);
	CLegacyHart::Fill_STRING(&label, &pFlatImage->label);
	
	image_table_index = pFlatImage->path;

	if(pFlatImage->entry.length != 0)
	{
		entry.data = new(unsigned char[pFlatImage->entry.length]);
		entry.length = pFlatImage->entry.length;
		memcpy(entry.data,
			pFlatImage->entry.data,
			entry.length);
	}
	
	if(masks.attr_avail.isMember(nsEDDEngine::image_id))
	{
		// pFlatImage->path is ulong. convert it to string to assign it to image_id

		wchar_t strImageId[100]={0}; 
		PS_Itow(pFlatImage->path, strImageId, 10);

		image_id = strImageId;
	}
	
	link.id	= pFlatImage->link;
	//link.type is not available in DDS
	valid	= CLegacyHart::SetBoolean(pFlatImage->valid);

	if(pFlatImage->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvIMAGE_DEPBIN( pFlatImage->depbin );
	}

}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_IMAGE::attr_map[] = {
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::link,
		nsEDDEngine::AttributeName::image_id, // IMAGE_PATH_ID
		(nsEDDEngine::AttributeName) -1,	//IMAGE_DEBUG_ID
		nsEDDEngine::AttributeName::image_table_index, //IMAGE_BINARY_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvIMAGE_DEPBIN::Hart_ConvIMAGE_DEPBIN( ::IMAGE_DEPBIN *pImageDepbin )
{
	HOOK_DEPBIN( pImageDepbin, db_help)
	HOOK_DEPBIN( pImageDepbin, db_label)

	if( pImageDepbin->db_path != NULL )
	{
		this->db_image_table_index = new Hart_ConvDEPBIN( pImageDepbin->db_path );
	}

	HOOK_DEPBIN( pImageDepbin, db_link)
	HOOK_DEPBIN( pImageDepbin, db_valid)
}


Hart_ConvFLAT_METHOD::Hart_ConvFLAT_METHOD( ::FLAT_METHOD *pFlatMethod )
{
	id = pFlatMethod->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatMethod->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatMethod->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatMethod->masks, nsEDDEngine::ITYPE_METHOD);

	class_attr = CLegacyHart::SetClassType(pFlatMethod->class_attr);
	def.size	=	pFlatMethod->def.size;

	if(pFlatMethod->def.data != 0)
	{
		def.data	= new wchar_t[pFlatMethod->def.size + 1];
		mbstowcs (def.data, pFlatMethod->def.data, pFlatMethod->def.size + 1);
	}
	 
	CLegacyHart::Fill_STRING(&help, &pFlatMethod->help);
	CLegacyHart::Fill_STRING(&label, &pFlatMethod->label);
	valid = CLegacyHart::SetBoolean(pFlatMethod->valid);
	
	switch(pFlatMethod->type)
	{
	case TYPE_VOID:
		type = nsEDDEngine::MT_VOID;
		break;
	case TYPE_INT__8:
		type = nsEDDEngine::MT_INT_8;
		break;
	case TYPE_INT_16:
		type = nsEDDEngine::MT_INT_16;
		break;
	case TYPE_INT_32:
		type = nsEDDEngine::MT_INT_32;
		break;
	case TYPE_INT_64:
		type = nsEDDEngine::MT_INT_64;
		break;
	case TYPE_FLOAT:
		type = nsEDDEngine::MT_FLOAT;
		break;
	case TYPE_DOUBLE:
		type = nsEDDEngine::MT_DOUBLE;
		break;
	case TYPE_UINT__8:
		type = nsEDDEngine::MT_UINT_8;
		break;
	case TYPE_UINT_16:
		type = nsEDDEngine::MT_UINT_16;
		break;
	case TYPE_UINT_32:
		type = nsEDDEngine::MT_UINT_32;
		break;
	case TYPE_UINT_64:
		type = nsEDDEngine::MT_UINT_64;
		break;
	case TYPE_DDSTRING:
		type = nsEDDEngine::MT_DDSTRING;
		break;
	case TYPE_DD_ITEM:
		type = nsEDDEngine::MT_DD_ITEM;
		break;
	default:
		break;
	}
	
	
	CLegacyHart::Fill_STRING(&params, &pFlatMethod->params);

	//access = pFlatMethod-> 

	if(pFlatMethod->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvMETHOD_DEPBIN( pFlatMethod->depbin );
	}
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_METHOD::attr_map[] = {
		nsEDDEngine::AttributeName::class_attr,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::definition,
		nsEDDEngine::AttributeName::validity,
		(nsEDDEngine::AttributeName) -1,	//METHOD_SCOPE_ID
		nsEDDEngine::AttributeName::method_type,
		nsEDDEngine::AttributeName::method_parameters,
		(nsEDDEngine::AttributeName) -1,	//METHOD_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvMETHOD_DEPBIN::Hart_ConvMETHOD_DEPBIN( ::METHOD_DEPBIN *pMethodDepbin)
{
	HOOK_DEPBIN( pMethodDepbin, db_class)
	HOOK_DEPBIN( pMethodDepbin, db_def)
	HOOK_DEPBIN( pMethodDepbin, db_help)
	HOOK_DEPBIN( pMethodDepbin, db_label)
	HOOK_DEPBIN( pMethodDepbin, db_valid)
	HOOK_DEPBIN( pMethodDepbin, db_type)
	HOOK_DEPBIN( pMethodDepbin, db_params)
}



Hart_ConvFLAT_MENU::Hart_ConvFLAT_MENU( ::FLAT_MENU *pFlatMenu)
{
	id = pFlatMenu->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatMenu->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatMenu->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatMenu->masks, nsEDDEngine::ITYPE_MENU);

	CLegacyHart::Fill_STRING(&help, &pFlatMenu->help);
	CLegacyHart::Fill_STRING(&label, &pFlatMenu->label);

	valid = CLegacyHart::SetBoolean( pFlatMenu->valid );

	switch( pFlatMenu->style )
	{
	case WINDOW_STYLE_TYPE:
		style = nsEDDEngine::FDI_WINDOW_STYLE_TYPE;
		break;
	case DIALOG_STYLE_TYPE:
		style = nsEDDEngine::FDI_DIALOG_STYLE_TYPE;
		break;
	case PAGE_STYLE_TYPE:
		style = nsEDDEngine::FDI_PAGE_STYLE_TYPE;
		break;
	case GROUP_STYLE_TYPE:
		style = nsEDDEngine::FDI_GROUP_STYLE_TYPE;
		break;
	case MENU_STYLE_TYPE:
		style = nsEDDEngine::FDI_MENU_STYLE_TYPE;
		break;
	case TABLE_STYLE_TYPE:
		style = nsEDDEngine::FDI_TABLE_STYLE_TYPE;
		break;
	default:
		style = nsEDDEngine::FDI_NO_STYLE_TYPE;
		break;
	}

	if(pFlatMenu->items.count != 0)
	{
		items.count = pFlatMenu->items.count;
		items.limit = pFlatMenu->items.count;

		items.list = new nsEDDEngine::MENU_ITEM[pFlatMenu->items.count];
		
		for(int i = 0; i < pFlatMenu->items.count; i++)
		{
			if(pFlatMenu->items.list[i].qual & READ_ONLY_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_READ_ONLY_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & DISPLAY_VALUE_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_DISPLAY_VALUE_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & REVIEW_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_REVIEW_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & NO_LABEL_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_NO_LABEL_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & NO_UNIT_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_NO_UNIT_ITEM;
			}
			if(pFlatMenu->items.list[i].qual & INLINE_ITEM)
			{
				items.list[i].qual |= nsEDDEngine::FDI_INLINE_ITEM;
			}

			CLegacyHart::Fill_OP_REF_TRAIL(&items.list[i].ref, &pFlatMenu->items.list[i].ref);

			// Change SEPARATORs into COLUMNBREAKs, if not a MENU STYLE
			if ( items.list[i].ref.op_ref_type == nsEDDEngine::STANDARD_TYPE )	// These can never be COMPLEX_TYPE
			{
				if((items.list[i].ref.op_info.type == nsEDDEngine::ITYPE_SEPARATOR) 
					|| (items.list[i].ref.desc_type == nsEDDEngine::ITYPE_SEPARATOR))
				{
					if (style != nsEDDEngine::FDI_MENU_STYLE_TYPE)
					{
						items.list[i].ref.op_info.type = nsEDDEngine::ITYPE_COLUMNBREAK;
						items.list[i].ref.desc_type = nsEDDEngine::ITYPE_COLUMNBREAK;
					}
				}
			}
		}
	}

	//access is not available in DDS
	// pre_edit_act, post_edit_act, pre_read_act, post_read_act, pre_write_act
	// and post_write_act are not available in DDS
	//CLegacyHart::Fill_ACTION_LIST(&pre_edit_act, &pFlatMenu->)
		
	if(pFlatMenu->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvMENU_DEPBIN( pFlatMenu->depbin  );
	}

}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_MENU::attr_map[] = {
		
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::items,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		nsEDDEngine::AttributeName::style,
		(nsEDDEngine::AttributeName) -1,	//MENU_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvMENU_DEPBIN::Hart_ConvMENU_DEPBIN( ::MENU_DEPBIN *pMenuDepbin)
{
	HOOK_DEPBIN( pMenuDepbin, db_label )
	HOOK_DEPBIN( pMenuDepbin, db_help )
	HOOK_DEPBIN( pMenuDepbin, db_items )
	HOOK_DEPBIN( pMenuDepbin, db_valid )
	HOOK_DEPBIN( pMenuDepbin, db_style )
	// Below depbins are not available in DDS
#if 0
	HOOK_DEPBIN( pMenuDepbin, db_access )
	HOOK_DEPBIN( pMenuDepbin, db_pre_edit_act )
	HOOK_DEPBIN( pMenuDepbin, db_post_edit_act )
	HOOK_DEPBIN( pMenuDepbin, db_pre_read_act )
	HOOK_DEPBIN( pMenuDepbin, db_post_read_act )
	HOOK_DEPBIN( pMenuDepbin, db_pre_write_act )
	HOOK_DEPBIN( pMenuDepbin, db_post_write_act )
#endif
}




Hart_ConvFLAT_EDIT_DISPLAY::Hart_ConvFLAT_EDIT_DISPLAY( ::FLAT_EDIT_DISPLAY *pFlatEditDisplay)
{
	id = pFlatEditDisplay->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatEditDisplay->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatEditDisplay->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatEditDisplay->masks, nsEDDEngine::ITYPE_EDIT_DISP);

	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&disp_items, &pFlatEditDisplay->disp_items);
	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&edit_items, &pFlatEditDisplay->edit_items);

	CLegacyHart::Fill_STRING(&help, &pFlatEditDisplay->help);
	CLegacyHart::Fill_STRING(&label, &pFlatEditDisplay->label);

	valid	= CLegacyHart::SetBoolean(pFlatEditDisplay->valid);

	CLegacyHart::Fill_ACTION_LIST(&pre_edit_act, &pFlatEditDisplay->pre_edit_act);
	CLegacyHart::Fill_ACTION_LIST(&post_edit_act, &pFlatEditDisplay->post_edit_act);

	if(pFlatEditDisplay->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvEDIT_DISPLAY_DEPBIN( pFlatEditDisplay->depbin  );
	}


}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_EDIT_DISPLAY::attr_map[] = {
		
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::edit_items,
		nsEDDEngine::AttributeName::display_items,
		nsEDDEngine::AttributeName::pre_edit_actions,
		nsEDDEngine::AttributeName::post_edit_actions,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::validity,
		(nsEDDEngine::AttributeName) -1,	//EDIT_DISPLAY_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvEDIT_DISPLAY_DEPBIN::Hart_ConvEDIT_DISPLAY_DEPBIN( ::EDIT_DISPLAY_DEPBIN *pEditDisplayDepbin )
{
	HOOK_DEPBIN( pEditDisplayDepbin, db_disp_items)
	HOOK_DEPBIN( pEditDisplayDepbin, db_edit_items)
	HOOK_DEPBIN( pEditDisplayDepbin, db_label)
	HOOK_DEPBIN( pEditDisplayDepbin, db_help)
	HOOK_DEPBIN( pEditDisplayDepbin, db_valid)
	HOOK_DEPBIN( pEditDisplayDepbin, db_pre_edit_act)
	HOOK_DEPBIN( pEditDisplayDepbin, db_post_edit_act)
}



Hart_ConvFLAT_REFRESH::Hart_ConvFLAT_REFRESH( ::FLAT_REFRESH *pFlatRefresh )
{

	id = pFlatRefresh->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatRefresh->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatRefresh->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatRefresh->masks, nsEDDEngine::ITYPE_REFRESH);

	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&depend_items, &pFlatRefresh->items.depend_items);
	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&update_items, &pFlatRefresh->items.update_items);

	if(pFlatRefresh->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvREFRESH_DEPBIN( pFlatRefresh->depbin  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_REFRESH::attr_map[] = {
		
		nsEDDEngine::AttributeName::refresh_items,
		(nsEDDEngine::AttributeName) -1,	//REFRESH
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvREFRESH_DEPBIN::Hart_ConvREFRESH_DEPBIN( ::REFRESH_DEPBIN *pRefreshDepbin)
{
	// can't use HOOK_DEPBIN macro as depbin names are different. 
	if(pRefreshDepbin->db_items != NULL)
	{
		this->db_depend_items = new Hart_ConvDEPBIN(pRefreshDepbin->db_items);
		this->db_update_items = new Hart_ConvDEPBIN(pRefreshDepbin->db_items);
	}

}

Hart_ConvFLAT_UNIT::Hart_ConvFLAT_UNIT( ::FLAT_UNIT *pFlatUnit )
{
	id = pFlatUnit->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatUnit->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatUnit->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatUnit->masks, nsEDDEngine::ITYPE_UNIT);

	CLegacyHart::Fill_OP_REF_TRAIL(&var, &pFlatUnit->items.var); 
	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&var_units, &pFlatUnit->items.var_units);
	
	if(pFlatUnit->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvUNIT_DEPBIN( pFlatUnit->depbin  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_UNIT::attr_map[] = {
		
		nsEDDEngine::AttributeName::unit_items,
		(nsEDDEngine::AttributeName) -1,	//UNIT_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvUNIT_DEPBIN::Hart_ConvUNIT_DEPBIN( ::UNIT_DEPBIN *pUnitDepbin )
{
	// can't use HOOK_DEPBIN macro as depbin names are different. 
	if(pUnitDepbin->db_items != NULL)
	{
		this->db_var		= new Hart_ConvDEPBIN(pUnitDepbin->db_items);
		this->db_var_units	= new Hart_ConvDEPBIN(pUnitDepbin->db_items);
	}
}

Hart_ConvFLAT_WAO::Hart_ConvFLAT_WAO( ::FLAT_WAO *pFlatWao )
{
	id = pFlatWao->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatWao->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatWao->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatWao->masks, nsEDDEngine::ITYPE_WAO);

	CLegacyHart::Fill_OP_REF_TRAIL_LIST(&items, &pFlatWao->items);
	
	if(pFlatWao->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvWAO_DEPBIN( pFlatWao->depbin  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_WAO::attr_map[] = {
		
		nsEDDEngine::AttributeName::relation_update_list,
		(nsEDDEngine::AttributeName) -1,	//WAO_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvWAO_DEPBIN::Hart_ConvWAO_DEPBIN( ::WAO_DEPBIN *pWaoDepbin )
{
	HOOK_DEPBIN( pWaoDepbin, db_items)
}


Hart_ConvFLAT_BLOCK::Hart_ConvFLAT_BLOCK( ::FLAT_BLOCK *pFlatBlock )
{
	id = pFlatBlock->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatBlock->symbol_name);
	//CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatBlock->masks);
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatBlock->masks, nsEDDEngine::ITYPE_BLOCK);

	characteristic = pFlatBlock->characteristic;

	CLegacyHart::Fill_MEMBER_LIST(&param, &pFlatBlock->param);
	CLegacyHart::Fill_ITEM_ID_LIST(&item_array, &pFlatBlock->item_array);
	CLegacyHart::Fill_ITEM_ID_LIST(&collect, &pFlatBlock->collect);
	CLegacyHart::Fill_ITEM_ID_LIST(&menu, &pFlatBlock->menu);
	CLegacyHart::Fill_ITEM_ID_LIST(&edit_disp, &pFlatBlock->edit_disp);
	CLegacyHart::Fill_ITEM_ID_LIST(&method, &pFlatBlock->method);
	CLegacyHart::Fill_ITEM_ID_LIST(&unit, &pFlatBlock->unit);
	CLegacyHart::Fill_ITEM_ID_LIST(&refresh, &pFlatBlock->refresh);
	CLegacyHart::Fill_ITEM_ID_LIST(&wao, &pFlatBlock->wao);

#if 0		// Not found in DDS FLAT_BLOCK
	CLegacyHart::Fill_ITEM_ID_LIST(&chart, &pFlatBlock->chart);
	CLegacyHart::Fill_ITEM_ID_LIST(&graph, &pFlatBlock->graph);
	CLegacyHart::Fill_ITEM_ID_LIST(&grid, &pFlatBlock->grid);
	CLegacyHart::Fill_ITEM_ID_LIST(&image, &pFlatBlock->image);
	CLegacyHart::Fill_ITEM_ID_LIST(&source, &pFlatBlock->source);
	CLegacyHart::Fill_ITEM_ID_LIST(&waveform, &pFlatBlock->waveform);
	CLegacyHart::Fill_ITEM_ID_LIST(&file, &pFlatBlock->file);
	CLegacyHart::Fill_ITEM_ID_LIST(&list, &pFlatBlock->list);
#endif
	if(pFlatBlock->depbin != NULL)
	{
		if (depbin) {		// Delete the old one, before overwriting.
			delete depbin;
		}

		depbin  =  new Hart_ConvBLOCK_DEPBIN( pFlatBlock->depbin  );
	}
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_BLOCK::attr_map[] = {
		
		nsEDDEngine::AttributeName::characteristics,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::parameters,
		nsEDDEngine::AttributeName::menus,
		nsEDDEngine::AttributeName::edit_display_items,
		nsEDDEngine::AttributeName::methods,
		nsEDDEngine::AttributeName::refresh_items,
		nsEDDEngine::AttributeName::unit_items,
		nsEDDEngine::AttributeName::write_as_one_items,
		(nsEDDEngine::AttributeName) -1,	//BLOCK_COLLECT_ID
		(nsEDDEngine::AttributeName) -1,	//BLOCK_ITEM_ARRAY_ID
		nsEDDEngine::AttributeName::parameter_lists,
		(nsEDDEngine::AttributeName) -1,	//BLOCK_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};


Hart_ConvBLOCK_DEPBIN::Hart_ConvBLOCK_DEPBIN( ::BLOCK_DEPBIN *pBlockDepbin )
{
	HOOK_DEPBIN( pBlockDepbin, db_characteristic )
	HOOK_DEPBIN( pBlockDepbin, db_help )
	HOOK_DEPBIN( pBlockDepbin, db_label )
	HOOK_DEPBIN( pBlockDepbin, db_param )
	HOOK_DEPBIN( pBlockDepbin, db_item_array )
	HOOK_DEPBIN( pBlockDepbin, db_collect )
	HOOK_DEPBIN( pBlockDepbin, db_menu )
	HOOK_DEPBIN( pBlockDepbin, db_edit_disp )
	HOOK_DEPBIN( pBlockDepbin, db_method )
	HOOK_DEPBIN( pBlockDepbin, db_unit )
	HOOK_DEPBIN( pBlockDepbin, db_refresh )
	HOOK_DEPBIN( pBlockDepbin, db_wao )
	HOOK_DEPBIN( pBlockDepbin, db_chart )
	HOOK_DEPBIN( pBlockDepbin, db_graph )
	//Below depbins are not available in DDS
#if 0
	HOOK_DEPBIN( pBlockDepbin, db_grid )
	HOOK_DEPBIN( pBlockDepbin, db_image )
#endif
	HOOK_DEPBIN( pBlockDepbin, db_axis )
	HOOK_DEPBIN( pBlockDepbin, db_source )
	HOOK_DEPBIN( pBlockDepbin, db_waveform )
	HOOK_DEPBIN( pBlockDepbin, db_file )
	HOOK_DEPBIN( pBlockDepbin, db_list )
}

Hart_ConvFLAT_RECORD::Hart_ConvFLAT_RECORD( ::FLAT_RECORD *pFlatRecord )
{
	id = pFlatRecord->id;
	CLegacyHart::Fill_STRING(&symbol_name, &pFlatRecord->symbol_name);
	
	CLegacyHart::Fill_FLAT_MASK(&masks, &pFlatRecord->masks, nsEDDEngine::ITYPE_RECORD);

	CLegacyHart::Fill_MEMBER_LIST(&members, &pFlatRecord->members);

	CLegacyHart::Fill_STRING(&help, &pFlatRecord->help);

	CLegacyHart::Fill_STRING(&label, &pFlatRecord->label);
	
	if ( pFlatRecord->resp_codes != 0 )	// Only copy if it is real
	{
		resp_codes.eType = nsEDDEngine::RESPONSE_CODES::RESP_CODE_TYPE_REF;
		resp_codes.response_codes.resp_code_ref = pFlatRecord->resp_codes;
	}

	//valid, write_mode, private_attr and visibility are not available in Legacy HART.
	//Default values are assigned. 

	valid = nsEDDEngine::Boolean::True;

	write_mode = (nsEDDEngine::WriteMode)0;   

	private_attr = nsEDDEngine::Boolean::False;

	visibility = nsEDDEngine::Boolean::True;
}

// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const nsEDDEngine::AttributeName
Hart_ConvFLAT_RECORD::attr_map[] = {
		
		nsEDDEngine::AttributeName::members,
		nsEDDEngine::AttributeName::label,
		nsEDDEngine::AttributeName::help,
		nsEDDEngine::AttributeName::response_codes,
		(nsEDDEngine::AttributeName) -1,	//RECORD_DEBUG_ID
		nsEDDEngine::AttributeName::_end_	// Marks the end of the array.
	};

Hart_ConvRECORD_DEPBIN::Hart_ConvRECORD_DEPBIN( ::RECORD_DEPBIN *pRecordDepbin )
{
	HOOK_DEPBIN( pRecordDepbin, db_members )
	HOOK_DEPBIN( pRecordDepbin, db_help )
	HOOK_DEPBIN( pRecordDepbin, db_label )
	HOOK_DEPBIN( pRecordDepbin, db_resp_codes )
	//Below depbins are not available in DDS
#if 0
	HOOK_DEPBIN( pRecordDepbin, db_valid )
	HOOK_DEPBIN( pRecordDepbin, db_private_attr )
	HOOK_DEPBIN( pRecordDepbin, db_visibility )
	HOOK_DEPBIN( pRecordDepbin, db_write_mode )
#endif
}

// Hart_ConvDEPBIN() - Fills in FDI DEPBIN from DDS DEPBIN
Hart_ConvDEPBIN::Hart_ConvDEPBIN( ::DEPBIN *pDepbin )
{
	this->bin_chunk = new unsigned char[pDepbin->bin_size];
	memcpy(this->bin_chunk, pDepbin->bin_chunk, pDepbin->bin_size);
	this->bin_size  = pDepbin->bin_size;

	this->allocated = true;		// Mark that this has been allocated

	if(pDepbin->dep.count != 0)
	{
		this->dep.count = pDepbin->dep.count;
		this->dep.limit = pDepbin->dep.count;
		this->dep.list  = (nsEDDEngine::OP_REF*)malloc(sizeof(nsEDDEngine::OP_REF) * pDepbin->dep.count); 
		
		for(int i = 0; i < pDepbin->dep.count; i++)
		{
			CLegacyHart::Fill_OP_REF(&this->dep.list[i], &pDepbin->dep.list[i]);
		}
	}
}

// Fill_FLAT_MASK() - Fills in FDI FLAT_MASKS from DDS FLAT_MASKS
void CLegacyHart::Fill_FLAT_MASK(nsEDDEngine::FLAT_MASKS* pFdiFlatMask, FLAT_MASKS* pDdiFlatMask, nsEDDEngine::ITEM_TYPE itemType)
{

	Fill_AttributeNameSet(pFdiFlatMask->attr_avail, pDdiFlatMask->attr_avail, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->bin_exists, pDdiFlatMask->bin_exists, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->bin_hooked, pDdiFlatMask->bin_hooked, itemType);
	Fill_AttributeNameSet(pFdiFlatMask->dynamic, pDdiFlatMask->dynamic, itemType);
	/*pFdiFlatMask->attr_avail = pDdiFlatMask->attr_avail;
	pFdiFlatMask->bin_exists = pDdiFlatMask->bin_exists;
	pFdiFlatMask->bin_hooked = pDdiFlatMask->bin_hooked;
	pFdiFlatMask->dynamic = pDdiFlatMask->dynamic;*/
}


// attr_map - Maps the bit number (position in the array) in DDS to the AttributeName in FDI
const
nsEDDEngine::AttributeName* attr_maps[] = {
	nullptr,								// ITYPE_NO_VALUE	= 0,
	Hart_ConvFLAT_VAR::attr_map,			// ITYPE_VARIABLE	= 1,
	Hart_ConvFLAT_COMMAND::attr_map,		// ITYPE_COMMAND	= 2,
	Hart_ConvFLAT_MENU::attr_map,			// ITYPE_MENU		= 3,
	Hart_ConvFLAT_EDIT_DISPLAY::attr_map,	// ITYPE_EDIT_DISP	= 4,
	Hart_ConvFLAT_METHOD::attr_map,			// ITYPE_METHOD		= 5,
	Hart_ConvFLAT_REFRESH::attr_map,		// ITYPE_REFRESH	= 6,
	Hart_ConvFLAT_UNIT::attr_map,			// ITYPE_UNIT		= 7,
	Hart_ConvFLAT_WAO::attr_map,			// ITYPE_WAO		= 8,
	Hart_ConvFLAT_ITEM_ARRAY::attr_map,		// ITYPE_ITEM_ARRAY	= 9,
	Hart_ConvFLAT_COLLECTION::attr_map,		// ITYPE_COLLECTION	= 10,
	nullptr,								// ITYPE_BLOCK_B	= 11,
	Hart_ConvFLAT_BLOCK::attr_map,			// ITYPE_BLOCK		= 12,
	nullptr,								// PROGRAM_ITYPE	= 13 //Not used in FDI
	Hart_ConvFLAT_RECORD::attr_map,			// ITYPE_RECORD		= 14,
	Hart_ConvFLAT_ARRAY::attr_map,			// ITYPE_ARRAY		= 15,
	nullptr,								// ITYPE_VAR_LIST	= 16,
	Hart_ConvFLAT_RESP_CODE::attr_map,		// ITYPE_RESP_CODES	= 17,
	nullptr,								// DOMAIN_ITYPE		= 18  //Not used for FDI
	nullptr,								// ITYPE_MEMBER		= 19,
	Hart_ConvFLAT_FILE::attr_map,			// ITYPE_FILE		= 20,
	Hart_ConvFLAT_CHART::attr_map,			// ITYPE_CHART		= 21,
	Hart_ConvFLAT_GRAPH::attr_map,			// ITYPE_GRAPH		= 22,
	Hart_ConvFLAT_AXIS::attr_map,			// ITYPE_AXIS		= 23,
	Hart_ConvFLAT_WAVEFORM::attr_map,		// ITYPE_WAVEFORM	= 24,
	Hart_ConvFLAT_SOURCE::attr_map,			// ITYPE_SOURCE		= 25,
	Hart_ConvFLAT_LIST::attr_map,			// ITYPE_LIST		= 26,
	Hart_ConvFLAT_GRID::attr_map,			// ITYPE_GRID		= 27,
	Hart_ConvFLAT_IMAGE::attr_map,			// ITYPE_IMAGE		= 28,
	nullptr,								// ITYPE_BLOB		= 29,
	nullptr,								// ITYPE_PLUGIN		= 30,
	nullptr,								// ITYPE_TEMPLATE	= 31,
	nullptr,								// ITYPE_RESERVED	= 32,
	nullptr,								// ITYPE_COMPONENT	= 33,
	nullptr,								// ITYPE_COMPONENT_FOLDER		= 34,
	nullptr,								// ITYPE_COMPONENT_DESCRIPTOR	= 35,
	nullptr,								// ITYPE_COMPONENT_RELATION		= 36,
	};




void CLegacyHart::Fill_Bitmask( unsigned long& bitmask, const nsEDDEngine::AttributeNameSet& ans, nsEDDEngine::ITEM_TYPE itype)
{
	bitmask = 0;

	if(itype == nsEDDEngine::ITYPE_REFRESH)
	{
		// If either of these attributes are found, set a single bit in the DDS bitmask
		if (	(ans.isMember(nsEDDEngine::relation_watch_list))
			||	(ans.isMember(nsEDDEngine::relation_update_list))
			)
		{
			bitmask |= REFRESH_ITEMS;
		}
	}
	else if(itype == nsEDDEngine::ITYPE_UNIT)
	{
		// If either of these attributes are found, set a single bit in the DDS bitmask
		if (	(ans.isMember(nsEDDEngine::relation_watch_variable))
			||	(ans.isMember(nsEDDEngine::relation_update_list))
			)
		{
			bitmask |= UNIT_ITEMS;
		}
	}
	else
	{
		ntcassert2 (itype < (sizeof(attr_maps) / sizeof(attr_maps[0])) );

		const nsEDDEngine::AttributeName *attr_map = attr_maps[itype];

		ntcassert2(attr_map != nullptr);


		// Look for the AttributeName in our attr_map
		for( int i = 0; attr_map[i] != nsEDDEngine::AttributeName::_end_ ; i++)
		{
            if (attr_map[i] != (nsEDDEngine::AttributeName)-1)
            {

			    if (ans.isMember(attr_map[i]))	// If found, set the corresponding bit, and go to next
			    {
				    bitmask |= (1 << i);
			    }
            }
		}

	}
}



void CLegacyHart::Fill_AttributeNameSet( nsEDDEngine::AttributeNameSet& ans, unsigned long bitmask, nsEDDEngine::ITEM_TYPE itype )
{
	ans.clear();

	if(itype == nsEDDEngine::ITYPE_REFRESH)
	{
		if (bitmask & REFRESH_ITEMS )	// Is this bit set?
		{
			ans.insert(nsEDDEngine::relation_watch_list);
			ans.insert(nsEDDEngine::relation_update_list);
		}
	}
	else if(itype == nsEDDEngine::ITYPE_UNIT)
	{
		if (bitmask & UNIT_ITEMS )	// Is this bit set?
		{
			ans.insert(nsEDDEngine::relation_watch_variable);
			ans.insert(nsEDDEngine::relation_update_list);
		}
	}
	else
	{
		ntcassert2 (itype < (sizeof(attr_maps) / sizeof(attr_maps[0])) );

		const nsEDDEngine::AttributeName *attr_map = attr_maps[itype];

		ntcassert2(attr_map != nullptr);

		// For each entry in attr_map, if the bit is set, add the corresponding AttributeName to the set
		for( int i = 0; bitmask && (attr_map[i] != nsEDDEngine::AttributeName::_end_) ; i++)
		{
			unsigned long cur_mask = 1 << i;

			if (bitmask & cur_mask )	// Is this bit set?
			{
				if(attr_map[i] != -1)
				{
					ans.insert(attr_map[i]);
				}
				bitmask &= ~cur_mask;	// Delete the bit
			}
		}
	}
}

