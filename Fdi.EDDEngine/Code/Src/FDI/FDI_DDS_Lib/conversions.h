//
//
//  conversions.h
//
//
// This file contains classes that are used to convert between the EDD Engine Interface classes
// and the old structures down in DDS. This is done here so that the EDD Engine interface does not know
// about the old structures.
//

#pragma once

#include "ddi_lib.h"
#include "nsEDDEngine/Attrs.h"

class FDI_ConvSTRING : public nsEDDEngine::STRING
{
public:
	static void CopyTo(::nsEDDEngine::STRING *pEDDString, ::STRING *pDDSString);
	static void CopyFrom(::STRING *pDDSString, ::nsEDDEngine::STRING *pEDDString);
};

class FDI_ConvEXPR : public nsEDDEngine::EXPR
{
public:
	static void CopyFrom( ::EXPR *pDDSExpr, ::nsEDDEngine::EXPR * pEDDExpr );
	static void CopyTo(::nsEDDEngine::EXPR * pEDDExpr, ::EXPR *pDDSExpr);
};

class FDI_ConvOP_REF_TRAIL : public nsEDDEngine::OP_REF_TRAIL
{
public:
	static void CopyFrom( ::OP_REF_TRAIL *pDDSOpRefTrail, ::nsEDDEngine::OP_REF_TRAIL *pEDDOpRefTrail );
};

class FDI_ConvDESC_REF : public nsEDDEngine::DESC_REF
{
public:
	static void CopyFrom( ::DESC_REF *pDDSDescRef, ::nsEDDEngine::DESC_REF *pEDDDescRef );	
	static void CopyTo(::nsEDDEngine::DESC_REF *pEDDDescRef, ::DESC_REF *pDDSDescRef );
};

class FDI_ConvRESPONSE_CODES : public nsEDDEngine::RESPONSE_CODES
{
public:
	static void CopyFrom( ::RESPONSE_CODES *pDDSResp_Codes, ::nsEDDEngine::RESPONSE_CODES *pEDDResp_Codes);
};


class FDI_ConvRESPONSE_CODE_LIST : public nsEDDEngine::RESPONSE_CODE_LIST
{
public:
	static void CopyFrom(::RESPONSE_CODE_LIST *pDDSRespCodeList, ::nsEDDEngine::RESPONSE_CODE_LIST *pEDDRespCodeList );
};


class FDI_ConvMEMBER_LIST : public nsEDDEngine::MEMBER_LIST
{
public:
	static void CopyFrom( ::MEMBER_LIST *pDDSMemList, ::nsEDDEngine::MEMBER_LIST * pEDDMemList);
};

class FDI_ConvOP_MEMBER_LIST : public nsEDDEngine::OP_MEMBER_LIST
{
public:	
	static void CopyFrom( ::OP_MEMBER_LIST *pDDSOpMemList, ::nsEDDEngine::OP_MEMBER_LIST * pEDDOpMemList);
};

class FDI_ConvITEM_ARRAY_ELEMENT_LIST : public nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST
{
public:
	static void CopyFrom(::ITEM_ARRAY_ELEMENT_LIST *pDDSIArrElemList, ::nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST *pEDDIArrElemList);
};

class FDI_ConvDATA_ITEM_LIST : public nsEDDEngine::DATA_ITEM_LIST
{
public:
	static void CopyFrom( ::DATA_ITEM_LIST *pDDSDataItemList, ::nsEDDEngine::DATA_ITEM_LIST *pEDDDataItemList );
};

class FDI_ConvDEFINITION : public nsEDDEngine::DEFINITION
{
public:
	static void CopyFrom( ::DEFINITION *pDDSDefinition, ::nsEDDEngine::DEFINITION *pEDDDefinition );
};

class FDI_ConvACTION_LIST : public nsEDDEngine::ACTION_LIST
{
public:
	static void CopyFrom( ::ACTION_LIST *pDDSActionList, ::nsEDDEngine::ACTION_LIST *pEDDActionList );
};

class FDI_ConvRANGE_DATA_LIST : public nsEDDEngine::RANGE_DATA_LIST
{
public:
	static void CopyFrom( ::RANGE_DATA_LIST *pDDSRangeDataList, ::nsEDDEngine::RANGE_DATA_LIST *pEDDRangeDataList );
};

class FDI_ConvENUM_VALUE_LIST : public nsEDDEngine::ENUM_VALUE_LIST
{
public:
	static void CopyFrom( ::ENUM_VALUE_LIST *pDDSEnumValueList, ::nsEDDEngine::ENUM_VALUE_LIST *pEDDEnumValueList );
};

class FDI_ConvVECTOR_LIST : public nsEDDEngine::VECTOR_LIST
{
public:
	static void CopyFrom( ::VECTOR_LIST *pDDSVectorList, ::nsEDDEngine::VECTOR_LIST *pEDDVectorList );
};

class FDI_ConvMENU_ITEM_LIST : public nsEDDEngine::MENU_ITEM_LIST
{
public:
	static void CopyFrom( ::MENU_ITEM_LIST *pDDSMenuItemList, ::nsEDDEngine::MENU_ITEM_LIST *pEDDMenuItemList );
};

class FDI_ConvOP_REF_TRAIL_LIST : public nsEDDEngine::OP_REF_TRAIL_LIST
{
public:
	static void CopyFrom( ::OP_REF_TRAIL_LIST *pDDSOpRefTrailList, ::nsEDDEngine::OP_REF_TRAIL_LIST *pEDDOpRefTrailList );
};


class FDI_ConvITEM_ID_LIST : public nsEDDEngine::ITEM_ID_LIST
{
public:
	static void CopyFrom( ::ITEM_ID_LIST *pDDSItemIdList, ::nsEDDEngine::ITEM_ID_LIST *pEDDItemIdList);
};


class FDI_ConvTYPE_SIZE : public nsEDDEngine::TYPE_SIZE
{
public:
	static void CopyFrom( ::TYPE_SIZE *pDDSTypeSize, ::nsEDDEngine::TYPE_SIZE *pEDDTypeSize );
};

class FDI_ConvEDDFileHeader
{
public:
	static void CopyFrom(DDOD_HEADER *ddod_header, ::nsEDDEngine::EDDFileHeader *header);
};


class FDI_ConvTRANSACTION
{
public:
	static void CopyFrom(TRANSACTION *pDDSTransaction, ::nsEDDEngine::TRANSACTION *pEDDTransaction);
};


class FDI_ConvTRANSACTION_LIST
{
public:
	static void CopyFrom(TRANSACTION_LIST *pDDSTransactionList, ::nsEDDEngine::TRANSACTION_LIST *pEDDTransactionList);
};

class FDI_ConvNUMBEREXPR
{
public:
	static void CopyFrom(NUMBER_EXPR *pDDSNumberExpr, ::nsEDDEngine::NUMBER_EXPR *pEDDNumberExpr);
};

class FDI_ConvRANGESET
{
public:
	static void CopyFrom(RANGE_SET *pDDSRangeSet, ::nsEDDEngine::RANGE_SET *pEDDRangeSet);
};

class FDI_ConvRANGE_LIST
{
public:
	static void CopyFrom(RANGE_LIST *pDDSTransactionList, ::nsEDDEngine::RANGE_LIST *pEDDTransactionList);
};

class FDI_ConvCOMPONENT_SPECIFIER_LIST : public nsEDDEngine::COMPONENT_SPECIFIER_LIST
{
public:
	static void CopyFrom(::COMPONENT_SPECIFIER_LIST *pDDSComponentSpecifierList, ::nsEDDEngine::COMPONENT_SPECIFIER_LIST *pEDDComponentSpecifierList);
};