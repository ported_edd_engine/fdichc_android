
#include "ddbCollAndArr.h"
#include "stdstring.h"


class hCRecord : public hCcollection 
{
	public:
		hCRecord(DevInfcHandle_t h, aCitemBase* paItemBase, CStdString sName) : hCcollection(h, paItemBase)
		{};

		virtual RETURNCODE getByIndex(UINT32 indexValue, hCgroupItemDescriptor** ppGID, bool suppress = true);
		virtual RETURNCODE getByName (string& indexName, hCgroupItemDescriptor** ppGID, bool suppress = true);
		RETURNCODE Label(wstring& retStr);
};
