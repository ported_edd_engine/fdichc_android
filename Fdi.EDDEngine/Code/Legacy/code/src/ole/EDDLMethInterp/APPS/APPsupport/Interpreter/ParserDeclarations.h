
#if !defined(INCLUDE_EC779043_753E_4BA7_876F_0AA85AECAD71)
#define INCLUDE_EC779043_753E_4BA7_876F_0AA85AECAD71

#pragma warning(disable: 4996) //WHS (EmersonProcess) Disable the VS2005 security warning 

#include "stdafx.h"
#include "typedefs.h"
#include "SafeVar.h"
#include "ddbdefs.h"
#include "PlatformCommon.h"
#ifdef __PARSEENGINE_EXPORTS

#define __PARSEENGINE_API __declspec(dllexport)
#define EXPIMP_TEMPLATE

#else

#define __PARSEENGINE_API __declspec(dllimport)
#define EXPIMP_TEMPLATE	extern

#endif


#include <vector>
class CRIDEError;
using namespace std;
typedef vector<CRIDEError*> ERROR_VEC;

#define BUFFER_SIZE			2000
#define LBOX				'['\

/* The maximum parameters the functions can take */
#define MAX_NUMBER_OF_FUNCTION_PARAMETERS	10

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version
//based on reverse engineering i added comments to go from this enum to the types listed in the eddl spec CJK
enum RUL_TOKEN_TYPE				{	RUL_TYPE_NONE=0, 
									RUL_KEYWORD, 
									RUL_SYMBOL,
									RUL_ARITHMETIC_OPERATOR, 
									RUL_ASSIGNMENT_OPERATOR,
									RUL_RELATIONAL_OPERATOR,
									RUL_LOGICAL_OPERATOR,
									
									//double, array of int, unsigned long, double, unsigned int
									RUL_NUMERIC_CONSTANT, 
									//char[], char*
									RUL_STR_CONSTANT,
									RUL_CHR_CONSTANT,
									//this appears to be referenced
									RUL_SIMPLE_VARIABLE, 
									RUL_ARRAY_VARIABLE, 
									RUL_SERVICE,
									RUL_TYPE_ERROR,
									RUL_COMMENT,
									RUL_EOF,
									//reference
									RUL_DD_ITEM,		//Vibhor 140705: Added
									RUL_NO_SUPPORT
								};


enum RUL_TOKEN_SUBTYPE			{	RUL_SUBTYPE_NONE=0,
									//INT Operators
									RUL_UPLUS, 
									RUL_UMINUS, 
									RUL_PLUS, 
									RUL_MINUS, 
									RUL_MUL, 
									RUL_DIV, 
									RUL_MOD,
									RUL_EXP,
									RUL_NOT_EQ,  
									RUL_LT,
									RUL_GT,
									RUL_EQ,  
									RUL_GE, 
									RUL_LE,
									RUL_LOGIC_AND,
									RUL_LOGIC_OR,
									RUL_LOGIC_NOT,
									RUL_ASSIGN,
									RUL_PLUS_ASSIGN,
									RUL_MINUS_ASSIGN,
									RUL_DIV_ASSIGN,
									RUL_MOD_ASSIGN,
									RUL_MUL_ASSIGN,
									RUL_PLUS_PLUS,
									RUL_MINUS_MINUS,
									RUL_PRE_PLUS_PLUS,
									RUL_PRE_MINUS_MINUS,
									RUL_BIT_AND_ASSIGN,
									RUL_BIT_OR_ASSIGN,
									RUL_BIT_XOR_ASSIGN,
									RUL_BIT_RSHIFT_ASSIGN,
									RUL_BIT_LSHIFT_ASSIGN,
									//FLOAT Operators
									RUL_FUPLUS, 
									RUL_FUMINUS, 
									RUL_FPLUS, 
									RUL_FMINUS, 
									RUL_FMUL, 
									RUL_FDIV,
									RUL_FMOD,
#ifdef XMTR
									RUL_FRAND,
#endif
									RUL_FEXP,
									RUL_I2F,
									RUL_F2I,
									RUL_NOT_FEQ,  
									RUL_FLT,
									RUL_FGT,
									RUL_FEQ,  
									RUL_FGE, 
									RUL_FLE,
									//DOUBLE Operators
									RUL_DUPLUS, 
									RUL_DUMINUS, 
									RUL_DPLUS, 
									RUL_DMINUS, 
									RUL_DMUL, 
									RUL_DDIV,
									RUL_DMOD,
									RUL_DEXP,
									RUL_I2D,
									RUL_D2I,
									RUL_F2D,
									RUL_D2F,
									RUL_NOT_DEQ,  
									RUL_DLT,
									RUL_DGT,
									RUL_DEQ,  
									RUL_DGE, 
									RUL_DLE,
									//String Operators
									RUL_SPLUS,
									RUL_SEQ,
									RUL_NOT_SEQ,
									//Keywords
									RUL_IF,
									RUL_ELSE,
									RUL_SWITCH,
									RUL_CASE,
									RUL_DEFAULT,
									RUL_WHILE,
									RUL_FOR,
									RUL_DO,
									RUL_CHAR_DECL,
									RUL_UNSIGNED_INTEGER_DECL,
									RUL_INTEGER_DECL,
									RUL_LONG_LONG_DECL,//WaltSigtermans March 14 2008 Added 8 byte integer
									RUL_LONG_DECL,
									RUL_UNSIGNED_SHORT_INTEGER_DECL,
									RUL_SHORT_INTEGER_DECL,
									RUL_REAL_DECL, 
									RUL_DOUBLE_DECL, 
									RUL_BOOLEAN_DECL, 
									RUL_STRING_DECL,
									RUL_ARRAY_DECL,
									//Added By Anil June 14 2005 --starts here
									RUL_DD_STRING_DECL,
									//Added By Anil June 14 2005 --Ends here
									RUL_UNSIGNED_CHAR_DECL,
									//Symbols
									RUL_LPAREN,
									RUL_RPAREN,
									RUL_LBRACK,
									RUL_RBRACK,
									RUL_LBOX,
									RUL_RBOX,
									RUL_SEMICOLON,
									RUL_COLON,
									RUL_COMMA,
									RUL_DOT,
									RUL_QMARK,
									RUL_SCOPE,
									//Constants
									RUL_CHAR_CONSTANT, 
									RUL_INT_CONSTANT, 
									RUL_REAL_CONSTANT,
									RUL_BOOL_CONSTANT,
									RUL_STRING_CONSTANT,
									//Service SubTypes
									RUL_SERVICE_INVOKE,
									RUL_SERVICE_ATTRIBUTE,
									//Rule Self Invoke
									RUL_RULE_ENGINE,
									RUL_INVOKE,
									//Object manager
									RUL_OM,
									//General
									RUL_DOLLAR,
									RUL_SUBTYPE_ERROR,
									// Jump statements
									RUL_BREAK,
									RUL_CONTINUE,
									RUL_RETURN,
									// Bit wise operators
									RUL_BIT_AND,
									RUL_BIT_OR,
									RUL_BIT_XOR,
									RUL_BIT_NOT,
									RUL_BIT_RSHIFT,
									RUL_BIT_LSHIFT,
									// Function
									RUL_FUNCTION,
									// DD (Global) Identifier
									RUL_DD_SIMPLE,//Anil August 26 2005 For handling DD variable
									RUL_DD_COMPLEX,//Anil August 26 2005 For handling DD variable
									RUL_DD_METHOD,//Anil Octobet 5 2005 for handling Method Calling Method
									RUL_DICT_STRING, //added by cjk for Profibus
									RUL_TIMET_DECL,
									RUL_UNSIGNED_LONG_LONG_DECL
								};

enum STATEMENT_TYPE				{
									STMT_DECL=0,
									STMT_ASSIGNMENT,
									STMT_SELECTION,
									STMT_ELSE,
									STMT_ITERATION,
									STMT_COMPOUND,
									STMT_OPTIONAL,
									STMT_EXPRESSION,
									STMT_SERVICE,
									STMT_RUL_INVOKE,
									STMT_asic,
									STMT_ASSIGNMENT_FOR,
									STMT_BREAK,
									STMT_CONTINUE,
									STMT_RETURN,
									STMT_CASE
								};


enum STMT_EXPR_TYPE				{
									EXPR_NONE=0,
									EXPR_ASSIGN,
									EXPR_IF,
									EXPR_WHILE,
									EXPR_FOR,
									EXPR_LVALUE,
									EXPR_CASE
								};

//extern _CHAR*	szTokenStrings[];
extern const _CHAR*	szTokenSubstrings[];

struct DFA_State
{
        const _CHAR*		szWord;
	RUL_TOKEN_TYPE		Type;
	RUL_TOKEN_SUBTYPE	SubType;
};

const DFA_State OM_Service[]	= {	{"Service",RUL_SERVICE,RUL_SERVICE_ATTRIBUTE}
								 };

enum DD_ITEM_TYPE
{
	DD_ITEM_VAR = 0,
	DD_ITEM_NONVAR = 1,
	DD_ITEM_METHOD = 2

};

enum DD_METH_AGR_PASSED_TYPE
{
	 DD_METH_AGR_PASSED_UNKNOWN			= 0
	,DD_METH_AGR_PASSED_BYVALUE			= 1
	,DD_METH_AGR_PASSED_BYREFERENCE		= 2
};

//This calss is used when Method is calling other method
//Arg info are filled in this class 
class METHOD_ARG_INFO
{
protected:		// changed from private: WS:EPM 17jul07
	char*							m_pchCalledArgName;
	char*							m_pchCallerArgName;	
	RUL_TOKEN_TYPE					m_Type;
	RUL_TOKEN_SUBTYPE				m_SubType;
	
public:
	bool m_IsReturnVar ;
	enum DD_METH_AGR_PASSED_TYPE    ePassedType;
	PARAM_REF						prDDItemId;
	
	METHOD_ARG_INFO()
	{
		m_pchCalledArgName =  NULL;
		m_pchCallerArgName = NULL;
		ePassedType   =  DD_METH_AGR_PASSED_UNKNOWN;
		m_Type = RUL_TYPE_NONE;
		m_SubType = RUL_SUBTYPE_NONE;
		m_IsReturnVar = false;
		// PARAM_REF prDDItemId has its own constructor
	}

	METHOD_ARG_INFO( const METHOD_ARG_INFO& methodArguments )
	{ 
		m_pchCalledArgName = NULL;
		m_pchCallerArgName = NULL;
		operator=(methodArguments);
	}

	virtual ~METHOD_ARG_INFO()
	{
		if(m_pchCalledArgName)
		{
			delete[] m_pchCalledArgName;
			m_pchCalledArgName = NULL;
		}
		if(m_pchCallerArgName)
		{
			delete[] m_pchCallerArgName;
			m_pchCallerArgName = NULL;
		}
	}
	METHOD_ARG_INFO& operator =( const METHOD_ARG_INFO& methodArguments )
	{
		m_Type = methodArguments.m_Type;
		m_SubType = methodArguments.m_SubType;
		if( methodArguments.m_pchCallerArgName )
		{
			SetCallerArgName(methodArguments.m_pchCallerArgName);
		}
		if( methodArguments.m_pchCalledArgName )
		{
			SetCalledArgName(methodArguments.m_pchCalledArgName);
		}
		m_IsReturnVar = methodArguments.m_IsReturnVar;
		ePassedType = methodArguments.ePassedType;
		prDDItemId = methodArguments.prDDItemId;
		return *this;
	}
public:
	void SetCalledArgName(const char* pCalledArgName)
	{
		if(m_pchCalledArgName)
		{
			delete[] m_pchCalledArgName;
			m_pchCalledArgName = NULL;
		}
		//Assgn the vlues
		m_pchCalledArgName = new char[ strlen(pCalledArgName) + 1];
		memset(m_pchCalledArgName,0,strlen(pCalledArgName) + 1);
		strcpy(m_pchCalledArgName,pCalledArgName);
	}
	void SetCallerArgName(const char* pCallerArgName)
	{
		//Fille the Caller Arg name also
		
		if(m_pchCallerArgName)
		{
			delete[] m_pchCallerArgName;
			m_pchCallerArgName = NULL;
		}
		m_pchCallerArgName = new char[ strlen(pCallerArgName) + 1];
		memset(m_pchCallerArgName,0,strlen(pCallerArgName) + 1);
		strcpy(m_pchCallerArgName,pCallerArgName);
	}
	void SetType(RUL_TOKEN_TYPE eType)
	{
		m_Type = eType;
	}
	void SetSubType(RUL_TOKEN_SUBTYPE eSubType)
	{
		m_SubType = eSubType;
	}

	RUL_TOKEN_TYPE GetType()
	{
		return m_Type;
	}
	RUL_TOKEN_SUBTYPE GetSubType()
	{
		return  m_SubType;
	}

	const char*	GetCalledArgName()
	{
		return m_pchCalledArgName;

	}

	const char*	GetCallerArgName()
	{
		return m_pchCallerArgName;

	}

};
typedef vector<METHOD_ARG_INFO> METHOD_ARG_INFO_VECTOR;

#define EAT_SPACE				while(isSpace(pszSource,i,Type,SubType,pszBuffer));

#define DELETE_PTR(x)		\
	if(x)					\
	{						\
		delete x;			\
		x = 0;				\
	}						


#define DELETE_ARR(x)		\
	if(x)					\
	{						\
		delete[] x;			\
		x = 0;				\
	}	
					
#endif

