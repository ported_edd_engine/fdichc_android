#include "StdAfx.h"
#pragma warning (disable : 4786)

#include "../APPS/APPsupport/BuiltinLib/RTN_CODE.H"
#include <Dictionary.h>


/*********************************************************************
 *
 *	Name: get_string_translation
 *	ShortDesc: return the requested translation of the string
 *
 *	Description:
 *		get_string_translation will take the given string, which is
 *		composed of phrases, and using the language/country code, extract 
 *		and copy the correct string into the output buffer
 *
 *	Inputs:
 *		string:			a pointer to the string to search for the
 *						correct translation
 *		lang_cntry:		a pointer to the language/country code string
 *		outbuf_size:	the size of the output buffer, in characters
 *
 *	Outputs:
 *		outbuf:			a pointer to the output buffer with the correct
 *						translation loaded
 *
 *	Returns:
 *		DDS_SUCCESS, DDI_INSUFFICIENT_BUFFER
 *
 *	Author: Vern Reinhardt
 *
 **********************************************************************/
int CDictionary ::get_string_translation(wchar_t *string, wchar_t *outbuf, int outbuf_size, const wchar_t *lang_cntry)
{
	//ADDED By Deepak , initializing all vars
	wchar_t 	*ci=NULL;			/* input character pointer */
	wchar_t 	*co=NULL;			/* output character pointer */
	wchar_t 	*first_phrp=NULL;		/* first phrase pointer */
	wchar_t 	*lang_cntry_phrp=NULL;	/* language + country phrase pointer */
	wchar_t 	*lang_only_phrp=NULL;	/* language-only phrase pointer */
	wchar_t		lang_only[20+1]={0};//[5];	/* language-only pulled from language/country code */
	wchar_t 	*new_def_phrp=NULL;	/* new-style default phrase pointer */
	wchar_t 	*old_def_phrp=NULL;	/* old-style language-only phrase pointer */
	wchar_t 	*out_phrp=NULL;		/* output phrase pointer */
	int			code_length=0;	/* length of language/country code, in characters */
	

	/*
	 *	If the input string is a null string (which is legal), we can
	 *	skip all the string processing and return the output string,
	 *	which we set to a null string.
	 */
	if (string[0] == 0) 
	{
		outbuf[0] = 0;
		return(BLTIN_SUCCESS);
	}

    /*
     * If the input country_code is "||" we return the untranslated string
     */
    if (wcscmp(L"||", lang_cntry) == 0)
    {
        /*
	     *	Check the length of the output buffer.  If the phrase to be output
	     *	is longer than the output buffer, return an error code.  Otherwise,
	     *	copy the phrase in the holding buffer into the output buffer.
	     */
	    if ((size_t) outbuf_size < (wcslen(string) + 1))
        {
		    outbuf[0] = '\0';
		    return DDI_INSUFFICIENT_BUFFER;
	    }
        else
        {
		    (void)PS_Wcscpy(outbuf, outbuf_size, string);
	    }
        return(DDS_SUCCESS);
    }


	/*
	 *	If the input country_code is full-size (i.e., seven characters),
	 *	extract the language code from the language/country code.
	 *	Otherwise, make the language-only code a null string.
	 */
	if (wcslen(lang_cntry) == 7) 
	{
		(void)wcsncpy(lang_only, lang_cntry, (size_t)3);
		lang_only[3] = COUNTRY_CODE_MARK;
		lang_only[4] = _T('\0');
	} 
	else 
	{
		lang_only[0] = _T('\0');
	}

	/*
	 *	Check to see if the input string begins with a COUNTRY_CODE_MARK.
	 *	If it does not, set the first-phrase pointer, then enter the loop.
	 */
	if (string[0] != COUNTRY_CODE_MARK) 
	{
		first_phrp = outbuf;
	}

	/*
	 *	The Loop:
	 *		On a character-by-character basis, check for any of the
	 *	possible language or language/country codes, or escape sequences.
	 *	Look for the specified language/country code in the input string
	 *	in this order:
	 *
	 *		- the complete language/country code
	 *		- the language-only code (new style)
	 *		- the language-only code (old style)
	 *
	 *	If one of the language/country codes matches, and the corresponding
	 *	phrase pointer is not yet set, save the address of that phrase.  In
	 *	any case that a substring in the form of a language/country code is
	 *	found, even if it's not one we're looking for, insert an end-of-string
	 *	character in the output buffer, then move the input string pointer
	 *	beyond the language/country code.  If no language/country code is
	 *	found, look for escape sequences.  Do this this until the input
	 *	string's end-of-string is encountered.
	 */

	for (co = outbuf, ci = string; *ci; ci++) 
	{

	/*
	 *	Look for the complete language/country code.
	 */
		if ((ci[0] == COUNTRY_CODE_MARK) && isalpha(ci[1]) &&
				isalpha(ci[2]) && (ci[3] == _T(' ')) && isalpha(ci[4])
				&& isalpha(ci[5]) && (ci[6] == COUNTRY_CODE_MARK)) 
		{
			code_length = 7;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) 
			{
				lang_cntry_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
                    (wcsncmp(ci, (const wchar_t*)DEF__LANG__CTRY, code_length) == 0))
			{
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (new style).
	 */
		} 
		else if ((ci[0] == COUNTRY_CODE_MARK) && isalpha(ci[1]) &&
				isalpha(ci[2]) && (ci[3] == COUNTRY_CODE_MARK)) 
		{
			code_length = 4;

			if ((lang_cntry_phrp == 0) &&
					(wcsncmp(ci, lang_cntry, code_length) == 0)) {
				lang_cntry_phrp = co + 1;
			}

			if ((lang_only_phrp == 0) && (lang_only[0] != _T('\0')) &&
					(wcsncmp(ci, lang_only, code_length) == 0)) {
				lang_only_phrp = co + 1;
			}

			if ((new_def_phrp == 0) &&
                    (wcsncmp(ci, (const wchar_t *)DEF__LANG__CTRY, code_length) == 0)) {
				new_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	Look for the language-only code (old style); default only.
	 */
		} else if ((ci[0] == COUNTRY_CODE_MARK) && isdigit(ci[1]) &&
				isdigit(ci[2]) && isdigit(ci[3])) 
		{
			code_length = 4;

			if ((old_def_phrp == 0) &&
					(wcsncmp(ci, L"|001", code_length) == 0)) 
			{
				old_def_phrp = co + 1;
			}

			if (first_phrp == 0) {
				first_phrp = co + 1;
			}

			*co++ = _T('\0');
			ci += (code_length - 1);

	/*
	 *	If the escape sequence character (\) is encountered, convert
	 *	the following character as required.  These are the escape
	 *	sequences required by the DDL Spec.
	 */

		} else if (*ci == _T('\\')) 
		{

			switch (*(ci + 1)) 
			{

				case _T('a'):
					*co++ = _T('\a');
					ci++;
					break;

				case _T('f'):
					*co++ = _T('\f');
					ci++;
					break;

				case _T('n'):
					*co++ = _T('\n');
					ci++;
					break;

				case _T('r'):
					*co++ = _T('\r');
					ci++;
					break;

				case _T('t'):
					*co++ = _T('\t');
					ci++;
					break;

				case _T('v'):
					*co++ = _T('\v');
					ci++;
					break;

				default:
					*co++ = *(ci + 1);
					ci++;
					break;
			}

	/*
	 *	This is the 'normal' case; this character has no special
	 *	significance, so just copy it to the output pointer.
	 */
		} 
		else 
		{
			*co++ = *ci;
		}
	}

	/*
	 *	Tack an end-of-string character onto the final phrase.
	 */
	*co++ = _T('\0');

	/*
	 *	We may have found a phrase to output.  Copy the highest priority
	 *	phrase into the holding buffer.  Priority is determined in this 
	 *	order, depending upon which string pointers have been assigned 
	 *	non-null values:
	 *
	 *		- the phrase specified by the complete language/country code,
	 *		- the phrase specified by just the language in the
	 *		  language/country code,
	 *		- the phrase specified by the new-style default
	 *		  language/country code,
	 *		- the phrase specified by the old-style default
	 *		  language/country code,
	 *		- the first phrase encountered in the input string.
	 */

	if (lang_cntry_phrp)
	{
		out_phrp = lang_cntry_phrp;
	} 
	else if (lang_only_phrp)
	{
		out_phrp = lang_only_phrp;
	}
	else if (new_def_phrp)
	{
		out_phrp = new_def_phrp;
	}
	else if (old_def_phrp)
	{
		out_phrp = old_def_phrp;
	} 
	else
	{
		out_phrp = first_phrp;
	}

	/*
	 *	Check the length of the output buffer.  If the phrase to be output
	 *	is longer than the output buffer, return an error code.  Otherwise,
	 *	copy the phrase in the holding buffer into the output buffer.  
	 */
	if ((size_t) outbuf_size < wcslen(out_phrp))
	{
		outbuf[0] = _T('\0');
		return BLTIN_BUFFER_TOO_SMALL;
	}
	else
	{
		(void)wcscpy(outbuf, out_phrp);
	}

	return BLTIN_SUCCESS;
}

