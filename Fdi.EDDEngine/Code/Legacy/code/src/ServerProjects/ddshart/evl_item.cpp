/**
* @(#) $Id: evl_item.c,v 1.4 1996/01/30 22:09:11 kenta Exp $
*/

#include "stdinc.h"

#pragma warning (disable : 4131)

#include "HARTBreaker.h"


#ifdef ISPTEST
#include "tst_fail.h"
#endif

#include "HART/HARTEDDEngine/DDSSupport.h"
#ifndef	_HARTBREAKER_

#define HB_DUMP_BINARY(sAttrName, pDepBin)

#endif	// !_HARTBREAKER_

typedef int (*eval_type)(unsigned char*, DDL_UINT, void*, OP_REF_LIST*, ENV_INFO*, OP_REF*);

/*********************************************************************
 *
 *	Name: item_mask_man()
 *	ShortDesc: item_mask_man() checks the state of the masks and calls eval() if necessary
 *
 *	Description:
 *		item_mask_man() handles all mask switch cases for the flat structure interface
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		attribute: 		the attribute to load if evaluation is necessary
 *		env_info:	the upcall information for this call to DDS
 *		var_needed:		the if the value of a variable is needed, but is not
 *						available, "var_needed" info is returned to the application
 *
 *	Outputs:
 *		masks:			masks are modified according to the action taken by item_mask_man()
 *		attribute:		attribute is loaded if the eval() was called
 *		var_needed:		var_needed specifies the variable needed to complete
 *						evaluation
 *		depbin:			the dependencies for this attribute
 *		var_needed:		the variable needed if parameter value service does not
 *						succeed.
 *
 *	Returns:
 *		DDL_SUCCESS, DDL_NULL_POINTER, DDL_DEFAULT_ATTRIBUTE, returns from (*eval)()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
item_mask_man(
unsigned long   attr_mask,
FLAT_MASKS     *masks,

eval_type		eval,
DEPBIN         *depbin,
void           *attribute,
ENV_INFO       *env_info,
OP_REF         *var_needed)
{

	int             rc;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(MASK_MAN_EVAL);
#endif

	/*
	 * No binary exists
	 */

	if (!(attr_mask & masks->bin_exists)) 
	{
		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;	/* value already exists */
		}

		/*
		 * Must be defaulted
		 */

		else 
		{
			rc = DDL_DEFAULT_ATTR;
		}
	}

	/*
	 * No binary hooked
	 */

	else if (!(attr_mask & masks->bin_hooked) && !(attr_mask & masks->dynamic)) 
	{
		/*
		 * If value is already available
		 */

		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;
		}

		/*
		 * error, binary should be hooked up
		 */

		else 
		{
			rc = DDL_BINARY_REQUIRED;
		}
	}

	else 
	{
		/*
		 * check the pointer
		 */
		if (depbin == NULL) 
		{
			rc = DDL_NULL_POINTER;
		}
		/*
		 * check masks for evaluating
		 */
		else if ((!(attr_mask & masks->attr_avail)) || (attr_mask & masks->dynamic)) 
		{
			rc = eval(depbin->bin_chunk, depbin->bin_size, attribute, &depbin->dep, env_info, var_needed);

			if (rc == DDL_SUCCESS) 
			{
				masks->attr_avail |= attr_mask;
			}

			if (depbin->dep.count) 
			{
				masks->dynamic |= attr_mask;
			}
			else 
			{
				/*
				 * Turn off the dynamic mask for this attribute
				 */
				masks->dynamic &= ~attr_mask;
			}
		}

		/*
		 * evaluation is not necessary
		 */
		
		else 
		{
			rc = DDL_SUCCESS;
		}
	}



	if (rc != DDL_SUCCESS) 
	{
		/*
		 * Turn off the attr_available mask for this attribute
		 */
		masks->attr_avail &= ~attr_mask;
	}

	return rc;
}



/*********************************************************************
 *
 *	Name: get_simple_var_ref
 *	ShortDesc: Get a variable id if it is all that is in a depbin 
 *
 *	Description:
 *		Get a variable id if the sequence in the depbin is:
 *			OBJECT_TAG, EXPRESSION_TAG, VARID_OPCODE, and integer value of varid
 *
 *	Inputs:
 *		env_info:
 *		depbin:    		the depbin structure for this attribute
 *		ref:			a pointer to the DESC_REF that should be filled in
 *	Outputs:
 *		ref:			The DESC_REF is filled in on success, or zeros if for any
 *						reason it could not be filled in
 *
 *	Returns:
 *		int				Error returns should be ignored.  It does not have the knowledge
 *						to determine if there is an actual error or the condition should be
 *						expected.  A return value is only provided because DDL_PARSE_TAG is
 *						being used and it contains a return in the definition.
 *
 *	Author: Mark Sandmann
 *
 **********************************************************************/

static
int
get_simple_var_ref(
	ENV_INFO    *env_info,
	DEPBIN		*depbin,
	OP_REF		*ref)
{
	int             rc = DDL_INSUFFICIENT_OCTETS;		/* rc is set by the DLL_PARSE_TAG() macro */
	int             r_code = DDL_INSUFFICIENT_OCTETS;	/* return code */



	// By default the ID and Type are returned as zeros.  Only when the conditions are
	// just right do we return a variable reference.
	ref->op_info.id = 0;
	ref->op_info.type = 0;
	ref->op_info.member = 0;
	ref->op_info_list.count = 0;
	ref->op_info_list.list = NULL;
	ref->op_ref_type = STANDARD_TYPE;

	// If there is a binary, we can determine if it is a simple VARID_OPCODE
	if (depbin != NULL) 
	{
		ulong bin_size= depbin->bin_size;
		uchar *bin_chunk = depbin->bin_chunk;

		// don't even think about it if bin_chunk is null
		if (bin_chunk != NULL)
		{
			DDL_UINT        len;	/* # of DATA bytes associated with TAG */
			DDL_UINT        tag;	/* "label" associated with current chunk */

			// The First tag needs to be an OBJECT_TAG
			DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
			if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == OBJECT_TAG) )
			{
				// The Second tag needs to be an EXPRESSION_TAG
				DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, &len);
				if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == EXPRESSION_TAG) )
				{
					// The Third Tag needs to be a VARID_OPCODE with no length field
					DDL_PARSE_TAG(&bin_chunk, &bin_size, &tag, (DDL_UINT *) NULL_PTR);
					if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == VARID_OPCODE) )
					{
						ulong result_value = 0;
						// process the remainder of the of the chunk to get the variable ID
						r_code = ddl_parse_integer_func(&bin_chunk, &bin_size, &result_value);

						// If we got a successful integer parse and the chunk has nothing else
						// then we can set the ref id and type.
						if ((r_code == DDL_SUCCESS) && (bin_size == 0))
						{
							ref->op_info.id = result_value;
							ref->op_info.type = VARIABLE_ITYPE;
						}
						else
						{
							r_code = DDL_INSUFFICIENT_OCTETS;
						}
					}
					else if ( (rc == DDL_SUCCESS) && (bin_size > 0) && (tag == VARREF_OPCODE) )
					{
						OP_REF tempOpRef = {STANDARD_TYPE, 0};
						OP_REF var_needed = {STANDARD_TYPE, 0};

						// process the remainder of the of the chunk to get the reference
						r_code = ddl_parse_op_ref(&bin_chunk, &bin_size, &tempOpRef, NULL, env_info, &var_needed);

						// If we got a successful reference parse and the chunk has nothing else
						// then we can return it
						if ((r_code == DDL_SUCCESS) && (bin_size == 0))
						{
							*ref = tempOpRef;
						}
						else
						{
							r_code = DDL_INSUFFICIENT_OCTETS;
						}
					}
					else
					{
						r_code = DDL_INSUFFICIENT_OCTETS;
					}
				}
			}
		}

	}
	return r_code;
}

/*********************************************************************
 *
 *	Name: load_errors
 *	ShortDesc: load the RETURN_LIST
 *
 *	Description:
 *		load_errors will append the RETURN_LIST with an error
 *
 *	Inputs:
 *		errors:		the RETURN_LIST
 *		rc:			the return code to add to the list
 *		var_needed:	the OP_REF if parameter value service does not succeed
 *		attr_mask:	the attribute that the error is for
 *
 *	Outputs:
 *		errors:		the appended RETURN_LIST
 *
 *	Returns:
 *		void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
void
load_errors(
RETURN_LIST    *errors,
int             rc,
OP_REF         *var_needed,
unsigned long   attr_mask)
{

	RETURN_INFO    *temp_errors;	/* temp storage for return info */


	if (errors->count >= RETURN_LIST_SIZE) {
		return;
	}

	temp_errors = &errors->list[errors->count++];
	temp_errors->bad_attr = attr_mask;

	temp_errors->var_needed.op_info.id = 0;
	temp_errors->var_needed.op_info.member = 0;
	temp_errors->var_needed.op_info.type = 0;


	switch (rc) {

	case DDL_DEFAULT_ATTR:

		/*
		* if load error is called with a return code of
		* DDL_DEFAULT_ATTR, convert this to a DDL_ENCODING ERROR
		*/
		temp_errors->rc = DDL_ENCODING_ERROR;
		break;	

	default:
		if (var_needed /*&& var_needed->op_info.id != 0*/) 
		{
			if( var_needed->op_ref_type == STANDARD_TYPE && var_needed->op_info.id != 0 )
			{
				temp_errors->var_needed.op_ref_type = STANDARD_TYPE;

				temp_errors->var_needed.op_info.id		= var_needed->op_info.id;
				temp_errors->var_needed.op_info.member	= var_needed->op_info.member;
				temp_errors->var_needed.op_info.type	= var_needed->op_info.type;
			}
			else if( var_needed->op_ref_type == COMPLEX_TYPE && var_needed->op_info_list.list != NULL )
			{
				temp_errors->var_needed.op_ref_type = COMPLEX_TYPE;

				temp_errors->var_needed.op_info_list.count = var_needed->op_info_list.count;

				temp_errors->var_needed.op_info_list.list = (OP_REF_INFO*) malloc((size_t) temp_errors->var_needed.op_info_list.count * sizeof( OP_REF_INFO ));

				for( int i = 0; i < temp_errors->var_needed.op_info_list.count; i++ )
				{
					temp_errors->var_needed.op_info_list.list[i].id		= var_needed->op_info_list.list[i].id;
					temp_errors->var_needed.op_info_list.list[i].member	= var_needed->op_info_list.list[i].member;
					temp_errors->var_needed.op_info_list.list[i].type	= var_needed->op_info_list.list[i].type;
				}
			}
		}	
		temp_errors->rc = rc;
		break;

	}
}

/*********************************************************************
 *
 *	Name: promote_range_data_element()
 *	ShortDesc: promote each element of a range data list to a specified type.
 *
 *	Description:
 *		Promote elements of a range data list to a specified type according to
 *		the following rules:
 *		specified type = int,uint,float,double and
 *				element type = int,uint,float,double
 *				-> promote element type.
 *		specified type = int,uint,float,double and
 *				element type = index,enum,bit_enum
 *				-> do nothing, go to next element.
 *		specified type = index,enum,bit_enum   and
 *				element type = don't care
				-> return.
 *
 *	Inputs:
 *		type:	specified type to promote range data element to.
 *		list:	list of range data elements to be promoted.
 *
 *	Outputs:
 *		list:	list of promoted range data elements.
 *
 *	Returns:
 *		DDL_SUCCESS,
 *		DDL_INVALID_PARAM
 *
 *	Author: Steve Beyerl
 *
 **********************************************************************/
static void
promote_range_data_element(
unsigned short  type,		/** desired type **/
RANGE_DATA_LIST *list)		/** list of MIN/MAX values **/
{

	int             i;	/** loop counter **/
	EXPR           *expr;	/** pointer to list of EXPR's **/

	if (!list) {
		return;
	}

	if (!list->list || (list->count == 0)) {
		return;
	}

	expr = list->list;	/** pointer to the first element of the list **/
	for (i = 0; i < list->count; i++, expr++) {
		switch (type) {	/** desired type **/
		case INTEGER:
			switch (expr->type) {	/** current type **/
			case INTEGER:
				if (expr->size > 4)
				{
					ASSERT_DBG(expr->size <= 8);
					expr->size = 8;
				}
				else
				{
					expr->size = 4;
				}
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.ll = (LONGLONG) expr->val.ull;
				expr->type = INTEGER;
				if (expr->size > 4)
				{
					ASSERT_DBG(expr->size <= 8);
					expr->size = 8;
				}
				else
				{
					expr->size = 4;
				}
				break;
            case FLOATG_PT:
				expr->val.ll = (LONGLONG)expr->val.f;
				expr->type = INTEGER;
				expr->size = 4;
				break;
            case DOUBLEG_PT:
				expr->val.ll = (LONGLONG) expr->val.d;
				expr->type = INTEGER;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
		case UNSIGNED:
			switch (expr->type) {
			case INTEGER:
				expr->val.ull = (ULONGLONG) expr->val.ll;
				expr->type = UNSIGNED;
				if (expr->size > 4)
				{
					ASSERT_DBG(expr->size <= 8);
					expr->size = 8;
				}
				else
				{
					expr->size = 4;
				}
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->type = UNSIGNED;
				if (expr->size > 4)
				{
					ASSERT_DBG(expr->size <= 8);
					expr->size = 8;
				}
				else
				{
					expr->size = 4;
				}
				break;
            case FLOATG_PT:
				expr->val.ull = (ULONGLONG) expr->val.f;
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
            case DOUBLEG_PT:
				expr->val.ull = (ULONGLONG) expr->val.d;
				expr->type = UNSIGNED;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
        case FLOATG_PT:
			switch (expr->type) {
			case INTEGER:
				expr->val.f = (float) expr->val.ll;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.f = (float) expr->val.ull;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
            case FLOATG_PT:
				break;
            case DOUBLEG_PT:
				expr->val.f = (float) expr->val.d;
                expr->type = FLOATG_PT;
				expr->size = 4;
				break;
			default:
				break;
			}
			break;
        case DOUBLEG_PT:
			switch (expr->type) {
			case INTEGER:
				expr->val.d = (double) expr->val.ll;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
			case UNSIGNED:
			case INDEX:
			case ENUMERATED:
			case BIT_ENUMERATED:
				expr->val.d = (double) expr->val.ull;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
            case FLOATG_PT:
				expr->val.d = (double) expr->val.f;
                expr->type = DOUBLEG_PT;
				expr->size = 8;
				break;
            case DOUBLEG_PT:
				break;
			default:
				break;
			}
			break;
		default:	/** specified type = enum,bit_enum,index **/
			return;
			/* NOTREACHED */
			break;
		}
	}			/* end of FOR */
}


/*********************************************************************
 *
 *	Name: eval_attr_reflist
 *	ShortDesc: evaluate a reflist
 *
 *	Description:
 *		eval_attr_reflist is a generic handler for reflists which are
 *		an optional attribute. It calls item_mask_man() and if DDL_DEFAULT is
 *		returned it loads an empty list and returns
 *
 *	Inputs:
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked, attr_avail, dynamic)
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		reflist:		the reference list to load
 *		depbin:    		the depbin structure for this attribute
 *
 *	Outputs:
 *		errors:			the appended RETURN_LIST if there was an error
 *		reflist:		the loaded reflist if eval was called
 *		depbin:			the dependencies for this attribute
 *
 *	Returns:
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_reflist(
FLAT_MASKS     *masks,
unsigned long   attr_mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
ITEM_ID_LIST   *reflist,
DEPBIN         *depbin)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp var_needed storage */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */

	rc = item_mask_man(attr_mask,
		masks,
		eval_reflist,
		depbin,
		reflist,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			reflist->count = 0;

			masks->attr_avail |= attr_mask;
		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}


static
int
eval_attr_data_reflist(
FLAT_MASKS     *masks,
unsigned long   attr_mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
DATA_ITEM_LIST *reflist,
DEPBIN         *depbin)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp var_needed storage */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */

	rc = item_mask_man(attr_mask,
		masks,
		eval_data_reflist,
		depbin,
		reflist,
		env_info,
		&var_needed);

	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			reflist->count = 0;

			masks->attr_avail |= attr_mask;
		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return rc;
}

/*********************************************************************
 *
 *	Name: eval_attr_string
 *	ShortDesc: evaluate a string
 *
 *	Description:
 *		eval_attr_string is a generic function for evaluating a string
 *		when that string can be defaulted
 *
 *	Inputs:
 *		attr_mask:		the mask which corresponds to the attribute being evaluated
 *		masks:     		the set of masks for that item (bin_exists, bin_hooked,
 *						attr_avail, dynamic)
 *		(*eval):   		the eval function to call if the attribuite need evaluating
 *		depbin:    		the depbin structure for this attribute
 *		string: 		the string to load if evaluation is necessary
 *		env_info:	the upcall information for this call to DDS
 *		errors:			the RETURN_LIST for this i_eval() call
 *		def_string:		indicates which default string should be gotten from
 *						standart.dct.
 *
 *	Outputs:
 *		depbin:			the dependencies for this attribute
 *		string:			the loaded string if eval was called
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		return 			void
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
void
eval_attr_string(
unsigned long   attr_mask,
FLAT_MASKS     *masks,
eval_type		eval,
DEPBIN         *depbin,
STRING         *string,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
unsigned long   def_string)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp var_needed storage */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */

	rc = item_mask_man(attr_mask,
		masks,
		eval,
		depbin,
		string,
		env_info,
		&var_needed);


	if (rc != DDL_SUCCESS) {

		if (rc == DDL_DEFAULT_ATTR) {

			rc = app_func_get_dict_string(env_info, def_string, string);


			/*
			 * If a string was not found, get the default error
			 * string.
			 */

			if (rc != DDL_SUCCESS) {

				rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING, string);
				if (rc != DDL_SUCCESS) {

					load_errors(errors, rc, &var_needed, attr_mask);
				}
			}

			if (rc == DDL_SUCCESS) {

				masks->attr_avail |= attr_mask;
			}
			else {
				load_errors(errors, rc, &var_needed, attr_mask);
			}

		}
		else {
			load_errors(errors, rc, &var_needed, attr_mask);
		}
	}

	return;
}

/*********************************************************************
 *
 *	Name: eval_attr_var_actions
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_attr_var_actions will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, var->id,			\
								"Variable", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

static
int
eval_attr_var_actions(
FLAT_VAR       *var,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc = DDI_INVALID_FLAT_MASKS;	/* return code */
	FLAT_VAR_ACTIONS *act;	/* temp pointer for actions struct */
	FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */


	act = var->actions;
	var_masks = &var->masks;



	if (mask & VAR_PRE_EDIT_ACT) {

		HB_DUMP_BINARY(	"VAR_PRE_EDIT_ACTIONS",
						(act->depbin ? act->depbin->db_pre_edit_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_EDIT_ACT, env_info,
			errors, &act->pre_edit_act, (act->depbin ? act->depbin->db_pre_edit_act : NULL));
	}

	if (mask & VAR_POST_EDIT_ACT) {

		HB_DUMP_BINARY(	"VAR_POST_EDIT_ACTIONS",
						(act->depbin ? act->depbin->db_post_edit_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_EDIT_ACT, env_info,
			errors, &act->post_edit_act, (act->depbin ? act->depbin->db_post_edit_act : NULL));
	}

	if (mask & VAR_PRE_READ_ACT) {

		HB_DUMP_BINARY(	"VAR_PRE_READ_ACTIONS",
						(act->depbin ? act->depbin->db_pre_read_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_READ_ACT, env_info,
			errors, &act->pre_read_act, (act->depbin ? act->depbin->db_pre_read_act : NULL));
	}

	if (mask & VAR_POST_READ_ACT) {

		HB_DUMP_BINARY(	"VAR_POST_READ_ACTIONS",
						(act->depbin ? act->depbin->db_post_read_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_READ_ACT, env_info,
			errors, &act->post_read_act, (act->depbin ? act->depbin->db_post_read_act : NULL));
	}

	if (mask & VAR_PRE_WRITE_ACT) {

		HB_DUMP_BINARY(	"VAR_PRE_WRITE_ACTIONS",
						(act->depbin ? act->depbin->db_pre_write_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_PRE_WRITE_ACT, env_info,
			errors, &act->pre_write_act, (act->depbin ? act->depbin->db_pre_write_act : NULL));
	}

	if (mask & VAR_POST_WRITE_ACT) {

		HB_DUMP_BINARY(	"VAR_POST_WRITE_ACTIONS",
						(act->depbin ? act->depbin->db_post_write_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_POST_WRITE_ACT, env_info,
			errors, &act->post_write_act, (act->depbin ? act->depbin->db_post_write_act : NULL));
	}

	if (mask & VAR_REFRESH_ACT) {

		HB_DUMP_BINARY(	"VAR_REFRESH_ACT",
						(act->depbin ? act->depbin->db_refresh_act : NULL));

		rc = eval_attr_reflist(var_masks, (unsigned long) VAR_REFRESH_ACT, env_info,
			errors, &act->refresh_act, (act->depbin ? act->depbin->db_refresh_act : NULL));
	}

	return rc;
}


/*********************************************************************
 *
 *	Name: eval_attr_var_misc
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_attr_var_misc will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

static
int
eval_attr_var_misc(
FLAT_VAR       *var,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc = DDI_INVALID_FLAT_MASKS;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_VAR_MISC  *misc;	/* temp pointer for misc struct */
	FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */

	misc = var->misc;
	var_masks = &var->masks;

	if (mask & VAR_UNIT) {
		HB_DUMP_BINARY(	"VAR_CONSTANT_UNIT",
						(misc->depbin ? misc->depbin->db_unit : NULL));

		eval_attr_string((unsigned long) VAR_UNIT,
			var_masks,
			eval_string,
			(misc->depbin ? misc->depbin->db_unit : NULL),
			&misc->unit,
			env_info,
			errors,
			DEFAULT_STD_DICT_STRING);
	}


	if (mask & VAR_WRITE_TIME_OUT) {

		HB_DUMP_BINARY(	"VAR_WRITE_TIMEOUT",
						(misc->depbin ? misc->depbin->db_write_time_out : NULL));

		rc = item_mask_man((unsigned long) VAR_WRITE_TIME_OUT,
			var_masks,
			eval_attr_int_expr,
			(misc->depbin ? misc->depbin->db_write_time_out : NULL),
			&misc->write_time_out,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->write_time_out = 0;
				var_masks->attr_avail |= (unsigned long) VAR_WRITE_TIME_OUT;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_WRITE_TIME_OUT);
			}
		}
	}

	if (mask & VAR_READ_TIME_OUT) {

		HB_DUMP_BINARY(	"VAR_READ_TIMEOUT",
						(misc->depbin ? misc->depbin->db_read_time_out : NULL));

		rc = item_mask_man((unsigned long) VAR_READ_TIME_OUT,
			var_masks,
			eval_attr_int_expr,
			(misc->depbin ? misc->depbin->db_read_time_out : NULL),
			&misc->read_time_out,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->read_time_out = 0;
				var_masks->attr_avail |= (unsigned long) VAR_READ_TIME_OUT;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_READ_TIME_OUT);
			}
		}
	}

	if (mask & VAR_MIN_VAL) {

		HB_DUMP_BINARY(	"VAR_MIN_VALUE",
						(misc->depbin ? misc->depbin->db_min_val : NULL));

		rc = item_mask_man((unsigned long) VAR_MIN_VAL,
			var_masks,
			eval_attr_min_values,
			(misc->depbin ? misc->depbin->db_min_val : NULL),
			&misc->min_val,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->min_val.count = 0;

				var_masks->attr_avail |= (unsigned long) VAR_MIN_VAL;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_MIN_VAL);
			}
		}
		else {		/** promote the list of EXPR's **/
			if (var_masks->attr_avail & (unsigned long) VAR_MIN_VAL) {
				promote_range_data_element(var->type_size.type, &misc->min_val);
			}
		}
	}

	if (mask & VAR_MAX_VAL) {

		HB_DUMP_BINARY(	"VAR_MAX_VALUE",
						(misc->depbin ? misc->depbin->db_max_val : NULL));

		rc = item_mask_man((unsigned long) VAR_MAX_VAL,
			var_masks,
			eval_attr_max_values,
			(misc->depbin ? misc->depbin->db_max_val : NULL),
			&misc->max_val,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->max_val.count = 0;

				var_masks->attr_avail |= (unsigned long) VAR_MAX_VAL;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_MAX_VAL);
			}
		}
		else {		/** promote the list of EXPR's **/
			if (var_masks->attr_avail & (unsigned long) VAR_MAX_VAL) {
				promote_range_data_element(var->type_size.type, &misc->max_val);
			}
		}
	}

	if (mask & VAR_SCALE) {

		HB_DUMP_BINARY(	"VAR_SCALING_FACTOR",
						(misc->depbin ? misc->depbin->db_scale : NULL));

		rc = item_mask_man((unsigned long) VAR_SCALE,
			var_masks,
			eval_attr_scaling_factor,
			(misc->depbin ? misc->depbin->db_scale : NULL),
			&misc->scale,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->scale.size = 1;
				misc->scale.type = INTEGER;
				misc->scale.val.ll = 1;
				var_masks->attr_avail |= (unsigned long) VAR_SCALE;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_SCALE);
			}
		}
	}

	if (mask & VAR_VALID) {

		HB_DUMP_BINARY(	"VAR_VALIDITY",
						(misc->depbin ? misc->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) VAR_VALID,
			var_masks,
			eval_attr_ulong,
			(misc->depbin ? misc->depbin->db_valid : NULL),
			&misc->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->valid = TRUE;
				var_masks->attr_avail |= (unsigned long) VAR_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_VALID);
			}
		}
	}

	if (mask & VAR_DEBUG)
	{
		HB_DUMP_BINARY( "VAR_DEBUG", (misc->depbin ? misc->depbin->db_debug_info : NULL) );

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) VAR_DEBUG, var_masks, eval_attr_debug_info, 
			(misc->depbin ? misc->depbin->db_debug_info : NULL), &misc->symbol_name, env_info, &var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				misc->symbol_name.str = L"TODO_Find_NaMe";
				var_masks->attr_avail |= (unsigned long) VAR_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_DEBUG);
			}
		}
	}

	if (mask & VAR_TIME_SCALE) {

		HB_DUMP_BINARY(	"VAR_TIME_SCALE",
						(misc->depbin ? misc->depbin->db_time_scale : NULL));

		rc = item_mask_man((unsigned long) VAR_TIME_SCALE,
			var_masks,
			eval_attr_time_scale,
			(misc->depbin ? misc->depbin->db_time_scale : NULL),
			&misc->time_scale,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				misc->time_scale = VAR_TIME_SCALE_NONE;
				var_masks->attr_avail |= (unsigned long) VAR_TIME_SCALE;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_TIME_SCALE);
			}
		}
	}

	if (mask & VAR_TIME_FORMAT) {

		HB_DUMP_BINARY(	"VAR_TIME_FORMAT",
			(misc->depbin ? misc->depbin->db_time_format : NULL));

		rc = item_mask_man((unsigned long) VAR_TIME_FORMAT,
			var_masks,
			eval_attr_display_format,
			(misc->depbin ? misc->depbin->db_time_format : NULL),
			&misc->time_format,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				int str_len = 2;
				misc->time_format.str = (wchar_t *) malloc((size_t) ((str_len+1) * sizeof(wchar_t)));
				if (misc->time_format.str == NULL)
				{
					return DDL_MEMORY_ERROR;
				}

				misc->time_format.len = (unsigned short)str_len;
				misc->time_format.flags = FREE_STRING;
				wcscpy( misc->time_format.str, L"%T" );
				misc->time_format.str[str_len] = L'\0';	// Null terminate
				var_masks->attr_avail |= (unsigned long) VAR_TIME_FORMAT;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_TIME_FORMAT);
			}
		}
	}

	return rc;
}



/*********************************************************************
 *
 *	Name: eval_item_var
 *	ShortDesc: evaluate the FLAT_VAR
 *
 *	Description:
 *		eval_item_var will load the FLAT_VAR structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var() call
 *
 *	Outputs:
 *		var:			the loaded FLAT_VAR structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_var_actions(), eval_attr_var_misc()
 *		eval_attr_string(), eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

int
eval_item_var(
FLAT_VAR       *var,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	unsigned long   index;	/* Standard dictionary index value */
	FLAT_MASKS     *var_masks;	/* temp ptr to the masks of a var */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_VAR);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) var, VARIABLE_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */

	var_masks = &var->masks;

	index = 0;
	/*
	 * Sanity check
	 */

	if ((var_masks->bin_hooked & VAR_MAIN_MASKS) && (var->depbin == NULL)) {

		return DDL_NULL_POINTER;
	}

	/**
         *  Fetch automatically looks for a TYPE binary chunk.
         *  Type needs to be evaluated, to determine if the desired type subattributes
         *  make sense.
         **/

	HB_DUMP_BINARY(	"VAR_TYPE",
					(var->depbin ? var->depbin->db_type_size : NULL));

	rc = item_mask_man((unsigned long) VAR_TYPE_SIZE,
		var_masks,
		eval_attr_type,
		(var->depbin ? var->depbin->db_type_size : NULL),
		&var->type_size,
		env_info,
		&var_needed);


	if (rc != DDL_SUCCESS) {
		load_errors(errors, rc, &var_needed, (unsigned long) VAR_TYPE_SIZE);
	}

	/**
         *  Some combinations of var_type and var_type_subattributes do not
         *  make any sense.  The following code is an attempt to remove
         *  invalid combinations of type and type_subattributes.
         **/

	if (var_masks->attr_avail & (unsigned long) VAR_TYPE_SIZE) {
		switch (var->type_size.type) {
		case INTEGER:
		case UNSIGNED:
        case FLOATG_PT:
        case DOUBLEG_PT:
			mask &= INVALID_ARITH_TYPE_SUBATTR_MASK;
			break;
		case ENUMERATED:
		case BIT_ENUMERATED:
			mask &= INVALID_ENUM_TYPE_SUBATTR_MASK;
			break;
		case INDEX:
			mask &= INVALID_INDEX_TYPE_SUBATTR_MASK;
			break;
		case EUC:
		case ASCII:
#ifdef HART
		case PACKED_ASCII:
#endif
		case PASSWORD:
		case BITSTRING:
			mask &= INVALID_STRING_TYPE_SUBATTR_MASK;
			break;
#ifdef HART
		case HART_DATE_FORMAT:
#endif
		case TIME:
		case DATE_AND_TIME:
		case DURATION:
			mask &= INVALID_DATE_TIME_TYPE_SUBATTR_MASK;
			break;

		case TIME_VALUE:
			mask &= INVALID_TIME_VALUE_TYPE_SUBATTR_MASK;
			break;

		default:	/* should never happen */
			mask &= INVALID_VAR_TYPE_SUBATTR_MASK;
			break;
		}
	}
	else {			/** TYPE is not available **/
		mask &= INVALID_VAR_TYPE_SUBATTR_MASK;
	}


	if (mask & VAR_CLASS) {

		HB_DUMP_BINARY(	"VAR_CLASS",
						(var->depbin ? var->depbin->db_class : NULL));

		rc = item_mask_man((unsigned long) VAR_CLASS,
			var_masks,
			eval_attr_bitstring,
			(var->depbin ? var->depbin->db_class : NULL),
			&var->class_attr,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) VAR_CLASS);
		}
	}


	if (mask & VAR_HANDLING) {

		HB_DUMP_BINARY(	"VAR_HANDLING",
						(var->depbin ? var->depbin->db_handling : NULL));

		rc = item_mask_man((unsigned long) VAR_HANDLING,
			var_masks,
			eval_attr_bitstring,
			(var->depbin ? var->depbin->db_handling : NULL),
			&var->handling,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->handling = READ_HANDLING | WRITE_HANDLING;
				var_masks->attr_avail |= (unsigned long) VAR_HANDLING;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_HANDLING);
			}
		}
	}

	if (mask & VAR_HELP) {

		HB_DUMP_BINARY(	"VAR_HELP",
						(var->depbin ? var->depbin->db_help : NULL));

		eval_attr_string((unsigned long) VAR_HELP,
			var_masks,
			eval_string,
			(var->depbin ? var->depbin->db_help : NULL),
			&var->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & VAR_LABEL) {

		HB_DUMP_BINARY(	"VAR_LABEL",
						(var->depbin ? var->depbin->db_label : NULL));

		eval_attr_string((unsigned long) VAR_LABEL,
			var_masks,
			eval_string,
			(var->depbin ? var->depbin->db_label : NULL),
			&var->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & VAR_DISPLAY) {
		HB_DUMP_BINARY(	"VAR_DISPLAY_FORMAT",
						(var->depbin ? var->depbin->db_display : NULL));

		rc = item_mask_man((unsigned long) VAR_DISPLAY,
			var_masks,
			eval_attr_display_format,
			(var->depbin ? var->depbin->db_display : NULL),
			&var->display,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {
			if (rc == DDL_DEFAULT_ATTR) {
				if (!(var_masks->attr_avail & VAR_TYPE_SIZE)) {
					load_errors(errors, DDL_VAR_TYPE_NEEDED, (OP_REF *) NULL,
						(unsigned long) VAR_DISPLAY);
				}
				else {
					
					rc = DDL_SUCCESS;

					switch (var->type_size.type) {
					case INTEGER:
						if (var->type_size.size <= 4)
						{
							/**
							* If a DISPLAY FORMAT string was not found in the binary,
							* use the integer DEFAULT DISPLAY FORMAT from the
							* standard dictionary.
							*/

							index = DEFAULT_STD_DICT_DISP_INT;
						}
						else
						{
							var->display.flags = DONT_FREE_STRING;
							var->display.str = L"lld";
							var->display.len = 3;
						}
						break;

					case UNSIGNED:
						if (var->type_size.size <= 4)
						{
							/**
							* If a DISPLAY FORMAT string was not found in the binary,
							* use the unsigned integer DEFAULT DISPLAY FORMAT from
							* the standard dictionary.
							*/

							index = DEFAULT_STD_DICT_DISP_UINT;
						}
						else
						{
							var->display.flags = DONT_FREE_STRING;
							var->display.str = L"llu";
							var->display.len = 3;
						}
						break;
					
					case TIME_VALUE:

						/**
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the floating point DEFAULT DISPLAY FORMAT from
						 * the standard dictionary.
						 */

						index = DEFAULT_STD_DICT_DISP_FLOAT;
						break;

                    case FLOATG_PT:

						/**
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the floating point DEFAULT DISPLAY FORMAT from
						 * the standard dictionary.
						 */

						//this is HART specified default display format
						var->display.flags = DONT_FREE_STRING;
						var->display.str = L"12.5g";
						var->display.len = (unsigned short)wcslen(var->display.str);
						break;

                    case DOUBLEG_PT:

						/**
						 * If a DISPLAY FORMAT string was not found in the binary,
						 * use the double DEFAULT DISPLAY FORMAT from the
						 * standard dictionary.
						 */

						//this is HART specified default display format
						var->display.flags = DONT_FREE_STRING;
						var->display.str = L"18.8g";
						var->display.len = (unsigned short)wcslen(var->display.str);
						break;

					default:	/** should never occur **/
						return DDL_INVALID_TYPE_SUBATTR;
					}

					/*
					 * If a DISPLAY FORMAT string was not
					 * found in the binary, use the FORMAT
					 * from the standard dictionary.
					 */
					if(index != 0)
					{
						rc = app_func_get_dict_string(env_info, index, &var->display);

						/*
						* If a string was not found, get the
						* default error string.
						*/

						if (rc != DDL_SUCCESS) {
							rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
								&var->display);
							if (rc != DDL_SUCCESS) {
								load_errors(errors, rc, &var_needed,
									(unsigned long) VAR_DISPLAY);
							}
						}
					}
					if ((rc == DDL_SUCCESS) || (rc == DDL_DEFAULT_ATTR))  {
						var->masks.attr_avail |= VAR_DISPLAY;
					}
				}
			}
			else {
				load_errors(errors, rc, &var_needed,
					(unsigned long) VAR_DISPLAY);
			}
		}
	}

	if (mask & VAR_EDIT) {
		HB_DUMP_BINARY(	"VAR_EDIT_FORMAT",
						(var->depbin ? var->depbin->db_edit : NULL));

		rc = item_mask_man((unsigned long) VAR_EDIT,
			var_masks,
			eval_attr_edit_format,
			(var->depbin ? var->depbin->db_edit : NULL),
			&var->edit,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {
			if (rc == DDL_DEFAULT_ATTR) {
				if (!(var_masks->attr_avail & VAR_TYPE_SIZE)) {
					load_errors(errors, DDL_VAR_TYPE_NEEDED, (OP_REF *) NULL,
						(unsigned long) VAR_EDIT);
				}
				else {

					rc = DDL_SUCCESS;

					switch (var->type_size.type) {
					case INTEGER:
						if (var->type_size.size <= 4)
						{
							/*
							 * If an EDIT FORMAT string was
							 * not found in the binary, use
							 * the integer DEFAULT EDIT
							 * FORMAT from the standard
							 * dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_INT;
						}
						else
						{
							var->edit.flags = DONT_FREE_STRING;
							var->edit.str = L"lld";
							var->edit.len = 3;
						}
						break;

					case UNSIGNED:
						if (var->type_size.size <= 4)
						{
							/*
							 * If an EDIT FORMAT string was
							 * not found in the binary, use
							 * the unsigned integer DEFAULT
							 * EDIT FORMAT from the
							 * standard dictionary.
							 */

							index = DEFAULT_STD_DICT_EDIT_UINT;
						}
						else
						{
							var->edit.flags = DONT_FREE_STRING;
							var->edit.str = L"llu";
							var->edit.len = 3;
						}
						break;

					case TIME_VALUE:

						/*
						 * If an EDIT FORMAT string was
						 * not found in the binary, use
						 * the float DEFAULT EDIT
						 * FORMAT from the standard
						 * dictionary.
						 */

						index = DEFAULT_STD_DICT_EDIT_FLOAT;
						break;

                    case FLOATG_PT:

						/*
						 * If an EDIT FORMAT string was
						 * not found in the binary, use
						 * the float DEFAULT EDIT
						 * FORMAT from the standard
						 * dictionary.
						 */

						//this is HART specified default edit format
						var->edit.flags = DONT_FREE_STRING;
						var->edit.str = L"12.5f";
						var->edit.len = (unsigned short)wcslen(var->edit.str);
						break;

                    case DOUBLEG_PT:

						/*
						 * If an EDIT FORMAT string was
						 * not found in the binary, use
						 * the double DEFAULT EDIT
						 * FORMAT from the standard
						 * dictionary.
						 */

						//this is HART specified default edit format
						var->edit.flags = DONT_FREE_STRING;
						var->edit.str = L"18.8f";
						var->edit.len = (unsigned short)wcslen(var->edit.str);
						break;

					default:	/** should never occur **/
						return DDL_INVALID_TYPE_SUBATTR;
					}

					/*
					 * If a EDIT FORMAT string was not
					 * found in the binary, use the
					 * DEFAULT_EDIT_FORMAT from the
					 * standard dictionary.
					 */
					if(index != 0)
					{
						rc = app_func_get_dict_string(env_info, index, &var->edit);

						/*
						 * If a string was not found, get the
						 * default error string.
						 */

						if (rc != DDL_SUCCESS) {
							rc = app_func_get_dict_string(env_info, DEFAULT_STD_DICT_STRING,
								&var->edit);
							if (rc != DDL_SUCCESS) {
								load_errors(errors, rc, &var_needed,
									(unsigned long) VAR_EDIT);
							}
						}
					}
					if ((rc == DDL_SUCCESS) || (rc == DDL_DEFAULT_ATTR))  {
						var->masks.attr_avail |= VAR_EDIT;
					}
				}
			}
			else {
				load_errors(errors, rc, &var_needed,
					(unsigned long) VAR_EDIT);
			}
		}
	}

	if (mask & VAR_ENUMS) {

		HB_DUMP_BINARY(	"VAR_ENUMS",
						(var->depbin ? var->depbin->db_enums : NULL));

		rc = item_mask_man((unsigned long) VAR_ENUMS,
			var_masks,
			eval_attr_enum,
			(var->depbin ? var->depbin->db_enums : NULL),
			&var->enums,
			env_info,
			&var_needed);


		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->enums.count = 0;

				var_masks->attr_avail |= (unsigned long) VAR_ENUMS;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_ENUMS);
			}
		}
	}

	if (mask & VAR_INDEX_ITEM_ARRAY) {

		HB_DUMP_BINARY(	"VAR_INDEX_ITEM_ARRAY",
						(var->depbin ? var->depbin->db_index_item_array : NULL));

		rc = item_mask_man((unsigned long) VAR_INDEX_ITEM_ARRAY,
			var_masks,
			eval_attr_arrayname,
			(var->depbin ? var->depbin->db_index_item_array : NULL),
			&var->index_item_array,
			env_info,
			&var_needed);


		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->index_item_array = 0;
				var_masks->attr_avail |= (unsigned long) VAR_INDEX_ITEM_ARRAY;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_INDEX_ITEM_ARRAY);
			}
		}
	}


	if (mask & VAR_RESP_CODES) {

		HB_DUMP_BINARY(	"VAR_RESP_CODES",
						(var->depbin ? var->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) VAR_RESP_CODES,
			var_masks,
			eval_attr_ref,
			(var->depbin ? var->depbin->db_resp_codes : NULL),
			&var->resp_codes,
			env_info,
			&var_needed);


		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var->resp_codes = 0;
				var_masks->attr_avail |= (unsigned long) VAR_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_RESP_CODES);
			}
		}
	}


	if (mask & VAR_DEFAULT_VALUE) {

		HB_DUMP_BINARY(	"VAR_RESP_CODES",
						(var->depbin ? var->depbin->db_default_value : NULL));

		rc = item_mask_man((unsigned long) VAR_DEFAULT_VALUE,
			var_masks,
			eval_default_value,
			(var->depbin ? var->depbin->db_default_value : NULL),
			&var->default_value,
			env_info,
			&var_needed);


		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR) 
			{
				var->default_value.type = 0;
				var_masks->attr_avail |= (unsigned long) VAR_DEFAULT_VALUE;
			}
			else 
			{
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_DEFAULT_VALUE);
			}
		}
	}


	if (mask & VAR_ACT_MASKS) {	/* if any action masks are set */

		if (var->actions == NULL) {

			return DDL_NULL_POINTER;
		}
		if ((var_masks->bin_hooked & VAR_ACT_MASKS) && (var->actions->depbin == NULL)) {

			return DDL_NULL_POINTER;
		}

		rc = eval_attr_var_actions(var, mask, env_info, errors);

	}

	if (mask & VAR_MISC_MASKS) {	/* if any misc masks are set */

		if (var->misc == NULL) {

			return DDL_NULL_POINTER;
		}
		if ((var_masks->bin_hooked & VAR_MISC_MASKS) && (var->misc->depbin == NULL)) {

			return DDL_NULL_POINTER;
		}

		rc = eval_attr_var_misc(var, mask, env_info, errors);
	}

#ifdef DEBUG
	dds_item_flat_check((void *) var, VARIABLE_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}



/*********************************************************************
 *
 *	Name: eval_item_program
 *	ShortDesc: evaluate the FLAT_PROGRAM
 *
 *	Description:
 *		eval_item_program will load the FLAT_PROGRAM structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_program() call
 *
 *	Outputs:
 *		program:		the loaded FLAT_PROGRAM structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, program->id,			\
								"Program", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_program(
FLAT_PROGRAM   *program,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *prog_masks;	/* temp ptr to the masks of a program */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_PROGRAM);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) program, PROGRAM_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	prog_masks = &program->masks;


	/*
	 * Sanity check
	 */

	if ((program->depbin == NULL) && (prog_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}


	if (mask & PROGRAM_ARGS) {

		HB_DUMP_BINARY(	"PROGRAM_ARGS",
						(program->depbin ? program->depbin->db_args : NULL));

		rc = item_mask_man((unsigned long) PROGRAM_ARGS,
			prog_masks,
			eval_attr_dataitems,
			(program->depbin ? program->depbin->db_args : NULL),
			&program->args,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) PROGRAM_ARGS);
		}
	}

	if (mask & PROGRAM_RESP_CODES) {

		HB_DUMP_BINARY(	"PROGRAM_RESP_CODES",
						(program->depbin ? program->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) PROGRAM_RESP_CODES,
			prog_masks,
			eval_attr_ref,
			(program->depbin ? program->depbin->db_resp_codes : NULL),
			&program->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				program->resp_codes = 0;
				prog_masks->attr_avail |= (unsigned long) PROGRAM_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) PROGRAM_RESP_CODES);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) program, PROGRAM_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_menu
 *	ShortDesc: evaluate the FLAT_MENU
 *
 *	Description:
 *		eval_item_menu will load the FLAT_MENU structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_menu() call
 *
 *	Outputs:
 *		menu:			the loaded FLAT_MENU structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, menu->id,			\
								"Menu", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_menu(
FLAT_MENU      *menu,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *menu_masks;	/* temp ptr to the masks of a menu */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_MENU);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) menu, MENU_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	menu_masks = &menu->masks;


	/*
	 * Sanity check
	 */

	if ((menu->depbin == NULL) && (menu_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}


	if (mask & MENU_LABEL) {

		HB_DUMP_BINARY(	"MENU_LABEL",
						(menu->depbin ? menu->depbin->db_label : NULL));

		eval_attr_string((unsigned long) MENU_LABEL,
			menu_masks,
			eval_string,
			(menu->depbin ? menu->depbin->db_label : NULL),
			&menu->label,
			env_info,
			errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & MENU_HELP) {

		HB_DUMP_BINARY(	"MENU_HELP",
						(menu->depbin ? menu->depbin->db_help : NULL));

		eval_attr_string((unsigned long) MENU_HELP,
			menu_masks,
			eval_string,
			(menu->depbin ? menu->depbin->db_help : NULL),
			&menu->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & MENU_ITEMS) {

		HB_DUMP_BINARY(	"MENU_ITEMS",
						(menu->depbin ? menu->depbin->db_items : NULL));

		rc = item_mask_man((unsigned long) MENU_ITEMS,
			menu_masks,
			eval_attr_menuitems,
			(menu->depbin ? menu->depbin->db_items : NULL),
			&menu->items,
			env_info,
			&var_needed);


		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) MENU_ITEMS);
		}
	}

	if (mask & MENU_STYLE) {

		HB_DUMP_BINARY(	"MENU_STYLE",
						(menu->depbin ? menu->depbin->db_style : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

		rc = DDL_SUCCESS;

        if (menu->depbin)
		{
			if (menu->depbin->db_style)
			{
				tmp_bin_chunk = menu->depbin->db_style->bin_chunk;
				tmp_bin_size = menu->depbin->db_style->bin_size;

				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != MENU_STYLE_TAG) {
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &menu->style);
				menu_masks->attr_avail |= MENU_STYLE;
			}
			else
			{
				rc = DDL_DEFAULT_ATTR;
			}
		}
		else
		{
			rc = DDL_DEFAULT_ATTR;
		}

		if (rc == DDL_DEFAULT_ATTR)
		{
			menu->style = NO_STYLE_TYPE;
			menu_masks->attr_avail |= MENU_STYLE;
		}
	}

	if (mask & MENU_VALID) {

		HB_DUMP_BINARY(	"MENU_VALIDITY",
						(menu->depbin ? menu->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) MENU_VALID,
			menu_masks,
			eval_attr_ulong,
			(menu->depbin ? menu->depbin->db_valid : NULL),
			&menu->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				menu->valid = TRUE;
				menu_masks->attr_avail |= (unsigned long) MENU_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) MENU_VALID);
			}
		}
	}

	if (mask & MENU_DEBUG)
	{
		HB_DUMP_BINARY( "MENU_DEBUG", ( menu->depbin ? menu->depbin->db_debug_info : NULL) );

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) MENU_DEBUG, menu_masks, eval_attr_debug_info, 
			(menu->depbin ? menu->depbin->db_debug_info : NULL), &menu->symbol_name, env_info, &var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				menu->symbol_name.str = L"TODO_Find_NaMe";
				menu_masks->attr_avail |= (unsigned long) MENU_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) MENU_DEBUG);
			}
		}
	}
#ifdef DEBUG
	dds_item_flat_check((void *) menu, MENU_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


///////////////////////////////////////////////////////////////////////////////////////
// Name: eval_method_type
// Desc: Reads the return value of a method from the binary and copies it into the flat
//
//
///////////////////////////////////////////////////////////////////////////////////////
int
eval_method_type(
unsigned long   attr_mask,
FLAT_MASKS     *masks,
eval_type		,
DEPBIN         *depbin,
void           *attribute,
ENV_INFO       *,
OP_REF         *)
{
	int             rc=DDL_SUCCESS;	/* return code */

#ifdef ISPTEST
	TEST_FAIL(MASK_MAN_EVAL);
#endif

	//
	// No binary exists
	//
	if (!(attr_mask & masks->bin_exists)) 
	{
		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;	/* value already exists */
		}
		else // Must be defaulted
		{
			rc = DDL_DEFAULT_ATTR;
		}
	}
	else if (!(attr_mask & masks->bin_hooked) && !(attr_mask & masks->dynamic)) // No binary hooked
	{
		//
		// If value is already available
		//
		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;
		}
		else // error, binary should be hooked up
		{
			rc = DDL_BINARY_REQUIRED;
		}
	}
	else // check the pointer
	{
		if (depbin == NULL) 
		{
			rc = DDL_NULL_POINTER;
		}
		else if ((!(attr_mask & masks->attr_avail)) || (attr_mask & masks->dynamic)) // check masks for evaluating
		{
			unsigned char  *chunk = depbin->bin_chunk;
			DDL_UINT        size = depbin->bin_size;
			DDL_UINT*		u_long = static_cast<DDL_UINT*>(attribute);
			DDL_UINT        tag;	/* identifier of a binary chunk */

			
			DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

			if (tag != PARAMETER_TAG) 
			{
				return DDL_ENCODING_ERROR;
			}

			DDL_PARSE_INTEGER(&chunk, &size, u_long);

			if (rc == DDL_SUCCESS) 
			{
				masks->attr_avail |= attr_mask;
			}
			if (depbin->dep.count) 
			{
				masks->dynamic |= attr_mask;
			}
			else // Turn off the dynamic mask for this attribute
			{
				masks->dynamic &= ~attr_mask;
			}
		}
		else // evaluation is not necessary
		{
			rc = DDL_SUCCESS;
		}
	}
	if (rc != DDL_SUCCESS) // Turn off the attr_available mask for this attribute
	{
		masks->attr_avail &= ~attr_mask;
	}
	return rc;
}

///////////////////////////////////////////////////////////////////////////////////////
// Name: eval_method_params
// Desc: Reads the parameters of a Method from the binary file and copies the contents 
//       into the flat.
//
//
///////////////////////////////////////////////////////////////////////////////////////
int
eval_method_params(
unsigned long   attr_mask,
FLAT_MASKS     *masks,
eval_type		,
DEPBIN         *depbin,
STRING         *attribute,
ENV_INFO       *,
OP_REF         *)
{
	int rc = DDL_SUCCESS;	// return code

#ifdef ISPTEST
	TEST_FAIL(MASK_MAN_EVAL);
#endif

	//
	// No binary exists
	//
	if (!(attr_mask & masks->bin_exists)) 
	{
		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;	/* value already exists */
		}
		else // Must be defaulted
		{
			rc = DDL_SUCCESS;
		}
	}
	else if (!(attr_mask & masks->bin_hooked) && !(attr_mask & masks->dynamic)) // No binary hooked
	{
		//
		// If value is already available
		//
		if (attr_mask & masks->attr_avail) 
		{
			rc = DDL_SUCCESS;
		}
		else // error, binary should be hooked up
		{
			rc = DDL_BINARY_REQUIRED;
		}
	}
	else // check the pointer
	{
		if (depbin == NULL) 
		{
			rc = DDL_NULL_POINTER;
		}
		else if ((!(attr_mask & masks->attr_avail)) || (attr_mask & masks->dynamic)) // check masks for evaluating
		{
			unsigned char  *chunk = depbin->bin_chunk;
			DDL_UINT        size = depbin->bin_size;
			DDL_UINT        tag;	/* identifier of a binary chunk */
			DDL_UINT		lVariableType;

			DDL_PARSE_TAG(&chunk, &size, &tag, (DDL_UINT *) NULL_PTR);

			if (tag != PARAMETER_SEQLIST_TAG) 
			{
				return DDL_ENCODING_ERROR;
			}
			
			unsigned long	lValueSize=2048;//something big
			char *pszValue = (char *)malloc( lValueSize );
			memset( pszValue, 0, lValueSize );

			do
			{
				unsigned long len=0;
				DDL_PARSE_TAG(&chunk, &size, &tag, &len );
				if (tag != PARAMETER_TAG) //We need to get the parameter tag
				{
					break;//Not it. Then bail
				}

				//read the variable type of the parameter
				DDL_PARSE_INTEGER(&chunk, &size, &lVariableType);
				if( lVariableType == 0 )//zero, is un-used.  Bail
				{
					break;
				}

				ulong modifiers=0;
				ddl_parse_bitstring(&chunk,&len, &modifiers );
				
				DDL_PARSE_TAG(&chunk,&size,&tag,&len);
				if(tag != NAME_STR_TAG) //This should be followed by the parameter name
				{
					break;//if not, something is wrong. Bail
				}

				unsigned long name_str_len=0;
				DDL_PARSE_INTEGER( &chunk, &size, &name_str_len);//Get the length of the parameter name

				if( name_str_len )//parameter name should be greater than zero bytes.
				{
					char *pszVariableName = (char *)malloc(name_str_len+1);//allocate the memory
					memset( pszVariableName, 0, name_str_len+1 );//clear the memory
					memcpy(pszVariableName, chunk, name_str_len);//copy the contents

					chunk += name_str_len;//move the pointers
					size -= name_str_len;
					
					if( lValueSize < (strlen(pszValue) + strlen(pszVariableName) + 20) )//will we have enough memory, if not - re-alloc
					{
						lValueSize = 2 * (strlen(pszValue) + strlen(pszVariableName) + 20);//calculate twice what will be necessary.
						char *pTempBuff = (char *)malloc( lValueSize );//malloc the memory
						strcpy( pTempBuff, pszValue );
						free( pszValue );//free the old buffer.
						pszValue = pTempBuff;//copy the pointer.
					}

					//- Note-replace this line with lines below
					//sprintf( pszValue, "%s%d;%d;%s;", pszValue, lVariableType, modifiers, pszVariableName );//build the total length string.
					//This prevents this error in CPPcheck:   The variable 'pszValue' is used both as a parameter and as destination in s[n]printf(). 
					//The origin and destination buffers overlap.

                    char strVarTypeAndModifiers[20]{0};
					sprintf(strVarTypeAndModifiers,"%d;%d;",lVariableType, modifiers);
					strcat(pszValue,strVarTypeAndModifiers);
					strcat(pszValue, pszVariableName);
					strcat(pszValue,";");

					free( pszVariableName );//free memory for the next round.
				}
			}while( size > 0 );


			size_t attribute_len = (unsigned short)strlen(pszValue);
			
			if (attribute_len)
			{
				attribute->str = (wchar_t *)malloc( (attribute_len + 1) * sizeof(wchar_t));
		
				if (!attribute->str)
				{
					free( pszValue );
					return DDL_MEMORY_ERROR;
				}

				int numChars = 0;	// Number of wide characters

				numChars = PS_MultiByteToWideChar(SB_CODE_PAGE, 0, pszValue, attribute_len,
																attribute->str, attribute_len + 1);

				if (numChars == 0)	// Error
				{
					return DDL_ENCODING_ERROR;
				}

				attribute->len = (unsigned short)numChars;
				attribute->flags = FREE_STRING;
				attribute->str[numChars] = L'\0';	// Null terminate
			}

			free( pszValue );//we are done building the parameters string, then free the memory.

			if (rc == DDL_SUCCESS) 
			{
				masks->attr_avail |= attr_mask;
			}
			if (depbin->dep.count) 
			{
				masks->dynamic |= attr_mask;
			}
			else // Turn off the dynamic mask for this attribute
			{
				masks->dynamic &= ~attr_mask;
			}
		}
		else // evaluation is not necessary
		{
			rc = DDL_SUCCESS;
		}
	}
	if (rc != DDL_SUCCESS) // Turn off the attr_available mask for this attribute
	{
		masks->attr_avail &= ~attr_mask;
	}
	return rc;
}

/*********************************************************************
 *
 *	Name: eval_item_edit_display
 *	ShortDesc: evaluate the FLAT_EDIT_DISPLAY
 *
 *	Description:
 *		eval_item_edit_display will load the FLAT_EDIT_DISPLAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_edit_display() call
 *
 *	Outputs:
 *		edit_display:	the loaded FLAT_EDIT_DISPLAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, edit_display->id,			\
								"Edit_Display", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif


int eval_item_edit_display( FLAT_EDIT_DISPLAY *edit_display, unsigned long mask, ENV_INFO *env_info, RETURN_LIST *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *edit_disp_masks;	/* temp ptr to the masks of a var */	 					 

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_EDIT_DISPLAY);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) edit_display, EDIT_DISP_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	edit_disp_masks = &edit_display->masks;

	/*
	 * Sanity check
	 */

	if ((edit_display->depbin == NULL) && (edit_disp_masks->bin_hooked)) 
	{
		return DDL_NULL_POINTER;
	}


	if (mask & EDIT_DISPLAY_DISP_ITEMS) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_DISPLAY_ITEMS",
						(edit_display->depbin ? edit_display->depbin->db_disp_items : NULL));

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_DISP_ITEMS,
			edit_disp_masks,
			eval_op_ref_trail_list,
			(edit_display->depbin ? edit_display->depbin->db_disp_items : NULL),
			&edit_display->disp_items,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR) 
			{
				edit_disp_masks->attr_avail |= (unsigned long) EDIT_DISPLAY_DISP_ITEMS;
			}
			else 
			{
				load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_DISP_ITEMS);
			}
		}
	}

	if (mask & EDIT_DISPLAY_EDIT_ITEMS) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_EDIT_ITEMS",
						(edit_display->depbin ? edit_display->depbin->db_edit_items : NULL));

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_EDIT_ITEMS,
			edit_disp_masks,
			eval_op_ref_trail_list,
			(edit_display->depbin ? edit_display->depbin->db_edit_items : NULL),
			&edit_display->edit_items,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_EDIT_ITEMS);
		}
	}

	if (mask & EDIT_DISPLAY_LABEL) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_LABEL",
						(edit_display->depbin ? edit_display->depbin->db_label : NULL));

		eval_attr_string((unsigned long) EDIT_DISPLAY_LABEL,
			edit_disp_masks,
			eval_string,
			(edit_display->depbin ? edit_display->depbin->db_label : NULL),
			&edit_display->label,
			env_info,
			errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & EDIT_DISPLAY_HELP) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_HELP",
						(edit_display->depbin ? edit_display->depbin->db_help : NULL));

		eval_attr_string((unsigned long) EDIT_DISPLAY_HELP,
			edit_disp_masks,
			eval_string,
			(edit_display->depbin ? edit_display->depbin->db_help : NULL),
			&edit_display->help,
			env_info,
			errors,
			DEFAULT_STD_DICT_HELP);
	}

	if (mask & EDIT_DISPLAY_VALID) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_VALIDITY",
			(edit_display->depbin ? edit_display->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) EDIT_DISPLAY_VALID,
			edit_disp_masks,
			eval_attr_ulong,
			(edit_display->depbin ? edit_display->depbin->db_valid : NULL),
			&edit_display->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR) 
			{
				edit_display->valid = TRUE;
				edit_disp_masks->attr_avail |= (unsigned long) EDIT_DISPLAY_VALID;
			}
			else 
			{
				load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_VALID);
			}
		}
	}

	if (mask & EDIT_DISPLAY_PRE_EDIT_ACT) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_PRE_EDIT_ACTIONS",
						(edit_display->depbin ? edit_display->depbin->db_pre_edit_act : NULL));

		rc = eval_attr_reflist(edit_disp_masks,
			(unsigned long) EDIT_DISPLAY_PRE_EDIT_ACT,
			env_info,
			errors,
			&edit_display->pre_edit_act,
			(edit_display->depbin ? edit_display->depbin->db_pre_edit_act : NULL));
	}

	if (mask & EDIT_DISPLAY_POST_EDIT_ACT) 
	{
		HB_DUMP_BINARY(	"EDIT_DISPLAY_POST_EDIT_ACTIONS",
						(edit_display->depbin ? edit_display->depbin->db_post_edit_act : NULL));

		rc = eval_attr_reflist(edit_disp_masks,
			(unsigned long) EDIT_DISPLAY_POST_EDIT_ACT,
			env_info,
			errors,
			&edit_display->post_edit_act,
			(edit_display->depbin ? edit_display->depbin->db_post_edit_act : NULL));

	}

	if( mask & EDIT_DISPLAY_DEBUG )
	{
		HB_DUMP_BINARY( "EDIT_DISPLAY_DEBUG", (edit_display->depbin ? edit_display->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) EDIT_DISPLAY_DEBUG,
			edit_disp_masks,
			eval_attr_debug_info,
			(edit_display->depbin ? edit_display->depbin->db_debug_info : NULL),
			&edit_display->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				edit_display->symbol_name.str = L"TODO_Find_NaMe";
				edit_disp_masks->attr_avail |= (unsigned long) EDIT_DISPLAY_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) EDIT_DISPLAY_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) edit_display, EDIT_DISP_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}




/*********************************************************************
 *
 *	Name: eval_item_method
 *	ShortDesc: evaluate the FLAT_METHOD
 *
 *	Description:
 *		eval_item_method will load the FLAT_METHOD structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_method() call
 *
 *	Outputs:
 *		method:			the loaded FLAT_METHOD structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from eval_attr_string(), eval_attr_reflist(), item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, method->id,			\
								"Method", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif


int
eval_item_method(
FLAT_METHOD    *method,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *meth_masks;	/* temp ptr to the masks of a method */
	unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_METHOD);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) method, METHOD_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	meth_masks = &method->masks;


	if ((method->depbin == NULL) && (meth_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}


	if (mask & METHOD_CLASS) {

		HB_DUMP_BINARY(	"METHOD_CLASS",
						(method->depbin ? method->depbin->db_class : NULL));

		rc = item_mask_man((unsigned long) METHOD_CLASS,
			meth_masks,
			eval_attr_bitstring,
			(method->depbin ? method->depbin->db_class : NULL),
			&method->class_attr,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) METHOD_CLASS);
		}
	}

	if (mask & METHOD_DEF) {

		HB_DUMP_BINARY(	"METHOD_DEFINITION",
						(method->depbin ? method->depbin->db_def : NULL));

		rc = item_mask_man((unsigned long) METHOD_DEF,
			meth_masks,
			eval_attr_definition,
			(method->depbin ? method->depbin->db_def : NULL),
			&method->def,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) METHOD_DEF);
		}
	}


	if (mask & METHOD_HELP) {

		HB_DUMP_BINARY(	"METHOD_HELP",
						(method->depbin ? method->depbin->db_help : NULL));

		eval_attr_string((unsigned long) METHOD_HELP,
			meth_masks,
			eval_string,
			(method->depbin ? method->depbin->db_help : NULL),
			&method->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & METHOD_LABEL) {

		HB_DUMP_BINARY(	"METHOD_LABEL",
						(method->depbin ? method->depbin->db_label : NULL));

		eval_attr_string((unsigned long) METHOD_LABEL,
			meth_masks,
			eval_string,
			(method->depbin ? method->depbin->db_label : NULL),
			&method->label,
			env_info,
			errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & METHOD_VALID) {

		HB_DUMP_BINARY(	"METHOD_VALIDITY",
						(method->depbin ? method->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) METHOD_VALID,
			meth_masks,
			eval_attr_ulong,
			(method->depbin ? method->depbin->db_valid : NULL),
			&method->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				method->valid = TRUE;
				meth_masks->attr_avail |= (unsigned long) METHOD_VALID;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) METHOD_VALID);
			}
		}
	}

	if (mask & METHOD_SCOPE) {

		HB_DUMP_BINARY(	"METHOD_SCOPE",
						(method->depbin ? method->depbin->db_scope : NULL));

		tmp_bin_chunk = method->depbin->db_scope->bin_chunk;
		tmp_bin_size = method->depbin->db_scope->bin_size;
		DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size,
			&method->scope);

		/*
		 * Must set the attr_available bits.
		 */

		meth_masks->attr_avail |= METHOD_SCOPE;
	}

	if (mask & METHOD_TYPE) 
	{
		HB_DUMP_BINARY(	"METHOD_TYPE",
						(method->depbin ? method->depbin->db_type : NULL));

		rc = eval_method_type((unsigned long) METHOD_TYPE,
			meth_masks,
			eval_attr_ulong,
			(method->depbin ? method->depbin->db_type : NULL),
			&method->type,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR) 
			{
				method->valid = TRUE;
				meth_masks->attr_avail |= (unsigned long) METHOD_TYPE;
			}
			else 
			{
				load_errors(errors, rc, &var_needed, (unsigned long) METHOD_TYPE);
			}
		}
	}

	if (mask & METHOD_PARAMS) 
	{
		HB_DUMP_BINARY(	"METHOD_METHOD_PARAMETERS",
						(method->depbin ? method->depbin->db_params : NULL));

		rc = eval_method_params((unsigned long) METHOD_PARAMS,
			meth_masks,
			eval_string,
			(method->depbin ? method->depbin->db_params : NULL),
			&method->params,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR) 
			{
				method->valid = TRUE;
				meth_masks->attr_avail |= (unsigned long) METHOD_PARAMS;
			}
			else 
			{
				load_errors(errors, rc, &var_needed, (unsigned long) METHOD_PARAMS);
			}
		}
	}

	if( mask & METHOD_DEBUG )
	{
		HB_DUMP_BINARY( "METHOD_DEBUG", ( method->depbin ? method->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) METHOD_DEBUG,
			meth_masks,
			eval_attr_debug_info,
			(method->depbin ? method->depbin->db_debug_info : NULL),
			&method->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				method->symbol_name.str = L"TODO_Find_NaMe";
				meth_masks->attr_avail |= (unsigned long) METHOD_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) METHOD_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) method, METHOD_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}



/*********************************************************************
 *
 *	Name: eval_item_refresh
 *	ShortDesc: evaluate the FLAT_REFRESH
 *
 *	Description:
 *		eval_item_refresh will load the FLAT_REFRESH structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_refresh() call
 *
 *	Outputs:
 *		refresh:		the loaded FLAT_REFRESH structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, refresh->id,			\
								"Refresh", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_refresh(
FLAT_REFRESH   *refresh,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_REFRESH);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) refresh, REFRESH_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */


	if ((refresh->depbin == NULL) && (refresh->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}


	if (mask & REFRESH_ITEMS) {

		HB_DUMP_BINARY(	"REFRESH_LIST",
						(refresh->depbin ? refresh->depbin->db_items : NULL));

		rc = item_mask_man((unsigned long) REFRESH_ITEMS,
			&refresh->masks,
			eval_attr_refresh,
			(refresh->depbin ? refresh->depbin->db_items : NULL),
			&refresh->items,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) REFRESH_ITEMS);
		}
	}

	if( mask & REFRESH_DEBUG )
	{
		HB_DUMP_BINARY( "REFRESH_DEBUG", ( refresh->depbin ? refresh->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) REFRESH_DEBUG,
			&refresh->masks,
			eval_attr_debug_info,
			(refresh->depbin ? refresh->depbin->db_debug_info : NULL),
			&refresh->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				refresh->symbol_name.str = L"TODO_Find_NaMe";
				refresh->masks.attr_avail |= (unsigned long) REFRESH_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) REFRESH_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) refresh, REFRESH_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}




/*********************************************************************
 *
 *	Name: eval_item_unit
 *	ShortDesc: evaluate the FLAT_UNIT
 *
 *	Description:
 *		eval_item_unit will load the FLAT_UNIT structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_unit() call
 *
 *	Outputs:
 *		unit:			the loaded FLAT_UNIT structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, unit->id,			\
								"Unit", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif
int
eval_item_unit(
FLAT_UNIT      *unit,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_UNIT);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) unit, UNIT_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */


	if ((unit->depbin == NULL) && (unit->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & UNIT_ITEMS) {

		HB_DUMP_BINARY(	"UNIT_LIST",
						(unit->depbin ? unit->depbin->db_items : NULL));

		rc = item_mask_man((unsigned long) UNIT_ITEMS,
			&unit->masks,
			eval_attr_unit,
			(unit->depbin ? unit->depbin->db_items : NULL),
			&unit->items,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) UNIT_ITEMS);
		}
	}

	if( mask & UNIT_DEBUG )
	{
		HB_DUMP_BINARY( "UNIT_DEBUG", ( unit->depbin ? unit->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) UNIT_DEBUG,
			&unit->masks,
			eval_attr_debug_info,
			(unit->depbin ? unit->depbin->db_debug_info : NULL),
			&unit->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				unit->symbol_name.str = L"TODO_Find_NaMe";
				unit->masks.attr_avail |= (unsigned long) UNIT_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) UNIT_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) unit, UNIT_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_wao
 *	ShortDesc: evaluate the FLAT_WAO
 *
 *	Description:
 *		eval_item_wao will load the FLAT_WAO structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_wao() call
 *
 *	Outputs:
 *		wao:			the loaded FLAT_WAO structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/

#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, wao->id,			\
								"Write_As_One", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_wao(
FLAT_WAO       *wao,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_WAO);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) wao, WAO_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */


	if ((wao->depbin == NULL) && (wao->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & WAO_ITEMS) {

		HB_DUMP_BINARY(	"WAO_REFERENCES",
						(wao->depbin ? wao->depbin->db_items : NULL));

		rc = item_mask_man((unsigned long) WAO_ITEMS,
			&wao->masks,
			eval_op_ref_trail_list,
			(wao->depbin ? wao->depbin->db_items : NULL),
			&wao->items,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) WAO_ITEMS);
		}
	}

	if( mask & WAO_DEBUG )
	{
		HB_DUMP_BINARY( "WAO_DEBUG", ( wao->depbin ? wao->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) WAO_DEBUG,
			&wao->masks,
			eval_attr_debug_info,
			(wao->depbin ? wao->depbin->db_debug_info : NULL),
			&wao->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				wao->symbol_name.str = L"TODO_Find_NaMe";
				wao->masks.attr_avail |= (unsigned long) WAO_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) WAO_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) wao, WAO_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_itemarray
 *	ShortDesc: evaluate the FLAT_ITEM_ARRAY
 *
 *	Description:
 *		eval_item_itemarray will load the FLAT_ITEM_ARRAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_itemarray() call
 *
 *	Outputs:
 *		item_array:		the loaded FLAT_ITEM_ARRAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, item_array->id,			\
								"Item_Array", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_itemarray(
FLAT_ITEM_ARRAY *item_array,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *item_array_masks;	/* temp ptr to the masks of an
						 * item_array */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_ITEM_ARRAY);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) item_array, ITEM_ARRAY_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	item_array_masks = &item_array->masks;


	if ((item_array->depbin == NULL) && (item_array_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & ITEM_ARRAY_ELEMENTS) {

		HB_DUMP_BINARY(	"ITEM_ARRAY_ELEMENTS",
						(item_array->depbin ? item_array->depbin->db_elements : NULL));

		rc = item_mask_man((unsigned long) ITEM_ARRAY_ELEMENTS,
			item_array_masks,
			eval_attr_itemarray,
			(item_array->depbin ? item_array->depbin->db_elements : NULL),
			&item_array->elements,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) ITEM_ARRAY_ELEMENTS);
		}
	}

	if (mask & ITEM_ARRAY_HELP) {

		HB_DUMP_BINARY(	"ITEM_ARRAY_HELP",
						(item_array->depbin ? item_array->depbin->db_help : NULL));

		eval_attr_string((unsigned long) ITEM_ARRAY_HELP,
			item_array_masks,
			eval_string,
			(item_array->depbin ? item_array->depbin->db_help : NULL),
			&item_array->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & ITEM_ARRAY_VALIDITY) {

		HB_DUMP_BINARY(	"ITEM_ARRAY_VALIDITY",
						(item_array->depbin ? item_array->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) ITEM_ARRAY_VALIDITY,
			item_array_masks,
			eval_attr_ulong,
			(item_array->depbin ? item_array->depbin->db_valid : NULL),
			&item_array->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				item_array->valid = TRUE;
				item_array_masks->attr_avail |= (unsigned long) ITEM_ARRAY_VALIDITY;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) ITEM_ARRAY_VALIDITY);
			}
		}
	}

	if (mask & ITEM_ARRAY_LABEL) {

		HB_DUMP_BINARY(	"ITEM_ARRAY_LABEL",
						(item_array->depbin ? item_array->depbin->db_label : NULL));

		eval_attr_string((unsigned long) ITEM_ARRAY_LABEL,
			item_array_masks,
			eval_string,
			(item_array->depbin ? item_array->depbin->db_label : NULL),
			&item_array->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if( mask & ITEM_ARRAY_DEBUG )
	{
		HB_DUMP_BINARY( "ITEM_ARRAY_DEBUG", ( item_array->depbin ? item_array->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) ITEM_ARRAY_DEBUG,
			item_array_masks,
			eval_attr_debug_info,
			(item_array->depbin ? item_array->depbin->db_debug_info : NULL),
			&item_array->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				item_array->symbol_name.str = L"TODO_Find_NaMe";
				item_array_masks->attr_avail |= (unsigned long) ITEM_ARRAY_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) ITEM_ARRAY_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) item_array, ITEM_ARRAY_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}

/*********************************************************************
 *
 *	Name: eval_item_array
 *	ShortDesc: evaluate the FLAT_ARRAY
 *
 *	Description:
 *		eval_item_array will load the FLAT_ARRAY structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_array() call
 *
 *	Outputs:
 *		array:			the loaded FLAT_ARRAY structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, array->id,			\
								"Array", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_array(
FLAT_ARRAY     *array,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *array_masks;	/* temp ptr to the masks of a array */
	unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	unsigned char  *tmp_bin_chunk;	/* temp ptr to DEPBIN chunk */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_ARRAY);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) array, ARRAY_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	array_masks = &array->masks;


	if ((array->depbin == NULL) && (array_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & ARRAY_NUM_OF_ELEMENTS) {

		HB_DUMP_BINARY(	"ARRAY_NUM_OF_ELEMENTS",
						(array->depbin ? array->depbin->db_num_of_elements : NULL));

		tmp_bin_chunk = array->depbin->db_num_of_elements->bin_chunk;
		tmp_bin_size = array->depbin->db_num_of_elements->bin_size;
		DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size,
			&array->num_of_elements);

		/*
		 * Must set the attr_available bits.
		 */

		array_masks->attr_avail |= ARRAY_NUM_OF_ELEMENTS;

	}

	if (mask & ARRAY_HELP) {

		HB_DUMP_BINARY(	"ARRAY_HELP",
						(array->depbin ? array->depbin->db_help : NULL));

		eval_attr_string((unsigned long) ARRAY_HELP,
			array_masks,
			eval_string,
			(array->depbin ? array->depbin->db_help : NULL),
			&array->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & ARRAY_LABEL) {

		HB_DUMP_BINARY(	"ARRAY_LABEL",
						(array->depbin ? array->depbin->db_label : NULL));

		eval_attr_string((unsigned long) ARRAY_LABEL,
			array_masks,
			eval_string,
			(array->depbin ? array->depbin->db_label : NULL),
			&array->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & ARRAY_TYPE) {

		HB_DUMP_BINARY(	"ARRAY_TYPE",
						(array->depbin ? array->depbin->db_type : NULL));

		rc = item_mask_man((unsigned long) ARRAY_TYPE,
			array_masks,
			eval_attr_ref,
			(array->depbin ? array->depbin->db_type : NULL),
			&array->type,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_TYPE);
		}
	}

	if (mask & ARRAY_RESP_CODES) {

		HB_DUMP_BINARY(	"ARRAY_RESP_CODES",
						(array->depbin ? array->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) ARRAY_RESP_CODES,
			array_masks,
			eval_attr_ref,
			(array->depbin ? array->depbin->db_resp_codes : NULL),
			&array->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				array->resp_codes = 0;
				array_masks->attr_avail |= (unsigned long) ARRAY_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_RESP_CODES);
			}
		}
	}

	if (mask & ARRAY_VALID) {

		HB_DUMP_BINARY(	"ARRAY_VALID",
						(array->depbin ? array->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) ARRAY_VALID,
			array_masks,
			eval_attr_ulong,
			(array->depbin ? array->depbin->db_valid : NULL),
			&array->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				array->valid = TRUE;
				array_masks->attr_avail |= (unsigned long) ARRAY_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_VALID);
			}
		}
	}

	if (mask & ARRAY_DEBUG)
	{
		HB_DUMP_BINARY( "ARRAY_DEBUG", ( array->depbin ? array->depbin->db_debug_info : NULL) );

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) ARRAY_DEBUG, array_masks, eval_attr_debug_info, 
			(array->depbin ? array->depbin->db_debug_info : NULL), &array->symbol_name, env_info, &var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				array->symbol_name.str = L"TODO_Find_NaMe";
				array_masks->attr_avail |= (unsigned long) ARRAY_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) ARRAY_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) array, ARRAY_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_collection
 *	ShortDesc: evaluate the FLAT_COLLECTION
 *
 *	Description:
 *		eval_item_collection will load the FLAT_COLLECTION structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_collection() call
 *
 *	Outputs:
 *		collection:		the loaded FLAT_COLLECTION structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, collection->id,			\
								"Collection", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_collection(
FLAT_COLLECTION *collection,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *coll_masks;	/* temp ptr to the masks of a
					 * collection */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_COLLECTION);
#endif


#ifdef DEBUG
	dds_item_flat_check((void *) collection, COLLECTION_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	coll_masks = &collection->masks;


	if ((collection->depbin == NULL) && (coll_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & COLLECTION_MEMBERS) {

		HB_DUMP_BINARY(	"COLLECTION_MEMBERS",
						(collection->depbin ? collection->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) COLLECTION_MEMBERS,
			coll_masks,
			eval_attr_op_members,
			(collection->depbin ? collection->depbin->db_members : NULL),
			&collection->members,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) COLLECTION_MEMBERS);
		}
	}

	if (mask & COLLECTION_HELP) {

		HB_DUMP_BINARY(	"COLLECTION_HELP",
						(collection->depbin ? collection->depbin->db_help : NULL));

		eval_attr_string((unsigned long) COLLECTION_HELP,
			coll_masks,
			eval_string,
			(collection->depbin ? collection->depbin->db_help : NULL),
			&collection->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & COLLECTION_LABEL) {

		HB_DUMP_BINARY(	"COLLECTION_LABEL",
						(collection->depbin ? collection->depbin->db_label : NULL));

		eval_attr_string((unsigned long) COLLECTION_LABEL,
			coll_masks,
			eval_string,
			(collection->depbin ? collection->depbin->db_label : NULL),
			&collection->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & COLLECTION_VALIDITY) {

		HB_DUMP_BINARY(	"COLLECTION_VALIDITY",
						(collection->depbin ? collection->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) COLLECTION_VALIDITY,
			coll_masks,
			eval_attr_ulong,
			(collection->depbin ? collection->depbin->db_valid : NULL),
			&collection->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				collection->valid = TRUE;
				coll_masks->attr_avail |= (unsigned long) COLLECTION_VALIDITY;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) COLLECTION_VALIDITY);
			}
		}
	}	
	
	if( mask & COLLECTION_DEBUG )
	{
		HB_DUMP_BINARY( "COLLECTION_DEBUG", ( collection->depbin ? collection->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) COLLECTION_DEBUG,
			coll_masks,
			eval_attr_debug_info,
			(collection->depbin ? collection->depbin->db_debug_info : NULL),
			&collection->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				collection->symbol_name.str = L"TODO_Find_NaMe";
				coll_masks->attr_avail |= (unsigned long) COLLECTION_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) COLLECTION_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) collection, COLLECTION_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}



/*********************************************************************
 *
 *	Name: eval_item_record
 *	ShortDesc: evaluate the FLAT_RECORD
 *
 *	Description:
 *		eval_item_record will load the FLAT_RECORD structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_record() call
 *
 *	Outputs:
 *		record:			the loaded FLAT_RECORD structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, record->id,		\
								"Record", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif


int
eval_item_record(
FLAT_RECORD    *record,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *rec_masks;	/* temp ptr to the masks of a record */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_RECORD);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) record, RECORD_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	rec_masks = &record->masks;


	if ((record->depbin == NULL) && (rec_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & RECORD_MEMBERS) {

		HB_DUMP_BINARY(	"RECORD_MEMBERS",
						(record->depbin ? record->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) RECORD_MEMBERS,
			rec_masks,
			eval_attr_members,
			(record->depbin ? record->depbin->db_members : NULL),
			&record->members,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) RECORD_MEMBERS);
		}
	}

	if (mask & RECORD_HELP) {

		HB_DUMP_BINARY(	"RECORD_HELP",
						(record->depbin ? record->depbin->db_help : NULL));

		eval_attr_string((unsigned long) RECORD_HELP,
			rec_masks,
			eval_string,
			(record->depbin ? record->depbin->db_help : NULL),
			&record->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & RECORD_LABEL) {

		HB_DUMP_BINARY(	"RECORD_LABEL",
						(record->depbin ? record->depbin->db_label : NULL));

		eval_attr_string((unsigned long) RECORD_LABEL,
			rec_masks,
			eval_string,
			(record->depbin ? record->depbin->db_label : NULL),
			&record->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & RECORD_RESP_CODES) {

		HB_DUMP_BINARY(	"RECORD_RESP_CODES",
						(record->depbin ? record->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) RECORD_RESP_CODES,
			rec_masks,
			eval_attr_ref,
			(record->depbin ? record->depbin->db_resp_codes : NULL),
			&record->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				record->resp_codes = 0;
				rec_masks->attr_avail |= (unsigned long) RECORD_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) RECORD_RESP_CODES);
			}
		}
	}

	if( mask & RECORD_DEBUG )
	{
		//
		// There is no debug information for records, so in order to get the symbol name 
		// of this one record that is apart of the device parameters we hard code the name. 
		// 
		record->symbol_name.str = L"hart_blk_char";
		rec_masks->attr_avail |= (unsigned long) RECORD_DEBUG;
		
	}

#ifdef DEBUG
	dds_item_flat_check((void *) record, RECORD_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}



/*********************************************************************
 *
 *	Name: eval_item_var_list
 *	ShortDesc: evaluate the FLAT_VAR_LIST
 *
 *	Description:
 *		eval_item_var_list will load the FLAT_VAR_LIST structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_var_list() call
 *
 *	Outputs:
 *		var_list:		the loaded FLAT_VAR_LIST structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, var_list->id,			\
								"Variable_List", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_var_list(
FLAT_VAR_LIST  *var_list,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *var_list_masks;	/* temp ptr to the masks of a var */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_VAR_LIST);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) var_list, VAR_LIST_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	var_list_masks = &var_list->masks;


	if ((var_list->depbin == NULL) && (var_list_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & VAR_LIST_MEMBERS) {

		HB_DUMP_BINARY(	"VAR_LIST_MEMBERS",
						(var_list->depbin ? var_list->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) VAR_LIST_MEMBERS,
			var_list_masks,
			eval_attr_members,
			(var_list->depbin ? var_list->depbin->db_members : NULL),
			&var_list->members,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) VAR_LIST_MEMBERS);
		}
	}

	if (mask & VAR_LIST_HELP) {

		HB_DUMP_BINARY(	"VAR_LIST_HELP",
						(var_list->depbin ? var_list->depbin->db_help : NULL));

		eval_attr_string((unsigned long) VAR_LIST_HELP,
			var_list_masks,
			eval_string,
			(var_list->depbin ? var_list->depbin->db_help : NULL),
			&var_list->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & VAR_LIST_LABEL) {

		HB_DUMP_BINARY(	"VAR_LIST_LABEL",
						(var_list->depbin ? var_list->depbin->db_label : NULL));

		eval_attr_string((unsigned long) VAR_LIST_LABEL,
			var_list_masks,
			eval_string,
			(var_list->depbin ? var_list->depbin->db_label : NULL),
			&var_list->label,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_LABEL);
	}

	if (mask & VAR_LIST_RESP_CODES) {

		HB_DUMP_BINARY(	"VAR_LIST_RESP_CODES",
						(var_list->depbin ? var_list->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) VAR_LIST_RESP_CODES,
			var_list_masks,
			eval_attr_ref,
			(var_list->depbin ? var_list->depbin->db_resp_codes : NULL),
			&var_list->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				var_list->resp_codes = 0;
				var_list_masks->attr_avail |= (unsigned long) VAR_LIST_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) VAR_LIST_RESP_CODES);
			}
		}
	}

	if( mask & VAR_LIST_DEBUG )
	{
		HB_DUMP_BINARY( "VAR_LIST_DEBUG", ( var_list->depbin ? var_list->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) VAR_LIST_DEBUG,
			var_list_masks,
			eval_attr_debug_info,
			(var_list->depbin ? var_list->depbin->db_debug_info : NULL),
			&var_list->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				var_list->symbol_name.str = L"TODO_Find_NaMe";
				var_list_masks->attr_avail |= (unsigned long) VAR_LIST_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) VAR_LIST_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) var_list, VAR_LIST_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}

/*********************************************************************
 *
 *	Name: eval_item_block
 *	ShortDesc: evaluate the FLAT_BLOCK
 *
 *	Description:
 *		eval_item_block will load the FLAT_BLOCK structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_block() call
 *
 *	Outputs:
 *		block:			the loaded FLAT_BLOCK structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man() eval_attr_string()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, block->id,			\
								"BlockA", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_block(
FLAT_BLOCK     *block,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *block_masks;	/* temp ptr to the masks of a block */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_BLOCK);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) block, BLOCK_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	block_masks = &block->masks;


	if ((block->depbin == NULL) && (block_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & BLOCK_CHARACTERISTIC) {

		HB_DUMP_BINARY(	"BLOCKA_CHARACTERISTICS",
						(block->depbin ? block->depbin->db_characteristic : NULL));

		rc = item_mask_man((unsigned long) BLOCK_CHARACTERISTIC,
			block_masks,
			eval_attr_ref,
			(block->depbin ? block->depbin->db_characteristic : NULL),
			&block->characteristic,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) 
		{
			if (rc == DDL_DEFAULT_ATTR)
			{
				block->characteristic = 0;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_CHARACTERISTIC);
			}
		}
	}

	if (mask & BLOCK_HELP) {

		HB_DUMP_BINARY(	"BLOCKA_HELP",
						(block->depbin ? block->depbin->db_help : NULL));

		eval_attr_string((unsigned long) BLOCK_HELP,
			block_masks,
			eval_string,
			(block->depbin ? block->depbin->db_help : NULL),
			&block->help,
			env_info,
			errors,
			(unsigned long) DEFAULT_STD_DICT_HELP);
	}

	if (mask & BLOCK_LABEL) {

		HB_DUMP_BINARY(	"BLOCKA_LABEL",
						(block->depbin ? block->depbin->db_label : NULL));

		eval_attr_string((unsigned long) BLOCK_LABEL,
			block_masks,
			eval_string,
			(block->depbin ? block->depbin->db_label : NULL),
			&block->label,
			env_info,
			errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & BLOCK_PARAM) {

		HB_DUMP_BINARY(	"BLOCKA_PARAMETERS",
						(block->depbin ? block->depbin->db_param : NULL));

		rc = item_mask_man((unsigned long) BLOCK_PARAM,
			block_masks,
			eval_attr_members,
			(block->depbin ? block->depbin->db_param : NULL),
			&block->param,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_PARAM);
		}
	}

	if (mask & BLOCK_PARAM_LIST) {

		HB_DUMP_BINARY(	"BLOCKA_PARAM_LIST",
						(block->depbin ? block->depbin->db_param_list : NULL));

		rc = item_mask_man((unsigned long) BLOCK_PARAM_LIST,
			block_masks,
			eval_attr_members,
			(block->depbin ? block->depbin->db_param_list : NULL),
			&block->param_list,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				block->param_list.count = 0;

				block_masks->attr_avail |= (unsigned long) BLOCK_PARAM_LIST;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) BLOCK_PARAM_LIST);
			}
		}
	}

	if (mask & BLOCK_ITEM_ARRAY) {

		HB_DUMP_BINARY(	"BLOCKA_ITEM_ARRAY_ITEMS",
						(block->depbin ? block->depbin->db_item_array : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_ITEM_ARRAY, env_info,
			errors, &block->item_array, (block->depbin ? block->depbin->db_item_array : NULL));
	}

	if (mask & BLOCK_COLLECT) {

		HB_DUMP_BINARY(	"BLOCKA_COLLECTION_ITEMS",
						(block->depbin ? block->depbin->db_collect : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_COLLECT, env_info,
			errors, &block->collect, (block->depbin ? block->depbin->db_collect : NULL));
	}

	if (mask & BLOCK_MENU) {

		HB_DUMP_BINARY(	"BLOCKA_MENU_ITEMS",
						(block->depbin ? block->depbin->db_menu : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_MENU, env_info,
			errors, &block->menu, (block->depbin ? block->depbin->db_menu : NULL));
	}

	if (mask & BLOCK_EDIT_DISP) {

		HB_DUMP_BINARY(	"BLOCKA_EDIT_DISPLAY_ITEMS",
						(block->depbin ? block->depbin->db_edit_disp : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_EDIT_DISP, env_info,
			errors, &block->edit_disp, (block->depbin ? block->depbin->db_edit_disp : NULL));
	}

	if (mask & BLOCK_METHOD) {

		HB_DUMP_BINARY(	"BLOCKA_METHOD_ITEMS",
						(block->depbin ? block->depbin->db_method : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_METHOD, env_info,
			errors, &block->method, (block->depbin ? block->depbin->db_method : NULL));
	}

	if (mask & BLOCK_UNIT) {

		HB_DUMP_BINARY(	"BLOCKA_UNIT_RELATION_ITEMS",
						(block->depbin ? block->depbin->db_unit : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_UNIT, env_info,
			errors, &block->unit, (block->depbin ? block->depbin->db_unit : NULL));
	}

	if (mask & BLOCK_REFRESH) {

		HB_DUMP_BINARY(	"BLOCKA_REFRESH_RELATION_ITEMS",
						(block->depbin ? block->depbin->db_refresh : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_REFRESH, env_info,
			errors, &block->refresh, (block->depbin ? block->depbin->db_refresh : NULL));
	}

	if (mask & BLOCK_WAO) {

		HB_DUMP_BINARY(	"BLOCKA_WAO_RELATION_ITEMS",
						(block->depbin ? block->depbin->db_wao : NULL));

		rc = eval_attr_reflist(block_masks, (unsigned long) BLOCK_WAO, env_info,
			errors, &block->wao, (block->depbin ? block->depbin->db_wao : NULL));
	}

	if( mask & BLOCK_DEBUG )
	{
		// In order to get the Block name for the device we have to Hard Code it.
		// There is only one standard block for every device so we already know the name.
		// There is no debug information for this item type.
		block->symbol_name.str = L"_MNG_HART_BLOCK";
		block_masks->attr_avail |= (unsigned long) BLOCK_DEBUG;
	}

#ifdef DEBUG
	dds_item_flat_check((void *) block, BLOCK_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_response_code
 *	ShortDesc: evaluate the FLAT_RESPONSE_CODE
 *
 *	Description:
 *		eval_item_response_code will load the FLAT_RESPONSE_CODE structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_response_code() call
 *
 *	Outputs:
 *		response_code:	the loaded FLAT_RESPONSE_CODE structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, resp_code->id,			\
								"Response_Codes", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_response_code(
FLAT_RESP_CODE *resp_code,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_RESPONSE_CODE);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) resp_code, RESP_CODES_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */


	if ((resp_code->depbin == NULL) && (resp_code->masks.bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & RESP_CODE_MEMBER) {

		HB_DUMP_BINARY(	"RESP_CODE_MEMBER",
						(resp_code->depbin ? resp_code->depbin->db_member : NULL));

		rc = item_mask_man((unsigned long) RESP_CODE_MEMBER,
			&resp_code->masks,
			eval_attr_resp_codes,
			(resp_code->depbin ? resp_code->depbin->db_member : NULL),
			&resp_code->member,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) RESP_CODE_MEMBER);
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) resp_code, RESP_CODES_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}


/*********************************************************************
 *
 *	Name: eval_item_domain
 *	ShortDesc: evaluate the FLAT_DOMAIN
 *
 *	Description:
 *		eval_item_domain will load the FLAT_DOMAIN structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_domain() call
 *
 *	Outputs:
 *		domain:			the loaded FLAT_DOMAIN structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, domain->id,			\
								"Domain", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_domain(
FLAT_DOMAIN    *domain,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *dom_masks;	/* temp ptr to the masks of a domain */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_DOMAIN);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) domain, DOMAIN_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	dom_masks = &domain->masks;


	if ((domain->depbin == NULL) && (dom_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & DOMAIN_HANDLING) {

		HB_DUMP_BINARY(	"DOMAIN_HANDLING",
						(domain->depbin ? domain->depbin->db_handling : NULL));

		rc = item_mask_man((unsigned long) DOMAIN_HANDLING,
			dom_masks,
			eval_attr_bitstring,
			(domain->depbin ? domain->depbin->db_handling : NULL),
			&domain->handling,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				domain->handling = READ_HANDLING | WRITE_HANDLING;
				dom_masks->attr_avail |= (unsigned long) DOMAIN_HANDLING;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) DOMAIN_HANDLING);
			}
		}
	}


	if (mask & DOMAIN_RESP_CODES) {

		HB_DUMP_BINARY(	"DOMAIN_RESP_CODES",
						(domain->depbin ? domain->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) DOMAIN_RESP_CODES,
			dom_masks,
			eval_attr_ref,
			(domain->depbin ? domain->depbin->db_resp_codes : NULL),
			&domain->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				domain->resp_codes = 0;
				dom_masks->attr_avail |= (unsigned long) DOMAIN_RESP_CODES;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) DOMAIN_RESP_CODES);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) domain, DOMAIN_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}

#ifdef HART
/*********************************************************************
 *
 *	Name: eval_item_command
 *	ShortDesc: evaluate the FLAT_COMMAND
 *
 *	Description:
 *		eval_item_command will load the FLAT_COMMAND structure with the
 *		evaluated binary based on what bits are set in the mask.
 *
 *	Inputs:
 *		mask:			bit mask specifying attributes to load
 *		env_info:		environment information
 *		errors:			the RETURN_LIST for this eval_item_command() call
 *
 *	Outputs:
 *		command:		the loaded FLAT_COMMAND structure
 *		errors:			the appended RETURN_LIST if there was an error
 *
 *	Returns:
 *		DDI_INVALID_FLAT_MASKS, DDL_NULL_POINTER, DDL_SUCCESS
 *		return values from item_mask_man()
 *
 *	Author: Chris Gustafson
 *
 **********************************************************************/
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, command->id,		\
								"Command", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif


int
eval_item_command(
FLAT_COMMAND   *command,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors)
{

	int             rc;	/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *com_masks;	/* temp ptr to the masks of a command */

#ifdef ISPTEST
	TEST_FAIL(I_EVAL_COMMAND);
#endif

#ifdef DEBUG
	dds_item_flat_check((void *) command, COMMAND_ITYPE);
#endif

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	com_masks = &command->masks;


	if ((command->depbin == NULL) && (com_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & COMMAND_NUMBER) {

		HB_DUMP_BINARY(	"COMMAND_NUMBER",
						(command->depbin ? command->depbin->db_number : NULL));

		rc = item_mask_man((unsigned long) COMMAND_NUMBER,
			com_masks,
			eval_attr_ulong,
			(command->depbin ? command->depbin->db_number : NULL),
			&command->number,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) COMMAND_NUMBER);
		}
	}


	if (mask & COMMAND_OPER) {

		HB_DUMP_BINARY(	"COMMAND_OPER",
						(command->depbin ? command->depbin->db_oper : NULL));

		rc = item_mask_man((unsigned long) COMMAND_OPER,
			com_masks,
			eval_attr_ulong,
			(command->depbin ? command->depbin->db_oper : NULL),
			&command->oper,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) COMMAND_OPER);
		}
	}

	if (mask & COMMAND_TRANS) {

		HB_DUMP_BINARY(	"COMMAND_TRANS",
						(command->depbin ? command->depbin->db_trans : NULL));

		rc = item_mask_man((unsigned long) COMMAND_TRANS,
			com_masks,
			eval_attr_transaction_list,
			(command->depbin ? command->depbin->db_trans : NULL),
			&command->trans,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) COMMAND_TRANS);
		}
	}

	if (mask & COMMAND_RESP_CODES) {

		HB_DUMP_BINARY(	"COMMAND_RESP_CODES",
						(command->depbin ? command->depbin->db_resp_codes : NULL));

		rc = item_mask_man((unsigned long) COMMAND_RESP_CODES,
			com_masks,
			eval_attr_resp_codes,
			(command->depbin ? command->depbin->db_resp_codes : NULL),
			&command->resp_codes,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) COMMAND_RESP_CODES);
		}
	}

	if(mask & COMMAND_DEBUG)
	{
		HB_DUMP_BINARY( "COMMAND_DEBUG", (command->depbin ? command->depbin->db_debug_info : NULL ));
		
		//Find symbol name for item
		//
		rc = item_mask_man((unsigned long) COMMAND_DEBUG, 
			com_masks, 
			eval_attr_debug_info,
			(command->depbin ? command->depbin->db_debug_info : NULL ),
			&command->symbol_name,
			env_info,
			&var_needed);
		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				command->symbol_name.str = L"TODO_Find_NaMe";
				com_masks->attr_avail |= (unsigned long) COMMAND_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) COMMAND_DEBUG);
			}
		}
	}

#ifdef DEBUG
	dds_item_flat_check((void *) command, COMMAND_ITYPE);
#endif

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;
	}
	else {

		return DDL_SUCCESS;
	}
}

#endif	// HART

//////////////////////////////////////////////////////////////////////////
//	Axis
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, axis->id,			\
								"Axis", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_axis(FLAT_AXIS *axis, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{

	int             rc;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *axis_masks;	/* temp ptr to the masks of a axis */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_AXIS);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	axis_masks = &axis->masks;

	if ((axis->depbin == NULL) && (axis_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & AXIS_HELP) {

		HB_DUMP_BINARY(	"AXIS_HELP",
						(axis->depbin ? axis->depbin->db_help : NULL));

		eval_attr_string((unsigned long) AXIS_HELP, axis_masks,
            eval_string,
			(axis->depbin ? axis->depbin->db_help : NULL),
			&axis->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & AXIS_LABEL) {

		HB_DUMP_BINARY(	"AXIS_LABEL",
						(axis->depbin ? axis->depbin->db_label : NULL));

		eval_attr_string((unsigned long) AXIS_LABEL, axis_masks,
			 eval_string,
			(axis->depbin ? axis->depbin->db_label : NULL),
			&axis->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & AXIS_MINVAL) {

		HB_DUMP_BINARY(	"AXIS_MIN_VALUE",
						(axis->depbin ? axis->depbin->db_min_value : NULL));

		// If a min value variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (axis->depbin && axis->depbin->db_min_value)
		{
			rc = get_simple_var_ref(env_info, axis->depbin->db_min_value, &axis->min_value_ref);

			if(rc == DDL_SUCCESS)
			{
				axis->masks.attr_avail |= AXIS_MIN_VAL_REF;
				axis->masks.bin_exists |= AXIS_MIN_VAL_REF;
			}
		} 

		rc = item_mask_man((unsigned long) AXIS_MINVAL, axis_masks,
			eval_attr_expr,
			(axis->depbin ? axis->depbin->db_min_value : NULL),
			(void *) &axis->min_value, env_info, &var_needed);

        if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
		{
			load_errors(errors, rc, &var_needed, (unsigned long) AXIS_MINVAL);
		}
	}

	if (mask & AXIS_MAXVAL) {
 
		HB_DUMP_BINARY(	"AXIS_MAX_VALUE",
						(axis->depbin ? axis->depbin->db_max_value : NULL));

		// If a max value variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (axis->depbin && axis->depbin->db_max_value)
		{
			rc = get_simple_var_ref(env_info, axis->depbin->db_max_value, &axis->max_value_ref);

			if(rc == DDL_SUCCESS)
			{
				axis->masks.attr_avail |= AXIS_MAX_VAL_REF;
				axis->masks.bin_exists |= AXIS_MAX_VAL_REF;
			}
		}

 		rc = item_mask_man((unsigned long) AXIS_MAXVAL, axis_masks,
			eval_attr_expr,
			(axis->depbin ? axis->depbin->db_max_value : NULL),
			(void *) &axis->max_value, env_info, &var_needed);

        if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
		{
			load_errors(errors, rc, &var_needed, (unsigned long) AXIS_MAXVAL);
		}
	}

	if (mask & AXIS_MAX_VAL_REF) {
 
		HB_DUMP_BINARY(	"AXIS_MAX_VALUE_REF",
						(axis->depbin ? axis->depbin->db_max_value : NULL));

		// If a max value variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (axis->depbin && axis->depbin->db_max_value)
		{
			rc = get_simple_var_ref(env_info, axis->depbin->db_max_value, &axis->max_value_ref);

			if(rc == DDL_SUCCESS)
			{
				axis->masks.attr_avail |= AXIS_MAX_VAL_REF;
				axis->masks.bin_exists |= AXIS_MAX_VAL_REF;
			}
		}
	}

	if (mask & AXIS_MIN_VAL_REF) {

		HB_DUMP_BINARY(	"AXIS_MIN_VALUE_REF",
						(axis->depbin ? axis->depbin->db_min_value : NULL));

		// If a min value variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (axis->depbin && axis->depbin->db_min_value)
		{
			rc = get_simple_var_ref(env_info, axis->depbin->db_min_value, &axis->min_value_ref);

			if(rc == DDL_SUCCESS)
			{
				axis->masks.attr_avail |= AXIS_MIN_VAL_REF;
				axis->masks.bin_exists |= AXIS_MIN_VAL_REF;
			}
		} 
	}

	if (mask & AXIS_SCALING) {

		HB_DUMP_BINARY(	"AXIS_SCALING",
						(axis->depbin ? axis->depbin->db_scaling : NULL));

		rc = item_mask_man((unsigned long) AXIS_SCALING, axis_masks,
			 eval_attr_ulong,
			(axis->depbin ? axis->depbin->db_scaling : NULL),
			(void *) &axis->scaling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                axis->scaling = LINEAR_SCALE;
                rc = DDL_SUCCESS;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) AXIS_SCALING);
        }
	}

	if (mask & AXIS_CONSTUNIT) {

		HB_DUMP_BINARY(	"AXIS_CONSTANT_UNIT",
						(axis->depbin ? axis->depbin->db_constant_unit : NULL));

		eval_attr_string((unsigned long) AXIS_CONSTUNIT, axis_masks,
             eval_string,
			(axis->depbin ? axis->depbin->db_constant_unit : NULL),
			&axis->constant_unit, env_info, errors,	DEFAULT_STD_DICT_STRING);
	}
	if (mask & AXIS_VALID) {

		HB_DUMP_BINARY(	"AXIS_VALID",
						(axis->depbin ? axis->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) AXIS_VALID,
			axis_masks,
			eval_attr_ulong,
			(axis->depbin ? axis->depbin->db_valid : NULL),
			&axis->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				axis->valid = TRUE;
				axis_masks->attr_avail |= (unsigned long) AXIS_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) AXIS_VALID);
			}
		}
	}

	if( mask & AXIS_DEBUG )
	{
		HB_DUMP_BINARY( "AXIS_DEBUG", ( axis->depbin ? axis->depbin->db_debug_info : NULL) ); 

		// Find symbol name for item parameter
		rc = item_mask_man((unsigned long) AXIS_DEBUG,
			axis_masks,
			eval_attr_debug_info,
			(axis->depbin ? axis->depbin->db_debug_info : NULL),
			&axis->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				axis->symbol_name.str = L"TODO_Find_NaMe";
				axis_masks->attr_avail |= (unsigned long) AXIS_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) AXIS_DEBUG);
			}
		}
	}

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Chart
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, chart->id,			\
								"Chart", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_chart(FLAT_CHART *chart, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *chart_masks;	/* temp ptr to the masks of a chart */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_CHART);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	chart_masks = &chart->masks;

	if ((chart->depbin == NULL) && (chart_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & CHART_HELP) {

		HB_DUMP_BINARY(	"CHART_HELP",
						(chart->depbin ? chart->depbin->db_help : NULL));

		eval_attr_string((unsigned long) CHART_HELP, chart_masks,
             eval_string,
			(chart->depbin ? chart->depbin->db_help : NULL),
			&chart->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & CHART_LABEL) {

		HB_DUMP_BINARY(	"CHART_LABEL",
						(chart->depbin ? chart->depbin->db_label : NULL));

		eval_attr_string((unsigned long) CHART_LABEL, chart_masks,
			 eval_string,
			(chart->depbin ? chart->depbin->db_label : NULL),
			&chart->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & CHART_CYCLETIME) {

		HB_DUMP_BINARY(	"CHART_CYCLE_TIME",
						(chart->depbin ? chart->depbin->db_cycle_time : NULL));

		rc = item_mask_man((unsigned long) CHART_CYCLETIME, chart_masks,
			 eval_attr_int_expr,
			(chart->depbin ? chart->depbin->db_cycle_time : NULL),
			(void *) &chart->cycle_time, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->cycle_time = 1000;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) CHART_CYCLETIME);
        }
	}

	if (mask & CHART_HEIGHT) {

		HB_DUMP_BINARY(	"CHART_HEIGHT",
						(chart->depbin ? chart->depbin->db_height : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (chart->depbin)
		{
			if (chart->depbin->db_height)
			{
				tmp_bin_chunk = chart->depbin->db_height->bin_chunk;
				tmp_bin_size = chart->depbin->db_height->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &chart->height);
			}
			else
			{
				chart->height = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			chart->height = MEDIUM_DISPSIZE;
		}

		chart_masks->attr_avail |= CHART_HEIGHT;
	}

	if (mask & CHART_WIDTH) {

		HB_DUMP_BINARY(	"CHART_WIDTH",
						(chart->depbin ? chart->depbin->db_width : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (chart->depbin)
		{
			if (chart->depbin->db_width)
			{
				tmp_bin_chunk = chart->depbin->db_width->bin_chunk;
				tmp_bin_size = chart->depbin->db_width->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &chart->width);
			}
			else
			{
				chart->width = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			chart->width = MEDIUM_DISPSIZE;
		}

		chart_masks->attr_avail |= CHART_WIDTH;
	}

	if (mask & CHART_LENGTH) {

		HB_DUMP_BINARY(	"CHART_LENGTH",
						(chart->depbin ? chart->depbin->db_length : NULL));

        rc = item_mask_man((unsigned long) CHART_LENGTH, chart_masks,
			 eval_attr_ulonglong_expr,
			(chart->depbin ? chart->depbin->db_length : NULL),
			(void *) &chart->length, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                chart->length = 600000;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) CHART_LENGTH);
        }
	}

	if (mask & CHART_TYPE) {

		HB_DUMP_BINARY(	"CHART_TYPE",
						(chart->depbin ? chart->depbin->db_type : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (chart->depbin)
		{
			if (chart->depbin->db_type)
			{
				tmp_bin_chunk = chart->depbin->db_type->bin_chunk;
				tmp_bin_size = chart->depbin->db_type->bin_size;

				/*
				* Strip off the CHART_TYPE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != CHART_TYPE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &chart->type);
			}
			else
			{
				chart->type = STRIP_CTYPE;
			}
		}
		else
		{
			chart->type = STRIP_CTYPE;
		}
		chart_masks->attr_avail |= CHART_TYPE;
	}

	if (mask & CHART_MEMBERS) {

		HB_DUMP_BINARY(	"CHART_MEMBERS",
						(chart->depbin ? chart->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) CHART_MEMBERS, chart_masks,
			 eval_attr_members,
			(chart->depbin ? chart->depbin->db_members : NULL),
			(void *) &chart->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {
			load_errors(errors, rc, &var_needed, (unsigned long) CHART_MEMBERS);
		}
	}

	if (mask & CHART_VALID) {

		HB_DUMP_BINARY(	"CHART_VALIDITY",
						(chart->depbin ? chart->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) CHART_VALID,
			chart_masks,
			eval_attr_ulong,
			(chart->depbin ? chart->depbin->db_valid : NULL),
			&chart->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				chart->valid = TRUE;
				chart_masks->attr_avail |= (unsigned long) CHART_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) CHART_VALID);
			}
		}
	}

	if( mask & CHART_DEBUG )
	{
		HB_DUMP_BINARY( "CHART_DEBUG", ( chart->depbin ? chart->depbin->db_debug_info : NULL) ); 
	
		//Find symbol name for the item 
		rc = item_mask_man((unsigned long) CHART_DEBUG,
			chart_masks,
			eval_attr_debug_info,
			(chart->depbin ? chart->depbin->db_debug_info : NULL),
			&chart->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				chart->symbol_name.str = L"TODO_Find_NaMe";
				chart_masks->attr_avail |= (unsigned long) CHART_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) CHART_DEBUG);
			}
		}
	}


	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	File
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, file->id,			\
								"File", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_file(FLAT_FILE *file, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *file_masks;	/* temp ptr to the masks of a file */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_FILE);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	file_masks = &file->masks;

	if ((file->depbin == NULL) && (file_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & FILE_HELP) {

		HB_DUMP_BINARY(	"FILE_HELP",
						(file->depbin ? file->depbin->db_help : NULL));

		eval_attr_string((unsigned long) FILE_HELP, file_masks,
             eval_string,
			(file->depbin ? file->depbin->db_help : NULL),
			&file->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & FILE_LABEL) {

		HB_DUMP_BINARY(	"FILE_LABEL",
						(file->depbin ? file->depbin->db_label : NULL));

		eval_attr_string((unsigned long) FILE_LABEL, file_masks,
			 eval_string,
			(file->depbin ? file->depbin->db_label : NULL),
			&file->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & FILE_MEMBERS) {

		HB_DUMP_BINARY(	"FILE_MEMBERS",
						(file->depbin ? file->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) FILE_MEMBERS, file_masks,
			 eval_attr_members,
			(file->depbin ? file->depbin->db_members : NULL),
			(void *) &file->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) FILE_MEMBERS);
		}
	}

	if( mask & FILE_DEBUG )
	{
		HB_DUMP_BINARY( "FILE_DEBUG", ( file->depbin ? file->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) FILE_DEBUG,
			file_masks,
			eval_attr_debug_info,
			(file->depbin ? file->depbin->db_debug_info : NULL),
			&file->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				file->symbol_name.str = L"TODO_Find_NaMe";
				file_masks->attr_avail |= (unsigned long) FILE_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) FILE_DEBUG);
			}
		}
	}

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Graph
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, graph->id,			\
								"Graph", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_graph(FLAT_GRAPH *graph, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *graph_masks;	/* temp ptr to the masks of a graph */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_GRAPH);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	graph_masks = &graph->masks;

	if ((graph->depbin == NULL) && (graph_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & GRAPH_HELP) {

		HB_DUMP_BINARY(	"GRAPH_HELP",
						(graph->depbin ? graph->depbin->db_help : NULL));

		eval_attr_string((unsigned long) GRAPH_HELP, graph_masks,
             eval_string,
			(graph->depbin ? graph->depbin->db_help : NULL),
			&graph->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}

	if (mask & GRAPH_LABEL) {

		HB_DUMP_BINARY(	"GRAPH_LABEL",
						(graph->depbin ? graph->depbin->db_label : NULL));

		eval_attr_string((unsigned long) GRAPH_LABEL, graph_masks,
			 eval_string,
			(graph->depbin ? graph->depbin->db_label : NULL),
			&graph->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & GRAPH_HEIGHT) {

		HB_DUMP_BINARY(	"GRAPH_HEIGHT",
						(graph->depbin ? graph->depbin->db_height : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (graph->depbin)
		{
			if (graph->depbin->db_height)
			{
				tmp_bin_chunk = graph->depbin->db_height->bin_chunk;
				tmp_bin_size = graph->depbin->db_height->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &graph->height);
			}
			else
			{
				graph->height = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			graph->height = MEDIUM_DISPSIZE;
		}

		graph_masks->attr_avail |= GRAPH_HEIGHT;
	}

	if (mask & GRAPH_WIDTH) {

		HB_DUMP_BINARY(	"GRAPH_WIDTH",
						(graph->depbin ? graph->depbin->db_width : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (graph->depbin)
		{
			if (graph->depbin->db_width)
			{
				tmp_bin_chunk = graph->depbin->db_width->bin_chunk;
				tmp_bin_size = graph->depbin->db_width->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &graph->width);
			}
			else
			{
				graph->width = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			graph->width = MEDIUM_DISPSIZE;
		}

		graph_masks->attr_avail |= GRAPH_WIDTH;
	}

	if (mask & GRAPH_MEMBERS) {

		HB_DUMP_BINARY(	"GRAPH_MEMBERS",
						(graph->depbin ? graph->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) GRAPH_MEMBERS, graph_masks,
			 eval_attr_members,
			(graph->depbin ? graph->depbin->db_members : NULL),
			(void *) &graph->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_MEMBERS);
		}
	}
    
	if (mask & GRAPH_XAXIS) {

		HB_DUMP_BINARY(	"GRAPH_X_AXIS",
						(graph->depbin ? graph->depbin->db_x_axis : NULL));

		rc = item_mask_man((unsigned long) GRAPH_XAXIS, graph_masks,
			 eval_attr_ref,
			(graph->depbin ? graph->depbin->db_x_axis : NULL),
			(void *) &graph->x_axis, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
		{
			load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_XAXIS);
		}
	}

	if (mask & GRAPH_VALID) {

		HB_DUMP_BINARY(	"GRAPH_VALIDITY",
						(graph->depbin ? graph->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) GRAPH_VALID,
			graph_masks,
			eval_attr_ulong,
			(graph->depbin ? graph->depbin->db_valid : NULL),
			&graph->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				graph->valid = TRUE;
				graph_masks->attr_avail |= (unsigned long) GRAPH_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_VALID);
			}
		}
	}

	if (mask & GRAPH_CYCLETIME) {

		HB_DUMP_BINARY(	"GRAPH_CYCLE_TIME",
						(graph->depbin ? graph->depbin->db_cycle_time : NULL));

		rc = item_mask_man((unsigned long) GRAPH_CYCLETIME,
			 graph_masks,
			 eval_attr_int_expr,
			(graph->depbin ? graph->depbin->db_cycle_time : NULL),
			(void *) &graph->cycle_time, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                graph->cycle_time = 0;	// Default HART Refresh Action cycle time
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_CYCLETIME);
        }
	}

	if( mask & GRAPH_DEBUG )
	{
		HB_DUMP_BINARY( "GRAPH_DEBUG", ( graph->depbin ? graph->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) GRAPH_DEBUG,
			graph_masks,
			eval_attr_debug_info,
			(graph->depbin ? graph->depbin->db_debug_info : NULL),
			&graph->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				graph->symbol_name.str = L"TODO_Find_NaMe";
				graph_masks->attr_avail |= (unsigned long) GRAPH_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) GRAPH_DEBUG);
			}
		}
	}

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Grid
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, grid->id,			\
								"Grid", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_grid(FLAT_GRID *grid, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *grid_masks;	/* temp ptr to the masks of a grid */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_GRID);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	grid_masks = &grid->masks;

	if ((grid->depbin == NULL) && (grid_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & GRID_HELP) {

		HB_DUMP_BINARY(	"GRID_HELP",
						(grid->depbin ? grid->depbin->db_help : NULL));

		eval_attr_string((unsigned long) GRID_HELP, grid_masks,
             eval_string,
			(grid->depbin ? grid->depbin->db_help : NULL),
			&grid->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}

	if (mask & GRID_LABEL) {

		HB_DUMP_BINARY(	"GRID_LABEL",
						(grid->depbin ? grid->depbin->db_label : NULL));

		eval_attr_string((unsigned long) GRID_LABEL, grid_masks,
			 eval_string,
			(grid->depbin ? grid->depbin->db_label : NULL),
			&grid->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & GRID_HEIGHT) {

		HB_DUMP_BINARY(	"GRID_HEIGHT",
						(grid->depbin ? grid->depbin->db_height : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (grid->depbin)
		{
			if (grid->depbin->db_height)
			{
				tmp_bin_chunk = grid->depbin->db_height->bin_chunk;
				tmp_bin_size = grid->depbin->db_height->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &grid->height);
			}
			else
			{
				grid->height = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			grid->height = MEDIUM_DISPSIZE;
		}

		grid_masks->attr_avail |= GRID_HEIGHT;
	}

	if (mask & GRID_WIDTH) {

		HB_DUMP_BINARY(	"GRID_WIDTH",
						(grid->depbin ? grid->depbin->db_width : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (grid->depbin)
		{
			if (grid->depbin->db_width)
			{
				tmp_bin_chunk = grid->depbin->db_width->bin_chunk;
				tmp_bin_size = grid->depbin->db_width->bin_size;

				/*
				* Strip off the SCOPE_SIZE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != SCOPE_SIZE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &grid->width);
			}
			else
			{
				grid->width = MEDIUM_DISPSIZE;
			}
		}
		else
		{
			grid->width = MEDIUM_DISPSIZE;
		}

		grid_masks->attr_avail |= GRID_WIDTH;
	}

	if (mask & GRID_ORIENT) {

		HB_DUMP_BINARY(	"GRID_ORIENTATION",
						(grid->depbin ? grid->depbin->db_orientation : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (grid->depbin)
		{
			if (grid->depbin->db_orientation)
			{
				tmp_bin_chunk = grid->depbin->db_orientation->bin_chunk;
				tmp_bin_size = grid->depbin->db_orientation->bin_size;

				/*
				* Strip off the GRID_ORIENT_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != GRID_ORIENT_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &grid->orientation);
			}
			else
			{
				grid->orientation = ORIENT_VERT;
			}
		}
		else
		{
			grid->orientation = ORIENT_VERT;
		}

		grid_masks->attr_avail |= GRID_ORIENT;
	}

	if (mask & GRID_MEMBERS) {

		HB_DUMP_BINARY(	"GRID_VECTORS",
						(grid->depbin ? grid->depbin->db_vectors : NULL));

		rc = item_mask_man((unsigned long) GRID_MEMBERS, grid_masks,
			 eval_attr_vectors,
			(grid->depbin ? grid->depbin->db_vectors : NULL),
			(void *) &grid->vectors, env_info, &var_needed);

		if (rc != DDL_SUCCESS) {

			load_errors(errors, rc, &var_needed, (unsigned long) GRID_MEMBERS);
		}
	}
    

	if (mask & GRID_HANDLING) {

		HB_DUMP_BINARY(	"GRID_HANDLING",
						(grid->depbin ? grid->depbin->db_handling : NULL));

		rc = item_mask_man((unsigned long) GRID_HANDLING,
			grid_masks,
			eval_attr_bitstring,
			(grid->depbin ? grid->depbin->db_handling : NULL),
			&grid->handling,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				grid->handling = READ_HANDLING | WRITE_HANDLING;
				grid_masks->attr_avail |= (unsigned long) GRID_HANDLING;
			}
			else {

				load_errors(errors, rc, &var_needed, (unsigned long) GRID_HANDLING);
			}
		}
	}

	if (mask & GRID_VALID) {

		HB_DUMP_BINARY(	"GRID_VALIDITY",
						(grid->depbin ? grid->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) GRID_VALID,
			grid_masks,
			eval_attr_ulong,
			(grid->depbin ? grid->depbin->db_valid : NULL),
			&grid->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				grid->valid = TRUE;
				grid_masks->attr_avail |= (unsigned long) GRID_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) GRID_VALID);
			}
		}
	}

	if( mask & GRID_DEBUG )
	{
		HB_DUMP_BINARY( "GRID_DEBUG", ( grid->depbin ? grid->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) GRID_DEBUG,
			grid_masks,
			eval_attr_debug_info,
			(grid->depbin ? grid->depbin->db_debug_info : NULL),
			&grid->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				grid->symbol_name.str = L"TODO_Find_NaMe";
				grid_masks->attr_avail |= (unsigned long) GRID_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) GRID_DEBUG);
			}
		}
	}

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Image
//////////////////////////////////////////////////////////////////////////

static int get_image(ENV_INFO *env_info, int image_table_index, BINARY_IMAGE *bin_image)
{
	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr *pConnectionMgr = pDDSSupport->GetConnectionManager();

	FLAT_DEVICE_DIR *flat_device_dir = NULL;
	int rc = pConnectionMgr->get_adt_dd_dev_tbls(env_info->block_handle,(void **) &flat_device_dir);

	if(rc)
	{
		return rc;
	}

	if(0 > image_table_index || image_table_index >= flat_device_dir->image_tbl.count)
	{
		return DDL_ENCODING_ERROR;
	}


	bool bImageFound = false;
	CStdString sCountryCode(env_info->lang_code);
	sCountryCode = sCountryCode.Trim(_T('|'));
	// search for image using current language ocde
	for(int language_index = 0; language_index < flat_device_dir->image_tbl.list[image_table_index].count; language_index++)
	{
		if(sCountryCode.Compare(flat_device_dir->image_tbl.list[image_table_index].list[language_index].language_code.str) == 0)
		{
			bin_image->length = flat_device_dir->image_tbl.list[image_table_index].list[language_index].length;
			bin_image->data = flat_device_dir->image_tbl.list[image_table_index].list[language_index].data;

			bImageFound = true;

			break;
		}
	}
	//This is workaround for HART tok issue. HART tok inserts lang code "enzz" for images when "en zz" langauge code is selcted in Trex.
	// This is fix for CCS Incident #416892. This fix will work for any langauge code e.g "xx zz". where "xx" is lang code.
	if(!bImageFound)
	{  
		if(sCountryCode.Find(_T(' ')) != -1)
		{
			sCountryCode.Remove(_T(' '));

			for(int language_index = 0; language_index < flat_device_dir->image_tbl.list[image_table_index].count; language_index++)
			{
				if(sCountryCode.Compare(flat_device_dir->image_tbl.list[image_table_index].list[language_index].language_code.str) == 0)
				{
					bin_image->length = flat_device_dir->image_tbl.list[image_table_index].list[language_index].length;
					bin_image->data = flat_device_dir->image_tbl.list[image_table_index].list[language_index].data;

					bImageFound = true;

					break;
				}
			}
		}
	}

	if(!bImageFound)
	{
		for(int language_index = 0; language_index < flat_device_dir->image_tbl.list[image_table_index].count; language_index++)
		{
			sCountryCode = _T("en");
			if(sCountryCode.Compare(flat_device_dir->image_tbl.list[image_table_index].list[language_index].language_code.str) == 0)
			{
				bin_image->length = flat_device_dir->image_tbl.list[image_table_index].list[language_index].length;
				bin_image->data	  = flat_device_dir->image_tbl.list[image_table_index].list[language_index].data; 	
				
				bImageFound = true;

				break;
			}
		}
	}

	return DDL_SUCCESS;
}

#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, image->id,			\
								"Image", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_image(FLAT_IMAGE *image, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *image_masks;	/* temp ptr to the masks of a image */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_IMAGE);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	image_masks = &image->masks;

	if ((image->depbin == NULL) && (image_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & IMAGE_HELP) {

		HB_DUMP_BINARY(	"IMAGE_HELP",
						(image->depbin ? image->depbin->db_help : NULL));

		eval_attr_string((unsigned long) IMAGE_HELP, image_masks,
             eval_string,
			(image->depbin ? image->depbin->db_help : NULL),
			&image->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}

	if ((mask & IMAGE_PATH) || (mask & IMAGE_BINARY)) {

		HB_DUMP_BINARY(	"IMAGE_PATH",
						(image->depbin ? image->depbin->db_path : NULL));

		rc = item_mask_man((unsigned long) IMAGE_PATH, image_masks,
			 eval_attr_ulong,
			(image->depbin ? image->depbin->db_path : NULL),
			(void *) &image->path, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
		    load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_PATH);
        }
	}

	if (mask & IMAGE_BINARY) {

		HB_DUMP_BINARY(	"IMAGE_BINARY",
						(image->depbin ? image->depbin->db_path : NULL));

		 /* The path must be present to find the image */
        if (!(image->masks.attr_avail & IMAGE_PATH))
		{
            image->masks.attr_avail &= ~IMAGE_BINARY;
		}
        else
        {
            if (get_image(env_info,image->path ,&image->entry))
			{
                image->masks.attr_avail &= ~IMAGE_BINARY;
			}
            else
			{
                image->masks.attr_avail |= IMAGE_BINARY;
				image->masks.bin_exists |= IMAGE_BINARY;
			}
        }
	}

	if (mask & IMAGE_LINK) {

		HB_DUMP_BINARY(	"IMAGE_LINK",
						(image->depbin ? image->depbin->db_link : NULL));

 		rc = item_mask_man((unsigned long) IMAGE_LINK, image_masks,
			 eval_attr_ref,
			(image->depbin ? image->depbin->db_link : NULL),
			(void *) &image->link, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
		{
			load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_LINK);
		}
	}

	if (mask & IMAGE_LABEL) {

		HB_DUMP_BINARY(	"IMAGE_LABEL",
						(image->depbin ? image->depbin->db_label : NULL));

		eval_attr_string((unsigned long) IMAGE_LABEL, image_masks,
			 eval_string,
			(image->depbin ? image->depbin->db_label : NULL),
			&image->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}

	if (mask & IMAGE_VALID) {

		HB_DUMP_BINARY(	"IMAGE_VALIDITY",
						(image->depbin ? image->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) IMAGE_VALID,
			image_masks,
			eval_attr_ulong,
			(image->depbin ? image->depbin->db_valid : NULL),
			&image->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				image->valid = TRUE;
				image_masks->attr_avail |= (unsigned long) IMAGE_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_VALID);
			}
		}
	}
	if( mask & IMAGE_DEBUG )
	{
		HB_DUMP_BINARY( "IMAGE_DEBUG", ( image->depbin ? image->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) IMAGE_DEBUG,
			image_masks,
			eval_attr_debug_info,
			(image->depbin ? image->depbin->db_debug_info : NULL),
			&image->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				image->symbol_name.str = L"TODO_Find_NaMe";
				image_masks->attr_avail |= (unsigned long) IMAGE_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) IMAGE_DEBUG);
			}
		}
	}


	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	List
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, list->id,			\
								"List", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_list(FLAT_LIST *list, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *list_masks;	/* temp ptr to the masks of a list */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_LIST);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	list_masks = &list->masks;

	if ((list->depbin == NULL) && (list_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & LIST_HELP) {

		HB_DUMP_BINARY(	"LIST_HELP",
						(list->depbin ? list->depbin->db_help : NULL));

		eval_attr_string((unsigned long) LIST_HELP, list_masks,
             eval_string,
			(list->depbin ? list->depbin->db_help : NULL),
			&list->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & LIST_LABEL) {

		HB_DUMP_BINARY(	"LIST_LABEL",
						(list->depbin ? list->depbin->db_label : NULL));

		eval_attr_string((unsigned long) LIST_LABEL, list_masks,
			 eval_string,
			(list->depbin ? list->depbin->db_label : NULL),
			&list->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & LIST_CAPACITY) {

		HB_DUMP_BINARY(	"LIST_CAPACITY",
						(list->depbin ? list->depbin->db_capacity : NULL));

		rc = item_mask_man((unsigned long) LIST_CAPACITY, list_masks,
			eval_attr_ulong,
			(list->depbin ? list->depbin->db_capacity : NULL),
			(void *) &list->capacity, env_info, &var_needed);
        /*
        rc = eval_attr_int_expr(list->depbin->db_capacity->bin_chunk,
            list->depbin->db_capacity->bin_size,
            &list->capacity, 
            &list->depbin->db_capacity->dep, 
            env_info, &var_needed);
            */

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                list->capacity = 0;
            else
    		    load_errors(errors, rc, &var_needed, (unsigned long) LIST_CAPACITY);
        }
    }
	if (mask & LIST_COUNT) {

		HB_DUMP_BINARY(	"LIST_COUNT",
						(list->depbin ? list->depbin->db_count : NULL));

		// If a count variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (list->depbin && list->depbin->db_count)
		{
			rc = get_simple_var_ref(env_info, list->depbin->db_count, &list->count_ref);

			if(rc == DDL_SUCCESS)
			{
				list->masks.attr_avail |= LIST_COUNT_REF;
				list->masks.bin_exists |= LIST_COUNT_REF;
			}
		} 

		rc = item_mask_man((unsigned long) LIST_COUNT, list_masks,
			 eval_attr_int_expr,
			(list->depbin ? list->depbin->db_count : NULL),
			(void *) &list->count, env_info, &var_needed);
            
        /* rc = eval_attr_int_expr(list->depbin->db_count->bin_chunk,
            list->depbin->db_count->bin_size,
            &list->count, 
            &list->depbin->db_count->dep, 
            env_info, &var_needed);
        */

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                list->count = 0;
            else
    		    load_errors(errors, rc, &var_needed, (unsigned long) LIST_COUNT);
        }
	}

	if (mask & LIST_COUNT_REF) {

		HB_DUMP_BINARY(	"LIST_COUNT_REF",
						(list->depbin ? list->depbin->db_count : NULL));

		// If a count variable reference can be determined simply from the depbin, get it
		// otherwise leave it as zeros
		if (list->depbin && list->depbin->db_count)
		{
			rc = get_simple_var_ref(env_info, list->depbin->db_count, &list->count_ref);

			if(rc == DDL_SUCCESS)
			{
				list->masks.attr_avail |= LIST_COUNT_REF;
				list->masks.bin_exists |= LIST_COUNT_REF;
			}
			 
		} 

	}

	if (mask & LIST_TYPE) {

		HB_DUMP_BINARY(	"LIST_TYPE",
						(list->depbin ? list->depbin->db_type : NULL));

		rc = item_mask_man((unsigned long) LIST_TYPE, list_masks,
			 eval_attr_ref,
			(list->depbin ? list->depbin->db_type : NULL),
			(void *) &list->type, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
			load_errors(errors, rc, &var_needed, (unsigned long) LIST_TYPE);
	}

	if (mask & LIST_VALID) {

		HB_DUMP_BINARY(	"LIST_VALID",
						(list->depbin ? list->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) LIST_VALID,
			list_masks,
			eval_attr_ulong,
			(list->depbin ? list->depbin->db_valid : NULL),
			&list->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				list->valid = TRUE;
				list_masks->attr_avail |= (unsigned long) LIST_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) LIST_VALID);
			}
		}
	}	
	
	if( mask & LIST_DEBUG )
	{
		HB_DUMP_BINARY( "LIST_DEBUG", ( list->depbin ? list->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) LIST_DEBUG,
			list_masks,
			eval_attr_debug_info,
			(list->depbin ? list->depbin->db_debug_info : NULL),
			&list->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				list->symbol_name.str = L"TODO_Find_NaMe";
				list_masks->attr_avail |= (unsigned long) LIST_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) LIST_DEBUG);
			}
		}
	}

	if (errors->count) {

		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Source
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, source->id,			\
								"Source", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_source(FLAT_SOURCE *source, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *source_masks;	/* temp ptr to the masks of a source */

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_SOURCE);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	source_masks = &source->masks;

	if ((source->depbin == NULL) && (source_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & SOURCE_HELP) {

		HB_DUMP_BINARY(	"SOURCE_HELP",
						(source->depbin ? source->depbin->db_help : NULL));

		eval_attr_string((unsigned long) SOURCE_HELP, source_masks,
             eval_string,
			(source->depbin ? source->depbin->db_help : NULL),
			&source->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & SOURCE_LABEL) {

		HB_DUMP_BINARY(	"SOURCE_LABEL",
						(source->depbin ? source->depbin->db_label : NULL));

		eval_attr_string((unsigned long) SOURCE_LABEL, source_masks,
			 eval_string,
			(source->depbin ? source->depbin->db_label : NULL),
			&source->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & SOURCE_MEMBERS) {

		HB_DUMP_BINARY(	"SOURCE_MEMBERS",
						(source->depbin ? source->depbin->db_members : NULL));

		rc = item_mask_man((unsigned long) SOURCE_MEMBERS, source_masks,
			 eval_attr_op_members,
			(source->depbin ? source->depbin->db_members : NULL),
			(void *) &source->members, env_info, &var_needed);

		if (rc != DDL_SUCCESS)
			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_MEMBERS);
	}
	if (mask & SOURCE_EMPHASIS) {

		HB_DUMP_BINARY(	"SOURCE_EMPHASIS",
						(source->depbin ? source->depbin->db_emphasis : NULL));

		rc = item_mask_man((unsigned long) SOURCE_EMPHASIS, source_masks,
			 eval_attr_ulong,
			(source->depbin ? source->depbin->db_emphasis : NULL),
			(void *) &source->emphasis, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->emphasis = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_EMPHASIS);
        }
	}
	if (mask & SOURCE_LINETYPE) {

		HB_DUMP_BINARY(	"SOURCE_LINE_TYPE",
						(source->depbin ? source->depbin->db_line_type : NULL));

		rc = item_mask_man((unsigned long) SOURCE_LINETYPE, source_masks,
			 eval_attr_source_waveform_line_type,
			(source->depbin ? source->depbin->db_line_type : NULL),
			(void *) &source->line_type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->line_type = DDS_LINE_TYPE_DATA_N;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_LINETYPE);
        }
	}
	if (mask & SOURCE_LINECOLOR) {

		HB_DUMP_BINARY(	"SOURCE_LINE_COLOR",
						(source->depbin ? source->depbin->db_line_color : NULL));

        rc = item_mask_man((unsigned long) SOURCE_LINECOLOR, source_masks,
			 eval_attr_int_expr,
			(source->depbin ? source->depbin->db_line_color : NULL),
			(void *) &source->line_color, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->line_color = LINE_COLOR_DEFAULT;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_LINECOLOR);
        }
	}
	if (mask & SOURCE_YAXIS) {

		HB_DUMP_BINARY(	"SOURCE_Y_AXIS",
						(source->depbin ? source->depbin->db_y_axis : NULL));

		rc = item_mask_man((unsigned long) SOURCE_YAXIS, source_masks,
			 eval_attr_ref,
			(source->depbin ? source->depbin->db_y_axis : NULL),
			(void *) &source->y_axis, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
		{ 
			load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_YAXIS);
		}
	}
	if (mask & SOURCE_VALID) {

		HB_DUMP_BINARY(	"SOURCE_VALIDITY",
						(source->depbin ? source->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) SOURCE_VALID,
			source_masks,
			eval_attr_ulong,
			(source->depbin ? source->depbin->db_valid : NULL),
			&source->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				source->valid = TRUE;
				source_masks->attr_avail |= (unsigned long) SOURCE_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_VALID);
			}
		}
	}

	if( mask & SOURCE_DEBUG )
	{
		HB_DUMP_BINARY( "SOURCE_DEBUG", ( source->depbin ? source->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) SOURCE_DEBUG,
			source_masks,
			eval_attr_debug_info,
			(source->depbin ? source->depbin->db_debug_info : NULL),
			&source->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				source->symbol_name.str = L"TODO_Find_NaMe";
				source_masks->attr_avail |= (unsigned long) SOURCE_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_DEBUG);
			}
		}
	}
	if (mask & SOURCE_INIT_ACTIONS) {

		HB_DUMP_BINARY(	"SOURCE_INIT_ACTIONS",
						(source->depbin ? source->depbin->db_init_actions : NULL));

		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_INIT_ACTIONS,
			env_info, errors, &source->init_actions,
			(source->depbin ? source->depbin->db_init_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->init_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_INIT_ACTIONS);
        }
	}
	if (mask & SOURCE_RFRSH_ACTIONS) {

		HB_DUMP_BINARY(	"SOURCE_RFRSH_ACTIONS",
						(source->depbin ? source->depbin->db_refresh_actions : NULL));

		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_RFRSH_ACTIONS,
			env_info, errors, &source->refresh_actions,
			(source->depbin ? source->depbin->db_refresh_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->refresh_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_RFRSH_ACTIONS);
        }
	}

	if (mask & SOURCE_EXIT_ACTIONS) {

		HB_DUMP_BINARY(	"SOURCE_EXIT_ACTIONS",
						(source->depbin ? source->depbin->db_exit_actions : NULL));

		rc = eval_attr_reflist(source_masks, (unsigned long) SOURCE_EXIT_ACTIONS,
			env_info, errors, &source->exit_actions,
			(source->depbin ? source->depbin->db_exit_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                source->exit_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) SOURCE_EXIT_ACTIONS);
        }
	}

	if (errors->count) {
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}


//////////////////////////////////////////////////////////////////////////
//	Waveform
//////////////////////////////////////////////////////////////////////////
#ifdef _HARTBREAKER_
#undef HB_DUMP_BINARY
#define HB_DUMP_BINARY(sAttrName, pDepBin)								\
	CHARTBreaker::DumpBinary(env_info->block_handle, waveform->id,			\
								"Waveform", sAttrName,					\
								(pDepBin ? pDepBin->bin_chunk : NULL),	\
								(pDepBin ? pDepBin->bin_size  : 0))
#endif

int
eval_item_waveform(FLAT_WAVEFORM *waveform, unsigned long mask, ENV_INFO *env_info,
	RETURN_LIST *errors)
{
	int             rc = 0;			/* return code */
	OP_REF          var_needed = {STANDARD_TYPE, 0};	/* temp storage for var_needed */
	FLAT_MASKS     *waveform_masks;	/* temp ptr to the masks of a waveform */
    EXPR tmpExpr;

#ifdef DDSTEST
	TEST_FAIL(I_EVAL_WAVEFORM);
#endif /* DDSTEST */

	var_needed.op_info.id = 0;	/* Zero id says that this is not set */
	waveform_masks = &waveform->masks;

	if ((waveform->depbin == NULL) && (waveform_masks->bin_hooked)) {

		return DDL_NULL_POINTER;
	}

	if (mask & WAVEFORM_HELP) {

		HB_DUMP_BINARY(	"WAVEFORM_HELP",
						(waveform->depbin ? waveform->depbin->db_help : NULL));

		eval_attr_string((unsigned long) WAVEFORM_HELP, waveform_masks,
             eval_string,
			(waveform->depbin ? waveform->depbin->db_help : NULL),
			&waveform->help, env_info, errors,	DEFAULT_STD_DICT_HELP);
	}
	if (mask & WAVEFORM_LABEL) {

		HB_DUMP_BINARY(	"WAVEFORM_LABEL",
						(waveform->depbin ? waveform->depbin->db_label : NULL));

		eval_attr_string((unsigned long) WAVEFORM_LABEL, waveform_masks,
			 eval_string,
			(waveform->depbin ? waveform->depbin->db_label : NULL),
			&waveform->label, env_info, errors,
			DEFAULT_STD_DICT_LABEL);
	}
	if (mask & WAVEFORM_EMPHASIS) {

		HB_DUMP_BINARY(	"WAVEFORM_EMPHASIS",
						(waveform->depbin ? waveform->depbin->db_emphasis : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_EMPHASIS, waveform_masks,
			 eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_emphasis : NULL),
			(void *) &waveform->emphasis, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->emphasis = 0;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_EMPHASIS);
        }
	}
	if (mask & WAVEFORM_LINETYPE) {

		HB_DUMP_BINARY(	"WAVEFORM_LINE_TYPE",
						(waveform->depbin ? waveform->depbin->db_line_type : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_LINETYPE, waveform_masks,
			 eval_attr_source_waveform_line_type,
			(waveform->depbin ? waveform->depbin->db_line_type : NULL),
			(void *) &waveform->line_type, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->line_type = DDS_LINE_TYPE_DATA_N;
            else
	    		load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_LINETYPE);
        }
	}
	if (mask & WAVEFORM_LINECOLOR) {

		HB_DUMP_BINARY(	"WAVEFORM_LINE_COLOR",
						(waveform->depbin ? waveform->depbin->db_line_color : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_LINECOLOR, waveform_masks,
			 eval_attr_int_expr,
			(waveform->depbin ? waveform->depbin->db_line_color : NULL),
			(void *) &waveform->line_color, env_info, &var_needed);

        if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->line_color = LINE_COLOR_DEFAULT;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_LINECOLOR);
        }
	}
	if (mask & WAVEFORM_HANDLING) {

		HB_DUMP_BINARY(	"WAVEFORM_HANDLING",
						(waveform->depbin ? waveform->depbin->db_handling : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_HANDLING, waveform_masks,
			 eval_attr_bitstring,
			(waveform->depbin ? waveform->depbin->db_handling : NULL),
			(void *) &waveform->handling, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->handling = READ_HANDLING | WRITE_HANDLING;
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_HANDLING);
        }
	}
	if (mask & WAVEFORM_INIT_ACTIONS) {

		HB_DUMP_BINARY(	"WAVEFORM_INIT_ACTIONS",
						(waveform->depbin ? waveform->depbin->db_init_actions : NULL));

		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_INIT_ACTIONS,
			env_info, errors, &waveform->init_actions,
			(waveform->depbin ? waveform->depbin->db_init_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->init_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_INIT_ACTIONS);
        }
	}
	if (mask & WAVEFORM_RFRSH_ACTIONS) {

		HB_DUMP_BINARY(	"WAVEFORM_RFRSH_ACTIONS",
						(waveform->depbin ? waveform->depbin->db_refresh_actions : NULL));

		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_RFRSH_ACTIONS,
			env_info, errors, &waveform->refresh_actions,
			(waveform->depbin ? waveform->depbin->db_refresh_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->refresh_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_RFRSH_ACTIONS);
        }
	}

	if (mask & WAVEFORM_EXIT_ACTIONS) {

		HB_DUMP_BINARY(	"WAVEFORM_EXIT_ACTIONS",
						(waveform->depbin ? waveform->depbin->db_exit_actions : NULL));

		rc = eval_attr_reflist(waveform_masks, (unsigned long) WAVEFORM_EXIT_ACTIONS,
			env_info, errors, &waveform->exit_actions,
			(waveform->depbin ? waveform->depbin->db_exit_actions : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->exit_actions.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_EXIT_ACTIONS);
        }
	}

	if (mask & WAVEFORM_TYPE) {

		HB_DUMP_BINARY(	"WAVEFORM_TYPE",
						(waveform->depbin ? waveform->depbin->db_type : NULL));

	    unsigned long   tmp_bin_size;	/* temp storage for DEPBIN chunk size */
	    unsigned char  *tmp_bin_chunk;	/* temp prt to DEPBIN chunk */

        if (waveform->depbin)
		{
			if (waveform->depbin->db_type)
			{
				tmp_bin_chunk = waveform->depbin->db_type->bin_chunk;
				tmp_bin_size = waveform->depbin->db_type->bin_size;

				/*
				* Strip off the CHART_TYPE_TAG.
				*/
				DDL_UINT        temp_tag;
				DDL_PARSE_TAG(&tmp_bin_chunk, &tmp_bin_size, &temp_tag, (DDL_UINT *) NULL_PTR);
				if (temp_tag != WAVE_TYPE_TAG)
				{
					return DDL_ENCODING_ERROR;
				}

				DDL_PARSE_INTEGER(&tmp_bin_chunk, &tmp_bin_size, &waveform->type);
			}
			else
			{
				return DDL_ENCODING_ERROR; //Must have type
			}
		}
		else
		{
			return DDL_ENCODING_ERROR; //Must have type
		}
		waveform_masks->attr_avail |= WAVEFORM_TYPE;
	}

	if (mask & WAVEFORM_X_INITIAL) {

		HB_DUMP_BINARY(	"WAVEFORM_X_INITIAL",
						(waveform->depbin ? waveform->depbin->db_x_initial : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_X_INITIAL, waveform_masks,
			 eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_x_initial : NULL),
			(void *) &waveform->x_initial, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                waveform->x_initial.size = 0;
                waveform->x_initial.type = 0;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_INITIAL);
        }
	}
	if (mask & WAVEFORM_X_INCREMENT) {

		HB_DUMP_BINARY(	"WAVEFORM_X_INCREMENT",
						(waveform->depbin ? waveform->depbin->db_x_increment : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_X_INCREMENT, waveform_masks,
			 eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_x_increment : NULL),
			(void *) &waveform->x_increment, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                waveform->x_increment.size = 0;
                waveform->x_increment.type = 0;
            }
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_INCREMENT);
        }
	}

	if (mask & WAVEFORM_X_VALUES) {

		HB_DUMP_BINARY(	"WAVEFORM_X_VALUES",
						(waveform->depbin ? waveform->depbin->db_x_values : NULL));

		rc = eval_attr_data_reflist(waveform_masks, (unsigned long) WAVEFORM_X_VALUES,
			env_info, errors, &waveform->x_values,
			(waveform->depbin ? waveform->depbin->db_x_values : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->x_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_X_VALUES);
        }
	}

	if (mask & WAVEFORM_Y_VALUES) {

		HB_DUMP_BINARY(	"WAVEFORM_Y_VALUES",
						(waveform->depbin ? waveform->depbin->db_y_values : NULL));

		rc = eval_attr_data_reflist(waveform_masks, (unsigned long) WAVEFORM_Y_VALUES,
			env_info, errors, &waveform->y_values,
			(waveform->depbin ? waveform->depbin->db_y_values : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->y_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_Y_VALUES);
        }
	}

	if (mask & WAVEFORM_POINT_COUNT) {

		HB_DUMP_BINARY(	"WAVEFORM_NUMBER_OF_POINTS",
						(waveform->depbin ? waveform->depbin->db_number_of_points : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_POINT_COUNT, waveform_masks,
			 eval_attr_expr,
			(waveform->depbin ? waveform->depbin->db_number_of_points : NULL),
			(void *) &tmpExpr, env_info, &var_needed);

		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
            {
                if (waveform->type ==  XY_WAVEFORM_TYPE)
                {
                    if (waveform->x_values.count == waveform->y_values.count)
                        waveform->number_of_points = waveform->x_values.count;
                    else
                        waveform->number_of_points = 0;
                }
                else if (waveform->type == YT_WAVEFORM_TYPE)
                {
                    waveform->number_of_points = waveform->y_values.count;
                }
                else
                {
                    waveform->number_of_points = 0;
                }
                waveform_masks->attr_avail |= (unsigned long) WAVEFORM_POINT_COUNT;
            }
            else
    			load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_POINT_COUNT);
        }
        else
            waveform->number_of_points = (ulong)tmpExpr.val.ull;
	}
	if (mask & WAVEFORM_KEYPTS_X) {

		HB_DUMP_BINARY(	"WAVEFORM_KEYPOINTS_XVALUES",
						(waveform->depbin ? waveform->depbin->db_key_x_values : NULL));

		rc = eval_attr_data_reflist(waveform_masks, (unsigned long) WAVEFORM_KEYPTS_X,
			env_info, errors, &waveform->key_x_values,
			(waveform->depbin ? waveform->depbin->db_key_x_values : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->key_x_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_KEYPTS_X);
        }
	}
	if (mask & WAVEFORM_KEYPTS_Y) {

		HB_DUMP_BINARY(	"WAVEFORM_KEYPOINTS_YVALUES",
						(waveform->depbin ? waveform->depbin->db_key_y_values : NULL));

		rc = eval_attr_data_reflist(waveform_masks, (unsigned long) WAVEFORM_KEYPTS_Y,
			env_info, errors, &waveform->key_y_values,
			(waveform->depbin ? waveform->depbin->db_key_y_values : NULL));
		if (rc != DDL_SUCCESS) 
        {
            if (rc == DDL_DEFAULT_ATTR)
                waveform->key_y_values.count = 0;
            else
			    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_KEYPTS_Y);
		}
	}
	if (mask & WAVEFORM_YAXIS) {

		HB_DUMP_BINARY(	"WAVEFORM_Y_AXIS",
						(waveform->depbin ? waveform->depbin->db_y_axis : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_YAXIS, waveform_masks,
			 eval_attr_ref,
			(waveform->depbin ? waveform->depbin->db_y_axis : NULL),
			(void *) &waveform->y_axis, env_info, &var_needed);

		if ((rc != DDL_SUCCESS) && (rc != DDL_DEFAULT_ATTR))
        {
		    load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_YAXIS);
        }
	}

	if( mask & WAVEFORM_DEBUG )
	{
		HB_DUMP_BINARY( "WAVEFORM_DEBUG", ( waveform->depbin ? waveform->depbin->db_debug_info : NULL) ); 

		//Find the symbol name for the item
		rc = item_mask_man((unsigned long) WAVEFORM_DEBUG,
			waveform_masks,
			eval_attr_debug_info,
			(waveform->depbin ? waveform->depbin->db_debug_info : NULL),
			&waveform->symbol_name,
			env_info,
			&var_needed);

		// If symbol name cannot be found in the debug attribute then we default to bogus name. 
		// We do not crash the creating of the .sym file
		if(rc != DDL_SUCCESS)
		{
			if( rc == DDL_DEFAULT_ATTR)
			{
				waveform->symbol_name.str = L"TODO_Find_NaMe";
				waveform_masks->attr_avail |= (unsigned long) WAVEFORM_DEBUG;
			}
			else
			{
				load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_DEBUG);
			}
		}
	}

	if (mask & WAVEFORM_VALID) 
	{
		HB_DUMP_BINARY(	"WAVEFORM_VALID",
						(waveform->depbin ? waveform->depbin->db_valid : NULL));

		rc = item_mask_man((unsigned long) WAVEFORM_VALID,
			waveform_masks,
			eval_attr_ulong,
			(waveform->depbin ? waveform->depbin->db_valid : NULL),
			&waveform->valid,
			env_info,
			&var_needed);

		if (rc != DDL_SUCCESS) {

			if (rc == DDL_DEFAULT_ATTR) {

				waveform->valid = TRUE;
				waveform_masks->attr_avail |= (unsigned long) WAVEFORM_VALID;
			}
			else {
				load_errors(errors, rc, &var_needed, (unsigned long) WAVEFORM_VALID);
			}
		}
	}

	if (errors->count) {
		return DDL_CHECK_RETURN_LIST;

	} else {
		return DDL_SUCCESS;
	}
}

/*********************************************************************
 *
 *	Name: eval_item
 *	ShortDesc: evaluate an entire item
 *
 *	Description:
 *		eval_item() is a distributor function for all "eval_item_*" routines.
 *		eval_item() "swithes" on the "item_type" and calls the corresponding
 *		"eval_item_*" routine.
 *
 *	Inputs:
 *		mask:			bit mask specifying which attributes are desired.
 *		env_info:		environment information
 *		item_type:		type of item to be evaluated (var, menu,....).
 *
 *	Outputs:
 *		item:			FLAT structure containing the requested item info.
 *		errors:			list of errors incurred processing an item.
 *
 *	Returns:
 *		DDL_INVALID_PARAM,
 *		return values from other "eval_item_*" functions
 *
 *	Author: steve beyerl
 *
 **********************************************************************/
int
eval_item(
void           *item,
unsigned long   mask,
ENV_INFO       *env_info,
RETURN_LIST    *errors,
ITEM_TYPE       item_type)
{

	int             rc;

#ifdef ISPTEST
	TEST_FAIL(EVAL_ONE_ITEM);
#endif

	switch (item_type) {
	case VARIABLE_ITYPE:
		rc = eval_item_var((FLAT_VAR *) item, mask, env_info, errors);
		break;
#ifdef HART
	case COMMAND_ITYPE:
		rc = eval_item_command((FLAT_COMMAND *) item, mask, env_info, errors);
		break;
#endif
	case MENU_ITYPE:
		rc = eval_item_menu((FLAT_MENU *) item, mask, env_info, errors);
		break;
	case EDIT_DISP_ITYPE:
		rc = eval_item_edit_display((FLAT_EDIT_DISPLAY *) item, mask, env_info, errors);
		break;
	case METHOD_ITYPE:
		rc = eval_item_method((FLAT_METHOD *) item, mask, env_info, errors);
		break;
	case REFRESH_ITYPE:
		rc = eval_item_refresh((FLAT_REFRESH *) item, mask, env_info, errors);
		break;
	case UNIT_ITYPE:
		rc = eval_item_unit((FLAT_UNIT *) item, mask, env_info, errors);
		break;
	case WAO_ITYPE:
		rc = eval_item_wao((FLAT_WAO *) item, mask, env_info, errors);
		break;
	case ITEM_ARRAY_ITYPE:
		rc = eval_item_itemarray((FLAT_ITEM_ARRAY *) item, mask, env_info, errors);
		break;
	case COLLECTION_ITYPE:
		rc = eval_item_collection((FLAT_COLLECTION *) item, mask, env_info, errors);
		break;
	case BLOCK_ITYPE:
		rc = eval_item_block((FLAT_BLOCK *) item, mask, env_info, errors);
		break;
	case PROGRAM_ITYPE:
		rc = eval_item_program((FLAT_PROGRAM *) item, mask, env_info, errors);
		break;
	case RECORD_ITYPE:
		rc = eval_item_record((FLAT_RECORD *) item, mask, env_info, errors);
		break;
	case ARRAY_ITYPE:
		rc = eval_item_array((FLAT_ARRAY *) item, mask, env_info, errors);
		break;
	case VAR_LIST_ITYPE:
		rc = eval_item_var_list((FLAT_VAR_LIST *) item, mask, env_info, errors);
		break;
	case RESP_CODES_ITYPE:
		rc = eval_item_response_code((FLAT_RESP_CODE *) item, mask, env_info, errors);
		break;
	case DOMAIN_ITYPE:
		rc = eval_item_domain((FLAT_DOMAIN *) item, mask, env_info, errors);
		break;
	case AXIS_ITYPE:
		rc = eval_item_axis((FLAT_AXIS *) item, mask, env_info, errors);
		break;
	case CHART_ITYPE:
		rc = eval_item_chart((FLAT_CHART *) item, mask, env_info, errors);
		break;
	case FILE_ITYPE:
		rc = eval_item_file((FLAT_FILE *) item, mask, env_info, errors);
		break;
	case GRAPH_ITYPE:
		rc = eval_item_graph((FLAT_GRAPH *) item, mask, env_info, errors);
		break;
	case GRID_ITYPE:
		rc = eval_item_grid((FLAT_GRID *) item, mask, env_info, errors);
		break;
	case IMAGE_ITYPE:
		rc = eval_item_image((FLAT_IMAGE *) item, mask, env_info, errors);
		break;
	case LIST_ITYPE:
		rc = eval_item_list((FLAT_LIST *) item, mask, env_info, errors);
		break;
	case SOURCE_ITYPE:
		rc = eval_item_source((FLAT_SOURCE *) item, mask, env_info, errors);
		break;
	case WAVEFORM_ITYPE:
		rc = eval_item_waveform((FLAT_WAVEFORM *) item, mask, env_info, errors);
		break;
	default:
		return DDL_INVALID_PARAM;
	}

	return rc;
}


EVAL_VAR_VALUE::tag_EVAL_VAR_VALUE()	// constructor
{
	type = DDSUNUSED;
	size = 0;
	// val ??;	// no need to set "val", since the "type" is DDSUNUSED
}

EVAL_VAR_VALUE::tag_EVAL_VAR_VALUE(const EVAL_VAR_VALUE &rhs)	// copy constructor
{
	// First do the constructor part
	type = DDSUNUSED;
	size = 0;
	// val ??;	// no need to set "val", since the "type" is DDSUNUSED

	// Now do the copy part
	*this = rhs;
}


#ifdef __ANDROID__
EVAL_VAR_VALUE::~tag_EVAL_VAR_VALUE()
#else
EVAL_VAR_VALUE::~EVAL_VAR_VALUE()
#endif
{
	switch(type)
	{
		case ASCII:
		case PACKED_ASCII:
		case PASSWORD:
		case BITSTRING:
		case EUC:
			ddl_free_string(&val.s);
			break;
	}
}


EVAL_VAR_VALUE& EVAL_VAR_VALUE::operator= (const EVAL_VAR_VALUE &rhs)
{
	if (this != &rhs)
	{
		this->~tag_EVAL_VAR_VALUE();	// Clear out the old value

		size = rhs.size;
		type = rhs.type;

		switch(type)
		{
		case INTEGER:
		case TIME_VALUE:
			val.ll = rhs.val.ll;
			break;
		case UNSIGNED:
		case ENUMERATED:
		case BIT_ENUMERATED:
		case INDEX:
		case HART_DATE_FORMAT:
			val.ull = rhs.val.ull;
			break;
        case FLOATG_PT:
			val.f = rhs.val.f;
			break;
        case DOUBLEG_PT:
			val.d = rhs.val.d;
			break;
		case ASCII:
		case PACKED_ASCII:
		case PASSWORD:
		case BITSTRING:
		case EUC:
			{
                val.s.str = wcsdup(rhs.val.s.str);	// Copy the string into our output
				val.s.len = rhs.val.s.len;
				val.s.flags = FREE_STRING;
			}

			break;
		}
	}

	return *this;
}
