#pragma once

typedef unsigned short  WORD;
typedef unsigned long DWORD;
typedef int BOOL;
typedef unsigned int UINT, UINT32;
typedef unsigned long long ULONGLONG;
typedef long long LONGLONG;
typedef unsigned char BYTE;


#ifndef _WIN32
#define TRUE	1
#define FALSE	0
#define MAX_PATH          260
#define SB_CODE_PAGE	1252
//XXX: Check if it is required to define the below items
#ifndef MAXDWORD
#define MAXDWORD 0xffffffff
#endif // !1

#ifndef E_FAIL
#define E_FAIL	1
#endif // !1

#ifndef LOWORD
#define LOWORD(l)       ((unsigned short)(l))
#endif // !1

#ifndef HIWORD
#define HIWORD(l)       ((unsigned short)(((unsigned long)(l) >> 16) & 0xFFFF))
#endif // !1

#endif


