#pragma once
#include "PIFileFind.h"
#include "PISemaphore.h"
#include "PI_CtypedptrArray.h"
#include "PIEventClass.h"
#include "typedefs.h"


// Header file sections

#include "../EDD_Engine_Windows/Semaphore.h"
#include "../EDD_Engine_Windows/FileFind.h"
#include "StringUtils.h"
#include "APIs.h"
#include "../EDD_Engine_Windows/EventClass.h"

// Windows specific instance
typedef PIFileFind<WindowsFileFind> FileFind;
typedef PISemaphore<WindowsSemaphore> Semaphore;
typedef PIEventClass<WindowsEventClass> EventClass;
