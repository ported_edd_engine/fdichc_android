////////////////////////////////////////////////////////////////////////
////	Constants defined by the DCI specification.
////
////	@(#) $Id: dci_srv.h,v 1.1 2012/07/10 20:59:28 rgretta Exp $
////////////////////////////////////////////////////////////////////////

#define DCI_H 1


////////////////////////////////////////////////////////////////////////
////	Status code values.
////////////////////////////////////////////////////////////////////////

#define SC_LOCAL		0				// Root Object property
#define SC_CONSTANT		1				// Constant value (eg DDS)
#define SC_STATIC		2				// Not changed by the device
#define SC_REFRESH		3				// Changed under refresh relation
#define SC_CHANGED		4				// Parameter cache value
#define SC_FS_DEPEND	5				// File server cache under refresh
#define SC_PS_DEPEND	6				// Plant server cache under refresh

#define SC_DYNAMIC		1000			// Dynamic age in milliseconds

