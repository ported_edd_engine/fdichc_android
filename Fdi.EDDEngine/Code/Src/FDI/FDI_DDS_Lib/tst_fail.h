/*
 * @(#) $Id: tst_fail.h,v 1.1 2012/07/10 20:59:49 rgretta Exp $
 */

#ifndef TST_FAIL_H
#define TST_FAIL_H

#ifdef __cplusplus
    extern "C" {
#endif

extern int (*incr_fail_func_count_ptr) P((int));

#ifdef DDSTEST

/*
 *	These defines are indices into the fail_func_table 
 *  in ddststsr.c module.  These defines and the fail_func_table
 *  need to be in sync to work.
 */

#define DDL_PARSE_INTEGER_FUNC			0
#define DDL_PARSE_TAG_FUNC				1
#define DDL_PARSE_FLOAT					2
#define DDL_ULONG_CHOICE   				3
#define EVAL_ULONG           			4
#define DDL_PARSE_BITSTRING   			5
#define DDL_BITSTRING_CHOICE   			6
#define EVAL_BITSTRING        			7
#define EVAL_DEFINITION      			8
#define DDL_SHRINK_DEPINFO   			9
#define APPEND_DEPINFO  				10
#define DDL_ADD_CHUNK_LIST	        	11
#define DDL_COND           	      		12
#define DDL_COND_LIST      	     		13
#define EVAL_STRING_TBL    	   			15
#define EVAL_ITEM_TBL       			17
#define EVAL_DICT_REF_TBL   			18
#define EVAL_BLK_TBL     				20
#define EVAL_REL_TBL    				21
#define EVAL_PARAM_TBL    		        22
#define EVAL_BLK_ITEM_TBL   			23
#define EVAL_PARAM_MEM_NAME_TBL			24
#define EVAL_BLK_ITEM_NAME_TBL      	27
#define EVAL_PARAM_MEM_TBL          	30
#define MASK_MAN_DIR                   	31
#define I_EVAL_BLOCK_DIR     		    32
#define I_EVAL_DEVICE_DIR         		33
#define DDL_SHRINK_OUTPUT_CLASS    		34
#define DDL_SHRINK_ENUM_LIST       		35
#define DDL_PARSE_ENUMS          		36
#define DDL_ENUMS_CHOICE        		37
#define DDL_POP                			38
#define DDL_CONVERT_TYPE      			39
#define FIND_VAR_MIN_MAX     			40
#define DDL_EXPR_NONZERO    			41
#define DDL_APPLY_OP       				42
#define DDL_DO_EVAL_EXPR            	43
#define DDL_EVAL_EXPR              		44
#define DDL_EXPR_CHOICE           		45
#define DDL_PROMOTE_TO            		46
#define DDL_EXPRS_EQUAL         		47
#define EVAL_EXPR              			48
#define EVAL_INT_EXPR         			49
#define SET_VAR_VALUE_UPCALL    		50
#define DDL_SHRINK_ITEM_ARRAY_LIST    	51
#define DDL_PARSE_ITEM_ARRAY      		52
#define DDL_ITEM_ARRAY_CHOICE    		53
#define EVAL_ITEM_ARRAY         		54
#define MASK_MAN_EVAL              		55
#define I_EVAL_VAR            			56
#define I_EVAL_PROGRAM       			57
#define I_EVAL_MENU         			58
#define I_EVAL_EDIT_DISPLAY    			59
#define I_EVAL_METHOD          			60
#define I_EVAL_REFRESH        			61
#define I_EVAL_UNIT           			62
#define I_EVAL_WAO           			63
#define I_EVAL_ITEM_ARRAY   			64
#define I_EVAL_ARRAY              		65
#define I_EVAL_COLLECTION        		66
#define I_EVAL_RECORD           		67
#define I_EVAL_VAR_LIST        			68
#define I_EVAL_BLOCK          			69
#define I_EVAL_RESPONSE_CODE    		70
#define I_EVAL_DOMAIN          			71
#define I_EVAL_COMMAND        			72
#define EVAL_ONE_ITEM        			73
#define DDL_SHRINK_MEMBERS_LIST    		74
#define DDL_PARSE_MEMBERS        		75
#define DDL_MEMBERS_CHOICE        		76
#define EVAL_MEMBERS               		77
#define DDL_SHRINK_MENUITEMS_LIST   	78
#define DDL_PARSE_MENUITEMS    			79
#define DDL_MENUITEMS_CHOICE    		80
#define EVAL_MENUITEMS      			81
#define DDL_PARSE_REF        			82
#define DDL_REF_CHOICE        			83
#define EVAL_REF               			84
#define DDL_SHRINK_REFLIST      		85
#define DDL_PARSE_REFLIST        		86
#define DDL_REFLIST_CHOICE        		87
#define DDL_OP_REF_TRAIL_CHOICE    		88
#define EVAL_REFLIST                	89
#define DDL_SHRINK_OP_REF_TRAIL_LIST   	90
#define DDL_OP_REF_TRAIL_LIST_CHOICE   	91
#define EVAL_OP_REF_TRAIL_LIST    		92
#define DDL_SHRINK_REFRESH     			93
#define DDL_REFRESH_CHOICE      		94
#define EVAL_REFRESH        			95
#define DDL_SHRINK_UNIT      			96
#define DDL_UNIT_CHOICE       			97
#define EVAL_UNIT              			98
#define DDL_SHRINK_RESP_CODES   		99
#define DDL_PARSE_RESP_CODES     		100
#define DDL_RSPCODES_CHOICE       		101
#define EVAL_RESP_CODES      			102
#define SET_FETCH_FUNC_PTR    			103
#define RESOLVE_FETCH       			104
#define RESOLVE_REF          			105
#define GET_BLOCK_ID          			106
#define STRING_ENUM_LOOKUP     			107
#define DDL_PARSE_STRING        		108
#define DDL_STRING_CHOICE        		109
#define EVAL_STRING               		110
#define SET_DICT_STRING_UPCALL     		111
#define SET_DEV_SPEC_STRING_UPCALL     	112
#define DDL_SHRINK_DATAITEMS_LIST    	113
#define DDL_MASK_WIDTH        			114
#define DDL_PARSE_DATAITEMS   			115
#define DDL_DATAITEMS_CHOICE   			116
#define EVAL_DATAITEMS          		117
#define DDL_SHRINK_TRANS_LIST     		118
#define DDL_ADD_TRANSACTION_LIST   		119
#define EVAL_TRANSACTION       			120
#define EVAL_TRANSACTION_LIST   		121
#define DDL_SHRINK_RANGE_LIST   		122
#define DDL_ADD_VALUE           		123
#define DDL_PARSE_ARTH_OPTIONS   		124
#define DDL_PARSE_OPTIONS      			125
#define DDL_PARSE_TYPE_SIZE    			126
#define DDL_PARSE_TYPE      			127
#define EVAL_TYPEINFO        			128
#define EVAL_TYPE             			129
#define EVAL_DISPLAY_FORMAT    			130
#define EVAL_EDIT_FORMAT     			131
#define EVAL_SCALING_FACTOR   			132
#define EVAL_MIN_VALUES        			133
#define EVAL_ITEM_ARRAYNAME     		134
#define EVAL_MAX_VALUES          		135
#define EVAL_ENUM                 		136
#define DDI_BLOCK_HANDLE_TO_BT_ELEM     137 
#define FIND_BT_ELEM                 	138
#define UNUSED1                         139    /* A function was removed */
#define UNUSED2                         140    /* A function was removed */
#define CONVERT_ITEM_SPEC               141
#define GET_EXTN_HDR                 	142
#define PARSE_ATTRIBUTE_ID              143
#define GET_ITEM_ATTR                 	144
#define GET_ROD_ITEM                 	145
#define ROD_FCH_ITEM_SCRATCHPAD_SIZE    146
#define ROD_FCH_DIR_SCRATCHPAD_SIZE     147
#define ROD_FETCH_DEVICE_DIR            148
#define ROD_FETCH_BLOCK_DIR             149
#define GET_DT_AND_DD_HANDLES           150
#define GET_ITEM_DD_REF                 151
#define FCH_ITEM_SCRATCHPAD_SIZE        152
#define FETCH_ITEM                 		153
#define GET_BLK_DIR_DD_REF              154
#define FCH_BLOCK_DIR_SCRATCHPAD_SIZE   155
#define FETCH_BLOCK_DIR                 156
#define FCH_DEVICE_DIR_SCRATCHPAD_SIZE  157
#define FETCH_DEVICE_DIR                158
#define GET_ROD_OBJECT                 	159
#define GET_LOCAL_DATA                 	160
#define EVAL_UPDATE_TBL					161
#define EVAL_PARAM_ELEM_TBL				162
#define	EVAL_IMAGE_TBL					163
#define	EVAL_VECTORS					164
#define DDL_VECTORLIST_CHOICE			165
#define DDL_PARSE_VECTORLIST			166
#define DDL_SHRINK_REFLIST				167
#define	DDL_PARSE_VECTOR				168
#define DDL_SHRINK_VECTOR				169


#define	MAXFAILFUNC 					(EVAL_PARAM_ELEM_TBL+1)

#define TEST_FAIL(func_index) \
		{ \
			if (incr_fail_func_count_ptr) {	\
				int		macro_rs; \
				macro_rs = (*incr_fail_func_count_ptr)(func_index); \
				if (macro_rs != SUCCESS) return (macro_rs); \
			}	\
		}

#else


#define	MAXFAILFUNC 0

#define TEST_FAIL(func_index)

#endif	/* DDSTEST */

#ifdef __cplusplus
    }
#endif

#endif	/* !TST_FAIL_H */

