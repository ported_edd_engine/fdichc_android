////////////////////////////////////////////////////////////////////////
////	Simple universal definitions.
////////////////////////////////////////////////////////////////////////
#pragma once
#ifndef STD_H
#define STD_H

#pragma warning(disable: 4127)
#pragma warning(disable: 4482)

#ifdef _UNICODE

#include <cstdlib>
#if 0
#include <crtdbg.h>
#endif

#include <cstdarg>
#include <cstdio>
#include <string>
#include <cctype>
#include <cstddef>
#include <limits>
#include <sys/timeb.h>
#include <cerrno>
#include <sys/stat.h>
#include <cmath>
#include <cfloat>
#if 0
#include <afx.h>                    
#include <afxwin.h>                 
#include <afxext.h>                  
#include <afxole.h>                  
#include <windows.h>
#include <windowsx.h>                
#include <winnetwk.h>
#include <winuser.h>
#endif
#include <fstream>

#endif

#include "PlatformCommon.h"
#include "stdstring.h"

#include <ntcassert.h>
#include <BssProgLog.h>
#include <EddEngineLog.h>
//#include "../../EDD_Engine_Common/APIs.h"
#define P(x)	x

/*
 *	Allow "prototyping" for reference, but use K&R syntax for code
 */

#define	LITTLE_ENDIAN	1	/* Uses LITTLE_ENDIAN byte order */

typedef enum { FMS_FALSE, FMS_TRUE } fms_boolean;


/*
 *	For use with qsort() and bsearch()
 */

typedef int (*CMP_FN_PTR)(const void *, const void *);



/*
 *  All purpose fatal_msg() and warn_msg() messages.
 */

void fatal_msg(LPCTSTR fmt, ...);
void warn_msg(LPCTSTR fmt, ...);

/*
 *  One global to tell us if we are the fileserver.
 *  If non-zero, it contains the database file name.
 */

extern const TCHAR* fileserver;

/*
 *  One global to tell us who the Windows login user name is.
 *  And one to tell us what that user's database key is.
 */

extern TCHAR WNetUserName[];
extern unsigned long host_address;
extern TCHAR host_name[];

/*
 *	Assert macros
 */

extern void panic(char *, ...);

#define NELEM(x) (sizeof(x) / sizeof((x)[0]))

#define REQUIRE(cond)	extern int __require[(cond)]


#define insist(x) { if (!(x)) DoDebugBreak(); }
#ifdef DEBUG

#  define ASSERT_DBG(cond) if(!(cond)) BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Assertion failed: \"%s\"", _T(#cond))
#  define ASSERT_RET(cond,param) \
		if(!(cond)) BssProgLog(__FILE__, __LINE__, BssError, param, L"Condition not true: passed value = %d", param)
#  define CRASH_RET(param) \
		BssProgLog(__FILE__, __LINE__, BssError, param, L"Crash: passed value = %d", param)
#  define CRASH_DBG() \
		BssProgLog(__FILE__, __LINE__, BssError, BSS_FAILURE, L"Crash")

#else

#  define ASSERT_DBG(cond)
#  define ASSERT_RET(cond,param)	if(!(cond)) return (param)
#  define CRASH_RET(param)		return (param)
#  define CRASH_DBG()

#endif	/* DEBUG */


//#if defined DEBUG || defined _DEBUG

//#  define VERIFY(x) (x)->verify()
#  define HAS_VERIFY void verify(void)
#  define VIRTUAL_VERIFY virtual void verify(void)
#  define VIRTUAL_VERIFY_NULL virtual void verify(void) = 0
#  define assume(x) x
 
void tracef(LPTSTR fmt, ...);
void ftracef(LPTSTR where, LPTSTR fmt, ...);

/*#else

#  define VERIFY(x)
#  define HAS_VERIFY
#  define VIRTUAL_VERIFY
#  define VIRTUAL_VERIFY_NULL
#  define assert(x)
#  define assume(x)

#ifdef NO_TRACE_
#else
__inline void tracef(char* fmt, ...) { char *x = fmt; }
__inline void ftracef(char* where, char* fmt, ...) { char *x = fmt; x = where; }
#endif
#endif
*/

/*
 *	Standard defines
 */

#define FALSE 0
#define TRUE 1

/*
 *	Standard typedefs
 */

typedef unsigned char	uchar;
typedef unsigned short	ushort;
typedef unsigned int	uint;
typedef unsigned long	ulong;

typedef signed char INT8;
typedef unsigned char UINT8;
typedef signed short INT16;
typedef unsigned short UINT16;
// VC 6.0 error on redefinition of these two --
//typedef signed long INT32;
//typedef unsigned long UINT32;

typedef ulong DDITEM;

struct BLOCK;

#define SB_CODE_PAGE	1252	// Code Page for singlebyte narrow/wide conversion

#endif				/* STD_H */
