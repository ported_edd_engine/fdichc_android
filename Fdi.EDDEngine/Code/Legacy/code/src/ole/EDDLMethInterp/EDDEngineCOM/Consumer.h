#pragma once

#include "nsConsumer\IBuiltinSupport.h"
#include "nsConsumer\IParamCache.h"
#include "nsEDDEngine\IEDDEngine.h"
#include <map>

namespace nsConsumer
{

	enum enumUIRequestType { WINNONE=0, 
							 WINEDIT=1,
							 WINSELECT=2		//for combo input, selection and menu rewquest
							};


	//this class is used for UI builtins and MI GUI
	class UIConsumer
	{
	public:
		UIConsumer(void);
		~UIConsumer(void);

		//public UI functions and variables used by MI client
		wchar_t* getUIMessage();
		enumUIRequestType getUIRequestType();
		void setUICurrentSel(unsigned long selection);
		unsigned long getUICurrentSel();
		void setUserInput(BOOLEAN aug);
		void setUserEnteredText(VARIANT* aug);	//called by UI window only
		void getUIOptionList(VARIANT* aug);		//called by UI window only

		enumUIRequestType m_ulRequestType;
		wchar_t m_wsUIMsg[1024];		//UI prompt message
		unsigned long m_ulCurrentSel;
		VARIANT m_vUIOptionList;		//for UI-input/selection/menu requests
		VARIANT m_vUIUserInput;			//for UI-user entered text for input request
	};

	class Consumer :  public IBuiltinSupport, public IParamCache
	{
	public:
		Consumer(void);
		~Consumer(void);

		//
		// Comm Builtins Support
		//		Route to Communications component
		// FF
		BS_ErrorCode ReadValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec );
		BS_ErrorCode WriteValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec );

		BS_ErrorCode GetResponseCode( void* /*pValueSpec*/,
							INT8 *pError_Class,
							INT8 *pError_Code,
							INT16 *pAddl_code,
							ITEM_ID *err_id, SUBINDEX *err_subindex );
		BS_ErrorCode GetCommError( void* /*pValueSpec*/, unsigned long *pError );

		// HART
		BS_ErrorCode SendCmdTrans(void* pValueSpec, int cmd_num, int trans, unsigned char *cmd_status );
		BS_ErrorCode GetMoreStatus(void* pValueSpec, unsigned char *more_data_status, unsigned char *more_data_info, int* more_data_info_size );	// Sends Cmd 48
		
		// PB
		BS_ErrorCode SendCommand( void* pValueSpec, ITEM_ID command_id, long *pErrorValue);


		//
		// Offline Support
		//		isOffline  returns a non-zero value when the Host Application state is currently "Offline". Otherwise, it returns zero.
		int isOffline( void* /*pValueSpec*/);


		//
		// Debug Builtins Support
		//int _BPSupport( char* MethodName, char* FileName, unsigned int LineNumber, /* inout */ LOCAL_VARIABLE_LIST* LocalVariableList );

		//_ERROR: iPriority = 0
		//_WARNING: iPriority = 1
		//_TRACE: iPriority = 2
		void LogMessage( void* /*pValueSpec*/, int iPriority, wchar_t *message); // used for _ERROR, _WARNING, _TRACE and LOG_MESSAGE

		void OnMethodDebugInfo(int iBlockInstance, void* pValueSpec, unsigned long lineNumber,
			/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml);

		// These are called internally from GetParamValue() if we have a list
		PC_ErrorCode GetListElement( int iBlockInstance, ITEM_ID list_id, int index, ITEM_ID element_id, ITEM_ID member_id, EVAL_VAR_VALUE *pValue );
		PC_ErrorCode GetListElement2( int iBlockInstance, ITEM_ID list_id, int index, ITEM_ID embedded_list_id, int embedded_list_index, ITEM_ID element_id, ITEM_ID member_id, ITEM_ID var_item_id, ITEM_ID var_member_id, EVAL_VAR_VALUE *pValue );

		//
		// List Cache Builtins Support
		BS_ErrorCode ListDeleteElementAt( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int start_index, int delete_count );
		BS_ErrorCode ListInsert( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, int insert_index, nsEDDEngine::OP_REF* pInsertedOpRef );

		// These are called internally from ListInsert() or ListDeleteElementAt()
		BS_ErrorCode GetListCapacity( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, VARIANT* pvtCapacity );
		BS_ErrorCode GetListElementCount( int iBlockInstance, void* pValueSpec, nsEDDEngine::OP_REF* pListOpRef, VARIANT* pvtCount );


		PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ EVAL_VAR_VALUE * pParamValue);
		PC_ErrorCode SetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName,  /* in */ const EVAL_VAR_VALUE * pParamValue);


		//
		// Param Cache Builtins Support
		//		Route to Param Cache
		unsigned long GetDefaultIndex(int iBlockInstance, void * pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec);
		PC_ErrorCode GetParamValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in/out */ EVAL_VAR_VALUE *pValue );
		PC_ErrorCode SetParamValue( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* in */ EVAL_VAR_VALUE *pValue );
		PC_ErrorCode AssignParamValue( void* pValueSpec, int iDestBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pDestParamSpec, 
													int iSrcBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER *pSrcParamSpec );
		
		void OnMethodExiting ( int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity );

		BS_ErrorCode ProcessChangedValues(int iBlockInstance, void* pValueSpec, nsEDDEngine::ChangedParamActivity eActivity);


		//
		//  UI Builtins Support
		//		Route to UI
		BS_ErrorCode AbortRequest( void* pValueSpec, wchar_t* pPrompt, AckCallback pCallback, void* state );		// Block, returns after AbortResponse

		// the callback function is called with a scope (VT_ERROR: E_ABORT, S_OK) to indicate whether it was cancelled or not.
		BS_ErrorCode AcknowledgementRequest( void* pValueSpec, wchar_t* pPrompt, AckCallback pCallback, void *state);		// Async; after receiving the AcknowledgementResponse,

		// Must return immediately. This request will be called twice per builtin.
		// One is when the builtin is started. Another one is when the delayed time is expired.
		BS_ErrorCode DelayMessageRequest( void* pValueSpec, unsigned long lSecondsToWait, wchar_t* pPrompt, AckCallback pCallback, void* state );

		BS_ErrorCode InfoRequest( void* pValueSpec, wchar_t* pPrompt, bool bPromptUpdate );										// Async, Must return immediately, so dynamic update is possible

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest until user aborts or okays.
		// pValue is InitialValue, which is set to final value during call
		BS_ErrorCode InputRequest( void* pValueSpec, wchar_t* pPrompt, /* in */ const EVAL_VAR_VALUE *pValue, const nsEDDEngine::FLAT_VAR* pVar, AckCallback pCallback, void* state );

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest until user aborts or okays.
		BS_ErrorCode ParameterInputRequest( int iBlockInstance, void* pValueSpec, wchar_t* pPrompt, /* in */ const nsEDDEngine::OP_REF_TRAIL* pParameter, AckCallback pCallback, void* state );

		// Async, Must return immediately, so dynamic update is possible. Then call InfoRequest until user aborts or okays.
		BS_ErrorCode SelectionRequest( void* pValueSpec, wchar_t* pPrompt, wchar_t* pOptions, AckCallback pCallback, void* state);

		BS_ErrorCode UIDRequest ( int iBlockInstance, void* pValueSpec, ITEM_ID menu, wchar_t* pOptions, AckCallback pCallback, void* state);

		//For Consumer instance use
		void setEDDEngineInstance(nsEDDEngine::IEDDEngine *ptr) {pEDDEngineInstance = ptr;};

		//For MI GUI and UI builtin use
		void EnterUIBuiltinSection();
		void LeaveUIBuiltinSection();
		UIConsumer *getUIConsumerPtr() {return pUIConsumer;};
		void uiBuiltinCallback(BOOLEAN isOkayBtn);
		bool isUIBuiltinRunning();
		bool isUICallbackWaiting();

	private:
		//For UI builtin test only
		bool convertVariantToEval(VARIANT *pvtValue, nsConsumer::EVAL_VAR_VALUE *evalValue);
		bool convertEvalToVariant(const nsConsumer::EVAL_VAR_VALUE *pevalValue, VARIANT *vtValue);
		void createUIConsumerInst();
		void deleteUIConsumerInst();

		nsEDDEngine::IEDDEngine *pEDDEngineInstance;

		// map is used as a parameter cache for test purpose
		typedef std::map<unsigned long long, EVAL_VAR_VALUE> PARAMETER_CACHE;
		typedef std::map<nsEDDEngine::AttributeName, EVAL_VAR_VALUE> ATTR_VALUE;

		PARAMETER_CACHE m_ParamCache;
		ATTR_VALUE m_AttrValueList;

		//For MI GUI and UI builtin use
		CRITICAL_SECTION m_uiBltinCriticalSection;
		AckCallback m_pUICallback;
		void *m_state;
		UIConsumer *pUIConsumer;
	};

}
