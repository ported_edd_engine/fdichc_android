// ArrayExpression.cpp //

#include "stdafx.h"
#include "ArrayExpression.h"
#include "GrammarNodeVisitor.h"
//#include "SymbolTable.h"

CArrayExpression::CArrayExpression()
{
	m_pToken = 0;
}

CArrayExpression::CArrayExpression(CToken* pToken)
		:m_pToken(pToken)
{
}

CArrayExpression::~CArrayExpression()
{
	DELETE_PTR(m_pToken);

	size_t nSize = m_vecExpressions.size();
	for(size_t i=0;i<nSize;i++)
	{
		delete m_vecExpressions[i];
	}
	m_vecExpressions.clear();
}

_INT32 CArrayExpression::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitArrayExpression(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

_INT32 CArrayExpression::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	return 0;
}

void CArrayExpression::Identify(
			_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,m_pToken->GetLexeme());
	strcat(szData,">");

	size_t nSize = m_vecExpressions.size();
	for(size_t i=0;i<nSize;i++)
	{
		m_vecExpressions[i]->Identify(szData);
	}

	strcat(szData,"</");
	strcat(szData,m_pToken->GetLexeme());
	strcat(szData,">");

}

CToken* CArrayExpression::GetToken()
{
	return m_pToken;
}

EXPR_VECTOR* CArrayExpression::GetExpressions()
{
	return &m_vecExpressions;
}

void CArrayExpression::AddDimensionExpr(
			CExpression* pExpr)
{
	m_vecExpressions.push_back(pExpr);
}

_INT32 CArrayExpression::GetLineNumber()
{
	if (m_vecExpressions.size() > 0)
	{
		return m_vecExpressions[m_vecExpressions.size()-1]->GetLineNumber();
	}
	else
	{
		return 0;
	}
}
