﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;


namespace MethodInterpreterApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : Application
    {
        public App()
        {
            this.DispatcherUnhandledException += UnhandledException;
            AppDomain.CurrentDomain.UnhandledException += DomainUnhandledException;
        }

        private void DomainUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            ReportException((Exception)args.ExceptionObject);
            this.Shutdown(-2);
        }

        private void ReportException(Exception e)
        {
            string message = String.Format("UnhandledException occurred\n{0}", e.ToString());
            Trace.TraceError(message, System.Diagnostics.EventLogEntryType.Error);
        }

        private void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
        {
            ReportException(args.Exception);
            args.Handled = false;
        }
    }
}
