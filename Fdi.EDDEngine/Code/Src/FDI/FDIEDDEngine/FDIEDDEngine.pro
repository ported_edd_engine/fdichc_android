-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T13:05:54
#
#-------------------------------------------------

QT       -= gui

TARGET = FDIEDDEngine
TEMPLATE = lib
CONFIG += shared_and_static build_all
#CONFIG +=plugin
DEFINES += FDIEDDENGINE_LIBRARY \
           UNICODE
CONFIG += shared_and_static build_all
QMAKE_LFLAGS += -Wl,--version-script=$$PWD/export_fdi
QMAKESPEC=linux-g++-32
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKE_CXXFLAGS += -std=gnu++11

SOURCES += Dependencies.cpp \
    dllmain.cpp \
    FDIEngine.cpp \
    stdafx.cpp \

HEADERS += ../../../Inc/FDI/FDIEDDEngine/DDSSupport.h\
    FDIEngine.h \
    FDIEngineFactory.h \
    resource.h \
    stdafx.h \

unix {
    target.path = /usr/lib/
    INSTALLS += target
}



CONFIG(debug, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/debug
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/release
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../inc/
INCLUDEPATH += $$PWD/../FDI_DDS_Lib/
INCLUDEPATH += $$PWD/../FDI_DDS_AdapterLib/
INCLUDEPATH += $$PWD/../../../Interfaces/EDD_Engine_Interfaces/
INCLUDEPATH += $$PWD/../../../Inc/$$"EDD Engine"/
INCLUDEPATH += $$PWD/../../../Inc/FDI/FDIEDDEngine/
INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Android
    LIBS += -L$$PWD/../lib/ -Wl,--whole-archive -lFDI_DDS_Lib -Wl,--whole-archive
    LIBS += -L$$PWD/../lib/  -Wl,--whole-archive -lFDI_DDS_AdapterLib -Wl,--no-whole-archive
    LIBS += -L$$DESTDIR/../../lib/  -Wl,--whole-archive -lEDD_Engine_Android -Wl,--no-whole-archive
}
else {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Linux
    unix:!macx: LIBS += -L$$PWD/../lib/ -lFDI_DDS_Lib
    unix:!macx: LIBS += -L$$PWD/../lib/ -lFDI_DDS_AdapterLib
    unix:!macx: LIBS += -L$$DESTDIR/../../lib/  -lEDD_Engine_Linux
}

LIBS += -L$$DESTDIR/ -lEDD_Engine_Interfaces
LIBS += -L$$DESTDIR/../../lib/ -Wl,--whole-archive -lEDD_Engine_Common -Wl,--no-whole-archive

INCLUDEPATH +=  $$PWD/../../../Legacy/code/lib
DEPENDPATH +=  $$PWD/../../../Legacy/code/lib
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR
INCLUDEPATH += $$PWD/../../../Legacy/code
DEPENDPATH += $$PWD/../../../Legacy/code
INCLUDEPATH += $$PWD/../
DEPENDPATH += $$PWD/../

unix:!macx: PRE_TARGETDEPS += $$PWD/../../../Legacy/code/lib/libEDD_Engine_Common.a
unix:!macx: PRE_TARGETDEPS += $$PWD/../lib/libFDI_DDS_AdapterLib.a

#contains(ANDROID_TARGET_ARCH,x86) {
#    ANDROID_EXTRA_LIBS =
#}
#LIBS += -landroid
