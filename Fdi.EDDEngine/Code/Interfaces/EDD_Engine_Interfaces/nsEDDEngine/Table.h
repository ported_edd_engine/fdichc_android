
// Table.h

#pragma once

#include "nsEDDEngine/Attrs.h"
#include <map>


class ManualUnitLookup;		// Forward declaration


namespace nsEDDEngine
{

	/*
	*	Abbreviations Used:
	*
	*	blk - block
	*	dict - dictionary
	*	dir - directory
	*	elem - element
	*	id - identifier
	*	mem - member
	*	param - parameter
	*	rec - record
	*	ref - reference
	*	rel - relation
	*	tbl - table
	*
	*/
typedef std::map<unsigned long long, nsEDDEngine::OP_REF_LIST*> DOMINANT_TBL;

	/*
	*	Block Directory Definitions
	*/
typedef union tag_DD_REFERENCE {
	OBJECT_INDEX	object_index;
} DD_REFERENCE;


/* Block Item Table */

typedef struct tag_BLK_ITEM_TBL_ELEM {
	ITEM_ID		blk_item_id;
	int			blk_item_name_tbl_offset;
} BLK_ITEM_TBL_ELEM;

typedef struct tag_BLK_ITEM_TBL {
	int					 count;
	BLK_ITEM_TBL_ELEM	*list;
} BLK_ITEM_TBL;


/* Block Item Name Table */

typedef struct tag_BLK_ITEM_NAME_TBL_ELEM {
	ITEM_ID		blk_item_name;
	int			item_tbl_offset;
	int			param_tbl_offset;
	int			param_list_tbl_offset;
	int			rel_tbl_offset;
	int			item_to_command_tbl_offset;

} BLK_ITEM_NAME_TBL_ELEM;

typedef struct tag_BLK_ITEM_NAME_TBL {
	int						 count;
	BLK_ITEM_NAME_TBL_ELEM	*list;
} BLK_ITEM_NAME_TBL;


/* Parameter Table */

typedef struct tag_PARAM_TBL_ELEM {
	int		blk_item_name_tbl_offset;
	int		param_mem_tbl_offset;
	int		param_mem_count;
	int		param_elem_tbl_offset;
	int		param_elem_count;

	int		array_elem_count;
	int		array_elem_item_tbl_offset;

	int		array_elem_type_or_var_type;
	int		array_elem_size_or_var_size;
	ulonglong	array_elem_class_or_var_class;

} PARAM_TBL_ELEM;

typedef struct tag_PARAM_TBL {
	int				 count;
	PARAM_TBL_ELEM	*list;
} PARAM_TBL;


/* Parameter Member Table */

typedef struct tag_PARAM_MEM_TBL_ELEM {
	int		item_tbl_offset;
	int		param_mem_type;
	int		param_mem_size;
	ulong	param_mem_class;
	int		rel_tbl_offset;
} PARAM_MEM_TBL_ELEM;

typedef struct tag_PARAM_MEM_TBL {
	int					 count;
	PARAM_MEM_TBL_ELEM	*list;
} PARAM_MEM_TBL;

/* Parameter Element Table */

typedef struct tag_PARAM_ELEM_TBL_ELEM {
	SUBINDEX	param_elem_subindex;
	int			rel_tbl_offset;
} PARAM_ELEM_TBL_ELEM;

typedef struct tag_PARAM_ELEM_TBL {
	int					 count;
	PARAM_ELEM_TBL_ELEM	*list;
} PARAM_ELEM_TBL;


/* Parameter Member Name Table */

typedef struct tag_PARAM_MEM_NAME_TBL_ELEM {
	ITEM_ID		param_mem_name;
	int			param_mem_offset;
} PARAM_MEM_NAME_TBL_ELEM;

typedef struct tag_PARAM_MEM_NAME_TBL {
	int							 count;
	PARAM_MEM_NAME_TBL_ELEM		*list;
} PARAM_MEM_NAME_TBL;

// Parameter List Table
typedef struct tab_PARAM_LIST_TBL_ELEM {
	int blk_item_name_offset;
	int param_list_mem_offset;
	int param_list_mem_count;
} PARAM_LIST_TBL_ELEM;

typedef struct tab_PARAM_LIST_TBL {
	int				 count;
	PARAM_LIST_TBL_ELEM	*list;
} PARAM_LIST_TBL;

// Parameter List Member Table
typedef struct tab_PARAM_LIST_MEM_TBL_ELEM {
	int blk_item_name_offset;
} PARAM_LIST_MEM_TBL_ELEM;

typedef struct tag_PARAM_LIST_MEM_TBL {
	int				 count;
	PARAM_LIST_MEM_TBL_ELEM	*list;
} PARAM_LIST_MEM_TBL;

// Parameter List Member Name Table
typedef struct tab_PARAM_LIST_MEM_NAME_TBL_ELEM {
	ITEM_ID		param_list_mem_name;
	int blk_item_name_offset;
} PARAM_LIST_MEM_NAME_TBL_ELEM;

typedef struct tag_PARAM_LIST_MEM_NAME_TBL {
	int				 count;
	PARAM_LIST_MEM_NAME_TBL_ELEM	*list;
} PARAM_LIST_MEM_NAME_TBL;


/* Relation Table */

typedef struct tag_REL_TBL_ELEM {
	int		wao_item_tbl_offset;
	int		unit_item_tbl_offset;
	int		update_tbl_offset;
	int		update_count;
} REL_TBL_ELEM;

typedef struct tag_REL_TBL {
	int				 count;
	REL_TBL_ELEM	*list;
	ManualUnitLookup *UnitLookup;
} REL_TBL;

/* Critical Parameter Table */
typedef struct tag_CRIT_PARAM_TBL {
	int						count;
	RESOLVED_REFERENCE		*list;
} CRIT_PARAM_TBL;

/* Characteristic Table */

typedef struct tag_CHAR_MEM_TBL_ELEM {
	int		item_tbl_offset;
	int		char_mem_type;
	int		char_mem_size;
	ulong	char_mem_class;
	int		rel_tbl_offset;
} CHAR_MEM_TBL_ELEM;

typedef struct tag_CHAR_MEM_TBL {
	int					count;
	CHAR_MEM_TBL_ELEM	*list;
} CHAR_MEM_TBL;

/* Characteristic Name Table */
typedef struct tag_CHAR_MEM_NAME_TBL_ELEM {
	ITEM_ID		char_mem_name;
	int			char_mem_offset;
} CHAR_MEM_NAME_TBL_ELEM;

typedef struct tag_CHAR_MEM_NAME_TBL {
	int					count;
	CHAR_MEM_NAME_TBL_ELEM	*list;
} CHAR_MEM_NAME_TBL;

typedef struct tag_UPDATE_TBL_ELEM {
	int				desc_it_offset ;

	RESOLVED_REFERENCE to_be_updated;
} UPDATE_TBL_ELEM;

typedef struct tag_UPDATE_TBL {
	int				 count;
	UPDATE_TBL_ELEM	*list;
} UPDATE_TBL;

/* Block Directory Binary */

typedef struct tag_BIN_BLOCK_DIR {
	unsigned long	bin_exists;
	unsigned long	bin_hooked;

	BININFO		crit_param_tbl;

	BININFO		blk_item_tbl;
	BININFO		blk_item_name_tbl;

	BININFO		param_tbl;
	BININFO		param_mem_tbl;
	BININFO		param_mem_name_tbl;
	BININFO		param_elem_tbl;

	BININFO		param_list_tbl;
	BININFO		param_list_mem_tbl;
	BININFO		param_list_mem_name_tbl;

	BININFO		char_mem_tbl;
	BININFO		char_mem_name_tbl;

	BININFO		rel_tbl;
	BININFO		update_tbl;

//	BININFO		command_tbl;
} BIN_BLOCK_DIR;


/* Block Directory Flat */

typedef struct tag_FLAT_BLOCK_DIR {
	unsigned long				attr_avail;

	CRIT_PARAM_TBL				crit_param_tbl;

	BLK_ITEM_TBL 				blk_item_tbl;
	BLK_ITEM_NAME_TBL			blk_item_name_tbl;

	PARAM_TBL     				param_tbl;
	PARAM_MEM_TBL 				param_mem_tbl;
	PARAM_MEM_NAME_TBL			param_mem_name_tbl;
	PARAM_ELEM_TBL				param_elem_tbl;

	PARAM_LIST_TBL				param_list_tbl;
	PARAM_LIST_MEM_TBL			param_list_mem_tbl;
	PARAM_LIST_MEM_NAME_TBL		param_list_mem_name_tbl;

	CHAR_MEM_TBL				char_mem_tbl;
	CHAR_MEM_NAME_TBL			char_mem_name_tbl;

	REL_TBL 					rel_tbl;
	UPDATE_TBL					update_tbl;
	DOMINANT_TBL				*dominant_tbl;
//	FD_FDI remove COMMAND_TBL					command_tbl;
} FLAT_BLOCK_DIR;


/*
 *	Device Tables
 */


/* Block Table */

typedef struct tag_BLK_TBL_ELEM {
	// FD_FDI new data
	//int					block_item_info_index;
	//int					charRec_item_info_index;
	//int					charRec_block_item_name_index;
	//int					block_tables_object_index;

	// FD_FDI obsolete data
	ITEM_ID				blk_id;
	int					item_tbl_offset;
	int					char_rec_item_tbl_offset;
	int					char_rec_bint_offset;
	DD_REFERENCE		blk_dir_dd_ref;

	int					usage;
	FLAT_BLOCK_DIR		flat_block_dir;
} BLK_TBL_ELEM;

typedef struct tag_BLK_TBL {
	int				 count;
	BLK_TBL_ELEM	*list;
} BLK_TBL;


/* Item Table */

typedef struct tag_ITEM_TBL_ELEM {
	ITEM_ID         item_id;
	DD_REFERENCE    dd_ref;	
	ITEM_TYPE     item_type;
	unsigned int	symbol_index;
	unsigned int	item_offset;
} ITEM_TBL_ELEM;

typedef struct tag_ITEM_TBL {
	int				 count;
	ITEM_TBL_ELEM	*list;
} ITEM_TBL;



/* String Table */

typedef struct tag_STRING_TBL {
	wchar_t		*root;		// buffer where all the strings are stored
	int			 count;		// Number of STRINGs
	STRING      *list;		// STRING->str pointers in this list point into the root buffer
} STRING_TBL;

typedef struct tag_DICT_TBL_ELEM {
	int dictionary_id;	// INTEGER
	STRING name;		// BYTE_STRING
	STRING dictionary_entry; // BYTE_STRING
} DICT_TBL_ELEM;


/* Dictionary Reference Table */

typedef struct tag_DICT_REF_TBL {
	int			 count;
	DICT_TBL_ELEM *list;//UINT32		*list;
} DICT_REF_TBL;


/* Image table */

typedef struct tag_IMAGE_ITEM {
    char language_code[6];
	int image_file_offset;
	int image_file_length;
} IMAGE_ITEM;

typedef struct tag_IMAGE_LIST {
	// FD_FDI new data
	int number_of_languages;
	IMAGE_ITEM	*list;

	// FD_FDI obsolete data
	wchar_t			*name;
	unsigned char	*data;
    unsigned long	count;

} IMAGE_LIST;

typedef struct tag_IMAGE_TBL {
	//unsigned char	*root;	// Memory chunk pointer
	int				count;
	IMAGE_LIST		*list;
} IMAGE_TBL;


typedef struct tag_SYMBOL_TBL_ELEM {
	int 		symbol_id;
	int			type;	
	int			subtype;	
	int			item_info_index;
	wchar_t		*name;
} SYMBOL_TBL_ELEM;

typedef struct tag_SYMBOL_TBL {

	int						count;
	SYMBOL_TBL_ELEM			*list;

} SYMBOL_TBL;

typedef struct tag_COMMAND_TBL_ELEM
{
	int				command_number;			// INTEGER
	int				command_item_info_index;// INTEGER
	int				transaction_number;		// INTEGER
	int				weight;					// INTEGER
	int				count;					// INTEGER
	COMMAND_INDEX	*index_list;

} COMMAND_TBL_ELEM;

typedef struct tag_ITEM_TO_COMMAND_TBL_ELEM {
	RESOLVED_REFERENCE resolved_ref; // RESOLVED_REFERENCE

	int number_of_read_commands; // INTEGER
	COMMAND_TBL_ELEM *read_command_list;

	int number_of_write_commands;// INTEGER
	COMMAND_TBL_ELEM *write_command_list;

} ITEM_TO_COMMAND_TBL_ELEM;

typedef struct tag_ITEM_TO_COMMAND_TBL {

	int						count;
	ITEM_TO_COMMAND_TBL_ELEM *list;

} ITEM_TO_COMMAND_TBL;


typedef struct tag_COMPLEX_ITEM_TO_CMD_TBL_ELEM{
	RESOLVED_REFERENCE resolved_ref;
	int item_to_command_table_offset;
	
}COMPLEX_ITEM_TO_CMD_TBL_ELEM;


typedef struct tag_COMPLEX_ITEM_TO_CMD_TBL{
	int count;
	COMPLEX_ITEM_TO_CMD_TBL_ELEM *list;

}COMPLEX_ITEM_TO_CMD_TBL;

typedef struct tag_COMMAND_NUMBER_TBL_ELEM {
	int command_number;
	int item_info_index;
} COMMAND_NUMBER_TBL_ELEM;

typedef struct tag_COMMAND_NUMBER_TBL {

	int						count;
	COMMAND_NUMBER_TBL_ELEM			*list;

} COMMAND_NUMBER_TBL;




/* Device Directory Binary */

typedef struct tag_BIN_DEVICE_DIR {
	unsigned long	bin_exists;
	unsigned long	bin_hooked;

	BININFO		blk_tbl;
	BININFO		item_tbl;
	BININFO		string_tbl;
	BININFO		dict_ref_tbl;
//	BININFO		local_var_tbl;
	BININFO		image_tbl;
//	BININFO		dict_string_table;
	BININFO		symbol_table;

	BININFO		item_to_command_tbl;
	BININFO		command_number_tbl;
	
} BIN_DEVICE_DIR;

typedef struct tag_IMAGE_LOCATION
{
	UINT32		header_size;
	UINT32		meta_data_size;
	UINT32		item_objects_size;
    wchar_t		filepath[512];
} IMAGE_LOCATION;

	/* Device Directory Flat */

typedef struct tag_FLAT_DEVICE_DIR {
	unsigned long		attr_avail;

	BLK_TBL				blk_tbl;
	ITEM_TBL			item_tbl;
	STRING_TBL			string_tbl;
	DICT_REF_TBL		dict_ref_tbl;
	IMAGE_TBL			image_tbl;
	SYMBOL_TBL			symbol_tbl;

	ITEM_TO_COMMAND_TBL item_to_command_tbl;
	COMPLEX_ITEM_TO_CMD_TBL complex_item_to_cmd_tbl;
	COMMAND_NUMBER_TBL command_number_tbl;

	EDDFileHeader		header;
	IMAGE_LOCATION		image_loc;
//	wchar_t				dd_file_path[512];
//	DDOD_HEADER			header;
} FLAT_DEVICE_DIR;

#if 0 // TODO MHD Determine whether we really need the macros below.
	/*
	*	Convenience Macros for Handling Device Tables
	*
	*/


	/* Block Table macros */

#define	BT_LIST(bt_ptr) (bt_ptr->list)
#define	BT_COUNT(bt_ptr) (bt_ptr->count)
#define	BTE(bt_ptr,elem) (&bt_ptr->list[elem])

	/* Block Table element macros */

#define	BTE_BLK_ID(bte_ptr) (bte_ptr->blk_id)
#define	BTE_BD_DD_REF(bte_ptr) (bte_ptr->blk_dir_dd_ref)
#define	BTE_IT_OFFSET(bte_ptr) (bte_ptr->item_tbl_offset)
#define	BTE_USAGE(bte_ptr) (bte_ptr->usage)

#define	BTE_BIT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_tbl)
#define	BTE_BINT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_name_tbl)
#define	BTE_PT(bte_ptr) (&bte_ptr->flat_block_dir.param_tbl)
#define	BTE_PMT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_tbl)
#define	BTE_PET(bte_ptr) (&bte_ptr->flat_block_dir.param_elem_tbl)
#define	BTE_PMNT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_name_tbl)
#define	BTE_RT(bte_ptr) (&bte_ptr->flat_block_dir.rel_tbl)
#define	BTE_UT(bte_ptr) (&bte_ptr->flat_block_dir.update_tbl)
#define	BTE_CT(bte_ptr) (&bte_ptr->flat_block_dir.command_tbl)
#define	BTE_CPT(bte_ptr) (&bte_ptr->flat_block_dir.crit_param_tbl)


	/* Item Table macros */

#define	IT_LIST(it_ptr) (it_ptr->list)
#define	IT_COUNT(it_ptr) (it_ptr->count)
#define	ITE(it_ptr,elem) (&it_ptr->list[elem])

	/* Item Table element macros */

#define	ITE_ITEM_ID(ite_ptr) (ite_ptr->item_id)
#define	ITE_DD_REF(ite_ptr) (ite_ptr->dd_ref)
#define	ITE_ITEM_TYPE(ite_ptr) (ite_ptr->item_type)



	/*
	*	Convenience Macros for Handling Block Tables
	*/


	/* Block Item Table macros */

#define	BIT_LIST(bit_ptr) (bit_ptr->list)
#define	BIT_COUNT(bit_ptr) (bit_ptr->count)
#define	BITE(bit_ptr,elem) (&bit_ptr->list[elem])

	/* Block Item Table element macros */

#define	BITE_BLK_ITEM_ID(bite_ptr) (bite_ptr->blk_item_id)
#define	BITE_BINT_OFFSET(bite_ptr) (bite_ptr->blk_item_name_tbl_offset)


	/* Block Item Name Table macros */

#define	BINT_LIST(bint_ptr) (bint_ptr->list)
#define	BINT_COUNT(bint_ptr) (bint_ptr->count)
#define	BINTE(bint_ptr,elem) (&bint_ptr->list[elem])

	/* Block Item Name Table element macros */

#define	BINTE_BLK_ITEM_NAME(binte_ptr) (binte_ptr->blk_item_name)
#define	BINTE_IT_OFFSET(binte_ptr) (binte_ptr->item_tbl_offset)
#define	BINTE_PT_OFFSET(binte_ptr) (binte_ptr->param_tbl_offset)
#define	BINTE_RT_OFFSET(binte_ptr) (binte_ptr->rel_tbl_offset)

#define	BINTE_RCT_OFFSET(binte_ptr) (binte_ptr->read_cmd_tbl_offset)
#define	BINTE_RCT_COUNT(binte_ptr) (binte_ptr->read_cmd_count)

#define	BINTE_WCT_OFFSET(binte_ptr) (binte_ptr->write_cmd_tbl_offset)
#define	BINTE_WCT_COUNT(binte_ptr) (binte_ptr->write_cmd_count)


	/* Parameter Table macros */

#define	PT_LIST(pt_ptr) (pt_ptr->list)
#define	PT_COUNT(pt_ptr) (pt_ptr->count)
#define	PTE(pt_ptr,elem) (&pt_ptr->list[elem])

	/* Parameter Table element macros */

#define	PTE_BINT_OFFSET(pte_ptr) (pte_ptr->blk_item_name_tbl_offset)
#define	PTE_PMT_OFFSET(pte_ptr) (pte_ptr->param_mem_tbl_offset)
#define	PTE_PM_COUNT(pte_ptr) (pte_ptr->param_mem_count)
#define	PTE_PET_OFFSET(pte_ptr) (pte_ptr->param_elem_tbl_offset)
#define	PTE_PE_COUNT(pte_ptr) (pte_ptr->param_elem_count)
#define	PTE_AE_IT_OFFSET(pte_ptr) (pte_ptr->array_elem_item_tbl_offset)
#define	PTE_PE_MX_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_VAR_TYPE(pte_ptr) (pte_ptr->array_elem_type_or_var_type)
#define	PTE_AE_VAR_SIZE(pte_ptr) (pte_ptr->array_elem_size_or_var_size)
#define	PTE_AE_VAR_CLASS(pte_ptr) (pte_ptr->array_elem_class_or_var_class)


	/* Parameter Member Table macros */

#define	PMT_LIST(pmt_ptr) (pmt_ptr->list)
#define	PMT_COUNT(pmt_ptr) (pmt_ptr->count)
#define	PMTE(pmt_ptr,elem) (&pmt_ptr->list[elem])

	/* Parameter Member Table element macros */

#define	PMTE_IT_OFFSET(pmte_ptr) (pmte_ptr->item_tbl_offset)
#define	PMTE_PM_TYPE(pmte_ptr) (pmte_ptr->param_mem_type)
#define	PMTE_PM_SIZE(pmte_ptr) (pmte_ptr->param_mem_size)
#define	PMTE_PM_CLASS(pmte_ptr) (pmte_ptr->param_mem_class)
#define	PMTE_RT_OFFSET(pmte_ptr) (pmte_ptr->rel_tbl_offset)

	/* Parameter Member Name Table macros */

#define	PMNT_LIST(pmnt_ptr) (pmnt_ptr->list)
#define	PMNT_COUNT(pmnt_ptr) (pmnt_ptr->count)
#define	PMNTE(pmnt_ptr,elem) (&pmnt_ptr->list[elem])

	/* Parameter Member Name Table element macros */

#define	PMNTE_PM_NAME(pmte_ptr) (pmte_ptr->param_mem_name)
#define	PMNTE_PM_OFFSET(pmte_ptr) (pmte_ptr->param_mem_offset)

	/* Parameter Element Table macros */

#define	PET_LIST(pet_ptr) (pet_ptr->list)
#define	PET_COUNT(pet_ptr) (pet_ptr->count)
#define	PETE(pet_ptr,elem) (&pet_ptr->list[elem])

	/* Parameter Element Table element macros */

#define	PETE_PE_SUBINDEX(pete_ptr) (pete_ptr->param_elem_subindex)
#define	PETE_RT_OFFSET(pete_ptr) (pete_ptr->rel_tbl_offset)

	/* Relation Table macros */

#define	RT_LIST(rt_ptr) (rt_ptr->list)
#define	RT_COUNT(rt_ptr) (rt_ptr->count)
#define	RTE(rt_ptr,elem) (&rt_ptr->list[elem])

	/* Relation Table element macros */

#define	RTE_WAO_IT_OFFSET(rte_ptr) (rte_ptr->wao_item_tbl_offset)
#define	RTE_UNIT_IT_OFFSET(rte_ptr) (rte_ptr->unit_item_tbl_offset)
#define	RTE_UT_OFFSET(rte_ptr) (rte_ptr->update_tbl_offset)
#define	RTE_U_COUNT(rte_ptr) (rte_ptr->update_count)
#define	RTE_UNIT_COUNT(rte_ptr) (rte_ptr->unit_count)

	/* Update Table macros */

#define	UT_LIST(ut_ptr) (ut_ptr->list)
#define	UT_COUNT(ut_ptr) (ut_ptr->count)
#define	UTE(ut_ptr,elem) (&ut_ptr->list[elem])

	/* Update Table element macros */

#define	UTE_OP_IT_OFFSET(ute_ptr) (ute_ptr->op_it_offset)
#define	UTE_OP_SUBINDEX(ute_ptr) (ute_ptr->op_subindex)
#define	UTE_DESC_IT_OFFSET(ute_ptr) (ute_ptr->desc_it_offset)


	/* Command Table macros */

#define	CT_LIST(ct_ptr) (ct_ptr->list)
#define	CT_COUNT(ct_ptr) (ct_ptr->count)
#define	CTE(ct_ptr, elem) (&ct_ptr->list[elem])

	/* Command Table element macros */

#define	CTE_NUMBER(cte_ptr) (cte_ptr->number)
#define	CTE_SUBINDEX(cte_ptr) (cte_ptr->subindex)
#define	CTE_COUNT(cte_ptr) (cte_ptr->count)
#define	CTE_LIST(cte_ptr) (cte_ptr->list)


	/* Critical Parameters Table macros */

#define	CPT_LIST(cpt_ptr) (cpt_ptr->list)
#define	CPT_COUNT(cpt_ptr) (cpt_ptr->count)


	/* Default table offset (for optional elements) */

#define DEFAULT_OFFSET      (-1)

#endif	// Table Macros

}
