/*
 *	@(#) $Id: od_defs.h,v 1.2 2012/07/20 22:38:57 rgretta Exp $
 *
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 */

#ifndef OD_DEFS_H
#define OD_DEFS_H

/*
 *	Definitions pertaining to the OD description object.
 */

/* Sizes of the data fields (in octets) */

#define	ODES_INDEX_SIZE				2
#define	FIRST_IN_SOD_SIZE			2
#define	SOD_LENGTH_SIZE				2

#define	ODES_SIZE					(ODES_INDEX_SIZE +		\
									 FIRST_IN_SOD_SIZE +	\
									 SOD_LENGTH_SIZE)

/* Offsets of the data fields (in octets) */

#define	ODES_INDEX_OFFSET			0

#define	FIRST_IN_SOD_OFFSET			(ODES_INDEX_OFFSET +	\
									 ODES_INDEX_SIZE)

#define	SOD_LENGTH_OFFSET			(FIRST_IN_SOD_OFFSET +	\
									 FIRST_IN_SOD_SIZE)


/*
 *	Definitions pertaining to the fixed part of a domain object.
 */

/* Sizes of the data fields (in octets) */

#define	OBJECT_INDEX_SIZE			2
#define	OBJECT_CODE_SIZE			1
#define	MAX_OCTETS_SIZE				4
#define	LOCAL_ADDRESS_SIZE			4

#define	FIXED_SIZE					(OBJECT_INDEX_SIZE +	\
									 OBJECT_CODE_SIZE +		\
									 MAX_OCTETS_SIZE +	\
									 LOCAL_ADDRESS_SIZE )

/* Offsets of the data fields (in octets) */

#define	OBJECT_INDEX_OFFSET			0

#define	OBJECT_CODE_OFFSET			(OBJECT_INDEX_OFFSET +	\
									 OBJECT_INDEX_SIZE)

#define	MAX_OCTETS_OFFSET			(OBJECT_CODE_OFFSET +	\
									 OBJECT_CODE_SIZE)

#define	LOCAL_ADDRESS_OFFSET		(MAX_OCTETS_OFFSET +	\
									 MAX_OCTETS_SIZE)



#endif	/* OD_DEFS_H */


