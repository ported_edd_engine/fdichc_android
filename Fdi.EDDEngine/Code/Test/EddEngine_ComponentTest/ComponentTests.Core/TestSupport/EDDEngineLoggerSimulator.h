//#include <windows.h>
//#include <map>
//#include <vector>
//#include <string>
#include "nsConsumer/IEDDEngineLogger.h"

using namespace nsConsumer;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            class EDDEngineLoggerSimulator : public nsConsumer::IEDDEngineLogger
            {
            public:
                EDDEngineLoggerSimulator();
                ~EDDEngineLoggerSimulator();
				bool ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory);
				void Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory);

            private:
               
            };

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests

