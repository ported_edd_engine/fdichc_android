#pragma once

#include "EDDEngineImpl.h"
#include "nsEDDEngine/IEDDEngine.h"
#include "cm_dds.h"


#include <cm_loc.h>
#include <cmTblManipulation.h>
#include "evl_lib.h"
#include "evl_loc.h"
#include "conversions.h"
#include "DDSSupport.h"
#include <map>


class CFDIEngine : public CEDDEngineImpl, public IDDSSupport
{
private:	// class members
        BLOCK_HANDLE m_block_handle;
		// Use a map to associate a BLOCK_INSTANCE (key) with a BLOCK_HANDLE (value)
		std::map<nsEDDEngine::BLOCK_INSTANCE, BLOCK_HANDLE>  m_mapBlockInst;
		DEVICE_HANDLE m_deviceHandle;
		DEVICE_TYPE_HANDLE m_deviceTypeHandle;
        DDOD_HEADER m_header;
        nsEDDEngine::FLAT_DEVICE_DIR *m_pflat_device_dir;
		CConnectionMgr m_connectionMgr;
		DDEF_DESC m_desc;
private:	// class methods
        BLOCK_HANDLE BlockSearch( unsigned short objectIndex );
		static int CompareObjectIndex( nsEDDEngine::CCrossblockInfo* pElement1 , nsEDDEngine::CCrossblockInfo* pElement2 );

public:
	CFDIEngine( const wchar_t *sEDDBinaryFilename, 
		 const wchar_t* /* pConfigXML */, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode);
	~CFDIEngine();

	
	int GetBlockCount();
	void CreateDevice(DDOD_HEADER header);
	nsEDDEngine::FLAT_DEVICE_DIR *GetFlatDeviceDir();

	BLOCK_HANDLE BlockInstToBlockHandle(nsEDDEngine::BLOCK_INSTANCE iBlockInstance);
	int BlockHandleToBlockInst(BLOCK_HANDLE bh, nsEDDEngine::BLOCK_INSTANCE *block_instance);
	nsEDDEngine::ITEM_TYPE Set_ITEM_TYPE(ITEM_TYPE itemType);
	void PostProcessFlatVar( nsEDDEngine::FLAT_VAR* pFlatVar );
	void PostProcessFlatTemplate( int iBlockInstance, nsEDDEngine::FLAT_TEMPLATE* pFlatTemplate );
	void PostProcessOctetVariable(nsEDDEngine::FLAT_VAR* pFlatVar);
#pragma region IConvenience Methods
	//
    // Unit Convenience functions
    //
    // Gets the Unit Relation ItemId for the specified parameter
    int GetParamUnitRelItemId(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, ITEM_ID * pUnitItemId);
    int GetAxisUnitRelItemId(int iBlockInstance, void* pValueSpec, ITEM_ID AxisItemId, ITEM_ID *pUnitItemId);

    // Command Convenience functions

    // Retrieves the List of Commands that can be used to read/write this parameter
    int GetCommandList(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER * pParamSpec, nsEDDEngine::CommandType eCmdType, nsEDDEngine::FDI_COMMAND_LIST * pCommandList);

	// Retrieves the ItemId of the Command indicated by the Command Number (HART only)
    int GetCmdIdFromNumber(int iBlockInstance, ulong ulCmdNumber, ITEM_ID * pCmdItemId);

    //
    // String Convenience functions
    //
    // Converts an EDD Engine Error code to a string
	wchar_t *GetErrorString(int iErrorNum);
	int GetStringTranslation(const wchar_t *string, const wchar_t *lang_code, wchar_t* outbuf, int outbuf_size); 

    //
    // Type Convenience functions
    //
    int GetItemType(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType);
    int GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE *pItemType, ITEM_ID *pItemId);
    int GetParamType(int iBlockInstance, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, nsEDDEngine::TYPE_SIZE *pTypeSize);
	
    int GetSymbolNameFromItemId(ITEM_ID item_id, wchar_t* item_name, int outbuf_size);
	int GetItemIdFromSymbolName(wchar_t* pItemName, ITEM_ID* IptemId);
#pragma endregion

	int GetItem( int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_ITEM_SPECIFIER * pItemSpec,nsEDDEngine::AttributeNameSet *pAttributeNameSet, const wchar_t *lang_code, nsEDDEngine::FDI_GENERIC_ITEM * pGenericItem );
	
	int GetDeviceDir(nsEDDEngine::FLAT_DEVICE_DIR **pDeviceDir);
	void GetEDDFileHeader(nsEDDEngine::EDDFileHeader* pEDDFileHeader);

	
	int AddFFBlocks( const nsEDDEngine::CFieldbusBlockInfo *pFieldbusBlockInfo, nsEDDEngine::BLOCK_INSTANCE *arrBlockInstance, int iCount );
	int GetCriticalParams(int iBlockInstance, nsEDDEngine::CRIT_PARAM_TBL** pCriticalParamTbl);
	int GetRelationsAndUpdateInfo(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER* pParamSpec, ITEM_ID* pWAO_Item, ITEM_ID* pUNIT_Item, nsEDDEngine::OP_REF_LIST* pUpdateList, nsEDDEngine::OP_REF_LIST* pDominantList = NULL);

#pragma region IDDSSupport Methods	
	int  DDS_GetParamValue( BLOCK_HANDLE bh, void* pValueSpec, nsEDDEngine::OP_REF *op_ref, nsConsumer::EVAL_VAR_VALUE *param_value);
	CConnectionMgr* GetConnectionManager();
	int GetBlockInstAndHandle( unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance, BLOCK_HANDLE* piBlockHandle );
    void DDS_Log( wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory);
    // Wrapper function for accesssing IParamCache function of the same name
	nsConsumer::PC_ErrorCode DDS_GetDynamicAttribute( BLOCK_HANDLE bh, void* pValueSpec, DESC_REF* desc_ref,
        const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue);

#pragma endregion

	void Init_ENV_INFO( ENV_INFO *env_info, void* pValueSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInstance, const wchar_t *lang_code );
	void Init_BLOCK_SPECIFIER( DDI_BLOCK_SPECIFIER *pBlockSpec, nsEDDEngine::BLOCK_INSTANCE iBlockInstance );

	int GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
	int GetDictionaryString( wchar_t* wsDictName,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
	int GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code );
	int ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE /*iBlockInstance*/, ITEM_ID /*ItemId*/, ITEM_ID /*MemberId*/, int* /*iSubIndex*/);
	
	int GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence);
	int GetBlockInstanceByTag( ITEM_ID iItemId, wchar_t* pTag, int* iOccurrence);
	int GetBlockInstanceCount( ITEM_ID iItemId, int* piCount);
	int ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance);

#ifdef _DEBUG	
	int DisplayDebugInfo();
#endif
};
