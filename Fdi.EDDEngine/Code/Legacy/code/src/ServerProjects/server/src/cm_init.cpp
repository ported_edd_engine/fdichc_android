
/**
 *	@(#) $Id: cm_init.cpp,v 1.8 1995/10/06 17:38:36 davcomf Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_init.c - Connection Manager Initialization Module
 *
 *	This file contains all functions pertaining to the
 *	initialization of the Connection Manager.  It also
 *	contains any functions that are common to all of the
 *	Connection Manager modules.
 *
 */

#include "stdinc.h"
#include "ConnectionMgr.h"


/***********************************************************************
 *
 *	Name:	cm_cleanup
 *
 *	ShortDesc:	Closes all the blocks and closes down communications
 *
 *	Description:
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Zero for success and non-zero for failure
 *
 *	Authors:
 *		Kent Anderson
 *
 *		CAY Move here from cm_comm.cpp
 *
 *		
 *
 **********************************************************************/

int CConnectionMgr::cm_cleanup(ENV_INFO *env_info)
{
	BLOCK_HANDLE	bh ;
	//int				r_code ; // CAY unused 

	/*
	 * Go through each block
	 */

	for (bh = 0 ; bh < active_blk_tbl.count; bh++) {
		ct_block_close(env_info, bh) ;
	}


	/*
	 * Free the Active Block, Device, and Device Type Tables.
	 */

	free(active_blk_tbl.list) ;
	active_blk_tbl.list = NULL ;
	active_blk_tbl.count = 0 ;

	free(active_dev_tbl.list) ;
	active_dev_tbl.list = NULL ;
	active_dev_tbl.count = 0 ;

	return(CM_SUCCESS);
}