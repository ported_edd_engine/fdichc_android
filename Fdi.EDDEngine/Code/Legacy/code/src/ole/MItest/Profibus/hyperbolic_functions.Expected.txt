﻿<Method>
{

int nChoice;

int itemID[1];

unsigned char ucVar;

DD_STRING sVar;

float x;

float y;

itemID[0] = 148;

get_dev_var_value(literal_string(xxx), itemID, method_time_value);

lassign(method_local_date1, 721522);

method_local_date2 = method_local_date1;

ucVar = 0x89;

get_local_var_value(literal_string(xxx), itemID, ucVar);

get_local_var_value(literal_string(xxx), itemID, sVar);

ACKNOWLEDGE(literal_string(xxx));

x = 4;

do

{

	nChoice = SELECT_FROM_LIST(literal_string(xxx), literal_string(xxx));

	switch( nChoice )

	{

		case 1:

		y = sinh(x);

		ACKNOWLEDGE(literal_string(xxx));

		break;

		case 2:

		y = cosh(x);

		ACKNOWLEDGE(literal_string(xxx));

		break;

		case 3:

		y = tanh(x);

		ACKNOWLEDGE(literal_string(xxx));

		break;

		default:

		break;

	}

} while (nChoice > 0);

}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Testing builtin get_local_var_value() with time value.
End of Function literal_string
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 124, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: TIME_VALUE, Value: 65535
IBuiltinSupport ParameterInputRequest() is being called with "Testing builtin get_local_var_value() with time value."
IBuiltinSupport ParameterInputRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: _REINVOKE_get_dev_var_value
  Number Of Input Parameters: 3
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: Testing builtin get_local_var_value() with time value.
    Param 2 Type: RUL_SAFEARRAY
    Param 2 Array[0]: 148
    Param 3 Type: RUL_INT
    Param 3 Value: 124
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_get_dev_var_value
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 125, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: EDD_DATE, Value: 1901402
IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 125, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: EDD_DATE, Value: 721522
Function Name: _REINVOKE_lassign
  Number Of Input Parameters: 2
    Param 1 Type: RUL_INT
    Param 1 Value: 125
    Param 2 Type: RUL_INT
    Param 2 Value: 721522
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_lassign
IBuiltinSupport GetParamValue() is being called with Standard Reference ID: 125, Subindex: 0, Param: 0
IBuiltinSupport GetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: EDD_DATE, Value: 721522
IBuiltinSupport SetParamValue() is being called with Standard Reference ID: 126, Subindex: 0, Param: 0
IBuiltinSupport SetParamValue with returned value 0 () is returned with EVAL_VAR_VALUE Type: EDD_DATE, Value: 721522
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Testing builtin get_local_var_value() with unsigned char.
End of Function literal_string
IBuiltinSupport InputRequest() is being called with "Testing builtin get_local_var_value() with unsigned char."
IBuiltinSupport InputRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: _REINVOKE_get_local_var_value
  Number Of Input Parameters: 3
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: Testing builtin get_local_var_value() with unsigned char.
    Param 2 Type: RUL_SAFEARRAY
    Param 2 Array[0]: 148
    Param 3 Type: RUL_CHARPTR
    Param 3 Value: ucVar
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_get_local_var_value
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_UNSIGNED_CHAR
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Testing builtin get_local_var_value() with DD_STRING.
End of Function literal_string
IBuiltinSupport InputRequest() is being called with "Testing builtin get_local_var_value() with DD_STRING."
IBuiltinSupport InputRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: _REINVOKE_get_local_var_value
  Number Of Input Parameters: 3
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: Testing builtin get_local_var_value() with DD_STRING.
    Param 2 Type: RUL_SAFEARRAY
    Param 2 Array[0]: 148
    Param 3 Type: RUL_CHARPTR
    Param 3 Value: sVar
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function _REINVOKE_get_local_var_value
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: This tests hyperbolic functions
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "This tests hyperbolic functions"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: This tests hyperbolic functions
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Select hyperbolic function to test
End of Function literal_string
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: Exit;sinh;cosh;tanh
End of Function literal_string
IBuiltinSupport SelectionRequest() is being called with "Select hyperbolic function to test"
UI Builtin Selection List: "Exit;sinh;cosh;tanh"
IBuiltinSupport SelectionRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: SELECT_FROM_LIST
  Number Of Input Parameters: 2
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: Select hyperbolic function to test
    Param 2 Type: RUL_DD_STRING
    Param 2 Value: Exit;sinh;cosh;tanh
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function SELECT_FROM_LIST
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><nChoice>0,</nChoice><itemID>[0]148,</itemID><ucVar>137,</ucVar><sVar></sVar><x>4.00000,</x><y>0.00000,</y></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being saved"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
