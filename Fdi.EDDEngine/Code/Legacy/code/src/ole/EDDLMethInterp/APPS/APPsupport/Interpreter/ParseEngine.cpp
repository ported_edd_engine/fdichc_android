
#include "stdafx.h"
#include "ParserDeclarations.h"
#include "SafeVar.h"

#ifdef _FULL_RULE_ENGINE

/*BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}*/

#endif

//const _CHAR* szTokenStrings[] = 	{	
//									"NONE",
//									"KEYWORD",
//									"SYMBOL",
//									"ARITHMETIC_OPERATOR",
//									"ASSIGNMENT_OPERATOR",
//									"RELATIONAL_OPERATOR",
//									"LOGICAL_OPERATOR",
//									"NUMERIC_CONSTANT", 
//									"STRING_CONSTANT",
//									"SIMPLE_VARIABLE", 
//									"ARRAY_VARIABLE", 
//									"SERVICE",
//									"TYPE_ERROR",
//									"COMMENT",
//									"EOF"
//
//							};


const _CHAR* szTokenSubstrings[] = {
									"SUBTYPE_NONE",
									//INT Operators
									"UPLUS", 
									"UMINUS", 
									"PLUS", 
									"MINUS", 
									"MUL", 
									"DIV", 
									"MOD",
									"EXP",
									"NOT_EQ",  
									"LT",
									"GT",
									"EQ",  
									"GE", 
									"LE",
									"LOGIC_AND",
									"LOGIC_OR",
									"LOGIC_NOT",
									"ASSIGN",
									//FLOAT Operators
									"FUPLUS", 
									"FUMINUS", 
									"FPLUS", 
									"FMINUS", 
									"FMUL", 
									"FDIV",
									"FMOD",
#ifdef XMTR
									"FRAND",
#endif
									"FEXP",
									"I2F",
									"F2I",
									"NOT_FEQ"
									"FLT",
									"FGT",
									"FEQ",  
									"FGE", 
									"FLE",
									//String Operators
									"SPLUS",
									"SEQ",
									"NOT_SEQ",
									//Keywords
									"IF",
									"ELSE",
									"WHILE",
									"CHAR_DECL",
									"INTEGER_DECL",
									"REAL_DECL", 
									"BOOLEAN_DECL", 
									"STRING_DECL",	
									//Symbols
									"LPAREN",
									"RPAREN",
									"LBRACK",
									"RBRACK",
									"LBOX",
									"RBOX",
									"SEMICOLON",
									"COMMA",
									"DOT",
									"SCOPE",
									//Constants
									"CHAR_CONSTANT", 
									"INT_CONSTANT", 
									"REAL_CONSTANT",
									"BOOL_CONSTANT",
									"STRING_CONSTANT",
									//Service SubTypes
									"SERVICE_INVOKE",
									"SERVICE_ATTRIBUTE",
									//Rule Self Invoke
									"RULE_ENGINE",
									"INVOKE",
									//Object manager
									"OM",
									//General
									"DOLLAR",
									"SUBTYPE_ERROR"
								};

void	TokenType_to_VariantType(
			RUL_TOKEN_TYPE token,
			RUL_TOKEN_SUBTYPE subtoken,
			VARIANT_TYPE& vt)
{
	switch(subtoken)
	{
	case RUL_CHAR_CONSTANT:
	case RUL_CHAR_DECL:
		vt = RUL_CHAR;
		break;
	case RUL_LONG_LONG_DECL:
		vt = RUL_LONGLONG;
		break;
	case RUL_UNSIGNED_LONG_LONG_DECL:
		vt = RUL_ULONGLONG;
		break;
		// Walt EPM 08sep08 - added
	case RUL_UNSIGNED_SHORT_INTEGER_DECL:
	case RUL_DD_STRING_DECL:
		vt = RUL_USHORT;
		break;
	case RUL_SHORT_INTEGER_DECL:
		vt = RUL_SHORT;
		break;
	case RUL_UNSIGNED_INTEGER_DECL:
		vt = RUL_UINT;
		break;
		// end add  Walt EPM 08sep08
	case RUL_INT_CONSTANT:
	case RUL_INTEGER_DECL:
	case RUL_LONG_DECL:
	case RUL_TIMET_DECL:
		vt = RUL_INT;
		break;
	case RUL_REAL_CONSTANT:
	case RUL_REAL_DECL:
		vt = RUL_FLOAT;
		break;
	case RUL_DOUBLE_DECL:
		vt = RUL_DOUBLE;
		break;
	case RUL_UNSIGNED_CHAR_DECL:
		vt = RUL_UNSIGNED_CHAR;
		break;
	}
}
