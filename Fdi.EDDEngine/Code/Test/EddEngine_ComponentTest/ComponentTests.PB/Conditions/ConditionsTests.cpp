#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_PB
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static String^ test_var_1_label;
            static FDI_GENERIC_ITEM* genericItem ;
            static ParamCacheSimulator* paramCacheSimulator;
            static EVAL_VAR_VALUE* test_var_2_value;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0007.DDL.bin");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize( paramCacheSimulator);
                

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                test_var_2_value = new EVAL_VAR_VALUE();
                test_var_2_value->type = VT_INTEGER;
                test_var_2_value->size = 4;
                test_var_2_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, test_var_2_value);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_contitioned_2", &test_var_1_specifier);
                genericItem =  engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                // get the label of "test_var_1"
                FLAT_VAR* test_var_1 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_1_label = gcnew String(test_var_1->label.c_str());
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete test_var_2_specifier;
                delete test_var_1_label;
                delete genericItem ;
                delete test_var_2_value;
                delete engine;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_some_missing_values_with_PB
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator;
            static EddEngineController^ engine;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0007.DDL.bin");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize( paramCacheSimulator);
                

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // configure ParamCacheSimulator to return no value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, nullptr);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_contitioned_2", &test_var_1_specifier);
                genericItem =  engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete test_var_2_specifier;
                delete genericItem;
                delete engine;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_error()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(1, static_cast<int>(genericItem->errors.count));
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_the_error_BAD_ATTR()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual((UINT)nsEDDEngine::validity, (UINT) genericItem->errors.list[0].bad_attr);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_error_with_the_correct_missing_item_id()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(test_var_2_specifier->id, genericItem->errors.list[0].var_needed.op_info.id);
            }
        };

        [TestClass]
        public ref class When_Call_unconditioned_Attributes_with_PB
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static  String^ test_var_2_label;
            static FDI_GENERIC_ITEM* genericItem;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE* test_var_2_value;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0007.DDL.bin");
                auto paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                test_var_2_value = new EVAL_VAR_VALUE();
                test_var_2_value->type = VT_INTEGER;
                test_var_2_value->size = 4;
                test_var_2_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, test_var_2_value);

                // request "test_var_2"; 
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_2", &test_var_1_specifier);
                genericItem =  engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE);

                // get the label of "test_var_2"
                FLAT_VAR* test_var_2 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_2_label = gcnew String(test_var_2->label.c_str());
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete test_var_2_specifier;
                delete test_var_2_label;
                delete genericItem;
                delete engine;
                delete test_var_2_value;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_attribute()
            {
                // check if the label of "test_var_1" has the expected value
                Assert::AreEqual("Test Variable 1", test_var_2_label);
            }
        };

    }
}
