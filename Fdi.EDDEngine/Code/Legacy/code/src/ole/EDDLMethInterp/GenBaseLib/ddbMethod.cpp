/*************************************************************************************************
 *
 * $Workfile: ddbMethod.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		09/09/03	sjv	started creation
 */
#include "stdafx.h"
#include <CMethodInterpreterDevice.h>
#include "varientTag.h"

//
// Constructor
//
hCmethod::hCmethod(DevInfcHandle_t h, aCitemBase* paItemBase) : hCitemBase(h,paItemBase)
{
}

hCmethod::hCmethod(hCmethod*    pSrc,  itemIdentity_t newID) : hCitemBase(pSrc,newID)
{
}

//
// Destructor
//
hCmethod::~hCmethod()
{
}


// required virtual
hCattrBase*   hCmethod::newHCattr(aCattrBase* pACattr)  // builder 
{	
	return NULL; /* an error */  
}


//
// Get the Method Definition
// input: char pointer for storing method definition string. No memory is allocated yet
// return: length of method definiton string in charactors.
//
size_t hCmethod::getDef( char* &chDefinition, wchar_t* &wchDefinition )
{
	size_t nDefinitionLength = 0;

	if( chDefinition == NULL )
	{
        //USES_CONVERSION;
		CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
		if( pDevice )
		{
			CValueVarient vtDefinition{};
			if( pDevice->ReadMethodData( itemId, _T("Definition"), &vtDefinition) )
			{
				//method definition in wide character
				wchDefinition = new wchar_t[wcslen(CV_WSTR(&vtDefinition).c_str()) + 1];
				PS_Wcscpy(wchDefinition, wcslen(CV_WSTR(&vtDefinition).c_str()) + 1, CV_WSTR(&vtDefinition).c_str());		//returned string with NULL termination

				//method definition in narrow character
				chDefinition = w2c(CV_WSTR(&vtDefinition).c_str());							//returned string with NULL termination
				nDefinitionLength = strlen(chDefinition);
			}
		}
	}

	return nDefinitionLength;
}

//
// Get the Label of the method
//
RETURNCODE hCmethod::Label(wstring& retStr) 
{ 
	RETURNCODE  rc = SUCCESS;

	retStr.clear();
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtValue{};
		rc = pDevice->ReadParameterData2( &vtValue, _T("Label"), 0, 0, this->itemId, 0 );
		if( rc == BLTIN_SUCCESS )
		{			
			retStr = CV_WSTR(&vtValue);
		}
	}
	return rc;
}

//
// This is the return type of the method
//
RETURNCODE hCmethod::getMethodType(hCmethodParam& mP)
{
	RETURNCODE retVal = FAILURE;

	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtType{};
		if( pDevice->ReadMethodData( itemId, _T("Type"), &vtType) )
		{
			if( vtType.vTagType == ValueTagType::CVT_I4 )
			{
				mP.type = (methodVarType_t)CV_I4(&vtType);
				retVal = SUCCESS;
			}
		}
	}
	return retVal;
}

//
// This gets the arguments to the method
//
RETURNCODE hCmethod::getMethodParams(ParamList_t& pL)
{
//	USES_CONVERSION;
	pL.clear();
	CMethodInterpreterDevice *pDevice = (CMethodInterpreterDevice *)devPtr();
	if( pDevice )
	{
		CValueVarient vtParams{};
		if( pDevice->ReadMethodData( itemId, _T("Params"), &vtParams) )
		{
			CStdString sParams;
			if( vtParams.vTagType == ValueTagType::CVT_WSTR )
			{
				sParams = CV_WSTR(&vtParams);

				while( sParams.GetLength() )
				{
					long lFirstEndPos = sParams.Find( _T(";") );
					if( lFirstEndPos > 0 )
					{
						long lSecondEndPos = sParams.Find( _T(";"), lFirstEndPos+1 );
						if( lSecondEndPos > 0 )
						{
							long lThirdEndPos = sParams.Find( _T(";"), lSecondEndPos+1 );
							if( lThirdEndPos > 0 )
							{
								CStdString sType = sParams.Mid( 0, lFirstEndPos );
								CStdString sModifiers = sParams.Mid( lFirstEndPos+1, lSecondEndPos-lFirstEndPos-1 );
								CStdString sName = sParams.Mid( lSecondEndPos+1, lThirdEndPos-lSecondEndPos-1 );
								sParams = sParams.Mid( lThirdEndPos+1, sParams.GetLength() - lThirdEndPos );
								char szName[MAX_PATH+1]={0};
								
								wcstombs( szName, sName, MAX_PATH );
								hCmethodParam param;
                                param.type = (methodVarType_t)ttoi( sType );
								if(param.type  == methodVarDDItem)
								{//this makes assumptions that if we are methodVarDDItem methodTypeFlag_t is methodType_NonFlaged
									param.flags = methodType_Reference;
								}
								else
								{
                                    param.flags = (methodTypeFlag_t)ttoi( sModifiers );

									//if this is value array, this is also reference for output
									if (param.flags & methodType_Array)
									{
										param.flags = (methodTypeFlag_t)(param.flags | (int)methodType_Reference);
									}
								}
								param.name = szName;
								pL.push_back( param );
							}
							else
							{
								break;
							}
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	return SUCCESS; // a normal conclusion
}



