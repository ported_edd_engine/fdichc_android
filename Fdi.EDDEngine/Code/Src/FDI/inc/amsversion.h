#ifndef _AMSVERSION_H
#define _AMSVERSION_H

// The _WIN32WINNT variable defines the development platform for the MSSDK to the complier.
// The WINVER variable defines the DeltaV product platform compatibility.
// The variable values are the same for each variable.  Any value is allowed on any machine.
// development platform.  Therefore, NT 4.0 can be defined on a Windows XP build machine.
// Combinations are allowed.
// o The Values are: NT 4.0(0x0400), Windows 2000(0x0500), Windows XP(0x0501)
// o NT 4.0 Development Platform (0x0400) Compatible with NT 4.0, Windows 2000(0x0500), Windows XP(0x0501)
// o Windows 2000 Development Platform (0x0500) Compatible with Windows 2000(0x0500), Windows XP(0x0501)
// o Windows XP Development Platform (0x0501) Compatible with Windows XP(0x0501)

#if defined (_WIN32_WINNT)
	#undef _WIN32_WINNT
#endif

#define _WIN32_WINNT 0x0502

#if defined (WINVER)
	#undef WINVER
#endif

#define WINVER 0x0502

// This hides the security warnings (strcpy, etc) from showing in the compiler
#define _CRT_SECURE_NO_WARNINGS 1
// This disables POSIX warnings
#define _CRT_NONSTDC_NO_WARNINGS 1
// Disables _stprintf warnings 
#define _CRT_NON_CONFORMING_SWPRINTFS 1
// Disable the warning for conditional expression is constant
#pragma warning(disable: 4127)

//
//   This file contains the define that is used to keep track of the version
//   number of the server.   It should be modified everytime a server is built.
//   The current format of the version number is:
//
//             a.b.c.d
//
//                  where          a - is the major release number (1,2,3, etc).
//                                 b - is the minor revision number (1,2,3, etc).
//                                 c - indicates Production Builds
//											development(300), alpha(200), beta(100), release(0) 
//										or Debug Builds
//											development(800), alpha(700), beta(600), release(500) 
//                                 d - is the build number (1,2,3, etc.).

#define AMS_MAJOR_VERSION 11
#define AMS_MINOR_VERSION 1

#ifdef NDEBUG
	#define SERVER_VERSION _T("11.1.001.045")
	#define AMS_MAINTENANCE_VERSION 001
#else
	#define SERVER_VERSION _T("11.1.501.045")
	#define AMS_MAINTENANCE_VERSION 501
#endif

#define AMS_BUILD_VERSION 045

#define AMS_SRV_CLASS_NAME		_T("AMS HART Server")			// Window class name
#define AMS_SRV_PS_NAME			_T("AMS Plant Server  ")	// PS window name
#define AMS_SRV_FS_NAME			_T("AMS File Server  ")		// FS window name
#define AMS_SRV_GENEX_NAME		_T("AMS GenEx Server  ")	// Generic Export window name
#define AMS_SRV_CONN_NAME		_T("AMS Connection Server  ")	// Connection Server window name
#define AMS_AT_TRANS_BROKER		_T("AMS Transaction Broker")
#define AMS_AT_DEVICE_BROKER	_T("AMS Device Broker")
#define AMS_HSE_SERVER      	_T("AMS HSE Server")
#define AMS_SRV_LIC_NAME		_T("AMS License Server")
#define AMS_SRV_OPC_NAME		_T("AMS OPC Server")
#define AMS_SRV_DA_NAME			_T("AMS Device Alert Server")

// Global mutex names for terminal services support
#define AMS_SRV_GLOBAL_PS_NAME					_T("Global/AMS Plant Server  ")
#define AMS_SRV_GLOBAL_FS_NAME					_T("Global/AMS File Server  ")
#define AMS_NETWORK_CONFIGURATION_GLOBAL		_T("Global/AMS_NETWORK_CONFIGURATION")

// Local mutex names for applications that can run in multiple terminal server sessions
#define AMS_SCAN_MANAGER_LOCAL					_T("Local/AMSScanMngr.AmsScanManager")

// Event name to be used by App to tell that license server is started
#define AMS_LICENSE_SRV_STARTED	_T("LicenseServerEvent")

// mutex name to tell app and server that network configuration is running
#define AMS_NETWORK_CONFIGURATION  _T("AMS_NETWORK_CONFIGURATION")
#define AMS_MAIN_APPLICATION   _T("FMSMDIFrame")
#define AMS_FF_SRV_CLASS_NAME _T("AMS FF Server")
#define AMS_PB_SRV_CLASS_NAME _T("AMS PB Server")
#define AMS_SCANNING_DEVICES _T("AMS SCANNING DEVICES") // prevents launching UI while scanning

#endif
