/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	@(#)cm_lib.h	30.4  30  14 Nov 1996
 *
 *	cm_lib.h - Connection Manager Library Definitions
 *
 *	This file contains all interface definitions for
 *	the Connection Manager Library.
 */


#pragma once

#include <string.h>     /* ANSI */
#include <stdio.h>

#include "AmsGenFiles/std.h"
#include <ServerProjects/rtn_code.h>
#include <AMSDds/DDLDEFS.H>
#include "AmsGenFiles/env_info.h"
#include <AMSDds/od_defs.h>
#include "comm.h"
#include "DDOD_Header.h"
#include "nsEDDEngine/FFBlockInfo.h"


/*
 *	Connection Manager Modes
 */

#define	CMM_ON_LINE		1
#define	CMM_OFF_LINE	2

#define DD_MAX_PATH 260


typedef struct  {
	ITEM_ID		item_id;
	char*		item_name_ptr;
} SYMTBL;

typedef struct {
	SYMTBL*		symidtbl;
	SYMTBL*		symnametbl;
	int			numelem;
} SYMINFO;







/*
 *	Connection Manager Typedefs
 */

typedef int		DEVICE_TYPE_HANDLE;
typedef int		NETWORK_HANDLE;
typedef int		ROD_HANDLE;
typedef int		FLAT_HANDLE;

typedef UINT8		BOOLEAN;

typedef struct {
	UINT8	password;
	UINT8	groups;
	UINT16	rights;
} ACCESS;

typedef struct	{
	BOOLEAN			ram_rom_flag;
	UINT8			name_length;
	BOOLEAN			access_protection_flag;
	UINT16			version;
	UINT16			stod_object_count;
	OBJECT_INDEX	sod_first_index;
	UINT16			sod_object_count;
	OBJECT_INDEX	dvod_first_index;
	UINT16			dvod_object_count;
	OBJECT_INDEX	dpod_first_index;
	UINT16			dpod_object_count;
} OD_DESCRIPTION_SPECIFIC;

typedef struct {
	UINT32			size;
	UINT32			local_address;
	UINT8			state;
	UINT8			upload_state;
	UINT8			usage;
} DOMAIN_SPECIFIC;

typedef struct {
	UINT8			 domain_count;
	BOOLEAN			 deleteable_flag;
	BOOLEAN			 reuseable_flag;
	UINT8			 state;	
	OBJECT_INDEX	*domain_list;
} PROGRAM_INVOCATION_SPECIFIC;

typedef struct {
	OBJECT_INDEX	data;
	UINT8			size;
	BOOLEAN			enabled_flag;
} EVENT_SPECIFIC;

typedef struct {
	UINT8			 symbol_size;
	char			*symbol;
} DATA_TYPE_SPECIFIC;

typedef struct {
	UINT8			 elem_count;
	OBJECT_INDEX	*type_list;
	UINT8			*size_list;
} DATA_TYPE_STRUCTURE_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
	UINT8			size;
} SIMPLE_VARIABLE_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
	UINT8			size;
	UINT8			elem_count;
} ARRAY_SPECIFIC;

typedef struct {
	OBJECT_INDEX	type;
} RECORD_SPECIFIC;

typedef struct {
	UINT8			 elem_count;
	BOOLEAN			 deleteable_flag;
	OBJECT_INDEX	*list;
} VARIABLE_LIST_SPECIFIC;

typedef union {
	OD_DESCRIPTION_SPECIFIC			od_description;
	DOMAIN_SPECIFIC					domain;
	PROGRAM_INVOCATION_SPECIFIC		program_invocation;
	EVENT_SPECIFIC					event;
	DATA_TYPE_SPECIFIC				data_type;
	DATA_TYPE_STRUCTURE_SPECIFIC	data_type_structure;
	SIMPLE_VARIABLE_SPECIFIC		simple_variable;
	ARRAY_SPECIFIC					array;
	RECORD_SPECIFIC					record;
	VARIABLE_LIST_SPECIFIC			variable_list;
} OBJECT_SPECIFIC;

typedef struct {
	OBJECT_INDEX		 index;
	UINT8				 code;
	char				*name;
	ACCESS				 access;
	OBJECT_SPECIFIC		*specific;
	UINT8				*extension;
	UINT8				*value;
} OBJECT;

typedef struct {
	ITEM_ID					block_id ;
	BLOCK_HANDLE			block_handle ;
} CM_BLOCK_LIST_ELEM;

typedef struct {
	int						count ;
	CM_BLOCK_LIST_ELEM		*list ;
} CM_BLOCK_LIST ;

typedef struct {
	ITEM_ID					block_type_id ;
} CM_BLOCK_TYPE_LIST_ELEM ;

typedef struct {
	int						count ;
	CM_BLOCK_TYPE_LIST_ELEM	*list ;
} CM_BLOCK_TYPE_LIST ;

typedef struct {
	unsigned short		cm_type;
	unsigned short		cm_size;
	union {
		BOOLEAN			cm_boolean;
		float			cm_float;
		double			cm_double;
		unsigned long	cm_ulong;
		long			cm_long;
		unsigned long	cm_time_value[2];
		char			*cm_stream;
        char			cm_ca[8];
	} cm_val;
} CM_VALUE;

typedef struct {
	int			cmvl_count;
	CM_VALUE	*cmvl_list;
} CM_VALUE_LIST;

typedef int		CREF;

typedef struct {
	UINT32          ddid_manufacturer;
	UINT16          ddid_device_type;
	UINT8           ddid_device_rev;
	UINT8           ddid_dd_rev;
    wchar_t			dd_path[DD_MAX_PATH];
} DD_DEVICE_ID;

typedef struct {
	int		(*build_dd_dev_tbl_fn_ptr) (ENV_INFO *, DEVICE_HANDLE);
	int		(*build_dd_blk_tbl_fn_ptr) (ENV_INFO *, BLOCK_HANDLE);
	int		(*remove_dd_dev_tbl_fn_ptr) (ENV_INFO *, DEVICE_HANDLE);
	int		(*remove_dd_blk_tbl_fn_ptr) (ENV_INFO *, BLOCK_HANDLE);
} DDS_TBL_FN_PTRS;

/* Active Block Table */


typedef struct {
	char           	*tag;
	OBJECT_INDEX	op_index ;
	unsigned int   	param_count;
	UINT32			 dd_blk_id;
	int				 dd_blk_tbl_offset;
	void			*app_info;
	DEVICE_HANDLE  	 active_dev_tbl_offset;
	nsEDDEngine::CCharacteristicRecord	blk_char_record;
	nsEDDEngine::BLOCK_TYPE		block_type;
	int				 usage;
} ACTIVE_BLK_TBL_ELEM;

typedef struct {
	int						  count;
	ACTIVE_BLK_TBL_ELEM		**list;
} ACTIVE_BLK_TBL;


/* Active Device Table */


typedef	int ADDR_TYPE ;
typedef	int BAUD_RATE ;

typedef struct {
	UINT8			mfrid ;
	UINT8			mfr_dev_type ;
	UINT8			device_id[3] ;
} LONG_ADDR ;


typedef	struct {
	ADDR_TYPE		addr_type ;
	LONG_ADDR		long_addr ;
	int				short_addr ;
} DEVICE_ADDRESS ;


typedef struct {
	CREF            opod_cref;
	CREF            ddod_cref;
	CREF            mib_cref;
} ADT_SPEC_A ;


typedef union {

	ADT_SPEC_A		adt_spec_a ;


} ADT_NET_SPEC ;
	
typedef struct {
	UINT16		 		station_address ;
	NETWORK_HANDLE		 network;
    wchar_t				 DeviceName[64];
	ADT_NET_SPEC		 adt_net_spec ;
	ROD_HANDLE			 oprod_handle;
	void				*app_info;
	DEVICE_TYPE_HANDLE	 active_dev_type_tbl_offset;
	int					 usage;
} ACTIVE_DEV_TBL_ELEM;

typedef struct {
	int						  count;
	ACTIVE_DEV_TBL_ELEM		**list;
} ACTIVE_DEV_TBL;


/* Active Device Type Table */

typedef struct {
	DD_DEVICE_ID    dd_device_id;
	ROD_HANDLE      rod_handle;
	void           *dd_dev_tbls;
	SYMINFO		   	*syminfo;
	int				usage;
} ACTIVE_DEV_TYPE_TBL_ELEM;

typedef struct {
	int							  count;
	ACTIVE_DEV_TYPE_TBL_ELEM	**list;
} ACTIVE_DEV_TYPE_TBL;


/* Network Table */

typedef int		NET_TYPE;
#define	NT_OFF_LINE		0	/* Off-Line Mode */

#define NT_A			1	/* Type A Network */
#define NT_A_SIM		2	/* Simulated Type A Network */


#define NT_ETHER		5	/* Ethernet Network (connected to FF) */

#define NT_CONTROL		6	/* Control Network */

typedef union {
	struct {
		UINT32          ip_address;
		UINT16          socket_address;
		UINT16          board;
	} ether_network;
	struct {
		UINT16          controller_address;
		UINT16          controller_port;
		UINT16          controller_device_address;
	} control_network;
} NET_ROUTE;

#define DF_FF				1	/* FF Device Family type */


typedef struct {
	NET_TYPE	 network_type;
	NET_ROUTE	 network_route;
	int			 device_family;
	int			 config_file_id;
} NETWORK_TBL_ELEM;

typedef struct {
	int					 count;
	NETWORK_TBL_ELEM	*list;
} NETWORK_TBL;


typedef struct {
	int				network;
	NET_TYPE        network_type;
	NET_ROUTE		network_route;
	UINT16          station_address;
	DD_DEVICE_ID    dd_device_id;
} IDENTIFY_CNF_A ;



typedef	struct {
	int						type ;
	union {

		IDENTIFY_CNF_A 	cnf_a ;


	} identify_cnf ;
} IDENTIFY_CNF ;


typedef struct {
	OBJECT_INDEX	op_index;
	int				network;
	NET_TYPE        network_type;
	NET_ROUTE		network_route;
	UINT16          station_address;
    wchar_t			DeviceName[64];
	DD_DEVICE_ID    dd_device_id;
} FIND_CNF_A ;

typedef	struct {
	ADDR_TYPE		addr_type ;
	LONG_ADDR		long_addr ;
	int				short_addr ;
} FF_ADDRESS ;


typedef struct {
	int					type ;
	union {


		FIND_CNF_A	find_cnf_a ;

	} find_cnf ;
} FIND_CNF;

#define CT_OPOD		1
#define CT_DDOD		2
#define CT_MIB		3

typedef int COMM_TYPE;



/*
 *	Connection Manager Tables
 */

/*
 *	Connection Manager Convenience Macros
 *
 *	The following macros provide access to Active Block, Active Device,
 *	Active Device Type, and Network information.  They may need
 *	replacement for systems that do not have direct access to the
 *	network.
 */

/*
 *	Function Prototypes that may be used in the Convenience Macros.
 */

/* Definitions for symbol file search mode.*/
#define	SM_EXPLICIT_DDREV	1
#define	SM_LATEST_DDREV		2




int read_sym_file(const wchar_t* pReleasePath, /*DD_DEVICE_ID*,*/ SYMINFO*);

void free_symtbl(SYMINFO*);

int server_item_id_to_name(SYMINFO*, ITEM_ID, wchar_t *, int outbuf_size );

int server_item_name_to_id(SYMINFO*, wchar_t *, ITEM_ID *);
