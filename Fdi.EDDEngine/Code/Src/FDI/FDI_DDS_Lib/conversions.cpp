#include "stdafx.h"
#include "conversions.h"

/*********************************************************************
 *
 *	Name: FDI_ConvSTRING::CopyTo
 *	ShortDesc: copy to string
 *
 *	Description:
 *		copy to string
 *
 *	Inputs:
 *		pEDDString:		source string
 *		pDDSString:		destination string
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvSTRING::CopyTo(::nsEDDEngine::STRING *pEDDString, ::STRING *pDDSString)
{
	if( pDDSString->str != NULL )	// If destination string is not empty
	{
		if (pDDSString->flags == tag_STRING::Flags::FREE_STRING)
		{
			free (pDDSString->str );	// Free it
			pDDSString->str = nullptr;
		}
	}

	// Intialize destination string
	pDDSString->str   = nullptr;
	pDDSString->len   = 0;
	pDDSString->flags = tag_STRING::Flags::NOT_SET;

	// Allocate memory for new string
	pDDSString->str = (wchar_t *)malloc((pEDDString->length()+1)*sizeof(wchar_t));

	if(!pDDSString->str)
		return;

    // PS_Wcscpy will null terminate the string
	memset(pDDSString->str, 0, (pDDSString->len+1)*sizeof(wchar_t));

	if(pEDDString->c_str())
	{
	    PS_Wcsncpy( pDDSString->str, pEDDString->length()+1, pEDDString->c_str(), _TRUNCATE );
	}

    pDDSString->len = (unsigned short)wcslen(pDDSString->str);

	pDDSString->flags = tag_STRING::Flags::FREE_STRING;

}

/*********************************************************************
 *
 *	Name: FDI_ConvSTRING::CopyFrom
 *	ShortDesc: copy STRING
 *
 *	Description:
 *		copy STRING
 *
 *	Inputs:
 *		pDDSString:		source 
 *		pEDDString:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvSTRING::CopyFrom(::STRING *pDDSString, ::nsEDDEngine::STRING *pEDDString)
{
	if( pEDDString->c_str() != NULL )
    {
        ASSERT(0);
    }
   
	if(pDDSString->str == NULL)
	{
		pEDDString = NULL;
	}
	else
	{
	   *pEDDString = pDDSString->str;
	}
}


class FDI_ConvEXPR_VALUE : public nsEDDEngine::EXPR_VALUE
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvEXPR_VALUE::CopyFrom
	 *	ShortDesc: copy EXPR_VALUE
	 *
	 *	Description:
	 *		copy EXPR_VALUE
	 *
	 *	Inputs:
	 *		pDDSExprValue:		source 
	 *		pEDDExprValue:		destination 
	 *		etype:				expr value type	
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom(::EXPR::tag_EXPR_Union *pDDSExprValue, ::nsEDDEngine::EXPR_VALUE *pEDDExprValue, nsEDDEngine::EXPR::ExprValueType etype )
	{
		switch(etype)
		{
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_NONE		:
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_FLOAT		:
			pEDDExprValue->f = pDDSExprValue->f;
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_DOUBLE	:
			pEDDExprValue->d = pDDSExprValue->d;
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_UNSIGNED  :
			pEDDExprValue->u = pDDSExprValue->u;
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_INTEGER	:
			pEDDExprValue->i = pDDSExprValue->i;
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_STRING	:
			::FDI_ConvSTRING::CopyFrom( &pDDSExprValue->s, &pEDDExprValue->s );
			break;
		case nsEDDEngine::EXPR::ExprValueType::EXPR_TYPE_BINARY	: 
			pEDDExprValue->b.assign(pDDSExprValue->b.ptr, pDDSExprValue->b.len); 
			break;
		default:
			break;
		}
	};	
};

/*********************************************************************
 *
 *	Name: FDI_ConvEXPR::CopyFrom
 *	ShortDesc: copy EXPR
 *
 *	Description:
 *		copy EXPR
 *
 *	Inputs:
 *		pDDSExpr:		source 
 *		pEDDExpr:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvEXPR::CopyFrom( ::EXPR *pDDSExpr, ::nsEDDEngine::EXPR * pEDDExpr )
{
	switch(pDDSExpr->type)
	{
	case DDS_TYPE_UNUSED:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_NONE;
		break;

	case INTEGER:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_INTEGER;
		break;

	case UNSIGNED:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_UNSIGNED;
		break;

    case FLOATG_PT:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_FLOAT;
		break;

    case DOUBLEG_PT:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_DOUBLE;
		break;

	case ASCII:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_STRING;
		break;

	case BITSTRING:
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_BINARY;
		break;

	default:
		ASSERT(0);
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_NONE;
		break;
	}

	pEDDExpr->size = pDDSExpr->size;
	::FDI_ConvEXPR_VALUE::CopyFrom( &pDDSExpr->val, &pEDDExpr->val, pEDDExpr->eType );
}

/*********************************************************************
 *
 *	Name: FDI_ConvEXPR::CopyTo
 *	ShortDesc: copy EXPR
 *
 *	Description:
 *		copy EXPR
 *
 *	Inputs:
 *		pEDDExpr:		source 
 *		pDDSExpr:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvEXPR::CopyTo(::nsEDDEngine::EXPR * pEDDExpr, ::EXPR *pDDSExpr)
{
	switch(pEDDExpr->eType)
	{
	case ExprValueType::EXPR_TYPE_NONE:
		pDDSExpr->type = DDS_TYPE_UNUSED;
		break;

	case ExprValueType::EXPR_TYPE_INTEGER:
		pDDSExpr->type = INTEGER;
		pDDSExpr->val.i = (long)pEDDExpr->val.i;
		break;

	case ExprValueType::EXPR_TYPE_UNSIGNED:
		pDDSExpr->type = UNSIGNED;
		pDDSExpr->val.u = (unsigned long)pEDDExpr->val.u;
		break;

	case ExprValueType::EXPR_TYPE_FLOAT:
        pDDSExpr->type = FLOATG_PT;
		pDDSExpr->val.f = pEDDExpr->val.f;
		break;

	case ExprValueType::EXPR_TYPE_DOUBLE:
		pDDSExpr->type = DOUBLE;
		pDDSExpr->val.d = pEDDExpr->val.d;
		break;

	case ExprValueType::EXPR_TYPE_STRING:
		pDDSExpr->type = ASCII;
		::FDI_ConvSTRING::CopyTo( &pEDDExpr->val.s, &pDDSExpr->val.s );
		break;

	case ExprValueType::EXPR_TYPE_BINARY: 
		pDDSExpr->type = BITSTRING;		
		pDDSExpr->val.b.len = pEDDExpr->val.b.length();
		pDDSExpr->val.b.ptr = (uchar *) malloc( pDDSExpr->val.b.len );
		memcpy(pDDSExpr->val.b.ptr, pEDDExpr->val.b.ptr(), pEDDExpr->val.b.length());
		break;

	default:
		ASSERT(0);
		pEDDExpr->eType = ExprValueType::EXPR_TYPE_NONE;
		break;
	}

	pDDSExpr->size = pEDDExpr->size;
}

class FDI_ConvRESOLVE_INFO : public nsEDDEngine::RESOLVE_INFO
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvRESOLVE_INFO::CopyFrom
	 *	ShortDesc: copy RESOLVE_INFO
	 *
	 *	Description:
	 *		copy RESOLVE_INFO
	 *
	 *	Inputs:
	 *		pDDSResolveInfo:		source 
	 *		pEDDResolveInfo:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::RESOLVE_INFO *pDDSResolveInfo, ::nsEDDEngine::RESOLVE_INFO *pEDDResolveInfo )
	{
		pEDDResolveInfo->type = (nsEDDEngine::ITEM_TYPE)pDDSResolveInfo->type;
		pEDDResolveInfo->id = pDDSResolveInfo->id;
		pEDDResolveInfo->element = pDDSResolveInfo->element;
	};
};


class FDI_ConvOP_REF_INFO : public nsEDDEngine::OP_REF_INFO
{
public:
	static void CopyFrom( ::OP_REF_INFO *pDDSOpRefInfo, nsEDDEngine::OP_REF_INFO *pEDDOpRefInfo )
	{
		pEDDOpRefInfo->id		= pDDSOpRefInfo->id;
		pEDDOpRefInfo->member	= pDDSOpRefInfo->member;
		pEDDOpRefInfo->type		= (nsEDDEngine::ITEM_TYPE)pDDSOpRefInfo->type;
	};
};
/*********************************************************************
 *
 *	Name:  FDI_ConvOP_REF_TRAIL::CopyFrom
 *	ShortDesc: copy OP_REF_TRAIL
 *
 *	Description:
 *		copy OP_REF_TRAIL
 *
 *	Inputs:
 *		pDDSOpRefTrail:		source 
 *		pEDDOpRefTrail:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvOP_REF_TRAIL::CopyFrom( ::OP_REF_TRAIL *pDDSOpRefTrail, ::nsEDDEngine::OP_REF_TRAIL *pEDDOpRefTrail )
{
	pEDDOpRefTrail->desc_id			= pDDSOpRefTrail->desc_id;
	pEDDOpRefTrail->desc_type		= (::nsEDDEngine::ITEM_TYPE)pDDSOpRefTrail->desc_type;
	pEDDOpRefTrail->desc_bit_mask	= pDDSOpRefTrail->bit_mask;

	FDI_ConvEXPR::CopyFrom( &pDDSOpRefTrail->expr, &pEDDOpRefTrail->expr );
	pEDDOpRefTrail->block_instance = pDDSOpRefTrail->block_instance;
	

	if( pDDSOpRefTrail->op_ref_type == STANDARD_TYPE )
	{
		pEDDOpRefTrail->op_ref_type = nsEDDEngine::STANDARD_TYPE;

		FDI_ConvOP_REF_INFO::CopyFrom( &pDDSOpRefTrail->op_info, &pEDDOpRefTrail->op_info );
		
		pEDDOpRefTrail->op_info_list.count = 0;
		pEDDOpRefTrail->op_info_list.list = nullptr;
	}
	else
	{
		pEDDOpRefTrail->op_ref_type = nsEDDEngine::COMPLEX_TYPE;

		pEDDOpRefTrail->op_info_list.count = pDDSOpRefTrail->op_info_list.count;
		pEDDOpRefTrail->op_info_list.list = new nsEDDEngine::OP_REF_INFO[pEDDOpRefTrail->op_info_list.count];

		for( int i = 0; i < pEDDOpRefTrail->op_info_list.count; i++ )
		{
			FDI_ConvOP_REF_INFO::CopyFrom( &pDDSOpRefTrail->op_info_list.list[i], &pEDDOpRefTrail->op_info_list.list[i] );
		}
	}

	

	if(pDDSOpRefTrail->trail_count != 0)
	{
		pEDDOpRefTrail->trail_count = pDDSOpRefTrail->trail_count;
		pEDDOpRefTrail->trail_limit = pDDSOpRefTrail->trail_count;

		pEDDOpRefTrail->trail = new ::nsEDDEngine::RESOLVE_INFO[pEDDOpRefTrail->trail_count];

		for(int i=0; i < pEDDOpRefTrail->trail_count;i++)
		{
			FDI_ConvRESOLVE_INFO::CopyFrom( &pDDSOpRefTrail->trail[i], &pEDDOpRefTrail->trail[i] );
		}
	}
}

/*********************************************************************
 *
 *	Name: FDI_ConvDESC_REF::CopyFrom
 *	ShortDesc: copy DESC_REF
 *
 *	Description:
 *		copy DESC_REF
 *
 *	Inputs:
 *		pDDSDescRef:		source 
 *		pEDDDescRef:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvDESC_REF::CopyFrom( ::DESC_REF *pDDSDescRef, ::nsEDDEngine::DESC_REF *pEDDDescRef )
{
	pEDDDescRef->id = pDDSDescRef->id;
	pEDDDescRef->type = (nsEDDEngine::ITEM_TYPE)pDDSDescRef->type;
}



/*********************************************************************
 *
 *	Name: FDI_ConvDESC_REF::CopyTo
 *	ShortDesc: copy DESC_REF
 *
 *	Description:
 *		copy DESC_REF
 *
 *	Inputs:
 *		pEDDDescRef:		source 
 *		pDDSDescRef:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvDESC_REF::CopyTo(::nsEDDEngine::DESC_REF *pEDDDescRef, ::DESC_REF *pDDSDescRef )
{
	pDDSDescRef->id = pEDDDescRef->id;
	pDDSDescRef->type = (ITEM_TYPE)pEDDDescRef->type;
}

/*********************************************************************
 *
 *	Name: FDI_ConvRESPONSE_CODES::CopyFrom
 *	ShortDesc: copy RESPONSE_CODES
 *
 *	Description:
 *		copy RESPONSE_CODES
 *
 *	Inputs:
 *		pDDSResp_Codes:		source 
 *		pEDDResp_Codes:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvRESPONSE_CODES::CopyFrom( ::RESPONSE_CODES *pDDSResp_Codes, ::nsEDDEngine::RESPONSE_CODES *pEDDResp_Codes)
{
	pEDDResp_Codes->eType =(::nsEDDEngine::RESPONSE_CODES::ResponseCodesType) pDDSResp_Codes->type;
	pEDDResp_Codes->response_codes.resp_code_ref = pDDSResp_Codes->response_codes.resp_code_ref;
	pEDDResp_Codes->response_codes.resp_code_list.count = pDDSResp_Codes->response_codes.resp_code_list.count;
	pEDDResp_Codes->response_codes.resp_code_list.limit = pDDSResp_Codes->response_codes.resp_code_list.limit;
	pEDDResp_Codes->response_codes.resp_code_list.list = new ::nsEDDEngine::RESPONSE_CODE[pDDSResp_Codes->response_codes.resp_code_list.count];
	for(int i= 0; i< pEDDResp_Codes->response_codes.resp_code_list.count;i++)
	{
		if(pDDSResp_Codes->response_codes.resp_code_list.list[i].evaled & RS_DESC_EVALED)
		{
			pEDDResp_Codes->response_codes.resp_code_list.list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_DESC_EVALED; 
			::FDI_ConvSTRING::CopyFrom(&pDDSResp_Codes->response_codes.resp_code_list.list[i].desc,&pEDDResp_Codes->response_codes.resp_code_list.list[i].desc);
		}

		if(pDDSResp_Codes->response_codes.resp_code_list.list[i].evaled & RS_HELP_EVALED)
		{
			pEDDResp_Codes->response_codes.resp_code_list.list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_HELP_EVALED; 
			::FDI_ConvSTRING::CopyFrom(&pDDSResp_Codes->response_codes.resp_code_list.list[i].help,&pEDDResp_Codes->response_codes.resp_code_list.list[i].help);
		}

		if(pDDSResp_Codes->response_codes.resp_code_list.list[i].evaled & RS_TYPE_EVALED)
		{
			pEDDResp_Codes->response_codes.resp_code_list.list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_TYPE_EVALED; 
			pEDDResp_Codes->response_codes.resp_code_list.list[i].type = (nsEDDEngine::ResponseCodeType)pDDSResp_Codes->response_codes.resp_code_list.list[i].type;
		}

		if(pDDSResp_Codes->response_codes.resp_code_list.list[i].evaled & RS_VAL_EVALED)
		{
			pEDDResp_Codes->response_codes.resp_code_list.list[i].evaled |= nsEDDEngine::RESPONSE_CODE::FDI_RS_VAL_EVALED; 
			pEDDResp_Codes->response_codes.resp_code_list.list[i].val = pDDSResp_Codes->response_codes.resp_code_list.list[i].val;
		}
		
	}
		
};


class FDI_ConvRESPONSE_CODE : public ::nsEDDEngine::RESPONSE_CODE
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvRESPONSE_CODE::CopyFrom
	 *	ShortDesc: copy RESPONSE_CODE
	 *
	 *	Description:
	 *		copy RESPONSE_CODE
	 *
	 *	Inputs:
	 *		pDDSRespCode:		source 
	 *		pEDDRespCode:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::RESPONSE_CODE *pDDSRespCode, ::nsEDDEngine::RESPONSE_CODE *pEDDRespCode )
	{
		pEDDRespCode->evaled = (::nsEDDEngine::RESPONSE_CODE::ResponseCodeEvaled)pDDSRespCode->evaled;
		pEDDRespCode->val = pDDSRespCode->val;
		pEDDRespCode->type = (nsEDDEngine::ResponseCodeType)pDDSRespCode->type;
		::FDI_ConvSTRING::CopyFrom( &pDDSRespCode->desc, &pEDDRespCode->desc );
		::FDI_ConvSTRING::CopyFrom( &pDDSRespCode->help, &pEDDRespCode->help );
	};
};

/*********************************************************************
 *
 *	Name: FDI_ConvRESPONSE_CODE_LIST::CopyFrom
 *	ShortDesc: copy RESPONSE_CODE_LIST
 *
 *	Description:
 *		copy RESPONSE_CODE_LIST
 *
 *	Inputs:
 *		pDDSRespCodeList:		source 
 *		pEDDRespCodeList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvRESPONSE_CODE_LIST::CopyFrom(::RESPONSE_CODE_LIST *pDDSRespCodeList, ::nsEDDEngine::RESPONSE_CODE_LIST *pEDDRespCodeList )
{
	pEDDRespCodeList->count = pDDSRespCodeList->count;
	pEDDRespCodeList->limit = pDDSRespCodeList->limit;

	pEDDRespCodeList->list = new ::nsEDDEngine::RESPONSE_CODE[pEDDRespCodeList->count];

	for( int i = 0; i < pEDDRespCodeList->count; i++ )
	{
		FDI_ConvRESPONSE_CODE::CopyFrom( &pDDSRespCodeList->list[i], &pEDDRespCodeList->list[i] );
	}
};


class FDI_ConvMEMBER : public nsEDDEngine::MEMBER
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvMEMBER::CopyFrom
	 *	ShortDesc: copy MEMBER
	 *
	 *	Description:
	 *		copy MEMBER
	 *
	 *	Inputs:
	 *		pDDSMember:		source string
	 *		pEDDMember:		destination string
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::MEMBER *pDDSMember, ::nsEDDEngine::MEMBER *pEDDMember )
	{
		pEDDMember->evaled = (nsEDDEngine::MEMBER::MemberEvaled)pDDSMember->evaled;
		pEDDMember->name = pDDSMember->name;
		FDI_ConvDESC_REF::CopyFrom( &pDDSMember->ref, &pEDDMember->ref );
		::FDI_ConvSTRING::CopyFrom( &pDDSMember->desc, &pEDDMember->desc );
		::FDI_ConvSTRING::CopyFrom( &pDDSMember->help, &pEDDMember->help );

	};



};


/*********************************************************************
	*
	*	Name: FDI_ConvOPMEMBER::CopyFrom
	*	ShortDesc: copy MEMBER
	*
	*	Description:
	*		copy OP_MEMBER to an OP_MEMBER
	*
	*	Inputs:
	*		pDDSOpMember:		source string
	*		pEDDOpMember:		destination string
	*
	*	Returns:
	*		void
	*
	**********************************************************************/
class FDI_ConvOP_MEMBER : public nsEDDEngine::OP_MEMBER
{
public:
	static void CopyFrom( ::OP_MEMBER *pDDSOpMember, ::nsEDDEngine::OP_MEMBER *pEDDOpMember )
	{
		pEDDOpMember->evaled = (nsEDDEngine::OP_MEMBER::OpMemberEvaled)pDDSOpMember->evaled;
		pEDDOpMember->name = pDDSOpMember->name;
		FDI_ConvOP_REF_TRAIL::CopyFrom( &pDDSOpMember->ref, &pEDDOpMember->ref );
		::FDI_ConvSTRING::CopyFrom( &pDDSOpMember->desc, &pEDDOpMember->desc );
		::FDI_ConvSTRING::CopyFrom( &pDDSOpMember->help, &pEDDOpMember->help );

	};
};


/*********************************************************************
 *
 *	Name: FDI_ConvMEMBER_LIST::CopyFrom
 *	ShortDesc: copy MEMBER_LIST
 *
 *	Description:
 *		copy MEMBER_LIST
 *
 *	Inputs:
 *		pDDSMemList:		source 
 *		pEDDMemList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvMEMBER_LIST::CopyFrom( ::MEMBER_LIST *pDDSMemList, ::nsEDDEngine::MEMBER_LIST * pEDDMemList)
{
		pEDDMemList->count = pDDSMemList->count;
		pEDDMemList->limit = pDDSMemList->count;
		
		pEDDMemList->list = new ::nsEDDEngine::MEMBER[pDDSMemList->count];
		
		for(int i = 0; i < pEDDMemList->count; i++)
		{
			FDI_ConvMEMBER::CopyFrom( &pDDSMemList->list[i], &pEDDMemList->list[i] );
		}
}


/*********************************************************************
 *
 *	Name: FDI_ConvMEMBER_LIST::CopyFrom
 *	ShortDesc: copy OP_MEMBER_LIST
 *
 *	Description:
 *		copy OP_MEMBER_LIST
 *
 *	Inputs:
 *		pDDSMemList:		source 
 *		pEDDMemList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvOP_MEMBER_LIST::CopyFrom( ::OP_MEMBER_LIST *pDDSOpMemList, ::nsEDDEngine::OP_MEMBER_LIST * pEDDOpMemList)
{
		pEDDOpMemList->count = pDDSOpMemList->count;
		pEDDOpMemList->limit = pDDSOpMemList->count;
		
		pEDDOpMemList->list = new ::nsEDDEngine::OP_MEMBER[pDDSOpMemList->count];
		
		for(int i = 0; i < pEDDOpMemList->count; i++)
		{
			FDI_ConvOP_MEMBER::CopyFrom( &pDDSOpMemList->list[i], &pEDDOpMemList->list[i] );
		}
}

/*********************************************************************
 *
 *	Name: FDI_ConvITEM_ARRAY_ELEMENT::CopyFrom
 *	ShortDesc: copy ITEM_ARRAY_ELEMENT
 *
 *	Description:
 *		copy ITEM_ARRAY_ELEMENT
 *
 *	Inputs:
 *		pDDSIArrayElem:		source 
 *		pEDDIArrayElem:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

class FDI_ConvITEM_ARRAY_ELEMENT : public nsEDDEngine::ITEM_ARRAY_ELEMENT
{
public:
	static void CopyFrom( ::ITEM_ARRAY_ELEMENT *pDDSIArrayElem, ::nsEDDEngine::ITEM_ARRAY_ELEMENT *pEDDIArrayElem)
	{
		pEDDIArrayElem->index = pDDSIArrayElem->index;
		::FDI_ConvSTRING::CopyFrom( &pDDSIArrayElem->desc, &pEDDIArrayElem->desc );
		pEDDIArrayElem->evaled = (nsEDDEngine::ItemArrayElementEvaled)pDDSIArrayElem->evaled;
		::FDI_ConvSTRING::CopyFrom( &pDDSIArrayElem->help, &pEDDIArrayElem->help );
		::FDI_ConvDESC_REF::CopyFrom( &pDDSIArrayElem->ref, &pEDDIArrayElem->ref ); 
	};

};

/*********************************************************************
 *
 *	Name: FDI_ConvITEM_ARRAY_ELEMENT_LIST::CopyFrom
 *	ShortDesc: copy ITEM_ARRAY_ELEMENT_LIST
 *
 *	Description:
 *		copy ITEM_ARRAY_ELEMENT_LIST
 *
 *	Inputs:
 *		pDDSIArrElemList:		source 
 *		pEDDIArrElemList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvITEM_ARRAY_ELEMENT_LIST::CopyFrom(::ITEM_ARRAY_ELEMENT_LIST *pDDSIArrElemList, ::nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST *pEDDIArrElemList)
{
	pEDDIArrElemList->count = pDDSIArrElemList->count;
	pEDDIArrElemList->limit = pDDSIArrElemList->limit;

	pEDDIArrElemList->list = new nsEDDEngine::ITEM_ARRAY_ELEMENT[pEDDIArrElemList->count];

	for( int i = 0; i < pEDDIArrElemList->count; i++ )
	{
		FDI_ConvITEM_ARRAY_ELEMENT::CopyFrom( &pDDSIArrElemList->list[i], &pEDDIArrElemList->list[i] );
	}

}



class FDI_ConvDATA_ITEM_VALUE : public nsEDDEngine::DATA_ITEM_VALUE
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvDATA_ITEM_VALUE::CopyFrom
	 *	ShortDesc: copy DATA_ITEM_VALUE
	 *
	 *	Description:
	 *		copy DATA_ITEM_VALUE
	 *
	 *	Inputs:
	 *		pDDSDataItemVal:		source 
	 *		pEDDDataItemVal:		destination 
	 *		type:					data type
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::DATA_ITEM::tag_DATA_ITEM_Union *pDDSDataItemVal, ::nsEDDEngine::DATA_ITEM_VALUE *pEDDDataItemVal, ::nsEDDEngine::DataItemType type)
	{
		switch( type )
		{
			case ::nsEDDEngine::DataItemType::FDI_DATA_CONSTANT:
				pEDDDataItemVal->iconst = pDDSDataItemVal->iconst;
				break;
			case ::nsEDDEngine::DataItemType::FDI_DATA_UNSIGNED:
				pEDDDataItemVal->uconst = pDDSDataItemVal->uconst;
				break;
			case ::nsEDDEngine::DataItemType::FDI_DATA_FLOATING:
				pEDDDataItemVal->fconst = pDDSDataItemVal->fconst;
				break;
			case ::nsEDDEngine::DataItemType::FDI_DATA_DOUBLE:
				pEDDDataItemVal->dconst = pDDSDataItemVal->dconst;
				break;
			case ::nsEDDEngine::DataItemType::FDI_DATA_REFERENCE:
				FDI_ConvOP_REF_TRAIL::CopyFrom( &pDDSDataItemVal->ref, &pEDDDataItemVal->ref );
				break;
			case ::nsEDDEngine::DataItemType::FDI_DATA_REF_FLAGS:
			case ::nsEDDEngine::DataItemType::FDI_DATA_REF_FLAGS_WIDTH:
			case ::nsEDDEngine::DataItemType::FDI_DATA_REF_WIDTH:
			case ::nsEDDEngine::DataItemType::FDI_DATA_STRING:
			default:
				break;
		}
	};
};



class FDI_ConvDATA_ITEM : public nsEDDEngine::DATA_ITEM
{
public:

	/*********************************************************************
	 *
	 *	Name: FDI_ConvDATA_ITEM::CopyFrom
	 *	ShortDesc: copy DATA_ITEM
	 *
	 *	Description:
	 *		copy DATA_ITEM
	 *
	 *	Inputs:
	 *		pDDSDataItem:		source 
	 *		pEDDDataItem:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/

	static void CopyFrom( ::DATA_ITEM *pDDSDataItem, ::nsEDDEngine::DATA_ITEM *pEDDDataItem )
	{
		pEDDDataItem->type = (::nsEDDEngine::DataItemType)pDDSDataItem->type;
		pEDDDataItem->flags = pDDSDataItem->flags;
		pEDDDataItem->width = pDDSDataItem->width;
		pEDDDataItem->data_item_mask = pDDSDataItem->data_item_mask;

		FDI_ConvDATA_ITEM_VALUE::CopyFrom( &pDDSDataItem->data, &pEDDDataItem->data, pEDDDataItem->type );
	};

};


/*********************************************************************
 *
 *	Name: FDI_ConvDATA_ITEM_LIST::CopyFrom
 *	ShortDesc: copy DATA_ITEM_LIST
 *
 *	Description:
 *		copy DATA_ITEM_LIST
 *
 *	Inputs:
 *		pDDSDataItemList:		source 
 *		pEDDDataItemList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvDATA_ITEM_LIST::CopyFrom( ::DATA_ITEM_LIST *pDDSDataItemList, ::nsEDDEngine::DATA_ITEM_LIST *pEDDDataItemList )
{
	pEDDDataItemList->count = pDDSDataItemList->count;
	pEDDDataItemList->limit = pDDSDataItemList->count;

	pEDDDataItemList->list = new nsEDDEngine::DATA_ITEM[pEDDDataItemList->count];

	for(int i = 0; i < pEDDDataItemList->count; i++)
	{
		FDI_ConvDATA_ITEM::CopyFrom( &pDDSDataItemList->list[i], &pEDDDataItemList->list[i] );
	}
}

/*********************************************************************
 *
 *	Name: FDI_ConvDEFINITION::CopyFrom
 *	ShortDesc: copy DEFINITION
 *
 *	Description:
 *		copy DEFINITION
 *
 *	Inputs:
 *		pDDSDefinition:		source 
 *		pEDDDefinition:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvDEFINITION::CopyFrom( ::DEFINITION *pDDSDefinition, ::nsEDDEngine::DEFINITION *pEDDDefinition )
{
	if(pEDDDefinition->data)
	{
		ASSERT(0);
	}

	pEDDDefinition->Dispose();

	pEDDDefinition->size = pDDSDefinition->size;

	if(!pEDDDefinition->size)
		return;

		// Add 1 to NULL terminate
	pEDDDefinition->data = (wchar_t *)malloc((pDDSDefinition->size +1)*sizeof(wchar_t));

	memcpy(pEDDDefinition->data, pDDSDefinition->data, pDDSDefinition->size*sizeof(wchar_t));

	pEDDDefinition->data[pEDDDefinition->size] = NULL;
}

class FDI_ConvACTION_VALUE : public nsEDDEngine::ACTION_VALUE
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvACTION_VALUE::CopyFrom
	 *	ShortDesc: copy ACTION_VALUE
	 *
	 *	Description:
	 *		copy ACTION_VALUE
	 *
	 *	Inputs:
	 *		pDDSActionVal:		source string
	 *		pEDDActionVal:		destination string
	 *		eType:				action type
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::ACTION::tag_ACTION_Union *pDDSActionVal, ::nsEDDEngine::ACTION_VALUE *pEDDActionVal, ::nsEDDEngine::ACTION::ActionType etype )
	{
		switch(etype)
		{
		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REFERENCE		:
			pEDDActionVal->meth_ref = pDDSActionVal->meth_ref;
			break;
		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_REF_WITH_ARGS	:
			::FDI_ConvOP_REF_TRAIL::CopyFrom( &pDDSActionVal->meth_ref_args, &pEDDActionVal->meth_ref_args );
			break;
		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_DEFINITION		:
			FDI_ConvDEFINITION::CopyFrom( &pDDSActionVal->meth_definition, &pEDDActionVal->meth_definition );
			break;
		case nsEDDEngine::ACTION::ActionType::ACTION_TYPE_NONE				:
		default:
			break;
		}
	};
};


class FDI_ConvACTION : public nsEDDEngine::ACTION
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvACTION::CopyFrom
	 *	ShortDesc: copy ACTION
	 *
	 *	Description:
	 *		copy ACTION
	 *
	 *	Inputs:
	 *		pDDSAction:		source 
	 *		pEDDAction:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom( ::ACTION *pDDSAction, ::nsEDDEngine::ACTION *pEDDAction)
	{
		pEDDAction->eType = (nsEDDEngine::ACTION::ActionType)pDDSAction->type;
		FDI_ConvACTION_VALUE::CopyFrom( &pDDSAction->action, &pEDDAction->action, pEDDAction->eType );
	};

};

/*********************************************************************
 *
 *	Name: FDI_ConvACTION_LIST::CopyFrom
 *	ShortDesc: copy ACTION_LIST
 *
 *	Description:
 *		copy ACTION_LIST
 *
 *	Inputs:
 *		pDDSActionList:		source 
 *		pEDDActionList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvACTION_LIST::CopyFrom( ::ACTION_LIST *pDDSActionList, ::nsEDDEngine::ACTION_LIST *pEDDActionList )
{
	pEDDActionList->count = pDDSActionList->count;
	pEDDActionList->limit = pDDSActionList->limit;

	pEDDActionList->list = new ::nsEDDEngine::ACTION[pEDDActionList->count];

	for(int i = 0; i < pEDDActionList->count; i++)
	{
		FDI_ConvACTION::CopyFrom( &pDDSActionList->list[i], &pEDDActionList->list[i] );
	}
}

/*********************************************************************
 *
 *	Name: FDI_ConvRANGE_DATA_LIST::CopyFrom
 *	ShortDesc: copy RANGE_DATA_LIST
 *
 *	Description:
 *		copy RANGE_DATA_LIST
 *
 *	Inputs:
 *		pDDSRangeDataList:		source 
 *		pEDDRangeDataList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvRANGE_DATA_LIST::CopyFrom( ::RANGE_DATA_LIST *pDDSRangeDataList, ::nsEDDEngine::RANGE_DATA_LIST *pEDDRangeDataList )
{
	pEDDRangeDataList->count = pDDSRangeDataList->count;
	pEDDRangeDataList->limit = pDDSRangeDataList->limit;

	pEDDRangeDataList->list = new ::nsEDDEngine::EXPR[pEDDRangeDataList->count];

	for(int i = 0; i < pEDDRangeDataList->count; i++)
	{
		FDI_ConvEXPR::CopyFrom( &pDDSRangeDataList->list[i], &pEDDRangeDataList->list[i] );
	}
}



class FDI_ConvSTATUS_CLASS_ELEM
{
public:

	/*********************************************************************
	 *
	 *	Name: FDI_ConvSTATUS_CLASS_ELEM::CopyFrom
	 *	ShortDesc: copy STATUS_CLASS_ELEM
	 *
	 *	Description:
	 *		copy STATUS_CLASS_ELEM
	 *
	 *	Inputs:
	 *		pDDSEnumValue:		source 
	 *		pEDDEnumValue:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom(::STATUS_CLASS_ELEM *pDDSEnumValue, ::nsEDDEngine::STATUS_CLASS_ELEM *pEDDEnumValue )
	{
		pEDDEnumValue->output_class.mode_and_reliability = (nsEDDEngine::ModeAndReliability)pDDSEnumValue->output_class.mode_and_reliability;
		pEDDEnumValue->output_class.which_output	= (unsigned short)pDDSEnumValue->output_class.which_output;
        pEDDEnumValue->base_class                   = (nsEDDEngine::BaseClass)pDDSEnumValue->base_class;
	};

};



class FDI_ConvSTATUS_CLASS 
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvSTATUS_CLASS::CopyFrom
	 *	ShortDesc: copy STATUS_CLASS
	 *
	 *	Description:
	 *		copy STATUS_CLASS
	 *
	 *	Inputs:
	 *		pDDSEnumValue:		source 
	 *		pEDDEnumValue:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom(::STATUS_CLASS *pDDSEnumValue, ::nsEDDEngine::STATUS_CLASS_LIST *pEDDEnumValue )
	{
		pEDDEnumValue->list = new ::nsEDDEngine::STATUS_CLASS_ELEM[pDDSEnumValue->count];
		pEDDEnumValue->limit = pDDSEnumValue->count;
		pEDDEnumValue->count = pDDSEnumValue->count;

		for(int i=0;i<pDDSEnumValue->count;i++)
		{
			FDI_ConvSTATUS_CLASS_ELEM::CopyFrom(&pDDSEnumValue->list[i], &pEDDEnumValue->list[i]);
		}
	};

};



class FDI_ConvENUM_VALUE : public nsEDDEngine::ENUM_VALUE
{
public:
	/*********************************************************************
	 *
	 *	Name: FDI_ConvENUM_VALUE::CopyFrom
	 *	ShortDesc: copy ENUM_VALUE
	 *
	 *	Description:
	 *		copy ENUM_VALUE
	 *
	 *	Inputs:
	 *		pDDSEnumValue:		source 
	 *		pEDDEnumValue:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/
	static void CopyFrom(::ENUM_VALUE *pDDSEnumValue, ::nsEDDEngine::ENUM_VALUE *pEDDEnumValue )
	{
		pEDDEnumValue->evaled = (::nsEDDEngine::ENUM_VALUE::EnumValueEvaled)pDDSEnumValue->evaled;
		::FDI_ConvSTRING::CopyFrom( &pDDSEnumValue->desc, &pEDDEnumValue->desc );
		::FDI_ConvSTRING::CopyFrom( &pDDSEnumValue->help, &pEDDEnumValue->help );
		pEDDEnumValue->func_class = (::nsEDDEngine::ClassType)pDDSEnumValue->func_class;

		pEDDEnumValue->actions = pDDSEnumValue->actions;
		FDI_ConvEXPR::CopyFrom(&pDDSEnumValue->val, &pEDDEnumValue->val);

		FDI_ConvSTATUS_CLASS::CopyFrom(&pDDSEnumValue->status_class, &pEDDEnumValue->status);
	};

};

/*********************************************************************
 *
 *	Name: FDI_ConvENUM_VALUE_LIST::CopyFrom
 *	ShortDesc: copy ENUM_VALUE_LIST
 *
 *	Description:
 *		copy ENUM_VALUE_LIST
 *
 *	Inputs:
 *		pDDSEnumValueList:		source 
 *		pEDDEnumValueList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
void FDI_ConvENUM_VALUE_LIST::CopyFrom( ::ENUM_VALUE_LIST *pDDSEnumValueList, ::nsEDDEngine::ENUM_VALUE_LIST *pEDDEnumValueList )
{
	if(!pDDSEnumValueList)
		return;

	pEDDEnumValueList->count = pDDSEnumValueList->count;
	pEDDEnumValueList->limit = pDDSEnumValueList->count;
	pEDDEnumValueList->list = new nsEDDEngine::ENUM_VALUE[pDDSEnumValueList->count];

	for(int i = 0; i < pEDDEnumValueList->count; i++)
	{
		FDI_ConvENUM_VALUE::CopyFrom( &pDDSEnumValueList->list[i], &pEDDEnumValueList->list[i] );
	}
}

class FDI_ConvVECTOR_ITEM_VALUE : public nsEDDEngine::VECTOR_ITEM_VALUE
{
public:

	/*********************************************************************
	 *
	 *	Name: FDI_ConvVECTOR_ITEM_VALUE::CopyFrom
	 *	ShortDesc: OP_REF_TRAIL
	 *
	 *	Description:
	 *		copy OP_REF_TRAIL
	 *
	 *	Inputs:
	 *		pDDSOpRefTrailItem:		source 
	 *		pEDDVectorValue:		destination 
	 *		eType:					data type
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/

	static void CopyFrom(::OP_REF_TRAIL *pDDSOpRefTrailItem, ::nsEDDEngine::VECTOR_ITEM_VALUE *pEDDVectorValue, ::nsEDDEngine::DataItemType etype)
	{
		switch(etype)
		{
		case	nsEDDEngine::DataItemType::FDI_DATA_CONSTANT			:
			pEDDVectorValue->iconst = (long)pDDSOpRefTrailItem->expr.val.i;
			break;
		case	nsEDDEngine::DataItemType::FDI_DATA_REFERENCE 			:
			::FDI_ConvOP_REF_TRAIL::CopyFrom( pDDSOpRefTrailItem, &pEDDVectorValue->ref );
			break;
		case	nsEDDEngine::DataItemType::FDI_DATA_FLOATING 			:
			pEDDVectorValue->fconst = pDDSOpRefTrailItem->expr.val.f;
			break;
		case	nsEDDEngine::DataItemType::FDI_DATA_DOUBLE 			:
			pEDDVectorValue->dconst = pDDSOpRefTrailItem->expr.val.d;
			break;
		case 	nsEDDEngine::DataItemType::FDI_DATA_UNSIGNED 			:
			pEDDVectorValue->uconst = (unsigned long)pDDSOpRefTrailItem->expr.val.u;
			break;
		case	nsEDDEngine::DataItemType::FDI_DATA_STRING				:
			::FDI_ConvSTRING::CopyFrom( &pDDSOpRefTrailItem->expr.val.s, &pEDDVectorValue->str );
			break;
		case	nsEDDEngine::DataItemType::FDI_DATA_REF_FLAGS			:
		case	nsEDDEngine::DataItemType::FDI_DATA_REF_WIDTH			:
		case	nsEDDEngine::DataItemType::FDI_DATA_REF_FLAGS_WIDTH	:
		default:
			break;
		}
	};

};

class FDI_ConvVECTOR_ITEM : public nsEDDEngine::VECTOR_ITEM
{
public:

	/*********************************************************************
	 *
	 *	Name: FDI_ConvVECTOR_ITEM::CopyFrom
	 *	ShortDesc: copy OP_REF_TRAIL
	 *
	 *	Description:
	 *		copy OP_REF_TRAIL
	 *
	 *	Inputs:
	 *		pDDSOpRefTrailItem:		source 
	 *		pEDDVectorItem:		destination 
	 *
	 *	Returns:
	 *		void
	 *
	 **********************************************************************/

	static void CopyFrom(::OP_REF_TRAIL *pDDSOpRefTrailItem, ::nsEDDEngine::VECTOR_ITEM *pEDDVectorItem)
	{
		ASSERT(pDDSOpRefTrailItem->op_ref_type == STANDARD_TYPE);

		switch(pDDSOpRefTrailItem->op_info.type)
		{
		case CONST_INTEGER_ITYPE:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_CONSTANT;
			break;
		case STRING_LITERAL_ITYPE:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_STRING;
			break;
		case CONST_FLOAT_ITYPE:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_FLOATING;
			break;
		case CONST_UNSIGNED_ITYPE:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_UNSIGNED;
			break;
		case CONST_DOUBLE_ITYPE:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_DOUBLE;
			break;
		default:
			pEDDVectorItem->type =::nsEDDEngine::DataItemType::FDI_DATA_REFERENCE;
			break;
		}

		FDI_ConvVECTOR_ITEM_VALUE::CopyFrom( pDDSOpRefTrailItem, &pEDDVectorItem->vector, pEDDVectorItem->type );
	};

};


class FDI_ConvVECTOR : public ::nsEDDEngine::VECTOR
{
public:

/*********************************************************************
 *
 *	Name: FDI_ConvVECTOR::CopyFrom
 *	ShortDesc: copy VECTOR
 *
 *	Description:
 *		copy VECTOR
 *
 *	Inputs:
 *		pEDDString:		pDDSVector 
 *		pDDSString:		pEDDVector 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

	static void CopyFrom( ::VECTOR *pDDSVector, ::nsEDDEngine::VECTOR *pEDDVector)
	{
		pEDDVector->count = pDDSVector->values.count;
		pEDDVector->limit = pDDSVector->values.count;

		::FDI_ConvSTRING::CopyFrom( &pDDSVector->description, &pEDDVector->description );

		pEDDVector->list = new nsEDDEngine::VECTOR_ITEM[pEDDVector->count];

		for(int i = 0; i < pEDDVector->count; i++)
		{
			FDI_ConvVECTOR_ITEM::CopyFrom( &pDDSVector->values.list[i], &pEDDVector->list[i] );
		}
	};


};

/*********************************************************************
 *
 *	Name: FDI_ConvVECTOR_LIST::CopyFrom
 *	ShortDesc: copy VECTOR_LIST
 *
 *	Description:
 *		copy VECTOR_LIST
 *
 *	Inputs:
 *		pDDSVectorList:		source 
 *		pEDDVectorList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
void FDI_ConvVECTOR_LIST::CopyFrom( ::VECTOR_LIST *pDDSVectorList, ::nsEDDEngine::VECTOR_LIST *pEDDVectorList )
{
	pEDDVectorList->count = pDDSVectorList->count;
	pEDDVectorList->limit = pDDSVectorList->count;

	pEDDVectorList->vectors = new nsEDDEngine::VECTOR[pEDDVectorList->count];

	for(int i = 0; i < pEDDVectorList->count; i++)
	{
		FDI_ConvVECTOR::CopyFrom( &pDDSVectorList->vectors[i], &pEDDVectorList->vectors[i] );
	}
}



class FDI_ConvMENU_ITEM : public nsEDDEngine::MENU_ITEM
{
public:
/*********************************************************************
 *
 *	Name: FDI_ConvMENU_ITEM::CopyFrom
 *	ShortDesc: copy MENU_ITEM
 *
 *	Description:
 *		copy MENU_ITEM
 *
 *	Inputs:
 *		pDDSMenuItem:		source 
 *		pEDDMenuItem:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
	static void CopyFrom( ::MENU_ITEM *pDDSMenuItem, ::nsEDDEngine::MENU_ITEM *pEDDMenuItem )
	{
		pEDDMenuItem->qual = (::nsEDDEngine::MenuItemQual)pDDSMenuItem->qual;
		::FDI_ConvOP_REF_TRAIL::CopyFrom( &pDDSMenuItem->ref, &pEDDMenuItem->ref );
	};
};

/*********************************************************************
 *
 *	Name: FDI_ConvMENU_ITEM_LIST::CopyFrom
 *	ShortDesc: copy MENU_ITEM_LIST
 *
 *	Description:
 *		copy MENU_ITEM_LIST
 *
 *	Inputs:
 *		pDDSMenuItemList:		source 
 *		pEDDMenuItemList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvMENU_ITEM_LIST::CopyFrom( ::MENU_ITEM_LIST *pDDSMenuItemList, ::nsEDDEngine::MENU_ITEM_LIST *pEDDMenuItemList )
{
	pEDDMenuItemList->count = pDDSMenuItemList->count;
	pEDDMenuItemList->limit = pDDSMenuItemList->count;

	pEDDMenuItemList->list = new ::nsEDDEngine::MENU_ITEM[pEDDMenuItemList->count];

	for(int i=0; i < pEDDMenuItemList->count; i++)
	{
		FDI_ConvMENU_ITEM::CopyFrom( &pDDSMenuItemList->list[i], &pEDDMenuItemList->list[i] );
	}
}

/*********************************************************************
 *
 *	Name: FDI_ConvOP_REF_TRAIL_LIST::CopyFrom
 *	ShortDesc: copy OP_REF_TRAIL_LIST
 *
 *	Description:
 *		copy OP_REF_TRAIL_LIST
 *
 *	Inputs:
 *		pDDSOpRefTrailList:		source 
 *		pEDDOpRefTrailList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvOP_REF_TRAIL_LIST::CopyFrom( ::OP_REF_TRAIL_LIST *pDDSOpRefTrailList, ::nsEDDEngine::OP_REF_TRAIL_LIST *pEDDOpRefTrailList )
{
	pEDDOpRefTrailList->~OP_REF_TRAIL_LIST();	// Ensure that we start with an empty list

	pEDDOpRefTrailList->count = pDDSOpRefTrailList->count;
	pEDDOpRefTrailList->limit = pDDSOpRefTrailList->count;

	pEDDOpRefTrailList->list = new ::nsEDDEngine::OP_REF_TRAIL[pEDDOpRefTrailList->count];

	for(int i = 0; i < pEDDOpRefTrailList->count;i++)
	{
		::FDI_ConvOP_REF_TRAIL::CopyFrom( &pDDSOpRefTrailList->list[i], &pEDDOpRefTrailList->list[i] );
	}
}

/*********************************************************************
 *
 *	Name: FDI_ConvITEM_ID_LIST::CopyFrom
 *	ShortDesc: copy ITEM_ID_LIST
 *
 *	Description:
 *		copy ITEM_ID_LIST
 *
 *	Inputs:
 *		pDDSItemIdList:		source 
 *		pEDDItemIdList:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvITEM_ID_LIST::CopyFrom( ::ITEM_ID_LIST *pDDSItemIdList, ::nsEDDEngine::ITEM_ID_LIST *pEDDItemIdList)
{
	pEDDItemIdList->count = pDDSItemIdList->count;
	pEDDItemIdList->limit = pDDSItemIdList->limit;

	pEDDItemIdList->list = new ITEM_ID[pEDDItemIdList->count];

	for(int i = 0; i < pEDDItemIdList->count; i++)
	{
		pEDDItemIdList->list[i] = pDDSItemIdList->list[i];
	}
};

/*********************************************************************
 *
 *	Name: FDI_ConvTYPE_SIZE::CopyFrom
 *	ShortDesc: copy TYPE_SIZE
 *
 *	Description:
 *		copy TYPE_SIZE
 *
 *	Inputs:
 *		pDDSTypeSize:		source 
 *		pEDDTypeSize:		destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
void FDI_ConvTYPE_SIZE::CopyFrom( ::TYPE_SIZE *pDDSTypeSize, ::nsEDDEngine::TYPE_SIZE *pEDDTypeSize )
{
	pEDDTypeSize->size = pDDSTypeSize->size;
	pEDDTypeSize->type = (::nsEDDEngine::VariableType)pDDSTypeSize->type;
}

/*********************************************************************
 *
 *	Name: FDI_ConvEDDFileHeader::CopyFrom
 *	ShortDesc: copy header
 *
 *	Description:
 *		copy header
 *
 *	Inputs:
 *		header:			source 
 *		ddod_header:	destination 
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
void FDI_ConvEDDFileHeader::CopyFrom(DDOD_HEADER *ddod_header, ::nsEDDEngine::EDDFileHeader *header)
{
	header->device_id.dd_revision		=ddod_header->dd_revision;
	header->device_id.device_revision	=ddod_header->device_revision;
	header->device_id.device_type		=ddod_header->device_type;		
	header->device_id.manufacturer		=ddod_header->manufacturer;		

	header->edd_profile = (nsEDDEngine::EDD_Profile)ddod_header->edd_profile;
	::memcpy(&header->filepath,ddod_header->filepath,sizeof(ddod_header->filepath));
	header->header_size = ddod_header->header_size;
	header->item_objects_size = ddod_header->item_objects_size;
	header->magic_number = ddod_header->magic_number;
	header->major_rev = ddod_header->major_revision;
	header->metadata_size = ddod_header->meta_data_size;
	header->minor_rev = ddod_header->minor_revision;
	header->tool_release = ddod_header->tool_release;
	header->reserved3 = ddod_header->reserved3;
	header->reserved4 = ddod_header->reserved4;
	header->signature = ddod_header->signature;
}

/*********************************************************************
 *
 *	Name: FDI_ConvTRANSACTION::CopyFrom
 *	ShortDesc: copy transaction
 *
 *	Description:
 *		copy to transaction
 *
 *	Inputs:
 *		pEDDTransaction:		source transaction
 *		pDDSTransaction:		destination transaction
 *
 *	Returns:
 *		void
 *
 **********************************************************************/

void FDI_ConvTRANSACTION::CopyFrom(TRANSACTION *pDDSTransaction, ::nsEDDEngine::TRANSACTION *pEDDTransaction)
{
	pEDDTransaction->number = pDDSTransaction->number;

	FDI_ConvDATA_ITEM_LIST::CopyFrom(	&pDDSTransaction->request,		&pEDDTransaction->request);
	FDI_ConvDATA_ITEM_LIST::CopyFrom(	&pDDSTransaction->reply,		&pEDDTransaction->reply);
	FDI_ConvRESPONSE_CODES::CopyFrom(	&pDDSTransaction->resp_codes,	&pEDDTransaction->resp_codes);
	FDI_ConvACTION_LIST::CopyFrom(		&pDDSTransaction->post_actions,	&pEDDTransaction->post_actions);
}

/*********************************************************************
 *
 *	Name: FDI_ConvTRANSACTION_LIST::CopyFrom
 *	ShortDesc: copy from transaction list
 *
 *	Description:
 *		copy from transaction list
 *
 *	Inputs:
 *		pEDDTransactionList:		source transaction list
 *		pDDSTransactionList:		destination transaction list
 *
 *	Returns:
 *		void
 *
 **********************************************************************/
void FDI_ConvTRANSACTION_LIST::CopyFrom(TRANSACTION_LIST *pDDSTransactionList, ::nsEDDEngine::TRANSACTION_LIST *pEDDTransactionList)
{
	pEDDTransactionList->count = pDDSTransactionList->count;
	pEDDTransactionList->limit = pDDSTransactionList->limit;

	pEDDTransactionList->list = new ::nsEDDEngine::TRANSACTION[pDDSTransactionList->count];

	for(int i = 0; i < pEDDTransactionList->count; i++)
	{
		FDI_ConvTRANSACTION::CopyFrom(&pDDSTransactionList->list[i], &pEDDTransactionList->list[i]);
	}
}

/*********************************************************************
*
*	Name: FDI_ConvNUMBEREXPR::CopyFrom
*	ShortDesc: copy number_expr
*
*	Description:
*		copy to number_expr
*
*	Inputs:
*		pEDDNumberExpr:		source number_expr
*		pDDSNumberExpr:		destination number_expr
*
*	Returns:
*		void
*
**********************************************************************/

void FDI_ConvNUMBEREXPR::CopyFrom(NUMBER_EXPR *pDDSNumberExpr, ::nsEDDEngine::NUMBER_EXPR *pEDDNumberExpr)
{
	pEDDNumberExpr->which = pDDSNumberExpr->which;

	FDI_ConvEXPR::CopyFrom(&pDDSNumberExpr->value, &pEDDNumberExpr->value);
}

/*********************************************************************
*
*	Name: FDI_ConvRANGESET::CopyFrom
*	ShortDesc: copy range_set
*
*	Description:
*		copy to range_set
*
*	Inputs:
*		pEDDRangeSet:		source range_set
*		pDDSRangeSet:		destination range_set
*
*	Returns:
*		void
*
**********************************************************************/

void FDI_ConvRANGESET::CopyFrom(RANGE_SET *pDDSRangeSet, ::nsEDDEngine::RANGE_SET *pEDDRangeSet)
{
	FDI_ConvNUMBEREXPR::CopyFrom(&pDDSRangeSet->max_num, &pEDDRangeSet->max_num);
	FDI_ConvNUMBEREXPR::CopyFrom(&pDDSRangeSet->min_num, &pEDDRangeSet->min_num);
}

/*********************************************************************
*
*	Name: FDI_ConvRANGE_LIST::CopyFrom
*	ShortDesc: copy from range list
*
*	Description:
*		copy from range list
*
*	Inputs:
*		pEDDRangeList:		source range list
*		pDDSTRangeList:		destination range list
*
*	Returns:
*		void
*
**********************************************************************/
void FDI_ConvRANGE_LIST::CopyFrom(RANGE_LIST *pDDSTRangeList, ::nsEDDEngine::RANGE_LIST *pEDDRangeList)
{
	::FDI_ConvDESC_REF::CopyFrom(&pDDSTRangeList->var_refrences, &pEDDRangeList->var_refrences);
	pEDDRangeList->count = pDDSTRangeList->count;
	pEDDRangeList->limit = pDDSTRangeList->limit;

	pEDDRangeList->list = new ::nsEDDEngine::RANGE_SET[pDDSTRangeList->count];

	for (int i = 0; i < pEDDRangeList->count; i++)
	{
		FDI_ConvRANGESET::CopyFrom(&pDDSTRangeList->list[i], &pEDDRangeList->list[i]);
	}
}

class FDI_ConvCOMPONENT_SPECIFIER : public nsEDDEngine::COMPONENT_SPECIFIER
{
public:
	/*********************************************************************
	*
	*	Name: FDI_ConvCOMPONENT_SPECIFIER::CopyFrom
	*	ShortDesc: copy COMPONENT_SPECIFIER
	*
	*	Description:
	*		copy COMPONENT_SPECIFIER
	*
	*	Inputs:
	*		pDDSComponentSpecifier:		source
	*		pEDDComponentSpecifier:		destination
	*
	*	Returns:
	*		void
	*
	**********************************************************************/
	static void CopyFrom(::COMPONENT_SPECIFIER *pDDSComponentSpecifier, ::nsEDDEngine::COMPONENT_SPECIFIER *pEDDComponentSpecifier)
	{
		FDI_ConvEXPR::CopyFrom(&pDDSComponentSpecifier->maximum_number, &pEDDComponentSpecifier->maximum_number);
		FDI_ConvEXPR::CopyFrom(&pDDSComponentSpecifier->minimum_number, &pEDDComponentSpecifier->minimum_number);
		FDI_ConvEXPR::CopyFrom(&pDDSComponentSpecifier->auto_create, &pEDDComponentSpecifier->auto_create);
		
		::FDI_ConvOP_REF_TRAIL::CopyFrom(&pDDSComponentSpecifier->cr_ref, &pEDDComponentSpecifier->cr_ref);

		FDI_ConvRANGE_LIST::CopyFrom(&pDDSComponentSpecifier->range_list, &pEDDComponentSpecifier->range_list);
	};
};

/*********************************************************************
*
*	Name: FDI_ConvCOMPONENT_SPECIFIER_LIST::CopyFrom
*	ShortDesc: copy COMPONENT_SPECIFIER_LIST
*
*	Description:
*		copy COMPONENT_SPECIFIER_LIST
*
*	Inputs:
*		pDDSComponentSpecifierList:		source
*		pEDDComponentSpecifierList:		destination
*
*	Returns:
*		void
*
**********************************************************************/

void FDI_ConvCOMPONENT_SPECIFIER_LIST::CopyFrom(::COMPONENT_SPECIFIER_LIST *pDDSComponentSpecifierList, ::nsEDDEngine::COMPONENT_SPECIFIER_LIST *pEDDComponentSpecifierList)
{
	pEDDComponentSpecifierList->count = pDDSComponentSpecifierList->count;
	pEDDComponentSpecifierList->limit = pDDSComponentSpecifierList->count;

	pEDDComponentSpecifierList->list = new ::nsEDDEngine::COMPONENT_SPECIFIER[pEDDComponentSpecifierList->count];

	for (int i = 0; i < pEDDComponentSpecifierList->count; i++)
	{
		FDI_ConvCOMPONENT_SPECIFIER::CopyFrom(&pDDSComponentSpecifierList->list[i], &pEDDComponentSpecifierList->list[i]);
	}
}

