#pragma once

#include "nsEDDEngine/Attrs.h"

#pragma warning(disable: 4482)

namespace nsConsumer
{
	// EVAL_VAR_VALUE_VALUE class is used to store the value of the variable. 
	class EVAL_VAR_VALUE_VALUE
	{
	public:
		float					f;		// VT_FLOAT

		double					d;		// VT_DOUBLE

        unsigned long long		u;		// VT_UNSIGNED
										// VT_ENUMERATED
										// VT_BIT_ENUMERATED
										// VT_INDEX
										// VT_TIME
										// VT_DATE_AND_TIME
										// VT_DURATION
										// VT_TIME_VALUE (4)
										// VT_OBJECT_REFERENCE
										// VT_BOOLEAN

        long long					i;		// VT_INTEGER
										// VT_TIME_VALUE (8)
										// VT_EDD_DATE

		nsEDDEngine::STRING		s;		// VT_ASCII
										// VT_PACKED_ASCII
										// VT_PASSWORD
										// VT_EUC
										// VT_VISIBLESTRING

		nsEDDEngine::BINARY		b;		// VT_BITSTRING
										// VT_OCTETSTRING

		EVAL_VAR_VALUE_VALUE()
		{
			Init();
		}

		void Init()
		{
			f=0;
			d=0;
			u=0;
			i=0;
			// STRING s has own constructor
		    // Binary has its own constructor  
		}
		//Copy Constructor
		EVAL_VAR_VALUE_VALUE (const EVAL_VAR_VALUE_VALUE &evalValueValue)
		{
			AssignEvalValValueValue(evalValueValue);
		}
		//Overloaded Assignment Operator
		EVAL_VAR_VALUE_VALUE& operator= (const EVAL_VAR_VALUE_VALUE &evalValueValue)
		{
			if (this != &evalValueValue)
			{
				this->~EVAL_VAR_VALUE_VALUE();
				AssignEvalValValueValue(evalValueValue);
			}
			return (*this);
		}
		~EVAL_VAR_VALUE_VALUE()
		{
			 // Binary has its own destructor
		}

	private:
		void AssignEvalValValueValue(const EVAL_VAR_VALUE_VALUE &evalValueValue)
		{
			f = evalValueValue.f;
			d = evalValueValue.d;
			u = evalValueValue.u;
			i = evalValueValue.i;
			s = evalValueValue.s;
			b = evalValueValue.b;
		}
	};

	// This class supports upcalls to an application.
	class EVAL_VAR_VALUE
	{
	public:
		nsEDDEngine::VariableType	type;	// valid variable type defined in VariableType enum. 
		unsigned int	size;				// number of bytes a value can occupy 
		EVAL_VAR_VALUE_VALUE val;			// value of a variable (obtained from app) 

		EVAL_VAR_VALUE()
		{
			type=nsEDDEngine::VariableType::VT_DDS_TYPE_UNUSED;
			size=0;
		}
		//Copy Constructor
		EVAL_VAR_VALUE (const EVAL_VAR_VALUE &evalValue)
		{
			AssignEvalVarValue(evalValue);
		}
		//Overloaded Assignment Operator
		EVAL_VAR_VALUE& operator= (const EVAL_VAR_VALUE &evalValue)
		{
			if (this != &evalValue)
			{
				AssignEvalVarValue(evalValue);
			}
			return (*this);
		}

		~EVAL_VAR_VALUE() {}

	private:
		void AssignEvalVarValue(const EVAL_VAR_VALUE &evalValue)
		{
			type = evalValue.type;
			size = evalValue.size;
			val = evalValue.val;
		}
	};


	enum PC_ErrorCode {
		PC_SUCCESS_EC		= 0,	// Request was successful
		PC_BUSY_EC			= 1,	// Value is not currently available, but may be in the future
								//		(This is usually because the value has not been read from the device yet.)
		PC_INVALID_EC		= 2,	// Value currently has VALIDITY equal to FALSE
		PC_CIRC_DEPEND_EC	= 3,   // Parameter manager has detected a Circular Dependency
		PC_OTHER_EC			= 4		// Any other error
	};

	// IParamCache provides parameter values when 
	// needed by the EDD Engine as it evaluates conditionals in an EDD Item definition
	class IParamCache
	{
	public:
		virtual ~IParamCache(void) {};

		// Gets the requested parameter value from the Consumer Parameter Cache
		virtual PC_ErrorCode GetParamValue(int iBlockInstance, void* pValueSpec, nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec, /* out */ EVAL_VAR_VALUE * pParamValue) = 0;

        //This method, provided by the consumer, accepts a view in the form
        // of an ITEM_ID, a member of of the enum AttributeName, and returns an
        // EVAL_VAR_VALUE containing the value of the the Attribute.
        // Specific uses are for VIEW_MIN and VIEW_MAX
		virtual PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
            const nsEDDEngine::AttributeName attributeName, /* out */ EVAL_VAR_VALUE * pParamValue) = 0;

	};

}
