

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Feb 21 10:20:08 2020
 */
/* Compiler settings for EDDEngineCOM.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EDDEngineCOM_i_h__
#define __EDDEngineCOM_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IEDDMethods_FWD_DEFINED__
#define __IEDDMethods_FWD_DEFINED__
typedef interface IEDDMethods IEDDMethods;

#endif 	/* __IEDDMethods_FWD_DEFINED__ */


#ifndef ___IEDDMethodsEvents_FWD_DEFINED__
#define ___IEDDMethodsEvents_FWD_DEFINED__
typedef interface _IEDDMethodsEvents _IEDDMethodsEvents;

#endif 	/* ___IEDDMethodsEvents_FWD_DEFINED__ */


#ifndef __EDDMethods_FWD_DEFINED__
#define __EDDMethods_FWD_DEFINED__

#ifdef __cplusplus
typedef class EDDMethods EDDMethods;
#else
typedef struct EDDMethods EDDMethods;
#endif /* __cplusplus */

#endif 	/* __EDDMethods_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IEDDMethods_INTERFACE_DEFINED__
#define __IEDDMethods_INTERFACE_DEFINED__

/* interface IEDDMethods */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IEDDMethods;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1957AB75-1215-492D-B773-70BCC34A995C")
    IEDDMethods : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BeginMethodInterp( 
            /* [in] */ BSTR sMethodName,
            /* [in] */ BOOLEAN bUseCallback,
            /* [out] */ INT *error) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE EndMethodInterp( 
            /* [out] */ INT *error) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CancelMethodInterp( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE isMethodCompleted( 
            /* [out] */ BOOLEAN *result) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE validMethodState( 
            /* [out] */ BOOLEAN *result) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE waitMethodCompleted( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetUIPromptNOption( 
            /* [out] */ BSTR *wsText,
            /* [out] */ VARIANT *vOption,
            /* [out] */ unsigned long *winType) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetUIUserInputNButton( 
            /* [in] */ VARIANT *vUserText,
            /* [in] */ unsigned long currentSel,
            /* [in] */ BOOLEAN bUserButton) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateDeviceInstanceNLoadDD( 
            /* [in] */ BSTR sDDName,
            /* [in] */ SHORT protocol,
            /* [in] */ BSTR sFFBlockName,
            /* [out] */ VARIANT *vMethodList,
            /* [out] */ INT *error) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LoadActionList( 
            /* [in] */ BSTR sItemID,
            /* [out] */ VARIANT *vActionList,
            /* [out] */ INT *error) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BeginVarActionMethodInterp( 
            /* [in] */ BSTR sVarItemID,
            /* [in] */ INT iActionIndex,
            /* [in] */ BOOLEAN bUseCallback,
            /* [out] */ INT *error) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE waitForTestToComplete( void) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IEDDMethodsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEDDMethods * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEDDMethods * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEDDMethods * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IEDDMethods * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IEDDMethods * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IEDDMethods * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IEDDMethods * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BeginMethodInterp )( 
            IEDDMethods * This,
            /* [in] */ BSTR sMethodName,
            /* [in] */ BOOLEAN bUseCallback,
            /* [out] */ INT *error);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *EndMethodInterp )( 
            IEDDMethods * This,
            /* [out] */ INT *error);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CancelMethodInterp )( 
            IEDDMethods * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *isMethodCompleted )( 
            IEDDMethods * This,
            /* [out] */ BOOLEAN *result);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *validMethodState )( 
            IEDDMethods * This,
            /* [out] */ BOOLEAN *result);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *waitMethodCompleted )( 
            IEDDMethods * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetUIPromptNOption )( 
            IEDDMethods * This,
            /* [out] */ BSTR *wsText,
            /* [out] */ VARIANT *vOption,
            /* [out] */ unsigned long *winType);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetUIUserInputNButton )( 
            IEDDMethods * This,
            /* [in] */ VARIANT *vUserText,
            /* [in] */ unsigned long currentSel,
            /* [in] */ BOOLEAN bUserButton);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateDeviceInstanceNLoadDD )( 
            IEDDMethods * This,
            /* [in] */ BSTR sDDName,
            /* [in] */ SHORT protocol,
            /* [in] */ BSTR sFFBlockName,
            /* [out] */ VARIANT *vMethodList,
            /* [out] */ INT *error);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LoadActionList )( 
            IEDDMethods * This,
            /* [in] */ BSTR sItemID,
            /* [out] */ VARIANT *vActionList,
            /* [out] */ INT *error);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BeginVarActionMethodInterp )( 
            IEDDMethods * This,
            /* [in] */ BSTR sVarItemID,
            /* [in] */ INT iActionIndex,
            /* [in] */ BOOLEAN bUseCallback,
            /* [out] */ INT *error);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *waitForTestToComplete )( 
            IEDDMethods * This);
        
        END_INTERFACE
    } IEDDMethodsVtbl;

    interface IEDDMethods
    {
        CONST_VTBL struct IEDDMethodsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEDDMethods_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IEDDMethods_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IEDDMethods_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IEDDMethods_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IEDDMethods_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IEDDMethods_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IEDDMethods_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IEDDMethods_BeginMethodInterp(This,sMethodName,bUseCallback,error)	\
    ( (This)->lpVtbl -> BeginMethodInterp(This,sMethodName,bUseCallback,error) ) 

#define IEDDMethods_EndMethodInterp(This,error)	\
    ( (This)->lpVtbl -> EndMethodInterp(This,error) ) 

#define IEDDMethods_CancelMethodInterp(This)	\
    ( (This)->lpVtbl -> CancelMethodInterp(This) ) 

#define IEDDMethods_isMethodCompleted(This,result)	\
    ( (This)->lpVtbl -> isMethodCompleted(This,result) ) 

#define IEDDMethods_validMethodState(This,result)	\
    ( (This)->lpVtbl -> validMethodState(This,result) ) 

#define IEDDMethods_waitMethodCompleted(This)	\
    ( (This)->lpVtbl -> waitMethodCompleted(This) ) 

#define IEDDMethods_GetUIPromptNOption(This,wsText,vOption,winType)	\
    ( (This)->lpVtbl -> GetUIPromptNOption(This,wsText,vOption,winType) ) 

#define IEDDMethods_SetUIUserInputNButton(This,vUserText,currentSel,bUserButton)	\
    ( (This)->lpVtbl -> SetUIUserInputNButton(This,vUserText,currentSel,bUserButton) ) 

#define IEDDMethods_CreateDeviceInstanceNLoadDD(This,sDDName,protocol,sFFBlockName,vMethodList,error)	\
    ( (This)->lpVtbl -> CreateDeviceInstanceNLoadDD(This,sDDName,protocol,sFFBlockName,vMethodList,error) ) 

#define IEDDMethods_LoadActionList(This,sItemID,vActionList,error)	\
    ( (This)->lpVtbl -> LoadActionList(This,sItemID,vActionList,error) ) 

#define IEDDMethods_BeginVarActionMethodInterp(This,sVarItemID,iActionIndex,bUseCallback,error)	\
    ( (This)->lpVtbl -> BeginVarActionMethodInterp(This,sVarItemID,iActionIndex,bUseCallback,error) ) 

#define IEDDMethods_waitForTestToComplete(This)	\
    ( (This)->lpVtbl -> waitForTestToComplete(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IEDDMethods_INTERFACE_DEFINED__ */



#ifndef __EDDEngineCOMLib_LIBRARY_DEFINED__
#define __EDDEngineCOMLib_LIBRARY_DEFINED__

/* library EDDEngineCOMLib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_EDDEngineCOMLib;

#ifndef ___IEDDMethodsEvents_DISPINTERFACE_DEFINED__
#define ___IEDDMethodsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IEDDMethodsEvents */
/* [uuid] */ 


EXTERN_C const IID DIID__IEDDMethodsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("927A875B-5F1F-4001-B933-8504223A7203")
    _IEDDMethodsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IEDDMethodsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IEDDMethodsEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IEDDMethodsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IEDDMethodsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IEDDMethodsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IEDDMethodsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IEDDMethodsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IEDDMethodsEvents * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } _IEDDMethodsEventsVtbl;

    interface _IEDDMethodsEvents
    {
        CONST_VTBL struct _IEDDMethodsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEDDMethodsEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _IEDDMethodsEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _IEDDMethodsEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _IEDDMethodsEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _IEDDMethodsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _IEDDMethodsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _IEDDMethodsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IEDDMethodsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_EDDMethods;

#ifdef __cplusplus

class DECLSPEC_UUID("7D93CDE5-D062-4630-9813-EB843866C29C")
EDDMethods;
#endif
#endif /* __EDDEngineCOMLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


