// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifdef _WIN32
#include "targetver.h"
#endif
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define _CRT_SECURE_NO_WARNINGS         // This hides the security warnings (strcpy, etc) from showing in the compiler

#define STRICT

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#ifdef _WIN32
#include <crtdbg.h>
//#include <afxwin.h>         //
#endif

// Windows Header Files:
//#include <windows.h>

#include "PlatformCommon.h"
#include "stdstring.h"
#include "varient.h"
#include "varientTag.h"
// TODO: reference additional headers your program requires here
