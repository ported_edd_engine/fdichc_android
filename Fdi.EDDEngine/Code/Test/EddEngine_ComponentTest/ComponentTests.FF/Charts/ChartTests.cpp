#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_reading_CHART_from_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_chart_item_ID;

            static FDI_GENERIC_ITEM* genericItem_strip;
            static FLAT_CHART* test_var_strip_chart;

            static FDI_GENERIC_ITEM* genericItem_sweep;
            static FLAT_CHART* test_var_sweep_chart;

            static FDI_GENERIC_ITEM* genericItem_scope;
            static FLAT_CHART* test_var_scope_chart;

            static FDI_GENERIC_ITEM* genericItem_horizontal;
            static FLAT_CHART* test_var_horizontal_chart;

            static FDI_GENERIC_ITEM* genericItem_vertical;
            static FLAT_CHART* test_var_vertical_chart;

            static FDI_GENERIC_ITEM* genericItem_gauge;
            static FLAT_CHART* test_var_gauge_chart;

            static FDI_GENERIC_ITEM* genericItem_default;
            static FLAT_CHART* test_var_default_chart;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0005\\TestCase0005.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_chart_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_chart_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_chart_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_chart_specifier;
                engine->FillParamSpecifier("chart_strip", &test_var_chart_specifier);
                genericItem_strip = engine->GetGenericItem(
                    test_var_chart_specifier.id, 
                    nsEDDEngine::ITYPE_CHART, 
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_strip_chart = dynamic_cast<FLAT_CHART*>(genericItem_strip->item);

                FDI_PARAM_SPECIFIER test_var_chart_sweep_specifier;
                engine->FillParamSpecifier("chart_sweep", &test_var_chart_sweep_specifier);
                genericItem_sweep = engine->GetGenericItem(
                    test_var_chart_sweep_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_sweep_chart = dynamic_cast<FLAT_CHART*>(genericItem_sweep->item);

                FDI_PARAM_SPECIFIER test_var_chart_scope_specifier;
                engine->FillParamSpecifier("chart_scope", &test_var_chart_scope_specifier);
                genericItem_scope = engine->GetGenericItem(
                    test_var_chart_scope_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_scope_chart = dynamic_cast<FLAT_CHART*>(genericItem_scope->item);

                FDI_PARAM_SPECIFIER test_var_chart_horizontal_specifier;
                engine->FillParamSpecifier("chart_horizontal", &test_var_chart_horizontal_specifier);
                genericItem_horizontal = engine->GetGenericItem(
                    test_var_chart_horizontal_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_horizontal_chart = dynamic_cast<FLAT_CHART*>(genericItem_horizontal->item);

                FDI_PARAM_SPECIFIER test_var_chart_vertical_specifier;
                engine->FillParamSpecifier("chart_vertical", &test_var_chart_vertical_specifier);
                genericItem_vertical = engine->GetGenericItem(
                    test_var_chart_vertical_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_vertical_chart = dynamic_cast<FLAT_CHART*>(genericItem_vertical->item);

                FDI_PARAM_SPECIFIER test_var_chart_gauge_specifier;
                engine->FillParamSpecifier("chart_gauge", &test_var_chart_gauge_specifier);
                genericItem_gauge = engine->GetGenericItem(
                    test_var_chart_gauge_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_gauge_chart = dynamic_cast<FLAT_CHART*>(genericItem_gauge->item);

                FDI_PARAM_SPECIFIER test_var_chart_default_specifier;
                engine->FillParamSpecifier("chart_default", &test_var_chart_default_specifier);
                genericItem_default = engine->GetGenericItem(
                    test_var_chart_default_specifier.id, 
                    nsEDDEngine::ITYPE_CHART,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_CHART),
                    iBlockInstance);
                test_var_default_chart = dynamic_cast<FLAT_CHART*>(genericItem_default->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem_strip;
                delete genericItem_sweep;
                delete genericItem_scope;
                delete genericItem_horizontal;
                delete genericItem_vertical;
                delete genericItem_gauge;
                delete genericItem_default;
                delete test_block_chart_item_ID;
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_chart_type_for_default_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(0, (int) test_var_default_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_cycle_time_for_default_chart()
            {
                Assert::AreEqual(1000, (int) test_var_default_chart->cycle_time);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_length_for_default_chart()
            {
                Assert::AreEqual(600000, (int) test_var_default_chart->length);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Strip_Chart()
            {
                Assert::AreEqual("Strip Chart", gcnew String(test_var_strip_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Sweep_Chart()
            {
                Assert::AreEqual("Sweep Chart", gcnew String(test_var_sweep_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Scope_Chart()
            {
                Assert::AreEqual("Scope Chart", gcnew String(test_var_scope_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Horizontal_Chart()
            {
                Assert::AreEqual("Horizontal Bar Chart", gcnew String(test_var_horizontal_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Vertical_Chart()
            {
                Assert::AreEqual("Vertical Bar Chart", gcnew String(test_var_vertical_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Gauge_Chart()
            {
                Assert::AreEqual("Gauge Chart", gcnew String(test_var_gauge_chart->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_help_text_for_strip_chart()
            {
                Assert::AreEqual("A Strip Chart", gcnew String(test_var_strip_chart->help.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_have_no_help_text_attribute_for_sweep_chart()
            {
                Assert::AreEqual("", gcnew String(test_var_sweep_chart->help.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_validity_for_sweep_chart()
            {
                // 0 = false
                Assert::AreEqual(0, (int)test_var_sweep_chart->valid);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_validity_for_scope_chart()
            {
                // 1 = true        
                Assert::AreEqual(1, (int)test_var_scope_chart->valid);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_validity_for_vertical_chart()
            {
                // 1 = true = default       
                Assert::AreEqual(1, (int)test_var_vertical_chart->valid);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_strip_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(0, (int) test_var_strip_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_sweep_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(1, (int) test_var_sweep_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_scope_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(2, (int) test_var_scope_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_horizontal_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(3, (int) test_var_horizontal_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_vertical_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(4, (int) test_var_vertical_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_chart_type_for_gauge_chart ()
            {
                // strip (0),
                // sweep (1),
                // scope (2),
                // horizontal-bar (3),
                // vertical-bar (4),
                // gauge (5)
                Assert::AreEqual(5, (int) test_var_gauge_chart->type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_number_of_members_for_vertical_chart()
            {
                Assert::AreEqual(2, (int)test_var_vertical_chart->members.count);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_number_of_members_for_horizontal_chart()
            {
                Assert::AreEqual(1, (int)test_var_horizontal_chart->members.count);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_height_for_strip_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(0, (int) test_var_strip_chart->height);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_width_for_strip_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(0, (int) test_var_strip_chart->height);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_height_for_sweep_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(2, (int) test_var_sweep_chart->height);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_width_for_sweep_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(3, (int) test_var_sweep_chart->width);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_height_for_scope_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(4, (int) test_var_scope_chart->height);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_width_for_scope_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(5, (int) test_var_scope_chart->width);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_width_for_gauge_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(5, (int) test_var_gauge_chart->width);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_height_for_vertical_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(6, (int) test_var_vertical_chart->height);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_width_for_vertical_chart()
            {
                //XX_SMALL (0),
                //    X_SMALL (1),
                //    SMALL (2),
                //    MEDIUM (3),
                //    LARGE (4),
                //    X_LARGE (5),
                //    XX_LARGE (6)}
                Assert::AreEqual(2, (int) test_var_vertical_chart->width);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_cycle_time_for_gauge_chart()
            {
                Assert::AreEqual(1500, (int) test_var_gauge_chart->cycle_time);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_length_for_gauge_chart()
            {
                Assert::AreEqual(17500, (int) test_var_gauge_chart->length);
            }
        };

        [TestClass]
        public ref class When_reading_SOURCE_from_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_source_item_ID;
            static FDI_GENERIC_ITEM* genericItem_source_1;
            static FDI_GENERIC_ITEM* genericItem_source_2;
            static FDI_GENERIC_ITEM* genericItem_source_3;
            static FDI_GENERIC_ITEM* genericItem_source_4;
            static FDI_GENERIC_ITEM* genericItem_source_5;
            static FDI_GENERIC_ITEM* genericItem_source_6;
            static FDI_GENERIC_ITEM* genericItem_source_7;
            static FDI_GENERIC_ITEM* genericItem_source_8;
            static FDI_GENERIC_ITEM* genericItem_source_9;
            static FLAT_SOURCE* test_var_source_1;
            static FLAT_SOURCE* test_var_source_2;
            static FLAT_SOURCE* test_var_source_3;
            static FLAT_SOURCE* test_var_source_4;
            static FLAT_SOURCE* test_var_source_5;
            static FLAT_SOURCE* test_var_source_6;
            static FLAT_SOURCE* test_var_source_7;
            static FLAT_SOURCE* test_var_source_8;
            static FLAT_SOURCE* test_var_source_9;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0005\\TestCase0005.ff6");
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_source_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_source_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_source_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                FDI_PARAM_SPECIFIER test_var_source_1_specifier;
                engine->FillParamSpecifier("chart_source_1", &test_var_source_1_specifier);
                genericItem_source_1= engine->GetGenericItem(
                    test_var_source_1_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_1 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_1->item);

                FDI_PARAM_SPECIFIER test_var_source_2_specifier;
                engine->FillParamSpecifier("chart_source_2", &test_var_source_2_specifier);
                genericItem_source_2 = engine->GetGenericItem(
                    test_var_source_2_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_2 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_2->item);

                FDI_PARAM_SPECIFIER test_var_source_3_specifier;
                engine->FillParamSpecifier("chart_source_3", &test_var_source_3_specifier);
                genericItem_source_3 = engine->GetGenericItem(
                    test_var_source_3_specifier.id,
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_3 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_3->item);

                FDI_PARAM_SPECIFIER test_var_source_4_specifier;
                engine->FillParamSpecifier("chart_source_4", &test_var_source_4_specifier);
                genericItem_source_4 = engine->GetGenericItem(
                    test_var_source_4_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_4 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_4->item);

                FDI_PARAM_SPECIFIER test_var_source_5_specifier;
                engine->FillParamSpecifier("chart_source_5", &test_var_source_5_specifier);
                genericItem_source_5 = engine->GetGenericItem(
                    test_var_source_5_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_5 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_5->item);

                FDI_PARAM_SPECIFIER test_var_source_6_specifier;
                engine->FillParamSpecifier("chart_source_6", &test_var_source_6_specifier);
                genericItem_source_6 = engine->GetGenericItem(
                    test_var_source_6_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_6 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_6->item);

                FDI_PARAM_SPECIFIER test_var_source_7_specifier;
                engine->FillParamSpecifier("chart_source_7", &test_var_source_7_specifier);
                genericItem_source_7 = engine->GetGenericItem(
                    test_var_source_7_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_7 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_7->item);

                FDI_PARAM_SPECIFIER test_var_source_8_specifier;
                engine->FillParamSpecifier("chart_source_8", &test_var_source_8_specifier);
                genericItem_source_8 = engine->GetGenericItem(
                    test_var_source_8_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_8 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_8->item);

                FDI_PARAM_SPECIFIER test_var_source_9_specifier;
                engine->FillParamSpecifier("chart_source_9", &test_var_source_9_specifier);
                genericItem_source_9 = engine->GetGenericItem(
                    test_var_source_9_specifier.id, 
                    nsEDDEngine::ITYPE_SOURCE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_SOURCE),
                    iBlockInstance);
                test_var_source_9 = dynamic_cast<FLAT_SOURCE*>(genericItem_source_9->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;

                delete genericItem_source_1;
                delete genericItem_source_2;
                delete genericItem_source_3;
                delete genericItem_source_4;
                delete genericItem_source_5;
                delete genericItem_source_6;
                delete genericItem_source_7;
                delete genericItem_source_8;
                delete genericItem_source_9;

                delete test_block_source_item_ID;
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_label_text_for_Source_1()
            {
                Assert::AreEqual("Chart Source 1", gcnew String(test_var_source_1->label.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_have_no_help_attribute_for_Source_1()
            {
                Assert::AreEqual("", gcnew String(test_var_source_1->help.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_help_text_for_source_2()
            {
                Assert::AreEqual("the Chart Source 2", gcnew String(test_var_source_2->help.c_str()));
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_validity_for_source_2()
            {
                // 1 = true        
                Assert::AreEqual(1,(int) test_var_source_2->valid);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_number_of_members_for_source_3()
            {
                Assert::AreEqual(1,(int) test_var_source_3->op_members.count);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_emphasis_for_source_3()
            {
                // 1 = true, 0 = false       
                Assert::AreEqual(1,(int) test_var_source_3->emphasis);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_emphasis_for_source_4()
            {
                // 1 = true, 0 = false       
                Assert::AreEqual(0,(int) test_var_source_4->emphasis);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_emphasis_for_source_2()
            {
                // 1 = true, 0 = false       
                Assert::AreEqual(0,(int) test_var_source_2->emphasis);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_color_for_source_3()
            {
                // BLACK = 0x000000 = 0
                Assert::AreEqual(123ul, test_var_source_3->line_color);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_default_line_type_for_source_1()
            {
                Assert::AreEqual((int) FDI_DATA0_LINETYPE, (int) test_var_source_1->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_color_for_source_4()
            {
                // 0x191970 = 1644912       
                Assert::AreEqual(1644912ul, test_var_source_4->line_color);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_2()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_TRANSPARENT_LINETYPE, (int)test_var_source_2->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_3()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_DATA1_LINETYPE, (int)test_var_source_3->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_9()
            {
                // LINE_TYPE DATA5;
                // http://fdiidebugzilla.codewrights.biz/cgi-bin/bugzilla/show_bug.cgi?id=171

                //data-n [1] INTEGER,
                Assert::AreEqual((int)LineType::FDI_DATA5_LINETYPE, (int)test_var_source_9->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_4()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_LOW_LINETYPE, (int)test_var_source_4->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_5()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_DATA0_LINETYPE, (int)test_var_source_5->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_6()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_LOWLOW_LINETYPE, (int)test_var_source_6->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_7()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_HIGH_LINETYPE, (int)test_var_source_7->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_line_type_for_source_8()
            {
                //data-n [1] INTEGER,
                //    low-low-limit [2] NULL,
                //    low-limit [3] NULL,
                //    high-limit [4] NULL,
                //    high-high-limit [5] NULL,
                //    transparent [6] NULL
                Assert::AreEqual((int)LineType::FDI_HIGHHIGH_LINETYPE, (int)test_var_source_8->line_type);
            }

            [TestMethod, TestCategory("ChartTest")]
            void It_should_return_the_correct_y_axis_for_source_1()
            {
                Assert::AreEqual(0x20016u,  test_var_source_1->y_axis);
            }
        };

        [TestClass]
        public ref class When_reading_AXIS_from_FF
        {
        public:
            static EddEngineController^ engine;
            static FLAT_AXIS* test_var_axis_1;
            static FLAT_AXIS* test_var_axis_2;
            static FLAT_AXIS* test_var_axis_3;
            static FDI_GENERIC_ITEM* genericItem_axis_1;
            static FDI_GENERIC_ITEM* genericItem_axis_2;
            static FDI_GENERIC_ITEM* genericItem_axis_3;
            static EVAL_VAR_VALUE* test_var_chart_max_value;
            static FDI_PARAM_SPECIFIER* test_var_chart_max_specifier;
            static FDI_PARAM_SPECIFIER* test_var_chart_min_specifier;
            static EVAL_VAR_VALUE* test_var_chart_min_value;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {

                engine = gcnew EddEngineController("FF\\TestCase0005\\TestCase0005.ff6");
                auto paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                // obtain item specifier (id) for "test_var_1"
                test_var_chart_max_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("chart_max_value_1", test_var_chart_max_specifier);

                // create value for "test_variable_int"
                test_var_chart_max_value = new EVAL_VAR_VALUE();
                test_var_chart_max_value->type = VT_INTEGER;
                test_var_chart_max_value->size = 4;
                test_var_chart_max_value->val.i = 10;

                // configure ParamCacheSimulator to return special value for "test_variable_int"
                paramCacheSimulator->SetParamValue(test_var_chart_max_specifier, test_var_chart_max_value);

                // get the ItemIds for the test_blocks
                ITEM_ID test_block_axis_item_id;
                engine->Instance->GetItemIdFromSymbolName(L"test_block", &test_block_axis_item_id);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = test_block_axis_item_id;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                //// obtain item specifier (id) for "chart_min_value_1"
                test_var_chart_min_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("chart_min_value_1", test_var_chart_min_specifier);

                // create value for "chart_min_value_1"
                test_var_chart_min_value = new EVAL_VAR_VALUE();
                test_var_chart_min_value->type = VT_INTEGER;
                test_var_chart_min_value->size = 4;
                test_var_chart_min_value->val.i = -10;

                // configure ParamCacheSimulator to return special value for "chart_min_value_1"
                paramCacheSimulator->SetParamValue(test_var_chart_min_specifier, test_var_chart_min_value);

                FDI_PARAM_SPECIFIER test_var_axis_1_specifier;
                engine->FillParamSpecifier("chart_axis_1", &test_var_axis_1_specifier);
                genericItem_axis_1 = engine->GetGenericItem(
                    test_var_axis_1_specifier.id, 
                    nsEDDEngine::ITYPE_AXIS ,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_AXIS),
                    iBlockInstance);
                test_var_axis_1 = dynamic_cast<FLAT_AXIS*>(genericItem_axis_1->item);

                FDI_PARAM_SPECIFIER test_var_axis_2_specifier;
                engine->FillParamSpecifier("chart_axis_2", &test_var_axis_2_specifier);
                genericItem_axis_2 = engine->GetGenericItem(
                    test_var_axis_2_specifier.id, 
                    nsEDDEngine::ITYPE_AXIS ,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_AXIS),
                    iBlockInstance);
                test_var_axis_2 = dynamic_cast<FLAT_AXIS*>(genericItem_axis_2->item);

                FDI_PARAM_SPECIFIER test_var_axis_3_specifier;
                engine->FillParamSpecifier("chart_axis_3", &test_var_axis_3_specifier);
                genericItem_axis_3 = engine->GetGenericItem(
                    test_var_axis_3_specifier.id, 
                    nsEDDEngine::ITYPE_AXIS ,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_AXIS),
                    iBlockInstance);
                test_var_axis_3 = dynamic_cast<FLAT_AXIS*>(genericItem_axis_3->item);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete genericItem_axis_1;
                delete genericItem_axis_2;
                delete genericItem_axis_3;
                delete test_var_chart_max_specifier;
                delete test_var_chart_min_specifier;
                delete test_var_chart_max_value;
                delete test_var_chart_min_value;
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_label_text_for_axis_1()
            {
                Assert::AreEqual("Axis 1", gcnew String(test_var_axis_1->label.c_str()));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_help_text_for_axis_1()
            {
                Assert::AreEqual("The Axis 1", gcnew String(test_var_axis_1->help.c_str()));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_have_an_emty_help_attribute_for_axis_2()
            {
                Assert::AreEqual("", gcnew String(test_var_axis_2->help.c_str()));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_have_an_emty_label_attribute_for_axis_2()
            {
                Assert::AreEqual("", gcnew String(test_var_axis_2->label.c_str()));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_constant_unit_for_axis_2()
            {
                Assert::AreEqual("a constant unit", gcnew String(test_var_axis_2->constant_unit.c_str()));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_have_no_constant_unit_for_axis_1()
            {
                Assert::AreEqual(0, (int) test_var_axis_1->masks.attr_avail.count(nsEDDEngine::constant_unit));
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_max_axis_for_axis_1()
            {
                Assert::AreEqual(10,(int) test_var_axis_1->max_axis.val.i);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_min_axis_for_axis_2()
            {
                Assert::AreEqual(-10, (int) test_var_axis_2->min_axis.val.i);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_min_axis_for_axis_3()
            {
                Assert::AreEqual(4.5, (double) test_var_axis_3->min_axis.val.d);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_max_axis_for_axis_3()
            {
                Assert::AreEqual(15, (int) test_var_axis_3->max_axis.val.i);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_scaling_for_axis_1()
            {
                // linear (0),
                // logarithmic (1)
                Assert::AreEqual(0,(int) test_var_axis_1->scaling);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_scaling_for_axis_2()
            {
                // linear (0),
                // logarithmic (1)
                Assert::AreEqual(1,(int) test_var_axis_2->scaling);
            }

            [TestMethod, TestCategory("AxisTest")]
            void It_should_return_the_correct_scaling_for_axis_3()
            {
                // linear (0),
                // logarithmic (1)
                Assert::AreEqual(1,(int) test_var_axis_3->scaling);
            }
        };

    } // namespace FF
} // namespace ComponentTests
