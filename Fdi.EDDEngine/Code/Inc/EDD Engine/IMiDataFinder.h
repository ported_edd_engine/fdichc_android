
#pragma once
//#include <comdef.h>
#include "nsEDDEngine/ProtocolType.h"
#include "nsEDDEngine/Ddldefs.h"
#include "nsEDDEngine/Flats.h"
#include "ddbdefs.h"



class IMiDataFinder
{
	public:
		
		virtual ~IMiDataFinder(void) {};

		virtual int ResolveNameToID( LPCTSTR, ITEM_ID *plItemId ) = 0;//todo use ITEM_ID
		virtual int GetValue( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, long lProperty, CValueVarient *pvtValue, const wchar_t *lang_code ) = 0;
		virtual nsEDDEngine::ITEM_TYPE GetItemType(int iBlockInstance, ITEM_ID lItemID) = 0;
		virtual int GetItemTypeAndItemId(int iBlockInstance, nsEDDEngine::FDI_ITEM_SPECIFIER* pItemSpec, nsEDDEngine::ITEM_TYPE* pItemType, ITEM_ID* pItemId) = 0;
		virtual ITEM_ID ResolveRefToID( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, long lIndex, const wchar_t *lang_code ) = 0;
		virtual int GetArrayElementLabel( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, long lIndex, CValueVarient *pvtValue, const wchar_t *lang_code ) = 0;
		
		// This GetResponseCodeString takes command number and response code to get response code string  
		// This should be used for HART builtin
		virtual int GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol, const wchar_t *lang_code ) = 0;
		
		// This GetResponseCodeString takes item id, member id and response code to get response code string  
		// This should be used for FF builtin
		virtual int GetResponseCodeString( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemId, ITEM_ID lMemberId, int nResponseCode, CValueVarient *pvtValue, const wchar_t *lang_code ) = 0;
		
		virtual int GetEnumVarString(nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString, const wchar_t *lang_code) = 0;
		virtual int GetVarInfo( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID lItemID, ITEM_ID lMemberID, nsEDDEngine::FLAT_VAR** pVar, const wchar_t *lang_code) = 0;  
		virtual int GetDictionaryString( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code ) = 0;
		virtual int GetDictionaryString( wchar_t* wsDictName,  wchar_t* pString, int iStringLen, const wchar_t *lang_code ) = 0;
		virtual int GetDevSpecString ( unsigned long ulIndex,  wchar_t* pString, int iStringLen, const wchar_t *lang_code ) = 0;
		virtual	int ResolveSubindex ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, ITEM_ID ItemId, ITEM_ID MemberId, int* iSubIndex) = 0;
		virtual	int LookupMemberId ( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, SUBINDEX ulSubIndex, const wchar_t *lang_code,
									 /* out */ITEM_ID* pulMemberId ) = 0;
	
		// For CrossBlock BuiltIns
		virtual int GetBlockInstanceByObjIndex(int iObjectIndex, int* iOccurrence) = 0;
		virtual int GetBlockInstanceByTag( ITEM_ID iItemId,wchar_t* pTag, int* iOccurrence) = 0;
		virtual int GetBlockInstanceCount( ITEM_ID iItemId, int* piCount) = 0;
		virtual int ConvertToBlockInstance(unsigned long ulItemId, int iOccurrence, nsEDDEngine::BLOCK_INSTANCE* piBlockInstance) = 0;
		virtual int GetItemsBetweenReferenceItems( /*in*/nsEDDEngine::BLOCK_INSTANCE iBlockInstance, /*in*/void* pValueSpec, /*in*/const wchar_t *lang_code,
												   /*in*/ITEM_ID ulRefItemId,
												   /*in*/const unsigned short usMaxArrayElem,
												   /*in, out*/nsEDDEngine::ITEM_TYPE peItemType[], /*in, out*/ITEM_ID pulItemId[], /*in, out*/ITEM_ID pulMemberId[],
												   /*out*/unsigned short* pArrayElemNum ) = 0;
	
		// Converts an EDD Engine Error code to a string
		virtual wchar_t *GetErrorString(int iErrorNum) = 0;

		virtual ITEM_ID resolveBlockRef( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulMemberId, ResolveType eResolveType, const wchar_t *lang_code ) = 0;
		virtual int GetActionList( nsEDDEngine::BLOCK_INSTANCE iBlockInstance, void* pValueSpec, ITEM_ID ulItemId, nsEDDEngine::ACTION_LIST *pOutput, const wchar_t *lang_code) = 0;
};
