/*****************************************************************************
 *	"@(#)$Id: sym.h,v 1.1 2012/07/10 20:59:30 rgretta Exp $"	
 ****************************************************************************/
#ifndef SYM_INC
#define SYM_INC

#ifndef CM_STRUCT_H
#include "cm_struc.h"
#endif


typedef struct tag_SYMTBL {
	ITEM_ID		SymbolId;
	CStdString		SymbolName;
} SYMTBL;

typedef struct tag_SYMINFO {
	SYMTBL*	symIdTbl;			// Sorted by Id, these are unique
	int		numIdElem;

	// ItemNames and MemberNames can be the same, so we split them into two tables
	SYMTBL*	symItemNameTbl;		// Sorted by SymbolName
	int		numItemNameElem;

	SYMTBL*	symMemberNameTbl;	// Sorted by SymbolName
	int		numMemberNameElem;
} SYMINFO;



int Fill_Syminfo(DEVICE_TYPE_HANDLE dth, SYMINFO* syminfo);

void Free_Syminfo(SYMINFO* syminfo);

int server_item_id_to_name  (SYMINFO* syminfo, ITEM_ID item_id, LPTSTR item_name, int outbuf_size);

int server_item_name_to_id  (SYMINFO* syminfo, LPCTSTR item_name,   ITEM_ID *item_id);
int server_member_name_to_id(SYMINFO* syminfo, LPCTSTR member_name, ITEM_ID *item_id);
#endif


