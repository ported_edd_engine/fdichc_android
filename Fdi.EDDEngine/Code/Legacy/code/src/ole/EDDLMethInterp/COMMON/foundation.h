/*************************************************************************************************
 *
 * $Workfile: foundation.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of those tiny little classes that everything is built on
 *		4/25/02	sjv	created
 *
 * #include "foundation.h"
 */

#ifndef _FOUNDATION_H
#define _FOUNDATION_H
#ifdef INC_DEBUG
#pragma message("In foundation.h") 
#endif

#pragma warning (disable : 4786) 

#include "DDLDEFS.H"
#include "ddbdefs.h"
#include <ostream>
#include <iostream>
#include "PlatformCommon.h"

// Utility functions for UTF8 and wstring
using namespace std;

extern wstring UTF82Unicode(const char* src);
extern string ws2as(const wstring& src);
extern wstring as2ws(const string& str );
extern wchar_t *c2w(char *str);
extern char *w2c(const wchar_t *wstr );
extern char *latin2utf8(char* s, char *buf, int bufsiz);
extern int latin2utf8size(char* s);
extern string addlinebreaks(char *s);


#include "logging.h"

#ifdef INC_DEBUG
#pragma message("    Finished Includes::foundation.h") 
#endif

extern const  /* referenceTpe = id2refTbl[itemType]; */
referenceType_t id2refTbl[]; 


class CrefType
{
#define MAX_REFTYPE_ENUM  ((int)rT_NotA_RefType) /* MAX_REFTYPE is NOT-A-REF */

	unsigned int refTyVal;		// has to be bigger than a char - see  referenceType_t
	static const char refTyName[][18];
	const char* pName;
    char returnString[128];

public:
	CrefType() {refTyVal = (MAX_REFTYPE_ENUM);};
	CrefType(unsigned int initVal) 
	{	refTyVal=initVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-value constructor set the value out of range00.(%d)\n",refTyVal);}
		#endif
	};
	CrefType (const CrefType& src)
	{	refTyVal = src.refTyVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-copy constructor set the value out of range01.(%d)\n", refTyVal);}
		#endif
	};
	CrefType& operator=(const CrefType& src)
	{	refTyVal = src.refTyVal;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-equal operator set the value out of range.02(%d)\n",refTyVal);
			    refTyVal=MAX_REFTYPE_ENUM;}
		#endif
		return *this;
	};
	CrefType& operator=(const unsigned short& src)
	{	refTyVal = src;
		pName = &(refTyName[refTyVal][0]);
		#ifdef _DEBUG
			if (refTyVal<0 || refTyVal>(MAX_REFTYPE_ENUM))
			{	LOGIT(CERR_LOG,"ERROR: CrefType-int equal operator set the value out of range03.(%d)\n",refTyVal);
			    refTyVal=MAX_REFTYPE_ENUM;}
		#endif
		return *this;
	};

	bool operator==(const int otherValue) { return ( otherValue == refTyVal );};
	bool operator==(const referenceType_t otherValue) { return (otherValue == (referenceType_t)refTyVal );};

	bool operator!=(const int otherValue) { return !( otherValue == refTyVal );};
	bool operator!=(const referenceType_t otherValue) { return !(otherValue == (referenceType_t)refTyVal );};


	unsigned int getType(void){ return refTyVal;};
	void         setType(unsigned int newType){if (newType>=0 && newType<=(MAX_REFTYPE_ENUM)) refTyVal = newType; else refTyVal = MAX_REFTYPE_ENUM; };
	const char*  getTypeStr(void)
				{
					if (refTyVal>=0 && refTyVal<=MAX_REFTYPE_ENUM)
					{	return refTyName[refTyVal];
					}
					else
					{
                        PS_VsnPrintf(returnString,127,"InvalidRefType (%x)",refTyVal);	//A null character is appended after the last character written
						return returnString;
					}
				};
	int/*boolean*/ isValidRef(void){ if (refTyVal >= 0 && refTyVal < MAX_REFTYPE_ENUM) return 1; else return 0;};

	void clear(void){refTyVal=MAX_REFTYPE_ENUM;pName = NULL;};
};

inline ostream &operator<<(ostream& ostr, CrefType& it )
{
	ostr << it.getTypeStr();
	return ostr;
};


/*******************************
#define	RESERVED_ITYPE1			0
#define VARIABLE_ITYPE 	       	1
#define COMMAND_ITYPE           2
#define MENU_ITYPE              3
#define EDIT_DISP_ITYPE         4
#define METHOD_ITYPE            5
#define REFRESH_ITYPE           6
#define UNIT_ITYPE              7
#define WAO_ITYPE 	       		8
#define ITEM_ARRAY_ITYPE        9
#define COLLECTION_ITYPE        10
#define	RESERVED_ITYPE2			11
#define BLOCK_ITYPE             12
#define PROGRAM_ITYPE           13
#define RECORD_ITYPE            14
#define ARRAY_ITYPE             15
#define VAR_LIST_ITYPE          16
#define RESP_CODES_ITYPE        17
#define DOMAIN_ITYPE            18
#define MEMBER_ITYPE            19
#define MAX_ITYPE				20	// must be last in list 
//
//	Special object type values
//
#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130
 // these additional ITYPES are used by resolve to build resolve trails 
#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202
************************************/
class CitemType
{
	unsigned int itmTyVal;		// actually an itemType_t but left an int so load & read can share
	static const char itmTyName[MAX_ITYPE][16];
	static const int ref2idTbl[];
public:
	operator long &()		{ return (long &)itmTyVal ;      };
	operator itemType_t ()	{ return ((itemType_t)itmTyVal); };

	unsigned int getType(void){ return itmTyVal;};

	const char*	getTypeStr(void)
				{return (itmTyVal>0 && itmTyVal<MAX_ITYPE)?itmTyName[itmTyVal]:itmTyName[0];};

	void clear(void){itmTyVal=0;};




	CitemType() {itmTyVal = 0xFFFFFFFF;};
	CitemType(unsigned int initVal) 
	{	itmTyVal=initVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal> MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-value constructor set the value out of range.(%d)\n",itmTyVal);}
		#endif
	};
	CitemType (const CitemType& src)
	{	itmTyVal = src.itmTyVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-copy constructor set the value out of range.(%d)\n",itmTyVal);}
		#endif
	};

	CitemType& operator=(const CitemType& src)
	{	itmTyVal = src.itmTyVal;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-equal operator set the value out of range.(%d)\n",itmTyVal);}
		#endif
		return *this;
	};
	//CitemType& operator=(const unsigned short& src)
	//{	itmTyVal = src;
	//	#ifdef _DEBUG
	//		if (itmTyVal<0 && itmTyVal>=MAX_ITYPE)
	//		{	cerr << "ERROR: CitemType-short equal operator set the value out of range.(" << itmTyVal << ")" << endl;}
	//	#endif
	//	return *this;
	//};
	CitemType& operator=(const long& src)
	{	itmTyVal = (unsigned int) src;
		#ifdef _DEBUG
			if (itmTyVal<0 && itmTyVal>MAX_ITYPE)// was '>='
			{	LOGIT(CERR_LOG,"ERROR: CitemType-long equal operator set the value out of range.(%d)\n",itmTyVal);}
		#endif
		return *this;
	};
	CitemType& operator=( CrefType& ref)
	{
		if (ref.getType() >= MAX_REFTYPE_ENUM )
			itmTyVal = MAX_REFTYPE_ENUM;
		else
			itmTyVal = ref2idTbl[ref.getType()];
		return *this;
	};
};

inline ostream &operator<<(ostream& ostr, CitemType& it )
{
	ostr << it.getTypeStr();
	return ostr;
}


/* stevev 20oct06 - added for device-object layout */
/*  this is defined in case we need to change to a float location value later */
#ifndef DDSizeLocType	/* it can be over-ridden */
#define DDSizeLocType  long 
#endif /* else, leave it as it came in */

typedef struct hv_point_s
{
	DDSizeLocType	xval;
	DDSizeLocType	yval;
         hv_point_s(){xval=yval=0;}
         hv_point_s(DDSizeLocType x,DDSizeLocType y){xval=x;yval=y;}
        bool operator==(const struct hv_point_s& p){ return ( (xval==p.xval)&&(yval==p.yval));}
        void clear(void){xval=yval=0;}
        bool isEmpty(void){ return ((xval==0)&&(yval==0));}
        hv_point_s& operator=(const struct hv_point_s& s){xval=s.xval;yval=s.yval;return *this;}
}
/* typedef */  hv_point_t; /* end-add 20oct06*/

typedef struct hv_location_s
{
	hv_point_t	topLeftPt;
	hv_point_t	rectSize;

         hv_location_s(){}//they each default to zero-zero
         hv_location_s(hv_point_t loc,hv_point_t siz){topLeftPt=loc;rectSize=siz;}

        void setValue(hv_point_t& tlPt, hv_point_t& recSz){topLeftPt=tlPt;rectSize=recSz;}
	bool operator==(const struct hv_location_s& p)
                                                        { return ( (topLeftPt==p.topLeftPt)&&(rectSize==p.rectSize));}
	bool operator!=(const struct hv_location_s& p)
                                                        { return !( (topLeftPt==p.topLeftPt)&&(rectSize==p.rectSize));}
        void clear(void)		{topLeftPt.clear();rectSize.clear();}
        bool isEmpty(void)		{ return ((topLeftPt.isEmpty())&&(rectSize.isEmpty()));}
	hv_location_s& operator=(const struct hv_location_s& s)
                                                        {topLeftPt=s.topLeftPt;rectSize=s.rectSize;return *this;}
}
/* typedef */  hv_location_t; /* end-add 20oct06*/

//ws 12mar08 - added from the unicode website (in lieu of .h file)
typedef unsigned long	UTF32;	/* at least 32 bits */
typedef unsigned short	UTF16;	/* at least 16 bits */
typedef unsigned char	UTF8;	/* typically 8 bits */
typedef unsigned char	Boolean; /* 0 or 1 */

/* Some fundamental constants */
#define UNI_REPLACEMENT_CHAR (UTF32)0x0000FFFD
#define UNI_MAX_BMP (UTF32)0x0000FFFF
#define UNI_MAX_UTF16 (UTF32)0x0010FFFF
#define UNI_MAX_UTF32 (UTF32)0x7FFFFFFF
#define UNI_MAX_LEGAL_UTF32 (UTF32)0x0010FFFF

typedef enum {
	conversionOK, 		/* conversion successful */
	sourceExhausted,	/* partial character in source, but hit end */
	targetExhausted,	/* insuff. room in target for conversion */
	sourceIllegal		/* source sequence is illegal/malformed */
} ConversionResult;

typedef enum {
	strictConversion = 0,
	lenientConversion
} ConversionFlags;

/* This is for C++ and does no harm in C */
#ifdef __cplusplus
extern "C" {
#endif

ConversionResult ConvertUTF8toUTF16 (
		const UTF8** sourceStart, const UTF8* sourceEnd, 
		UTF16** targetStart, UTF16* targetEnd, ConversionFlags flags);

ConversionResult ConvertUTF16toUTF8 (
		const UTF16** sourceStart, const UTF16* sourceEnd, 
		UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags);

Boolean isLegalUTF8Sequence(const UTF8 *source, const UTF8 *sourceEnd);

#ifdef __cplusplus
}
#endif
//end ws 12mar08

#endif//_FOUNDATION_H

/*************************************************************************************************
 *
 *   $History: foundation.h $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 7:39a
 * Updated in $/DD Tools/DDB/Common
 * Update Header and footer to HART cod standard
 * 
 *************************************************************************************************
 */
