/*************************************************************************************************
 *
 * $Workfile: Endian.h$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2003, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		definitions of endian converters
 *		04/30/03	sjv	started creation
 */

#define USS(a) ((unsigned short)(a))
#define ULL(b) ((unsigned long)(b))

#ifdef IS_BIG_ENDIAN /* NOT INTEL */
#else				 /* IS  INTEL */

// #define REVERSE_S(s) { unsigned short sl = (( ((unsigned short)s) & 0x00FF) << 8);\
// (s)=( ( ((unsigned short)s)>>8 ) | sl); }
// #define REVERSE_L(l) { unsigned long s2 = (((unsigned long)l)&0xFFFF);REVERSE_S(s2);\
// s2<<= 16; l = (((unsigned long)l) >> 16); REVERSE_S(l); l|=s2;}


// in-situ converters

#define REVERSE_S(s) (  ( USS(s) << 8 ) | ( USS(s) >> 8 )  )

#define REVERSE_L(l) (  ( ULL(REVERSE_S(ULL(l))) << 16) | ( REVERSE_S(ULL(l) >> 16) &0xFFFF)  )
//                                   note: the mask in the second half is due to compiler error

/* FLOATING NOTES */
/*   float to reversed UINT32::   REVERSE_L( *((unsigned long*)(&fVariable)) );//via pointer-cast
	 or through an intermediate:: memcpy(&uINT32,&fVariable,sizeof(fVariable)); REVERSE_L(uINT32);

     UINT32 to reversed float::   this MUST go through an intermediate variable since it needs to 
								  be pointer-cast (or memcopied) from a reversed int to a float 
								  and one cannot take the address of an equation.

							::	  tempINT32 = REVERSE_L(incomingUINT32); 
							::	                              finalFLOAT = *((float*) &tempINT32);
*/

#endif
/*************************************************************************************************
 *
 *   $History: Endian.h $
 * 
 * *****************  Version 1  *****************
 * User: Stevev       Date: 5/05/03    Time: 3:28p
 * Created in $/DD Tools/DDB/Common
 * 
 *************************************************************************************************
 */