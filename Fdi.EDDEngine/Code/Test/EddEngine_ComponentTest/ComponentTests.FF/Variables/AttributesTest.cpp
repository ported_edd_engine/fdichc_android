#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"
#include <wtypes.h>


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_loading_and_reading_an_int_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem_int;
            static ITEM_ID* test_int_item_ID;
            static FLAT_VAR* test_int;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_int", &test_var_specifier);
                genericItem_int = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_int = dynamic_cast<FLAT_VAR*>(genericItem_int->item);

                // get the item ids for parameters
                test_int_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_int", test_int_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_int_item_ID;
                delete test_block_item_ID;
                delete genericItem_int;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_int()
            {
                Assert::AreEqual((int) VT_INTEGER, (int) test_int->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_int_parameter()
            {
                Assert::AreEqual(*test_int_item_ID, test_int->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_int()
            {
                Assert::AreEqual((int) 4, (int) test_int->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_float_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_float_item_ID;
            static FLAT_VAR* test_float;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_float", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_float = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_float_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_float", test_float_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_float_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_float()
            {
                Assert::AreEqual((int) VT_FLOAT, (int) test_float->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_float_parameter()
            {
                Assert::AreEqual(*test_float_item_ID, test_float->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_float()
            {
                Assert::AreEqual((int) 4, (int) test_float->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_double_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_double_item_ID;
            static FLAT_VAR* test_double;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_double", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_double = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_double_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_double", test_double_item_ID);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_double_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_double()
            {
                Assert::AreEqual((int) VT_DOUBLE, (int) test_double->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_double_parameter()
            {
                Assert::AreEqual(*test_double_item_ID, test_double->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_double()
            {
                Assert::AreEqual((int) 8, (int) test_double->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_enumerated_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_enumerated_item_ID;
            static FLAT_VAR* test_enumerated;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {

                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");
                
                engine->Initialize();
                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_enumerated", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_enumerated = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_enumerated_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_enumerated", test_enumerated_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_enumerated_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_enumerated()
            {
                Assert::AreEqual((int) VT_ENUMERATED, (int) test_enumerated->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_enumerated_parameter()
            {
                Assert::AreEqual(*test_enumerated_item_ID, test_enumerated->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_enumerated()
            {
                Assert::AreEqual((int) 1 , (int) test_enumerated->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_bit_enumerated_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_bit_enumerated_item_ID;
            static FLAT_VAR* test_bit_enumerated;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_bit_enumerated", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_bit_enumerated = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_bit_enumerated_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_bit_enumerated", test_bit_enumerated_item_ID);                

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_bit_enumerated_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_bit_enumerated()
            {
                Assert::AreEqual((int) VT_BIT_ENUMERATED, (int) test_bit_enumerated->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_bit_enumerated_parameter()
            {
                Assert::AreEqual(*test_bit_enumerated_item_ID, test_bit_enumerated->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_bit_enumerated()
            {
                Assert::AreEqual((int) 1, (int) test_bit_enumerated->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_ascii_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_ascii_item_ID;
            static FLAT_VAR* test_ascii;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_ascii", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_ascii = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_ascii_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_ascii", test_ascii_item_ID);                

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_ascii_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_ascii()
            {
                Assert::AreEqual((int) VT_ASCII, (int) test_ascii->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_ascii_parameter()
            {
                Assert::AreEqual(*test_ascii_item_ID, test_ascii->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_ascii()
            {
                Assert::AreEqual((int) 10, (int) test_ascii->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_uint_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem_uint;
            static ITEM_ID* test_uint_item_ID;
            static FLAT_VAR* test_uint;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_uint", &test_var_specifier);
                genericItem_uint = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_uint = dynamic_cast<FLAT_VAR*>(genericItem_uint->item);

                // get the parameter's item ids
                test_uint_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_uint", test_uint_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_uint_item_ID;
                delete test_block_item_ID;
                delete genericItem_uint;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_uint()
            {
                Assert::AreEqual((int) VT_UNSIGNED, (int) test_uint->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_uint_parameter()
            {
                Assert::AreEqual(*test_uint_item_ID, test_uint->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_uint()
            {
                Assert::AreEqual((int) 4, (int) test_uint->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_boolean_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_boolean_item_ID;
            static FLAT_VAR* test_boolean;
            static VariableType test_boolean_type;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_boolean", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_boolean = dynamic_cast<FLAT_VAR*>(genericItem->item);

                //get the ParamType for all parameters
                TYPE_SIZE test_boolean_ParamType;
                int errorCode = engine->Instance->GetParamType(iBlockInstance, &test_var_specifier, &test_boolean_ParamType);
                test_boolean_type = test_boolean_ParamType.type;
                Assert::AreEqual(0, errorCode);

                // get the parameter's item ids
                test_boolean_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_boolean", test_boolean_item_ID);  

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_boolean_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_boolean()
            {
                Assert::AreEqual((int) VT_BOOLEAN, (int) test_boolean->type_size.type);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void GetParamType_should_return_VT_BOOLEAN_for_test_boolean()
            {
                Assert::AreEqual((int) VT_BOOLEAN, (int) test_boolean_type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_boolean_parameter()
            {
                Assert::AreEqual(*test_boolean_item_ID, test_boolean->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_size_for_the_test_boolean()
            {
                Assert::AreEqual((int) 1, (int) test_boolean->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_packed_euc_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_packed_ascii_item_ID;
            static FLAT_VAR* test_packed_ascii;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();
                
                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_packed_ascii", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_packed_ascii = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_packed_ascii_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_packed_ascii", test_packed_ascii_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_packed_ascii_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_packed_ascii()
            {
                Assert::AreEqual((int) VT_EUC, (int) test_packed_ascii->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_packed_ascii_parameter()
            {
                Assert::AreEqual(*test_packed_ascii_item_ID, test_packed_ascii->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_packed_ascii()
            {
                Assert::AreEqual((int) 10, (int) test_packed_ascii->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_password_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_password_item_ID;
            static FLAT_VAR* test_password;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_password", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_password = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_password_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_password", test_password_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_password_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_password()
            {
                Assert::AreEqual((int) VT_PASSWORD, (int) test_password->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_password_parameter()
            {
                Assert::AreEqual(*test_password_item_ID, test_password->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_password()
            {
                Assert::AreEqual((int) 10, (int) test_password->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_visiblestring_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_visiblestring_item_ID;
            static FLAT_VAR* test_visiblestring;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_visiblestring", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_visiblestring = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_visiblestring_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_visiblestring", test_visiblestring_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_visiblestring_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_visiblestring()
            {
                Assert::AreEqual((int) VT_VISIBLESTRING, (int) test_visiblestring->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_visiblestring_parameter()
            {
                Assert::AreEqual(*test_visiblestring_item_ID, test_visiblestring->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_visiblestring()
            {
                Assert::AreEqual((int) 10, (int) test_visiblestring->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_bitstring_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_bitstring_item_ID;
            static FLAT_VAR* test_bitstring;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_bitstring", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_bitstring = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_bitstring_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_bitstring", test_bitstring_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_bitstring_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_bitstring()
            {
                Assert::AreEqual((int) VT_BITSTRING, (int) test_bitstring->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_bitstring_parameter()
            {
                Assert::AreEqual(*test_bitstring_item_ID, test_bitstring->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_bitstring()
            {
                Assert::AreEqual((int) 24, (int) test_bitstring->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_octetstring_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_octetstring_item_ID;
            static FLAT_VAR* test_octetstring;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_octetstring", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_octetstring = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_octetstring_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_octetstring", test_octetstring_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_octetstring_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_octetstring()
            {
                Assert::AreEqual((int) VT_OCTETSTRING, (int) test_octetstring->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_octetstring_parameter()
            {
                Assert::AreEqual(*test_octetstring_item_ID, test_octetstring->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_octetstring()
            {
                Assert::AreEqual((int) 10, (int) test_octetstring->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_date_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_date_item_ID;
            static FLAT_VAR* test_date;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();
                
                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_date", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_date = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_date_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_date", test_date_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_date_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_date()
            {
                Assert::AreEqual((int) VT_EDD_DATE, (int) test_date->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_date_parameter()
            {
                Assert::AreEqual(*test_date_item_ID, test_date->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_date()
            {
                Assert::AreEqual((int) 3, (int) test_date->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_time_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_time_item_ID;
            static FLAT_VAR* test_time;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_time", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_time = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_time_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_time", test_time_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_time_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_time()
            {
                Assert::AreEqual((int) VT_TIME, (int) test_time->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_time_parameter()
            {
                Assert::AreEqual(*test_time_item_ID, test_time->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_time()
            {
                Assert::AreEqual((int) 6, (int) test_time->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_date_and_time_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_date_and_time_item_ID;
            static FLAT_VAR* test_date_and_time;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_date_and_time", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_date_and_time = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_date_and_time_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_date_and_time", test_date_and_time_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_date_and_time_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_date_and_time()
            {
                Assert::AreEqual((int) VT_DATE_AND_TIME, (int) test_date_and_time->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_date_and_time_parameter()
            {
                Assert::AreEqual(*test_date_and_time_item_ID, test_date_and_time->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_date_and_time()
            {
                Assert::AreEqual((int) 7, (int) test_date_and_time->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_time_value_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_time_value_item_ID;
            static FLAT_VAR* test_time_value;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_time_value", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_time_value = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_time_value_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_time_value", test_time_value_item_ID);                

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_time_value_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_time_value()
            {
                Assert::AreEqual((int) VT_TIME_VALUE, (int) test_time_value->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_time_value_parameter()
            {
                Assert::AreEqual(*test_time_value_item_ID, test_time_value->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_time_value()
            {
                Assert::AreEqual((int) 8, (int) test_time_value->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_duration_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_duration_item_ID;
            static FLAT_VAR* test_duration;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_duration", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                test_duration = dynamic_cast<FLAT_VAR*>(genericItem->item);

                // get the parameter's item ids
                test_duration_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_duration", test_duration_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_duration_item_ID;
                delete test_block_item_ID;
                delete genericItem;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type_for_the_test_duration()
            {
                Assert::AreEqual((int) VT_DURATION, (int) test_duration->type_size.type);
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_duration_parameter()
            {
                Assert::AreEqual(*test_duration_item_ID, test_duration->id);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_correct_type__size_for_the_test_duration()
            {
                Assert::AreEqual((int) 6, (int) test_duration->type_size.size);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_an_array_double_parameter_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static ITEM_ID* test_array_double_item_ID;
            static FLAT_ARRAY* test_array_double;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // get the parameters                
                FDI_PARAM_SPECIFIER test_var_specifier;
                engine->FillParamSpecifier("test_array_double", &test_var_specifier);
                genericItem = engine->GetGenericItem(
                    test_var_specifier.id, 
                    nsEDDEngine::ITYPE_ARRAY,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_ARRAY),
                    iBlockInstance);

                test_array_double = dynamic_cast<FLAT_ARRAY*>(genericItem->item);

                // get the parameter's item ids
                test_array_double_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_array_double", test_array_double_item_ID);
                
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_array_double_item_ID;
                delete test_block_item_ID;
            }

            [TestMethod, TestCategory("ParameterTest")]
            void It_should_have_the_correct_item_id_for_the_test_array_double_parameter()
            {
                Assert::AreEqual(*test_array_double_item_ID, test_array_double->id);
            }
        };

        [TestClass]
        public ref class When_loading_and_reading_a_block_with_multiple_parameters_in_FF
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* test_block_item_ID;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_BLOCK* test_block;
            static ITEM_ID* test_int_item_ID;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("FF\\TestCase0002\\TestCase0002.ff6");                
                engine->Initialize();                

                // get the ItemIds for the test_blocks
                test_block_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_block", test_block_item_ID);
                
                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::FUNCTION_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = *test_block_item_ID;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                genericItem = engine->GetGenericItem(
                    *test_block_item_ID,
                    nsEDDEngine::ITYPE_BLOCK,
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_BLOCK),
                    iBlockInstance);

                test_block = dynamic_cast<FLAT_BLOCK*>(genericItem->item);
                
                // get the item ids for test_int
                test_int_item_ID = new ITEM_ID();
                engine->Instance->GetItemIdFromSymbolName(L"test_int", test_int_item_ID);
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem;
                delete test_block_item_ID;
                delete test_int_item_ID;
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_label()
            {
                Assert::AreEqual("Test Block", gcnew String(test_block->label.c_str()));
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_the_expected_help_text()
            {
                Assert::AreEqual("Help Test Block", gcnew String(test_block->help.c_str()));
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_18_local_Parameter_in_list()
            {
                Assert::AreEqual(18u, (UINT32) test_block->local_param.count);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_19_parameter_in_list()
            {
                Assert::AreEqual(19u, (UINT32) test_block->param.count);
            }

            [TestMethod, TestCategory("AttributeTest")]
            void It_should_have_as_second_parameter_in_list_the_test_int_value()
            {
                Assert::AreEqual(*test_int_item_ID, test_block->param.list[1].ref.id);
            }
            
        };
    } // namespace FF
} // namespace ComponentTests
