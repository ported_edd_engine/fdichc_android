#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace ProfiBus
    {
        [TestClass]
        public ref class When_request_a_GRAPH_from_PB
        {
        public:
            static FLAT_GRAPH* test_graph_1;
            static FLAT_GRAPH* test_graph_2;
            static FLAT_GRAPH* test_graph_3;
            static FDI_GENERIC_ITEM* genericItem_graph_1;
            static FDI_GENERIC_ITEM* genericItem_graph_2;
            static FDI_GENERIC_ITEM* genericItem_graph_3;
            static EddEngineController^ engine;
            static ITEM_ID* test_graph_axis_1_item_ID;
            static ITEM_ID* test_graph_axis_2_item_ID;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0005.DDL.bin");
                engine->Initialize();                

                FDI_PARAM_SPECIFIER test_var_graph_1_specifier;
                engine->FillParamSpecifier("graph_1", &test_var_graph_1_specifier);
                genericItem_graph_1 = engine->GetGenericItem(test_var_graph_1_specifier.id, nsEDDEngine::ITYPE_GRAPH);
                test_graph_1 = dynamic_cast<FLAT_GRAPH*>(genericItem_graph_1->item);

                FDI_PARAM_SPECIFIER test_var_graph_2_specifier;
                engine->FillParamSpecifier("graph_2", &test_var_graph_2_specifier);
                genericItem_graph_2 = engine->GetGenericItem(test_var_graph_2_specifier.id, nsEDDEngine::ITYPE_GRAPH);
                test_graph_2 = dynamic_cast<FLAT_GRAPH*>(genericItem_graph_2->item);

                FDI_PARAM_SPECIFIER test_var_graph_3_specifier;
                engine->FillParamSpecifier("graph_3", &test_var_graph_3_specifier);
                genericItem_graph_3 = engine->GetGenericItem(test_var_graph_3_specifier.id, nsEDDEngine::ITYPE_GRAPH);
                test_graph_3 = dynamic_cast<FLAT_GRAPH*>(genericItem_graph_3->item);

                // get the item ids for all parameters
                test_graph_axis_2_item_ID = new ITEM_ID();
                int returnCode =   engine->Instance->GetItemIdFromSymbolName(L"graph_axis_2", test_graph_axis_2_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
                test_graph_axis_1_item_ID = new ITEM_ID();
                returnCode = engine->Instance->GetItemIdFromSymbolName(L"graph_axis_1", test_graph_axis_1_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem_graph_1;
                delete genericItem_graph_2;
                delete genericItem_graph_3;

                delete test_graph_axis_1_item_ID;
                delete test_graph_axis_2_item_ID;
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_default_value_for_Height()
            {
                Assert::AreEqual(3, (int) test_graph_2->height);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_default_value_for_Width()
            {
                Assert::AreEqual(3, (int) test_graph_2->width);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_default_value_for_Validity()
            {
                Assert::AreEqual(1, (int) test_graph_2->valid);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_an_empty_help_attribute_for_graph_2()
            {
                Assert::AreEqual("", gcnew String(test_graph_2->help.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_an_empty_label_attribute_for_graph_2()
            {
                Assert::AreEqual("", gcnew String(test_graph_2->label.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_cycle_time_for_graph_1()
            {
                Assert::AreEqual(1500, (int) test_graph_1->cycle_time); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_cycle_time_for_graph_3()
            {
                Assert::AreEqual(3500, (int) test_graph_3->cycle_time); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_height_for_graph_1()
            {
                Assert::AreEqual(0, (int) test_graph_1->height);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_label_for_graph_1()
            {
                Assert::AreEqual("Graph 1", gcnew String(test_graph_1->label.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_label_for_graph_3()
            {
                Assert::AreEqual("label graph 3", gcnew String(test_graph_3->label.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_help_for_graph_1()
            {
                Assert::AreEqual("this is graph 1", gcnew String(test_graph_1->help.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_help_for_graph_3()
            {
                Assert::AreEqual("this is graph 3", gcnew String(test_graph_3->help.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_validity_for_graph_1()
            {
                Assert::AreEqual(1, (int)test_graph_1->valid);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_validity_for_graph_3()
            {
                Assert::AreEqual(0, (int)test_graph_3->valid);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_width_for_graph_1()
            {
                Assert::AreEqual(1, (int) test_graph_1->width);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_Item_Id_for_Axis_in_graph_1()
            {
                Assert::AreEqual(*test_graph_axis_1_item_ID,  test_graph_1->x_axis);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_Item_Id_for_Axis_in_graph_3()
            {
                Assert::AreEqual(*test_graph_axis_2_item_ID,  test_graph_3->x_axis);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_number_of_members_for_graph_1()
            {
                Assert::AreEqual(1, (int) test_graph_1->members.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_number_of_members_for_graph_2()
            {
                Assert::AreEqual(1, (int) test_graph_2->members.count);
            }
            [TestMethod, TestCategory("GraphTest")]
            void It_should_have_the_correct_number_of_members_for_graph_3()
            {
                Assert::AreEqual(8, (int) test_graph_3->members.count);
            }
        };

        [TestClass]
        public ref class When_request_a_WAVEFORM_from_PB
        {
        public:
            static FLAT_WAVEFORM* test_waveform_1;
            static FLAT_WAVEFORM* test_waveform_2;
            static FLAT_WAVEFORM* test_waveform_3;
            static FLAT_WAVEFORM* test_waveform_4;
            static FLAT_WAVEFORM* test_waveform_5;
            static FLAT_WAVEFORM* test_waveform_6;
            static FLAT_WAVEFORM* test_waveform_7;
            static FLAT_WAVEFORM* test_waveform_8;
            static FLAT_WAVEFORM* test_waveform_9;

            static FDI_GENERIC_ITEM* genericItem_waveform_1;
            static FDI_GENERIC_ITEM* genericItem_waveform_2;
            static FDI_GENERIC_ITEM* genericItem_waveform_3;
            static FDI_GENERIC_ITEM* genericItem_waveform_4;
            static FDI_GENERIC_ITEM* genericItem_waveform_5;
            static FDI_GENERIC_ITEM* genericItem_waveform_6;
            static FDI_GENERIC_ITEM* genericItem_waveform_7;
            static FDI_GENERIC_ITEM* genericItem_waveform_8;
            static FDI_GENERIC_ITEM* genericItem_waveform_9;

            static EddEngineController^  engine;

            static ITEM_ID* test_graph_axis_3_item_ID;

            static EVAL_VAR_VALUE* test_variable_uint_value;
            static EVAL_VAR_VALUE* test_variable_float_value;
            static EVAL_VAR_VALUE* test_variable_int_value;
            static FDI_PARAM_SPECIFIER* test_variable_int_specifier;
            static FDI_PARAM_SPECIFIER* test_variable_uint_specifier;
            static FDI_PARAM_SPECIFIER* test_variable_float_specifier;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                engine = gcnew EddEngineController("ProfiBus\\FDI_PB_Test_0005.DDL.bin");
                ParamCacheSimulator*  paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                //// obtain item specifier (id) test_variable_uint
                test_variable_uint_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_variable_uint", test_variable_uint_specifier);

                // create value for "test_variable_int"
                test_variable_uint_value = new EVAL_VAR_VALUE();
                test_variable_uint_value->type = VT_UNSIGNED;
                test_variable_uint_value->size = 4;
                test_variable_uint_value->val.i = 10;

                // configure ParamCacheSimulator to return special value for "test_variable_uint"
                paramCacheSimulator->SetParamValue(test_variable_uint_specifier, test_variable_uint_value);

                //// obtain item specifier (id) test_variable_int
                test_variable_int_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_variable_int", test_variable_int_specifier);

                // create value for "test_variable_int"
                test_variable_int_value = new EVAL_VAR_VALUE();
                test_variable_int_value->type = VT_INTEGER;
                test_variable_int_value->size = 4;
                test_variable_int_value->val.i = -23;

                // configure ParamCacheSimulator to return special value for "test_variable_int"
                paramCacheSimulator->SetParamValue(test_variable_int_specifier, test_variable_int_value);


                //// obtain item specifier (id) for "chart_min_value_1"
                test_variable_float_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_variable_float", test_variable_float_specifier);

                // create value for "chart_min_value_1"
                test_variable_float_value = new EVAL_VAR_VALUE();
                test_variable_float_value->type = VT_FLOAT;
                test_variable_float_value->size = 4;
                test_variable_float_value->val.f = 4.2f;

                // configure ParamCacheSimulator to return special value for "test_variable_float"
                paramCacheSimulator->SetParamValue(test_variable_float_specifier, test_variable_float_value);

                FDI_PARAM_SPECIFIER test_waveform_1_specifier;
                engine->FillParamSpecifier("waveform_1", &test_waveform_1_specifier);
                genericItem_waveform_1 = engine->GetGenericItem(test_waveform_1_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_1 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_1->item);

                FDI_PARAM_SPECIFIER test_waveform_2_specifier;
                engine->FillParamSpecifier("waveform_2", &test_waveform_2_specifier);
                genericItem_waveform_2 = engine->GetGenericItem(test_waveform_2_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_2 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_2->item);

                FDI_PARAM_SPECIFIER test_waveform_3_specifier;
                engine->FillParamSpecifier("waveform_3", &test_waveform_3_specifier);
                genericItem_waveform_3 = engine->GetGenericItem(test_waveform_3_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_3 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_3->item);

                FDI_PARAM_SPECIFIER test_waveform_4_specifier;
                engine->FillParamSpecifier("waveform_4", &test_waveform_4_specifier);
                genericItem_waveform_4 = engine->GetGenericItem(test_waveform_4_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_4 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_4->item);

                FDI_PARAM_SPECIFIER test_waveform_5_specifier;
                engine->FillParamSpecifier("waveform_5", &test_waveform_5_specifier);
                genericItem_waveform_5 = engine->GetGenericItem(test_waveform_5_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_5 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_5->item);

                FDI_PARAM_SPECIFIER test_waveform_6_specifier;
                engine->FillParamSpecifier("waveform_6", &test_waveform_6_specifier);
                genericItem_waveform_6 = engine->GetGenericItem(test_waveform_6_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_6 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_6->item);

                FDI_PARAM_SPECIFIER test_waveform_7_specifier;
                engine->FillParamSpecifier("waveform_7", &test_waveform_7_specifier);
                genericItem_waveform_7 = engine->GetGenericItem(test_waveform_7_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_7 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_7->item);

                FDI_PARAM_SPECIFIER test_waveform_8_specifier;
                engine->FillParamSpecifier("waveform_8", &test_waveform_8_specifier);
                genericItem_waveform_8 = engine->GetGenericItem(test_waveform_8_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_8 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_8->item);

                FDI_PARAM_SPECIFIER test_waveform_9_specifier;
                engine->FillParamSpecifier("waveform_9", &test_waveform_9_specifier);
                genericItem_waveform_9 = engine->GetGenericItem(test_waveform_9_specifier.id, nsEDDEngine::ITYPE_WAVEFORM);
                test_waveform_9 = dynamic_cast<FLAT_WAVEFORM*>(genericItem_waveform_9->item);

                // get the item ids for all parameters
                test_graph_axis_3_item_ID = new ITEM_ID();
                int returnCode = engine->Instance->GetItemIdFromSymbolName(L"graph_axis_3", test_graph_axis_3_item_ID);
                Assert::IsTrue(returnCode == 0, "GetItemIdFromSymbolName failed. ReturnCode = " + returnCode);
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete genericItem_waveform_1;
                delete genericItem_waveform_2;
                delete genericItem_waveform_3;
                delete genericItem_waveform_4;
                delete genericItem_waveform_5;
                delete genericItem_waveform_6;
                delete genericItem_waveform_7;
                delete genericItem_waveform_8;
                delete genericItem_waveform_9;

                delete test_graph_axis_3_item_ID;
                delete test_variable_uint_specifier;
                delete test_variable_float_specifier;
                delete test_variable_uint_value;
                delete test_variable_float_value;
                delete test_variable_int_value;
                delete test_variable_int_specifier;
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_default_value_for_emphasis()
            {
                Assert::AreEqual(0, (int) test_waveform_2->emphasis);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_default_value_for_handling()
            {
                Assert::AreEqual(3, (int) test_waveform_2->handling);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_default_value_for_lineType()
            {
                Assert::AreEqual(16, (int) test_waveform_2->line_type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_default_value_for_validity()
            {
                Assert::AreEqual(1, (int) test_waveform_2->valid);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_emphasis_for_waveform_1()
            {
                Assert::AreEqual(1, (int) test_waveform_1->emphasis);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_handling_for_waveform_1()
            {
                Assert::AreEqual(1, (int) test_waveform_1->handling);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_handling_for_waveform_3()
            {
                Assert::AreEqual(3, (int) test_waveform_3->handling);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_handling_for_waveform_4()
            {
                Assert::AreEqual(2, (int) test_waveform_4->handling);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_help_for_waveform_1()
            {
                Assert::AreEqual("this is waveform 1", gcnew String(test_waveform_1->help.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_points_for_waveform_1()
            {               
                Assert::AreEqual(4, (int) test_waveform_1->number_of_points);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_points_for_waveform_2()
            {
                Assert::AreEqual(1, (int) test_waveform_2->number_of_points);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_points_for_waveform_8()
            {
                Assert::AreEqual(4, (int) test_waveform_8->number_of_points);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_waveform_1()
            {
                // xy (0), yt (1), horizontal (2), vertical (3)
                Assert::AreEqual(1, (int) test_waveform_1->waveform_type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_waveform_2()
            {
                Assert::AreEqual(0, (int) test_waveform_2->waveform_type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_waveform_3()
            {
                Assert::AreEqual(2, (int) test_waveform_3->waveform_type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_waveform_4()
            {
                Assert::AreEqual(3, (int) test_waveform_4->waveform_type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_x_inital_value_for_waveform_1()
            {
                Assert::AreEqual(-5, (int) test_waveform_1->x_initial.val.i);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_x_increment_value_for_waveform_1()
            {
                Assert::AreEqual(5, (int) test_waveform_1->x_increment.val.u);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_y_values_for_waveform_1()
            {
                Assert::AreEqual(4, (int) test_waveform_1->y_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_y_values_for_waveform_2()
            {
                Assert::AreEqual(1, (int) test_waveform_2->y_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_x_values_for_waveform_2()
            {
                Assert::AreEqual(1, (int) test_waveform_2->x_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_y_values_for_waveform_3()
            {
                Assert::AreEqual(4, (int) test_waveform_3->y_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_x_values_for_waveform_4()
            {
                Assert::AreEqual(4, (int) test_waveform_4->x_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_first_x_value_in_waveform_2()
            {
                Assert::AreEqual((int)FDI_DATA_UNSIGNED, (int) test_waveform_2->x_values.list[0].type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_first_y_value_in_waveform_2()
            {
                Assert::AreEqual((int)FDI_DATA_UNSIGNED, (int) test_waveform_2->y_values.list[0].type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_value_for_first_y_value_in_waveform_2()
            {
                Assert::AreEqual(11, (int) test_waveform_2->y_values.list[0].data.uconst);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_value_for_first_x_value_in_waveform_2()
            {
                Assert::AreEqual((unsigned long)10, test_waveform_2->x_values.list[0].data.uconst);    
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_first_y_value_in_waveform_3()
            {
                //nsEDDEngine::FDI_DATA_REFERENCE
                Assert::AreEqual(1,(int) test_waveform_3->y_values.list[0].type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_third_y_value_in_waveform_3()
            {
                //nsEDDEngine::FDI_DATA_REFERENCE
                Assert::AreEqual(1,(int) test_waveform_3->y_values.list[2].type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_item_id_for_third_y_value_in_waveform_3()
            {
                Assert::AreEqual(test_variable_float_specifier->id, test_waveform_3->y_values.list[2].data.ref.op_info.id);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_x_key_points_in_waveform_3()
            {
                Assert::AreEqual(4, (int) test_waveform_3->key_x_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_y_key_points_in_waveform_3()
            {
                Assert::AreEqual(4, (int) test_waveform_3->key_y_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_type_for_first_x_key_points_in_waveform_3()
            {
                Assert::AreEqual(1, (int) test_waveform_3->key_x_values.list[0].type);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_item_id_of_second_y_key_points_in_waveform_3()
            {
                Assert::AreEqual(test_variable_int_specifier->id,  test_waveform_3->key_y_values.list[1].data.ref.op_info.id);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_item_id_for_first_y_value_in_waveform_1()
            {
                Assert::AreEqual(test_variable_uint_specifier->id, test_waveform_1->y_values.list[0].data.ref.op_info.id);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_item_id_for_second_y_value_in_waveform_1()
            {
                Assert::AreEqual(test_variable_int_specifier->id, test_waveform_1->y_values.list[1].data.ref.op_info.id);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_label_for_waveform_1()
            {
                Assert::AreEqual("this is WaveForm 1", gcnew String(test_waveform_1->label.c_str()));
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineColor_for_waveform_1()
            {
                Assert::AreEqual(255, (int)test_waveform_1->line_color);   
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineColor_for_waveform_3()
            {
                Assert::AreEqual(0x00FF00, (int) test_waveform_3->line_color); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_1()
            {
                Assert::AreEqual(17, (int) test_waveform_1->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_9()
            {
                Assert::AreEqual(19, (int) test_waveform_9->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_3()
            {
                Assert::AreEqual(16, (int) test_waveform_3->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_4()
            {
                Assert::AreEqual(2, (int) test_waveform_4->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_5()
            {
                Assert::AreEqual(3, (int) test_waveform_5->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_6()
            {
                Assert::AreEqual(4, (int) test_waveform_6->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_7()
            {
                Assert::AreEqual(5, (int) test_waveform_7->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_lineType_for_waveform_8()
            {
                Assert::AreEqual(6, (int) test_waveform_8->line_type); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_x_values_for_waveform_8()
            {
                Assert::AreEqual(4, (int) test_waveform_8->x_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_number_of_y_values_for_waveform_8()
            {
                Assert::AreEqual(4, (int) test_waveform_8->y_values.count);
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_validity_for_waveform_1()
            {
                Assert::AreEqual(1, (int) test_waveform_1->valid); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_validity_for_waveform_3()
            {
                Assert::AreEqual(0, (int) test_waveform_3->valid); 
            }

            [TestMethod, TestCategory("GraphTest")]
            void It_should_return_the_correct_yAxis_Item_Id_for_waveform_1()
            {
                Assert::AreEqual(*test_graph_axis_3_item_ID,  test_waveform_1->y_axis); 
            }
        };

    }
}
