#include "stdafx.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "MethodInterfaceDefs.h"
#include "MEE.h"
#include "varientTag.h"
DWORD CBuiltIn:: resolve_group_reference(DWORD dwItemId, DWORD dwIndex, itemType_t typeCheck)
{
	int  rc = SUCCESS;
    hCitemBase* pItm = NULL;
    hCgroupItemDescriptor*  pGID = NULL;
			
	DWORD dwRetVal = 0;
	itemID_t iId = 0;
	/* stevev 25jan07 - expand to handle all group item types */
	bool paramsOK = (typeCheck == iT_ItemArray  || // sjv 25jan07
					 typeCheck == iT_Collection || 
					 typeCheck == iT_File       || 
					 typeCheck == iT_Array      || 
					 typeCheck == iT_List        );
	
    rc = m_pDevice->getItemBySymNumber(dwItemId, &pItm);
    if ( rc == SUCCESS && pItm != NULL &&  paramsOK )
    {
/* stevev 25jan07 replaced the following */
/*Vibhor 120204: Start of Code*/
//				if(typeCheck == iT_Collection)
//				{
//					rc = ((hCcollection *)pItm)->getByIndex(dwIndex, &pGID );
//				}
//				else if(typeCheck == iT_ItemArray)
//				{
//					rc = ((hCitemArray *)pItm)->getByIndex(dwIndex, &pGID );
//				}
/*Vibhor 120204: End of Code*/	
		/* stevev 18feb08 - additional types with unique acess techniques */
		if (pItm->getIType() == iT_ItemArray  || 
			pItm->getIType() == iT_Collection || 
			pItm->getIType() == iT_File        )
		{
/* stevev 25jan07 end of remove, replaced with the following */
			rc = ((hCgrpItmBasedClass *)pItm)->getByIndex(dwIndex, &pGID );			
/* stevev 25jan07: end of replacement **/
			
            if ( rc == SUCCESS && pGID != NULL )
            {
                    hCreference localRef(pGID->getRef());
                    rc = localRef.resolveID(iId);
                    if ( rc != SUCCESS)
                    {
						iId = 0;
                    }
					/*<START>Fixing the leaks Added by ANOOP	09APR2004*/
					delete pGID;
					pGID=NULL;
				
					/*<END>Fixing the leaks Added by ANOOP 09APR2004*/
            }
		}
		else
		if (pItm->getIType() == iT_Array)
		{
			hCarray* pL = (hCarray*)pItm;
			if ( pL->isInGroup(dwIndex) )
			{
				hCitemBase* pI = pL->operator [](dwIndex);
				iId = pI->getID();
			}
			// else bad index - fall thru to error return
		}
		else
		if (pItm->getIType() == iT_List)
		{
			hClist* pL = (hClist*)pItm;
			if ( pL->isInGroup(dwIndex) )
			{
				hCitemBase* pI = pL->operator [](dwIndex);
				iId = pI->getID();
			}
			// else bad index - fall thru to error return
		}
		// else fall through to error handling
    }
	RAZE(pGID);// stevev via HOMZ 21feb07 - maybe alloc'd in getbyindex w/error
	return (DWORD)iId;
}

/*Vibhor 011103: Since these builtins don't have a return code the resolved value is 
 returned as such , which is 0 in case of resolution is unsuccessful; The invoke 
 function should check the return value of such builtins*/

//This function is used by FF to return element item ID of item array
unsigned long CBuiltIn:: resolve_array_ref2	(unsigned long ulBlockId, unsigned long ulBlockInst, unsigned long ulItemId, unsigned long ulIndex)
{
	ITEM_ID ulRefId = 0;

	if (ProtocolType::FF != m_pMeth->GetStartedProtocol())
	{
		ulRefId = m_pMeth->ReturnOrInsertGlobalToken(ulBlockId, ulBlockInst, ulItemId, ulIndex);
	}
	else
	{
		//must be Fieldbus here
		//check the item type first
		CValueVarient vtItemType{};
		if (m_pDevice->ReadParameterData2(&vtItemType, _T("Kind"), ulBlockId, ulBlockInst, ulItemId, ulIndex) == BLTIN_SUCCESS)
		{
			

			//builtin resolve_array_ref2() is used to resolve item array reference
			itemType_e iItemType = m_pDevice->GetItemType(CV_I4(&vtItemType));

			if (iItemType == iT_Array)
			{
				//if the item type is value array, just return item ID without resolving. Legacy FF TOK used it by mistake.
				ulRefId = ulItemId;
			}
			else
			{
				CValueVarient vtType{};
				if ( m_pDevice->ReadParameterData2( &vtType, _T("ItemID"), ulBlockId, ulBlockInst, ulItemId, ulIndex) == BLTIN_SUCCESS )
				{
					
					ulRefId = CV_UI4(&vtType);
				}
			}
		}
	}

	if (ulRefId == 0)
	{
		ulResolveStatus = BLTIN_BAD_ID;
	}
	else
	{
		ulResolveStatus = BLTIN_SUCCESS;
	}

	return ulRefId;
}

unsigned long CBuiltIn::resolve_record_ref2(unsigned long ulBlockId, unsigned long ulBlockInstance, unsigned long ulItemId, unsigned long ulIndex)
{
	unsigned long ulRetVal = 0;

	if (ProtocolType::FF != m_pMeth->GetStartedProtocol())
	{
		ulRetVal = m_pMeth->ReturnOrInsertGlobalToken(ulBlockId, ulBlockInstance, ulItemId, ulIndex);
	}
	else
	{
		//must be Fieldbus here
		CValueVarient vtType{};
		if ( m_pDevice->ReadParameterData2( &vtType, _T("ItemID"), ulBlockId, ulBlockInstance, ulItemId, ulIndex) == BLTIN_SUCCESS )
		{
			
			ulRetVal = CV_UI4(&vtType);
		}
	}

	if (ulRetVal == 0)
	{
		ulResolveStatus = BLTIN_BAD_ID;
	}
	else
	{
		ulResolveStatus = BLTIN_SUCCESS;
	}

	return ulRetVal;
}/*End resolve_record_ref2*/

//This function seems used by HART tokenizer only. Is it still used?
long CBuiltIn:: resolve_param_ref(long lItemId)
{
	
	if ( lItemId < 0xC0009F00 || lItemId > 0xC0009f62)
        {
	        /* error handling */
			ulResolveStatus = BLTIN_BAD_ID; 
            return 0;
        }
        else
        {
			long lValue = 0;
			DWORD dwItemValue, dwTemp;
			dwItemValue = (DWORD)lItemId;
			dwTemp = dwItemValue - 0x3fff9fef;
            return (long)dwTemp;
        }

}/*End resolve_param_ref*/

// Builtin resolve_selector_ref() is obsolete for FDI. But keep it for legacy FF.
ITEM_ID CBuiltIn:: resolve_selector_ref (ITEM_ID /*item_id*/, ITEM_ID /*index*/, ITEM_ID /*selector*/){
	
	ITEM_ID ulRetVal = 0;

	return ulRetVal;

}
