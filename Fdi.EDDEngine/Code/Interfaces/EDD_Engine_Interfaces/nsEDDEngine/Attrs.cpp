#include "stdafx.h"
#include "nsEDDEngine/Attrs.h"
#include "PlatformCommon.h"

 
// Binary default constructor
nsEDDEngine::BINARY::BINARY()
{
	this->m_length = 0;
	this->m_ptr = nullptr;
}

// Binary constructor with params
nsEDDEngine::BINARY::BINARY(const uchar* s,unsigned length)
{
	m_ptr = nullptr;
	m_length = 0;	
	assign(s, length);
}

// Binary Destructor
nsEDDEngine::BINARY::~BINARY()
{
		delete[] m_ptr; // free pointer
		m_ptr = nullptr;	
		m_length = 0;	// set length to 0
}

// Binary copy constructor
nsEDDEngine::BINARY::BINARY(const BINARY &pDdiBinary)
{
	m_length = 0;		// Set length to 0
	m_ptr = nullptr;

	assign(pDdiBinary.m_ptr, pDdiBinary.m_length);
}

// Assign function to copy passed data
nsEDDEngine::BINARY& nsEDDEngine::BINARY:: assign(const uchar* s,unsigned length)
{
	if(m_ptr != nullptr)
	{
        //BINARY::~BINARY(); // Clear string contents
        this->~BINARY(); // Clear string contents
	}

	if(s == nullptr)
	{
		return (*this);
	}

	m_ptr = new uchar[length];
	memcpy(m_ptr,s,length);
	this->m_length = length;
	return (*this);
}

// Binary object assignment operator
nsEDDEngine::BINARY& nsEDDEngine::BINARY:: operator=(const nsEDDEngine::BINARY &pDdiBinary)
{
	if(this != &pDdiBinary) // Check passed binary object is not same
	{
		assign(pDdiBinary.m_ptr, pDdiBinary.m_length);
	}
	return (*this); // Return calling object
}

// Returns the length of the binary string
unsigned nsEDDEngine::BINARY::length() const 
{
	return  m_length;
}

// Returns a pointer to the binary string
const uchar* nsEDDEngine::BINARY::ptr() const
{
	return m_ptr;
}

  


//Default constructor
nsEDDEngine::STRING::STRING()
{
	flags = FDI_NOT_SET;//Set string flag
	len = 0;					//Set length to 0
	str = nullptr;				//Set str pointer to null
}

//Constructor with wchar type input parameter
nsEDDEngine::STRING::STRING(const wchar_t* s)
{
	if(s == nullptr)
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
		return;
	}

	flags = FDI_FREE_STRING;	//Free memory as creating new buffer
	size_t tmpLen = wcslen(s);
	str = new wchar_t[tmpLen+1]; //Allocate wchar type memory

	if(str)
	{
		PS_Wcsncpy(str, (tmpLen+1), s, (tmpLen+1)); //Copy data
		len = wcslen(str); //Set string length
	}
	else
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
	}
}

//Constructor with char type input parameter
nsEDDEngine::STRING::STRING(const char* s)
{
	if(s == nullptr)
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
		return;
	}

	flags = FDI_FREE_STRING; //Free memory as creating new buffer
	size_t tmpLen = strlen(s);
	size_t rtnLen = 0;
	str = new wchar_t[tmpLen + 1]; //Allocate char type memory
	if(str)
	{
        PS_Mbtowcs(&rtnLen, str, (tmpLen + 1), s, (tmpLen + 1)); // Copy data
		len = wcslen(str); //Set string length
	}
	else
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
	}
}

//Constructor with STRING type input parameter
nsEDDEngine::STRING::STRING(const nsEDDEngine::STRING &pDdiString)
{
	AssignString(pDdiString); //Call to AssignString function
}

//Destructor	
nsEDDEngine::STRING::~STRING()
{
	//Check string set for free
	if (flags == FDI_FREE_STRING) 
	{	
		delete str;
	}

	//Reset class data
	flags = FDI_NOT_SET;
	len = 0;
	str = nullptr;	
}


//Returns the length of the string
unsigned nsEDDEngine::STRING::length() const 
{
	return  (unsigned) len;
}

//Returns a pointer to the string
const wchar_t* nsEDDEngine::STRING::c_str() const
{
	return (str);
}

//AssignString is used to assign all the attributes of STRING
void nsEDDEngine::STRING::AssignString(const nsEDDEngine::STRING &pDdiString)
{

	if (pDdiString.c_str() == nullptr)
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
		return;
	}

	len = pDdiString.len; //Get passed string length

	if (pDdiString.flags == FDI_FREE_STRING) //Check passed STRING object flags set to free string
	{
		flags  = FDI_FREE_STRING; //Set STRING flag to set free string
		str = new wchar_t[pDdiString.len + 1]; //Allocate wchar type memory
        PS_Wcscpy(str, pDdiString.len + 1, pDdiString.str); //Copy data
		if(str)
		{
		
			len = wcslen(str); //Set string length
		}
		else
		{
			flags = FDI_NOT_SET;//Set string flag
			len = 0;			//Set length to 0
			str = nullptr;		//Set str pointer to null
		}
	}
	else
	{
		flags  = FDI_DONT_FREE_STRING; //Set STRING flag to don't free string
		str	   = pDdiString.str; //Assign passed object str pointer to STRING class str pointer
		len = wcslen(str); //Set string length
	}

	
}

//Use to set string contents, but not allocate memory
nsEDDEngine::STRING& nsEDDEngine::STRING:: assign(const wchar_t* s,bool bCopy)
{
	if(s == nullptr)
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
		return (*this);
	}

	//Clear the existing string before using for new
	if (flags == FDI_FREE_STRING) //Check free string flag set
		{	
			delete str; //delete object
		}
	str = nullptr; //Set string value to null
	
	//Perform assignment operation
	if(true == bCopy) //Check copy flag null or not
	{
		str = new wchar_t[wcslen(s)+1]; //Allocate memory
        PS_Wcsncpy(str,(wcslen(s)+1),s,(wcslen(s) + 1)); //Copy data
		flags = FDI_FREE_STRING; //Set free string flag
	}
	else
	{
		flags = FDI_DONT_FREE_STRING; //Set don't free string flag
		str = (wchar_t*)s; //assign passed string pointer to str pointer
	}

	if(str)
	{
		len = wcslen(str); //Set string length
	}
	else
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
	}

	return (*this);
}

//Overloaded Assignment Operator for STRING
nsEDDEngine::STRING& nsEDDEngine::STRING:: operator=(const nsEDDEngine::STRING &pDdiString)
{
	if(this != &pDdiString) //Check passed STRING object is not same
	{
        //STRING::~STRING(); //Clear string contents
        this->~STRING(); //Clear string contents
		AssignString(pDdiString);//Call assign function to set all attributes to new STRING object
	}
	return (*this); //Return calling object
}

//Overloaded Assignment Operator for wchar_t
nsEDDEngine::STRING& nsEDDEngine::STRING:: operator=(const wchar_t* pDdiString)
{
	if(pDdiString !=nullptr)
	{

        //STRING::~STRING();//Clear string contents
        this->~STRING(); //Clear string contents
		flags = FDI_FREE_STRING; //Set free string flag
        str = new wchar_t[wcslen(pDdiString) + 1]{0}; //Allocate wchar type memory
        PS_Wcsncpy(str,(wcslen(pDdiString) + 1),pDdiString,(wcslen(pDdiString) + 1)); //Copy data
	
		if(str)
		{
			len = wcslen(str); //Set string length
		}
		else
		{
			flags = FDI_NOT_SET;//Set string flag
			len = 0;			//Set length to 0
			str = nullptr;		//Set str pointer to null
		}
	}
	else
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
	}
	return (*this); //Return calling object
}

//Overloaded Assignment Operator for char
nsEDDEngine::STRING& nsEDDEngine::STRING:: operator=(const char* pDdiString)
{
	if(pDdiString !=nullptr)
	{
        //STRING::~STRING();//Clear string contents
        this->~STRING(); //Clear string contents
        flags = FDI_FREE_STRING; //Set free string flag
		size_t rtnlen=0; 
		str = new wchar_t[strlen(pDdiString)+1]; //Allocate wchar type memory
        PS_Mbtowcs(&rtnlen,str,(strlen(pDdiString)+1),pDdiString,(strlen(pDdiString)+1)); //Copy data
		if(str)
		{
			len = wcslen(str); //Set string length
		}
		else
		{
			flags = FDI_NOT_SET;//Set string flag
			len = 0;			//Set length to 0
			str = nullptr;		//Set str pointer to null
		}
	}
	else
	{
		flags = FDI_NOT_SET;//Set string flag
		len = 0;			//Set length to 0
		str = nullptr;		//Set str pointer to null
	}
	return (*this); //Return calling object
}


//copy constructor for EXPR_VALUE
nsEDDEngine::EXPR_VALUE::EXPR_VALUE (const EXPR_VALUE &exprValue)
{
	AssignExprValue(exprValue);

}

//Overloaded Assignment Operator for EXPR_VALUE
nsEDDEngine::EXPR_VALUE& nsEDDEngine::EXPR_VALUE:: operator=(const EXPR_VALUE &exprValue)
{
	AssignExprValue(exprValue);
	return (*this);
}

//AssignExprValue is used to assign all the attributes of EXPR_VALUE
void nsEDDEngine::EXPR_VALUE:: AssignExprValue(const EXPR_VALUE &exprValue)
{
	f = exprValue.f;
	d = exprValue.d;
	u = exprValue.u;
	i = exprValue.i;
	s = exprValue.s;
	b = exprValue.b;  
}

//copy constructor for RESPONSE_CODE_LIST
nsEDDEngine::RESPONSE_CODE_LIST::RESPONSE_CODE_LIST(const RESPONSE_CODE_LIST &response_code_list)
{
	AssignResponseCodeList(response_code_list);
}

//Overloaded Assignment Operator for RESPONSE_CODE_LIST
nsEDDEngine::RESPONSE_CODE_LIST& nsEDDEngine::RESPONSE_CODE_LIST:: operator= (const RESPONSE_CODE_LIST &response_code_list)
{
	if(this != &response_code_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignResponseCodeList(response_code_list);
	}
	return *this;
}

//AssignResponseCodeList is used to assign all the attributes of list RESPONSE_CODE_LIST
void nsEDDEngine::RESPONSE_CODE_LIST::AssignResponseCodeList(const RESPONSE_CODE_LIST& response_code_list)
{
    count   = response_code_list.count;     // Count is the number of list entries used.
	limit   = response_code_list.count;     // This will never change in size so allocate count elements
    if (count == 0) //if count is zero no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new RESPONSE_CODE[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for RESPONSE_CODE is called
			list[i]  =  response_code_list.list[i];
        }
    }
}


//Copy Constructor for RESPONSE_CODES_VALUE
nsEDDEngine::RESPONSE_CODES_VALUE::RESPONSE_CODES_VALUE(const RESPONSE_CODES_VALUE &response_code_value)
{
	AssignResponseCodeValue(response_code_value);
}

//Overloaded Assignment Operator for RESPONSE_CODES_VALUE
nsEDDEngine::RESPONSE_CODES_VALUE& nsEDDEngine::RESPONSE_CODES_VALUE:: operator= (const RESPONSE_CODES_VALUE  &response_code_value)
{
	AssignResponseCodeValue(response_code_value);
	return *this;
}

//AssignResponseCodeValue is used to assign all the attributes of RESPONSE_CODES_VALUE
void nsEDDEngine::RESPONSE_CODES_VALUE::AssignResponseCodeValue(const  RESPONSE_CODES_VALUE  &response_code_value)
{
	resp_code_ref	= response_code_value.resp_code_ref;
	resp_code_list  = response_code_value.resp_code_list;
}


//Copy Constructor for RESPONSE_CODES
nsEDDEngine::RESPONSE_CODES::RESPONSE_CODES(const RESPONSE_CODES &responsecode_list)
{
	AssignResponseCodes(responsecode_list);
}

//Overloaded Assignment Operator for RESPONSE_CODES
nsEDDEngine::RESPONSE_CODES& nsEDDEngine::RESPONSE_CODES::operator= (const RESPONSE_CODES &responsecode_list)
{
	AssignResponseCodes(responsecode_list);
	return *this;
}

//AssignResponseCodes is used to assign all the attributes of RESPONSE_CODES
void nsEDDEngine::RESPONSE_CODES::AssignResponseCodes(const RESPONSE_CODES &responsecode_list)
{
	eType		   = responsecode_list.eType;
	response_codes = responsecode_list.response_codes;
}

nsEDDEngine::RESPONSE_CODES::~RESPONSE_CODES()
{
	if (this->eType == RESP_CODE_TYPE_LIST) 
	{
		this->response_codes.resp_code_list.Dispose();
	}
	else
	{
		this->response_codes.resp_code_ref = 0;
	}

	this->eType = RESP_CODE_TYPE_NONE;
}

void nsEDDEngine::RESPONSE_CODE_LIST::Dispose()
{
	this->limit = 0;
	this->count = 0;
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor for RESPONSE_CODE
nsEDDEngine::RESPONSE_CODE::RESPONSE_CODE(const RESPONSE_CODE &response_code)
{
	AssignResponseCode(response_code);
}

//Overloaded assignment operator for RESPONSE_CODE
nsEDDEngine::RESPONSE_CODE& nsEDDEngine::RESPONSE_CODE::operator= (const RESPONSE_CODE &response_code)
{
	AssignResponseCode(response_code);
	return *this;
}

//AssignResponseCode is used to assign all the attributes of RESPONSE_CODE
void nsEDDEngine::RESPONSE_CODE::AssignResponseCode(const RESPONSE_CODE &response_code)
{
	evaled  = response_code.evaled;
	val		= response_code.val;
	type	= response_code.type;
	desc	= response_code.desc;
	help	= response_code.help;
}

nsEDDEngine::RESPONSE_CODE::~RESPONSE_CODE()
{
}

nsEDDEngine::OP_REF::OP_REF()
{
    op_ref_type = STANDARD_TYPE;
    // op_info has own constructor
    // op_info_list has own constructor
    block_instance = 0;
};


//Copy Constructor for OP_REF
nsEDDEngine::OP_REF::OP_REF(const OP_REF &op_ref)
{
	AssignOp_Ref(op_ref);
}

//Overloaded assignment operator for OP_REF
 nsEDDEngine::OP_REF& nsEDDEngine::OP_REF:: operator= (const OP_REF &op_ref)
{
	if (this != &op_ref)
	{
		AssignOp_Ref(op_ref);
	}
	return *this;
}


nsEDDEngine::OP_REF& nsEDDEngine::OP_REF:: operator= (const OP_REF_TRAIL &op_ref_trail)
{
	AssignOp_Ref(op_ref_trail);
	return *this;
}


 //AssignOp_Ref is used to assign all the attributes of OP_REF
 void nsEDDEngine::OP_REF::AssignOp_Ref(const OP_REF &op_ref)
 {
	 op_ref_type     =  op_ref.op_ref_type;
	 op_info         =  op_ref.op_info;
	 op_info_list    =  op_ref.op_info_list;

	 block_instance	 =  op_ref.block_instance;
 }

 void nsEDDEngine::OP_REF::AssignOp_Ref(const OP_REF_TRAIL &op_ref_trail)
 {
	 op_ref_type		= op_ref_trail.op_ref_type;
	 op_info				= op_ref_trail.op_info;
	 op_info_list		= op_ref_trail.op_info_list;

	 block_instance	= op_ref_trail.block_instance; 
 }

nsEDDEngine::OP_REF::~OP_REF()
{
    op_ref_type = STANDARD_TYPE;
    // op_info has own destructor
    // op_info_list has own destructor
    block_instance = 0;
}

//Copy Constructor for OP_REF_LIST
nsEDDEngine::OP_REF_LIST::OP_REF_LIST(const OP_REF_LIST &op_ref_list)
{
	AssignOpRefList(op_ref_list);
}

//Overloaded assignment operator for OP_REF_LIST
nsEDDEngine::OP_REF_LIST& nsEDDEngine::OP_REF_LIST::operator= (const OP_REF_LIST &op_ref_list)
{
	if(this != &op_ref_list)
	{
		if (this->list != nullptr)
		{
			free (this->list);
			this->list = nullptr;
		}

		AssignOpRefList(op_ref_list);
	}
	return *this;
}

//AssignOpRefList is used to assign all the attributes of list OP_REF_LIST
void nsEDDEngine::OP_REF_LIST::AssignOpRefList(const OP_REF_LIST &op_ref_list)
{
	count = op_ref_list.count;
	limit = op_ref_list.count;   // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
		list		= (nsEDDEngine::OP_REF *) malloc((size_t) (count * sizeof(nsEDDEngine::OP_REF)));

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for OP_REF is called
			list[i] = op_ref_list.list[i];
		}
	}

}

nsEDDEngine::OP_REF_LIST::~OP_REF_LIST()
{
	if( this->list )
		free(this->list);
	this->list = nullptr;
	this->limit = 0;
	this->count = 0;
}

_INTERFACE
bool nsEDDEngine::operator<(const nsEDDEngine::OP_REF_INFO &_Left, const nsEDDEngine::OP_REF_INFO &_Right)
{
	bool bRet = false;

	if (_Left.id < _Right.id)
	{
		bRet = true;
	}
	else if ( (_Left.id == _Right.id) && (_Left.member < _Right.member) )
	{
		bRet = true;
	}

	return bRet;
}



_INTERFACE
bool nsEDDEngine::operator==(const nsEDDEngine::OP_REF_INFO &_Left, const nsEDDEngine::OP_REF_INFO &_Right)
{
	bool bRet = false;

	if ( (_Left.id == _Right.id) && (_Left.member == _Right.member) )
	{
		bRet = true;
	}

	return bRet;
}

_INTERFACE
bool nsEDDEngine::operator!=(const nsEDDEngine::OP_REF_INFO &_Left, const nsEDDEngine::OP_REF_INFO &_Right)
{
	bool bRet = !(_Left == _Right);

	return bRet;
}


// copy constructor for OP_REF_INFO_LIST
nsEDDEngine::OP_REF_INFO_LIST::OP_REF_INFO_LIST( const OP_REF_INFO_LIST &op_ref_info_list )
{
    AssignOpRefInfoList(op_ref_info_list);
}

// Overloaded Assignment Operator for OP_REF_INFO_LIST
nsEDDEngine::OP_REF_INFO_LIST& nsEDDEngine::OP_REF_INFO_LIST::operator= (const OP_REF_INFO_LIST &op_ref_info_list )
{
	if (this != &op_ref_info_list)
    {
        delete [] this->list;
        this->list = nullptr;
        AssignOpRefInfoList(op_ref_info_list);
    }
    return *this;
}
// AssignOpRefInfoList is used to assign all the attributes of list OP_REF_INFO_LIST
void nsEDDEngine::OP_REF_INFO_LIST::AssignOpRefInfoList( const OP_REF_INFO_LIST &op_ref_info_list )
{
    count   = op_ref_info_list.count;
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list		= new OP_REF_INFO[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for MEMBER is called
			list[i]   = op_ref_info_list.list[i];
		}
	}

}

nsEDDEngine::OP_REF_INFO_LIST::OP_REF_INFO_LIST()
{
	count	= 0;
	list	= nullptr;
}

// destructor for OP_REF_INFO_LIST
nsEDDEngine::OP_REF_INFO_LIST::~OP_REF_INFO_LIST()
{
	delete [] this->list;
	this->list = nullptr;
	this->count = 0;
}

nsEDDEngine::OP_REF_INFO::OP_REF_INFO()
{
	id		= 0;
	member	= 0;
	type	= ITYPE_NO_VALUE;
};
nsEDDEngine::OP_REF_INFO::~OP_REF_INFO()
{
	id		= 0;
	member	= 0;
	type	= ITYPE_NO_VALUE;
};

// constructor for FDI_PARAM_SPECIFIER
nsEDDEngine::FDI_PARAM_SPECIFIER::FDI_PARAM_SPECIFIER()
{
	eType		= FDI_PS_ITEM_ID;
	id			= 0;
	param		= 0;
	subindex	= 0;
	// RefList has own constructor
}

// copy constructor for FDI_PARAM_SPECIFIER
nsEDDEngine::FDI_PARAM_SPECIFIER::FDI_PARAM_SPECIFIER( const FDI_PARAM_SPECIFIER &rhs )
{
	this->eType		= rhs.eType;
	this->id		= rhs.id;
	this->param		= rhs.param;
	this->subindex	= rhs.subindex;
	this->RefList	= rhs.RefList;
}

// destructor for FDI_PARAM_SPECIFIER
nsEDDEngine::FDI_PARAM_SPECIFIER::~FDI_PARAM_SPECIFIER()
{
	eType		= FDI_PS_ITEM_ID;
	id			= 0;
	param		= 0;
	subindex	= 0;
	// RefList has own destructor
}

_INTERFACE
bool nsEDDEngine::operator<(const nsEDDEngine::FDI_PARAM_SPECIFIER &_Left, const nsEDDEngine::FDI_PARAM_SPECIFIER &_Right)
{
	bool bRet = false;

	if (_Left.eType < _Right.eType)			// First compare the eType
	{
		bRet = true;
	}
	else if (_Left.eType == _Right.eType)	// If eType is the same, compare correct contents
	{
		switch (_Left.eType)
		{
		case FDI_PS_ITEM_ID:
			bRet = (_Left.id < _Right.id);
			break;

		case FDI_PS_PARAM_OFFSET:
			bRet = (_Left.param < _Right.param);
			break;

		case FDI_PS_ITEM_ID_SI:
			if (_Left.id < _Right.id )
			{
				bRet = true;
			}
			else if (_Left.id == _Right.id)
			{
				bRet = (_Left.subindex < _Right.subindex);
			}
			break;

		case FDI_PS_PARAM_OFFSET_SI:
			if (_Left.param < _Right.param )
			{
				bRet = true;
			}
			else if (_Left.param == _Right.param)
			{
				bRet = (_Left.subindex < _Right.subindex);
			}
			break;

		case FDI_PS_ITEM_ID_COMPLEX:
			if (_Left.RefList.count < _Right.RefList.count)
			{
				bRet = true;
			}
			else if (_Left.RefList.count == _Right.RefList.count)
			{
				int i = 0;
				for (    ; i < _Left.RefList.count; i++)	// Loop through the RefInfo list
				{
					if (_Left.RefList.list[i] == _Right.RefList.list[i])
					{
						continue;		// If this entry is the same, continue
					}
					else if (_Left.RefList.list[i] < _Right.RefList.list[i])	// If this is less
					{
						bRet = true;	// If this is "less than", exit with "true".
						break;
					}
					else
					{
						bRet = false;	// If this is "greater than", exit with "false"
						break;
					}
				}

				if (i == _Left.RefList.count)	// If we got through the whole list, they are the same
				{
					bRet = false;		// If the list was the same, exit with "false"
				}
			}
			break;
		}
	}

	return bRet;
}


//copy constructor for MEMBER_LIST
nsEDDEngine::MEMBER_LIST::MEMBER_LIST(const MEMBER_LIST &member_list)
{
	AssignMemberList(member_list);
}

//Overloaded Assignment Operator for MEMBER_LIST
nsEDDEngine::MEMBER_LIST& nsEDDEngine::MEMBER_LIST:: operator= (const MEMBER_LIST &member_list)
{
	if(this != &member_list)
	{
		delete [] this->list;
		this->list = nullptr;
	
		AssignMemberList(member_list);
	}

	return *this;
}

//AssignMemberList is used to assign all the attributes of list MEMBER_LIST
void nsEDDEngine::MEMBER_LIST::AssignMemberList(const MEMBER_LIST &member_list)
{
	count	= member_list.count;
	limit   = member_list.count; // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list		= new MEMBER[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for MEMBER is called
			list[i]   = member_list.list[i];
		}
	}
}

nsEDDEngine::MEMBER_LIST::~MEMBER_LIST()
{
	delete [] this->list;
	this->list = nullptr;
	this->limit = 0;
	this->count = 0;
}

//Copy Constructor for MEMBER
nsEDDEngine::MEMBER::MEMBER(const MEMBER &member)
{
	AssignMember(member);
}

//Overloaded Assignment Operator for MEMBER
nsEDDEngine::MEMBER& nsEDDEngine::MEMBER:: operator= (const MEMBER &member)
{
	AssignMember(member);
	return *this;
}

//AssignMember is used to assign all the attributes of MEMBER
void nsEDDEngine::MEMBER::AssignMember(const MEMBER &member)
{
	evaled = member.evaled;
	name   = member.name;
	ref    = member.ref;
	desc   = member.desc;
	help   = member.help;
}

nsEDDEngine::MEMBER::~MEMBER()
{
	// this->ref has own constructor no need to call explicitly.
}


//copy constructor for OP_MEMBER_LIST
nsEDDEngine::OP_MEMBER_LIST::OP_MEMBER_LIST(const OP_MEMBER_LIST &op_member_list)
{
	AssignOpMemberList(op_member_list);
}

//Overloaded Assignment Operator for OP_MEMBER_LIST
nsEDDEngine::OP_MEMBER_LIST& nsEDDEngine::OP_MEMBER_LIST:: operator= (const OP_MEMBER_LIST &op_member_list)
{
	if(this != &op_member_list)
	{
		delete [] this->list;
		this->list = nullptr;
	
		AssignOpMemberList(op_member_list);
	}

	return *this;
}

//AssignOpMemberList is used to assign all the attributes of list OP_MEMBER_LIST
void nsEDDEngine::OP_MEMBER_LIST::AssignOpMemberList(const OP_MEMBER_LIST &op_member_list)
{
	count	= op_member_list.count;
	limit   = op_member_list.count; // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list		= new OP_MEMBER[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for OP_MEMBER is called
			list[i]   = op_member_list.list[i];
		}
	}
}

nsEDDEngine::OP_MEMBER_LIST::~OP_MEMBER_LIST()
{
	delete [] this->list;
	this->list = nullptr;
	this->limit = 0;
	this->count = 0;
}

//Copy Constructor for OP_MEMBER
nsEDDEngine::OP_MEMBER::OP_MEMBER(const OP_MEMBER &op_member)
{
	AssignOpMember(op_member);
}

//Overloaded Assignment Operator for OP_MEMBER
nsEDDEngine::OP_MEMBER& nsEDDEngine::OP_MEMBER:: operator= (const OP_MEMBER &op_member)
{
	AssignOpMember(op_member);
	return *this;
}

//AssignMember is used to assign all the attributes of OP_MEMBER
void nsEDDEngine::OP_MEMBER::AssignOpMember(const OP_MEMBER &op_member)
{
	evaled = op_member.evaled;
	name   = op_member.name;
	ref    = op_member.ref;
	desc   = op_member.desc;
	help   = op_member.help;
}

nsEDDEngine::OP_MEMBER::~OP_MEMBER()
{

}

//copy constructor for ITEM_ARRAY_ELEMENT_LIST
nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST::ITEM_ARRAY_ELEMENT_LIST(const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list)
{
	AssignItemArrayElementList(item_array_element_list);
}

//Overloaded Assignment Operator for ITEM_ARRAY_ELEMENT_LIST
nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST& nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST:: operator= (const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list)
{
	if(this != &item_array_element_list)
	{
		delete [] this->list;
		this->list = nullptr;
	
		AssignItemArrayElementList(item_array_element_list);
	}

	return *this;
}

//AssignItemArrayElementList is used to assign all the attributes of list ITEM_ARRAY_ELEMENT_LIST
void nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST::AssignItemArrayElementList(const ITEM_ARRAY_ELEMENT_LIST &item_array_element_list)
{
	count	= item_array_element_list.count;
	limit   = item_array_element_list.count; // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
		list		= new ITEM_ARRAY_ELEMENT[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for ITEM_ARRAY_ELEMENT is called
			list[i]   = item_array_element_list.list[i];
		}
	}
}

nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST::~ITEM_ARRAY_ELEMENT_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor for ITEM_ARRAY_ELEMENT
nsEDDEngine::ITEM_ARRAY_ELEMENT::ITEM_ARRAY_ELEMENT(const ITEM_ARRAY_ELEMENT &item_array_element)
{
	AssignItemArrayElement(item_array_element);
}

//Overloaded Assignment Operator for ITEM_ARRAY_ELEMENT
nsEDDEngine::ITEM_ARRAY_ELEMENT& nsEDDEngine::ITEM_ARRAY_ELEMENT:: operator= (const ITEM_ARRAY_ELEMENT &item_array_element)
{
	AssignItemArrayElement(item_array_element);
	return *this;
}

//AssignItemArrayElement is used to assign all the attributes of ITEM_ARRAY_ELEMENT
void nsEDDEngine::ITEM_ARRAY_ELEMENT::AssignItemArrayElement(const ITEM_ARRAY_ELEMENT &item_array_element)
{
	evaled = item_array_element.evaled;
	index  = item_array_element.index;
	ref    = item_array_element.ref;
	desc   = item_array_element.desc;
	help   = item_array_element.help;
}

nsEDDEngine::ITEM_ARRAY_ELEMENT::~ITEM_ARRAY_ELEMENT()
{
}

//Copy Constructor for TRANSACTION
nsEDDEngine::TRANSACTION::TRANSACTION(const TRANSACTION &transaction)
{
	AssignTransaction(transaction);
}
		
//Overloaded Assignment Operator for TRANSACTION
nsEDDEngine::TRANSACTION& nsEDDEngine::TRANSACTION:: operator= (const TRANSACTION &transaction)
{

	AssignTransaction(transaction);
	return *this;
}

//AssignTransaction is used to assign all the attributes of TRANSACTION
void nsEDDEngine::TRANSACTION::AssignTransaction(const TRANSACTION &transaction)
{
	number		 = transaction.number;
	request		 = transaction.request;
	reply		 = transaction.reply;
	resp_codes   = transaction.resp_codes;
	post_actions = transaction.post_actions;
}

//Copy Constructor for TRANSACTION_LIST
nsEDDEngine::TRANSACTION_LIST::TRANSACTION_LIST(const TRANSACTION_LIST &transaction_list)
{
	AssignTransactionList(transaction_list);
}
		
//Overloaded Assignment Operator for TRANSACTION_LIST 
nsEDDEngine::TRANSACTION_LIST& nsEDDEngine::TRANSACTION_LIST:: operator= (const TRANSACTION_LIST &transaction_list)
{
	if(this != &transaction_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignTransactionList(transaction_list);
	}
	return *this;
}

//AssignTransactionList is used to assign all the attributes of list TRANSACTION_LIST
void nsEDDEngine::TRANSACTION_LIST::AssignTransactionList(const TRANSACTION_LIST &transaction_list)
{
	count	= transaction_list.count;
	limit   = transaction_list.count;  // This will never change in size so allocate count elements

	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new TRANSACTION[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for TRANSACTION is called
			list[i] = transaction_list.list[i];
		}
	}
}


nsEDDEngine::TRANSACTION_LIST::~TRANSACTION_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//copy constructor for DATA_ITEM_LIST
nsEDDEngine::DATA_ITEM_LIST::DATA_ITEM_LIST(const DATA_ITEM_LIST &data_item_list)
{
	AssignDataItemList(data_item_list);
}

//Overloaded Assignment Operator for DATA_ITEM_LIST
nsEDDEngine::DATA_ITEM_LIST& nsEDDEngine::DATA_ITEM_LIST:: operator= (const DATA_ITEM_LIST &data_item_list)
{
	if(this != &data_item_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignDataItemList(data_item_list);
	}
	return *this;
}

//AssignDataItemList is used to assign all the attributes of list DATA_ITEM_LIST
void nsEDDEngine::DATA_ITEM_LIST::AssignDataItemList(const DATA_ITEM_LIST &data_item_list)
{
	count	= data_item_list.count;
	limit   = data_item_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new DATA_ITEM[count];

        for(int i = 0; i < count; i++)
        {
			list[i] = data_item_list.list[i];
		}
	}
}


nsEDDEngine::DATA_ITEM_LIST::~DATA_ITEM_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor forDATA_ITEM_VALUE
nsEDDEngine::DATA_ITEM_VALUE::DATA_ITEM_VALUE(const DATA_ITEM_VALUE &data_item_value)
{
	AssignDataItemValue(data_item_value);
}

//Assignment Operator for DATA_ITEM_VALUE
nsEDDEngine::DATA_ITEM_VALUE& nsEDDEngine::DATA_ITEM_VALUE::operator= (const DATA_ITEM_VALUE &data_item_value)
{
	AssignDataItemValue(data_item_value);
	return *this;
}

//AssignDataItemValue is used to assign all the attributes of DATA_ITEM_VALUE
void nsEDDEngine::DATA_ITEM_VALUE::AssignDataItemValue(const DATA_ITEM_VALUE &data_item_value)
{
	iconst = data_item_value.iconst;
	ref    = data_item_value.ref;
	fconst = data_item_value.fconst;
	dconst = data_item_value.dconst;
	uconst = data_item_value.uconst;
}

//Copy Constructor for DATA_ITEM
nsEDDEngine::DATA_ITEM::DATA_ITEM(const DATA_ITEM &data_item)
{
	AssignDataItem(data_item);
}

//Assignment Operator for DATA_ITEM
nsEDDEngine::DATA_ITEM& nsEDDEngine::DATA_ITEM::operator= (const DATA_ITEM &data_item)
{
	AssignDataItem(data_item);
	return *this;
}

//AssignDataItem is used to assign all the attributes of DATA_ITEM
void nsEDDEngine::DATA_ITEM::AssignDataItem(const DATA_ITEM &data_item)
{
	data_item_mask = data_item.data_item_mask;
	type		   = data_item.type;
	width		   = data_item.width;
	flags		   = data_item.flags;
	data		   = data_item.data;
}

nsEDDEngine::DATA_ITEM::~DATA_ITEM()
{
	if (type == FDI_DATA_REFERENCE)
	{
		this->data.ref.Dispose();
	}

	this->type = (DataItemType)0;
	this->data.iconst = 0;
}

//Copy Constructor for OP_REF_TRAIL
nsEDDEngine::OP_REF_TRAIL::OP_REF_TRAIL(const OP_REF_TRAIL &rhs)
{
	AssignOpRefTrail(rhs);
}

//Assignment Operator for OP_REF_TRAIL
nsEDDEngine::OP_REF_TRAIL& nsEDDEngine::OP_REF_TRAIL::operator= (const OP_REF_TRAIL &rhs)
{
	if( this != &rhs)
	{
		delete [] this->trail;
		this->trail = nullptr;

		AssignOpRefTrail(rhs);
	}
	return *this;
}

//AssignOpRefTrail is used to assign all the attributes of OP_REF_TRAIL
void nsEDDEngine::OP_REF_TRAIL::AssignOpRefTrail(const OP_REF_TRAIL &rhs)
{
		desc_bit_mask	=		rhs.desc_bit_mask;
		block_instance	=		rhs.block_instance;
		desc_id			=		rhs.desc_id;
		desc_type		=		rhs.desc_type;
		expr			=		rhs.expr;
		op_info			=		rhs.op_info;
		op_info_list	=		rhs.op_info_list;
		op_ref_type		=		rhs.op_ref_type;
		trail_count		=		rhs.trail_count;
		trail_limit		=		rhs.trail_count;	// When copying, set limit to count, too

		if (trail_count == 0) // no list to copy
		{
				trail = nullptr;
		}
		else    // If the list is to be copied
		{
			trail	= new RESOLVE_INFO[rhs.trail_count];
			for(int j = 0; j < rhs.trail_count ;j++)
			{
					trail[j] = rhs.trail[j];
			}
		}
				
}

void nsEDDEngine::OP_REF_TRAIL::Dispose()
{
	delete [] this->trail;
	this->trail	= nullptr;
	this->trail_count	= 0;
	this->trail_limit	= 0;
	this->expr.Dispose();
}

//Copy Constructor for ACTION_VALUE
nsEDDEngine::ACTION_VALUE::ACTION_VALUE(const ACTION_VALUE &action_value)
{
	AssignActionValue(action_value);
}

//Assignment Operator for ACTION_VALUE
nsEDDEngine::ACTION_VALUE& nsEDDEngine::ACTION_VALUE::operator= (const ACTION_VALUE &action_value)
{
	AssignActionValue(action_value);
	return *this;
}

//AssignActionValue is used to assign all the attributes of ACTION_VALUE
void nsEDDEngine::ACTION_VALUE::AssignActionValue(const ACTION_VALUE &action_value)
{
	meth_ref		= action_value.meth_ref;
	meth_ref_args	= action_value.meth_ref_args;
	meth_definition = action_value.meth_definition;		
}

//Copy Constructor for ACTION
nsEDDEngine::ACTION::ACTION(const ACTION &action_or)
{
	AssignAction(action_or);
}

//Assignment Operator for ACTION
nsEDDEngine::ACTION& nsEDDEngine::ACTION::operator= (const ACTION &action_or)
{
	AssignAction(action_or);
	return * this;
}

//AssignAction is used to assign all the attributes of ACTION
void nsEDDEngine::ACTION::AssignAction(const ACTION &action_or)
{
	 eType		= action_or.eType;
	 action     = action_or.action;

}

//Copy Constructor for ACTION_LIST
nsEDDEngine::ACTION_LIST::ACTION_LIST(const ACTION_LIST &action_list)
{
	 AssignActionList(action_list);
}

//Assignment Operator for ACTION_LIST
nsEDDEngine::ACTION_LIST& nsEDDEngine::ACTION_LIST::operator= (const ACTION_LIST &action_list)
{
	if(this != &action_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignActionList(action_list);
	}
	return *this;
}

//AssignActionList is used to assign all the attributes of list ACTION_LIST
void nsEDDEngine::ACTION_LIST::AssignActionList(const ACTION_LIST &action_list)
{
	count	= action_list.count;
	limit   = action_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new ACTION[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for ACTION is called
			list[i] =  action_list.list[i];
		}
	}

}


nsEDDEngine::ACTION_LIST::~ACTION_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}



//Copy Constructor for DEFINITION
nsEDDEngine::DEFINITION::DEFINITION(const DEFINITION &definition)
{
	AssignDefination(definition);
}

//Overloaded Assignment Operator for DEFINITION
nsEDDEngine::DEFINITION& nsEDDEngine::DEFINITION:: operator= (const DEFINITION &definition)
{
	if(this != &definition)
	{
		delete [] this->data;
		this ->data = nullptr;
	
		AssignDefination(definition);
	}
	return *this;
}

//AssignDefination is used to assign all the attributes of DEFINITION
void nsEDDEngine::DEFINITION::AssignDefination(const DEFINITION &definition)
{
	size = definition.size;
	if(definition.data != nullptr)
	{
		data = new wchar_t[size + 1];
        PS_Wcscpy(data, size+1, definition.data);
	}
}

void nsEDDEngine::DEFINITION::Dispose()
{
	delete [] this->data;
	this->data = nullptr;
	this->size = 0;
}

//copy constructor for RANGE_DATA_LIST
nsEDDEngine::RANGE_DATA_LIST::RANGE_DATA_LIST(const RANGE_DATA_LIST &range_data_list)
{
	AssignRangeDataList(range_data_list);
}

//Overloaded Assignment Operator for RANGE_DATA_LIST
nsEDDEngine::RANGE_DATA_LIST& nsEDDEngine::RANGE_DATA_LIST:: operator= (const RANGE_DATA_LIST &range_data_list)
{
	if(this != &range_data_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignRangeDataList(range_data_list);
	}
	return *this;
}

//AssignRangeDataList is used to assign all the attributes of list RANGE_DATA_LIST
void nsEDDEngine::RANGE_DATA_LIST::AssignRangeDataList(const RANGE_DATA_LIST &range_data_list)
{
	count	= range_data_list.count;
	limit   = range_data_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new EXPR[count];

        for(int i = 0; i < count; i++)
        {  
			//Assignment operator for EXPR is called
			list[i] = range_data_list.list[i];
		}
	}
}

nsEDDEngine::RANGE_DATA_LIST::~RANGE_DATA_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//copy constructor for ENUM_VALUE_LIST
nsEDDEngine::ENUM_VALUE_LIST::ENUM_VALUE_LIST(const ENUM_VALUE_LIST &enum_value_list)
{
	AssignEnumValueList(enum_value_list);
}

//Overloaded Assignment Operator for ENUM_VALUE_LIST
nsEDDEngine::ENUM_VALUE_LIST& nsEDDEngine::ENUM_VALUE_LIST:: operator= (const ENUM_VALUE_LIST &enum_value_list)
{
	if(this != &enum_value_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignEnumValueList(enum_value_list);
	}
	return *this;
}

//AssignEnumValueList is used to assign all the attributes of list ENUM_VALUE_LIST
void nsEDDEngine::ENUM_VALUE_LIST::AssignEnumValueList(const ENUM_VALUE_LIST &enum_value_list)
{
	count	= enum_value_list.count;
	limit   = enum_value_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new ENUM_VALUE[count];

        for(int i = 0; i < count; i++)
        {
			list[i] = enum_value_list.list[i];
		}
	}
}

nsEDDEngine::ENUM_VALUE_LIST::~ENUM_VALUE_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}


//Copy Constructor for STATUS_CLASS_LIST
nsEDDEngine::STATUS_CLASS_LIST::STATUS_CLASS_LIST(const STATUS_CLASS_LIST &status_class)
{
	AssignStatusClassList(status_class);
}

//Assignment Operator for STATUS_CLASS
nsEDDEngine::STATUS_CLASS_LIST& nsEDDEngine::STATUS_CLASS_LIST::operator= (const STATUS_CLASS_LIST &status_class)
{
	if(this != &status_class)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignStatusClassList(status_class);
	}
	return *this;
}

//AssignStatusClassList is used to assign all the attributes of STATUS_CLASS_LIST
void nsEDDEngine::STATUS_CLASS_LIST::AssignStatusClassList(const STATUS_CLASS_LIST &status_class)
{
	count	= status_class.count;
	limit   = status_class.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new STATUS_CLASS_ELEM[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for STATUS_CLASS_ELEM is called
			list[i] =  status_class.list[i];
		}
	}
}

// Destructor for STATUS_CLASS
// It needs to delete all the contents of the list before destroying itself.
nsEDDEngine::STATUS_CLASS_LIST::~STATUS_CLASS_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor for ENUM_VALUE
nsEDDEngine::ENUM_VALUE::ENUM_VALUE(const ENUM_VALUE &enum_value)
{
	AssignEnumValue(enum_value);
}

//Assignment Operator for ENUM_VALUE
nsEDDEngine::ENUM_VALUE& nsEDDEngine::ENUM_VALUE::operator= (const ENUM_VALUE &enum_value)
{
	AssignEnumValue(enum_value);
	return *this;
}

//AssignEnumValue is used to assign all the attributes of ENUM_VALUE
void nsEDDEngine::ENUM_VALUE::AssignEnumValue(const ENUM_VALUE &enum_value)
{
	evaled		= enum_value.evaled;
	val			= enum_value.val;
	desc		= enum_value.desc;
	help		= enum_value.help;
	status		= enum_value.status;
	func_class	= enum_value.func_class;
	actions		= enum_value.actions;
}

// Destructor for ENUM_VALUE
nsEDDEngine::ENUM_VALUE::~ENUM_VALUE()
{
}


//Copy Constructor 
nsEDDEngine::EXPR::EXPR(const EXPR &expr)
{
	AssignExpr(expr);
}
		
//Overloaded Assignment Operator 
nsEDDEngine::EXPR& nsEDDEngine::EXPR::operator= (const EXPR &expr)
{
	AssignExpr(expr);
	return *this;
}

//AssignExpr is used for assign all the attributes of EXPR
void nsEDDEngine::EXPR:: AssignExpr(const EXPR &expr)
{
	eType	= expr.eType;
	size	= expr.size;
	val	    = expr.val;
}

void nsEDDEngine::EXPR::Dispose()
{
	

	this->eType = nsEDDEngine::EXPR::EXPR_TYPE_NONE;
	this->size = 0;
	this->val.i = 0;
}

void nsEDDEngine::EXPR::SetExprAsPerVariableType(const TYPE_SIZE &TypeSize)
{
	if (this->eType == EXPR_TYPE_NONE)
	{
		return; // There is nothing to do here because we don't have value to convert.
	}

	// Figure out what kind of conversion we need.
	switch(TypeSize.type)
	{
	case nsEDDEngine::VT_DDS_TYPE_UNUSED:
		{
			ConvertTo(EXPR_TYPE_NONE, TypeSize.size );
		}
		break;
	case nsEDDEngine::VT_INTEGER: 
	case nsEDDEngine::VT_EDD_DATE:	
		{
			ConvertTo(EXPR_TYPE_INTEGER, TypeSize.size );
		}
		break;
	case nsEDDEngine::VT_UNSIGNED: 	
	case nsEDDEngine::VT_ENUMERATED:
	case nsEDDEngine::VT_BIT_ENUMERATED:
	case nsEDDEngine::VT_INDEX:
	case nsEDDEngine::VT_TIME: 			
	case nsEDDEngine::VT_DATE_AND_TIME: 	
	case nsEDDEngine::VT_DURATION: 
	case nsEDDEngine::VT_OBJECT_REFERENCE:
	case nsEDDEngine::VT_BOOLEAN:
		{
			ConvertTo(EXPR_TYPE_UNSIGNED, TypeSize.size );
		}
		break;
	case nsEDDEngine::VT_FLOAT:
		{
			ConvertTo(EXPR_TYPE_FLOAT, TypeSize.size );
		}
		break;
	case nsEDDEngine::VT_DOUBLE:
		{
			ConvertTo(EXPR_TYPE_DOUBLE, TypeSize.size );
		}
		break;
	case nsEDDEngine::VT_ASCII: 			
	case nsEDDEngine::VT_PACKED_ASCII: 	
	case nsEDDEngine::VT_PASSWORD: 		 
	case nsEDDEngine::VT_EUC:
	case nsEDDEngine::VT_VISIBLESTRING:
		{
			ConvertTo(EXPR_TYPE_STRING, TypeSize.size);
		}
		break;
	case nsEDDEngine::VT_BITSTRING:
		{
			ConvertTo(EXPR_TYPE_BINARY, TypeSize.size / 8); // For BITSTRING, size specifies the number of bits, which must be a multiple of 8
		}
		break;
	case nsEDDEngine::VT_OCTETSTRING:
		{
			ConvertTo(EXPR_TYPE_BINARY, TypeSize.size);
		}
		break;
	case nsEDDEngine::VT_TIME_VALUE:	
		{
			if(TypeSize.size == 4)
			{
				ConvertTo(EXPR_TYPE_UNSIGNED, TypeSize.size );
			}
			else if (TypeSize.size == 8)
			{
				ConvertTo(EXPR_TYPE_INTEGER, TypeSize.size );
			}
		}
		break;
	default:
		break;
	}

}
// Converts and EXPR in place to a new ExprValueType and size.
// This works only for numeric types.
void nsEDDEngine::EXPR::ConvertTo(ExprValueType newType, unsigned int newSize)
{

	_ASSERT(this->eType != EXPR_TYPE_NONE); // We shouldn't come here if there is nothing to convert
	bool bConverted = true;		// Assume that we converted successfully

	switch(newType)		// Look at the type that we want to convert to
	{
	case EXPR_TYPE_FLOAT:
		{
			switch(eType)			// Check current type
			{
			case EXPR_TYPE_FLOAT:	// We are already of the right type
				break;

			case EXPR_TYPE_DOUBLE:
				val.f = (float)val.d;	// Convert to a float
				val.d = 0.0;			// Clean up the old
				break;

			case EXPR_TYPE_UNSIGNED:
				val.f = (float)val.u;	// Convert to a float
				val.u = 0;				// Clean up the old
				break;

			case EXPR_TYPE_INTEGER:
				val.f = (float)val.i;	// Convert to a float
				val.i = 0;				// Clean up the old
				break;

			case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
			case EXPR_TYPE_STRING:	// Don't try to convert strings.
			case EXPR_TYPE_BINARY:
			default:
				bConverted = false;
				break;
			}
		}
		break;

	case EXPR_TYPE_DOUBLE:
		{
			switch(eType)			// Check current type
			{
			case EXPR_TYPE_FLOAT:
				val.d = (double)val.f;	// Convert to a double
				val.f = 0.0;			// Clean up the old
				break;

			case EXPR_TYPE_DOUBLE:	// We are already of the right type
				break;

			case EXPR_TYPE_UNSIGNED:
				val.d = (double)val.u;	// Convert to a double
				val.u = 0;				// Clean up the old
				break;

			case EXPR_TYPE_INTEGER:
				val.d = (double)val.i;	// Convert to a double
				val.i = 0;				// Clean up the old
				break;

			case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
			case EXPR_TYPE_STRING:	// Don't try to convert strings.
			case EXPR_TYPE_BINARY:
			default:
				bConverted = false;
				break;
			}
		}
		break;

	case EXPR_TYPE_UNSIGNED:
		// Same pattern here except set the val.u
		switch(eType)			// Check current type
		{
		case EXPR_TYPE_FLOAT:
			val.u = (ULONGLONG)val.f;	// Convert to a unsigned
			val.f = 0.0;			// Clean up the old
			break;

		case EXPR_TYPE_DOUBLE:	
			val.u = (ULONGLONG)val.d;	// Convert to a unsigned
			val.d = 0.0;				// Clean up the old
			break;

		case EXPR_TYPE_UNSIGNED:    // We are already of the right type
			break;

		case EXPR_TYPE_INTEGER:
			val.u = (ULONGLONG)val.i;	// Convert to a unsigned
			val.i = 0;				// Clean up the old
			break;

		case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
		case EXPR_TYPE_STRING:	// Don't try to convert strings.
		case EXPR_TYPE_BINARY:
		default:
			bConverted = false;
			break;
		}
		break;

	case EXPR_TYPE_INTEGER:
		// Same pattern here except set the val.i
		switch(eType)			// Check current type
		{
		case EXPR_TYPE_FLOAT:
			val.i = (LONGLONG)val.f;	// Convert to a integer
			val.f = 0.0;			// Clean up the old
			break;

		case EXPR_TYPE_DOUBLE:	
			val.i = (LONGLONG)val.d;	// Convert to a integer
			val.d = 0.0;				// Clean up the old
			break;

		case EXPR_TYPE_UNSIGNED: 
			val.i = (LONGLONG)val.u;	// Convert to a integer
			val.u = 0;				// Clean up the old
			break;

		case EXPR_TYPE_INTEGER:			// We are already of the right type
			break;

		case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
		case EXPR_TYPE_STRING:	// Don't try to convert strings.
		case EXPR_TYPE_BINARY:
		default:
			bConverted = false;
			break;
		}
		break;

	case EXPR_TYPE_STRING:	
		switch (eType)			// Check current type
		{
		case EXPR_TYPE_STRING:
		{
			if (val.s.c_str())
			{
				if (val.s.length() > newSize) // If default string size is greater than the size defined in DD
				{
					wchar_t *tmp = new wchar_t[newSize + 1];
                    PS_Wcsncpy(tmp, newSize + 1, val.s.c_str(), _TRUNCATE); // Truncate the default string to the exact size defined in DD
					val.s = tmp;// re-assign the truncated value
					delete[]tmp;
				}
			}
			else
			{
				bConverted = false;
			}
		}
		break;
		case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
		case EXPR_TYPE_FLOAT:
		case EXPR_TYPE_DOUBLE:
		case EXPR_TYPE_UNSIGNED:
		case EXPR_TYPE_INTEGER:
		case EXPR_TYPE_BINARY:
		default:
			bConverted = false;
			break;
		}
		break;
	

	case EXPR_TYPE_BINARY:  
		switch (eType)			// Check current type
		{
			case EXPR_TYPE_BINARY:
			{
				if (val.b.ptr())
				{
					if (val.b.length() > newSize) // If default string size is greater than the size defined in DD
					{
						uchar *tmp = new uchar[newSize];
						memcpy(tmp, val.b.ptr(), newSize);
						val.b.assign(tmp, newSize);// re-assign the truncated value
						delete[]tmp;
					}
				}
				else
				{
					bConverted = false;
				}
			}
			break;
			case EXPR_TYPE_STRING:
			{
				if (val.s.c_str())
				{
					wchar_t hexdigit[] = L"0123456789ABCDEF";
					unsigned char szBuf[32] = { 0 }; //  max size 32 byte.
					int strlength = (int)val.s.length();
					unsigned long numBytes = 0;

					wchar_t *sVal = new wchar_t[strlength + 1];

					//copy string to be converted
                    PS_Wcscpy(sVal, strlength + 1, val.s.c_str());
					//Convert the string to the Upper case. There may be some lower case characters used in hex string. 
					
					PS_Wcsupr(sVal, strlength + 1);

					// check If string contains only hex chnaracter, return if not.
					for (int i = 0; i < strlength; i++)
					{
						if (!iswxdigit(sVal[i]))
						{
							delete[] sVal;
							//We are here because the string provided is not valid hex string to convert into binary data. 
							//But still we will set the new type and size and null value.
							eType = newType;
							size = newSize;
							val.b.assign(nullptr, 0);
							//val.b.length = 0;
							return;
						}	
					}
					const int hexsize = wcslen(hexdigit);
					for (int i = 0; i < strlength; i++)
					{
						// convert the 1st nibble of the encrypted data
						wchar_t ch = sVal[i++];

						wchar_t* hexdchar = wmemchr(hexdigit, ch, hexsize);
						int hexdigitIdx = hexdchar - hexdigit;
						hexdigitIdx = hexdigitIdx << 4;
							
						// convert the 2nd nibble 
						ch = sVal[i];
						hexdchar = wmemchr(hexdigit, ch, hexsize);
						int hexdigitIdx2 = hexdchar - hexdigit;
						hexdigitIdx |= hexdigitIdx2;

						szBuf[numBytes++] = (unsigned char)hexdigitIdx;
					}
					

					if (numBytes >= newSize)
					{
						val.b.assign(szBuf, newSize); // Assign with size deined in DD 
					}
					else
					{
						val.b.assign(szBuf, numBytes);// string is shorter than DD size. Assign the string size.
					}
						
					delete[] sVal;						
					
				}
				else
				{
					bConverted = false;
				}
			}
			break;
			
			case EXPR_TYPE_NONE:	// Don't currently have a value, so we can't convert
			case EXPR_TYPE_FLOAT:
			case EXPR_TYPE_DOUBLE:
			case EXPR_TYPE_UNSIGNED:
			case EXPR_TYPE_INTEGER:
			default:
				bConverted = false;
				break;
		}
		break;
	case EXPR_TYPE_NONE:
		Dispose();			// Reset this EXPR (Not a likely request.)
		break;
	default:
		bConverted = false;
		break;
	}

	if (bConverted)	// Update the type and size if we did the conversion
	{
		eType = newType;
		size = newSize;
	}
	else
	{
		_ASSERT(0);
	}
}

//copy constructor for VECTOR_LIST
nsEDDEngine::VECTOR_LIST::VECTOR_LIST(const VECTOR_LIST &vector_list)
{
	AssignVectorList(vector_list);
}

//Overloaded Assignment Operator for VECTOR_LIST
nsEDDEngine::VECTOR_LIST& nsEDDEngine::VECTOR_LIST:: operator= (const VECTOR_LIST &vector_list)
{
	if(this != &vector_list)
	{
		delete [] this->vectors;
		this->vectors= nullptr;

		AssignVectorList(vector_list);
	}
	return *this;
}

//AssignVectorList is used to assign all the attributes of list VECTOR_LIST
void nsEDDEngine::VECTOR_LIST::AssignVectorList(const VECTOR_LIST &vector_list)
{
	count	= vector_list.count;
	limit   = vector_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        vectors = nullptr;
    }
    else            // There is a list to copy
    {
		vectors = new VECTOR[count];

		for(int i = 0; i < count; i++)
		{
			vectors[i] = vector_list.vectors[i];
		}
	}
}


nsEDDEngine::VECTOR_LIST::~VECTOR_LIST()
{
	delete [] this->vectors;
	this->vectors = nullptr;
}

//copy constructor for VECTOR
nsEDDEngine::VECTOR::VECTOR(const VECTOR &vector)
{
	AssignVector(vector);
}

//Overloaded Assignment Operator for VECTOR
nsEDDEngine::VECTOR& nsEDDEngine::VECTOR:: operator= (const VECTOR &vector)
{
	if(this != &vector)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignVector(vector);
	}
	return *this;
}

//AssignVector is used for assign all the attributes of list VECTOR
void nsEDDEngine::VECTOR::AssignVector(const VECTOR &vector)
{
	count        = vector.count ;
	limit        = vector.count ;  // This will never change in size so allocate count elements
	description  = vector.description ;
	list		 = new VECTOR_ITEM[count];

	for(int j = 0; j < vector.count; j++)
	{
		list[j]	= vector.list[j];
	}	
}

nsEDDEngine::VECTOR::~VECTOR()
{

	delete [] this->list;
	this->list = nullptr;
}

//copy constructor for VECTOR_ITEM_VALUE
nsEDDEngine::VECTOR_ITEM_VALUE::VECTOR_ITEM_VALUE(const VECTOR_ITEM_VALUE &vector_item_value)
{
	AssignVectorItemValue(vector_item_value);
}

//Overloaded Assignment Operator for VECTOR_ITEM_VALUE
nsEDDEngine::VECTOR_ITEM_VALUE& nsEDDEngine::VECTOR_ITEM_VALUE:: operator= (const VECTOR_ITEM_VALUE &vector_item_value)
{
	AssignVectorItemValue(vector_item_value);
	return *this;
}

//AssignVectorItemValue is used to assign all the attributes of VECTOR_ITEM_VALUE
void nsEDDEngine::VECTOR_ITEM_VALUE::AssignVectorItemValue(const VECTOR_ITEM_VALUE &vector_item_value)
{
	iconst = vector_item_value.iconst;
	ref    = vector_item_value.ref;
	fconst = vector_item_value.fconst;
	str    = vector_item_value.str;
}

//copy constructor for VECTOR_ITEM
nsEDDEngine::VECTOR_ITEM::VECTOR_ITEM(const VECTOR_ITEM &vector_item)
{
	AssignVectorItem(vector_item);
}

//Overloaded Assignment Operator for VECTOR_ITEM
nsEDDEngine::VECTOR_ITEM& nsEDDEngine::VECTOR_ITEM:: operator= (const VECTOR_ITEM &vector_item)
{
	AssignVectorItem(vector_item);
	return *this;
}

//AssignVectorItem is used to assign all the attributes of VECTOR_ITEM
void nsEDDEngine::VECTOR_ITEM::AssignVectorItem(const VECTOR_ITEM &vector_item)
{
	type   = vector_item.type;
	vector = vector_item.vector;
}

nsEDDEngine::VECTOR_ITEM::~VECTOR_ITEM()
{
	if (type == FDI_DATA_REFERENCE)
	{
		this->vector.ref.Dispose();
	}

	this->type = (DataItemType)0;
	this->vector.iconst = 0;
}

//copy constructor for BINARY_IMAGE
nsEDDEngine::BINARY_IMAGE::BINARY_IMAGE(const BINARY_IMAGE &binary_image)
{
	AssignBinaryImage(binary_image);
}

//Overloaded Assignment Operator for BINARY_IMAGE
nsEDDEngine::BINARY_IMAGE& nsEDDEngine::BINARY_IMAGE:: operator= (const BINARY_IMAGE &binary_image)
{
	if(this != &binary_image)
	{
		delete [] this->data;
		this->data = nullptr;

		AssignBinaryImage(binary_image);
	}

	return *this;
}

//AssignBinaryImage is used to assign all the attributes of BINARY_IMAGE
void nsEDDEngine::BINARY_IMAGE::AssignBinaryImage(const BINARY_IMAGE &binary_image)
{
	length   = binary_image.length;
	
	if(binary_image.data != nullptr)
	{
		data = new unsigned char[length];
		memcpy(data, binary_image.data, length);
	}
}

nsEDDEngine::BINARY_IMAGE::~BINARY_IMAGE()
{
    Dispose();
}

void nsEDDEngine::BINARY_IMAGE::Dispose()
{
	//delete data;
    if (data)
    {
        delete(data);
        data = nullptr;
    }
}


//copy constructor for MENU_ITEM
nsEDDEngine::MENU_ITEM::MENU_ITEM(const MENU_ITEM &menu_item)
{
	AssignMenuItem(menu_item);
}

//Overloaded Assignment Operator for MENU_ITEM
nsEDDEngine::MENU_ITEM& nsEDDEngine::MENU_ITEM:: operator= (const MENU_ITEM &menu_item)
{
	AssignMenuItem(menu_item);
	return *this;
}

//AssignMenuItem is used to assign all the attributes of MENU_ITEM
void nsEDDEngine::MENU_ITEM::AssignMenuItem(const MENU_ITEM &menu_item)
{
	qual = menu_item.qual;
	ref  = menu_item.ref;
}

//copy constructor for MENU_ITEM_LIST
nsEDDEngine::MENU_ITEM_LIST::MENU_ITEM_LIST(const MENU_ITEM_LIST &menu_item_list)
{
	AssignMenuItemList(menu_item_list);
}

//Overloaded Assignment Operator for MENU_ITEM_LIST
nsEDDEngine::MENU_ITEM_LIST& nsEDDEngine::MENU_ITEM_LIST:: operator= (const MENU_ITEM_LIST &menu_item_list)
{
	if(this != &menu_item_list)
	{
		delete [] this->list;
		this->list =nullptr;

		AssignMenuItemList(menu_item_list);
	}
	return *this;
}

//AssignMenuItemList is used to assign all the attributes of list MENU_ITEM_LIST
void nsEDDEngine::MENU_ITEM_LIST::AssignMenuItemList(const MENU_ITEM_LIST &menu_item_list)
{
	count	= menu_item_list.count;
	limit   = menu_item_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new MENU_ITEM[count];

        for(int i = 0; i < count; i++)
        {   
			//Assignment operator for MENU_ITEM is called
			list[i] = menu_item_list.list[i];
		}
	}

}

nsEDDEngine::MENU_ITEM_LIST::~MENU_ITEM_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}


nsEDDEngine::MENU_ITEM::~MENU_ITEM()
{
	this->ref.Dispose();
}

//copy constructor for OP_REF_TRAIL_LIST
nsEDDEngine::OP_REF_TRAIL_LIST::OP_REF_TRAIL_LIST(const OP_REF_TRAIL_LIST &op_ref_trail_list)
{
	AssignOpRefTrialList(op_ref_trail_list);
}

//Overloaded Assignment Operator for OP_REF_TRAIL_LIST
nsEDDEngine::OP_REF_TRAIL_LIST& nsEDDEngine::OP_REF_TRAIL_LIST::operator= (const OP_REF_TRAIL_LIST &op_ref_trail_list)
{
	if(this != &op_ref_trail_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignOpRefTrialList(op_ref_trail_list);
	}
	return *this;
}

//AssignOpRefTrialList is used to assign all the attributes of list OP_REF_TRAIL_LIST
void nsEDDEngine::OP_REF_TRAIL_LIST::AssignOpRefTrialList(const OP_REF_TRAIL_LIST &op_ref_trail_list)
{
	count	= op_ref_trail_list.count;
	limit   = op_ref_trail_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new OP_REF_TRAIL[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for OP_REF_TRAIL is called
			list[i] = op_ref_trail_list.list[i];
		}
	}

}


nsEDDEngine::OP_REF_TRAIL_LIST::~OP_REF_TRAIL_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//copy constructor for ITEM_ID_LIST
nsEDDEngine::ITEM_ID_LIST::ITEM_ID_LIST(const ITEM_ID_LIST &item_id_list)
{
	AssignItemIdList(item_id_list);
}

//Overloaded Assignment Operator for ITEM_ID_LIST
nsEDDEngine::ITEM_ID_LIST& nsEDDEngine::ITEM_ID_LIST:: operator= (const ITEM_ID_LIST &item_id_list)
{
	if(this != &item_id_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignItemIdList(item_id_list);
	}
	return *this;
}

//AssignItemIdList is used to assign all the attributes of list ITEM_ID_LIST
void nsEDDEngine::ITEM_ID_LIST::AssignItemIdList(const ITEM_ID_LIST &item_id_list)
{
	count	= item_id_list.count;
	limit   = item_id_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new ITEM_ID[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for ITEM_ID is called
			list[i] = item_id_list.list[i];
		}
	}

}


nsEDDEngine::ITEM_ID_LIST::~ITEM_ID_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor for DEFAULT_VALUE_ITEM
nsEDDEngine::DEFAULT_VALUE_ITEM::DEFAULT_VALUE_ITEM(const DEFAULT_VALUE_ITEM &default_value_item)
{
	AssignDefaultValueItem(default_value_item);
}
		
//Overloaded Assignment Operator for DEFAULT_VALUE_ITEM
nsEDDEngine::DEFAULT_VALUE_ITEM& nsEDDEngine::DEFAULT_VALUE_ITEM:: operator= (const DEFAULT_VALUE_ITEM &default_value_item)
{
	AssignDefaultValueItem(default_value_item);
	return *this;
}

//AssignDefaultValueItem is used to assign all the attributes of list DEFAULT_VALUE_ITEM
void nsEDDEngine::DEFAULT_VALUE_ITEM::AssignDefaultValueItem(const DEFAULT_VALUE_ITEM &default_value_item)
{
	ref		= default_value_item.ref;
	value   = default_value_item.value;
}

//Copy Constructor for DEFAULT_VALUES_LIST
nsEDDEngine::DEFAULT_VALUES_LIST::DEFAULT_VALUES_LIST(const DEFAULT_VALUES_LIST &default_value_list)
{
	AssignDefaultValueList(default_value_list);
}
		
//Overloaded Assignment Operator for DEFAULT_VALUES_LIST
nsEDDEngine::DEFAULT_VALUES_LIST& nsEDDEngine::DEFAULT_VALUES_LIST:: operator= (const DEFAULT_VALUES_LIST &default_value_list)
{
	if(this != &default_value_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignDefaultValueList(default_value_list);
	}
	return *this;
}

//AssignDefaultValueList is used to assign all the attributes of list DEFAULT_VALUES_LIST
void nsEDDEngine::DEFAULT_VALUES_LIST::AssignDefaultValueList(const DEFAULT_VALUES_LIST &default_value_list)
{
	count	= default_value_list.count;
	limit   = default_value_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new DEFAULT_VALUE_ITEM[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for DEFAULT_VALUE_ITEM is called
			list[i]	 = default_value_list.list[i];
		}
	}

}


nsEDDEngine::DEFAULT_VALUES_LIST::~DEFAULT_VALUES_LIST()
{
	delete [] this->list;
}	


nsEDDEngine::RESOLVED_REFERENCE::RESOLVED_REFERENCE()
{
	Init();
}

void nsEDDEngine::RESOLVED_REFERENCE::Init()
{
	pResolvedRef = nullptr;
	size = 0;
}

nsEDDEngine::RESOLVED_REFERENCE::~RESOLVED_REFERENCE()
{
	Dispose();
}

void nsEDDEngine::RESOLVED_REFERENCE::Dispose()
{
	delete [] pResolvedRef;
	pResolvedRef = nullptr;
	size = 0;
}

nsEDDEngine::RESOLVED_REFERENCE& nsEDDEngine::RESOLVED_REFERENCE::operator= (const RESOLVED_REFERENCE &resolved_ref)
{
	if(this != &resolved_ref)
	{
		Set(resolved_ref.pResolvedRef, resolved_ref.size);
	}
	return *this;
}
bool nsEDDEngine::RESOLVED_REFERENCE::operator==(const RESOLVED_REFERENCE &rhs)
{
	bool ret = false;

	if (	(size == rhs.size)		// They are the same if their contents is the same
		&&	(memcmp(pResolvedRef, rhs.pResolvedRef, (size*sizeof(*pResolvedRef)) ) == 0) )
	{
		ret = true;
	}

	return ret;
}

void nsEDDEngine::RESOLVED_REFERENCE::Set( unsigned long *pRef, int len )
{
	if (pResolvedRef != nullptr)
	{
		Dispose();
	}

	size = len;
	pResolvedRef = new unsigned long[size];

    PS_Memcpy(pResolvedRef, (size*sizeof(*pResolvedRef)), pRef, (len*sizeof(*pRef)) );
}


//Copy Constructor for COMPONENT_SPECIFIER
nsEDDEngine::COMPONENT_SPECIFIER::COMPONENT_SPECIFIER(const COMPONENT_SPECIFIER &component_specifier)
{
	AssignComponentSpecifier(component_specifier);
}
		
//Overloaded Assignment Operator for COMPONENT_SPECIFIER
nsEDDEngine::COMPONENT_SPECIFIER& nsEDDEngine::COMPONENT_SPECIFIER:: operator= (const COMPONENT_SPECIFIER &component_specifier)
{
	AssignComponentSpecifier(component_specifier);
	return *this;
}

//AssignComponentSpecifier is used to assign all the attributes of list COMPONENT_SPECIFIER
void nsEDDEngine::COMPONENT_SPECIFIER::AssignComponentSpecifier(const COMPONENT_SPECIFIER &component_specifier)
{
	cr_ref = component_specifier.cr_ref;
	filter          = component_specifier.filter;
	maximum_number  = component_specifier.maximum_number;
	minimum_number  = component_specifier.minimum_number;
	auto_create     = component_specifier.auto_create;
	range_list      = component_specifier.range_list;
}

nsEDDEngine::COMPONENT_SPECIFIER::~COMPONENT_SPECIFIER()
{
	maximum_number.Dispose();
	minimum_number.Dispose();
	auto_create.Dispose();
};


//Copy Constructor for COMPONENT_SPECIFIER_LIST
nsEDDEngine::COMPONENT_SPECIFIER_LIST::COMPONENT_SPECIFIER_LIST(const COMPONENT_SPECIFIER_LIST &component_specifier_list)
{
	AssignComponentSpecifierList(component_specifier_list);
}
		
//Overloaded Assignment Operator for COMPONENT_SPECIFIER_LIST
nsEDDEngine::COMPONENT_SPECIFIER_LIST& nsEDDEngine::COMPONENT_SPECIFIER_LIST:: operator= (const COMPONENT_SPECIFIER_LIST &component_specifier_list)
{
	if(this != &component_specifier_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignComponentSpecifierList(component_specifier_list);
	}
	return *this;
}

//AssignComponentSpecifierList is used to assign all the attributes of list COMPONENT_SPECIFIER_LIST
void nsEDDEngine::COMPONENT_SPECIFIER_LIST::AssignComponentSpecifierList(const COMPONENT_SPECIFIER_LIST &component_specifier_list)
{
	count	= component_specifier_list.count;
	limit   = component_specifier_list.limit;
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new COMPONENT_SPECIFIER[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for COMPONENT_SPECIFIER is called
			list[i] = component_specifier_list.list[i];
		}
	}

}

nsEDDEngine::COMPONENT_SPECIFIER_LIST::~COMPONENT_SPECIFIER_LIST()
{
	delete[] this->list;
	this->list = nullptr;
}

//Copy Constructor for NUMBER_EXPR
nsEDDEngine::NUMBER_EXPR::NUMBER_EXPR(const NUMBER_EXPR &number_expr)
{
	AssignNumberExpr(number_expr);
}

//Overloaded assignment operator for NUMBER_EXPR
nsEDDEngine::NUMBER_EXPR& nsEDDEngine::NUMBER_EXPR::operator= (const NUMBER_EXPR &number_expr)
{
	AssignNumberExpr(number_expr);
	return *this;
}

//AssignNumberExpr is used to assign all the attributes of NUMBER_EXPR
void nsEDDEngine::NUMBER_EXPR::AssignNumberExpr(const NUMBER_EXPR &number_expr)
{
	which = number_expr.which ;
	value = number_expr.value;
}

//Copy Constructor for RANGE_SET
nsEDDEngine::RANGE_SET::RANGE_SET(const RANGE_SET &range_set)
{
	AssignRangeset(range_set);
}

//Overloaded assignment operator for RANGE_SET
nsEDDEngine::RANGE_SET& nsEDDEngine::RANGE_SET::operator= (const RANGE_SET &range_set)
{
	AssignRangeset(range_set);
	return *this;
}

//AssignRangeset is used to assign all the attributes of RANGE_SET
void nsEDDEngine::RANGE_SET::AssignRangeset(const RANGE_SET &range_set)
{
	 max_num = range_set.max_num;
	 min_num = range_set.min_num;
}

//Copy Constructor for RANGE_LIST
nsEDDEngine::RANGE_LIST::RANGE_LIST(const RANGE_LIST &range_list)
{
	AssignRangeList(range_list);
}

//Overloaded assignment operator for RANGE_LIST
nsEDDEngine::RANGE_LIST& nsEDDEngine::RANGE_LIST::operator= (const RANGE_LIST &range_list)
{
	if(this != &range_list)
	{
		delete [] this->list;
		this->list = nullptr;

		AssignRangeList(range_list);
	}
	return *this;
}

//AssignRangeList is used to assign all the attributes of list RANGE_LIST
void nsEDDEngine::RANGE_LIST::AssignRangeList(const RANGE_LIST &range_list)
{
	var_refrences = range_list.var_refrences;
	count = range_list.count;
	limit   = range_list.count;  // This will never change in size so allocate count elements
	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new RANGE_SET[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for RANGE_SET is called
			list[i] = range_list.list[i];
		}
	}

}

nsEDDEngine::RANGE_LIST::~RANGE_LIST()
{
	delete [] this->list;
	this->list = nullptr;
}

//Copy Constructor for ITEM_INFORMATION
nsEDDEngine::ITEM_INFORMATION::ITEM_INFORMATION(const ITEM_INFORMATION &item_info)
{
	AssignItemInformation(item_info);
}

//Overloaded assignment operator for ITEM_INFORMATION
nsEDDEngine::ITEM_INFORMATION& nsEDDEngine::ITEM_INFORMATION::operator= (const ITEM_INFORMATION &item_info)
{
	AssignItemInformation(item_info);
	return *this;
}

//AssignItemInformation is used to assign all the attributes of list ITEM_INFORMATION
void nsEDDEngine::ITEM_INFORMATION::AssignItemInformation(const ITEM_INFORMATION &item_info)
{
	file_name	= item_info.file_name;
	line_number	= item_info.line_number;
	attrs		= item_info.attrs;
}

//Copy Constructor for ATTR_INFO_LIST
nsEDDEngine::ATTR_INFO_LIST::ATTR_INFO_LIST(const ATTR_INFO_LIST &attr_info_list)
{
	AssignAttrInfoList(attr_info_list);
}

//Overloaded assignment operator for ATTR_INFO_LIST
nsEDDEngine::ATTR_INFO_LIST& nsEDDEngine::ATTR_INFO_LIST::operator= (const ATTR_INFO_LIST &attr_info_list)
{
	if(this != &attr_info_list)
	{
		delete [] this->list;
		this->list= nullptr;

		AssignAttrInfoList(attr_info_list);
	}

	return *this;
}

//AssignAttrInfoList is used to assign all the attributes of list ATTR_INFO_LIST
void nsEDDEngine::ATTR_INFO_LIST::AssignAttrInfoList(const ATTR_INFO_LIST &attr_info_list)
{
	count	= attr_info_list.count;
	limit   = attr_info_list.count;  

	if (count == 0) // no list to copy
    {
        list = nullptr;
    }
    else            // There is a list to copy
    {
        list = new ATTR_INFO[count];

        for(int i = 0; i < count; i++)
        {
			//Assignment operator for ATTR_INFO is called
			list[i] = attr_info_list.list[i];
		}
	}
}

nsEDDEngine::ATTR_INFO_LIST::~ATTR_INFO_LIST()
{
	delete[] this->list;
	this->list = nullptr;
}

//Copy Constructor for ATTR_INFO
nsEDDEngine::ATTR_INFO::ATTR_INFO(const ATTR_INFO &attr_info)
{
	AssignAttrInfo(attr_info);
}

//Overloaded assignment operator for ATTR_INFO
nsEDDEngine::ATTR_INFO& nsEDDEngine::ATTR_INFO::operator= (const ATTR_INFO &attr_info)
{
	AssignAttrInfo(attr_info);
	return *this;
}

//AssignAttrInfo is used to assign all the attributes of list ATTR_INFO
void nsEDDEngine::ATTR_INFO::AssignAttrInfo(const ATTR_INFO &attr_info)
{
	attr_file_name	= attr_info.attr_file_name;
	attr_lineno		= attr_info.attr_lineno;
	attr_name = attr_info.attr_name;

}
