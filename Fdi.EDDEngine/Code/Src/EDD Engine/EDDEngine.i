%module EDDEngineSharp

#pragma SWIG nowarn=314


%{

#include <map>
#include <windows.h>
#include <cstdio>

#include "nsEDDEngine\AttributeName.h"
#include "nsEDDEngine\IItemInfo.h"
#include "nsEDDEngine\ITableInfo.h"
#include "nsEDDEngine\IConvenience.h"
#include "nsEDDEngine\IEDDEngine.h"
#include "nsEDDEngine\Common.h"
#include "nsEDDEngine\Table.h"
#include "nsEDDEngine\Flats.h"
#include "nsEDDEngine\Ddldefs.h"
#include "EddEngineImplementation.h"

using namespace nsEDDEngine;

%}

%ignore *::operator=;
%ignore *::operator==;
%ignore *::operator+;
%ignore *::operator+=;
%ignore *::operator-;
%ignore *::operator-=;
%ignore *::operator|;
%ignore *::operator|=;
%ignore *::operator&;
%ignore *::operator&=;
%ignore *::operator bool;

%ignore *::Dispose;
%ignore *::All_Attrs;

%ignore nsEDDEngine::DEFINITION::data;
%ignore nsEDDEngine::tag_STRING_TBL::root;
%ignore nsEDDEngine::tag_SYMBOL_TBL_ELEM::name;
%ignore nsEDDEngine::tag_IMAGE_LIST::name;

%typemap(csbase) nsEDDEngine::ClassType "ulong"
%typemap(csclassmodifiers) nsEDDEngine::ItemBase "public partial class"
%typemap(csclassmodifiers) nsEDDEngine::OP_REF_TRAIL "public partial class"

%rename(BoolValue) nsEDDEngine::Boolean;

%include "std_string.i"
%include "std_wstring.i"
%include "std_map.i"
%include "wchar.i"
%include "windows.i"

%define _WIN32 %enddef
%include "AttributeName.h"
%include "Ddldefs.h"
%include "Common.h"
%include "Attrs.h"

%include "IItemInfo.h"
%include "ITableInfo.h"
%include "IConvenience.h"
%include "Table.h"
%include "Flats.h"
%include "EddEngineImplementation.h"
%include "IEDDEngine.h"
%include "Common.h"

%template(AttributeMap) std::map<nsEDDEngine::AttributeName, nsEDDEngine::AttributeItem>;


%extend nsEDDEngine::tag_ITEM_TBL
{
  tag_ITEM_TBL_ELEM* get(int i)
  {
    return &$self->list[i];
  }
}

%extend nsEDDEngine::tag_BLK_TBL
{
  tag_BLK_TBL_ELEM* get(int i)
  {
    return &$self->list[i];
  }
}

%extend nsEDDEngine::MENU_ITEM_LIST
{
  MENU_ITEM* get(int i)
  {
    return &$self->list[i];
  }
}

%extend nsEDDEngine::DEPBIN
{
	unsigned char getBin(int i)
  {
    return $self->bin_chunk[i];
  }
}



%extend nsEDDEngine::OP_REF_TRAIL
{
	nsEDDEngine::tag_ITEM_TBL_ELEM* GetTableElement(nsEDDEngine::IEDDEngine* edd)
	{
		// TODO: this is propably not the fastest way to find an item in the table => improve
		nsEDDEngine::ITEM_TBL_ELEM* it = nullptr;
		nsEDDEngine::FLAT_DEVICE_DIR* dir;
		int returnCode = edd->GetDeviceDir(&dir);
		if (returnCode != 0)
			return nullptr;

		for (int i = 0; i < dir->item_tbl.count; ++i)
		{
			if (dir->item_tbl.list[i].item_id == $self->desc_id)
				it = &dir->item_tbl.list[i];
		}

		return it;
	}

	nsEDDEngine::RESOLVE_INFO* getTrail(int i) const
	{
		return &$self->trail[i];
	}
}

%extend nsEDDEngine::DEFINITION 
{
	wchar_t get(int i)
	{
		return $self->data[i];
	}
}

%extend nsEDDEngine::OP_REF_TRAIL_LIST
{
	OP_REF_TRAIL* get(int i)
  {
    return &$self->list[i];
  }
}

%extend nsEDDEngine::tag_STRING_TBL {
	STRING* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::tag_SYMBOL_TBL{
	SYMBOL_TBL_ELEM* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::tag_SYMBOL_TBL_ELEM {
	wchar_t* Name()
	{
		return $self->name;
	}
}

%extend nsEDDEngine::tag_DICT_REF_TBL {
DICT_TBL_ELEM* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::ITEM_ID_LIST {
ITEM_ID get(int i)
	{
		return $self->list[i];
	}
}

%extend nsEDDEngine::TRANSACTION_LIST{
TRANSACTION* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::DATA_ITEM_LIST{
DATA_ITEM* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::RANGE_DATA_LIST{
EXPR* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::ENUM_VALUE_LIST{
ENUM_VALUE* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::ACTION_LIST{
ACTION* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::MEMBER_LIST {
MEMBER* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::RESPONSE_CODE_LIST{
RESPONSE_CODE* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::OP_MEMBER_LIST{
OP_MEMBER* get(int i)
	{
		return &$self->list[i];
	}
}

%extend nsEDDEngine::tag_IMAGE_TBL
{
IMAGE_LIST* get(int i)
{
	return &$self->list[i];
}
}

%extend nsEDDEngine::tag_IMAGE_LIST
{
IMAGE_ITEM* get(int i)
{
	return &$self->list[i];
}
}

%extend nsEDDEngine::OP_REF_INFO_LIST
{
OP_REF_INFO* get(int i)
{
	return &$self->list[i];
}
}

%extend nsEDDEngine::OP_REF_LIST
{
OP_REF* get(int i)
{
	return &$self->list[i];
}
}


%extend nsEDDEngine::ITEM_ARRAY_ELEMENT_LIST
{
ITEM_ARRAY_ELEMENT* get(int i)
{
	return &$self->list[i];
}
}

%extend nsEDDEngine::BINARY_IMAGE
{
unsigned char get(int i)
{
	return $self->data[i];
}
}

%extend nsEDDEngine::tag_ITEM_TBL_ELEM
{
	nsEDDEngine::AttributeNameSet getAllAttributes(nsEDDEngine::ITEM_TYPE itemType)
	{
		

			nsEDDEngine::AttributeNameSet allAttributes;

			switch (itemType)
			{
			case nsEDDEngine::ITYPE_NO_VALUE:              // 0
					break;
			case nsEDDEngine::ITYPE_VARIABLE:              // 1
					allAttributes = nsEDDEngine::FLAT_VAR::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_COMMAND:               // 2
					allAttributes = nsEDDEngine::FLAT_COMMAND::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_MENU:                  // 3
					allAttributes = nsEDDEngine::FLAT_MENU::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_EDIT_DISP:             // 4
					allAttributes = nsEDDEngine::FLAT_EDIT_DISPLAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_METHOD:                // 5
					allAttributes = nsEDDEngine::FLAT_METHOD::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_REFRESH:               // 6
					allAttributes = nsEDDEngine::FLAT_REFRESH::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_UNIT:                  // 7
					allAttributes = nsEDDEngine::FLAT_UNIT::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_WAO:                   // 8
					allAttributes = nsEDDEngine::FLAT_WAO::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_ITEM_ARRAY:            // 9
					allAttributes = nsEDDEngine::FLAT_ITEM_ARRAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_COLLECTION:            // 10
					allAttributes = nsEDDEngine::FLAT_COLLECTION::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOCK_B:               // 11
					allAttributes = nsEDDEngine::FLAT_BLOCK_B::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOCK:                 // 12
					allAttributes = nsEDDEngine::FLAT_BLOCK::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_RECORD:                // 14
					allAttributes = nsEDDEngine::FLAT_RECORD::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_ARRAY:                 // 15
					allAttributes = nsEDDEngine::FLAT_ARRAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_VAR_LIST:              // 16
					allAttributes = nsEDDEngine::FLAT_VAR_LIST::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_RESP_CODES:            // 17
					break;
			case nsEDDEngine::ITYPE_MEMBER:                // 19
					//todo: how to handle this case?
					break;
			case nsEDDEngine::ITYPE_FILE:                  // 20
					allAttributes = nsEDDEngine::FLAT_FILE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_CHART:                 // 21
					allAttributes = nsEDDEngine::FLAT_CHART::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_GRAPH:                 // 22
					allAttributes = nsEDDEngine::FLAT_GRAPH::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_AXIS:                  // 23
					allAttributes = nsEDDEngine::FLAT_AXIS::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_WAVEFORM:              // 24
					allAttributes = nsEDDEngine::FLAT_WAVEFORM::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_SOURCE:                // 25
					allAttributes = nsEDDEngine::FLAT_SOURCE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_LIST:                  // 26
					allAttributes = nsEDDEngine::FLAT_LIST::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_GRID:                  // 27
					allAttributes = nsEDDEngine::FLAT_GRID::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_IMAGE:                 // 28
					allAttributes = nsEDDEngine::FLAT_IMAGE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOB:                  // 29
			case nsEDDEngine::ITYPE_PLUGIN:                // 30
			case nsEDDEngine::ITYPE_TEMPLATE:              // 31
			case nsEDDEngine::ITYPE_RESERVED:              // 32
			case nsEDDEngine::ITYPE_COMPONENT:             // 33
			case nsEDDEngine::ITYPE_COMPONENT_FOLDER:      // 34
			case nsEDDEngine::ITYPE_COMPONENT_REFERENCE:   // 35
			case nsEDDEngine::ITYPE_COMPONENT_RELATION:    // 36
			default:
					// todo: handle error
					break;
			}
			return allAttributes;
	}

	nsEDDEngine::ItemBase* element(nsEDDEngine::IEDDEngine* edd)
	{
		nsEDDEngine::FLAT_DEVICE_DIR* myDir;
		auto ret = edd->GetDeviceDir(&myDir);
		if (ret != 0)
			return nullptr;

		nsEDDEngine::ItemBase *retItem = nullptr;

		for (auto blockInstance = 0; blockInstance <= myDir->blk_tbl.count; ++blockInstance)
		{
			auto n = blockInstance;

			nsEDDEngine::FDI_ITEM_SPECIFIER itemSpecifier;
			itemSpecifier.eType = nsEDDEngine::FDI_ITEM_ID;
			itemSpecifier.item.id = $self->item_id;
			itemSpecifier.subindex = 0;

			nsEDDEngine::ITEM_TYPE itemType;

			auto res = edd->GetItemType(n, &itemSpecifier, &itemType);

			if (res != 0)
				continue;

			auto attributes = nsEDDEngine_tag_ITEM_TBL_ELEM_getAllAttributes($self, $self->item_type);

			nsEDDEngine::FDI_GENERIC_ITEM* genericItem = new nsEDDEngine::FDI_GENERIC_ITEM(itemType); 

			nsEDDEngine::FDI_ITEM_SPECIFIER item_spec      = { nsEDDEngine::FDI_ITEM_ID, $self->item_id, 0 };
			nsEDDEngine::AttributeNameSet   attribute_list = attributes;

			edd->GetItem(n, 0, &item_spec, &attribute_list, L"en", genericItem);

			if (res || genericItem->errors.count > 0)
			{
				// OutputDebugString(edd->GetErrorString(genericItem->errors.list[0].rc));

				auto error1747Found = false;
				for (auto errorNumber = 0; errorNumber < genericItem->errors.count; ++errorNumber)
				{
					if (genericItem->errors.list[errorNumber].rc == -1747)
					{
						error1747Found = true;
						break;
					}
				}

				if (!error1747Found && blockInstance < 1)
				{
					if (res == -1517 && genericItem->errors.count > 0)
						return nullptr;
					else
						return nullptr;
				}
			}
			else
			{
				retItem = genericItem->item;
				break;
			}
		}

		return retItem;
	}
}




