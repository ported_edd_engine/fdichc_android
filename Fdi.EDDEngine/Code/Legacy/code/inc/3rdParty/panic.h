/* @(#) $Id: panic.h,v 1.30 1996/01/30 22:08:11 kenta Exp $
 *  Copyright 1991 Rosemount, Inc., all rights reserved.
 *
 *
 *  Panic Definitions
 *
 */

#ifndef _PANIC_INCLUDED
#define _PANIC_INCLUDED 1

#ifndef STD_H
#  include "std.h"
#endif

#ifdef __cplusplus
    extern "C" {
#endif

/*lint +fva variable number of arguments */
#if !defined(NO_P4_DOS)

#if defined(MSDOS) || defined(CODECENTER) || defined(SABER) || defined(SVR4) || defined(_WIN32)

extern void panic P((char *,...));

#else

extern void panic P((const char *,...));

#endif

#endif

/*lint -fva end of varying arguments */

#ifdef __cplusplus
    }
#endif

#endif
