#pragma once


#include "cm_lib.h"
#include "AMSDds/table.h"
#include "cm_rod.h"
#include "rmalloc.h"
class ASTRING;

const int DD_PATH_LENGTH = 256 ;


class CConnectionMgr
{
private:
	ACTIVE_BLK_TBL				active_blk_tbl;
	ACTIVE_DEV_TBL				active_dev_tbl;
	
	NETWORK_TBL			network_tbl;
	
	/* Function Prototypes for Active Block Information */
public:
	CConnectionMgr();
	~CConnectionMgr();
    //wchar_t release_path[DD_PATH_LENGTH];
#pragma region Active Block Table Methods
	void SET_ABT_DD_BLK_ID(BLOCK_HANDLE bh, UINT32 dbi);
	int get_active_blk_tbl_count();
	DEVICE_HANDLE ABT_ADT_OFFSET(BLOCK_HANDLE bh);
	char* ABT_TAG(BLOCK_HANDLE bh);
	void SET_ABT_TAG(BLOCK_HANDLE bh, char* t);
	int ABT_PARAM_COUNT(BLOCK_HANDLE bh);
	UINT32 ABT_DD_BLK_ID(BLOCK_HANDLE bh);
	UINT32 ABT_CR_ITEM_ID(BLOCK_HANDLE bh);
	void SET_ABT_CHAR_RECORD( BLOCK_HANDLE bh, nsEDDEngine::CCharacteristicRecord charrecord );
	void SET_ABT_BLOCK_TYPE( BLOCK_HANDLE bh, nsEDDEngine::BLOCK_TYPE block );
	OBJECT_INDEX ABT_A_OP_INDEX(BLOCK_HANDLE bh);
	void SET_ABT_A_OP_INDEX(BLOCK_HANDLE bh, OBJECT_INDEX oi);
	char* ABT_CR_TAG( BLOCK_HANDLE bh );
	void SET_ABT_PARAM_COUNT(BLOCK_HANDLE bh, unsigned int pc);
	void SET_ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh, int dbto);
	int ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh);
	void SET_ABT_ADT_OFFSET(BLOCK_HANDLE bh, DEVICE_HANDLE adto);
	DEVICE_TYPE_HANDLE ABT_ADTT_OFFSET(BLOCK_HANDLE bh);
	DEVICE_TYPE_HANDLE ADT_ADTT_OFFSET(DEVICE_HANDLE dh);
	void SET_ABT_ADTT_OFFSET(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto);
	void SET_ADT_ADTT_OFFSET(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto);
	ROD_HANDLE ABT_DD_HANDLE(BLOCK_HANDLE bh);
	ROD_HANDLE ADT_DD_HANDLE(DEVICE_HANDLE dh);

	int get_string_tbl_handle(DEVICE_HANDLE device_handle, STRING_TBL **string_tbl_handle);

	/* Active Device Table Functions*/
	DD_DEVICE_ID* ADT_DD_DEVICE_ID(DEVICE_HANDLE dh);
	void SET_ADT_DD_DEVICE_ID(DEVICE_HANDLE dh, DD_DEVICE_ID* ddi);
	
	void SET_ADT_DD_HANDLE(DEVICE_HANDLE dh, ROD_HANDLE rod_handle);
	void* ADT_DD_DEV_TBLS(DEVICE_HANDLE dh);

	/* Function Prototypes for Active Block Information */

	int set_abt_dd_blk_tbl_offset (BLOCK_HANDLE, int);
	int set_abt_param_count (BLOCK_HANDLE, unsigned int);
	int set_abt_dd_blk_id (BLOCK_HANDLE, UINT32);

	int set_abt_tag (BLOCK_HANDLE, char *);
	int set_abt_op_index (BLOCK_HANDLE, OBJECT_INDEX); // Need to change definition


	int set_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE) ; // Need to change definition
	int set_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ; // Need to change definition
	int set_abt_app_info (BLOCK_HANDLE, void *);
	int set_abt_adt_offset (BLOCK_HANDLE, DEVICE_HANDLE);
	int set_abt_usage (BLOCK_HANDLE, int);

	int set_abt_dd_handle (BLOCK_HANDLE, ROD_HANDLE); // Need to change definition
	int set_abt_dd_dev_tbls (BLOCK_HANDLE, void *); // Need to change definition
	int set_abt_dev_family (BLOCK_HANDLE, int); // Need to change definition

	/* Function Prototypes for Active Device Information */

	int set_adt_network (DEVICE_HANDLE, NETWORK_HANDLE);
	int set_adt_station_address (DEVICE_HANDLE, UINT16);
	int set_adt_oprod_handle (DEVICE_HANDLE, ROD_HANDLE);
	int set_adt_adtt_offset (DEVICE_HANDLE, DEVICE_TYPE_HANDLE);
	int set_adt_app_info (DEVICE_HANDLE, void *);
	int set_adt_usage (DEVICE_HANDLE, int);

	int set_adt_a_opod_cref (DEVICE_HANDLE, CREF);
	int set_adt_a_ddod_cref (DEVICE_HANDLE, CREF);
	int set_adt_a_mib_cref (DEVICE_HANDLE, CREF);

	int set_adt_dd_handle (DEVICE_HANDLE, ROD_HANDLE); // Need to change definition
	int set_adt_dd_dev_tbls (DEVICE_HANDLE, void *);
	int set_adt_dd_device_id (DEVICE_HANDLE, DD_DEVICE_ID *) ; // Need to change definition
	int set_adt_dev_family (DEVICE_HANDLE, int); // Need to change definition
	int set_adt_net_type (DEVICE_HANDLE, NET_TYPE); // Need to change definition
	int set_adt_net_route (DEVICE_HANDLE, NET_ROUTE *); // Need to change definition

	wchar_t* ADT_DEVICE_NAME(DEVICE_HANDLE);
	void SET_ADT_DEVICE_NAME(DEVICE_HANDLE, wchar_t*);
	/* Function Prototypes for Active Block Information */

	//int get_abt_tag (BLOCK_HANDLE, char *);
	//int get_abt_count (void);
	int get_abt_op_index (BLOCK_HANDLE, OBJECT_INDEX *);
	int get_abt_param_count (BLOCK_HANDLE, unsigned int *);
	int get_abt_dd_blk_id (BLOCK_HANDLE, ITEM_ID *);
	int get_abt_dd_blk_tbl_offset (BLOCK_HANDLE, int *);


	int get_abt_adtt_offset (BLOCK_HANDLE, DEVICE_TYPE_HANDLE *) ;
	int get_abt_dd_device_id (BLOCK_HANDLE, DD_DEVICE_ID *) ; // Need to change definition
	int get_abt_app_info (BLOCK_HANDLE, void **);
	int get_abt_adt_offset (BLOCK_HANDLE, DEVICE_HANDLE *);
	int get_abt_usage (BLOCK_HANDLE, int *);
	int get_abt_dd_handle (BLOCK_HANDLE, ROD_HANDLE *);
	int get_abt_dd_dev_tbls (BLOCK_HANDLE, void **);
	int get_abt_dev_family (BLOCK_HANDLE, int *);


	
	

	

	/* Function Prototypes for Active Device Information */

	int get_adt_count (void);
	int get_adt_network (DEVICE_HANDLE, NETWORK_HANDLE *);
	int get_adt_station_address (DEVICE_HANDLE, UINT16 *);
	int get_adt_oprod_handle (DEVICE_HANDLE, ROD_HANDLE *);
	int get_adt_app_info (DEVICE_HANDLE, void **);
	int get_adt_adtt_offset (DEVICE_HANDLE, DEVICE_TYPE_HANDLE *);
	int get_adt_usage (DEVICE_HANDLE, int *);

	int get_adt_a_opod_cref (DEVICE_HANDLE, CREF *); // Need to change definition
	int get_adt_a_ddod_cref (DEVICE_HANDLE, CREF *); // Need to change definition
	int get_adt_a_mib_cref (DEVICE_HANDLE, CREF *); // Need to change definition


	int get_adt_dd_handle (DEVICE_HANDLE, ROD_HANDLE *); // Need to change definition
	int get_adt_dd_device_id (DEVICE_HANDLE, DD_DEVICE_ID *) ; // Need to change definition
	int get_adt_dd_dev_tbls (DEVICE_HANDLE, void **); // Need to change definition
	int get_adt_dev_family (DEVICE_HANDLE, int *); // Need to change definition
	int get_adt_net_type (DEVICE_HANDLE, NET_TYPE *); // Need to change definition
	int get_adt_net_route (DEVICE_HANDLE, NET_ROUTE *); // Need to change definition
	
	NETWORK_HANDLE ADT_NETWORK(DEVICE_HANDLE dh);
	ROD_HANDLE ADT_OPROD_HANDLE(DEVICE_HANDLE dh);
	int ADT_USAGE(DEVICE_HANDLE dh);

	/* Function Prototypes for Network Information */

	int get_nt_net_count (void);
	int get_nt_net_type (NETWORK_HANDLE, NET_TYPE *);
	int get_nt_net_route (NETWORK_HANDLE, NET_ROUTE *);
	int get_nt_dev_family (NETWORK_HANDLE, int *);
	int get_nt_cfg_file_id (NETWORK_HANDLE, int *);

	/* Function Prototypes for Handle Validity */
	int valid_block_handle(BLOCK_HANDLE bh);
	int valid_device_handle (DEVICE_HANDLE);
	int valid_network_handle (NETWORK_HANDLE);
	int valid_dd_handle (ROD_HANDLE);

	/* Function Prototypes for Network Information */

	int set_nt_net_type (NETWORK_HANDLE, NET_TYPE);
	int set_nt_net_route (NETWORK_HANDLE, NET_ROUTE *);
	int set_nt_dev_family (NETWORK_HANDLE, int);
	int set_nt_cfg_file_id (NETWORK_HANDLE, int);

	/* dds_cm.c - Connection Manager DD Support Functions */

	int ds_dd_device_load (ENV_INFO *env_info, DEVICE_HANDLE, ROD_HANDLE *);
	int ds_ddod_load(DEVICE_HANDLE device_handle, ROD_HANDLE *rod_handle);
	int ds_dd_block_load (ENV_INFO *, BLOCK_HANDLE, ROD_HANDLE *);
	int ds_dd_device_load_ex (ENV_INFO *, DEVICE_HANDLE, ROD_HANDLE *); //
	int ds_dd_block_load_ex (ENV_INFO *, BLOCK_HANDLE, ROD_HANDLE *);

	/*
	*	Connection Manager Internal Function Prototypes
	*/

	/* cm_tbl.c - Connection Manager Table Functions */

	int ct_init (void);
	void ct_cleanup ();
	BLOCK_HANDLE ct_block_search (OBJECT_INDEX usObjectIndex);
	int ct_new_block(BLOCK_HANDLE *block_handle);
	void ct_free_block(ENV_INFO *, BLOCK_HANDLE block_handle);
	int ct_block_fill(OBJECT_INDEX usObjectIndex, FIND_CNF *find_cnf, BLOCK_HANDLE *block_handle); // Need to change the definition
	int ct_block_open (const wchar_t* sDeviceName, OBJECT_INDEX usObjectIndex, DD_DEVICE_ID *dd_device_id, BLOCK_HANDLE *block_handle); // Need to change the definition
	int ct_block_close (ENV_INFO *, BLOCK_HANDLE);
	int ct_device_close (ENV_INFO *, DEVICE_HANDLE);
	void ct_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs);

	DEVICE_HANDLE ct_device_search(const wchar_t *sDeviceName);
	int ct_new_device(DEVICE_HANDLE *device_handle);
	void ct_free_device(ENV_INFO *env_info, DEVICE_HANDLE device_handle);
	int ct_device_fill(FIND_CNF *find_cnf, DEVICE_ADDRESS * /*device_address*/,DEVICE_HANDLE *device_handle);
	int ct_device_open(const wchar_t* sDeviceName, DD_DEVICE_ID *dd_device_id, DEVICE_HANDLE *device_handle);

	/* cm_init.c - Connection Manager Initialization Functions */
	int cm_init (wchar_t *, int);
	void cm_cleanup ();
	void cleanup_dds_tbls ();
	int open_blk (ENV_INFO* env_info, const wchar_t* sDeviceName, ITEM_ID blockId, OBJECT_INDEX usObjectIndex, BLOCK_HANDLE* block_handle, const DDOD_HEADER *header);
	int open_device (ENV_INFO* env_info, const wchar_t *szFilename, const wchar_t* sDeviceName, DEVICE_HANDLE* device_handle, DDOD_HEADER *header );
	void init_dds_tbls (const wchar_t* pReleaseDir);
	void cm_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs);

	/* cm_comm Methods */
	int init_network_table(int cm_mode);
	void cleanup_network_table();
	int cr_init(int cm_mode);
	void cr_cleanup();

	/* cm_dds.c - Connection Manager DD Support Functions */
	int ds_init(wchar_t *path);
	void ds_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs);
#pragma endregion
};

