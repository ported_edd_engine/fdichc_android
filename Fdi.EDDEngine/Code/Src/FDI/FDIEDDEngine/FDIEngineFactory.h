#pragma once

#include "EDDEngineImpl.h"

#if defined (EXPORT_FDI_DDS_API)
	#define __FDI_DDS_API __declspec(dllexport)
#else
	#define __FDI_DDS_API __declspec(dllimport)
#endif


class __FDI_DDS_API FDIEngineFactory
{
	public:
		// Factory for an EDDEngine instance
		static CEDDEngineImpl* CreateFDIDDS( const wchar_t *sEDDBinaryFilename,
			 const wchar_t *pConfigXML, nsConsumer::IEDDEngineLogger *pIEDDEngineLogger, nsEDDEngine::EDDEngineFactoryError *pErrorCode);

	private:
		FDIEngineFactory& operator=(const FDIEngineFactory &rhs);
};
