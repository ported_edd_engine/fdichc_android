
/**
 *	@(#) $Id: cm_init.cpp,v 1.1 2012/07/10 20:59:28 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_init.c - Connection Manager Initialization Module
 *
 *	This file contains all functions pertaining to the
 *	initialization of the Connection Manager.  It also
 *	contains any functions that are common to all of the
 *	Connection Manager modules.
 *
 */

#include "STDINC.H"

#include "cm_loc.h"

#include "cmTblManipulation.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

#include "debug.h"
#include "debugon.h"


#ifdef _DEBUG
#ifdef _MFC_VER
//#include <afx.h>
#define new DEBUG_NEW
#endif
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void fatal_msg(LPCTSTR fmt, ...);

////////////////////////////////////////////////////////////////////////
//
// Initialization and cleanup
//
////////////////////////////////////////////////////////////////////////

/***********************************************************************
 *
 *	Name:	cm_cleanup
 *
 *	ShortDesc:	Closes all the blocks and closes down communications
 *
 *	Description:
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Zero for success and non-zero for failure
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/
int CConnectionMgr::cm_cleanup()
{
	//
	// Cleanup the leftover block entries,
	// this will in turn clean up the device
	// and device type entries as well
	//
	for (BLOCK_HANDLE blockHandle = 0; blockHandle < active_blk_tbl.count; blockHandle++) 
	{
		CCmTblManipulation::BlockClose(blockHandle, TRUE);
	}

	
	//
	// Free the Active Block, Device, and Device Type Tables.
	//
	free(active_blk_tbl.list);
	active_blk_tbl.list = NULL;
	active_blk_tbl.count = 0;
	active_blk_tbl.limit = 0;

	free(active_dev_tbl.list);
	active_dev_tbl.list = NULL;
	active_dev_tbl.count = 0;
	active_dev_tbl.limit = 0;



	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_init
 *
 *	ShortDesc:  Initialize the Connection Table Manager.
 *
 *	Description:
 *		The ct_init function initializes the Connection (Active)
 *		Tables by setting each table count to zero and initializing
 *		each table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
int CConnectionMgr::ct_init(void)
{
	//
	// Set each table count to zero and initialize each table
	// list pointer.
	//

	active_blk_tbl.count = 0;
	active_blk_tbl.limit = 0;
	active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM **) malloc(1);
	if (!active_blk_tbl.list) 
	{
		return(CM_NO_MEMORY);
	}

	active_dev_tbl.count = 0;
	active_dev_tbl.limit = 0;
	active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM **) malloc(1);
	if (!active_dev_tbl.list) 
	{
		return(CM_NO_MEMORY);
	}


	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  cm_init
 *	ShortDesc:  Initialize the Connection Manager.
 *
 *	Description:
 *		The cm_init function initializes the Connection Manager by
 *		initializing each of its sub-systems.
 *
 *	Inputs:
 *		mode - the Connection Manager mode to be used:
 *			   1. On-Line (for communication with devices).
 *					(CMM_ON_LINE)
 *			   2. Off-Line (for no communication with devices).
 *					(CMM_OFF_LINE)
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_init function.
 *		Return values from rod_init function.
 *		Return values from ds_init function.
 *		Return values from cr_init function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
int CConnectionMgr::cm_init (int /* mode */)
{
	FunctionDebug FD;
	FD.DEBUG_WHEN(g_debugMain);
	FD.SET_FUNCTION(_T("cm_init"));
	FD.ENTER_METHOD();

	USES_CONVERSION;

	//
	// Table Manager
	//
	int r_code = ct_init();
	if (r_code != CM_SUCCESS) 
	{
		wchar_t tmpStr[256] = {0};
		dds_error_user_string(r_code, tmpStr, 256);

		TCHAR message[1024] = {0};
        swprintf(message, 1024,_T("Table Manager failed to initialize.\n%s"), tmpStr);
		FD.OUTPUT_METHOD_EVENT(message);
		fatal_msg(message);

		FD.OUTPUT_METHOD_MESSAGE(_T("returning r_code=%ld"), r_code);
		return(r_code);
	}

	FD.OUTPUT_METHOD_MESSAGE(_T("returning CM_SUCCESS"));
	return(CM_SUCCESS);
}



