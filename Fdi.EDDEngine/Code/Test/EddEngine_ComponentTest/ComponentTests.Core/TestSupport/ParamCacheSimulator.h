#ifndef _PARAMCACHE_SIMULATOR_H_
#define _PARAMCACHE_SIMULATOR_H_

#include <windows.h>
#include <map>
#include <vector>
#include <string>
#include "nsConsumer/IParamCache.h"
#include "nsEDDEngine/IConvenience.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            class ParamCacheSimulator : public nsConsumer::IParamCache
            {
            public:
                ParamCacheSimulator();
                ~ParamCacheSimulator();

                void SetEddEngine(nsEDDEngine::IConvenience *convenience);

                void SetParamValue(
                    nsEDDEngine::FDI_PARAM_SPECIFIER *paramSpecifier,
                    nsConsumer::EVAL_VAR_VALUE *paramValue);
                
                virtual nsConsumer::PC_ErrorCode GetParamValue(
                    int iBlockInstance,
                    void* pValueSpec,
                    nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec,
                    nsConsumer::EVAL_VAR_VALUE* pParamValue);

		        nsConsumer::PC_ErrorCode GetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF* op_ref,
                    const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue);
				nsConsumer::PC_ErrorCode SetDynamicAttribute( int iBlockInstance, void* pValueSpec, const nsEDDEngine::OP_REF *pOpRef,
					const nsEDDEngine::AttributeName attributeName,  /* in */ const nsConsumer::EVAL_VAR_VALUE * pParamValue);


                std::vector<nsEDDEngine::FDI_PARAM_SPECIFIER> RequestedParameters;

            private:
                nsEDDEngine::IConvenience *_Convenience;

                typedef std::map<std::string, nsConsumer::EVAL_VAR_VALUE*> ParameterValueMap;
                ParameterValueMap _ParameterValues;

                std::string ParamSpecifierToString(nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier);
            };

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests

#endif