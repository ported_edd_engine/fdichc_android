#pragma once


#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"
	// Rod Manager


class CRodMgr
{
public:
	CRodMgr();
	~CRodMgr();
	int			rod_get_tok_major_rev(ROD_HANDLE rhandle);
	int			valid_rod_handle( ROD_HANDLE );
	int			ds_ddod_file_load( ENV_INFO *env_info, LPTSTR filename,	ROD_HANDLE *rod_handle);
	int			rod_close(ROD_HANDLE rod_handle);
	bool		is_tok_major_rev_6_or_8(ROD_HANDLE rhandle);
	bool		is_string_encoded_in_utf8(ROD_HANDLE rhandle);
	int			rod_get(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
	int			rod_get_image(ROD_HANDLE rod_handle, DDL_UINT offset, DDL_UINT size, BINARY_LANGUAGE_IMAGE* image);
	DDOD_HEADER * get_header(ROD_HANDLE rhandle);
private:
	ROD_TBL		rod_tbl; 
	RHEAP		rod_heap; 

	char *		rod_malloc(int size);
	char *		rod_calloc(int number, int size);
	char *		rod_realloc(char *pointer, int size);
	void		rod_free(char *pointer);
	void		rod_object_free(OBJECT *object);
	int			rod_member(char *pointer);
	ROD_HANDLE	rod_new_rod(wchar_t * filename);
	ROD_HANDLE	rod_open(OBJECT *object_0, ROD_HANDLE rod_handle);
	int			rod_object_lookup(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT **object);
	int			rod_put_image_data(BINARY_IMAGE *image_data, ROD_HANDLE rod_handle);
	int			rod_image_heap_check(BINARY_IMAGE *image);
	int			rod_put(ROD_HANDLE rod_handle, OBJECT_INDEX object_index, OBJECT *object);
	int			rod_object_heap_check(OBJECT *object);
	int			rod_set_header(ROD_HANDLE rhandle, DDOD_HEADER header);
	int			rod_set_tok_major_rev(ROD_HANDLE rhandle, int tok_major_rev);
	int			ds_get_object( ENV_INFO* env_info, FILE * file,	ROD_HANDLE  rod_handle);
	int			ds_get_image_data(FILE * file,	DDOD_HEADER* header, ROD_HANDLE rod_handle);
	int			ds_get_object_value(FILE * file, DDOD_HEADER *header, OBJECT *object);	
};


