#pragma once

#include <nsEDDEngine/ProtocolType.h>
using nsEDDEngine::ProtocolType;
#include "nsEDDEngine/Attrs.h"
using nsEDDEngine::ACTION_LIST;
#include "nsConsumer/IParamCache.h"
#include <ddbdefs.h>
#include "nsConsumer/IEDDEngineLogger.h"
#include "stdstring.h"

#ifdef EDDENGINEMI_EXPORTS
#define __MI_API __declspec(dllexport)
#else
#define __MI_API __declspec(dllimport)
#endif


class GetDataEventArgs
{
public:
	CValueVarient* ItemIdentifier;
	unsigned long luItemSubIndex;
	long lPropertyType;

	GetDataEventArgs(CValueVarient* vItemIdentifier, unsigned long luSubIndex, long lPropType)
	{
		ItemIdentifier = vItemIdentifier;
		luItemSubIndex = luSubIndex;
		lPropertyType = lPropType;
	}
	
private:
	GetDataEventArgs();		// Don't use default constructor
};


class ExecuteEventArgs
{
public:
	CValueVarient* vProperty;
	CValueVarient* vPropertyValue1;
	CValueVarient* vPropertyValue2;

	ExecuteEventArgs(CValueVarient* varg1, CValueVarient* varg2, CValueVarient* varg3)
	{
		vProperty = varg1;
		vPropertyValue1 = varg2;
		vPropertyValue2 = varg3;
	}

private:
	ExecuteEventArgs();		// Don't use default constructor
};



class IManagedMiEngineSig
{
public:
	virtual ~IManagedMiEngineSig(void) {};

	virtual int engineGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen ) = 0;
	virtual void OnMethodComplete (nsEDDEngine::Mth_ErrorCode iMethodCompleteReturnCode) = 0;
	virtual bool waitForMiEngineComplete() = 0;
	virtual int OnCommCmdExecute (int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize) = 0;
	virtual void OnCommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity) = 0;
	virtual	int GetValueByItemID2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId, long lPropertyType, CValueVarient *pvtValue ) = 0;
	virtual int engineGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput) = 0;
	virtual	int ResolveItemNameToID( CStdString sItemName, unsigned long *memberID ) = 0;

	virtual int OnListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount) = 0;
	virtual int OnUIExecute(ACTION_UI_DATA& stUIData) = 0;
	virtual int OnUIMethRspExecute(CValueVarient& vtInputValue) = 0;
	virtual int OnRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol) = 0;
	virtual int OnDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen) = 0;
	virtual int OnLitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen) = 0;
	virtual int OnSendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType) = 0;
	
	virtual int OnResponseCodeStringExecute(unsigned long iItemId, unsigned long iMemberId, int iRsponseCode, CValueVarient *pvtValue) = 0;
	virtual int OnDdsErrorExecute(wchar_t* pErrorString) = 0;
	virtual unsigned long OnCommErrorExecute() = 0;
	virtual int OnResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId) = 0;
	virtual int engineAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef) = 0;

	virtual	int OnAccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType) = 0;

	virtual int engineAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *vtValue, variableType_t eValType, AccessType eAccessType, long lSize) = 0;
	virtual int engineAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType) = 0;
	virtual int engineWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal) = 0;
	virtual ITEM_ID OnBlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType) = 0;
	virtual int OnBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount) = 0;
	virtual int OnBlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex) = 0;
	virtual int OnBlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned  long *pulRelativeIndex) = 0;
	virtual int OnGetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data) = 0;
	virtual void engineMethodDebugMessage(unsigned long lineNumber,
		/* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml) = 0;
	virtual void engineLogMessage(int priorityVal, wchar_t *msg) = 0;
	virtual int engineIsOffline() = 0;
	virtual long engineSendCommand(unsigned long commandId, long *commandError) = 0;
	virtual int engineAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType) = 0;
	virtual int engineGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString) = 0;
	virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0) = 0;


    wchar_t engine_sLanguageCode[10]{0};
};

class IManagedServerMi
{
public:
	virtual ~IManagedServerMi(void) {};

	virtual int serverGetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen ) = 0;
	virtual void MethodComplete(nsEDDEngine::Mth_ErrorCode retVal) = 0;
	virtual int CommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize) = 0;
	virtual void CommitValueExecute(nsEDDEngine::ChangedParamActivity onExitActivity) = 0;
	virtual int GetValueExecute2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, unsigned long ulMemberID, long lParamType, CValueVarient *pvtValue ) = 0;
	virtual int serverGetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemID, ACTION_LIST *pOutput) = 0;
	virtual int ResolveNameToIDExecute( CStdString sItemName, unsigned long *memberID ) = 0;
	virtual int ListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount) = 0;
	virtual int UIExecute(ACTION_UI_DATA& stUIData) = 0;
	virtual int UIMethRspExecute(CValueVarient& vtRsp) = 0;
	virtual int RspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol) = 0;
	virtual int DictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen) = 0;
	virtual int LitStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen) = 0;
	virtual int ParamSendExecute(const PARAM_REF *pOpRef, AccessType iAccessType) = 0;
	
	virtual	int ResponseCodeStringExecute(int iItemId, int iMemberId, int iRsponseCode, CValueVarient *pvtValue) = 0;
	virtual int DdsErrorExecute(wchar_t* pErrorString) = 0;
	virtual unsigned long CommErrorExecute() = 0;
	virtual int ResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId) = 0;
	virtual int serverAssign(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef) = 0;

    virtual int AccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType) = 0;

	virtual int serverAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eValType, AccessType eAccessType, long lSize) = 0;
	virtual int serverAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType) = 0;
	virtual int serverWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal) = 0;
	virtual ITEM_ID BlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType) = 0;
	virtual int BlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount) = 0;
	virtual	int BlockInstanceByObjIndexExecute(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex) = 0;
	virtual int BlockInstanceByBlockTagExecute(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex) = 0;
	virtual int GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data) = 0;
	virtual void serverMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml) = 0;
	virtual void serverLogMessage(int priorityVal, wchar_t *msg) = 0;
	virtual int serverIsOffline() = 0;
	virtual long serverSendCommand(unsigned long commandId, long *commandError) = 0;
	virtual int serverAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType) = 0;
	virtual int serverGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString) = 0;
	virtual void MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0) = 0;

	const wchar_t* server_sLanguageCode;
};


class IManagedServerMiSig
{
public:
	virtual ~IManagedServerMiSig() {};

	virtual int ExecuteMethod(long lMethodItemID, ProtocolType protocol) = 0;
	virtual void SetCancelled() = 0;
	virtual void SetMethodUIEvent() = 0;
};

class __MI_API ManagedServerMiFactory
{
public:
	static IManagedServerMiSig * CreateManagedServerMi(IManagedMiEngineSig * engine);

private:
	ManagedServerMiFactory& operator=(const ManagedServerMiFactory &rhs);
};

