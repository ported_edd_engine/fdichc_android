/*	
 * 
 *	11/10/94 @(#)tx_hart.h	1.3
 *	Copyright 1992 Rosemount, Inc.- All rights reserved
 *
 */

#ifndef TX_HART_H
#define TX_HART_H


const BYTE MIN_SHORT_ADDRESS = 0;
const BYTE MAX_SHORT_ADDRESS = 63;

#define MAX_DATA_BYTES 252	// includes resp_code and device_status
#define MAX_DATA_BITS (MAX_DATA_BYTES * 8)

#define	SHORT_ADDR_TYPE			(0x1)
#define LONG_ADDR_TYPE			(0x2)
typedef	int HART_ADDR_TYPE ;

/* Format of a HART extended address. */
typedef BYTE LONG_ADDRESS[5];

/* Buffer used to store the address field from an incoming HART message. Address
   may be in long or short format. */
typedef struct
{
	HART_ADDR_TYPE	AddressType;
    BYTE			ShortAddress;	/* Short format address. */
    LONG_ADDRESS	LongAddress;	/* Long format address. */
} HART_ADDRESS;

	//*********************************************************************************************
	//**			CAUTION				CAUTION				CAUTION
	//**	
	//**	This OPCserver method presumes knowledge of Plantserver data structures.
	//**	
	//**	In many cases changes to the Plantserver's DL_REQUEST structure also requires
	//**	changes to CAMSOPCItemSuffixHARTFrame:: (byte offsets, etc.).
	//**	
	//**			CAUTION				CAUTION				CAUTION
	//*********************************************************************************************

/* A message from the application to be transmitted by layer 2.  */
typedef struct DL_request {
    HART_ADDRESS address;
    unsigned char command;                  /* Command number of transmit message. */
    unsigned char byte_count;
    unsigned char data[250];                 /* Data to transmit. */
} DL_REQUEST;

typedef enum master_address {
     HL_SECONDARY_MASTER, HL_PRIMARY_MASTER
} MASTER_ADDRESS;

/* Macro to validate the master address. */
#define HL_VALID_MASTER_ADDR(ADDR)				\
  ((ADDR) == HL_PRIMARY_MASTER ? 1				\
   : (ADDR) == HL_SECONDARY_MASTER ? 1 : 0)

#endif
