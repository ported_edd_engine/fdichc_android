#pragma once

#include "MethodThread.h"
#include <Ole/EDDLMethInterp/ManagedMiInterfaces.h>
#include "ddbGeneral.h"		//for g_nDeviceHandle


class CProfibusDPMethodThread : public CMethodThread
{
public:
	CProfibusDPMethodThread(IManagedServerMi * managedMi, long lMethodID, ProtocolType protocol);

	virtual ~CProfibusDPMethodThread();
	virtual nsEDDEngine::Mth_ErrorCode ExecuteMethodFunction();
	virtual BOOL InitInstance(); 
	void MethodExecutionThreadProc(const void* arg);
	void CreateMethodExecutionThread();


protected:
	virtual int GetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen );
	virtual int SendCommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize);
	virtual void CommitValues(nsEDDEngine::ChangedParamActivity onExitActivity);
	virtual int GetItemByItemId2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, unsigned long ulMemberID, CStdString sProperty, CValueVarient *pOutput );
	virtual int GetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, ACTION_LIST *pOutput);
	virtual int GetItemIdByName( CStdString sItemName, unsigned long *pItemId );
	virtual int SendListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount);
	virtual int SendUIExecute(ACTION_UI_DATA& stUIData);
	virtual int ReceiveUIMethRspExecute(CValueVarient& vtRsp);
	virtual int GetRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol);
	virtual int GetDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen);
	virtual int GetLiteralString( unsigned long ulIndex, wchar_t *pOutput, long lStrLen );
	virtual int GetResponseCodeStringExecute(int iItemId, int iMemberId, int iRsponseCode, CValueVarient *pvtValue);
	virtual int getDdsErrorExecute(wchar_t* pErrorString);
	virtual unsigned long getCommErrorExecute();
	virtual int SendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType);
	virtual int getResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId);
	virtual int assignExecute(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef);

    virtual int threadAccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType);

	virtual int threadAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eType, AccessType eAccessType, long lSize);
	virtual int threadAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType);
	virtual int threadWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal);
	virtual ITEM_ID resolveBlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType);
	virtual int getBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount);
	virtual int getBlockInstanceByObjIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex);
	virtual int getBlockInstanceByBlockTag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex);
	virtual int GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data);
	virtual void threadLogMessage(int priorityVal, wchar_t *msg);
	virtual void threadMethodDebugMessage(unsigned long lineNumber, /* in */ const wchar_t* currentMethodDebugXml, /* out */ const wchar_t** modifiedMethodDebugXml);
	virtual int threadIsOffline();
	virtual long threadSendCommand(unsigned long commandId, long *commandError);
	virtual int threadAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType);
	virtual int threadGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString);
	virtual void threadMILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename = "",  UINT32 lineNumber = 0);


private:
	IManagedServerMi *m_ManagedMi;
	static DevInfcHandle_t g_nDeviceHandle;
};
