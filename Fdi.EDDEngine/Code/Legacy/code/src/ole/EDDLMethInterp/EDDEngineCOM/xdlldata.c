// wrapper for dlldata.c

#ifdef _MERGE_PROXYSTUB // merge proxy stub DLL

#define REGISTER_PROXY_DLL //DllRegisterServer, etc.

//#define _WIN32_WINNT 0x0500	//for WinNT 4.0 or Win95 with DCOM
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif

#define USE_STUBLESS_PROXY	//defined only with MIDL switch /Oicf

#pragma comment(lib, "rpcns4.lib")
#pragma comment(lib, "rpcrt4.lib")

#define ENTRY_PREFIX	Prx

#include "dlldata.c"
#include "EDDEngineCOM_p.c"
#else
#define void_error
#endif //_MERGE_PROXYSTUB
