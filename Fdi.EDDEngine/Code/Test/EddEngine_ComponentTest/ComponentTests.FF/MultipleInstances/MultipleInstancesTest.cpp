#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;
namespace ComponentTests
{
	namespace FF
	{
		[TestClass]
		public ref class When_instantiating_multiple_edd_engine_instances_from_different_threads_with_FF5
		{
			static bool _isCrashed = false;

		public:
			[ClassInitialize]
			static void Setup(TestContext^ context)
			{				
				System::Threading::Thread^ thread1 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
				System::Threading::Thread^ thread2 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
				System::Threading::Thread^ thread3 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
				System::Threading::Thread^ thread4 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
				System::Threading::Thread^ thread5 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 
				System::Threading::Thread^ thread6 = gcnew System::Threading::Thread(gcnew System::Threading::ThreadStart(ThreadProc)); 

				thread1->Start();
				thread2->Start();
				thread3->Start();
				thread4->Start();
				thread5->Start();
				thread6->Start();

				thread1->Join();
				thread2->Join();
				thread3->Join();
				thread4->Join();
				thread5->Join();
				thread6->Join();

			}

			static void ThreadProc()
			{
				try
				{	
					EddEngineController^ controller = gcnew EddEngineController("FF\\TestExampleDDFF5\\001151\\0c02\\0201.ff5");
					ParamCacheSimulator *paramCache = new ParamCacheSimulator();
					controller->Initialize(paramCache);
					paramCache->SetEddEngine(controller->Instance);
				}
				catch (...)
				{
					_isCrashed = true;
				}

			}

			[TestMethod, TestCategory("MultipleInstancesTest")]
			void It_should_not_crash()
			{
				Assert::IsFalse(_isCrashed);
			}
		};
	}
}
