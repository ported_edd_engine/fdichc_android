////////////////////////////////////////////////////////////////////////////////////////////////////
// File: "InvokeReparamHelper.h"
//
// Author: Mike DeCoster
//
// Creation Date: 5-24-07
//
// Purpose: Helper class that helps build the params for an InvokeFunction.  This is used
// to redirect new functionality to old functionality
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __INVOKEREPARAMHELPER_H_
#define __INVOKEREPARAMHELPER_H_

#include "INTER_VARIANT.h"
#include "ParserDeclarations.h"
#include "BuiltInInfo.h"

class CBuiltIn;
class hCddbDevice;
class FunctionExpression;

class CInvokeReparamHelper
{
public:
	CInvokeReparamHelper(hCddbDevice* pDevice);
	virtual ~CInvokeReparamHelper();

	bool PushFuncExpression(FunctionExpression* pFuncExp);
	////////////////////////////////////////////////////////////////////////////////////////////////
	// PushParam
	//
	// pushes the param as is
	//
	// IN:		param			the param to push
	//
	// RETURN:	bool			true on success, false if no room to push
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam(INTER_VARIANT& param);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// PushParam_DeviceGlobal
	//
	// builds a new INTER_VARIANT to store the numeric ID of the device parameter
	// by translating values stored in the ComplexExpression object accessible via
	// the FunctionExpression pointer
	//
	// IN:		pFuncExp		the function expression containing the ComplexExpression
	//								that has the token we are interested in
	//
	//			uFuncParamNum	indicates which parameter of the FunctionExpression we should
	//								get our token from
	//
	// RETURN:	bool			true on success, false if no room to push
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam_DeviceGlobal(FunctionExpression* pFuncExp, unsigned int uFuncParamNum);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// PushParam_LocalVar
	//
	// builds a new INTER_VARIANT to store the string name of the local variable
	//
	// IN:		pFuncExp		the function expression containing the ComplexExpression
	//								that has the token we are interested in
	//
	//			uFuncParamNum	indicates which parameter of the FunctionExpression we should
	//								get our token from
	//
	// RETURN:	bool			true on success, false if no room to push
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam_LocalVar(FunctionExpression* pFuncExp, unsigned int uFuncParamNum);

	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam_DictionaryID(FunctionExpression* pFuncExp, unsigned int uFuncParamNum);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// PushParam_Literal
	//
	// places the literal string passed in into an INTER_VARIANT and places that on the stack
	// of params
	//
	// RETURN:	bool			true on success, false if no room to push
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam_Literal(const char* szConst);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// PushParamNull
	//
	// Places a 0 int on the param list
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool PushParam();

	////////////////////////////////////////////////////////////////////////////////////////////////
	// ReInvoke
	//
	// Calls InvokeFunction on m_pBuiltIn with newly mimic'd builtin
	////////////////////////////////////////////////////////////////////////////////////////////////
	bool ReInvoke(CBuiltIn* pBuiltIn);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Simple Accessors and Mutators
	////////////////////////////////////////////////////////////////////////////////////////////////
	void SetName(BUILTIN_NAME enName) { m_enName = enName; }
	void SetReturnValue(INTER_VARIANT *pVarReturnValue) { m_pReturnValue = pVarReturnValue; }
	void SetBuiltInReturnCode(int* pBuiltinReturnCode) { m_pBuiltInReturnCode = pBuiltinReturnCode; }

protected:
	INTER_VARIANT m_pVarParams[MAX_NUMBER_OF_FUNCTION_PARAMETERS];
	unsigned int m_uNumParams;
	hCddbDevice* m_pDevice;
	BUILTIN_NAME m_enName;
	INTER_VARIANT* m_pReturnValue;
	int *m_pBuiltInReturnCode;
	FunctionExpression *m_pFuncExp;

	// unimplemented
	CInvokeReparamHelper(CInvokeReparamHelper& rs) { ; }
	CInvokeReparamHelper& operator=(CInvokeReparamHelper& rs) { return *this; }
};

#endif