#ifndef _HELPER_H
#define _HELPER_H

#include <string>
#include "nsConsumer/IBuiltinSupport.h"
#include "BuiltinSupportSimulator.h"
#include <nsEDDEngine/Common.h>

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            class Helper
            {
            private:
                Helper();

            public:
                static nsEDDEngine::STRING* CreateSTRINGFromWstring(const std::wstring& name);
                static std::wstring CreateWstringFromSTRING(const nsEDDEngine::STRING& text);
                static void SetAcknowledgeFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode errorCode);
                static void SetSelectionRequestFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode errorCode, unsigned selectionResult);
                static void SetListInsertFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode , int* indexCalledWith);
                static void SetListDeleteElementAtFunction(BuiltinSupportSimulator* builtinSupport, nsConsumer::BS_ErrorCode, int* indexProvided, int* countProvided);
                static void SetGetDynamicAttributeFunctionSingleCall(BuiltinSupportSimulator* builtinSupport, nsConsumer::PC_ErrorCode, nsConsumer::EVAL_VAR_VALUE& valueToReturn);
                static void SetGetParamValueFunction(BuiltinSupportSimulator* builtinsupport, nsConsumer::PC_ErrorCode errorCode, ITEM_ID* calledItemId, SUBINDEX* calledSubindex, nsEDDEngine::ParamSpecifierType * calledType, nsConsumer::EVAL_VAR_VALUE& valueToReturn);
            };

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests

#endif // _HELPER_H
