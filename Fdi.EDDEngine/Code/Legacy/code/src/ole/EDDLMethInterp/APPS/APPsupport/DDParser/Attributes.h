
/*!!!!!   Take care of moderating the contents of this file		!!!!!*/


#ifndef ATTRS_H
#define ATTRS_H

//#include <windows.h>

#include <iostream>

#include "DDLDEFS.H"

// moved to common as tags_sa.h    #include "DDLTags.h"
#include "TAGS_SA.H"

#include <vector>

using namespace std;


/*#ifdef __cplusplus
	extern "C" {
#endif /* __cplusplus */

/*
 * Expressions
 */

/*
 * Types of expressions.
 * The permitted type fields are the same as variables types (ddldefs.h)
 */

class Element; /*Forward declaration*/

typedef vector	<Element>  Expression;


typedef	unsigned short	OBJECT_INDEX;
typedef	unsigned long	SUBINDEX;

/*
 * Defines for REFERENCE_ITEM types
 */


/*Vibhor 260903: Adding following definition for REFERENCE*/

struct REF {
	unsigned short  type;

	union {
		ITEM_ID         id;		/* image number too  */
		Expression *     index;
		unsigned long   member;	/* member name id */

	}               val;

	REF (){type = DEFAULT_TYPE_REF;val.index = NULL;val.id = 0;};
	
	void Cleanup ();
	
};


typedef vector <REF> REFERENCE; /*Though defined as a vector , its just one Reference */


typedef vector <REFERENCE> REFERENCE_LIST;/*This is actual List of Reference */


/*
 * Binary and Dependency Info
 */

struct BININFO{
	unsigned long   size;
	unsigned char  *chunk;
};

struct DEPBIN{
	unsigned long   bin_size;
	unsigned char  *bin_chunk;
};


#define	DONT_FREE_STRING	0X00
#define	FREE_STRING			0X01
#define ISEMPTYSTRING       0X10

/*Vibhor 230903 : Defining this structure for handling enumerated varible 
references in strings */

struct ENUM_REF{
	
	union{
		ITEM_ID		iD;  /*valid if ENUMERATION_STRING_TAG*/
		REFERENCE	*ref; /*valid if ENUM_REF_STRING_TAG */ //Vibhor 051103
	}enmVar;
	unsigned long enumValue;
	ENUM_REF(){enmVar.ref=NULL;enmVar.iD=0;enumValue=0xffffffff;};
};


struct STRING {
	
	char            *str;	/* the pointer to the string */
	unsigned short  len;	/* the length of the string */
	unsigned short  flags;	/* memory allocation flags */
	unsigned long	strType; /*Simply store the tag of string type*/

	/*The above 3 guys would be Null in the following cases*/

	ENUM_REF		enumStr; /*reference to an enumerated varible*/
	ITEM_ID			varId; /*id of a variable of type string to be entered by the user @ runtime*/
	REFERENCE 		varRef; /* reference to a variable of type string
							   if present, this value has to be 
								entered by the user @ runtime!!!*/
	STRING(){ str = NULL; len=0,flags=0; enumStr.enmVar.ref = NULL;
			  enumStr.enumValue = 0L;
	          strType = DEV_SPEC_STRING_TAG;varId=0;varRef.clear();};
	
	void Cleanup();


};


/* Masks for all members */

#define MEMBER_DESC_EVALED		0X01
#define MEMBER_HELP_EVALED		0X02
#define MEMBER_LOC_EVALED		0X04
#define MEMBER_REF_EVALED		0X08

/* The masks for ITEM_ARRAY_ELEMENT */

#define IA_DESC_EVALED		MEMBER_DESC_EVALED
#define IA_HELP_EVALED		MEMBER_HELP_EVALED
#define IA_INDEX_EVALED		MEMBER_LOC_EVALED
#define IA_REF_EVALED		MEMBER_REF_EVALED


struct ITEM_ARRAY_ELEMENT{
	unsigned short  evaled;
	unsigned long   index;
	REFERENCE		item;
	STRING          desc;
	STRING          help;
	string	        mem_name;/* collections only stevev 15sep05 (for now)*/

	void Cleanup();
	ITEM_ARRAY_ELEMENT(){evaled=0;index=0;};
	ITEM_ARRAY_ELEMENT& operator=(const ITEM_ARRAY_ELEMENT& s)
	{evaled=s.evaled;index=s.index;item=s.item;desc=s.desc;
	 help=s.help;mem_name=s.mem_name; return *this;};
};


typedef vector <ITEM_ARRAY_ELEMENT> ITEM_ARRAY_ELEMENT_LIST;


/* The masks for MEMBER */

#define MEM_DESC_EVALED		MEMBER_DESC_EVALED
#define MEM_HELP_EVALED		MEMBER_HELP_EVALED
#define MEM_NAME_EVALED		MEMBER_LOC_EVALED
#define MEM_REF_EVALED		MEMBER_REF_EVALED


struct MEMBER{
	unsigned short  evaled;
	unsigned long   name;
	REFERENCE		item;
	STRING          desc;
	STRING          help;
	string	        member_name;

	MEMBER(){
		evaled=0;
		name=0;
	};
	void Cleanup();
			
};


typedef vector <MEMBER> MEMBER_LIST;

/*
 * Menu
 */

struct MENU_ITEM{
	REFERENCE	    item;
	unsigned short  qual; /*This is a bit string*/
	
	MENU_ITEM(){
	qual=0;
	};
	void Cleanup();

};


typedef vector <MENU_ITEM> MENU_ITEM_LIST;

/*
 * Response Codes
 */

/* The masks for RESPONSE_CODE */

#define RS_DESC_EVALED		0X01
#define RS_HELP_EVALED		0X02
#define RS_TYPE_EVALED		0X04
#define RS_VAL_EVALED		0X08


struct RESPONSE_CODE{
	
	unsigned short  evaled;
	unsigned short	val;
	unsigned short	type;
	STRING          desc;
	STRING          help;

	RESPONSE_CODE(){
		evaled=0,val=0,type=0;
	};
	void Cleanup();

};


typedef  vector <RESPONSE_CODE> RESPONSE_CODE_LIST;

/*
 * Relation Types
 */


struct REFRESH_RELATION{
	
		REFERENCE_LIST	 watch_list;
		REFERENCE_LIST	 update_list;

		REFRESH_RELATION(){};
	void Cleanup();
					
};



struct UNIT_RELATION{
		
		REFERENCE		unit_var;
		REFERENCE_LIST	var_units;

		UNIT_RELATION(){};
	void Cleanup();

};


/*
 * Definitions
 */

struct DEFINITION{

	unsigned long   size;
	char           *data;
	
//	DEFINITION(){};
//	~DEFINITION(){ if (data != NULL){ delete data;}};

};


struct METHOD_PARAM
{
	int param_type;
	int param_modifiers;
	char* param_name;//struct owns memory-delete[]

	void Clear(){param_type=param_modifiers=0;param_name=NULL;}
};

typedef vector <METHOD_PARAM> METHOD_PARAM_LIST;

/*
 * Variable Types
 */

struct TYPE_SIZE{
	unsigned short  type;
	unsigned short  size;
};


/*
 * Enumerations
 */

struct OUTPUT_STATUS{
	unsigned short  kind;
	unsigned short  which;
	unsigned short  oclass;

	OUTPUT_STATUS(){ kind = 0;which = 0; oclass = 0;};
//	~OUTPUT_STATUS(){};
	
};



typedef vector<OUTPUT_STATUS> OUTPUT_STATUS_LIST;

struct BIT_ENUM_STATUS{
	unsigned long   status_class;
	OUTPUT_STATUS_LIST oclasses;
	
	BIT_ENUM_STATUS(){ status_class = 0; };
	~BIT_ENUM_STATUS()
	{
		oclasses.clear();

	};
};


/* The masks for ENUM_VALUE */

#define ENUM_ACTIONS_EVALED		0X01
#define ENUM_CLASS_EVALED		0X02
#define ENUM_DESC_EVALED		0X04
#define ENUM_HELP_EVALED		0X08
#define ENUM_STATUS_EVALED		0X10
#define ENUM_VAL_EVALED			0X20


struct ENUM_VALUE{
	unsigned short  evaled;
	unsigned long   val;
	STRING          desc;
	STRING          help;
	unsigned long   func_class;	/* functional class */
	BIT_ENUM_STATUS status;
	ITEM_ID         actions;

	ENUM_VALUE(){ func_class = 0; actions = 0;evaled=0,val=0,func_class=0;};
	void Cleanup();

};


typedef vector <ENUM_VALUE> ENUM_VALUE_LIST;


/*
 * Data Fields
 */

struct DATA_ITEM{
	union {
		unsigned short  iconst;
		REFERENCE*   ref;
		float           fconst;
	}               data;
	unsigned short  type;
	unsigned short  flags;
	unsigned short  width;
	
	DATA_ITEM(){ data.ref = NULL;data.fconst=0.0;type=0;flags=0;width=0;};
	void Cleanup();

};


typedef vector<DATA_ITEM> DATA_ITEM_LIST;

/*
 * Transactions
 */

typedef vector<ITEM_ID> ITEM_ID_LIST;

struct TRANSACTION{
	unsigned long   number;
	DATA_ITEM_LIST  request;
	DATA_ITEM_LIST  reply;
	RESPONSE_CODE_LIST rcodes;
#ifdef XMTR
	REFERENCE_LIST	post_rqst_rcv_act;
#endif
	
	TRANSACTION(){
		number=0;request.clear();reply.clear();rcodes.clear();
	};
	TRANSACTION& operator=(const TRANSACTION& s)
	{number = s.number;
	request = s.request;
	reply   = s.reply;
	rcodes  = s.rcodes;	
#ifdef XMTR
	post_rqst_rcv_act=s.post_rqst_rcv_act;
#endif
	return *this;};

	void Cleanup();
};


typedef vector <TRANSACTION> TRANSACTION_LIST;

struct MIN_MAX{
	unsigned long	which; /*which min/max value*/
	union{

		ITEM_ID		id; /*ID of the variable in case of MIN / MAX -ID*/
		REFERENCE*   ref;/*Reference of the vaiable in case of MIN / MAX  - REF*/

	}variable;
	void clear() {which = 0; variable.ref = NULL;};
			
};



/*Vibhor 280903 : Moving this definition from DDLConditional.h */

class Element
{
	
public:

	//BYTE  byElemType ; /*1-21 =>OpCode ; 22 =>Int 23=>float 24-29 => ref_id */
	unsigned char byElemType;
	union {
	//	BYTE			byOpCode;
		unsigned char	byOpCode;
		unsigned long	ulConst;
		float			fConst;
		STRING*         pSTRCNST;
		ITEM_ID			varId;  /* for VAR_ID*/
		REFERENCE*		varRef; /* fpr VAR_REF*/
		MIN_MAX			minMax; /* for MIN/MAX - ID & REF*/
	}	elem;

	Element(){byElemType = 0;  elem.pSTRCNST = NULL; elem.minMax.clear();};// it's a union sjv
	Element( const Element& cconst) 
	{	byElemType = cconst.byElemType;
		memcpy(&elem, &(cconst.elem),sizeof(elem)); }

	void clean(){ byElemType = 0;/* elem = {0}; */ elem.minMax.clear();};
	
	void Cleanup();
};

/* stevev 24aug06 - the Attribute is actually a list of MIN_MAX 
 *	 but the MIN_MAX is a which AND a Conditional that resolves to an expression.
 *	 ie the List is Non-Conditional but the Value IS conditional.

struct MIN_MAX_VALUE{
	unsigned long which; //which min-max
	Expression	  value; //expression containing the min / max value 

	MIN_MAX_VALUE(){};
	void Cleanup();

};
*/

/* moved from conditional.h because this file is included in conditional.h (we need it here)*/
typedef enum {
	DDL_SECT_TYPE_DIRECT = 0  /*Direct*/
	, DDL_SECT_TYPE_CONDNL   /*Condiional*/
	, DDL_SECT_TYPE_CHUNKS    /*Possible Combination of Direct & Conditional*/
} DDL_COND_SECTION_TYPE;

typedef vector  <DDL_COND_SECTION_TYPE>     SectionCondList;
class DDlConditional;

union VALUES;
typedef vector  <VALUES>   ValueList;


struct MIN_MAX_VALUE{
	unsigned long which; //which min-max
	//Expression	  value; //expression containing the min / max value 
//	bool isMinMaxConditional;
	DDlConditional	*pCond;	//DDL_ATTR_DATA_TYPE_EXPRESSION
//	VALUES			*pVals;
//	ValueList		 directVals;
//	SectionCondList	 isChunkConditionalList;

	MIN_MAX_VALUE(){pCond=NULL; /*pVals = NULL;*/ which = 0;};
	void Cleanup();

};

typedef vector <MIN_MAX_VALUE> MIN_MAX_LIST;



/*Vibhor 230903: Adding these definitions for using in 
parsing of TYPE_SIZE attribute of a var*/

#define DEFAULT_SIZE		1
#define BOOLEAN_SIZE		1
#define FLOAT_SIZE			4
#define DURATION_SIZE		6
#define TIME_SIZE			6
#define TIME_VALUE_SIZE		6
#define DATE_AND_TIME_SIZE	7
#define DOUBLE_SIZE			8

#define DATE_SIZE			3
/*
 * Mask used to peek at the next tag in the chunk
 */
#define TAG_MASK 0x7f

#define HART_ARRAYNAME_INDICATOR	0	/* used only for type INDEX */
#define HART_SUBATTR_INDICATOR		1	/* list of subattributes exists */


/*Vibhor 270804: Start of Code*/

struct LINE_TYPE{

	unsigned short type;
	int			   qual; /*1. valid only if type == DATA
						   2. if qual = Null ==> DATA, 
							  if qual > 0 ==> DATA#*/

};
/*End LINE_TYPE*/

/*Vibhor 270804: End of Code*/

/* stevev 25mar05 - adding items */


/*
 * Grid Member Fields
 */

struct GRID_SET 
{
	STRING         desc;
	REFERENCE_LIST values;
	
	GRID_SET(){ };
	void Cleanup();
};


typedef vector<GRID_SET> GRID_SET_LIST;

/* end stevev 25mar05 */

/* stevev 06may05 ---- debug info */
typedef struct MEMBER_DEBUG_s
{
	string	symbol_name;
	unsigned long flags;
	unsigned long member_value;

	MEMBER_DEBUG_s(){flags=0;member_value=0;};
	~MEMBER_DEBUG_s(){symbol_name.erase();};
}
MEMBER_DEBUG_T;
typedef vector<MEMBER_DEBUG_T> MEMBER_DEBUG_LIST;

typedef struct ATTR_DEBUG_INFO_s
{
	unsigned long	attr_tag;
	unsigned long	attr_lineNo;
	STRING			attr_filename;
	MEMBER_DEBUG_LIST	attr_member_list;/* empty if not a member type */

	ATTR_DEBUG_INFO_s(){attr_tag=attr_lineNo=0;};
	void Cleanup();
}
ATTR_DEBUG_INFO_T;
typedef vector<ATTR_DEBUG_INFO_T*> ATTR_DEBUG_LIST;

struct ITEM_DEBUG_INFO
{
	string			symbol_name;
	STRING			file_name;
	unsigned long	lineNo;
	unsigned long	flags;
	ATTR_DEBUG_LIST attr_list;
	
	ITEM_DEBUG_INFO(){ lineNo=0;flags=0;};
	void Cleanup()   /* delete the memory in attr list of ptrs */;
};
/* end  stevev 06may05 */
/*
#ifdef __cplusplus
	 }
#endif /* __cplusplus */

#endif	/* ATTRS_H */
