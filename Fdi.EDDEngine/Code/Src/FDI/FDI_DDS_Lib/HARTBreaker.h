/*
 * CHARTBreaker is used to dump EDD binary into files.
 * The intended use is for testing the PROFIBUS Tokenizer
 *   to see if it is dumping out the same binary encoded attributes.
 */

#pragma once

#ifdef _HARTBREAKER_

#include <shlwapi.h>
#include <atlpath.h>
#include "dds_upcl.h"


class CHARTBreaker
{
public:
	CHARTBreaker(void);
	~CHARTBreaker(void);

				// Used to capture the directory where this EDD is found
	static void SetEDDDirectory(DEVICE_HANDLE dh, wchar_t *filename) ;

			// Create the directories based on ItemType and Id
	static void CreateDirectories(CPath &pathDir, const CStdString &strItemType, const CStdString &strItemId);

				// Called to dump an attribute
	static void DumpBinary(	BLOCK_HANDLE bh, unsigned long ItemId,
							char *sItemType, char *sAttrName,
							unsigned char* pChunk, unsigned long iSize);
};


enum DumpType {
	DumpString,
	DumpULong,
	DumpItemId,
	DumpScaling,
	DumpClass,
	DumpDisplaySize,
	DumpChartType,
	DumpLineColor,
	DumpLineType,
	DumpOrientation,
	DumpBlockBType,
	DumpWaveFormType,
	DumpMenuStyle,
	DumpOperation,
	DumpMethodType,
	DumpAccess,
	DumpEntry,
	DumpBoolean,
	DumpHandling,
	DumpExpr,
	DumpDescRef,
	DumpTypeSize,
	DumpPurpose,
	DumpRespCodes,
	DumpMemberList,
	DumpItemArrayElementList,
	DumpResponseCodeList,
	DumpEnumValueList,
	DumpVectorList,
	DumpItemIdList,
	DumpActionList,
	DumpResponseCodes,
	DumpTransactionList,
	DumpDataItemList,
	DumpRangeDataList,
	DumpMenuItemList,
	DumpOpRefTrailList,
	DumpUnitRelation,
	DumpRefreshRelation,
	DumpDefinition,
	DumpBinaryImage
};


class CFlatDump
{
public:
	CFlatDump(BLOCK_HANDLE bh, unsigned long ItemId, wchar_t *sItemType, bool bAlwaysCreate);
	CFlatDump(BLOCK_HANDLE bh, unsigned long ItemId, wchar_t *sItemType, ITEM_TYPE subtype);
	~CFlatDump();

	void Add(wchar_t *sAttrName, STRING* pString);
	void Add(wchar_t *sAttrName, ulong* pValue, DumpType etype);
	void Add(wchar_t *sAttrName, DESC_REF* pDescRef);
	void Add(wchar_t *sAttrName, TYPE_SIZE* pTypeSize);
	void Add(wchar_t *sAttrName, PURPOSE* pPurpose);
	void Add(wchar_t *sAttrName, EXPR* pExpr);
	void Add(wchar_t *sAttrName, MEMBER_LIST *pMemberList);
	void Add(wchar_t *sAttrName, ITEM_ARRAY_ELEMENT_LIST *pList);
	void Add(wchar_t *sAttrName, RESPONSE_CODE_LIST *pResponseCodeList);
	void Add(wchar_t *sAttrName, ENUM_VALUE_LIST *pEnumValueList);
	void Add(wchar_t *sAttrName, VECTOR_LIST *pVectorList);
	void Add(wchar_t *sAttrName, ITEM_ID_LIST *pItemIdList);
	void Add(wchar_t *sAttrName, ACTION_LIST *pActionList);
	void Add(wchar_t *sAttrName, RESPONSE_CODES *pResponseCodes);
	void Add(wchar_t *sAttrName, TRANSACTION_LIST *pTransactionList);
	void Add(wchar_t *sAttrName, DATA_ITEM_LIST *pDataItemList);
	void Add(wchar_t *sAttrName, RANGE_DATA_LIST *pRangeDataList);
	void Add(wchar_t *sAttrName, MENU_ITEM_LIST *pMenuItemList);
	void Add(wchar_t *sAttrName, OP_REF_TRAIL_LIST *pOpRefTrailList);
	void Add(wchar_t *sAttrName, UNIT_RELATION *pUnitRelation);
	void Add(wchar_t *sAttrName, REFRESH_RELATION *pRefreshRelation);
	void Add(wchar_t *sAttrName, DEFINITION *pDefinition);
	void Add(wchar_t *sAttrName, BINARY_IMAGE *pBinaryImage);

	// functions used elsewhere during FlatDump
	static TCHAR *IdToName(DEVICE_TYPE_HANDLE dth, ITEM_ID Id);
	static char *ItemTypeToString(ITEM_TYPE type);

private:
	void Construct(	BLOCK_HANDLE bh, unsigned long ItemId,
					wchar_t *sItemType, ITEM_TYPE subtype,
					bool bAlwaysCreate);

	void AddHelper(wchar_t *sAttrName, void *value, DumpType eDumpType);

	// Dump routines - use m_pDC
	void Dump_String(STRING* pString);
	void Dump_ULong(ulong* pULong);
	void Dump_Scaling(ulong* pScaling);
	void Dump_Class(ulong* pClass);
	void Dump_DisplaySize(ulong* pDisplaySize);
	void Dump_ChartType(ulong* pChartType);
	void Dump_LineColor(ulong* pLineColor);
	void Dump_LineType(ulong* pLineType);
	void Dump_Orientation(ulong* pOrientation);
	void Dump_BlockBType(ulong* pBlockBType);
	void Dump_WaveFormType(ulong* pWaveFormType);
	void Dump_MenuStyle(ulong* pMenuStyle);
	void Dump_Operation(ulong* pOperation);
	void Dump_MethodType(ulong* pMethodType);
	void Dump_Access(ulong* pAccess);
	void Dump_Entry(ulong* pEntry);
	void Dump_Boolean(ulong* pBoolean);
	void Dump_Handling(ulong* pHandling);
	void Dump_DescRef(DESC_REF* pDescRef);
	void Dump_TypeSize(TYPE_SIZE* pTypeSize);
	void Dump_Purpose(PURPOSE* pPurpose);
	void Dump_OpRefTrail(OP_REF_TRAIL* pOpRefTrail);
	void Dump_ItemId(ITEM_ID* pItemId);
	void Dump_Expr(EXPR* pExpr);
	void Dump_MemberList(MEMBER_LIST *pMemberList);
	void Dump_ItemArrayElementList(ITEM_ARRAY_ELEMENT_LIST *pList);
	void Dump_ResponseCodeList(RESPONSE_CODE_LIST *pResponseCodeList);
	void Dump_EnumValueList(ENUM_VALUE_LIST *pEnumValueList);
	void Dump_VectorList(VECTOR_LIST *pVectorList);
	void Dump_VectorItem(VECTOR_ITEM *pVectorItem);
	void Dump_ItemIdList(ITEM_ID_LIST *pItemIdList);
	void Dump_ActionList(ACTION_LIST *pActionList);
	void Dump_ResponseCodes(RESPONSE_CODES *pResponseCodes);
	void Dump_TransactionList(TRANSACTION_LIST *pTransactionList);
	void Dump_Transaction(TRANSACTION *pTransaction);
	void Dump_DataItemList(DATA_ITEM_LIST *pDataItemList);
	void Dump_DataItem(DATA_ITEM *pDataItem);
	void Dump_RangeDataList(RANGE_DATA_LIST *pRangeDataList);
	void Dump_MenuItemList(MENU_ITEM_LIST *pMenuItemList);
	void Dump_MenuItem(MENU_ITEM *pMenuItem);
	void Dump_OpRefTrailList(OP_REF_TRAIL_LIST *pOpRefTrailList);
	void Dump_UnitRelation(UNIT_RELATION *pUnitRelation);
	void Dump_RefreshRelation(REFRESH_RELATION *pRefreshRelation);
	void Dump_Definition(DEFINITION *pDefinition);
	void Dump_BinaryImage(BINARY_IMAGE *pBinaryImage);

	// Utility routines - just return values
	char *VariableTypeToString(ushort VarType);
	char *ScalingToString(ulong ulScaling);
	char *ClassToString(ulong ulClass);
	char *StatusClassToString(ulong ulStatusClass);
	char *DisplaySizeToString(ulong ulDisplaySize);
	char *ChartTypeToString(ulong ulChartType);
	char *LineTypeToString(ulong ulLineType);
	char *OrientationToString(ulong ulOrientation);
	char *BlockBTypeToString(ulong ulBlockBType);
	char *WaveFormTypeToString(ulong ulWaveFormType);
	char *MenuStyleToString(ulong ulMenuStyle);
	char *OperationToString(ulong ulOperation);
	char *HandlingToString(ulong ulHandling);
	char *ResponseCodeTypeToString(ulong ulResponseCodeType);
	char *MenuQualifierToString(ulong ulMenuQualifier);
	char *MethodTypeToString(ulong ulMethodType);
	char *AccessToString(ulong ulAccess);
	char *EntryToString(ulong ulEntry);
	char *PurposeToString(ulong ulPurpose);


private:
	DEVICE_TYPE_HANDLE m_dth;
	CPath m_pathFlat;
	CMemFile m_memFile;
	CDumpContext *m_pDC;	// Points to the CDumpContext and also indicates if
							// we want to write the file.

};


#endif	// _HARTBREAKER_
