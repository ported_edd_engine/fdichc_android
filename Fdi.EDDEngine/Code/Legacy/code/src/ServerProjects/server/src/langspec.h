// langspec.h

// used to obtain language specific information.


#ifndef __LANGSPEC_H__
#define __LANGSPEC_H__

#define DDS_COUNTRY_CODE_MARK	'|'
#define DDS_COUNTRY_CODE_SIZE	2
// combined country_code with country code marks.
#define DDS_COUNTRY_CODE_BUF_SIZE	(DDS_COUNTRY_CODE_SIZE + 2)
#define DDS_COUNTRY_CODE_DEFAULT	_T("en")	// in case we can't find it.

#endif