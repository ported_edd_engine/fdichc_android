#pragma once

#include "nsConsumer\IEDDEngineLogger.h"
#include "nsEDDEngine/ProtocolType.h"

using namespace nsConsumer;
using namespace nsEDDEngine;

class CEDDEngineLogger : public nsConsumer::IEDDEngineLogger
{
	
public:
	CEDDEngineLogger();
	~CEDDEngineLogger(void);

	bool ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory);
    void Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory);
	void CloseLogFile();
	void OpenLogFile(ProtocolType eProtocolType, bool bIsActionMethod);
private:
	//ProtocolType m_eProtocolType;
	//bool m_bIsActionMethod;
	//bool m_bIsLogFileCreated;
	FILE* m_pMethodLogFile;
	FILE* m_pMIErrorLogFile;
};