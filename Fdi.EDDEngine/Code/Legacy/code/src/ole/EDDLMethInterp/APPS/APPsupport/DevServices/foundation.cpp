/*************************************************************************************************
 *
 * $Workfile: foundation.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 *
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		the home of those tiny little classes that everything is built on
 * 4/25/02	sjv	created
 */

#include "stdafx.h"
//#include <windows.h>  /* TEMPORARY - to use Bills Code */
#include "foundation.h"
#include "Char.h"


static const int halfShift  = 10; /* used for shifting by 10 bits */
static const UTF32 halfBase = 0x0010000UL;
static const UTF32 halfMask = 0x3FFUL;

#define UNI_SUR_HIGH_START  (UTF32)0xD800
#define UNI_SUR_HIGH_END    (UTF32)0xDBFF
#define UNI_SUR_LOW_START   (UTF32)0xDC00
#define UNI_SUR_LOW_END     (UTF32)0xDFFF

/*
 * Once the bits are split out into bytes of UTF-8, this is a mask OR-ed
 * into the first byte, depending on how many bytes follow.  There are
 * as many entries in this table as there are UTF-8 sequence types.
 * (I.e., one byte sequence, two byte... etc.). Remember that sequencs
 * for *legal* UTF-8 will be 4 or fewer bytes total.
 */
static const UTF8 firstByteMark[7] = { 0x00, 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };
/*
 * Index into the table below with the first byte of a UTF-8 sequence to
 * get the number of trailing bytes that are supposed to follow it.
 * Note that *legal* UTF-8 values can't have 4 or 5-bytes. The table is
 * left as-is for anyone who may want to do such conversion, which was
 * allowed in earlier algorithms.
 */
static const char trailingBytesForUTF8[256] = {
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5
};

/*
 * Magic values subtracted from a buffer value during UTF8 conversion.
 * This table contains as many values as there might be trailing bytes
 * in a UTF-8 sequence.
 */
static const UTF32 offsetsFromUTF8[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 
		     0x03C82080UL, 0xFA082080UL, 0x82082080UL };



/* --------------------------------------------------------------------- */

/* The interface converts a whole buffer to avoid function-call overhead.
 * Constants have been gathered. Loops & conditionals have been removed as
 * much as possible for efficiency, in favor of drop-through switches.
 * (See "Note A" at the bottom of the file for equivalent code.)
 * If your compiler supports it, the "isLegalUTF8" call can be turned
 * into an inline function.
 */

/* --------------------------------------------------------------------- */

ConversionResult ConvertUTF16toUTF8 ( const UTF16** sourceStart, const UTF16* sourceEnd, 
	UTF8** targetStart, UTF8* targetEnd, ConversionFlags flags) 
{
    ConversionResult result = conversionOK;
    const UTF16* source = *sourceStart;
    UTF8* target = *targetStart;
    while (source < sourceEnd) 
	{
	UTF32 ch;
	unsigned short bytesToWrite = 0;
	const UTF32 byteMask = 0xBF;
	const UTF32 byteMark = 0x80; 
	const UTF16* oldSource = source; /* In case we have to back up because of target overflow. */
	ch = *source++;
	/* If we have a surrogate pair, convert to UTF32 first. */
	if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_HIGH_END) 
	{
	    /* If the 16 bits following the high surrogate are in the source buffer... */
	    if (source < sourceEnd) 
		{
		UTF32 ch2 = *source;
		/* If it's a low surrogate, convert to UTF32. */
		if (ch2 >= UNI_SUR_LOW_START && ch2 <= UNI_SUR_LOW_END) {
		    ch = ((ch - UNI_SUR_HIGH_START) << halfShift)
			+ (ch2 - UNI_SUR_LOW_START) + halfBase;
		    ++source;
		} 
		else if (flags == strictConversion) 
		{ /* it's an unpaired high surrogate */
		    --source; /* return to the illegal value itself */
		    result = sourceIllegal;
		    break;
		}
	    } 
		else 
		{ /* We don't have the 16 bits following the high surrogate. */
		--source; /* return to the high surrogate */
		result = sourceExhausted;
		break;
	    }
	} 
	else if (flags == strictConversion) 
	{
	    /* UTF-16 surrogate values are illegal in UTF-32 */
	    if (ch >= UNI_SUR_LOW_START && ch <= UNI_SUR_LOW_END) 
		{
		--source; /* return to the illegal value itself */
		result = sourceIllegal;
		break;
	    }
	}
	/* Figure out how many bytes the result will require */
	if (ch < (UTF32)0x80) {	     bytesToWrite = 1;
	} else if (ch < (UTF32)0x800) {     bytesToWrite = 2;
	} else if (ch < (UTF32)0x10000) {   bytesToWrite = 3;
	} else if (ch < (UTF32)0x110000) {  bytesToWrite = 4;
	} else {			    bytesToWrite = 3;
					    ch = UNI_REPLACEMENT_CHAR;
	}

	target += bytesToWrite;
	if (target > targetEnd) {
	    source = oldSource; /* Back up source pointer! */
	    target -= bytesToWrite; result = targetExhausted; break;
	}
	switch (bytesToWrite) { /* note: everything falls through. */
	    case 4: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 3: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 2: *--target = (UTF8)((ch | byteMark) & byteMask); ch >>= 6;
	    case 1: *--target =  (UTF8)(ch | firstByteMark[bytesToWrite]);
	}
	target += bytesToWrite;
    }
    *sourceStart = source;
    *targetStart = target;
    return result;
}

//
//
//
static Boolean isLegalUTF8(const UTF8 *source, int length) 
{
    UTF8 a;
    const UTF8 *srcptr = source+length;
    switch (length) 
	{
    default: 
		return false;
	/* Everything else falls through when "true"... */
    case 4: 
		if ((a = (*--srcptr)) < 0x80 || a > 0xBF) 
		{
			return false;
		}
    case 3: 
		if ((a = (*--srcptr)) < 0x80 || a > 0xBF) 
		{
			return false;
		}
    case 2: 
		if ((a = (*--srcptr)) > 0xBF) 
		{
			return false;
		}

		switch (*source) 
		{
			/* no fall-through in this inner switch */
			case 0xE0: if (a < 0xA0) return false; break;
			case 0xED: if (a > 0x9F) return false; break;
			case 0xF0: if (a < 0x90) return false; break;
			case 0xF4: if (a > 0x8F) return false; break;
			default:   if (a < 0x80) return false;
		}

    case 1: 
		if (*source >= 0x80 && *source < 0xC2) 
		{
			return false;
		}
    }
    if (*source > 0xF4) 
	{
		return false;
	}
    return true;
}

/*
 * Exported function to return whether a UTF-8 sequence is legal or not.
 * This is not used here; it's just exported.
 */
Boolean isLegalUTF8Sequence(const UTF8 *source, const UTF8 *sourceEnd) 
{
    int length = trailingBytesForUTF8[*source]+1;
    if (source+length > sourceEnd) 
	{
		return false;
    }
    return isLegalUTF8(source, length);
}


ConversionResult ConvertUTF8toUTF16 (
	const UTF8** sourceStart, const UTF8* sourceEnd, 
	UTF16** targetStart, UTF16* targetEnd, ConversionFlags flags )
{
    ConversionResult result = conversionOK;
    const UTF8* source = *sourceStart;
    UTF16* target = *targetStart;
    while (source < sourceEnd) {
	UTF32 ch = 0;
	unsigned short extraBytesToRead = trailingBytesForUTF8[*source];
	if (source + extraBytesToRead >= sourceEnd) {
	    result = sourceExhausted; break;
	}
	/* Do this check whether lenient or strict */
	if (! isLegalUTF8(source, extraBytesToRead+1)) {
	    result = sourceIllegal;
	    break;
	}
	/*
	 * The cases all fall through. See "Note A" below.
	 */
	switch (extraBytesToRead) {
	    case 5: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
	    case 4: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
	    case 3: ch += *source++; ch <<= 6;
	    case 2: ch += *source++; ch <<= 6;
	    case 1: ch += *source++; ch <<= 6;
	    case 0: ch += *source++;
	}
	ch -= offsetsFromUTF8[extraBytesToRead];

	if (target >= targetEnd) {
	    source -= (extraBytesToRead+1); /* Back up source pointer! */
	    result = targetExhausted; break;
	}
	if (ch <= UNI_MAX_BMP) { /* Target is a character <= 0xFFFF */
	    /* UTF-16 surrogate values are illegal in UTF-32 */
	    if (ch >= UNI_SUR_HIGH_START && ch <= UNI_SUR_LOW_END) {
		if (flags == strictConversion) {
		    source -= (extraBytesToRead+1); /* return to the illegal value itself */
		    result = sourceIllegal;
		    break;
		} else {
		    *target++ = UNI_REPLACEMENT_CHAR;
		}
	    } else {
		*target++ = (UTF16)ch; /* normal case */
	    }
	} else if (ch > UNI_MAX_UTF16) {
	    if (flags == strictConversion) {
		result = sourceIllegal;
		source -= (extraBytesToRead+1); /* return to the start */
		break; /* Bail out; shouldn't continue */
	    } else {
		*target++ = UNI_REPLACEMENT_CHAR;
	    }
	} else {
	    /* target is a character in range 0xFFFF - 0x10FFFF. */
	    if (target + 1 >= targetEnd) {
		source -= (extraBytesToRead+1); /* Back up source pointer! */
		result = targetExhausted; break;
	    }
	    ch -= halfBase;
	    *target++ = (UTF16)((ch >> halfShift) + UNI_SUR_HIGH_START);
	    *target++ = (UTF16)((ch & halfMask) + UNI_SUR_LOW_START);
	}
    }
    *sourceStart = source;
    *targetStart = target;
    return result;
}



/********************** A global foundation function ************************/
string TStr2AStr(const wstring& src)
{
	string dest;
	size_t nSize = wcslen(src.c_str());
	size_t nSuperSize = 4*nSize;
	if( nSize )
	{
		wchar_t *szsrc = new wchar_t[nSize+2];
		char *szdest = new char[nSuperSize+2];//The UTF-8 may be a larger string than the source

		memset(szsrc,0,(sizeof(wchar_t) * (nSize + 2)));
		memset(szdest,0,(sizeof(char) * (nSuperSize + 2)));	

		PS_Wcsncpy( szsrc, nSize+1, src.c_str(), nSize+1 );//size +1 to include null termination

		UTF16* pUTF16 = (UTF16 *)szsrc;
		UTF8 * pUTF8 = (UTF8 *)szdest;
		ConvertUTF16toUTF8 ( (const UTF16 **)&pUTF16, (UTF16 *)&szsrc[nSize+1], &pUTF8, (UTF8 *)&szdest[nSuperSize+1],lenientConversion);
		dest = szdest;

		delete [] szdest;
		delete [] szsrc;
	}
	return dest;
}

//
// This will return true if the string in question contains at least 1 UTF-8 character.
//
bool IsStringUTF8( unsigned char *szTestString )
{
	bool bRetVal = false;
	size_t stringLength = strlen((char*)szTestString);
	for( size_t nIndex=0; nIndex<stringLength; nIndex++ )
	{
		if( (szTestString[nIndex]&0xC0) == 0xC0 )
		{
			int nLength = 2;//assume that the length is 2 bytes
			if( (szTestString[nIndex]&0xF0) == 0xE0)//check if the first 3 hi bytes set and fourth zero
			{
				nLength=3;//Ah ha, it is three bytes long.
			}
			else if( (szTestString[nIndex]&0xF8) == 0xF0)//check if first 4 hi bytes set and fifth is zero
			{
				nLength=4;//ah ha, it is four bytes long.
			}//UTF-8 max character length is 4 bytes.
			if( isLegalUTF8((const UTF8 *)&szTestString[nIndex], nLength) )
			{
				bRetVal = true;
				if( (nIndex + nLength -1 ) < stringLength )
				{
					nIndex+=(nLength-1);
				}
			}
		}
	}
	return bRetVal;
}

wstring AStr2TStr(const string& src )
{
	wstring dest;
	size_t nSize = strlen(src.c_str());
	size_t nSuperSize = 4*nSize;
	if( nSize )
	{
		char *szsrc = new char[nSuperSize+2];
		wchar_t *szdest = new wchar_t[nSuperSize+2];//unicode will be the same size or smaller, unless it is not valid utf8

		memset(szsrc,0,(sizeof(char) * (nSuperSize + 2)));
		memset(szdest,0,(sizeof(wchar_t) * (nSuperSize + 2)));	

		PS_Strncpy(szsrc,nSuperSize+1,src.c_str(),nSize+1);//make sure to copy the null termination

		if( !IsStringUTF8( (unsigned char *)szsrc ) )
		{
			char *szlatindest = new char[nSuperSize+2];

			memset(szlatindest,0,(sizeof(char) * (nSuperSize + 2)));

			PS_Strncpy( szsrc, nSuperSize+1, src.c_str(), nSize+1 );
			latin2utf8( szsrc, szlatindest, (int)nSuperSize+1 );//This may convert into something bigger!
			PS_Strncpy( szsrc, nSuperSize+1, szlatindest, nSuperSize+1 );
			nSize = strlen(szsrc);//re-calculate the new string size.

			delete [] szlatindest;

		}
		else
		{
			PS_Strncpy( szsrc, nSuperSize+1, src.c_str(), nSize+1 );//This should be a straight UTF8 to Unicode conversion.
		}

		UTF16* pUTF16 = (UTF16 *)szdest;
		UTF8 * pUTF8 = (UTF8 *)szsrc;
		ConvertUTF8toUTF16 ( (const UTF8 **)&pUTF8, (UTF8 *)&szsrc[nSize+1], &pUTF16, (UTF16 *)&szdest[nSize+1], lenientConversion);
		dest = szdest;

		delete [] szdest;
		delete [] szsrc;
	}
	return dest;
}

//double _wtof(const wchar_t* a )
//{
//	cerr<<"ERROR: *** Wide char string to float has not been implemented!!!!"<<endl;
//	return 0.0;
//}

/****** end global functions *************************************************/
const char CitemType::itmTyName[MAX_ITYPE][16] = {
	{"Undefined"},		// 0	RESERVED_ITYPE1	
	{"Variable"},		// 1	VARIABLE_ITYPE 	
	{"Command"},		// 2	COMMAND_ITYPE   
	{"Menu"},			// 3	MENU_ITYPE      
	{"Edit-Display"},	// 4	EDIT_DISP_ITYPE 
	{"Method"},			// 5	METHOD_ITYPE    
	{"Refresh"},		// 6	REFRESH_ITYPE   
	{"Units"},			// 7	UNIT_ITYPE      
	{"Write-As-One"},	// 8	WAO_ITYPE 	    
	{"Reference-Array"},// 9	ITEM_ARRAY_ITYPE
	{"Collection"},		//10	COLLECTION_ITYPE
	{"Undefined11"},	//11	  RESERVED_ITYPE2	
	{"Block"},			//12	BLOCK_ITYPE     
	{"Program"},		//13	PROGRAM_ITYPE   
	{"Record"},			//14	RECORD_ITYPE    
	{"Value-Array"},	//15	ARRAY_ITYPE     
	{"Variable-List"},	//16	VAR_LIST_ITYPE  
	{"Response-Code"},	//17	RESP_CODES_ITYPE
	{"Domain"},			//18	DOMAIN_ITYPE
	{"Member"},			//19	MEMBER_ITYPE
	{"File"},			//FILE_ITYPE	
	{"Chart"},			//CHART_ITYPE
	{"Graph"},			//GRAPH_ITYPE
	{"Axis"},			//AXIS_ITYPE	
	{"Waveform"},		//WAVEFORM_ITYPE
	{"Source"},			//SOURCE_ITYPE
	{"List"},			//LIST_ITYPE	
	{"Grid"},			//GRID_ITYPE	
	{"Image"}			//IMAGE_ITYPE   
};    

// tied to ddbdefs.h - referenceType_t    
const char CrefType::refTyName[MAX_REFTYPE_ENUM+1][18] = {
	{"Item_ID"},		// 0___undocumented					// to ZERO
	{"ItemArray_ID"},	// 1								// to ITEM_ARRAY_ITYPE
	{"Collection_ID"},	// 2								// to COLLECTION_ITYPE
	{"via_ItemArray"},	// 3  exp-index, ref-array			// to  ITEM_ARRAY_ITYPE
	{"via_Collection"},	// 4  member-itemId, ref-collection // to  COLLECTION_ITYPE
	{"via_Record"},		// 5  member-itemId, ref-record 	// to  RECORD_ITYPE,
	{"via_Array"},		// 6  exp-index, ref-array			// to  ARRAY_ITYPE,
	{"via_VarList"},	// 7  member-itemId, ref-var list	// to  VAR_LIST_ITYPE,
	{"via_Param"},		// 8  int: parameter member id		// to  PARAM_ITYPE,-------special  200
	{"via_ParamList"},  // 9  int: param-list-name			// to  PARAM_LIST_ITYPE---special  201
	{"via_Block"},		//10  int: member name				// to  BLOCK_ITYPE,<BLOCK_CHAR_ITYPE in resolve>-special 202
	{"Block_ID"},		//11								// to BLOCK_ITYPE,
	{"Variable_ID"},	//12								// to VARIABLE_ITYPE,
	{"Menu_ID"},		//13								// to MENU_ITYPE,
	{"Edit-Display_ID"},//14								// to EDIT_DISP_ITYPE,
	{"Method_ID"},		//15								// to METHOD_ITYPE,
	{"Refresh_ID"},		//16								// to REFRESH_ITYPE,
	{"Unit-Relation_ID"},//17								// to UNIT_ITYPE,
	{"Write-As-One_ID"},//18								// to WAO_ITYPE,
	{"Record_ID"},		//19								// to RECORD_ITYPE,
	{"Array_ID"},		//20___undocumented					// to ARRAY_ITYPE,
	{"Var-List_ID"},	//21___undocumented					// to VAR_LIST_ITYPE,
	{"Program_ID"},		//22___undocumented					// to PROGRAM_ITYPE,
	{"Domain_ID"},		//23___undocumented					// to DOMAIN_ITYPE,
	{"ResponseCode_ID"},//24___undocumented					// to RESP_CODES_ITYPE
	{"File_ID"},
	{"Chart_ID"},
	{"Graph_ID"},
	{"Axis_ID"},
	{"Waveform_ID"},
	{"Source_ID"},	//30
	{"List_ID"},

	{"Image"},
	{"Separator"},
	{"Constant"},
	{"via_File"},
	{"via_List"},
	{"via_BitEnum"},
	{"Grid_ID"},	//rT_Grid_id,
	{"Rowbreak"},	//rT_Row_Break,
	{"via_Chart"},	//rT_via_Chart,		// 40
	{"via_Graph"},	//rT_via_Graph,
	{"via_Source"},	//rT_via_Source,
	{"via_Attribute"},	//rT_via_Attr,
	{"Not_a_RefType"}
};
		
const
int CitemType::ref2idTbl[] = 
{	0, /*ITEM_ID_REF */	// 0->  0 reserved??
	ITEM_ARRAY_ITYPE,	// 1->  9
	COLLECTION_ITYPE,	// 2-> 10
	ITEM_ARRAY_ITYPE, /*   3->  9 via ItemArray*/
	COLLECTION_ITYPE, /*   4-> 10 via Collection*/
	RECORD_ITYPE,		// 5-> 14 via_Record
	ARRAY_ITYPE,		// 6-> 15 via_Array
	VAR_LIST_ITYPE,		// 7-> 16 via_VarList
	PARAM_ITYPE,		// 8->200 via_Param
	PARAM_LIST_ITYPE,	// 9->201 via_ParamList
	BLOCK_ITYPE,		//10-> 12 via_Block
	BLOCK_ITYPE,		//11-> 12
	VARIABLE_ITYPE,		//12->  1
	MENU_ITYPE,			//13->  3
	EDIT_DISP_ITYPE,	//14->  4
	METHOD_ITYPE,		//15->  5
	REFRESH_ITYPE,		//16->
	UNIT_ITYPE,			//17->
	WAO_ITYPE,			//18->
	RECORD_ITYPE,		//19->
	ARRAY_ITYPE,		//20->
	VAR_LIST_ITYPE,		//21->
	PROGRAM_ITYPE,		//22->
	DOMAIN_ITYPE,		//23->
	RESP_CODES_ITYPE,	//24-> 17
	/* new reference types */
	FILE_ITYPE,
	CHART_ITYPE,
	GRAPH_ITYPE,
	AXIS_ITYPE,
	WAVEFORM_ITYPE,
	SOURCE_ITYPE,		//30
	LIST_ITYPE,

	IMAGE_ITYPE,
	MAX_ITYPE,			// Constant Separator,		/* == colbreak */
	MAX_ITYPE,			// Constant
	FILE_ITYPE,		// 35
	LIST_ITYPE,			//	  via_List,
	VARIABLE_ITYPE,		//	  via_BitEnum,
	GRID_ITYPE,
	MAX_ITYPE,			//Constant  Row_Break,
	CHART_ITYPE,		// 40 via_Chart
	GRAPH_ITYPE,		//	  via_Graph
	SOURCE_ITYPE,		//	  via_Source,
	MAX_ITYPE,			//	  via_Attr,
	MAX_REFTYPE_ENUM			//44->ERROR
};

/* global */
const  /* referenceTpe = id2refTbl[itemType]; */
referenceType_t id2refTbl[] = 
{
	rT_Item_id,				//	  RESERVED_ITYPE1			0
	rT_Variable_id,			//  VARIABLE_ITYPE 	       	1
		rT_NotA_RefType,	//  COMMAND_ITYPE           2
	rT_Menu_id,				//  MENU_ITYPE              3
	rT_Edit_Display_id,		//  EDIT_DISP_ITYPE         4
	rT_Method_id,			//  METHOD_ITYPE            5
	rT_Refresh_id,			//  REFRESH_ITYPE           6
	rT_Unit_id,				//  UNIT_ITYPE              7
	rT_WAO_id,				//  WAO_ITYPE 	       		8
	rT_ItemArray_id,		//  ITEM_ARRAY_ITYPE        9
	rT_Collect_id,			//  COLLECTION_ITYPE        10
		rT_NotA_RefType,	//	  RESERVED_ITYPE2			11
	rT_Block_id,			//  BLOCK_ITYPE             12
	rT_Program_id,			//   PROGRAM_ITYPE           13
	rT_Record_id,			//  RECORD_ITYPE            14
	rT_Array_id,			//  ARRAY_ITYPE             15
	rT_VarList_id,			//   VAR_LIST_ITYPE          16
	rT_ResponseCode_id,		//   RESP_CODES_ITYPE        17
	rT_Domain_id,			//   DOMAIN_ITYPE            18
		rT_NotA_RefType,	//   MEMBER_ITYPE            19
	rT_File_id,				//  FILE_ITYPE				20
	rT_Chart_id,			//  CHART_ITYPE				21
	rT_Graph_id,			//  GRAPH_ITYPE				22
	rT_Axis_id,				//  AXIS_ITYPE				23
	rT_Waveform_id,			//  WAVEFORM_ITYPE			24
	rT_Source_id,			//  SOURCE_ITYPE			25
	rT_List_id,				//  LIST_ITYPE				26
	rT_Grid_id,				//  GRID_ITYPE				27
	rT_Image,				//  IMAGE_ITYPE             28
		rT_NotA_RefType		// MAX_ITYPE				29	/* must be last i
};
		

string ws2as(const wstring& src)
{
	string dest;
    dest.resize(src.size());
    for (unsigned int i=0; i<src.size(); i++)
        dest[i] = src[i] < 256 ? src[i] : '?';
	return dest;
}

wstring as2ws(const string& str )
{
	wstring dest;
    dest.resize(str.size());
    for (unsigned int i=0; i<str.size(); i++)
        dest[i] = (short)str[i];
	return dest;
}

//#ifdef _UNICODE
//#ifndef UNICODE
//  #define UNICODE
//#endif
//#endif
//#include <windows.h>  /* TEMPORARY - to use Bills Code */
//#include <string>

using namespace std;

// convert UTF-8 multi-byte string to Unicode little-endian wstring
// use bill's library function
wchar_t *c2w(char *c)
{
	if (c == NULL)
	{
		return NULL;
	}

	// No output buffer since we are calculating length
	const int size = PS_MultiByteToWideChar(CP_UTF8, 0, c, (int)strlen(c), NULL, 0);
	wchar_t *w = new wchar_t[size + 1];

	if (w)
	{
		PS_MultiByteToWideChar(CP_UTF8, 0, c, (int)strlen(c), w, size);
		w[size] = _T('\0');
	}
	return (w);
}


// convert wstring to UTF-8 for printing
// use bill's library function
char *w2c(const wchar_t *w)
{
	if (w == NULL)
	{
		return NULL;
	}

	// No output buffer since we are calculating length
	const int size = PS_WideCharToMultiByte(CP_UTF8, 0, w, (int)wcslen(w), NULL, 0, NULL, NULL);
	char *c = new char[sizeof(char)*size + 1];

	if (c)
	{
		PS_WideCharToMultiByte(CP_UTF8, 0, w, (int)wcslen(w), c, size, NULL, NULL);
		c[size] = '\0';
	}

	return c;
}


// latin-1 string translated to UFT-8 narrow encoding and placed into buffer.
// pointer to buffer is returned
// characters > bufsiz are truncated
char *latin2utf8(char *s, char *buf, int bufsize)
{
	unsigned char* p;
	int i, c;

	p = (unsigned char*)s;
	i = 0;
	for (c = *(p++); c; c = *(p++))
	{
		if (i == bufsize - 1)
			break;

		if ( c >= 128 )
		{
			buf[i++] = (char)(((c & 0x7c0)>>6) | 0xc0);
			buf[i++] = (char)((c & 0x3f) | 0x80);
		}
		else
			buf[i++] = c;

	}

	buf[i] = '\0';

	return buf;
} 

// calculate storage in bytes required to hold latin1 string
// converted to utf8 WITHOUT trailing null
int latin2utf8size(char *s)
{
	int n = 0;
	while (unsigned int c = *(s++))
	{
		if ( c >= 128 )
			n += 2;

		else
			n++;
	}

	return n;
} 


// convert narrow utf8 string to wstring
// this is a functional equivalent to c2w() above
wstring UTF82Unicode(const char* src)
{
	wstring dest;
	size_t sz = strlen(src);
	dest.resize(sz);//src.size());
	unsigned int utf_idx=0;
	unsigned int ws_idx=0;
	int c, c1, c2, c3;

	while ( utf_idx < (unsigned int)sz )
	{
		c = static_cast<unsigned char>(src[utf_idx++]);

		if ((c & 0x80) == 0)
			 dest[ws_idx++] = c & 0x7F;

		else if ((c & 0xE0) == 0xC0){
			 c1 = c;
			 c2 = src[utf_idx++];
			 dest[ws_idx++] = ((c1 & 0x1F) << 6) | (c2 & 0x3F);
		}
		else if ((c & 0xF0) == 0xE0) {
			 c1 = c;
			 c2 = src[utf_idx++];
			 c3 = src[utf_idx++];
			 dest[ws_idx++] = ((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6) | (c3 & 0x3F);
		}
	}
	dest.resize(ws_idx);
	return dest;
}
  
// break text up into no more than 80 chars on a line
string addlinebreaks(char *s)
{
	string p(s);
	size_t n = p.length();
	if (n < 80)
		return p;

	string q("\n");
	size_t pos = 0;
	while (pos < n)
	{
		q = q + p.substr(pos, 80) + "\n";
		pos += 80;
	}

	return q;
}

/*************************************************************************************************
 *
 *   $History: foundation.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/Common/RdWrCommon
 * updated header and footer as per HART coding spec.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 3/31/03    Time: 12:31p
 * Updated in $/DD Tools/DDB/Common/RdWrCommon
 * 
 *************************************************************************************************
 */
