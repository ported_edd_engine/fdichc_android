#include "stdafx.h"

#include "MyParamCache.h"

#include <string>
#include <sstream>

using namespace nsEDDEngine;
static unsigned long getParamValueCallDepth = 0;

nsEDDEngine::ItemBase* element(nsEDDEngine::tag_ITEM_TBL_ELEM *self,nsEDDEngine::IEDDEngine *edd);

	ParamCache::ParamCache()
		 : eddEngine(nullptr), myDir(nullptr)
	{
		getParamValueCallDepth = 0;
	}

ParamCache::~ParamCache()
{
	if (myDir != nullptr)
	{
		delete myDir;
		myDir = nullptr;
	}
		cache.clear();
}

	void ParamCache::SetEddEngine(nsEDDEngine::IEDDEngine* engine)
	{
		this->eddEngine = engine;

		if (eddEngine->GetDeviceDir(&myDir))
		{
			myDir = nullptr;
		}
	}

#define MAX_PARAM_VALUE_CALL_DEPTH 20

nsConsumer::PC_ErrorCode ParamCache::GetParamValue(
			int iBlockInstance,
			void* pValueSpec,
			nsEDDEngine::FDI_PARAM_SPECIFIER *pParamSpec,
			nsConsumer::EVAL_VAR_VALUE * pParamValue)
{
			// remember the requested parameters...
			//RequestedParameters.push_back(*pParamSpec);

			// check pParamValue argument
			if (pParamValue == NULL)
			{
					return nsConsumer::PC_INVALID_EC;
			}
			getParamValueCallDepth++;
			// This may or may not be a circular dependecy, but it's very likely if we cross this many nested calls.
			if (getParamValueCallDepth > MAX_PARAM_VALUE_CALL_DEPTH)
			{
				getParamValueCallDepth--;
				return nsConsumer::PC_CIRC_DEPEND_EC;
			}


			// try to get the parameter value from the internal cache
			auto item = cache.find(ParamSpecifierToString(pParamSpec));
			if (item != cache.end())
			{
				if (item->second.type != VT_DDS_TYPE_UNUSED)
				{
			    nsConsumer::EVAL_VAR_VALUE paramValue = item->second;
					{
							*pParamValue = paramValue;
							getParamValueCallDepth--;
							return nsConsumer::PC_SUCCESS_EC;
					}	
				}
			}

			// parameter value is not found in the internal cache, so we return the default value

			// Get the variable type by calling IConvenience::GetParamType
			TYPE_SIZE TypeSize;

			int rcode = eddEngine->GetParamType(iBlockInstance, pParamSpec, &TypeSize );
			if (rcode != 0)
			{
				getParamValueCallDepth--;
				return nsConsumer::PC_INVALID_EC;
			}


			// set EVAL_VAR_VALUE::type and EVAL_VAR_VALUE::size
			pParamValue->type = TypeSize.type;
			pParamValue->size = TypeSize.size;
			pParamValue->val.b.assign(0, 0);

			// try to find a default value

			auto id = pParamSpec->id;
			bool found = false;

			ITEM_ID lastRec_helper = rec_helper;
			if (rec_helper == 0)
			{
				rec_helper = id;
				lastRec_helper = 0;
			}

			if (myDir != nullptr && id != lastRec_helper)
			{
				for (auto myid = 0; myid < myDir->item_tbl.count; ++myid)
				{
					if (myDir->item_tbl.list[myid].item_id != id)
						continue;

					auto itemBase = element(&myDir->item_tbl.list[myid], eddEngine);
					if (itemBase == nullptr)
						continue;

					if (itemBase->item_type == ITEM_TYPE::ITYPE_VARIABLE)
					{
						auto var = dynamic_cast<FLAT_VAR* >(itemBase);
						if (pParamValue->type == VT_INTEGER && var->default_value.eType == EXPR::EXPR_TYPE_INTEGER)
						{
							pParamValue->val.i = var->default_value.val.i;
							found  = true;
						}
						else if ((pParamValue->type == VT_UNSIGNED || pParamValue->type == VT_ENUMERATED || pParamValue->type == VT_INDEX || pParamValue->type == VT_BIT_ENUMERATED) && var->default_value.eType == EXPR::EXPR_TYPE_UNSIGNED)
						{
							pParamValue->val.u = var->default_value.val.u;
							found = true;
						}
						else if (pParamValue->type == VT_FLOAT && var->default_value.eType == EXPR::EXPR_TYPE_FLOAT)
						{
							pParamValue->val.f = var->default_value.val.f;
							found = true;
						}
						else if (pParamValue->type == VT_DOUBLE && var->default_value.eType == EXPR::EXPR_TYPE_DOUBLE)
						{
							pParamValue->val.f = var->default_value.val.f;
							found = true;
						}
					}

					break;
				}
			}

			if (!found)
			{
				switch (pParamValue->type)
				{
				case VT_INTEGER:
						pParamValue->val.i = 0;
						found = true;
						break;
				case VT_UNSIGNED:
				case VT_ENUMERATED:
				case VT_BIT_ENUMERATED:
				case VT_INDEX:
				case VT_BOOLEAN:
						pParamValue->val.u = 0;
						found = true;
						break;
				case VT_FLOAT:
						pParamValue->val.f = 0.0;
						found = true;
						break;
				case VT_DOUBLE:
						pParamValue->val.d = 0.0;
						found = true;
						break;
				case VT_ASCII:
				case VT_PACKED_ASCII:
				case VT_PASSWORD:
				case VT_EUC:  
				case VT_BITSTRING:
						pParamValue->val.s.assign(L"",false);
						found = true;
						break;
						//case VT_HART_DATE_FORMAT:
				case VT_DATE_AND_TIME:
				case VT_DURATION:
						break;
					break;
				default:
						break;
				}
			}

			if (found)
			{
				cache[ParamSpecifierToString(pParamSpec)] = *pParamValue;
			}

			rec_helper = 0;

			getParamValueCallDepth--;

			return nsConsumer::PC_SUCCESS_EC;
	}

nsEDDEngine::AttributeNameSet getAllAttributes(nsEDDEngine::tag_ITEM_TBL_ELEM *self,nsEDDEngine::ITEM_TYPE itemType){
		

			nsEDDEngine::AttributeNameSet allAttributes;

			switch (itemType)
			{
			case nsEDDEngine::ITYPE_NO_VALUE:              // 0
					break;
			case nsEDDEngine::ITYPE_VARIABLE:              // 1
					allAttributes = nsEDDEngine::FLAT_VAR::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_COMMAND:               // 2
					allAttributes = nsEDDEngine::FLAT_COMMAND::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_MENU:                  // 3
					allAttributes = nsEDDEngine::FLAT_MENU::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_EDIT_DISP:             // 4
					allAttributes = nsEDDEngine::FLAT_EDIT_DISPLAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_METHOD:                // 5
					allAttributes = nsEDDEngine::FLAT_METHOD::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_REFRESH:               // 6
					allAttributes = nsEDDEngine::FLAT_REFRESH::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_UNIT:                  // 7
					allAttributes = nsEDDEngine::FLAT_UNIT::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_WAO:                   // 8
					allAttributes = nsEDDEngine::FLAT_WAO::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_ITEM_ARRAY:            // 9
					allAttributes = nsEDDEngine::FLAT_ITEM_ARRAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_COLLECTION:            // 10
					allAttributes = nsEDDEngine::FLAT_COLLECTION::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOCK_B:               // 11
					allAttributes = nsEDDEngine::FLAT_BLOCK_B::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOCK:                 // 12
					allAttributes = nsEDDEngine::FLAT_BLOCK::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_RECORD:                // 14
					allAttributes = nsEDDEngine::FLAT_RECORD::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_ARRAY:                 // 15
					allAttributes = nsEDDEngine::FLAT_ARRAY::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_VAR_LIST:              // 16
					allAttributes = nsEDDEngine::FLAT_VAR_LIST::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_RESP_CODES:            // 17
					break;
			case nsEDDEngine::ITYPE_MEMBER:                // 19
					//todo: how to handle this case?
					break;
			case nsEDDEngine::ITYPE_FILE:                  // 20
					allAttributes = nsEDDEngine::FLAT_FILE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_CHART:                 // 21
					allAttributes = nsEDDEngine::FLAT_CHART::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_GRAPH:                 // 22
					allAttributes = nsEDDEngine::FLAT_GRAPH::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_AXIS:                  // 23
					allAttributes = nsEDDEngine::FLAT_AXIS::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_WAVEFORM:              // 24
					allAttributes = nsEDDEngine::FLAT_WAVEFORM::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_SOURCE:                // 25
					allAttributes = nsEDDEngine::FLAT_SOURCE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_LIST:                  // 26
					allAttributes = nsEDDEngine::FLAT_LIST::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_GRID:                  // 27
					allAttributes = nsEDDEngine::FLAT_GRID::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_IMAGE:                 // 28
					allAttributes = nsEDDEngine::FLAT_IMAGE::All_Attrs;
					break;
			case nsEDDEngine::ITYPE_BLOB:                  // 29
			case nsEDDEngine::ITYPE_PLUGIN:                // 30
			case nsEDDEngine::ITYPE_TEMPLATE:              // 31
			case nsEDDEngine::ITYPE_RESERVED:              // 32
			case nsEDDEngine::ITYPE_COMPONENT:             // 33
			case nsEDDEngine::ITYPE_COMPONENT_FOLDER:      // 34
			case nsEDDEngine::ITYPE_COMPONENT_REFERENCE:  // 35
			case nsEDDEngine::ITYPE_COMPONENT_RELATION:    // 36
			default:
					// todo: handle error
					break;
			}
			return allAttributes;
	}
nsEDDEngine::ItemBase* element(nsEDDEngine::tag_ITEM_TBL_ELEM *self,nsEDDEngine::IEDDEngine *edd){
		nsEDDEngine::FDI_ITEM_SPECIFIER itemSpecifier;
		itemSpecifier.eType = nsEDDEngine::FDI_ITEM_ID;
		itemSpecifier.item.id = self->item_id;
		itemSpecifier.subindex = 0;

		nsEDDEngine::ITEM_TYPE itemType;

		auto res = edd->GetItemType(0, &itemSpecifier, &itemType);

		if (res != 0)
			return nullptr;

		auto attributes = getAllAttributes(self, self->item_type);

		nsEDDEngine::FDI_GENERIC_ITEM* genericItem = new nsEDDEngine::FDI_GENERIC_ITEM(itemType); 

		nsEDDEngine::FDI_ITEM_SPECIFIER item_spec      = { nsEDDEngine::FDI_ITEM_ID, self->item_id, 0 };
		nsEDDEngine::AttributeNameSet   attribute_list = attributes;

		res = edd->GetItem(0, 0, &item_spec, &attribute_list, L"en", genericItem);

		if (res || genericItem->errors.count > 0)
		{
			OutputDebugString(edd->GetErrorString(genericItem->errors.list[0].rc));

			if (res == -1517 && genericItem->errors.count > 0)
				return genericItem->item;
			else
				return nullptr;
		}

		return genericItem->item;
	}

nsConsumer::PC_ErrorCode ParamCache::GetDynamicAttribute(int /*iBlockInstance*/, void* pValueSpec, const nsEDDEngine::OP_REF* op_ref,
			const nsEDDEngine::AttributeName attributeName, nsConsumer::EVAL_VAR_VALUE * pParamValue)
	{
			return nsConsumer::PC_ErrorCode::PC_INVALID_EC;
	}

	std::string ParamCache::ParamSpecifierToString(nsEDDEngine::FDI_PARAM_SPECIFIER* paramSpecifier)
	{
			std::stringstream result;
	
			if (paramSpecifier == nullptr)
			{
					throw "paramSpecifier is nullptr";
			}
	
			switch (paramSpecifier->eType)
			{
			case FDI_PS_ITEM_ID:
					result << "ITEM_ID:" << paramSpecifier->id;
					break;
			case FDI_PS_ITEM_ID_SI:
					result << "ITEM_ID_SI:" << paramSpecifier->id << ":" << paramSpecifier->subindex;
					break;
			case FDI_PS_PARAM_OFFSET:
					result << "PARAM_OFFSET:" << paramSpecifier->param;
					break;
			case FDI_PS_PARAM_OFFSET_SI:
					result << "PARAM_OFFSET_SI:" << paramSpecifier->param << ":" << paramSpecifier->subindex;
					break;
			default:
					throw "FDI_PARAM_SPECIFIER type unknown";
					break;
			}
	
			return result.str();
	}

