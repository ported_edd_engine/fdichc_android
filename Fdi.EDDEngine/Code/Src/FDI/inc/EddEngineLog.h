#pragma once
#include "env_info.h"


typedef enum LogSeverity
	{
       Critical,
       Error,
	   Warning,
       Information,
       Verbose,
	} LogSeverity;


void EddEngineLog(
	ENV_INFO	*env_info,
	const char *cFilename,				// source code filename
	UINT32 lineNumber,					// source code line-number
	LogSeverity severity,						// see documentation
	wchar_t* wCategory,
	LPCWSTR wMsgFormat,			        // a 'printf' format
	...								    //    and it's parameters
);


#define BssError Error
#define BssWarning Warning
#define BssInformation Information



