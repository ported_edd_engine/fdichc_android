#include "stdafx.h"
#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace FF
    {
        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_FF
        {
        public: 
            static EddEngineController^ engine;
            static ParamCacheSimulator* paramCacheSimulator;
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("FF\\TestCase0004\\TestCase0004.ff6");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);
                
                // get the ItemIds for the test_blocks
                ITEM_ID test_block_item_id;
                engine->Instance->GetItemIdFromSymbolName(L"test_block", &test_block_item_id);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = test_block_item_id;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                
                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                EVAL_VAR_VALUE test_var_2_value;
                test_var_2_value.type = VT_INTEGER;
                test_var_2_value.size = 4;
                test_var_2_value.val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, &test_var_2_value);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_contitioned_2", &test_var_1_specifier);
                FDI_GENERIC_ITEM* test_var_1 = engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE, 
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                // get the label of "test_var_1"
                auto test_var_1_label = gcnew String(dynamic_cast<FLAT_VAR*>(test_var_1->item)->label.c_str());
                
                delete pFieldbusBlockInfo;
            }
            
            [ClassCleanup]
            static void Cleanup()
            {    
                delete engine;
                delete test_var_2_specifier;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_some_missing_values_with_FF
        {
        public: 
            static EddEngineController^ engine;
            static ParamCacheSimulator* paramCacheSimulator;
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static FDI_GENERIC_ITEM* genericItem;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("FF\\TestCase0004\\TestCase0004.ff6");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                // get the ItemIds for the test_blocks
                ITEM_ID test_block_item_id;
                engine->Instance->GetItemIdFromSymbolName(L"test_block", &test_block_item_id);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = test_block_item_id;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // configure ParamCacheSimulator to return no value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, nullptr);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_contitioned_2", &test_var_1_specifier);
                genericItem =  engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE, 
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_var_2_specifier;
                delete genericItem;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_error()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(1, static_cast<int>(genericItem->errors.count));
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_the_error_BAD_ATTR()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual((UINT)nsEDDEngine::validity, (UINT) genericItem->errors.list[0].bad_attr);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_error_with_the_correct_missing_item_id()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(test_var_2_specifier->id, genericItem->errors.list[0].var_needed.op_info.id);
            }
        };

        [TestClass]
        public ref class When_Call_unconditioned_Attributes_with_FF
        {
        public: 
            static EddEngineController^ engine;
            static ParamCacheSimulator* paramCacheSimulator;
            static String^ test_var_2_label;
            
            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("FF\\TestCase0004\\TestCase0004.ff6");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                // get the ItemIds for the test_blocks
                ITEM_ID test_block_item_id;
                engine->Instance->GetItemIdFromSymbolName(L"test_block", &test_block_item_id);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = test_block_item_id;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);

                // obtain item specifier (id) for "test_var_2"
                FDI_PARAM_SPECIFIER test_var_2_specifier;
                engine->FillParamSpecifier("test_var_2", &test_var_2_specifier);

                // create value for "test_var_2"
                EVAL_VAR_VALUE test_var_2_value;
                test_var_2_value.type = VT_INTEGER;
                test_var_2_value.size = 4;
                test_var_2_value.val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(&test_var_2_specifier, &test_var_2_value);

                // request "test_var_2"; 
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_2", &test_var_1_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_var_1_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE, 
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                // get the label of "test_var_2"
                FLAT_VAR* test_var_2 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_2_label = gcnew String(test_var_2->label.c_str());

                delete genericItem;
                delete pFieldbusBlockInfo;
            }

            [ClassCleanup]
            static void Cleanup()
            {
                delete engine;
                delete test_var_2_label;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_attribute()
            {
                // check if the label of "test_var_2" has the expected value
                Assert::AreEqual("Test Variable 1", test_var_2_label);
            }
        };

        [TestClass]
        public ref class When_Call_bitenumerated_conditioned_Attributes_with_FF
        {
        public: 
            static EddEngineController^ engine;
            static ParamCacheSimulator* paramCacheSimulator;
            static FDI_PARAM_SPECIFIER* test_input_bit_enum_specifier;

            [ClassInitialize]
            static void Setup(TestContext^ context)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController("FF\\TestCase0004\\TestCase0004.ff6");
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);
                
                // get the ItemIds for the test_blocks
                ITEM_ID test_block_item_id;
                engine->Instance->GetItemIdFromSymbolName(L"test_block", &test_block_item_id);

                // create new function Block
                nsEDDEngine::CFieldbusBlockInfo* pFieldbusBlockInfo = new nsEDDEngine::CFieldbusBlockInfo();
                pFieldbusBlockInfo->m_BlockType = BLOCK_TYPE::RESOURCE_BLOCK;
                pFieldbusBlockInfo->m_CharRecord.m_ulDDItemId = test_block_item_id;
                pFieldbusBlockInfo->m_usObjectIndex = 1000;
                int iBlockInstance; // out
                const int iCount = 1;
                engine->Instance->AddFFBlocks(pFieldbusBlockInfo, &iBlockInstance, iCount);
                
                // obtain item specifier (id) for "test_input_bit_enum"
                test_input_bit_enum_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_input_bit_enum", test_input_bit_enum_specifier);

                // create value for "test_input_bit_enum"
                EVAL_VAR_VALUE test_input_bit_enum_value;
                test_input_bit_enum_value.type = VT_BIT_ENUMERATED;
                test_input_bit_enum_value.size = 4;
                test_input_bit_enum_value.val.u = 0x00008000;

                // configure ParamCacheSimulator to return special value for "test_input_bit_enum"
                paramCacheSimulator->SetParamValue(test_input_bit_enum_specifier, &test_input_bit_enum_value);

                // request "cond0027b_enum_var"; this will trigger reading of "test_input_bit_enum"
                FDI_PARAM_SPECIFIER cond0027b_enum_var_specifier;
                engine->FillParamSpecifier("cond0027b_enum_var", &cond0027b_enum_var_specifier);
                FDI_GENERIC_ITEM* cond0027b_enum_var = engine->GetGenericItem(cond0027b_enum_var_specifier.id, 
                    nsEDDEngine::ITYPE_VARIABLE, 
                    engine->GetAllAttributes(nsEDDEngine::ITYPE_VARIABLE),
                    iBlockInstance);

                // get the enumeration list of "cond0027b_enum_var"
                auto cond0027b_enum_var_label = dynamic_cast<FLAT_VAR*>(cond0027b_enum_var->item)->enums;
                
                delete pFieldbusBlockInfo;
            }
            
            [ClassCleanup]
            static void Cleanup()
            {    
                delete engine;
                delete test_input_bit_enum_specifier;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_input_bit_enum_specifier" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_item_id()
            {
                // check if "test_input_bit_enum_specifier" was requested from the parameter cache with correct item id
                Assert::AreEqual(test_input_bit_enum_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_subindex()
            {
                // check if "test_input_bit_enum_specifier" was requested from the parameter cache with correct subindex
                Assert::AreEqual(test_input_bit_enum_specifier->subindex, paramCacheSimulator->RequestedParameters[0].subindex);
            }
        };

    } // namespace FF
} // namespace ComponentTests
