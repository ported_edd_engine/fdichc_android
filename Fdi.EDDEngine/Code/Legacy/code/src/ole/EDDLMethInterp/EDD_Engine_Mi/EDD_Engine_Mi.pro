##-------------------------------------------------
##
## Project created by QtCreator 2017-12-05T17:14:24
##
##-------------------------------------------------

#QT       -= gui

#TARGET = EDD_Engine_Mi
#TEMPLATE = lib
##CONFIG +=plugin
#CONFIG += shared_and_static build_all
#DEFINES += EDD_ENGINE_MI_LIBRARY \
#           UNICODE

#QMAKESPEC=linux-g++-32
## The following define makes your compiler emit warnings if you use
## any feature of Qt which as been marked as deprecated (the exact warnings
## depend on your compiler). Please consult the documentation of the
## deprecated API in order to know how to port your code away from it.
#DEFINES += QT_DEPRECATED_WARNINGS

## You can also make your code fail to compile if you use deprecated APIs.
## In order to do so, uncomment the following line.
## You can also select to disable deprecated APIs only up to a certain version of Qt.
##DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#SOURCES += AMSMethodFunction.cpp \
#    #EDD_Engine_Mi.cpp \
#    CProfiDPServerDevice.cpp \
#    ManagedServerMi.cpp \
#    stdafx.cpp \

#HEADERS += ../../../../inc/FF/AMSUtil/DeviceIniFileDefines.h \
#    ../../../../inc/Inf/NtcSpecies/BssProgLog.h \
#    ../../../../inc/Inf/NtcSpecies/ntcassert.h \
#    ../../../../inc/Inf/NtcSpecies/Ntcassert2.h \
#    AMSMethodFunction.h\
#    CProfiDPServerDevice.h \
#    ManagedServerMi.h \

#unix {
#    target.path = /usr/lib/
#    INSTALLS += target
#}

#CONFIG(debug, debug|release) {
#    DESTDIR = ../../../../bin/debug
#    BUILD_DIR = $$PWD/debug
#}
#CONFIG(release, debug|release) {
#    DESTDIR = ../../../../bin/release
#    BUILD_DIR = $$PWD/release
#}
#include($$DESTDIR/../../../../Src/SuppressWarning.pri)

#OBJECTS_DIR = $$BUILD_DIR/.obj
#MOC_DIR = $$BUILD_DIR/.moc
#RCC_DIR = $$BUILD_DIR/.rcc
#UI_DIR = $$BUILD_DIR/.u


##PRE_TARGETDEPS += $$DESTDIR/../../lib/libDevServices.a \
##                  $$DESTDIR/../../lib/libGenBaseLib.a \
##                  $$DESTDIR/libEDD_Engine_Interfaces.a \
##                  $$DESTDIR/../../lib/libEDD_Engine_Common.a \
##                  $$DESTDIR/../../lib/libEDD_Engine_Linux.a \

##LIBS +=           $$DESTDIR/../../lib/ lDevServices \
##                  $$DESTDIR/../../lib/ lGenBaseLib \
##                  $$DESTDIR/ lEDD_Engine_Interfaces \
##                  $$DESTDIR/../../lib/ lEDD_Engine_Common \
##                  $$DESTDIR/../../lib/ lEDD_Engine_Linux \


##LIBS += $$DESTDIR/ -lGenBaseLib
#INCLUDEPATH += $$PWD/../APPS/APPsupport/BuiltinLib/
#INCLUDEPATH += $$PWD/../APPS/APPsupport/DDParser/
#INCLUDEPATH += $$PWD/../APPS/APPsupport/DevServices/
#INCLUDEPATH += $$PWD/../APPS/APPsupport/Interpreter/
#INCLUDEPATH += $$PWD/../APPS/APPsupport/MEE/
#INCLUDEPATH += $$PWD/../APPS/APPsupport/ParserInfc/
#INCLUDEPATH += $$PWD/../COMMON/
#INCLUDEPATH += $$PWD/../../../../inc/
#INCLUDEPATH += $$PWD/../../../../../../Interfaces/EDD_Engine_Interfaces/
#INCLUDEPATH += $$PWD/../GenBaseLib/
#INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Common

#android {
#    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Android
#    LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Android -Wl,--no-whole-archive
#}
#else {
#    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Linux
#    unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Linux -Wl,--no-whole-archive
#}

#unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Common -Wl,--no-whole-archive
#unix:!macx: LIBS += -L$$PWD/../../../../lib/  -Wl,--whole-archive -lDevServices -Wl,--no-whole-archive
#unix:!macx: LIBS += -L$$PWD/../../../../lib/  -Wl,--whole-archive -lGenBaseLib -Wl,--no-whole-archive
#unix:!macx: LIBS += -L$$DESTDIR/ -lEDD_Engine_Interfaces

#INCLUDEPATH += $$PWD/../../../../
#DEPENDPATH += $$PWD/../../../../
#INCLUDEPATH += $$PWD/../../../../lib/
#DEPENDPATH += $$PWD/../../../../lib/
#INCLUDEPATH += $$DESTDIR
#DEPENDPATH += $$DESTDIR

#android {
#    PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Android.a
#}
#else {
#    unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Linux.a
#}

#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Common.a
#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libDevServices.a
#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libGenBaseLib.a
#unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Interfaces.a

##contains(ANDROID_TARGET_ARCH,x86) {
##    ANDROID_EXTRA_LIBS =
##}
##LIBS += -landroid


 #-------------------------------------------------
 #
 # Project created by QtCreator 2017-12-05T17:14:24
 #
 #-------------------------------------------------

 QT       -= gui

 TARGET = EDD_Engine_Mi
 TEMPLATE = lib
 #CONFIG +=plugin
 CONFIG += shared_and_static build_all
 DEFINES += EDD_ENGINE_MI_LIBRARY \
            UNICODE

 QMAKESPEC=linux-g++-32
 # The following define makes your compiler emit warnings if you use
 # any feature of Qt which as been marked as deprecated (the exact warnings
 # depend on your compiler). Please consult the documentation of the
 # deprecated API in order to know how to port your code away from it.
 DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=gnu++11

 # You can also make your code fail to compile if you use deprecated APIs.
 # In order to do so, uncomment the following line.
 # You can also select to disable deprecated APIs only up to a certain version of Qt.
 #DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

 SOURCES += AMSMethodFunction.cpp \
     #EDD_Engine_Mi.cpp \
     CProfiDPServerDevice.cpp \
     ManagedServerMi.cpp \
     stdafx.cpp \

 HEADERS += ../../../../inc/FF/AMSUtil/DeviceIniFileDefines.h \
     ../../../../inc/Inf/NtcSpecies/BssProgLog.h \
     ../../../../inc/Inf/NtcSpecies/ntcassert.h \
     ../../../../inc/Inf/NtcSpecies/Ntcassert2.h \
     AMSMethodFunction.h\
     CProfiDPServerDevice.h \
     ManagedServerMi.h \

 unix {
     target.path = /usr/lib/
     INSTALLS += target
 }

 CONFIG(debug, debug|release) {
     DESTDIR = ../../../../bin/debug
     BUILD_DIR = $$PWD/debug
 }
 CONFIG(release, debug|release) {
     DESTDIR = ../../../../bin/release
     BUILD_DIR = $$PWD/release
 }
 include($$DESTDIR/../../../../Src/SuppressWarning.pri)

 OBJECTS_DIR = $$BUILD_DIR/.obj
 MOC_DIR = $$BUILD_DIR/.moc
 RCC_DIR = $$BUILD_DIR/.rcc
 UI_DIR = $$BUILD_DIR/.u


 #PRE_TARGETDEPS += $$DESTDIR/../../lib/libDevServices.a \
 #                  $$DESTDIR/../../lib/libGenBaseLib.a \
 #                  $$DESTDIR/libEDD_Engine_Interfaces.a \
 #                  $$DESTDIR/../../lib/libEDD_Engine_Common.a \
 #                  $$DESTDIR/../../lib/libEDD_Engine_Linux.a \

 #LIBS +=           $$DESTDIR/../../lib/ lDevServices \
 #                  $$DESTDIR/../../lib/ lGenBaseLib \
 #                  $$DESTDIR/ lEDD_Engine_Interfaces \
 #                  $$DESTDIR/../../lib/ lEDD_Engine_Common \
 #                  $$DESTDIR/../../lib/ lEDD_Engine_Linux \


 #LIBS += $$DESTDIR/ -lGenBaseLib
 INCLUDEPATH += $$PWD/../APPS/APPsupport/BuiltinLib/
 INCLUDEPATH += $$PWD/../APPS/APPsupport/DDParser/
 INCLUDEPATH += $$PWD/../APPS/APPsupport/DevServices/
 INCLUDEPATH += $$PWD/../APPS/APPsupport/Interpreter/
 INCLUDEPATH += $$PWD/../APPS/APPsupport/MEE/
 INCLUDEPATH += $$PWD/../APPS/APPsupport/ParserInfc/
 INCLUDEPATH += $$PWD/../COMMON/
 INCLUDEPATH += $$PWD/../../../../inc/
 INCLUDEPATH += $$PWD/../../../../../../Interfaces/EDD_Engine_Interfaces/
 INCLUDEPATH += $$PWD/../GenBaseLib/
 INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Common
#-INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Linux

android {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Android
    LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Android -Wl,--no-whole-archive
}
else {
    INCLUDEPATH += $$PWD/../../../../../../Src/EDD_Engine_Linux
    unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Linux -Wl,--no-whole-archive
}


#-unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Linux -Wl,--no-whole-archive
 unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lEDD_Engine_Common -Wl,--no-whole-archive
 unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lDevServices -Wl,--no-whole-archive
 unix:!macx: LIBS += -L$$PWD/../../../../lib/ -Wl,--whole-archive -lGenBaseLib -Wl,--no-whole-archive
 unix:!macx: LIBS += -L$$DESTDIR/ -lEDD_Engine_Interfaces

 INCLUDEPATH += $$PWD/../../../../
 DEPENDPATH += $$PWD/../../../../
 INCLUDEPATH += $$PWD/../../../../lib/
 DEPENDPATH += $$PWD/../../../../lib/
 INCLUDEPATH += $$DESTDIR
 DEPENDPATH += $$DESTDIR
# INCLUDEPATH += $$PWD/../APPS/APPsupport/DevServices
#-unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Linux.a
android {
    PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Android.a
}
else {
    unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Linux.a
}

 unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libEDD_Engine_Common.a
 #unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libDevServices.a
 unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../lib/libGenBaseLib.a
 unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Interfaces.a
