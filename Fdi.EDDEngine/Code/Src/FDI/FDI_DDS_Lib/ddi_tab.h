/*
 * @(#) $Id: ddi_tab.h,v 1.2 2012/07/20 22:38:53 rgretta Exp $
 */

#ifndef DDI_TAB_H
#define DDI_TAB_H

#include "dds_upcl.h"
#include "ddi_lib.h"

#ifdef __cplusplus
    extern "C" {
#endif



extern int compare_bt_elem P((nsEDDEngine::BLK_TBL_ELEM *,nsEDDEngine::BLK_TBL_ELEM *));


extern int conv_block_spec P((ENV_INFO *, DDI_BLOCK_SPECIFIER *,
                              BLOCK_HANDLE *));


/*	
 * 	function prototypes needed for conversion to dd reference 
 */

extern int	tr_gdr_block_handle	P((ENV_INFO *, BLOCK_HANDLE,nsEDDEngine::DD_REFERENCE *)) ;

extern int 	tr_gdr_device_type_handle_blockname P((ENV_INFO *, DEVICE_TYPE_HANDLE, ITEM_ID, nsEDDEngine::DD_REFERENCE * )) ;

extern int	tr_block_to_ite P((nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_id_to_ite P((ENV_INFO *env_info, nsEDDEngine::ITEM_TBL *, ITEM_ID, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_name_to_ite P((nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *, ITEM_ID, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_param_offset_to_op_index P((ENV_INFO *, BLOCK_HANDLE,int,OBJECT_INDEX *));

extern int	tr_op_index_to_ite P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *,OBJECT_INDEX, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_param_to_ite P((nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *,int, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_name_si_to_ite P((nsEDDEngine::ITEM_TBL *, nsEDDEngine::BLK_TBL_ELEM *,ITEM_ID,SUBINDEX, nsEDDEngine::ITEM_TBL_ELEM **)) ;

extern int	tr_op_index_si_to_ite P((ENV_INFO *, BLOCK_HANDLE,nsEDDEngine::ITEM_TBL *,nsEDDEngine::BLK_TBL_ELEM *,OBJECT_INDEX,SUBINDEX, nsEDDEngine::ITEM_TBL_ELEM **));

/*	
 * 	function prototypes needed for conversion to type and size 
 */
extern int	tr_param_to_size P((ENV_INFO *, BLOCK_HANDLE,nsEDDEngine::BLK_TBL_ELEM *,int,TYPE_SIZE *)) ;
extern int	tr_param_si_to_size P((ENV_INFO *, BLOCK_HANDLE,nsEDDEngine::BLK_TBL_ELEM *,int,SUBINDEX,TYPE_SIZE *)) ;

extern int	tr_block_tag_to_block_handle P((ENV_INFO *, BLOCK_TAG, BLOCK_HANDLE *));

/*
 * Prototypes needed for Relation Conveniences
 */

extern int	tr_id_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_name_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *, ITEM_ID, int *)) ;
extern int	tr_op_index_to_bint_offset P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::BLK_TBL_ELEM *,OBJECT_INDEX, int *)) ;
extern int	tr_op_index_si_to_bint_offset P((ENV_INFO *, BLOCK_HANDLE, nsEDDEngine::BLK_TBL_ELEM *,OBJECT_INDEX, int *,SUBINDEX)) ;
extern int	tr_param_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *,int, int *)) ;

extern int	tr_id_si_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *,SUBINDEX,ITEM_ID, int *)) ;
extern int	tr_name_si_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *,SUBINDEX,ITEM_ID,int *)) ;
extern int	tr_param_si_to_bint_offset P((nsEDDEngine::BLK_TBL_ELEM *,SUBINDEX,int, int *)) ;
extern int	tr_bint_offset_to_rt_offset P((nsEDDEngine::BLK_TBL_ELEM *,int,int *)) ;
extern int	tr_bint_offset_si_to_rt_offset P((nsEDDEngine::BLK_TBL_ELEM *, SUBINDEX, int, int *)) ;

#ifdef __cplusplus
    }
#endif

#endif				/* DDI_TAB_H */

