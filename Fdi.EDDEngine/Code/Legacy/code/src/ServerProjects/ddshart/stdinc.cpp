/******************************************************************************
 *  evlinc.cpp cay
 *
 *  Copyright 2011 Emerson Process, Inc.- All rights reserved
 *
 *  Description : Source file that includes all the dds includes.
 *                Using this file and evlinc.h to create a pre-compiled header
 *                file, evlinc.pch.  This decrease build time.
 *
 ******************************************************************************/

#include "stdinc.h"

// stdafx.cpp : source file that includes just the standard includes
//  stdafx.pch will be the pre-compiled header
//  stdafx.obj will contain the pre-compiled type information
