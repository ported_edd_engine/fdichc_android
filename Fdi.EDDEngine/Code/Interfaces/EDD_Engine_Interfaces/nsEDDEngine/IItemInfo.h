#pragma once

#include "nsEDDEngine/Flats.h"

namespace nsEDDEngine
{


	//
	//	Interface Class IItemInfo
	//
	// IItemInfo returns data structures that correspond to an EDD Item and its Attributes.
	class IItemInfo
	{
	public:
		virtual ~IItemInfo(void) {};

		// Fills in an item data structure with the requested attributes
		// NOTE: iBlockInstance == 0 indicates the entire device
		virtual int GetItem( int iBlockInstance, void* pValueSpec, FDI_ITEM_SPECIFIER * pItemSpec, AttributeNameSet *pAttributeNameSet, const wchar_t* lang_code, FDI_GENERIC_ITEM * pGenericItem ) = 0;
#ifdef _DEBUG
		virtual int DisplayDebugInfo() = 0;
#endif
	};

}
