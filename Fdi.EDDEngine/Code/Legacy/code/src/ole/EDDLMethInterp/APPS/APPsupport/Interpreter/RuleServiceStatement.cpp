// RuleServiceStatement.cpp //

#include "stdafx.h"
#include "RuleServiceStatement.h"
#include "GrammarNodeVisitor.h"

#include "ErrorDefinitions.h"


CRuleServiceStatement::CRuleServiceStatement()
{
	m_pRuleName = 0;
}

CRuleServiceStatement::~CRuleServiceStatement()
{
	DELETE_PTR(m_pRuleName);
}

_INT32 CRuleServiceStatement::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitRuleService(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

// Rule Service Statement is of the form
//	<RuleEngine> <::> <Invoke> <(> <RuleName> <)>;
_INT32 CRuleServiceStatement::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CToken* pToken=0;
	try
	{
//Munch a <RuleEngine>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			||  (RUL_KEYWORD != pToken->GetType()) 
			|| (RUL_RULE_ENGINE != pToken->GetSubType()))
		{
			DELETE_PTR(pToken);
			throw(C_UM_ERROR_MISSINGTOKEN);
		}
		DELETE_PTR(pToken);

//Munch a <::>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SYMBOL != pToken->GetType())
			|| RUL_SCOPE != pToken->GetSubType())
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGSCOPE);
		}
		DELETE_PTR(pToken);

//Munch a <Invoke>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			||  (RUL_KEYWORD != pToken->GetType()) 
			|| (RUL_INVOKE != pToken->GetSubType()))
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGINVOKE);
		}
		DELETE_PTR(pToken);

//Munch a <(>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SYMBOL != pToken->GetType())
			|| RUL_LPAREN != pToken->GetSubType())
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGLPAREN);
		}
		DELETE_PTR(pToken);

//Munch a <RuleName> -- this is a string
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_STRING_CONSTANT != pToken->GetSubType()
				&& RUL_STRING_DECL != pToken->GetSubType())
			)
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGRNAME);
		}
		m_pRuleName = pToken;
		pToken = 0;
		
//Munch a <)>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SYMBOL != pToken->GetType())
			|| RUL_RPAREN != pToken->GetSubType())
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGRPAREN);
		}
		DELETE_PTR(pToken);

		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SEMICOLON != pToken->GetSubType()))
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGSC);
		}
		DELETE_PTR(pToken);
		return PARSE_SUCCESS;
	}
	catch(_INT32 error)
	{
		plexAnal->MovePast(
			RUL_SYMBOL,
			RUL_SEMICOLON,
			pSymbolTable);
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}
	return PARSE_FAIL;
}

void CRuleServiceStatement::Identify(
		_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,szTokenSubstrings[RUL_RULE_ENGINE]);
	strcat(szData,">");
	
	if(m_pRuleName)
		m_pRuleName->Identify(szData);

	strcat(szData,"</");
	strcat(szData,szTokenSubstrings[RUL_RULE_ENGINE]);
	strcat(szData,">");
}

void CRuleServiceStatement::GetRuleName(
		_CHAR* szData)
{
	strcat(szData,m_pRuleName->GetLexeme());
}

_INT32 CRuleServiceStatement::GetLineNumber()
{
	if (m_pRuleName)
	{
		return m_pRuleName->GetLineNumber();
	}
	else
	{
		return 0;
	}
}
