// GrammarNode.cpp //

#include "stdafx.h"
#include "GrammarNode.h"
//#include "SymbolTable.h"

CGrammarNode::CGrammarNode()
{
	SetNodeType(NODE_TYPE_INVALID);
}

CGrammarNode::~CGrammarNode()
{

}


_INT32 CGrammarNode::Execute(
			CGrammarNodeVisitor*	pVisitor,
			CSymbolTable*			pSymbolTable,
			INTER_VARIANT*		pvar,
			ERROR_VEC*				pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return 0;
}

_INT32 CGrammarNode::CreateParseSubTree(
			CLexicalAnalyzer*	plexAnal, 
			CSymbolTable*		pSymbolTable,
			ERROR_VEC*			pvecErrors)
{
	return 0;
}

void CGrammarNode::Identify(
			_CHAR* szData)
{
}

_INT32 CGrammarNode::GetLineNumber()
{
	return 0;
}

void CGrammarNode::SetStmtScopeStack(SCOPE_STACK sScope)
{
	m_sStmtScope = sScope;
}

SCOPE_STACK CGrammarNode::GetStmtScopeStack()
{
	return m_sStmtScope;
}

bool CGrammarNode::isErrorIn(_INT32 ErrNumber, ERROR_VEC* pvecErrors)
{
	bool bFound = false;
	for (ERROR_VEC::iterator iT = pvecErrors->begin(); iT != pvecErrors->end(); iT++)
	{
		if ((*iT)->getErrorNumber() == (_INT32)ErrNumber)
		{
			bFound = true;
			break;
		}
	}
	return bFound;
}
