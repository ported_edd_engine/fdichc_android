#include "STDINC.H"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"

int CConnectionMgr::CreateBlockEntry(BLOCK_HANDLE *BlockHandle, DEVICE_HANDLE deviceHandle)
{
	
    active_blk_tbl.limit = active_blk_tbl.limit + 1;
    active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM**) realloc((char*) active_blk_tbl.list,
        (unsigned int) (sizeof(ACTIVE_BLK_TBL_ELEM*) * active_blk_tbl.limit));
    if (!active_blk_tbl.list) 
    {
        return(CM_NO_MEMORY);
    }

    //
    // Init the newly allocated entries
    //
    for (int i = active_blk_tbl.count; i < active_blk_tbl.limit; i++)
    {
        active_blk_tbl.list[i] = NULL;
    }

    BLOCK_HANDLE blockHandle = active_blk_tbl.count;
    active_blk_tbl.count++;

    //
    // Allocate a new Active Block Table element where the block
    // handle is.
    //
    active_blk_tbl.list[blockHandle] = new ACTIVE_BLK_TBL_ELEM;

	// Default to unset values
    SET_ABT_TAG(blockHandle, NULL);
	SET_ABT_ISP_OP_INDEX(blockHandle, 0);
    SET_ABT_PARAM_COUNT(blockHandle, (unsigned)-1);
	SET_ABT_DD_BLK_ID(blockHandle, 0);
    SET_ABT_DD_BLK_TBL_OFFSET(blockHandle,-1);

	// Connect up to device handle
	SET_ABT_ADT_OFFSET(blockHandle, deviceHandle);

	// Mark that we are now referencing this device handle
	SET_ADT_USAGE( deviceHandle, ADT_USAGE(deviceHandle) +1 );
    
	*BlockHandle = blockHandle;
    return 0;
}

int CDeviceTypeMgr::CreateActiveDeviceTypeTableEntry(DEVICE_TYPE_HANDLE *deviceTypeHandle)
{
	active_dev_type_tbl.limit = active_dev_type_tbl.limit + 1;
    active_dev_type_tbl.list = (ACTIVE_DEV_TYPE_TBL_ELEM**) realloc((char *)active_dev_type_tbl.list,
        (unsigned int)(sizeof(ACTIVE_DEV_TYPE_TBL_ELEM*) * active_dev_type_tbl.limit));
    
	if (!active_dev_type_tbl.list) 
    {
        return(CM_NO_MEMORY);
    }

    //
    // Init the newly allocated entries
    //
    for (int i = active_dev_type_tbl.count; i < active_dev_type_tbl.limit; i++)
    {
        active_dev_type_tbl.list[i] = NULL;
    }

    *deviceTypeHandle = active_dev_type_tbl.count;
    active_dev_type_tbl.count++;
    active_dev_type_tbl.list[*deviceTypeHandle] = new ACTIVE_DEV_TYPE_TBL_ELEM;

	return 0;
}

int CConnectionMgr::CreateActiveDeviceTableEntry(DEVICE_HANDLE *deviceHandle)
{

    active_dev_tbl.limit = active_dev_tbl.limit + 1;
    active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM**) realloc((char*) active_dev_tbl.list,
        (unsigned int) (sizeof(ACTIVE_DEV_TBL_ELEM*) * active_dev_tbl.limit));

    if (!active_dev_tbl.list) 
    {
        return(CM_NO_MEMORY);
    }

    //
    // Init the newly allocated entries
    //
    for (int i = active_dev_tbl.count; i < active_dev_tbl.limit; i++)
    {
        active_dev_tbl.list[i] = NULL;
    }

	*deviceHandle = active_dev_tbl.count;
    active_dev_tbl.count++;

    //
    // Allocate a new Active Device Table element where the device
    // handle is.
    //
    active_dev_tbl.list[*deviceHandle] = new ACTIVE_DEV_TBL_ELEM;

	return 0;
}


void CConnectionMgr::SetActiveDeviceTableData(DEVICE_TYPE_HANDLE * pDeviceTypeHandle, DEVICE_HANDLE deviceHandle, DDOD_HEADER header)
{
	ROD_HANDLE          rod_handle = 0;
    DD_DEVICE_ID        ddDeviceId;
	
    rod_handle = -1;

    ddDeviceId.ddid_manufacturer = header.manufacturer;
    ddDeviceId.ddid_device_type = header.device_type;
    ddDeviceId.ddid_device_rev = header.device_revision;
    ddDeviceId.ddid_dd_rev = header.dd_revision;
	wcscpy(ddDeviceId.dd_file_path,header.filepath);
	ddDeviceId.header = header;

	*pDeviceTypeHandle = g_DeviceTypeMgr.ct_device_type_search(&ddDeviceId);

	if (g_DeviceTypeMgr.valid_device_type_handle(*pDeviceTypeHandle)) 
	{	
		g_DeviceTypeMgr.SET_ADTT_USAGE(*pDeviceTypeHandle,g_DeviceTypeMgr.ADTT_USAGE(*pDeviceTypeHandle) + 1);
	}
	else
	{
		g_DeviceTypeMgr.CreateActiveDeviceTypeTableEntry(pDeviceTypeHandle);

		g_DeviceTypeMgr.SET_ADTT_DD_DEVICE_ID(*pDeviceTypeHandle, &ddDeviceId);
		g_DeviceTypeMgr.SET_ADTT_ALARM_BLOCK_INDEX(*pDeviceTypeHandle, 0);
		g_DeviceTypeMgr.SET_ADTT_DD_HANDLE(*pDeviceTypeHandle, -1);
		g_DeviceTypeMgr.SET_ADTT_DD_DEV_TBLS(*pDeviceTypeHandle, 0);
		g_DeviceTypeMgr.SET_ADTT_SYMBOL_INFO(*pDeviceTypeHandle, 0);
		g_DeviceTypeMgr.SET_ADTT_COMPATIBILITY(*pDeviceTypeHandle, DD_NOT_LOADED);
		g_DeviceTypeMgr.SET_ADTT_USAGE(*pDeviceTypeHandle, 1);
	}

    SET_ADT_ADTT_OFFSET(deviceHandle, *pDeviceTypeHandle);
    SET_ADT_USAGE(deviceHandle, 0);
    SET_ADT_AM_BEING_COMMISSIONED(deviceHandle, false);
}

void CConnectionMgr::CleanTables(DEVICE_HANDLE dh)
{
	int i;
	int rc;

	int device_type_handle = ADT_ADTT_OFFSET(dh);

	for (i = 0; i < active_blk_tbl.limit;i++)
	{
		// #fdi
		//rc = ddi_remove_dd_blk_tbls(i);

		//
		// Remove tag if exits.
		//
		if (ABT_TAG(i)) 
		{
			free(ABT_TAG(i));
			SET_ABT_TAG(i, NULL);
		}
			
		//
		// Free the Active Block Table element and null the element
		// pointer.
		//

		delete active_blk_tbl.list[i];
		active_blk_tbl.list[i] = NULL;
	}

	for (i = 0; i < active_dev_tbl.limit;i++)
	{
		//
		// Free the Active Device Table element and null the element
		// pointer.
		//
		delete active_dev_tbl.list[i];
		active_dev_tbl.list[i] = NULL;
	}

	if (!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle))
	{
		return;//If it is a bad device type handle, punt.
	}

	if (g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) > 1)
	{
		g_DeviceTypeMgr.SET_ADTT_USAGE(device_type_handle,g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) - 1); 
	}
	else
	{
		ROD_HANDLE rod_handle = 0;
		g_DeviceTypeMgr.get_adtt_dd_handle( device_type_handle, &rod_handle );

		if (g_DeviceTypeMgr.VALID_ROD_HANDLE(rod_handle)) {
			int	r_code = SUCCESS;  //Restrict to local scope for following call
			r_code = g_DeviceTypeMgr.rod_close(rod_handle);
			ASSERT_DBG((r_code == CM_SUCCESS));
		}
			
		// Remove dd device tables
		rc = ddi_remove_dd_dev_tbls(NULL, device_type_handle);
		ASSERT_DBG((rc == CM_SUCCESS) && (rc != DDI_DEVICE_TABLES_NOT_FOUND) && !g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(device_type_handle));
		
		// Remove symbol information
		rc = remove_sym_info(device_type_handle);
		ASSERT_DBG((rc == CM_SUCCESS) && !g_DeviceTypeMgr.ADTT_SYMBOL_INFO(device_type_handle));
		
		//Delete the final device type information.
		g_DeviceTypeMgr.ct_remove_device_type(device_type_handle);
	
	}

	cm_cleanup();
	
 }

int CConnectionMgr::get_active_blk_tbl_count()
{
	return active_blk_tbl.count;
}


char* CConnectionMgr::ABT_TAG(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->tag;
}


void CConnectionMgr::SET_ABT_TAG(BLOCK_HANDLE bh, char* t)
{
	active_blk_tbl.list[bh]->tag = t;
}


DEVICE_HANDLE CConnectionMgr::ABT_ADT_OFFSET(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->active_dev_tbl_offset;
}


void CConnectionMgr::SET_ABT_ADT_OFFSET(BLOCK_HANDLE bh, DEVICE_HANDLE adto)
{
	active_blk_tbl.list[bh]->active_dev_tbl_offset = adto;
}


DEVICE_TYPE_HANDLE CConnectionMgr::ABT_ADTT_OFFSET(BLOCK_HANDLE bh)
{
	return ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh));
}


void CConnectionMgr::SET_ABT_ADTT_OFFSET(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto)
{
	SET_ADT_ADTT_OFFSET(ABT_ADT_OFFSET(bh), adtto);
}


void CConnectionMgr::SET_ABT_PARAM_COUNT(BLOCK_HANDLE bh, unsigned int pc)
{
	active_blk_tbl.list[bh]->param_count = pc;
}	


void CConnectionMgr::SET_ABT_DD_BLK_ID(BLOCK_HANDLE bh, UINT32 dbi)
{
	active_blk_tbl.list[bh]->dd_blk_id = dbi;
}


UINT32 CConnectionMgr::ABT_DD_BLK_ID(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->dd_blk_id;
}


void CConnectionMgr::SET_ABT_CHAR_RECORD( BLOCK_HANDLE bh, nsEDDEngine::CCharacteristicRecord charrecord )
{
	active_blk_tbl.list[bh]->blk_char_record = charrecord;
}


void CConnectionMgr::SET_ABT_BLOCK_TYPE( BLOCK_HANDLE bh, nsEDDEngine::BLOCK_TYPE block )
{
	active_blk_tbl.list[bh]->block_type = block;
}


void CConnectionMgr::SET_ABT_ISP_OP_INDEX(BLOCK_HANDLE bh, OBJECT_INDEX oi)
{
	active_blk_tbl.list[bh]->op_index = oi;
}


int CConnectionMgr::ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->dd_blk_tbl_offset;
}


void CConnectionMgr::SET_ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh, int dbto)
{
	active_blk_tbl.list[bh]->dd_blk_tbl_offset = dbto;
}

ROD_HANDLE CConnectionMgr::ABT_DD_HANDLE(BLOCK_HANDLE bh)
{
	return ADT_DD_HANDLE(ABT_ADT_OFFSET(bh));
}


UINT32 CConnectionMgr::ABT_CR_ITEM_ID(BLOCK_HANDLE bh)
{
	return active_blk_tbl.list[bh]->blk_char_record.m_ulDDItemId;
}


char* CConnectionMgr::ABT_CR_TAG( BLOCK_HANDLE bh )
{
	return active_blk_tbl.list[bh]->blk_char_record.m_pTag;
}


int CConnectionMgr::valid_block_handle (BLOCK_HANDLE bh)
{
	return	((bh >= 0) && (bh < active_blk_tbl.count) 
			&& (active_blk_tbl.list[bh]));
}


DEVICE_TYPE_HANDLE CConnectionMgr::ADT_ADTT_OFFSET(DEVICE_HANDLE dh)
{
	ASSERT_RET(valid_device_handle(dh), -1);

	return active_dev_tbl.list[dh]->active_dev_type_tbl_offset;
}


void CConnectionMgr::SET_ADT_ADTT_OFFSET(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto)
{
	ntcassert2(valid_device_handle(dh));

	active_dev_tbl.list[dh]->active_dev_type_tbl_offset = adtto;
}


//void CConnectionMgr::SET_ADT_VIEW_TIME(DEVICE_HANDLE dh, VIEWTYPE eView, DATE dt)
//{
//	ntcassert2(valid_device_handle(dh));
//
//	active_dev_tbl.list[dh]->m_eView = eView;
//	active_dev_tbl.list[dh]->m_dtHistoricalTime = dt;
//}


int CConnectionMgr::ADT_USAGE(DEVICE_HANDLE dh)
{
	ASSERT_RET(valid_device_handle(dh), -1);

	return active_dev_tbl.list[dh]->usage;
}


void CConnectionMgr::SET_ADT_USAGE(DEVICE_HANDLE dh, int i)
{
	ntcassert2(valid_device_handle(dh));

	active_dev_tbl.list[dh]->usage = i;
}


DD_DEVICE_ID* CConnectionMgr::ADT_DD_DEVICE_ID(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh));
}


void CConnectionMgr::SET_ADT_DD_DEVICE_ID(DEVICE_HANDLE dh, DD_DEVICE_ID* ddi)
{
	g_DeviceTypeMgr.SET_ADTT_DD_DEVICE_ID(ADT_ADTT_OFFSET(dh), ddi);
}


ROD_HANDLE CConnectionMgr::ADT_DD_HANDLE(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh));
}


void CConnectionMgr::SET_ADT_DD_HANDLE(DEVICE_HANDLE dh, ROD_HANDLE ddh)
{
	g_DeviceTypeMgr.SET_ADTT_DD_HANDLE(ADT_ADTT_OFFSET(dh), ddh);
}


void CConnectionMgr::SET_ADT_AM_BEING_COMMISSIONED(DEVICE_HANDLE dth, bool bAmBeingCommissioned)
{
	active_dev_tbl.list[dth]->m_bAmBeingCommissioned = bAmBeingCommissioned;
}


nsEDDEngine::FLAT_DEVICE_DIR* CConnectionMgr::ADT_DD_DEV_TBLS(DEVICE_HANDLE dh)
{
	return g_DeviceTypeMgr.ADTT_DD_DEV_TBLS(ADT_ADTT_OFFSET(dh));
}


DD_DEVICE_ID* CDeviceTypeMgr::ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth)
{
	return &active_dev_type_tbl.list[dth]->m_dd_device_id;
}


void CDeviceTypeMgr::SET_ADTT_DD_DEVICE_ID(DEVICE_TYPE_HANDLE dth, DD_DEVICE_ID* ddi)
{
	memcpy((char *)&(active_dev_type_tbl.list[dth]->m_dd_device_id),
			(char *)ddi,
			sizeof(DD_DEVICE_ID));
}


void CDeviceTypeMgr::ADTT_DEVICE_NAMES(DEVICE_TYPE_HANDLE dth, CStdString &strMfgName, CStdString &strDevTypeName)
{
	strMfgName = active_dev_type_tbl.list[dth]->m_strManufacturerName;
	strDevTypeName = active_dev_type_tbl.list[dth]->m_strDeviceTypeName;
}

void CDeviceTypeMgr::SET_ADTT_DEVICE_NAMES(DEVICE_TYPE_HANDLE dth, const CStdString &strMfgName, const CStdString &strDevTypeName)
{
	active_dev_type_tbl.list[dth]->m_strManufacturerName = strMfgName;
	active_dev_type_tbl.list[dth]->m_strDeviceTypeName = strDevTypeName;
}


ULONG CDeviceTypeMgr::ADTT_ALARM_BLOCK_INDEX(DEVICE_TYPE_HANDLE dth)
{
	return (active_dev_type_tbl.list[dth]->m_ulAlarmBlockIndex);
}

void CDeviceTypeMgr::SET_ADTT_ALARM_BLOCK_INDEX(DEVICE_TYPE_HANDLE dth, ULONG ulAlarmBlockIndex)
{
	active_dev_type_tbl.list[dth]->m_ulAlarmBlockIndex = ulAlarmBlockIndex;
}


ROD_HANDLE CDeviceTypeMgr::ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->rod_handle;
}


void CDeviceTypeMgr::SET_ADTT_DD_HANDLE(DEVICE_TYPE_HANDLE dth, ROD_HANDLE ddh)
{
	active_dev_type_tbl.list[dth]->rod_handle = ddh;
}

SYMINFO* CDeviceTypeMgr::ADTT_SYMBOL_INFO(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->pSymInfo;
}


void CDeviceTypeMgr::SET_ADTT_SYMBOL_INFO(DEVICE_TYPE_HANDLE dth, SYMINFO* pSymInfo)
{
	active_dev_type_tbl.list[dth]->pSymInfo = pSymInfo;
}

int CDeviceTypeMgr::ADTT_USAGE(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->usage;
}

void CDeviceTypeMgr::SET_ADTT_USAGE(DEVICE_TYPE_HANDLE dth, int u)
{
	active_dev_type_tbl.list[dth]->usage = u;
}


CM_COMPAT CDeviceTypeMgr::ADTT_COMPATIBILITY(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->compatibility;
}


void CDeviceTypeMgr::SET_ADTT_COMPATIBILITY(DEVICE_TYPE_HANDLE dth, CM_COMPAT c)
{
	active_dev_type_tbl.list[dth]->compatibility = c;
}


nsEDDEngine::FLAT_DEVICE_DIR* CDeviceTypeMgr::ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth)
{
	return active_dev_type_tbl.list[dth]->dd_dev_tbls;
}


void CDeviceTypeMgr::SET_ADTT_DD_DEV_TBLS(DEVICE_TYPE_HANDLE dth, nsEDDEngine::FLAT_DEVICE_DIR* ddt)
{
	active_dev_type_tbl.list[dth]->dd_dev_tbls = ddt;
}

int CRodMgr::VALID_ROD_HANDLE(ROD_HANDLE rh)
{
	return ((rh >= 0) 
			&& (rh < rod_tbl.count) 
			&& (rod_tbl.list[rh]));
}
int CDeviceTypeMgr::VALID_ROD_HANDLE(ROD_HANDLE rh)
{
	return m_RodMgr.VALID_ROD_HANDLE(rh);
}
