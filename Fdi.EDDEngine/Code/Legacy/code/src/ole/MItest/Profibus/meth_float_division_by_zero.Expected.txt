﻿<Method>
{

float a, b, c, d, e;

float f_inf;

a = 1.0;

b = 0.0;

c = a / b;

f_inf = c;

ACKNOWLEDGE(literal_string(xxx));

a = 1.0;

b = 0.0;

c = -a / b;

ACKNOWLEDGE(literal_string(xxx));

a = 1.0;

b = f_inf;

c = a / b;

ACKNOWLEDGE(literal_string(xxx));

a = 1.0;

b = f_inf;

c = a / -b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = a * b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = -f_inf;

c = a * (b);

ACKNOWLEDGE(literal_string(xxx));

a = -f_inf;

b = f_inf;

c = (a) * b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = -a * -b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = a + b;

ACKNOWLEDGE(literal_string(xxx));

a = 0.0;

b = 0.0;

c = a / b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = a - b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = -a + b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = -a - b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = a / b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = -a / b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = a / -b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = f_inf;

c = -a / -b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = 0.0;

c = a * b;

ACKNOWLEDGE(literal_string(xxx));

a = f_inf;

b = 0.0;

c = -a * b;

ACKNOWLEDGE(literal_string(xxx));

}
</Method>
<Function>
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: n / 0.0: Expected: INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "n / 0.0: Expected: INF, Actual:          inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: n / 0.0: Expected: INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -n / 0.0: Expected: -INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-n / 0.0: Expected: -INF, Actual:         -inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -n / 0.0: Expected: -INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: n / +inf: Expected: 0.0, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "n / +inf: Expected: 0.0, Actual:            0"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: n / +inf: Expected: 0.0, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: n / -inf: Expected: -0.0, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "n / -inf: Expected: -0.0, Actual:           -0"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: n / -inf: Expected: -0.0, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf * inf: Expected: INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf * inf: Expected: INF, Actual:          inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf * inf: Expected: INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf * -inf: Expected: -INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf * -inf: Expected: -INF, Actual:         -inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf * -inf: Expected: -INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf * inf: Expected: -INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf * inf: Expected: -INF, Actual:         -inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf * inf: Expected: -INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf * -inf: Expected: INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf * -inf: Expected: INF, Actual:          inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf * -inf: Expected: INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf + inf: Expected: INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf + inf: Expected: INF, Actual:          inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf + inf: Expected: INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: 0.0 / 0.0: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "0.0 / 0.0: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: 0.0 / 0.0: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf - inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf - inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf - inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf + inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf + inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf + inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf - inf: Expected: -INF, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf - inf: Expected: -INF, Actual:         -inf"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf - inf: Expected: -INF, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf / inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf / inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf / inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf / inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf / inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf / inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf / -inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf / -inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf / -inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf / -inf: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf / -inf: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf / -inf: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: inf * 0: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "inf * 0: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: inf * 0: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
Function Name: literal_string
  Number Of Input Parameters: 1
    Param 1 Type: RUL_SHORT
    Param 1 Value: xxx
  Returned Param Type: RUL_DD_STRING
  Returned Param Value: -inf * 0: Expected: NAN, Actual: %{c}
End of Function literal_string
IBuiltinSupport AcknowledgementRequest() is being called with "-inf * 0: Expected: NAN, Actual:          nan"
IBuiltinSupport AcknowledgementRequest with returned value 0 () is returned
IBuiltinSupport OnUIMethRspExecute() is being called
IBuiltinSupport OnUIMethRspExecute with returned value 0 () is returned
Function Name: ACKNOWLEDGE
  Number Of Input Parameters: 1
    Param 1 Type: RUL_DD_STRING
    Param 1 Value: -inf * 0: Expected: NAN, Actual: %{c}
  Returned Param Type: RUL_INT
  Returned Param Value: 0
End of Function ACKNOWLEDGE
</Function>
<SymbolTable><_bi_rc>0,</_bi_rc><a>inf,</a><b>0.00000,</b><c>nan,</c><d>0.00000,</d><e>0.00000,</e><f_inf>inf,</f_inf></SymbolTable>
IBuiltinSupport OnMethodExiting() is being called with "modified value being saved"
IBuiltinSupport OnMethodExiting() is returned
IBuiltinSupport is done. MI Engine OnMethodInterpreterComplete() is being called with "method success"
