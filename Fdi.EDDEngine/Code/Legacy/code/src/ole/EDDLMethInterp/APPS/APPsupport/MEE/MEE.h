#ifndef MEE_H
#define MEE_H

#pragma warning (disable : 4786)
#include <list>
#include <vector>
#include "Interpreter.h"
#include "BuiltIn.h"
#include "SymbolTable.h"
#include "ddbArray.h"
#include "HARTsupport.h"


// MEE error return codes
#define MEE_BAD_PARAM_PASSED_IN			(MEE_ERROR_BASE +  1)	//this is reserved for variable read/write failure
#define MEE_DDITEM_NAME_NOT_RESOLVED	(MEE_ERROR_BASE +  2)
#define MEE_NUMERIC_ARRAYNAME			(MEE_ERROR_BASE +  3)
#define MEE_INVALID_REFERENCE			(MEE_ERROR_BASE +  4)	//this is reserved for variable read/write failure
#define MEE_NOT_AN_ARRAY_TYPE			(MEE_ERROR_BASE +  5)
#define MEE_INVALID_INDEX			    (MEE_ERROR_BASE +  6)
#define MEE_ITEM_ATTRIBUTE_MISMATCH		(MEE_ERROR_BASE +  7)
#define MEE_NON_NUMERIC_4_MINMAX        (MEE_ERROR_BASE +  8)
#define MEE_MINMAX_FIND_FAILURE         (MEE_ERROR_BASE +  9)
#define MEE_UNIMPLEMENTED               (MEE_ERROR_BASE + 10)
#define MEE_ATTR_NOT_AVAILABLE          (MEE_ERROR_BASE + 11)
#define MEE_NO_MEMBERS_IN_ITEM			(MEE_ERROR_BASE + 12)
#define MEE_MEMBER_NOT_FOUND			(MEE_ERROR_BASE + 13)
#define MEE_NOT_DDITEM					(MEE_ERROR_BASE + 14)
#define MEE_ABORTED						(MEE_ERROR_BASE + 15)


typedef std::list<long> ITEMID_LIST;

typedef vector<hCitemBase*> ddbItemList_t;
class hCmethodParam;
typedef vector<hCmethodParam> ParamList_t;

// Anil December 16 2005 deleted codes related to Plot builtins. Please refer the previous version

//Added By Anil July 19 2005 --starts here
struct DOT_OP_ATTR
{
	string			strDotOpName;
	unsigned int	uiUniqueID;
	unsigned int	uiVarType;	
public:
	DOT_OP_ATTR()
	{
		uiUniqueID = 0;
		uiVarType = 0;
	}

	DOT_OP_ATTR(string strTemp,unsigned int uiTemp1,unsigned int uiTemp2)
	{
		strDotOpName = strTemp;
		uiUniqueID = uiTemp1;
		uiVarType = uiTemp2;
	}
};

typedef std::map<string, DOT_OP_ATTR> DOT_OP_ATTR_MAP;
typedef std::map<string, long> VARIABLE_STATUS_MAP;	//used for profibus only

//Below are the Types of the Dot operator That Can Exists
#define DOT_ID_NONE                 0
#define DOT_ID_LABEL				1
#define DOT_ID_HELP					2
#define DOT_ID_MIN_VALUE			3
#define DOT_ID_MAX_VALUE			4
#define DOT_ID_DFLT_VALUE			5
#define DOT_ID_VIEW_MIN				6
#define DOT_ID_VIEW_MAX				7
#define DOT_ID_COUNT				8
#define DOT_ID_CAPACITY				9
#define DOT_ID_FIRST				10
#define DOT_ID_LAST					11
#define DOT_ID_VARIABLE_STATUS		12
#define DOT_ID_SCALING_FACTOR		13
#define DOT_ID_LENGTH				14
#define DOT_ID_NUMBER_OF_ELEMENTS	15
#define DOT_ID_TOTAL				16
/* not allowed - stevev 16aug07
#define DOT_ID_DISPLAY_FORMAT		3
#define DOT_ID_EDIT_FORMAT			4
**/

//Below are the data types ogf Dot Operator
#define DOT_TYPE_INTEGER			1
#define DOT_TYPE_REAL				2
#define DOT_TYPE_DOUBLE				3
#define DOT_TYPE_STRING				4
#define DOT_TYPE_BOOLEAN			5
#define DOT_TYPE_LIST				6

//Added By Anil July 19 2005 --Ends here

//Reference Proxy used Masks
#define REF_PROXY_MASK				(0xFFFF0000)
#define REF_PROXY_BITS				(0x0000FFFF)

class OneMeth
{
private:
	char *m_pszDefinition;
	CInterpreter	*m_pInterpreter;
	CBuiltIn		*m_pBuiltinLib;

	hCddbDevice		*m_pDevice;

	

	/* The Item Id of the method being executed */
	long	m_lMethodItemId;
	ProtocolType m_startedProtocol;

	char	*m_pchMethodSourceCode;

	/* List that holds the abort methods */
	ITEMID_LIST			abortMethodList;
	ITEMID_LIST::iterator ListIterator;

	/* Flag is set to true if any of the ABORT builtins are called */
	bool	m_bMethodAborted;


public:
	/* HART only method info */

    int m_iAutoRetryLimit;
    
	unsigned char m_byCommAbortMask;
    unsigned char m_byCommRetryMask;
    unsigned char m_byStatusAbortMask;
    unsigned char m_byStatusRetryMask;
    unsigned char m_byRespAbortMask[RESP_MASK_LEN];
    unsigned char m_byRespRetryMask[RESP_MASK_LEN];
    unsigned char m_byReturnNodevAbortMask;
    unsigned char m_byReturnNodevRetryMask;

	unsigned char m_byXmtrCommAbortMask;
    unsigned char m_byXmtrCommRetryMask;
    unsigned char m_byXmtrStatusAbortMask;
    unsigned char m_byXmtrStatusRetryMask;
	unsigned char m_byXmtrRespAbortMask[RESP_MASK_LEN];
    unsigned char m_byXmtrRespRetryMask[RESP_MASK_LEN];
    unsigned char m_byXmtrReturnNodevAbortMask;
    unsigned char m_byXmtrReturnNodevRetryMask;
    unsigned char m_byXmtrDataAbortMask[DATA_MASK_LEN];
    unsigned char m_byXmtrDataRetryMask[DATA_MASK_LEN];
	
	UIntList_t m_vFFCommAbortMask;
	UIntList_t m_vFFCommRetryMask;
	UIntList_t m_vFFRespAbortMask;
	UIntList_t m_vFFRespRetryMask;
	bool m_bFFAllCommAbortMask;
	bool m_bFFAllCommRetryMask;
	bool m_bFFAllRespAbortMask;
	bool m_bFFAllRespRetryMask;

public:
	OneMeth():m_pInterpreter(NULL)
		, m_pBuiltinLib(NULL)
		, m_pDevice(NULL)
		, m_lMethodItemId(0)
		, m_pchMethodSourceCode(NULL)
		, m_bMethodAborted(false)
		, m_pMEE(NULL)
		, m_startedProtocol((ProtocolType)0)
	{
		m_pszDefinition = NULL;
		m_iAutoRetryLimit = 0;

		m_byCommAbortMask = 0;
		m_byCommRetryMask = 0x7F;
		m_byStatusAbortMask = DS_DEVMALFUNCTION;
		m_byStatusRetryMask = 0;
		m_byReturnNodevAbortMask = 0xFF;
		m_byReturnNodevRetryMask = 0;
		
		m_byXmtrCommAbortMask = 0;;
		m_byXmtrCommRetryMask = 0x7F;
		m_byXmtrStatusAbortMask = DS_DEVMALFUNCTION;
		m_byXmtrStatusRetryMask = 0;
		m_byXmtrReturnNodevAbortMask = 0xFF;
		m_byXmtrReturnNodevRetryMask = 0;
		
		m_vFFCommAbortMask.clear();
		m_vFFCommRetryMask.clear();
		m_vFFRespAbortMask.clear(); 
		m_vFFRespRetryMask.clear();
		m_bFFAllCommAbortMask = false;
		m_bFFAllCommRetryMask = true;
		m_bFFAllRespAbortMask = false;
		m_bFFAllRespRetryMask= true;

		int i = 0;
 		for(i = 0; i < RESP_MASK_LEN; i++)
		{
			m_byRespAbortMask[i] = 0;
			m_byRespRetryMask[i] = 0;
			m_byXmtrRespAbortMask[i] = 0;
			m_byXmtrRespRetryMask[i] = 0;
		}

		m_byRespAbortMask[2] = 0x01;		/*CR_ACCESS_RESTRICTED = 0x10*/
		m_byRespAbortMask[8] = 0x01;		/*CR_COMMAND_NOT_IMPL = 0x40*/
		m_byXmtrRespAbortMask[2] = 0x01;	/*CR_ACCESS_RESTRICTED = 0x10*/
		m_byXmtrRespAbortMask[8] = 0x01;	/*CR_COMMAND_NOT_IMPL = 0x40*/

		for(i = 0; i < DATA_MASK_LEN; i++)
		{
			m_byXmtrDataAbortMask[i] = 0;
			m_byXmtrDataRetryMask[i] = 0;
		}
	}

	~OneMeth()
	{
		if( m_pszDefinition )
		{
			delete [] m_pszDefinition;
			m_pszDefinition=NULL;
		}
		if (m_pInterpreter != NULL)
		{
			delete m_pInterpreter;
		}
		if (m_pBuiltinLib != NULL)
		{
			delete m_pBuiltinLib;
		}
	}


	INTERPRETER_STATUS ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId);
	//Anil Octobet 5 2005 for handling Method Calling Method
	//Added Overloaded function 
	INTERPRETER_STATUS ExecuteMethod(MEE * pMEE,hCddbDevice *pDevice, long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg,vector<INTER_VARIANT>* vectInterVar);

	int process_abort();

	int _add_abort_method(long lMethodId);

	int _remove_abort_method(long lMethodId);

	int remove_all_abort();

	void send_on_exit(); // for FF and Profibus builtin send_on_exit

	void save_on_exit(); // for FF builtin save_on_exit

	void discard_on_exit();

	bool GetMethodAbortStatus(); 

	bool RefreshWaveform(long lRfrshMethID);

	//int ExecuteActions(ddbItemList_t actionList);

	/*Arun 190505 Start of code */

	int _push_abort_method(long lMethodId);

	int _pop_abort_method();

	/*End of code*/

	ProtocolType GetStartedProtocol() { return m_startedProtocol; }
	void SetStartedProtocol(ProtocolType protocol) { m_startedProtocol = protocol; }
	unsigned long UpdateGlobalToken(unsigned long ulProxy, ITEM_ID ulMemberId);
	unsigned long InsertGlobalToken(unsigned long ulBlockId, unsigned long ulBlockInst, ITEM_ID ulItemId, ITEM_ID ulMemberId);
	unsigned long ReturnOrInsertGlobalToken(unsigned long ulBlockId, unsigned long ulBlockInst, unsigned long ulItemId, ITEM_ID ulMemberId);
	PARAM_REF* GetGlobalTokenOperative(const unsigned long ulIndex);
	int ExecuteRefreshActionsInMethod(/*in*/ hCVar *pVar, /*in, out*/ INTER_VARIANT *pActionVal);

private:
	INTERPRETER_STATUS ExecMethod(long lMethodItemId);
	//Anil Octobet 5 2005 for handling Method Calling Method
	//Added an OverLoaded function
	INTERPRETER_STATUS ExecMethod(long lMethodItemId, METHOD_ARG_INFO_VECTOR* vectMethArg, vector<INTER_VARIANT>* vectInterVar);
	wstring GetMethodArgString(METHOD_ARG_INFO_VECTOR* vectMethArg, vector<INTER_VARIANT>* vectInterVar);
	void	AllocLibrary();
	void	FreeLibrary();
	void	DisplayMessageAndAbort(char *pchMessage);
	int		CheckMethodReturnedVariable(hCmethod* pMeth);

public:
	MEE				*m_pMEE; //Vibhor 010705: Made it public to expose global symbol table

};

/*Vibhor 061204: Start of Code*/
class hCVar;
class MEE
{
private:
	
	hCddbDevice		*m_pDevice;
	CInterpreter	*m_pMEEInterpreter;	//dynamic pointer used when calling GetVariableValue()
	/* The Item Id of the method being executed */
	long	m_lMethodItemId;
	unsigned int m_NoOneMethInstance;
	ProtocolType m_startedProtocol;
	bool	m_bIsMethReturnType;	//whether method returned type is defined, excluding sub-method type

//	vector <OneMeth*> MethStack; //To support methods calling methods

	
	
public:
	/*Exposed interfaces*/
	ProtocolType GetStartedProtocol() { return m_startedProtocol; }
	void SetStartedProtocol(ProtocolType protocol) { m_startedProtocol = protocol; }
	void SetInterpreterPtr(CInterpreter *pInterp) { m_pMEEInterpreter = pInterp; }
	bool IsMethodCancelled();
	bool GetIsMethReturnType() {return m_bIsMethReturnType;}
	void SetIsMethReturnType(bool flag) {m_bIsMethReturnType = flag;}

	unsigned int IncreaseOneMethRefNo()
	{
		return(++m_NoOneMethInstance);
	}
	unsigned int DecreaseOneMethRefNo()
	{
		return(--m_NoOneMethInstance);
	}

	unsigned int GetOneMethRefNo()
	{
		return m_NoOneMethInstance;
	}	

	nsEDDEngine::ChangedParamActivity m_eOnExitActivity;

	INTERPRETER_STATUS ExecuteMethod(hCddbDevice *pDevice, long lMethodItemId); //Var-Action Execution call



	MEE ():m_pDevice(NULL)
			,m_pMEEInterpreter(NULL)
		   ,m_lMethodItemId(0)
		   , m_startedProtocol((ProtocolType)0)
		   //,MethStack.clear()
	{
		BuildDotOpNameToAttrMap();
		BuildSecondaryAttrList();
		m_NoOneMethInstance = 0;
	}

	~MEE()
	{
		m_MapDotOpNameToAttr.clear();
		m_MapVarStatusNameToValue.clear();
	}
	
/*Vibhor 060705: Start of Code*/

	CSymbolTable m_GlobalSymTable; //Vibhor 010705: Added for DD Variables

	//accessors for Global (DD) data in interpreter

	/* Finds the token in Global symbol table
	   If found returns the index else
	   Searches the Device for the same,
	   If found makes an entry in the global symbol table 
	   & returns the index
	   Otherwise returns -1, i.e. Not found
	 */
	long FindGlobalToken(const char* pszTokenName, const char* szFullLexeme, long lLineNum, DD_ITEM_TYPE &DDitemType);

	void ResolveVarExp(/* in */const char* pchVarExp, /* in */bool bMethParam, /* out */INTER_VARIANT *pivIndex, /* in, out */PARAM_REF *pOpRef);
	void UpdatePARAM_REF_withItemArrayListIndex (/* in */unsigned long ulIndex, /* in */unsigned long ulDepth, /* in, out */PARAM_REF *pOpRef);
	
	int SetVariableValue(const char* pszVariableName,int iValue);
	int SetVariableValue(const char* pszVariableName,float fValue);
	int SetVariableValue(const char* pszVariableName,double dValue);
	int SetVariableValue(const char* pszVariableName,bool bValue);
	
/*Vibhor 060705: End of Code*/
	string          methodNameString;// stevev 21sep07 - for help in debugging

private:
	void RemoveLanguageCode(const char* szOriginalString, char** szWithoutLanguageCode);//Added By Anil July 11 2005 Utility Function to get rid of Language Code
	void RemoveLanguageCode(const wchar_t* szOriginalString, wchar_t** szWithoutLanguageCode);
	void BuildDotOpNameToAttrMap();//Anil July 19 2005
	
	//Added By Anil August 22 2005 --starts here
	//RETURNCODE GetDDItemAttrValue(hCitemBase* pIB, const char* szDotExpression,INTER_VARIANT* pvar);
	//RETURNCODE GetDDItemAttrValue(hCitemBase* pIB, const char* pAttrName,CValueVarient* pvar, hCitemBase*& rpIB);
	RETURNCODE GetAttrValue(/* in */ hCitemBase* pIB, /* in */ char* pAttrName, /* in */ PARAM_REF *pOpRef, /* out */ INTER_VARIANT* pvar);
	RETURNCODE PutDynamicAttrValue(/* in */ hCitemBase* pIB, /* in */ string pAttrName, /* in */ PARAM_REF *pOpRef, /* out */ CValueVarient* pVal);

	void BuildSecondaryAttrList();	
	RETURNCODE GetGroupItemPtr(hCitemBase* pIBFinal,int iIndexValue,hCgroupItemDescriptor** pGID);
	//Below attribute Doesn't have any Datatype. 
	//If any DD Expression Ends with this, then it is invalid
	vector<string> m_svSecondaryAtt;
	//Please see the MEE.cpp file for the function comments
	RETURNCODE ValidateExp(const char* szDDExp, char** szLastAttr,bool &bPrimaryAttr);
	RETURNCODE GetDDVarValNEvalAssType(hCitemBase* pIBFinal, PARAM_REF *pOpRef, INTER_VARIANT* ivVariableValue, CValueVarient* pvtCV, RUL_TOKEN_SUBTYPE AssignType = RUL_ASSIGN);
	RETURNCODE GetDDAttrNEvalAssType(/* in */ string pAttrName, /* in */ PARAM_REF *pOpRef, /* in */ INTER_VARIANT* ivVariableValue, /* out */ CValueVarient* pvtValue, /* in */ RUL_TOKEN_SUBTYPE AssignType);
	RETURNCODE GetDDVarVal(PARAM_REF *pOpRef, INTER_VARIANT* pvar);
	RETURNCODE GetDDArrayVal(hCarray* phCArr, PARAM_REF *pOpRef, INTER_VARIANT* pvar);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	RETURNCODE GetCallerArgList(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,vector<string>* strvCallerArgList);
	RETURNCODE UpdateCalledMethArgInfo(METHOD_ARG_INFO_VECTOR*vectMethArgInfo, ParamList_t* TempParamList,vector<string>* strvCallerArgList);
	RETURNCODE ValidateMethodArgs(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,ParamList_t* TempParamList);
	void CastMethodArgValues(METHOD_ARG_INFO_VECTOR* vectMethArgInfo, vector<INTER_VARIANT>* vectInterVar);
	RETURNCODE UpdateRetunMethArgInfo(METHOD_ARG_INFO_VECTOR* vectMethArgInfo,hCmethodParam* phCmethodReturnValue,vector<INTER_VARIANT>* vectInterVar);
	RETURNCODE ResolveDot(/* in */ string sLeftString, /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef);
	RETURNCODE ResolveBracket(/* in */ string sLeftString, /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef);
	RETURNCODE ResolveParenth(/* in */ string sLeftString, /* in */ char* pRightString, /* out */ CValueVarient* pValue, /* in/out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef);
	RETURNCODE ResolveOperItemIDOrDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef, /* out */ CValueVarient* pValue, /* out */ hCitemBase*& pIBFinal);


public:
	//Added By Anil August 22 2005 --starts here
	//Please see the MEE.cpp file for the function comments
	RETURNCODE ResolveExp(const char* szDDitemName,const char* szDDExp,unsigned long ulLenOfexpToResolve,hCitemBase**  pIBFinal);

	DOT_OP_ATTR_MAP m_MapDotOpNameToAttr;
	DOT_OP_ATTR_MAP::iterator m_MapDotOpNameToAttrIter;	
	DOT_OP_ATTR     DotOpAttr[DOT_ID_TOTAL];

	VARIABLE_STATUS_MAP m_MapVarStatusNameToValue;

	/* added stevev 16aug07 to map legal attributes to DD item types */
typedef vector<int>      DOT_OP_LIST_t;// we just need the names   eg DOT_ID_LAST
typedef DOT_OP_LIST_t::iterator  DOT_OP_ITER_t;
	DOT_OP_LIST_t		 AvailAttrOP[ iT_MaxType ]; // an array of DOT_ID vectors

	RETURNCODE ResolveDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef, /* out */INTER_VARIANT* pvar, /* out */hCitemBase** ppDevObjVar = NULL);
	RETURNCODE ResolveNUpdateDDExp(/* in */const char* szComplexDDExpre, /* in */PARAM_REF *pOpRef, /* in/out */INTER_VARIANT* ivVariableValue, /* in */RUL_TOKEN_SUBTYPE	AssignType = RUL_ASSIGN);
	//Added:Anil Octobet 5 2005 for handling Method Calling Method
	INTERPRETER_STATUS ResolveMethodExp(const char* szComplexDDExpre,const char* szTokenName,INTER_VARIANT* pvar,vector<INTER_VARIANT>* vectInterVar,METHOD_ARG_INFO_VECTOR* vectMethArgInfo);
	//Added By Anil August 22 2005 --Ends here

	/* * * * * added by stevev 15aug07 * * * * */
	RETURNCODE ResolveComplexReference(/* in */ char* ReferenceString, /* out */ CValueVarient* pValue, /* out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef);
	// helpers of helpers.........................
	bool isAttr(/* in */ const char* pAttrStr);		//checking primary attribute
	bool isSecondaryAttr(/* in */ const char* pAttrStr, /* in */ hCitemBase* pIB, /* out */ hCitemBase*& rpDevObjVar);
	RETURNCODE getMemberInfo(/* in */ hCitemBase* pLeftItem, /* in */ const char*   pMemberName, 
							 /* out */ CValueVarient* pValue, /* out */ hCitemBase*& rpDevObjVar, /* out */ PARAM_REF *pOpRef);
	/* * end stevev additions 15auf07 * */

	RETURNCODE GetDictionaryString(char* chStringKey, char* chReturnbuffer,  int nMaxSize);
	void WriteMethodRetunedValue(INTER_VARIANT* pvar);

	//this function is used for CParser and CIntertretedVisitor classes to read/write parameter cache
	RETURNCODE convertMEEcodeToVISITcode(RETURNCODE mixedMEEcode);
	RETURNCODE convertMixedCodeToMEECode(RETURNCODE mixedMEEcode);
};

#endif /* MEE_H */

/*Vibhor 061204: End of Code*/

/***************************************************************************************
Vibhor : 071204: Comment

The MEE is now enhanced to support more than one methods in a single Device->ExecMethod 
instance call. So for a device still only one method will execute at a time, but for the 
Method environment, there could be other methods being executed due to the execution sequence,
e.g Actions on Waveforms being displayed on a graph in a method, or methods calling methods.

But overall, the method execution will be sequential. In this whole design, the device , 
method_interface and the MEE are Global objects. Each instance of a method now executes, in
a OneMeth object and has its own Interpreter and BuiltinLib.But the UI interactions from all
child methods(those not explicitly called from the device) will be on the same method UI as 
for the Parent method. 

/***************************************************************************************/













