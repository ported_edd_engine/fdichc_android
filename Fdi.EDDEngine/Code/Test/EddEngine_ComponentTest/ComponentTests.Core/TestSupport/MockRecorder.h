#pragma once

#include <string>
#include <map>
#include <iostream>
#include "nsConsumer/IParamCache.h"

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
        {
            class MockRecorder;

            typedef void(*MockMethod)(const wchar_t* methodName, MockRecorder* mockRecorder);

            class MockRecorder
            {
            private:
                typedef std::map<std::wstring, void*> ParameterMap;
                typedef std::pair<std::wstring, void*> ParameterPair;


                typedef std::map<std::wstring, int> CountMap;
                typedef std::pair<std::wstring, int> CountPair;

                typedef std::map<std::wstring, MockMethod> MethodDelegateMap;
                typedef std::pair<std::wstring, MockMethod> MethodDelegatePair;

                nsConsumer::EVAL_VAR_VALUE *_inputValue;

                ParameterMap _Parameters;
                ParameterMap _ReturnValues;
                MethodDelegateMap _MethodDelegates;

                CountMap _CallCount;
                MockMethod _MethodDelegate;

            public:
                MockRecorder();
                ~MockRecorder();
                /// <summary>
                /// Registers method delegate that processes call specified as methodName parameter.
                /// </summary>
                /// <param name="methodName">The method name for which delegate is registered.</param>
                /// <param name="methodDelegate">The function pointer that processes specified call.</param>
                void RegisterMethodDelegate(const std::wstring methodName, MockMethod methodDelegate);

                void RegisterNewMethodDelegate(const std::wstring methodName, MockMethod methodDelegate);

                /// <summary>
                /// Checks if the mock method delegate is defined for specified method name.
                /// </summary>
                /// <param name="methodName">The method name for which delegate should be checked.</param>
                /// <returns>True, if mock method delegate was defined. Otherwise returns false.</returns>
                bool HasMethodDelegate(const std::wstring methodName);

                /// <summary>
                /// Invokes method delegate that was set by <see cref="SetMethodDelegate"/> method.
                /// </summary>
                /// <param name="methodName">The method name that should be executed.</param>
                void InvokeMethdodDelegate(const std::wstring methodName);

                void RegisterParameter(std::wstring key, nsConsumer::EVAL_VAR_VALUE* parameter);

                void RegisterParameter(std::wstring key, void* parameter);
                void UnregisterParameter(std::wstring key);
                void* ReplayReturnValue(std::wstring key);
                void RegisterCall(std::wstring key);

                wchar_t* Copy(const wchar_t* itemToCopy);

                void Record(std::wstring key, void* returnValue);
                void RecordAnew(std::wstring key, void* returnValue);
                void* GetParameter(std::wstring key);
                bool WasCalled(std::wstring key, int times);
                bool WasCalled(std::wstring key);
                int GetCallCount(std::wstring key);

                bool IsReturnValueRecorded(std::wstring key);
            };

        } // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests