#include "stdafx.h"
#ifdef _WIN32
#include <direct.h>
#endif
#include <map>
#include "MethodInterfaceDefs.h"
#include "AMSMethodFunction.h"
#include "CProfiDPServerDevice.h"
#include "ProtectedDeviceList.h"
#include "BuiltInMapper.h"
#include "Ole/EDDLMethInterp/pathtype.h"
#include "Parser.h"
#include <thread>


class CDeviceList
{
	public:
		DevInfcHandle_t nDeviceHandle;
		CProfibusDPServerDevice *m_pDevice;
		CDeviceList( DevInfcHandle_t h, CProfibusDPServerDevice *pDevice )
		{
			nDeviceHandle = h;
			m_pDevice = pDevice;
		}
};



CProfibusDPServerDevice *FindDevicePointer( DevInfcHandle_t h )
{
	CProfibusDPServerDevice *pRetVal = NULL;
	std::list<CDeviceList *>* list =  CProtectedDeviceList::Instance()->LockList();
	std::list<CDeviceList *>::iterator it;
	for (it = list->begin(); it != list->end(); it++)
	{
		CDeviceList *pDevice = (*it);
		if (pDevice)
		{
			if (pDevice->nDeviceHandle == h)
			{
				pRetVal = pDevice->m_pDevice;
				break;
			}
		}
	}
	CProtectedDeviceList::Instance()->ReleaseList();
	return pRetVal;
}

//
//
//
void RemoveDevicePointer( DevInfcHandle_t h )
{
	std::list<CDeviceList *>* list =   CProtectedDeviceList::Instance()->LockList();
	std::list<CDeviceList *>::iterator it;
	for (it = list->begin(); it != list->end(); it++)
	{
		CDeviceList *pDevice = (*it);
		if (pDevice)
		{
			if (pDevice->nDeviceHandle == h)
			{
				list->erase(it);
				delete pDevice;
				break;
			}
		}
	}
	CProtectedDeviceList::Instance()->ReleaseList();
}

DevInfcHandle_t CProfibusDPMethodThread::g_nDeviceHandle = 0;


//
//
//
CProfibusDPMethodThread::CProfibusDPMethodThread(IManagedServerMi * managedMi,
												 long lMethodID,
												 ProtocolType protocol)
                                                : CMethodThread(lMethodID, protocol)
{
	m_ManagedMi = managedMi;
	
}
	
CProfibusDPMethodThread::~CProfibusDPMethodThread()
{

}

void CProfibusDPMethodThread::CreateMethodExecutionThread()
{
	std::function<void(const void *)> methodFuncPtr =
		std::bind(&CProfibusDPMethodThread::MethodExecutionThreadProc, this, this);

    std::thread methThread(methodFuncPtr, this);

	//Detaches from main thread to run indpendently
	methThread.detach();
}

void CProfibusDPMethodThread::MethodExecutionThreadProc(const void* arg)
{
	CProfibusDPMethodThread* pInstance = static_cast<CProfibusDPMethodThread*>(const_cast<void*>(arg));
	pInstance->InitInstance();	
}

//
// This is the entry point to running a method.
//
nsEDDEngine::Mth_ErrorCode CProfibusDPMethodThread::ExecuteMethodFunction()
{
	std::list<CDeviceList *>* list = CProtectedDeviceList::Instance()->LockList();

	nsEDDEngine::Mth_ErrorCode eMethodReturnCode = nsEDDEngine::METH_FAILED;
	int nCurrent = g_nDeviceHandle++;//Keep track of each instance of device objects.

	CProfibusDPServerDevice MethInterpDevice( nCurrent, m_lMethodID, m_ManagedMi->server_sLanguageCode, this );
	list->push_back(new CDeviceList(nCurrent, &MethInterpDevice));//Add us to the list
	SetDevicePointer(&MethInterpDevice);
	PS_TRACE(L"ExecuteMethodFunction[%p]-number of devices[%d]\n", this, nCurrent);
	CProtectedDeviceList::Instance()->ReleaseList();

	try
	{
        MethInterpDevice.Populate();

			INTERPRETER_STATUS eRetVal = (INTERPRETER_STATUS)MethInterpDevice.ExecuteTheMethod();

			//convert enum INTERPRETER_STATUS to enum Mth_ErrorCode in IMethods.h
			switch (eRetVal)
			{
			case INTERPRETER_STATUS_OK:
				eMethodReturnCode = nsEDDEngine::METH_SUCCESS;
				break;
			case INTERPRETER_STATUS_EXECUTION_ABORT:
				eMethodReturnCode = nsEDDEngine::METH_ABORTED;
				break;
			case INTERPRETER_STATUS_EXECUTION_CANCEL:
				eMethodReturnCode = nsEDDEngine::METH_CANCELLED;
				break;
			case INTERPRETER_STATUS_INVALID:
			case INTERPRETER_STATUS_PARSE_ERROR:
			case INTERPRETER_STATUS_EXECUTION_ERROR:
			case INTERPRETER_STATUS_UNKNOWN_ERROR:
				eMethodReturnCode = nsEDDEngine::METH_FAILED;
				break;
			}

	}
	catch(_INT32 error)
	{
		RemoveDevicePointer(nCurrent);//remove us from the list.
		throw (error);
	}
	
	RemoveDevicePointer(nCurrent);//remove us from the list.
	return eMethodReturnCode;
}

//
//
//
BOOL CProfibusDPMethodThread::InitInstance()
{
	nsEDDEngine::Mth_ErrorCode retVal = nsEDDEngine::METH_FAILED;

	try
	{
		if (IsThreadCancelled())
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}

		//the StartUp() is called to create a single CBuiltInMapper
		CBuiltInMapper::GetInstance()->LockBIMapper(GetProtocolType());
		if (!CBuiltInMapper::GetInstance()->IsStarted())
		{
			CBuiltInMapper::GetInstance()->StartUp(GetProtocolType());
		}
		CBuiltInMapper::GetInstance()->ReleaseBIMapper();
		PS_TRACE(L"InitInstance[%p]\n",this);

		if (IsThreadCancelled())
		{
			throw(C_UM_ERROR_METH_CANCELLED);
		}
			
		retVal = ExecuteMethodFunction();
		PS_TRACE(L"InitInstance[%p]-executed[%d]\n",this, retVal);

		m_ManagedMi->MethodComplete(retVal);
		PS_TRACE(L"InitInstance[%p]-methodcomplete[%d]\n",this, retVal);

		//delete upper class instance
		delete m_ManagedMi;
	}
	catch(...)	//last catch clause handles any type of exception, including C exceptions and system- or application-generated exceptions.
	{
		if (IsThreadCancelled())
		{
			retVal = nsEDDEngine::METH_CANCELLED;
		}
		else
		{
			retVal = nsEDDEngine::METH_ABORTED;
		}
		m_ManagedMi->MethodComplete(retVal);
		PS_TRACE(L"InitInstance[%p]-methodcomplete[%d]\n",this, retVal);
	
		//delete upper class instance
		delete m_ManagedMi;
	}

	return FALSE;
}


int CProfibusDPMethodThread::GetDictionaryString( wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen )
{
	long iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverGetDictionaryString(sDictionaryKey, sValue, iStringLen);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return iRetVal;
}


int CProfibusDPMethodThread::SendCommCmdExecute(int commandNumber, int transactionNumber, unsigned char* pucCmdStatus, unsigned char* pchMoreDataInfo, int& iMoreDataSize)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->CommCmdExecute(commandNumber, transactionNumber, pucCmdStatus, pchMoreDataInfo, iMoreDataSize);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::SendListOpExecute(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->ListOpExecute(ulListId, iListIndex, ulEmbListId, iIndex, ulItemId, iCount);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::SendValueExecute(const PARAM_REF *pOpRef, AccessType iAccessType)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->ParamSendExecute(pOpRef, iAccessType);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::threadAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverAccessAllDeviceValues(eAccessType);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::SendUIExecute(ACTION_UI_DATA& stUIData)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel || stUIData.bMethodAbortedSignalToUI)
	{
		//don't continue on UI builtin if the method is cancelled except this is an abort builtin
		iRetVal = m_ManagedMi->UIExecute(stUIData);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		iRetVal = BI_ABORT;
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::ReceiveUIMethRspExecute(CValueVarient& vtRsp)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->UIMethRspExecute(vtRsp);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		iRetVal = BI_ABORT;
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::GetRspCodeStrExecute(int nCommandNumber, int nResponseCode, CValueVarient *pvtValue, nsEDDEngine::ProtocolType protocol)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->RspCodeStrExecute(nCommandNumber, nResponseCode, pvtValue, protocol);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::GetDictStringExecute(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->DictStringExecute(ulIndex, pString, iStringLen);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

//
//
//
int CProfibusDPMethodThread::GetLiteralString( unsigned long ulIndex, wchar_t *pString, long lStrLen )
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->LitStringExecute(ulIndex, pString, lStrLen);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

//
//
//
void CProfibusDPMethodThread::CommitValues(nsEDDEngine::ChangedParamActivity onExitActivity)
{
	if (!m_bCancel)
	{
		m_ManagedMi->CommitValueExecute(onExitActivity);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}
}


int CProfibusDPMethodThread::GetItemIdByName( CStdString sItemName, unsigned long *pItemId )
{
	int iRetVal = BI_ABORT; 

	if (!m_bCancel)
	{
		iRetVal = (m_ManagedMi->ResolveNameToIDExecute(sItemName, pItemId));
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}


int CProfibusDPMethodThread::GetItemByItemId2( unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, unsigned long ulMemberID, CStdString sProperty, CValueVarient *pOutput )
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if (!m_bCancel)
	{
		CValueVarient vMember{}; 
		long ppType = MapStringToPPType( sProperty, vMember );
		unsigned long memberID = ulMemberID;	//could be member id of collection or subindex of array in HART
		bool bMoreDDS = true;

		iRetVal = BLTIN_SUCCESS;
		if (vMember.vTagType == ValueTagType::CVT_UI4)
		{
			memberID = CV_UI4(&vMember);
		}
		else if (vMember.vTagType == ValueTagType::CVT_WSTR)
		{
			CStdString memberName;
			memberName = (CStdString)(CV_WSTR(&vMember));
			iRetVal = m_ManagedMi->ResolveNameToIDExecute(memberName, &memberID);

			if ((iRetVal == BLTIN_SUCCESS) && (ppType == PP_MemberID))
			{
				CV_VT(pOutput, ValueTagType::CVT_UI4);
				CV_UI4(pOutput) = memberID;
				iRetVal = BLTIN_SUCCESS;
				bMoreDDS = false;
			}
		}

		if ((bMoreDDS) && (iRetVal == BLTIN_SUCCESS))
		{
			iRetVal = m_ManagedMi->GetValueExecute2(ulBlkId, ulBlkNum, uItemID, memberID, ppType, pOutput);
		}
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}
int CProfibusDPMethodThread::GetItemActionList(unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long uItemID, ACTION_LIST *pOutput)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverGetItemActionList(ulBlkId, ulBlkNum, uItemID, pOutput);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}
//
//
//
hCdeviceSrvc* hCobject::devPtr(void)
{
	return FindDevicePointer(devHndl());
}

int CProfibusDPMethodThread::GetResponseCodeStringExecute(int iItemId, int iMemberId, int iResponseCode, CValueVarient *pvtValue)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->ResponseCodeStringExecute(iItemId, iMemberId, iResponseCode, pvtValue);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}
int CProfibusDPMethodThread::getDdsErrorExecute(wchar_t* pErrorString)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->DdsErrorExecute(pErrorString);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

unsigned long CProfibusDPMethodThread::getCommErrorExecute()
{
	unsigned long iRetVal = 0;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->CommErrorExecute();
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::getResponseCodeExecute(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId)
{
	int iRetVal = BLTIN_NOT_IMPLEMENTED;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->ResponseCodeExecute(pulResponseCode, pulErrorItemId, pulErrorMemberId);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::assignExecute(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverAssign(pdst_opRef, psrc_opRef);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}


int CProfibusDPMethodThread::threadAccessDynamicAttributeExecute(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->AccessDynamicAttributeExecute(pOpRef, pParamValue, ulAttribute, eAccessType);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

ITEM_ID CProfibusDPMethodThread::resolveBlockRefExecute(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType)
{
	ITEM_ID ulRetVal = 0;

	if (!m_bCancel)
	{
		ulRetVal = m_ManagedMi->BlockRefExecute(ulBlockItemId, iOccurrence, ulMemberId, eResolveType);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (ulRetVal);
}

int CProfibusDPMethodThread::threadAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, variableType_t eType, AccessType eAccessType, long lSize)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverAccessTypeValue2(pOpRef, value, eType, eAccessType, lSize);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::threadAccessActionVar(/*in, out*/ nsEDDEngine::ACTION *pAction, /*in, out*/ nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/ AccessType eAccessType)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverAccessActionVar(pAction, pActionVal, eAccessType);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::threadWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverWriteMethRtnVar(pReturnedVal);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::getBlockInstanceCountExecute(unsigned long ulBlockId, unsigned long *pulCount)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->BlockInstanceCountExecute(ulBlockId, pulCount);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::getBlockInstanceByObjIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->BlockInstanceByObjIndexExecute(ulObjectIndex, pulRelativeIndex);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::getBlockInstanceByBlockTag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->BlockInstanceByBlockTagExecute(ulBlockId, pchBlockTag, pulRelativeIndex);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::GetListElement2Execute(unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->GetListElement2Execute(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, data);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

void CProfibusDPMethodThread::threadLogMessage(int priorityVal, wchar_t *msg)
{
	if (!m_bCancel)
	{
		m_ManagedMi->serverLogMessage(priorityVal, msg);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

}

void CProfibusDPMethodThread::threadMethodDebugMessage(unsigned long lineNumber, const wchar_t* currentMethodDebugXml, const wchar_t** modifiedMethodDebugXml)
{
	if (!m_bCancel)
	{
		m_ManagedMi->serverMethodDebugMessage(lineNumber, currentMethodDebugXml, modifiedMethodDebugXml);
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

}

int CProfibusDPMethodThread::threadIsOffline()
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverIsOffline();
	}

	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

long CProfibusDPMethodThread::threadSendCommand(unsigned long commandId, long *commandError)
{
	long iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverSendCommand(commandId, commandError);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

int CProfibusDPMethodThread::threadGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString)
{
	int iRetVal = BI_ABORT;

	if (!m_bCancel)
	{
		iRetVal = m_ManagedMi->serverGetEnumVarString(ulItemId, ulEnumValue, sEnumString);
	}
	
	//after the function is done, check the cancel indicator again
	if (m_bCancel)
	{
		throw(C_UM_ERROR_METH_CANCELLED);
	}

	return (iRetVal);
}

void CProfibusDPMethodThread::threadMILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename,  UINT32 lineNumber)
{
	m_ManagedMi->MILog(sMessage, eLogSeverity, sCategory, cFilename, lineNumber);
}

