// FunctionExpression.cpp //

#include "stdafx.h"

//#if _MSC_VER >= 1300  // HOMZ - port to 2003, VS7
//stevev 20feb07-merge- contents moved to ddbGeneral #include "..\DevServices\stdafx.h"   // HOMZ
//#endif
#include "ddbGeneral.h"	// stevev 20feb07 - merge to get rid of 'stdafx.h'
/* comutil.h uses Bill's TRUE/FALSE that the general erroneously defines on purpose */
#undef TRUE
#define TRUE 1
#undef FALSE
#define FALSE 0

#ifdef _WIN32
#include <comdef.h>
#endif
#include "ExpParser.h"
#include "OMServiceExpression.h"
#include "FunctionExpression.h"
#include "SymbolTable.h"
#include "GrammarNodeVisitor.h"
#include "MEE.h"

#include "ErrorDefinitions.h"
#include "BuiltInMapper.h"

FunctionExpression::FunctionExpression()
{
	m_pchFunctionName =	0;
	m_i32ParameterCount = -1;
	/*<START>TSRPRASAD 09MAR2004 Fix the memory leaks	*/
	for (int iLoopVar = 0;iLoopVar < MAX_NUMBER_OF_FUNCTION_PARAMETERS;iLoopVar++)
	{
		m_pExpression[iLoopVar] = NULL;
		m_pConstantTokens[iLoopVar] = NULL;
	}
	/*<END>TSRPRASAD 09MAR2004 Fix the memory leaks	*/
}

FunctionExpression::~FunctionExpression()
{
	if(m_pchFunctionName)
	{
		delete [] m_pchFunctionName;
		m_pchFunctionName = 0;
	}
	
	/*<START>TSRPRASAD 09MAR2004 Fix the memory leaks	*/
	for (int iLoopVar = 0;iLoopVar < m_i32ParameterCount;iLoopVar++)
	{
		DELETE_PTR(m_pExpression[iLoopVar]);
		DELETE_PTR(m_pConstantTokens[iLoopVar]);
	}
	/*<END>TSRPRASAD 09MAR2004 Fix the memory leaks	*/

	m_i32ParameterCount = -1;
}

_INT32 FunctionExpression::Execute(
			CGrammarNodeVisitor* pVisitor,
			CSymbolTable* pSymbolTable,
			INTER_VARIANT* pvar,
			ERROR_VEC*	pvecErrors,
			RUL_TOKEN_SUBTYPE	AssignType)//Anil August 26 2005 to Fix a[exp1] += exp2
{
	return pVisitor->visitFunctionExpression(
		this,
		pSymbolTable,
		pvar,
		pvecErrors,
		AssignType);//Anil August 26 2005 to Fix a[exp1] += exp2
}

_INT32 FunctionExpression::CreateParseSubTree(
			CLexicalAnalyzer* plexAnal, 
			CSymbolTable* pSymbolTable,
			ERROR_VEC*	pvecErrors)
{
	CToken* pToken=0;
	try
	{
//Munch a <FUNCTION NAME>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| !pToken->IsFunctionToken())
		{
			DELETE_PTR(pToken);
			throw(C_UM_ERROR_MISSINGTOKEN);
		}

		m_pchFunctionName = new _UCHAR[strlen(pToken->GetLexeme()) + 1];
		strcpy((char *)m_pchFunctionName, pToken->GetLexeme());

		/* Now get the details of the function */
		CBuiltInInfo Func; //WHS May 24 2007 BUILTIN SUBCLASSING
		if (GetFunctionDetails(pToken, &Func, plexAnal) == 0)
		{
#ifdef _DEBUG
			MessageBox(NULL, L"Function name Not Found", NULL, 0);
#endif 
			DELETE_PTR(pToken);
			return PARSE_FAIL;
		}
		
		DELETE_PTR(pToken);

//Munch a <(>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SYMBOL != pToken->GetType())
			|| RUL_LPAREN != pToken->GetSubType())
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGLPAREN);
		}
		DELETE_PTR(pToken);

		/* Now get the parameters */
		m_i32ParameterCount = Func.GetNumberOfParams();  //WHS May 24 2007 BUILTIN SUBCLASSING

		for (int iLoopVar = 0; iLoopVar < (int)Func.GetNumberOfParams(); iLoopVar++)   //WHS May 24 2007 BUILTIN SUBCLASSING
		{
			m_pTokenType[iLoopVar] = Func.GetParam(iLoopVar);  //WHS May 24 2007 BUILTIN SUBCLASSING
			switch(Func.GetParam(iLoopVar)) //WHS May 24 2007 BUILTIN SUBCLASSING
			{
				case RUL_NO_SUPPORT:
				case RUL_NUMERIC_CONSTANT:
				case RUL_SIMPLE_VARIABLE:
				case RUL_ARRAY_VARIABLE:
				case RUL_DD_ITEM:			//Vibhor 140705: Added
				{
					bool bParenPresent = false;
					/* Check if there is a <(>*/
					/*if((LEX_FAIL != plexAnal->GetNextToken(&pToken,pSymbolTable)) 
						&& pToken
						&& RUL_SYMBOL == pToken->GetType()
						&& RUL_LPAREN == pToken->GetSubType())
					{
						bParenPresent = true;
					}
					else
					{
						plexAnal->UnGetToken();
					}
					DELETE_PTR(pToken);*/

					CExpParser expParser;
					m_pExpression[iLoopVar]
						= expParser.ParseExpression(plexAnal,pSymbolTable,EXPR_WHILE, pvecErrors);
					if(!m_pExpression[iLoopVar])
					{
						throw(C_WHILE_ERROR_MISSINGEXP);
					}

					/*if <(> was present, check for <)>*/
					if (bParenPresent)
					{
						/* Check if there is a <(>*/
						if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
							|| !pToken
							|| (RUL_SYMBOL != pToken->GetType())
							|| RUL_RPAREN != pToken->GetSubType())
						{
							plexAnal->UnGetToken();
						}
						DELETE_PTR(pToken);
					}

					break;
				}
				case RUL_STR_CONSTANT:
				{//Note CJK has made changes in here that are specific to Profibus
					//this is the reason I didnt change CExpParser expParser;  expParser.ParseExpression
					//to handle these new use cases as technically they should be here
					bool bParenPresent = false;
					bool bEnterWhile = true;
					bool bSearchConcat = true;
					int iCountLeftParenthisis = 0;

					plexAnal->CaptureState();
					if (HasDDItem(plexAnal, pSymbolTable))
					{//added for the Profibus use case where they have a DD Item (VARIABLE)in a builtin that expects a string
					// I had added a use case here that went out and got the string value in the token on the fly
					// but that failed when the DD Method modifies the VARIABLE inside of the definition
					//You have the old value of the VARIABLE not the new one. So lets create a
						plexAnal->ReturnState();
						CExpParser expParser;
						m_pExpression[iLoopVar]
							= expParser.ParseExpression(plexAnal,pSymbolTable,EXPR_WHILE, pvecErrors);
						if(!m_pExpression[iLoopVar])
						{
							throw(C_WHILE_ERROR_MISSINGEXP);
						}
					}
					else
					{
						plexAnal->ReturnState();
						/* Check if there is a <(>*/
						//Anil 16 November 2005
						//This is for handling the Multiplle left parathissi come oin the Built in calls
						while(bEnterWhile)
						{
							if((LEX_FAIL != plexAnal->GetNextToken(&pToken,pSymbolTable)) 
								&& pToken
								&& RUL_SYMBOL == pToken->GetType()
								&& RUL_LPAREN == pToken->GetSubType())
							{
								bParenPresent = true;
								iCountLeftParenthisis++;
								bEnterWhile = true;
							}
							else
							{
								plexAnal->UnGetToken();
								bEnterWhile = false;
							}
							DELETE_PTR(pToken);
						}

						if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
							|| !pToken
							|| !pToken->IsConstant())
						{
							if (pToken->IsVariable())
							{
								plexAnal->UnGetToken();
								m_pTokenType[iLoopVar] = RUL_ARRAY_VARIABLE;
								CExpParser expParser;
								m_pExpression[iLoopVar]
									= expParser.ParseExpression
														(
															plexAnal
															, pSymbolTable
															, EXPR_WHILE
															, pvecErrors
														);
								if(!m_pExpression[iLoopVar])
								{
									throw(C_WHILE_ERROR_MISSINGEXP);
								}
							}
							// stevev 30jan08 - added to handle function-as-string
							else
							if (pToken->IsFunctionToken())
							{
								plexAnal->UnGetToken();
								m_pTokenType[iLoopVar] = RUL_ARRAY_VARIABLE;// may need RUL_STR_CONSTANT
								CExpParser expParser;
								m_pExpression[iLoopVar]
									= expParser.ParseExpression
														(
															plexAnal
															, pSymbolTable
															, EXPR_WHILE
															, pvecErrors
														);
								if(!m_pExpression[iLoopVar])
								{
									throw(C_WHILE_ERROR_MISSINGEXP);
								}
							}
							// else - we don't handle other possibilities...
							DELETE_PTR(pToken);
						}
						else
						{
							m_pConstantTokens[iLoopVar] = new CToken;
							*m_pConstantTokens[iLoopVar] = *pToken;

							DELETE_PTR(pToken);
						}

						/*if <(> was present, check for <)>*/
						if (bParenPresent)
						{
							/* Check if there is a <(>*/
							//Anil 16 November 2005						
							//This is for handling the Multiplle left parathissi come oin the Built in calls					
							for(int iCount = 0;  iCount<iCountLeftParenthisis ; iCount++)
							{
								if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
									|| !pToken
									|| (RUL_SYMBOL != pToken->GetType())
									|| RUL_RPAREN != pToken->GetSubType())
								{
									plexAnal->UnGetToken();
								}
								DELETE_PTR(pToken);
							}
						}
						
					}
				}
				break;
				default:
					DELETE_PTR(pToken);
					return PARSE_FAIL;
					break;
				
			}
//Munch a <,>
			if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
				|| !pToken
				|| (RUL_SYMBOL != pToken->GetType())
				|| RUL_COMMA != pToken->GetSubType())
			{
				plexAnal->UnGetToken();
				//throw(C_RS_ERROR_MISSINGLPAREN);		//last parameter has no <,> followed
			}
			DELETE_PTR(pToken);
		}// next function parameter

//Munch a <)>
		if((LEX_FAIL == plexAnal->GetNextToken(&pToken,pSymbolTable)) 
			|| !pToken
			|| (RUL_SYMBOL != pToken->GetType())
			|| RUL_RPAREN != pToken->GetSubType())
		{
			plexAnal->UnGetToken();
			throw(C_RS_ERROR_MISSINGLPAREN);
		}
		DELETE_PTR(pToken);
		return PARSE_SUCCESS;

	}
	catch(_INT32 error)
	{
		DELETE_PTR(pToken);
		ADD_ERROR(error);
	}

	return 0;
}

void FunctionExpression::Identify(
		_CHAR* szData)
{
	strcat(szData,"<");
	strcat(szData,(const char*)m_pchFunctionName);
	strcat(szData,">");

	strcat(szData,"</");
	strcat(szData,(const char*)m_pchFunctionName);
	strcat(szData,">");
}

//#include "RuleOMItfExports.h"
//#include "RuleObjMgrInterface.h"
//#include "OMObject.h"

_INT32 OIDConvertIntToChar2(
			_INT32 i32OID, 
			_CHAR* chOID)
{
	union unionOID
	{
		_CHAR				chOID[5];
		struct strOID
		{
			_INT32			nOID;
			_UCHAR			chOID;
		}sOID;
	};
	unionOID uOID;
	uOID.sOID.nOID	=	i32OID;
	uOID.sOID.chOID	=	0;
	memcpy(chOID,uOID.chOID,5);

	return 0;
}

_INT32 FunctionExpression::GetLineNumber()
{
	return -1;
}

_INT32 FunctionExpression::GetFunctionDetails(
											  CToken *pToken
											  , CBuiltInInfo *pFunc
											  , CLexicalAnalyzer* plexAnal
											  )
{
	_INT32 iRetVal = 0;		//failure
	MEE *mee = plexAnal->GetMEEInterface();

	if (mee == NULL)
	{
		CBuiltInMapper::GetInstance()->LockBIMapper((ProtocolType)0);
	}
	else
	{
		CBuiltInMapper::GetInstance()->LockBIMapper(mee->GetStartedProtocol());
	}

	CBuiltInMapper *pMapper = CBuiltInMapper::GetInstance();	 //WHS May 24 2007 BUILTIN SUBCLASSING
	int iNumberOfFunctions = pMapper->GetSize(); //WHS May 24 2007 BUILTIN SUBCLASSING

	for (int iLoopVar = 0;iLoopVar < iNumberOfFunctions;iLoopVar++)
	{
		if (strcmp(pToken->GetLexeme(), (*pMapper)[iLoopVar].GetName().c_str()) == 0) //WHS May 24 2007 BUILTIN SUBCLASSING
		{
			*pFunc = (*pMapper)[iLoopVar]; //WHS May 24 2007 BUILTIN SUBCLASSING
			iRetVal = 1;	//successfully found
			break;
		}
	}
	CBuiltInMapper::GetInstance()->ReleaseBIMapper();

	return iRetVal;
}


bool FunctionExpression::HasDDItem(CLexicalAnalyzer* plexAnal, 
								   CSymbolTable* pSymbolTable)
{
	bool bKeepSearching = true;
	bool bHasDDItem = false ;
	CToken* pToken = NULL;
	int nTokenCount = 0;

	try
	{
		do
		{
			if (LEX_FAIL != plexAnal->GetNextToken(&pToken, pSymbolTable) && pToken)
			{
				if ((RUL_DD_ITEM == pToken->GetType())
					|| (RUL_ARITHMETIC_OPERATOR == pToken->GetType() && RUL_PLUS == pToken->GetSubType())
					|| (RUL_SYMBOL == pToken->GetType() && RUL_LBOX == pToken->GetSubType()))
				{
					bHasDDItem = true;
					bKeepSearching = false;
				}
				else if (RUL_SYMBOL == pToken->GetType() &&
					(RUL_SEMICOLON == pToken->GetSubType() || RUL_COMMA == pToken->GetSubType()))
				{
					bKeepSearching = false;
				}
			}
			else
			{
				bKeepSearching = false;
			}
			DELETE_PTR(pToken);
			pToken = NULL;

		} while (bKeepSearching);

		return bHasDDItem;
	}
	catch (_INT32 error)
	{
		DELETE_PTR(pToken);
		throw(error);
	}
}
