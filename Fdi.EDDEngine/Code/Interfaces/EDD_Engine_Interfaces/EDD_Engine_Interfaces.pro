#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T12:45:53
#
#-------------------------------------------------

QT       -= gui
QMAKESPEC=linux-g++-32

TARGET = EDD_Engine_Interfaces
TEMPLATE = lib
CONFIG += shared_and_static build_all
#CONFIG +=plugin

DEFINES += EDD_ENGINE_INTERFACES_LIBRARY \
           UNICODE


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#QMAKE_CXXFLAGS += -std=c++11 -Wno-narrowing #todo

QMAKE_CXXFLAGS += -std=gnu++11

SOURCES += nsEDDEngine/AttributeNameSet.cpp \
    nsEDDEngine/Attrs.cpp \
    nsEDDEngine/Convenience.cpp \
    nsEDDEngine/Flats.cpp \
    stdafx.cpp \


HEADERS += nsConsumer/IBuiltinSupport.h \
    nsConsumer/IEDDEngineLogger.h \
    nsConsumer/IParamCache.h \
    nsEDDEngine/AttributeName.h \
    nsEDDEngine/Attrs.h \
    nsEDDEngine/Common.h \
    nsEDDEngine/Ddldefs.h \
    nsEDDEngine/EDDEngineFactory.h \
    nsEDDEngine/FFBlockInfo.h \
    nsEDDEngine/Flats.h \
    nsEDDEngine/IConvenience.h \
    nsEDDEngine/IEDDEngine.h \
    nsEDDEngine/IItemInfo.h \
    nsEDDEngine/IMethods.h \
    nsEDDEngine/ITableInfo.h \
    nsEDDEngine/ProtocolType.h \
    nsEDDEngine/Table.h \
    resource.h \
#    stdafx.h \
#    targetver.h \

unix {
    target.path = /usr/bin/
    INSTALLS += target
}

CONFIG(debug, debug|release) {
    DESTDIR = ../../Legacy/code/bin/debug
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    DESTDIR = ../../Legacy/code/bin/release
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../../Src/SuppressWarning.pri)


OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../../Src/EDD_Engine_Common/

android {
    INCLUDEPATH += $$PWD/../../Src/EDD_Engine_Android/
}
else {
    INCLUDEPATH += $$PWD/../../Src/EDD_Engine_Linux/
}



unix:!macx: PRE_TARGETDEPS += $$DESTDIR/../../lib/libEDD_Engine_Common.a
#unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Linux.a
#unix:!macx: PRE_TARGETDEPS += $$PWD/../FDI/bin/libFDI_DDS_AdapterLib.a
#unix:!macx: PRE_TARGETDEPS += $$PWD/../FDI/bin/libFDI_DDS_Lib.a

unix:!macx: LIBS += -L$$PWD/../../Legacy/code/lib -lEDD_Engine_Common
#unix:!macx: LIBS += -L$$PWD/../../Legacy/code/lib -lEDD_Engine_Linux



INCLUDEPATH += $$PWD/../../Legacy/code/lib/
DEPENDPATH += $$PWD/../../Legacy/code/lib
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR

