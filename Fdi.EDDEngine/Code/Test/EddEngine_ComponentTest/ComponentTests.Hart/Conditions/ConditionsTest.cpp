#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestConditionedAttributesWithAllValues
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static ParamCacheSimulator* paramCacheSimulator;
            static FDI_GENERIC_ITEM* genericItem;
            static String^ test_var_1_label;
            static EddEngineController^ engine;
            static  EVAL_VAR_VALUE* test_var_2_value;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                
                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                test_var_2_value = new EVAL_VAR_VALUE();
                test_var_2_value->type = VT_INTEGER;
                test_var_2_value->size = 4;
                test_var_2_value->val.i = 23;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, test_var_2_value);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_1_specifier);
                genericItem =  engine->GetVariableItem(test_var_1_specifier.id);

                // get the label of "test_var_1"
                FLAT_VAR* test_var_1 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_1_label = gcnew String(test_var_1->label.c_str());
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
                delete test_var_1_label;
                delete test_var_2_value;
                delete test_var_2_specifier;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_correct_attribute()
            {
                // check if the label of "test_var_1" has the expected value
                Assert::AreEqual("Condition is true", test_var_1_label);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditonedAttributesWithAllAvailableButWrongValue
        {
        public: 
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static String^ test_var_1_label;
            static FDI_GENERIC_ITEM* genericItem ;
            static ParamCacheSimulator* paramCacheSimulator;
            static EVAL_VAR_VALUE* test_var_2_value;
            static EddEngineController^ engine;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize( paramCacheSimulator);                

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                test_var_2_value = new EVAL_VAR_VALUE();
                test_var_2_value->type = VT_INTEGER;
                test_var_2_value->size = 4;
                test_var_2_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, test_var_2_value);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_1_specifier);
                genericItem =  engine->GetVariableItem(test_var_1_specifier.id);

                // get the label of "test_var_1"
                FLAT_VAR* test_var_1 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_1_label = gcnew String(test_var_1->label.c_str());
            }

            static void CleanupBase()
            {
                delete test_var_2_specifier;
                delete test_var_1_label;
                delete genericItem ;
                delete test_var_2_value;
                delete engine;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_attribute()
            {
                // check if the label of "test_var_1" has the expected value
                Assert::AreEqual("Condition is false", test_var_1_label);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditonedAttributesWithMissingValue
        {
        public: 
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator;
            static  EddEngineController^ engine ;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize( paramCacheSimulator);
                
                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // configure ParamCacheSimulator to return no value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, nullptr);

                // request "test_var_1"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_1_specifier);
                genericItem =  engine->GetVariableItem(test_var_1_specifier.id);
            }

            static void CleanupBase()
            {
                delete test_var_2_specifier;
                delete genericItem;
                delete engine;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_parameterCache_for_the_correct_ID()
            {
                // check if "test_var_2" was requested from the parameter cache
                Assert::AreEqual(test_var_2_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_error()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(1, static_cast<int>(genericItem->errors.count));
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_item_with_the_error_BAD_ATTR()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual((UINT)nsEDDEngine::label,(UINT) genericItem->errors.list[0].bad_attr);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_an_error_with_the_correct_missing_item_id()
            {
                // check if the evaluation of the label of "test_var_1" failed because of missing "test_var_2"
                Assert::AreEqual(test_var_2_specifier->id, genericItem->errors.list[0].var_needed.op_info.id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditonedAttributesWithInvalidParameter
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_4_specifier;
            static FLAT_VAR* test_var_4 ;
            static String^ test_var_3_label;
            static FDI_GENERIC_ITEM* genericItem;
            static FDI_GENERIC_ITEM* genericItem4;
            static ParamCacheSimulator* paramCacheSimulator;
            static EddEngineController^  engine;
            static EVAL_VAR_VALUE* test_var_4_value;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize( paramCacheSimulator);
                
                // obtain item specifier (id) for "test_var_2"
                test_var_4_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_4", test_var_4_specifier);

                // create value for "test_var_4"
                test_var_4_value = new EVAL_VAR_VALUE();
                test_var_4_value->type = VT_INTEGER;
                test_var_4_value->size = 4;
                test_var_4_value->val.i = 23;

                // configure ParamCacheSimulator to return special value for "test_var_4"
                paramCacheSimulator->SetParamValue(test_var_4_specifier, test_var_4_value);

                // request "test_var_1"; this will trigger reading of "test_var_4"
                FDI_PARAM_SPECIFIER test_var_3_specifier;
                engine->FillParamSpecifier("test_var_3", &test_var_3_specifier);
                genericItem =  engine->GetVariableItem(test_var_3_specifier.id);
                genericItem4 =  engine->GetVariableItem(test_var_4_specifier->id);
                // get the label of "test_var_3"
                FLAT_VAR* test_var_3 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_4 = dynamic_cast<FLAT_VAR*>(genericItem4->item);
                test_var_3_label = gcnew String(test_var_3->label.c_str());
            }

            static void CleanupBase()
            {
                delete engine;
                delete test_var_4_specifier;

                delete test_var_3_label;
                delete genericItem;
                delete genericItem4;

                delete test_var_4_value;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_be_invalid_for_the_value()
            {
                //see: sources\EddEngine\EDDEngine\Code\Interfaces\EDD_Engine_Interfaces\nsEDDEngine\Ddldefs.h    
                int actual = test_var_4->valid;
                int expected = 0; 
                // check if the test_var_4 is invalid (set in DD)
                Assert::AreEqual(expected, actual, "0 = false, 1= true");
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_be_return_the_correct_attribute()
            {
                Assert::AreEqual("Condition is true", test_var_3_label);

            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParameterCache()
            {
                // check if "test_var_4" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParameterCache_for_the_expected_Item_ID()
            {
                // check if "test_var_4" was requested from the parameter cache
                Assert::AreEqual(test_var_4_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestUnconditionedAttributes
        {
        public: 
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static String^ test_var_2_label;
            static FDI_GENERIC_ITEM* genericItem;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE* test_var_2_value;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                auto paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                

                // obtain item specifier (id) for "test_var_2"
                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // create value for "test_var_2"
                test_var_2_value = new EVAL_VAR_VALUE();
                test_var_2_value->type = VT_INTEGER;
                test_var_2_value->size = 4;
                test_var_2_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_2"
                paramCacheSimulator->SetParamValue(test_var_2_specifier, test_var_2_value);

                // request "test_var_2"; 
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_2", &test_var_1_specifier);
                genericItem =  engine->GetVariableItem(test_var_1_specifier.id);

                // get the label of "test_var_2"
                FLAT_VAR* test_var_2 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_2_label = gcnew String(test_var_2->label.c_str());
            }

            static void CleanupBase()
            {
                delete test_var_2_specifier;
                delete test_var_2_label;
                delete genericItem;
                delete engine;
                delete test_var_2_value;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_attribute()
            {
                // check if the label of "test_var_1" has the expected value
                Assert::AreEqual("Test Variable 2", test_var_2_label);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestDependenyInformationForLabel
        {
        public: 
            static nsEDDEngine::VAR_DEPBIN* test_var_1_dependencies;
            static FDI_PARAM_SPECIFIER* test_var_2_specifier;
            static EddEngineController^ engine;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                test_var_2_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_2", test_var_2_specifier);

                // request dependency information for "test_var_1";
                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_1_specifier);
                FDI_GENERIC_ITEM* genericItem = engine->GetGenericItem(test_var_1_specifier.id, ITYPE_VARIABLE);//, nsEDDEngine::item_information);
                FLAT_VAR*  test_var_1 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_1_dependencies = test_var_1->depbin;
            }

            static void CleanupBase()
            {
                delete test_var_1_dependencies;
                delete test_var_2_specifier;
                delete engine;

            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_one_dependency()
            {
                nsEDDEngine::OP_REF_LIST& test_var_1_label_dependencies = test_var_1_dependencies->db_label->dep;

                Assert::AreEqual(1, static_cast<int>(test_var_1_label_dependencies.count));
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_dependency_to_test_var_2()
            {
                nsEDDEngine::OP_REF_LIST& test_var_1_label_dependencies = test_var_1_dependencies->db_label->dep;

                Assert::AreEqual(test_var_2_specifier->id, test_var_1_label_dependencies.list[0].op_info.id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditionedAttributeValidity
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_6_specifier;
            static  int test_var_5_valid;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator ;
            static  EddEngineController^ engine;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);
                
                // obtain item specifier (id) for "test_var_6"
                test_var_6_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_6", test_var_6_specifier);

                // create value for "test_var_6"
                EVAL_VAR_VALUE* test_var_6_value = new EVAL_VAR_VALUE();
                test_var_6_value->type = VT_INTEGER;
                test_var_6_value->size = 4;
                test_var_6_value->val.i = 23;

                // configure ParamCacheSimulator to return special value for "test_var_6"
                paramCacheSimulator->SetParamValue(test_var_6_specifier, test_var_6_value);

                // request "test_var_5"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_5_specifier;
                engine->FillParamSpecifier("test_var_5", &test_var_5_specifier);
                genericItem = engine->GetVariableItem(test_var_5_specifier.id);

                // get the label of "test_var_5"
                FLAT_VAR* test_var_5 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_5_valid = test_var_5->valid;

                delete test_var_6_value;
            }

            static void CleanupBase()
            {
                delete genericItem;
                delete test_var_6_specifier;
                delete engine;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_correct_attribute()
            {
                int expected = 1;
                // check if the label of "test_var_5" is true
                Assert::AreEqual(expected, test_var_5_valid, "0 = false, 1= true");
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(test_var_6_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditionedAttributeValidityWithWrongValue
        {
        public:
            static FDI_PARAM_SPECIFIER* test_var_6_specifier;
            static  int test_var_5_valid;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator ;
            static  EddEngineController^   engine;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);

                // obtain item specifier (id) for "test_var_6"
                test_var_6_specifier = new FDI_PARAM_SPECIFIER();
                engine->FillParamSpecifier("test_var_6", test_var_6_specifier);

                // create value for "test_var_6"
                EVAL_VAR_VALUE* test_var_6_value = new EVAL_VAR_VALUE();
                test_var_6_value->type = VT_INTEGER;
                test_var_6_value->size = 4;
                test_var_6_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_6"
                paramCacheSimulator->SetParamValue(test_var_6_specifier, test_var_6_value);

                // request "test_var_5"; this will trigger reading of "test_var_2"
                FDI_PARAM_SPECIFIER test_var_5_specifier;
                engine->FillParamSpecifier("test_var_5", &test_var_5_specifier);
                genericItem =  engine->GetVariableItem(test_var_5_specifier.id);

                // get the label of "test_var_5"
                FLAT_VAR* test_var_5 = dynamic_cast<FLAT_VAR*>(genericItem->item);
                test_var_5_valid = test_var_5->valid;

                delete test_var_6_value;

            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
                delete test_var_6_specifier;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_the_correct_attribute()
            {
                int expected = 0; 
                // check if the label of "test_var_5" is false
                Assert::AreEqual(expected, test_var_5_valid, "0 = false, 1= true");
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_correct_ID()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(test_var_6_specifier->id, paramCacheSimulator->RequestedParameters[0].id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditionedMenuItemEvaluatesTrue
        {
        public:
            static FLAT_MENU* page_menu_2;
            static ITEM_ID test_var_1_item_id;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator ;
            static  EddEngineController^ engine;
            static  EVAL_VAR_VALUE* test_var_6_value;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);                

                // obtain item specifier (id) for "test_var_6"
                FDI_PARAM_SPECIFIER test_var_6_specifier;
                engine->FillParamSpecifier("test_var_6", &test_var_6_specifier);

                FDI_PARAM_SPECIFIER test_var_1_specifier;
                engine->FillParamSpecifier("test_var_1", &test_var_1_specifier);
                test_var_1_item_id = test_var_1_specifier.id;

                // create value for "test_var_6"
                test_var_6_value = new EVAL_VAR_VALUE();
                test_var_6_value->type = VT_INTEGER;
                test_var_6_value->size = 4;
                test_var_6_value->val.i = 23;

                // configure ParamCacheSimulator to return special value for "test_var_6"
                paramCacheSimulator->SetParamValue(&test_var_6_specifier, test_var_6_value);

                // request "page_menu_2"; this will trigger reading of "test_var_6"
                FDI_PARAM_SPECIFIER page_menu_2_specifier;
                engine->FillParamSpecifier("page_menu_2", &page_menu_2_specifier);
                genericItem = engine->GetGenericItem(page_menu_2_specifier.id);
                page_menu_2 = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
                delete test_var_6_value;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_a_flat_menu()
            {
                Assert::AreNotEqual((int)page_menu_2, 0);
                // AreEqual(expected, test_var_5_valid, "0 = false, 1= true");
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_contain_two_menu_items()
            {
                Assert::AreEqual(UInt16(2), page_menu_2->items.count);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_contain_test_var_1()
            {
                Assert::AreEqual(test_var_1_item_id, page_menu_2->items.list[0].ref.op_info.id);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestConditionedMenuitemEvaluatesFalse
        {
        public: 
            static FLAT_MENU* page_menu_2;
            static ITEM_ID test_var_2_item_id;
            static FDI_GENERIC_ITEM* genericItem;
            static ParamCacheSimulator* paramCacheSimulator;
            static EddEngineController^ engine;
            static EVAL_VAR_VALUE* test_var_6_value;

            static void SetupBase(String^ testFileName)
            {
                // initialize EDD engine with simulated ParamCache
                engine = gcnew EddEngineController(testFileName);
                paramCacheSimulator = new ParamCacheSimulator();
                engine->Initialize(paramCacheSimulator);
                paramCacheSimulator->SetEddEngine(engine->Instance);
                
                // obtain item specifier (id) for "test_var_6"
                FDI_PARAM_SPECIFIER test_var_6_specifier;
                engine->FillParamSpecifier("test_var_6", &test_var_6_specifier);

                FDI_PARAM_SPECIFIER test_var_2_specifier;
                engine->FillParamSpecifier("test_var_2", &test_var_2_specifier);
                test_var_2_item_id = test_var_2_specifier.id;

                // create value for "test_var_6"
                test_var_6_value = new EVAL_VAR_VALUE();
                test_var_6_value->type = VT_INTEGER;
                test_var_6_value->size = 4;
                test_var_6_value->val.i = 42;

                // configure ParamCacheSimulator to return special value for "test_var_6"
                paramCacheSimulator->SetParamValue(&test_var_6_specifier, test_var_6_value);

                // request "page_menu_2"; this will trigger reading of "test_var_6"
                FDI_PARAM_SPECIFIER page_menu_2_specifier;
                engine->FillParamSpecifier("page_menu_2", &page_menu_2_specifier);
                genericItem = engine->GetGenericItem(page_menu_2_specifier.id);
                page_menu_2 = dynamic_cast<FLAT_MENU*>(genericItem->item);
            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
                delete test_var_6_value;
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_return_a_flat_menu()
            {
                Assert::AreNotEqual((int)page_menu_2, 0);
                // AreEqual(expected, test_var_5_valid, "0 = false, 1= true");
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_call_the_ParamCacheManager_for_the_Parameter()
            {
                // check if "test_var_6" was requested from the parameter cache
                Assert::AreEqual(unsigned(1), paramCacheSimulator->RequestedParameters.size());
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_contain_three_menu_items()
            {
                Assert::AreEqual(UInt16(3), page_menu_2->items.count);
            }

            [TestMethod, TestCategory("ConditionsTest")]
            void It_should_contain_test_var_2()
            {
                Assert::AreEqual(test_var_2_item_id, page_menu_2->items.list[0].ref.op_info.id);
            }
        };


        // WORKAROUND: use TestInitalize 
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initalize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with 7, but every test use only 1)

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_all_Values_available_with_FM8 : BaseTestConditionedAttributesWithAllValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_all_Values_available_with_FMA : BaseTestConditionedAttributesWithAllValues
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_all_Values_available_but_wrong_value_with_FM8 : BaseTestConditonedAttributesWithAllAvailableButWrongValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_all_Values_available_but_wrong_value_with_FMA : BaseTestConditonedAttributesWithAllAvailableButWrongValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_some_missing_values_with_FM8 : BaseTestConditonedAttributesWithMissingValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_some_missing_values_with_FMA : BaseTestConditonedAttributesWithMissingValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");

            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_one_Parameter_is_invalid_with_FM8 : BaseTestConditonedAttributesWithInvalidParameter
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attributes_with_one_Parameter_is_invalid_with_FMA : BaseTestConditonedAttributesWithInvalidParameter
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_unconditioned_Attributes_with_FM8 : BaseTestUnconditionedAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_unconditioned_Attributes_with_FMA : BaseTestUnconditionedAttributes
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_dependency_information_for_a_label_of_a_variable_with_FM8 : BaseTestDependenyInformationForLabel
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup]
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_dependency_information_for_a_label_of_a_variable_with_FMA : BaseTestDependenyInformationForLabel
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attribute_validity_with_all_Values_available_with_FM8 : BaseTestConditionedAttributeValidity
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attribute_validity_with_all_Values_available_with_FMA : BaseTestConditionedAttributeValidity
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attribute_validity_with_all_Values_available_but_wrong_value_with_FM8 : BaseTestConditionedAttributeValidityWithWrongValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_Attribute_validity_with_all_Values_available_but_wrong_value_with_FMA : BaseTestConditionedAttributeValidityWithWrongValue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_menu_item_list_evaluates_to_true_with_FM8 : BaseTestConditionedMenuItemEvaluatesTrue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup]
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_menu_item_list_evaluates_to_true_with_FMA : BaseTestConditionedMenuItemEvaluatesTrue
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_menu_item_list_evaluates_to_false_with_FM8 : BaseTestConditionedMenuitemEvaluatesFalse
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fm8");
            }

            [TestCleanup] 
            void CleanUp() 
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_Call_conditioned_menu_item_list_evaluates_to_false_with_FMA : BaseTestConditionedMenuitemEvaluatesFalse
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0004.fmA");
            }

            [TestCleanup] 
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
