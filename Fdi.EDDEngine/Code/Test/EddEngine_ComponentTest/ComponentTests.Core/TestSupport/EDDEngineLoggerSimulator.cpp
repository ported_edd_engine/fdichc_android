#include "EDDEngineLoggerSimulator.h"
#include <iostream>
#include <sstream>

//using namespace nsEDDEngine;

namespace ComponentTests
{
    namespace Core
    {
        namespace TestSupport
		{
			EDDEngineLoggerSimulator::EDDEngineLoggerSimulator() 
			{
			}

			EDDEngineLoggerSimulator::~EDDEngineLoggerSimulator()
			{

			}
			bool EDDEngineLoggerSimulator::ShouldLog(LogSeverity eLogSeverity, wchar_t *sCategory)
			{
				return true;
			}
			void EDDEngineLoggerSimulator::Log( wchar_t *sMessage, LogSeverity eLogSeverity, wchar_t *sCategory)
			{
				
			}

		} // namespace TestSupport
    } // namespace Core
} // namespace ComponentTests
