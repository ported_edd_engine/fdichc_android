#ifndef lint
static char SCCSID[] = "@(#)cm_tbl.c	30.5  30  14 Nov 1996";
#endif /* lint */
/**
 *		Device Description Services Rel. 4.2
 *		Copyright 1994-1996 - Fieldbus Foundation
 *		All rights reserved.
 */

/*
 *	cm_tbl.c - Connection Manager Tables Module
 *
 *	This file contains all functions pertaining to the Connection
 *	(Active) Tables.
 */
#include "stdinc.h"
#include <stdlib.h>


#include "cm_lib.h"
#include "panic.h"
#include "DDI_LIB.H"
#include "env_info.h"
#include "int_diff.h"
#include "dev_type.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"
extern char	*strdup();

/*
 *	Function pointers for removing DDS Tables and Application
 *	Information from the Active Tables.
 */

static int (*remove_dd_dev_tbl_fn_ptr) P((ENV_INFO *, DEVICE_HANDLE)) = 0;
static int (*remove_dd_blk_tbl_fn_ptr) P((ENV_INFO *, BLOCK_HANDLE)) = 0;
static int (*remove_blk_app_info_fn_ptr) P((ENV_INFO *, BLOCK_HANDLE)) = 0;
static int (*remove_dev_app_info_fn_ptr) P((ENV_INFO *, DEVICE_HANDLE)) = 0;


/***********************************************************************
 *
 *	Name:  ct_init
 *
 *	ShortDesc:  Initialize the Connection Table Manager.
 *
 *	Description:
 *		The ct_init function initializes the Connection (Active)
 *		Tables by setting each table count to zero and initializing
 *		each table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_init()
{
	/*
	 *	Set each table count to zero and initialize each table
	 *	list pointer.
	 */

	active_blk_tbl.count = 0;
	active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM **) malloc(1);
	if (!active_blk_tbl.list) {
		return(CM_NO_MEMORY);
	}

	active_dev_tbl.count = 0;
	active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM **) malloc(1);
	if (!active_dev_tbl.list) {
		return(CM_NO_MEMORY);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_cleanup
 *
 *	ShortDesc:  Cleanup the Connection Table Manager.
 *
 *	Description:
 *		The ct_init function initializes the Connection (Active)
 *		Tables by setting each table count to zero and initializing
 *		each table list pointer.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ct_cleanup()
{
	/*
	 *	Free each table list pointer.
	 */

	active_blk_tbl.count = 0;
	free (active_blk_tbl.list);
	active_blk_tbl.list = nullptr;

	active_dev_tbl.count = 0;
	free (active_dev_tbl.list);
	active_dev_tbl.list = nullptr;
}


/***********************************************************************
 *
 *	Name:  ct_init_dds_tbl_fn_ptrs
 *
 *	ShortDesc:  Initialize the DDS table function pointers.
 *
 *	Description:
 *		The ct_init_dds_tbl_fn_ptrs function links the pointers of
 *		the DDS functions that the Connection Manager calls when any
 *		DDS tables need to be removed.
 *
 *	Inputs:
 *		dds_tbl_fn_ptrs - the necessary DDS table function pointers.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		None.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ct_init_dds_tbl_fn_ptrs(DDS_TBL_FN_PTRS *dds_tbl_fn_ptrs)
{

	/*
	 *	Link the pointers of the functions that remove DD device
	 *	and DD block tables (DDS Tables).
	 */

	remove_dd_dev_tbl_fn_ptr = dds_tbl_fn_ptrs->remove_dd_dev_tbl_fn_ptr;

	remove_dd_blk_tbl_fn_ptr = dds_tbl_fn_ptrs->remove_dd_blk_tbl_fn_ptr;
}

/***********************************************************************
 *
 *	Name:  ct_block_search
 *
 *	ShortDesc:  Search for a block in the Active Block Table using
 *				a tag.
 *
 *	Description:
 *		The ct_block_search function takes an Object Index and searches
 *		through the valid (allocated) Active Block Table elements in
 *		the Active Block Table list, looking for the element that
 *		matches the Object Index.  If found, the corresponding block handle
 *		will be returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		usObjectIndex - unique identifier of a block.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The block handle of the matching Active Block Table element
 *		if successful or -1 if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

BLOCK_HANDLE CConnectionMgr::ct_block_search(OBJECT_INDEX usObjectIndex)
{

	BLOCK_HANDLE	block_handle;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (block_handle = 0; block_handle < active_blk_tbl.count;
			block_handle ++) {
		
		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_blk_tbl.list[block_handle]) {
	
			/*
			 *	Check the tag of the element.
			 */

			if (ABT_A_OP_INDEX(block_handle) == usObjectIndex) {
				return(block_handle);
			}
		}
	}

	return(-1);
}


/***********************************************************************
 *
 *	Name:  ct_device_search
 *
 *	ShortDesc:  Search for a device in the Active Device Table using
 *				a network and station address.
 *
 *	Description:
 *		The ct_device_search function takes a network and station
 *		address and searches through the valid (allocated) Active
 *		Device Table elements in the Active Device Table list,
 *		looking for the element that matches the network and station
 *		address.  If found, the corresponding device handle will be
 *		returned.  Otherwise an error code will be returned.
 *
 *	Inputs:
 *		network - unique identifier of a device (along with station
 *				  address).
 *		station_address - unique identifier of a device (along with
 *						  network).
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device handle of the matching Active Device Table element
 *		if successful or negative (-1) if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

DEVICE_HANDLE CConnectionMgr::ct_device_search(const wchar_t *sDeviceName)
{

	DEVICE_HANDLE	device_handle;

	/*
	 *	Go through each element in the Active Device Table.
	 */

	for (device_handle = 0; device_handle < active_dev_tbl.count;
			device_handle ++) {

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_tbl.list[device_handle]) {

			/*
			 *	Check the network and station address of the element.
			 */

			if (wcscmp(ADT_DEVICE_NAME(device_handle), sDeviceName) == 0) {
				return(device_handle);
			}
		}
	}

	return(-1);
}




/***********************************************************************
 *
 *	Name:  ct_device_type_search
 *
 *	ShortDesc:  Search for a device type in the Active Device Type
 *				Table using a DD device ID.
 *
 *	Description:
 *		The ct_device_type_search function takes a DD device ID and
 *		searches through the valid (allocated) Active Device Type
 *		Table elements in the Active Device Type Table list, looking
 *		for the element that matches the DD device ID.  If found, the
 *		corresponding device type handle will be returned.  Otherwise
 *		an error will be returned.
 *
 *	Inputs:
 *		dd_device_id - unique identifier of a device type.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		The device type handle of the matching Active Device Type
 *		Table element if successful or -1 if not found.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

DEVICE_TYPE_HANDLE CDeviceTypeMgr::ct_device_type_search(DD_DEVICE_ID *dd_device_id)
{

	DEVICE_TYPE_HANDLE	device_type_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (device_type_handle = 0; device_type_handle < active_dev_type_tbl.count; device_type_handle ++)
	{

		/*
		 *	Check to make sure that the element is valid.
		 */

		if (active_dev_type_tbl.list[device_type_handle])
		{

			/*
			 *	Check the DD device ID of the element.
			 */
			DD_DEVICE_ID * pDeviceID = ADTT_DD_DEVICE_ID(device_type_handle);

			if (pDeviceID->ddid_manufacturer ==dd_device_id->ddid_manufacturer &&
					pDeviceID->ddid_device_type ==dd_device_id->ddid_device_type &&
					pDeviceID->ddid_device_rev ==dd_device_id->ddid_device_rev &&
					(wcscmp(pDeviceID->dd_path, dd_device_id->dd_path) ==0)) 
					{
						return(device_type_handle);
					}
			}
		}

	return(-1);
}


/***********************************************************************
 *
 *	Name:  ct_new_block
 *
 *	ShortDesc:  Create a new Active Block Table element.
 *
 *	Description:
 *		The ct_new_block function creates a new Active Block Table
 *		element either in the first unused (deallocated) element
 *		space or at the end of the existing Active Block Table list.
 *		The corresponding block handle will be returned unless there
 *		is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		block_handle - handle of the new Active Block Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_new_block(BLOCK_HANDLE *block_handle)
{

	BLOCK_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Block Table.
	 */

	for (temp_handle = 0; temp_handle < active_blk_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_blk_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Block Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_blk_tbl.count) {

		/*
		 *	Extend the Active Block Table.
		 */

		active_blk_tbl.list = (ACTIVE_BLK_TBL_ELEM **) realloc(
				(char *)active_blk_tbl.list,
				(unsigned int)(sizeof(ACTIVE_BLK_TBL_ELEM *) *
				(active_blk_tbl.count + 1)));

		if (!active_blk_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_blk_tbl.count ++;
		temp_handle = active_blk_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Block Table element where the block
	 *	handle is.
	 */

	active_blk_tbl.list[temp_handle] =
			(ACTIVE_BLK_TBL_ELEM *) calloc(1, sizeof(ACTIVE_BLK_TBL_ELEM));

	if (!active_blk_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}
	
	*block_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_new_device
 *
 *	ShortDesc:  Create a new Active Device Table element.
 *
 *	Description:
 *		The ct_new_device function creates a new Active Device Table
 *		element either in the first unused (deallocated) element space
 *		or at the end of the existing Active Device Table list.  The
 *		corresponding device handle will be returned unless there is
 *		an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_handle - handle of the new Active Device Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_new_device(DEVICE_HANDLE *device_handle)
{

	DEVICE_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Device Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_tbl.count) {

		/*
		 *	Extend the Active Device Table.
		 */

		active_dev_tbl.list = (ACTIVE_DEV_TBL_ELEM **) realloc(
				(char *)active_dev_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TBL_ELEM *) *
				(active_dev_tbl.count + 1)));

		if (!active_dev_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_tbl.count ++;
		temp_handle = active_dev_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Table element where the device
	 *	handle is.
	 */

	active_dev_tbl.list[temp_handle] =
			(ACTIVE_DEV_TBL_ELEM *) calloc(1, sizeof(ACTIVE_DEV_TBL_ELEM));

	if (!active_dev_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_new_device_type
 *
 *	ShortDesc:  Create a new Active Device Type table element.
 *
 *	Description:
 *		The ct_new_device_type function creates a new Active Device
 *		Type Table element either in the first unused (deallocated)
 *		element space or at the end of the existing Active Device
 *		Type Table list.  The corresponding device type table handle
 *		will be returned unless there is an allocation error.
 *
 *	Inputs:
 *		None.
 *
 *	Outputs:
 *		device_type_handle - handle of the new Active Device Type
 *							 Table element.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CDeviceTypeMgr::ct_new_device_type(DEVICE_TYPE_HANDLE *device_type_handle)
{

	DEVICE_TYPE_HANDLE	temp_handle;

	/*
	 *	Go through each element in the Active Device Type Table.
	 */

	for (temp_handle = 0; temp_handle < active_dev_type_tbl.count;
			temp_handle ++) {

		/*
		 *	Check if the element has been deallocated.
		 */

		if (!active_dev_type_tbl.list[temp_handle]) {
			break;
		}
	}

	/*
	 *	Check if the entire Active Device Type Table was gone through
	 *	without finding any deallocated elements.
	 */

	if (temp_handle >= active_dev_type_tbl.count) {

		/*
		 *	Extend the Active Device Type Table.
		 */

		active_dev_type_tbl.list = (ACTIVE_DEV_TYPE_TBL_ELEM **) realloc(
				(char *)active_dev_type_tbl.list,
				(unsigned int)(sizeof(ACTIVE_DEV_TYPE_TBL_ELEM *) *
				(active_dev_type_tbl.count + 1)));

		if (!active_dev_type_tbl.list) {
			return(CM_NO_MEMORY);
		}
		active_dev_type_tbl.count ++;

		temp_handle = active_dev_type_tbl.count - 1;
	}

	/*
	 *	Allocate a new Active Device Type Table element where the
	 *	device type handle is.
	 */

	active_dev_type_tbl.list[temp_handle] =
			(ACTIVE_DEV_TYPE_TBL_ELEM *) calloc(1,
			sizeof(ACTIVE_DEV_TYPE_TBL_ELEM));

	if (!active_dev_type_tbl.list[temp_handle]) {
		return(CM_NO_MEMORY);
	}

	*device_type_handle = temp_handle;
	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_free_block
 *
 *	ShortDesc:  Remove a specific element of the Active Block Table.
 *
 *	Description:
 *		The ct_free_block function removes a specific element of
 *		the Active Block Table.  Note that the element will be
 *		deallocated but the Active Block Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		block_handle - handle (offset) of the specific Active Block
 *					   Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ct_free_block(ENV_INFO *env_info,BLOCK_HANDLE block_handle)
{
	int r_code = CM_SUCCESS;

	/*
	 *	Remove any data connected to the Active Block Table and
	 *	deallocate the block.
	 */

	if (remove_dd_blk_tbl_fn_ptr != 0) {
		r_code = (*remove_dd_blk_tbl_fn_ptr)(env_info, block_handle);
		ASSERT_DBG(!r_code);
	}

	if (remove_blk_app_info_fn_ptr != 0) {
		r_code = (*remove_blk_app_info_fn_ptr)(env_info, block_handle);
		ASSERT_DBG(!r_code);
		
	}

	/*
	 *	Check if a tag exits.
	 */

	if (ABT_TAG(block_handle)) {
		free(ABT_TAG(block_handle));
	}

	/*
	 *	Free the Active Block Table element and null the element pointer.
	 */

	free((char *)active_blk_tbl.list[block_handle]);
	active_blk_tbl.list[block_handle] = 0;
}


/***********************************************************************
 *
 *	Name:  ct_free_device
 *
 *	ShortDesc:  Remove a specific member of the Active Device Table.
 *
 *	Description:
 *		The ct_free_device function removes a specific element of
 *		the Active Device Table.  Note that the element will be
 *		deallocated but the Active Device Table list will not be
 *		reduced.
 *
 *	Inputs:
 *		device_handle - handle (offset) of the specific Active Device
 *						Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CConnectionMgr::ct_free_device(ENV_INFO *env_info, DEVICE_HANDLE device_handle)
{
	int r_code = CM_SUCCESS;

	/*
	 *	Close the OPROD of the device, terminate communication
	 *	with the device, remove any data connected to the Active
	 *	Device Table, and deallocate the device.
	 */
	
	if (g_DeviceTypeMgr.valid_rod_handle(ADT_OPROD_HANDLE(device_handle))) {
		r_code = g_DeviceTypeMgr.rod_close(ADT_OPROD_HANDLE(device_handle));
		ASSERT_DBG(r_code == CM_SUCCESS);

		set_adt_oprod_handle(device_handle, -1);
	}

	if (remove_dev_app_info_fn_ptr != 0) {
		r_code = (*remove_dev_app_info_fn_ptr)(env_info, device_handle);
		ASSERT_DBG(!r_code);
		void *app_info;
		r_code = get_adt_app_info(device_handle, &app_info);
		ASSERT_DBG(!r_code);
		
	}

	/*
	 *	Free the Active Device Table element and null the element
	 *	pointer.
	 */

	free((char *)active_dev_tbl.list[device_handle]);
	active_dev_tbl.list[device_handle] = 0;
}


/***********************************************************************
 *
 *	Name:  ct_free_device_type
 *
 *	ShortDesc:  Remove a specific member of the Active Device Type
 *				Table.
 *
 *	Description:
 *		The ct_free_device_type function removes a specific element
 *		of the Active Device Type Table.  Note that the element will
 *		be deallocated but the Active Device Type Table list will not
 *		be reduced.
 *
 *	Inputs:
 *		device_type_handle - handle (offset) of the specific Active
 *							 Device Type Table element to be removed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		Void.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

void CDeviceTypeMgr::ct_free_device_type( ENV_INFO* env_info, DEVICE_TYPE_HANDLE device_type_handle)
{
	int r_code = CM_SUCCESS;

	/*
	 *	Remove the DD of the device type, remove any data connected to
	 *	the Active Device Type Table, and deallocate the device type.
	 */

	ROD_HANDLE rod_handle = ADTT_DD_HANDLE(device_type_handle);
	if (valid_rod_handle(rod_handle))
	{
		r_code = rod_close( rod_handle );
		ASSERT_DBG(r_code == CM_SUCCESS);
	}

	if (remove_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*remove_dd_dev_tbl_fn_ptr)(env_info, device_type_handle);
		ASSERT_DBG(!r_code);
		ASSERT_DBG(!ADTT_DD_DEV_TBLS(device_type_handle));
	}

		r_code = remove_sym_info(env_info, device_type_handle);
		ASSERT_DBG(!r_code);
		ASSERT_DBG(!ADTT_SYM_INFO(device_type_handle));
	

	/*
	 *	Free the Active Device Type Table element and null the
	 *	element pointer.
	 */

	free((char *)active_dev_type_tbl.list[device_type_handle]);
	active_dev_type_tbl.list[device_type_handle] = 0;
}

/***********************************************************************
 *
 *	Name:  ct_device_fill
 *
 *	ShortDesc:  Fill in the table entries for a device
 *
 *	Description:
 *				Open a new active device and initialize the active device
 *				with the values passed in with find_cnf.  Also, open a new
 *				active device type if necessary.
 *
 *	Inputs:
 *		find_cnf - the structure identifies a device.  The
 *				   structure is normally filled by either
 *				   find tag or identify service.
 *		device_address - the address of the identified device.
 *					It is saved into the connection tables
 *					for later use.
 *
 *	Outputs:
 *		device_handle - the corresponding device handle of the
 *						opened device.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_COMM.
 *		Return values from ct_new_device function.
 *		Return values from cr_initiate_comm function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_device_fill(FIND_CNF *find_cnf, DEVICE_ADDRESS * /*device_address*/, DEVICE_HANDLE *device_handle)
{
	int					r_code  = 0;

	DEVICE_TYPE_HANDLE	device_type_handle ;
	DD_DEVICE_ID		dd_device_id ;
	NETWORK_HANDLE		network ;
	UINT16				stat_addr ;

	/*
	 *	Allocate a new Active Device Table element and initialize
	 *	its members.
	 */

	r_code = ct_new_device(device_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	network = find_cnf->find_cnf.find_cnf_a.network ;
	stat_addr = find_cnf->find_cnf.find_cnf_a.station_address ;
	set_adt_network(*device_handle, network);
	set_adt_station_address(*device_handle, stat_addr); 
	SET_ADT_DEVICE_NAME(*device_handle, find_cnf->find_cnf.find_cnf_a.DeviceName) ;

	set_adt_a_opod_cref(*device_handle, -1);
	set_adt_a_ddod_cref(*device_handle, -1);
	set_adt_a_mib_cref(*device_handle, -1);

	(void)memcpy((char *)&dd_device_id,
			(char *)&find_cnf->find_cnf.find_cnf_a.dd_device_id,
			sizeof(DD_DEVICE_ID)) ;

	set_adt_oprod_handle(*device_handle, -1);
	set_adt_app_info(*device_handle, 0);
	set_adt_usage(*device_handle, 1);
	

	/*
	 *	Search the Active Device Type Table for the DD device ID.
	 */

	device_type_handle = g_DeviceTypeMgr.ct_device_type_search(&dd_device_id);

	/*
	 *	Check if the device type is already in use.
	 */

	if (device_type_handle >= 0) {

		/*
		 *	Increment the device type's usage, set the Active
		 *	Device Type Table offset of the device, and return.
		 */
		g_DeviceTypeMgr.set_adtt_usage(device_type_handle,
				g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) + 1) ;
		SET_ADT_ADTT_OFFSET(*device_handle, device_type_handle) ;
		return(CM_SUCCESS) ;
	}

	/*
	 *	Allocate a new Active Device Type Table element and
	 *	initialize its members.
	 */

	r_code = g_DeviceTypeMgr.ct_new_device_type(&device_type_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	g_DeviceTypeMgr.SET_ADTT_DD_DEVICE_ID(device_type_handle, &dd_device_id) ;
	g_DeviceTypeMgr.SET_ADTT_DD_HANDLE(device_type_handle, -1) ;
	g_DeviceTypeMgr.set_adtt_dd_dev_tbls(device_type_handle, 0) ;
	g_DeviceTypeMgr.set_adtt_sym_info(device_type_handle, 0) ;
	g_DeviceTypeMgr.set_adtt_usage(device_type_handle, 1) ;
	SET_ADT_ADTT_OFFSET(*device_handle, device_type_handle) ;

	return(CM_SUCCESS) ;
}


/***********************************************************************
 *
 *	Name:  fill_find_cnf
 *
 *	ShortDesc:  Fill the FIND_CNF data struct
 *
 *	Description:
 *
 *	Inputs:
 *		tag - tag of the block to be opened.
 *
 *	Outputs:
 *		device_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from cr_find_block function.
 *		Return values from ct_block_fill function.
 *
 *	Authors:
 *		Mike Dieter
 *
 **********************************************************************/
static void
fill_find_cnf( FIND_CNF *find_cnf, const wchar_t* sDeviceName, const DD_DEVICE_ID *dd_device_id )
{
	ASSERT_DBG(find_cnf && sDeviceName && dd_device_id);

	find_cnf->type = NT_OFF_LINE;

	find_cnf->find_cnf.find_cnf_a.network = 0;		// We only have one "network".
	find_cnf->find_cnf.find_cnf_a.network_type = NT_OFF_LINE;

	wcscpy(find_cnf->find_cnf.find_cnf_a.DeviceName, sDeviceName);

	memcpy((void*)&(find_cnf->find_cnf.find_cnf_a.dd_device_id), dd_device_id,
						sizeof(*dd_device_id));
}


/***********************************************************************
 *
 *	Name:  ct_device_open
 *
 *	ShortDesc:  Open a session with a tagged block.
 *
 *	Description:
 *		The ct_block_open function takes a block tag and opens a new
 *		session with a block that matches the tag.  A system-wide tag
 *		search may be used if the block was not found in the Active
 *		Block Table.  If successful, the corresponding block handle 
 *		will be returned.  If necessary, the a new active block will be
 *		allocated and filled.  Otherwise, an error code will be 
 *		returned.
 *
 *	Inputs:
 *		tag - tag of the block to be opened.
 *
 *	Outputs:
 *		device_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from cr_find_block function.
 *		Return values from ct_block_fill function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_device_open(const wchar_t* sDeviceName, DD_DEVICE_ID *dd_device_id, DEVICE_HANDLE *device_handle)
{
	ASSERT_RET(sDeviceName && device_handle, CM_BAD_POINTER);
	 
	/*
	 *	Search the Active Device Table for the tag.
	 */

	*device_handle = ct_device_search(sDeviceName);

	/*
	 *	Check if the the block is already in use.
	 */

	if (*device_handle >= 0) {

		/*
		 *	Increment the block's usage and return.
		 */
		set_adt_usage(*device_handle, ADT_USAGE(*device_handle) + 1);
		return(CM_SUCCESS);
	}

	//
	// Fill in the FIND_CNF data struct.
	//
	FIND_CNF find_cnf = {0};

	fill_find_cnf( &find_cnf, sDeviceName, dd_device_id );

	//
	// Fill the device table entry
	//
	DEVICE_ADDRESS device_address = {0};

	int r_code = ct_device_fill(&find_cnf, &device_address, device_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_device_close
 *
 *	ShortDesc:  Close a previously opened session with a device.
 *
 *	Description:
 *		The ct_block_close function takes a block handle and closes
 *		a previously opened session with a block.  The function checks
 *		for the corresponding Active Block, Device, and Device Type
 *		elements that are no longer used, and deallocates them.
 *
 *	Inputs:
 *		block_handle - handle of the block to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_BLOCK_CLOSE.
 *		Return values from rod_close function.
 *		Return values from cr_terminate_comm function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_device_close(ENV_INFO *env_info, DEVICE_HANDLE device_handle)
{

	bool	device_deallocate = false;
	bool	device_type_deallocate = false;

	ASSERT_RET(valid_device_handle(device_handle), CM_BAD_DEVICE_HANDLE) ;

	DEVICE_TYPE_HANDLE device_type_handle = ADT_ADTT_OFFSET(device_handle);

	/*
	 *	Check if the device has multiple sessions.
	 */

	if (ADT_USAGE(device_handle) > 1) {

		/*
		 *	Decrement the device's usage.
		 */
		set_adt_usage(device_handle, ADT_USAGE(device_handle) - 1);
		//SET_ADT_USAGE(device_handle,ADT_USAGE(device_handle) - 1);

	} else {

		/*
		 *	Set the flag to deallocate the device.
		 */

		device_deallocate = true;

		/*
		 *	Check if the device type has multiple sessions.
		 */

		if (g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) > 1) {

			/*
			 *	Decrement the device type's usage.
			 */
			g_DeviceTypeMgr.set_adtt_usage(device_type_handle,
					g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) - 1);

		} else {

			/*
			 *	Set the flag to deallocate the device type.
			 */

			device_type_deallocate = true;
		}
	}

	/*
	 *	Check if the device is to be deallocated.
	 */

	if (device_deallocate)
	{
		ct_free_device(env_info, device_handle);
	}

	/*
	 *	Check if the device type is to be deallocated.
	 */

	if (device_type_deallocate)
	{
		g_DeviceTypeMgr.ct_free_device_type(env_info, device_type_handle);
	}

	return(CM_SUCCESS);
}



/***********************************************************************
 *
 *	Name:  ct_block_fill
 *
 *	ShortDesc:  Fill in the table entries for a device
 *
 *	Description:
 *				Open a new active block and initialize
 *				the active block with the values passed
 *				in with find_cnf and Object Index.  Also, open a new
 *				active device type if necessary.
 *
 *	Inputs:
 *			usObjectIndex - Object Index of the block to fill
 *			find_cnf - information to initialize the block with
 *
 *	Outputs:
 *		block_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from ct_new_block function.
 *		Return values from ct_device_fill function.
 *
 *	Authors:
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_block_fill(OBJECT_INDEX usObjectIndex, FIND_CNF *find_cnf, BLOCK_HANDLE *block_handle)
{

	DEVICE_HANDLE		device_handle;
	int					r_code;
	DEVICE_ADDRESS		device_address ;

	ASSERT_RET( find_cnf && block_handle, CM_BAD_POINTER ) ;

	/*
	 *	Allocate a new Active Block Table element and initialize
	 *	its members.
	 */

	(void)memset( (char *)&device_address, 0, sizeof(device_address) );

	r_code = ct_new_block(block_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	SET_ABT_A_OP_INDEX(*block_handle, usObjectIndex);
	SET_ABT_PARAM_COUNT(*block_handle, (unsigned)-1);
	SET_ABT_DD_BLK_ID(*block_handle, 0);
	SET_ABT_DD_BLK_TBL_OFFSET(*block_handle, -1);
	set_abt_app_info(*block_handle, 0);
	set_abt_usage(*block_handle, 1);

	/*
	 *	Search the Active Device Table for the network and
	 *	station address.
	 */

	device_handle = ct_device_search( find_cnf->find_cnf.find_cnf_a.DeviceName ) ;

	/*
	 *	Check if the device is already in use.
	 */

	if (device_handle >= 0) {

		/*
		 *	Increment the device's usage, set the Active Device
		 *	Table offset of the block, and return.
		 */
		set_adt_usage(device_handle, ADT_USAGE(device_handle) + 1);
		SET_ABT_ADT_OFFSET(*block_handle, device_handle);
		return(CM_SUCCESS);
	}

	r_code = ct_device_fill(find_cnf, &device_address, &device_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	SET_ABT_ADT_OFFSET(*block_handle, device_handle) ;
	return(CM_SUCCESS) ;
}




/***********************************************************************
 *
 *	Name:  ct_block_open
 *
 *	ShortDesc:  Open a session with a block.
 *
 *	Description:
 *		The ct_block_open function takes an Object Index and opens a new
 *		session with a block that matches the Object Index.
 *		If successful, the corresponding block handle 
 *		will be returned.  If necessary, the a new active block will be
 *		allocated and filled.  Otherwise, an error code will be 
 *		returned.
 *
 *	Inputs:
 *		usObjectIndex - OBJECT_INDEX of the block to be opened.
 *
 *	Outputs:
 *		block_handle - the corresponding block handle of the
 *					   opened block.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		Return values from cr_find_block function.
 *		Return values from ct_block_fill function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *		Kent Anderson
 *
 **********************************************************************/

int CConnectionMgr::ct_block_open(const wchar_t* sDeviceName, OBJECT_INDEX usObjectIndex, DD_DEVICE_ID *dd_device_id, BLOCK_HANDLE *block_handle)
{
	ASSERT_RET(block_handle, CM_BAD_POINTER);
	 
	/*
	 *	Search the Active Block Table for the tag.
	 */

	*block_handle = ct_block_search(usObjectIndex);

	/*
	 *	Check if the the block is already in use.
	 */

	if (*block_handle >= 0) {

		/*
		 *	Increment the block's usage and return.
		 */
		int iUsage;
		get_abt_usage(*block_handle, &iUsage);
		set_abt_usage(*block_handle, iUsage + 1);
		return(CM_SUCCESS);
	}

	//
	// Fill in the FIND_CNF data struct.
	//
	FIND_CNF find_cnf = {0};

	fill_find_cnf( &find_cnf, sDeviceName, dd_device_id );

	//
	// Fill the block table entry
	//
	int r_code = ct_block_fill(usObjectIndex, &find_cnf, block_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ct_block_close
 *
 *	ShortDesc:  Close a previously opened session with a block.
 *
 *	Description:
 *		The ct_block_close function takes a block handle and closes
 *		a previously opened session with a block.  The function checks
 *		for the corresponding Active Block, Device, and Device Type
 *		elements that are no longer used, and deallocates them.
 *
 *	Inputs:
 *		block_handle - handle of the block to be closed.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_BAD_BLOCK_HANDLE.
 *		CM_BAD_BLOCK_CLOSE.
 *		Return values from rod_close function.
 *		Return values from cr_terminate_comm function.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CConnectionMgr::ct_block_close(ENV_INFO *env_info, BLOCK_HANDLE block_handle)
{

	bool	block_deallocate = false;
	bool	device_deallocate = false;
	bool	device_type_deallocate = false;

	ASSERT_RET(valid_block_handle(block_handle), CM_BAD_BLOCK_HANDLE) ;

	/*
	 *	Check to make sure that the block handle is valid, and
	 *	get the device handle and device type handle.
	 */

	if (!valid_block_handle(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}
	DEVICE_HANDLE device_handle = ABT_ADT_OFFSET(block_handle);
	DEVICE_TYPE_HANDLE device_type_handle = ADT_ADTT_OFFSET(device_handle);

	/*
	 *	Check if the block has multiple sessions.
	 */
	int iUsage;
	get_abt_usage(block_handle, &iUsage);
	if (iUsage > 1) {

		/*
		 *	Decrement the block's usage and return.
		 */
		int usage;
		get_abt_usage(block_handle, &usage);
		set_abt_usage(block_handle,usage - 1);
		return(CM_SUCCESS);

	} else {

		/*
		 *	Set the flag to deallocate the block.
		 */

		block_deallocate = true;

		/*
		 *	Check if the device has multiple sessions.
		 */

		if (ADT_USAGE(device_handle) > 1) {

			/*
			 *	Decrement the device's usage.
			 */
			set_adt_usage(device_handle, ADT_USAGE(device_handle) - 1);

		} else {

			/*
			 *	Set the flag to deallocate the device.
			 */

			device_deallocate = true;

			/*
			 *	Check if the device type has multiple sessions.
			 */

			if (g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) > 1) {

				/*
				 *	Decrement the device type's usage.
				 */
				g_DeviceTypeMgr.set_adtt_usage(device_type_handle,
						g_DeviceTypeMgr.ADTT_USAGE(device_type_handle) - 1);

			} else {

				/*
				 *	Set the flag to deallocate the device type.
				 */

				 device_type_deallocate = true;
			}
		}
	}

	/*
	 *	Check if the block is to be deallocated.
	 */

	if (block_deallocate)
	{
		ct_free_block(env_info, block_handle);
	}

	/*
	 *	Check if the device is to be deallocated.
	 */

	if (device_deallocate)
	{
		ct_free_device(env_info, device_handle);
	}

	/*
	 *	Check if the device type is to be deallocated.
	 */

	if (device_type_deallocate)
	{
		g_DeviceTypeMgr.ct_free_device_type(env_info, device_type_handle);
	}

	return(CM_SUCCESS);
}
