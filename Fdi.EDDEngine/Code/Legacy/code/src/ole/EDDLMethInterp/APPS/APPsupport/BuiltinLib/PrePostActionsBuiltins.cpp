#include "stdafx.h"
#include "ddbDevice.h"
#include "Interpreter.h"
#include "BuiltIn.h"
#include "MEE.h"
#include "RTN_CODE.H"
#include "NAN_DEF.h"
#include "../../../EDD_Engine_Mi/CProfiDPServerDevice.h"



/***** Start Pre and Post actions builtins *****/
//this is a not FDI builtin. remove it later when modifying FDIMethodInterpreterTests.ddl
int CBuiltIn::igetval(/*out*/__int64& llValue)
{
	nsConsumer::EVAL_VAR_VALUE value;
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_UNSIGNED:
		case nsEDDEngine::VT_INDEX:
		case nsEDDEngine::VT_ENUMERATED:
		case nsEDDEngine::VT_BIT_ENUMERATED:
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DATE_AND_TIME:
		case nsEDDEngine::VT_DURATION:
		case nsEDDEngine::VT_BOOLEAN:
			llValue = (INT64)value.val.u;
			break;
		case nsEDDEngine::VT_INTEGER:
		case nsEDDEngine::VT_EDD_DATE:
			llValue = (INT64)value.val.i;
			break;
		case nsEDDEngine::VT_TIME_VALUE:
			if (value.size <= 4)
			{
				llValue = (INT64)value.val.u;
			}
			else
			{
				llValue = (INT64)value.val.i;
			}
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	return nRetVal;// single point return
}

long CBuiltIn::put_unsigned(UINT32 uiValue)
{
	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_UNSIGNED:
		case nsEDDEngine::VT_ENUMERATED:
		case nsEDDEngine::VT_BIT_ENUMERATED:
		case nsEDDEngine::VT_INDEX:
		case nsEDDEngine::VT_BOOLEAN:
			value.val.u = (UINT32) uiValue;
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}

		if (nRetVal == BLTIN_SUCCESS)
		{
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	int retVal = ConvertToBLTINCode(nRetVal);
	return (retVal);
}

int CBuiltIn::fgetval(/*out*/double& dValue)
{
	nsConsumer::EVAL_VAR_VALUE value;
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_FLOAT:
			dValue = (double)value.val.f;
			break;
		case nsEDDEngine::VT_DOUBLE:
			dValue = value.val.d;
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	return(nRetVal);
}

int CBuiltIn::isetval(/*in, out*/long long& llValue)
{
	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_UNSIGNED:
		case nsEDDEngine::VT_INDEX:
		case nsEDDEngine::VT_ENUMERATED:
		case nsEDDEngine::VT_BIT_ENUMERATED:
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DATE_AND_TIME:
		case nsEDDEngine::VT_DURATION:
		case nsEDDEngine::VT_BOOLEAN:
			value.val.u = (unsigned long long) llValue;
			break;
		case nsEDDEngine::VT_INTEGER:
		case nsEDDEngine::VT_EDD_DATE:
			value.val.i = llValue;
			break;
		case nsEDDEngine::VT_TIME_VALUE:
			if (value.size <= 4)
			{
				value.val.u = (unsigned long long) llValue;
			}
			else
			{
				value.val.i = llValue;
			}
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}
	
		if (nRetVal == BLTIN_SUCCESS)
		{
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	return nRetVal;
}

int CBuiltIn::fsetval(/*in, out*/double& dValue)
{
	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_FLOAT:
			value.val.f = (float)dValue;
			break;
		case nsEDDEngine::VT_DOUBLE:
			value.val.d = dValue;
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}
	
		if (nRetVal == BLTIN_SUCCESS)
		{
			//write action value
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	return nRetVal;
}

int CBuiltIn::lsetval(/*in, out*/int& lValue)
{
	long long llValue = lValue;
	int iRetVal = isetval(llValue);
	lValue = (int)llValue;
	return iRetVal;
}

//This function is used by FF profile
int CBuiltIn::put_double(double dValue)
{
	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_FLOAT:
			value.val.f = (float)dValue;
			break;
		case nsEDDEngine::VT_DOUBLE:
			value.val.d = dValue;
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}

		if (nRetVal == BLTIN_SUCCESS)
		{
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	int retVal = ConvertToBLTINCode(nRetVal);
	return (retVal);
}

//This function is used by FF profile
long CBuiltIn::put_signed(long lValue)
{
	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_INTEGER:
			value.val.i = lValue;
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}
	
		if (nRetVal == BLTIN_SUCCESS)
		{
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	int retVal = ConvertToBLTINCode(nRetVal);
	return (retVal);
}

//Added By Anil July 01 2005 --starts here
int CBuiltIn::discard_on_exit()
{
	m_pMeth->discard_on_exit(); 
	return BI_SUCCESS;
}

//Added By Anil July 01 2005 --Ends here

/* the floating point processor/library does not support a HART NAN - copde it into an int */
UINT CBuiltIn::NaN_value()
{
	/* WRONG stevev 11jul05  return(SINGLE_QUIET_NAN_VALUE);  */
	return (HART_NAN);
}

	
int CBuiltIn::get_action_string(/*in*/FunctionExpression* pFuncExp, /*in*/int length,
								/*out*/INTER_VARIANT* pivString)
{
	int iReturnValue = BLTIN_NOT_IMPLEMENTED;

	if (m_pDevice)
	{
		//get action variable value, type and size
		nsConsumer::EVAL_VAR_VALUE value;
		iReturnValue = m_pDevice->devAccessActionVar(NULL, &value, READ);

		if (iReturnValue == BLTIN_SUCCESS)
		{
			//check if the variable is BITSTRING
			switch (value.type)
			{
			case nsEDDEngine::VT_BITSTRING:
			case nsEDDEngine::VT_OCTETSTRING:
				{
					//get value
					const uchar* str = value.val.b.ptr();
					if (value.val.b.length() < (unsigned int)length)
					{
						length = (int)value.val.b.length();
					}

					//output the value
					BYTE_STRING( bitStr, length );
					memcpy(bitStr.bs, str, length);

					if ( ! SetByteStringParam(pFuncExp, pivString, 0, bitStr) )
					{
						DEL_BYTE_STR( bitStr );
						return BLTIN_WRONG_DATA_TYPE;	//wrong parameter type
					}

					DEL_BYTE_STR( bitStr );
				}
				break;
			case nsEDDEngine::VT_ASCII:
			case nsEDDEngine::VT_PACKED_ASCII:
			case nsEDDEngine::VT_PASSWORD:
			case nsEDDEngine::VT_EUC:
			case nsEDDEngine::VT_VISIBLESTRING:
				{
					//get value
				    char pchString[MAX_DD_STRING]={0};
					const wchar_t* str = value.val.s.c_str();
					size_t convertedChars = 0;
					iReturnValue = PS_Wcstombs(&convertedChars, pchString, length, str, _TRUNCATE);	//allow NULL termination character
					if (iReturnValue == BLTIN_SUCCESS)
					{
						if ((int)strlen(pchString) < length)
						{
							length = (int)strlen(pchString);
						}

						//output the value
						if ( ! SetCharStringParam(pFuncExp, pivString, 0, pchString, length) )
						{
							return BLTIN_WRONG_DATA_TYPE;	//wrong parameter type
						}
					}
				}
				break;
			default:
				iReturnValue = BLTIN_DDS_ERROR;
				break;
			}
		}
	}

	return iReturnValue;
}

// stevev 27dec07 - only wide strings are available from the methods
//					BUILTIN_ssetval in BuiltinInvoke converts to string
//					only normal ascii may be set to a dd variable 
int CBuiltIn::put_action_string(/*in*/INTER_VARIANT* pivValue,
								/*in*/ int length,
								/*out*/ wchar_t* outString)
{
	int iReturnValue = BLTIN_NOT_IMPLEMENTED;

	if (m_pDevice)
	{
		//get action variable type and size first
		nsConsumer::EVAL_VAR_VALUE value;
		iReturnValue = m_pDevice->devAccessActionVar(NULL, &value, READ);

		//check if the variable is BITSTRING
		if (iReturnValue == BLTIN_SUCCESS)
		{
			switch (value.type)
			{
			case nsEDDEngine::VT_BITSTRING:
			case nsEDDEngine::VT_OCTETSTRING:
				{
					//get input value
					unsigned char pchInputString[MAX_DD_STRING]={0};
					if ( ! GetUnsignedCharArrayParam(pchInputString, length, pivValue, 0) )
					{
						return BLTIN_WRONG_DATA_TYPE;	//wrong parameter type
					}

					value.val.b.assign((const uchar*)pchInputString, value.size);
					value.size = value.val.b.length();

					//write the value
					iReturnValue = m_pDevice->devAccessActionVar(NULL, &value, WRITE);

					//output string
					if (outString)
					{
						if (length > (int)value.size)
						{
							length = (int)value.size;
						}
						//Convert char* string to a wchar_t* string
						size_t convertedChars = 0;
						PS_Mbtowcs(&convertedChars, outString, length, (char *)pchInputString, _TRUNCATE);
					}
				}
				break;
			case nsEDDEngine::VT_ASCII:
			case nsEDDEngine::VT_PACKED_ASCII:
			case nsEDDEngine::VT_PASSWORD:
			case nsEDDEngine::VT_EUC:
			case nsEDDEngine::VT_VISIBLESTRING:
				{
					//get input value
					char pchInputString[MAX_DD_STRING]={0};
					if ( ! GetCharStringParam(pchInputString, length, pivValue, 0) )
					{
						return BLTIN_WRONG_DATA_TYPE;	//wrong parameter type
					}
					pchInputString[length] = '\0';

					//Convert char* string to a wchar_t* string
					size_t convertedChars = 0;
					size_t sourceStrLength = strlen(pchInputString) + 1;
					wchar_t *wchString = new wchar_t[sourceStrLength];
					PS_Mbtowcs(&convertedChars, wchString, sourceStrLength, pchInputString, _TRUNCATE);
						
					value.val.s.assign((const wchar_t*)wchString, true);
					value.size = wcslen(wchString);

					//write the value
					iReturnValue = m_pDevice->devAccessActionVar(NULL, &value, WRITE);

					delete [] wchString;

					//output string
					if (outString)
					{
						if (length > (int)value.size)
						{
							length = (int)value.size;
						}
						PS_Wcsncpy(outString, (length+1), value.val.s.c_str(), _TRUNCATE);
					}
				}
				break;
			default:
				iReturnValue = BLTIN_DDS_ERROR;
				break;
			}
		}
	}

	return iReturnValue;
}


long CBuiltIn::get_double(double *pdVal)
{
	nsConsumer::EVAL_VAR_VALUE value;
	int iRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (iRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_FLOAT:
			*pdVal = (double)value.val.f;
			break;
		case nsEDDEngine::VT_DOUBLE:
			*pdVal = value.val.d;
			break;
		default:
			*pdVal = 0.0;
			iRetVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	int retVal = ConvertToBLTINCode(iRetVal);
	return (retVal);
}


long CBuiltIn::get_date(unsigned char *dateBuf, long *len)
{
	unsigned long long ullDate = 0;
	nsConsumer::EVAL_VAR_VALUE value;

	long retVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (retVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DATE_AND_TIME:
		case nsEDDEngine::VT_DURATION:
			ullDate = value.val.u;
			break;
		case nsEDDEngine::VT_TIME_VALUE:
			ullDate = value.val.i;			//FF TIME_VALUE(4) is not supported by spec.
			break;
		default:
			retVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	convertUllTo8Char(ullDate, dateBuf, *len);

	int iRetVal = ConvertToBLTINCode(retVal);
	return(iRetVal);
}

long CBuiltIn::get_signed(long* lval)
{
	*lval = 0;		//initial value
	nsConsumer::EVAL_VAR_VALUE value;

	int iRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (iRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_INTEGER:
			*lval = (long)value.val.i;
			break;
		default:
			iRetVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	int retVal = ConvertToBLTINCode(iRetVal);
	return retVal;
}

long CBuiltIn::get_unsigned(unsigned long* ulval)
{
	*ulval = 0;		//initial value
	nsConsumer::EVAL_VAR_VALUE value;

	int iRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (iRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_UNSIGNED:
		case nsEDDEngine::VT_ENUMERATED:
		case nsEDDEngine::VT_BIT_ENUMERATED:
		case nsEDDEngine::VT_INDEX:
		case nsEDDEngine::VT_BOOLEAN:
			*ulval = (unsigned long)value.val.u;
			break;
		default:
			iRetVal = BLTIN_DDS_ERROR;
			break;
		}
	}

	int retVal = ConvertToBLTINCode(iRetVal);
	return retVal;
}

int CBuiltIn::put_date(const unsigned char* dateBuf, long size)
{
	unsigned long long ullDate = 0;

	convert8CharToUll (dateBuf, size, ullDate);

	nsConsumer::EVAL_VAR_VALUE value;

	//get action variable type and size first
	int nRetVal = m_pDevice->devAccessActionVar(NULL, &value, READ);
	if (nRetVal == BLTIN_SUCCESS)
	{
		switch(value.type)
		{
		case nsEDDEngine::VT_TIME:
		case nsEDDEngine::VT_DATE_AND_TIME:
		case nsEDDEngine::VT_DURATION:
			value.val.u = ullDate;
			break;
		case nsEDDEngine::VT_TIME_VALUE:
			value.val.i = ullDate;			//FF TIME_VALUE(4) is not supported by spec.
			break;
		default:
			nRetVal = BLTIN_DDS_ERROR;
			break;
		}

		if (nRetVal == BLTIN_SUCCESS)
		{
			nRetVal = m_pDevice->devAccessActionVar(NULL, &value, WRITE);
		}
	}

	int retVal = ConvertToBLTINCode(nRetVal);
	return (retVal);
}
/*End of code*/	

/* End Pre and Post actions builtins */


