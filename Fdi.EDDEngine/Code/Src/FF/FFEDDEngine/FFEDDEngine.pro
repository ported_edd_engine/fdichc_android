#-------------------------------------------------
#
# Project created by QtCreator 2017-11-24T13:08:52
#
#-------------------------------------------------

QT       -= gui

TARGET = FFEDDEngine
TEMPLATE = lib
CONFIG += shared_and_static build_all

DEFINES += FFEDDENGINE_LIBRARY \
           UNICODE

QMAKE_LFLAGS += -Wl,--version-script=$$PWD/export_ff
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKE_CXXFLAGS += -std=gnu++11

SOURCES += globals.cpp \
    LegacyFF.cpp \
    LegacyFFAttrs.cpp \
    LegacyFFFlats.cpp \
    LegacyFFTables.cpp \
    stdafx.cpp

HEADERS += ../../../Inc/FF/FFEDDEngine/DDSSupport.h\
    ../../../Inc/FF/FFEDDEngine/LegacyFFFactory.h\
    LegacyFF.h \
    LegacyFFFlats.h \
    resource.h \
    stdafx.h \
    targetver.h

unix {
    target.path = /usr/lib/
    INSTALLS += target
}

CONFIG(debug, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/debug
    BUILD_DIR = $$PWD/debug
}
CONFIG(release, debug|release) {
    DESTDIR = ../../../Legacy/code/bin/release
    BUILD_DIR = $$PWD/release
}
include($$DESTDIR/../../../../Src/SuppressWarning.pri)

OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.rcc
UI_DIR = $$BUILD_DIR/.u

INCLUDEPATH += $$PWD/../../../Legacy/code/inc/FF/AMSDds
INCLUDEPATH += $$PWD/../../../Legacy/code/inc/FF/FFHelper
INCLUDEPATH += $$PWD/../../../Legacy/code/inc/FF/AmsGenFiles
INCLUDEPATH += $$PWD/../../../Legacy/code/inc/FF
INCLUDEPATH += $$PWD/../../../Legacy/code/inc
INCLUDEPATH += $$PWD/../../../Inc/FF/FFEDDEngine
INCLUDEPATH += $$PWD/../../../Inc/$$"EDD Engine"
INCLUDEPATH += $$PWD/../../../Interfaces/EDD_Engine_Interfaces
INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Common

android {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Android
    LIBS += -L$$DESTDIR/../../lib/ -Wl,--whole-archive -lEDD_Engine_Android -Wl,--no-whole-archive
}
else {
    INCLUDEPATH += $$PWD/../../../Src/EDD_Engine_Linux
    unix:!macx: LIBS += -L$$DESTDIR/../../lib/ -lEDD_Engine_Linux
}

unix:!macx: LIBS += -L$$DESTDIR/../../lib/ -Wl,--whole-archive -lEDD_Engine_Common -Wl,--no-whole-archive

android {
    LIBS += -L$$DESTDIR/../../lib/ -Wl,--whole-archive -lAMSDds -Wl,--no-whole-archive
    LIBS += -L$$DESTDIR/../../lib/ -Wl,--whole-archive -lFFHelper -Wl,--no-whole-archive
}
else {
    unix:!macx: LIBS += -L$$DESTDIR/../../lib/ -lAMSDds
    unix:!macx: LIBS += -L$$DESTDIR/../../lib/ -lFFHelper
}

unix:!macx: LIBS += -L$$DESTDIR/  -lEDD_Engine_Interfaces


INCLUDEPATH += $$DESTDIR/../../lib
DEPENDPATH += $$DESTDIR/../../lib
INCLUDEPATH += $$DESTDIR
DEPENDPATH += $$DESTDIR
unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libEDD_Engine_Interfaces.a

#contains(ANDROID_TARGET_ARCH,x86) {
#    ANDROID_EXTRA_LIBS =
#}
#LIBS += -landroid
