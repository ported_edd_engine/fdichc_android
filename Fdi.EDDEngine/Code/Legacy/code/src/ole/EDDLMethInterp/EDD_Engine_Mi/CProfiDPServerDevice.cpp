#include "stdafx.h"
#include <map>
#include "CProfiDPServerDevice.h"
#include "MEE.h"
#include "MethodThread.h"
#include "ServerDictionary.h"
#include "BI_CODES.H"
#include "ServerProjects/CommonToolkitStrings.h"


CProfibusDPServerDevice::CProfibusDPServerDevice(  DevInfcHandle_t h, long lMethodID, const wchar_t* sLanguageCode, CMethodThread *pMethodThread ) : CMethodInterpreterDevice(h)
{
	m_nMethodItemID = lMethodID;
	m_pMethodThread = pMethodThread;

	m_bSearchedForAutoSaveValues = false;

	device_sLanguageCode = sLanguageCode;
}

//
// Destructor
//
CProfibusDPServerDevice::~CProfibusDPServerDevice()
{
	
	ClearItemMapping();

	if( dictionary )
	{
		delete dictionary;
		dictionary = NULL;
	}
}

//
// This method does a lot of up-front work.
//
HRESULT CProfibusDPServerDevice::Populate()
{
	USES_CONVERSION;
	HRESULT hRetVal = S_OK;

	//create the dictionary
	dictionary = new CServerDictionary(this);

	return hRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadMethodData( long lItemId, CStdString sValue, CValueVarient *pOutput)
{
	bool bRetVal = false;

	try
	{
		if ( m_pMethodThread )
		{
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemId, 0, sValue, pOutput ) == BLTIN_SUCCESS)
			{
				bRetVal = true;
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
//
//
RETURNCODE  CProfibusDPServerDevice::ReadParameterData2( CValueVarient *pOutput, CStdString sValue, unsigned long ulBlkId, unsigned long ulBlkNum, unsigned long ulItemId, unsigned long ulMemberId )
{
	RETURNCODE iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		if( m_pMethodThread )
		{
			iRetVal = m_pMethodThread->GetItemByItemId2( ulBlkId, ulBlkNum, ulItemId, ulMemberId, sValue, pOutput );
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return iRetVal;
}

RETURNCODE CProfibusDPServerDevice::ReadParameterActionList( /*in*/ unsigned long ulBlkId, /*in*/ unsigned long ulBlkNum, /*in*/ unsigned long ulItemId, /*out*/ nsEDDEngine::ACTION_LIST *pOutput)
{
	RETURNCODE iRetVal = BLTIN_NOT_IMPLEMENTED;

	try
	{
		if (m_pMethodThread)
		{
			iRetVal = m_pMethodThread->GetItemActionList(ulBlkId, ulBlkNum, ulItemId, pOutput);
		}
	}
	catch (_INT32 error)
	{
		throw error;
	}
	return iRetVal;
}

bool CProfibusDPServerDevice::ReadValueArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput)
{
	bool bRetVal = false;

	try
	{
		if( m_pMethodThread )
		{
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemId, 0, sValue, pOutput ) == BLTIN_SUCCESS)
			{
				bRetVal = true;
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadItemArrayData( long lItemId, CStdString sValue, CValueVarient *pOutput)
{
	bool bRetVal = false;

	try
	{
		if( m_pMethodThread )
		{
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemId, 0, sValue, pOutput ) == BLTIN_SUCCESS)
			{
				bRetVal = true;
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadCollectionData( long lItemId, CStdString sValue, CValueVarient *pOutput)
{
	bool bRetVal = false;

	try
	{
		if( m_pMethodThread )
		{
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemId, 0, sValue, pOutput ) == BLTIN_SUCCESS)
			{
				bRetVal = true;
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadFileData( long lItemId, CStdString sValue, CValueVarient *pOutput)
{
	bool bRetVal = false;

	try
	{
		if( m_pMethodThread )
		{
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemId, 0, sValue, pOutput ) == BLTIN_SUCCESS)
			{
				bRetVal = true;
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadData( CStdString sValue, CValueVarient* /*pOutput*/)
{
	bool bRetVal = false;
	return bRetVal;
}

//
//
//
bool CProfibusDPServerDevice::ReadEnumerationData( unsigned long ulItemId, unsigned long ulMemberId, hCenumList& returnList )
{
	bool bRetVal = false;
	CValueVarient vtOutput{};
	if(m_pMethodThread)
	{	
		CStdString sMoniker;

		sMoniker.Format(_T("Member!$%lu!ENUMASXML"), ulMemberId);

		if (m_pMethodThread->GetItemByItemId2( 0, 0, ulItemId, ulMemberId, sMoniker, &vtOutput ) == BLTIN_SUCCESS)
		{
			
			bRetVal = returnList.BuildFromEnumXML(CV_WSTR(&vtOutput));
		}

	}
	return bRetVal;
}

//
//
//Function commitChangedValues() delivers changed parameter activity on method exit to builtin support
void CProfibusDPServerDevice::commitChangedValues(nsEDDEngine::ChangedParamActivity onExitActivity)
{
	if( m_pMethodThread )
	{
		try
		{
			m_pMethodThread->CommitValues(onExitActivity);
		}
		catch(_INT32 error)
		{
			throw error;
		}
	}
}

//
//  Get the name and kind associated with an itemid
//
bool CProfibusDPServerDevice::AddMapBasedOnItemID( long lItemID )
{
	bool bRetVal = false;
	
	try
	{
		if( m_pMethodThread )
		{
			CStdString sName;
			long lKind = 0;
			CValueVarient vtOutput{};
			if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemID, 0, _T("Name"), &vtOutput ) == BLTIN_SUCCESS)
			{
				
				sName = CV_WSTR(&vtOutput);

				vtOutput.clear();
				if (m_pMethodThread->GetItemByItemId2( 0, 0, lItemID, 0, _T("Kind"), &vtOutput ) == BLTIN_SUCCESS)
				{
					
					lKind = CV_I4(&vtOutput);
					
					// Add the map
					if( lKind && lItemID )
					{
						m_aItemIdMapping.push_back( new CItemMapping( lItemID, GetItemType(lKind), sName ) );	//0 means no element or member handling here
						bRetVal = true;
					}
				}
			}
		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}

//
// get the itemid and kind associated with an item name.
//
bool CProfibusDPServerDevice::AddMapBasedOnName( CStdString sName )
{
	bool bRetVal = false;

	try
	{
		if( m_pMethodThread )
		{
			unsigned long ulItemID = 0;
			int iRetVal = m_pMethodThread->GetItemIdByName(sName, &ulItemID);

			if (iRetVal == BLTIN_SUCCESS)
			{
				CValueVarient vtOutput{};
				if (m_pMethodThread->GetItemByItemId2( 0, 0, ulItemID, 0, _T("Kind"), &vtOutput ) == BLTIN_SUCCESS)
				{
					
					long lKind = CV_I4(&vtOutput);
					
					// Add the map
					if( lKind && ulItemID )
					{
						m_aItemIdMapping.push_back( new CItemMapping( ulItemID, GetItemType(lKind), sName ) );
						bRetVal = true;
					}

				}
			}

		}
	}
	catch(_INT32 error)
	{
		throw error;
	}
	return bRetVal;
}


RETURNCODE CProfibusDPServerDevice::getItemBySymName ( string& symName, hCitemBase** ppReturnedItempointer )
{
	RETURNCODE nRetVal = BLTIN_NOT_IMPLEMENTED;
 	nRetVal = CMethodInterpreterDevice::getItemBySymName( symName, ppReturnedItempointer );
	return nRetVal;
}


//This routine is used for communication builtins
RETURNCODE CProfibusDPServerDevice::sendCommMethodCmd( int commandNumber, int transactionNumber, uchar* pucCmdStatus, uchar* pchMoreDataInfo, int& iMoreDataSize )
{

	//CJK was here  9/3/2009 WS did this
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( commandNumber == 0xFFFF )//a command# of 0xFFFF means there is NO command, this is used for local params
	{
		nRetVal = BI_SUCCESS;//this is not an error, we have successfully done nothing.
	}
	else
	{
		if( m_pMethodThread )
		{
			nRetVal = m_pMethodThread->SendCommCmdExecute( commandNumber, transactionNumber, pucCmdStatus, pchMoreDataInfo, iMoreDataSize );
			if (iMoreDataSize > MAX_XMTR_STATUS_LEN)
			{
				nRetVal = BLTIN_BUFFER_TOO_SMALL;
			}
		}
	}

	return nRetVal;
}

//This routine is used for Retrieving attributes that are dynamic in nature at the UI level.
RETURNCODE CProfibusDPServerDevice::AccessDynamicAttributeMethod(const PARAM_REF *pOpRef, CValueVarient* pParamValue, const varAttrType_t ulAttribute, AccessType eAccessType)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->threadAccessDynamicAttributeExecute(pOpRef, pParamValue, ulAttribute, eAccessType);
	}

	return nRetVal;
}


//This routine is used for list deletion/insertion builtins
RETURNCODE CProfibusDPServerDevice::sendListOpMethod(unsigned long ulListId, int iListIndex, unsigned long ulEmbListId, int iIndex, unsigned long ulItemId, int iCount)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->SendListOpExecute(ulListId, iListIndex, ulEmbListId, iIndex, ulItemId, iCount);
	}

	return nRetVal;
}

//This routine is used for UI builtins
RETURNCODE CProfibusDPServerDevice::sendUIMethod(ACTION_UI_DATA& stUIData)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->SendUIExecute(stUIData);
	}

	return nRetVal;
}

//This routine is used for UI builtins
RETURNCODE CProfibusDPServerDevice::receiveUIMethodRsp(CValueVarient& vtRsp)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->ReceiveUIMethRspExecute(vtRsp);
	}

	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::get_rspcode_string( int nCommandNumber, int nResponseCode, tchar* pchResponseString, int nLength, nsEDDEngine::ProtocolType protocol )
{
	RETURNCODE nRetVal = BI_COMM_ERR;
	pchResponseString[0] = L'\0';

	if( m_pMethodThread )
	{
		CValueVarient vReponseString{}; 
		nRetVal = m_pMethodThread->GetRspCodeStrExecute(nCommandNumber, nResponseCode, &vReponseString, protocol);
		if ((nRetVal == BI_SUCCESS))
		{
			if (vReponseString.vTagType == ValueTagType::CVT_WSTR)
			{
				PS_Wcsncpy(pchResponseString, nLength, CV_WSTR(&vReponseString).c_str(), wcslen(CV_WSTR(&vReponseString).c_str()));				
			}
			else
			{
				nRetVal = BI_ERROR;
			}
		}
	}
	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::getDictionaryString(unsigned long ulIndex, wchar_t* pString, int iStringLen)
{
	RETURNCODE nRetVal = BI_COMM_ERR;
	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->GetDictStringExecute(ulIndex, pString, iStringLen);
	}
	return nRetVal;
}

int CProfibusDPServerDevice::ReadLiteralString( unsigned long ulStringID, wchar_t* sValue, long lStrLen )
{
	RETURNCODE nRetVal = BI_COMM_ERR;
	if( m_pMethodThread )
	{
		try
		{
			nRetVal = m_pMethodThread->GetLiteralString(ulStringID, sValue, lStrLen);
		}
		catch(_INT32 error)
		{
			throw error;
		}
	}
	return nRetVal;
}

//
// This kicks off the execution of the method.
//
// If returned value is greater than or equal to 0, this routine execution is succeeded. see function ExecuteMethodFunction() in AMSMethodFunction.cpp
INTERPRETER_STATUS CProfibusDPServerDevice::ExecuteTheMethod()
{
	INTERPRETER_STATUS isRetVal = INTERPRETER_STATUS_INVALID;

	if( pMEE )
	{
		//propagate protocol type to MEE instance for method execution. The protocol type will be used to distinguish proper CBuiltInInfo[]
		pMEE->SetStartedProtocol(m_pMethodThread->GetProtocolType());

		//initialize changed value behavior on method exit
		if (pMEE->GetStartedProtocol() == nsEDDEngine::PROFIBUS)
		{
			pMEE->m_eOnExitActivity = nsEDDEngine::Save;
		}
		else
		{
			pMEE->m_eOnExitActivity = nsEDDEngine::Discard;
		}

		isRetVal = pMEE->ExecuteMethod( this, m_nMethodItemID );

		if( isRetVal != INTERPRETER_STATUS_OK )
		{
			pMEE->m_eOnExitActivity = nsEDDEngine::Discard;
		}

		commitChangedValues(pMEE->m_eOnExitActivity);
	}

	return isRetVal;
}

//
// Check if the device has been cancelled
//
bool CProfibusDPServerDevice::IsDeviceCancelled()
{
	return m_pMethodThread->IsThreadCancelled();
}

void CProfibusDPServerDevice::ClearDeviceCancel()
{
	m_pMethodThread->SetCancel(false);
}


int CProfibusDPServerDevice::ReadDictionaryString(wchar_t *sDictionaryKey, wchar_t *sValue, int iStringLen )
{
	int hRetVal = BI_COMM_ERR;
	if( m_pMethodThread )
	{
		hRetVal = m_pMethodThread->GetDictionaryString(sDictionaryKey, sValue, iStringLen );
	}
	return hRetVal;
}

RETURNCODE CProfibusDPServerDevice::getResponseCodeString(int iItemId, int iMemberId, int iResponseCode, tchar *pchResponseCodeString, int iResponseCodeStringLength)
{
	RETURNCODE nRetVal = BI_COMM_ERR;
	
	if( m_pMethodThread )
	{
		CValueVarient vReponseString{}; 
		nRetVal = m_pMethodThread->GetResponseCodeStringExecute(iItemId, iMemberId, iResponseCode, &vReponseString);
		if ((nRetVal == BLTIN_SUCCESS) && (vReponseString.vTagType == ValueTagType::CVT_WSTR))
		{
			wcsncpy(pchResponseCodeString, CV_WSTR(&vReponseString).c_str(), iResponseCodeStringLength);
			if (wcslen(CV_WSTR(&vReponseString).c_str()) > (unsigned int)iResponseCodeStringLength)
			{
				pchResponseCodeString[iResponseCodeStringLength] = L'\0';
			}
			
		}
	}
	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::getDdsError(tchar *pchErrorString, int iMaxLength)
{
	RETURNCODE nRetVal = BI_COMM_ERR;
	
	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getDdsErrorExecute( pchErrorString );
		
		int strLen = (int)wcslen(pchErrorString);

		if((strLen > 0) && (iMaxLength > 0))
		{
			// Truncate the output string to the given max length.
			if (strLen <= iMaxLength)
			{
				pchErrorString[strLen] = _T('\0');
			}
			else
			{
				pchErrorString[iMaxLength] = _T('\0');
			}
		}
		else
		{
			pchErrorString[0] = _T('\0');
		}
		
	}

	return nRetVal;
}

unsigned long CProfibusDPServerDevice::getCommError()
{
	unsigned long nRetVal = (unsigned long)BLTIN_CANNOT_READ_VARIABLE;
	
	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getCommErrorExecute( );
	}

	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::sendParamValues(const PARAM_REF *pOpRef, AccessType iAccessType){

	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->SendValueExecute(pOpRef, iAccessType);
	}

	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::devAccessAllDeviceValues(nsEDDEngine::ChangedParamActivity eAccessType)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->threadAccessAllDeviceValues(eAccessType);
	}

	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::getResponseCode(unsigned long *pulResponseCode, unsigned long *pulErrorItemId, unsigned long *pulErrorMemberId)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getResponseCodeExecute(pulResponseCode, pulErrorItemId, pulErrorMemberId);
	}

	return nRetVal;
}

RETURNCODE CProfibusDPServerDevice::assignValue(const PARAM_REF *pdst_opRef, const PARAM_REF *psrc_opRef)
{
	RETURNCODE iRetVal = BI_COMM_ERR;

	if(m_pMethodThread != NULL)
	{
		iRetVal = m_pMethodThread->assignExecute(pdst_opRef, psrc_opRef);
	}

	return iRetVal;
}

/* This function generally returns an item ID of a block, list or record member.
 * When argument ulItemId is non-zero, this function is used for record. The argument ulItemId is the record item ID.
 * When argument ulItemId is zero, this function is used for block or list.
 */
ITEM_ID CProfibusDPServerDevice::ResolveBlockRef(ITEM_ID ulBlockItemId, unsigned long iOccurrence, ITEM_ID ulMemberId, ResolveType eResolveType)
{
	unsigned long ulRetVal = 0;

	if( m_pMethodThread )
	{
		ulRetVal = m_pMethodThread->resolveBlockRefExecute(ulBlockItemId, iOccurrence, ulMemberId, eResolveType);
	}

	return ulRetVal;
}

RETURNCODE CProfibusDPServerDevice::devAccessTypeValue2(const PARAM_REF *pOpRef, CValueVarient *value, AccessType eAccessType)
{
	RETURNCODE iRetVal = BI_COMM_ERR;

	if(m_pMethodThread != NULL)
	{
		try
		{
			//find the variable item ID and member ID
			ITEM_ID ulBlkId = pOpRef->ulBlockItemId;
			unsigned long ulBlkNum = pOpRef->ulOccurrence;
			ITEM_ID itemID = 0;
			ITEM_ID memberID = 0;

			if (pOpRef->op_ref_type == STANDARD_TYPE)
			{
				itemID = pOpRef->op_info.id;
				memberID = pOpRef->op_info.member;
			}
			else	//COMPLEX_TYPE
			{
				itemID = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].id;
				memberID = pOpRef->op_info_list.list[pOpRef->op_info_list.count - 1].member;
			}

			
			CStdString sProperty;

			
			variableType_t eType = vT_undefined;
			long lSize = 0;
			iRetVal = BLTIN_SUCCESS;
			if (eAccessType == WRITE)
			{
				//get variable type from DDS
				CValueVarient vtType{};
				sProperty.Empty();
				sProperty.Format(DCI_PROPERTY_TYPE);
				iRetVal = ReadParameterData2(&vtType, sProperty, ulBlkId, ulBlkNum, itemID, memberID);
				
				if (iRetVal == BLTIN_SUCCESS)
				{
					eType = (variableType_t)CV_INT(&vtType);

					//get variable size from DDS
					CValueVarient vtSize{};
					sProperty.Empty();
					sProperty.Format(DCI_PROPERTY_SIZE);
					iRetVal = ReadParameterData2(&vtSize, sProperty, ulBlkId, ulBlkNum, itemID, memberID);
					
					if (iRetVal == BLTIN_SUCCESS)
					{
						lSize = CV_INT(&vtSize);
					}
				}
			}
			if (iRetVal == BLTIN_SUCCESS)
			{
				iRetVal = m_pMethodThread->threadAccessTypeValue2(pOpRef, value, eType, eAccessType, lSize);
			}
			
		}
		catch(_INT32 error)
		{
			throw error;
		}
	}

	return iRetVal;
}

/* This function is to read/write action variable only in CMiEngine if input parameter pAction is NULL.
* This function is to read/write action and action variable in CMiEngine if input parameter pAction is not NULL.
*/
RETURNCODE CProfibusDPServerDevice::devAccessActionVar(/*in, out*/nsEDDEngine::ACTION *pAction, /*in, out*/nsConsumer::EVAL_VAR_VALUE *pActionVal, /*in*/AccessType eAccessType)
{
	RETURNCODE iRetVal = BI_COMM_ERR;

	if(m_pMethodThread != NULL)
	{
		iRetVal = m_pMethodThread->threadAccessActionVar(pAction, pActionVal, eAccessType);
	}

	return iRetVal;
}

RETURNCODE CProfibusDPServerDevice::devWriteMethRtnVar(/*in*/ nsConsumer::EVAL_VAR_VALUE *pReturnedVal)
{
	RETURNCODE iRetVal = BI_COMM_ERR;

	if(m_pMethodThread != NULL)
	{
		iRetVal = m_pMethodThread->threadWriteMethRtnVar(pReturnedVal);
	}

	return iRetVal;
}

long CProfibusDPServerDevice::getBlockInstanceCount(unsigned long ulBlockId, unsigned long *pulCount)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getBlockInstanceCountExecute(ulBlockId, pulCount );
	}

	return nRetVal;
}

long CProfibusDPServerDevice::getBlockByObjectIndex(unsigned long ulObjectIndex, unsigned long *pulRelativeIndex)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getBlockInstanceByObjIndex(ulObjectIndex, pulRelativeIndex );
	}

	return nRetVal;
}

long CProfibusDPServerDevice::getBlockByBlockTag(unsigned long ulBlockId, TCHAR *pchBlockTag, unsigned long *pulRelativeIndex)
{
	RETURNCODE nRetVal = BI_COMM_ERR;

	if( m_pMethodThread )
	{
		nRetVal = m_pMethodThread->getBlockInstanceByBlockTag( ulBlockId, pchBlockTag, pulRelativeIndex );
	}

	return nRetVal;
}

long CProfibusDPServerDevice::getListElem2 (unsigned long ulListId, int iIndex, unsigned long ulEmbListId, int iEmbListIndex, unsigned long ulElementId, unsigned long ulSubElementId, CValueVarient* data)
{
	int iRetVal = BI_COMM_ERR;

    if( m_pMethodThread )
    {
        iRetVal = m_pMethodThread->GetListElement2Execute(ulListId, iIndex, ulEmbListId, iEmbListIndex, ulElementId, ulSubElementId, data);
    }
    return iRetVal;
}

void CProfibusDPServerDevice::deviceLogMessage(int priorityVal, wchar_t *msg)
{
    if( m_pMethodThread )
    {
		m_pMethodThread->threadLogMessage(priorityVal, msg);
	}
}

void CProfibusDPServerDevice::deviceMethodDebugMessage(unsigned long lineNumber, const wchar_t* currentMethodDebugXml, const wchar_t** modifiedMethodDebugXml)
{
	if (m_pMethodThread)
	{
		m_pMethodThread->threadMethodDebugMessage(lineNumber, currentMethodDebugXml, modifiedMethodDebugXml);
	}
}

int CProfibusDPServerDevice::deviceIsOffline()
{
    if( m_pMethodThread )
    {
		return (m_pMethodThread->threadIsOffline());
	}
	else
	{
		return 1;	//offline
	}
}

long CProfibusDPServerDevice::deviceSendCommand(unsigned long commandId, long *commandError)
{
	long iRetVal = BI_COMM_ERR;

    if( m_pMethodThread )
    {
        iRetVal = m_pMethodThread->threadSendCommand(commandId, commandError);
    }

    return iRetVal;
}

RETURNCODE CProfibusDPServerDevice::deviceGetEnumVarString(unsigned long ulItemId, unsigned long ulEnumValue, CValueVarient *sEnumString)
{
	int iRetVal = BI_COMM_ERR;

    if( m_pMethodThread )
    {
        iRetVal = m_pMethodThread->threadGetEnumVarString(ulItemId, ulEnumValue, sEnumString);
    }

    return iRetVal;
}

void CProfibusDPServerDevice::MILog(wchar_t *sMessage, nsConsumer::LogSeverity eLogSeverity, wchar_t *sCategory, const char *cFilename,  UINT32 lineNumber)
{
	m_pMethodThread->threadMILog(sMessage, eLogSeverity, sCategory, cFilename, lineNumber);
}

int CProfibusDPServerDevice::GetMemberIdByName( CStdString sMemberName, unsigned long *pMemberId )
{
	int iRetVal = BI_COMM_ERR;

    if( m_pMethodThread )
    {
        iRetVal = m_pMethodThread->GetItemIdByName(sMemberName, pMemberId);
    }

    return iRetVal;
}
