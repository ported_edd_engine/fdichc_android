/******************************************************************************
 *  @(#) $Id: STDINC.H,v 1.1 2012/07/10 20:59:22 rgretta Exp $
 *
 *  Copyright 1994 Rosemount, Inc.- All rights reserved
 *
 *  Description : Include file for the method interpreter include files, that
 *                are used frequently, but are changed infrequently
 *
 ******************************************************************************/


#if !defined(AFX_STDAFX_H__BB9A0484_1FAF_11D1_AAE3_0060972A1A83__INCLUDED_)
#define AFX_STDAFX_H__BB9A0484_1FAF_11D1_AAE3_0060972A1A83__INCLUDED_

// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define STRICT

#define VC_EXTRALEAN

#define _CRTDBG_MAP_ALLOC
#ifdef _WIN32
#include <crtdbg.h>
#endif

// Define threading model before including MFC to avoid 'multiple threading model definition warning' warning
#define _ATL_APARTMENT_THREADED

//#include <amsversion.h>

#include <malloc.h>
//#include <afxwin.h>
//#include <afxdisp.h>

/******************************************************************************
 *
 * Standard C header files
 *
 ******************************************************************************/
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <limits.h>
#ifdef __ANDROID__
#include <sys/time.h>
#else
#include <sys/timeb.h>
#endif
#include <errno.h>
#include <sys/stat.h>
#include <math.h>            //needed for fmstime & other time stuff
#include <float.h>

/******************************************************************************
 *
 * Operating System dependent header files
 *
 ******************************************************************************/

//#include <afx.h>                     /* MFC core and standard      */
//#include <afxwin.h>                  /* MFC core and standard      */
//#include <afxext.h>                  /* MFC extensions             */
//#include <afxole.h>                  /* MFC OLE classes            */
//#include <afxtempl.h>				   /* MFC template calsses       */
//#include <windows.h>
//#include <windowsx.h>            /* Windows Memory             */
//#include <winnetwk.h>
//#include <winuser.h>

//#include <atlbase.h>

#include "PlatformCommon.h"

#include "stdstring.h"

#if 0
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
class CExeModule:public CComModule
{
	public:
	CExeModule();
	~CExeModule();

	LONG Unlock();
	HRESULT RegisterServer(BOOL bRegTypeLib = FALSE, const CLSID* pCLSID = NULL);
	void MonitorShutdown();
	void StartMonitor();

	DWORD m_dwThreadID;
	HANDLE m_hCOMShutdownEvent ;		// Signaled when not serving any COM objects
	HANDLE m_hControlledShutdownEvent ;	// Signaled when no CControlledShutdown objects are active
	bool m_bActivity;
};
extern CExeModule _Module;
#endif
//#include <atlcom.h>

#include <std.h>
#include <od_defs.h>
#include <ddldefs.h>

#include <Ntcassert2.h>
#include <BssProgLog.h>
#include <EddEngineLog.h>

#if 0
/* window parameters */
extern HWND server_window;      /* window handle */

extern TCHAR WNetUserName[];
#endif

/******************************************************************************/


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__BB9A0484_1FAF_11D1_AAE3_0060972A1A83__INCLUDED)
