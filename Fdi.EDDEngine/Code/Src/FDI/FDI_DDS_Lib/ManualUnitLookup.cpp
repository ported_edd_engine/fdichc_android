#include "stdafx.h"

#include "ManualUnitLookup.h"
#include "ddi_tab.h"
#include "dds_tab.h"
#include "FDI/FDIEDDEngine/DDSSupport.h"



// ManualUnitLookup_Lock constructor
ManualUnitLookup_Lock::ManualUnitLookup_Lock(ManualUnitLookup* pClassToLock)
{
	m_pClassToLock = pClassToLock;

	m_pClassToLock->m_cs.lock();
}

// ManualUnitLookup_Lock destructor
ManualUnitLookup_Lock::~ManualUnitLookup_Lock()
{
	m_pClassToLock->m_cs.unlock();
}



// ManualUnitLookup constructor
ManualUnitLookup::ManualUnitLookup()
{
	

	// Other class members have their own constructors
}


// ManualUnitLookup destructor
ManualUnitLookup::~ManualUnitLookup()
{
	UNITS_VECTOR::iterator Iter;

	// Clean out the vecStaticUnits vector
	for (	Iter  = vecStaticUnits.begin();
			Iter != vecStaticUnits.end();
			Iter  = vecStaticUnits.begin()	)
	{
		DDI_GENERIC_ITEM *gi_unit = *Iter;

		delete (nsEDDEngine::FLAT_UNIT *)gi_unit->item;	// "delete" the FLAT_UNIT
		free (gi_unit);					// "free()" the DDI_GENERIC_ITEM itself

		vecStaticUnits.erase(Iter);		// erase this entry from the vector
	}

	// Clean out the vecConditionalUnits vector
	for (	Iter  = vecConditionalUnits.begin();
			Iter != vecConditionalUnits.end();
			Iter  = vecConditionalUnits.begin()	)
	{
		DDI_GENERIC_ITEM *gi_unit = *Iter;

		delete (nsEDDEngine::FLAT_UNIT *)gi_unit->item;	// "delete" the FLAT_UNIT
		free (gi_unit);						// "free()" the DDI_GENERIC_ITEM itself

		vecConditionalUnits.erase(Iter);	// erase this entry from the vector
	}

	
}


// This is the part of the Init() function which evaluates the UNIT and stores it in the proper vector

int ManualUnitLookup::EvalAndStore(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec, ITEM_ID item_id )
{
	nsEDDEngine::FDI_ITEM_SPECIFIER is_unit = {nsEDDEngine::FDI_ITEM_ID, 0, 0};
	is_unit.eType = nsEDDEngine::FDI_ITEM_ID;
	is_unit.item.id = item_id;

	DDI_GENERIC_ITEM *gi_unit = (DDI_GENERIC_ITEM *)calloc(1, sizeof(DDI_GENERIC_ITEM));
	nsEDDEngine::AttributeNameSet mask = nsEDDEngine::relation_update_list;
	// Get this UNITs definition

	int rc = ddi_get_item(block_spec, &is_unit, env_info, mask, gi_unit);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	nsEDDEngine::FLAT_UNIT *pUnit = (nsEDDEngine::FLAT_UNIT *)gi_unit->item;

	if (pUnit->IsDynamic(nsEDDEngine::relation_update_list) == false)	// This UNIT is statically defined
	{
		vecStaticUnits.push_back(gi_unit);
	}
	else
	{								// This UNIT has conditional references in it
		vecConditionalUnits.push_back(gi_unit);
	}

	return rc;
}


int ManualUnitLookup::Init(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Create the vecStaticUnits and vecConditionalUnits vectors

	// Foreach UNIT in this EDD (for HART and PROFIBUS) or in this Block (for FF),
	// call ddi_get_item to get the definition
	// store the FLAT_UNITs into one of two vectors
	//   vecStaticUnits - for UNITs that are statically defined
	//   vecConditionalUnits - for UNITs that have conditionals in them

	IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
	CConnectionMgr* pConnMgr = pDDSSupport->GetConnectionManager();

	BLOCK_HANDLE block_handle = 0;

	int rc = conv_block_spec(env_info, block_spec, &block_handle);
	if (rc != SUCCESS) {
		return(rc) ;
	}

	nsEDDEngine::FLAT_DEVICE_DIR *flat_device_dir = nullptr;

	rc = pConnMgr->get_abt_dd_dev_tbls(block_handle, 
			(void **)&flat_device_dir) ;
	if (rc != SUCCESS) {
		return(rc) ;
	}


	if ((flat_device_dir->header.edd_profile == nsEDDEngine::PROFILE_FF) || (flat_device_dir->header.edd_profile == nsEDDEngine::PROFILE_ISA100))	// Get the UNITS from the Block and process them
	{
		// Eval the block
		nsEDDEngine::BLK_TBL_ELEM *bt_elem = nullptr;		// Block Table element pointer

		rc = block_handle_to_bt_elem(env_info, block_handle, &bt_elem);
		if (rc != DDS_SUCCESS) {
			return rc;
		}

		nsEDDEngine::FDI_ITEM_SPECIFIER blk_item_spec;

		blk_item_spec.eType = nsEDDEngine::ItemSpecifierType::FDI_ITEM_ID;
		blk_item_spec.item.id = bt_elem->blk_id;
		blk_item_spec.subindex = 0;

		nsEDDEngine::AttributeNameSet req_mask = nsEDDEngine::unit_items;
		DDI_GENERIC_ITEM gi = {0};

		rc = ddi_get_item(block_spec, &blk_item_spec, env_info, req_mask, &gi); 
		if (rc) {
			return rc;
		}

		// Loop on all the units mentioned in the block and call EvalAndStore
		nsEDDEngine::FLAT_BLOCK *flat = (nsEDDEngine::FLAT_BLOCK *)gi.item;
		for (int i=0; i < flat->unit.count; i++)
		{
			EvalAndStore(env_info, block_spec, flat->unit.list[i]);
		}

		delete flat;
	}
	else	// HART and PROFIBUS
	{
		nsEDDEngine::ITEM_TBL *it = &flat_device_dir->item_tbl;	// Get the UNITs from the entire EDD and process

		for (int i = 0; i < it->count; i++)
		{
			if (it->list[i].item_type == UNIT_ITYPE)	// If this is a UNIT
			{
				EvalAndStore(env_info, block_spec, it->list[i].item_id);
			}
		}
	}

	return rc;
}


int ManualUnitLookup::FindUnitRel(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitFromTbl, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	int rc = DDL_SUCCESS;

	if (	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID)
		||	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI)
		||	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX) )
	{

		// Look up this param in the map to see if we have it cached already.
		UNITS_MAP::const_iterator cIter = mapUnitLookup.find(*param_spec);

		if (cIter == mapUnitLookup.cend())	// This param is not found, process it and return the result
		{
			rc = FindForFirstTime(env_info, block_spec, unitFromTbl, param_spec, pUnitID);
		}
		else	// The units for this param have been cached...
		{
			if (cIter->second.etype == CachedUnit::StaticUnit)	// The cached unit was static
			{
				// This param is found and we have an ITEM_ID already stored.
				*pUnitID = cIter->second.UnitID;
			}
			else if (cIter->second.etype == CachedUnit::ConditionalUnit)	// The cached unit was conditional
			{
				// This param is found, but its UNITs are not static.
				// Look in the vecConditionalUnits vector, using the cached UnitID as the best guess
				rc = FindInConditionalVector(env_info, block_spec, cIter->second.UnitID, param_spec, pUnitID);
			}
			else
			{
				// This param is found, but it's UNITs were not found the last time.
				// Look in the vecConditionalUnits vector, using the Table Units as the best guess
				rc = FindInConditionalVector(env_info, block_spec, unitFromTbl, param_spec, pUnitID);
			}
		}
	}
	else
	{
		rc = DDI_INVALID_PARAM;	// This param_spec->type cannot be processed
	}

	return rc;
}


int ManualUnitLookup::FindForFirstTime(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitFromTbl, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Search for the UNIT relation in this order
	//	- Use the unitFromTbl relation, if it works and is in varStaticUnits
	//	- Search varStaticUnits, use the first one that works
	//	- Use the unitFromTbl relation, if it works and is in varConditionalUnits
	//	- Search varConditionalUnits, use the first one that works

	// If the UNIT relation is from the varStaticUnits vector, cache it and mark as "StaticUnit".
	// If the UNIT relation is from the varConditionalUnits vector, cache it and mark as
	//		"ConditionalUnit". It will need to be verified next time.
	// If a UNIT relation is not found, cache a zero and mark as "NoUnit". We will look in
	//      varConditionalUnits next time. The conditions might be better and we might
	//      find a match.

	UNITS_VECTOR::const_iterator cIter;

	//
	// Look first in the vecStaticUnits for the "unitFromTbl" relation, if we have one
	//
	if (unitFromTbl != 0)
	{
		for (cIter = vecStaticUnits.cbegin(); cIter != vecStaticUnits.cend(); cIter++)
		{
			DDI_GENERIC_ITEM *gi_unit = *cIter;

			nsEDDEngine::FLAT_UNIT *pUnit = (nsEDDEngine::FLAT_UNIT *)gi_unit->item;

			// Looking only for the UNIT mentioned in the Relation table
			   //////////////////////////
			if (unitFromTbl == pUnit->id)
			{ 
				if (IsMyUnit(pUnit, param_spec))	// Found it.
				{								// Cache this value for next time
					mapUnitLookup[*param_spec] = CachedUnit ( CachedUnit::StaticUnit, pUnit->id );

					*pUnitID = pUnit->id;
					return DDL_SUCCESS;
				}
			}
		}
	}

	//
	// If not found, look in the vecStaticUnits vector for any relation that works.
	//
	for (cIter = vecStaticUnits.cbegin(); cIter != vecStaticUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		nsEDDEngine::FLAT_UNIT *pUnit = (nsEDDEngine::FLAT_UNIT *)gi_unit->item;

		if (unitFromTbl == pUnit->id )
		{
			continue;		// We already checked this unit, above
		}

		if (IsMyUnit(pUnit, param_spec))	// Found it.
		{								// Cache this value for next time
			mapUnitLookup[*param_spec] = CachedUnit ( CachedUnit::StaticUnit, pUnit->id );

			*pUnitID = pUnit->id;
			return DDL_SUCCESS;
		}
	}

	//
	// If still not found, look in the varConditionalUnits vector.
	//
	int rc = FindInConditionalVector(env_info, block_spec, unitFromTbl, param_spec, pUnitID);

	return rc;
}


int ManualUnitLookup::FindInConditionalVector(ENV_INFO *env_info, DDI_BLOCK_SPECIFIER *block_spec,
		ITEM_ID unitBestGuess, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec, ITEM_ID *pUnitID)
{
	ManualUnitLookup_Lock lock(this);		// Lock this class for thread-safety

	// Foreach FLAT_UNIT in the vecConditionalUnits vector
	// Call ddi_get_item() to get the current definition.
	// Look to see if this paramID is in this FLAT_UNIT
	// If so, return it
	// if not, continue to the next one in the vector

	// If not found, return an error
	int rc = DDL_SUCCESS;

	UNITS_VECTOR::const_iterator cIter;

	//
	// Look first in the vecConditionalUnits for the "unitBestGuess" relation, if we have one
	//
	if (unitBestGuess != 0)
	{
		for (cIter = vecConditionalUnits.cbegin(); cIter != vecConditionalUnits.cend(); cIter++)
		{
			DDI_GENERIC_ITEM *gi_unit = *cIter;

			nsEDDEngine::FLAT_UNIT *pUnit = (nsEDDEngine::FLAT_UNIT *)gi_unit->item;

			// Looking only for the UNIT that is our Best Guess
				//////////////////////////
			if (unitBestGuess == pUnit->id)
			{
				nsEDDEngine::FDI_ITEM_SPECIFIER is_unit = {nsEDDEngine::FDI_ITEM_ID, 0, 0};
				is_unit.eType = nsEDDEngine::FDI_ITEM_ID;
				is_unit.item.id = pUnit->id;

				nsEDDEngine::AttributeNameSet mask = nsEDDEngine::relation_update_list;

				rc = ddi_get_item(block_spec, &is_unit, env_info, mask, gi_unit);
				if (rc != SUCCESS) {
					return(rc) ;
				}

				if (IsMyUnit(pUnit, param_spec))	// Found it.
				{								// Cache this value for next time
					mapUnitLookup[*param_spec] = CachedUnit ( CachedUnit::ConditionalUnit, pUnit->id );

					*pUnitID = pUnit->id;
					return DDL_SUCCESS;
				}
			}
		}
	}

	//
	// Next look in the vecConditionalUnits vector for any relation that works.
	//
	for (cIter = vecConditionalUnits.cbegin(); cIter != vecConditionalUnits.cend(); cIter++)
	{
		DDI_GENERIC_ITEM *gi_unit = *cIter;

		nsEDDEngine::FLAT_UNIT *pUnit = (nsEDDEngine::FLAT_UNIT *)gi_unit->item;

		if (unitBestGuess == pUnit->id )
		{
			continue;		// We already checked this unit, above
		}

		nsEDDEngine::FDI_ITEM_SPECIFIER is_unit = {nsEDDEngine::FDI_ITEM_ID, 0, 0};
		is_unit.eType = nsEDDEngine::FDI_ITEM_ID;
		is_unit.item.id = pUnit->id;

		nsEDDEngine::AttributeNameSet mask = nsEDDEngine::relation_update_list;

		rc = ddi_get_item(block_spec, &is_unit, env_info, mask, gi_unit);
		if (rc != SUCCESS) {
			return(rc) ;
		}

		if (IsMyUnit(pUnit, param_spec))	// Found it.
		{								// Cache this value for next time
			mapUnitLookup[*param_spec] = CachedUnit ( CachedUnit::ConditionalUnit, pUnit->id );

			*pUnitID = pUnit->id;
			return DDL_SUCCESS;
		}
	}

	// No FLAT_UNIT found, so cache a zero and mark with "NoUnit"
	mapUnitLookup[*param_spec] = CachedUnit ( CachedUnit::NoUnit, 0 );

	// Not found, so return an error
	return DDI_TAB_NO_UNIT;
}


bool
ManualUnitLookup::IsMyUnit(nsEDDEngine::FLAT_UNIT *pUnit, nsEDDEngine::FDI_PARAM_SPECIFIER *param_spec)
{
	// Look in this FLAT_UNIT to see if this param_spec is on the right side

    for (int i=0; i < pUnit->var_units.count; i++)
    {
		nsEDDEngine::OP_REF_TRAIL *pUnitOpRefTrail = &pUnit->var_units.list[i];

		if ( pUnitOpRefTrail->op_ref_type == STANDARD_TYPE )		// If the unit is STANDARD, compare the OP_REF_INFO parts
		{
			if (	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID)	// If this param_spec is "STANDARD"
				||	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_SI)
				)
			{
				nsEDDEngine::OP_REF_INFO *pUnitOpRef = &(pUnitOpRefTrail->op_info);

				// Create an OP_REF_INFO with the param_spec contents
				nsEDDEngine::OP_REF_INFO ParamOpRef;
				ParamOpRef.id = param_spec->id;
				ParamOpRef.member = param_spec->subindex;

				// Compare
				if (*pUnitOpRef == ParamOpRef)	/* We have found the matching unit relation */
				{
					return true;
				}
				else if (	(pUnitOpRef->type == nsEDDEngine::ITYPE_ARRAY)	// If it is an entire ARRAY,
						&&	(pUnitOpRef->id == ParamOpRef.id) )				// you only need to match the id.
				{
					return true;
				}
			}
		}
		else	// COMPLEX - Compare the OP_REF_INFO_LIST
		{
			if (	(param_spec->eType == nsEDDEngine::FDI_PS_ITEM_ID_COMPLEX)	// Only compare if the param_spec is also COMPLEX
				&&	(pUnitOpRefTrail->op_info_list.count == param_spec->RefList.count)	// And have the same count of OP_REF_INFO entries
				)
			{
				int j = 0;		// Loop counter
				for(     ; j < pUnitOpRefTrail->op_info_list.count; j++ )	// All entries must match
				{
					if ( pUnitOpRefTrail->op_info_list.list[j] != param_spec->RefList.list[j])
					{
						/* This entry did not match */
						break;
					}
				}

				if (j == pUnitOpRefTrail->op_info_list.count)	// If we went through the whole loop
				{
					return true;								// We have a match!
				}
			}
		}
    }

	return false;
}
