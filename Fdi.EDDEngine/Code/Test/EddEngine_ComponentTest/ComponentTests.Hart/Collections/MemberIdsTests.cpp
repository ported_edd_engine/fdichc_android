#include "TestSupport/EddEngineController.h"
#include "TestSupport/ParamCacheSimulator.h"

using namespace nsEDDEngine;
using namespace nsConsumer;
using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace ComponentTests::Core::TestSupport;

namespace ComponentTests
{
    namespace Hart
    {
        [TestClass, Ignore]
        public ref class BaseTestMemberID
        {
        public: 
            static EddEngineController^ engine;
            static ITEM_ID* ResolvedMemberId;
            static int ReturnCode;

            static void SetupBase(String^ testFileName)
            {
                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                ResolvedMemberId = new ITEM_ID;
                ReturnCode = engine->Instance->GetItemIdFromSymbolName(L"COL_VAR_1", ResolvedMemberId);
            }

            static void CleanupBase()
            {
                delete engine;
                delete ResolvedMemberId;
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_success_retun_code_0()
            {
                Assert::AreEqual(0, ReturnCode);
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_correct_member_id()
            {
                Assert::AreEqual(16395u, *ResolvedMemberId);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestMemberName
        {
        public: 
            static EddEngineController^ engine;
            static wchar_t* MemberName;
            static int ReturnCode;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();

                const size_t memberNameSize = 100;
                MemberName = new wchar_t[memberNameSize];
                ReturnCode = engine->Instance->GetSymbolNameFromItemId(16395u, MemberName, memberNameSize);
            }

            static void CleanupBase()
            {
                delete engine;
                delete MemberName;
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_success_retun_code_0()
            {
                Assert::AreEqual(0, ReturnCode);
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_correct_member_name()
            {
                Assert::AreEqual(gcnew System::String("COL_VAR_1"), gcnew System::String(MemberName));
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestArrayMemberAccess
        {
        public:
            static EddEngineController^ engine;
            static ITEM_ID* ResolvedMemberId;
            static ITEM_TYPE* ResolvedItemTypeArray;
            static ITEM_TYPE* ResolvedItemTypeItem;
            static int ReturnCode;

            static void SetupBase(String^ testFileName)
            {
                FDI_ITEM_SPECIFIER arraySpec;
                FDI_ITEM_SPECIFIER itemSpec;
                ResolvedItemTypeArray = new ITEM_TYPE;
                ResolvedItemTypeItem = new ITEM_TYPE;

                engine = gcnew EddEngineController(testFileName);
                engine->Initialize();

                ResolvedMemberId = new ITEM_ID;
                ReturnCode = engine->Instance->GetItemIdFromSymbolName(L"array_of_values", ResolvedMemberId);
                ITEM_ID x = *ResolvedMemberId;
                Assert::AreEqual(0, ReturnCode, "EddEngine failed to get an item ID for 'array_of_values'");
                arraySpec.eType = FDI_ITEM_ID;
                arraySpec.item.id = *ResolvedMemberId;
                arraySpec.subindex = 0;
                ReturnCode = engine->Instance->GetItemType(0, &arraySpec, ResolvedItemTypeArray);
                Assert::AreEqual(0, ReturnCode, "EddEngine failed to get the item type for the array");

                FDI_GENERIC_ITEM* itemToGet = new FDI_GENERIC_ITEM(ITYPE_ARRAY);
                AttributeNameSet setToAsk = type_definition | number_of_elements;
                engine->Instance->GetItem(0, 0, &arraySpec, &setToAsk, L"en", itemToGet);

                FLAT_ARRAY* accessArray = dynamic_cast<FLAT_ARRAY*>(itemToGet->item);

                Assert::IsTrue(nullptr != accessArray, "EddEngine did not return a FLAT_ARRAY for the array itemId");
                itemSpec.eType = FDI_ITEM_ID;
                itemSpec.item.id = accessArray->type;
                itemSpec.subindex = 0;
                ReturnCode = engine->Instance->GetItemType(0, &itemSpec, ResolvedItemTypeItem);
            }

            static void CleanupBase()
            {
                delete engine;
                delete ResolvedMemberId;
                delete ResolvedItemTypeArray;
                delete ResolvedItemTypeItem;
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_success_retun_code_0()
            {
                Assert::AreEqual(0, ReturnCode);
            }

            [TestMethod, TestCategory("MemberIdsTest")]
            void It_should_return_the_correct_member_id()
            {
                Assert::AreEqual(16392u, *ResolvedMemberId);
            }

            [TestMethod, TestCategory("MemberTests")]
            void It_should_return_the_correct_type_for_the_array()
            {
                Assert::AreEqual((UINT)ITYPE_ARRAY, (UINT)*ResolvedItemTypeArray);
            }

            [TestMethod, TestCategory("MemberTests")]
            void It_should_return_the_correct_type_for_the_array_member()
            {
                Assert::AreEqual((UINT)ITYPE_VARIABLE, (UINT)*ResolvedItemTypeItem);
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestArray
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_ITEM_ARRAY* test_array;
            static ITEM_ID expectedItemId;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();                

                FDI_PARAM_SPECIFIER test_array_specifier;
                engine->FillParamSpecifier("ArrayOfVariable", &test_array_specifier);
                genericItem = engine->GetGenericItem(test_array_specifier.id, nsEDDEngine::ITYPE_ITEM_ARRAY);
                test_array = dynamic_cast<FLAT_ITEM_ARRAY*>(genericItem->item);
                expectedItemId = test_array_specifier.id;

            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }


            [TestMethod, TestCategory("ArrayTest")]
            void It_should_have_the_label_attribute()
            {
                Assert::AreEqual(1, (int) test_array->masks.attr_avail.count(label));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_have_the_help_attribute()
            {
                Assert::AreEqual(1, (int) test_array->masks.attr_avail.count(help));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_expected_Item_Id_for_the_array()
            {
                Assert::AreEqual(expectedItemId, test_array->id);
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_contain_2_elements()
            {
                Assert::AreEqual(2u, (UINT32) test_array->elements.count);
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_label_of_the_array()
            {
                Assert::AreEqual(gcnew System::String("Array of Variable"), gcnew System::String(test_array->label.c_str()));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_help_of_the_array()
            {
                Assert::AreEqual(gcnew System::String("Array of Variable Help"), gcnew System::String(test_array->help.c_str()));
            }
        };

        [TestClass, Ignore]
        public ref class BaseTestValueArray
        {
        public:
            static EddEngineController^ engine;
            static FDI_GENERIC_ITEM* genericItem;
            static FLAT_ARRAY* test_array;
            static ITEM_ID expectedItemId;
            static ITEM_ID expectedItemIdOfElements;

            static void SetupBase(String^ testCaseName)
            {
                engine = gcnew EddEngineController(testCaseName);
                engine->Initialize();                

                FDI_PARAM_SPECIFIER test_array_specifier;
                engine->FillParamSpecifier("array_of_values", &test_array_specifier);
                genericItem = engine->GetGenericItem(test_array_specifier.id, nsEDDEngine::ITYPE_ARRAY);
                test_array = dynamic_cast<FLAT_ARRAY*>(genericItem->item);
                expectedItemId = test_array_specifier.id;

                FDI_PARAM_SPECIFIER test_elements_specifier;
                engine->FillParamSpecifier("myvariable1", &test_elements_specifier);
                expectedItemIdOfElements= test_elements_specifier.id;

            }

            static void CleanupBase()
            {
                delete engine;
                delete genericItem;
            }
            
            [TestMethod, TestCategory("ArrayTest")]
            void It_should_have_the_label_attribute()
            {
                Assert::AreEqual(1, (int) test_array->masks.attr_avail.count(label));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_have_the_help_attribute()
            {
                Assert::AreEqual(1, (int) test_array->masks.attr_avail.count(help));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_expected_Item_Id_for_the_array()
            {
                Assert::AreEqual(expectedItemId, test_array->id);
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_contain_100_elements()
            {
                Assert::AreEqual(100u, (UINT32) test_array->num_of_elements);
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_label_of_the_array()
            {
                Assert::AreEqual(gcnew System::String("Test label"), gcnew System::String(test_array->label.c_str()));
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_return_the_help_of_the_array()
            {
                Assert::AreEqual(gcnew System::String("Test help"), gcnew System::String(test_array->help.c_str()));
            }
            
            [TestMethod, TestCategory("ArrayTest")]
            void It_should_of_type_array()
            {
                Assert::AreEqual((int)nsEDDEngine::ITYPE_ARRAY,(int) test_array->item_type);
            }

            [TestMethod, TestCategory("ArrayTest")]
            void It_should_have_the_expected_Item_id_for_the_type_of_the_elements()
            {
                Assert::AreEqual(expectedItemIdOfElements, test_array->type);
            }
        };

        // WORKAROUND: use TestInitalize 
        // - MSTest runs all test in a not controllable order 
        // - ClassInitalize initialize the (static) BaseClass only once, regardless if other testclasses with the same Base are already finished their tests
        // -> Some of the tests runs with an other configuration as wanted
        // -> DANGER: Most tests passes anyway, but not with the wanted configuration
        // possible Improvement:
        // -> Change TestEnvironment to nonstatic 
        // -> Split BaseClass to avoid initializing every item every time (example: Chart with 7, but every test use only 1)

        [TestClass]
        public ref class When_requesting_a_collection_member_id_with_FM8 : BaseTestMemberID
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0007.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_collection_member_id_with_FMA : BaseTestMemberID
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0007.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };


        [TestClass]
        public ref class When_requesting_a_collection_member_name_with_FM8 : BaseTestMemberName
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0007.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_requesting_a_collection_member_name_with_FMA : BaseTestMemberName
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0007.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };



        [TestClass]
        public ref class When_checking_the_types_of_array_and_array_member_with_FMA : BaseTestArrayMemberAccess
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0020.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_checking_the_types_of_array_and_array_member_with_FM8 : BaseTestArrayMemberAccess
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0020.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };



        [TestClass]
        public ref class When_getting_a_reference_array_with_FMA : BaseTestArray
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0020.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_getting_a_reference_array_with_FM8 : BaseTestArray
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0020.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };


        [TestClass]
        public ref class When_getting_a_value_array_with_FMA : BaseTestValueArray
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0023.fmA");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

        [TestClass]
        public ref class When_getting_a_value_array_with_FM8 : BaseTestValueArray
        {
        public:
            [TestInitialize]
            void Setup()
            {
                SetupBase("Hart\\TestCase0023.fm8");
            }

            [TestCleanup]
            void CleanUp()
            {
                CleanupBase();
            }
        };

    } // namespace Hart
} // namespace ComponentTests
