/*****************************************************************************
 *	"@(#)$Id: sym.h,v 1.3 1996/03/14 04:08:57 stevbey Exp $"	
 ****************************************************************************/
#ifndef SYM_INC
#define SYM_INC

#ifndef CM_STRUCT_H
#include "cm_struc.h"
#endif


typedef struct  {
	ITEM_ID		item_id;
	char*		item_name_ptr;
} SYMTBL;

typedef struct {
	SYMTBL*		symidtbl;
	SYMTBL*		symnametbl;
	int			numelem;
} SYMINFO;



int read_sym_file(ENV_INFO* env_info, DD_DEVICE_ID*, SYMINFO*);

void free_symtbl(SYMINFO*);

int server_item_id_to_name(ENV_INFO* env_info, SYMINFO*, ITEM_ID, LPTSTR, int outbuf_size);

int server_item_name_to_id(ENV_INFO* env_info, SYMINFO*, LPCTSTR, ITEM_ID *, bool bBssLog = true);
#endif


