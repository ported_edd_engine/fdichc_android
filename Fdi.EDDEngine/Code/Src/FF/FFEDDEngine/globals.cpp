

#include "stdafx.h"
#include "DDSSupport.h"

 //Param Cache

int app_func_get_param_value (ENV_INFO *env_info, ::OP_REF *op_ref, ::EVAL_VAR_VALUE *param_value)
{
	int rc = PC_FAIL;

	if (env_info->handle_type == ENV_INFO::BlockHandle)
	{
		IDDSSupport *pDDSSupport = static_cast<IDDSSupport*>(env_info->app_info);
		rc = pDDSSupport->DDS_GetParamValue( env_info->block_handle, env_info->value_spec, op_ref, param_value );
	}
	else
	{
		rc = PC_INTERNAL_ERROR;
	}
	
	return rc;
}

