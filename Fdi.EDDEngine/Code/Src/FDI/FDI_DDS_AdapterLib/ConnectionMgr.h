#pragma once

#include "cm_loc.h"
#include "cm_dds.h"
#include "rmalloc.h"
#include "sym.h"


class CConnectionMgr
{
private:
	ACTIVE_BLK_TBL		active_blk_tbl;
	ACTIVE_DEV_TBL		active_dev_tbl;

public:

	void CleanTables(DEVICE_HANDLE dh);

#pragma region Active Block Table Methods
	int CreateBlockEntry(BLOCK_HANDLE *BlockHandle, DEVICE_HANDLE deviceHandle);
	
	int get_active_blk_tbl_count();

	char* ABT_TAG(BLOCK_HANDLE bh);
	void SET_ABT_TAG(BLOCK_HANDLE bh, char* t);

	DEVICE_HANDLE ABT_ADT_OFFSET(BLOCK_HANDLE bh);
	void SET_ABT_ADT_OFFSET(BLOCK_HANDLE bh, DEVICE_HANDLE adto);

	DEVICE_TYPE_HANDLE ABT_ADTT_OFFSET(BLOCK_HANDLE bh);
	void SET_ABT_ADTT_OFFSET(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE adtto);

	void SET_ABT_PARAM_COUNT(BLOCK_HANDLE bh, unsigned int pc);

	void SET_ABT_DD_BLK_ID(BLOCK_HANDLE bh, UINT32 dbi);
	UINT32 ABT_DD_BLK_ID(BLOCK_HANDLE bh);

	void SET_ABT_CHAR_RECORD( BLOCK_HANDLE bh, nsEDDEngine::CCharacteristicRecord charrecord );

	void SET_ABT_BLOCK_TYPE( BLOCK_HANDLE bh, nsEDDEngine::BLOCK_TYPE block );

	void SET_ABT_ISP_OP_INDEX(BLOCK_HANDLE bh, OBJECT_INDEX oi);

	int ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh);
	void SET_ABT_DD_BLK_TBL_OFFSET(BLOCK_HANDLE bh, int dbto);

	ROD_HANDLE ABT_DD_HANDLE(BLOCK_HANDLE bh);
	UINT32 ABT_CR_ITEM_ID(BLOCK_HANDLE bh);
	char* ABT_CR_TAG( BLOCK_HANDLE bh );
	int get_abt_op_index (BLOCK_HANDLE bh, OBJECT_INDEX *oi);

	int get_abt_param_count (BLOCK_HANDLE bh, unsigned int *pc);
	int set_abt_param_count (BLOCK_HANDLE bh, unsigned int pc);

	int get_abt_dd_blk_id (BLOCK_HANDLE bh, ITEM_ID *dbi);
	int get_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int *dbto);
	int get_abt_adtt_offset(BLOCK_HANDLE bh, DEVICE_TYPE_HANDLE *dth);
	int get_abt_adt_offset (BLOCK_HANDLE bh, DEVICE_HANDLE *dh);
	int get_abt_dd_handle(BLOCK_HANDLE bh, ROD_HANDLE *ddh);
	int get_abt_dd_dev_tbls(BLOCK_HANDLE bh, void **ddt);
	int set_abt_dd_blk_tbl_offset (BLOCK_HANDLE bh, int dbto);

#pragma endregion

#pragma region Active Device Table Functions
	int CreateActiveDeviceTableEntry(DEVICE_HANDLE *deviceHandle);
	void SetActiveDeviceTableData(DEVICE_TYPE_HANDLE  * deviceTypeHandle, DEVICE_HANDLE deviceHandle, DDOD_HEADER header);
	DEVICE_TYPE_HANDLE ADT_ADTT_OFFSET(DEVICE_HANDLE dh);
	void SET_ADT_ADTT_OFFSET(DEVICE_HANDLE dh, DEVICE_TYPE_HANDLE adtto);
	void SET_ADT_VIEW_TIME(DEVICE_HANDLE dh, VIEWTYPE eView, DATE dt);
	int ADT_USAGE(DEVICE_HANDLE dh);
	void SET_ADT_USAGE(DEVICE_HANDLE dh, int i);
	DD_DEVICE_ID* ADT_DD_DEVICE_ID(DEVICE_HANDLE dh);
	void SET_ADT_DD_DEVICE_ID(DEVICE_HANDLE dh, DD_DEVICE_ID* ddi);
	ROD_HANDLE ADT_DD_HANDLE(DEVICE_HANDLE dh);
	void SET_ADT_DD_HANDLE(DEVICE_HANDLE dh, ROD_HANDLE ddh);
	void SET_ADT_AM_BEING_COMMISSIONED(DEVICE_HANDLE dth, bool bAmBeingCommissioned);
	nsEDDEngine::FLAT_DEVICE_DIR* ADT_DD_DEV_TBLS(DEVICE_HANDLE dh);
	int get_adt_dd_dev_tbls (DEVICE_HANDLE, void **);
	int get_adt_dd_handle(DEVICE_HANDLE dh, ROD_HANDLE *ddh);
#pragma endregion

#pragma region Handle Validity Methods
	int valid_block_handle(BLOCK_HANDLE bh);
	int valid_device_handle (DEVICE_HANDLE dh);
	int valid_dd_handle (ROD_HANDLE ddh);
#pragma endregion

#pragma region Connection Manager Init/Cleanup
	int cm_init(int);
	int ct_init(void);
	int cm_cleanup();
#pragma endregion

#pragma region Connection Manager Support Methods
	BLOCK_HANDLE ct_block_search(char *tag);
#pragma endregion

#pragma region Connection Manager DD Support Methods
	int ds_dd_block_load(ENV_INFO *env_info, BLOCK_HANDLE block_handle);
	int ds_dd_device_load(ENV_INFO *env_info, DEVICE_HANDLE	device_handle, ROD_HANDLE* dd_handle);
	int ds_ddod_load(ENV_INFO *env_info, DEVICE_HANDLE	device_handle, ROD_HANDLE* dd_handle);
	int ds_get_object(FILE *file, OBJECT *obj, ROD_HANDLE *rod_handle);
	int ds_get_ddef_desc(ENV_INFO *env_info, FILE *file, DDEF_DESC *ddef_desc);
#pragma endregion

	int load_sym_info(DEVICE_TYPE_HANDLE);
	int remove_sym_info (DEVICE_TYPE_HANDLE);
	int Fill_Syminfo(DEVICE_TYPE_HANDLE dth, SYMINFO* syminfo);

	void set_dd_path( wchar_t path );
};