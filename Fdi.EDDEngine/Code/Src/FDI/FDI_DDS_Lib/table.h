/*
 *	@(#) $Id: table.h,v 1.4 2012/09/06 22:28:01 rgretta Exp $
 *	Copyright 1993 Rosemount, Inc.- All rights reserved
 */

#ifndef TABLE_H
#define TABLE_H


#ifndef FLATS_H
#include "flats.h"
#endif

#ifndef DDS_UPCL_H
#include "dds_upcl.h"
#endif


/*
 *	Convenience Macros for Handling Device Tables
 */


/* Block Table macros */

#define	BT_LIST(bt_ptr) (bt_ptr->list)
#define	BT_COUNT(bt_ptr) (bt_ptr->count)
#define	BTE(bt_ptr,elem) (&bt_ptr->list[elem])

/* Block Table element macros */
#define	BTE_PLT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_tbl)
#define	BTE_PLMNT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_mem_name_tbl)
#define	BTE_PLMT(bte_ptr) (&bte_ptr->flat_block_dir.param_list_mem_tbl)

#define	BTE_BLK_ID(bte_ptr) (bte_ptr->blk_id)
#define	BTE_BD_DD_REF(bte_ptr) (bte_ptr->blk_dir_dd_ref)
#define	BTE_IT_OFFSET(bte_ptr) (bte_ptr->item_tbl_offset)
#define	BTE_USAGE(bte_ptr) (bte_ptr->usage)

#define	BTE_BIT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_tbl)
#define	BTE_BINT(bte_ptr) (&bte_ptr->flat_block_dir.blk_item_name_tbl)
#define	BTE_PT(bte_ptr) (&bte_ptr->flat_block_dir.param_tbl)
#define	BTE_PMT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_tbl)
#define	BTE_PET(bte_ptr) (&bte_ptr->flat_block_dir.param_elem_tbl)
#define	BTE_PMNT(bte_ptr) (&bte_ptr->flat_block_dir.param_mem_name_tbl)
#define	BTE_RT(bte_ptr) (&bte_ptr->flat_block_dir.rel_tbl)
#define	BTE_UT(bte_ptr) (&bte_ptr->flat_block_dir.update_tbl)
#define	BTE_CT(bte_ptr) (&bte_ptr->flat_block_dir.command_tbl)
#define	BTE_CPT(bte_ptr) (&bte_ptr->flat_block_dir.crit_param_tbl)

#define	BTE_CR_IT_OFFSET(bte_ptr) (bte_ptr->char_rec_item_tbl_offset)
#define	BTE_CMT(bte_ptr) (&bte_ptr->flat_block_dir.char_mem_tbl)

#define	BTE_CMNT(bte_ptr) (&bte_ptr->flat_block_dir.char_mem_name_tbl)


/* Item Table macros */

#define	IT_LIST(it_ptr) (it_ptr->list)
#define	IT_COUNT(it_ptr) (it_ptr->count)
#define	ITE(it_ptr,elem) (&it_ptr->list[elem])

/* Item Table element macros */

#define	ITE_ITEM_ID(ite_ptr) (ite_ptr->item_id)
#define	ITE_DD_REF(ite_ptr) (ite_ptr->dd_ref)
#define	ITE_ITEM_TYPE(ite_ptr) (ite_ptr->item_type)

/* Characteristic Member Table macros */

#define	CMT_COUNT(cmt_ptr) (cmt_ptr->count)
#define	CMTE(cmt_ptr,elem) (&cmt_ptr->list[elem])

/* Characteristic Member Table element macros */

#define	CMTE_IT_OFFSET(cmte_ptr) (cmte_ptr->item_tbl_offset)
#define	CMTE_CM_TYPE(cmte_ptr) (cmte_ptr->char_mem_type)
#define	CMTE_CM_SIZE(cmte_ptr) (cmte_ptr->char_mem_size)

/* Characteristic Member Name Table element macros */

#define	CMNTE_CM_NAME(cmnte_ptr) (cmnte_ptr->char_mem_name)
#define	CMNTE_CM_OFFSET(cmnte_ptr) (cmnte_ptr->char_mem_offset)

/* Characteristic Member Name Table macros */

#define	CMNT_LIST(cmnt_ptr) (cmnt_ptr->list)
#define	CMNT_COUNT(cmnt_ptr) (cmnt_ptr->count)

/*
 *	Convenience Macros for Handling Block Tables
 */


/* Block Item Table macros */

#define	BIT_LIST(bit_ptr) (bit_ptr->list)
#define	BIT_COUNT(bit_ptr) (bit_ptr->count)
#define	BITE(bit_ptr,elem) (&bit_ptr->list[elem])

/* Block Item Table element macros */

#define	BITE_BLK_ITEM_ID(bite_ptr) (bite_ptr->blk_item_id)
#define	BITE_BINT_OFFSET(bite_ptr) (bite_ptr->blk_item_name_tbl_offset)
#define	BINTE(bint_ptr,elem) (&bint_ptr->list[elem])


/* Block Item Name Table macros */

#define	BINT_LIST(bint_ptr) (bint_ptr->list)
#define	BINT_COUNT(bint_ptr) (bint_ptr->count)
#define	BINTE(bint_ptr,elem) (&bint_ptr->list[elem])

/* Block Item Name Table element macros */

#define	BINTE_BLK_ITEM_NAME(binte_ptr) (binte_ptr->blk_item_name)
#define	BINTE_IT_OFFSET(binte_ptr) (binte_ptr->item_tbl_offset)
#define	BINTE_PT_OFFSET(binte_ptr) (binte_ptr->param_tbl_offset)
#define	BINTE_PLT_OFFSET(binte_ptr) (binte_ptr->param_list_tbl_offset)
#define	BINTE_RT_OFFSET(binte_ptr) (binte_ptr->rel_tbl_offset)
#define	BINTE_ITCT_OFFSET(binte_ptr) (binte_ptr->item_to_command_tbl_offset)


/* Parameter List Table macros */

#define	PLT_LIST(plt_ptr) (plt_ptr->list)
#define	PLT_COUNT(plt_ptr) (plt_ptr->count)
#define	PLTE(plt_ptr,elem) (&plt_ptr->list[elem])


/* Parameter Table macros */

#define	PT_LIST(pt_ptr) (pt_ptr->list)
#define	PT_COUNT(pt_ptr) (pt_ptr->count)
#define	PTE(pt_ptr,elem) (&pt_ptr->list[elem])

/* Parameter Table element macros */

#define	PTE_BINT_OFFSET(pte_ptr) (pte_ptr->blk_item_name_tbl_offset)
#define	PTE_PMT_OFFSET(pte_ptr) (pte_ptr->param_mem_tbl_offset)
#define	PTE_PM_COUNT(pte_ptr) (pte_ptr->param_mem_count)
#define	PTE_PET_OFFSET(pte_ptr) (pte_ptr->param_elem_tbl_offset)
#define	PTE_PE_COUNT(pte_ptr) (pte_ptr->param_elem_count)
#define	PTE_AE_IT_OFFSET(pte_ptr) (pte_ptr->array_elem_item_tbl_offset)
#define	PTE_PE_MX_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_COUNT(pte_ptr) (pte_ptr->array_elem_count)
#define	PTE_AE_VAR_TYPE(pte_ptr) (pte_ptr->array_elem_type_or_var_type)
#define	PTE_AE_VAR_SIZE(pte_ptr) (pte_ptr->array_elem_size_or_var_size)
#define	PTE_AE_VAR_CLASS(pte_ptr) (pte_ptr->array_elem_class_or_var_class)


/* Parameter Member Table macros */

#define	PMT_LIST(pmt_ptr) (pmt_ptr->list)
#define	PMT_COUNT(pmt_ptr) (pmt_ptr->count)
#define	PMTE(pmt_ptr,elem) (&pmt_ptr->list[elem])

/* Parameter Member Table element macros */

#define	PMTE_IT_OFFSET(pmte_ptr) (pmte_ptr->item_tbl_offset)
#define	PMTE_PM_TYPE(pmte_ptr) (pmte_ptr->param_mem_type)
#define	PMTE_PM_SIZE(pmte_ptr) (pmte_ptr->param_mem_size)
#define	PMTE_PM_CLASS(pmte_ptr) (pmte_ptr->param_mem_class)
#define PMTE_RT_OFFSET(pmte_ptr) (pmte_ptr->rel_tbl_offset)

/* Parameter List Table element macros */

#define	PLTE_BINT_OFFSET(plte_ptr) (plte_ptr->blk_item_name_tbl_offset)
#define	PLTE_PLMT_OFFSET(plte_ptr) (plte_ptr->param_list_mem_offset)
#define	PLTE_PLM_COUNT(plte_ptr) (plte_ptr->param_list_mem_count)

/* Parameter List Member Table macros */

#define	PLMT_LIST(plmt_ptr) (plmt_ptr->list)
#define	PLMT_COUNT(plmt_ptr) (plmt_ptr->count)
#define	PLMTE(plmt_ptr,elem) (&plmt_ptr->list[elem])

/* Parameter List Member Name Table macros */

#define	PLMNT_LIST(plmnt_ptr) (plmnt_ptr->list)
#define	PLMNT_COUNT(plmnt_ptr) (plmnt_ptr->count)
#define	PLMNTE(plmnt_ptr,elem) (&plmnt_ptr->list[elem])

/* Parameter List Member Table element macros */

#define	PLMTE_BINT_OFFSET(plmte_ptr) (plmte_ptr->blk_item_name_offset)
//blk_item_name_tbl_offset

/* Parameter List Member Table Name element macros */

#define	PLMNTE_PLM_NAME(plmnte_ptr) (plmnte_ptr->param_list_mem_name)
//#define	PLMNTE_PLMT_OFFSET(plmnte_ptr) (plmnte_ptr->param_list_mem_tbl_offset)


/* Parameter Member Name Table macros */

#define	PMNT_LIST(pmnt_ptr) (pmnt_ptr->list)
#define	PMNT_COUNT(pmnt_ptr) (pmnt_ptr->count)
#define	PMNTE(pmnt_ptr,elem) (&pmnt_ptr->list[elem])

/* Parameter Member Name Table element macros */

#define	PMNTE_PM_NAME(pmte_ptr) (pmte_ptr->param_mem_name)
#define	PMNTE_PM_OFFSET(pmte_ptr) (pmte_ptr->param_mem_offset)

/* Parameter Element Table macros */

#define	PET_LIST(pet_ptr) (pet_ptr->list)
#define	PET_COUNT(pet_ptr) (pet_ptr->count)
#define	PETE(pet_ptr,elem) (&pet_ptr->list[elem])

/* Parameter Element Table element macros */

#define	PETE_PE_SUBINDEX(pete_ptr) (pete_ptr->param_elem_subindex)
#define	PETE_RT_OFFSET(pete_ptr) (pete_ptr->rel_tbl_offset)

/* Relation Table macros */

#define	RT_LIST(rt_ptr) (rt_ptr->list)
#define	RT_COUNT(rt_ptr) (rt_ptr->count)
#define	RTE(rt_ptr,elem) (&rt_ptr->list[elem])

/* Relation Table element macros */

#define	RTE_WAO_IT_OFFSET(rte_ptr) (rte_ptr->wao_item_tbl_offset)
#define	RTE_UNIT_IT_OFFSET(rte_ptr) (rte_ptr->unit_item_tbl_offset)
#define	RTE_UT_OFFSET(rte_ptr) (rte_ptr->update_tbl_offset)
#define	RTE_U_COUNT(rte_ptr) (rte_ptr->update_count)
#define	RTE_UNIT_COUNT(rte_ptr) (rte_ptr->unit_count)

/* Update Table macros */

#define	UT_LIST(ut_ptr) (ut_ptr->list)
#define	UT_COUNT(ut_ptr) (ut_ptr->count)
#define	UTE(ut_ptr,elem) (&ut_ptr->list[elem])

/* Update Table element macros */

#define	UTE_DESC_IT_OFFSET(ute_ptr) (ute_ptr->desc_it_offset)


/* Command Table macros */

#define	CT_LIST(ct_ptr) (ct_ptr->list)
#define	CT_COUNT(ct_ptr) (ct_ptr->count)
#define	CTE(ct_ptr, elem) (&ct_ptr->list[elem])

/* Command Table element macros */

#define	CTE_NUMBER(cte_ptr) (cte_ptr->number)
#define	CTE_SUBINDEX(cte_ptr) (cte_ptr->subindex)
#define	CTE_COUNT(cte_ptr) (cte_ptr->count)
#define	CTE_LIST(cte_ptr) (cte_ptr->list)


/* Critical Parameters Table macros */

#define	CPT_LIST(cpt_ptr) (cpt_ptr->list)
#define	CPT_COUNT(cpt_ptr) (cpt_ptr->count)


/* Default table offset (for optional elements) */

#define DEFAULT_OFFSET      (-1)

#endif	/* TABLE_H */

