/*************************************************************************************************
 *
 * $Workfile: ddbReference.cpp$
 * 16Mar06 - stevev
 *     Revision, Date and Author have been removed due to the incredible merge
 *     overhead required to reconcile the Revision diffs from a branch.  
 *     Most CVS support tools (like Tortoise) can obtain & report this
 *     information for you.
 **
 *************************************************************************************************
 * The content of this file is the 
 *     Proprietary and Confidential property of the HART Communication Foundation
 * Copyright (c) 2002, HART Communication Foundation, All Rights Reserved 
 *************************************************************************************************
 *
 * Description:
 *		home of the reference class implementation
 * 8/07/2	sjv	created from reference.cpp
 */

#include "stdafx.h"
#include "ddbItemBase.h"
#include "ddbReferenceNexpression.h"


#ifdef _DEBUG
	#include <assert.h>
	#undef DBG_EXECUTION /* define to show a bunch */
	#undef DBG_REFTRAIL  /* define to clog reference resolutions */
#else /* release */
	#define assert(f)
#endif


void hCreference::onnew(void)
{
	rtype = 0; // item_id
	isRef = 0;
	id    = 0; // an illegal id
	type  = 0; // item_id - not really
	subindex=0;
	iName = 0;
	exprType = eT_Unknown;
	pExpr = NULL;
	pRef  = NULL;
	grpTrail.clear();
}

hCreference::hCreference(DevInfcHandle_t h) : hCobject(h)
{
	onnew();
};
	
hCreference::hCreference(const hCreference& cr) : hCobject(cr.devHndl()) 
{ 
	onnew();
	operator=(cr);  
};

hCreference::~hCreference() 
{
	// designed for object destructing this one to call
	destroy();
	//if (pExpr != NULL ) { delete pExpr; pExpr = NULL; }
	//if ( pRef != NULL ) { delete pRef;  pRef  = NULL; }
};

#ifdef _DEBUG
	#ifdef _WIN64
	#define NOTEMPTY(a) (a!=NULL && a!=(void*)0xcdcdcdcdcdcdcdcd && a!=(void*)0xdddddddddddddddd && a!=(void*)0xbaadf00dbaadf00d)
	#define ISEMPTY(b)  (b==NULL || b==(void*)0xcdcdcdcdcdcdcdcd || b==(void*)0xdddddddddddddddd || b==(void*)0xbaadf00dbaadf00d)
	#else
	#define NOTEMPTY(a) (a!=NULL && a!=(void*)0xcdcdcdcd && a!=(void*)0xdddddddd && a!=(void*)0xbaadf00d)
	#define ISEMPTY(b)  (b==NULL || b==(void*)0xcdcdcdcd || b==(void*)0xdddddddd || b==(void*)0xbaadf00d)
	#endif
#else
	#define NOTEMPTY(a) (a!=NULL)
	#define ISEMPTY(b)  (b==NULL)
#endif

/*--------------------- reference part using expression -----*/
bool hCreference:: isEmpty(void)
{  
	if (pExpr != NULL && pExpr->expressionL.size() > 0){return false;} 
	if (pRef  != NULL && !(pRef->isEmpty()))  {return false;}
	if (id != 0 || (rtype == rT_NotA_RefType || rtype != 0)) { return false;}
	return true;
}

/*** Reference functions that use expression information ****/
hCreference& hCreference::operator=(const hCreference& cr)
{	
	rtype      = cr.rtype;
	isRef      = cr.isRef;
	id         = cr.id;
	type       = cr.type;
		subindex   = cr.subindex;
	iName      = cr.iName;
	exprType   = cr.exprType;
	grpTrail   = cr.grpTrail;
#ifdef _DEBUG
	bool gotnew= false;
#endif

if (pExpr != NULL && pExpr->expressionL.size() != 0)
{
	LOGIT(CLOG_LOG,"op= destination ref is not empty\n");
}
	if NOTEMPTY(cr.pExpr )
	{
		//if (pExpr == NULL) // does not exist
		if ISEMPTY(pExpr)
		{
			pExpr  = new hCexpression(cr.pExpr->devHndl());
			assert(pExpr->expressionL.size() == 0);
#ifdef _DEBUG
			gotnew= true;
#endif
		}// else overwrite
		assert(pExpr->expressionL.size() == 0);
		*pExpr = *cr.pExpr;
		assert(pExpr->expressionL.size() == cr.pExpr->expressionL.size());
	}
	else 
	{
		//if (pExpr != NULL) 
		RAZE(pExpr);
	}

	//if (cr.pRef != NULL)
	if NOTEMPTY(cr.pRef)
	{
		//if (pRef == NULL) // does not exist
		if ISEMPTY(pRef)
		{
			pRef  = new hCreference(cr.pRef->devHndl());
		}// else overwrite
		*pRef = *cr.pRef;
	}
	else 
	{
		//if (pRef != NULL) delete pRef;
		RAZE(pRef) ;
	}
	return *this;
};	


/*** make this Reference a duplicate of the passed ****/
/*   assume that current contents are useless         */
void hCreference::duplicate(hCpayload* pPayLd, bool replicate)
{	
	if (pPayLd == NULL || ! pPayLd->validPayload(pltReference) )// nothing to do 
	{
		LOGIT(CERR_LOG|CLOG_LOG,"ERROR: reference's duplicate with a bad parameter.\n");
		return;
	}
	hCreference* pR = (hCreference*)pPayLd;

	rtype      = pR->rtype;
	isRef      = pR->isRef;
	id         = pR->id;
	type       = pR->type;
	subindex   = pR->subindex;
	iName      = pR->iName;
	exprType   = pR->exprType;
	grpTrail   = pR->grpTrail;

	if ( ! isRef || ( isRef && id != 0) )
	{// is a direct reference---------------
		if (replicate)
		{// don't copy, replicate
			hCitemBase * pItem = NULL;
			RETURNCODE
			rc = devPtr()->getItemBySymNumber(id, &pItem);
			if ( rc == SUCCESS && pItem != NULL )
			{
				itemType_t iType = pItem->getIType();
				if (iType == iT_Variable   ||
					iType == iT_ItemArray  ||
					iType == iT_Collection ||
					iType == iT_Array      ||
					iType == iT_List  )
					
				{
					return; // tha, tha, that's all folks
				}
				// else fall through to duplicate
			}
		}
	}
	// else not direct or direct without replication


	if NOTEMPTY(pR->pExpr )
	{
		pExpr = new hCexpression(devHndl());
		pExpr->setDuplicate(*(pR->pExpr));
	}
	else 
	{
		pExpr = NULL;
	}

	if NOTEMPTY(pR->pRef)
	{
		pRef = new hCreference(devHndl());
		pRef->duplicate(pR->pRef, replicate);
	}
	else 
	{
		pRef = NULL;
	}
	return;
};	

void  hCreference::setEqual(void* pAclass)
{
	aCreference* paRef = (aCreference*) pAclass;
	*this = *paRef;
}


void hCreference::clear(void) 
{	rtype      = 0;
	isRef      = 0;
	id         = 0;
	type       = 0L;
		subindex   = 0;
	iName      = 0;
	exprType   = eT_Unknown;
	grpTrail.clear();
	RAZE(pExpr);//pExpr      = NULL; // assume others have handled destruction
	RAZE(pRef); //pRef       = NULL; // assume others have handled destruction
};

hCreference& hCreference::operator=(const aCreference& cr)
{	rtype      = cr.rtype;
	isRef      = cr.isRef;
	id         = cr.id;
	type       = cr.type;
		subindex   = cr.subindex;
	iName      = cr.iName;
	exprType   = cr.exprType;

	if ( isRef || (rtype == rT_Image)     || (rtype == rT_Separator) || 
		          (rtype == rT_Constant)  || (rtype == rT_Row_Break) )
	{
		if (cr.pExpr != NULL)
		{
			if (pExpr == NULL) // does not exist
			{
				pExpr  = new hCexpression(devHndl());
			}// else overwrite
			assert(pExpr->expressionL.size() == 0);
			*pExpr = *cr.pExpr;
			assert(pExpr->expressionL.size() == cr.pExpr->expressionL.size());
		}
		else 
		{
			RAZE(pExpr);
		}

		if (cr.pRef != NULL)
		{
			if (pRef == NULL) // does not exist
			{
				pRef  = new hCreference(devHndl());
			}// else overwrite
			*pRef = *cr.pRef;
		}
		else 
		{
			RAZE(pRef);
		}
	}
	else // nota reference
	{
		pExpr    = NULL;
		pRef     = NULL;
	}
if ((rtype != rT_Image) && (rtype != rT_Separator) && (rtype != rT_Constant) && (rtype != rT_Row_Break) )
{
	if ( (pRef!=NULL) ^ (pExpr!=NULL) ) 
	{
		if ( (cr.pRef!=NULL) ^ (cr.pExpr!=NULL) )
		{
			clog << "aReference with un balanced construction.-Src Error "<<endl;
		}
		else
		{
			cerr << "Reference with un balanced construction."<<endl;
		}
	}
}
#ifdef _DEBUG
if ( cr.pExpr != NULL && (rtype == rT_via_Collect || rtype == rT_via_File))
{
	if ( cr.pExpr->expressionL.size() != 1 || pExpr->expressionL.size() != 1 )
	{
		LOGIT(CLOG_LOG,"A %s reference with %d expression elements.\n",
			rtype.getTypeStr(),cr.pExpr->expressionL.size());
	}
}

#endif
	return *this;
};	

bool hCreference::operator==(const hCreference& ri)
{
	if ( rtype != ((hCreference)ri).rtype.getType() ) return false;
	if ( isRef != ri.isRef ) return false;
	if ( id    != ri.id    ) return false;
	if ( type.getType()  != ((hCreference)ri).type.getType()  ) return false;
	if ( subindex != ri.subindex ) return false;
	if ( iName    != ri.iName )    return false;
	if ( exprType != ri.exprType ) return false;

	if ( pExpr != NULL && ri.pExpr != NULL )// both not null
	{
		if ( ! ( (*pExpr) == (*(ri.pExpr))) ) // same as expr != expr
		{
			return false;
		}
	}
	else if (pExpr != ri.pExpr)// not both null
	{
		return false; // one null, one not
	}
	
	if ( pRef != NULL && ri.pRef != NULL )// both not null
	{
		if ( ! ( (*pRef) == (*ri.pRef)) ) // recurse, same as ref != ref
		{
			return false;
		}
	}
	else if (pRef != ri.pRef)// not both null
	{
		return false; // one null, one not
	}
	return true;
}


// resolve this reference (self) into an item id, then look up the ptr by symbol number
RETURNCODE hCreference::resolveID( hCitemBase*& r2pReturnedPtr, bool withStrings)
{
	RETURNCODE rc = SUCCESS;	
	itemID_t        itemNum = 0;
	hCitemBase*     pItem = NULL;

	if ( ( rc = resolveID(itemNum, withStrings) ) == SUCCESS)
	{
		if (itemNum == 0 ) // a constant
		{
			r2pReturnedPtr = 0;
			rc = SUCCESS;
		}
		else
		if ( devPtr()->getItemBySymNumber(itemNum, &pItem) == SUCCESS )
		{
			r2pReturnedPtr = pItem;
			rc = SUCCESS;
		}
		else
		{
			cerr << "ERROR: reference could not get item number 0x" << hex << itemNum <<dec<< endl;
			rc = APP_RESOLUTION_ERROR;
			r2pReturnedPtr = NULL;
		}
	}
	else
	{
		cerr << "ERROR: reference could not resolve the item number."<<endl;
		rc = APP_RESOLUTION_ERROR;
	}


	return rc;
}

// returns an error code & value < 0 upon error 
//       or SUCCESS & zero itemID if constant,           returns the item id otherwise
RETURNCODE hCreference::resolveID( itemID_t&   returnedID, bool withStrings )
{
	RETURNCODE rc = FAILURE;

	itemID_t     retVal = 0;
	returnedID   = -1; // error return value

	try{ // Deepak 112504

	if ( ! isRef || ( isRef && id != 0) )
	{
		returnedID = id;
		if (grpTrail.isEmpty())
		{
			grpTrail.itmID = id;
			rc = devPtr()->getItemBySymNumber(id, &(grpTrail.itmPtr));
			if ( rc == SUCCESS && grpTrail.itmPtr != NULL )
			{
				grpTrail.containerNumber = NOT_PART_OF_GROUP;
				if ( withStrings )
				{
					grpTrail.itmPtr->Label(grpTrail.lablStr);
					grpTrail.itmPtr->Help (grpTrail.helpStr);
				}
				// leave all the pointers empty
			}
			else
			{
				LOGIT(CERR_LOG,"ERROR: symbol 0x%04x failed to get an item pointer.\n",id);
				grpTrail.itmID  = 0;
				grpTrail.itmPtr = NULL;
			}
		}// else - this won't change.. leave the original fetches	
#ifdef DBG_REFTRAIL
		clog<<"REFTRAIL: entry resolution "<< ((isRef)?"IS REFERENCE":"IS DIRECT") 
			<<" with an ID = 0x"<< hex << ((int)id) << dec << endl
			<<"--------:" << endl;
#endif
		return SUCCESS;
	}
	// else do the function
	}//try
	catch(...)
	{
		cerr<<"ResolveID Inside catch(...) \n"<<endl;
	} // Deepak 112504

	return rc;
}



void hCreference::addUniqueUInt(UIntList_t& intlist, UINT32 newInt)
{
	for (UIntList_t::iterator pI=intlist.begin();pI < intlist.end();pI++)
	{
		if (*pI == newInt)  return; // duplicate
	}
	intlist.push_back(newInt);// does not exist, add
}

/* referenced item ids for all expression values used with all reference'd source items */
RETURNCODE hCreference::resolveAllIDs(UIntList_t&      allPossibleIDsReturned, bool stopAtID)
{
	RETURNCODE rc = FAILURE;
	return rc;
}

RETURNCODE hCreference::resolveAllIDs(vector<hCitemBase*>& allPossiblePtrsReturned)
{
	RETURNCODE rc = SUCCESS;	
	return rc;
}
#ifdef ADD_DEBUG
extern int IsCmd;
#endif




/* these have to be after expression declaration */
inline
RETURNCODE hCreference::destroy(void) 
{ 
/*Vibhor 050404: Start of Code*/
	try{
	if ( pExpr != NULL )
	{ /*for now, just delete*/ 
		/* test */pExpr->destroy();
		RAZE(pExpr);
	} 
	}
	catch(...)
	{
		cerr <<"TempLog: Caught hCreference::destroy(void) in RAZE(pExpr)inside catch(...)"<<endl;
	}

	if ( pRef != NULL ) 
	{ 
		try
		{
			pRef->destroy();  
		}
		catch(...)
		{
			cerr <<"TempLog: Caught hCreference::destroy(void) in pRef->destroy();inside catch(...)"<<endl;

		}
		try
		{
			RAZE(pRef);
		}
		catch(...)
		{
			cerr <<"TempLog: Caught hCreference::destroy(void) in RAZE(pRef)inside catch(...)"<<endl;

		}
	
	
	}
	try
	{
	//VMKP 260304
	grpTrail.clear();
		
	}
	catch(...)
	{
		cerr<<"inline RETURNCODE hCreference::destroy(void) grpTrail.clear(); inside catch(...) !!" <<endl;
	}
	return SUCCESS;

/*Vibhor 050404: End of Code*/
};

/*Vibhor 243004: Start of Code*/
/*Note: this code has simply been moved from Doc to this place*/
RETURNCODE hCreference::getItemLabel(wstring &strLabel)
{
	RETURNCODE rc = SUCCESS;
	wstring strTemp;
	hCitemBase *pItemBase = NULL;

	rc = resolveID(pItemBase);

	if(rc == SUCCESS && NULL != pItemBase)
	{
		strTemp = L"";
	
		if (getRefType() == rT_via_Collect  ||	getRefType() == rT_viaItemArray )
		{	// rules
			// go down the pSource till the next is empty
			// for each if desc not empty, overwrite current
			// when done take what was found and add a space and this desc
			hCvarGroupTrail* pVGT = getGroupTrailPtr()->pSourceTrail;// our first ref
			while ( pVGT != NULL )
			{
				if ( ! (pVGT->containerDesc.empty()) )
				{// overwrite
					strTemp = pVGT->containerDesc;
				}// if it's empty, NO ACTION
				pVGT = pVGT->pSourceTrail;
			}
			if ( ! (getGroupTrailPtr()->containerDesc.empty()) && !(strTemp.empty()))
			{
				strTemp += L" ";// delimiter, 275 uses a space, wally wanted a dot
						//                            too bad Wally...
			}// else we have nothing to separate from
			if ( ! (getGroupTrailPtr()->containerDesc.empty()) )
			{
				strTemp += getGroupTrailPtr()->containerDesc;
			}
			rc = SUCCESS;
		}
		if ( strTemp.empty() ) // ended up with nothing (or it ain't groupItem material)
		{
			rc = pItemBase->Label(strTemp);
		}
			
/* stevev 04aug06 - changed the below to the above **************************						
			strTemp = getGroupTrailPtr()->pSourceTrail->containerDesc;
			if ( (! (strTemp.empty())) && strTemp != " " )
			{
				if ( ! (getGroupTrailPtr()->containerDesc.empty()) )
				{
					strTemp += " ";// delimiter, 275 uses a space, wally wanted a dot
							//                            too bad Wally...
					strTemp += getGroupTrailPtr()->containerDesc;
				}
					rc = SUCCESS;
			}
			else
			{// the source is empty, use the item
				rc = pItemBase->Label(strTemp);
			}
		}
		else
		{
			rc = pItemBase->Label(strTemp);
		}
****** end   stevev 04aug06 ***********************************************/
    	strLabel = strTemp;
	}
	else
	{
		strLabel = L" "; // This should never be reached !!!!!
		clog<<"getItemLabel : Couldn't resolve the reference ... returning blank label"<<endl;

	}
	return rc;

}/*end getItemLabel()*/

/*Vibhor 243004: End of Code*/

/* stevev 25oct04 - new for eddl - bit-enum and array should be the same here */
RETURNCODE hCreference::resolveExpr(CValueVarient& retValue)
{	
	RETURNCODE rc = SUCCESS;
	hCexpressionTrail* pExprTrail = NULL;

	if ( pExpr == NULL )
	{ 
		retValue.clear();
		rc = APP_RESOLUTION_ERROR;
	} 
	else
	if ( getRefType() == rT_via_Attr )
	{
		itemID_t      refVal = 0;
		CValueVarient attrNum{};
		
		if ( isRef && pExpr != NULL && pExpr->expressionL.size() > 0)
		{	
			// stevev 22aug06 - changed technique to get # and which 
			unsigned which = 0; 
			if (pExpr->expressionL.begin()->exprElemType == eet_INTCST )
			{
				attrNum = pExpr->expressionL.begin()->expressionData;
				which   = pExpr->expressionL.begin()->dependency.which;// already shifted..30aug06 >> 16;// it's in the upper word
			}
			else
			{// incorrect encoding of attribute reference
				
				LOGIT(CERR_LOG|CLOG_LOG,"Incorrect Coding of Attribute-Reference.\n"); 
				rc = FAILURE;
			}
			// was pre 22aug06::  attrNum = pExpr->resolve(&pExprTrail,false);
			
			if ( rc == SUCCESS && attrNum.vIsValid  && pRef != NULL ) 
			{
				rc = SUCCESS; 
				#ifdef DBG_REFTRAIL
				if (pRef->isaRef())
				{
					clog<<    "------- reference Expression via_attribute isaRef "
								"w/ id = 0x"<<hex << ((int)pRef->getID()) <<dec;
				}
				clog<<"          with a pRef (resolving)" << endl;	
				#endif	
				rc = pRef->resolveID(refVal);
				hCvarGroupTrail* pRefGrpTrail = pRef->getGroupTrailPtr();

				if ( rc == SUCCESS  && pRefGrpTrail != NULL && pRefGrpTrail->itmPtr != NULL)
				{// we have an itemID of the Item we are targetting
					if (devPtr() == NULL)
					{
						cerr <<"ERROR: No device to dereference with."<<endl;
						rc = FAILURE;
					}
					else
					{	
						rc = FAILURE;
					}// endelse - have a device
				}
				else
				{// the reference could not be resolved with a trail
					cerr << "Ref Expression Failure Attr 1."<<endl;
					rc = APP_PROGRAMMER_ERROR;
				}
			}
			else 
			{
				cerr << "Ref Expression Failure Attr 2."<<endl;
				rc = FAILURE;
			}

		}
		else
		{// error
			cerr << "Ref Expression Failure Attr 3."<<endl;
			rc = FAILURE;
		}
	}
	else
	{
		rc = FAILURE;
	}
	return rc;
}


/*************** hCdepends **************************************************/
/*
 *
 *
 */

void hCdepends::setDuplicate(hCdepends& rSrc)
{
	useOptionVal = rSrc.useOptionVal;
	which        = rSrc.which;
	depRef.duplicate(&(rSrc.depRef), false);// expressions are not dup'd
}

/*************** hCexpressElement *******************************************/
/*
 *
 *
 */

hCexpressElement& hCexpressElement::operator=(const hCexpressElement& srcElem)
{   exprElemType  = srcElem.exprElemType; 
	expressionData= srcElem.expressionData;
	hCdepends* pD = (hCdepends*)(&srcElem.dependency);// sjv 5jul06 - convert to non-const
	dependency    = *pD; return *this;
}

void hCexpressElement::setDuplicate(hCexpressElement& rSrc)
{
	exprElemType   = rSrc.exprElemType;
	expressionData = rSrc.expressionData;
	dependency.setDuplicate(rSrc.dependency);
}

bool hCexpressElement::operator==(const hCexpressElement& ri)
{
	if (exprElemType != ri.exprElemType) return false;
	if ( ! ( expressionData == ri.expressionData)) return false;
	if ( ! ( dependency == ri.dependency) ) return false;
	return true;
}

/*************** hCexpression ***********************************************/
/*
 *
 *
 */

hCexpression :: hCexpression(const hCexpression& src) : hCobject( src.devHndl() )
{
	lastError    = SUCCESS;
//XPX	executionStack.clear();

	entryCnt = 0;
#ifdef _DEBUG
	if (src.entryCnt > 0 )
	{
		LOGIT(CLOG_LOG,"Copy constructor for Expression with an entry count!\n");
	}
#endif
	operator=(src);
}


hCexpression :: ~hCexpression() 
{
//XPX	executionStack.clear(); 

	expressionL.clear();
}

RETURNCODE hCexpression :: destroy(void)
{
	RETURNCODE rc = SUCCESS;
	hRPNexpress_t::iterator exprPos;
/*Vibhor 050404: Start of Code*/
	try
	{
		for (exprPos = expressionL.begin();exprPos < expressionL.end();exprPos++)
		{// exprPos isa ptr 2a hCexpressElement
			rc |= exprPos->destroy();
		}
//XPX	executionStack.clear();
	}
	catch(...)
	{
		cerr<<"RETURNCODE hCexpression :: destroy(void) inside catch(...) !!"<<endl;
	}
	return rc;
}

hCexpression& hCexpression :: operator=(const hCexpression& srcAExpr) 
{
	hCexpressElement wrkElem(devHndl());
	hRPNexpress_t* pL  = (hRPNexpress_t*)(&srcAExpr.expressionL);//sjv 5jul06 make nn-const
	for(hRPNexpress_t::iterator iT = pL->begin();
	    iT != pL->end();++iT)
	{//it is a ptr 2 aCexpressElement
		wrkElem = (*iT);
		expressionL.push_back(wrkElem); 
		wrkElem.clear();
	}		
	return *this;						
};


bool hCexpression :: operator==(hCexpression& ei)// removed const from arg
{
	bool ret;
	hRPNexpress_t::iterator myExprElem;
	hRPNexpress_t::iterator eiExprElem;//ptr to exprElem

	if ( expressionL.size() != ei.expressionL.size() )
	{// not equal
		ret = false;// different
	}
	else
	{// same number of items
		if ( expressionL.size() == 0 ) return true;// they are empty so equal

		for(myExprElem = expressionL.begin(),eiExprElem= ei.expressionL.begin();
		    myExprElem!= expressionL.end() &&eiExprElem!= ei.expressionL.end(); 
			++myExprElem,++eiExprElem)
		{
			if ( (*myExprElem) == (*eiExprElem) )
			{// we are still equal
				ret = true;
			}
			else
			{	ret = false;
			}
			if ( ! ret ) 
				break;// out of for loop to return false
		}
	}

	return ret;
}




RETURNCODE hCexpression :: pull_back(CValueVarient& returnVarient, varientList_t& actvStk)
{
	RETURNCODE rc = SUCCESS;

//XPX	if (executionStack.size() < 1)
	try//DEEPAK 251104	
	{
	if (actvStk.size() < 1)
	{
		cerr << "ERROR:  expression execution has an opcode with no operands."<<endl;
		rc = APP_EXPRESSION_ERR;
	}
	else
	{// we will always use one
//XPX		returnVarient = executionStack.back();
		returnVarient = actvStk.back();
//XPX		executionStack.pop_back();
		actvStk.pop_back();
		if (returnVarient.vType != CValueVarient::isBool       && 
			returnVarient.vType != CValueVarient::isIntConst   && 
			returnVarient.vType != CValueVarient::isFloatConst &&
			returnVarient.vType != CValueVarient::isString     )
		{
			LOGIT(CLOG_LOG,"ERROR: expression execution has an unusable operand of type %s and value of: %s\n",
				varientTypeStrings[returnVarient.vType],((wstring)returnVarient).c_str());
			/* stevev 10/13/04 */
			returnVarient.vType = CValueVarient::isBool;
			returnVarient = false;
			returnVarient.vIsValid = false;
			/* end stevev 10/13/04 */
// don't pop 'em twice		executionStack.pop_back();
			rc = APP_EXPRESSION_ERR;
		}// else continue, all ok
	}
	}//try
	catch(...)
	{
		cerr<<"Inside catch(...) PULL_BACK\n"<<endl;
	}
//DEEPAK 251104
	return rc;
}


CValueVarient hCexpression :: execUnary(expressElemType_t Opcode,         CValueVarient& op1)
{
	CValueVarient rVar{};
	return rVar;
}




RETURNCODE hCexpression :: toBoolT(CValueVarient& returnVarient, CValueVarient& op1, CValueVarient& op2)
{
	return SUCCESS;
}

/* this is expected to only be called for indexes or collections
   if it's a constant, add it and use it
   if it's a variable, verify it's an index and get all valid values
   otherwise - error
**/
// hCexpressionTrail:
//	bool        isVar;		// else is constant
//	itemID_t	srcID;		// @  isVar::  symbol number of the INDEX variable
//	int			srcIdxVal; 

//RETURNCODE hCexpression :: resolveAllIndexes(vector<UINT32>& valueList)// for array expressions
RETURNCODE hCexpression :: resolveAllIndexes(ExprTrailList_t& exprValueList)
{// essentially a regular resolve using a valid value list for variable references
	RETURNCODE rc = SUCCESS;
	return rc;
}


RETURNCODE hCexpression :: resolveIndex(hCreference* pArrayRef, hCindex*& r2pIndex)
{
	RETURNCODE rc = SUCCESS;
	return rc;
}

/*    set this as a duplicate of the passed-in expression */
void  hCexpression :: setDuplicate(hCexpression& rSrc)
{
	hCexpressElement locExprEle(devHndl());
//XPX	executionStack.clear();
	expressionL.clear();
	for (hRPNexpress_t::iterator iT = rSrc.expressionL.begin(); iT < rSrc.expressionL.end();++iT)
	{// iT isa ptr2a hCexpressElement
		locExprEle.setDuplicate(*(iT));
		expressionL.push_back(locExprEle);
		locExprEle.clear();
	}
}


void  hCexprPayload::setEqual(void* pAclass)
{// aCminmaxVal
	aCExpressionPayload* pAcExprPay = (aCExpressionPayload*)pAclass;
// self is a hCexpression
	hCexpression::operator=(pAcExprPay->theExpression) ;
}

void  hCexprPayload::duplicate(hCpayload* pPayLd, bool replicate)
{
	hCexprPayload* pExprPay = (hCexprPayload*)pPayLd;

	hCexpression::setDuplicate(*((hCexpression*)pExprPay)) ;
}

RETURNCODE  hCexprPayload::setValue(CValueVarient newValue)
{
	RETURNCODE rc = SUCCESS;
	hCexpressElement wrkElem(devHndl());

	switch(newValue.vType)
	{	
	case CValueVarient::isIntConst:
		wrkElem.exprElemType = eet_INTCST;
		break;
	case CValueVarient::isFloatConst:
		wrkElem.exprElemType = eet_FPCST;
		break;
	/*isBool,
	  isOpcode,
	  isDepIndex,
	  isString,
	  isSymID  */
	default:
		wrkElem.exprElemType = eeT_Unknown;
		rc = APP_TYPE_UNKNOWN;
		break;
	}// endswitch

	wrkElem.expressionData = newValue;

//  self is a hCexpression
	expressionL.push_back(wrkElem);

	return rc;
}

hCexprPayload& hCexprPayload::operator=(const hCexprPayload& srcExpr)
{
	*((hCexpression*)this) = (hCexpression)srcExpr;
	return *this;
}



/*************************************************************************************************
 *
 *   $History: ddbReference.cpp $
 * 
 * *****************  Version 3  *****************
 * User: Stevev       Date: 5/05/03    Time: 10:21a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * Milestone: SDC sends and recieves a command zero. Xmtr automatically
 * handles commands.
 * 
 * *****************  Version 2  *****************
 * User: Stevev       Date: 4/09/03    Time: 6:28a
 * Updated in $/DD Tools/DDB/ddbRead/ddbLib
 * updated header and footer as per HART coding spec.
 * 
 *************************************************************************************************
 */
