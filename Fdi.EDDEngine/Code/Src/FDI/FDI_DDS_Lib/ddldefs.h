// MyDdlDefs.h
/*
 *	@(#) $Id: ddldefs.h,v 1.6 2012/09/12 22:40:55 rgretta Exp $
 *  Copyright 1992 Rosemount, Inc., all rights reserved.
 */

#include "nsEDDEngine/Ddldefs.h"

#pragma once


#define DIAGNOSTIC_ROOT_MENU				_T("diagnostic_root_menu")
#define PROCESS_VARIABLES_ROOT_MENU			_T("process_variables_root_menu")
#define DEVICE_ROOT_MENU					_T("device_root_menu")
#define OFFLINE_ROOT_MENU					_T("offline_root_menu")
#define DEVICE_ICON_IMAGE					_T("device_icon")
#define RESPONSE_CODE_ITEM_NAME				_T("std_ResponseCode")
#define ACTIONS_EXECUTION_STATUS_ITEM_NAME	_T("var_SumReqResponse_1") 


typedef unsigned long	ITEM_ID;
typedef unsigned short	ITEM_TYPE;
typedef unsigned short  ITEM_SIZE;
typedef unsigned long	DDL_UINT;	/* Unparsed integer from binary */
typedef unsigned long long	DDL_UINT_LONG;	/* Unparsed integer from binary */
typedef	unsigned long	OFFSET;


#define INVALID_ITEM_ID  0L

// Parse integer macro for FF only
/*  
 *  Parse integer macro.
 *  This macro can handle parsing simple (one byte) integers.
 *  If the Most Significant Bit of CHUNK is set, the integer occupies
 *  multiple bytes.  In this case "ddl_parse_integer_func()" should
 *  be called.
 */
#define DDL_PARSE_INTEGER(C,L,V)    \
    {   \
        if (**(C) & 0x80) { \
            rc = ddl_parse_integer_func((C), (L), (V)); \
            if (rc != DDL_SUCCESS)  \
                return rc;  \
        } else {    \
            if ((V) != 0) {*(V) = **(C);}   \
            ++(*(C));   \
            --(*(L));   \
        }   \
    }

#define DDL_PARSE_INTEGER_LONG(C,L,V)    \
    {   \
        if (**(C) & 0x80) { \
            rc = ddl_parse_integer_long_func((C), (L), (V)); \
            if (rc != DDL_SUCCESS)  \
                return rc;  \
        } else {    \
            if ((V) != 0) {*(V) = **(C);}   \
            ++(*(C));   \
            --(*(L));   \
        }   \
    }


/*
 * Major and minor DDOD Binary revision numbers.
 * These are used by the Tokenizer and DDS to form a Version number:
 *
 * 		<major ddod revision>.<minor ddod revision>.<software revision>
 *
 * The major and minor binary revision numbers are assigned here. 
 * The software revision numbers are assigned independently by the
 * Tokenizer and DDS in separate header files.
 */
#define DDOD_REVISION_MAJOR		4
#define DDOD_REVISION_MINOR		0

/*
 * Value assigned to unused table offsets in the Block Directories.
 */
#define UNUSED_OFFSET   ((OFFSET)~0)

/*
 *  Item Types.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the item_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */
#define	RESERVED_ITYPE1					nsEDDEngine::ITYPE_NO_VALUE
#define VARIABLE_ITYPE 	       			nsEDDEngine::ITYPE_VARIABLE
#define COMMAND_ITYPE					nsEDDEngine::ITYPE_COMMAND
#define MENU_ITYPE						nsEDDEngine::ITYPE_MENU
#define EDIT_DISP_ITYPE					nsEDDEngine::ITYPE_EDIT_DISP
#define METHOD_ITYPE					nsEDDEngine::ITYPE_METHOD
#define REFRESH_ITYPE					nsEDDEngine::ITYPE_REFRESH
#define UNIT_ITYPE						nsEDDEngine::ITYPE_UNIT
#define WAO_ITYPE 	       				nsEDDEngine::ITYPE_WAO
#define ITEM_ARRAY_ITYPE				nsEDDEngine::ITYPE_ITEM_ARRAY
#define COLLECTION_ITYPE				nsEDDEngine::ITYPE_COLLECTION
#define	BLOCK_B_ITYPE					nsEDDEngine::ITYPE_BLOCK_B
#define BLOCK_ITYPE						nsEDDEngine::ITYPE_BLOCK
#define RECORD_ITYPE					nsEDDEngine::ITYPE_RECORD
#define ARRAY_ITYPE						nsEDDEngine::ITYPE_ARRAY
#define VAR_LIST_ITYPE					nsEDDEngine::ITYPE_VAR_LIST
#define RESP_CODES_ITYPE				nsEDDEngine::ITYPE_RESP_CODES
#define MEMBER_ITYPE					nsEDDEngine::ITYPE_MEMBER
#define FILE_ITYPE						nsEDDEngine::ITYPE_FILE
#define CHART_ITYPE						nsEDDEngine::ITYPE_CHART
#define GRAPH_ITYPE						nsEDDEngine::ITYPE_GRAPH
#define AXIS_ITYPE						nsEDDEngine::ITYPE_AXIS
#define WAVEFORM_ITYPE					nsEDDEngine::ITYPE_WAVEFORM
#define SOURCE_ITYPE					nsEDDEngine::ITYPE_SOURCE
#define LIST_ITYPE						nsEDDEngine::ITYPE_LIST
#define GRID_ITYPE						nsEDDEngine::ITYPE_GRID
#define	IMAGE_ITYPE						nsEDDEngine::ITYPE_IMAGE
#define BLOB_ITYPE						nsEDDEngine::ITYPE_BLOB
#define PLUGIN_ITYPE					nsEDDEngine::ITYPE_PLUGIN
#define	TEMPLATE_ITYPE					nsEDDEngine::ITYPE_TEMPLATE
#define	COMPONENT_ITYPE					nsEDDEngine::ITYPE_COMPONENT
#define	COMPONENT_FOLDER_ITYPE			nsEDDEngine::ITYPE_COMPONENT_FOLDER
#define	COMPONENT_REFERENCE_ITYPE		nsEDDEngine::ITYPE_COMPONENT_REFERENCE
#define	COMPONENT_REL_ITYPE				nsEDDEngine::ITYPE_COMPONENT_RELATION

// These ITYPEs don't identify ITEMs, but are used to identify types of references.
#define SEPARATOR_ITYPE					nsEDDEngine::ITYPE_SEPARATOR
#define ROWBREAK_ITYPE					nsEDDEngine::ITYPE_ROWBREAK
#define COLUMNBREAK_ITYPE				nsEDDEngine::ITYPE_COLUMNBREAK
#define ENUM_BIT_ITYPE					nsEDDEngine::ITYPE_ENUM_BIT
#define STRING_LITERAL_ITYPE			nsEDDEngine::ITYPE_STRING_LITERAL
#define CONST_INTEGER_ITYPE				nsEDDEngine::ITYPE_CONST_INTEGER
#define	CONST_FLOAT_ITYPE				nsEDDEngine::ITYPE_CONST_FLOAT
#define CONST_UNSIGNED_ITYPE			nsEDDEngine::ITYPE_CONST_UNSIGNED
#define CONST_DOUBLE_ITYPE				nsEDDEngine::ITYPE_CONST_DOUBLE
#define	SELECTOR_ITYPE					nsEDDEngine::ITYPE_SELECTOR
#define	LOCAL_PARAM_ITYPE				nsEDDEngine::ITYPE_LOCAL_PARAM
#define	METH_ARGS_ITYPE					nsEDDEngine::ITYPE_METH_ARGS
#define ATTR_ITYPE                      nsEDDEngine::ITYPE_ATTR
#define BLOCK_XREF_ITYPE				nsEDDEngine::ITYPE_BLOCK_XREF

#define MAX_ITYPE						nsEDDEngine::ITYPE_MAX	/* must be last in list */	


/*
 *	Special object type values
 */

#define FORMAT_OBJECT_TYPE		128
#define DEVICE_DIR_TYPE			129
#define BLOCK_DIR_TYPE			130

 /* these additional ITYPES are used by resolve to build resolve trails */

#define PARAM_ITYPE             200
#define PARAM_LIST_ITYPE        201
#define BLOCK_CHAR_ITYPE        202

#define AMORPHOUS_ARRAY_ITYPE   250
/*
 * Types of variables.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the var_type_str in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define DDS_TYPE_UNUSED nsEDDEngine::VT_DDS_TYPE_UNUSED
#define INTEGER 		nsEDDEngine::VT_INTEGER
#define UNSIGNED 		nsEDDEngine::VT_UNSIGNED
#define FLOAT 			nsEDDEngine::VT_FLOAT
#define FLOATG_PT 		nsEDDEngine::VT_FLOAT
#define DOUBLE 			nsEDDEngine::VT_DOUBLE
#define DOUBLEG_PT 		nsEDDEngine::VT_DOUBLE
#define ENUMERATED 		nsEDDEngine::VT_ENUMERATED
#define BIT_ENUMERATED 	nsEDDEngine::VT_BIT_ENUMERATED
#define INDEX 			nsEDDEngine::VT_INDEX
#define ASCII 			nsEDDEngine::VT_ASCII
#define PACKED_ASCII 	nsEDDEngine::VT_PACKED_ASCII
#define PASSWORD 		nsEDDEngine::VT_PASSWORD
#define BITSTRING 		nsEDDEngine::VT_BITSTRING
#define HART_DATE_FORMAT	nsEDDEngine::VT_EDD_DATE
#define TIME 			nsEDDEngine::VT_TIME
#define DATE_AND_TIME 	nsEDDEngine::VT_DATE_AND_TIME
#define DURATION 		nsEDDEngine::VT_DURATION
#define EUC				nsEDDEngine::VT_EUC
#define OCTETSTRING		nsEDDEngine::VT_OCTETSTRING
#define VISIBLESTRING	nsEDDEngine::VT_VISIBLESTRING
#define TIME_VALUE		nsEDDEngine::VT_TIME_VALUE
#define OBJECT_REFERENCE	nsEDDEngine::VT_OBJECT_REFERENCE
#define BOOLEAN_T		nsEDDEngine::VT_BOOLEAN
#define iMAX_TYPE		(nsEDDEngine::VT_BOOLEAN+1)	/* must be last in list */

/*
 * Types of members.
 *	WARNING- If you change this list in ANY way, you MUST
 *	change the mem_type in symbol.c also! These defines
 *	index into an array, so they should be consecutive.
 */

#define	COLLECTION_MEM		1
#define	PARAM_MEM			2
#define	PARAM_LIST_MEM		3
#define	VAR_LIST_MEM		4
#define	RECORD_MEM			5
#define	CHARACTERISTIC_MEM  6	/* must be last item in list */

#define MEMBER_STRINGS  {\
	"",					/*required since items start at 1*/\
	"collection",\
	"parameter",\
	"parameter-list",\
	"variable-list",\
	"record",\
	"file",\
	"chart",\
	"source",\
	"graph",\
	"characteristic",\
	NULL}		/*array must be NULL terminated for searches*/
#define NO_ATTRTAG		0xffff

#define MEMBER2ATTRTAG { NO_ATTRTAG, COLLECTION_MEMBERS_ID, NO_ATTRTAG, NO_ATTRTAG,\
		VAR_LIST_MEMBERS_ID, RECORD_MEMBERS_ID, FILE_MEMBERS_ID, CHART_MEMBERS_ID, \
		SOURCE_MEMBERS_ID, GRAPH_MEMBERS_ID, BLOCK_CHARACTERISTIC_ID, NO_ATTRTAG}


/*
 * Handling of variables.
 */
#define READ_HANDLING_BIT			0
#define WRITE_HANDLING_BIT			1

#define READ_HANDLING 				(1<<READ_HANDLING_BIT)
#define WRITE_HANDLING 				(1<<WRITE_HANDLING_BIT)

/*
 * Ordinary status classes of bit enumerated variables.
 */

#define HARDWARE_STATUS				0x000001
#define SOFTWARE_STATUS				0x000002
#define PROCESS_STATUS				0x000004
#define MODE_STATUS					0x000008
#define DATA_STATUS					0x000010
#define MISC_STATUS					0x000020
#define EVENT_STATUS				0x000040
#define STATE_STATUS				0x000080
#define SELF_CORRECTING_STATUS		0x000100
#define CORRECTABLE_STATUS			0x000200
#define UNCORRECTABLE_STATUS		0x000400
#define SUMMARY_STATUS				0x000800
#define DETAIL_STATUS				0x001000
#define MORE_STATUS					0x002000
#define COMM_ERROR_STATUS			0x004000
#define IGNORE_IN_TEMPORARY_STATUS	0x008000
#define BAD_OUTPUT_STATUS			0x010000
//#define IGNORE_IN_HOST_STATUS		0x020000	// Not used in PROFIBUS
//#define ERROR_STATUS				0x040000	// Not used in PROFIBUS
//#define WARNING_STATUS			0x080000	// Not used in PROFIBUS
//#define INFO_STATUS				0x100000	// Not used in PROFIBUS


/*
 * Variable Object Reference values
 */

#define OBJ_REF_ROOT		0
#define OBJ_REF_PARENT		1
#define OBJ_REF_CHILD		2
#define OBJ_REF_SELF		3
#define OBJ_REF_NEXT		4
#define OBJ_REF_PREV		5
#define OBJ_REF_FIRST		6
#define OBJ_REF_LAST		7


/*
 * Data item types.
 */

#define DATA_CONSTANT 					0
#define DATA_REFERENCE 					1
#define DATA_REF_FLAGS					2
#define DATA_REF_WIDTH					3
#define DATA_REF_FLAGS_WIDTH			4
#define DATA_FLOATING 					5
#define DATA_STRING						6
#define DATA_UNSIGNED					7
#define DATA_DOUBLE						8

/*
 * Command data item flags.
 */

#define INFO_DATA_ITEM 					0x0001
#define INDEX_DATA_ITEM 				0x0002

/*
 * data item flags.
 */

#define WIDTH_PRESENT 					0x8000

/*
 * Menu item flags.
 */

#define READ_ONLY_ITEM 					0x01
#define DISPLAY_VALUE_ITEM 				0x02
#define REVIEW_ITEM 					0x04
#define NO_LABEL_ITEM					0x08
#define NO_UNIT_ITEM					0x10
#define INLINE_ITEM						0x20		/* only valid for IMAGEs */
#define HIDDEN_ITEM						0x40		// New for PROFIBUS

#define FF_READ_ONLY_ITEM 				0x0001
#define FF_DISPLAY_VALUE_ITEM 			0x0002
#define FF_REVIEW_ITEM 					0x0004
#define FF_HIDDEN_ITEM					0x0008
#define FF_NO_LABEL_ITEM				0x0010
#define FF_NO_UNIT_ITEM					0x0020
#define FF_SEPARATOR_ITEM				0x0040
#define FF_STRING_ITEM					0x0080
#define FF_IMAGE_ITEM					0x0100
#define FF_INLINE_ITEM					0x0200
#define FF_ROWBREAK_ITEM				0x0400
#define FF_COLUMNBREAK_ITEM				0x0800
#define FF_BIT_ENUM_WITH_MASK_ITEM		0x1000


/*
 * Menu ENTRY type
 *		New for PROFIBUS
 */
#define ROOT_ENTRY_TYPE			1

/*
 * Menu PURPOSE types
 *		New for PROFIBUS
 */
#define CATALOG_PURPOSE_TYPE		0x0001
#define DIAGNOSE_PURPOSE_TYPE		0x0002
#define LOAD_TO_APP_PURPOSE_TYPE	0x0004
#define LOAD_TO_DEV_PURPOSE_TYPE	0x0008
#define MENU_PURPOSE_TYPE			0x0010
#define PROCESS_VALUE_PURPOSE_TYPE	0x0020
#define SIMULATION_PURPOSE_TYPE		0x0040
#define TABLE_PURPOSE_TYPE			0x0080


/*
 * Menu and Method ACCESS type
 *		New for PROFIBUS
 */
//#define ONLINE_ACCESS			0
//#define OFFLINE_ACCESS			1

/*
 * Method type flags
 */
#define SCALING_TYPE_METHOD			0x00000001
#define USER_TYPE_METHOD			0x00000010

/*
 * Method type
 */
#define TYPE_VOID   	0
#define TYPE_INT__8		1	/* signed char  */
#define TYPE_INT_16		2	/* short        */
#define TYPE_INT_32		3	/* int & long   */
#define TYPE_FLOAT		4	/* FLOAT    */
#define TYPE_DOUBLE		5	/* DOUBLE_FLOAT */
#define TYPE_UINT__8 	6
#define TYPE_UINT_16 	7
#define TYPE_UINT_32	8
#define TYPE_INT_64 	9	/* currently unsupported */
#define TYPE_UINT_64	10	/* currently unsupported */
#define TYPE_DDSTRING	11
#define TYPE_DD_ITEM    12


/*
 * Method parameter details
 */

#define ARRAYFLAG   	1	/* isArray - is declared '[]' or '[' chain-expr ']' */
#define REFECFLAG		2	/* isReference - ( has a '&' in front of it */
#define CONSTFLAG		4	/* isConst - for posterity */


/*
 * Chart types.
 */

#define GAUGE_CTYPE			1
#define HORIZ_BAR_CTYPE		2
#define SCOPE_CTYPE			3
#define STRIP_CTYPE			4
#define SWEEP_CTYPE			5
#define VERT_BAR_CTYPE		6

#define FF_CHART_TYPE_GAUGE            0
#define FF_CHART_TYPE_HORIZONTAL_BAR   1
#define FF_CHART_TYPE_SCOPE            2
#define FF_CHART_TYPE_STRIP            3
#define FF_CHART_TYPE_SWEEP            4
#define FF_CHART_TYPE_VERTICAL_BAR     5

/*
 * Display sizes.
 */

#define XX_SMALL_DISPSIZE	1
#define X_SMALL_DISPSIZE	2
#define SMALL_DISPSIZE		3
#define MEDIUM_DISPSIZE		4
#define LARGE_DISPSIZE		5
#define X_LARGE_DISPSIZE	6
#define XX_LARGE_DISPSIZE	7

#define FF_DISPLAY_SIZE_XX_SMALL         0
#define FF_DISPLAY_SIZE_X_SMALL          1
#define FF_DISPLAY_SIZE_SMALL            2
#define FF_DISPLAY_SIZE_MEDIUM           3
#define FF_DISPLAY_SIZE_LARGE            4
#define FF_DISPLAY_SIZE_X_LARGE          5
#define FF_DISPLAY_SIZE_XX_LARGE         6

/*
 * Waveform types.
 */

#define YT_WAVEFORM_TYPE	1
#define XY_WAVEFORM_TYPE	2
#define HORZ_WAVEFORM_TYPE	3
#define VERT_WAVEFORM_TYPE	4

#define FF_WAVEFORM_TYPE_XY            0
#define FF_WAVEFORM_TYPE_YT            1
#define FF_WAVEFORM_TYPE_HORIZONTAL    2
#define FF_WAVEFORM_TYPE_VERTICAL      3

/*
 * LINE types.
 */

#define DATA_LINETYPE		1
#define LOWLOW_LINETYPE		2
#define LOW_LINETYPE		3
#define HIGH_LINETYPE		4
#define HIGHHIGH_LINETYPE	5
#define TRANSPARENT_LINETYPE 6

#define DDS_LINE_TYPE_DATA_N           16  /* all higher values are reserved */

/*
 * Grid Orientation.
 */

#define ORIENT_VERT			1
#define ORIENT_HORIZ		2

#define FF_ORIENT_VERT      0
#define FF_ORIENT_HORIZ     1

/*
 * SCALE types.
 */

#define LINEAR_SCALE		1
#define LOG_SCALE			2

#define FF_LINEAR_SCALE		0
#define FF_LOG_SCALE		1

/*
 * HALFPOINT types.
 */

#define X_VALUE				1
#define Y_VALUE				2

/* SOURCE constants */
#define LINE_COLOR_DEFAULT  0xffffffff

///////////////////////////////////////////////////////////////////////////////

/*
 * Table Identifiers for DDOD Directory Objects.
 * These are also used as bit positions for the
 * Directory Table Masks defined below.
 */

#define	BLOCK_NAME_SIZE		32	/* This is the max size of a block name */

/* DEVICE DIRECTORY tables */

#define	STRING_TBL_ID				 	0
#define	DICT_REF_TBL_ID				 	1
#define IMAGE_TBL_ID					2
#define ITEM_TO_COMMAND_TBL_ID			3
#define SYMBOL_TBL_ID					4
#define	ITEM_TBL_ID					 	5
#define	BLK_TBL_ID			 			6
#define	COMMAND_NUMBER_TBL_ID			7

#define	MAX_DEVICE_TBL_ID			 	8


#define CRIT_PARAM_TBL_ID				10
#define	BLK_ITEM_TBL_ID				 	11
#define	BLK_ITEM_NAME_TBL_ID		 	12

#define PARAM_TBL_ID				 	13
#define	PARAM_MEM_TBL_ID			 	14
#define	PARAM_MEM_NAME_TBL_ID			15
#define PARAM_ELEM_TBL_ID				16
// FD_FDI added
#define PARAM_LIST_TBL_ID				17
#define PARAM_LIST_MEM_TBL_ID			18
#define PARAM_LIST_MEM_NAME_TBL_ID		19

#define	CHAR_MEM_TBL_ID				 	20	// In PROFIBUS Binary, but not used
#define CHAR_MEM_NAME_TBL_ID			21	// In PROFIBUS Binary, but not used

#define	REL_TBL_ID					 	22
#define UPDATE_TBL_ID					23


#define	MAX_BLOCK_TBL_ID				24	// 	must be last in list

// Default refresh action cycle time
#define DEFAULT_REFRESH_CYCLE_TIME		1000
/*
 *	Directory Table Masks.
 */

/* DEVICE DIRECTORY table masks */

#define	BLK_TBL_MASK					(1<<BLK_TBL_ID)
#define	ITEM_TBL_MASK					(1<<ITEM_TBL_ID)
#define	STRING_TBL_MASK					(1<<STRING_TBL_ID)
#define	DICT_REF_TBL_MASK				(1<<DICT_REF_TBL_ID)
//#define	LOCAL_VAR_TBL_MASK				(1<<LOCAL_VAR_TBL_ID)
#define IMAGE_TBL_MASK					(1<<IMAGE_TBL_ID)
//#define DICT_STRING_TBL_MASK			(1<<DICT_STRING_TBL_ID)
#define SYMBOL_TBL_MASK					(1<<SYMBOL_TBL_ID)

#define ITEM_TO_COMMAND_TBL_MASK		(1<<ITEM_TO_COMMAND_TBL_ID)
#define COMMAND_NUMBER_TBL_MASK			(1<<COMMAND_NUMBER_TBL_ID) 


#define DEVICE_TBL_MASKS		BLK_TBL_MASK				| \
								ITEM_TBL_MASK				| \
								STRING_TBL_MASK				| \
								DICT_REF_TBL_MASK			| \
								IMAGE_TBL_MASK				| \
								SYMBOL_TBL_MASK				| \
								ITEM_TO_COMMAND_TBL_MASK	| \
								COMMAND_NUMBER_TBL_MASK		

/* BLOCK DIRECTORY table masks */

#define	BLK_ITEM_TBL_MASK				(1<<BLK_ITEM_TBL_ID)
#define	BLK_ITEM_NAME_TBL_MASK			(1<<BLK_ITEM_NAME_TBL_ID)
#define PARAM_TBL_MASK					(1<<PARAM_TBL_ID)
#define	PARAM_MEM_TBL_MASK				(1<<PARAM_MEM_TBL_ID)
#define PARAM_MEM_NAME_TBL_MASK			(1<<PARAM_MEM_NAME_TBL_ID)
#define PARAM_ELEM_TBL_MASK				(1<<PARAM_ELEM_TBL_ID)
#define PARAM_LIST_TBL_MASK				(1<<PARAM_LIST_TBL_ID)
#define PARAM_LIST_MEM_TBL_MASK			(1<<PARAM_LIST_MEM_TBL_ID)
#define PARAM_LIST_MEM_NAME_TBL_MASK	(1<<PARAM_LIST_MEM_NAME_TBL_ID)
#define	CHAR_MEM_TBL_MASK				(1<<CHAR_MEM_TBL_ID)
#define CHAR_MEM_NAME_TBL_MASK			(1<<CHAR_MEM_NAME_TBL_ID)
#define	REL_TBL_MASK					(1<<REL_TBL_ID)
#define	UPDATE_TBL_MASK					(1<<UPDATE_TBL_ID)
#define CRIT_PARAM_TBL_MASK				(1<<CRIT_PARAM_TBL_ID)

#define PARAM_LIST_TBL_MASK					(1<<PARAM_LIST_TBL_ID)
#define PARAM_LIST_MEM_TBL_MASK				(1<<PARAM_LIST_MEM_TBL_ID)
#define PARAM_LIST_MEM_NAME_TBL_MASK			(1<<PARAM_LIST_MEM_NAME_TBL_ID)


#define BLOCK_TBL_MASKS			BLK_ITEM_TBL_MASK | \
								BLK_ITEM_NAME_TBL_MASK | \
								PARAM_TBL_MASK | \
								PARAM_MEM_TBL_MASK | \
								PARAM_MEM_NAME_TBL_MASK | \
								PARAM_ELEM_TBL_MASK | \
								CHAR_MEM_TBL_MASK | \
								CHAR_MEM_NAME_TBL_MASK | \
								REL_TBL_MASK | \
								UPDATE_TBL_MASK | \
								CRIT_PARAM_TBL_MASK | \
								PARAM_LIST_TBL_MASK | \
								PARAM_LIST_MEM_TBL_MASK | \
								PARAM_LIST_MEM_NAME_TBL_MASK



