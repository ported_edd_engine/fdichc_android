
/**
 *	@(#) $Id: cm_dds.cpp,v 1.34 1996/06/26 15:53:33 gabravi Exp $
 *	Copyright 1993 Rosemount, Inc. - All rights reserved
 *
 *	cm_dds.c - Connection Manager DD Support Module
 *
 *	This file contains all functions pertaining to DD
 *	Support in the Connection Manager.
 *
 */


#include "stdinc.h"
#include "cm_dds.h"
#include "tags_sa.h"
#include "sysproto.h"
#include "fmscrypt.h"
#include <Inf/NtcSpecies/Ntcassert2.h>

#include "HARTBreaker.h"
#include "dev_type.h"
#include "ConnectionMgr.h"
#include "DeviceTypeMgr.h"
#include "HART/HARTEDDEngine/DDSSupport.h"

////////////////////////////////////////////////////////////////////////
//
//	Tracing Disabled 
//
#pragma message("Tracing Disabled")
#undef tracef
#define tracef // 
////////////////////////////////////////////////////////////////////////




/*
 *	Function Pointers
 */

static int (* const build_dd_dev_tbl_fn_ptr) P((ENV_INFO *, DEVICE_HANDLE)) = ddi_build_dd_dev_tbls;
static int (* const build_dd_blk_tbl_fn_ptr) P((ENV_INFO *, BLOCK_HANDLE)) = ddi_build_dd_blk_tbls;


inline BOOL FileExists(LPCTSTR fname) { return PS_GetFileAttributes(const_cast<wchar_t *>(fname)) != 0xFFFFFFFF; }

extern int calc_item_ddod_object_data_size(ROD_HANDLE rhandle,
                                           UINT8* pExtension,
                                           ITEM_TYPE itemType,
                                           UINT32* pTotal);


/***********************************************************************
 *
 *	Name:  cm_get_integer
 *
 *	ShortDesc:  Get integer at specified address and size.
 *
 *	Inputs:
 *		bin_ptr - pointer to the binary data.
 *		bin_size - size of the binary data.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		integer value of the binary data.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static UINT32
cm_get_integer( UINT8 *bin_ptr, int bin_size)

{
	UINT32	value = 0;
	int		byte_num;

	/*
	 *	Go through each byte and total up the integer value.
	 */

	for (byte_num = 0; byte_num < bin_size; byte_num ++) {

		value <<= 8;
		/* Codecenter thinks values which equal 0xbf have not been set*/
		/*SUPPRESS 113*/
		value += (UINT32)bin_ptr[byte_num];
	}

	return(value);
}



////////////////////////////////////////////////////////////////////////
//
//	Name:  IsBackwardCompatibilityCrossed
//
//	ShortDesc:  Figures out if backward compatibility is crossed per SRS 331.
//
//	Description:
//		The IsBackwardCompatibilityCrossed function tells if backward compatibility
//		is crossed. 
//
//	Inputs:
//		nDeviceHartRev		- the device HART rev
//		nDDHartRev			- the DD HART rev
//
//	Outputs:
//		None
//
//	Returns:
//		TRUE  if backward compatibility is crossed.
//		FALSE if backward compatibility is not crossed.
//
//	Author:
//		Ying Xu (5/3/2001)
//
////////////////////////////////////////////////////////////////////////

BOOL IsBackwardCompatibilityCrossed(int nDeviceHartRev, int nDDHartRev)
{
	BOOL bRetVal =TRUE;
	switch (nDeviceHartRev)
	{
	case 7:
		switch (nDDHartRev)
		{
		case 7:
		case 6:
		case 5:
			bRetVal =FALSE;
			break;
		case 4:
		case 3:
		case 2:
			bRetVal =TRUE;
			break;
		default:
			ntcassert2(false);
			break;
		}
		break;
	case 6:
	case 5:
		switch (nDDHartRev)
		{
		case 6:
		case 5:
			bRetVal =FALSE;
			break;
		case 7:
		case 4:
		case 3:
		case 2:
			bRetVal =TRUE;
			break;
		default:
			ntcassert2(false);
			break;
		}
		break;
	case 4:
	case 3:
	case 2:
		switch (nDDHartRev)
		{
		case 4:
		case 3:
		case 2:
			bRetVal =FALSE;
			break;
		case 7:
		case 6:
		case 5:
			bRetVal =TRUE;
			break;
		default:
			ntcassert2(false);
			break;
		}
		break;
	default:
		ntcassert2(false);
		break;

	}
	
	return bRetVal;
}


#undef MAX_FILE_LENGTH
#undef MAX_FILE_COUNT


/***********************************************************************
 *
 *	Name:  ds_get_header
 *
 *	ShortDesc:  Get the DDOD file header from the DDOD file.
 *
 *	Description:
 *		The function takes a DDOD file and gets all of
 *		the header information.
 *
 *	Inputs:
 *		file - the DDOD file pointer.
 *
 *	Outputs:
 *		header - the DDOD file header information. 
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

 int
ds_get_header(
	FILE * file,
	DDOD_HEADER *header)
{
	UINT8			header_buffer[HEADER_SIZE];
	int				r_code = 0;

	/*
	 *	Get the DDOD Header into the buffer.
	 */


		/*
	 *	Read the DDOD Header into the buffer.
	 */

	r_code = fread(header_buffer, 1, HEADER_SIZE, file);
	if (r_code != HEADER_SIZE) 
	{
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the DDOD Header, and perform verification
	 *	where applicable.
     */

	header->magic_number =
			cm_get_integer(
			&header_buffer[MAGIC_NUMBER_OFFSET],
			MAGIC_NUMBER_SIZE);

	if (header->magic_number != cm_get_integer (
	        (unsigned char*)magicNumberPlaintext, magicNumberSize)) {
		return(CM_FILE_ERROR);
	}

	header->header_size =
			cm_get_integer(
			&header_buffer[HEADER_SIZE_OFFSET],
			HEADER_SIZE_SIZE);

	header->objects_size =
			cm_get_integer(
			&header_buffer[OBJECTS_SIZE_OFFSET],
			OBJECTS_SIZE_SIZE);

	header->data_size =
			cm_get_integer(
			&header_buffer[DATA_SIZE_OFFSET],
			DATA_SIZE_SIZE);

	header->manufacturer =
			cm_get_integer(
			&header_buffer[MANUFACTURER_OFFSET],
			MANUFACTURER_SIZE);

	header->device_type =
			(UINT16) cm_get_integer(
			&header_buffer[DEVICE_TYPE_OFFSET],
			DEVICE_TYPE_SIZE);

	header->device_revision =
			(UINT8) cm_get_integer(
			&header_buffer[DEVICE_REV_OFFSET],
			DEVICE_REV_SIZE);

	header->dd_revision =
			(UINT8) cm_get_integer(
			&header_buffer[DD_REV_OFFSET],
			DD_REV_SIZE);

	header->lit80_major_rev =
			(UINT8) cm_get_integer(
			&header_buffer[LIT80_MAJOR_REV_OFFSET],
			LIT80_MAJOR_REV_SIZE);

	header->lit80_mminor_rev =
			(UINT8) cm_get_integer(
			&header_buffer[LIT80_MINOR_REV_OFFSET],
			LIT80_MINOR_REV_SIZE);

	header->reserved1 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED1_OFFSET],
			RESERVED1_SIZE);

	header->reserved2 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED2_OFFSET],
			RESERVED2_SIZE);

	header->reserved3 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED3_OFFSET],
			RESERVED3_SIZE);

	header->reserved4 =
			(UINT32) cm_get_integer(
			&header_buffer[RESERVED4_OFFSET],
			RESERVED4_SIZE);

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_od_description
 *
 *	ShortDesc:  Get the OD Description from the DDOD file.
 *
 *	Description:
 *		The ds_get_od_description function takes a DDOD file and gets
 *		the Object Dictionary (OD) description.
 *
 *	Inputs:
 *		file_view - the DDOD file's memory mapped view object.
 *
 *	Outputs:
 *		od_description - the OD description of the DDOD.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

static int
ds_get_od_description(
	FILE             *file,
	OD_DESCRIPTION_SPECIFIC *od_description)


{
    UINT8		od_description_buffer[ODES_SIZE];

	int r_code = 0;
	/*
	 *	Read the OD Description into the buffer.
	 */

    r_code = fread(od_description_buffer, 1, ODES_SIZE, file);
    if (r_code != ODES_SIZE) 
	{
		return(CM_FILE_ERROR);
	}

    /*
	 *	Get each field in the OD Description.
     */

	od_description->ram_rom_flag =
			(UINT8) cm_get_integer(
			&od_description_buffer[ROM_RAM_FLAG_OFFSET],
			ROM_RAM_FLAG_SIZE);

	od_description->name_length =
			(UINT8) cm_get_integer(
			&od_description_buffer[NAME_LENGTH_OFFSET],
			NAME_LENGTH_SIZE);

	od_description->access_protection_flag =
			(UINT8) cm_get_integer(
			&od_description_buffer[ACCSS_PROTECT_OFFSET],
			ACCSS_PROTECT_SIZE);

	od_description->version =
			(UINT16) cm_get_integer(
			&od_description_buffer[VERSION_OD_OFFSET],
			VERSION_OD_SIZE);

	od_description->stod_object_count =
			(UINT16) cm_get_integer(
			&od_description_buffer[STOD_LENGTH_OFFSET],
			STOD_LENGTH_SIZE);

	od_description->sod_first_index =
			(OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_SOD_OFFSET],
			FIRST_IN_SOD_SIZE);

	od_description->sod_object_count =
			(UINT16) cm_get_integer(
			&od_description_buffer[SOD_LENGTH_OFFSET],
			SOD_LENGTH_SIZE);

	od_description->dvod_first_index =
			(OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DVOD_OFFSET],
			FIRST_IN_DVOD_SIZE);

	od_description->dvod_object_count =
			(UINT16) cm_get_integer(
			&od_description_buffer[DVOD_LENGTH_OFFSET],
			DVOD_LENGTH_SIZE);

	od_description->dpod_first_index =
			(OBJECT_INDEX) cm_get_integer(
			&od_description_buffer[FIRST_IN_DPOD_OFFSET],
			FIRST_IN_DPOD_SIZE);

	od_description->dpod_object_count =
			(UINT16) cm_get_integer(
			&od_description_buffer[DPOD_LENGTH_OFFSET],
			DPOD_LENGTH_SIZE);

	return(CM_SUCCESS);
}


int CRodMgr::ds_get_image_data(FILE * file,	DDOD_HEADER* header, ROD_HANDLE rod_handle)
{
	int r_code;
	
	UINT32 image_data_offset = header->header_size +
								header->objects_size +
								header->data_size;
	

	fseek(file, 0, SEEK_END);
	UINT32 size = ftell(file) - image_data_offset;

    if (size <0)
	{
		return(CM_FILE_ERROR);
	}
	
	BINARY_IMAGE *image_data;
	image_data = (BINARY_IMAGE *) rod_calloc(1,sizeof(BINARY_IMAGE));
	if (!image_data) {
		return(CM_NO_ROD_MEMORY);
	}
	
	if (size >0)
	{
		image_data->data = (unsigned char *) rod_calloc(size, sizeof(unsigned char));
		if (!image_data->data)
		{
			return(CM_NO_ROD_MEMORY);
		}
		image_data->length = size;

		r_code = fseek(file, (long)image_data_offset, SEEK_SET);
		if (r_code != 0) 
		{
			return(CM_FILE_ERROR);
		}
		if (fread (image_data->data,1,size, file) != size)
		{
			return(CM_FILE_ERROR);
		}
	}

	r_code = rod_put_image_data(image_data, rod_handle);

	return(CM_SUCCESS);
}

/***********************************************************************
 *
 *	Name:  ds_get_object
 *
 *	ShortDesc:  Get an object from the DDOD file.
 *
 *	Description:
 *		The ds_get_object function takes a DDOD file and ROD handle
 *		and gets an object from the file and stores it into the
 *		corresponding ROD.
 *
 *	Inputs:
 *		file_view - the DDOD file's memory mapped view object.
 *		rod_handle - the handle of the ROD in which the object will
 *					 be stored.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_NO_ROD_MEMORY.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::ds_get_object(ENV_INFO* env_info, FILE * file,	ROD_HANDLE  rod_handle)
{
	UINT8		 object_buffer[FIXED_SIZE];
	OBJECT		*object;
	UINT8		 extension_length_buffer[EXTEN_LENGTH_SIZE];
	UINT8		 extension_length;
	int			 r_code;

	/*
	 *	Allocate space in the ROD heap for an object, and for a
	 *	domain object's specific information.
	 */

	object = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
	if (!object) {
		return(CM_NO_ROD_MEMORY);
	}

	object->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object->specific) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Get the fixed fields of the (domain) object into the buffer.
	 */


		/*
	 *	Read the fixed fields of the (domain) object into the buffer.
	 */

    r_code = fread(object_buffer, 1, FIXED_SIZE, file);
    if (r_code != FIXED_SIZE) 
	{
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get each field in the (domain) object, and perform
	 *	verification where applicable.
	 */

	object->index =
			(OBJECT_INDEX) cm_get_integer(
			&object_buffer[OBJECT_INDEX_OFFSET],
			OBJECT_INDEX_SIZE);

	object->code =
			(UINT8) cm_get_integer(
			&object_buffer[OBJECT_CODE_OFFSET],
			OBJECT_CODE_SIZE);

	if (object->code != OC_DOMAIN) {
		return(CM_FILE_ERROR);
	}

	object->specific->domain.size =
			(UINT16) cm_get_integer(
			&object_buffer[MAX_OCTETS_OFFSET],
			MAX_OCTETS_SIZE);

	object->access.password =
			(UINT8) cm_get_integer(
			&object_buffer[PASSWORD_OFFSET],
			PASSWORD_SIZE);

	object->access.groups =
			(UINT8) cm_get_integer(
			&object_buffer[ACCESS_GROUP_OFFSET],
			ACCESS_GROUP_SIZE);

	object->access.rights =
			(UINT16) cm_get_integer(
			&object_buffer[ACCESS_RIGHTS_OFFSET],
			ACCESS_RIGHTS_SIZE);

	object->specific->domain.local_address =
			(UINT32) cm_get_integer(
			&object_buffer[LOCAL_ADDRESS_OFFSET],
			LOCAL_ADDRESS_SIZE);

	object->specific->domain.state =
			(UINT8) cm_get_integer(
			&object_buffer[DOMAIN_STATE_OFFSET],
			DOMAIN_STATE_SIZE);

	object->specific->domain.upload_state =
			(UINT8) cm_get_integer(
			&object_buffer[UPLOAD_STATE_OFFSET],
			UPLOAD_STATE_SIZE);

	object->specific->domain.usage =
			(UINT8) cm_get_integer(
			&object_buffer[COUNTER_OFFSET],
			COUNTER_SIZE);

	/*
	 *	Read the extension length field of the (domain) object
	 *	into the buffer.
	 */


    r_code = fread(extension_length_buffer, 1, EXTEN_LENGTH_SIZE, file);
    if (r_code != EXTEN_LENGTH_SIZE) 
	{
		return(CM_FILE_ERROR);
	}

	/*
	 *	Get the extension length field and allocate space in the
	 *	ROD heap for the (domain) object's extension.
	 */

	extension_length =
		(UINT8) cm_get_integer(
		&extension_length_buffer[EXTEN_LENGTH_OFFSET],
		EXTEN_LENGTH_SIZE);

	object->extension = (UINT8 *) rod_malloc( 
			(int)(EXTEN_LENGTH_SIZE + extension_length));
	if (!object->extension) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the first field in the extension to the length, and
	 *	read in the remaining part of the extension.
	 */

	object->extension[EXTEN_LENGTH_OFFSET] = extension_length;

	r_code = fread((object->extension + 1), 1, (int)extension_length, file);
	if (r_code != extension_length) 
	{
		return(CM_FILE_ERROR);
	}

	/*
	 *	Put the (domain) object into the ROD.
	 */

	r_code = rod_put(rod_handle,object->index,object);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	
	//Instead of using size of this directory header to determin if this is tokenizer 6 or 8
	//just get the tokenizer version from rod which is already set.
	bool bTok8 = false;
	int tok_rev = rod_get_tok_major_rev(rod_handle);
	if (tok_rev == 8)
	{
		bTok8 = true;
	}

	if (object->specific->domain.size == 0xFFFF &&  object->extension[ITEM_TYPE_OFFSET] == DEVICE_DIR_TYPE)
	{
		UINT32 total_dir_tbl_size = 0;
		UINT8* data_ptr = NULL;
		UINT32 tbl_size = 0;

		if (!bTok8)
		{
			data_ptr = object->extension + BLK_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;
		}

		if (bTok8)
		{
			data_ptr = object->extension + ITEM_REF_OFFSET_8;
		}
		else
		{
			data_ptr = object->extension + ITEM_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
						    (unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		if (!bTok8)
		{
			data_ptr = object->extension + PROG_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;
		}

		if (!bTok8)
		{
			data_ptr = object->extension + DOMAIN_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;
		}

		if (bTok8)
		{
			data_ptr = object->extension + STRING_REF_OFFSET_8;
		}
		else
		{
			data_ptr = object->extension + STRING_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
						    (unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		if (bTok8)
		{
			data_ptr = object->extension + DICT_REF_REF_OFFSET_8;
		}
		else
		{
			data_ptr = object->extension + DICT_REF_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
						    (unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		if (!bTok8)
		{
			data_ptr = object->extension + LOCAL_VAR_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;
		}

		if (bTok8)
		{
			data_ptr = object->extension + CMD_NUM_ID_REF_OFFSET_8;
		}
		else
		{
			data_ptr = object->extension + CMD_NUM_ID_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
						    (unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		if (bTok8)
		{
			data_ptr = object->extension + IMAGE_REF_OFFSET_8;
		}
		else
		{
			data_ptr = object->extension + IMAGE_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
						    (unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		object->specific->domain.size = total_dir_tbl_size;
	}
	else if (object->specific->domain.size == 0xFFFF &&  object->extension[ITEM_TYPE_OFFSET] == BLOCK_DIR_TYPE)
	{
		UINT32 total_dir_tbl_size = 0;
		UINT8* data_ptr = NULL;
		UINT32 tbl_size = 0;

		//calculate our starting position
		//
		//Add the size of ITEM TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + BLK_ITEM_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + BLK_ITEM_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the ITEM_NAME TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + BLK_ITEM_NAME_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + BLK_ITEM_NAME_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the PARAM TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + PARAM_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + PARAM_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//the following is NOT created by the version 8 tokenzier
		if( !bTok8 )//version 6 ONLY!
		{
			//
			//Add the size of the PARAM MEMBER TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_MEM_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the PARAM MEMBER_NAME TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_MEM_NAME_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the PARAM_ELEMENT TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_ELEM_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the PARAM_LIST TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_LIST_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the PARAM_LIST_MEMBER TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_LIST_MEM_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the PARAM_LIST_MEMBER_NAME TABLE (used for only 6  )
			//
			data_ptr = object->extension + PARAM_LIST_MEM_NAME_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the CHARACTERISTIC_MEMBER TABLE (used for only 6  )
			//
			data_ptr = object->extension + CHAR_MEM_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;

			//
			//Add the size of the CHARACTERISTIC_MEMBER_NAME TABLE (used for only 6  )
			//
			data_ptr = object->extension + CHAR_MEM_NAME_REF_OFFSET;
			tbl_size = (((unsigned long) data_ptr[4]) << 24) |
							  (((unsigned long) data_ptr[5]) << 16) |
							  (((unsigned long) data_ptr[6]) << 8 ) |
								(unsigned long) data_ptr[7];
			total_dir_tbl_size += tbl_size;
		}//this is the end of TOKENIZER 6 ONLY tables

		//
		//Add the size of the RELATION TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + REL_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + REL_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the UPDATE TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + UPDATE_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + UPDATE_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the COMMAND TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + COMMAND_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + COMMAND_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the CRITICAL TABLE (used for both 6 and 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			data_ptr = object->extension + CRIT_PARAM_REF_OFFSET_8;
		}
		else//version 6 tokenizer offset
		{
			data_ptr = object->extension + CRIT_PARAM_REF_OFFSET;
		}
		tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
		total_dir_tbl_size += tbl_size;

		//
		//Add the size of the RESOLVE REFERENCE TABLE (used for 8 )
		//
		if( bTok8 )//version 8 tokenizer offset
		{
			if(extension_length > RESLV_REF_OFFSET_8)
			{
				data_ptr = object->extension + RESLV_REF_OFFSET_8;

				tbl_size = (((unsigned long) data_ptr[4]) << 24) |
						  (((unsigned long) data_ptr[5]) << 16) |
						  (((unsigned long) data_ptr[6]) << 8 ) |
							(unsigned long) data_ptr[7];
				total_dir_tbl_size += tbl_size;
			}
		}
		

		//OK we have calculated the extended size of the block dir table.  
		//Now store that extended size into the domain size.
		object->specific->domain.size = total_dir_tbl_size;
	}
	else if (object->specific->domain.size == 0xFFFF && object->extension[ITEM_TYPE_OFFSET] < MAX_ITYPE)
	{
            UINT32 iCalcSize = 0;
            r_code = calc_item_ddod_object_data_size(rod_handle,
                                                     object->extension,
                                                     object->extension[ITEM_TYPE_OFFSET],
                                                     &iCalcSize);
            if (r_code != SUCCESS)
            {
                EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART",
                    L"Error (%d) while calculating the extended domain size for extension type = %d", r_code, object->extension[ITEM_TYPE_OFFSET] );
                return r_code;
            }

            object->specific->domain.size = iCalcSize;
	}

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_get_object_value
 *
 *	ShortDesc:  Get an object's value.
 *
 *	Description:
 *		The ds_get_object_value function takes a DDOD file, DDOD file
 *		header, and object and gets the objects value.  It will attach
 *		the object's value to the object (both being in the ROD).
 *
 *	Inputs:
 *		file_view - the DDOD file's memory mapped view object.
 *		header - the header of the DDOD file.
 *		object - the object whose value will be retrieved.
 *
 *	Outputs:
 *		None.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *		CM_NO_ROD_MEMORY.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/

int CRodMgr::ds_get_object_value(FILE * file, DDOD_HEADER *header, OBJECT *object)
{
	UINT32		 offset;
	UINT8		*domain_data;

	int r_code = 0;

	/*
	 *	Check if there is no object to get a value for, or no
	 *	domain data to get.
	 */

	if (!object) {
		return(CM_SUCCESS);
	}
	if (!object->specific) {
		return(CM_BAD_OBJECT);
	}
	if (!object->specific->domain.size) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}
	if (object->specific->domain.local_address == 0xFFFFFFFF) {
		object->value = (UINT8 *)0;
		return(CM_SUCCESS);
	}

	/*
	 *	Calculate the offset into the DDOD file where the domain
	 *	data is located.
	 */

	offset = object->specific->domain.local_address +
			header->header_size + header->objects_size;

	/*
	 *	Allocate space in the ROD heap for the domain data.
	 */

	domain_data =
			(UINT8 *) rod_malloc((int)object->specific->domain.size);
	if (!domain_data) {
		return(CM_NO_ROD_MEMORY);
	}

	/*
	 *	Set the file position and read in the domain data.
	 */

	r_code = fseek(file, (long)offset, SEEK_SET);
	if (r_code != 0) 
	{
		return(CM_FILE_ERROR);
	}
	r_code = fread(domain_data, 1,
			(int)object->specific->domain.size, file);
	
	if (r_code != (int)object->specific->domain.size) 
	{
		return(CM_FILE_ERROR);
	}
	/*
	 *	Connect the domain data to the ROD object.
	 */

	object->value = domain_data;

	return(CM_SUCCESS);
}


/***********************************************************************
 *
 *	Name:  ds_ddod_file_load
 *
 *	ShortDesc:  Load a DDOD file.
 *
 *	Description:
 *		The ds_ddod_file_load function takes a DDOD file name and loads
 *		the corresponding DDOD file into a ROD.
 *
 *	Inputs:
 *		filename - the name of the DDOD file that is to be loaded.
 *
 *	Outputs:
 *		rod_handle - the handle of the ROD in which the DDOD was
 *					 loaded.
 *
 *	Returns:
 *		CM_SUCCESS.
 *		CM_FILE_ERROR.
 *
 *	Authors:
 *		Rick Sharpe
 *		Jon Reuter
 *
 **********************************************************************/
	
int CRodMgr::ds_ddod_file_load( ENV_INFO* env_info, LPTSTR filename,	ROD_HANDLE *rod_handle)
{   
	DDOD_HEADER					 header;
	OBJECT						*object_0;
	OD_DESCRIPTION_SPECIFIC		*od_description;
	int							 object_num;
	int							 r_code;

	FILE *						file;
		//Locks the shared data in device type manager.
	CLockDeviceTypeMgr dtMgrLock;

	/*
	 *	Open the DDOD file and get the Header information.
	 */

    file = PS_wfopen((const wchar_t*)filename, L"rb");
	if (!file) {
		return(CM_BAD_FILE_OPEN);
	}


	r_code = ds_get_header(file, &header);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	
	/*
	 *	Allocate space in the ROD heap for an object, and set it up
	 *	as an OD description object.
	 */

	object_0 = (OBJECT *) rod_calloc(1,sizeof(OBJECT));
	if (object_0 == NULL) {
		return(CM_NO_ROD_MEMORY);
	}

	object_0->index = 0;
	object_0->code = OC_OD_DESCRIPTION;

	/*
	 *	Allocate space in the ROD heap for an OD description object's
	 *	specific information, and set it up to describe the DDROD.
	 */

	object_0->specific = (OBJECT_SPECIFIC *) rod_calloc(1,
			sizeof(OBJECT_SPECIFIC));
	if (!object_0->specific) {
		return(CM_NO_ROD_MEMORY);
	}
	od_description = (OD_DESCRIPTION_SPECIFIC *)object_0->specific;

	r_code = ds_get_od_description(file, od_description);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}

	/*
	 *	Open up the DDROD with object 0.
	 */
	/*
	 *	Allocate a new ROD and initialize its description.
	 */

	*rod_handle = rod_new_rod((LPWSTR)filename);

	if (*rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);

	}
	
	*rod_handle = rod_open(object_0,*rod_handle);
	if (*rod_handle < 0) {
		return(CM_BAD_ROD_OPEN);
	}

	//To support fms format
	CStdString sExt(filename);
	sExt.MakeLower();
	
	if (sExt.Find(CStdString(".") + FORMAT_5_EXT) >0)
	{
		r_code = rod_set_tok_major_rev(*rod_handle, 5);
	}
	else if (sExt.Find(CStdString(".") + FORMAT_6_EXT) >0)
	{
		r_code = rod_set_tok_major_rev(*rod_handle, 6);
	}
	else if (sExt.Find(CStdString(".") + FORMAT_8_EXT) >0)
	{
		r_code = rod_set_tok_major_rev(*rod_handle, 8);
	}
	/*
	 *	Load all of the objects in the S-OD.  Note that these will
	 *	all be domain objects.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object(env_info, file, *rod_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Load all of the object's values in the S-OD.  Note that these
	 *	will all be domain data.
	 */

	for (object_num = 0; object_num < od_description->sod_object_count;
			object_num ++) {
		r_code = ds_get_object_value(file, &header, 
				rod_tbl.list[*rod_handle]->sod[object_num]);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}
	//Load header
	r_code = rod_set_header(*rod_handle, header);
	if (r_code != CM_SUCCESS)
	{
		return(r_code);
	}	
	
	/* Load image data */

	if (sExt.Find(CStdString(".") + FORMAT_6_EXT) >0 || sExt.Find(CStdString(".") + FORMAT_8_EXT) >0)
	{
		r_code = ds_get_image_data(file, &header, *rod_handle);
		if (r_code != CM_SUCCESS)
		{
			return(r_code);
		}
	}

	r_code = fclose(file);
	if (r_code) {
		return(CM_BAD_FILE_CLOSE);
	}

	return(CM_SUCCESS);
}

int CConnectionMgr::ds_dd_block_load(
	ENV_INFO *env_info,
	BLOCK_HANDLE block_handle,
	ROD_HANDLE *rod_handle,
	int /* start_param_cache */)


{
	int			r_code;

	/*
	 *	Check to make sure that the block handle is valid.
	 */

	if (!valid_block_handle(block_handle)) {
		return(CM_BAD_BLOCK_HANDLE);
	}

	/*
	 *	Check to make sure that there is a DD Block ID.
	 */
	ITEM_ID temp_id = 0;
	get_abt_dd_blk_id( block_handle, &temp_id );
	if (temp_id == 0) 
	{
		return(CM_NO_DD_BLK_REF);
	}

	/*
	 *	Check if the DD has been loaded for the device.
	 */
	ROD_HANDLE temp_rod_handle = 0;
	get_abt_dd_handle( block_handle, &temp_rod_handle );
	if (!g_DeviceTypeMgr.valid_rod_handle(temp_rod_handle)) 
	{
		DEVICE_HANDLE dev_handle;
		get_abt_adt_offset( block_handle, &dev_handle );
		r_code = ds_dd_device_load(env_info, dev_handle,
				rod_handle);
		if (r_code != CM_SUCCESS) {
			return(r_code);
		}
	}

	/*
	 *	Build the DD block tables.
	 */

	if (build_dd_blk_tbl_fn_ptr != 0) 
	{
		r_code = (*build_dd_blk_tbl_fn_ptr)(env_info, block_handle);
	
		if (r_code) 
		{
			return(r_code);
		}
	}

	get_abt_dd_handle( block_handle, &temp_rod_handle );
	*rod_handle = temp_rod_handle;
	return CM_SUCCESS;
}

int CDeviceTypeMgr::ds_ddod_file_load( ENV_INFO* env_info, LPTSTR filename,	ROD_HANDLE *rod_handle)
{
	return m_RodMgr.ds_ddod_file_load(env_info,filename,rod_handle);
}

int CConnectionMgr::ds_dd_load( ENV_INFO *env_info,
	BLOCK_HANDLE bh, int start_param_cache)

{
	int			r_code;
	ROD_HANDLE	rod_handle = 0;
	FLAT_DEVICE_DIR		*flat_device_dirp ;
	BLK_TBL				*bt ;
	PARAM_TBL			*pt ;
	BLK_TBL_ELEM		*bt_elem ;
	DEVICE_HANDLE		dh ;
	DEVICE_TYPE_HANDLE	dth ;

	/* 
	 * 	Load the DD information corresponding to the
	 *  Device That was identified.
	 */	
 
 	if (!valid_block_handle(bh)) {
		return(CM_BAD_BLOCK_HANDLE) ;
	}

	get_abt_adt_offset( bh, &dh );

	if (!valid_device_handle(dh)) {
		return(CM_BAD_DEVICE_HANDLE) ;
	}
 	
 	dth = get_adt_adtt_offset(dh) ;

	if (!g_DeviceTypeMgr.valid_device_type_handle(dth)) {
		return(CM_BAD_DEVICE_TYPE_HANDLE) ;
	}

	r_code = ds_dd_device_load(env_info, dh, &rod_handle) ;
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}

	g_DeviceTypeMgr.get_adtt_dd_dev_tbls( dth, (void**)&flat_device_dirp );
	if (flat_device_dirp == NULL) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"ADTT_DD_DEV_TBLS returned NULL");
		return(CM_BAD_DD_DEV_LOAD) ;
	}

	/*
	 * Set the Item Id of the block	and clobber the block table
	 * offset so the block loader doesn't get confused.
	 */
	//
	//   There is a bug in the system where the active block dd table offset is
	//   being set to -1.   This cannot be!!! We will check it here and set it 
	//   to zero.   This is an extreme kludge!!!   Dave C on 2/9/96
	// 
	
	bt = &flat_device_dirp->blk_tbl ;
	int temp_offset = 0;
	get_abt_dd_blk_tbl_offset( bh, &temp_offset );
	bt_elem = BTE(bt,temp_offset) ;
	set_abt_dd_blk_id(bh, BTE_BLK_ID(bt_elem)) ;
	set_abt_dd_blk_tbl_offset(bh, -1) ;

	/*
	 *	Load the DD information correpsonding to the block.
	 */

	r_code = ds_dd_block_load(env_info, bh, &rod_handle, start_param_cache) ;
	if (r_code != CM_SUCCESS) {
		return(r_code) ;
	}
	
	/*
	 * Set the number of parameters in the block (using the number found
	 * in the DD).
	 */

	pt = BTE_PT(bt_elem) ;
	set_abt_param_count(bh, PT_COUNT(pt)) ;

	return(CM_SUCCESS);
}


int CConnectionMgr::ds_ddod_load( ENV_INFO *env_info, DEVICE_HANDLE	device_handle, ROD_HANDLE* rod_handle)
{
	//tracef("BEGIN: ds_ddod_load\n");

	
	int				r_code;

	DD_DEVICE_ID	temp_dd_device_id   =	{0};

	
	 	//Check to make sure that the device handle is valid.
	
	if(!valid_device_handle(device_handle)) {
		return CM_BAD_DEVICE_HANDLE;
	}

	
	 	//Obtain the device ID.
	DD_DEVICE_ID* dd_device_id = get_adt_dd_device_id( device_handle );
	
	
	(void) memcpy(&temp_dd_device_id, dd_device_id, sizeof(DD_DEVICE_ID));

	
		//Load the DDOD file into a ROD.
	
	(void) memcpy(dd_device_id, &temp_dd_device_id, sizeof(DD_DEVICE_ID));

	ROD_HANDLE temp_rod_handle = 0;
	r_code = g_DeviceTypeMgr.ds_ddod_file_load(env_info, dd_device_id->dd_path, &temp_rod_handle);



	if(r_code != CM_SUCCESS) return r_code;


	
	 //Obtain device type handle
	
	DEVICE_TYPE_HANDLE device_type_handle = get_adt_adtt_offset(device_handle) ;
	if(!g_DeviceTypeMgr.valid_device_type_handle(device_type_handle)) return CM_BAD_DEVICE_TYPE_HANDLE;

	
		//Update the compatibility flag in the Active Device Type Table.
	
	#define LOADED	dd_device_id->ddid_device_rev
	#define DESIRED	temp_dd_device_id.ddid_device_rev

	if(temp_dd_device_id.ddid_manufacturer == dd_device_id->ddid_manufacturer &&
		temp_dd_device_id.ddid_device_type == dd_device_id->ddid_device_type &&
		temp_dd_device_id.ddid_device_rev == dd_device_id->ddid_device_rev)
	{
		g_DeviceTypeMgr.set_adtt_compatibility(device_type_handle, CURRENT_DD_LOADED) ;
	}
	else
	{
		g_DeviceTypeMgr.set_adtt_compatibility(device_type_handle, OLDER_DD_LOADED) ;
	}

	
		//Put the DD Handle into the Active Device Type Table.

	set_adt_dd_handle(device_handle, &temp_rod_handle);

	*rod_handle = temp_rod_handle;

	//tracef("END: ds_ddod_load\n");
	
	return CM_SUCCESS;
}



int CConnectionMgr::ds_dd_device_load(
	ENV_INFO *env_info,
	DEVICE_HANDLE device_handle,
	ROD_HANDLE *rod_handle)
{
	#pragma warning(disable: 4100)
	int		r_code;
	
	/*
	 *	Check to make sure that the device handle is valid.
	 */

	if (!valid_device_handle(device_handle)) {
		return(CM_BAD_DEVICE_HANDLE);
	}

	/*
	 * Check to make sure the device tables need to be loaded.
	 */

	void *ddt;
	get_adt_dd_dev_tbls(device_handle, (void**)&ddt);

	if (ddt != NULL) {

	  	/*
		 *	Build any device type specific Application information.
		 */
		r_code = load_dev_type_info(env_info, get_adt_adtt_offset(device_handle));
		
		if (r_code) 
		{
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"Error reading DD error - %d", r_code);
			return(CM_BAD_DD_DEV_LOAD);
		}

  		return(CM_SUCCESS) ;
	}

	
	/*
	 *	Otherwise, load a DDOD.
	*/

	r_code = ds_ddod_load(env_info, device_handle, rod_handle);
	if (r_code != CM_SUCCESS) {
		return(r_code);
	}
	
	/*
	 *	Build the DD device tables.
	 */

	if (build_dd_dev_tbl_fn_ptr != 0) {
		r_code = (*build_dd_dev_tbl_fn_ptr)
				(env_info, get_adt_adtt_offset(device_handle));
		if (r_code) 
		{
			EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"Error reading DD error - %d", r_code);
			return(CM_BAD_DD_DEV_LOAD);
		}
	}


	/*
	 *	Build any device type specific Application information.
	 */

	r_code = load_dev_type_info(env_info, get_adt_adtt_offset(device_handle));
	
	if (r_code) 
	{
		EddEngineLog(env_info, __FILE__, __LINE__, BssError, L"LegacyHART", L"Error reading DD error - %d", r_code);
		return(CM_BAD_DD_DEV_LOAD);
	}

	return(CM_SUCCESS);
}


